
Public Class CommonErrorMessageHelper
    Public Const MESSAGES_NO_ACCESS As String = " Anda tidak mendapatkan hak akses halaman ini "
    Public Const MESSAGE_INSERT_FAILED As String = " A record already exists with the same primary key! "
    Public Const MESSAGE_INSERT_SUCCESS As String = " Record is added successfully! "
    Public Const MESSAGE_DELETE_SUCCESS As String = " Record is deleted successfully! "
    Public Const MESSAGE_DELETE_FAILED As String = " Unable to delete the selected record. Record already used! "
    Public Const MESSAGE_UPDATE_SUCCESS As String = " Record Is Update Successfully "
    Public Const MESSAGE_UPDATE_FAILED As String = " Record Is Update Failed "
    Public Const MESSAGE_GENERATE As String = " Case Template sudah tergenerate ..."
    Public Const MESSAGE_UNGENERATE As String = " Case Template belum tergenerate, tekan Bangun ..."
    Public Const MESSAGES_DOC_TARGET_CHANGED As String = " Dokumen Target tidak sama "
    Public Const MESSAGES_DATA_INCOMPLETE As String = " Data tidak lengkap "
    Public Const MESSAGES_FIELD_ALREADY_EXISTS As String = " Field sudah ada "
    Public Const MESSAGE_USER_FAILED As String = " Maaf, nama user salah "
    Public Const MESSAGE_PASS_FAILED As String = " Maaf, password salah "
    Public Const MESSAGE_IS_DEFAULT_EXIST As String = " Default=1 sudah dipunyai oleh DocumentID yang bersangkutan "
    Public Const MESSAGE_SCREEN_ID_DO_NOT_FILLED As String = " Screen ID jangan diisi "
    Public Const MESSAGE_DATA_DETAIL_NOT_FOUND As String = "Detail Data tidak diketemukan !"
    Public Const MESSAGE_DATA_DETAIL_NOT_FOUND_ASSETUSAGE As String = "Check Data Asset Usage "

End Class
