

Public Class CommonSimpleRuleHelper

    Public Function SetJumlahGrid(ByVal Jml As Int16) As Int16

        If (Jml >= 0 And Jml <= 12) Then
            Return 1
        ElseIf (Jml >= 13 And Jml <= 24) Then
            Return 2
        ElseIf (Jml >= 25 And Jml <= 36) Then
            Return 3
        ElseIf (Jml >= 36 And Jml <= 48) Then
            Return 4
        ElseIf (Jml >= 49 And Jml <= 60) Then
            Return 5
        ElseIf (Jml >= 61 And Jml <= 72) Then
            Return 6
        ElseIf (Jml >= 73 And Jml <= 84) Then
            Return 7
        ElseIf (Jml >= 85 And Jml <= 96) Then
            Return 8
        ElseIf (Jml >= 97 And Jml <= 108) Then
            Return 9
        ElseIf (Jml >= 109 And Jml <= 120) Then
            Return 10
        ElseIf (Jml >= 121 And Jml <= 132) Then
            Return 11
        ElseIf (Jml >= 133 And Jml <= 144) Then
            Return 12
        Else
            Return 0
        End If
    End Function

    Public Function ConvertDateReturnAsString(ByVal pStrValue As String) As String
        Dim lStrValue As String = Trim(pStrValue)
        Dim lArrValue As Array
        lArrValue = CType(lStrValue.Split(CChar("/")), Array)
        If UBound(lArrValue) = 2 Then
            lArrValue.SetValue(Right(CStr("0" + lArrValue.GetValue(0).ToString), 2), 0)
            lArrValue.SetValue(Right(CStr("0" + lArrValue.GetValue(1).ToString), 2), 1)
            If Len(lArrValue.GetValue(2)) = 2 Then
                lArrValue.SetValue(Left(CStr(Year(Now())), 2) + lArrValue.GetValue(2).ToString, 2)
            End If
            lStrValue = lArrValue.GetValue(1).ToString & "/" & lArrValue.GetValue(0).ToString & "/" & lArrValue.GetValue(2).ToString
        End If
        Return lStrValue
    End Function


End Class
