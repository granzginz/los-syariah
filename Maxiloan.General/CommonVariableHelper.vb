
'Public Class CommonVariableHelper

'    'Default Vairabel Untuk Navigation
'    Public Const DEFAULT_CURRENT_PAGE As Integer = 1
'    Public Const DEFAULT_PAGE_SIZE As Int16 = 10
'    Public Const DEFAULT_CURRENT_PAGE_NUMBER As Int32 = 1
'    Public Const DEFAULT_TOTALPAGES As Double = 1
'    Public Const DEFAULT_RECORD_COUNT As Int64 = 1


'    'Variabel Umum
'    Public Const VIEW_BRANCH As String = "ViewBranch"
'    Public Const VIEW_INSURANCE_STANDARD_PREMIUM As String = "ViewInsuranceStandardPremium"
'    Public Const BRANCH_SOURCE_FOR_CUSTOMER As String = "CustomerRate"
'    Public Const BRANCH_SOURCE_FOR_PREMIUM_RATE As String = "StandardRate"

'    Public Const INS_QUOTA_LISTING As String = "InsQuotaListing"
'    Public Const INS_QUOTA_LISTING_DETAIL_MONTH_YEAR As String = "InsQuotaListingDetail"
'    Public Const NEW_APP_INSURANCE_FORM As String = "NEWAPPINS"

'    Public Const PAGE_SOURCE_REPORT_INSURANCE_STANDARD_PREMIUM As String = "InsuranceStandardPremiumReport"

'    Public Const PAGE_SOURCE_CREATE_SPPA As String = "CreateSPPA"
'    Public Const PAGE_SOURCE_GRID_TENOR_VIEW_INSURANCE_ASSET_DETAIL As String = "ViewInsuranceAssetDetailGridTenor"
'    Public Const PAGE_SOURCE_INSURANCE_DETAIL_INFORMATION As String = "InsuranceDetailInformation"
'    Public Const PAGE_SOURCE_ASSET_INSURANCE_DETAIL_INFORMATION As String = "AssetInsuranceDetailInformation"
'    Public Const PAGE_SOURCE_GET_CLAIM_HISTORY As String = "GetClaimHistory"
'    Public Const PAGE_SOURCE_GET_ENDORSMENT_HISTORY As String = "GetEndorsmentHistory"
'    Public Const PAGE_SOURCE_GET_ENDORSMENT_HISTORY_DETAIL_POPUP As String = "GetEndorsmentHistoryDetailPopUp"
'    Public Const PAGE_SOURCE_GET_INSURANCE_BILLING_SELECT As String = "GetInsBillingSelect"
'    Public Const PAGE_SOURCE_GET_INSURANCE_COVERING_INQUIRY As String = "GetInsuranceCoveringInquiry"
'    Public Const PAGE_SOURCE_PRINT_SPPA_REPORT As String = "PrintSPPAReport"

'    'Nama Aplikasi
'    Public Const APPLICATION_NAME As String = "MAXILOAN"

'    'Nama Form 
'    Public Const FORM_NAME_SPPA As String = "InsSPPA"
'    Public Const FORM_NAME_INSURANCE_QUOTA As String = "INSQUOTA"
'    Public Const FORM_NAME_NEW_APPLICATION As String = "NEWAPPINS"
'    Public Const FORM_NAME_NEW_APPLICATION_BY_COMPANY As String = "NEWAPPINSBYCOMPANY"
'    Public Const FORM_NAME_LISTING_BRANCH_INSURANCE_STANDARD_PREMIUM As String = "INSRATESTD"
'    Public Const FORM_NAME_RATE_INSURANCE_STANDARD_PREMIUM As String = "INSRATESTDRATE"
'    Public Const FORM_NAME_LISTING_BRANCH_INSURANCE_PREMIUM_TO_CUSTOMER As String = "INSRATETOCUST"
'    Public Const FORM_NAME_RATE_INSURANCE_PREMIUM_TO_CUSTOMER As String = "INSRATETOCUSTRATE"
'    Public Const FORM_NAME_GENERAL_SETTING As String = "GENERALSETTING"
'    Public Const FORM_NAME_INSCO_ALLOCATION As String = "InsCoAllocation"
'    Public Const FORM_NAME_PRINT_RENEWAL_LETTER As String = "InsRenewalPrint"
'    Public Const FORM_NAME_INS_EDIT_RENEWAL As String = "InsEditRenewal"
'    Public Const FORM_NAME_INS_INQUIRY_EXISTING_COVER As String = "InqInsExistingCover"
'    Public Const FORM_NAME_COL_PRINT_TAGIH As String = "CollPrintRN"


'    'Form Feature
'    Public Const FORM_FEATURE_PRINT As String = "Print"
'    Public Const FORM_FEATURE_VIEW As String = "View"
'    Public Const FORM_FEATURE_ENTRY As String = "Entry"
'    Public Const FORM_FEATURE_YEAR As String = "Year"
'    Public Const FORM_FEATURE_EDIT As String = "Edit"
'    Public Const FORM_FEATURE_ADD As String = "Add"
'    Public Const FORM_FEATURE_DEL As String = "Del"
'    Public Const FORM_FEATURE_COPY As String = "Copy"
'    Public Const FORM_FEATURE_SRC As String = "Srch"

'    'SQL Query
'    Public Const SQL_QUERY_COVERAGE_TYPEMS As String = "Select ID,Description from tblCoverageType Order by ID "
'    Public Const SQL_QUERY_PAIDBYCUST_MS_PAGESOURCE_COMPANY_AT_CUST As String = "Select ltrim(upper(ID)) as ID, description from tblpaidbycuststatus Where ID='PAID' Order by ID"
'    Public Const SQL_QUERY_PAIDBYCUST_MS As String = "Select ltrim(upper(ID)) as ID, description from tblpaidbycuststatus Order by ID "
'    Public Const SQL_QUERY_TPL As String = "select TPLAmount,TPLPremium from insurancetpltocust "

'End Class


Public Class CommonVariableHelper

    'Default Vairabel Untuk Navigation
    Public Const DEFAULT_CURRENT_PAGE As Integer = 1
    Public Const DEFAULT_PAGE_SIZE As Int16 = 10
    Public Const DEFAULT_CURRENT_PAGE_NUMBER As Int32 = 1
    Public Const DEFAULT_TOTALPAGES As Double = 1
    Public Const DEFAULT_RECORD_COUNT As Int64 = 1


    'Variabel Umum
    Public Const VIEW_BRANCH As String = "ViewBranch"
    Public Const VIEW_INSURANCE_STANDARD_PREMIUM As String = "ViewInsuranceStandardPremium"
    Public Const BRANCH_SOURCE_FOR_CUSTOMER As String = "CustomerRate"
    Public Const BRANCH_SOURCE_FOR_PREMIUM_RATE As String = "StandardRate"

    Public Const INS_QUOTA_LISTING As String = "InsQuotaListing"
    Public Const INS_QUOTA_LISTING_DETAIL_MONTH_YEAR As String = "InsQuotaListingDetail"
    Public Const NEW_APP_INSURANCE_FORM As String = "NEWAPPINS"

    Public Const PAGE_SOURCE_REPORT_INSURANCE_STANDARD_PREMIUM As String = "InsuranceStandardPremiumReport"

    Public Const PAGE_SOURCE_CREATE_SPPA As String = "CreateSPPA"
    Public Const PAGE_SOURCE_GRID_TENOR_VIEW_INSURANCE_ASSET_DETAIL As String = "ViewInsuranceAssetDetailGridTenor"
    Public Const PAGE_SOURCE_INSURANCE_DETAIL_INFORMATION As String = "InsuranceDetailInformation"
    Public Const PAGE_SOURCE_ASSET_INSURANCE_DETAIL_INFORMATION As String = "AssetInsuranceDetailInformation"
    Public Const PAGE_SOURCE_GET_CLAIM_HISTORY As String = "GetClaimHistory"
    Public Const PAGE_SOURCE_GET_ENDORSMENT_HISTORY As String = "GetEndorsmentHistory"
    Public Const PAGE_SOURCE_GET_ENDORSMENT_HISTORY_DETAIL_POPUP As String = "GetEndorsmentHistoryDetailPopUp"
    Public Const PAGE_SOURCE_GET_INSURANCE_BILLING_SELECT As String = "GetInsBillingSelect"
    Public Const PAGE_SOURCE_GET_INSURANCE_COVERING_INQUIRY As String = "GetInsuranceCoveringInquiry"
    Public Const PAGE_SOURCE_PRINT_SPPA_REPORT As String = "PrintSPPAReport"

    'Nama Aplikasi
    Public Const APPLICATION_NAME As String = "MAXILOAN"

    'Nama Form 
    Public Const FORM_NAME_SPPA As String = "InsSPPA"
    Public Const FORM_NAME_INSURANCE_QUOTA As String = "INSQUOTA"
    Public Const FORM_NAME_NEW_APPLICATION As String = "NEWAPPINS"
    Public Const FORM_NAME_NEW_APPLICATION_BY_COMPANY As String = "NEWAPPINSBYCOMPANY"
    Public Const FORM_NAME_LISTING_BRANCH_INSURANCE_STANDARD_PREMIUM As String = "INSRATESTD"
    Public Const FORM_NAME_RATE_INSURANCE_STANDARD_PREMIUM As String = "INSRATESTDRATE"
    Public Const FORM_NAME_LISTING_BRANCH_INSURANCE_PREMIUM_TO_CUSTOMER As String = "INSRATETOCUST"
    Public Const FORM_NAME_RATE_INSURANCE_PREMIUM_TO_CUSTOMER As String = "INSRATETOCUSTRATE"
    Public Const FORM_NAME_GENERAL_SETTING As String = "GENERALSETTING"
    Public Const FORM_NAME_INSCO_ALLOCATION As String = "InsCoAllocation"
    Public Const FORM_NAME_PRINT_RENEWAL_LETTER As String = "InsRenewalPrint"
    Public Const FORM_NAME_INS_EDIT_RENEWAL As String = "InsEditRenewal"
    Public Const FORM_NAME_INS_INQUIRY_EXISTING_COVER As String = "InqInsExistingCover"
    Public Const FORM_NAME_COL_PRINT_TAGIH As String = "CollPrintRN"


    'Form Feature
    Public Const FORM_FEATURE_PRINT As String = "Print"
    Public Const FORM_FEATURE_VIEW As String = "View"
    Public Const FORM_FEATURE_ENTRY As String = "Entry"
    Public Const FORM_FEATURE_YEAR As String = "Year"
    Public Const FORM_FEATURE_EDIT As String = "Edit"
    Public Const FORM_FEATURE_ADD As String = "Add"
    Public Const FORM_FEATURE_DEL As String = "Del"
    Public Const FORM_FEATURE_COPY As String = "Copy"
    Public Const FORM_FEATURE_SRC As String = "Srch"

    'SQL Query
    Public Const SQL_QUERY_COVERAGE_TYPEMS As String = "Select ID,Description from tblCoverageType Order by ID "
    Public Const SQL_QUERY_PAIDBYCUST_MS_PAGESOURCE_COMPANY_AT_CUST As String = "Select ltrim(upper(ID)) as ID, description from tblpaidbycuststatus Where ID='PAID' Order by ID"
    Public Const SQL_QUERY_PAIDBYCUST_MS As String = "Select ltrim(upper(ID)) as ID, description from tblpaidbycuststatus Order by ID "
    Public Const SQL_QUERY_TPL As String = "select TPLAmount,TPLPremium from newinsurancetpltocust "
    Public Const SQL_QUERY_OLDTPL As String = "select TPLAmount,TPLPremium from insurancetpltocust "

End Class






