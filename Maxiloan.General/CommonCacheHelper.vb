

Public Class CommonCacheHelper

    Public Const CACHE_COVERAGE_TYPE As String = "CacheCoverageType"

    Public Const CACHE_INSURANCE_COYBRANCH_NEWAPPINSURANCE As String = "CacheInsuranceCoyBranchNewInsurance"
    Public Const CACHE_INSCOBRANCH_FROM_COPYRATE As String = "CacheInsCobranch"

    Public Const CACHE_CASHIER_LIST As String = "CacheCashier"
    Public Const CACHE_HEAD_CASHIER As String = "CacheHeadCashier"

    Public Const CACHE_BANK_MASTER As String = "CacheBankMaster"
    Public Const CACHE_BANKACCOUNT As String = "CacheBankAccount"
    Public Const CACHE_BANK_ACCOUNT_NO_CONDITION As String = "CacheBankAccountNoCondition"

    Public Const CACHE_BRANCH As String = "CacheBranch"
    Public Const CACHE_BRANCH_ALL As String = "CacheBranchAll"
    Public Const CACHE_BRANCH_COLLECTION As String = "CacheCollection"
    Public Const CACHE_BRANCH_STANDARD_COPYRATE As String = "CacheBranchStandardCopyRate"
    Public Const CACHE_BRANCH_FROM_COPYRATE As String = "CacheBranchPremiumCustomerCopyRate"
    Public Const CACHE_INSURANCE_BRANCH As String = "CacheInsuranceBranch"
    Public Const CACHE_MASTER_NOTARY As String = "CacheMasterNotary"

    Public Const CACHE_VIEW_INSURANCE_DETAIL_PARAMETER_BRANCH_ID As String = "CacheViewInsuranceDetailParamaterBranchID"
    Public Const CACHE_VIEW_INSURANCE_DETAIL_PARAMETER_APPLICATION_ID As String = "CacheViewInsuranceDetailParamaterApplicationID"


    Public Const CACHE_ERROR_MESSAGE As String = "CacheErrorMessage"
    Public Const CACHE_REPORTBI As String = "CacheReportBI"

    Public Const DEFAULT_TIME_DURATION_CACHE_HOUR As Int16 = 10 'Set 10 Jam
    Public Const DEFAULT_TIME_DURATION_CACHE_MINUTES As Int16 = 30 ' set 30 Menit 
    Public Const DEFAULT_TIME_DURATION_CACHE_DAYS As Int16 = 5 'Set 10 Jam
    Public Const DEFAULT_TIME_DURATION_CACHE_ONEHOUR As Int16 = 1

    Public Const CACHE_APBANK As String = "CacheAPBank"
    Public Const CACHE_BANK_ACCOUNT_RECONCILE As String = "CacheBankAccountReconcile"

    Public Const CACHE_CGID As String = "CacheCGID"
    Public Const CACHE_COLLECTOR As String = "CacheCollector"
    Public Const CACHE_DESKCOLL As String = "CacheDeskColl"

    Public Const CACHE_KOTAKABUPATEN As String = "CacheKotaKabupaten"
End Class
