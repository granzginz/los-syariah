
Imports Maxiloan.Interface
Imports Maxiloan.Parameter

Public Class PembayaranAngsuranFunding : Inherits ComponentBase
    Implements IPembayaranAngsuranFunding
    Public Function getListPembayaranAngsuran(ByVal CustomClass As Parameter.PembayaranAngsuranFunding) As Parameter.PembayaranAngsuranFunding Implements [Interface].IPembayaranAngsuranFunding.getListPembayaranAngsuran
        Dim oSQLE As New SQLEngine.FundMgt.PembayaranAngsuranFunding
        Return oSQLE.getListPembayaranAngsuran(CustomClass)
    End Function

    Public Sub DisbursePembayaranAngsuran(ByVal CustomClass As Parameter.DisbursePembayaranAngsuranFunding) Implements [Interface].IPembayaranAngsuranFunding.DisbursePembayaranAngsuran
        Dim oSQLE As New SQLEngine.FundMgt.PembayaranAngsuranFunding
        oSQLE.DisbursePembayaranAngsuranFunding(CustomClass)
    End Sub

    Public Function getListBonHijauFundingDisburse(ByVal CustomClass As Parameter.PembayaranAngsuranFunding) As Parameter.PembayaranAngsuranFunding Implements [Interface].IPembayaranAngsuranFunding.getListBonHijauFundingDisburse
        Dim oSQLE As New SQLEngine.FundMgt.PembayaranAngsuranFunding
        Return oSQLE.getListBonHijauFundingDisburse(CustomClass)
    End Function

    Public Function GetListData(strConnection As String, cmdWhere As String, currentPage As Integer, pagesize As Integer, SortBy As String) As Parameter.PembayaranAngsuranFunding Implements [Interface].IPembayaranAngsuranFunding.GetListData
        Dim oSQLE As New SQLEngine.FundMgt.PembayaranAngsuranFunding
        Return oSQLE.GetListData(strConnection, cmdWhere, currentPage, pagesize, SortBy)
    End Function

    Public Function getFundingAngsJthTempoSummary(customClass As Parameter.PembayaranAngsuranFunding) As Parameter.PembayaranAngsuranFunding Implements [Interface].IPembayaranAngsuranFunding.getFundingAngsJthTempoSummary
        Dim oSQLE As New SQLEngine.FundMgt.PembayaranAngsuranFunding
        Return oSQLE.getFundingAngsJthTempoSummary(customClass)
    End Function

    Public Sub disburseFundingAngsuranJthTempo(CustomClass As Parameter.DisbursePembayaranAngsuranFunding) Implements [Interface].IPembayaranAngsuranFunding.disburseFundingAngsuranJthTempo
        Dim oSQLE As New SQLEngine.FundMgt.PembayaranAngsuranFunding
        oSQLE.disburseFundingAngsuranJthTempo(CustomClass)
    End Sub

    Public Function FundingPrepaymentReport(CustomClass As Parameter.PembayaranAngsuranFunding) As Parameter.PembayaranAngsuranFunding Implements [Interface].IPembayaranAngsuranFunding.FundingPrepaymentReport
        Dim oSQLE As New SQLEngine.FundMgt.PembayaranAngsuranFunding
        Return oSQLE.FundingPrepaymentReport(CustomClass)
    End Function

    Public Function BPKBLoanReport(CustomClass As Parameter.PembayaranAngsuranFunding) As Parameter.PembayaranAngsuranFunding Implements [Interface].IPembayaranAngsuranFunding.BPKBLoanReport
        Dim oSQLE As New SQLEngine.FundMgt.PembayaranAngsuranFunding
        Return oSQLE.BPKBLoanReport(CustomClass)
    End Function

    Public Function BPKBFotoCopyRequestReport(CustomClass As Parameter.PembayaranAngsuranFunding) As Parameter.PembayaranAngsuranFunding Implements [Interface].IPembayaranAngsuranFunding.BPKBFotoCopyRequestReport
        Dim oSQLE As New SQLEngine.FundMgt.PembayaranAngsuranFunding
        Return oSQLE.BPKBFotoCopyRequestReport(CustomClass)
    End Function

    Public Function BPKBPickupReport(CustomClass As Parameter.PembayaranAngsuranFunding) As Parameter.PembayaranAngsuranFunding Implements [Interface].IPembayaranAngsuranFunding.BPKBPickupReport
        Dim oSQLE As New SQLEngine.FundMgt.PembayaranAngsuranFunding
        Return oSQLE.BPKBPickupReport(CustomClass)
    End Function

    Public Function TAFPrepaymentReport(CustomClass As Parameter.PembayaranAngsuranFunding) As Parameter.PembayaranAngsuranFunding Implements [Interface].IPembayaranAngsuranFunding.TAFPrepaymentReport
        Dim oSQLE As New SQLEngine.FundMgt.PembayaranAngsuranFunding
        Return oSQLE.TAFPrepaymentReport(CustomClass)
    End Function

    Public Function RequestCalculationFundingPrepaymentReport(CustomClass As Parameter.PembayaranAngsuranFunding) As Parameter.PembayaranAngsuranFunding Implements [Interface].IPembayaranAngsuranFunding.RequestCalculationFundingPrepaymentReport
        Dim oSQLE As New SQLEngine.FundMgt.PembayaranAngsuranFunding
        Return oSQLE.RequestCalculationFundingPrepaymentReport(CustomClass)
    End Function

    Public Function getFundingpaymentAdvance(customClass As Parameter.PembayaranAngsuranFunding) As Parameter.PembayaranAngsuranFunding Implements [Interface].IPembayaranAngsuranFunding.getFundingpaymentAdvance
        Dim oSQLE As New SQLEngine.FundMgt.PembayaranAngsuranFunding
        Return oSQLE.getFundingpaymentAdvance(customClass)
    End Function

    Public Function disburseFundingpaymentAdvance(customClass As Parameter.DisbursePembayaranAngsuranFunding) As Parameter.DisbursePembayaranAngsuranFunding Implements [Interface].IPembayaranAngsuranFunding.disburseFundingpaymentAdvance
        Dim oSQLE As New SQLEngine.FundMgt.PembayaranAngsuranFunding
        Return oSQLE.disburseFundingpaymentAdvance(customClass)
    End Function

    Public Function BayarBatchJthTempoList(customClass As Parameter.PembayaranAngsuranFunding) As Parameter.PembayaranAngsuranFunding Implements [Interface].IPembayaranAngsuranFunding.BayarBatchJthTempoList
        Dim oSQLE As New SQLEngine.FundMgt.PembayaranAngsuranFunding
        Return oSQLE.bayarBatchJthTempoList(customClass)
    End Function

    Public Function disburseBatchPerjtTempo(customClass As Parameter.PembayaranAngsuranFunding) As Parameter.PembayaranAngsuranFunding Implements [Interface].IPembayaranAngsuranFunding.disburseBatchPerjtTempo
        Dim oSQLE As New SQLEngine.FundMgt.PembayaranAngsuranFunding
        Return oSQLE.disburseBatchPerjtTempo(customClass)
    End Function

    Public Function RemoveSelectionAgreementBatch(customClass As FundingContractBatch) As FundingContractBatch Implements IPembayaranAngsuranFunding.RemoveSelectionAgreementBatch
        Dim oSQLE As New SQLEngine.FundMgt.PembayaranAngsuranFunding
        Return oSQLE.RemoveSelectionAgreementBatch(customClass)
    End Function

    Public Function AppendSelectionAgreementBatch(oCustom As FundingContractBatch) As FundingContractBatch Implements IPembayaranAngsuranFunding.AppendSelectionAgreementBatch
        Dim oSQLE As New SQLEngine.FundMgt.PembayaranAngsuranFunding
        Return oSQLE.AppendSelectionAgreementBatch(oCustom)

    End Function

    Public Function InstallmentScheduleTL(customClass As Parameter.PembayaranAngsuranFunding) As Parameter.PembayaranAngsuranFunding Implements IPembayaranAngsuranFunding.InstallmentScheduleTL
        Dim oSQLE As New SQLEngine.FundMgt.PembayaranAngsuranFunding
        Return oSQLE.InstallmentScheduleTL(customClass)
    End Function
End Class
