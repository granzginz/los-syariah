﻿#Region "Imports"
Imports Maxiloan.Interface
#End Region

Public Class FundingCriteriaValue : Inherits ComponentBase
    Implements IFundingCriteriaValue

    Dim oSQLE As New SQLEngine.FundMgt.FundingCriteriaValue

    Public Function FundingCriteriaValueDelete(ByVal oCustomClass As Parameter.FundingCriteriaValue) As String Implements [Interface].IFundingCriteriaValue.FundingCriteriaValueDelete
        Return oSQLE.FundingCriteriaValueDelete(oCustomClass)
    End Function

    Public Function FundingCriteriaValueSaveAdd(ByVal oCustomClass As Parameter.FundingCriteriaValue) As String Implements [Interface].IFundingCriteriaValue.FundingCriteriaValueSaveAdd
        Return oSQLE.FundingCriteriaValueSaveAdd(oCustomClass)
    End Function

    Public Sub FundingCriteriaValueSaveEdit(ByVal oCustomClass As Parameter.FundingCriteriaValue) Implements [Interface].IFundingCriteriaValue.FundingCriteriaValueSaveEdit
        oSQLE.FundingCriteriaValueSaveEdit(oCustomClass)
    End Sub

    Public Function GetFundingCriteriaValue(ByVal oCustomClass As Parameter.FundingCriteriaValue) As Parameter.FundingCriteriaValue Implements [Interface].IFundingCriteriaValue.GetFundingCriteriaValue
        Return oSQLE.GetFundingCriteriaValue(oCustomClass)
    End Function

    Public Function GetFundingCriteriaValueList(ByVal oCustomClass As Parameter.FundingCriteriaValue) As Parameter.FundingCriteriaValue Implements [Interface].IFundingCriteriaValue.GetFundingCriteriaValueList
        Return oSQLE.GetFundingCriteriaValueList(oCustomClass)
    End Function
End Class
