﻿#Region "Imports"
Imports Maxiloan.Interface
#End Region

Public Class FundingContractCriteria : Inherits ComponentBase
    Implements IFundingContractCriteria

    Dim oSQLE As New SQLEngine.FundMgt.FundingContractCriteria

    Public Function FundingContractCriteriaDelete(ByVal oCustomClass As Parameter.FundingContractCriteria) As String Implements [Interface].IFundingContractCriteria.FundingContractCriteriaDelete
        Return oSQLE.FundingContractCriteriaDelete(oCustomClass)
    End Function

    Public Function FundingContractCriteriaSaveAdd(ByVal oCustomClass As Parameter.FundingContractCriteria) As String Implements [Interface].IFundingContractCriteria.FundingContractCriteriaSaveAdd
        Return oSQLE.FundingContractCriteriaSaveAdd(oCustomClass)
    End Function

    Public Sub FundingContractCriteriaSaveEdit(ByVal oCustomClass As Parameter.FundingContractCriteria) Implements [Interface].IFundingContractCriteria.FundingContractCriteriaSaveEdit
        oSQLE.FundingContractCriteriaSaveEdit(oCustomClass)
    End Sub

    Public Function GetFundingContractCriteriaList(ByVal oCustomClass As Parameter.FundingContractCriteria) As Parameter.FundingContractCriteria Implements [Interface].IFundingContractCriteria.GetFundingContractCriteriaList
        Return oSQLE.GetFundingContractCriteriaList(oCustomClass)
    End Function

    Public Function GetCombo(ByVal customclass As Parameter.FundingContractCriteria) As System.Data.DataTable Implements [Interface].IFundingContractCriteria.GetCombo
        Return oSQLE.GetCombo(customclass)
    End Function

    Public Function GetFundingContractCriteriaListColl(ByVal oCustomClass As Parameter.FundingContractCriteria) As List(Of Parameter.FundingContractCriteria) Implements [Interface].IFundingContractCriteria.GetFundingContractCriteriaListColl
        Return oSQLE.GetFundingContractCriteriaListColl(oCustomClass)
    End Function
End Class
