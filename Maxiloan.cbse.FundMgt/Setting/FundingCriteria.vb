﻿#Region "Imports"
Imports Maxiloan.Interface
#End Region

Public Class FundingCriteria : Inherits ComponentBase
    Implements IFundingCriteria

    Dim oSQLE As New SQLEngine.FundMgt.FundingCriteria

    Public Function FundingCriteriaDelete(ByVal oCustomClass As Parameter.FundingCriteria) As String Implements [Interface].IFundingCriteria.FundingCriteriaDelete
        Return oSQLE.FundingCriteriaDelete(oCustomClass)
    End Function

    Public Function FundingCriteriaSaveAdd(ByVal oCustomClass As Parameter.FundingCriteria) As String Implements [Interface].IFundingCriteria.FundingCriteriaSaveAdd
        Return oSQLE.FundingCriteriaSaveAdd(oCustomClass)
    End Function

    Public Sub FundingCriteriaSaveEdit(ByVal oCustomClass As Parameter.FundingCriteria) Implements [Interface].IFundingCriteria.FundingCriteriaSaveEdit
        oSQLE.FundingCriteriaSaveEdit(oCustomClass)
    End Sub

    Public Function GetFundingCriteria(ByVal oCustomClass As Parameter.FundingCriteria) As Parameter.FundingCriteria Implements [Interface].IFundingCriteria.GetFundingCriteria
        Return oSQLE.GetFundingCriteria(oCustomClass)
    End Function

    Public Function GetFundingCriteriaList(ByVal oCustomClass As Parameter.FundingCriteria) As Parameter.FundingCriteria Implements [Interface].IFundingCriteria.GetFundingCriteriaList
        Return oSQLE.GetFundingCriteriaList(oCustomClass)
    End Function
    Public Sub ExecQuery(ByVal oCustomClass As Parameter.FundingCriteria) Implements [Interface].IFundingCriteria.ExecQuery
        oSQLE.ExecQuery(oCustomClass)
    End Sub

    Public Function GetSQLList(oCustomClass As Parameter.FundingCriteria) As Parameter.FundingCriteria Implements [Interface].IFundingCriteria.GetSQLList
        Return oSQLE.GetSQLList(oCustomClass)
    End Function

    Public Sub SaveReq(oCustomClass As Parameter.FundingCriteria) Implements [Interface].IFundingCriteria.SaveReq
        oSQLE.SaveReq(oCustomClass)
    End Sub
End Class
