

#Region "Imports"
Imports Maxiloan.Interface
Imports Maxiloan.Parameter

#End Region

Public Class FundingCompany : Inherits ComponentBase
    Implements IFundingCompany

#Region "FundingCompany"


    Public Function FundingCompanyReport(ByVal co As Parameter.FundingCompany) As Parameter.FundingCompany Implements [Interface].IFundingCompany.FundingCompanyReport
        Dim DA As New SQLEngine.FundMgt.FundingCompany
        Return DA.ListFundingCompanyReport(co)
    End Function

    Public Function FundingCompanyList(ByVal co As Parameter.FundingCompany) As Parameter.FundingCompany Implements [Interface].IFundingCompany.FundingCompanyList
        Dim DA As New SQLEngine.FundMgt.FundingCompany
        Return DA.ListFundingCompany(co)
    End Function

    Public Function FundingCompanyAdd(ByVal co As Parameter.FundingCompany, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal) As String Implements [Interface].IFundingCompany.FundingCompanyAdd
        Try
            Dim DA As New SQLEngine.FundMgt.FundingCompany
            DA.FundingCompanyAdd(co, oClassAddress, oClassPersonal)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Function

    Public Function GetComboInputBank(ByVal strcon As String) As System.Data.DataTable Implements [Interface].IFundingCompany.GetComboInputBank
        Dim DA As New SQLEngine.FundMgt.FundingCompany
        Return DA.GetComboInputBank(strcon)
    End Function

    Public Sub FundingCompanyDelete(ByVal co As Parameter.FundingCompany) Implements [Interface].IFundingCompany.FundingCompanyDelete
        Try
            Dim DA As New SQLEngine.FundMgt.FundingCompany
            DA.FundingCompanyDelete(co)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub

    Public Sub FundingCompanyEdit(ByVal co As Parameter.FundingCompany, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal) Implements [Interface].IFundingCompany.FundingCompanyEdit
        Try
            Dim DA As New SQLEngine.FundMgt.FundingCompany
            DA.FundingCompanyEdit(co, oClassAddress, oClassPersonal)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub

    Public Function FundingCompanyListByID(ByVal co As Parameter.FundingCompany) As Parameter.FundingCompany Implements [Interface].IFundingCompany.FundingCompanyListByID
        Dim DA As New SQLEngine.FundMgt.FundingCompany
        Return DA.ListFundingCompanyByID(co)
    End Function

#End Region

#Region "FundingContract"

    Public Function FundingContractAdd(ByVal co As Parameter.FundingContract) As String Implements [Interface].IFundingCompany.FundingContractAdd
        Try
            Dim DA As New SQLEngine.FundMgt.FundingCompany
            DA.FundingContractAdd(co)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Function

    Public Function FundingContractByID(ByVal co As Parameter.FundingContract) As Parameter.FundingContract Implements [Interface].IFundingCompany.FundingContractByID
        Dim DA As New SQLEngine.FundMgt.FundingCompany
        Return DA.ListFundingContractByID(co)
    End Function

    Public Sub FundingContractDelete(ByVal co As Parameter.FundingContract) Implements [Interface].IFundingCompany.FundingContractDelete
        Try
            Dim DA As New SQLEngine.FundMgt.FundingCompany
            DA.FundingContractDelete(co)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub

    Public Sub FundingContractEdit(ByVal co As Parameter.FundingContract) Implements [Interface].IFundingCompany.FundingContractEdit
        Try
            Dim DA As New SQLEngine.FundMgt.FundingCompany
            DA.FundingContractEdit(co)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub

#End Region

#Region "FundingContractPlafondBranch"
    Public Function FundingContractPlafondBranchAdd(ByVal co As Parameter.FundingContractBranch) As String Implements [Interface].IFundingCompany.FundingContractPlafondBranchAdd
        Try
            Dim DA As New SQLEngine.FundMgt.FundingCompany
            DA.FundingContractPlafondBranchAdd(co)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Function

    Public Function FundingContractPlafondBranchByID(ByVal co As Parameter.FundingContractBranch) As Parameter.FundingContractBranch Implements [Interface].IFundingCompany.FundingContractPlafondBranchByID
        Dim DA As New SQLEngine.FundMgt.FundingCompany
        Return DA.ListFundingContractPlafondBranchByID(co)
    End Function

    Public Sub FundingContractPlafondBranchDelete(ByVal co As Parameter.FundingContractBranch) Implements [Interface].IFundingCompany.FundingContractPlafondBranchDelete
        Try
            Dim DA As New SQLEngine.FundMgt.FundingCompany
            DA.FundingContractPlafondBranchDelete(co)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub

    Public Sub FundingContractPlafondBranchEdit(ByVal co As Parameter.FundingContractBranch) Implements [Interface].IFundingCompany.FundingContractPlafondBranchEdit
        Try
            Dim DA As New SQLEngine.FundMgt.FundingCompany
            DA.FundingContractPlafondBranchEdit(co)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub

#End Region

#Region "FundingContractDoc"
    Public Function FundingContractDocAdd(ByVal co As Parameter.FundingContract) As String Implements [Interface].IFundingCompany.FundingContractDocAdd
        Try
            Dim DA As New SQLEngine.FundMgt.FundingCompany
            DA.FundingContractDocAdd(co)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Function

    Public Sub FundingContractDocDelete(ByVal co As Parameter.FundingContract) Implements [Interface].IFundingCompany.FundingContractDocDelete
        Try
            Dim DA As New SQLEngine.FundMgt.FundingCompany
            DA.FundingContractDocDelete(co)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub
#End Region

#Region "FundingContractBatch"
    Public Sub FundingContractBatchEdit(ByVal co As Parameter.FundingContractBatch) Implements [Interface].IFundingCompany.FundingContractBatchEdit
        Try
            Dim DA As New SQLEngine.FundMgt.FundingCompany
            DA.FundingContractBatchEdit(co)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub

    Public Function FundingContractBatchAdd(ByVal co As Parameter.FundingContractBatch) As String Implements [Interface].IFundingCompany.FundingContractBatchAdd
        Try
            Dim DA As New SQLEngine.FundMgt.FundingCompany
            DA.FundingContractBatchAdd(co)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Function

    Public Function FundingContractBatchInsAdd(ByVal co As Parameter.FundingContractBatch) As String Implements [Interface].IFundingCompany.FundingContractBatchInsAdd
        Try
            Dim DA As New SQLEngine.FundMgt.FundingCompany
            DA.FundingContractBatchInsAdd(co)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Function

    Public Sub FundingUpdateAgreementSelected(ByVal co As Parameter.FundingContractBatch) Implements [Interface].IFundingCompany.FundingUpdateAgreementSelected
        Try
            Dim DA As New SQLEngine.FundMgt.FundingCompany
            DA.FundingUpdateAgreementSelected(co)

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub

    Public Sub FundingUpdateAgreementExecution(ByVal co As Parameter.FundingContractBatch) Implements [Interface].IFundingCompany.FundingUpdateAgreementExecution
        Try
            Dim DA As New SQLEngine.FundMgt.FundingCompany
            DA.FundingUpdateAgreementExecution(co)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub
    Public Sub FundingUpdateAgreementSecondExecution(ByVal CustomClass As Parameter.FundingContractBatch) Implements [Interface].IFundingCompany.FundingUpdateAgreementSecondExecution
        Try
            Dim DA As New SQLEngine.FundMgt.FundingCompany
            DA.FundingUpdateAgreementSecondExecution(CustomClass)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub
#End Region

    Public Sub FundingDrowDownReceive(ByVal CustomClass As Parameter.FundingContractBatch) Implements [Interface].IFundingCompany.FundingDrowDownReceive
        Try
            Dim DA As New SQLEngine.FundMgt.FundingCompany
            DA.FundingDrowDownReceive(CustomClass)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub

    Public Function GetGeneralEditView(ByVal oCustomClass As Parameter.FundingContractBatch) As Parameter.FundingContractBatch Implements [Interface].IFundingCompany.GetGeneralEditView
        Try
            Dim DA As New SQLEngine.FundMgt.FundingCompany
            Return DA.GetGeneralEditView(oCustomClass)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Function

    Public Sub PaymentOutInstallmentProcess(ByVal CustomClass As Parameter.FundingContractBatch) Implements [Interface].IFundingCompany.PaymentOutInstallmentProcess
        Try
            Dim DA As New SQLEngine.FundMgt.FundingCompany
            DA.PaymentOutInstallmentProcess(CustomClass)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub
    Public Sub PrepaymentBPKBReplacing(ByVal CustomClass As Parameter.FundingContractBatch) Implements [Interface].IFundingCompany.PrepaymentBPKBReplacing
        Try
            Dim DA As New SQLEngine.FundMgt.FundingCompany
            DA.PrepaymentBPKBReplacing(CustomClass)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub
    Public Sub PrepaymentPaymentOut(ByVal CustomClass As Parameter.FundingContractBatch) Implements [Interface].IFundingCompany.PrepaymentPaymentOut
        Try
            Dim DA As New SQLEngine.FundMgt.FundingCompany
            DA.PrepaymentPaymentOut(CustomClass)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub
    Public Sub PaymentOutFees(ByVal CustomClass As Parameter.FundingContractBatch) Implements [Interface].IFundingCompany.PaymentOutFees
        Try
            Dim DA As New SQLEngine.FundMgt.FundingCompany
            DA.PaymentOutFees(CustomClass)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub
    Public Sub AddFundingAgreementInstallment(ByVal CustomClass As Parameter.FundingContractBatch) Implements [Interface].IFundingCompany.AddFundingAgreementInstallment
        Try
            Dim DA As New SQLEngine.FundMgt.FundingCompany
            DA.AddFundingAgreementInstallment(CustomClass)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub
    Public Sub FundingRescheduleAgreementInstallment(ByVal CustomClass As Parameter.FundingContractBatch) Implements [Interface].IFundingCompany.FundingRescheduleAgreementInstallment
        Try
            Dim DA As New SQLEngine.FundMgt.FundingCompany
            DA.FundingRescheduleAgreementInstallment(CustomClass)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub
    Public Sub FundingReschedulePrepaymentAgreementInstallment(ByVal CustomClass As Parameter.FundingContractBatch) Implements [Interface].IFundingCompany.FundingReschedulePrepaymentAgreementInstallment
        Try
            Dim DA As New SQLEngine.FundMgt.FundingCompany
            DA.FundingReschedulePrepaymentAgreementInstallment(CustomClass)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub
    Public Sub FundingUpdateContractBatchInst(ByVal CustomClass As Parameter.FundingContractBatch) Implements [Interface].IFundingCompany.FundingUpdateContractBatchInst
        Try
            Dim DA As New SQLEngine.FundMgt.FundingCompany
            DA.FundingUpdateContractBatchInst(CustomClass)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub
    Public Function GetPrepaymentEditView(ByVal oCustomClass As Parameter.FundingContractBatch) As Parameter.FundingContractBatch Implements [Interface].IFundingCompany.GetPrepaymentEditView
        Try
            Dim DA As New SQLEngine.FundMgt.FundingCompany
            Return DA.GetPrepaymentEditView(oCustomClass)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Function
    Public Sub FundingAgreementSelect(ByVal CustomClass As Parameter.FundingContractBatch) Implements [Interface].IFundingCompany.FundingAgreementSelect
        Try
            Dim DA As New SQLEngine.FundMgt.FundingCompany
            DA.FundingAgreementSelect(CustomClass)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub FundingAgreementSelectFromUpload(ByVal CustomClass As Parameter.FundingContractBatch) Implements [Interface].IFundingCompany.FundingAgreementSelectFromUpload
        Try
            Dim DA As New SQLEngine.FundMgt.FundingCompany
            DA.FundingAgreementSelectFromUpload(CustomClass)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub DueDateChangeSave(ByVal oCustomClass As Parameter.FundingContractBatch) Implements [Interface].IFundingCompany.DueDateChangeSave
        Dim DExec As New SQLEngine.FundMgt.FundingCompany
        DExec.DueDateChangeSave(oCustomClass)
    End Sub

#Region "FundingContractNegCov"
    Public Function FundingContractNegCovGet(ByVal CustomClass As Parameter.FundingContract) As Parameter.FundingContract Implements [Interface].IFundingCompany.FundingContractNegCovGet
        Try
            Dim DA As New SQLEngine.FundMgt.FundingCompany
            Return DA.FundingContractNegCovGet(CustomClass)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Function

    Public Sub FundingContractNegCovUpdate(ByVal strcon As String, ByVal setField As String, ByVal WhereBy As String) Implements [Interface].IFundingCompany.FundingContractNegCovUpdate
        Try
            Dim DA As New SQLEngine.FundMgt.FundingCompany
            DA.FundingContractNegCovUpdate(strcon, setField, WhereBy)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub
#End Region

    Public Sub AddFundingAgreementInstallmentDraft(ByVal Company As Parameter.FundingContractBatch) Implements [Interface].IFundingCompany.AddFundingAgreementInstallmentDraft
        Try
            Dim DA As New SQLEngine.FundMgt.FundingCompany
            DA.AddFundingAgreementInstallmentDraft(Company)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub FundingContractBatchInsAdd2(ByVal customClass As Parameter.FundingContractBatch) Implements [Interface].IFundingCompany.FundingContractBatchInsAdd2
        Dim DA As New SQLEngine.FundMgt.FundingCompany
        DA.FundingContractBatchInsAdd2(customClass)
    End Sub

    Public Function FundingDraftSoftcopy(ByVal customClass As Parameter.FundingContractBatch) As System.Data.DataSet Implements [Interface].IFundingCompany.FundingDraftSoftcopy
        Try
            Dim DA As New SQLEngine.FundMgt.FundingCompany
            Return DA.FundingDraftSoftcopy(customClass)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function FundingContractBatchInsAddKMK(ByVal customClass As Parameter.FundingContractBatch) As String Implements [Interface].IFundingCompany.FundingContractBatchInsAddKMK
        Try
            Dim DA As New SQLEngine.FundMgt.FundingCompany
            DA.FundingContractBatchInsAddKMK(customClass)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub AddFundingAgreementInstallmentKMK(ByVal Company As Parameter.FundingContractBatch) Implements [Interface].IFundingCompany.AddFundingAgreementInstallmentKMK
        Try
            Dim DA As New SQLEngine.FundMgt.FundingCompany
            DA.AddFundingAgreementInstallmentKMK(Company)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub genFAIEfektif(ByVal customClass As Parameter.FundingContractBatch) Implements [Interface].IFundingCompany.genFAIEfektif
        Dim DA As New SQLEngine.FundMgt.FundingCompany
        DA.genFAIExec(customClass)
    End Sub

    Public Function FundingAgreementSelection(customClass As Parameter.FundingContractBatch) As Parameter.FundingContractBatch Implements [Interface].IFundingCompany.FundingAgreementSelection
        Dim DA As New SQLEngine.FundMgt.FundingCompany
        Return DA.FundingAgreementSelection(customClass)
    End Function

    Public Function FundingAgreementList(customClass As Parameter.FundingContractBatch) As Parameter.FundingContractBatch Implements [Interface].IFundingCompany.FundingAgreementList
        Dim DA As New SQLEngine.FundMgt.FundingCompany
        Return DA.FundingAgreementList(customClass)
    End Function

    Public Sub FundingContractRateEdit(oClass As Parameter.FundingContractRate) Implements [Interface].IFundingCompany.FundingContractRateEdit
        Dim DA As New SQLEngine.FundMgt.FundingCompany
        DA.FundingContractRateEdit(oClass)
    End Sub

    Public Sub FundingDrowDownReceiveCheckPeriod(ByVal CustomClass As Parameter.FundingContractBatch) Implements [Interface].IFundingCompany.FundingDrowDownReceiveCheckPeriod
        Try
            Dim DA As New SQLEngine.FundMgt.FundingCompany
            DA.FundingDrowDownReceiveCheckPeriod(CustomClass)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub FundingDrowDownReceiveCheckScheme(ByVal CustomClass As Parameter.FundingContractBatch) Implements [Interface].IFundingCompany.FundingDrowDownReceiveCheckScheme
        Try
            Dim DA As New SQLEngine.FundMgt.FundingCompany
            DA.FundingDrowDownReceiveCheckScheme(CustomClass)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub FundingDrowDownReceiveDueDateRangeAdd(ByVal CustomClass As Parameter.FundingContractBatch) Implements [Interface].IFundingCompany.FundingDrowDownReceiveDueDateRangeAdd
        Try
            Dim DA As New SQLEngine.FundMgt.FundingCompany
            DA.FundingDrowDownReceiveDueDateRangeAdd(CustomClass)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.BusinessComponent, ex)
            Throw ex
        End Try
    End Sub

    Public Function FundingTotalSelected(customClass As Parameter.FundingContractBatch) As Parameter.FundingContractBatch Implements [Interface].IFundingCompany.FundingTotalSelected
        Try
            Dim DA As New SQLEngine.FundMgt.FundingCompany
            Return DA.FundingTotalSelected(customClass)
        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Public Sub PrepaymentBatchProcess(CustomClass As Parameter.FundingContractBatch) Implements [Interface].IFundingCompany.PrepaymentBatchProcess
        Try
            Dim DA As New SQLEngine.FundMgt.PembayaranAngsuranFunding
            DA.prepaymentBatchProcess(CustomClass)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function FundingAgreementInstallmentView(customClass As Parameter.FundingContractBatch) As Parameter.FundingContractBatch Implements [Interface].IFundingCompany.FundingAgreementInstallmentView
        Try
            Dim DA As New SQLEngine.FundMgt.FundingCompany
            Return DA.FundingAgreementInstallmentView(customClass)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function ListSPCReport(customclass As Parameter.FundingCompany) As System.Data.DataSet Implements [Interface].IFundingCompany.ListSPCReport
        Try
            Dim DA As New SQLEngine.FundMgt.FundingCompany
            Return DA.ListSPCReport(customclass)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function FundPrintSave(ByVal oCustomClass As Parameter.FundingCompany) As String Implements [Interface].IFundingCompany.FundPrintSave
        Dim oSQLE As New SQLEngine.FundMgt.FundingCompany
        Return oSQLE.FundPrintSave(oCustomClass)
    End Function

    Public Function ListPrint(customclass As Parameter.FundingContractBatch) As Parameter.FundingContractBatch Implements [Interface].IFundingCompany.ListPrint
        Dim oSQLE As New SQLEngine.FundMgt.FundingCompany
        Return oSQLE.ListPrint(customclass)
    End Function
    Public Function SavePrint(ByVal customclass As Parameter.FundingContractBatch) As Parameter.FundingContractBatch Implements [Interface].IFundingCompany.SavePrint
        Dim oSQLE As New SQLEngine.FundMgt.FundingCompany
        Return oSQLE.SavePrint(customclass)
    End Function
    Public Function ListReportPrepay(ByVal customclass As Parameter.FundingContractBatch) As Parameter.FundingContractBatch Implements [Interface].IFundingCompany.ListReportPrepay
        Dim oSQLE As New SQLEngine.FundMgt.FundingCompany
        Return oSQLE.ListReportPrepay(customclass)
    End Function
    Public Function FundingAgreementJnFnc(customClass As Parameter.FundingContractBatch) As Parameter.FundingContractBatch Implements [Interface].IFundingCompany.FundingAgreementJnFnc
        Dim DA As New SQLEngine.FundMgt.FundingCompany
        Return DA.FundingAgreementJnFnc(customClass)
    End Function

    Public Function GetFundingInquiryAgreementPledgeWillFinish(oCustomClass As Parameter.FundingContractBatch) As Parameter.FundingContractBatch Implements [Interface].IFundingCompany.GetFundingInquiryAgreementPledgeWillFinish
        Dim DA As New SQLEngine.FundMgt.FundingCompany
        Return DA.GetFundingInquiryAgreementPledgeWillFinis(oCustomClass)
    End Function
    Public Function GetComboInputBankAdministrasi(strcon As String) As System.Data.DataTable Implements [Interface].IFundingCompany.GetComboInputBankAdministrasi
        Dim DA As New SQLEngine.FundMgt.FundingCompany
        Return DA.GetComboInputBankAdministrasi(strcon)
    End Function

    Public Function GetComboInputBankBeban(strcon As String) As System.Data.DataTable Implements [Interface].IFundingCompany.GetComboInputBankBeban
        Dim DA As New SQLEngine.FundMgt.FundingCompany
        Return DA.GetComboInputBankBeban(strcon)
    End Function
    Public Function ListPrintPaymentOut(customclass As Parameter.FundingContractBatch) As Parameter.FundingContractBatch Implements [Interface].IFundingCompany.ListPrintPaymentOut
        Dim oSQLE As New SQLEngine.FundMgt.FundingCompany
        Return oSQLE.ListPrintPaymentOut(customclass)
    End Function
    Public Function SavePrintPaymentOut(ByVal customclass As Parameter.FundingContractBatch) As Parameter.FundingContractBatch Implements [Interface].IFundingCompany.SavePrintPaymentOut
        Dim oSQLE As New SQLEngine.FundMgt.FundingCompany
        Return oSQLE.SavePrintPaymentOut(customclass)
    End Function
    Public Function ListReportPaymentOut(ByVal customclass As Parameter.FundingContractBatch) As Parameter.FundingContractBatch Implements [Interface].IFundingCompany.ListReportPaymentOut
        Dim oSQLE As New SQLEngine.FundMgt.FundingCompany
        Return oSQLE.ListReportPaymentOut(customclass)
    End Function

    Public Sub FundingBatchInstallmentSelectFromUpload(ByVal CustomClass As Parameter.FundingContractBatch) Implements [Interface].IFundingCompany.FundingBatchInstallmentSelectFromUpload
        Try
            Dim DA As New SQLEngine.FundMgt.FundingCompany
            DA.FundingBatchInstallmentSelectFromUpload(CustomClass)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub FundingAgreementUploadUpdateExcel(customClass As FundingContractBatch) Implements IFundingCompany.FundingAgreementUploadUpdateExcel
        Try
            Dim DA As New SQLEngine.FundMgt.FundingCompany
            DA.FundingAgreementUploadUpdateExcel(customClass)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function FundingBatchRate(oClass As FundingContractRate) As FundingContractRate Implements IFundingCompany.FundingBatchRate
        Dim oSQLE As New SQLEngine.FundMgt.FundingCompany
        Return oSQLE.FundingBatchRate(oClass)
    End Function

    Public Sub UpdateFundingAgreementSelect(ByVal CustomClass As Parameter.FundingContractBatch) Implements [Interface].IFundingCompany.UpdateFundingAgreementSelect
        Try
            Dim DA As New SQLEngine.FundMgt.FundingCompany
            DA.UpdateFundingAgreementSelect(CustomClass)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

End Class
