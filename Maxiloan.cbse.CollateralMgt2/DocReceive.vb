

#Region "Imports"
Imports System.IO
Imports System.Data
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface

#End Region

Public Class DocReceive : Inherits ComponentBase
    Implements IDoc

#Region "DocReceive"

    Public Function SPPADList(ByVal customclass As Parameter.DocRec) As Parameter.DocRec Implements [Interface].IDoc.SPPADList
        Dim DA As New SQLEngine.CollateralMgt2.DocReceive
        Return DA.SPPADList(customclass)
    End Function
    Public Function ListDoc(ByVal customclass As Parameter.DocRec) As Parameter.DocRec Implements [Interface].IDoc.ListDoc
        Dim DAListDoc As New SQLEngine.CollateralMgt2.DocReceive
        Return DAListDoc.ListDocReceive(customclass)
    End Function

    Public Function ListDocAssetBNI(ByVal customclass As Parameter.DocAssetBNI) As Parameter.DocAssetBNI Implements [Interface].IDoc.ListDocAssetBNI
        Dim DAListDocAssetBNI As New SQLEngine.CollateralMgt2.DocReceive
        Return DAListDocAssetBNI.ListDocAssetBNI(customclass)
    End Function

    Public Function InqListDocAssetBNI(ByVal customclass As Parameter.DocAssetBNI) As Parameter.DocAssetBNI Implements [Interface].IDoc.InqListDocAssetBNI
        Dim DAInqListDocAssetBNI As New SQLEngine.CollateralMgt2.DocReceive
        Return DAInqListDocAssetBNI.InqListDocAssetBNI(customclass)
    End Function

    Public Function DocListPagingAssetBNI(ByVal customclass As Parameter.DocAssetBNI) As Parameter.DocAssetBNI Implements [Interface].IDoc.DocListPagingAssetBNI
        Dim DAListDocPagingAssetBNI As New SQLEngine.CollateralMgt2.DocReceive
        Return DAListDocPagingAssetBNI.DocListPagingAssetBNI(customclass)
    End Function

    Public Function GetFLoc(ByVal customclass As Parameter.DocRec) As System.Data.DataTable Implements [Interface].IDoc.GetFLoc
        Dim DAGetFLoc As New SQLEngine.CollateralMgt2.DocReceive
        Return DAGetFLoc.GetFLoc(customclass)
    End Function

    Public Function GetRack(ByVal strConnection As String, ByVal strBranch As String, Optional isFund As Boolean = False) As System.Data.DataTable Implements [Interface].IDoc.GetRack
        Dim DAGetFLoc As New SQLEngine.CollateralMgt2.DocReceive
        Return DAGetFLoc.GetRack(strConnection, strBranch, isFund)
    End Function


    Public Function DocListPaging(ByVal customclass As Parameter.DocRec) As Parameter.DocRec Implements [Interface].IDoc.DocListPaging
        Dim DAListDoc As New SQLEngine.CollateralMgt2.DocReceive
        Return DAListDoc.DocListPaging(customclass)
    End Function

    Public Function DocRecSave(ByVal customclass As Parameter.DocRec) As Parameter.DocRec Implements [Interface].IDoc.DocRecSave
        Dim DADocS As New SQLEngine.CollateralMgt2.DocReceive
        Return DADocS.SaveDocReceive(customclass)
    End Function

    Public Function SaveDocReceiveJanji(ByVal customclass As Parameter.DocRec) As Parameter.DocRec Implements [Interface].IDoc.SaveDocReceiveJanji
        Dim DADocS As New SQLEngine.CollateralMgt2.DocReceive
        Return DADocS.SaveDocReceiveJanji(customclass)
    End Function


    Public Function GetAssetRegistration(ByVal customclass As Parameter.DocRec) As Parameter.DocRec Implements [Interface].IDoc.GetAssetRegistration
        Dim DADocS As New SQLEngine.CollateralMgt2.DocReceive
        Return DADocS.GetAssetRegistration(customclass)
    End Function

    Public Function GetTaxDate(ByVal customclass As Parameter.DocRec) As Parameter.DocRec Implements [Interface].IDoc.GetTaxDate
        Dim DADocS As New SQLEngine.CollateralMgt2.DocReceive
        Return DADocS.GetTaxDate(customclass)
    End Function

    Public Function GetBranchName(ByVal customclass As Parameter.DocRec) As Parameter.DocRec Implements [Interface].IDoc.GetBranchName
        Dim DADocS As New SQLEngine.CollateralMgt2.DocReceive
        Return DADocS.GetBranchName(customclass)
    End Function
#End Region
#Region "DocBorrow"

    Public Function DocBorrowSave(ByVal customclass As Parameter.DocRec) As Parameter.DocRec Implements [Interface].IDoc.DocBorrowSave
        Dim DAB As New SQLEngine.CollateralMgt2.DocReceive
        Return DAB.SaveDocBorrow(customclass)
    End Function

#End Region

#Region "DocBorrowPledging"

    Public Function DocBorrowPledging(ByVal customclass As Parameter.DocRec) As Parameter.DocRec Implements [Interface].IDoc.DocBorrowPledgingSave
        Dim DAB As New SQLEngine.CollateralMgt2.DocReceive
        Return DAB.SaveDocBorrowPledging(customclass)
    End Function

#End Region
#Region "DocRelease"
    Public Function DocReleaseSave(ByVal customclass As Parameter.DocRec) As Parameter.DocRec Implements [Interface].IDoc.DocReleaseSave
        Dim DAL As New SQLEngine.CollateralMgt2.DocReceive
        Return DAL.SaveDocRelease(customclass)
    End Function


    Public Function ListInfo(ByVal customclass As Parameter.DocRec) As Parameter.DocRec Implements [Interface].IDoc.ListInfo
        Dim DAList As New SQLEngine.CollateralMgt2.DocReceive
        Return DAList.ListInfo(customclass)
    End Function

    Public Function PrintBPKBSerahTerima(ByVal customclass As Parameter.DocRec) As Parameter.DocRec Implements [Interface].IDoc.PrintBPKBSerahTerima
        Dim DAList As New SQLEngine.CollateralMgt2.DocReceive
        Return DAList.PrintBPKBSerahTerima(customclass)
    End Function
#End Region
#Region "DocChangeLoc"
    Public Function DocChangeLocSave(ByVal customclass As Parameter.DocRec) As Parameter.DocRec Implements [Interface].IDoc.DocChangeLocSave
        Dim DALoc As New SQLEngine.CollateralMgt2.DocReceive
        Return DALoc.SaveDocChangeLoc(customclass)
    End Function
#End Region
#Region "Inquiry"
    Public Function GetRackBranch(ByVal customclass As Parameter.DocRec) As System.Data.DataTable Implements [Interface].IDoc.GetRackBranch
        Dim DAGetFLoc As New SQLEngine.CollateralMgt2.DocReceive
        Return DAGetFLoc.GetRackBranch(customclass)
    End Function

    Public Function ListInquiry1(ByVal customclass As Parameter.DocRec) As Parameter.DocRec Implements [Interface].IDoc.ListInquiry
        Dim DAGetFLoc As New SQLEngine.CollateralMgt2.DocReceive
        Return DAGetFLoc.ListInquiry(customclass)
    End Function

    Public Function GetAssetHistory(ByVal customclass As Parameter.DocRec) As Parameter.DocRec Implements [Interface].IDoc.GetAssetHistory
        Dim DAGetFLoc As New SQLEngine.CollateralMgt2.DocReceive
        Return DAGetFLoc.GetAssetHistory(customclass)
    End Function
    Public Function GetBPKBPosition(ByVal customclass As Parameter.DocRec) As Parameter.DocRec Implements [Interface].IDoc.GetBPKBPosition
        Dim DAGetFLoc As New SQLEngine.CollateralMgt2.DocReceive
        Return DAGetFLoc.GetBPKBPosition(customclass)
    End Function

    Public Function SaveDocAssetBNI(ByVal customclass As Parameter.DocAssetBNI) As Parameter.DocAssetBNI Implements [Interface].IDoc.SaveDocAssetBNI
        Dim DASaveDocAssetBNI As New SQLEngine.CollateralMgt2.DocReceive
        Return DASaveDocAssetBNI.SaveDocAssetBNI(customclass)
    End Function

    Public Function ListTboInquiry(cnn As String, currentPage As Integer, PageSize As Integer, branchid As String, businessdate As DateTime, aging As DateTime, where As String) As Parameter.DocRec Implements [Interface].IDoc.ListTboInquiry
        Dim DAGetFLoc As New SQLEngine.CollateralMgt2.DocReceive
        Return DAGetFLoc.ListTboInquiry(cnn, currentPage, PageSize, branchid, businessdate, aging, where)

    End Function
#End Region
#Region "Setting"

    Public Function AddRack(ByVal customclass As Parameter.DocRec) As Parameter.DocRec Implements [Interface].IDoc.AddRack
        Dim DARackAdd As New SQLEngine.CollateralMgt2.DocReceive
        Return DARackAdd.AddRack(customclass)
    End Function

    Public Function DeleteRack(ByVal customclass As Parameter.DocRec) As Parameter.DocRec Implements [Interface].IDoc.DeleteRack
        Dim DARackDel As New SQLEngine.CollateralMgt2.DocReceive
        Return DARackDel.DeleteRack(customclass)
    End Function

#Region "Filling"
    Public Function AddFill(ByVal customclass As Parameter.DocRec) As Parameter.DocRec Implements [Interface].IDoc.AddFill
        Dim DAFillAdd As New SQLEngine.CollateralMgt2.DocReceive
        Return DAFillAdd.AddFill(customclass)
    End Function

    Public Function DeleteFill(ByVal customclass As Parameter.DocRec) As Parameter.DocRec Implements [Interface].IDoc.DeleteFill
        Dim DAFillDel As New SQLEngine.CollateralMgt2.DocReceive
        Return DAFillDel.DeleteFill(customclass)
    End Function

#End Region
#End Region
#Region "Reports"
    Public Function NotExistsMainDocReport(ByVal customclass As Parameter.DocRec) As Parameter.DocRec Implements [Interface].IDoc.NotExistsMainDocReport
        Dim DAListDoc As New SQLEngine.CollateralMgt2.DocReceive
        Return DAListDoc.NotExistsMainDocReport(customclass)
    End Function
    Public Function SummaryBPKBReport(ByVal customclass As Parameter.DocRec) As Parameter.DocRec Implements [Interface].IDoc.SummaryBPKBReport
        Dim DAListDoc As New SQLEngine.CollateralMgt2.DocReceive
        Return DAListDoc.SummaryBPKBReport(customclass)
    End Function
    Public Function CreateSPAssetDocument(ByVal customclass As Parameter.DocRec) As Parameter.DocRec Implements [Interface].IDoc.CreateSPAssetDocument
        Dim DAListDoc As New SQLEngine.CollateralMgt2.DocReceive
        Return DAListDoc.CreateSPAssetDocument(customclass)
    End Function
    Public Function SavePrintSPAssetDoc(ByVal customclass As Parameter.DocRec) As Parameter.DocRec Implements [Interface].IDoc.SavePrintSPAssetDoc
        Dim DAListDoc As New SQLEngine.CollateralMgt2.DocReceive
        Return DAListDoc.SavePrintSPAssetDoc(customclass)
    End Function
    Public Function SavePrintSPPADoc(ByVal customclass As Parameter.DocRec) As Parameter.DocRec Implements [Interface].IDoc.SavePrintSPPADoc
        Dim DA As New SQLEngine.CollateralMgt2.DocReceive
        Return DA.SavePrintSPPADoc(customclass)
    End Function
    Public Function AgreementListADWithDrawal(ByVal customclass As Parameter.DocRec) As Parameter.DocRec Implements [Interface].IDoc.AgreementListADWithDrawal
        Dim DAListDoc As New SQLEngine.CollateralMgt2.DocReceive
        Return DAListDoc.AgreementListADWithDrawal(customclass)
    End Function
    Public Function ListPemeriksaanBPKB(ByVal customclass As Parameter.DocRec) As Parameter.DocRec Implements [Interface].IDoc.ListPemeriksaanBPKB
        Dim DAListDoc As New SQLEngine.CollateralMgt2.DocReceive
        Return DAListDoc.ListPemeriksaanBPKB(customclass)
    End Function

    Public Function ListReportPemeriksaanBPKB(ByVal customclass As Parameter.DocRec) As Parameter.DocRec Implements [Interface].IDoc.ListReportPemeriksaanBPKB
        Dim DAListDoc As New SQLEngine.CollateralMgt2.DocReceive
        Return DAListDoc.ListReportPemeriksaanBPKB(customclass)
    End Function

    Public Function ListBlokirBPKB(ByVal customclass As Parameter.DocRec) As Parameter.DocRec Implements [Interface].IDoc.ListBlokirBPKB
        Dim DAListDoc As New SQLEngine.CollateralMgt2.DocReceive
        Return DAListDoc.ListBlokirBPKB(customclass)
    End Function

    Public Function ListReportBlokirBPKB(ByVal customclass As Parameter.DocRec) As Parameter.DocRec Implements [Interface].IDoc.ListReportBlokirBPKB
        Dim DAListDoc As New SQLEngine.CollateralMgt2.DocReceive
        Return DAListDoc.ListReportBlokirBPKB(customclass)
    End Function

    Public Function ListBukaBlokirBPKB(ByVal customclass As Parameter.DocRec) As Parameter.DocRec Implements [Interface].IDoc.ListBukaBlokirBPKB
        Dim DAListDoc As New SQLEngine.CollateralMgt2.DocReceive
        Return DAListDoc.ListBukaBlokirBPKB(customclass)
    End Function

    Public Function ListReportBukaBlokirBPKB(ByVal customclass As Parameter.DocRec) As Parameter.DocRec Implements [Interface].IDoc.ListReportBukaBlokirBPKB
        Dim DAListDoc As New SQLEngine.CollateralMgt2.DocReceive
        Return DAListDoc.ListReportBukaBlokirBPKB(customclass)
    End Function

    Public Function ListPinjamNamaBPKB(ByVal customclass As Parameter.DocRec) As Parameter.DocRec Implements [Interface].IDoc.ListPinjamNamaBPKB
        Dim DAListDoc As New SQLEngine.CollateralMgt2.DocReceive
        Return DAListDoc.ListPinjamNamaBPKB(customclass)
    End Function

    Public Function ListReportPinjamNamaBPKB(ByVal customclass As Parameter.DocRec) As Parameter.DocRec Implements [Interface].IDoc.ListReportPinjamNamaBPKB
        Dim DAListDoc As New SQLEngine.CollateralMgt2.DocReceive
        Return DAListDoc.ListReportPinjamNamaBPKB(customclass)
    End Function

    Public Function GetSPReport(customclass As Parameter.DocRec) As Parameter.DocRec Implements [Interface].IDoc.GetSPReport
        Dim DAListDoc As New SQLEngine.CollateralMgt2.DocReceive
        Return DAListDoc.GetSPReport(customclass)
    End Function
#End Region
#Region "DocPledge"
    Public Function DocPledgeProcess(ByVal customclass As Parameter.DocRec) As Parameter.DocRec Implements [Interface].IDoc.DocPledgeProcess
        Dim DAB As New SQLEngine.CollateralMgt2.DocReceive
        Return DAB.DocPledgeProcess(customclass)
    End Function
    Public Function GetDocPledgePaging(ByVal customClass As Parameter.DocRec) As Parameter.DocRec Implements [Interface].IDoc.GetDocPledgePaging

        Dim DA As New SQLEngine.CollateralMgt2.DocReceive
        Return DA.GetDocPledgePaging(customClass)

    End Function
    Public Function ProcessReportPrepaymentRequest(ByVal customClass As Parameter.DocRec) As Parameter.DocRec Implements [Interface].IDoc.ProcessReportPrepaymentRequest
        Dim DA As New SQLEngine.CollateralMgt2.DocReceive
        Return DA.ProcessReportPrepaymentRequest(customClass)
    End Function
#End Region
#Region "AdditionalProcess"
    Public Function SaveEdit(ByVal customclass As Parameter.DocRec, ByVal oData1 As DataTable) As Parameter.DocRec Implements [Interface].IDoc.SaveEdit
        Dim DAGetFLoc As New SQLEngine.CollateralMgt2.DocReceive
        Return DAGetFLoc.SaveEdit(customclass, oData1)
    End Function
#End Region
#Region "DocPledgeRecSave"
    Public Function DocPledgeRecSave(ByVal customclass As Parameter.DocRec) As Parameter.DocRec Implements [Interface].IDoc.DocPledgeRecSave
        Dim DADocS As New SQLEngine.CollateralMgt2.DocReceive
        Return DADocS.DocPledgeRecSave(customclass)
    End Function
#End Region


    Public Function getAssetDocumentStock(ByVal customclass As Parameter.DocRec) As Parameter.DocRec Implements [Interface].IDoc.getAssetDocumentStock
        Dim DAListDoc As New SQLEngine.CollateralMgt2.DocReceive
        Return DAListDoc.getAssetDocumentStock(customclass)
    End Function

    Public Function PemeriksaanBPKBSave(customclass As Parameter.DocRec) As Parameter.DocRec Implements [Interface].IDoc.PemeriksaanBPKBSave
        Dim DAListDoc As New SQLEngine.CollateralMgt2.DocReceive
        Return DAListDoc.PemeriksaanBPKBSave(customclass)
    End Function

    Public Function HasilCekBPKBSave(customclass As Parameter.DocRec) As Parameter.DocRec Implements [Interface].IDoc.HasilCekBPKBSave
        Dim DAListDoc As New SQLEngine.CollateralMgt2.DocReceive
        Return DAListDoc.HasilCekBPKBSave(customclass)
    End Function

    Public Function DoCreateStockOpname(cnn As String, rows As Parameter.StockOpDoc) As String Implements [Interface].IDoc.DoCreateStockOpname
        Dim DAListDoc As New SQLEngine.CollateralMgt2.StockOpname
        Return DAListDoc.DoCreateStockOpname(cnn, rows)
    End Function

    Public Function GetResultOpnamePage(cnn As String, currentPage As Integer, pageSize As Integer, opNameNo As String) As Parameter.StockOpDoc Implements [Interface].IDoc.GetResultOpnamePage
        Dim DAListDoc As New SQLEngine.CollateralMgt2.StockOpname
        Return DAListDoc.GetResultOpnamePage(cnn, currentPage, pageSize, opNameNo)
    End Function


    Public Function GetProcesResultOnhandOpnamePage(cnn As String, currentPage As Integer, pageSize As Integer, opNameNo As String) As Parameter.StockOpDoc Implements [Interface].IDoc.GetProcesResultOnhandOpnamePage
        Dim DAListDoc As New SQLEngine.CollateralMgt2.StockOpname
        Return DAListDoc.GetProcesResultOpnamePage(cnn, currentPage, pageSize, opNameNo, True)
    End Function

    Public Function GetProcesResultActualOpnamePage(cnn As String, currentPage As Integer, pageSize As Integer, opNameNo As String) As Parameter.StockOpDoc Implements [Interface].IDoc.GetProcesResultActualOpnamePage
        Dim DAListDoc As New SQLEngine.CollateralMgt2.StockOpname
        Return DAListDoc.GetProcesResultOpnamePage(cnn, currentPage, pageSize, opNameNo, False)
    End Function
    Public Function ProsesStockOpname(cnn As String, StockOpnameNo As String, ActStockOpnameNo As String, prosesDate As DateTime) As String Implements [Interface].IDoc.ProsesStockOpname
        Dim DAListDoc As New SQLEngine.CollateralMgt2.StockOpname
        Return DAListDoc.ProsesStockOpname(cnn, StockOpnameNo, ActStockOpnameNo, prosesDate)
    End Function

    Public Function getOpnameNo(cnn As String, prm As DateTime, str As String) As String Implements [Interface].IDoc.getOpnameNo
        Dim DAListDoc As New SQLEngine.CollateralMgt2.StockOpname
        Return DAListDoc.getOpnameNo(cnn, prm, str)
    End Function

    Public Function GetResultActualOpnamePage(cnn As String, currentPage As Integer, pageSize As Integer, opNameNo As String) As Parameter.StockOpDoc Implements [Interface].IDoc.GetResultActualOpnamePage
        Dim DAListDoc As New SQLEngine.CollateralMgt2.StockOpname
        Return DAListDoc.GetResultActualOpnamePage(cnn, currentPage, pageSize, opNameNo)
    End Function

    Public Function DocInformation(ByVal customclass As Parameter.DocRec) As DataTable Implements [Interface].IDoc.DocInformation
        Dim DAListDoc As New SQLEngine.CollateralMgt2.StockOpname
        Return DAListDoc.DocInformation(customclass)
    End Function

    Public Sub SaveStockOpnameActual(ByVal CustomClass As Parameter.StockOpDoc) Implements [Interface].IDoc.SaveStockOpnameActual
        Dim DAListDoc As New SQLEngine.CollateralMgt2.StockOpname
        DAListDoc.SaveStockOpnameActual(CustomClass)
    End Sub

    Public Function ProcessReportBBNRequest(ByVal customClass As Parameter.DocRec) As Parameter.DocRec Implements [Interface].IDoc.ProcessReportBBNRequest
        Dim DA As New SQLEngine.CollateralMgt2.DocReceive
        Return DA.ProcessReportBBNRequest(customClass)
    End Function

End Class
  