﻿
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class MailType : Inherits Maxiloan.SQLEngine.DataAccessBase
#Region " Private Const "
    'Stored Procedure name    
    Private Const LIST_SELECT As String = "spPagingMailType"
    Private Const LIST_BY_ID As String = "spMailTypeEdit"
    Private Const LIST_ADD As String = "spMailTypeSaveAdd"
    Private Const LIST_UPDATE As String = "spMailTypeSaveEdit"
    Private Const LIST_DELETE As String = "spMailTypeDelete"
    Private Const LIST_REPORT As String = "spMailTypeReport"
    Private Const LIST_SECTOR As String = "spMailTypeSector"
#End Region

    Public Function GetMailType(ByVal oCustomClass As Parameter.MailType) As Parameter.MailType
        Dim oReturnValue As New Parameter.MailType
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.MailType.GetMailType")
        End Try
    End Function

    Public Function GetMailTypeReport(ByVal oCustomClass As Parameter.MailType) As Parameter.MailType
        Dim oReturnValue As New Parameter.MailType
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@CmdWhere", SqlDbType.VarChar, 1000)
        params(0).Value = oCustomClass.WhereCond
        Try
            oReturnValue.listdataReport = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_REPORT, params)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.MailType.GetMailTypeReport")
        End Try
    End Function

    Public Function GetSector(ByVal oCustomClass As Parameter.MailType) As Parameter.MailType
        Dim oReturnValue As New Parameter.MailType
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SECTOR).Tables(0)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.MailType.GetSector")
        End Try
    End Function

    Public Function GetMailTypeEdit(ByVal ocustomClass As Parameter.MailType) As Parameter.MailType
        Dim reader As SqlDataReader
        Dim params(0) As SqlParameter
        Dim oReturnValue As New Parameter.MailType
        params(0) = New SqlParameter("@MailTypeID", SqlDbType.Char, 10)
        params(0).Value = ocustomClass.MailTypeID
        Try
            reader = SqlHelper.ExecuteReader(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_BY_ID, params)
            If reader.Read Then
                oReturnValue.Description = reader("description").ToString
                oReturnValue.PICName = reader("PICName").ToString
            End If
            reader.Close()
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.MailType.GetMailTypeEdit")
        End Try
    End Function

    Public Function MailTypeSaveAdd(ByVal ocustomClass As Parameter.MailType) As String
        Dim ErrMessage As String = ""
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@MailTypeID", SqlDbType.Char, 10)
        params(0).Value = ocustomClass.MailTypeID
        params(1) = New SqlParameter("@Description", SqlDbType.VarChar, 100)
        params(1).Value = ocustomClass.Description
        params(2) = New SqlParameter("@PICName", SqlDbType.Char, 10)
        params(2).Value = ocustomClass.PICName
        params(3) = New SqlParameter("@PICJobTitle", SqlDbType.Decimal)
        params(3).Value = ocustomClass.PICJobTitle
        params(4) = New SqlParameter("@Err", SqlDbType.VarChar, 50)
        params(4).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_ADD, params)
            ErrMessage = CType(params(4).Value, String)
            If ErrMessage <> "" Then
                Return ErrMessage
            End If
            Return ""
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.MailType.MailTypeSaveAdd")
        End Try
    End Function

    Public Sub MailTypeSaveEdit(ByVal ocustomClass As Parameter.MailType)
        Dim params(3) As SqlParameter
        params(0) = New SqlParameter("@Id", SqlDbType.Char, 10)
        params(0).Value = ocustomClass.MailTypeID
        params(1) = New SqlParameter("@Description", SqlDbType.VarChar, 100)
        params(1).Value = ocustomClass.Description
        params(2) = New SqlParameter("@PICName", SqlDbType.Char, 10)
        params(2).Value = ocustomClass.PICName
        params(3) = New SqlParameter("@PICJobTitle", SqlDbType.Decimal)
        params(3).Value = ocustomClass.PICJobTitle
        Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_UPDATE, params)
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.MailType.MailTypeSaveEdit")
        End Try
    End Sub

    Public Function MailTypeDelete(ByVal ocustomClass As Parameter.MailType) As String
        Dim Err As Integer
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@MailTypeID", SqlDbType.Char, 10)
        params(0).Value = ocustomClass.MailTypeID
        Try
            Err = SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_DELETE, params)
            Return ""
        Catch ex As Exception
            Return "ERROR: Unable to delete the selected record. Record already used!"
            'Throw New Exception("Error On DataAccess.Setting.MailType.MailTypeDelete")
        End Try
    End Function
End Class
