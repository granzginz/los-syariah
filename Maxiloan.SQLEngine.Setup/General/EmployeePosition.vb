

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region


Public Class EmployeePosition : Inherits DataAccessBase

    Private Const spEmployeePositionList As String = "spEmployeePositionList"
    Private Const spEmployeePositionReport As String = "spEmployeePositionReport"
    Private Const spEmployeePositionAdd As String = "spEmployeePositionAdd"
    Private Const spEmployeePositionEdit As String = "spEmployeePositionEdit"
    Private Const spEmployeePositionDelete As String = "spEmployeePositionDelete"

    Public Function EmployeePositionList(ByVal customclass As Parameter.EmployeePosition) As Parameter.EmployeePosition

        Dim params() As SqlParameter = New SqlParameter(4) {}
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customclass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customclass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = customclass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = customclass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            customclass.listdata = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spEmployeePositionList, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
        Catch ex As Exception
        End Try

        Return customclass
    End Function

    Public Sub EmployeePositionAdd(ByVal customclass As Parameter.EmployeePosition)
        Dim oSaveTransaction As SqlTransaction
        Dim oConnection As New SqlConnection(customclass.strConnection)

        Dim params() As SqlParameter = New SqlParameter(1) {}

        params(0) = New SqlParameter("@EmployeePositionID", SqlDbType.VarChar, 10)
        params(0).Value = customclass.EmployeePositionID

        params(1) = New SqlParameter("@EmployeePosition", SqlDbType.VarChar, 50)
        params(1).Value = customclass.EmployeeDesription

        Try
            If oConnection.State = ConnectionState.Closed Then oConnection.Open()
            oSaveTransaction = oConnection.BeginTransaction
            SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, spEmployeePositionAdd, params)
            oSaveTransaction.Commit()
        Catch ex As Exception
            oSaveTransaction.Rollback()
            Throw ex
        End Try
    End Sub

    Public Sub EmployeePositionEdit(ByVal customclass As Parameter.EmployeePosition)
        Dim oSaveTransaction As SqlTransaction
        Dim oConnection As New SqlConnection(customclass.strConnection)

        Dim params() As SqlParameter = New SqlParameter(1) {}

        params(0) = New SqlParameter("@EmployeePositionID", SqlDbType.VarChar, 10)
        params(0).Value = customclass.EmployeePositionID

        params(1) = New SqlParameter("@EmployeePosition", SqlDbType.VarChar, 50)
        params(1).Value = customclass.EmployeeDesription

        Try
            If oConnection.State = ConnectionState.Closed Then oConnection.Open()
            oSaveTransaction = oConnection.BeginTransaction
            SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, spEmployeePositionEdit, params)
            oSaveTransaction.Commit()
        Catch ex As Exception
            oSaveTransaction.Rollback()
            Throw ex
        End Try

    End Sub

    Public Sub EmployeePositionDelete(ByVal customclass As Parameter.EmployeePosition)
        Dim params() As SqlParameter = New SqlParameter(0) {}

        params(0) = New SqlParameter("@EmployeePositionID", SqlDbType.VarChar, 10)
        params(0).Value = customclass.EmployeePositionID

        Try
            SqlHelper.ExecuteNonQuery(customclass.strConnection, CommandType.StoredProcedure, spEmployeePositionDelete, params)
        Catch ex As Exception

        End Try

    End Sub

    Public Function EmployeePositionReport(ByVal customclass As Parameter.EmployeePosition) As DataTable

        Try
            Dim oEmployeePosition As New Parameter.EmployeePosition

            oEmployeePosition.listdata = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spEmployeePositionReport).Tables(0)

            Return oEmployeePosition.listdata
            'Trace.WriteIf(MethodTrace.Enabled, "Exit")

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)
        End Try
    End Function



End Class
