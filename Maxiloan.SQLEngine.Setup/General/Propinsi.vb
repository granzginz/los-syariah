
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class Propinsi : Inherits Maxiloan.SQLEngine.DataAccessBase
#Region " Private Const "
    'Stored Procedure name    
    Private Const LIST_SELECT As String = "spPagingPropinsi"
    Private Const LIST_SELECT_ALL As String = "spPropinsiListAll"
    Private Const LIST_BY_ID As String = "spPropinsiList"
    Private Const LIST_ADD As String = "spProvinceSaveAdd"
    Private Const LIST_UPDATE As String = "spProvinceSaveEdit"
    Private Const LIST_DELETE As String = "spPropinsiDelete"
    Private Const LIST_REPORT As String = "spPropinsiReport"
#End Region

    Public Function GetPropinsi(ByVal oCustomClass As Parameter.Propinsi) As Parameter.Propinsi
        Dim oReturnValue As New Parameter.Propinsi
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.Listdata = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            oReturnValue.Totalrecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.Propinsi.GetPropinsi")
        End Try
    End Function

    Public Function GetPropinsiList(ByVal ocustomClass As Parameter.Propinsi) As Parameter.Propinsi
        Dim reader As SqlDataReader
        Dim params(0) As SqlParameter
        Dim oReturnValue As New Parameter.Propinsi
        params(0) = New SqlParameter("@Description", SqlDbType.VarChar, 50)
        params(0).Value = ocustomClass.Description
        Try
            reader = SqlHelper.ExecuteReader(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_BY_ID, params)
            If reader.Read Then
                oReturnValue.Description = reader("Description").ToString
            End If
            reader.Close()
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.Propinsi.GetPropinsiList")
        End Try
    End Function

    Public Function GetPropinsiCombo(ByVal customclass As Parameter.Propinsi) As DataTable
        Return SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, LIST_SELECT_ALL).Tables(0)
    End Function

    Public Function ProvinceSaveAdd(ByVal ocustomClass As Parameter.Propinsi) As String
        Dim ErrMessage As String = ""
        Dim params(2) As SqlParameter
 
        params(0) = New SqlParameter("@Description", SqlDbType.VarChar, 50)
        params(0).Value = ocustomClass.Description
        params(1) = New SqlParameter("@InsuranceAreaId", SqlDbType.Char, 1)
        params(1).Value = ocustomClass.InsuranceAreaId
        params(2) = New SqlParameter("@Err", SqlDbType.VarChar, 50)
        params(2).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_ADD, params)
            ErrMessage = CType(params(2).Value, String)
            If ErrMessage <> "" Then
                Return ErrMessage
            End If
            Return ""
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.Propinsi.ProvinceSaveAdd")
        End Try
    End Function
    Public Sub ProvinceSaveEdit(ByVal ocustomClass As Parameter.Propinsi)
        Dim params(1) As SqlParameter
        params(0) = New SqlParameter("@Description", SqlDbType.VarChar, 50)
        params(0).Value = ocustomClass.Description
        params(1) = New SqlParameter("@InsuranceAreaId", SqlDbType.Char, 1)
        params(1).Value = ocustomClass.InsuranceAreaId
        Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_UPDATE, params)
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.Propinsi.ProvinceSaveEdit")
        End Try
    End Sub
End Class
