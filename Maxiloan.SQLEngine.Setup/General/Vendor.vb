
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class Vendor : Inherits Maxiloan.SQLEngine.DataAccessBase
#Region " Private Const "
    'Stored Procedure name    
    Private Const LIST_SELECT As String = "spVendorPaging"
    Private Const LIST_BY_ID As String = "spVendorView"
    Private Const LIST_ADD As String = "spVendorSaveAdd"
    Private Const LIST_UPDATE As String = "spVendorSaveEdit"
    Private Const LIST_DELETE As String = "spVendorDelete"

#End Region

    Public Function GetVendor(ByVal oCustomClass As Parameter.Vendor) As Parameter.Vendor
        Dim oReturnValue As New Parameter.Vendor
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.Listdata = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            oReturnValue.Totalrecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.Vendor.GetVendor")
        End Try
    End Function

    Public Function GetVendorList(ByVal ocustomClass As Parameter.Vendor) As Parameter.Vendor
        Dim reader As SqlDataReader
        Dim params(0) As SqlParameter
        Dim oReturnValue As New Parameter.Vendor
        params(0) = New SqlParameter("@Id", SqlDbType.Char, 20)
        params(0).Value = ocustomClass.VendorID
        Try
            reader = SqlHelper.ExecuteReader(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_BY_ID, params)
            If reader.Read Then
                oReturnValue.VendorID = ocustomClass.VendorID
                oReturnValue.VendorID = reader("VendorID").ToString
                oReturnValue.NamaVendor = reader("NamaVendor").ToString
                oReturnValue.BidangUsaha = reader("BidangUsaha").ToString
                oReturnValue.NPWP = reader("NPWP").ToString
                oReturnValue.TDP = reader("TDP").ToString
                oReturnValue.SIUP = reader("SIUP").ToString
                oReturnValue.TglMulai = reader("TglMulai").ToString
                oReturnValue.Alamat = reader("Alamat").ToString
                oReturnValue.RT = reader("RT").ToString
                oReturnValue.RW = reader("RW").ToString
                oReturnValue.Kelurahan = reader("Kelurahan").ToString
                oReturnValue.Kecamatan = reader("Kecamatan").ToString
                oReturnValue.Kota = reader("Kota").ToString
                oReturnValue.KodePos = reader("KodePos").ToString
                oReturnValue.AreaPhone1 = reader("AreaPhone1").ToString
                oReturnValue.Phone1 = reader("Phone1").ToString
                oReturnValue.AreaPhone2 = reader("AreaPhone2").ToString
                oReturnValue.Phone2 = reader("Phone2").ToString
                oReturnValue.AreaFax = reader("AreaFax").ToString
                oReturnValue.Fax = reader("Fax").ToString
                oReturnValue.Kontak = reader("Kontak").ToString
                oReturnValue.Jabatan = reader("Jabatan").ToString
                oReturnValue.Email = reader("Email").ToString
                oReturnValue.NoHP = reader("NoHP").ToString
                oReturnValue.BankID = reader("BankID").ToString
                oReturnValue.BankBranchID = CInt(reader("BankBranchID").ToString)
                oReturnValue.AccounNo = reader("AccounNo").ToString
                oReturnValue.AccountName = reader("AccountName").ToString
            End If
            reader.Close()
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.Vendor.GetVendorList")
        End Try
    End Function

    Public Function VendorSaveAdd(ByVal ocustomClass As Parameter.Vendor) As String
        Dim ErrMessage As String = ""
        Dim params(30) As SqlParameter
        params(0) = New SqlParameter("@VendorID", SqlDbType.Char, 20)
        params(0).Value = ocustomClass.VendorID
        params(1) = New SqlParameter("@NamaVendor", SqlDbType.VarChar, 50)
        params(1).Value = ocustomClass.NamaVendor
        params(2) = New SqlParameter("@BidangUsaha", SqlDbType.VarChar, 50)
        params(2).Value = ocustomClass.BidangUsaha
        params(3) = New SqlParameter("@NPWP", SqlDbType.VarChar, 50)
        params(3).Value = ocustomClass.NPWP
        params(4) = New SqlParameter("@TDP", SqlDbType.VarChar, 50)
        params(4).Value = ocustomClass.TDP
        params(5) = New SqlParameter("@SIUP", SqlDbType.VarChar, 50)
        params(5).Value = ocustomClass.SIUP
        params(6) = New SqlParameter("@TglMulai", SqlDbType.Char, 10)
        params(6).Value = ocustomClass.TglMulai
        params(7) = New SqlParameter("@Alamat", SqlDbType.VarChar, 50)
        params(7).Value = ocustomClass.Alamat
        params(8) = New SqlParameter("@RT", SqlDbType.Char, 3)
        params(8).Value = ocustomClass.RT
        params(9) = New SqlParameter("@RW", SqlDbType.Char, 3)
        params(9).Value = ocustomClass.RW
        params(10) = New SqlParameter("@Kelurahan", SqlDbType.VarChar, 30)
        params(10).Value = ocustomClass.Kelurahan
        params(11) = New SqlParameter("@Kecamatan", SqlDbType.VarChar, 30)
        params(11).Value = ocustomClass.Kecamatan
        params(12) = New SqlParameter("@Kota", SqlDbType.VarChar, 30)
        params(12).Value = ocustomClass.Kota
        params(13) = New SqlParameter("@KodePos", SqlDbType.Char, 5)
        params(13).Value = ocustomClass.KodePos
        params(14) = New SqlParameter("@AreaPhone1", SqlDbType.Char, 4)
        params(14).Value = ocustomClass.AreaPhone1
        params(15) = New SqlParameter("@Phone1", SqlDbType.VarChar, 15)
        params(15).Value = ocustomClass.Phone1
        params(16) = New SqlParameter("@AreaPhone2", SqlDbType.Char, 4)
        params(16).Value = ocustomClass.AreaPhone2
        params(17) = New SqlParameter("@Phone2", SqlDbType.VarChar, 15)
        params(17).Value = ocustomClass.Phone2
        params(18) = New SqlParameter("@AreaFax", SqlDbType.Char, 4)
        params(18).Value = ocustomClass.AreaFax
        params(19) = New SqlParameter("@Fax", SqlDbType.VarChar, 15)
        params(19).Value = ocustomClass.Fax
        params(20) = New SqlParameter("@Kontak", SqlDbType.VarChar, 50)
        params(20).Value = ocustomClass.Kontak
        params(21) = New SqlParameter("@Jabatan", SqlDbType.VarChar, 50)
        params(21).Value = ocustomClass.Jabatan
        params(22) = New SqlParameter("@Email", SqlDbType.VarChar, 50)
        params(22).Value = ocustomClass.Email
        params(23) = New SqlParameter("@NoHP", SqlDbType.VarChar, 15)
        params(23).Value = ocustomClass.NoHP
        params(24) = New SqlParameter("@BankID", SqlDbType.Char, 5)
        params(24).Value = ocustomClass.BankID
        params(25) = New SqlParameter("@BankBranchID", SqlDbType.Int)
        params(25).Value = ocustomClass.BankBranchID
        params(26) = New SqlParameter("@AccounNo", SqlDbType.VarChar, 25)
        params(26).Value = ocustomClass.AccounNo
        params(27) = New SqlParameter("@AccountName", SqlDbType.VarChar, 50)
        params(27).Value = ocustomClass.AccountName
        params(28) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(28).Value = ocustomClass.BranchId
        params(29) = New SqlParameter("@businessdate", SqlDbType.DateTime)
        params(29).Value = ocustomClass.BusinessDate

        params(30) = New SqlParameter("@Err", SqlDbType.VarChar, 50)
        params(30).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_ADD, params)
            ErrMessage = CType(params(30).Value, String)
            If ErrMessage <> "" Then
                Return ErrMessage
            End If
            Return ""
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.Vendor.VendorSaveAdd")
        End Try
    End Function

    Public Sub VendorSaveEdit(ByVal ocustomClass As Parameter.Vendor)
        Dim params(27) As SqlParameter
        params(0) = New SqlParameter("@VendorID", SqlDbType.Char, 20)
        params(0).Value = ocustomClass.VendorID
        params(1) = New SqlParameter("@NamaVendor", SqlDbType.VarChar, 50)
        params(1).Value = ocustomClass.NamaVendor
        params(2) = New SqlParameter("@BidangUsaha", SqlDbType.VarChar, 50)
        params(2).Value = ocustomClass.BidangUsaha
        params(3) = New SqlParameter("@NPWP", SqlDbType.VarChar, 50)
        params(3).Value = ocustomClass.NPWP
        params(4) = New SqlParameter("@TDP", SqlDbType.VarChar, 50)
        params(4).Value = ocustomClass.TDP
        params(5) = New SqlParameter("@SIUP", SqlDbType.VarChar, 50)
        params(5).Value = ocustomClass.SIUP
        params(6) = New SqlParameter("@TglMulai", SqlDbType.VarChar, 10)
        params(6).Value = ocustomClass.TglMulai
        params(7) = New SqlParameter("@Alamat", SqlDbType.VarChar, 50)
        params(7).Value = ocustomClass.Alamat
        params(8) = New SqlParameter("@RT", SqlDbType.Char, 3)
        params(8).Value = ocustomClass.RT
        params(9) = New SqlParameter("@RW", SqlDbType.Char, 3)
        params(9).Value = ocustomClass.RW
        params(10) = New SqlParameter("@Kelurahan", SqlDbType.VarChar, 30)
        params(10).Value = ocustomClass.Kelurahan
        params(11) = New SqlParameter("@Kecamatan", SqlDbType.VarChar, 30)
        params(11).Value = ocustomClass.Kecamatan
        params(12) = New SqlParameter("@Kota", SqlDbType.VarChar, 30)
        params(12).Value = ocustomClass.Kota
        params(13) = New SqlParameter("@KodePos", SqlDbType.Char, 5)
        params(13).Value = ocustomClass.KodePos
        params(14) = New SqlParameter("@AreaPhone1", SqlDbType.Char, 4)
        params(14).Value = ocustomClass.AreaPhone1
        params(15) = New SqlParameter("@Phone1", SqlDbType.VarChar, 15)
        params(15).Value = ocustomClass.Phone1
        params(16) = New SqlParameter("@AreaPhone2", SqlDbType.Char, 4)
        params(16).Value = ocustomClass.AreaPhone2
        params(17) = New SqlParameter("@Phone2", SqlDbType.VarChar, 15)
        params(17).Value = ocustomClass.Phone2
        params(18) = New SqlParameter("@AreaFax", SqlDbType.Char, 4)
        params(18).Value = ocustomClass.AreaFax
        params(19) = New SqlParameter("@Fax", SqlDbType.VarChar, 15)
        params(19).Value = ocustomClass.Fax
        params(20) = New SqlParameter("@Kontak", SqlDbType.VarChar, 50)
        params(20).Value = ocustomClass.Kontak
        params(21) = New SqlParameter("@Jabatan", SqlDbType.VarChar, 50)
        params(21).Value = ocustomClass.Jabatan
        params(22) = New SqlParameter("@Email", SqlDbType.VarChar, 50)
        params(22).Value = ocustomClass.Email
        params(23) = New SqlParameter("@NoHP", SqlDbType.VarChar, 15)
        params(23).Value = ocustomClass.NoHP
        params(24) = New SqlParameter("@BankID", SqlDbType.Char, 5)
        params(24).Value = ocustomClass.BankID
        params(25) = New SqlParameter("@BankBranchID", SqlDbType.Int)
        params(25).Value = ocustomClass.BankBranchID
        params(26) = New SqlParameter("@AccounNo", SqlDbType.VarChar, 25)
        params(26).Value = ocustomClass.AccounNo
        params(27) = New SqlParameter("@AccountName", SqlDbType.VarChar, 50)
        params(27).Value = ocustomClass.AccountName

        Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_UPDATE, params)
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.Vendor.VendorSaveEdit")
        End Try
    End Sub

    Public Function VendorDelete(ByVal ocustomClass As Parameter.Vendor) As String
        Dim Err As Integer
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@Id", SqlDbType.Char, 20)
        params(0).Value = ocustomClass.VendorID
        Try
            Err = SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_DELETE, params)
            Return ""
        Catch ex As Exception
            Return "ERROR: Unable to delete the selected record. Record already used!"
            'Throw New Exception("Error On DataAccess.Setting.Vendor.VendorDelete")
        End Try
    End Function
End Class
