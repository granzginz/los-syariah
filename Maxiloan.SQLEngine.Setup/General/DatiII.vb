
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class DatiII : Inherits Maxiloan.SQLEngine.DataAccessBase
#Region " Private Const "
    'Stored Procedure name    
    Private Const LIST_SELECT As String = "spPagingDati"
    Private Const LIST_BY_ID As String = "spDatiList"
    Private Const LIST_ADD As String = "spDatiSaveAdd"
    Private Const LIST_UPDATE As String = "spDatiSaveEdit"
    Private Const LIST_DELETE As String = "spDatiDelete"
    Private Const LIST_REPORT As String = "spDatiReport"
#End Region

    Public Function GetDati(ByVal oCustomClass As Parameter.Dati) As Parameter.Dati
        Dim oReturnValue As New Parameter.Dati
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.Listdata = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            oReturnValue.Totalrecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.Dati.GetDati")
        End Try
    End Function

    Public Function GetDatiReport(ByVal oCustomClass As Parameter.Dati) As Parameter.Dati
        Dim oReturnValue As New Parameter.Dati
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@CmdWhere", SqlDbType.VarChar, 1000)
        params(0).Value = oCustomClass.WhereCond
        Try
            oReturnValue.ListdataReport = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_REPORT, params)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.Dati.GetDatiReport")
        End Try
    End Function

    Public Function GetDatiList(ByVal ocustomClass As Parameter.Dati) As Parameter.Dati
        Dim reader As SqlDataReader
        Dim params(0) As SqlParameter
        Dim oReturnValue As New Parameter.Dati
        params(0) = New SqlParameter("@Id", SqlDbType.Char, 4)
        params(0).Value = ocustomClass.DatiID
        Try
            reader = SqlHelper.ExecuteReader(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_BY_ID, params)
            If reader.Read Then
                oReturnValue.Type = reader("Type").ToString
                oReturnValue.NamaKabKot = reader("NamaKabKot").ToString
                oReturnValue.Propinsi = reader("Propinsi").ToString
            End If
            reader.Close()
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.Dati.GetDatiList")
        End Try
    End Function

    Public Function DatiSaveAdd(ByVal ocustomClass As Parameter.Dati) As String
        Dim ErrMessage As String = ""
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@DatiID", SqlDbType.VarChar, 4)
        params(0).Value = ocustomClass.DatiID
        params(1) = New SqlParameter("@Type", SqlDbType.VarChar, 10)
        params(1).Value = ocustomClass.Type
        params(2) = New SqlParameter("@NamaKabKot", SqlDbType.VarChar, 50)
        params(2).Value = ocustomClass.NamaKabKot
        params(3) = New SqlParameter("@Propinsi", SqlDbType.VarChar, 50)
        params(3).Value = ocustomClass.Propinsi
        params(4) = New SqlParameter("@Err", SqlDbType.VarChar, 50)
        params(4).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_ADD, params)
            ErrMessage = CType(params(4).Value, String)
            If ErrMessage <> "" Then
                Return ErrMessage
            End If
            Return ""
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.Dati.DatiSaveAdd")
        End Try
    End Function

    Public Sub DatiSaveEdit(ByVal ocustomClass As Parameter.Dati)
        Dim params(3) As SqlParameter
        params(0) = New SqlParameter("@DatiID", SqlDbType.VarChar, 4)
        params(0).Value = ocustomClass.DatiID
        params(1) = New SqlParameter("@Type", SqlDbType.VarChar, 10)
        params(1).Value = ocustomClass.Type
        params(2) = New SqlParameter("@NamaKabKot", SqlDbType.VarChar, 50)
        params(2).Value = ocustomClass.NamaKabKot
        params(3) = New SqlParameter("@Propinsi", SqlDbType.VarChar, 50)
        params(3).Value = ocustomClass.Propinsi
        Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_UPDATE, params)
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.Dati.DatiSaveEdit")
        End Try
    End Sub

    Public Function DatiDelete(ByVal ocustomClass As Parameter.Dati) As String
        Dim Err As Integer
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@Id", SqlDbType.Char, 10)
        params(0).Value = ocustomClass.DatiID
        Try
            Err = SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_DELETE, params)
            Return ""
        Catch ex As Exception
            Return "ERROR: Unable to delete the selected record. Record already used!"            
        End Try
    End Function
End Class
