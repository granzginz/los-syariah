
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class EconomySector : Inherits Maxiloan.SQLEngine.DataAccessBase
#Region " Private Const "
    'Stored Procedure name    
    Private Const LIST_SELECT As String = "spPagingEconomySector"
    Private Const LIST_ADD As String = "spEconomySectorSaveAdd"
    Private Const LIST_UPDATE As String = "spEconomySectorSaveEdit"
    Private Const LIST_DELETE As String = "spEconomySectorDelete"
    Private Const LIST_REPORT As String = "spEconomySectorReport"
#End Region

    Public Function GetEconomySector(ByVal oCustomClass As Parameter.EconomySector) As Parameter.EconomySector
        Dim oReturnValue As New Parameter.EconomySector
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.EconomySector.GetEconomySector")
        End Try
    End Function

    Public Function GetEconomySectorReport(ByVal oCustomClass As Parameter.EconomySector) As Parameter.EconomySector
        Dim oReturnValue As New Parameter.EconomySector
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@CmdWhere", SqlDbType.VarChar, 1000)
        params(0).Value = oCustomClass.WhereCond
        Try
            oReturnValue.listdataReport = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_REPORT, params)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.EconomySector.GetEconomySectorReport")
        End Try
    End Function

    Public Function EconomySectorSaveAdd(ByVal customClass As Parameter.EconomySector) As String
        Dim ErrMessage As String = ""
        Dim params(2) As SqlParameter
        params(0) = New SqlParameter("@Id", SqlDbType.Char, 10)
        params(0).Value = customClass.EconomySectorId
        params(1) = New SqlParameter("@Description", SqlDbType.VarChar, 100)
        params(1).Value = customClass.Description
        params(2) = New SqlParameter("@Err", SqlDbType.VarChar, 50)
        params(2).Direction = ParameterDirection.Output
        Try
            Dim oReturnValue As New Parameter.EconomySector
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, LIST_ADD, params)
            ErrMessage = CType(params(2).Value, String)
            If ErrMessage <> "" Then
                Return ErrMessage
            End If
            Return ""
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)
            Throw New Exception("Error On DataAccess.Setting.EconomySector.EconomySectorSaveAdd")
        End Try
    End Function

    Public Sub EconomySectorSaveEdit(ByVal customClass As Parameter.EconomySector)
        Dim params(1) As SqlParameter
        params(0) = New SqlParameter("@Id", SqlDbType.Char, 10)
        params(0).Value = customClass.EconomySectorId
        params(1) = New SqlParameter("@Description", SqlDbType.VarChar, 100)
        params(1).Value = customClass.Description
        Try
            Dim oReturnValue As New Parameter.EconomySector
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, LIST_UPDATE, params)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)
            Throw New Exception("Error On DataAccess.Setting.EconomySector.EconomySectorSaveEdit")

        End Try
    End Sub

    Public Function EconomySectorDelete(ByVal customClass As Parameter.EconomySector) As String
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@Id", SqlDbType.Char, 10)
        params(0).Value = customClass.EconomySectorId
        Try
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, LIST_DELETE, params)
            Return ""
        Catch ex As Exception
            Return "ERROR: Unable to delete the selected record. Record already used!"
        End Try
    End Function
End Class
