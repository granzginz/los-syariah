﻿#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region
Public Class Currency : Inherits Maxiloan.SQLEngine.DataAccessBase
    Public Sub GetCurrency(ByRef dto As Parameter.Currency)
        Dim listData As DataTable
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@wherecond", SqlDbType.VarChar)
        params(0).Value = dto.WhereCond
        
        Try
            listData = SqlHelper.ExecuteDataset(dto.strConnection, CommandType.StoredProcedure, "GLMasterCurrency_select", params).Tables(0)
            dto.DTOList = New List(Of Parameter.Currency.Detail)
            For Each dt As DataRow In listData.Rows
                dto.DTOList.Add(New Parameter.Currency.Detail With {
                                 .CurrencyId = dt("CurrencyId").ToString(),
                                 .CurrencyName = dt("CurrencyName").ToString(),
                                 .Description = dt("Description").ToString(),
                                 .Rounded = CType(dt("Rounded"), Decimal),
                                 .DailyExchangeRate = CType(dt("DailyExchangeRate"), Decimal),
                                 .InstallmentRounded = CType(dt("InstallmentRounded"), Decimal),
                                 .InsuranceRounded = CType(dt("InsuranceRounded"), Decimal)
                             })
            Next
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.Currency.GetCurrency")
        End Try
    End Sub

End Class
