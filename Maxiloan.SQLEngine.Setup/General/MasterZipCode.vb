

#Region " Imports "
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class MasterZipCode : Inherits Maxiloan.SQLEngine.DataAccessBase
#Region " Private Const "
    'Stored Procedure name    
    Private Const LIST_SELECT As String = "spZipCodePaging"
    Private Const LIST_BY_ID_REPORT As String = "spZipCodeIdReport"
    Private Const LIST_ADD As String = "spZipCodeAdd"
    Private Const LIST_DELETE As String = "spZipCodeDelete"
    'Private Const LIST_UPDATE As String = "spZipCodeUpdate"
#End Region

#Region " GetMasterZipCode "
    Public Function GetMasterZipCode(ByVal oMasterZipCode As Parameter.MasterZipCode) As Parameter.MasterZipCode

        Dim params(4) As SqlParameter

        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oMasterZipCode.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oMasterZipCode.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(2).Value = oMasterZipCode.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 50)
        params(3).Value = oMasterZipCode.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            Dim oReturnValue As New Parameter.MasterZipCode

            oReturnValue.ListData = SqlHelper.ExecuteDataset(oMasterZipCode.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)

            Return oReturnValue            

        Catch ex As Exception
            Throw New Exception(ex.Message)

        End Try
    End Function
#End Region

#Region " GetZipCodeIdReport "
    Public Function GetZipCodeIdReport(ByVal oEntities As Parameter.MasterZipCode) As Parameter.MasterZipCode
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@cmdWhere", SqlDbType.VarChar)
        params(0).Value = oEntities.WhereCond
        Try
            Dim oReturnValue As New Parameter.MasterZipCode

            oReturnValue.ListDataReport = SqlHelper.ExecuteDataset(oEntities.strConnection, CommandType.StoredProcedure, LIST_BY_ID_REPORT, params)
            Return oReturnValue
            'Trace.WriteIf(MethodTrace.Enabled, "Exit")

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)
        End Try

    End Function
#End Region


#Region " MasterZipCodeAdd "
    Public Function MasterZipCodeAdd(ByVal customClass As Parameter.MasterZipCode) As String

        Dim params(8) As SqlParameter

        params(0) = New SqlParameter("@Kelurahan", SqlDbType.VarChar, 30)
        params(0).Value = customClass.Kelurahan
        params(1) = New SqlParameter("@Kecamatan", SqlDbType.VarChar, 30)
        params(1).Value = customClass.Kecamatan
        params(2) = New SqlParameter("@City", SqlDbType.VarChar, 30)
        params(2).Value = customClass.City
        params(3) = New SqlParameter("@ZipCode", SqlDbType.Char, 5)
        params(3).Value = customClass.ZipCode

        params(4) = New SqlParameter("@ResultOutput", SqlDbType.VarChar, 5)
        params(4).Direction = ParameterDirection.Output

        params(5) = New SqlParameter("@CGID", SqlDbType.VarChar, 3)
        params(5).Value = customClass.CGID

        params(6) = New SqlParameter("@CollectorID", SqlDbType.VarChar, 12)
        params(6).Value = customClass.CollectorID

        params(7) = New SqlParameter("@Provinsi", SqlDbType.VarChar, 50)
        params(7).Value = customClass.Provinsi

        params(8) = New SqlParameter("@kodewilayah", SqlDbType.VarChar, 10)
        params(8).Value = customClass.SandiWilayah

        Try
            Dim oReturnValue As New Parameter.MasterZipCode

            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, LIST_ADD, params)
            Return CType(params(4).Value, String)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)
            'Throw New Exception("Error On DataAccess.ZipCode.MasterZipCode.MasterZipCodeAdd")
        End Try

    End Function
#End Region

    '#Region " MasterZipCodeUpdate "
    'Public Sub MasterZipCodeUpdate(ByVal customClass As Parameter.MasterZipCode)
    '    Dim params(5) As SqlParameter

    '    params(0) = New SqlParameter("@Kelurahan", SqlDbType.VarChar)
    '    params(0).Value = customClass.Kelurahan
    '    params(1) = New SqlParameter("@Kecamatan", SqlDbType.VarChar)
    '    params(1).Value = customClass.Kecamatan
    '    params(2) = New SqlParameter("@City", SqlDbType.VarChar)
    '    params(2).Value = customClass.City
    '    params(3) = New SqlParameter("@ZipCode", SqlDbType.VarChar)
    '    params(3).Value = customClass.ZipCode
    '    params(4) = New SqlParameter("@UsrUpd", SqlDbType.VarChar)
    '    params(4).Value = customClass.UserName
    '    params(5) = New SqlParameter("@DtmUpd", SqlDbType.DateTime)
    '    params(5).Value = customClass.DtmUpd


    '    Dim reader As SqlDataReader
    '    Try
    '        Dim oReturnValue As New Parameter.MasterZipCode

    '        SqlHelper.ExecuteNonQuery(m_connection, CommandType.StoredProcedure, LIST_UPDATE, params)

    '    Catch ex As Exception
    '        'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)
    '        Throw New Exception("Error On DataAccess.Company.MasterCompany.MasterCompanyEdt")
    '    Finally
    '        If Not m_connection Is Nothing Then
    '            m_connection.Close()
    '        End If
    '    End Try

    'End Sub
    '#End Region

#Region " MasterZipCodeDelete"
    Public Function MasterZipCodeDelete(ByVal customClass As Parameter.MasterZipCode) As String        
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@Kelurahan", SqlDbType.VarChar, 30)
        params(0).Value = customClass.Kelurahan
        params(1) = New SqlParameter("@Kecamatan", SqlDbType.VarChar, 30)
        params(1).Value = customClass.Kecamatan
        params(2) = New SqlParameter("@City", SqlDbType.VarChar, 30)
        params(2).Value = customClass.City
        params(3) = New SqlParameter("@ZipCode", SqlDbType.Char, 5)
        params(3).Value = customClass.ZipCode
        params(4) = New SqlParameter("@ResultOutput", SqlDbType.VarChar, 5)
        params(4).Direction = ParameterDirection.Output

        'params(4) = New SqlParameter("@Output", SqlDbType.VarChar)
        'params(4).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, LIST_DELETE, params)
            Return CType(params(4).Value, String)
        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)
            Throw New Exception("Error On DataAccess.ZipCode.MasterZipCode.MasterZipCodeDelete")
        End Try
    End Function
#End Region

End Class
