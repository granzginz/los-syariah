
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class MasterTC : Inherits Maxiloan.SQLEngine.DataAccessBase

#Region " Private Const "
    'Stored Procedure name    

    Private Const sp_List As String = "spTermConditionList"
    Private Const sp_ADD As String = "spTermConditionAdd"
    Private Const sp_BYID As String = "spTermConditionByID"
    Private Const sp_EDIT As String = "spTermConditionEdit"
    Private Const sp_DEL As String = "spTermConditionDelete"
    Private Const sp_REPORT As String = "spTermConditionReport"

    Private Const spTCCL_List As String = "spTCCheckList"
    Private Const spTCCL_ADD As String = "spTCCheckListAdd"
    Private Const spTCCL_BYID As String = "spTCCheckListByID"
    Private Const spTCCL_EDIT As String = "spTCCheckListEdit"
    Private Const spTCCL_DEL As String = "spTCCheckListDelete"
    Private Const spTCCL_REPORT As String = "spTCCheckListReport"

#End Region

#Region "MasterTC"

    Public Function ListMasterTC(ByVal customclass As Parameter.MasterTC) As Parameter.MasterTC
        Dim params() As SqlParameter = New SqlParameter(4) {}

        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customclass.CurrentPage

        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customclass.PageSize

        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(2).Value = customclass.WhereCond

        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
        params(3).Value = customclass.SortBy

        params(4) = New SqlParameter("@TotalRecords", SqlDbType.SmallInt)
        params(4).Direction = ParameterDirection.Output

        Try
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, sp_List, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)


        Catch ex As Exception

        End Try
        Return customclass
    End Function

#Region " MasterTCAdd"
    Public Sub MasterTCAdd(ByVal customClass As Parameter.MasterTC)
        Dim params(3) As SqlParameter
        params(0) = New SqlParameter("@TCID", SqlDbType.VarChar, 50)
        params(0).Value = customClass.TCID
        params(1) = New SqlParameter("@TCName", SqlDbType.VarChar, 50)
        params(1).Value = customClass.TCName
        params(2) = New SqlParameter("@IsForPersonal", SqlDbType.Int)
        params(2).Value = customClass.IsForPersonal
        params(3) = New SqlParameter("@IsForCompany", SqlDbType.Int)
        params(3).Value = customClass.IsForCompany
        Try
            Dim oReturnValue As New Parameter.MasterTC
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, sp_ADD, params)
        Catch ex As Exception
            Throw New Exception("A record already exist with the same primary key!")
        End Try
    End Sub
#End Region

#Region " MasterTCEdit"
    Public Sub MasterTCEdit(ByVal customClass As Parameter.MasterTC)

        Dim params(3) As SqlParameter


        params(0) = New SqlParameter("@TCID", SqlDbType.Char, 50)
        params(0).Value = customClass.TCID
        params(1) = New SqlParameter("@TCName", SqlDbType.VarChar, 100)
        params(1).Value = customClass.TCName
        params(2) = New SqlParameter("@IsForPersonal", SqlDbType.Int)
        params(2).Value = customClass.IsForPersonal
        params(3) = New SqlParameter("@IsForCompany", SqlDbType.Int)
        params(3).Value = customClass.IsForCompany

        Try
            Dim oReturnValue As New Parameter.MasterTC
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, sp_EDIT, params)

        Catch ex As Exception
            Throw New Exception("INFO: Unable to edit the selected record")

        End Try

    End Sub
#End Region

#Region "MasterTCDelete"
    Public Sub MasterTCDelete(ByVal oMasterTC As Parameter.MasterTC)

        Dim params(0) As SqlParameter

        With oMasterTC

            params(0) = New SqlParameter("@TCID", SqlDbType.VarChar, 50)
            params(0).Value = .TCID

        End With

        Try
            SqlHelper.ExecuteNonQuery(oMasterTC.strConnection, CommandType.StoredProcedure, sp_DEL, params)
        Catch ex As Exception
            Throw New Exception("INFO: Unable to delete the selected record. Record already used !")
        End Try
    End Sub
#End Region

#Region "MasterTCReport"
    Public Function MasterTCReport(ByVal customclass As Parameter.MasterTC) As Parameter.MasterTC

        Dim dt As New DataTable
        Dim oMasterTC As New Parameter.MasterTC

        Try
            oMasterTC.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, sp_REPORT).Tables(0)

        Catch ex As Exception

        End Try
        Return oMasterTC

    End Function
#End Region

#Region "MasterTCByID"
    Public Function ListMasterTCByID(ByVal customclass As Parameter.MasterTC) As Parameter.MasterTC

        Dim params() As SqlParameter = New SqlParameter(0) {}

        params(0) = New SqlParameter("@TCID", SqlDbType.Char, 50)
        params(0).Value = customclass.TCID

        Try
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, sp_BYID, params).Tables(0)

        Catch ex As Exception

        End Try

        Return customclass
    End Function
#End Region

#End Region


#Region "TCCL"

    Public Function ListTCCL(ByVal customclass As Parameter.MasterTC) As Parameter.MasterTC
        Dim params() As SqlParameter = New SqlParameter(5) {}


        params(0) = New SqlParameter("@TCID", SqlDbType.Char, 50)
        params(0).Value = customclass.TCID

        params(1) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(1).Value = customclass.CurrentPage

        params(2) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(2).Value = customclass.PageSize

        params(3) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(3).Value = customclass.WhereCond

        params(4) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
        params(4).Value = customclass.SortBy

        params(5) = New SqlParameter("@TotalRecords", SqlDbType.SmallInt)
        params(5).Direction = ParameterDirection.Output

        Try
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spTCCL_List, params).Tables(0)
            customclass.TotalRecord = CInt(params(5).Value)
        Catch ex As Exception

        End Try
        Return customclass
    End Function

#Region " TCCLAdd"
    Public Sub TCCLAdd(ByVal customClass As Parameter.MasterTC)

        Dim params(2) As SqlParameter


        params(0) = New SqlParameter("@TCID", SqlDbType.VarChar, 50)
        params(0).Value = customClass.TCID
        params(1) = New SqlParameter("@SequenceNo", SqlDbType.SmallInt)
        params(1).Value = customClass.SequenceNo
        params(2) = New SqlParameter("@Description", SqlDbType.VarChar, 100)
        params(2).Value = customClass.Description
        Try
            Dim oReturnValue As New Parameter.MasterTC
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, spTCCL_ADD, params)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)
            Throw New Exception("A record already exist with the same primary key!")

        End Try

    End Sub
#End Region

#Region " TCCLEdit"
    Public Sub TCCLEdit(ByVal customClass As Parameter.MasterTC)

        Dim params(2) As SqlParameter


        params(0) = New SqlParameter("@TCID", SqlDbType.VarChar, 50)
        params(0).Value = customClass.TCID
        params(1) = New SqlParameter("@SequenceNo", SqlDbType.SmallInt)
        params(1).Value = customClass.SequenceNo
        params(2) = New SqlParameter("@Description", SqlDbType.VarChar, 100)
        params(2).Value = customClass.Description
        Try
            Dim oReturnValue As New Parameter.MasterTC
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, spTCCL_EDIT, params)

        Catch ex As Exception
            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)
            Throw New Exception("INFO: Unable to edit the selected record")

        End Try

    End Sub
#End Region

#Region "TCCLDelete"
    Public Sub TCCLDelete(ByVal oTCCL As Parameter.MasterTC)

        Dim params(2) As SqlParameter

        With oTCCL

            params(0) = New SqlParameter("@TCID", SqlDbType.VarChar, 50)
            params(0).Value = .TCID
            params(1) = New SqlParameter("@Description", SqlDbType.VarChar, 100)
            params(1).Value = .Description
            params(2) = New SqlParameter("@SequenceNO", SqlDbType.SmallInt)
            params(2).Value = .SequenceNo

        End With

        Try
            SqlHelper.ExecuteNonQuery(oTCCL.strConnection, CommandType.StoredProcedure, spTCCL_DEL, params)
        Catch ex As Exception
            Throw New Exception("INFO: Unable to delete the selected record. Record already used !")
        End Try
    End Sub
#End Region

#Region "TCCLReport"
    Public Function TCCLReport(ByVal customclass As Parameter.MasterTC) As Parameter.MasterTC

        Dim dt As New DataTable
        Dim oTCCL As New Parameter.MasterTC

        Dim params(0) As SqlParameter

        With oTCCL
            params(0) = New SqlParameter("@TCID", SqlDbType.VarChar, 50)
            params(0).Value = customclass.TCID
        End With

        Try
            oTCCL.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spTCCL_REPORT, params).Tables(0)

        Catch ex As Exception

        End Try
        Return oTCCL

    End Function
#End Region

#Region "TCCLByID"
    Public Function ListTCCLByID(ByVal customclass As Parameter.MasterTC) As Parameter.MasterTC

        Dim params() As SqlParameter = New SqlParameter(1) {}

        params(0) = New SqlParameter("@TCID", SqlDbType.Char, 50)
        params(0).Value = customclass.TCID
        params(1) = New SqlParameter("@Description", SqlDbType.VarChar, 100)
        params(1).Value = customclass.Description

        Try
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spTCCL_BYID, params).Tables(0)

        Catch ex As Exception

        End Try

        Return customclass
    End Function
#End Region

#End Region

End Class
