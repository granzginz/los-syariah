


#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class GeneralSetting : Inherits Maxiloan.SQLEngine.DataAccessBase

#Region " Private Const "
    Private LIST_UPDATE_GENERAL_SETTING As String = "spGeneralSettingSaveEdit"
    Private LIST_SELECT_GENERALSETTING As String = "spPagingGeneralSetting"
    Private LIST_REPORT_GENERAL_SETTING As String = "SpGeneralSettingReport"
    Private LIST_SELECT_GENERALSETTING_BY_ID As String = "spGeneralSettingByID"
#End Region

#Region "GetGeneralSetting"

    Public Function GetGeneralSetting(ByVal oCustomClass As Parameter.GeneralSetting) As Parameter.GeneralSetting
        Dim oReturnValue As New Parameter.GeneralSetting
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT_GENERALSETTING, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.GeneralSetting")

        End Try
    End Function
#End Region

#Region "GeneralSettingSaveEdit"

    Public Function GeneralSettingSaveEdit(ByVal customClass As Parameter.GeneralSetting) As Boolean
        Dim params(2) As SqlParameter
        params(0) = New SqlParameter("@GSID", SqlDbType.VarChar, 50)
        params(0).Value = customClass.GSID
        params(1) = New SqlParameter("@GSName", SqlDbType.VarChar, 100)
        params(1).Value = customClass.GsName
        params(2) = New SqlParameter("@GSValue", SqlDbType.VarChar, 3000)
        params(2).Value = customClass.GsValue
        Try
            Dim oReturnValue As New Parameter.GeneralSetting
            SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, LIST_UPDATE_GENERAL_SETTING, params)
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.GeneralSetting")

        End Try
    End Function
#End Region

#Region "GetGeneralSettingReport"
    Public Function GetGeneralSettingReport(ByVal customClass As Parameter.GeneralSetting) As Parameter.GeneralSetting
        Dim oReturnValue As New Parameter.GeneralSetting
        Dim spName As String
        spName = LIST_REPORT_GENERAL_SETTING

        Dim params(1) As SqlParameter
        params(0) = New SqlParameter("@WhereBy", SqlDbType.VarChar, 1000)
        params(0).Value = customClass.WhereCond
        params(1) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
        params(1).Value = customClass.SortBy
        Try
            oReturnValue.ListDataReport = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, spName, params)
            Return oReturnValue
        Catch ex As Exception
            '        Throw New Exception("Error On DataAccess.Insurance.PremiumToCustomer.GetPremiumToCustomerReport")
        End Try
    End Function
#End Region

    Public Function GetGeneralSettingByID(ByVal oCustomClass As Parameter.GeneralSetting) As Parameter.GeneralSetting
        Dim oReturnValue As New Parameter.GeneralSetting
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@GSID", SqlDbType.VarChar, 50)
        params(0).Value = oCustomClass.GSID
        
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT_GENERALSETTING_BY_ID, params).Tables(0)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function getGeneralSettingValue(oClass As Parameter.GeneralSetting) As String
        Dim params(1) As SqlParameter

        params(0) = New SqlParameter("@GSID", SqlDbType.VarChar, 50)
        params(0).Value = oClass.GSID
        params(1) = New SqlParameter("@ModuleID", SqlDbType.Char, 3)
        params(1).Value = oClass.ModuleID

        Try
            Return SqlHelper.ExecuteScalar(oClass.strConnection, CommandType.Text, "SELECT dbo.fnGetGeneralSettingValue(@GSID,@ModuleID)", params).ToString
        Catch ex As Exception
            Throw New Exception(ex.Message)
            Return ""
        End Try

    End Function
End Class
