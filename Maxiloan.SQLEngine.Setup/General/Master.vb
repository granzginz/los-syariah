
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class Master : Inherits Maxiloan.SQLEngine.DataAccessBase
#Region " Private Const "
    'Stored Procedure name    
    Private Const LIST_SELECT As String = "spPagingMaster"
    'Private Const LIST_EDIT As String = "spMasterEdit"
    Private Const LIST_ADD As String = "spMasterSaveAdd"
    Private Const LIST_UPDATE As String = "spMasterSaveEdit"
    Private Const LIST_DELETE As String = "spMasterDelete"
    Private Const LIST_REPORT As String = "spMasterReport"
    Private Const LIST_KEYWORD As String = "spMasterKeyWord"
#End Region


    Public Function GetMaster(ByVal oMaster As Parameter.Master) As Parameter.Master
        Dim oReturnValue As New Parameter.Master
        Dim params(5) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oMaster.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oMaster.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oMaster.WhereCond
        params(3) = New SqlParameter("@Table", SqlDbType.VarChar)
        params(3).Value = oMaster.Table
        params(4) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(4).Value = oMaster.SortBy
        params(5) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(5).Direction = ParameterDirection.Output
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oMaster.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(5).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.Master.GetMaster")

        End Try
    End Function

    Public Function GetMasterReport(ByVal oMaster As Parameter.Master) As Parameter.Master
        Dim oReturnValue As New Parameter.Master
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@Table", SqlDbType.VarChar)
        params(0).Value = oMaster.Table
        Try
            oReturnValue.listdataReport = SqlHelper.ExecuteDataset(oMaster.strConnection, CommandType.StoredProcedure, LIST_REPORT, params)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.Master.GetMasterReport")

        End Try
    End Function

    Public Function GetKeyWord(ByVal oMaster As Parameter.Master) As Parameter.Master
        Dim oReturnValue As New Parameter.Master
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oMaster.strConnection, CommandType.StoredProcedure, LIST_KEYWORD).Tables(0)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.Master.GetKeyWord")
        End Try
    End Function
    Public Function MasterSaveAdd(ByVal oMaster As Parameter.Master) As String
        Dim ErrMessage As String = ""
        Dim params(3) As SqlParameter
        params(0) = New SqlParameter("@Id", SqlDbType.VarChar, 10)
        params(0).Value = oMaster.Id
        params(1) = New SqlParameter("@Description", SqlDbType.VarChar, 50)
        params(1).Value = oMaster.Description
        params(2) = New SqlParameter("@Err", SqlDbType.VarChar, 50)
        params(2).Direction = ParameterDirection.Output
        params(3) = New SqlParameter("@Table", SqlDbType.VarChar)
        params(3).Value = oMaster.Table
        Try
            SqlHelper.ExecuteNonQuery(oMaster.strConnection, CommandType.StoredProcedure, LIST_ADD, params)
            ErrMessage = CType(params(2).Value, String)
            If ErrMessage <> "" Then
                Return ErrMessage
            End If
            Return ""
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.Master.MasterSaveAdd")

        End Try
    End Function

    Public Sub MasterSaveEdit(ByVal oMaster As Parameter.Master)
        Dim params(2) As SqlParameter
        params(0) = New SqlParameter("@Id", SqlDbType.VarChar, 10)
        params(0).Value = oMaster.Id
        params(1) = New SqlParameter("@Description", SqlDbType.VarChar, 50)
        params(1).Value = oMaster.Description
        params(2) = New SqlParameter("@Table", SqlDbType.VarChar)
        params(2).Value = oMaster.Table
        Try
            SqlHelper.ExecuteNonQuery(oMaster.strConnection, CommandType.StoredProcedure, LIST_UPDATE, params)
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.Master.MasterSaveEdit")

        End Try
    End Sub

    Public Function MasterDelete(ByVal oMaster As Parameter.Master) As String
        Dim params(1) As SqlParameter
        params(0) = New SqlParameter("@Id", SqlDbType.Char, 10)
        params(0).Value = oMaster.Id
        params(1) = New SqlParameter("@Table", SqlDbType.VarChar, 50)
        params(1).Value = oMaster.Table
        Try
            SqlHelper.ExecuteNonQuery(oMaster.strConnection, CommandType.StoredProcedure, LIST_DELETE, params)
            Return ""
        Catch ex As Exception
            Return "ERROR: Unable to delete the selected record. Record already used!"
            '    Throw New Exception("Error On DataAccess.Setting.Master.MasterDelete")

        End Try
    End Function
End Class
