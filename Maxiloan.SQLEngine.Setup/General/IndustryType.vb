
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class IndustryType : Inherits Maxiloan.SQLEngine.DataAccessBase
#Region " Private Const "
    'Stored Procedure name    
    Private Const LIST_SELECT As String = "spPagingIndustryType"
    Private Const LIST_BY_ID As String = "spIndustryTypeEdit"
    Private Const LIST_ADD As String = "spIndustryTypeSaveAdd"
    Private Const LIST_UPDATE As String = "spIndustryTypeSaveEdit"
    Private Const LIST_DELETE As String = "spIndustryTypeDelete"
    Private Const LIST_REPORT As String = "spIndustryTypeReport"
    Private Const LIST_SECTOR As String = "spIndustryTypeSector"
#End Region

    Public Function GetIndustryType(ByVal oCustomClass As Parameter.IndustryType) As Parameter.IndustryType
        Dim oReturnValue As New Parameter.IndustryType
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.IndustryType.GetIndustryType")
        End Try
    End Function

    Public Function GetIndustryTypeReport(ByVal oCustomClass As Parameter.IndustryType) As Parameter.IndustryType
        Dim oReturnValue As New Parameter.IndustryType
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@CmdWhere", SqlDbType.VarChar, 1000)
        params(0).Value = oCustomClass.WhereCond
        Try
            oReturnValue.listdataReport = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_REPORT, params)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.IndustryType.GetIndustryTypeReport")
        End Try
    End Function

    Public Function GetSector(ByVal oCustomClass As Parameter.IndustryType) As Parameter.IndustryType
        Dim oReturnValue As New Parameter.IndustryType
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SECTOR).Tables(0)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.IndustryType.GetSector")
        End Try
    End Function

    Public Function GetIndustryTypeEdit(ByVal ocustomClass As Parameter.IndustryType) As Parameter.IndustryType
        Dim reader As SqlDataReader
        Dim params(0) As SqlParameter
        Dim oReturnValue As New Parameter.IndustryType
        params(0) = New SqlParameter("@Id", SqlDbType.Char, 10)
        params(0).Value = ocustomClass.Id
        Try
            reader = SqlHelper.ExecuteReader(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_BY_ID, params)
            If reader.Read Then
                oReturnValue.Sector = reader("description").ToString
                oReturnValue.Omset = reader("PercentOmset").ToString
            End If
            reader.Close()
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.IndustryType.GetIndustryTypeEdit")
        End Try
    End Function

    Public Function IndustryTypeSaveAdd(ByVal ocustomClass As Parameter.IndustryType) As String
        Dim ErrMessage As String = ""
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@Id", SqlDbType.Char, 10)
        params(0).Value = ocustomClass.Id
        params(1) = New SqlParameter("@Description", SqlDbType.VarChar, 100)
        params(1).Value = ocustomClass.Description
        params(2) = New SqlParameter("@Sector", SqlDbType.Char, 10)
        params(2).Value = ocustomClass.Sector
        params(3) = New SqlParameter("@Omset", SqlDbType.Decimal)
        params(3).Value = ocustomClass.Omset
        params(4) = New SqlParameter("@Err", SqlDbType.VarChar, 50)
        params(4).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_ADD, params)
            ErrMessage = CType(params(4).Value, String)
            If ErrMessage <> "" Then
                Return ErrMessage
            End If
            Return ""
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.IndustryType.IndustryTypeSaveAdd")
        End Try
    End Function

    Public Sub IndustryTypeSaveEdit(ByVal ocustomClass As Parameter.IndustryType)
        Dim params(3) As SqlParameter
        params(0) = New SqlParameter("@Id", SqlDbType.Char, 10)
        params(0).Value = ocustomClass.Id
        params(1) = New SqlParameter("@Description", SqlDbType.VarChar, 100)
        params(1).Value = ocustomClass.Description
        params(2) = New SqlParameter("@Sector", SqlDbType.Char, 10)
        params(2).Value = ocustomClass.Sector
        params(3) = New SqlParameter("@Omset", SqlDbType.Decimal)
        params(3).Value = ocustomClass.Omset
        Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_UPDATE, params)
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.IndustryType.IndustryTypeSaveEdit")
        End Try
    End Sub

    Public Function IndustryTypeDelete(ByVal ocustomClass As Parameter.IndustryType) As String
        Dim Err As Integer
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@Id", SqlDbType.Char, 10)
        params(0).Value = ocustomClass.Id
        Try
            Err = SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_DELETE, params)
            Return ""
        Catch ex As Exception
            Return "ERROR: Unable to delete the selected record. Record already used!"
            'Throw New Exception("Error On DataAccess.Setting.IndustryType.IndustryTypeDelete")
        End Try
    End Function
End Class
