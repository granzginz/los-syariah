
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region
Public Class Reason
    Private Const SPREASONADD As String = "spReasonAdd"
    Private Const SPREASONEDIT As String = "spReasonEdit"
    Private Const SPREASONDELETE As String = "spReasonDelete"
    Private Const SPREASONVIEW As String = "spReasonView"
    Private Const SPREASONTYPELIST As String = "spReasonTypeList"
    Private Const PARAM_REASONID As String = "@ReasonID"
    Private Const PARAM_REASONTYPEID As String = "@ReasonTypeID"
    Private Const PARAM_DESCRIPTION As String = "@Description"
    Private Const PARAM_ISSYSTEM As String = "@IsSystem"

    Public Sub ReasonAdd(ByVal customclass As Parameter.Reason)
        Dim objcon As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction

        Dim params() As SqlParameter = New SqlParameter(2) {}

        params(0) = New SqlParameter(PARAM_REASONTYPEID, SqlDbType.VarChar, 5)
        params(0).Value = customclass.ReasonTypeID
        params(1) = New SqlParameter(PARAM_REASONID, SqlDbType.VarChar, 10)
        params(1).Value = customclass.ReasonID
        params(2) = New SqlParameter(PARAM_DESCRIPTION, SqlDbType.VarChar, 25)
        params(2).Value = customclass.ReasonDescription

        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction
            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, SPREASONADD, params)
            objtrans.Commit()
        Catch ex As Exception
            objtrans.Rollback()
            Throw New Exception(ex.Message)
        Finally
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
        End Try
    End Sub

    Public Sub ReasonDelete(ByVal customclass As Parameter.Reason)
        Dim objcon As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction

        Dim params() As SqlParameter = New SqlParameter(1) {}
        params(0) = New SqlParameter(PARAM_REASONTYPEID, SqlDbType.VarChar, 5)
        params(0).Value = customclass.ReasonTypeID
        params(1) = New SqlParameter(PARAM_REASONID, SqlDbType.VarChar, 10)
        params(1).Value = customclass.ReasonID
        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction
            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, SPREASONDELETE, params)
            objtrans.Commit()
        Catch ex As Exception
            objtrans.Rollback()
            Throw New Exception(ex.Message)
        Finally
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
        End Try
    End Sub

    Public Sub ReasonEdit(ByVal customclass As Parameter.Reason)
        Dim params() As SqlParameter = New SqlParameter(2) {}
        Dim objcon As New SqlConnection(customclass.strConnection)
        Dim objtrans As SqlTransaction

        params(0) = New SqlParameter(PARAM_REASONTYPEID, SqlDbType.VarChar, 5)
        params(0).Value = customclass.ReasonTypeID
        params(1) = New SqlParameter(PARAM_REASONID, SqlDbType.VarChar, 10)
        params(1).Value = customclass.ReasonID
        params(2) = New SqlParameter(PARAM_DESCRIPTION, SqlDbType.VarChar, 25)
        params(2).Value = customclass.ReasonDescription

        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction
            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, SPREASONEDIT, params)
            objtrans.Commit()
        Catch ex As Exception
            objtrans.Rollback()
            Throw New Exception(ex.Message)
        Finally
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
        End Try
    End Sub

    Public Function ReasonView(ByVal customclass As Parameter.Reason) As Parameter.Reason
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Dim objread As SqlDataReader
        params(0) = New SqlParameter(PARAM_REASONTYPEID, SqlDbType.VarChar, 5)
        params(0).Value = customclass.ReasonTypeID
        params(1) = New SqlParameter(PARAM_REASONID, SqlDbType.VarChar, 10)
        params(1).Value = customclass.ReasonID
        Try
            objread = SqlHelper.ExecuteReader(customclass.strConnection, CommandType.StoredProcedure, SPREASONVIEW, params)
            If objread.Read Then
                With customclass
                    .ReasonID = CStr(objread("ReasonID"))
                    .ReasonTypeID = CStr(objread("ReasonTypeID"))
                    .ReasonDescription = CStr(objread("Description"))
                    .IsSystem = CBool(objread("IsSystem"))
                End With
            End If
            objread.Close()
            Return customclass
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function GetReasonTypeCombo(ByVal customclass As Parameter.Reason) As DataTable
        Return SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, SPREASONTYPELIST).Tables(0)
    End Function

End Class
