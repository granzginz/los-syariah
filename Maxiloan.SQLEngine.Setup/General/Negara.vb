
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class Negara : Inherits Maxiloan.SQLEngine.DataAccessBase
#Region " Private Const "
    'Stored Procedure name    
    Private Const LIST_SELECT As String = "spPagingNegara"
    Private Const LIST_SELECT_ALL As String = "spNegaraListAll"
    Private Const LIST_BY_ID As String = "spNegaraList"
    Private Const LIST_ADD As String = "spNegaraSaveAdd"
    Private Const LIST_UPDATE As String = "spNegaraSaveEdit"
    Private Const LIST_DELETE As String = "spNegaraDelete"
    Private Const LIST_REPORT As String = "spNegaraReport"
#End Region

    Public Function GetNegara(ByVal oCustomClass As Parameter.Negara) As Parameter.Negara
        Dim oReturnValue As New Parameter.Negara
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.Listdata = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, LIST_SELECT, params).Tables(0)
            oReturnValue.Totalrecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.Negara.GetNegara")
        End Try
    End Function

    Public Function GetNegaraList(ByVal ocustomClass As Parameter.Negara) As Parameter.Negara
        Dim reader As SqlDataReader
        Dim params(0) As SqlParameter
        Dim oReturnValue As New Parameter.Negara
        params(0) = New SqlParameter("@Description", SqlDbType.VarChar, 50)
        params(0).Value = ocustomClass.Description
        Try
            reader = SqlHelper.ExecuteReader(ocustomClass.strConnection, CommandType.StoredProcedure, LIST_BY_ID, params)
            If reader.Read Then
                oReturnValue.Description = reader("Description").ToString
            End If
            reader.Close()
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.Setting.Negara.GetNegaraList")
        End Try
    End Function

    Public Function GetNegaraCombo(ByVal customclass As Parameter.Negara) As DataTable
        Return SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, LIST_SELECT_ALL).Tables(0)
    End Function
End Class
