

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class Company : Inherits DataAccessBase
    Private Const spCOMPANY_LIST As String = "spCOMPANYLIST"
    Private Const spCOMPANY_BYID As String = "spCompanyByID"
    Private Const spCOMPANY_ADD As String = "spCOMPANYAdd"
    Private Const spCOMPANY_EDIT As String = "spCOMPANYEdit"
    Private Const spCOMPANY_DEL As String = "spCOMPANYDelete"
    Private Const spCOMPANY_Report As String = "spCOMPANYReport"

    Private Const spCOMPANY_BOD_LIST As String = "spCOMPANYBODLIST"
    Private Const spCOMPANY_BOD_BYID As String = "spCompanyBODByID"
    Private Const spCOMPANY_BOD_ADD As String = "spCOMPANYBODAdd"
    Private Const spCOMPANY_BOD_EDIT As String = "spCOMPANYBODEdit"
    Private Const spCOMPANY_BOD_DEL As String = "spCOMPANYBODDelete"
    Private Const spCOMPANY_BOD_Report As String = "spCOMPANYBODReport"

    Private Const spCOMPANY_Commisioner_LIST As String = "spCOMPANYCommisionerLIST"
    Private Const spCOMPANY_Commisioner_BYID As String = "spCompanyCommisionerByID"
    Private Const spCOMPANY_Commisioner_ADD As String = "spCOMPANYCommisionerAdd"
    Private Const spCOMPANY_Commisioner_EDIT As String = "spCOMPANYCommisionerEdit"
    Private Const spCOMPANY_Commisioner_DEL As String = "spCOMPANYCommisionerDelete"
    Private Const spCOMPANY_Commisioner_Report As String = "spCOMPANYCommisionerReport"

    Private Const spGETCOMBO_INPUTTYPE As String = "select id, description as name from tblIDType"


#Region "COMPANY"

#Region "CompanyList"
    Public Function ListCompany(ByVal customclass As Parameter.Company) As Parameter.Company

        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter("@TotalRecords", SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spCOMPANY_LIST, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            WriteException("AgreementTransfer", "AssetDocPaging", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region "CompanybyID"
    Public Function ListCompanyByID(ByVal customclass As Parameter.Company) As Parameter.Company
        Dim params() As SqlParameter = New SqlParameter(1) {}
        params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(0).Value = customclass.WhereCond

        params(1) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
        params(1).Value = customclass.SortBy
        Try
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spCOMPANY_BYID, params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("Company", "ListCompanyByID", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region " CompanyAdd"
    Public Sub CompanyAdd(ByVal oCompany As Parameter.Company, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal)

        Dim params(24) As SqlParameter
        Dim objcon As New SqlConnection(oCompany.strConnection)
        Dim objtrans As SqlTransaction = Nothing
        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction
            params(0) = New SqlParameter("@CompanyID", SqlDbType.Char, 3)
            params(0).Value = oCompany.CompanyID
            params(1) = New SqlParameter("@CompanyFullName", SqlDbType.VarChar, 50)
            params(1).Value = oCompany.CompanyFullName
            params(2) = New SqlParameter("@CompanyShortName", SqlDbType.Char, 20)
            params(2).Value = oCompany.CompanyShortName
            params(3) = New SqlParameter("@CompanyInitialName", SqlDbType.Char, 5)
            params(3).Value = oCompany.CompanyInitialName


            params(4) = New SqlParameter("@CompanyAddress", SqlDbType.VarChar, 100)
            params(4).Value = oClassAddress.Address

            params(5) = New SqlParameter("@CompanyRT", SqlDbType.Char, 3)
            params(5).Value = oClassAddress.RT
            params(6) = New SqlParameter("@CompanyRW", SqlDbType.Char, 3)
            params(6).Value = oClassAddress.RW

            params(7) = New SqlParameter("@CompanyKelurahan", SqlDbType.VarChar, 30)
            params(7).Value = oClassAddress.Kelurahan
            params(8) = New SqlParameter("@CompanyKecamatan", SqlDbType.VarChar, 30)
            params(8).Value = oClassAddress.Kecamatan
            params(9) = New SqlParameter("@CompanyCity", SqlDbType.VarChar, 30)
            params(9).Value = oClassAddress.City
            params(10) = New SqlParameter("@CompanyZipCode", SqlDbType.Char, 5)
            params(10).Value = oClassAddress.ZipCode
            params(11) = New SqlParameter("@CompanyAreaPhone1", SqlDbType.Char, 4)
            params(11).Value = oClassAddress.AreaPhone1
            params(12) = New SqlParameter("@CompanyPhone1", SqlDbType.Char, 15)
            params(12).Value = oClassAddress.Phone1
            params(13) = New SqlParameter("@CompanyAreaPhone2", SqlDbType.Char, 4)
            params(13).Value = oClassAddress.AreaPhone2
            params(14) = New SqlParameter("@CompanyPhone2", SqlDbType.Char, 15)
            params(14).Value = oClassAddress.Phone2

            params(15) = New SqlParameter("@CompanyAreaFax", SqlDbType.VarChar, 4)
            params(15).Value = oClassAddress.AreaFax
            params(16) = New SqlParameter("@CompanyFax", SqlDbType.VarChar, 10)
            params(16).Value = oClassAddress.Fax

            params(17) = New SqlParameter("@ContactPersonName", SqlDbType.VarChar, 50)
            params(17).Value = oClassPersonal.PersonName
            params(18) = New SqlParameter("@ContactPersonTitle", SqlDbType.VarChar, 50)
            params(18).Value = oClassPersonal.PersonTitle
            params(19) = New SqlParameter("@ContactEmail", SqlDbType.VarChar, 30)
            params(19).Value = oClassPersonal.Email
            params(20) = New SqlParameter("@ContactHP", SqlDbType.VarChar, 20)
            params(20).Value = oClassPersonal.MobilePhone

            params(21) = New SqlParameter("@NPWP", SqlDbType.VarChar, 30)
            params(21).Value = oCompany.NPWP
            params(22) = New SqlParameter("@TDP", SqlDbType.VarChar, 30)
            params(22).Value = oCompany.TDP
            params(23) = New SqlParameter("@SIUP", SqlDbType.VarChar, 30)
            params(23).Value = oCompany.SIUP


            params(24) = New SqlParameter("@AppMgr", SqlDbType.VarChar, 50)
            params(24).Value = getReferenceDBName()



            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spCOMPANY_ADD, params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("Company", "CompanyAdd", exp.Message + exp.StackTrace)
            Throw New Exception("A record already exists with the same primary key !")
        Finally
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
        End Try
    End Sub
#End Region

#Region " CompanyEdit"
    Public Sub CompanyEdit(ByVal oCompany As Parameter.Company, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal)
        Dim params(24) As SqlParameter
        Dim objcon As New SqlConnection(oCompany.strConnection)
        Dim objtrans As SqlTransaction = Nothing
        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction
            params(0) = New SqlParameter("@CompanyID", SqlDbType.Char, 3)
            params(0).Value = oCompany.CompanyID
            params(1) = New SqlParameter("@CompanyFullName", SqlDbType.VarChar, 50)
            params(1).Value = oCompany.CompanyFullName
            params(2) = New SqlParameter("@CompanyShortName", SqlDbType.Char, 20)
            params(2).Value = oCompany.CompanyShortName
            params(3) = New SqlParameter("@CompanyInitialName", SqlDbType.Char, 5)
            params(3).Value = oCompany.CompanyInitialName


            params(4) = New SqlParameter("@CompanyAddress", SqlDbType.VarChar, 100)
            params(4).Value = oClassAddress.Address

            params(5) = New SqlParameter("@CompanyRT", SqlDbType.Char, 3)
            params(5).Value = oClassAddress.RT
            params(6) = New SqlParameter("@CompanyRW", SqlDbType.Char, 3)
            params(6).Value = oClassAddress.RW

            params(7) = New SqlParameter("@CompanyKelurahan", SqlDbType.VarChar, 30)
            params(7).Value = oClassAddress.Kelurahan
            params(8) = New SqlParameter("@CompanyKecamatan", SqlDbType.VarChar, 30)
            params(8).Value = oClassAddress.Kecamatan
            params(9) = New SqlParameter("@CompanyCity", SqlDbType.VarChar, 30)
            params(9).Value = oClassAddress.City
            params(10) = New SqlParameter("@CompanyZipCode", SqlDbType.Char, 5)
            params(10).Value = oClassAddress.ZipCode
            params(11) = New SqlParameter("@CompanyAreaPhone1", SqlDbType.Char, 4)
            params(11).Value = oClassAddress.AreaPhone1
            params(12) = New SqlParameter("@CompanyPhone1", SqlDbType.Char, 15)
            params(12).Value = oClassAddress.Phone1
            params(13) = New SqlParameter("@CompanyAreaPhone2", SqlDbType.Char, 4)
            params(13).Value = oClassAddress.AreaPhone2
            params(14) = New SqlParameter("@CompanyPhone2", SqlDbType.Char, 15)
            params(14).Value = oClassAddress.Phone2

            params(15) = New SqlParameter("@CompanyAreaFax", SqlDbType.VarChar, 4)
            params(15).Value = oClassAddress.AreaFax
            params(16) = New SqlParameter("@CompanyFax", SqlDbType.VarChar, 10)
            params(16).Value = oClassAddress.Fax

            params(17) = New SqlParameter("@ContactPersonName", SqlDbType.VarChar, 50)
            params(17).Value = oClassPersonal.PersonName
            params(18) = New SqlParameter("@ContactPersonTitle", SqlDbType.VarChar, 50)
            params(18).Value = oClassPersonal.PersonTitle
            params(19) = New SqlParameter("@ContactEmail", SqlDbType.VarChar, 30)
            params(19).Value = oClassPersonal.Email
            params(20) = New SqlParameter("@ContactHP", SqlDbType.VarChar, 20)
            params(20).Value = oClassPersonal.MobilePhone

            params(21) = New SqlParameter("@NPWP", SqlDbType.VarChar, 30)
            params(21).Value = oCompany.NPWP
            params(22) = New SqlParameter("@TDP", SqlDbType.VarChar, 30)
            params(22).Value = oCompany.TDP
            params(23) = New SqlParameter("@SIUP", SqlDbType.VarChar, 30)
            params(23).Value = oCompany.SIUP

            params(24) = New SqlParameter("@AppMgr", SqlDbType.VarChar, 50)
            params(24).Value = getReferenceDBName()


            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spCOMPANY_EDIT, params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("Company", "CompanyEdit", exp.Message + exp.StackTrace)
            Throw New Exception("A record already exists with the same primary key !")
        Finally
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
        End Try
    End Sub
#End Region

#Region "CompanyDelete"
    Public Sub CompanyDelete(ByVal oCompany As Parameter.Company)

        Dim params(1) As SqlParameter
        Dim objcon As New SqlConnection(oCompany.strConnection)
        Dim objtrans As SqlTransaction = Nothing

        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction
            With oCompany
                params(0) = New SqlParameter("@CompanyID", SqlDbType.Char, 3)
                params(0).Value = .CompanyID
                params(1) = New SqlParameter("@AppMgr", SqlDbType.VarChar, 50)
                params(1).Value = getReferenceDBName()
            End With
            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spCOMPANY_DEL, params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("Company", "CompanyDelete", exp.Message + exp.StackTrace)
            Throw New Exception("INFO: Unable to delete the selected record. Record already used !")
        Finally
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
        End Try
    End Sub
#End Region

#Region "CompanyList"
    Public Function ListCompanyReport(ByVal customclass As Parameter.Company) As Parameter.Company
        Try
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spCOMPANY_Report).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("Company", "ListCompanyReport", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#End Region

#Region "COMPANY BOD"


    Public Function GetComboInputType(ByVal strcon As String) As DataTable
        Dim customClass As New Parameter.CompanyBOD
        Try
            customClass.ListData = SqlHelper.ExecuteDataset(strcon, CommandType.Text, spGETCOMBO_INPUTTYPE).Tables(0)
            Return customClass.ListData
        Catch exp As Exception
            WriteException("Company", "GetComboInputType", exp.Message + exp.StackTrace)
        End Try
    End Function

#Region "CompanyBODList"

    Public Function ListCompanyBOD(ByVal customclass As Parameter.CompanyBOD) As Parameter.CompanyBOD
        Dim params() As SqlParameter = New SqlParameter(5) {}

        params(0) = New SqlParameter("@CompanyId", SqlDbType.Char, 3)
        params(0).Value = customclass.CompanyID

        params(1) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(1).Value = customclass.CurrentPage

        params(2) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(2).Value = customclass.PageSize

        params(3) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
        params(3).Value = customclass.WhereCond

        params(4) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
        params(4).Value = customclass.SortBy

        params(5) = New SqlParameter("@TotalRecords", SqlDbType.SmallInt)
        params(5).Direction = ParameterDirection.Output

        Try
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spCOMPANY_BOD_LIST, params).Tables(0)
            customclass.TotalRecord = CInt(params(5).Value)
            Return customclass
        Catch exp As Exception
            WriteException("Company", "ListCompanyBOD", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function ListCompanyBODByID(ByVal customclass As Parameter.CompanyBOD) As Parameter.CompanyBOD
        Dim params() As SqlParameter = New SqlParameter(1) {}

        params(0) = New SqlParameter("@CompanyId", SqlDbType.Char, 3)
        params(0).Value = customclass.CompanyID

        params(1) = New SqlParameter("@BODID", SqlDbType.Char, 10)
        params(1).Value = customclass.BODID

        Try
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spCOMPANY_BOD_BYID, params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("Company", "ListCompanyBOD", exp.Message + exp.StackTrace)
        End Try

    End Function

#End Region

#Region " CompanyBODAdd"

    Public Sub CompanyBODAdd(ByVal oCompany As Parameter.CompanyBOD, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal)
        Dim params() As SqlParameter = New SqlParameter(21) {}
        Dim objcon As New SqlConnection(oCompany.strConnection)
        Dim objtrans As SqlTransaction = Nothing
        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction
            params(0) = New SqlParameter("@CompanyID", SqlDbType.Char, 3)
            params(0).Value = oCompany.CompanyID
            params(1) = New SqlParameter("@BODID", SqlDbType.Char, 10)
            params(1).Value = oCompany.BODID
            params(2) = New SqlParameter("@BODName", SqlDbType.VarChar, 50)
            params(2).Value = oCompany.BODName
            params(3) = New SqlParameter("@Title", SqlDbType.VarChar, 100)
            params(3).Value = oCompany.BODTitle

            params(4) = New SqlParameter("@BODAddress", SqlDbType.VarChar, 100)
            params(4).Value = oClassAddress.Address

            params(5) = New SqlParameter("@BODRT", SqlDbType.Char, 3)
            params(5).Value = oClassAddress.RT
            params(6) = New SqlParameter("@BODRW", SqlDbType.Char, 3)
            params(6).Value = oClassAddress.RW

            params(7) = New SqlParameter("@BODKelurahan", SqlDbType.VarChar, 30)
            params(7).Value = oClassAddress.Kelurahan
            params(8) = New SqlParameter("@BODKecamatan", SqlDbType.VarChar, 30)
            params(8).Value = oClassAddress.Kecamatan
            params(9) = New SqlParameter("@BODCity", SqlDbType.VarChar, 30)
            params(9).Value = oClassAddress.City
            params(10) = New SqlParameter("@BODZipCode", SqlDbType.Char, 5)
            params(10).Value = oClassAddress.ZipCode
            params(11) = New SqlParameter("@BODAreaPhone1", SqlDbType.Char, 4)
            params(11).Value = oClassAddress.AreaPhone1
            params(12) = New SqlParameter("@BODPhone1", SqlDbType.Char, 15)
            params(12).Value = oClassAddress.Phone1
            params(13) = New SqlParameter("@BODAreaPhone2", SqlDbType.Char, 4)
            params(13).Value = oClassAddress.AreaPhone2
            params(14) = New SqlParameter("@BODPhone2", SqlDbType.Char, 15)
            params(14).Value = oClassAddress.Phone2

            params(15) = New SqlParameter("@BODAreaFax", SqlDbType.VarChar, 4)
            params(15).Value = oClassAddress.AreaFax
            params(16) = New SqlParameter("@BODFax", SqlDbType.VarChar, 10)
            params(16).Value = oClassAddress.Fax


            params(17) = New SqlParameter("@BODEmail", SqlDbType.VarChar, 30)
            params(17).Value = oClassPersonal.Email
            params(18) = New SqlParameter("@BODHP", SqlDbType.VarChar, 20)
            params(18).Value = oClassPersonal.MobilePhone

            params(19) = New SqlParameter("@BODNPWP", SqlDbType.VarChar, 30)
            params(19).Value = oCompany.BODNPWP
            params(20) = New SqlParameter("@BODIDType", SqlDbType.VarChar, 10)
            params(20).Value = oCompany.BODIDType
            params(21) = New SqlParameter("@BODIDNumber", SqlDbType.VarChar, 20)
            params(21).Value = oCompany.BODIDNumber

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spCOMPANY_BOD_ADD, params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("Company", "CompanyBODAdd", exp.Message + exp.StackTrace)
            Throw New Exception("A record already exists with the same primary key !")
        Finally
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
        End Try
    End Sub
#End Region

#Region " CompanyBODEdit"
    Public Sub CompanyBODEdit(ByVal oCompany As Parameter.CompanyBOD, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal)

        Dim params() As SqlParameter = New SqlParameter(21) {}
        Dim objcon As New SqlConnection(oCompany.strConnection)
        Dim objtrans As SqlTransaction = Nothing
        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction
            params(0) = New SqlParameter("@CompanyID", SqlDbType.Char, 3)
            params(0).Value = oCompany.CompanyID
            params(1) = New SqlParameter("@BODID", SqlDbType.Char, 10)
            params(1).Value = oCompany.BODID
            params(2) = New SqlParameter("@BODName", SqlDbType.VarChar, 50)
            params(2).Value = oCompany.BODName
            params(3) = New SqlParameter("@Title", SqlDbType.VarChar, 100)
            params(3).Value = oCompany.BODTitle

            params(4) = New SqlParameter("@BODAddress", SqlDbType.VarChar, 100)
            params(4).Value = oClassAddress.Address

            params(5) = New SqlParameter("@BODRT", SqlDbType.Char, 3)
            params(5).Value = oClassAddress.RT
            params(6) = New SqlParameter("@BODRW", SqlDbType.Char, 3)
            params(6).Value = oClassAddress.RW

            params(7) = New SqlParameter("@BODKelurahan", SqlDbType.VarChar, 30)
            params(7).Value = oClassAddress.Kelurahan
            params(8) = New SqlParameter("@BODKecamatan", SqlDbType.VarChar, 30)
            params(8).Value = oClassAddress.Kecamatan
            params(9) = New SqlParameter("@BODCity", SqlDbType.VarChar, 30)
            params(9).Value = oClassAddress.City
            params(10) = New SqlParameter("@BODZipCode", SqlDbType.Char, 5)
            params(10).Value = oClassAddress.ZipCode
            params(11) = New SqlParameter("@BODAreaPhone1", SqlDbType.Char, 4)
            params(11).Value = oClassAddress.AreaPhone1
            params(12) = New SqlParameter("@BODPhone1", SqlDbType.Char, 15)
            params(12).Value = oClassAddress.Phone1
            params(13) = New SqlParameter("@BODAreaPhone2", SqlDbType.Char, 4)
            params(13).Value = oClassAddress.AreaPhone2
            params(14) = New SqlParameter("@BODPhone2", SqlDbType.Char, 15)
            params(14).Value = oClassAddress.Phone2

            params(15) = New SqlParameter("@BODAreaFax", SqlDbType.VarChar, 4)
            params(15).Value = oClassAddress.AreaFax
            params(16) = New SqlParameter("@BODFax", SqlDbType.VarChar, 10)
            params(16).Value = oClassAddress.Fax

            params(17) = New SqlParameter("@BODEmail", SqlDbType.VarChar, 30)
            params(17).Value = oClassPersonal.Email
            params(18) = New SqlParameter("@BODHP", SqlDbType.VarChar, 20)
            params(18).Value = oClassPersonal.MobilePhone

            params(19) = New SqlParameter("@BODNPWP", SqlDbType.VarChar, 30)
            params(19).Value = oCompany.BODNPWP
            params(20) = New SqlParameter("@BODIDType", SqlDbType.VarChar, 10)
            params(20).Value = oCompany.BODIDType
            params(21) = New SqlParameter("@BODIDNumber", SqlDbType.VarChar, 20)
            params(21).Value = oCompany.BODIDNumber
            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spCOMPANY_BOD_EDIT, params)
            objtrans.Commit()
        Catch exp As Exception
            'Throw New TechnicalException(TechnicalExceptions.DataAccess, ex)
            objtrans.Rollback()
            WriteException("Company", "CompanyBODEdit", exp.Message + exp.StackTrace)
            Throw New Exception("A record already exists with the same primary key !")
        Finally
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
        End Try
    End Sub
#End Region

#Region "CompanyDelete"
    Public Sub CompanyBODDelete(ByVal oCompany As Parameter.CompanyBOD)
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Dim objcon As New SqlConnection(oCompany.strConnection)
        Dim objtrans As SqlTransaction = Nothing
        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction
            With oCompany

                params(0) = New SqlParameter("@CompanyID", SqlDbType.Char, 3)
                params(0).Value = .CompanyID
                params(1) = New SqlParameter("@BODID", SqlDbType.Char, 10)
                params(1).Value = .BODID
            End With
            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spCOMPANY_BOD_DEL, params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("Company", "CompanyBODDelete", exp.Message + exp.StackTrace)
            Throw New Exception("INFO: Unable to delete the selected record. Record already used !")
        Finally
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
        End Try
    End Sub
#End Region

    Public Function ListCompanyBODReport(ByVal customclass As Parameter.CompanyBOD) As Parameter.CompanyBOD
        Dim params(0) As SqlParameter
        params(0) = New SqlParameter("@CompanyID", SqlDbType.Char, 3)
        params(0).Value = customclass.CompanyID
        Try
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spCOMPANY_BOD_Report, params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("Company", "ListCompanyBODReport", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region "COMPANY COMMISIONER"

#Region "CompanyCommisionerList"

    Public Function ListCompanyCommisioner(ByVal customclass As Parameter.CompanyCommisioner) As Parameter.CompanyCommisioner
        Dim params() As SqlParameter = New SqlParameter(5) {}
        Try
            params(0) = New SqlParameter("@CompanyId", SqlDbType.Char, 3)
            params(0).Value = customclass.CompanyID

            params(1) = New SqlParameter("@CurrentPage", SqlDbType.Int)
            params(1).Value = customclass.CurrentPage

            params(2) = New SqlParameter("@PageSize", SqlDbType.Int)
            params(2).Value = customclass.PageSize

            params(3) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
            params(3).Value = customclass.WhereCond

            params(4) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
            params(4).Value = customclass.SortBy

            params(5) = New SqlParameter("@TotalRecords", SqlDbType.SmallInt)
            params(5).Direction = ParameterDirection.Output


            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spCOMPANY_Commisioner_LIST, params).Tables(0)
            customclass.TotalRecord = CInt(params(5).Value)
            Return customclass
        Catch exp As Exception
            WriteException("Company", "ListCompanyCommisioner", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function ListCompanyCommisionerByID(ByVal customclass As Parameter.CompanyCommisioner) As Parameter.CompanyCommisioner
        Dim params() As SqlParameter = New SqlParameter(1) {}

        params(0) = New SqlParameter("@CompanyId", SqlDbType.Char, 3)
        params(0).Value = customclass.CompanyID

        params(1) = New SqlParameter("@CommisionerID", SqlDbType.Char, 10)
        params(1).Value = customclass.CommisionerID

        Try
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spCOMPANY_Commisioner_BYID, params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("Company", "ListCompanyCommisionerByID", exp.Message + exp.StackTrace)
        End Try

    End Function

#End Region

#Region " CompanyCommisionerAdd"

    Public Sub CompanyCommisionerAdd(ByVal oCompany As Parameter.CompanyCommisioner, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal)
        Dim params() As SqlParameter = New SqlParameter(21) {}
        Dim objcon As New SqlConnection(oCompany.strConnection)
        Dim objtrans As SqlTransaction = Nothing
        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction
            params(0) = New SqlParameter("@CompanyID", SqlDbType.Char, 3)
            params(0).Value = oCompany.CompanyID
            params(1) = New SqlParameter("@CommisionerID", SqlDbType.Char, 10)
            params(1).Value = oCompany.CommisionerID
            params(2) = New SqlParameter("@CommisionerName", SqlDbType.VarChar, 50)
            params(2).Value = oCompany.CommisionerName
            params(3) = New SqlParameter("@Title", SqlDbType.VarChar, 100)
            params(3).Value = oCompany.CommisionerTitle

            params(4) = New SqlParameter("@CommisionerAddress", SqlDbType.VarChar, 100)
            params(4).Value = oClassAddress.Address

            params(5) = New SqlParameter("@CommisionerRT", SqlDbType.Char, 3)
            params(5).Value = oClassAddress.RT
            params(6) = New SqlParameter("@CommisionerRW", SqlDbType.Char, 3)
            params(6).Value = oClassAddress.RW

            params(7) = New SqlParameter("@CommisionerKelurahan", SqlDbType.VarChar, 30)
            params(7).Value = oClassAddress.Kelurahan
            params(8) = New SqlParameter("@CommisionerKecamatan", SqlDbType.VarChar, 30)
            params(8).Value = oClassAddress.Kecamatan
            params(9) = New SqlParameter("@CommisionerCity", SqlDbType.VarChar, 30)
            params(9).Value = oClassAddress.City
            params(10) = New SqlParameter("@CommisionerZipCode", SqlDbType.Char, 5)
            params(10).Value = oClassAddress.ZipCode
            params(11) = New SqlParameter("@CommisionerAreaPhone1", SqlDbType.Char, 4)
            params(11).Value = oClassAddress.AreaPhone1
            params(12) = New SqlParameter("@CommisionerPhone1", SqlDbType.Char, 15)
            params(12).Value = oClassAddress.Phone1
            params(13) = New SqlParameter("@CommisionerAreaPhone2", SqlDbType.Char, 4)
            params(13).Value = oClassAddress.AreaPhone2
            params(14) = New SqlParameter("@CommisionerPhone2", SqlDbType.Char, 15)
            params(14).Value = oClassAddress.Phone2

            params(15) = New SqlParameter("@CommisionerAreaFax", SqlDbType.VarChar, 4)
            params(15).Value = oClassAddress.AreaFax
            params(16) = New SqlParameter("@CommisionerFax", SqlDbType.VarChar, 10)
            params(16).Value = oClassAddress.Fax


            params(17) = New SqlParameter("@CommisionerEmail", SqlDbType.VarChar, 30)
            params(17).Value = oClassPersonal.Email
            params(18) = New SqlParameter("@CommisionerHP", SqlDbType.VarChar, 20)
            params(18).Value = oClassPersonal.MobilePhone

            params(19) = New SqlParameter("@CommisionerNPWP", SqlDbType.VarChar, 30)
            params(19).Value = oCompany.CommisionerNPWP
            params(20) = New SqlParameter("@CommisionerIDType", SqlDbType.VarChar, 10)
            params(20).Value = oCompany.CommisionerIDType
            params(21) = New SqlParameter("@CommisionerIDNumber", SqlDbType.VarChar, 20)
            params(21).Value = oCompany.CommisionerIDNumber

            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spCOMPANY_Commisioner_ADD, params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("Company", "CompanyCommisionerAdd", exp.Message + exp.StackTrace)
            Throw New Exception("A record already exists with the same primary key !")
        Finally
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
        End Try
    End Sub
#End Region

#Region " CompanyCommisionerEdit"
    Public Sub CompanyCommisionerEdit(ByVal oCompany As Parameter.CompanyCommisioner, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal)

        Dim params() As SqlParameter = New SqlParameter(21) {}
        Dim objcon As New SqlConnection(oCompany.strConnection)
        Dim objtrans As SqlTransaction = Nothing
        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction

            params(0) = New SqlParameter("@CompanyID", SqlDbType.Char, 3)
            params(0).Value = oCompany.CompanyID
            params(1) = New SqlParameter("@CommisionerID", SqlDbType.Char, 10)
            params(1).Value = oCompany.CommisionerID
            params(2) = New SqlParameter("@CommisionerName", SqlDbType.VarChar, 50)
            params(2).Value = oCompany.CommisionerName
            params(3) = New SqlParameter("@Title", SqlDbType.VarChar, 100)
            params(3).Value = oCompany.CommisionerTitle

            params(4) = New SqlParameter("@CommisionerAddress", SqlDbType.VarChar, 100)
            params(4).Value = oClassAddress.Address

            params(5) = New SqlParameter("@CommisionerRT", SqlDbType.Char, 3)
            params(5).Value = oClassAddress.RT
            params(6) = New SqlParameter("@CommisionerRW", SqlDbType.Char, 3)
            params(6).Value = oClassAddress.RW

            params(7) = New SqlParameter("@CommisionerKelurahan", SqlDbType.VarChar, 30)
            params(7).Value = oClassAddress.Kelurahan
            params(8) = New SqlParameter("@CommisionerKecamatan", SqlDbType.VarChar, 30)
            params(8).Value = oClassAddress.Kecamatan
            params(9) = New SqlParameter("@CommisionerCity", SqlDbType.VarChar, 30)
            params(9).Value = oClassAddress.City
            params(10) = New SqlParameter("@CommisionerZipCode", SqlDbType.Char, 5)
            params(10).Value = oClassAddress.ZipCode
            params(11) = New SqlParameter("@CommisionerAreaPhone1", SqlDbType.Char, 4)
            params(11).Value = oClassAddress.AreaPhone1
            params(12) = New SqlParameter("@CommisionerPhone1", SqlDbType.Char, 15)
            params(12).Value = oClassAddress.Phone1
            params(13) = New SqlParameter("@CommisionerAreaPhone2", SqlDbType.Char, 4)
            params(13).Value = oClassAddress.AreaPhone2
            params(14) = New SqlParameter("@CommisionerPhone2", SqlDbType.Char, 15)
            params(14).Value = oClassAddress.Phone2

            params(15) = New SqlParameter("@CommisionerAreaFax", SqlDbType.VarChar, 4)
            params(15).Value = oClassAddress.AreaFax
            params(16) = New SqlParameter("@CommisionerFax", SqlDbType.VarChar, 10)
            params(16).Value = oClassAddress.Fax

            params(17) = New SqlParameter("@CommisionerEmail", SqlDbType.VarChar, 30)
            params(17).Value = oClassPersonal.Email
            params(18) = New SqlParameter("@CommisionerHP", SqlDbType.VarChar, 20)
            params(18).Value = oClassPersonal.MobilePhone

            params(19) = New SqlParameter("@CommisionerNPWP", SqlDbType.VarChar, 30)
            params(19).Value = oCompany.CommisionerNPWP
            params(20) = New SqlParameter("@CommisionerIDType", SqlDbType.VarChar, 10)
            params(20).Value = oCompany.CommisionerIDType
            params(21) = New SqlParameter("@CommisionerIDNumber", SqlDbType.VarChar, 20)
            params(21).Value = oCompany.CommisionerIDNumber
            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spCOMPANY_Commisioner_EDIT, params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("Company", "CompanyCommisionerEdit", exp.Message + exp.StackTrace)
            Throw New Exception("A record already exists with the same primary key !")
        Finally
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
        End Try
    End Sub
#End Region

#Region "CompanyCommisionerDelete"
    Public Sub CompanyCommisionerDelete(ByVal oCompany As Parameter.CompanyCommisioner)
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Dim objcon As New SqlConnection(oCompany.strConnection)
        Dim objtrans As SqlTransaction = Nothing
        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction
            With oCompany
                params(0) = New SqlParameter("@CompanyID", SqlDbType.Char, 3)
                params(0).Value = .CompanyID
                params(1) = New SqlParameter("@CommisionerID", SqlDbType.Char, 10)
                params(1).Value = .CommisionerID
            End With
            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spCOMPANY_Commisioner_DEL, params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("Company", "CompanyCommisionerDelete", exp.Message + exp.StackTrace)
            Throw New Exception("INFO: Unable to delete the selected record. Record already used !")
        Finally
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
        End Try
    End Sub
#End Region

    Public Function ListCompanyCommisionerReport(ByVal customclass As Parameter.CompanyCommisioner) As Parameter.CompanyCommisioner
        Dim params(0) As SqlParameter
        Try
            params(0) = New SqlParameter("@CompanyID", SqlDbType.Char, 3)
            params(0).Value = customclass.CompanyID

            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spCOMPANY_Commisioner_Report, params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("Company", "ListCompanyCommisionerReport", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region


End Class
