

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class AreaDtAcc : Inherits DataAccessBase
    Private Const spAreaMnt As String = "spAreaMnt"
    Private Const spRegionalMnt As String = "spRegionalMnt"
    Private Const spAreaAdd As String = "spAreaAdd"
    Private Const spRegionalAdd As String = "spRegionalAdd"
    Private Const spBranchLimitEdit As String = "spBranchLimitEdit"
    Private Const spAreaUpdate As String = "spAreaUpdate"
    Private Const spRegionalUpdate As String = "spRegionalUpdate"
    Private Const spAreaDelete As String = "spAreaDelete"
    Private Const spRegionalDelete As String = "spRegionalDelete"
    Private Const spAreaReport As String = "spAreaReport"
    Private Const spAreaInformasi As String = "spAreaInformasi"


    Public Function ListArea(ByVal customclass As Parameter.Area) As Parameter.Area

        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter("@TotalRecords", SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            If customclass.FormID = "Regional" Then
                customclass.ListArea = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spRegionalMnt, params).Tables(0)
                customclass.TotalRecord = CInt(params(4).Value)
            Else
                customclass.ListArea = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spAreaMnt, params).Tables(0)
                customclass.TotalRecord = CInt(params(4).Value)
            End If
            Return customclass
        Catch exp As Exception
            WriteException("AreaDtAcc", "ListArea", exp.Message + exp.StackTrace)
        End Try
    End Function

    Public Function AreaInfo(ByVal customclass As Parameter.Area) As Parameter.Area

        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter("@AreaID", SqlDbType.Char, 3)
            params(0).Value = customclass.AreaID
            customclass.ListArea = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spAreaInformasi, params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("AreaDtAcc", "AreaInfo", exp.Message + exp.StackTrace)
        End Try
    End Function


    Public Function SaveArea(ByVal customClass As Parameter.Area) As String

        Dim objCon As New SqlConnection(customClass.strConnection)
        Dim transaction As SqlTransaction = Nothing
        Dim params() As SqlParameter = New SqlParameter(25) {}
        If customClass.FormID = "Regional" Then
            Try
                If objCon.State = ConnectionState.Closed Then objCon.Open()
                transaction = objCon.BeginTransaction
                params(0) = New SqlParameter("@RegionalID", SqlDbType.Char, 3)
                params(1) = New SqlParameter("@RegionalFullName", SqlDbType.VarChar, 50)
                params(2) = New SqlParameter("@RegionalInitialName", SqlDbType.VarChar, 5)
                params(3) = New SqlParameter("@RegionalManager", SqlDbType.VarChar, 50)
                params(4) = New SqlParameter("@RegionalAddress", SqlDbType.VarChar, 100)
                params(5) = New SqlParameter("@RegionalRT", SqlDbType.VarChar, 3)
                params(6) = New SqlParameter("@RegionalRW", SqlDbType.VarChar, 3)
                params(7) = New SqlParameter("@RegionalKelurahan", SqlDbType.VarChar, 30)
                params(8) = New SqlParameter("@RegionalKecamatan", SqlDbType.VarChar, 30)
                params(9) = New SqlParameter("@RegionalCity", SqlDbType.VarChar, 30)
                params(10) = New SqlParameter("@RegionalZipCode", SqlDbType.VarChar, 5)
                params(11) = New SqlParameter("@RegionalAreaPhone1", SqlDbType.VarChar, 4)
                params(12) = New SqlParameter("@RegionalPhone1", SqlDbType.VarChar, 10)
                params(13) = New SqlParameter("@RegionalAreaPhone2", SqlDbType.VarChar, 4)
                params(14) = New SqlParameter("@RegionalPhone2", SqlDbType.VarChar, 10)
                params(15) = New SqlParameter("@RegionalAreaFax", SqlDbType.VarChar, 4)
                params(16) = New SqlParameter("@RegionalFax", SqlDbType.VarChar, 10)
                params(17) = New SqlParameter("@ContactPersonName", SqlDbType.VarChar, 50)
                params(18) = New SqlParameter("@ContactPersonJobTitle", SqlDbType.VarChar, 50)
                params(19) = New SqlParameter("@ContactPersonEmail", SqlDbType.VarChar, 30)
                params(20) = New SqlParameter("@ContactPersonHP", SqlDbType.VarChar, 20)
                params(21) = New SqlParameter("@UsrUpd", SqlDbType.VarChar, 20)
                params(22) = New SqlParameter("@DtmUpd", SqlDbType.SmallDateTime, 8)
                params(23) = New SqlParameter("@strerror", SqlDbType.VarChar, 100)
                params(24) = New SqlParameter("@AppMgr", SqlDbType.VarChar, 50)
                params(25) = New SqlParameter("@ApprovalLimit", SqlDbType.Decimal)

                With customClass
                    params(0).Value = .AreaID
                    params(1).Value = .AreaFullName
                    params(2).Value = .AreaInitialName
                    params(3).Value = .AreaManager
                    params(4).Value = .AreaAddress
                    params(5).Value = .AreaRT
                    params(6).Value = .AreaRW
                    params(7).Value = .AreaKelurahan
                    params(8).Value = .AreaKecamatan
                    params(9).Value = .AreaCity
                    params(10).Value = .AreaZipCode
                    params(11).Value = .AreaAreaPhone1
                    params(12).Value = .AreaPhone1
                    params(13).Value = .AreaAreaPhone2
                    params(14).Value = .AreaPhone2
                    params(15).Value = .AreaAreaFax
                    params(16).Value = .AreaFax
                    params(17).Value = .ContactPersonName
                    params(18).Value = .ContactPersonJobTitle
                    params(19).Value = .ContactPersonEmail
                    params(20).Value = .ContactPersonHP
                    params(21).Value = "Temp" '.LoginId
                    params(22).Value = CDate(Now)
                    params(23).Direction = ParameterDirection.Output
                    params(24).Value = getReferenceDBName()
                    params(25).Value = .ApprovalLimit
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spRegionalAdd, params)
                End With
                transaction.Commit()
                Return CStr(params(23).Value)
            Catch exp As Exception
                transaction.Rollback()
                WriteException("AreaDtAcc", "SaveArea", exp.Message + exp.StackTrace)
            Finally
                If objCon.State = ConnectionState.Open Then objCon.Close()
                objCon.Dispose()
            End Try
        Else
            Try
                If objCon.State = ConnectionState.Closed Then objCon.Open()
                transaction = objCon.BeginTransaction
                params(0) = New SqlParameter("@AreaID", SqlDbType.Char, 3)
                params(1) = New SqlParameter("@AreaFullName", SqlDbType.VarChar, 50)
                params(2) = New SqlParameter("@AreaInitialName", SqlDbType.VarChar, 5)
                params(3) = New SqlParameter("@AreaManager", SqlDbType.VarChar, 50)
                params(4) = New SqlParameter("@AreaAddress", SqlDbType.VarChar, 100)
                params(5) = New SqlParameter("@AreaRT", SqlDbType.VarChar, 3)
                params(6) = New SqlParameter("@AreaRW", SqlDbType.VarChar, 3)
                params(7) = New SqlParameter("@AreaKelurahan", SqlDbType.VarChar, 30)
                params(8) = New SqlParameter("@AreaKecamatan", SqlDbType.VarChar, 30)
                params(9) = New SqlParameter("@AreaCity", SqlDbType.VarChar, 30)
                params(10) = New SqlParameter("@AreaZipCode", SqlDbType.VarChar, 5)
                params(11) = New SqlParameter("@AreaAreaPhone1", SqlDbType.VarChar, 4)
                params(12) = New SqlParameter("@AreaPhone1", SqlDbType.VarChar, 10)
                params(13) = New SqlParameter("@AreaAreaPhone2", SqlDbType.VarChar, 4)
                params(14) = New SqlParameter("@AreaPhone2", SqlDbType.VarChar, 10)
                params(15) = New SqlParameter("@AreaAreaFax", SqlDbType.VarChar, 4)
                params(16) = New SqlParameter("@AreaFax", SqlDbType.VarChar, 10)
                params(17) = New SqlParameter("@ContactPersonName", SqlDbType.VarChar, 50)
                params(18) = New SqlParameter("@ContactPersonJobTitle", SqlDbType.VarChar, 50)
                params(19) = New SqlParameter("@ContactPersonEmail", SqlDbType.VarChar, 30)
                params(20) = New SqlParameter("@ContactPersonHP", SqlDbType.VarChar, 20)
                params(21) = New SqlParameter("@UsrUpd", SqlDbType.VarChar, 20)
                params(22) = New SqlParameter("@DtmUpd", SqlDbType.SmallDateTime, 8)
                params(23) = New SqlParameter("@strerror", SqlDbType.VarChar, 100)
                params(24) = New SqlParameter("@AppMgr", SqlDbType.VarChar, 50)
                params(25) = New SqlParameter("@ApprovalLimit", SqlDbType.Decimal)

                With customClass
                    params(0).Value = .AreaID
                    params(1).Value = .AreaFullName
                    params(2).Value = .AreaInitialName
                    params(3).Value = .AreaManager
                    params(4).Value = .AreaAddress
                    params(5).Value = .AreaRT
                    params(6).Value = .AreaRW
                    params(7).Value = .AreaKelurahan
                    params(8).Value = .AreaKecamatan
                    params(9).Value = .AreaCity
                    params(10).Value = .AreaZipCode
                    params(11).Value = .AreaAreaPhone1
                    params(12).Value = .AreaPhone1
                    params(13).Value = .AreaAreaPhone2
                    params(14).Value = .AreaPhone2
                    params(15).Value = .AreaAreaFax
                    params(16).Value = .AreaFax
                    params(17).Value = .ContactPersonName
                    params(18).Value = .ContactPersonJobTitle
                    params(19).Value = .ContactPersonEmail
                    params(20).Value = .ContactPersonHP
                    params(21).Value = "Temp" '.LoginId
                    params(22).Value = CDate(Now)
                    params(23).Direction = ParameterDirection.Output
                    params(24).Value = getReferenceDBName()
                    params(25).Value = .ApprovalLimit
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spAreaAdd, params)
                End With
                transaction.Commit()
                Return CStr(params(23).Value)
            Catch exp As Exception
                transaction.Rollback()
                WriteException("AreaDtAcc", "SaveArea", exp.Message + exp.StackTrace)
            Finally
                If objCon.State = ConnectionState.Open Then objCon.Close()
                objCon.Dispose()
            End Try
        End If
    End Function

    Public Function UpdateArea(ByVal customClass As Parameter.Area) As String
        Dim objCon As New SqlConnection(customClass.strConnection)
        Dim transaction As SqlTransaction = Nothing
        Dim oReturn As New Parameter.Area

        Dim paramsBranchLimit(3) As SqlParameter
        Dim params() As SqlParameter = New SqlParameter(25) {}
        Dim paramsArea() As SqlParameter = New SqlParameter(27) {}

        If customClass.FormID = "BranchLimit" Then
            Try
                paramsBranchLimit(0) = New SqlParameter("@IDBranchStatus", SqlDbType.VarChar, 10)
                paramsBranchLimit(0).Value = customClass.IDBranchStatus
                paramsBranchLimit(1) = New SqlParameter("@Description", SqlDbType.VarChar, 50)
                paramsBranchLimit(1).Value = customClass.Description
                paramsBranchLimit(2) = New SqlParameter("@ApprovalLimit", SqlDbType.Decimal)
                paramsBranchLimit(2).Value = customClass.ApprovalLimit
                paramsBranchLimit(3) = New SqlParameter("@strerror", SqlDbType.VarChar, 100)
                paramsBranchLimit(3).Direction = ParameterDirection.Output
                SqlHelper.ExecuteNonQuery(customClass.strConnection, CommandType.StoredProcedure, spBranchLimitEdit, paramsBranchLimit)
                oReturn.Output = CType(paramsBranchLimit(3).Value, String)
                ' Return oReturn
            Catch ex As Exception
                Throw New Exception(ex.Message)
            Finally
                If Not objCon Is Nothing Then
                    objCon.Close()
                End If
            End Try
        ElseIf customClass.FormID = "Regional" Then
            Try
                If objCon.State = ConnectionState.Closed Then objCon.Open()
                transaction = objCon.BeginTransaction
                params(0) = New SqlParameter("@RegionalID", SqlDbType.Char, 3)
                params(1) = New SqlParameter("@RegionalFullName", SqlDbType.VarChar, 50)
                params(2) = New SqlParameter("@RegionalInitialName", SqlDbType.VarChar, 5)
                params(3) = New SqlParameter("@RegionalManager", SqlDbType.VarChar, 50)
                params(4) = New SqlParameter("@RegionalAddress", SqlDbType.VarChar, 100)
                params(5) = New SqlParameter("@RegionalRT", SqlDbType.VarChar, 3)
                params(6) = New SqlParameter("@RegionalRW", SqlDbType.VarChar, 3)
                params(7) = New SqlParameter("@RegionalKelurahan", SqlDbType.VarChar, 30)
                params(8) = New SqlParameter("@RegionalKecamatan", SqlDbType.VarChar, 30)
                params(9) = New SqlParameter("@RegionalCity", SqlDbType.VarChar, 30)
                params(10) = New SqlParameter("@RegionalZipCode", SqlDbType.VarChar, 5)
                params(11) = New SqlParameter("@RegionalAreaPhone1", SqlDbType.VarChar, 4)
                params(12) = New SqlParameter("@RegionalPhone1", SqlDbType.VarChar, 10)
                params(13) = New SqlParameter("@RegionalAreaPhone2", SqlDbType.VarChar, 4)
                params(14) = New SqlParameter("@RegionalPhone2", SqlDbType.VarChar, 10)
                params(15) = New SqlParameter("@RegionalAreaFax", SqlDbType.VarChar, 4)
                params(16) = New SqlParameter("@RegionalFax", SqlDbType.VarChar, 10)
                params(17) = New SqlParameter("@ContactPersonName", SqlDbType.VarChar, 50)
                params(18) = New SqlParameter("@ContactPersonJobTitle", SqlDbType.VarChar, 50)
                params(19) = New SqlParameter("@ContactPersonEmail", SqlDbType.VarChar, 30)
                params(20) = New SqlParameter("@ContactPersonHP", SqlDbType.VarChar, 20)
                params(21) = New SqlParameter("@UsrUpd", SqlDbType.VarChar, 20)
                params(22) = New SqlParameter("@DtmUpd", SqlDbType.SmallDateTime, 8)
                params(23) = New SqlParameter("@strerror", SqlDbType.VarChar, 100)
                params(24) = New SqlParameter("@AppMgr", SqlDbType.VarChar, 50)
                params(25) = New SqlParameter("@ApprovalLimit", SqlDbType.Decimal)
                With customClass
                    params(0).Value = .AreaID
                    params(1).Value = .AreaFullName
                    params(2).Value = .AreaInitialName
                    params(3).Value = .AreaManager
                    params(4).Value = .AreaAddress
                    params(5).Value = .AreaRT
                    params(6).Value = .AreaRW
                    params(7).Value = .AreaKelurahan
                    params(8).Value = .AreaKecamatan
                    params(9).Value = .AreaCity
                    params(10).Value = .AreaZipCode
                    params(11).Value = .AreaAreaPhone1
                    params(12).Value = .AreaPhone1
                    params(13).Value = .AreaAreaPhone2
                    params(14).Value = .AreaPhone2
                    params(15).Value = .AreaAreaFax
                    params(16).Value = .AreaFax
                    params(17).Value = .ContactPersonName
                    params(18).Value = .ContactPersonJobTitle
                    params(19).Value = .ContactPersonEmail
                    params(20).Value = .ContactPersonHP
                    params(21).Value = "Me" '.LoginId
                    params(22).Value = CDate(Now)
                    params(23).Direction = ParameterDirection.Output
                    params(24).Value = getReferenceDBName()
                    params(25).Value = .ApprovalLimit
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spRegionalUpdate, params)
                End With
                transaction.Commit()
                Return CStr(params(23).Value)
            Catch exp As Exception
                transaction.Rollback()
                WriteException("AreaDtAcc", "UpdateArea", exp.Message + exp.StackTrace)
            Finally
                If objCon.State = ConnectionState.Open Then objCon.Close()
                objCon.Dispose()
            End Try
        Else
            Try
                If objCon.State = ConnectionState.Closed Then objCon.Open()
                transaction = objCon.BeginTransaction
                paramsArea(0) = New SqlParameter("@AreaID", SqlDbType.Char, 3)
                paramsArea(1) = New SqlParameter("@AreaFullName", SqlDbType.VarChar, 50)
                paramsArea(2) = New SqlParameter("@AreaInitialName", SqlDbType.VarChar, 5)
                paramsArea(3) = New SqlParameter("@AreaManager", SqlDbType.VarChar, 50)
                paramsArea(4) = New SqlParameter("@AreaAddress", SqlDbType.VarChar, 100)
                paramsArea(5) = New SqlParameter("@AreaRT", SqlDbType.VarChar, 3)
                paramsArea(6) = New SqlParameter("@AreaRW", SqlDbType.VarChar, 3)
                paramsArea(7) = New SqlParameter("@AreaKelurahan", SqlDbType.VarChar, 30)
                paramsArea(8) = New SqlParameter("@AreaKecamatan", SqlDbType.VarChar, 30)
                paramsArea(9) = New SqlParameter("@AreaCity", SqlDbType.VarChar, 30)
                paramsArea(10) = New SqlParameter("@AreaZipCode", SqlDbType.VarChar, 5)
                paramsArea(11) = New SqlParameter("@AreaAreaPhone1", SqlDbType.VarChar, 4)
                paramsArea(12) = New SqlParameter("@AreaPhone1", SqlDbType.VarChar, 10)
                paramsArea(13) = New SqlParameter("@AreaAreaPhone2", SqlDbType.VarChar, 4)
                paramsArea(14) = New SqlParameter("@AreaPhone2", SqlDbType.VarChar, 10)
                paramsArea(15) = New SqlParameter("@AreaAreaFax", SqlDbType.VarChar, 4)
                paramsArea(16) = New SqlParameter("@AreaFax", SqlDbType.VarChar, 10)
                paramsArea(17) = New SqlParameter("@ContactPersonName", SqlDbType.VarChar, 50)
                paramsArea(18) = New SqlParameter("@ContactPersonJobTitle", SqlDbType.VarChar, 50)
                paramsArea(19) = New SqlParameter("@ContactPersonEmail", SqlDbType.VarChar, 30)
                paramsArea(20) = New SqlParameter("@ContactPersonHP", SqlDbType.VarChar, 20)
                paramsArea(21) = New SqlParameter("@UsrUpd", SqlDbType.VarChar, 20)
                paramsArea(22) = New SqlParameter("@DtmUpd", SqlDbType.SmallDateTime, 8)
                paramsArea(23) = New SqlParameter("@strerror", SqlDbType.VarChar, 100)
                paramsArea(24) = New SqlParameter("@AppMgr", SqlDbType.VarChar, 50)
                paramsArea(25) = New SqlParameter("@ApprovalLimit", SqlDbType.Decimal)
                paramsArea(26) = New SqlParameter("@RegionalID", SqlDbType.Char, 3)
                paramsArea(27) = New SqlParameter("@RegionalIDNew", SqlDbType.Char, 3)
                With customClass
                    paramsArea(0).Value = .AreaID
                    paramsArea(1).Value = .AreaFullName
                    paramsArea(2).Value = .AreaInitialName
                    paramsArea(3).Value = .AreaManager
                    paramsArea(4).Value = .AreaAddress
                    paramsArea(5).Value = .AreaRT
                    paramsArea(6).Value = .AreaRW
                    paramsArea(7).Value = .AreaKelurahan
                    paramsArea(8).Value = .AreaKecamatan
                    paramsArea(9).Value = .AreaCity
                    paramsArea(10).Value = .AreaZipCode
                    paramsArea(11).Value = .AreaAreaPhone1
                    paramsArea(12).Value = .AreaPhone1
                    paramsArea(13).Value = .AreaAreaPhone2
                    paramsArea(14).Value = .AreaPhone2
                    paramsArea(15).Value = .AreaAreaFax
                    paramsArea(16).Value = .AreaFax
                    paramsArea(17).Value = .ContactPersonName
                    paramsArea(18).Value = .ContactPersonJobTitle
                    paramsArea(19).Value = .ContactPersonEmail
                    paramsArea(20).Value = .ContactPersonHP
                    paramsArea(21).Value = "Me" '.LoginId
                    paramsArea(22).Value = CDate(Now)
                    paramsArea(23).Direction = ParameterDirection.Output
                    paramsArea(24).Value = getReferenceDBName()
                    paramsArea(25).Value = .ApprovalLimit
                    paramsArea(26).Value = .RegionalID
                    paramsArea(27).Value = .RegionalIDNew
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spAreaUpdate, paramsArea)
                End With
                transaction.Commit()
                Return CStr(paramsArea(23).Value)
            Catch exp As Exception
                transaction.Rollback()
                WriteException("AreaDtAcc", "UpdateArea", exp.Message + exp.StackTrace)
            Finally
                If objCon.State = ConnectionState.Open Then objCon.Close()
                objCon.Dispose()
            End Try
        End If
    End Function

    Public Function DeleteArea(ByVal CustomClass As Parameter.Area) As String
        Dim objCon As New SqlConnection(CustomClass.strConnection)
        Dim transaction As SqlTransaction = Nothing

        If CustomClass.FormID = "Regional" Then
            Try
                If objCon.State = ConnectionState.Closed Then objCon.Open()
                transaction = objCon.BeginTransaction
                Dim params() As SqlParameter = New SqlParameter(2) {}
                params(0) = New SqlParameter("@RegionalID", SqlDbType.VarChar, 3)
                params(1) = New SqlParameter("@strerror", SqlDbType.VarChar, 100)
                params(2) = New SqlParameter("@AppMgr", SqlDbType.VarChar, 50)
                With CustomClass
                    params(0).Value = .AreaID
                    params(1).Direction = ParameterDirection.Output
                    params(2).Value = getReferenceDBName()
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spRegionalDelete, params)
                End With
                transaction.Commit()
                Return CStr(params(1).Value)
            Catch exp As Exception
                transaction.Rollback()
                WriteException("AreaDtAcc", "DeleteArea", exp.Message + exp.StackTrace)
            Finally
                If objCon.State = ConnectionState.Open Then objCon.Close()
                objCon.Dispose()
            End Try
        Else
            Try
                If objCon.State = ConnectionState.Closed Then objCon.Open()
                transaction = objCon.BeginTransaction
                Dim params() As SqlParameter = New SqlParameter(2) {}
                params(0) = New SqlParameter("@AreaID", SqlDbType.VarChar, 3)
                params(1) = New SqlParameter("@strerror", SqlDbType.VarChar, 100)
                params(2) = New SqlParameter("@AppMgr", SqlDbType.VarChar, 50)
                With CustomClass
                    params(0).Value = .AreaID
                    params(1).Direction = ParameterDirection.Output
                    params(2).Value = getReferenceDBName()
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spAreaDelete, params)
                End With
                transaction.Commit()
                Return CStr(params(1).Value)
            Catch exp As Exception
                transaction.Rollback()
                WriteException("AreaDtAcc", "DeleteArea", exp.Message + exp.StackTrace)
            Finally
                If objCon.State = ConnectionState.Open Then objCon.Close()
                objCon.Dispose()
            End Try
        End If
    End Function

    Public Function ReportArea(ByVal CustomClass As Parameter.Area) As Parameter.Area
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
            params(0).Value = CustomClass.WhereCond

            params(1) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
            params(1).Value = CustomClass.SortBy

            CustomClass.ListReportArea = SqlHelper.ExecuteDataset(CustomClass.strConnection, CommandType.StoredProcedure, spAreaReport, params)
            Return CustomClass
        Catch exp As Exception
            WriteException("AreaDtAcc", "ReportArea", exp.Message + exp.StackTrace)
        End Try
    End Function

    
End Class
