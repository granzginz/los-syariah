

#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region

Public Class Employee : Inherits DataAccessBase

#Region "Const"
    Private Const spEmployee_LIST As String = "spEmployeeLIST"
    Private Const spEmployee_BYID As String = "spEmployeeByID"
    Private Const spEmployee_ADD As String = "spEmployeeAdd"
    Private Const spEmployee_EDIT As String = "spEmployeeEdit"
    Private Const spEmployee_DEL As String = "spEmployeeDelete"
    Private Const spEmployee_Report As String = "spEmployeeReport"
    Private Const spEmployee_Pindah As String = "spEmployeePindahCabang"
    'modify by ario 10/02/2018
    Private Const Query_Combo_Position As String = "select employeePositionID as ID, cast(employeePositionID+'-'+EmployeePosition as varchar) as Name from tblEmployeeposition  order by EmployeePosition  ASC"

    Private Const Query_Combo_AOSupervisor As String = "select employeeid as ID,employeeName as Name from branchemployee where employeePosition in('AOSPV','BM')"
    Private Const Query_Combo_IncentiveCard As String = "select employeeIncentiveCardID as ID, CardName as Name from EmployeeIncentiveCardH"
    Private Const Query_Combo_CASupervisor As String = "select employeeid as ID,employeeName as Name from branchemployee where employeePosition='CASPV'"
    Private Const Query_Combo_PenaltyCard As String = "select OverduePenaltyCardID as ID, CardName as Name from overduePenaltyCardH"

#End Region

#Region "EmployeeList"
    Public Function ListEmployee(ByVal customclass As Parameter.Employee) As Parameter.Employee

        Dim params() As SqlParameter = New SqlParameter(5) {}


        Try
            params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
            params(0).Value = customclass.BranchId

            params(1) = New SqlParameter("@CurrentPage", SqlDbType.Int)
            params(1).Value = customclass.CurrentPage

            params(2) = New SqlParameter("@PageSize", SqlDbType.Int)
            params(2).Value = customclass.PageSize

            params(3) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
            params(3).Value = customclass.WhereCond

            params(4) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
            params(4).Value = customclass.SortBy

            params(5) = New SqlParameter("@TotalRecords", SqlDbType.SmallInt)
            params(5).Direction = ParameterDirection.Output

            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spEmployee_LIST, params).Tables(0)
            customclass.TotalRecord = CInt(params(5).Value)
            Return customclass
        Catch exp As Exception
            WriteException("Employee", "ListEmployee", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region " EmployeeAdd"
    Public Sub EmployeeAdd(ByVal customClass As Parameter.Employee, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal)

        Dim params() As SqlParameter = New SqlParameter(31) {}

        Dim objcon As New SqlConnection(customClass.strConnection)
        Dim objtrans As SqlTransaction = Nothing
        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction
            params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
            params(0).Value = customClass.BranchId
            params(1) = New SqlParameter("@EmployeeID", SqlDbType.VarChar, 20)
            params(1).Value = customClass.EmployeeID
            params(2) = New SqlParameter("@EmployeeName", SqlDbType.VarChar, 50)
            params(2).Value = customClass.EmployeeName
            params(3) = New SqlParameter("@EmployeePosition", SqlDbType.VarChar, 10)
            params(3).Value = customClass.Position
            params(4) = New SqlParameter("@AOLevelStatus", SqlDbType.VarChar, 1)
            params(4).Value = customClass.AOLevel
            params(5) = New SqlParameter("@AOSupervisor ", SqlDbType.VarChar, 20)
            params(5).Value = customClass.AOSupervisor
            params(6) = New SqlParameter("@AOSalesStatus ", SqlDbType.VarChar, 1)
            params(6).Value = customClass.AOSales
            params(7) = New SqlParameter("@AOFirstDate ", SqlDbType.VarChar, 10)
            params(7).Value = customClass.AOFirstDate
            params(8) = New SqlParameter("@EmployeeIncentiveCardID ", SqlDbType.VarChar, 10)
            params(8).Value = customClass.EmployeeIncentiveCard
            params(9) = New SqlParameter("@CASupervisor", SqlDbType.VarChar, 20)
            params(9).Value = customClass.CASupervisor
            params(10) = New SqlParameter("@OverduePenaltyCardID", SqlDbType.VarChar, 10)
            params(10).Value = customClass.OverduePenaltyCard


            params(11) = New SqlParameter("@Address", SqlDbType.VarChar, 100)
            params(11).Value = oClassAddress.Address
            params(12) = New SqlParameter("@RT", SqlDbType.VarChar, 3)
            params(12).Value = oClassAddress.RT
            params(13) = New SqlParameter("@RW", SqlDbType.VarChar, 3)
            params(13).Value = oClassAddress.RW

            params(14) = New SqlParameter("@Kelurahan", SqlDbType.VarChar, 30)
            params(14).Value = oClassAddress.Kelurahan
            params(15) = New SqlParameter("@Kecamatan", SqlDbType.VarChar, 30)
            params(15).Value = oClassAddress.Kecamatan
            params(16) = New SqlParameter("@City", SqlDbType.VarChar, 30)
            params(16).Value = oClassAddress.City
            params(17) = New SqlParameter("@ZipCode", SqlDbType.Char, 5)
            params(17).Value = oClassAddress.ZipCode
            params(18) = New SqlParameter("@AreaPhone1", SqlDbType.Char, 4)
            params(18).Value = oClassAddress.AreaPhone1
            params(19) = New SqlParameter("@Phone1", SqlDbType.VarChar, 15)
            params(19).Value = oClassAddress.Phone1
            params(20) = New SqlParameter("@AreaPhone2", SqlDbType.VarChar, 4)
            params(20).Value = oClassAddress.AreaPhone2
            params(21) = New SqlParameter("@Phone2", SqlDbType.VarChar, 15)
            params(21).Value = oClassAddress.Phone2
            params(22) = New SqlParameter("@AreaFax", SqlDbType.VarChar, 4)
            params(22).Value = oClassAddress.AreaFax
            params(23) = New SqlParameter("@Fax", SqlDbType.VarChar, 15)
            params(23).Value = oClassAddress.Fax


            params(24) = New SqlParameter("@ContactPersonEmail", SqlDbType.VarChar, 30)
            params(24).Value = oClassPersonal.Email
            params(25) = New SqlParameter("@ContactPersonHP", SqlDbType.VarChar, 20)
            params(25).Value = oClassPersonal.MobilePhone

            'parameter bank account
            params(26) = New SqlParameter("@BankID", SqlDbType.Char, 5)
            params(26).Value = customClass.BankID
            params(27) = New SqlParameter("@BankBranchID", SqlDbType.Int)
            params(27).Value = customClass.BankBranchId
            params(28) = New SqlParameter("@AccountNo", SqlDbType.VarChar, 25)
            params(28).Value = customClass.AccountNo
            params(29) = New SqlParameter("@AccountName", SqlDbType.VarChar, 50)
            params(29).Value = customClass.AccountName
            params(30) = New SqlParameter("@InitialName", SqlDbType.Char, 3)
            params(30).Value = customClass.Inisial

            params(31) = New SqlParameter("@AppMgr", SqlDbType.VarChar, 50)
            params(31).Value = getReferenceDBName()




            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spEmployee_ADD, params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("Employee", "EmployeeAdd", exp.Message + exp.StackTrace)
            Throw New Exception("A record already Exist !")
        Finally
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
        End Try
    End Sub
#End Region

#Region " EmployeeEdit"
    Public Sub EmployeeEdit(ByVal customClass As Parameter.Employee, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal)

        Dim params() As SqlParameter = New SqlParameter(32) {}

        Dim objcon As New SqlConnection(customClass.strConnection)
        Dim objtrans As SqlTransaction = Nothing
        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction
            params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 3)
            params(0).Value = customClass.BranchId
            params(1) = New SqlParameter("@EmployeeID", SqlDbType.VarChar, 20)
            params(1).Value = customClass.EmployeeID
            params(2) = New SqlParameter("@EmployeeName", SqlDbType.VarChar, 50)
            params(2).Value = customClass.EmployeeName
            params(3) = New SqlParameter("@EmployeePosition", SqlDbType.VarChar, 10)
            params(3).Value = customClass.Position
            params(4) = New SqlParameter("@AOLevelStatus", SqlDbType.VarChar, 1)
            params(4).Value = customClass.AOLevel
            params(5) = New SqlParameter("@AOSupervisor ", SqlDbType.VarChar, 20)
            params(5).Value = customClass.AOSupervisor
            params(6) = New SqlParameter("@AOSalesStatus ", SqlDbType.VarChar, 1)
            params(6).Value = customClass.AOSales
            params(7) = New SqlParameter("@AOFirstDate ", SqlDbType.VarChar, 10)
            params(7).Value = customClass.AOFirstDate
            params(8) = New SqlParameter("@EmployeeIncentiveCardID ", SqlDbType.VarChar, 10)
            params(8).Value = customClass.EmployeeIncentiveCard
            params(9) = New SqlParameter("@CASupervisor", SqlDbType.VarChar, 20)
            params(9).Value = customClass.CASupervisor
            params(10) = New SqlParameter("@OverduePenaltyCardID", SqlDbType.VarChar, 10)
            params(10).Value = customClass.OverduePenaltyCard


            params(11) = New SqlParameter("@Address", SqlDbType.VarChar, 100)
            params(11).Value = oClassAddress.Address
            params(12) = New SqlParameter("@RT", SqlDbType.VarChar, 3)
            params(12).Value = oClassAddress.RT
            params(13) = New SqlParameter("@RW", SqlDbType.VarChar, 3)
            params(13).Value = oClassAddress.RW

            params(14) = New SqlParameter("@Kelurahan", SqlDbType.VarChar, 30)
            params(14).Value = oClassAddress.Kelurahan
            params(15) = New SqlParameter("@Kecamatan", SqlDbType.VarChar, 30)
            params(15).Value = oClassAddress.Kecamatan
            params(16) = New SqlParameter("@City", SqlDbType.VarChar, 30)
            params(16).Value = oClassAddress.City
            params(17) = New SqlParameter("@ZipCode", SqlDbType.VarChar, 5)
            params(17).Value = oClassAddress.ZipCode
            params(18) = New SqlParameter("@AreaPhone1", SqlDbType.VarChar, 4)
            params(18).Value = oClassAddress.AreaPhone1
            params(19) = New SqlParameter("@Phone1", SqlDbType.VarChar, 15)
            params(19).Value = oClassAddress.Phone1
            params(20) = New SqlParameter("@AreaPhone2", SqlDbType.VarChar, 4)
            params(20).Value = oClassAddress.AreaPhone2
            params(21) = New SqlParameter("@Phone2", SqlDbType.VarChar, 15)
            params(21).Value = oClassAddress.Phone2
            params(22) = New SqlParameter("@AreaFax", SqlDbType.Char, 4)
            params(22).Value = oClassAddress.AreaFax
            params(23) = New SqlParameter("@Fax", SqlDbType.VarChar, 15)
            params(23).Value = oClassAddress.Fax

            params(24) = New SqlParameter("@ContactPersonEmail", SqlDbType.VarChar, 30)
            params(24).Value = oClassPersonal.Email
            params(25) = New SqlParameter("@ContactPersonHP", SqlDbType.VarChar, 20)
            params(25).Value = oClassPersonal.MobilePhone

            params(26) = New SqlParameter("@isActive", SqlDbType.Bit)
            params(26).Value = customClass.isActive

            'parameter bank account
            params(27) = New SqlParameter("@BankID", SqlDbType.Char, 5)
            params(27).Value = customClass.BankID
            params(28) = New SqlParameter("@BankBranchID", SqlDbType.Int)
            params(28).Value = customClass.BankBranchId
            params(29) = New SqlParameter("@AccountNo", SqlDbType.VarChar, 25)
            params(29).Value = customClass.AccountNo
            params(30) = New SqlParameter("@AccountName", SqlDbType.VarChar, 50)
            params(30).Value = customClass.AccountName
            params(31) = New SqlParameter("@InitialName", SqlDbType.Char, 3)
            params(31).Value = customClass.Inisial
            params(32) = New SqlParameter("@AppMgr", SqlDbType.VarChar, 50)
            params(32).Value = getReferenceDBName()
            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spEmployee_EDIT, params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("Employee", "EmployeeEdit", exp.Message + exp.StackTrace)
            Throw New Exception("INFO: Unable to edit the selected record")
        Finally
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
        End Try
    End Sub
#End Region

#Region "EmployeeDelete"
    Public Sub EmployeeDelete(ByVal oEmployee As Parameter.Employee)

        Dim params(2) As SqlParameter
        Dim objcon As New SqlConnection(oEmployee.strConnection)
        Dim objtrans As SqlTransaction = Nothing
        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction
            With oEmployee

                params(0) = New SqlParameter("@EmployeeID", SqlDbType.VarChar, 20)
                params(0).Value = .EmployeeID

                params(1) = New SqlParameter("@BranchID", SqlDbType.VarChar, 10)
                params(1).Value = .BranchId


                params(2) = New SqlParameter("@AppMgr", SqlDbType.VarChar, 50)
                params(2).Value = getReferenceDBName()


            End With


            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spEmployee_DEL, params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("Employee", "EmployeeDelete", exp.Message + exp.StackTrace)
            Throw New Exception("INFO: Unable to delete the selected record. Record already used !")
        Finally
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
        End Try
    End Sub
#End Region

#Region "EmployeeReport"
    Public Function EmployeeReport(ByVal customclass As Parameter.Employee) As DataTable

        Dim dt As New DataTable

        Dim params() As SqlParameter = New SqlParameter(2) {}
        Try
            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(0).Value = customclass.BranchId

            params(1) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
            params(1).Value = customclass.WhereCond

            params(2) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
            params(2).Value = customclass.SortBy


            dt = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spEmployee_Report, params).Tables(0)
            Return dt
        Catch exp As Exception
            WriteException("Employee", "EmployeeReport", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region "EmployeeByID"
    Public Function ListEmployeeByID(ByVal customclass As Parameter.Employee) As Parameter.Employee

        Dim params() As SqlParameter = New SqlParameter(1) {}

        Try
            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(0).Value = customclass.BranchId

            params(1) = New SqlParameter("@EmployeeID", SqlDbType.VarChar, 20)
            params(1).Value = customclass.EmployeeID

            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spEmployee_BYID, params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("Employee", "ListEmployeeByID", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

    Public Function GetCombo(ByVal strcon As String, ByVal strCombo As String, ByVal BranchID As String) As DataTable
        Dim Query As String = ""
        Dim dt As New DataTable

        'If strBranchID <> "" Then QueryWhere = " Where BranchID =" & strBranchID
        Select Case strCombo
            Case "Position"
                Query = Query_Combo_Position
            Case "AOSupervisor"
                Query = Query_Combo_AOSupervisor & " and branchid = '" & BranchID & "'"
            Case "IncentiveCard"
                Query = Query_Combo_IncentiveCard
            Case "CASupervisor"
                Query = Query_Combo_CASupervisor & " and branchid = '" & BranchID & "'"
            Case "PenaltyCard"
                Query = Query_Combo_PenaltyCard
        End Select
        Try
            dt = SqlHelper.ExecuteDataset(strcon, CommandType.Text, Query).Tables(0)
            Return dt
        Catch exp As Exception
            WriteException("Employee", "GetCombo", exp.Message + exp.StackTrace)
        End Try
    End Function

#Region "EmployeePindahCabang"
    Public Sub EmployeePindahCabang(ByVal oEmployee As Parameter.Employee)

        Dim params(3) As SqlParameter
        Dim objcon As New SqlConnection(oEmployee.strConnection)
        Dim objtrans As SqlTransaction = Nothing
        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objtrans = objcon.BeginTransaction
            With oEmployee

                params(0) = New SqlParameter("@EmployeeID", SqlDbType.VarChar, 20)
                params(0).Value = .EmployeeID
                params(1) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                params(1).Value = .BranchId
                params(2) = New SqlParameter("@AppMgr", SqlDbType.VarChar, 50)
                params(2).Value = getReferenceDBName()
                params(3) = New SqlParameter("@DestBranchID", SqlDbType.Char, 3)
                params(3).Value = .DestBranchId
            End With


            SqlHelper.ExecuteNonQuery(objtrans, CommandType.StoredProcedure, spEmployee_Pindah, params)
            objtrans.Commit()
        Catch exp As Exception
            objtrans.Rollback()
            WriteException("Employee", "EmployeePindahCabang", exp.Message + exp.StackTrace)
            Throw New Exception("INFO: Unable to move employee to the other branch. Record already used !")
        Finally
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
        End Try
    End Sub
#End Region
End Class
