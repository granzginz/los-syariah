
#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions

#End Region
Public Class Branch : Inherits Maxiloan.SQLEngine.DataAccessBase

#Region "Const"
    Private Const spBRANCH_LIST As String = "spBRANCHLIST"
    Private Const spBRANCH_BYID As String = "spBRANCHByID"
    Private Const spBRANCH_ADD As String = "spBRANCHAdd"
    Private Const spBRANCH_EDIT As String = "spBRANCHEdit"
    Private Const spBRANCH_DEL As String = "spBRANCHDelete"
    Private Const spBRANCH_Report As String = "spBRANCHReport"
    Private Const spSettingBranchBudgetPrint As String = "spSettingBranchBudgetPrint"
    Private Const spSettingBranchBudget As String = "spSettingBranchBudget"
    Private Const spSettingBranchForecastPrint As String = "spSettingBranchForecastPrint"
    Private Const spSettingBranchForecast As String = "spSettingBranchForecast"
    Private Const Query_Combo_Company As String = "select companyID as Id, companyfullname as Name from company"
    Private Const Query_Combo_Area As String = "select areaid as id, areafullname as name from area"
    Private Const Query_Combo_AOCard As String = "select AOIncentiveCardID as ID, CardName as Name from AOIncentiveCardH"

#End Region
#Region "BranchList"
    Public Function ListBranch(ByVal customclass As Parameter.Branch) As Parameter.Branch

        Dim params() As SqlParameter = New SqlParameter(4) {}

        Try
            params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter("@TotalRecords", SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spBRANCH_LIST, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch exp As Exception
            WriteException("Branch", "ListBranch", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region
#Region " BranchAdd"
    Public Sub BranchAdd(ByVal customClass As Parameter.Branch, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal)
        Dim conn As New SqlConnection(customClass.strConnection)
        Dim transaction As SqlTransaction = Nothing
        Dim params(32) As SqlParameter
        Dim oReturnValue As New Parameter.Branch
        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            transaction = conn.BeginTransaction
            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(0).Value = customClass.BranchId
            params(1) = New SqlParameter("@BranchFullName", SqlDbType.VarChar, 50)
            params(1).Value = customClass.BranchFullName
            params(2) = New SqlParameter("@BranchInitialName", SqlDbType.Char, 5)
            params(2).Value = customClass.BranchInitialName
            params(3) = New SqlParameter("@CompanyID", SqlDbType.Char, 3)
            params(3).Value = customClass.CompanyID
            params(4) = New SqlParameter("@AreaID", SqlDbType.VarChar, 3)
            params(4).Value = customClass.AreaID

            params(5) = New SqlParameter("@Address", SqlDbType.VarChar, 100)
            params(5).Value = oClassAddress.Address
            params(6) = New SqlParameter("@RT", SqlDbType.Char, 3)
            params(6).Value = oClassAddress.RT
            params(7) = New SqlParameter("@RW", SqlDbType.Char, 3)
            params(7).Value = oClassAddress.RW

            params(8) = New SqlParameter("@Kelurahan", SqlDbType.VarChar, 30)
            params(8).Value = oClassAddress.Kelurahan
            params(9) = New SqlParameter("@Kecamatan", SqlDbType.VarChar, 30)
            params(9).Value = oClassAddress.Kecamatan
            params(10) = New SqlParameter("@City", SqlDbType.VarChar, 30)
            params(10).Value = oClassAddress.City
            params(11) = New SqlParameter("@ZipCode", SqlDbType.Char, 5)
            params(11).Value = oClassAddress.ZipCode
            params(12) = New SqlParameter("@AreaPhone1", SqlDbType.Char, 4)
            params(12).Value = oClassAddress.AreaPhone1
            params(13) = New SqlParameter("@Phone1", SqlDbType.Char, 15)
            params(13).Value = oClassAddress.Phone1
            params(14) = New SqlParameter("@AreaPhone2", SqlDbType.Char, 4)
            params(14).Value = oClassAddress.AreaPhone2
            params(15) = New SqlParameter("@Phone2", SqlDbType.Char, 15)
            params(15).Value = oClassAddress.Phone2
            params(16) = New SqlParameter("@AreaFax", SqlDbType.VarChar, 4)
            params(16).Value = oClassAddress.AreaFax
            params(17) = New SqlParameter("@Fax", SqlDbType.VarChar, 10)
            params(17).Value = oClassAddress.Fax


            params(18) = New SqlParameter("@ContactPersonName", SqlDbType.VarChar, 50)
            params(18).Value = oClassPersonal.PersonName
            params(19) = New SqlParameter("@ContactPersonTitle", SqlDbType.VarChar, 50)
            params(19).Value = oClassPersonal.PersonTitle
            params(20) = New SqlParameter("@ContactPersonEmail", SqlDbType.VarChar, 30)
            params(20).Value = oClassPersonal.Email
            params(21) = New SqlParameter("@ContactPersonHP", SqlDbType.VarChar, 20)
            params(21).Value = oClassPersonal.MobilePhone


            params(22) = New SqlParameter("@BranchManager", SqlDbType.VarChar, 50)
            params(22).Value = customClass.BranchManager
            params(23) = New SqlParameter("@ADH", SqlDbType.VarChar, 50)
            params(23).Value = customClass.ADH
            params(24) = New SqlParameter("@ARControlName", SqlDbType.VarChar, 50)
            params(24).Value = customClass.ARControlName

            params(25) = New SqlParameter("@BranchStatus", SqlDbType.VarChar, 10)
            params(25).Value = customClass.BranchStatus
            params(26) = New SqlParameter("@ProductivityValue", SqlDbType.Int)
            params(26).Value = customClass.ProductivityValue
            params(27) = New SqlParameter("@ProductivityCollection", SqlDbType.Int)
            params(27).Value = customClass.ProductivityCollection

            params(28) = New SqlParameter("@NumOfEmployee", SqlDbType.Int)
            params(28).Value = customClass.EmployeeNumber
            params(29) = New SqlParameter("@AOIncentiveCardID", SqlDbType.Char, 10)
            params(29).Value = customClass.AOCard
            params(30) = New SqlParameter("@AssetDocCustodianName", SqlDbType.Char, 50)
            params(30).Value = customClass.Custodian
            params(31) = New SqlParameter("@STNKPICName", SqlDbType.VarChar, 20)
            params(31).Value = customClass.STNKPICName


            params(32) = New SqlParameter("@AppMgr", SqlDbType.VarChar, 50)
            params(32).Value = getReferenceDBName()


            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spBRANCH_ADD, params)
            transaction.Commit()
        Catch exp As Exception
            WriteException("Branch", "BranchAdd", exp.Message + exp.StackTrace)
            transaction.Rollback()
            Throw New Exception("A record already exist with the same primary key!")
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try

    End Sub
#End Region

#Region " BranchEdit"
    Public Sub BranchEdit(ByVal customClass As Parameter.Branch, ByVal oClassAddress As Parameter.Address, ByVal oClassPersonal As Parameter.Personal)
        Dim conn As New SqlConnection(customClass.strConnection)
        Dim transaction As SqlTransaction = Nothing

        Dim params(32) As SqlParameter
        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            transaction = conn.BeginTransaction
            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(0).Value = customClass.BranchId
            params(1) = New SqlParameter("@BranchFullName", SqlDbType.VarChar, 50)
            params(1).Value = customClass.BranchFullName
            params(2) = New SqlParameter("@BranchInitialName", SqlDbType.Char, 5)
            params(2).Value = customClass.BranchInitialName
            params(3) = New SqlParameter("@CompanyID", SqlDbType.Char, 3)
            params(3).Value = customClass.CompanyID
            params(4) = New SqlParameter("@AreaID", SqlDbType.VarChar, 3)
            params(4).Value = customClass.AreaID

            params(5) = New SqlParameter("@Address", SqlDbType.VarChar, 100)
            params(5).Value = oClassAddress.Address
            params(6) = New SqlParameter("@RT", SqlDbType.Char, 3)
            params(6).Value = oClassAddress.RT
            params(7) = New SqlParameter("@RW", SqlDbType.Char, 3)
            params(7).Value = oClassAddress.RW

            params(8) = New SqlParameter("@Kelurahan", SqlDbType.VarChar, 30)
            params(8).Value = oClassAddress.Kelurahan
            params(9) = New SqlParameter("@Kecamatan", SqlDbType.VarChar, 30)
            params(9).Value = oClassAddress.Kecamatan
            params(10) = New SqlParameter("@City", SqlDbType.VarChar, 30)
            params(10).Value = oClassAddress.City
            params(11) = New SqlParameter("@ZipCode", SqlDbType.Char, 5)
            params(11).Value = oClassAddress.ZipCode
            params(12) = New SqlParameter("@AreaPhone1", SqlDbType.Char, 4)
            params(12).Value = oClassAddress.AreaPhone1
            params(13) = New SqlParameter("@Phone1", SqlDbType.Char, 15)
            params(13).Value = oClassAddress.Phone1
            params(14) = New SqlParameter("@AreaPhone2", SqlDbType.Char, 4)
            params(14).Value = oClassAddress.AreaPhone2
            params(15) = New SqlParameter("@Phone2", SqlDbType.Char, 15)
            params(15).Value = oClassAddress.Phone2
            params(16) = New SqlParameter("@AreaFax", SqlDbType.VarChar, 4)
            params(16).Value = oClassAddress.AreaFax
            params(17) = New SqlParameter("@Fax", SqlDbType.VarChar, 10)
            params(17).Value = oClassAddress.Fax


            params(18) = New SqlParameter("@ContactPersonName", SqlDbType.VarChar, 50)
            params(18).Value = oClassPersonal.PersonName
            params(19) = New SqlParameter("@ContactPersonTitle", SqlDbType.VarChar, 50)
            params(19).Value = oClassPersonal.PersonTitle
            params(20) = New SqlParameter("@ContactPersonEmail", SqlDbType.VarChar, 30)
            params(20).Value = oClassPersonal.Email
            params(21) = New SqlParameter("@ContactPersonHP", SqlDbType.VarChar, 20)
            params(21).Value = oClassPersonal.MobilePhone


            params(22) = New SqlParameter("@BranchManager", SqlDbType.VarChar, 50)
            params(22).Value = customClass.BranchManager
            params(23) = New SqlParameter("@ADH", SqlDbType.VarChar, 50)
            params(23).Value = customClass.ADH
            params(24) = New SqlParameter("@ARControlName", SqlDbType.VarChar, 50)
            params(24).Value = customClass.ARControlName

            params(25) = New SqlParameter("@branchStatus", SqlDbType.VarChar, 10)
            params(25).Value = customClass.BranchStatus
            params(26) = New SqlParameter("@ProductivityValue", SqlDbType.Int)
            params(26).Value = customClass.ProductivityValue
            params(27) = New SqlParameter("@ProductivityCollection", SqlDbType.Int)
            params(27).Value = customClass.ProductivityCollection

            params(28) = New SqlParameter("@NumOfEmployee", SqlDbType.Int)
            params(28).Value = customClass.EmployeeNumber
            params(29) = New SqlParameter("@AOCard", SqlDbType.Char, 10)
            params(29).Value = customClass.AOCard
            params(30) = New SqlParameter("@AssetDocCustodianName", SqlDbType.VarChar, 50)
            params(30).Value = customClass.Custodian
            params(31) = New SqlParameter("@STNKPICName", SqlDbType.VarChar, 20)
            params(31).Value = customClass.STNKPICName



            params(32) = New SqlParameter("@AppMgr", SqlDbType.VarChar, 50)
            params(32).Value = getReferenceDBName()



            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spBRANCH_EDIT, params)
            transaction.Commit()
        Catch exp As Exception
            transaction.Rollback()
            WriteException("Branch", "BranchEdit", exp.Message + exp.StackTrace)
            Throw New Exception("INFO: Unable to edit the selected record")
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Sub
#End Region

#Region "BranchDelete"
    Public Sub BranchDelete(ByVal oBranch As Parameter.Branch)
        Dim conn As New SqlConnection(oBranch.strConnection)
        Dim transaction As SqlTransaction = Nothing
        Dim params(1) As SqlParameter


        Try
            If conn.State = ConnectionState.Closed Then conn.Open()
            transaction = conn.BeginTransaction
            With oBranch

                params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
                params(0).Value = .BranchId

                params(1) = New SqlParameter("@AppMgr", SqlDbType.VarChar, 50)
                params(1).Value = getReferenceDBName()





            End With

            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spBRANCH_DEL, params)
            transaction.Commit()
        Catch exp As Exception
            transaction.Rollback()
            WriteException("Branch", "BranchDelete", exp.Message + exp.StackTrace)
            Throw New Exception("INFO: Unable to delete the selected record. Record already used !")
        Finally
            If conn.State = ConnectionState.Open Then conn.Close()
            conn.Dispose()
        End Try
    End Sub
#End Region
#Region "BranchReport"
    Public Function Report(ByVal customclass As Parameter.Branch) As Parameter.Branch
        Try
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spBRANCH_Report).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("Branch", "Report", exp.Message + exp.StackTrace)
        End Try
    End Function
#End Region

#Region "BranchByID"
    Public Function ListBranchByID(ByVal customclass As Parameter.Branch) As Parameter.Branch

        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
            params(0).Value = customclass.BranchId
            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spBRANCH_BYID, params).Tables(0)
            Return customclass
        Catch exp As Exception
            WriteException("Branch", "ListBranchByID", exp.Message + exp.StackTrace)
        End Try

    End Function
#End Region

    Public Function GetCombo(ByVal strcon As String, ByVal strCombo As String) As DataTable
        Dim Query As String = ""
        Dim dt As New DataTable

        Select Case strCombo
            Case "Company"
                Query = Query_Combo_Company

            Case "Area"
                Query = Query_Combo_Area

            Case "AOCard"
                Query = Query_Combo_AOCard
        End Select

        Try
            dt = SqlHelper.ExecuteDataset(strcon, CommandType.Text, Query).Tables(0)
            Return dt
        Catch exp As Exception
            WriteException("Branch", "GetCombo", exp.Message + exp.StackTrace)
        End Try



    End Function
#Region " Branch Budget "
    'fungsi ini untuk menampilkan budget yang ada dari ao yang dipilih berdasar aoid dan branchid
    Public Function BranchBudgetListing(ByVal customclass As Parameter.Branch) As Parameter.Branch
        Dim data As New Parameter.Branch
        Dim params() As SqlParameter = New SqlParameter(7) {}
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customclass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customclass.PageSize
        params(2) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
        params(2).Value = customclass.SortBy
        params(3) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(3).Value = customclass.BranchId
        params(4) = New SqlParameter("@Year", SqlDbType.Int)
        params(4).Value = customclass.Year
        params(5) = New SqlParameter("@Month", SqlDbType.Int)
        params(5).Value = customclass.Month
        params(6) = New SqlParameter("@Asset", SqlDbType.Char, 3)
        params(6).Value = customclass.AssetStatus
        params(7) = New SqlParameter("@TotalRecords", SqlDbType.SmallInt)
        params(7).Direction = ParameterDirection.Output
        Try
            data.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spSettingBranchBudget, params).Tables(0)
            data.TotalRecord = CInt(params(7).Value)
            Return data
        Catch exp As Exception
            WriteException("BranchBudget", "BranchBudget", exp.Message + exp.StackTrace)
        End Try

    End Function
    Public Function BranchBudgetPrint(ByVal customclass As Parameter.Branch) As Parameter.Branch
        Dim data As New Parameter.Branch

        Dim params() As SqlParameter = New SqlParameter(3) {}
        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customclass.BranchId
        params(1) = New SqlParameter("@Year", SqlDbType.Int)
        params(1).Value = customclass.Year
        params(2) = New SqlParameter("@Month", SqlDbType.Int)
        params(2).Value = customclass.Month
        params(3) = New SqlParameter("@Asset", SqlDbType.Char, 3)
        params(3).Value = customclass.AssetStatus


        Try
            data.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spSettingBranchBudgetPrint, params).Tables(0)
            Return data


        Catch exp As Exception
            WriteException("BranchBudgetPrint", "BranchBudgetPrint", exp.Message + exp.StackTrace)
        End Try

    End Function
#End Region

#Region " Branch Forecast "
    'fungsi ini untuk menampilkan budget yang ada dari ao yang dipilih berdasar aoid dan branchid
    Public Function BranchForecastListing(ByVal customclass As Parameter.Branch) As Parameter.Branch
        Dim data As New Parameter.Branch
        Dim params() As SqlParameter = New SqlParameter(7) {}
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = customclass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = customclass.PageSize
        params(2) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
        params(2).Value = customclass.SortBy
        params(3) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(3).Value = customclass.BranchId
        params(4) = New SqlParameter("@Year", SqlDbType.Int)
        params(4).Value = customclass.Year
        params(5) = New SqlParameter("@Month", SqlDbType.Int)
        params(5).Value = customclass.Month
        params(6) = New SqlParameter("@Asset", SqlDbType.Char, 3)
        params(6).Value = customclass.AssetStatus
        params(7) = New SqlParameter("@TotalRecords", SqlDbType.SmallInt)
        params(7).Direction = ParameterDirection.Output
        Try
            data.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spSettingBranchForecast, params).Tables(0)
            data.TotalRecord = CInt(params(7).Value)
            Return data
        Catch exp As Exception
            WriteException("BranchForecast", "BranchForecast", exp.Message + exp.StackTrace)
        End Try

    End Function
    Public Function BranchForecastPrint(ByVal customclass As Parameter.Branch) As Parameter.Branch
        Dim data As New Parameter.Branch

        Dim params() As SqlParameter = New SqlParameter(3) {}
        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 3)
        params(0).Value = customclass.BranchId
        params(1) = New SqlParameter("@Year", SqlDbType.Int)
        params(1).Value = customclass.Year
        params(2) = New SqlParameter("@Month", SqlDbType.Int)
        params(2).Value = customclass.Month
        params(3) = New SqlParameter("@Asset", SqlDbType.Char, 3)
        params(3).Value = customclass.AssetStatus


        Try
            data.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spSettingBranchForecastPrint, params).Tables(0)
            Return data


        Catch exp As Exception
            WriteException("BranchForecastPrint", "BranchForecastPrint", exp.Message + exp.StackTrace)
        End Try

    End Function
#End Region


End Class
