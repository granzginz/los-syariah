﻿#Region "Imports"
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions
#End Region


Public Class AuthorizedSigner : Inherits Maxiloan.SQLEngine.DataAccessBase


#Region " Private Const sp "

    'Stored Procedure name    
    Private Const SP_AuthorizedSignerPaging As String = "spAuthorizedSignerPaging"
    Private Const SP_AuthorizedSignerView As String = "spAuthorizedSignerView"
    Private Const SP_AuthorizedSignerSaveAdd As String = "spAuthorizedSignerSaveAdd"
    Private Const SP_AuthorizedSignerSaveEdit As String = "spAuthorizedSignerSaveEdit"
    Private Const SP_AuthorizedSignerDelete As String = "spAuthorizedSignerDelete"

#End Region


    Public Function GetAuthorizedSigner(ByVal oCustomClass As Parameter.AuthorizedSigner) As Parameter.AuthorizedSigner
        Dim oReturnValue As New Parameter.AuthorizedSigner
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.Listdata = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, SP_AuthorizedSignerPaging, params).Tables(0)
            oReturnValue.TotalRecord = CType(params(4).Value, Int64)

            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function



    Public Function GetAuthorizedSignerList(ByVal ocustomClass As Parameter.AuthorizedSigner) As Parameter.AuthorizedSigner
        Dim reader As SqlDataReader
        Dim params(0) As SqlParameter
        Dim oReturnValue As New Parameter.AuthorizedSigner
        params(0) = New SqlParameter("@Id", SqlDbType.Char, 20)
        params(0).Value = ocustomClass.DocumentID
        Try
            reader = SqlHelper.ExecuteReader(ocustomClass.strConnection, CommandType.StoredProcedure, SP_AuthorizedSignerView, params)
            If reader.Read Then
                oReturnValue.DocumentID = ocustomClass.DocumentID
                oReturnValue.DocumentID = reader("DocumentID").ToString
                oReturnValue.Signer1 = reader("Signer1").ToString
                oReturnValue.Signer2 = reader("Signer2").ToString
                oReturnValue.Signer3 = reader("Signer3").ToString
                oReturnValue.JabatanSigner1 = reader("JabatanSigner1").ToString
                oReturnValue.JabatanSigner2 = reader("JabatanSigner2").ToString
                oReturnValue.JabatanSigner3 = reader("JabatanSigner3").ToString

            End If
            reader.Close()
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
            'Throw New Exception("Error On DataAccess.Setting.AuthorizedSigner.GetAuthorizedSignerList")
        End Try
    End Function

    Public Function AuthorizedSignerSaveAdd(ByVal ocustomClass As Parameter.AuthorizedSigner) As String

        Dim ErrMessage As String = ""
        Dim params(8) As SqlParameter

        params(0) = New SqlParameter("@DocumentID", SqlDbType.Char, 20)
        params(0).Value = ocustomClass.DocumentID
        params(1) = New SqlParameter("@Signer1", SqlDbType.VarChar, 50)
        params(1).Value = ocustomClass.Signer1
        params(2) = New SqlParameter("@Signer2", SqlDbType.VarChar, 50)
        params(2).Value = ocustomClass.Signer2
        params(3) = New SqlParameter("@Signer3", SqlDbType.VarChar, 50)
        params(3).Value = ocustomClass.Signer3
        params(4) = New SqlParameter("@JabatanSigner1", SqlDbType.VarChar, 50)
        params(4).Value = ocustomClass.JabatanSigner1
        params(5) = New SqlParameter("@JabatanSigner2", SqlDbType.VarChar, 50)
        params(5).Value = ocustomClass.JabatanSigner2
        params(6) = New SqlParameter("@JabatanSigner3", SqlDbType.VarChar, 50)
        params(6).Value = ocustomClass.JabatanSigner3
        params(7) = New SqlParameter("@BranchID", SqlDbType.NChar, 3)
        params(7).Value = ocustomClass.BranchId
        params(8) = New SqlParameter("@Err", SqlDbType.VarChar, 50)
        params(8).Direction = ParameterDirection.Output

        Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, SP_AuthorizedSignerSaveAdd, params)

            Return ""
        Catch ex As Exception
            Throw New Exception(ex.Message)
            'Throw New Exception("Error On DataAccess.Setting.AuthorizedSigner.AuthorizedSignerSaveEdit")
        End Try
    End Function



    Public Function AuthorizedSignerSaveEdit(ByVal ocustomClass As Parameter.AuthorizedSigner) As String

        Dim params(8) As SqlParameter

        params(0) = New SqlParameter("@DocumentID", SqlDbType.Char, 20)
        params(0).Value = ocustomClass.DocumentID
        params(1) = New SqlParameter("@Signer1", SqlDbType.VarChar, 50)
        params(1).Value = ocustomClass.Signer1
        params(2) = New SqlParameter("@Signer2", SqlDbType.VarChar, 50)
        params(2).Value = ocustomClass.Signer2
        params(3) = New SqlParameter("@Signer3", SqlDbType.VarChar, 50)
        params(3).Value = ocustomClass.Signer3
        params(4) = New SqlParameter("@JabatanSigner1", SqlDbType.VarChar, 50)
        params(4).Value = ocustomClass.JabatanSigner1
        params(5) = New SqlParameter("@JabatanSigner2", SqlDbType.VarChar, 50)
        params(5).Value = ocustomClass.JabatanSigner2
        params(6) = New SqlParameter("@JabatanSigner3", SqlDbType.VarChar, 50)
        params(6).Value = ocustomClass.JabatanSigner3
        params(7) = New SqlParameter("@BranchID", SqlDbType.NChar, 3)
        params(7).Value = ocustomClass.BranchId
        params(8) = New SqlParameter("@Err", SqlDbType.VarChar, 50)
        params(8).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, SP_AuthorizedSignerSaveEdit, params)

            Return ""
        Catch ex As Exception
            Throw New Exception(ex.Message)
            'Throw New Exception("Error On DataAccess.Setting.AuthorizedSigner.AuthorizedSignerSaveEdit")
        End Try
    End Function


    Public Function AuthorizedSignerDelete(ByVal ocustomClass As Parameter.AuthorizedSigner) As String
        Dim Err As Integer
        Dim params(1) As SqlParameter
        params(0) = New SqlParameter("@BranchID", SqlDbType.Char, 20)
        params(0).Value = ocustomClass.BranchId
        params(1) = New SqlParameter("@DocumentID", SqlDbType.Char, 20)
        params(1).Value = ocustomClass.DocumentID
        Try
            Err = SqlHelper.ExecuteNonQuery(ocustomClass.strConnection, CommandType.StoredProcedure, SP_AuthorizedSignerDelete, params)
            Return ""
        Catch ex As Exception
            Return "ERROR: Unable to delete the selected record. Record already used!"
            'Throw New Exception("Error On DataAccess.Setting.AuthorizedSigner.AuthorizedSignerDelete")
        End Try
    End Function

End Class
