﻿#Region "Imports"
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
#End Region

Public Class ExampleName : Inherits DataAccessBase
    Private Const spExampleNameMnt As String = "spExampleNameMnt"

    Public Function ListExampleName(ByVal customclass As Parameter.ExampleName) As Parameter.ExampleName
        Dim params() As SqlParameter = New SqlParameter(4) {}
        Try
            params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
            params(0).Value = customclass.CurrentPage

            params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
            params(1).Value = customclass.PageSize

            params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar, 1000)
            params(2).Value = customclass.WhereCond

            params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 100)
            params(3).Value = customclass.SortBy

            params(4) = New SqlParameter("@TotalRecords", SqlDbType.SmallInt)
            params(4).Direction = ParameterDirection.Output

            customclass.ListExampleName = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, spExampleNameMnt, params).Tables(0)
            customclass.TotalRecord = CInt(params(4).Value)
            Return customclass
        Catch ex As Exception
            WriteException("ExampleName", "ListExampleName", ex.Message + ex.StackTrace)
        End Try
    End Function

End Class
