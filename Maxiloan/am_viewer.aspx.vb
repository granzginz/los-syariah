﻿Imports System.Data.SqlClient
Imports Maxiloan.Controller

Public Class am_viewer
    Inherits Maxiloan.Webform.WebBased
    Private oController As New ReportingServiceController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Me.FormID = Request("formid")
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Dim oClass As New Parameter.CatalogRS

                With oClass
                    .strConnection = GetConnectionStringRS()
                    .FormID = Me.FormID
                End With

                Try
                    oClass = oController.getCatalogRS(oClass)
                    rsweb1.ServerReport.ReportPath = oClass.ReportPath
                    rsweb1.ServerReport.ReportServerUrl = New Uri(Me.VirtualDirectoryRS)
                Catch ex As Exception
                    lblMessage.Text = ex.Message
                End Try
            End If
        End If
    End Sub

End Class