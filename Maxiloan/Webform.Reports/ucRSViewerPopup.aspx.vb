﻿Imports Maxiloan.Webform.UserController

Public Class ucRSViewerPopup
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents ucRSViewer1 As ucRSViewer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not IsPostBack Then
            With ucRSViewer1
                .ReportPath = Request("rsname")
                .ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Remote
                .ReportID = Request("formid")
            End With
        End If
    End Sub

End Class