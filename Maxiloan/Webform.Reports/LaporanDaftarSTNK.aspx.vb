﻿Imports Microsoft.Reporting.WebForms
Imports Maxiloan.Webform.UserController

Public Class LaporanDaftarSTNK
    'Inherits System.Web.UI.Page
    Inherits Maxiloan.Webform.WebBased
    Public Property ReportPath As String
        Get
            Return CType(ViewState("ReportPath"), String)
        End Get
        Set(ByVal value As String)
            ViewState("ReportPath") = value
        End Set
    End Property

    Public Property ProcessingMode As ProcessingMode
        Get
            Return CType(ViewState("ProcessingMode"), ProcessingMode)
        End Get
        Set(ByVal value As ProcessingMode)
            ViewState("ProcessingMode") = value
        End Set
    End Property

    Public Property ReportID As String
        Get
            Return CType(ViewState("ReportID"), String)
        End Get
        Set(ByVal value As String)
            ViewState("ReportID") = value
        End Set
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then

            Dim irsc As IReportServerCredentials = New CustomReportCredentials(Me.UIDRS, PassRS, Me.ServerNameRS)


            With ReportViewer1


                .ProcessingMode = ProcessingMode.Remote
                .ServerReport.ReportServerCredentials = irsc
                .ServerReport.ReportServerUrl = New Uri(Me.VirtualDirectoryRS)
                .ServerReport.ReportPath = "/Maxiloan/CollateralMgt/DaftarBPKB"
                '.ShowParameterPrompts = False
                '.ShowRefreshButton = False
                '.ShowWaitControlCancelLink = False
                '.ShowBackButton = False
                '.ShowCredentialPrompts = False
                Dim parametersCollection = New List(Of ReportParameter)()

                Dim paramList As New List(Of ReportParameter)()

                paramList.Add(New ReportParameter("Cabang", Me.sesBranchId.Replace("'", ""), False))
             
                .ServerReport.SetParameters(paramList)
                .ShowToolBar = True
                .ShowReportBody = True
                .ShowParameterPrompts = False
                .ServerReport.Refresh()
            End With
        End If
    End Sub

End Class