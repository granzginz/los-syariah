﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ReportSetting.aspx.vb"
    Inherits="Maxiloan.Webform.Reports.ReportSetting" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ReportSetting</title>
    <link rel="Stylesheet" type="text/css" href="../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../Include/Buttons.css" />
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <link href="../include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../js/jquery-1.9.1.min.js"></script>
    <script src="../js/jquery-ui-1.10.3.custom.min.js"></script>
</head>
<body>
    <asp:UpdateProgress ID="upg1" runat="server" AssociatedUpdatePanelID="ChildControl">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="sm" runat="server" />
    <asp:UpdatePanel runat="server" ID="ChildControl">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
                onclick="hideMessage();"></asp:Label>
            <div runat="server" id="jlookupContent" />
         
    
         
            <asp:Panel ID="PnlGrid" runat="server">
                <div class="form_title">
                    <div class="form_single">
                        <h3>
                            DAFTAR REPORTS SETTING
                        </h3>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:GridView ID="GridView1" CssClass="grid_general" Width="100%" DataKeyNames="ReportGroupID"
                                AutoGenerateColumns="false" runat="server" HeaderStyle-CssClass="th" RowStyle-CssClass="item_grid"
                                OnRowEditing="EditGroupReport" OnRowUpdating="UpdateGroupReport"  
                                OnRowCancelingEdit="CancelEdit" GridLines="None" 
                                EmptyDataText="DATA KOSONG" ShowFooter="True">
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderStyle Width="10"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ImgBtn" ImageUrl="../Images/Plus.gif" CommandName="Expand" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ADD Report Detail">
                                        <HeaderStyle Width="150"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="left"></ItemStyle>
                                        <ItemTemplate>
                                            <a href='<%# GenerateLinkTo() %>?ReportGroupID=<%# DataBinder.eval(Container,"DataItem.ReportGroupID") %>&ReportGroupName=<%# DataBinder.eval(Container,"DataItem.ReportGroupName") %>
                                                                            &ID=0
                                                                             &ItemID=
                                                                             &Path=
                                                                             &Name=
                                                                             &Description=
                                                                             &IsBranch=true
                                                                            &activiti="ADD"'>ADD Report Detail
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField SortExpression="" HeaderText="Kode Report Group">
                                        <HeaderStyle Width="150"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="left"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblReportGroupID" runat="server" Text='<%#Container.DataItem("ReportGroupID")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtEditReportGroupID" runat="server" Width="90%" Text='<%#Container.DataItem("ReportGroupID")%>'
                                                Enabled="false"></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtReportGroupID" MaxLength="10" Width="90%" runat="server"></asp:TextBox>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField SortExpression="" HeaderText="Nama Report Group">
                                        <HeaderStyle Width="800"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="left"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblReportGroupName" runat="server" Text='<%#Container.DataItem("ReportGroupName")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtEditReportGroupName" runat="server" Width="90%" Text='<%#Container.DataItem("ReportGroupName")%>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtReportGroupName" MaxLength="200" Width="90%" runat="server"></asp:TextBox>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkRemove" runat="server" CommandArgument='<%#Container.DataItem("ReportGroupID")%>'
                                                OnClientClick="return confirm('Do you want to delete?')" Text="Delete" OnClick="DeleteReportGroup"></asp:LinkButton>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Button ID="btnAdd" runat="server" Text="Add" OnClick="AddNewGroupReport" CssClass="small buttongo blue" CommandName = "Footer"/>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:CommandField ShowEditButton="True" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:PlaceHolder ID="objPHOrderDetails" runat="server">
                                                <tr>
                                                    <td width="9" />
                                                    <td colspan="100%">
                                                        <div id="div<%# Eval("ReportGroupID") %>">
                                                            <asp:GridView ID="GridView2" runat="server" DataKeyNames="ReportGroupID" AutoGenerateColumns="false"
                                                                Width="100%">
                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="No." ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="Middle"
                                                                        HeaderStyle-HorizontalAlign="left" HeaderStyle-Width="20" ItemStyle-Width="20">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblID" Text='<%# Eval("ID") %>' runat="server" Visible="false"></asp:Label>
                                                                            <asp:Label ID="lblNomor" Text='<%# Eval("Nomor") %>' runat="server"></asp:Label>
                                                                            <asp:Label ID="lblItemID" Text='<%# Eval("ItemID") %>' runat="server" Visible="false"></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="URL" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="Middle"
                                                                        HeaderStyle-HorizontalAlign="left" HeaderStyle-Width="400" ItemStyle-Width="400">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblPath" Text='<%# Eval("Path") %>' runat="server"></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Nama" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="Middle"
                                                                        HeaderStyle-HorizontalAlign="left" HeaderStyle-Width="250" ItemStyle-Width="250">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblName" Text='<%# Eval("Name") %>' runat="server"></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Keterangan" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="Middle"
                                                                        HeaderStyle-HorizontalAlign="left" HeaderStyle-Width="400" ItemStyle-Width="400">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblDescription" Text='<%# Eval("Description") %>' runat="server"></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                       <asp:TemplateField HeaderText="Branch" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle"
                                                                        HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="50" ItemStyle-Width="50">
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="cbIsBranch" Checked='<%# Eval("IsBranch") %>' Enabled="false" runat="server" />   
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Edit" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle"
                                                                        HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="50" ItemStyle-Width="50">
                                                                        <ItemTemplate>
                                                                            <a href='<%# GenerateLinkTo() %>?ReportGroupID=<%# DataBinder.eval(Container,"DataItem.ReportGroupID") %>&ReportGroupName=<%# DataBinder.eval(Container,"DataItem.ReportGroupName") %>
                                                                            &ID=<%# DataBinder.eval(Container,"DataItem.ID") %>
                                                                             &ItemID=<%# DataBinder.eval(Container,"DataItem.ItemID") %>
                                                                             &Path=<%# DataBinder.eval(Container,"DataItem.Path") %>
                                                                             &Name=<%# DataBinder.eval(Container,"DataItem.Name") %>
                                                                             &Description=<%# DataBinder.eval(Container,"DataItem.Description") %>
                                                                             &IsBranch=<%# DataBinder.eval(Container,"DataItem.IsBranch") %>
                                                                            &activiti="EDIT"'>Edit
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Delete" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle"
                                                                        HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="50" ItemStyle-Width="50">
                                                                        <ItemTemplate>
                                                                            <asp:LinkButton ID="lnkRemoveDetail" runat="server" CommandArgument='<%#Container.DataItem("ID")%>'
                                                                                OnClientClick="return confirm('Do you want to delete?')" Text="Delete" OnClick="DeleteReportDetal"></asp:LinkButton>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                            </asp:PlaceHolder>
                                            </div> </td> </tr>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                              
    <EmptyDataTemplate>
        <tr>
            <th scope="col" style="width: 150px;">
                Kode Report Group
            </th>
            <th scope="col" style="width: 500px;">
                Nama Report Group
            </th>
         
        </tr>
        <tr>
            <td>
                <asp:TextBox ID="txtReportGroupID" MaxLength="10" Width="150px" runat="server" />
            </td>
            <td>
                <asp:TextBox ID="txtReportGroupName" MaxLength="200" Width="500px" runat="server" />
            </td>
         
            <td>
                <asp:Button ID="btnAdd" runat="server" Text="Add" OnClick="AddNewGroupReport" CssClass="small buttongo blue" CommandName = "EmptyDataTemplate" />
            </td>
        </tr>
    </EmptyDataTemplate>
                                <HeaderStyle CssClass="th" />
                                <RowStyle CssClass="item_grid" />
                            </asp:GridView>
                            <div class="button_gridnavigation">
                                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../Images/grid_navbutton01.png"
                                    CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="First">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../Images/grid_navbutton02.png"
                                    CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Prev">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../Images/grid_navbutton03.png"
                                    CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Next">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../Images/grid_navbutton04.png"
                                    CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Last">
                                </asp:ImageButton>
                                Page&nbsp;
                                <asp:TextBox ID="txtGoPage" runat="server" Width="34px">1</asp:TextBox>
                                <asp:Button ID="BtnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                                    EnableViewState="False"></asp:Button>
                                <asp:RangeValidator ID="rgvGo" runat="server" CssClass="validator_general" Type="Integer"
                                    MaximumValue="999999999" ErrorMessage="No Halaman Salah" MinimumValue="1" ControlToValidate="txtGopage"></asp:RangeValidator>
                                <asp:RequiredFieldValidator ID="rfvGo" runat="server" CssClass="validator_general"
                                    ErrorMessage="No Halaman Salah" ControlToValidate="txtGopage" Display="Dynamic"></asp:RequiredFieldValidator>
                            </div>
                            <div class="label_gridnavigation">
                                <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                                <asp:Label ID="lblTotRec" runat="server"></asp:Label>&nbsp;record(s)
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="GridView1" EventName="RowDataBound" />
            <asp:AsyncPostBackTrigger ControlID="imbFirstPage" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="imbPrevPage" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="imbNextPage" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="imbLastPage" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="BtnGoPage" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
    </form>
</body>
</html>
