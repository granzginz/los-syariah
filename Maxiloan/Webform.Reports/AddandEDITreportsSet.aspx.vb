﻿#Region "Imports"
Imports Maxiloan.Parameter

Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
Imports System.Data.SqlClient

#End Region

Public Class AddandEDITreportsSet
    Inherits Maxiloan.Webform.WebBased
#Region "Property"
    Property Activiti() As String
        Get
            Return ViewState("Activiti").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("Activiti") = Value
        End Set
    End Property

#End Region
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not Page.IsPostBack Then
            doBind()
        End If

    End Sub
    Public Sub doBind()

        Dim ReportGroupID As String = Request.QueryString("ReportGroupID").Trim
        Dim ReportGroupName As String = Request.QueryString("ReportGroupName").Trim
        Dim IsBranch As Boolean = CBool(Request.QueryString("IsBranch").Trim)
        Dim activiti As String = Request.QueryString("activiti").Trim
        Me.Activiti = Request.QueryString("activiti").Trim
        hdnReportGroupId.Value = ReportGroupID
        lblReportGroupID.Text = ReportGroupID.Trim.ToString
        lblReportGroupName.Text = ReportGroupName.Trim.ToString
        chbIsBranch.Checked = IsBranch
        If Request.QueryString("activiti").Trim = "ADD" Then
            hdnID.Value = "0"
            hdnItemID.Value = ""
            txtNameReport.Text = ""
            txtPath.Text = ""
            txtDescription.Text = ""
        Else
            Dim ID As String = Request.QueryString("ID").Trim
            Dim ItemID As String = Request.QueryString("ItemID").Trim
            Dim Path As String = Request.QueryString("Path").Trim
            Dim Name As String = Request.QueryString("Name").Trim
            Dim Description As String = Request.QueryString("Description").Trim
            hdnID.Value = ID
            hdnItemID.Value = ItemID.Trim.ToString
            txtNameReport.Text = Name.Trim.ToString
            txtPath.Text = Path.Trim.ToString
            txtDescription.Text = Description.Trim.ToString
        End If
    End Sub
    Protected Sub BtnSave_Click(sender As Object, e As EventArgs) Handles BtnSave.Click
        'If Page.IsValid Then
        Dim ID2 As Int64 = hdnID.Value.Trim
        Dim ReportGroupID2 As String = hdnReportGroupId.Value.Trim
        Dim ItemID2 As String = hdnItemID.Value.Trim
        Dim Path2 As String = txtPath.Text.Trim
        Dim Name2 As String = txtNameReport.Text.Trim
        Dim Description2 As String = txtDescription.Text.Trim
        Dim Branch2 As Boolean = chbIsBranch.Checked
        SaveDetailData(ID2, ReportGroupID2, ItemID2, Path2, Name2, Description2, Branch2)
        'End If
    End Sub
    Protected Sub BtnCancel_Click(sender As Object, e As EventArgs) Handles BtnCancel.Click
        Response.Redirect("ReportSetting.aspx")
    End Sub
    Protected Sub SaveDetailData(ByVal ID As Int64, ByVal ReportGroupID As String, ByVal ItemID As String,
                                ByVal Path As String, ByVal Name As String, ByVal Description As String, ByVal IsBranch As Boolean)

        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(GetConnectionString)

        Dim objReader As SqlDataReader

        Try


            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objCommand.Parameters.Clear()
            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.CommandText = "sp_TblReportSetting_ADDandUpdate"
            objCommand.Connection = objConnection

            objCommand.Parameters.Add("@ID", SqlDbType.Int).Value = ID
            objCommand.Parameters.Add("@ReportGroupID", SqlDbType.Char, 10).Value = ReportGroupID
            objCommand.Parameters.Add("@ItemID", SqlDbType.NVarChar, 425).Value = ItemID
            objCommand.Parameters.Add("@Path", SqlDbType.NVarChar, 425).Value = Path
            objCommand.Parameters.Add("@Name", SqlDbType.NVarChar, 425).Value = Name
            objCommand.Parameters.Add("@Description", SqlDbType.NVarChar, 512).Value = Description
            objCommand.Parameters.Add("@IsBranch", SqlDbType.Bit).Value = IsBranch



            objReader = objCommand.ExecuteReader

            objReader.Close()

        Catch ex As Exception
            Response.Write(ex.Message)
            ShowMessage(lblMessage, ex.Message, True)
        Finally
            objCommand.Dispose()
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
            Response.Redirect("ReportSetting.aspx")
        End Try

    End Sub
    
End Class