﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="LaporanDaftarSTNK.aspx.vb" Inherits="Maxiloan.Webform.Reports.LaporanDaftarSTNK" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>DaftarSTNK</title>
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        function showReports() {
            var element = document.getElementById("ReportViewer1_ctl09");

            if (element) {
                element.style.overflow = "visible";
            }
        }
    </script>
</head>
<body onload="showReports();">
    <form id="form1" runat="server">
   <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h4>
               LAPORAN DAFTAR STNK
                </h4>
        </div>
    </div> 
        <div runat="server" class="form_box">
        <div class="form_single">
             <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <rsweb:ReportViewer ID="ReportViewer1" runat="server" DocumentMapWidth="100%" Height="100%" ZoomMode="FullPage" Width="100%">
        </rsweb:ReportViewer>
        </div>
    </div>
    
    </form>
</body>
</html>
