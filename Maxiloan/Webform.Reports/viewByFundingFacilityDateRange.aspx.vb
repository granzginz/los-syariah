﻿Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.Parameter
Imports Microsoft.Reporting.WebForms
Imports System.Web.Services

Public Class viewByFundingFacilityDateRange
    Inherits WebBased

    Private m_controller As New DataUserControlController
    Private oAssetDataController As New AssetDataController
    Private oClass As New Parameter.ControlsRS
    Protected WithEvents ucfunding As ucFundingReport
    Protected WithEvents txtStartDate As ucDateCE
    Protected WithEvents txtEndDate As ucDateCE

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then Exit Sub

        If Page.IsPostBack Then

            Dim targetOfPostBack As String = Request.Params("__EVENTTARGET").ToString

            If targetOfPostBack = "btnView" Then

                Dim strReportFile As String = "../Webform.Reports/ucRSViewerPopup.aspx?formid=" & Request("formid") & "&rsname=" & Request("rsname")

                Response.Write("<script language = javascript>" & vbCrLf _
                & "var x = screen.width; " & vbCrLf _
                & "var y = screen.height; " & vbCrLf _
                & "window.open('" & strReportFile & "','_blank', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes') " & vbCrLf _
                & "</script>")
            End If
        End If

        If Not Page.IsPostBack Then
            Me.FormID = Request("formid")
            ucfunding.bindComboCompany()
            ucfunding.bindComboContract()
            ucfunding.bindComboBatch()
            ucfunding.IsKonsumen = False
            lblTitleReport.Text = GetTitleReports(FormID).ToString

            txtStartDate.IsRequired = True
            txtEndDate.IsRequired = True
        End If
    End Sub

    Private Function GetTitleReports(ReportsName As String) As String
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim strTitleReports As String        
        Dim customClassx As New GeneralPagingController
        Dim customClass As New Parameter.GeneralPaging

        With customClass
            .strConnection = GetConnectionString()
            .WhereCond = "FormID = '" & ReportsName & "' "
            .CurrentPage = 1
            .PageSize = 10
            .SortBy = ""
            .SpName = "spGetTitleReports"
        End With
        customClass = customClassx.GetGeneralPaging(customClass)

        dtsEntity = customClass.ListData
        dtvEntity = dtsEntity.DefaultView
        strTitleReports = dtvEntity(0)("TitleReportsName")
        Return strTitleReports
    End Function

    Private Sub btnView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnView.Click
        With oClass
            .FormID = Me.FormID
            .BankCompany = ucfunding.BankCompany
            .Kontrak = ucfunding.Kontrak
            .StartDate = ConvertDate2(txtStartDate.Text).ToString("MM/dd/yyyy")
            .EndDate = ConvertDate2(txtEndDate.Text).ToString("MM/dd/yyyy")
        End With

        Response.AppendCookie(RSConfig.setCookies(Request.Cookies("RSCOOKIES"), oClass))
    End Sub

End Class