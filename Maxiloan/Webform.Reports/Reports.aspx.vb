﻿Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.Parameter
Imports Microsoft.Reporting.WebForms

Public Class Reports
    Inherits Maxiloan.Webform.WebBased
    Private m_controller As New DataUserControlController
    Private oAssetDataController As New AssetDataController
    Private oClass As New Parameter.ControlsRS
    Protected WithEvents txtBulan As ucNumberFormat
    Protected WithEvents txtTahun As ucNumberFormat

#Region " Private Const "
    Dim m_Company As New FundingCompanyController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private lnkContractNo As LinkButton

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack Then

            'Dim strReportFile As String = "../Webform.Reports/ucRSViewerPopup.aspx?formid=" & Request("formid") & "&rsname=" & Request("rsname")

            'Response.Write("<script language = javascript>" & vbCrLf _
            '& "var x = screen.width; " & vbCrLf _
            '& "var y = screen.height; " & vbCrLf _
            '& "window.open('" & strReportFile & "','_blank', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes') " & vbCrLf _
            '& "</script>")
        End If

        If Not Page.IsPostBack Then
            Me.FormID = Request("formid")

            bindBranch()

            Dim WhereBranch As String = "BranchID in (" & selectedBranch() & ")"

            If Me.FormID = "LAPARDETAIL" Then
                bindListBoxCollectorType_CL()
                bindCollectorGrp()
            End If

            'If Me.FormID = "PDCJT" Then BindSearchBy()
            If Me.FormID = "STKOPNBPKB" Then bindStopOpname()
            If Me.FormID = "LAPARODCOLL" Or Me.FormID = "DPDCOLLFORCAST" Or Me.FormID = "LAPARODCOLLACT" Or Me.FormID = "DPDTIMSUS" Or Me.FormID = "DPDNS" Or Me.FormID = "OLDLOANBALANCE" Or Me.FormID = "LAPDPDBYCOLLECTOR" Or Me.FormID = "REALJUALCMO" Then bindAssetType()

            If Me.FormID = "HASILCRDSCOR" Or Me.FormID = "FUNAGRINSTJF" Then bindNoAplikasi()
            'If Me.FormID = "LAPHASILKUNCOLL" Then bindListBoxCollectorType_CL()
            If Me.FormID = "LAPHASILKUNCOLL" Or Me.FormID = "LAPANGSCOLL" Or Me.FormID = "LAPTAGIHANJT" Then bindListBoxCollectorType_CL()
            If Me.FormID = "LAPKEGDESKCOLL" Then bindCollectorType_D()
            ' If Me.FormID = "LAPTUNGGCA" Or Me.FormID = "DAFTRANREKTUNAI" Then FillCboEmp("BranchEmployee", cboCreditAnalist, WhereBranch + " and (EmployeePosition='CA' or EmployeePosition='AO') ")
            ' Perubahan Imron
            If Me.FormID = "LAPTUNGGCA" Then FillCboEmp("BranchEmployee", cboCreditAnalist, WhereBranch + " and (EmployeePosition='CA' or EmployeePosition='AO') ")
            If Me.FormID = "LAPTUNGGCMO" Then FillCboEmpReport("BranchEmployee", cboCMO, WhereBranch + " and (EmployeePosition='AO') ")
            If Me.FormID = "LAPTUNGGSVY" Then FillCboEmp("BranchEmployee", cboCMO, WhereBranch + " and (EmployeePosition='SVY' or EmployeePosition='AO') ")

            If Me.FormID = "DAFTRANREKBANK" Or Me.FormID = "DAFTRANREKBANKDETAIL" Or Me.FormID = "PEMTRANREKBANKDETAIL" Or Me.FormID = "DAFTRANREKBANKALL" Or Me.FormID = "LAPTRANSUSP" Or Me.FormID = "LAPOUSTSUSP" Or Me.FormID = "LAPALOKSUSP" Or Me.FormID = "DAFPEMANGBANK" Then bindrekBank("B")
            If Me.FormID = "DAFTRANREKTUNAI" Then bindrekBank("C")

            'If Me.FormID = "DAFTRANREKTUNAI" Then bindrekBank("B")

            If Me.FormID = "LAPTRANHARIKASIR" Then bindBankAccountbyType("B")
            'If Me.FormID = "LAPTRANHARIKASIR" Or Me.FormID = "DAFTRANREKTUNAI" Or Me.FormID = "LAPANGSTUNAI" Then bindKasir()
            ' Perubahan Imron
            ' If Me.FormID = "LAPTRANHARIKASIR" Or Me.FormID = "LAPANGSTUNAI" Then bindKasir()
            If Me.FormID = "LAPANGSTUNAI" Then bindKasir()
            If Me.FormID = "LAPTRANHARIKASIR" Then bindKasir()
            If Me.FormID = "LAPTRANKASBANK" Then bindBankAccount()
            If Me.FormID = "DAFJAMTPPOLIS" Or Me.FormID = "ANGSBANKDTL" Or Me.FormID = "FUTUREAMOUNT" Or Me.FormID = "DAFJAMTPBPKB" Then bindComboCompany()
            If Me.FormID = "KOJATEM" Or Me.FormID = "DAFJAMTPPOLIS""" Then bindComboCompany()
            If Me.FormID = "TANDATERIMAJAM" Or Me.FormID = "DAFANGSBANKBATCH" Or Me.FormID = "FUNBATCHINSTJF" Or Me.FormID = "FUNBATCHINSTTL" Then
                bindComboCompany()
                bindComboContract()
                bindComboBatch()
            End If
            If Me.FormID = "DAFANGSBANKKONS" Then
                bindComboCompany()
                bindComboContract()
                bindComboBatch()
                bindNamaKonsumen()
            End If
            If Me.FormID = "LAPCREDITDELIQ" Then
                bindListBoxCollectorType_CL()
            End If
            If Me.FormID = "DAFTERIMAPOLIS" Or Me.FormID = "DAFTANGAS" Then
                bindChkInsuranceCompany()
                'bindcboInsuranceCompany()

            End If

            If Me.FormID = "LAPHUTANGAS" Then bindChkInsuranceCompany()
            If IsHoBranch Then
                If Me.FormID = "LAPTAGAS" Then bindChkInsuranceCompanyHo()

            Else
                If Me.FormID = "LAPTAGAS" Then bindChkInsuranceCompany()
            End If


            'If IsHoBranch Then
            '    If Me.FormID = "DPDFORCASH" Then ChkAll.Visible = False
            'End If
            'If Me.FormID = "LAPTAGAS" Then bindChkInsuranceCompany()
            'If Me.FormID = "DAFJURNALDTL" Or Me.FormID = "DAFJURNALSUM" Then bindTransactionType()
            'If Me.FormID = "REKRENCANABAYAR" Then bindComboAPType()
            If Me.FormID = "REKRENCANABAYARLAIN" Then bindComboAPType()
            If Me.FormID = "HITUNGDENDA" Then bindNoKontrak()
            If Me.FormID = "R_ANALISASCORES" Or Me.FormID = "R_PRODUKPASSENGER" Or Me.FormID = "R_ANALISADOWNPAY" Or Me.FormID = "R_ANALISATENOR" Or Me.FormID = "R_ANALISAKONDISI" Or Me.FormID = "R_ANALISAUSIA" Or Me.FormID = "R_ANALISAMERK" Or Me.FormID = "R_ANALISAPENDAPATAN" Or Me.FormID = "R_ANALISAKATEGORI" Or Me.FormID = "R_ANALISNK" Or Me.FormID = "R_ANALISCOMMERCIAL" Or Me.FormID = "R_ANALISPOKOK" Or Me.FormID = "R_ANALISANGSURAN" Or Me.FormID = "R_ANALISAUNITCAB" Or Me.FormID = "R_ANALISAPRODCAB" Or Me.FormID = "R_ANALISACMO" Or Me.FormID = "R_ANALISADEALER" Then BindBulanTahun()
            If Me.FormID = "R_ANALISARATEWIL" Then bindArea()
            If Me.FormID = "DAFTRIALBAL" Then
                cboMonth.Enabled = True
                txtYear.Text = Me.BusinessDate.Date.Year.ToString

            End If

            oClass.isHOBRanch = Me.IsHoBranch
            oClass.FormID = Me.FormID

            lblTitleReport.Text = GetTitleReports(FormID).ToString

            oClass = RSConfig.setControls(oClass)

            With oClass

                'dvBranch.Visible = .isFilterMultiBranch
                ' dvBranchAll.Visible = .isFilterBranchAll
                dvArea.Visible = .isFilterArea
                dvBulanTahun.Visible = .isFilterBulanTahun
                dvBranch.Visible = .isFilterBranch
                dvDateRange.Visible = .isFilterRangeDate
                dvTglBayar.Visible = .isFilterTglBayar
                dvSingleDate.Visible = .isFilterSingleDate
                dvKondisiSKT.Visible = .isFilterKondisiSKT
                dvPeriodLapARODStatCMO.Visible = .isFilterPeriodLapARODStatCMO
                dvLmNunggak.Visible = .isFilterLmNunggak
                dvCollectorCLAll.Visible = .isFilterCollectorCLALL
                dvCollectorCL.Visible = .isFilterCollectorCL
                dvCollectorGrp.Visible = .isFilterCollectorGrp
                dvCollectorDAll.Visible = .isFilterCollectorDALL
                dvCollectorD.Visible = .isFilterCollectorD
                dvStatusAsset.Visible = .isFilterStatus
                dvCreditAnalist.Visible = .isFilterCreditAnalist
                dvCMO.Visible = .isFilterCMO
                dvSurveyor.Visible = .isFilterSurveyor

                dvRekBankType_B.Visible = .isFilterRekeningBankType_B
                dvRekBankType_C.Visible = .isFilterRekeningBankType_C
                dvBankAccount.Visible = .isFilterRekeningBankAccount
                dvChkColl_CL.Visible = .isFilterChkColl_CL
                dvChkAssetType.Visible = .isFilterChkAssetType
                dvKasir.Visible = .isFilterKasir
                dvBankCompany.Visible = .isFilterBankCompany
                dvKontrak.Visible = .isFilterKontrak
                dvNoBatch.Visible = .isFilterNoBatch
                dvNamaKonsumen.Visible = .isFilterNamaKonsumen
                dvchkInsuranceCompany.Visible = .isFilterCabangAsuransi
                dvStatusPolis.Visible = .isFilterStatusPolis
                dvYear.Visible = .isFilterYear
                dvTransactionType.Visible = .isFilterTransactionType
                dvNoKontrak.Visible = .isFilterNoKontrak
                dvMonth.Visible = .isFilterMonth
                dvPilihan.Visible = .isFilterPilihan
                dvInvoice.Visible = .isFilterInvoice
                dvNoAplikasi.Visible = .isFilterNoAplikasi
                'dvInsuranceCompany.Visible = .isFilterCabangAsuransi
                dvDateRange1.Visible = .isFilterRangeDate1
                dvRekBank.Visible = .isFilterRekBank
                DivDateTimeRange.Visible = .isFilterSingleDateTime
                dvAPType.Visible = .isFilterAPType
                'cboStatusBPKB.Visible = .isFilterStatusBPKB
                dvStockOpname.Visible = .isFilterStockOpname
                dvPilihTypeTanggal.Visible = .isFilterTypeTanggal
                If Me.FormID = "PDCJT" Then
                    dvsearchby.Visible = .isSearchBy
                End If
            End With

        End If

    End Sub

    Private Sub BindBulanTahun()
        txtBulan.Text = "0"
        txtTahun.Text = Me.BusinessDate.Date.Year.ToString
        With txtBulan
            .RequiredFieldValidatorEnable = True
            .RangeValidatorEnable = True
            .RangeValidatorMinimumValue = "0"
            .RangeValidatorMaximumValue = "12"
        End With
        With txtTahun
            .RequiredFieldValidatorEnable = True
            .RangeValidatorEnable = True
            .RangeValidatorMinimumValue = "0"
            .RangeValidatorMaximumValue = "9999"
        End With
    End Sub
    Private Sub bindBranch()

        Dim DtBranchName As New DataTable
        DtBranchName = CType(Me.Cache.Item(CACHE_BRANCH_ALL), DataTable)
        If DtBranchName Is Nothing Then
            Dim DtBranchNameCache As New DataTable
            DtBranchNameCache = m_controller.GetBranchAll(GetConnectionString)
            Me.Cache.Insert(CACHE_BRANCH_ALL, DtBranchNameCache, Nothing, DateTime.Now.AddHours(DEFAULT_TIME_DURATION_CACHE_HOUR), TimeSpan.Zero)
            DtBranchName = CType(Me.Cache.Item(CACHE_BRANCH_ALL), DataTable)
        End If

        With chkBranch
            .DataTextField = "Name"
            .DataValueField = "ID"
            .DataSource = DtBranchName
            .DataBind()

        End With
    End Sub
    Private Sub bindrekBank(ByVal strType As String)
        'Dim DtrekBank As New DataTable
        'DtrekBank = CType(Me.Cache.Item("DtrekBank"), DataTable)
        'If DtrekBank Is Nothing Then
        Dim DtrekBankCache As New DataTable
        DtrekBankCache = m_controller.GetBankAccount(GetConnectionString, selectedBranch, strType, "")
        'Me.Cache.Insert("DtrekBank", DtrekBankCache, Nothing, DateTime.Now.AddHours(DEFAULT_TIME_DURATION_CACHE_HOUR), TimeSpan.Zero)
        'DtrekBank = CType(Me.Cache.Item("DtrekBank"), DataTable)
        'End If

        With CkBoxListRekBank
            .DataTextField = "Name"
            .DataValueField = "ID"
            .DataSource = DtrekBankCache.DefaultView
            .DataBind()
        End With

    End Sub

    Private Sub bindBankAccount()
        'spGetReportBankAccount
        'spGetReportDafTranRekBank()

        cboBankAccount.DataSource = m_controller.GetBankAccountbranch(GetConnectionString(), selectedBranch())
        cboBankAccount.DataTextField = "Name"
        cboBankAccount.DataValueField = "ID"
        cboBankAccount.DataBind()
        cboBankAccount.Items.Insert(0, "Select One")
        cboBankAccount.Items(0).Value = "0"
    End Sub

    Private Sub bindArea()
        Dim DtAreaName As New DataTable
        DtAreaName = CType(Me.Cache.Item("AreaAll"), DataTable)
        If DtAreaName Is Nothing Then
            Dim m_Doc As New DocReceiveController
            Dim oData As New DataTable
            Dim oDoc As New DocRec
            oDoc.strConnection = GetConnectionString()
            oDoc.WhereCond = ""
            oDoc.SpName = "spGetAreaAll"
            oDoc = m_Doc.GetSPReport(oDoc)
            oData = oDoc.ListDataReport.Tables(0)

            Me.Cache.Insert("AreaAll", oData, Nothing, DateTime.Now.AddHours(DEFAULT_TIME_DURATION_CACHE_HOUR), TimeSpan.Zero)
            DtAreaName = CType(Me.Cache.Item("AreaAll"), DataTable)
        End If

        With chkArea
            .DataTextField = "Name"
            .DataValueField = "ID"
            .DataSource = DtAreaName
            .DataBind()
        End With


    End Sub

    'Private Sub bindChkColl()
    '    Dim oClass As New Parameter.ControlsRS
    '    With oClass
    '        .strConnection = GetConnectionString()
    '        .BranchId = selectedBranch()
    '    End With
    '    oClass = m_controller.GetCollectorReportLapHasilKunColl(oClass)
    '    Dim dt As DataTable
    '    dt = oClass.ListData

    '    With chkStatus
    '        .DataTextField = "NamaCollector"
    '        .DataValueField = "CollectorID"
    '        .DataSource = oClass
    '        .DataBind()
    '    End With
    'End Sub

    ' GetInsuranceCompanyBranchALL
    Private Sub bindChkInsuranceCompany()
        Dim oClass As New Parameter.ControlsRS
        With oClass
            .strConnection = GetConnectionString()
            .BranchId = selectedBranch()
        End With
        oClass = m_controller.GetInsuranceCompanyBranchALL(oClass)
        Dim dt As DataTable
        dt = oClass.ListData

        With chkInsuranceCompany
            .DataTextField = "Name"
            .DataValueField = "ChildID"
            .DataSource = dt
            .DataBind()
        End With
    End Sub

    Private Sub bindChkInsuranceCompanyHo()

        Dim strID As New StringBuilder
        Dim customClassx As New GeneralPagingController
        Dim customClass As New Parameter.GeneralPaging

        With customClass
            .strConnection = GetConnectionString()
            .WhereCond = strID.ToString()
            .CurrentPage = 1
            .PageSize = 1
            .SortBy = ""
            .SpName = "spReportGetInsuranceHO"
        End With
        customClass = customClassx.GetGeneralPaging(customClass)

        With chkInsuranceCompany
            .DataTextField = "Name"
            .DataValueField = "ChildID"
            .DataSource = customClass.ListData.DefaultView()
            .DataBind()
        End With
    End Sub
    'Private Sub bindcboInsuranceCompany()
    '    Dim oClass As New Parameter.ControlsRS
    '    With oClass
    '        .strConnection = GetConnectionString()
    '        .BranchId = selectedBranch()
    '    End With
    '    oClass = m_controller.GetInsuranceCompanyBranchALL(oClass)
    '    Dim dt As DataTable
    '    dt = oClass.ListData

    '    With cboInsuranceCompany
    '        .DataTextField = "Name"
    '        .DataValueField = "ChildID"
    '        .DataSource = dt
    '        .DataBind()
    '    End With
    'End Sub


    'Private Sub bindCollectorType_CL()

    '    Dim oClass As New Parameter.ControlsRS
    '    With oClass
    '        .strConnection = GetConnectionString()
    '        .BranchId = selectedBranch()
    '    End With

    '    oClass = m_controller.GetCollectorReportLapHasilKunColl(oClass)
    '    Dim dt As New DataTable
    '    dt = oClass.ListData

    '    cboCollectorCL.DataSource = dt
    '    cboCollectorCL.DataTextField = "NamaCollector"
    '    cboCollectorCL.DataValueField = "CollectorID"
    '    cboCollectorCL.DataBind()
    '    cboCollectorCL.Items.Insert(0, "Select One")
    '    cboCollectorCL.Items(0).Value = "0"

    'End Sub

    Private Sub bindListBoxCollectorType_CL()

        Dim oClass As New Parameter.ControlsRS
        With oClass
            .strConnection = GetConnectionString()
            .BranchId = selectedBranch()
        End With

        'Dim DtrekBankCache As New DataTable
        'DtrekBankCache = m_controller.GetBankAccount(GetConnectionString, selectedBranch, strType, "")

        oClass = m_controller.GetCollectorReportLapHasilKunColl(oClass)
        Dim dt As New DataTable
        dt = oClass.ListData

        With ckbCollectorCL
            .DataTextField = "NamaCollector"
            .DataValueField = "CollectorID"
            .DataSource = dt
            .DataBind()
        End With

    End Sub
    Private Sub bindCollectorGrp()
        Dim strID As New StringBuilder

        'For Each oItem As ListItem In chkBranch.Items
        '    If oItem.Selected Then
        '        strID.Append(IIf(strID.ToString = "", "'" & oItem.Value & "'", ",'" & oItem.Value & "'"))
        '    End If
        'Next

        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim customClassx As New GeneralPagingController
        Dim customClass As New Parameter.GeneralPaging

        With customClass
            .strConnection = GetConnectionString()
            .WhereCond = selectedBranch()
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = ""
            .SpName = "spReportCollectorGrp"
        End With
        customClass = customClassx.GetGeneralPaging(customClass)

        dtsEntity = customClass.ListData
        dtvEntity = dtsEntity.DefaultView
        cboCollectorGrp.DataValueField = "ID"
        cboCollectorGrp.DataTextField = "Name"
        cboCollectorGrp.DataSource = dtvEntity
        cboCollectorGrp.DataBind()
    End Sub
    Private Sub bindCollectorType_D()

        Dim oClass As New Parameter.ControlsRS

        With oClass
            .strConnection = GetConnectionString()
            .BranchId = selectedBranch()
        End With
        'spGetCollectorReportLapKegDeskColl
        oClass = m_controller.GetCollectorReportLapKegDeskColl(oClass)

        Dim dt As New DataTable
        dt = oClass.ListData

        cboCollectorD.DataSource = dt
        cboCollectorD.DataTextField = "NamaCollector"
        cboCollectorD.DataValueField = "CollectorID"
        cboCollectorD.DataBind()
        'cboCollectorD.Items.Insert(0, "Select One")
        'cboCollectorD.Items(0).Value = "0"

    End Sub

    '[spGetReportEmployeeLapTunggCMO]
    Protected Sub FillCboEmp(ByVal Table As String, ByVal cboName As DropDownList, ByVal Where As String)
        Dim oAssetData As New Parameter.AssetData
        Dim oData As New DataTable

        oAssetData.strConnection = GetConnectionString()
        oAssetData.Where = Where
        oAssetData.Table = Table
        oAssetData = oAssetDataController.GetCboEmp(oAssetData)

        oData = oAssetData.ListData
        cboName.DataSource = oData
        cboName.DataTextField = "Name"
        cboName.DataValueField = "ID"
        cboName.DataBind()
        cboName.Items.Insert(0, "Select One")
        cboName.Items(0).Value = "Select One"
    End Sub

    Protected Sub FillCboEmpReport(ByVal Table As String, ByVal cboName As DropDownList, ByVal Where As String)
        Dim oAssetData As New Parameter.AssetData
        Dim oData As New DataTable

        oAssetData.strConnection = GetConnectionString()
        oAssetData.Where = Where
        oAssetData.Table = Table
        oAssetData = oAssetDataController.GetCboEmpReport(oAssetData)

        oData = oAssetData.ListData
        cboName.DataSource = oData
        cboName.DataTextField = "Name"
        cboName.DataValueField = "ID"
        cboName.DataBind()
        cboName.Items.Insert(0, "Select One")
        cboName.Items(0).Value = "Select One"
        'cboRekBankType_B.Items.Insert(0, "Select One")
        'cboRekBankType_B.Items(0).Value = "0"


    End Sub

    Private Sub bindBankAccountbyType(ByVal strType As String)
        'spGetReportDafTranRekBank()       
        cboRekBankType_B.DataSource = m_controller.GetBankAccount(GetConnectionString, selectedBranch, strType, "")
        cboRekBankType_B.DataTextField = "Name"
        cboRekBankType_B.DataValueField = "ID"
        cboRekBankType_B.DataBind()
        cboRekBankType_B.Items.Insert(0, "Select One")
        cboRekBankType_B.Items(0).Value = "0"
    End Sub

    Private Sub bindKasir()
        Dim oClass As New Parameter.ControlsRS
        With oClass
            .strConnection = GetConnectionString()
            .BranchId = selectedBranch()
        End With
        'spGetReportDafTranRekBank()
        oClass = m_controller.GetReportKasir(oClass)
        Dim dt As New DataTable
        dt = oClass.ListData

        cboRekBankType_C.DataSource = dt
        cboRekBankType_C.DataTextField = "EmployeeName"
        cboRekBankType_C.DataValueField = "LoginId"
        cboRekBankType_C.DataBind()
        cboRekBankType_C.Items.Insert(0, "")
        cboRekBankType_C.Items(0).Value = "0"

        cboKasir.DataSource = dt
        cboKasir.DataTextField = "EmployeeName"
        cboKasir.DataValueField = "LoginId"
        cboKasir.DataBind()
        cboKasir.Items.Insert(0, "")
        cboKasir.Items(0).Value = "0"
    End Sub

    Private Sub bindComboCompany()
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim customClassx As New GeneralPagingController
        Dim customClass As New Parameter.GeneralPaging

        With customClass
            .strConnection = GetConnectionString()
            .WhereCond = ""
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = ""
            .SpName = "spFundingGetComboCompany"
        End With
        customClass = customClassx.GetGeneralPaging(customClass)

        dtsEntity = customClass.ListData
        dtvEntity = dtsEntity.DefaultView
        cboBankCompany.DataValueField = "FundingCoyId"
        cboBankCompany.DataTextField = "FundingCoyName"
        cboBankCompany.DataSource = dtvEntity
        cboBankCompany.DataBind()
    End Sub

    Private Sub bindComboAPType()

        Dim customClassx As New GeneralPagingController
        Dim customClass As New Parameter.GeneralPaging

        With customClass
            .strConnection = GetConnectionString()
            .WhereCond = ""
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = ""
            .SpName = "spTblAPType"
        End With
        customClass = customClassx.GetGeneralPaging(customClass)
        Dim dt As DataTable
        dt = customClass.ListData
        With cboAPType
            '.DataTextField = "DescriptioncboCMO"
            .DataTextField = "Description"
            .DataValueField = "ID"
            .DataSource = dt
            .DataBind()
        End With
    End Sub

    Private Function GetTitleReports(ReportsName As String) As String
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim strTitleReports As String
        Dim a As String
        Dim customClassx As New GeneralPagingController
        Dim customClass As New Parameter.GeneralPaging

        With customClass
            .strConnection = GetConnectionString()
            .WhereCond = "FormID = '" & ReportsName & "' "
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = ""
            .SpName = "spGetTitleReports"
        End With
        customClass = customClassx.GetGeneralPaging(customClass)

        dtsEntity = customClass.ListData
        dtvEntity = dtsEntity.DefaultView
        strTitleReports = dtvEntity(0)("TitleReportsName")
        Return strTitleReports
    End Function

    Sub bindComboContract()
        Dim oClass As New Parameter.ControlsRS
        With oClass
            .strConnection = GetConnectionString()
            .CoyID = cboBankCompany.SelectedValue
        End With
        oClass = m_controller.GetReportComboContract(oClass)
        Dim dt As New DataTable
        dt = oClass.ListData
        cboKontrak.DataSource = dt
        cboKontrak.DataTextField = "NamaKontrak"
        cboKontrak.DataValueField = "FundingContractNo"
        cboKontrak.DataBind()
    End Sub

    Sub bindComboBatch()
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim customClassx As New GeneralPagingController
        Dim customClass As New Parameter.GeneralPaging
        With customClass
            .strConnection = GetConnectionString()
            .WhereCond = "WHERE fundingCoyID = '" & cboBankCompany.SelectedValue & "' AND FundingContractNo = '" & cboKontrak.SelectedValue & "' "
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = ""
            .SpName = "spGetReportComboBatch"
        End With

        customClass = customClassx.GetGeneralPaging(customClass)

        dtsEntity = customClass.ListData
        dtvEntity = dtsEntity.DefaultView
        cboNoBatch.DataValueField = "FundingBatchNo"
        cboNoBatch.DataTextField = "FundingBatchNo"
        cboNoBatch.DataSource = dtvEntity
        cboNoBatch.DataBind()
        'cboNoBatch.Items.Insert(0, "All")
        'cboNoBatch.Items(0).Value = "All"
    End Sub
    Sub bindNamaKonsumen()
        Dim oClass As New Parameter.ControlsRS
        With oClass
            .strConnection = GetConnectionString()
            .Kontrak = cboKontrak.SelectedValue
            .NoBatch = cboNoBatch.SelectedValue
        End With
        oClass = m_controller.GetReportNamaKonsumen(oClass)  'lagi tes

        Dim dt As DataTable
        dt = oClass.ListData
        With cboNamaKonsumen
            .DataSource = dt
            .DataTextField = "NamaKonsumen"
            .DataValueField = "ApplicationID"
            .DataBind()
        End With
    End Sub

    Private Sub bindTransactionType()
        Dim oClass As New Parameter.ControlsRS
        With oClass
            .strConnection = GetConnectionString()

        End With
        oClass = m_controller.GetReportTransactionType(oClass)
        Dim dt As DataTable
        dt = oClass.ListData

        With chkTransactionType
            .DataTextField = "TransactionType"
            .DataValueField = "transactionid"
            .DataSource = dt
            .DataBind()
        End With
    End Sub

    'bind AgreementNo
    Private Sub bindNoKontrak()
        Dim oClass As New Parameter.ControlsRS
        With oClass
            .strConnection = GetConnectionString()
            .BranchId = selectedBranch()
        End With
        oClass = m_controller.GetReportNoKontrak(oClass)

        Dim dt As DataTable
        dt = oClass.ListData

        With cboNoKontrak
            .DataSource = dt
            .DataTextField = "NoKontrak"
            .DataValueField = "ApplicationID"
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = "0"
        End With

    End Sub
    Private Sub bindNoAplikasi()

        Dim oClass As New Parameter.ControlsRS
        With oClass
            .strConnection = GetConnectionString()
            .BranchId = Me.sesBranchId.Replace("'", "")
        End With
        oClass = m_controller.GetReportNoAplikasi(oClass)
        Dim dt As New DataTable
        dt = oClass.ListData
        With cboNoAplikasi
            .DataSource = dt
            .DataTextField = "NoNama"
            .DataValueField = "ApplicationID"
            .DataBind()
        End With
    End Sub
    Private Sub bindStopOpname()
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim customClassx As New GeneralPagingController
        Dim customClass As New Parameter.GeneralPaging

        With customClass
            .strConnection = GetConnectionString()
            .WhereCond = ""
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = ""
            .SpName = "spReportGetStockOpname"
        End With
        customClass = customClassx.GetGeneralPaging(customClass)

        dtsEntity = customClass.ListData
        dtvEntity = dtsEntity.DefaultView
        cboStockOpname.DataValueField = "ID"
        cboStockOpname.DataTextField = "Name"
        cboStockOpname.DataSource = dtvEntity
        cboStockOpname.DataBind()
    End Sub
    Private Sub bindAssetType()
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim customClassx As New GeneralPagingController
        Dim customClass As New Parameter.GeneralPaging

        With customClass
            .strConnection = GetConnectionString()
            .WhereCond = ""
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = ""
            .SpName = "spReportGetAssetType"
        End With
        customClass = customClassx.GetGeneralPaging(customClass)

        dtsEntity = customClass.ListData
        dtvEntity = dtsEntity.DefaultView
        chkAssetTypeID.DataValueField = "ID"
        chkAssetTypeID.DataTextField = "Name"
        chkAssetTypeID.DataSource = dtvEntity
        chkAssetTypeID.DataBind()
        'chkAssetTypeID.Items.Insert(0, "All")
        'chkAssetTypeID.Items(0).Value = "All"

    End Sub

    Private Sub btnView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnView.Click
        Me.SearchBy = ""
        If Me.FormID = "PDCJT" Then BindSearchBy()
        'If oSearchBy.Text.Trim <> "" Then
        '    Me.SearchBy = Me.SearchBy & " and " & oSearchBy.ValueID & " like '%" & oSearchBy.Text.Trim.Replace("'", "''").Replace("%", "") & "%'"
        'End If

        With oClass
            .SelectedBranch = selectedBranch()
            .CabangAsuransi = selectedInsuranceCompany()
            .RekBank = selectedRekBank()
            .TransID = selecteJenisTransaksi()
            .CollectorType_CL = selectedCollector()
            If txtStartDate.Text.Trim <> "" Then
                .StartDate = ConvertDate2(txtStartDate.Text).ToString("MM/dd/yyyy")
            End If
            If txtEndDate.Text.Trim <> "" Then
                .EndDate = ConvertDate2(txtEndDate.Text).ToString("MM/dd/yyyy")
            End If

            If txtStartDate1.Text.Trim <> "" Then
                .StartDate1 = ConvertDate2(txtStartDate1.Text).ToString("MM/dd/yyyy")
            End If
            If txtEndDate1.Text.Trim <> "" Then
                .EndDate1 = ConvertDate2(txtEndDate1.Text).ToString("MM/dd/yyyy")
            End If

            If txtStartDateTime.Text.Trim <> "" Then
                .StartDateTime = ConvertDate2(txtStartDateTime.Text).ToString("MM/dd/yyyy")
            End If
            If txtEndDateTime.Text.Trim <> "" Then
                .EndDateTime = ConvertDate2(txtEndDateTime.Text).ToString("MM/dd/yyyy")
            End If

            If txtSingleDate.Text.Trim <> "" Then
                .SingleDate = ConvertDate2(txtSingleDate.Text).ToString("MM/dd/yyyy")
            End If

            If txtTglBayar.Text.Trim <> "" Then
                .TglBayar = ConvertDate2(txtTglBayar.Text).ToString("MM/dd/yyyy")
            End If
            .KondisiSKT = cboKondisiSKT.SelectedValue
            .StartMonth = cboStartMonth.SelectedValue
            .EndMonth = cboEndMonth.SelectedValue
            .StartYear = txtStartYear.Text
            .EndYear = txtEndYear.Text
            .StartYear2 = txtlnstart.Text
            .EndYear2 = txtlnend.Text
            '.CollectorType_D = cboCollectorD.SelectedValue
            .CollectorType_D = selectedCollectorD()
            .CollectorType_Grp = cboCollectorGrp.SelectedValue
            '.CollectorType_CL = cboCollectorCL.SelectedValue
            .JenisTask = cboJenisTask.SelectedValue
            '.StatusRepo = selectedStatusRepo()
            .StatusRepo = chkStatusAsset.SelectedValue
            .CreditAnalist = cboCreditAnalist.SelectedValue
            .CMO = cboCMO.SelectedValue.Replace(" ", "")
            .Surveyor = cboSurveyor.SelectedValue
            '.StopOpname = chkStopOpname.SelectedValue
            .RekeningBankkType_B = cboRekBankType_B.SelectedValue
            .RekeningBankkType_C = cboRekBankType_C.SelectedValue
            .ChkColl_CL = chkColl.SelectedValue

            ''AssetType
            '.ChkAssetType = chkAssetTypeID.SelectedItem.Value
            .ChkAssetType = selectedAssetType()
            .Kasir = cboKasir.SelectedValue
            '.Kasir = selectedKasir()
            selectedKasir()
            selectedBankAccount()
            '.RekeningBankAccount = cboBankAccount.SelectedValue
            .RekeningBankAccount = selectedBankAccount()
            .BankCompany = cboBankCompany.SelectedValue
            .Kontrak = cboKontrak.SelectedValue
            .NoBatch = cboNoBatch.SelectedValue
            .NamaKonsumen = cboNamaKonsumen.SelectedValue

            .StatusPolis = cboStatusPolis.SelectedValue
            .Year = txtYear.Text
            .Month = cboMonth.SelectedValue
            .Invoice = CStr(IIf(txtInvoice.Text.Trim = "", " ", txtInvoice.Text))
            '.TransactionType = chkTransactionType.SelectedValue
            .NoKontrak = cboNoKontrak.SelectedValue.Replace(" ", "")
            .Pilihan = chkPilihan.SelectedValue
            .NoAplikasi = cboNoAplikasi.SelectedValue
            .Bulan = CInt(txtBulan.Text)
            .Tahun = CInt(txtTahun.Text)
            .AreaID = chkArea.SelectedValue.Trim
            .StartTime = txtStartTime.Text
            .EndTime = txtEndTime.Text
            .ApType = cboAPType.SelectedValue
            '.StatusBPKB = cboStatusBPKB.SelectedValue.Trim
            .StockOpname = cboStockOpname.SelectedValue
            .TypeTanggal = chkPilihTypeTanggal.SelectedValue
            .SearchBy = Me.SearchBy
        End With

        Response.AppendCookie(RSConfig.setCookies(Request.Cookies("RSCOOKIES"), oClass))

        Dim strReportFile As String = "../Webform.Reports/ucRSViewerPopup.aspx?formid=" & Request("formid") & "&rsname=" & Request("rsname")

        Response.Write("<script language = javascript>" & vbCrLf _
        & "var x = screen.width; " & vbCrLf _
        & "var y = screen.height; " & vbCrLf _
        & "window.open('" & strReportFile & "','_blank', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes') " & vbCrLf _
        & "</script>")
    End Sub

    Private Function selectedBranch() As String
        If IsHoBranch Then
            dvBranchAll.Visible = True
            Dim strID As New StringBuilder

            For Each oItem As ListItem In chkBranch.Items
                If oItem.Selected Then
                    'strID.Append(IIf(strID.ToString = "", "'" & oItem.Value & "'", ",'" & oItem.Value & "'"))
                    strID.Append(IIf(strID.ToString = "", oItem.Value, "," & oItem.Value))
                End If
            Next
            Return strID.ToString
        Else
            Return Replace(Me.sesBranchId, "'", "")
        End If
    End Function
    Private Function selecteJenisTransaksi() As String
        Dim strID As New StringBuilder
        For Each oItem As ListItem In chkTransactionType.Items
            If oItem.Selected Then
                strID.Append(IIf(strID.ToString = "", oItem.Value, "," & oItem.Value))
            End If
        Next
        Return strID.ToString
    End Function

    Private Function selectedRekBank() As String

        Dim strID As New StringBuilder

        For Each oItem As ListItem In CkBoxListRekBank.Items
            If oItem.Selected Then
                'strID.Append(IIf(strID.ToString = "", "'" & oItem.Value & "'", ",'" & oItem.Value & "'"))
                strID.Append(IIf(strID.ToString = "", oItem.Value, "," & oItem.Value))
            End If
        Next
        Return strID.ToString

    End Function

    Private Function selectedInsuranceCompany() As String

        Dim strID As New StringBuilder

        For Each oItem As ListItem In chkInsuranceCompany.Items
            If oItem.Selected Then
                strID.Append(IIf(strID.ToString = "", oItem.Value, "," & oItem.Value))
            End If
        Next
        Return strID.ToString

    End Function

    Private Function selectedBankAccount() As String

        Dim strID As New StringBuilder

        For Each oItem As ListItem In cboBankAccount.Items
            If oItem.Selected Then
                strID.Append(IIf(strID.ToString = "", oItem.Value, "," & oItem.Value))
            End If
        Next
        Return strID.ToString

    End Function

    Private Function selectedKasir() As String

        Dim strID As New StringBuilder

        For Each oItem As ListItem In cboKasir.Items
            If oItem.Selected Then
                strID.Append(IIf(strID.ToString = "", oItem.Value, "," & oItem.Value))
            End If
        Next
        Return strID.ToString

    End Function


    Private Function selectedCollector() As String

        Dim strID As New StringBuilder

        For Each oItem As ListItem In ckbCollectorCL.Items
            If oItem.Selected Then
                strID.Append(IIf(strID.ToString = "", oItem.Value, "," & oItem.Value))
            End If
        Next
        Return strID.ToString

    End Function

    Private Function selectedCollectorD() As String

        Dim strID As New StringBuilder

        For Each oItem As ListItem In cboCollectorD.Items
            If oItem.Selected Then
                strID.Append(IIf(strID.ToString = "", oItem.Value, "," & oItem.Value))
            End If
        Next
        Return strID.ToString

    End Function
    Private Function selectedCollectorGrp() As String

        Dim strID As New StringBuilder

        For Each oItem As ListItem In cboCollectorGrp.Items
            If oItem.Selected Then
                strID.Append(IIf(strID.ToString = "", oItem.Value, "," & oItem.Value))
            End If
        Next
        Return strID.ToString

    End Function

    Private Function selectedAssetType() As String

        Dim strID As New StringBuilder

        For Each oItem As ListItem In chkAssetTypeID.Items
            If oItem.Selected Then
                strID.Append(IIf(strID.ToString = "", oItem.Value, "," & oItem.Value))
            End If
        Next
        Return strID.ToString

    End Function

    'Private Function selectedStatusRepo() As String
    '    If IsHoBranch Then
    '        Dim strID As New StringBuilder

    '        For Each oItem As ListItem In chkStatus.Items
    '            If oItem.Selected Then
    '                strID.Append(IIf(strID.ToString = "", oItem.Value, "," & oItem.Value))
    '            End If
    '        Next
    '        Return strID.ToString
    '    Else
    '        Return Replace(Me.sesBranchId, "'", "")
    '    End If
    'End Function

    Private Sub cboBankCompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBankCompany.SelectedIndexChanged
        bindComboContract()
        bindComboBatch()
        bindNamaKonsumen()
    End Sub

    Private Sub cboKontrak_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboKontrak.SelectedIndexChanged
        bindComboBatch()
        bindNamaKonsumen()
    End Sub

    Private Sub cbonobatch_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboNoBatch.SelectedIndexChanged
        bindNamaKonsumen()
    End Sub
    Protected Sub CheckBox1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkBranch.SelectedIndexChanged
        Dim strID As New StringBuilder

        For Each oItem As ListItem In chkBranch.Items
            If oItem.Selected Then
                strID.Append(IIf(strID.ToString = "", "'" & oItem.Value & "'", ",'" & oItem.Value & "'"))
                For Each oItemBank As ListItem In CkBoxListRekBank.Items
                    oItemBank.Selected = True
                Next
                For Each oItemCollector As ListItem In ckbCollectorCL.Items
                    oItemCollector.Selected = True
                Next

                For Each oItemCollectorD As ListItem In cboCollectorD.Items
                    oItemCollectorD.Selected = True
                Next
            End If
        Next

        Dim customClassx As New GeneralPagingController
        Dim customClass As New Parameter.GeneralPaging

        With customClass
            .strConnection = GetConnectionString()
            .WhereCond = strID.ToString()
            .CurrentPage = 1
            .PageSize = 1
            .SortBy = ""
            .SpName = "spGetBankAccountByBranchs"
        End With
        customClass = customClassx.GetGeneralPaging(customClass)
        With CkBoxListRekBank
            .DataTextField = "Name"
            .DataValueField = "ID"
            .DataSource = customClass.ListData.DefaultView()
            .DataBind()
        End With


        With customClass
            .strConnection = GetConnectionString()
            .WhereCond = strID.ToString()
            .CurrentPage = 1
            .PageSize = 1
            .SortBy = ""
            .SpName = "spGetKasirAll"
        End With
        customClass = customClassx.GetGeneralPaging(customClass)

        With cboKasir
            .DataTextField = "EmployeeName"
            .DataValueField = "LoginID"
            .DataSource = customClass.ListData.DefaultView()
            .DataBind()
        End With

        ''Kolector

        'Collector
        With customClass
            .strConnection = GetConnectionString()
            .WhereCond = strID.ToString()
            .CurrentPage = 1
            .PageSize = 1
            .SortBy = ""
            .SpName = "SPGetCollectorReport"
        End With
        customClass = customClassx.GetGeneralPaging(customClass)

        With ckbCollectorCL

            .DataTextField = "EmployeeName"
            .DataValueField = "CollectorID"
            .DataSource = customClass.ListData.DefaultView()
            .DataBind()
        End With

        'CollectorD
        With customClass
            .strConnection = GetConnectionString()
            .WhereCond = strID.ToString()
            .CurrentPage = 1
            .PageSize = 1
            .SortBy = ""
            .SpName = "SPGetCollectorDReport"
        End With
        customClass = customClassx.GetGeneralPaging(customClass)

        With cboCollectorD

            .DataTextField = "EmployeeName"
            .DataValueField = "CollectorID"
            .DataSource = customClass.ListData.DefaultView()
            .DataBind()
        End With


        ''groupCollector

        With customClass
            .strConnection = GetConnectionString()
            .WhereCond = strID.ToString()
            .CurrentPage = 1
            .PageSize = 1
            .SortBy = ""
            .SpName = "SPGetCollectorGroup"
        End With
        customClass = customClassx.GetGeneralPaging(customClass)

        With cboCollectorGrp

            .DataTextField = "Name"
            .DataValueField = "ID"
            .DataSource = customClass.ListData.DefaultView()
            .DataBind()
        End With

        bindBankAccount()

        ChkAll.Checked = False
    End Sub
    Protected Sub CheckBox2_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ChkAll.CheckedChanged
        If (ChkAll.Checked) Then

            Dim strID As New StringBuilder

            For Each oItem As ListItem In chkBranch.Items
                oItem.Selected = True
                If oItem.Selected Then
                    strID.Append(IIf(strID.ToString = "", "'" & oItem.Value & "'", ",'" & oItem.Value & "'"))
                    For Each oItemBank As ListItem In CkBoxListRekBank.Items
                        oItemBank.Selected = True
                    Next
                    For Each oItemCollector As ListItem In ckbCollectorCL.Items
                        oItemCollector.Selected = True
                    Next

                    For Each oItemCollectorGroup As ListItem In cboCollectorGrp.Items
                        oItemCollectorGroup.Selected = True
                    Next
                    For Each oItemCollectorD As ListItem In cboCollectorD.Items
                        oItemCollectorD.Selected = True
                    Next
                End If
            Next

            Dim customClassx As New GeneralPagingController
            Dim customClass As New Parameter.GeneralPaging

            With customClass
                .strConnection = GetConnectionString()
                .WhereCond = strID.ToString()
                .CurrentPage = 1
                .PageSize = 1
                .SortBy = ""
                .SpName = "spGetBankAccountByBranchs"
            End With
            customClass = customClassx.GetGeneralPaging(customClass)
            With CkBoxListRekBank
                .DataTextField = "Name"
                .DataValueField = "ID"
                .DataSource = customClass.ListData.DefaultView()
                .DataBind()
            End With


            With customClass
                .strConnection = GetConnectionString()
                .WhereCond = strID.ToString()
                .CurrentPage = 1
                .PageSize = 1
                .SortBy = ""
                .SpName = "spGetKasirAll"
            End With
            customClass = customClassx.GetGeneralPaging(customClass)

            With cboKasir
                .DataTextField = "EmployeeName"
                .DataValueField = "LoginID"
                .DataSource = customClass.ListData.DefaultView()
                .DataBind()
            End With

            ''Kolector

            'CollectorCL
            With customClass
                .strConnection = GetConnectionString()
                .WhereCond = strID.ToString()
                .CurrentPage = 1
                .PageSize = 1
                .SortBy = ""
                .SpName = "SPGetCollectorReport"
            End With
            customClass = customClassx.GetGeneralPaging(customClass)

            With ckbCollectorCL

                .DataTextField = "EmployeeName"
                .DataValueField = "CollectorID"
                .DataSource = customClass.ListData.DefaultView()
                .DataBind()
            End With


            'CollectorD
            With customClass
                .strConnection = GetConnectionString()
                .WhereCond = strID.ToString()
                .CurrentPage = 1
                .PageSize = 1
                .SortBy = ""
                .SpName = "SPGetCollectorDReport"
            End With
            customClass = customClassx.GetGeneralPaging(customClass)

            With cboCollectorD

                .DataTextField = "EmployeeName"
                .DataValueField = "CollectorID"
                .DataSource = customClass.ListData.DefaultView()
                .DataBind()
            End With

            'CollectorGroup
            With customClass
                .strConnection = GetConnectionString()
                .WhereCond = strID.ToString()
                .CurrentPage = 1
                .PageSize = 1
                .SortBy = ""
                .SpName = "SPGetCollectorGroup"
            End With
            customClass = customClassx.GetGeneralPaging(customClass)

            With cboCollectorGrp

                .DataTextField = "Name"
                .DataValueField = "ID"
                .DataSource = customClass.ListData.DefaultView()
                .DataBind()
            End With

        Else
            'Return Replace(Me.sesBranchId, "'", "")
            For Each oItem As ListItem In chkBranch.Items
                oItem.Selected = False
            Next
            For Each oItemBank As ListItem In CkBoxListRekBank.Items
                oItemBank.Selected = False
            Next
            For Each oItemKasir As ListItem In cboKasir.Items
                oItemKasir.Selected = False
            Next
            For Each oItemCollector As ListItem In ckbCollectorCL.Items
                oItemCollector.Selected = False
            Next

            For Each oItemCollectorGroup As ListItem In cboCollectorGrp.Items
                oItemCollectorGroup.Selected = False
            Next
            For Each oItemCollectorD As ListItem In cboCollectorD.Items
                oItemCollectorD.Selected = False
            Next
        End If
        'bindListBoxCollectorType_CL()
        'bindBankAccount()
    End Sub

    Private Sub BindSearchBy()
        Me.SearchBy = Me.SearchBy & " and " & cmbSearchList.SelectedItem.Value.Trim & " like '%" & txtSearch.Text.Trim.Replace("'", "''").Replace("%", "") & "%'"
    End Sub

    Protected Sub ChkAllCollectorCL_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ChkAllCollectorCL.CheckedChanged
        If (ChkAllCollectorCL.Checked) Then

            Dim strID As New StringBuilder

            For Each oItem As ListItem In chkBranch.Items
                If oItem.Selected Then
                    strID.Append(IIf(strID.ToString = "", "'" & oItem.Value & "'", ",'" & oItem.Value & "'"))
                    For Each oItemCollector As ListItem In ckbCollectorCL.Items
                        oItemCollector.Selected = True
                    Next
                End If
            Next
        Else
            For Each oItemCollector As ListItem In ckbCollectorCL.Items
                oItemCollector.Selected = False
            Next
        End If

    End Sub

    Protected Sub ChkAllCollectorD_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ChkAllCollectorD.CheckedChanged
        If (ChkAllCollectorD.Checked) Then

            Dim strID As New StringBuilder

            For Each oItem As ListItem In chkBranch.Items
                If oItem.Selected Then
                    strID.Append(IIf(strID.ToString = "", "'" & oItem.Value & "'", ",'" & oItem.Value & "'"))
                    For Each oItemCollectorD As ListItem In cboCollectorD.Items
                        oItemCollectorD.Selected = True
                    Next
                End If
            Next
        Else
            For Each oItemCollectorD As ListItem In cboCollectorD.Items
                oItemCollectorD.Selected = False
            Next
        End If

    End Sub
End Class