﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="viewByFundingFacilityDateRange.aspx.vb"
    Inherits="Maxiloan.Webform.Reports.viewByFundingFacilityDateRange" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ucNumberFormat" Src="../Webform.UserController/ucNumberFormat.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucFundingReport" Src="../Webform.UserController/ucFundingReport.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucDateCE" Src="../Webform.UserController/ucDateCE.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/ReportingService.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <link href="../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css"
        rel="Stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="sm1">
    </asp:ScriptManager>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h4>
                <asp:Label ID="lblTitleReport" runat="server" Text=""></asp:Label>
            </h4>
        </div>
    </div>
    <div class="form_box_uc">
        <uc1:ucfundingreport id="ucfunding" runat="server"></uc1:ucfundingreport>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Jatuh Tempo
            </label>
            <uc1:ucdatece id="txtStartDate" runat="server" />
            <label class="label_auto">
                s/d</label>
            <uc1:ucdatece id="txtEndDate" runat="server" />
        </div>
    </div>
    <div class="form_button">
        <asp:Button runat="server" ID="btnView" Text="View Report" CssClass="small button blue"
            UseSubmitBehavior="False" />
    </div>
    </form>
</body>
</html>
