﻿Imports Maxiloan.Controller
Imports System.Data.SqlClient

Public Class ReportSetting
    Inherits Maxiloan.Webform.WebBased
#Region "Property "

    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property

#End Region
#Region " Private Const "

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1


#End Region
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.FormID = "REPORTSETTING"
            Me.SearchBy = ""
            BindGridViewEntity(Me.SearchBy)
        End If
    End Sub
    Sub BindGridViewEntity(ByVal cmdWhere As String)

        Dim dtsEntity As DataTable
        Dim customClassx As New GeneralPagingController
        Dim customClass As New Parameter.GeneralPaging
        Me.SearchBy = cmdWhere

        If Me.Sort = "" Then
            Me.Sort = " ReportGroupID "
        End If

        With customClass
            .strConnection = GetConnectionString()
            .WhereCond = Me.SearchBy
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = Me.Sort
            .SpName = "spReportGroupPaging"
        End With
        customClass = customClassx.GetGeneralPaging(customClass)

        If Not customClass Is Nothing Then
            dtsEntity = customClass.ListData
            recordCount = customClass.TotalRecords
        Else
            recordCount = 0
        End If

        GridView1.DataSource = dtsEntity.DefaultView
        'GridView1.CurrentPageIndex = 0
        GridView1.PageIndex = 0
        GridView1.DataBind()

        PagingFooter()
    End Sub
#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            lblTotPage.Text = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblTotRec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select

        BindGridViewEntity(Me.SearchBy)

    End Sub
    Private Sub BtnGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnGoPage.Click
        If IsNumeric(txtGoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtGoPage.Text, Int32)
                BindGridViewEntity(Me.SearchBy)
            End If
        End If
    End Sub
#End Region
    Protected Sub GridView1_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound

        
   
        Dim row As GridViewRow = e.Row
        Dim strSort As String = String.Empty

        If row.DataItem Is Nothing Then
            Return
        End If

        Dim imgBtn As ImageButton
        imgBtn = CType(e.Row.FindControl("ImgBtn"), ImageButton)
        imgBtn.CommandArgument = e.Row.RowIndex.ToString()
        Dim gv As New GridView()

        gv.Visible = True
        Dim objPH As PlaceHolder = CType(e.Row.FindControl("objPHOrderDetails"), PlaceHolder)
        objPH.Visible = False
        imgBtn.ImageUrl = "../Images/Plus.gif"

    End Sub

    Protected Sub grdOrders_RowCommand(ByVal sender As Object, ByVal e As GridViewCommandEventArgs) Handles GridView1.RowCommand

        If (e.CommandName = "Expand") Then
            lblMessage.Visible = False
            Dim gv As GridView = CType(sender, GridView)
            Dim rowIndex As Int32 = Convert.ToInt32(e.CommandArgument.ToString())
            Dim imgbtn As ImageButton = CType(gv.Rows(rowIndex).FindControl("ImgBtn"), ImageButton)


            Dim key = gv.DataKeys(rowIndex)(0).ToString()
            ViewState("AppID") = key
            Dim objPH As PlaceHolder = CType(gv.Rows(rowIndex).FindControl("objPHOrderDetails"), PlaceHolder)
            Dim gv2 As New GridView()

            If (imgbtn.ImageUrl.Trim().ToUpper() = "../IMAGES/PLUS.GIF") Then
                objPH.Visible = True
                imgbtn.ImageUrl = "../images/Minus.gif"
                gv2 = CType(gv.Rows(rowIndex).FindControl("GridView2"), GridView)

                Dim objCommandDetail As New SqlCommand
                Dim objconnectionDetail As New SqlConnection(GetConnectionString)
                Dim objreadDetail As SqlDataReader
                If objconnectionDetail.State = ConnectionState.Closed Then objconnectionDetail.Open()
                objCommandDetail.CommandType = CommandType.StoredProcedure
                objCommandDetail.Connection = objconnectionDetail
                objCommandDetail.CommandText = "spViewTblReportSetting"
                objCommandDetail.Parameters.Add("@ReportGroupID", SqlDbType.VarChar, 10).Value = key.Trim
                objreadDetail = objCommandDetail.ExecuteReader()

                gv2.DataSource = objreadDetail
                gv2.DataBind()
                objreadDetail.Close()
            Else
                imgbtn.ImageUrl = "../images/Plus.gif"
                objPH.Visible = False
            End If

        End If
    End Sub

    Protected Sub SaveData(ByVal ReportGroupID As String, ByVal ReportGroupName As String, ByVal ActionData As Boolean)

        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(GetConnectionString)

        Dim objReader As SqlDataReader

        Try

            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objCommand.Parameters.Clear()
            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.CommandText = "sp_TblReportGroup_ADDandUpdate"
            objCommand.Connection = objConnection

            objCommand.Parameters.Add("@ReportGroupID", SqlDbType.Char, 10).Value = ReportGroupID
            objCommand.Parameters.Add("@ReportGroupName", SqlDbType.VarChar, 200).Value = ReportGroupName
            objCommand.Parameters.Add("@ActionData", SqlDbType.Bit).Value = ActionData

            objReader = objCommand.ExecuteReader

            If objReader.Read Then
                ShowMessage(lblMessage, CStr(objReader.Item("DataReport")), CBool(objReader.Item("StsMsg")))
            End If
            objReader.Close()

        Catch ex As Exception
            Response.Write(ex.Message)
            ShowMessage(lblMessage, ex.Message, True)
        Finally
            objCommand.Dispose()
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try

    End Sub
    Protected Sub DeleteData(ByVal ReportGroupID As String)

        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(GetConnectionString)


        Dim objReader As SqlDataReader

        Try

            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objCommand.Parameters.Clear()
            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.CommandText = "sp_TblReportGroup_Delete"
            objCommand.Connection = objConnection

            objCommand.Parameters.Add("@ReportGroupID", SqlDbType.Char, 10).Value = ReportGroupID

            objReader = objCommand.ExecuteReader

            If objReader.Read Then
                ShowMessage(lblMessage, CStr(objReader.Item("DataReport")), CBool(objReader.Item("StsMsg")))
            End If
            objReader.Close()

        Catch ex As Exception
            Response.Write(ex.Message)
            ShowMessage(lblMessage, ex.Message, True)
        Finally
            objCommand.Dispose()
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try

    End Sub
    Protected Sub DeleteDetailData(ByVal id As Int64)

        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(GetConnectionString)

        Dim objReader As SqlDataReader

        Try

            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objCommand.Parameters.Clear()
            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.CommandText = "sp_TblReportSetting_Delete"
            objCommand.Connection = objConnection

            objCommand.Parameters.Add("@ID", SqlDbType.BigInt).Value = id

            objReader = objCommand.ExecuteReader

            If objReader.Read Then
                ShowMessage(lblMessage, CStr(objReader.Item("DataReport")), CBool(objReader.Item("StsMsg")))
              
            End If
            objReader.Close()

        Catch ex As Exception
            Response.Write(ex.Message)
            ShowMessage(lblMessage, ex.Message, True)
        Finally
            objCommand.Dispose()
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try

    End Sub

    Protected Sub EditGroupReport(ByVal sender As Object, ByVal e As GridViewEditEventArgs)
        lblMessage.Visible = False
        GridView1.EditIndex = e.NewEditIndex
        BindGridViewEntity(Me.SearchBy)


    End Sub

    Protected Sub CancelEdit(ByVal sender As Object, ByVal e As GridViewCancelEditEventArgs)
        lblMessage.Visible = False
        GridView1.EditIndex = -1
        BindGridViewEntity(Me.SearchBy)


    End Sub

    Protected Sub UpdateGroupReport(ByVal sender As Object, ByVal e As GridViewUpdateEventArgs)
        lblMessage.Visible = False
        Dim txtEditReportGroupID As String = DirectCast(GridView1.Rows(e.RowIndex).FindControl("txtEditReportGroupID"), TextBox).Text

        Dim txtEditReportGroupName As String = DirectCast(GridView1.Rows(e.RowIndex).FindControl("txtEditReportGroupName"), TextBox).Text

        SaveData(txtEditReportGroupID, txtEditReportGroupName, True)
        GridView1.EditIndex = -1

        BindGridViewEntity(Me.SearchBy)

    End Sub


    Protected Sub AddNewGroupReport(ByVal sender As Object, ByVal e As EventArgs)
        lblMessage.Visible = False
        Dim control As Control = Nothing
        If (Not (GridView1.FooterRow) Is Nothing) Then
            control = GridView1.FooterRow
        Else
            control = GridView1.Controls(0).Controls(0)
        End If
        Dim reportGroupID As String = CType(control.FindControl("txtReportGroupID"), TextBox).Text
        Dim reportGroupName As String = CType(control.FindControl("txtReportGroupName"), TextBox).Text

        SaveData(reportGroupID, reportGroupName, False)

        BindGridViewEntity(Me.SearchBy)
    End Sub

    Protected Sub DeleteReportGroup(ByVal sender As Object, ByVal e As EventArgs)
        lblMessage.Visible = False
        Dim lnkRemove As LinkButton = DirectCast(sender, LinkButton)
        DeleteData(lnkRemove.CommandArgument)
        BindGridViewEntity(Me.SearchBy)

    End Sub

    Protected Sub DeleteReportDetal(ByVal sender As Object, ByVal e As EventArgs)
        lblMessage.Visible = False
        Dim lnkRemoveDetail As LinkButton = DirectCast(sender, LinkButton)
        DeleteDetailData(lnkRemoveDetail.CommandArgument)
        BindGridViewEntity(Me.SearchBy)

    End Sub
    Public Function GenerateLinkTo() As String
        Return "AddandEDITreportsSet.aspx"
    End Function

End Class