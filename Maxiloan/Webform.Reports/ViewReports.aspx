﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewReports.aspx.vb" Inherits="Maxiloan.Webform.Reports.ViewReports" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ViewReports</title>
    <link href="../css/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/default/easyui.css" type="text/css">
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <script src="../js/jquery-1.9.1.min.js"></script>
    <script src="../js/jquery-ui-1.10.3.custom.min.js"></script>
    <script type="text/javascript" src="../js/jquery.min.js"></script>
    <script type="text/javascript" src="../js/jquery.easyui.min.js"></script>
</head>
<body>
    <%--   <asp:UpdateProgress ID="upg1" runat="server" AssociatedUpdatePanelID="ChildControl">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>--%>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="sm" runat="server"  ScriptMode="Release"/>
    <%--  <asp:UpdatePanel runat="server" ID="ChildControl">
        <ContentTemplate>--%>
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
        onclick="hideMessage();"></asp:Label>
        <body class="easyui-layout" style="text-align:left">
   <%-- <div class="easyui-layout" id="cc" style="width: 100%; height: 500px;">--%>
        <div data-options="region:'west',split:true" title="Daftar Reports" style="width: 320px;">
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" OnRowCommand="GridView1_RowCommand" 
                HeaderStyle-CssClass="th" RowStyle-CssClass="item_grid" GridLines="None" Width="100%"
                ShowHeader="False">
                <Columns>
                    <asp:TemplateField HeaderText="No." ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="Middle"
                        HeaderStyle-HorizontalAlign="left" HeaderStyle-Width="20" ItemStyle-Width="20">
                        <ItemTemplate>
                            <asp:Label ID="lblID" Text='<%# Eval("ID") %>' runat="server" Visible="false"></asp:Label>
                            <asp:Label ID="lblNomor" Text='<%# Eval("Nomor") %>' runat="server"></asp:Label>
                            <asp:Label ID="lblItemID" Text='<%# Eval("ItemID") %>' runat="server" Visible="false"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="URL" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="Middle"
                        Visible="false" HeaderStyle-HorizontalAlign="left" HeaderStyle-Width="400" ItemStyle-Width="400">
                        <ItemTemplate>
                            <asp:Label ID="lblPath" Text='<%# Eval("Path") %>' runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Nama" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="Middle"
                        Visible="false" HeaderStyle-HorizontalAlign="left" HeaderStyle-Width="250" ItemStyle-Width="250">
                        <ItemTemplate>
                            <asp:Label ID="lblName" Text='<%# Eval("Name") %>' runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Keterangan" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="Middle"
                        HeaderStyle-HorizontalAlign="left" HeaderStyle-Width="400" ItemStyle-Width="400">
                        <ItemTemplate>
                            <asp:Label ID="lblDescription" Text='<%# Eval("Description") %>' runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Branch" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle"
                        Visible="false" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="50" ItemStyle-Width="50">
                        <ItemTemplate>
                            <asp:CheckBox ID="cbIsBranch" Checked='<%# Eval("IsBranch") %>' Enabled="false" runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderStyle Width="10"></HeaderStyle>
                        <ItemTemplate>
                            <asp:ImageButton ID="ImgBtnRpt" ImageUrl="../images/selection.png" CommandName="Select" CommandArgument='<%# Container.DataItemIndex %>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
        <div data-options="region:'center',title:'View Reports',iconCls:'icon-ok'">
             <rsweb:ReportViewer ID="ReportViewer1" runat="server" Height="98%" 
            Width="99%" DocumentMapWidth="10%">
        </rsweb:ReportViewer>
        </div>
   </body>
    <%--        </ContentTemplate>
    </asp:UpdatePanel>--%>
    </form>

</body>
</html>
