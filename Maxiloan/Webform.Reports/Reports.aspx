﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Reports.aspx.vb" Inherits="Maxiloan.Webform.Reports.Reports" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ucNumberFormat" Src="../Webform.UserController/ucNumberFormat.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Reports</title>
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="sm1">
    </asp:ScriptManager>
    <%--<uc1:ucrsviewer id="ucRSViewer1" runat="server" />--%>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h4>
                <asp:Label ID="lblTitleReport" runat="server" Text=""></asp:Label>
                </h4>
        </div>
    </div>    
     
    <%--<div runat="server" id="dvBranch" class="form_box">
        <div class="form_left">
            <label class="label_general">
                Cabang
            </label>
            <div class="checkbox_wrapper_ws">
                <asp:CheckBoxList runat="server" ID="chkBranch" RepeatDirection="Vertical" RepeatLayout="Table" AutoPostBack="True" OnSelectedIndexChanged="CheckBox1_CheckedChanged"
                    CssClass="opt_single">
                </asp:CheckBoxList>
            </div>
        </div>
    </div>--%>
    <div runat="server" id="dvBranchAll" class="form_box" visible="false">
      <div class="form_left">
            <label class="label_general">
                All
            </label>
                <asp:CheckBox runat="server" ID="ChkAll" RepeatDirection="Vertical" RepeatLayout="Table" AutoPostBack="True" OnSelectedIndexChanged="CheckBox2_CheckedChanged"
                    CssClass="opt_single">
                </asp:CheckBox>
            </div>
            </div>
     <div runat="server" id="dvBranch" class="form_box">
        <div class="form_left">
            <label class="label_general">
                Cabang
            </label>
            <div class="checkbox_wrapper_ws">
                <asp:CheckBoxList runat="server" ID="chkBranch" RepeatDirection="Vertical" RepeatLayout="Table" AutoPostBack="True" OnSelectedIndexChanged="CheckBox1_CheckedChanged"
                    CssClass="opt_single">
                </asp:CheckBoxList>
            </div>
        </div>
    </div>
    <div runat="server" id="dvArea" class="form_box">
        <div class="form_left">
            <label class="label_general">
                Area
            </label>
            <div class="checkbox_wrapper_ws">
                <asp:CheckBoxList runat="server" ID="chkArea" RepeatDirection="Vertical" RepeatLayout="Table"
                    CssClass="opt_single">
                </asp:CheckBoxList>
            </div>
        </div>
    </div>
    <div runat="server" id="dvBulanTahun" class="form_box">
        <div class="form_left">
            <label>
                Sampai bulan ke-
            </label>
            <uc1:ucnumberformat id="txtBulan" runat="server"></uc1:ucnumberformat>
        </div>
        <div class="form_right">
            <label>
                Tahun
            </label>
            <uc1:ucnumberformat id="txtTahun" runat="server"></uc1:ucnumberformat>
        </div>
    </div>
    <div runat="server" id="dvDateRange" class="form_box">
        <div class="form_single">
            <label class="label_general">
                Mulai Tanggal
            </label>
            <asp:TextBox runat="server" ID="txtStartDate" CssClass="small_text"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ID="rfvDate1" ControlToValidate="txtStartDate"
                Display="Dynamic" ErrorMessage="Isi tanggal!" CssClass="validator_general"></asp:RequiredFieldValidator>
            <asp:CalendarExtender runat="server" ID="ce1" TargetControlID="txtStartDate" Format="dd/MM/yyyy">
            </asp:CalendarExtender>
           
            <label class="label_auto">
                s/d</label>
            <asp:TextBox runat="server" ID="txtEndDate" CssClass="small_text"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ID="rfvDate2" ControlToValidate="txtEndDate"
                Display="Dynamic" ErrorMessage="Isi tanggal!" CssClass="validator_general"></asp:RequiredFieldValidator>
            <asp:CalendarExtender runat="server" ID="ce2" TargetControlID="txtEndDate" Format="dd/MM/yyyy">
            </asp:CalendarExtender>
        </div>
    </div>

    <div runat="server" id="DivDateTimeRange" class="form_box">
        <div class="form_single">
            <label class="label_general">
                Mulai Tanggal
            </label>
            <asp:TextBox runat="server" ID="txtStartDateTime" CssClass="small_text"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ID="rfvStartDateTime" ControlToValidate="txtStartDateTime"
                Display="Dynamic" ErrorMessage="Isi tanggal!" CssClass="validator_general"></asp:RequiredFieldValidator>
            <asp:CalendarExtender runat="server" ID="ceStartDateTime" TargetControlID="txtStartDateTime" Format="dd/MM/yyyy">
            </asp:CalendarExtender>
            <label class="label_auto">
                jam</label>
                 <asp:TextBox ID="txtStartTime" Text="00:00"  runat="server" Width="40px"></asp:TextBox>
                  
        <asp:MaskedEditExtender ID="msStartTime" runat="server"  
            Mask="99:99" MaskType="Time" TargetControlID="txtStartTime" 
            UserTimeFormat="TwentyFourHour" InputDirection="LeftToRight" ClearTextOnInvalid="True" MessageValidatorTip="True">
        </asp:MaskedEditExtender>
         <asp:MaskedEditValidator ID="StartTimeMaskedEditValidator" runat="server" 
                         ControlExtender="msStartTime"
                         ControlToValidate="txtStartTime" IsValidEmpty="false" 
                         MaximumValue="23:59" MinimumValue="00:00"
                         EmptyValueMessage="Enter Time" 
                         MaximumValueMessage="23:59" 
                         InvalidValueBlurredMessage="Time is Invalid"
                         MinimumValueMessage="Time must be grater than 00:00"
                         EmptyValueBlurredText="Enter Time"
                         CssClass="validator_general"></asp:MaskedEditValidator>
            <label class="label_auto">
                s/d</label>
            <asp:TextBox runat="server" ID="txtEndDateTime" CssClass="small_text"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ID="rfvEndDateTime" ControlToValidate="txtEndDateTime"
                Display="Dynamic" ErrorMessage="Isi tanggal!" CssClass="validator_general"></asp:RequiredFieldValidator>
            <asp:CalendarExtender runat="server" ID="ceEndDateTime" TargetControlID="txtEndDateTime" Format="dd/MM/yyyy">
            </asp:CalendarExtender>
            <label class="label_auto">
                jam</label>
                 <asp:TextBox ID="txtEndTime" Text="23:59"  runat="server" Width="40px"></asp:TextBox>
                 
        <asp:MaskedEditExtender ID="msEndTime" runat="server"  
            Mask="99:99" MaskType="Time" TargetControlID="txtEndTime" 
            UserTimeFormat="TwentyFourHour" InputDirection="LeftToRight" ClearTextOnInvalid="True" MessageValidatorTip="True">
        </asp:MaskedEditExtender>
        <asp:MaskedEditValidator ID="EndTimeMaskedEditValidator" runat="server" 
                         ControlExtender="msEndTime"
                         ControlToValidate="txtEndTime" IsValidEmpty="false" 
                         MaximumValue="23:59" MinimumValue="00:00"
                         EmptyValueMessage="Enter Time" 
                         MaximumValueMessage="23:59" 
                         InvalidValueBlurredMessage="Time is Invalid"
                         MinimumValueMessage="Time must be grater than 00:00"
                         EmptyValueBlurredText="Enter Time"
                         CssClass="validator_general"></asp:MaskedEditValidator>
        </div>
    </div>

    <div runat="server" id="dvDateRange1" class="form_box">
        <div class="form_single">
            <label class="label_general">
                Mulai Tgl. Valuta
            </label>
            <asp:TextBox runat="server" ID="txtStartDate1" CssClass="small_text"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator8" ControlToValidate="txtStartDate1"
                Display="Dynamic" ErrorMessage="Isi tanggal!" CssClass="validator_general"></asp:RequiredFieldValidator>
            <asp:CalendarExtender runat="server" ID="CalendarExtender2" TargetControlID="txtStartDate1" Format="dd/MM/yyyy">
            </asp:CalendarExtender>
            <label class="label_auto">
                s/d</label>
            <asp:TextBox runat="server" ID="txtEndDate1" CssClass="small_text"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator9" ControlToValidate="txtEndDate1"
                Display="Dynamic" ErrorMessage="Isi tanggal!" CssClass="validator_general"></asp:RequiredFieldValidator>
            <asp:CalendarExtender runat="server" ID="CalendarExtender3" TargetControlID="txtEndDate1" Format="dd/MM/yyyy">
            </asp:CalendarExtender>
        </div>
    </div>
    <div runat="server" id="dvSingleDate" class="form_box">
        <div class="form_single">
            <label class="label_general">
                Tanggal
            </label>
            <asp:TextBox runat="server" ID="txtSingleDate" CssClass="small_text"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtSingleDate"
                Display="Dynamic" ErrorMessage="Isi tanggal!" CssClass="validator_general"></asp:RequiredFieldValidator>
            <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtSingleDate"
                Format="dd/MM/yyyy">
            </asp:CalendarExtender>
        </div>
    </div>

     <div runat="server" id="dvTglBayar" class="form_box">
        <div class="form_single">
            <label class="label_general">
                Tanggal Bayar
            </label>
            <asp:TextBox runat="server" ID="txtTglBayar" CssClass="small_text"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator10" ControlToValidate="txtTglBayar"
                Display="Dynamic" ErrorMessage="Isi tanggal!" CssClass="validator_general"></asp:RequiredFieldValidator>
            <asp:CalendarExtender runat="server" ID="CalendarExtender7" TargetControlID="txtTglBayar"
                Format="dd/MM/yyyy">
            </asp:CalendarExtender>
        </div>
    </div>

      <div runat="server" id="dvRekBank" class="form_box">
        <div class="form_left">
            <label class="label_general">
                Rekening Bank
            </label>
            <div class="checkbox_wrapper_ws">
                <asp:CheckBoxList runat="server" ID="CkBoxListRekBank" RepeatDirection="Vertical" RepeatLayout="Table"
                    CssClass="opt_single">
                </asp:CheckBoxList>
            </div>
        </div>
    </div>
    <div runat="server" id="dvKondisiSKT" class="form_box">
        <div class="form_single">
            <label class="label_general">
                Kondisi
            </label>
            <asp:DropDownList runat="server" ID="cboKondisiSKT">
                <asp:ListItem Text="SKT - HARI INI" Value="TD" Selected="true"></asp:ListItem>
                <asp:ListItem Text="SKT - OUSTANDING" Value="OP"></asp:ListItem>
                <asp:ListItem Text="SKT - BERHASIL DITARIK" Value="CL"></asp:ListItem>
            </asp:DropDownList>
        </div>
    </div>
    <div runat="server" id="dvPeriodLapARODStatCMO" class="form_box">
        <div class="form_single">
            <label class="label_general">
                Periode</label>
            <asp:DropDownList runat="server" ID="cboStartMonth">
                <asp:ListItem Text="Januari" Value="1" Selected="True"></asp:ListItem>
                <asp:ListItem Text="Februari" Value="2"></asp:ListItem>
                <asp:ListItem Text="Maret" Value="3"></asp:ListItem>
                <asp:ListItem Text="April" Value="4"></asp:ListItem>
                <asp:ListItem Text="Mei" Value="5"></asp:ListItem>
                <asp:ListItem Text="Juni" Value="6"></asp:ListItem>
                <asp:ListItem Text="Juli" Value="7"></asp:ListItem>
                <asp:ListItem Text="Agustus" Value="8"></asp:ListItem>
                <asp:ListItem Text="September" Value="9"></asp:ListItem>
                <asp:ListItem Text="Oktober" Value="10"></asp:ListItem>
                <asp:ListItem Text="November" Value="11"></asp:ListItem>
                <asp:ListItem Text="Desember" Value="12"></asp:ListItem>
            </asp:DropDownList>
            <asp:TextBox runat="server" ID="txtStartYear" CssClass="small_text"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ControlToValidate="txtStartYear"
                Display="Dynamic" ErrorMessage="Isi Tahun" CssClass="validator_general"></asp:RequiredFieldValidator>
            <label class="label_auto">
                s/d
            </label>
            <asp:DropDownList runat="server" ID="cboEndMonth">
                <asp:ListItem Text="Januari" Value="1" Selected="True"></asp:ListItem>
                <asp:ListItem Text="Februari" Value="2"></asp:ListItem>
                <asp:ListItem Text="Maret" Value="3"></asp:ListItem>
                <asp:ListItem Text="April" Value="4"></asp:ListItem>
                <asp:ListItem Text="Mei" Value="5"></asp:ListItem>
                <asp:ListItem Text="Juni" Value="6"></asp:ListItem>
                <asp:ListItem Text="Juli" Value="7"></asp:ListItem>
                <asp:ListItem Text="Agustus" Value="8"></asp:ListItem>
                <asp:ListItem Text="September" Value="9"></asp:ListItem>
                <asp:ListItem Text="Oktober" Value="10"></asp:ListItem>
                <asp:ListItem Text="November" Value="11"></asp:ListItem>
                <asp:ListItem Text="Desember" Value="12"></asp:ListItem>
            </asp:DropDownList>
            <asp:TextBox runat="server" ID="txtEndYear" CssClass="small_text"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ControlToValidate="txtEndYear" Display="dynamic"
                ErrorMessage="isi tahun" CssClass="validator_general" ID="RequiredFieldValidator3"></asp:RequiredFieldValidator>
        </div>
    </div>
    <div runat="server" id="dvLmNunggak" class="form_box">
        <div class="form_single">
            <label class="label_general">
                Lama Nunggak Dari</label>
            <asp:TextBox runat="server" ID="txtlnstart" CssClass="small_text"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator4" ControlToValidate="txtlnstart"
                Display="Dynamic" ErrorMessage="Isi hari" CssClass="validator_general"></asp:RequiredFieldValidator>
            <label class="label_auto">
                Sampai Hari ke
            </label>
            <asp:TextBox runat="server" ID="txtlnend" CssClass="small_text"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ControlToValidate="txtlnend" Display="dynamic"
                ErrorMessage="isi hari" CssClass="validator_general" ID="RequiredFieldValidator5"></asp:RequiredFieldValidator>
        </div>
    </div>
    <div runat="server" id="dvCollectorGrp" class="form_box">
        <div class="form_single">
            <label class="label_general">
                Wilayah Collector</label>
          <%--  <asp:DropDownList runat="server" ID="cboCollectorGrp">
            </asp:DropDownList>--%>
            <div class="checkbox_wrapper_ws">
                <asp:CheckBoxList runat="server" ID="cboCollectorGrp" RepeatDirection="Vertical" RepeatLayout="Table"
                    CssClass="opt_single">
                </asp:CheckBoxList>
            </div>
        </div>
    </div>
    <div runat="server" id="dvCollectorCLAll" class="form_box" visible="false">
		<div class="form_left">
            <label class="label_general">
                Check All Field Collector
            </label>
                <asp:CheckBox runat="server" ID="ChkAllCollectorCL" RepeatDirection="Vertical" RepeatLayout="Table" AutoPostBack="True" OnSelectedIndexChanged="ChkAllCollectorCL_CheckedChanged"
                    CssClass="opt_single">
                </asp:CheckBox>
        </div>
    </div>
    <div runat="server" id="dvCollectorCL" class="form_box">
        <div class="form_single">
            <label class="label_general">
                Collector</label>

            <div class="checkbox_wrapper_ws">
                <asp:CheckBoxList runat="server" ID="ckbCollectorCL" RepeatDirection="Vertical" RepeatLayout="Table"
                    CssClass="opt_single">
                </asp:CheckBoxList>
            </div>

        </div>
    </div>
    <div runat="server" id="dvCollectorDAll" class="form_box" visible="false">
		<div class="form_left">
            <label class="label_general">
                Check All Deskcoll
            </label>
                <asp:CheckBox runat="server" ID="ChkAllCollectorD" RepeatDirection="Vertical" RepeatLayout="Table" AutoPostBack="True" OnSelectedIndexChanged="ChkAllCollectorD_CheckedChanged"
                    CssClass="opt_single">
                </asp:CheckBox>
        </div>
    </div>    
    <div runat="server" id="dvCollectorD" class="form_box">
        <div class="form_single">
            <label class="label_general">
                Collector</label>
                <div class="checkbox_wrapper_ws">
                 <asp:CheckBoxList runat="server" ID="cboCollectorD" RepeatDirection="Vertical" RepeatLayout="Table"
                    CssClass="opt_single">
                </asp:CheckBoxList>
                </div>
       </div>
         <div class="form_single">
            <label class="label_auto">
                Jenis Kegiatan
            </label>
            <asp:DropDownList runat="server" ID="cboJenisTask" Enabled="false">
               <%-- <asp:ListItem Text="Remind Installment" Value="ri"></asp:ListItem>--%>
                <asp:ListItem Text="OverDue Call" Value="oc" Selected="True">
                </asp:ListItem>
            </asp:DropDownList>
        </div>
    </div>
    <div runat="server" id="dvStatusAsset" class="form_box">
        <div class="form_left">
            <label class="label_general">
                Status</label>
            <asp:CheckBoxList runat="server" ID="chkStatusAsset" RepeatDirection="Vertical" RepeatLayout="Table"
                CssClass="opt_single">
                <asp:ListItem Text="Repossess" Value="r" Selected="True"></asp:ListItem>
                <asp:ListItem Text="Inventory" Value="i"></asp:ListItem>
                <asp:ListItem Text="Sold" Value="s"></asp:ListItem>
                <asp:ListItem Text="Paid" Value="p"></asp:ListItem>
            </asp:CheckBoxList>
        </div>
    </div>
    <div runat="server" id="dvCreditAnalist" class="form_box">
        <div class="form_single">
            <label class="label_general">
                Pembiayaan Analyst</label>
            <asp:DropDownList runat="server" ID="cboCreditAnalist">
            </asp:DropDownList>
        </div>
    </div>
    <div runat="server" id="dvCMO" class="form_box">
        <div class="form_single">
            <label class="label_general">
                Nama CMO</label>
            <asp:DropDownList runat="server" ID="cboCMO">
            </asp:DropDownList>
        </div>
    </div>
    <div runat="server" id="dvSurveyor" class="form_box">
        <div class="form_single">
            <label class="label_general">
                Nama Surveyor</label>
            <asp:DropDownList runat="server" ID="cboSurveyor">
            </asp:DropDownList>
        </div>
    </div>
   <%-- <div runat="server" id="dvStopOpname" class="form_box">
        <div class="form_left">
            <label class="label_general">
                StopOpname
            </label>
            <div class="label_general">
                <asp:DropDownList runat="server" ID="chkStopOpname">
                </asp:DropDownList>
            </div>
        </div>
    </div>--%>
    <div runat="server" id="dvRekBankType_B" class="form_box">
        <div class="form_single">
            <label class="label_general">
                Rekening Bank</label>
            <asp:DropDownList runat="server" ID="cboRekBankType_B">
            </asp:DropDownList>
        </div>
    </div>
    <div runat="server" id="dvRekBankType_C" class="form_box">
        <div class="form_single">
            <label class="label_general">
                Rekening Bank</label>
            <asp:DropDownList runat="server" ID="cboRekBankType_C">
            </asp:DropDownList>
        </div>
    </div>
   <%-- <div runat="server" id="dvBankAccount" class="form_box">
        <div class="form_single">
            <label class="label_general">
                Bank Account</label>
            <asp:DropDownList runat="server" ID="cboBankAccount">
            </asp:DropDownList>
        </div>
    </div>--%>
    <div runat="server" id="dvBankAccount" class="form_box">
        <div class="form_single">
            <label class="label_general">
                Bank Account</label>
            <asp:CheckBoxList runat="server" ID="cboBankAccount" RepeatDirection="Vertical" RepeatLayout="Table" CssClass="opt_single">
            </asp:CheckBoxList>
        </div>
    </div>
    <div runat="server" id="dvChkColl_CL" class="form_box">
        <div class="form_single">
            <label class="label_general">
                Collector
            </label>
            <asp:CheckBoxList runat="server" ID="chkColl" RepeatDirection="Vertical" RepeatLayout="Table"
                CssClass="opt_single">
            </asp:CheckBoxList>
        </div>
    </div>
    <div runat="server" id="dvChkAssetType" class="form_box">
        <div class="form_single">
            <label class="label_general">
                AssetType
            </label>
            <asp:CheckBoxList runat="server" ID="chkAssetTypeID" RepeatDirection="Vertical" RepeatLayout="Table"
                CssClass="opt_single">
            </asp:CheckBoxList>
        </div>
    </div>
    <%--<div runat="server" id="dvKasir" class="form_box">
        <div class="form_single">
            <label class="label_general">
                Kasir</label>
            <asp:DropDownList runat="server" ID="cboKasir">
            </asp:DropDownList>
        </div>
    </div>--%>
    <div runat="server" id="dvKasir" class="form_box">
        <div class="form_left">
            <label class="label_general">
                Kasir
            </label>
            <div class="checkbox_wrapper_ws">
                <asp:CheckBoxList runat="server" ID="cboKasir" RepeatDirection="Vertical" RepeatLayout="Table"
                    CssClass="opt_single">
                </asp:CheckBoxList>
            </div>
        </div>
    </div>
    <div runat="server" id="dvBankCompany" class="form_box">
        <div class="form_single">
            <label class="label_general">
                Bank</label>
            <asp:DropDownList runat="server" ID="cboBankCompany" AutoPostBack="True">
            </asp:DropDownList>

        </div>
    </div>
     <div runat="server" id="dvStockOpname" class="form_box">
        <div class="form_single">
            <label class="label_general">
                StockOpname</label>
            <asp:DropDownList runat="server" ID="cboStockOpname" >
            </asp:DropDownList>

        </div>
    </div>
    <div runat="server" id="dvKontrak" class="form_box">
        <div class="form_single">
            <label class="label_general">
                No Kontrak
            </label>
            <asp:DropDownList runat="server" ID="cboKontrak" AutoPostBack="True">
            </asp:DropDownList>
        </div>
    </div>
    <div runat="server" id="dvNoBatch" class="form_box">
        <div class="form_single">
            <label class="label_general">
                No Batch
            </label>
            <asp:DropDownList runat="server" ID="cboNoBatch" AutoPostBack="True">
            </asp:DropDownList>
            <%--AutoPostBack="true" OnSelectedIndexChanged="bindNamaKonsumen"--%>
        </div>
    </div>
    <div runat="server" id="dvNamaKonsumen" class="form_box">
        <div class="form_single">
            <label class="label_general">
                Nama Konsumen
            </label>
            <asp:DropDownList runat="server" ID="cboNamaKonsumen">
            </asp:DropDownList>
        </div>
    </div>
    <div runat="server" id="dvchkInsuranceCompany" class="form_box">
        <div class="form_single">
            <label class="label_general">
                Perusahaan Asuransi
            </label>
            <div class="checkbox_wrapper_ws">
                <asp:CheckBoxList runat="server" ID="chkInsuranceCompany" RepeatDirection="Vertical"
                    RepeatLayout="Table" CssClass="opt_single">
                </asp:CheckBoxList>
            </div>
        </div>
    </div>
    <div runat="server" id="dvStatusPolis" class="form_box">
        <div class="form_single">
            <label class="label_general">
                Status Polis
            </label>
            <asp:DropDownList runat="server" ID="cboStatusPolis">
                <asp:ListItem Text="SUDAH TERIMA" Value="O"></asp:ListItem>
                <asp:ListItem Text="BELUM TERIMA" Value="N"></asp:ListItem>
            </asp:DropDownList>
        </div>
    </div>
    <div runat="server" id="dvYear" class="form_box">
        <div class="form_single">
            <label class="label_general">
                Tahun
            </label>
            <asp:TextBox runat="server" ID="txtYear" CssClass="small_text"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator6" ControlToValidate="txtYear"
                Display="Dynamic" ErrorMessage="Isi Tahun" CssClass="validator_general"></asp:RequiredFieldValidator>
        </div>
    </div>
    <div runat="server" id="dvInvoice" class="form_box">
        <div class="form_single">
            <label class="label_general">
                Invoice No
            </label>
            <asp:TextBox runat="server" ID="txtInvoice" CssClass="small_text"></asp:TextBox>
           <%-- <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator7" ControlToValidate="txtInvoice"
                Display="Dynamic" ErrorMessage="Isi Nomor Tagihan" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
        </div>
    </div>
  <%--  <div runat="server" id="dvInsuranceCompany" class="form_box">
        <div class="form_single">
            <label class="label_general">
                Asuransi
            </label>
            <asp:DropDownList runat="server" ID="cboInsuranceCompany">
            </asp:DropDownList>
        </div>
    </div>--%>
    <div runat="server" id="dvTransactionType" class="form_box">
        <div class="form_left">
            <label class="label_general">
                Jenis Transaksi
            </label>
            <div class="checkbox_wrapper_ws">
                <asp:CheckBoxList runat="server" ID="chkTransactionType" RepeatDirection="Vertical"
                    RepeatLayout="Table" CssClass="opt_single">
                </asp:CheckBoxList>
            </div>
        </div>
    </div>
    <div runat="server" id="dvNoKontrak" class="form_box">
        <div class="form_single">
            <label class="label_general">
                No Kontrak
            </label>
            <asp:DropDownList ID="cboNoKontrak" runat="server">
            </asp:DropDownList>
        </div>
    </div>
    <div runat="server" id="dvMonth" class="form_box">
        <div class="form_single">
            <label class="label_general">
                Bulan</label>
            <asp:DropDownList runat="server" ID="cboMonth">
                <asp:ListItem Text="Januari" Value="1" Selected="True"></asp:ListItem>
                <asp:ListItem Text="Februari" Value="2"></asp:ListItem>
                <asp:ListItem Text="Maret" Value="3"></asp:ListItem>
                <asp:ListItem Text="April" Value="4"></asp:ListItem>
                <asp:ListItem Text="Mei" Value="5"></asp:ListItem>
                <asp:ListItem Text="Juni" Value="6"></asp:ListItem>
                <asp:ListItem Text="Juli" Value="7"></asp:ListItem>
                <asp:ListItem Text="Agustus" Value="8"></asp:ListItem>
                <asp:ListItem Text="September" Value="9"></asp:ListItem>
                <asp:ListItem Text="Oktober" Value="10"></asp:ListItem>
                <asp:ListItem Text="November" Value="11"></asp:ListItem>
                <asp:ListItem Text="Desember" Value="12"></asp:ListItem>
            </asp:DropDownList>
        </div>
    </div>
    <div runat="server" id="dvNoAplikasi" class="form_box">
        <div class="form_single">
            <label class="label_general">
                No Aplikasi</label>
            <asp:DropDownList runat="server" ID="cboNoAplikasi">
            </asp:DropDownList>
        </div>
    </div>
    <div runat="server" id="dvPilihan" class="form_box">
        <div class="form_left">
            <label class="label_general">
                Pilihan</label>
            <asp:CheckBoxList runat="server" ID="chkPilihan" RepeatDirection="Vertical" RepeatLayout="Table"
                CssClass="opt_single">
                <asp:ListItem Text="DAILY" Value="D" Selected="True"></asp:ListItem>
                <asp:ListItem Text="MONTHLY" Value="M"></asp:ListItem>
            </asp:CheckBoxList>
        </div>
    </div>
     <div runat="server" id="dvAPType" class="form_box">
        <div class="form_left">
            <label class="label_general">
                <%--AP Type--%>
                Jenis Pembayaran/Pencairan
            </label>
             <asp:DropDownList runat="server" ID="cboAPType">
              
            </asp:DropDownList>
        </div>
    </div>
    <%--<div runat="server" id="dvChkAssetType" class="form_box">
        <div class="form_single">
            <label class="label_general">
                AssetType
            </label>
            <asp:DropDownList runat="server" ID="chkAssetTypeID">
            </asp:DropDownList>
        </div>
    </div>--%>
    <div runat="server" id="dvPilihTypeTanggal" class="form_box">
         <div class="form_single">
            <label class="label_general">
                Pilih Type Tanggal</label>
            <asp:RadioButtonList runat="server" ID="chkPilihTypeTanggal" >
                <asp:ListItem Text="Activity Date" Value="A" Selected="True"></asp:ListItem>
                <asp:ListItem Text="Promise Date" Value="P"></asp:ListItem>
            </asp:RadioButtonList>
        </div>
    </div>
    <div runat="server" id="dvsearchby" class="form_box" visible="false">
            <%--<uc2:ucsearchby id="oSearchBy" runat="server"></uc2:ucsearchby>            --%>
                <label>Cari Berdasarkan</label>
                <asp:dropdownlist id="cmbSearchList" runat="server">
                <asp:ListItem Text="Customer" Value="cust.Name" Selected="true"></asp:ListItem>
                <asp:ListItem Text="Agreement No" Value="pdc_cte.ListKontrak"></asp:ListItem>
                </asp:dropdownlist>
	            <asp:textbox id="txtSearch" runat="server" ></asp:textbox>
    </div>
    <div class="form_button">
        <asp:Button runat="server" ID="btnView" Text="View Report" CssClass="small button blue" />
    </div>
    
    </form>
   
</body>
</html>
