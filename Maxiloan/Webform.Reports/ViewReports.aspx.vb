﻿Imports System.Data.SqlClient
Imports Microsoft.Reporting.WebForms
Imports Maxiloan.Webform.UserController

Public Class ViewReports
    Inherits Maxiloan.Webform.WebBased
#Region "Property"
    Property ReportGroupID() As String
        Get
            Return ViewState("ReportGroupID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ReportGroupID") = Value
        End Set
    End Property

#End Region
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not Page.IsPostBack Then
            Me.ReportGroupID = Request.QueryString("ReportGroupID").Trim
            BindGridViewEntity(Me.ReportGroupID)
        End If
    End Sub
    Sub BindGridViewEntity(ByVal rptGroupID As String)
        Dim objCommandDetail As New SqlCommand
        Dim objconnectionDetail As New SqlConnection(GetConnectionString)
        Dim objreadDetail As SqlDataReader
        If objconnectionDetail.State = ConnectionState.Closed Then objconnectionDetail.Open()
        objCommandDetail.CommandType = CommandType.StoredProcedure
        objCommandDetail.Connection = objconnectionDetail
        objCommandDetail.CommandText = "spViewTblReportSetting"
        objCommandDetail.Parameters.Add("@ReportGroupID", SqlDbType.VarChar, 10).Value = rptGroupID.Trim
        objreadDetail = objCommandDetail.ExecuteReader()

        GridView1.DataSource = objreadDetail
        GridView1.DataBind()
        objreadDetail.Close()
    End Sub


    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As GridViewCommandEventArgs) Handles GridView1.RowCommand

        If (e.CommandName = "Select") Then
            Dim irsc As IReportServerCredentials = New CustomReportCredentials(Me.UIDRS, Me.PassRS, Me.ServerNameRS)

            Dim gv As GridView = CType(sender, GridView)
            Dim rowIndex As Int32 = Convert.ToInt32(e.CommandArgument.ToString())
            Dim lblPath As Label = CType(gv.Rows(rowIndex).FindControl("lblPath"), Label)
            Dim cbIsBranch As CheckBox = CType(gv.Rows(rowIndex).FindControl("cbIsBranch"), CheckBox)
            With ReportViewer1
                .ProcessingMode = ProcessingMode.Remote
                .ServerReport.ReportServerCredentials = irsc
                .ServerReport.ReportServerUrl = New Uri(Me.VirtualDirectoryRS)
                .ServerReport.ReportPath = lblPath.Text.Trim
                .ShowParameterPrompts = True
                .ShowRefreshButton = True
                .ShowWaitControlCancelLink = True
                .ShowBackButton = True
                .ShowCredentialPrompts = True

                Dim parametersCollection = New List(Of ReportParameter)()

                Dim paramList As New List(Of ReportParameter)()

                If cbIsBranch.Checked = True Then
                    If Not (Me.IsHoBranch) Then
                        paramList.Add(New ReportParameter("Cabang", Me.sesBranchId.Replace("'", "").Trim, False))
                    Else
                        paramList.Add(New ReportParameter("Cabang", "", True))
                    End If
                Else

                End If


                .ServerReport.SetParameters(paramList)

                .ServerReport.Refresh()
            End With


        End If
    End Sub
End Class