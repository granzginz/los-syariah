﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AddandEDITreportsSet.aspx.vb" Inherits="Maxiloan.Webform.Reports.AddandEDITreportsSet" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   
    <title>ADDEDITREPORTSSET</title>

    
     <link rel="Stylesheet" type="text/css" href="../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../Include/Buttons.css" />
    
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <link href="../include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../js/jquery-1.9.1.min.js"></script>
    <script src="../js/jquery-ui-1.10.3.custom.min.js"></script>
</head>
<body>
 
     <asp:UpdateProgress ID="upg1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
      <asp:UpdatePanel runat="server" ID="UpdatePanel1">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
                onclick="hideMessage();"></asp:Label>
    <div runat="server" id="jlookupContent" />
            <asp:HiddenField ID="hdnID" runat="server" />
             <asp:HiddenField ID="hdnReportGroupId" runat="server" />
      <div class="form_title">  <div class="title_strip"> </div>
            <div class="form_single">
                <h4>
                   SETING REPORT
                </h4>
            </div>
        </div>
        <div class="form_box">
         <div class="form_left">
                <label>
                    Kode Group Reports
                </label>
                <asp:Label ID="lblReportGroupID" runat="server" EnableViewState="False"></asp:Label>
            </div>
         </div>
         <div class="form_box">
          <div class="form_left">
                <label>
                   Nama Group Reports
                </label>
                <asp:Label ID="lblReportGroupName" runat="server" EnableViewState="False"></asp:Label>
        </div>
        </div>
        <div class="form_box">
          <div class="form_single">
                <div style="float:left">
                <label class="label_req">Nama Reports</label>
                  <asp:HiddenField runat="server" ID="hdnItemID" />  
                
                     <%-- <asp:TextBox runat="server" ID="txtaplikasiid" Enabled="false" CssClass="medium_text"></asp:TextBox>--%>
                <asp:TextBox runat="server" ID="txtNameReport" Enabled="false" CssClass="medium_text"></asp:TextBox>
                  <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" ControlToValidate="txtNameReport"
                        Display="Dynamic" ErrorMessage="Harap isi nama reportnya" CssClass="validator_general"></asp:RequiredFieldValidator>
               </div>
               <div  style="margin-top:2px">
                
                     <button class="small buttongo blue" style="display:inherit" onclick ="showPopup(); return false;">...</button>  
               </div>
               </div>
               </div>
        <div class="form_box">
           <div class="form_single">
                <label class="label_req">
                   URL Report
                </label>
               <asp:TextBox runat="server" ID="txtPath" Enabled="false" CssClass="medium_text"></asp:TextBox>
                  <asp:RequiredFieldValidator ID="Requiredfieldvalidator4" runat="server" ControlToValidate="txtPath"
                        Display="Dynamic" ErrorMessage="Harap isi URL reportnya" CssClass="validator_general"></asp:RequiredFieldValidator>
         </div>
         </div>
         <div class="form_box">
          <div class="form_left">
                <label class="label_req">
                   Keterangan
                </label>
                <asp:TextBox runat="server" ID="txtDescription"  CssClass="medium_text"></asp:TextBox>
                   <asp:RequiredFieldValidator ID="Requiredfieldvalidator3" runat="server" ControlToValidate="txtDescription"
                        Display="Dynamic" ErrorMessage="Harap nama reportnya" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>

    <div class="form_box">
          <div class="form_left">
                <label>
                   Parameter Cabang
                </label>
              <asp:CheckBox ID="chbIsBranch" runat="server" />
            </div>
        </div>

  
      
       
        <div class="form_button">
            <asp:Button ID="BtnSave" runat="server" CausesValidation="true" Text="Save" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtnCancel" runat="server" CausesValidation="false" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
  </ContentTemplate>
        
    </asp:UpdatePanel>
    </form>
   
     <script type="text/javascript">
         function showPopup() {
             var reportGroupID = document.getElementById("lblReportGroupID").innerHTML;

              OpenJLookup('<%= ResolveClientUrl("~/webform.Utility/jLookup/CatalogReports.aspx?itemID=" & hdnItemID.ClientID & "&path=" & txtPath.ClientID  & "&name=" & txtNameReport.ClientID  & "&description=" & txtDescription.ClientID  &  "&reportGroupID=' + reportGroupID + '" ) %>', 'Daftar Akun', '<%= jlookupContent.ClientID %>', 'HandleLookupPaymentAlloc');
         };
     </script> 
</body>
</html>
