﻿Imports System.Globalization

Public Class ucDateCE
    Inherits ControlBased

    Public Event TextChanged As TextChangedHandler
    Public Delegate Sub TextChangedHandler(ByVal sender As Object, ByVal e As System.EventArgs)

    Private Sub txtDateCE_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDateCE.TextChanged
        RaiseEvent TextChanged(sender, e)
    End Sub

    Public Function GetDate() As Date
        Dim pdate As Date = Now
        Date.TryParseExact(txtDateCE.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, pdate)
        Return pdate
    End Function
    Public Property Text As String
        Get
            Return txtDateCE.Text.Trim
        End Get
        Set(ByVal value As String)
            txtDateCE.Text = value
        End Set
    End Property

    Public WriteOnly Property IsRequired As Boolean
        Set(ByVal value As Boolean)
            rfvDateCE.Enabled = value
        End Set
    End Property

    Public WriteOnly Property Enabled As Boolean
        Set(ByVal value As Boolean)
            txtDateCE.Enabled = value
        End Set
    End Property

    Public Property AutoPostBack As Boolean
        Get
            Return txtDateCE.AutoPostBack
        End Get
        Set(ByVal value As Boolean)
            txtDateCE.AutoPostBack = value
        End Set
    End Property



End Class