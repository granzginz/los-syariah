﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucProspectTab.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.ucProspectTab" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<div class="tab_container">
    <div id="tabAplikasi" runat="server">
        <asp:HyperLink ID="hypAplikasi" runat="server">APLIKASI</asp:HyperLink></div>
    <div id="tabAsset" runat="server">
        <asp:HyperLink ID="hypAsset" runat="server">UNIT & PINJAMAN</asp:HyperLink></div>
    <div id="tabDemografi" runat="server">
        <asp:HyperLink ID="hypDemografi" runat="server">DEMOGRAFI</asp:HyperLink></div>
    <div id="tabFinancial" runat="server">
        <asp:HyperLink ID="hypFinancial" runat="server">FINANCIAL EXP</asp:HyperLink></div>
</div>
