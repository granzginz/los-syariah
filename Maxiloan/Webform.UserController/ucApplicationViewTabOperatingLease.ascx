﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucApplicationViewTabOperatingLease.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.ucApplicationViewTabOperatingLease" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<div class="tab_container_view">
<div class="tab_container">
    <div id="tabAplikasi" runat="server">
        <asp:HyperLink ID="hypAplikasi" runat="server">APLIKASI</asp:HyperLink></div>
    <div id="tabAsset" runat="server">
        <asp:HyperLink ID="hypAsset" runat="server">ASSET</asp:HyperLink></div>
    <div id="tabAsuransi" runat="server">
        <asp:HyperLink ID="hypAsuransi" runat="server">ASURANSI</asp:HyperLink></div>
    <div id="tabFinancial" runat="server">
        <asp:HyperLink ID="hypFinancial" runat="server">FINANCIAL-1</asp:HyperLink></div>
    <div id="tabFinancial2" runat="server">
        <asp:HyperLink ID="hypFinancial2" runat="server">FINANCIAL-2</asp:HyperLink></div>
   <%-- <div id="tabRefund" runat="server">
        <asp:HyperLink ID="hypRefund" runat="server">INSENTIF</asp:HyperLink></div>--%>
    <div id="tabSurvey" runat="server">
        <asp:HyperLink ID="hypSurvey" runat="server">SURVEY & KYC</asp:HyperLink></div>
</div>
</div>