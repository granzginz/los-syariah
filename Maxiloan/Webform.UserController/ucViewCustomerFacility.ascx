﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucViewCustomerFacility.ascx.vb" Inherits="Maxiloan.Webform.UserController.ucViewCustomerFacility" %>
 <div class="form_title">
    <div class="form_single">
        <h3>DATA FASILITAS</h3>
    </div>
</div>
<div class="form_box">
    <div class="form_left">
        <label>Nama Fasilitas</label>
        <asp:Label runat="server" ID="lblNamaFasilitas"></asp:Label>
    </div>
    <div class="form_right">
        <label>No Fasilitas</label>
        <asp:Label runat="server" ID="lblNoFasilitas"></asp:Label>
    </div>
</div>
<div class="form_box">
    <div class="form_left">
        <label>Periode Pencairan</label>
        <asp:Label runat="server" ID="lblPeriode"></asp:Label>
    </div>
    <div class="form_right">
        <label>Type Fasilitas</label>
        <asp:Label runat="server" ID="lblFasilitasType"></asp:Label>
    </div>
</div>
<div class="form_box">
    <div class="form_left">
        <label>Plafond</label>
        <asp:Label runat="server" ID="lblPlafond"></asp:Label>
    </div>
    <div class="form_right">
        <label>Sisa Plafond</label>
        <asp:Label runat="server" ID="lblSisaPlafond"></asp:Label>
    </div>
</div>
<div class="form_box">
    <div class="form_left">
        <label>Minimum Pencairan/Batch</label>
        <asp:Label runat="server" ID="lblMinimum"></asp:Label>
    </div>
    <div class="form_right">
        <label>Application Module</label>
        <asp:Label runat="server" ID="lblApplicationModule"></asp:Label>
    </div>
</div>   
<div class="form_box">
    <div class="form_left">
        <label>Tanggal Akhir Fasilitas</label>
        <asp:Label runat="server" ID="lbltglakhirfasilitas"></asp:Label>
    </div>
    <div class="form_right">

    </div>
</div> 
