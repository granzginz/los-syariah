﻿Public Class ucText
    Inherits System.Web.UI.UserControl

#Region "Properties"
    Public Property Text() As String
        Get
            Return CType(ViewState("text"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("text") = Value
        End Set
    End Property

    Public Property MaxLength() As Integer
        Get
            Return CType(ViewState("maxlength"), Integer)
        End Get
        Set(ByVal Value As Integer)
            ViewState("maxlength") = Value
        End Set
    End Property

    Public Property ColumnWidth() As Integer
        Get
            Return CType(ViewState("columnwidth"), Integer)
        End Get
        Set(ByVal Value As Integer)
            ViewState("columnwidth") = Value
        End Set
    End Property

    Public Property ErrorMessage() As String
        Get
            Return CType(ViewState("ErrorMessage"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ErrorMessage") = Value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Text = txtText.Text

    End Sub

    Public Sub BindText()
        txtText.Text = CType(ViewState("text"), String)
        txtText.MaxLength = CType(ViewState("maxlength"), Integer)
        txtText.Width.Pixel(CType(Me.ColumnWidth, Integer))
        rfvText.ErrorMessage = Me.ErrorMessage
        rfvText.Enabled = True
        rfvText.Visible = True
    End Sub

End Class