﻿Imports Maxiloan.Parameter
Imports Microsoft.Reporting.WebForms

Public Module RSConfig
    Public Function setCookies(ByVal oCookie As HttpCookie, ByVal oClass As Parameter.ControlsRS) As HttpCookie

        If oCookie Is Nothing Then
            Dim oCookieNew As New HttpCookie("RSCOOKIES")

            With oCookieNew
                .Values.Add("TglBayar", oClass.TglBayar)
                '.Values.Add("StopOpname", oClass.StopOpname)
                .Values.Add("Cabang", oClass.SelectedBranch)
                .Values.Add("StartDate", oClass.StartDate)
                .Values.Add("EndDate", oClass.EndDate)
                .Values.Add("KondisiSKT", oClass.KondisiSKT)
                .Values.Add("StartMonth", oClass.StartMonth)
                .Values.Add("EndMonth", oClass.EndMonth)
                .Values.Add("StartYear", oClass.StartYear)
                .Values.Add("EndYear", oClass.EndYear)

                .Values.Add("StartYear2", oClass.StartYear2)
                .Values.Add("EndYear2", oClass.EndYear2)

                .Values.Add("StartDay", oClass.StartYear2)
                .Values.Add("EndDay", oClass.EndYear2)
                .Values.Add("CollectorType_CL", oClass.CollectorType_CL)
                .Values.Add("CollectorType_D", oClass.CollectorType_D)
                .Values.Add("JenisTask", oClass.JenisTask)
                .Values.Add("   ", oClass.StatusRepo)
                .Values.Add("CA", oClass.CreditAnalist)
                .Values.Add("CMO", oClass.CMO.Replace(" ", ""))
                .Values.Add("Surveyor", oClass.Surveyor)
                .Values.Add("RekeningBankkType_C", oClass.RekeningBankkType_C)
                .Values.Add("RekeningBankkType_B", oClass.RekeningBankkType_B)
                .Values.Add("BankCompany", oClass.BankCompany)
                .Values.Add("Kontrak", oClass.Kontrak)
                .Values.Add("NoBatch", oClass.NoBatch)
                .Values.Add("Tanggal", oClass.SingleDate)
                .Values.Add("SingleDate", oClass.SingleDate)
                .Values.Add("DayDate", oClass.SingleDate)
                .Values.Add("NamaKonsumen", oClass.NamaKonsumen)
                .Values.Add("StatusPolis", oClass.StatusPolis)
                .Values.Add("TransID", oClass.TransID)
                .Values.Add("NoKontrak", oClass.NoKontrak)
                .Values.Add("Pilihan", oClass.Pilihan)
                .Values.Add("NoAplikasi", oClass.NoAplikasi)
                .Values.Add("Bulan", oClass.Bulan.ToString)
                .Values.Add("Tahun", oClass.Year.ToString)
                .Values.Add("Year", oClass.Year.ToString)
                .Values.Add("AreaID", oClass.AreaID.ToString)
                .Values.Add("Kasir", oClass.Kasir.ToString)
                .Values.Add("StartDate1", oClass.StartDate1)
                .Values.Add("EndDate1", oClass.EndDate1)
                .Values.Add("RekBank", oClass.RekBank)
                .Values.Add("StartDateTime", oClass.StartDateTime)
                .Values.Add("EndDateTime", oClass.EndDateTime)
                .Values.Add("StartTime", oClass.StartTime)
                .Values.Add("EndTime", oClass.EndTime)
                .Values.Add("ApType", oClass.ApType)
                .Values.Add("CabangAsuransi", oClass.CabangAsuransi)
                .Values.Add("NoFaktur", oClass.Invoice)
                .Values.Add("Month", oClass.Month)
                .Values.Add("StockOpNo", oClass.StockOpname)
                .Values.Add("CollectorType_Grp", oClass.CollectorType_Grp)
                .Values.Add("Assettype", oClass.ChkAssetType)
                .Values.Add("TypeTanggal", oClass.TypeTanggal)
                .Values.Add("SearchBy", oClass.SearchBy)
                '.Values.Add("StatusBPKB", oClass.StatusBPKB)

            End With

            Return oCookieNew
        Else
            With oCookie
                .Values("TglBayar") = oClass.TglBayar
                ' .Values("StopOpname") = oClass.StopOpname
                .Values("Cabang") = oClass.SelectedBranch
                .Values("StartDate") = oClass.StartDate
                .Values("EndDate") = oClass.EndDate
                .Values("KondisiSKT") = oClass.KondisiSKT
                .Values("StartMonth") = oClass.StartMonth
                .Values("EndMonth") = oClass.EndMonth
                .Values("StartYear") = oClass.StartYear
                .Values("EndYear") = oClass.EndYear

                .Values("StartYear2") = oClass.StartYear2
                .Values("EndYear2") = oClass.EndYear2

                .Values("StartDay") = oClass.StartYear2
                .Values("EndDay") = oClass.EndYear2

                .Values("CollectorType_CL") = oClass.CollectorType_CL
                .Values("CollectorType_D") = oClass.CollectorType_D
                .Values("JenisTask") = oClass.JenisTask
                .Values("StatusRepo") = oClass.StatusRepo
                .Values("CA") = oClass.CreditAnalist
                .Values("CMO") = oClass.CMO.Replace(" ", "")
                .Values("Surveyor") = oClass.Surveyor
                .Values("RekeningBankkType_C") = oClass.RekeningBankkType_C
                .Values("RekeningBankkType_B") = oClass.RekeningBankkType_B
                .Values("RekeningBankAccount") = oClass.RekeningBankAccount
                .Values("BankCompany") = oClass.BankCompany
                .Values("Kontrak") = oClass.Kontrak
                .Values("NoBatch") = oClass.NoBatch
                .Values("Tanggal") = oClass.SingleDate
                .Values("SingleDate") = oClass.SingleDate
                .Values("DayDate") = oClass.SingleDate
                .Values("StatusPolis") = oClass.StatusPolis
                .Values("TransID") = oClass.TransID
                .Values("NoKontrak") = oClass.NoKontrak
                .Values("Pilihan") = oClass.Pilihan
                .Values("NoAplikasi") = oClass.NoAplikasi
                .Values("Bulan") = oClass.Bulan.ToString
                .Values("Tahun") = oClass.Year.ToString
                .Values("Year") = oClass.Year.ToString
                .Values("AreaID") = oClass.AreaID.ToString
                .Values("Kasir") = oClass.Kasir.ToString
                .Values("StartDate1") = oClass.StartDate1
                .Values("EndDate1") = oClass.EndDate1
                .Values("RekBank") = oClass.RekBank
                .Values("StartDateTime") = oClass.StartDateTime
                .Values("EndDateTime") = oClass.EndDateTime
                .Values("StartTime") = oClass.StartTime
                .Values("EndTime") = oClass.EndTime
                .Values("ApType") = oClass.ApType
                .Values("CabangAsuransi") = oClass.CabangAsuransi
                .Values("NoFaktur") = oClass.Invoice
                .Values("Month") = oClass.Month
                .Values("StockOpNo") = oClass.StockOpname
                .Values("CollectorType_Grp") = oClass.CollectorType_Grp
                .Values("Assettype") = oClass.ChkAssetType
                .Values("TypeTanggal") = oClass.TypeTanggal
                .Values("SearchBy") = oClass.SearchBy
                '.Values("StatusBPKB") = oClass.StatusBPKB
            End With
            Return oCookie
        End If
    End Function

    Sub ObjectFalse()

        Dim oClass As New Parameter.ControlsRS
        With oClass
            .isFilterChkAssetType = False
            .isFilterTglBayar = False
            .isFilterStockOpname = False
            .isFilterBulanTahun = False
            .isFilterArea = False
            .isFilterBranch = False
            .isFilterSingleDate = False
            .isFilterRangeDate = False
            .isFilterKondisiSKT = False
            .isFilterPeriodLapARODStatCMO = False
            .isFilterCollectorCL = False
            .isFilterCollectorD = False
            .isFilterCreditAnalist = False
            .isFilterCMO = False
            .isFilterSurveyor = False
            .isFilterRekeningBankType_B = False
            .isFilterRekeningBankType_C = False
            .isFilterChkColl_CL = False
            .isFilterKasir = False
            .isFilterRekeningBankAccount = False
            .isFilterBankCompany = False
            .isFilterKontrak = False
            .isFilterNoBatch = False
            .isFilterNamaKonsumen = False
            .isFilterCabangAsuransi = False
            .isFilterStatus = False
            .isFilterStatusPolis = False
            .isFilterYear = False
            .isFilterInvoice = False
            .isFilterTransactionType = False
            .isFilterNoKontrak = False
            .isFilterMonth = False
            .isFilterPilihan = False
            .isFilterStatusBPKB = False
            .isFilterAPType = False
            .isFilterCollectorGrp = False
            .isFilterTypeTanggal = False
            .isSearchBy = False
        End With
    End Sub

    Public Function setControls(ByVal oClass As Parameter.ControlsRS) As Parameter.ControlsRS
        With oClass

            ObjectFalse()
            .isFilterBranch = .isHOBRanch

            Select Case .FormID.ToUpper
                Case "FUNDPREPYT"
                    .isFilterTglBayar = True
                Case "STKOPNBPKB"
                    .isFilterStockOpname = True
                Case "REALJUALSUPP", "REALJUALTENOR", "LAPTARIKASSET", "LAPANGSBANK", "LAPANGSBEDACAB", "PETTYCASHDTL", "PETTYCASHSUM",
                    "REKREALISASIBAYAR", "LAPJUALASSET", "DAFPERIKSABPKB", "LAPJUALASSET", "DAFPERIKSABPKB", "LAPJUALASSET", "MUTDOKBPKB", "LAPPELUNASAN", "LAPSUSPEND", "HASILAPKMASUK", "LAPAPK", "LAPMGTCA", "LAPPERFCA", "REKAPAPKMASUK", "REKAPCAIRKREDIT", "STATSPKDANPO", "DAFTJURNALDETBYAKUN", "ADFPAYMENT",
                    "REKREALISASIBAYARLAIN", "HAPUSFIDUCIA", "DAFTFIDUCIA", "RPTAPPPENDING", "APPMONRPT", "AGDOWNLOAD", "LAPWO", "LAPKONTRAKLNS", "PDCDEPRPT", "RPTDAPDCPOST", "LAPORANPPH",
                     "PPDROPAID", "PPDROREQUEST"
                    .isFilterRangeDate = True
                Case "REKRENCANABAYAR"
                    .isFilterRangeDate = True
                    '.isFilterAPType = True
                Case "LAPANGSTUNAI"
                    .isFilterKasir = True
                    .isFilterSingleDateTime = True
                Case "R_PRODUKNASIONAL", "PAYMENTVOUCHER"
                    .isFilterRangeDate = True
                Case "R_ANALISASCORES", "R_PRODUKPASSENGER", "R_ANALISADOWNPAY", "R_ANALISATENOR", "R_ANALISAKONDISI", "R_ANALISAUSIA", "R_ANALISAMERK", "R_ANALISAPENDAPATAN", "R_ANALISAKATEGORI", "R_ANALISNK", "R_ANALISCOMMERCIAL", "R_ANALISPOKOK", "R_ANALISANGSURAN", "R_ANALISAUNITCAB", "R_ANALISAPRODCAB", "R_ANALISACMO", "R_ANALISADEALER"
                    .isFilterBulanTahun = True
                Case "R_ANALISARATEWIL"
                    .isFilterBulanTahun = True
                    .isFilterArea = True
                Case "R_ANALISARATECAB"
                    .isFilterBulanTahun = True
                Case "SKTNOCOLL", "TVCMONBOARD", "LAPSKTTANPAEKS", "STOPACC", "DPDPRINCIPALACTUAL"
                Case "DAFTARSKT"
                    .isFilterRangeDate = True
                    .isFilterKondisiSKT = True
                Case "LAPARODCMO", "LAPARODSUPPL", "LAPARSUMMARY", "BPKBODSUM", "LAPBPKBBLMTRM", "FUTUREAMOUNT", "DAILYBPKB", "LAPACCINTRS", "APINSURANCE", "ARMUTATION", "DPDBYUNIT", "DPDFORCASH", "DPDPRINFORCAST",
                    "PAYMENTINDOMART"
                    .isFilterSingleDate = True
                Case "LAPARODCOLL", "DPDCOLLFORCAST", "DPDTIMSUS", "DPDNS", "OLDLOANBALANCE", "LAPDPDBYCOLLECTOR"
                    .isFilterSingleDate = True
                    .isFilterChkAssetType = True

                    ''Laporan DPD Actual
                Case "LAPARODCOLLACT"
                    .isFilterChkAssetType = True

                Case "LAPARDETAIL"
                    .isFilterSingleDate = True
                    .isFilterCollectorGrp = True
                    .isFilterCollectorCL = True
                Case "LAPTUNGSUM"
                    .isFilterBranch = False
                    .isFilterSingleDate = True
                Case "LAPARODSTATCMO"
                    .isFilterPeriodLapARODStatCMO = True
                Case "LAPCREDITDELIQ"
                    .isFilterSingleDate = True
                    .isFilterLmNunggak = True
                    'Case "LAPHASILKUNCOLL"
                    '    .isFilterRangeDate = True
                    '    .isFilterCollectorCL = True
                Case "LAPKEGDESKCOLL"
                    .isFilterRangeDate = True
                    .isFilterCollectorD = True
                    .isFilterCollectorDALL = True
                Case "LAPKENDTARIKAN", "DAFPERIKSAAS", "LAPENDORS", "KOJATEM", "LOANPORTOFOLIO"
                    .isFilterRangeDate = True
                Case "LAPSTATUSASSET"
                    .isFilterSingleDate = True
                    .isFilterStatus = True
                Case "LAPTUNGGCA"
                    .isFilterSingleDate = True
                    .isFilterCreditAnalist = True
                Case "LAPTUNGGCMO"
                    .isFilterSingleDate = True
                    .isFilterCMO = True
                Case "LAPTUNGGSVY"
                    .isFilterSingleDate = True
                    .isFilterCreditAnalist = True
                    .isFilterSurveyor = True
                Case "DAFTRANREKBANK", "DAFTRANREKBANKDETAIL", "PEMTRANREKBANKDETAIL", "DAFTRANREKBANKALL", "DAFPEMANGBANK"
                    .isFilterRangeDate = True
                    .isFilterRangeDate1 = True
                    .isFilterRekBank = True

                Case "LAPTRANSUSP"
                    .isFilterRangeDate = True
                    .isFilterRangeDate1 = True
                    .isFilterRekBank = True

                Case "DAFTRANREKTUNAI", "LAPALOKSUSP"
                    '.isFilterSingleDate = True
                    '.isFilterCreditAnalist = True
                    '.isFilterRekeningBankType_C = True
                    .isFilterRangeDate = True
                    .isFilterRekBank = True
                    'Case "LAPALOKSUSP"
                    '    .isFilterRangeDate = True
                    '    .isFilterRekBank = True
                    'Case "LAPANGSCOLL", "LAPHASILKUNCOLL"
                Case "LAPANGSCOLL", "LAPTAGIHANJT"
                    .isFilterRangeDate = True
                    .isFilterCollectorCL = True
                Case "LAPOUSTSUSP"
                    .isFilterRangeDate = True
                    .isFilterRangeDate1 = True
                    .isFilterRekBank = True
                Case "LAPTRANHARIKASIR"
                    .isFilterSingleDate = True
                    '.isFilterRangeDate = True
                    '.isFilterRekeningBankType_B = True
                    .isFilterKasir = True
                Case "LAPTRANKASBANK"
                    .isFilterRangeDate = True
                    .isFilterRekeningBankAccount = True
                Case "PROYTERIMAANGS", "LAPPINJBANK", "LAPPROANGSBANK", "PROYTERIMAANGSURAN", "ANGSBANKJT", "FUNINSTPAID", "FUNPREPAY", "JADWALANGSBYJT", "JADWALANGSSUM"
                    .isFilterRangeDate = True
                Case "DAFJAMTPPOLIS", "DAFJAMTPBPKB"
                    .isFilterBankCompany = True
                Case "TANDATERIMAJAM"
                    .isFilterSingleDate = True
                    .isFilterBankCompany = True
                    .isFilterKontrak = True
                    .isFilterNoBatch = True

                Case "FUNBATCHINSTJF", "FUNBATCHINSTTL"
                    .isFilterBankCompany = True
                    .isFilterKontrak = True
                    .isFilterNoBatch = True
                Case "ANGSBANKDTL"
                    .isFilterRangeDate = True
                    .isFilterBankCompany = True
                Case "DAFANGSBANKBATCH"
                    .isFilterBankCompany = True
                    .isFilterKontrak = True
                    .isFilterNoBatch = True
                Case "DAFANGSBANKKONS"
                    .isFilterBankCompany = True
                    .isFilterKontrak = True
                    .isFilterNoBatch = True
                    .isFilterNamaKonsumen = True
                Case "LAPOUSTPINJBANK", "COLLPFMNC", "FLOWBUCKET"
                    .isFilterSingleDate = True
                Case "DAFTANGAS"                '
                    .isFilterRangeDate = True
                    .isFilterCabangAsuransi = True
                Case "DAFTERIMAPOLIS"
                    .isFilterRangeDate = True
                    .isFilterCabangAsuransi = True
                    .isFilterStatusPolis = True
                Case "LAPHUTANGAS"
                    .isFilterRangeDate = True
                    .isFilterCabangAsuransi = True
                Case "LAPSTATAS"
                    .isFilterYear = True
                Case "LAPTAGAS"
                    .isFilterCabangAsuransi = True
                    .isFilterInvoice = True
                Case "DAFJURNALDTL", "DAFJURNALSUM", "RCVRPT"
                    .isFilterRangeDate = True
                    '.isFilterTransactionType = True
                Case "HITUNGDENDA"
                    .isFilterSingleDate = True
                    .isFilterNoKontrak = True
                    'Case "LAPACCINTRS"
                    '    .isFilterYear = True
                    '    .isFilterMonth = True
                Case "LAPPENDBUNGA"
                    .isFilterRangeDate = True
                    .isFilterPilihan = True
                Case "HASILCRDSCOR"
                    .isFilterNoAplikasi = True
                Case "FUNAGRINSTJF"
                    .isFilterNoAplikasi = True
                Case "DAFTRIALBAL"
                    .isFilterMonth = True
                    .isFilterYear = True
                Case "REKORBOOKING"
                    '.isFilterMonth = True
                    '.isFilterYear = True
                    .isFilterSingleDate = True
                    'Case "LAPDAFBPKB"
                    '    .isFilterStatusBPKB = True
                    'Case "OLDLOANBALANCE"
                    '    .isFilterArea = True
                    '    .isFilterSingleDate = True
                Case "DPDPRINCIPAL", "OD30UP", "DAILYBPKB", "LAPODANGS2SD5", "LAPARODCMO", "LAPARODSUPPL", "LAPDAFBPKB"
                    .isFilterSingleDate = True

                    'Case "DPDBYUNIT"
                    '    .isFilterSingleDate = True
                    '    .isFilterBranch = False

                    'Case "DPDFORCASH"
                    '    .isFilterRangeDate = True
                    '    .isFilterBranch = False
                Case "REKRENCANABAYARLAIN"
                    .isFilterRangeDate = True
                    .isFilterAPType = True

                Case "LAPHASILKUNCOLL"
                    .isFilterTypeTanggal = True
                    .isFilterRangeDate = True
                    .isFilterCollectorCL = True
                    .isFilterCollectorCLALL = True
                Case "PDCJT"
                    .isFilterRangeDate = True
                    .isSearchBy = True

                Case "REALJUALCMO"
                    .isFilterRangeDate = True
                    .isFilterChkAssetType = True

                Case "DAFJIRM"
                    .isFilterSingleDate = True
                    .isFilterBranch = True

            End Select


        End With

        Return oClass
    End Function


    Public Function getRSParameters(ByVal FormID As String, ByVal oCookie As HttpCookie) As ReportParameter()

        Select Case FormID.ToUpper
            ', "REKRENCANABAYAR"
            Case "REALJUALSUPP", "REALJUALTENOR", "LAPTARIKASSET", "LAPANGSBANK", "LAPANGSBEDACAB", "PETTYCASHDTL", "PETTYCASHSUM",
                "REKREALISASIBAYAR", "LAPJUALASSET", "DAFPERIKSABPKB", "LAPJUALASSET", "DAFPERIKSABPKB", "LAPJUALASSET", "MUTDOKBPKB", "LAPPELUNASAN", "LAPSUSPEND", "HASILAPKMASUK", "LAPAPK", "LAPMGTCA", "LAPPERFCA", "REKAPAPKMASUK", "REKAPCAIRKREDIT", "STATSPKDANPO", "DAFTJURNALDETBYAKUN", "ADFPAYMENT", "REKREALISASIBAYARLAIN", "HAPUSFIDUCIA", "DAFTFIDUCIA", "HAPUSFIDUCIA", "DAFTFIDUCIA", "RPTAPPPENDING", "APPMONRPT",
                "AGDOWNLOAD", "LAPWO", "LAPKONTRAKLNS", "PDCDEPRPT", "RPTDAPDCPOST", "LAPORANPPH", "PPDROPAID", "PPDROREQUEST"

                Dim oParameters() As ReportParameter = New ReportParameter(2) {}

                oParameters(0) = New ReportParameter("Cabang", Split(oCookie.Values("Cabang"), ","))
                oParameters(1) = New ReportParameter("StartDate", oCookie.Values("StartDate"))
                oParameters(2) = New ReportParameter("EndDate", oCookie.Values("EndDate"))

                Return oParameters
            Case "REKRENCANABAYAR"
                Dim oParameters() As ReportParameter = New ReportParameter(2) {}

                oParameters(0) = New ReportParameter("Cabang", Split(oCookie.Values("Cabang"), ","))
                oParameters(1) = New ReportParameter("StartDate", oCookie.Values("StartDate"))
                oParameters(2) = New ReportParameter("EndDate", oCookie.Values("EndDate"))
                'oParameters(3) = New ReportParameter("APType", oCookie.Values("APType"))
                Return oParameters
            Case "DPDPRINCIPAL", "OD30UP", "DAILYBPKB", "LAPODANGS2SD5", "BPKBODSUM", "LAPBPKBBLMTRM", "LAPACCINTRS", "LAPARODCMO", "LAPARODSUPPL", "LAPARSUMMARY", "LAPDAFBPKB", "HAPUSFIDUCIA", "DAFTFIDUCIA", "APINSURANCE", "ARMUTATION", "DPDBYUNIT", "DPDFORCASH",
                "DPDPRINFORCAST", "PAYMENTINDOMART", "REKORBOOKING", "FLOWBUCKET"

                Dim oParameters() As ReportParameter = New ReportParameter(1) {}
                oParameters(0) = New ReportParameter("Cabang", Split(oCookie.Values("Cabang"), ","))
                oParameters(1) = New ReportParameter("Tanggal", oCookie.Values("Tanggal"))

                Return oParameters

            Case "LAPARODCOLL", "DPDCOLLFORCAST", "DPDTIMSUS", "DPDNS", "OLDLOANBALANCE", "LAPDPDBYCOLLECTOR"

                Dim oParameters() As ReportParameter = New ReportParameter(2) {}
                oParameters(0) = New ReportParameter("Cabang", Split(oCookie.Values("Cabang"), ","))
                oParameters(1) = New ReportParameter("Tanggal", oCookie.Values("Tanggal"))
                oParameters(2) = New ReportParameter("Assettype", oCookie.Values("Assettype"))

                Return oParameters

                ''Laporan DPD Actual

            Case "LAPARODCOLLACT"

                Dim oParameters() As ReportParameter = New ReportParameter(1) {}
                oParameters(0) = New ReportParameter("Cabang", Split(oCookie.Values("Cabang"), ","))
                oParameters(1) = New ReportParameter("Assettype", oCookie.Values("Assettype"))

                Return oParameters

            Case "LAPARDETAIL"
                Dim oParameters() As ReportParameter = New ReportParameter(3) {}

                oParameters(0) = New ReportParameter("Cabang", Split(oCookie.Values("Cabang"), ","))
                oParameters(1) = New ReportParameter("Tanggal", oCookie.Values("Tanggal"))
                oParameters(2) = New ReportParameter("CollectorType_Grp", oCookie.Values("CollectorType_Grp"))
                oParameters(3) = New ReportParameter("Collector", Split(oCookie.Values("CollectorType_CL"), ","))

                Return oParameters
            Case "R_PRODUKNASIONAL", "PAYMENTVOUCHER"
                Dim oParameters() As ReportParameter = New ReportParameter(1) {}

                oParameters(0) = New ReportParameter("fDateFr", oCookie.Values("StartDate"))
                oParameters(1) = New ReportParameter("fDateTo", oCookie.Values("EndDate"))

                Return oParameters
            Case "R_ANALISASCORES", "R_PRODUKPASSENGER", "R_ANALISADOWNPAY", "R_ANALISATENOR", "R_ANALISAKONDISI", "R_ANALISAUSIA", "R_ANALISAMERK", "R_ANALISAPENDAPATAN", "R_ANALISAKATEGORI", "R_ANALISNK", "R_ANALISCOMMERCIAL", "R_ANALISPOKOK", "R_ANALISANGSURAN", "R_ANALISAUNITCAB", "R_ANALISAPRODCAB", "R_ANALISACMO", "R_ANALISADEALER"
                Dim oParameters() As ReportParameter = New ReportParameter(1) {}

                oParameters(0) = New ReportParameter("month", oCookie.Values("Bulan"))
                oParameters(1) = New ReportParameter("year", oCookie.Values("Tahun"))

                Return oParameters
            Case "R_ANALISARATEWIL"
                Dim oParameters() As ReportParameter = New ReportParameter(2) {}

                oParameters(0) = New ReportParameter("month", oCookie.Values("Bulan"))
                oParameters(1) = New ReportParameter("year", oCookie.Values("Tahun"))
                oParameters(2) = New ReportParameter("areaID", oCookie.Values("AreaID"))

                Return oParameters
            Case "R_ANALISARATECAB"
                Dim oParameters() As ReportParameter = New ReportParameter(2) {}

                oParameters(0) = New ReportParameter("month", oCookie.Values("Bulan"))
                oParameters(1) = New ReportParameter("year", oCookie.Values("Tahun"))
                oParameters(2) = New ReportParameter("branchID", oCookie.Values("Cabang"))

                Return oParameters
            Case "SKTNOCOLL", "TVCMONBOARD", "LAPSKTTANPAEKS", "STOPACC", "DPDPRINCIPALACTUAL"
                Dim oParameters() As ReportParameter = New ReportParameter(0) {}
                oParameters(0) = New ReportParameter("Cabang", Split(oCookie.Values("Cabang"), ","))
                Return oParameters

            Case "DAFTARSKT"
                Dim oParameters() As ReportParameter = New ReportParameter(3) {}
                oParameters(0) = New ReportParameter("Cabang", Split(oCookie.Values("Cabang"), ","))
                oParameters(1) = New ReportParameter("Kondisi", oCookie.Values("KondisiSKT"))
                oParameters(2) = New ReportParameter("StartDate", oCookie.Values("StartDate"))
                oParameters(3) = New ReportParameter("EndDate", oCookie.Values("EndDate"))
                Return oParameters

            Case "FUTUREAMOUNT"

                Dim oParameter() As ReportParameter = New ReportParameter(1) {}
                oParameter(0) = New ReportParameter("Cabang", Split(oCookie.Values("Cabang"), ","))
                oParameter(1) = New ReportParameter("StartDate", Split(oCookie.Values("Tanggal"), ","))
                Return oParameter

            Case "LAPARODSTATCMO"
                Dim oParameter() As ReportParameter = New ReportParameter(4) {}
                oParameter(0) = New ReportParameter("Cabang", Split(oCookie.Values("Cabang"), ","))
                oParameter(1) = New ReportParameter("StartMonth", Split(oCookie.Values("StartMonth"), ","))
                oParameter(2) = New ReportParameter("EndMonth", Split(oCookie.Values("EndMonth"), ","))
                oParameter(3) = New ReportParameter("StartYear", Split(oCookie.Values("StartYear"), ","))
                oParameter(4) = New ReportParameter("EndYear", Split(oCookie.Values("EndYear"), ","))
                Return oParameter

            Case "LAPCREDITDELIQ"
                Dim oParameter() As ReportParameter = New ReportParameter(3) {}
                oParameter(0) = New ReportParameter("Cabang", Split(oCookie.Values("Cabang"), ","))

                oParameter(1) = New ReportParameter("StartDay", Split(oCookie.Values("StartDay"), ","))
                oParameter(2) = New ReportParameter("EndDay", Split(oCookie.Values("EndDay"), ","))
                oParameter(3) = New ReportParameter("Tanggal", Split(oCookie.Values("Tanggal"), ","))
                Return oParameter

            Case "LAPKEGDESKCOLL"
                Dim oParameter() As ReportParameter = New ReportParameter(4) {}
                oParameter(0) = New ReportParameter("Cabang", Split(oCookie.Values("Cabang"), ","))
                oParameter(1) = New ReportParameter("Kolektor", Split(oCookie.Values("CollectorType_D"), ","))
                oParameter(2) = New ReportParameter("JenisTask", oCookie.Values("JenisTask"))
                oParameter(3) = New ReportParameter("StartDate", oCookie.Values("StartDate"))
                oParameter(4) = New ReportParameter("EndDate", oCookie.Values("EndDate"))
                Return oParameter

            Case "LAPKENDTARIKAN", "DAFPERIKSAAS", "LAPENDORS", "KOJATEM", "LOANPORTOFOLIO"
                Dim oParameter() As ReportParameter = New ReportParameter(2) {}
                oParameter(0) = New ReportParameter("Cabang", Split(oCookie.Values("Cabang"), ","))
                oParameter(1) = New ReportParameter("StartDate", oCookie.Values("StartDate"))
                oParameter(2) = New ReportParameter("EndDate", oCookie.Values("EndDate"))
                Return oParameter

            Case "LAPSTATUSASSET"
                Dim oParameter() As ReportParameter = New ReportParameter(2) {}
                oParameter(0) = New ReportParameter("Cabang", Split(oCookie.Values("Cabang"), ","))
                oParameter(1) = New ReportParameter("Tanggal", Split(oCookie.Values("Tanggal"), ","))
                oParameter(2) = New ReportParameter("StatusRepo", oCookie.Values("StatusRepo"))
                Return oParameter

            Case "LAPTUNGGCA"
                Dim oParameter() As ReportParameter = New ReportParameter(2) {}
                oParameter(0) = New ReportParameter("Cabang", Split(oCookie.Values("Cabang"), ","))
                oParameter(1) = New ReportParameter("Tanggal", Split(oCookie.Values("SingleDate"), ","))
                oParameter(2) = New ReportParameter("CA", oCookie.Values("CreditAnalist"))
                Return oParameter

            Case "LAPTUNGGSVY"
                Dim oParameter() As ReportParameter = New ReportParameter(2) {}
                oParameter(0) = New ReportParameter("Cabang", Split(oCookie.Values("Cabang"), ","))
                oParameter(1) = New ReportParameter("Tanggal", Split(oCookie.Values("SingleDate"), ","))
                oParameter(2) = New ReportParameter("Surveyor", oCookie.Values("Surveyor"))
                Return oParameter

            Case "DAFTRANREKBANK", "DAFTRANREKBANKDETAIL", "PEMTRANREKBANKDETAIL", "DAFTRANREKBANKALL", "DAFPEMANGBANK"
                Dim oParameter() As ReportParameter = New ReportParameter(5) {}
                oParameter(0) = New ReportParameter("Cabang", Split(oCookie.Values("Cabang"), ","))
                oParameter(1) = New ReportParameter("StartDate", oCookie.Values("StartDate"))
                oParameter(2) = New ReportParameter("EndDate", oCookie.Values("EndDate"))
                oParameter(3) = New ReportParameter("StartValDate", oCookie.Values("StartDate1"))
                oParameter(4) = New ReportParameter("EndValDate", oCookie.Values("EndDate1"))
                oParameter(5) = New ReportParameter("RekeningBank", Split(oCookie.Values("RekBank"), ","))

                Return oParameter

            Case "LAPTRANSUSP"
                Dim oParameter() As ReportParameter = New ReportParameter(5) {}
                oParameter(0) = New ReportParameter("Cabang", Split(oCookie.Values("Cabang"), ","))
                oParameter(1) = New ReportParameter("StartDate", oCookie.Values("StartDate"))
                oParameter(2) = New ReportParameter("EndDate", oCookie.Values("EndDate"))
                oParameter(3) = New ReportParameter("StartValDate", oCookie.Values("StartDate1"))
                oParameter(4) = New ReportParameter("EndValDate", oCookie.Values("EndDate1"))
                oParameter(5) = New ReportParameter("RekeningBank", oCookie.Values("RekBank"))

                Return oParameter
            Case "DAFTRANREKTUNAI", "LAPALOKSUSP"
                Dim oParameter() As ReportParameter = New ReportParameter(3) {}
                oParameter(0) = New ReportParameter("Cabang", Split(oCookie.Values("Cabang"), ","))
                oParameter(1) = New ReportParameter("StartDate", oCookie.Values("StartDate"))
                oParameter(2) = New ReportParameter("EndDate", oCookie.Values("EndDate"))
                oParameter(3) = New ReportParameter("RekeningBank", oCookie.Values("RekBank"))

                Return oParameter

                'Case "LAPANGSCOLL", "LAPHASILKUNCOLL"
            Case "LAPANGSCOLL", "LAPTAGIHANJT"
                Dim oParameter() As ReportParameter = New ReportParameter(3) {}
                oParameter(0) = New ReportParameter("Cabang", Split(oCookie.Values("Cabang"), ","))
                oParameter(1) = New ReportParameter("Kolektor", Split(oCookie.Values("CollectorType_CL"), ","))
                oParameter(2) = New ReportParameter("StartDate", Split(oCookie.Values("StartDate"), ","))
                oParameter(3) = New ReportParameter("EndDate", Split(oCookie.Values("EndDate"), ","))
                Return oParameter

            Case "LAPOUSTSUSP"
                Dim oParameter() As ReportParameter = New ReportParameter(5) {}
                'oParameter(0) = New ReportParameter("Cabang", Split(oCookie.Values("Cabang"), ","))
                'oParameter(1) = New ReportParameter("StartDate", oCookie.Values("StartDate"))
                'oParameter(2) = New ReportParameter("EndDate", oCookie.Values("EndDate"))
                ''oParameter(3) = New ReportParameter("Bank", oCookie.Values("RekeningBankkType_B"))
                'oParameter(3) = New ReportParameter("Bank", oCookie.Values("RekBank"))
                'oParameter(4) = New ReportParameter("Tanggal", Split(oCookie.Values("Tanggal"), ","))
                'Return oParameter

                oParameter(0) = New ReportParameter("Cabang", Split(oCookie.Values("Cabang"), ","))
                oParameter(1) = New ReportParameter("StartDate", oCookie.Values("StartDate"))
                oParameter(2) = New ReportParameter("EndDate", oCookie.Values("EndDate"))
                oParameter(3) = New ReportParameter("StartValDate", oCookie.Values("StartDate1"))
                oParameter(4) = New ReportParameter("EndValDate", oCookie.Values("EndDate1"))
                oParameter(5) = New ReportParameter("Bank", Split(oCookie.Values("RekBank"), ","))
                Return oParameter

            Case "LAPTRANHARIKASIR"
                Dim oParameter() As ReportParameter = New ReportParameter(2) {}
                oParameter(0) = New ReportParameter("Cabang", Split(oCookie.Values("Cabang"), ","))
                'oParameter(1) = New ReportParameter("Kasir", oCookie.Values("Kasir"))
                oParameter(1) = New ReportParameter("Kasir", Split(oCookie.Values("Kasir"), ","))
                oParameter(2) = New ReportParameter("DayDate", oCookie.Values("DayDate"))
                Return oParameter

            Case "LAPTRANKASBANK"
                Dim oParameter() As ReportParameter = New ReportParameter(3) {}
                oParameter(0) = New ReportParameter("Cabang", Split(oCookie.Values("Cabang"), ","))
                'oParameter(1) = New ReportParameter("RekBank", oCookie.Values("RekeningBankAccount"))
                oParameter(1) = New ReportParameter("RekBank", Split(oCookie.Values("RekeningBankAccount"), ","))
                oParameter(2) = New ReportParameter("StartDate", oCookie.Values("StartDate"))
                oParameter(3) = New ReportParameter("EndDate", oCookie.Values("EndDate"))
                Return oParameter

            Case "PROYTERIMAANGS", "LAPPINJBANK", "LAPPROANGSBANK", "PROYTERIMAANGSURAN", "ANGSBANKJT", "FUNINSTPAID", "FUNPREPAY", "JADWALANGSBYJT", "JADWALANGSSUM"
                Dim oParameter() As ReportParameter = New ReportParameter(1) {}
                oParameter(0) = New ReportParameter("StartDate", oCookie.Values("StartDate"))
                oParameter(1) = New ReportParameter("EndDate", oCookie.Values("EndDate"))
                Return oParameter

            Case "DAFJAMTPPOLIS", "DAFJAMTPBPKB"
                Dim oParameter() As ReportParameter = New ReportParameter(0) {}
                oParameter(0) = New ReportParameter("PerusahaanFunding", oCookie.Values("BankCompany"))
                Return oParameter

            Case "TANDATERIMAJAM"
                Dim oParameter() As ReportParameter = New ReportParameter(3) {}
                oParameter(0) = New ReportParameter("Bank", Split(oCookie.Values("BankCompany"), ","))
                oParameter(1) = New ReportParameter("Kontrak", oCookie.Values("Kontrak"))
                oParameter(2) = New ReportParameter("NoBatch", oCookie.Values("NoBatch"))
                oParameter(3) = New ReportParameter("Tanggal", oCookie.Values("Tanggal"))
                Return oParameter

            Case "FUNBATCHINSTJF", "FUNBATCHINSTTL"
                Dim oParameter() As ReportParameter = New ReportParameter(2) {}
                oParameter(0) = New ReportParameter("Kreditur", Split(oCookie.Values("BankCompany"), ","))
                oParameter(1) = New ReportParameter("NoFasilitas", oCookie.Values("Kontrak"))
                oParameter(2) = New ReportParameter("NoBatch", oCookie.Values("NoBatch"))
                Return oParameter

            Case "ANGSBANKDTL"
                Dim oParameter() As ReportParameter = New ReportParameter(2) {}
                oParameter(0) = New ReportParameter("Funding", Split(oCookie.Values("BankCompany"), ","))
                oParameter(1) = New ReportParameter("StartDate", oCookie.Values("StartDate"))
                oParameter(2) = New ReportParameter("EndDate", oCookie.Values("EndDate"))
                Return oParameter

            Case "DAFANGSBANKBATCH"
                Dim oParameter() As ReportParameter = New ReportParameter(2) {}
                oParameter(0) = New ReportParameter("Bank", Split(oCookie.Values("BankCompany"), ","))
                oParameter(1) = New ReportParameter("Kontrak", oCookie.Values("Kontrak"))
                oParameter(2) = New ReportParameter("NoBatch", oCookie.Values("NoBatch"))
                Return oParameter


            Case "DAFANGSBANKKONS"
                Dim oParameter() As ReportParameter = New ReportParameter(3) {}
                oParameter(0) = New ReportParameter("Bank", Split(oCookie.Values("BankCompany"), ","))
                oParameter(1) = New ReportParameter("Kontrak", oCookie.Values("Kontrak"))
                oParameter(2) = New ReportParameter("NoBatch", oCookie.Values("NoBatch"))
                oParameter(3) = New ReportParameter("Konsumen", oCookie.Values("NamaKonsumen"))
                Return oParameter

            Case "LAPOUSTPINJBANK", "COLLPFMNC"
                Dim oParameter() As ReportParameter = New ReportParameter(0) {}
                oParameter(0) = New ReportParameter("Tanggal", Split(oCookie.Values("SingleDate"), ","))
                Return oParameter

            Case "DAFTANGAS"
                Dim oParameter() As ReportParameter = New ReportParameter(3) {}
                oParameter(0) = New ReportParameter("Cabang", Split(oCookie.Values("Cabang"), ","))
                oParameter(1) = New ReportParameter("CabangAsuransi", oCookie.Values("CabangAsuransi"))
                oParameter(2) = New ReportParameter("StartDate", oCookie.Values("StartDate"))
                oParameter(3) = New ReportParameter("EndDate", oCookie.Values("EndDate"))
                Return oParameter

            Case "DAFTERIMAPOLIS"
                Dim oParameter() As ReportParameter = New ReportParameter(4) {}
                oParameter(0) = New ReportParameter("Cabang", Split(oCookie.Values("Cabang"), ","))
                oParameter(1) = New ReportParameter("StartDate", oCookie.Values("StartDate"))
                oParameter(2) = New ReportParameter("EndDate", oCookie.Values("EndDate"))
                'oParameter(3) = New ReportParameter("CabangAsuransi", oCookie.Values("CabangAsuransi"))
                oParameter(3) = New ReportParameter("CabangAsuransi", Split(oCookie.Values("CabangAsuransi"), ","))
                oParameter(4) = New ReportParameter("StatusPolis", oCookie.Values("StatusPolis"))
                Return oParameter

            Case "LAPHUTANGAS"
                Dim oParameter() As ReportParameter = New ReportParameter(3) {}
                oParameter(0) = New ReportParameter("Cabang", Split(oCookie.Values("Cabang"), ","))
                oParameter(1) = New ReportParameter("CabangAsuransi", oCookie.Values("CabangAsuransi"))
                oParameter(2) = New ReportParameter("StartDate", oCookie.Values("StartDate"))
                oParameter(3) = New ReportParameter("EndDate", oCookie.Values("EndDate"))
                Return oParameter

            Case "LAPSTATAS"
                Dim oParameter() As ReportParameter = New ReportParameter(1) {}
                oParameter(0) = New ReportParameter("Cabang", Split(oCookie.Values("Cabang"), ","))
                oParameter(1) = New ReportParameter("Tahun", oCookie.Values("Tahun"))
                Return oParameter

            Case "LAPTAGAS"
                Dim oParameter() As ReportParameter = New ReportParameter(2) {}
                oParameter(0) = New ReportParameter("Cabang", Split(oCookie.Values("Cabang"), ","))
                oParameter(1) = New ReportParameter("CabangAsuransi", oCookie.Values("CabangAsuransi"))
                oParameter(2) = New ReportParameter("NoFaktur", oCookie.Values("NoFaktur"))
                Return oParameter

            Case "DAFJURNALDTL", "DAFJURNALSUM", "RCVRPT"
                Dim oParameter() As ReportParameter = New ReportParameter(2) {}
                oParameter(0) = New ReportParameter("Cabang", Split(oCookie.Values("Cabang"), ","))
                'oParameter(1) = New ReportParameter("TransID", oCookie.Values("TransID"))
                oParameter(1) = New ReportParameter("StartDate", oCookie.Values("StartDate"))
                oParameter(2) = New ReportParameter("EndDate", oCookie.Values("EndDate"))
                Return oParameter


            Case "LAPHUTANGAS"
                Dim oParameter() As ReportParameter = New ReportParameter(3) {}
                oParameter(0) = New ReportParameter("Cabang", Split(oCookie.Values("Cabang"), ","))
                'oParameter(1) = New ReportParameter("TransID", oCookie.Values("TransID"))
                oParameter(1) = New ReportParameter("StartDate", oCookie.Values("StartDate"))
                oParameter(2) = New ReportParameter("EndDate", oCookie.Values("EndDate"))
                oParameter(3) = New ReportParameter("CabangAsuransi", oCookie.Values("CabangAsuransi"))
                Return oParameter

            Case "LAPPENDBUNGA"
                Dim oParameter() As ReportParameter = New ReportParameter(3) {}
                oParameter(0) = New ReportParameter("Cabang", Split(oCookie.Values("Cabang"), ","))
                oParameter(1) = New ReportParameter("DMOption", oCookie.Values("Pilihan"))
                oParameter(2) = New ReportParameter("StartDate", oCookie.Values("StartDate"))
                oParameter(3) = New ReportParameter("EndDate", oCookie.Values("EndDate"))
                Return oParameter

            Case "HASILCRDSCOR"
                Dim oParameter() As ReportParameter = New ReportParameter(1) {}
                oParameter(0) = New ReportParameter("Cabang", Split(oCookie.Values("Cabang"), ","))
                oParameter(1) = New ReportParameter("NoAplikasi", oCookie.Values("NoAplikasi"))

                Return oParameter

            Case "FUNAGRINSTJF"
                Dim oParameter() As ReportParameter = New ReportParameter(0) {}
                oParameter(0) = New ReportParameter("NoAplikasi", oCookie.Values("NoAplikasi"))

                Return oParameter

            Case "HITUNGDENDA"
                Dim oParameter() As ReportParameter = New ReportParameter(2) {}
                oParameter(0) = New ReportParameter("Cabang", Split(oCookie.Values("Cabang"), ","))
                oParameter(1) = New ReportParameter("NoAplikasi", oCookie.Values("NoKontrak"))
                oParameter(2) = New ReportParameter("Tanggal", oCookie.Values("Tanggal"))

                Return oParameter
            Case "LAPANGSTUNAI"
                Dim oParameter() As ReportParameter = New ReportParameter(3) {}
                oParameter(0) = New ReportParameter("Cabang", Split(oCookie.Values("Cabang"), ","))
                'oParameter(1) = New ReportParameter("Kasir", oCookie.Values("Kasir"))
                oParameter(1) = New ReportParameter("Kasir", Split(oCookie.Values("Kasir"), ","))
                oParameter(2) = New ReportParameter("StartDate", oCookie.Values("StartDateTime") + " " + oCookie.Values("StartTime"))
                oParameter(3) = New ReportParameter("EndDate", oCookie.Values("EndDateTime") + " " + oCookie.Values("EndTime"))


                Return oParameter

            Case "LAPAPKBLMCOV", "LAPPREPAID"
                Dim oParameter() As ReportParameter = New ReportParameter(0) {}
                oParameter(0) = New ReportParameter("Cabang", Split(oCookie.Values("Cabang"), ","))
                'oParameter(1) = New ReportParameter("StatusBPKB", oCookie.Values("StatusBPKB"))
                Return oParameter

                'Case "STKOPNBPKB"
                '    Dim oParameter() As ReportParameter = New ReportParameter(0) {}
                '    oParameter(0) = New ReportParameter("Cabang", Split(oCookie.Values("Cabang"), ","))
                '    'oParameter(1) = New ReportParameter("StopOpname", oCookie.Values("StatusBPKB"))
                '    Return oParameter
            Case "LAPTUNGSUM"

                Dim oParameter() As ReportParameter = New ReportParameter(0) {}
                oParameter(0) = New ReportParameter("Tanggal", Split(oCookie.Values("Tanggal"), ","))
                Return oParameter

            Case "LAPTUNGGCMO"
                Dim oParameter() As ReportParameter = New ReportParameter(2) {}
                oParameter(0) = New ReportParameter("Cabang", Split(oCookie.Values("Cabang"), ","))
                oParameter(1) = New ReportParameter("CMO", oCookie.Values("CMO"))
                oParameter(2) = New ReportParameter("Tanggal", Split(oCookie.Values("Tanggal"), ","))
                Return oParameter

            Case "DAFTRIALBAL"
                Dim oParameter() As ReportParameter = New ReportParameter(1) {}
                oParameter(0) = New ReportParameter("Bln", oCookie.Values("Month"))
                oParameter(1) = New ReportParameter("year", oCookie.Values("Year"))
                Return oParameter
                'Case "REKORBOOKING"
                '    Dim oParameter() As ReportParameter = New ReportParameter(2) {}
                '    oParameter(0) = New ReportParameter("Cabang", Split(oCookie.Values("Cabang"), ","))
                '    oParameter(1) = New ReportParameter("Bln", oCookie.Values("Month"))
                '    oParameter(2) = New ReportParameter("year", oCookie.Values("Year"))
                '    Return oParameter
                'Case "DAILYBPKB"
                '    Dim oParameter() As ReportParameter = New ReportParameter(1) {}
                '    oParameter(0) = New ReportParameter("Cabang", Split(oCookie.Values("Cabang"), ","))
                '    oParameter(1) = New ReportParameter("StopOpname", oCookie.Values("StopOpname"))

            Case "STKOPNBPKB"
                Dim oParameter() As ReportParameter = New ReportParameter(1) {}
                oParameter(0) = New ReportParameter("Cabang", Split(oCookie.Values("Cabang"), ","))
                ' oParameter(1) = New ReportParameter("StockOpNo", Split(oCookie.Values("StockOpNo"), ","))
                oParameter(1) = New ReportParameter("StockOpNo", oCookie.Values("StockOpNo"))
                Return oParameter


            Case "FUNDPREPYT"
                Dim oParameter() As ReportParameter = New ReportParameter(0) {}
                oParameter(0) = New ReportParameter("TglBayar", Split(oCookie.Values("TglBayar"), ","))

                Return oParameter

            Case "DAFTARANGSURANPERJATUHTEMPO"
                Dim oParameter() As ReportParameter = New ReportParameter(3) {}
                oParameter(0) = New ReportParameter("FundingCoyID", Split(oCookie.Values("BankCompany"), ","))
                oParameter(1) = New ReportParameter("FundingContractNo", Split(oCookie.Values("Kontrak"), ","))
                oParameter(2) = New ReportParameter("StartDate", oCookie.Values("StartDate"))
                oParameter(3) = New ReportParameter("EndDate", oCookie.Values("EndDate"))

                Return oParameter

            Case "REKRENCANABAYARLAIN"
                Dim oParameters() As ReportParameter = New ReportParameter(3) {}

                oParameters(0) = New ReportParameter("Cabang", Split(oCookie.Values("Cabang"), ","))
                oParameters(1) = New ReportParameter("StartDate", oCookie.Values("StartDate"))
                oParameters(2) = New ReportParameter("EndDate", oCookie.Values("EndDate"))
                oParameters(3) = New ReportParameter("ApType", oCookie.Values("ApType"))
                Return oParameters

            Case "LAPHASILKUNCOLL"
                Dim oParameter() As ReportParameter = New ReportParameter(4) {}
                oParameter(0) = New ReportParameter("Cabang", Split(oCookie.Values("Cabang"), ","))
                oParameter(1) = New ReportParameter("Kolektor", Split(oCookie.Values("CollectorType_CL"), ","))
                oParameter(2) = New ReportParameter("StartDate", Split(oCookie.Values("StartDate"), ","))
                oParameter(3) = New ReportParameter("EndDate", Split(oCookie.Values("EndDate"), ","))
                oParameter(4) = New ReportParameter("TypeTanggal", Split(oCookie.Values("TypeTanggal"), ","))
                Return oParameter

            Case "PDCJT"
                Dim oParameter() As ReportParameter = New ReportParameter(3) {}

                oParameter(0) = New ReportParameter("Cabang", Split(oCookie.Values("Cabang"), ","))
                oParameter(1) = New ReportParameter("StartDate", oCookie.Values("StartDate"))
                oParameter(2) = New ReportParameter("EndDate", oCookie.Values("EndDate"))
                oParameter(3) = New ReportParameter("SearchBy", oCookie.Values("SearchBy"))
                Return oParameter

            Case "REALJUALCMO"
                Dim oParameter() As ReportParameter = New ReportParameter(3) {}

                oParameter(0) = New ReportParameter("Cabang", Split(oCookie.Values("Cabang"), ","))
                oParameter(1) = New ReportParameter("StartDate", oCookie.Values("StartDate"))
                oParameter(2) = New ReportParameter("EndDate", oCookie.Values("EndDate"))
                oParameter(3) = New ReportParameter("Assettype", oCookie.Values("Assettype"))
                Return oParameter

            Case "DAFJIRM"

                Dim oParameters() As ReportParameter = New ReportParameter(1) {}
                oParameters(0) = New ReportParameter("Tanggal", oCookie.Values("Tanggal"))
                oParameters(1) = New ReportParameter("Cabang", Split(oCookie.Values("Cabang"), ","))

                Return oParameters

            Case Else

                Return Nothing
        End Select


    End Function
End Module
