﻿Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.Controller

Public Class ucBGNoDate
    Inherits ControlBased

#Region " Private Const "
    Private m_controller As New DataUserControlController
    Protected WithEvents DueDate As ucDateCE
#End Region

#Region "Property"
    Public Property InDueDate() As String
        Get
            Return DueDate.Text
        End Get
        Set(ByVal Value As String)
            DueDate.Text = Value
        End Set
    End Property

    Public Property BGNumber() As String
        Get
            Return (CType(cmbBGNo.SelectedItem.Text, String))
        End Get
        Set(ByVal Value As String)
            cmbBGNo.SelectedIndex = cmbBGNo.Items.IndexOf(cmbBGNo.Items.FindByText(Value))
        End Set
    End Property

    Public Property BGNumberEnable() As Boolean
        Get
            Return cmbBGNo.Enabled
        End Get
        Set(ByVal Value As Boolean)
            cmbBGNo.Enabled = Value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not IsPostBack Then
            DueDate.IsRequired = True
        End If
    End Sub

    Public Sub BindBGNODate(ByVal strWhere As String)
        cmbBGNo.Items.Clear()
        Dim DtBGNumber As New DataTable
        Dim dvBGNumber As New DataView

        'If DtBGNumber Is Nothing Then
        '    Dim dtBGNumberCache As New DataTable
        '    dtBGNumberCache = m_controller.GetBGNumber(GetConnectionString, strWhere)
        '    Me.Cache.Insert("BGNumber", dtBGNumberCache, Nothing, DateTime.Now.AddHours(DEFAULT_TIME_DURATION_CACHE_HOUR), TimeSpan.Zero)
        '    DtBGNumber = CType(Me.Cache.Item("BGNumber"), DataTable)
        'End If
        DtBGNumber = m_controller.GetBGNumber(GetConnectionString, strWhere)
        dvBGNumber = DtBGNumber.DefaultView

        With cmbBGNo
            .DataValueField = "ID"
            .DataTextField = "ID"
            .DataSource = dvBGNumber
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = "0"
            .Items.Insert(1, "None")
            .Items(1).Value = "None"

        End With
    End Sub

    Protected Sub cmbBGNo_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmbBGNo.SelectedIndexChanged
        If cmbBGNo.SelectedValue = "None" Or cmbBGNo.SelectedValue = "0" Then            
            DueDate.IsRequired = False
        End If
    End Sub

    Public Sub DueDateValidatorFalse()
        DueDate.IsRequired = False
    End Sub
    Public Sub DueDateValidatorTrue()
        DueDate.IsRequired = True
    End Sub
End Class