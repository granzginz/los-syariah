﻿Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.Controller
Imports System.IO
Imports System.Collections.Generic
Imports System.Drawing

Public Class UcNotaAsuransi
    Inherits ControlBased
    Dim bytes As Byte()

#Region "uc control declaration"
    Public Delegate Sub TextChangedHandler(ByVal sender As Object, ByVal e As System.EventArgs)
    Public Event TextChanged_txtPremiGross As TextChangedHandler
    Public Event TextChanged_txtKomisi As TextChangedHandler
    Public Event TextChanged_txtPremiNet As TextChangedHandler
    Public Event TextChanged_txtPPN As TextChangedHandler
    Public Event TextChanged_txtPPH As TextChangedHandler
    Public Event TextChanged_txtBiayaPolis As TextChangedHandler
    Public Event TextChanged_txtBiayaMaterai As TextChangedHandler
    Public Event TextChanged_txtTotalTagihan As TextChangedHandler

    Protected WithEvents txtTglNota As ucDateCE
    Public WithEvents txtPremiGross As ucNumberFormat
    Public WithEvents txtKomisi As ucNumberFormat
    Public WithEvents txtPremiNet As ucNumberFormat
    Public WithEvents txtPPN As ucNumberFormat
    Public WithEvents txtPPH As ucNumberFormat
    Public WithEvents txtBiayaPolis As ucNumberFormat
    Public WithEvents txtBiayaMaterai As ucNumberFormat
    Public WithEvents txtTotalTagihan As ucNumberFormat
    Dim dtCSV, dtSQL As New DataTable
#End Region
#Region "Properties"
    Public Property Asuransilbl As String
        Get
            Return lblAsuransi.Text
        End Get
        Set(ByVal Asuransilbl As String)
            lblAsuransi.Text = Asuransilbl
        End Set
    End Property
    Public Property NoKontraklbl As String
        Get
            Return lblNoKontrak.Text
        End Get
        Set(ByVal NoKontraklbl As String)
            lblNoKontrak.Text = NoKontraklbl
        End Set
    End Property
    Public Property NoNotatxt As String
        Get
            Return txtNoNota.Text
        End Get
        Set(ByVal NoNotatxt As String)
            txtNoNota.Text = NoNotatxt
        End Set
    End Property
    Public Property NamaKonsumenlbl As String
        Get
            Return lblNamaKonsumen.Text
        End Get
        Set(ByVal NamaKonsumenlbl As String)
            lblNamaKonsumen.Text = NamaKonsumenlbl
        End Set
    End Property
    Public Property TglNota As String
        Get
            Return txtTglNota.Text
        End Get
        Set(ByVal TglNota As String)
            txtTglNota.Text = TglNota
        End Set
    End Property
    Public Property Tenorlbl As String
        Get
            Return lblTenor.Text
        End Get
        Set(ByVal Tenorlbl As String)
            lblTenor.Text = Tenorlbl
        End Set
    End Property
    Public Property Periodelbl As String
        Get
            Return lblPeriode.Text
        End Get
        Set(ByVal Periodelbl As String)
            lblPeriode.Text = Periodelbl
        End Set
    End Property
    Public Property PremiGrosslbl As String
        Get
            Return lblPremiGross.Text
        End Get
        Set(ByVal PremiGrosslbl As String)
            lblPremiGross.Text = PremiGrosslbl
        End Set
    End Property
    Public Property PremiGrosstxt As String
        Get
            Return txtPremiGross.Text
        End Get
        Set(ByVal PremiGrosstxt As String)
            txtPremiGross.Text = PremiGrosstxt
        End Set
    End Property
    Public Property PremisGrossSelisihlbl As String
        Get
            Return lblPremisGrossSelisih.Text
        End Get
        Set(ByVal PremisGrossSelisihlbl As String)
            lblPremisGrossSelisih.Text = PremisGrossSelisihlbl
        End Set
    End Property
    Public Property Komisilbl As String
        Get
            Return lblKomisi.Text
        End Get
        Set(ByVal Komisilbl As String)
            lblKomisi.Text = Komisilbl
        End Set
    End Property
    Public Property Komisitxt As String
        Get
            Return txtKomisi.Text
        End Get
        Set(ByVal Komisitxt As String)
            txtKomisi.Text = Komisitxt
        End Set
    End Property
    Public Property KomisiSelisihlbl As String
        Get
            Return lblKomisiSelisih.Text
        End Get
        Set(ByVal KomisiSelisihlbl As String)
            lblKomisiSelisih.Text = KomisiSelisihlbl
        End Set
    End Property
    Public Property PremiNetlbl As String
        Get
            Return lblPremiNet.Text
        End Get
        Set(ByVal PremiNetlbl As String)
            lblPremiNet.Text = PremiNetlbl
        End Set
    End Property
    Public Property PremiNettxt As String
        Get
            Return txtPremiNet.Text
        End Get
        Set(ByVal PremiNettxt As String)
            txtPremiNet.Text = PremiNettxt
        End Set
    End Property
    Public Property PremiNetSelisihlbl As String
        Get
            Return lblPremiNetSelisih.Text
        End Get
        Set(ByVal PremiNetSelisihlbl As String)
            lblPremiNetSelisih.Text = PremiNetSelisihlbl
        End Set
    End Property
    Public Property PPNlbl As String
        Get
            Return lblPPN.Text
        End Get
        Set(ByVal PPNlbl As String)
            lblPPN.Text = PPNlbl
        End Set
    End Property
    Public Property PPNtxt As String
        Get
            Return txtPPN.Text
        End Get
        Set(ByVal PPNtxt As String)
            txtPPN.Text = PPNtxt
        End Set
    End Property
    Public Property PPNSelisihlbl As String
        Get
            Return lblPPNSelisih.Text
        End Get
        Set(ByVal PPNSelisihlbl As String)
            lblPPNSelisih.Text = PPNSelisihlbl
        End Set
    End Property
    Public Property PPHlbl As String
        Get
            Return lblPPH.Text
        End Get
        Set(ByVal PPHlbl As String)
            lblPPH.Text = PPHlbl
        End Set
    End Property
    Public Property PPHtxt As String
        Get
            Return txtPPH.Text
        End Get
        Set(ByVal PPHtxt As String)
            txtPPH.Text = PPHtxt
        End Set
    End Property
    Public Property PPHSelisihlbl As String
        Get
            Return lblPPHSelisih.Text
        End Get
        Set(ByVal PPHSelisihlbl As String)
            lblPPHSelisih.Text = PPHSelisihlbl
        End Set
    End Property
    Public Property BiayaPolislbl As String
        Get
            Return lblBiayaPolis.Text
        End Get
        Set(ByVal BiayaPolislbl As String)
            lblBiayaPolis.Text = BiayaPolislbl
        End Set
    End Property
    Public Property BiayaPolistxt As String
        Get
            Return txtBiayaPolis.Text
        End Get
        Set(ByVal BiayaPolistxt As String)
            txtBiayaPolis.Text = BiayaPolistxt
        End Set
    End Property
    Public Property BiayaPolisSelisihlbl As String
        Get
            Return lblBiayaPolisSelisih.Text
        End Get
        Set(ByVal BiayaPolisSelisihlbl As String)
            lblBiayaPolisSelisih.Text = BiayaPolisSelisihlbl
        End Set
    End Property
    Public Property BiayaMaterailbl As String
        Get
            Return lblBiayaMaterai.Text
        End Get
        Set(ByVal BiayaMaterailbl As String)
            lblBiayaMaterai.Text = BiayaMaterailbl
        End Set
    End Property
    Public Property BiayaMateraitxt As String
        Get
            Return txtBiayaMaterai.Text
        End Get
        Set(ByVal BiayaMateraitxt As String)
            txtBiayaMaterai.Text = BiayaMateraitxt
        End Set
    End Property
    Public Property BiayaMateraiSelisihlbl As String
        Get
            Return lblBiayaMateraiSelisih.Text
        End Get
        Set(ByVal BiayaMateraiSelisihlbl As String)
            lblBiayaMateraiSelisih.Text = BiayaMateraiSelisihlbl
        End Set
    End Property
    Public Property TotalTagihanlbl As String
        Get
            Return lblTotalTagihan.Text
        End Get
        Set(ByVal TotalTagihanlbl As String)
            lblTotalTagihan.Text = TotalTagihanlbl
        End Set
    End Property
    Public Property TotalTagihantxt As String
        Get
            Return txtTotalTagihan.Text
        End Get
        Set(ByVal TotalTagihantxt As String)
            txtTotalTagihan.Text = TotalTagihantxt
        End Set
    End Property
    Public Property TotalTagihanSelisihlbl As String
        Get
            Return lblTotalTagihanSelisih.Text
        End Get
        Set(ByVal TotalTagihanSelisihlbl As String)
            lblTotalTagihanSelisih.Text = TotalTagihanSelisihlbl
        End Set
    End Property
    Public Property AdjSelisihInsPayablelbl As String
        Get
            Return lblAdjSelisihInsPayable.Text
        End Get
        Set(ByVal AdjSelisihInsPayablelbl As String)
            lblAdjSelisihInsPayable.Text = AdjSelisihInsPayablelbl
        End Set
    End Property
    Public Property cboAdjSelsihInsPayableID() As String
        Get
            Return (CType(cboAdjSelsihInsPayable.SelectedItem.Value, String))
        End Get
        Set(ByVal Value As String)
            cboAdjSelsihInsPayable.SelectedIndex = cboAdjSelsihInsPayable.Items.IndexOf(cboAdjSelsihInsPayable.Items.FindByValue(Value))
        End Set
    End Property
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            txtTglNota.IsRequired = True
            lblMessage.Visible = False
            With txtPremiGross                
                .RangeValidatorEnable = True                                
                .AutoPostBack = True
            End With

            With txtKomisi
                .RangeValidatorEnable = True                
                .AutoPostBack = True
            End With
            With txtPremiNet
                '.RangeValidatorEnable = True                
                '.AutoPostBack = True
                .Enabled = False
            End With
            With txtPPN
                .RangeValidatorEnable = True                
                .AutoPostBack = True
            End With
            With txtPPH
                .RangeValidatorEnable = True                
                .AutoPostBack = True
            End With
            With txtBiayaPolis
                .RangeValidatorEnable = True                
                .AutoPostBack = True
            End With
            With txtBiayaMaterai
                .RangeValidatorEnable = True                
                .AutoPostBack = True
            End With
            With txtTotalTagihan
                .Enabled = False
                '.RangeValidatorEnable = True
                '.AutoPostBack = True
            End With
        End If
        btnupload.Visible = True
        btndownload.Visible = False
        cekatchpolis()
    End Sub
    Public Sub txtPremiGross_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtPremiGross.TextChanged
        RaiseEvent TextChanged_txtPremiGross(sender, e)
    End Sub
    Public Sub txtKomisi_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtKomisi.TextChanged
        RaiseEvent TextChanged_txtKomisi(sender, e)
    End Sub
    Public Sub txtPremiNet_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtPremiNet.TextChanged
        RaiseEvent TextChanged_txtPremiNet(sender, e)
    End Sub
    Public Sub txtPPN_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtPPN.TextChanged
        RaiseEvent TextChanged_txtPPN(sender, e)
    End Sub
    Public Sub txtPPH_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtPPH.TextChanged
        RaiseEvent TextChanged_txtPPH(sender, e)
    End Sub
    Public Sub txtBiayaPolis_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtBiayaPolis.TextChanged
        RaiseEvent TextChanged_txtBiayaPolis(sender, e)
    End Sub
    Public Sub txtBiayaMaterai_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtBiayaMaterai.TextChanged
        RaiseEvent TextChanged_txtBiayaMaterai(sender, e)
    End Sub
    Public Sub txtTotalTagihan_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtTotalTagihan.TextChanged
        RaiseEvent TextChanged_txtTotalTagihan(sender, e)
    End Sub

#Region "Upload Attachment Polis"

    Private Sub cekatchpolis()
        If File.Exists(pathFile("Polis", lblNoKontrak.Text.Trim) + "LampiranPolis.pdf") Then
            lblfileuploaded.Text = "LampiranPolis.pdf"
            btndownload.Visible = True
            btnupload.Visible = False
        End If
    End Sub

    Private Function pathFile(ByVal Group As String, ByVal KontrakID As String) As String
        Dim strDirectory As String = ""
        strDirectory = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\" & "Insurance\" & "\" & Group & "\" & KontrakID & "\"
        'strDirectory = getpathFolder() & "Insurance\" & "\" & Group & "\" & KontrakID & "\"
        If Not System.IO.Directory.Exists(strDirectory) Then
            System.IO.Directory.CreateDirectory(strDirectory)
        End If
        Return strDirectory
    End Function

    Protected Sub btnupload_Click(sender As Object, e As EventArgs) Handles btnupload.Click
        Dim ss = lblNoKontrak.Text
        Dim FileName As String = ""
        Dim strExtension As String = ""
        If uplPolis.HasFile Then
            If uplPolis.PostedFile.ContentType = "application/pdf" Then
                FileName = "LampiranPolis"
                strExtension = Path.GetExtension(uplPolis.PostedFile.FileName)
                Try
                    uplPolis.PostedFile.SaveAs(pathFile("Polis", lblNoKontrak.Text.Trim()) + FileName + ".pdf")
                    cekatchpolis() 
                Catch ex As Exception
                    lblMessage.Text = ex.Message
                    lblMessage.Visible = True
                    btndownload.Visible = False
                    btnupload.Visible = True
                End Try 
            Else
                lblMessage.Text = "Silahkan pilih file *.pdf"
                lblMessage.Visible = True
                Return
            End If
        End If 
    End Sub

    Protected Sub btndownload_Click(sender As Object, e As EventArgs) Handles btndownload.Click
        uplPolis.Visible = True
        Response.Redirect(getDownloadPath("Insurance/" & "/Polis/" + lblNoKontrak.Text.Replace(" ", "") + "/LampiranPolis.pdf"))
        'Response.ContentType = "application/.pdf"
        'Response.AddHeader("Content-Disposition", Convert.ToString("attachment; filename=") & lblfileuploaded.Text.Trim)
        'Response.TransmitFile(Server.MapPath("../xml/" & "Insurance/" & "/Polis/" + lblNoKontrak.Text.Replace(" ", "") + "/LampiranPolis.pdf"))
        'Response.End()
    End Sub
    Private Function getpathFolder() As String
        Dim oClass As New Parameter.GeneralSetting
        Dim oCtr As New GeneralSettingController
        With oClass
            .strConnection = GetConnectionString()
            .GSID = "UploadFolder"
            .ModuleID = "GEN"
        End With
        Try
            Return oCtr.GetGeneralSettingValue(oClass).Trim
        Catch ex As Exception
            Return "0"
        End Try
    End Function
    Private Function getDownloadPath(filePath As String) As String
        Return Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd("/"c) + "/Webform.Utility/GetImage.aspx?file=" & Server.UrlEncode(getpathFolder() & filePath)
    End Function
#End Region

    Public Sub cboAdjSelsihInsPayablereq(n As Integer)
        If n = 1 Then
            IdAdjSelsihInsPayable.Attributes("class") = "label_req"
            rvAdjSelisihInsPayable.Visible = True
        ElseIf n = 0 Then
            IdAdjSelsihInsPayable.Attributes("class") = ""
            rvAdjSelisihInsPayable.Visible = False
        End If
    End Sub
End Class