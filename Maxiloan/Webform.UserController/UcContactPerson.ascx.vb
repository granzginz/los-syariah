﻿Public Class UcContactPerson
    Inherits System.Web.UI.UserControl
#Region "Property"
    Public Property ContactPerson() As String
        Get
            Return ViewState("ContactPerson").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ContactPerson") = Value
        End Set
    End Property
    Public Property ContactPersonTitle() As String
        Get
            Return ViewState("ContactPersonTitle").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ContactPersonTitle") = Value
        End Set
    End Property
    Public Property MobilePhone() As String
        Get
            Return ViewState("MobilePhone").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("MobilePhone") = Value
        End Set
    End Property
    Public Property Email() As String
        Get
            Return ViewState("Email").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("Email") = Value
        End Set
    End Property
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.ContactPerson = txtContactPerson.Text.Trim
        Me.ContactPersonTitle = txtContactPersonTitle.Text.Trim
        Me.MobilePhone = txtMobilePhone.Text.Trim
        Me.Email = txtEmail.Text.Trim
    End Sub
    Public Sub EnabledContactPerson()
        rfvContactPerson.Enabled = True
        rfvContactPerson.Visible = True
    End Sub
    Public Sub BindContacPerson()
        txtContactPerson.Text = Me.ContactPerson
        txtContactPersonTitle.Text = Me.ContactPersonTitle
        txtMobilePhone.Text = Me.MobilePhone
        txtEmail.Text = Me.Email
    End Sub
    Public Sub BorderContactNone(ByVal Read As Boolean, ByVal Style As BorderStyle)
        txtContactPerson.ReadOnly = Read
        txtContactPerson.BorderStyle = Style
        txtContactPersonTitle.ReadOnly = Read
        txtContactPersonTitle.BorderStyle = Style
        txtMobilePhone.ReadOnly = Read
        txtMobilePhone.BorderStyle = Style
        txtEmail.ReadOnly = Read
        txtEmail.BorderStyle = Style
    End Sub
End Class