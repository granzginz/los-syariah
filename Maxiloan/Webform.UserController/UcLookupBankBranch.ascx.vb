﻿Imports Maxiloan.Parameter
Imports Maxiloan.Controller

Public Class UcLookupBankBranch
    Inherits ControlBased
    Private m_controller As New LookUpBankBranchController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Public Delegate Sub CatSelectedHandler(ByVal BankBranchID As String,
                                           ByVal BankCode As String,
                                           ByVal BankBranchName As String,
                                           ByVal Address As String,
                                           ByVal City As String,
                                           ByVal BankID As String)
    Public Event CatSelected As CatSelectedHandler

    Public Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property

    Public Property ApplicationId() As String
        Get
            Return CType(ViewState("ApplicationId"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationId") = Value
        End Set
    End Property

    Public Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property

    Public Property BranchID() As String
        Get
            Return CType(ViewState("BranchID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("BranchID") = Value
        End Set
    End Property

    Public Property BankID() As String
        Get
            Return CType(ViewState("BankID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("BankID") = Value
        End Set
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load


    End Sub

    Public Sub BindGridEntity(ByVal cmdWhere As String)
        Dim dttEntity As DataTable = Nothing
        Dim oCustomClass As New Maxiloan.Parameter.LookUpBankBranch

        If cmdWhere <> "" Then cmdWhere = cmdWhere & " and "
        If BankID <> "" Then cmdWhere = cmdWhere & " BankID = '" & BankID & "'"

        oCustomClass = m_controller.GetListData(GetConnectionString, cmdWhere, currentPage, pageSize, Me.SortBy)

        If Not oCustomClass Is Nothing Then
            dttEntity = oCustomClass.ListData
            recordCount = oCustomClass.TotalRecords
        Else
            recordCount = 0
        End If

        dtgPaging.DataSource = dttEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()
        PagingFooter()
        Popup()
    End Sub

#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            'lblMessage.Text = "Data not found....."
            'lblMessage.Visible = True
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblTotRec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridEntity(Me.CmdWhere)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGoPage.Click
        If IsNumeric(txtGoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtGoPage.Text, Int32)
                BindGridEntity(Me.CmdWhere)
            End If
        End If
    End Sub
#End Region

    Protected Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub

    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        cboSearchBy.SelectedIndex = 0
        txtSearchBy.Text = ""
        Me.CmdWhere = txtSearchBy.Text
        BindGridEntity(Me.CmdWhere)
    End Sub

    Private Sub imbSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        pnlSubList.Visible = True
        Try
            If txtSearchBy.Text <> "" Then
                If cboSearchBy.SelectedItem.Value = "BankCode" Then
                    Me.CmdWhere = "BankCode LIKE '%" & txtSearchBy.Text & "%'"
                ElseIf cboSearchBy.SelectedItem.Value = "BankBranchName" Then
                    Me.CmdWhere = "BankBranchName LIKE '%" & txtSearchBy.Text & "%'"
                ElseIf cboSearchBy.SelectedItem.Value = "Address" Then
                    Me.CmdWhere = "Address LIKE '%" & txtSearchBy.Text & "%'"
                ElseIf cboSearchBy.SelectedItem.Value = "City" Then
                    Me.CmdWhere = "City LIKE '%" & txtSearchBy.Text & "%'"
                End If
                Context.Trace.Write("Var Me.CmdWhere = " + Me.CmdWhere)
                BindGridEntity(Me.CmdWhere)
            Else
                Me.CmdWhere = txtSearchBy.Text
                BindGridEntity(Me.CmdWhere)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub

    Public Sub Search()
        pnlSubList.Visible = True
        Try
            If txtSearchBy.Text <> "" Then
                If cboSearchBy.SelectedItem.Value = "BankCode" Then
                    Me.CmdWhere = "BankCode LIKE '%" & txtSearchBy.Text & "%'"
                ElseIf cboSearchBy.SelectedItem.Value = "BankBranchName" Then
                    Me.CmdWhere = "BankBranchName LIKE '%" & txtSearchBy.Text & "%'"
                ElseIf cboSearchBy.SelectedItem.Value = "Address" Then
                    Me.CmdWhere = "Address LIKE '%" & txtSearchBy.Text & "%'"
                ElseIf cboSearchBy.SelectedItem.Value = "City" Then
                    Me.CmdWhere = "City LIKE '%" & txtSearchBy.Text & "%'"
                End If
                Context.Trace.Write("Var Me.CmdWhere = " + Me.CmdWhere)
                BindGridEntity(Me.CmdWhere)
            Else
                Me.CmdWhere = txtSearchBy.Text
                BindGridEntity(Me.CmdWhere)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub
    Protected Sub dtgPaging_ItemCommand(ByVal sender As Object, ByVal e As DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        If e.CommandName = "Select" Then
            Dim i As Integer = e.Item.ItemIndex
            RaiseEvent CatSelected(dtgPaging.DataKeys.Item(i).ToString.Trim,
                                   dtgPaging.Items(i).Cells.Item(1).Text.Trim,
                                   dtgPaging.Items(i).Cells.Item(2).Text.Trim,
                                   dtgPaging.Items(i).Cells.Item(3).Text.Trim,
                                   dtgPaging.Items(i).Cells.Item(4).Text.Trim,
                                   dtgPaging.Items(i).Cells.Item(5).Text.Trim)
        End If        
    End Sub

    Public Sub Popup()
        Me.ModalPopupExtender.Show()
    End Sub

    Protected Sub imbExit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Me.ModalPopupExtender.Hide()
    End Sub
End Class