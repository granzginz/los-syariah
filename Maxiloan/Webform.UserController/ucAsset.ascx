﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucAsset.ascx.vb" Inherits="Maxiloan.Webform.UserController.ucAsset" %>
<div class="form_box">
    <div class="form_single">
        <label class="label_req">
            Kendaraan</label>
        <asp:TextBox ID="txtAssetName" runat="server" CssClass="long_text"></asp:TextBox>
        <asp:HyperLink ID="hpLookup" runat="server" ImageUrl="../images/IconDetail.gif"></asp:HyperLink>
        <asp:Label ID="lblPF" runat="server"></asp:Label>        
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtAssetName"
            ErrorMessage="Pilih kendaraan!" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
        <input type="hidden" id="hdnAssetCode" runat="server" name="hdnAssetCode" />
        <input type="hidden" id="hdnAssetName" runat="server" name="hdnAssetName" />        
    </div>
</div>