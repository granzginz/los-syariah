﻿Public Class UcLookupEmployee
    Inherits ControlBased

    Public Property BranchID() As String
        Get
            Return CType(ViewState("branchid"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("branchid") = Value
        End Set
    End Property

    Public Property EmployeeName() As String
        Get
            Return CType(txtEmployeeName.Text.Trim, String)
        End Get
        Set(ByVal Value As String)
            txtEmployeeName.Text = Value
        End Set
    End Property

    Public Property EmployeeID() As String
        Get
            If Me.HiddenEmployeeID Then
                Return hdnEmployeeID.Value
            Else
                Return txtEmployeeID.Text
            End If
        End Get
        Set(ByVal Value As String)
            If Me.HiddenEmployeeID Then
                hdnEmployeeID.Value = Value
            Else
                txtEmployeeID.Text = Value
            End If
        End Set
    End Property

    Public Property Style() As String
        Get
            Return CStr(ViewState("style"))
        End Get
        Set(ByVal Value As String)
            ViewState("style") = Value
        End Set
    End Property

    Public Property HiddenEmployeeID() As Boolean
        Get
            Return CBool(ViewState("HiddenEmployeeID"))
        End Get
        Set(ByVal Value As Boolean)
            ViewState("HiddenEmployeeID") = Value
        End Set
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not Me.IsPostBack Then
            txtEmployeeID.Attributes.Add("readonly", "true")
        End If
    End Sub

    Public Sub BindData()
        If Me.HiddenEmployeeID Then
            pnlEmployeeID.Visible = False
            hdnEmployeeID.Value = Me.EmployeeID
        Else
            pnlEmployeeID.Visible = True
            txtEmployeeID.Text = Me.EmployeeID
        End If
        txtEmployeeName.Text = Me.EmployeeName
        If Me.HiddenEmployeeID Then
            hpLookup1.NavigateUrl = "javascript:OpenWinEmployee('" & hdnEmployeeID.ClientID & "','" & txtEmployeeName.ClientID & "','" & Me.Style & "','" & Me.BranchID & "')"
            hpLookup.Visible = False
            hpLookup1.Visible = True
        Else
            hpLookup.NavigateUrl = "javascript:OpenWinEmployee('" & txtEmployeeID.ClientID & "','" & txtEmployeeName.ClientID & "','" & Me.Style & "','" & Me.BranchID & "')"
            hpLookup.Visible = True
            hpLookup1.Visible = False
        End If

    End Sub


End Class