﻿#Region "Import"
Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Interface

Imports Maxiloan.cbse
Imports Maxiloan.Controller
#End Region

Public Class UCPaymentDetailD
    Inherits ControlBased
    Private oController As New DataUserControlController
    Private dtBankAccount As New DataTable
    Protected WithEvents txtAmountReceive As ucNumberFormat
    Protected WithEvents txtValueDate As ucDateCE
    Public Delegate Sub TextChangedHandler(ByVal sender As Object, ByVal e As System.EventArgs)
    Public Event Changed As TextChangedHandler

#Region "Property"

#Region "PaymentDetail"

    Public Property ReceivedFrom() As String
        Get
            Return CType(txtReceivedFrom.Text.Trim, String)
        End Get
        Set(ByVal Value As String)
            txtReceivedFrom.Text = Value
        End Set
    End Property

    Public Property ReferenceNo() As String
        Get
            Return CType(txtReferenceNo.Text.Trim, String)
        End Get
        Set(ByVal Value As String)
            txtReferenceNo.Text = Value
        End Set
    End Property

    Public Property WayOfPayment() As String
        Get
            Return CType(cmbWayOfPayment.SelectedItem.Value.Trim, String)
        End Get
        Set(ByVal Value As String)
            cmbWayOfPayment.SelectedIndex = cmbWayOfPayment.Items.IndexOf(cmbWayOfPayment.Items.FindByValue(Value))
        End Set
    End Property

    Public ReadOnly Property WayOfPaymentName() As String
        Get
            Return CType(cmbWayOfPayment.SelectedItem.Text.Trim, String)
        End Get
    End Property

    Public ReadOnly Property BankAccount() As String
        Get
            Return CType(cmbBankAccount.SelectedValue.Trim, String)
        End Get
    End Property

    Public ReadOnly Property BankAccountName() As String
        Get
            Return CType(cmbBankAccount.SelectedItem.Text, String)
        End Get
    End Property

    Public Property ValueDate() As String
        Get
            Return CType(txtValueDate.Text, String)
        End Get
        Set(ByVal Value As String)
            txtValueDate.Text = Value
        End Set
    End Property

    Public Property AmountReceive() As Double
        Get
            Return CType(txtAmountReceive.Text.Trim, Double)
        End Get
        Set(ByVal Value As Double)
            txtAmountReceive.Text = CStr(Value)
        End Set
    End Property

    Public Property Notes() As String
        Get
            Return CType(txtNotes.Text.Trim, String)
        End Get
        Set(ByVal Value As String)
            txtNotes.Text = Value.Trim
        End Set
    End Property

    Public Property JenisTransfer() As String
        Get
            Return rboJenisTransfer.SelectedValue
        End Get
        Set(ByVal value As String)
            rboJenisTransfer.SelectedValue = value
        End Set
    End Property

    Public Property BeneficiaryBankID() As String
        Get
            Return (CType(UcBankAccount1.BankID, String))
        End Get
        Set(ByVal Value As String)
            UcBankAccount1.BankID = Value
        End Set
    End Property

    Public Property BeneficiaryBankBranchID() As String
        Get
            Return (CType(UcBankAccount1.BankBranchId, String))
        End Get
        Set(ByVal Value As String)
            UcBankAccount1.BankBranchId = Value
        End Set
    End Property

    Public Property BeneficiaryBankName() As String
        Get
            Return (CType(UcBankAccount1.BankName, String))
        End Get
        Set(ByVal Value As String)
            UcBankAccount1.BankName = Value
        End Set
    End Property

    Public Property BeneficiaryBankBranchCode() As String
        Get
            Return (CType(UcBankAccount1.BankCodeCabang, String))
        End Get
        Set(ByVal Value As String)
            UcBankAccount1.BankCodeCabang = Value
        End Set
    End Property

    Public Property BeneficiaryBankCode() As String
        Get
            Return (CType(UcBankAccount1.BankCodeBank, String))
        End Get
        Set(ByVal Value As String)
            UcBankAccount1.BankCodeBank = Value
        End Set
    End Property

    Public Property BeneficiaryBankCity() As String
        Get
            Return (CType(UcBankAccount1.City, String))
        End Get
        Set(ByVal Value As String)
            UcBankAccount1.City = Value
        End Set
    End Property

    Public Property BeneficiaryBankBranchName() As String
        Get
            Return (CType(UcBankAccount1.BankBranchName, String))
        End Get
        Set(ByVal Value As String)
            UcBankAccount1.BankBranchName = Value
        End Set
    End Property

    Public Property BeneficiaryBankAccountNo() As String
        Get
            Return (CType(UcBankAccount1.AccountNo, String))
        End Get
        Set(ByVal Value As String)
            UcBankAccount1.AccountNo = Value
        End Set
    End Property

    Public Property BeneficiaryBankAccountName() As String
        Get
            Return (CType(UcBankAccount1.AccountName, String))
        End Get
        Set(ByVal Value As String)
            UcBankAccount1.AccountName = Value
        End Set
    End Property

#End Region

    Private Property ListBankAccount() As DataTable
        Get
            Return (CType(ViewState("ListBankAccount"), DataTable))
        End Get
        Set(ByVal Value As DataTable)
            ViewState("ListBankAccount") = Value
        End Set
    End Property

    Public Property IsTitle() As Boolean
        Get
            Return (CType(ViewState("IsTitle"), Boolean))
        End Get
        Set(ByVal Value As Boolean)
            ViewState("IsTitle") = Value
        End Set
    End Property

    Public Property WithOutPrepaid() As Boolean
        Get
            Return (CType(ViewState("withoutprepaid"), Boolean))
        End Get
        Set(ByVal Value As Boolean)
            ViewState("withoutprepaid") = Value
        End Set
    End Property

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Dim objrow As DataRow()

        If Not IsPostBack Then
            txtValueDate.IsRequired = True
            'hdnBankAccount.Value = ""
            BindWayOfPayment()
            BindBankAccount()
            With txtAmountReceive
                .RequiredFieldValidatorEnable = True
                .RangeValidatorEnable = True                
                .TextCssClass = "numberAlign regular_text"
            End With
        End If

        'ViewState("BankAccountName") = ""

        'If Not IsNothing(hdnBankAccount.Value) Then
        '    objrow = Me.ListBankAccount.Select(" ID = '" & hdnBankAccount.Value.Trim & "'")
        '    If objrow.Length > 0 Then
        '        ViewState("BankAccountName") = CStr(objrow(0)("Name"))
        '    End If
        'End If
    End Sub

#Region "Bind Bank Account"
    Private Sub BindBankAccount()
        'Me.Cache.Remove(CACHE_BANKACCOUNT & Me.sesBranchId.Replace("'", ""))
        dtBankAccount = CType(Me.Cache.Item(CACHE_BANKACCOUNT & Me.sesBranchId.Replace("'", "")), DataTable)
        If dtBankAccount Is Nothing Then
            Dim dtBankAccountCache As New DataTable
            dtBankAccountCache = oController.GetBankAccount(GetConnectionString, Me.sesBranchId, "", "")
            Me.Cache.Insert(CACHE_BANKACCOUNT & Me.sesBranchId.Replace("'", ""), dtBankAccountCache, Nothing, DateTime.Now.AddHours(DEFAULT_TIME_DURATION_CACHE_HOUR), TimeSpan.Zero)
            dtBankAccount = CType(Me.Cache.Item(CACHE_BANKACCOUNT & Me.sesBranchId.Replace("'", "")), DataTable)
        End If
        Me.ListBankAccount = dtBankAccount
        'scriptJava.InnerHtml = GenerateScript(dtBankAccount)
    End Sub
#End Region

#Region "Bind WayOfPayment"
    Private Sub BindWayOfPayment()
        Dim oDataTable As DataTable = New DataTable
        Dim oRow As DataRow
        Dim splitListData() As String
        Dim splitRow() As String
        Dim i As Integer
        Dim strListData As String

        oDataTable.Columns.Add("ID", System.Type.GetType("System.String"))
        oDataTable.Columns.Add("Description", System.Type.GetType("System.String"))

        oRow = oDataTable.NewRow()
        oRow("ID") = ""
        oRow("Description") = ""

        'If Me.WithOutPrepaid Then
        '    strListData = "CA,Cash-BA,Bank"
        'Else
        '    strListData = "CA,Cash-BA,Bank-CP,Prepaid"
        'End If

        If Me.WithOutPrepaid Then
            strListData = "CA,Cash-BA,Bank-GT,EBanking"
        Else
            strListData = "CA,Cash-BA,Bank-CP,Prepaid-GT,EBanking"
        End If

        splitListData = Split(strListData, "-")

        For i = 0 To UBound(splitListData)
            splitRow = Split(splitListData(i), ",")
            oRow("ID") = splitRow(0)
            oRow("Description") = splitRow(1)
            oDataTable.Rows.Add(oRow)
            oRow = oDataTable.NewRow()
        Next

        cmbWayOfPayment.DataValueField = "ID"
        cmbWayOfPayment.DataTextField = "Description"
        cmbWayOfPayment.DataSource = oDataTable
        cmbWayOfPayment.DataBind()
        cmbWayOfPayment.Items.Insert(0, "Select One")
        cmbWayOfPayment.Items(0).Value = "0"
    End Sub
#End Region

    '#Region "Generate Script"
    'tambahan html script untuk cmbBankAccount jika generate script aktif
    'onchange="setCboDetlVal(this.options[this.selectedIndex].value);"
    'tambahan html script untuk cmbWayOfPayment jika generate script aktif
    '<%--        <asp:DropDownList ID="cmbWayOfPayment" runat="server" onchange="<%#WOPonChange()%>">
    '    </asp:DropDownList>--%>
    '    Protected Function WOPonChange() As String
    '        hdnBankAccount.Value = ""
    '        Return "WOPChange('" & Trim(cmbWayOfPayment.ClientID) & "','" & _
    '                Trim(cmbBankAccount.ClientID) & "','" & _
    '                Trim(hdnBankAccount.ClientID) & "','" & _
    '                Trim(txtAmountReceive.ClientID) & _
    '                "',((this.selectedIndex == -1) ? null : ListData[this.selectedIndex]));"
    '    End Function

    '    Private Function GenerateScript(ByVal DtTable As DataTable) As String
    '        Dim strScript As String
    '        Dim strScript1 As String
    '        Dim strType As String
    '        Dim IsGT As Boolean
    '        Dim DataRow As DataRow()
    '        Dim i As Int32
    '        Dim j As Int32
    '        Dim k As Int32

    '        strScript = "<script language=""JavaScript"">" & vbCrLf
    '        strScript &= " var a; " & vbCrLf
    '        strScript &= "ListData = new Array(" & vbCrLf
    '        strType = ""

    '        If Me.WithOutPrepaid Then
    '            k = cmbWayOfPayment.Items.Count - 1
    '        Else
    '            k = cmbWayOfPayment.Items.Count - 2
    '        End If

    '        For j = 0 To k
    '            'DataRow = DtTable.Select("Type = '" & Left(cmbWayOfPayment.Items(j).Value.Trim, 1) & "'")
    '            'DataRow = DtTable.Select(" IsgenerateTextAvailable = 1 ")

    '            If cmbWayOfPayment.Items(j).Value.Trim = "GT" Then
    '                DataRow = DtTable.Select(" IsgenerateTextAvailable = 1 ")
    '                IsGT = True
    '            Else
    '                DataRow = DtTable.Select("Type = '" & Left(cmbWayOfPayment.Items(j).Value.Trim, 1) & "'")
    '                IsGT = False
    '            End If

    '            If DataRow.Length > 0 Then
    '                For i = 0 To DataRow.Length - 1
    '                    If strType <> CStr(DataRow(i)("Type")).Trim Or IsGT Then
    '                        strType = CStr(DataRow(i)("Type")).Trim
    '                        strScript &= "new Array(" & vbCrLf
    '                        strScript1 = ""
    '                    End If

    '                    If strScript1 = "" Then
    '                        strScript1 = " new Array('" & CStr(DataRow(i)("Name")).Trim & "','" & CStr(DataRow(i)("ID")).Trim & "') "
    '                    Else
    '                        strScript1 &= "," & vbCrLf & " new Array('" & CStr(DataRow(i)("Name")).Trim & "','" & CStr(DataRow(i)("ID")).Trim & "') "
    '                    End If
    '                Next
    '                strScript &= strScript1 & ")," & vbCrLf
    '            Else
    '                strScript &= " null," & vbCrLf
    '            End If
    '        Next

    '        If Right(strScript.Trim, 5) = "null," Then
    '            strScript = Left(strScript.Trim, strScript.Trim.Length - 1)
    '        Else
    '            strScript = Left(strScript.Trim, strScript.Trim.Length - 2)
    '        End If

    '        strScript &= vbCrLf & "));" & vbCrLf
    '        strScript &= "</script>"
    '        Return strScript
    '    End Function


    '    'Private Function GenerateScript(ByVal DtTable As DataTable) As String
    '    '    Return "<script language=""JavaScript"">ListData = new Array(null,new Array(new Array('PETTY CASH HO','PTCS001') ),new Array(new Array('BANK YUDHA BAKTI GIRO','BYB') ),new Array( new Array('DUMMY - ANISA','BANK000')));</script>"
    '    'End Function

    '#End Region

    'Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)
    '    Dim DtChild As DataTable = Me.ListBankAccount
    '    Dim i As Integer

    '    For i = 0 To DtChild.Rows.Count - 1
    '        Page.ClientScript.RegisterForEventValidation(cmbBankAccount.UniqueID, CStr(DtChild.Rows(i).Item("ID")).Trim)
    '    Next

    '    MyBase.Render(writer)
    'End Sub

    Protected Sub bindBeneficiaryBankAcc()
        With UcBankAccount1
            .ValidatorTrue()
            .BindBankAccount()
        End With
    End Sub

    Private Sub cmbWayOfPayment_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbWayOfPayment.SelectedIndexChanged
        bindBeneficiaryBankAcc()
        If cmbWayOfPayment.SelectedValue = "GT" Then
            pnlJenisTransfer.Visible = True
            loadComboBankAccount("IsgenerateTextAvailable = 1")
        Else
            pnlJenisTransfer.Visible = False
            loadComboBankAccount("Type = '" & Left(cmbWayOfPayment.SelectedValue.Trim, 1) & "'")
        End If
        RaiseEvent Changed(sender, e)
    End Sub

    Protected Sub loadComboBankAccount(ByVal strSelect As String)
        Dim dv1 As New DataView(Me.ListBankAccount, strSelect, "", DataViewRowState.Added)

        With cmbBankAccount
            .Items.Clear()
            .DataValueField = "ID"
            .DataTextField = "Name"
            .DataSource = dv1
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = "0"
        End With
    End Sub
End Class