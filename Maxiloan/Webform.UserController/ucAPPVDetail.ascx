﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucAPPVDetail.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.ucAPPVDetail" %>
<div class="form_box_title">
    <div class="form_single">
        <h4>
            PAYMENT VOUCHER DETAIL</h4>
    </div>
</div>
<div class="form_box">
    <div class="form_left">
        <label>
            Cabang</label>
        <asp:Label ID="lblBranch" runat="server"></asp:Label>
    </div>
    <div class="form_right">
        <label>
            Jenis A/P
        </label>
        <asp:Label ID="lblAPType" runat="server"></asp:Label>
    </div>
</div>
<div class="form_box">
    <div class="form_left">
        <label>
            No PV</label>
        <asp:Label ID="lblPVNo" runat="server"></asp:Label>
    </div>
    <div class="form_right">
        <label>
            Tanggal PV</label>
        <asp:Label ID="lblPVDate" runat="server"></asp:Label>
    </div>
</div>
<div class="form_box">
    <div class="form_left">
        <label>
            Cara Pembayaran</label>
        <asp:Label ID="lblWOP" runat="server"></asp:Label>
        <asp:Label ID="lblJdlAccount" runat="server"></asp:Label>
        <asp:Label ID="lblBankAccount" runat="server"></asp:Label>
    </div>
    <div class="form_right">
        <label>
            Permintaan dari</label>
        <asp:Label ID="lblRequest" runat="server"></asp:Label>
    </div>
</div>
<div class="form_box">
    <div class="form_left">
        <label>
            Perkiraan Saldo</label>
        <asp:Label ID="lblBalance" runat="server"></asp:Label>
    </div>
</div>
<div class="form_box">
    <div class="form_single">
        <label class="label_general">
            Catatan</label>
        <asp:TextBox ID="txtNotes" runat="server" TextMode="MultiLine" CssClass="multiline_textbox"></asp:TextBox>
    </div>
</div>
