﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UcFinancialData.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.UcFinancialData" %>
<div class="form_title">
    <div class="form_single">
        <h3>
            VIEW - FINANCIAL
        </h3>
    </div>
</div>
<div class="form_box">
    <div>
        <div class="form_left">
            <label>
                Nama Customer
            </label>
            <asp:Label ID="lblCustName" runat="server" EnableViewState="False"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                No Aplikasi
            </label>
            <asp:Label ID="lblAppID" runat="server" EnableViewState="False"></asp:Label>
        </div>
    </div>
</div>
<div class="form_box">
    <div>
        <div class="form_left">
            <label>
                Jenis Bunga
            </label>
            <asp:Label ID="lblInterestType" runat="server" EnableViewState="False"></asp:Label>
        </div>
    </div>
</div>
<div class="form_box">
    <div>
        <div class="form_left">
            <label>
                Skema Angsuran
            </label>
            <asp:Label ID="lblInstScheme" runat="server" EnableViewState="False"></asp:Label>
        </div>
    </div>
</div>
<div class="form_box">
    <div>
        <div class="form_left">
            <label>
                PREMI ASURANSI
            </label>
            PREMI ASURANSI BAYAR DIMUKA
        </div>
    </div>
</div>
<div class="form_box">
    <div>
        <div class="form_left">
            <label>
                Asuransi Asset
            </label>
            <asp:Label ID="lblInsPremium" runat="server" EnableViewState="False"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Asuransi Asset
            </label>
            <asp:Label ID="lblInsAdv" runat="server" EnableViewState="False"></asp:Label>
        </div>
    </div>
</div>
<div class="form_box_title">
    <div class="form_single">
        <h4>
            ASURANSI DIKREDITKAN/CAPITALIZED
        </h4>
    </div>
</div>
<div class="form_box">
    <div>
        <div class="form_left">
            <label>
                Asuransi Asset
            </label>
            <asp:Label ID="lblInsCapitalized" runat="server" EnableViewState="False"></asp:Label>
        </div>
    </div>
</div>
<div class="form_box_title">
    <div class="form_single">
        <h4>
            DATA FINANCIAL
        </h4>
    </div>
</div>
<div class="form_box">
    <div>
        <div class="form_left">
            <label>
                Total Harga OTR
            </label>
            <asp:Label ID="lblOTR" runat="server" EnableViewState="False"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Uang Muka
            </label>
            <asp:Label ID="lblDP" runat="server" EnableViewState="False"></asp:Label>
        </div>
    </div>
</div>
<div class="form_box">
    <div>
        <div class="form_left">
            <label>
                NTF / Pokok Hutang
            </label>
            <asp:Label ID="lblNTF" runat="server" EnableViewState="False"></asp:Label>
        </div>
    </div>
</div>
<div class="form_box">
    <div>
        <div class="form_left">
            <label>
                Bunga Effective
            </label>
            <asp:Label ID="lblEffectiveRate" runat="server" EnableViewState="False"></asp:Label>&nbsp;%
        </div>
        <div class="form_right">
            <label>
                Bunga Flat
            </label>
            <asp:Label ID="lblFlatRate" runat="server" EnableViewState="False"></asp:Label>&nbsp;%
        </div>
    </div>
</div>
<div class="form_box">
    <div>
        <div class="form_left">
            <label>
                Bunga Supplier
            </label>
            <asp:Label ID="lblSuppRate" runat="server" EnableViewState="False"></asp:Label>&nbsp;%
        </div>
    </div>
</div>
<div class="form_box">
    <div>
        <div class="form_left">
            <label>
                Frequency Pembayaran
            </label>
            <asp:Label ID="lblPymtFreq" runat="server" EnableViewState="False"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Angsuran Pertama
            </label>
            <asp:Label ID="lblFirstInst" runat="server" EnableViewState="False"></asp:Label>
        </div>
    </div>
</div>
<div class="form_box">
    <div>
        <div class="form_left">
            <label>
                Jangka Waktu Angsuran
            </label>
            <asp:Label ID="lblNumInst" runat="server" EnableViewState="False"></asp:Label>&nbsp;Tenor
            <asp:Label ID="lblTenor" runat="server" EnableViewState="False"></asp:Label>
        </div>
    </div>
</div>
<div class="form_box">
    <div>
        <div class="form_left">
            <label>
                Jumlah Angsuran
            </label>
            <asp:Label ID="lblInstAmt" runat="server" EnableViewState="False"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Gross Yield
            </label>
            <asp:Label ID="lblGross" runat="server" EnableViewState="False"></asp:Label>&nbsp;%
        </div>
    </div>
</div>
<div class="form_box">
    <div>
        <div class="form_left">
            <label>
                Grace Period
            </label>
            <asp:Label ID="lblGrace1" runat="server" EnableViewState="False"></asp:Label>&nbsp;
            <asp:Label ID="lblGrace2" runat="server" EnableViewState="False"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                <asp:Label ID="lblLabel" runat="server" EnableViewState="False"></asp:Label>
            </label>
            <asp:Label ID="lblDiff" runat="server" EnableViewState="False"></asp:Label>
        </div>
    </div>
</div>
<div class="form_title">
    <div class="form_single">
        <h4>
            AMORTISASI
        </h4>
    </div>
</div>
<div class="form_box_header">
    <div class="form_single">
        <asp:DataGrid ID="dtgAmortization" runat="server" EnableViewState="False" Width="100%"
            CssClass="grid_general" CellSpacing="1" CellPadding="3" BorderWidth="0px" AutoGenerateColumns="False">
            <ItemStyle CssClass="item_grid" />
            <HeaderStyle CssClass="th" />
            <Columns>
                <asp:BoundColumn DataField="No" HeaderText="NO"></asp:BoundColumn>
                <asp:BoundColumn DataField="InstallmentAmount" HeaderText="ANGSURAN" DataFormatString="{0:###,###,##.#0;;0.00}"
                    ItemStyle-CssClass="item_grid_right" HeaderStyle-CssClass="th_right"></asp:BoundColumn>
                <asp:BoundColumn DataField="PrincipalAmount" HeaderText="POKOK" DataFormatString="{0:###,###,##.#0;;0.00}"
                    ItemStyle-CssClass="item_grid_right" HeaderStyle-CssClass="th_right"></asp:BoundColumn>
                <asp:BoundColumn DataField="InterestAmount" HeaderText="BUNGA" DataFormatString="{0:###,###,##.#0;;0.00}"
                    ItemStyle-CssClass="item_grid_right" HeaderStyle-CssClass="th_right"></asp:BoundColumn>
                <asp:BoundColumn DataField="OutstandingPrincipal" HeaderText="SISA POKOK" DataFormatString="{0:###,###,##.#0;;0.00}"
                    ItemStyle-CssClass="item_grid_right" HeaderStyle-CssClass="th_right"></asp:BoundColumn>
                <asp:BoundColumn DataField="OutstandingInterest" HeaderText="SISA BUNGA" DataFormatString="{0:###,###,##.#0;;0.00}"
                    ItemStyle-CssClass="item_grid_right" HeaderStyle-CssClass="th_right"></asp:BoundColumn>
            </Columns>
        </asp:DataGrid>
    </div>
</div>
