﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UcHasilSurvey.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.UcHasilSurvey" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../ucViewApplication.ascx" TagName="ucViewApplication" TagPrefix="uc3" %>
<%@ Register Src="../ucViewCustomerDetail.ascx" TagName="ucViewCustomerDetail" TagPrefix="uc2" %>
<%@ Register Src="../ucApplicationTab.ascx" TagName="ucApplicationTab" TagPrefix="uc7" %>
<%@ Register Src="../ucNumberFormat.ascx" TagName="ucNumberFormat" TagPrefix="uc1" %>

<div class="form_title">
    <div class="form_single">
        <h3>
            VIEW - HASIL SURVEY
        </h3>
    </div>
</div>
<div class="form_box_header">
    <div class="form_single">
        <h4>
            DATA CUSTOMER</h4>
    </div>
</div>
<div class="form_box">        
        <div class="form_left">
            <uc3:ucViewApplication ID="ucViewApplication1" runat="server" />
        </div>
        <div class="form_right">
            <uc2:ucViewCustomerDetail ID="ucViewCustomerDetail1" runat="server" />
        </div>    
</div>
<div class="form_box_header">
    <div class="form_single">
        <h4>
            HASIL SURVEY</h4>
    </div>
</div>
<div class="form_box">    
        <div class="form_left">
            <label>
                Tanggal Survey
            </label>
            <asp:Label runat="server" ID="lblTanggalSurvey"></asp:Label>            
        </div>
        <div class="form_right">
            <label>
                Saldo Rata-rata
            </label>
            <asp:Label runat="server" ID="lblSaldoRataRata"></asp:Label>            
        </div>    
</div>
<div class="form_box">
    <div>
        <div class="form_left">
            <label>
                Surveyor
            </label>
            <asp:Label runat="server" ID="lblSurveyor"></asp:Label>
            <asp:DropDownList runat="server" Visible="false" ID="cboSurveyor">
            </asp:DropDownList>
        </div>
        <div class="form_right">
            <label>
                Saldo Awal
            </label>
            <asp:Label runat="server" ID="lblSaldoAwal"></asp:Label>            
        </div>
    </div>
</div>
<div class="form_box">
    <div>
        <div class="form_left">
            <label>
                Lama Survey
            </label>
            <asp:Label runat="server" ID="lblLamaSurvey"></asp:Label>            
        </div>
        <div class="form_right">
            <label>
                Saldo Akhir
            </label>
            <asp:Label runat="server" ID="lblSaldoAkhir"></asp:Label>            
        </div>
    </div>
</div>
<div class="form_box">
    <div>
        <div class="form_left">
            <label>
                Credit Analyst
            </label>
            <asp:Label runat="server" ID="lblCreditAnalyst"></asp:Label>
            <asp:DropDownList runat="server" Visible="false" ID="cboCreditAnalyst">
            </asp:DropDownList>
        </div>
        <div class="form_right">
            <label>
                Jumlah Pemasukan
            </label>
            <asp:Label runat="server" ID="lblJumlahPemasukan"></asp:Label>            
        </div>
    </div>
</div>
<div class="form_box">
    <div>
        <div class="form_left">
            <label>
                Tanggal Konfirmasi Survey
            </label>
            <asp:Label runat="server" ID="lblTglKonfirmasiSurvey"></asp:Label>  
        </div>
        <div class="form_right">
            <label>
                Jumlah Pengeluaran
            </label>
            <asp:Label runat="server" ID="lblJumlahPengeluaran"></asp:Label>            
        </div>
    </div>
</div>
<div class="form_box">
    <div>
        <div class="form_left">
        </div>
        <div class="form_right">
            <label>
                Jumlah Hari Transaksi
            </label>
            <asp:Label runat="server" ID="lblJumlahHariTransaksi"></asp:Label>            
        </div>
    </div>
</div>
<div class="form_box">
    <div class="form_left">
    </div>
    <div class="form_right">
        <label>
            Jenis Rekening
        </label>
        <asp:Label runat="server" ID="lblBankAccType"></asp:Label>
        <asp:DropDownList Visible="false" ID="cboBankAccType" runat="server">
        </asp:DropDownList>
    </div>
</div>
<div class="form_box_title">
    <div class="form_single">
        <h4>
            DATA LAINNYA
        </h4>
    </div>
</div>
<div class="form_box">
    <div class="form_single">
        <label>
            Jumlah Kendaraan Yang Dimiliki?</label>
        <asp:Label runat="server" ID="lblJumlahKendaraan"></asp:Label>        
    </div>
</div>
<div class="form_box">
    <div class="form_single">
        <label class="label_general">
            Ada Garasi?</label>
        <asp:Label runat="server" ID="lblGarasi"></asp:Label>
        <asp:RadioButtonList ID="rboGarasi" Visible="false" runat="server" RepeatDirection="Horizontal" CssClass="opt_single">
            <asp:ListItem Value="True">Ya</asp:ListItem>
            <asp:ListItem Value="False" Selected="True">Tidak</asp:ListItem>
        </asp:RadioButtonList>
    </div>
</div>
<div class="form_box">
    <div class="form_single">
        <label>
            Pertama Kali Kredit</label>
        <asp:Label runat="server" ID="lblPertamaKredit"></asp:Label>
        <asp:DropDownList ID="cboPertamaKredit" Visible="false" runat="server">
            <asp:ListItem Value="RO">Repeat Order</asp:ListItem>
            <asp:ListItem Selected="True" Value="YA">Ya</asp:ListItem>
            <asp:ListItem Value="TA">Tidak, Ada Bukti</asp:ListItem>
            <asp:ListItem Value="TT">Tidak, Tidak Ada Bukti</asp:ListItem>
            <asp:ListItem Value="AO">Additional Order</asp:ListItem>
        </asp:DropDownList>
    </div>
</div>
<div class="form_box">
    <div class="form_single">
        <label>
            Order Ke</label>
        <asp:Label runat="server" ID="lblOrderKe"></asp:Label>        
    </div>
</div>
<div class="form_box_title">
    <div class="form_single">
        <h4>
            HASIL ANALISA CREDIT ANALYST</h4>
    </div>
</div>
<div class="form_box">
    <div class="form_single">
        <asp:Label runat="server" ID="lblSurveyorNotes" ></asp:Label>        
    </div>
</div>
<div class="form_box">
    <div class="form_single box_important">
        <asp:Panel ID="pnlScoring" runat="server" Visible="false">
            <h3>
                CREDIT SCORE =
                <asp:Label ID="lblResult" runat="server" Visible="false"></asp:Label>
                <asp:Label ID="lblGrade" runat="server"></asp:Label>
            </h3>
        </asp:Panel>
    </div>
</div>
