﻿
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports System.Data.SqlClient
#End Region



Public Class UcAssetData
    Inherits ControlBased


#Region "Constanta"
    ' Dim style As String = "ACCACQ"
    Dim objrow As DataRow
    Dim intLoop As Integer
    Dim status As Boolean = True

    Private m_controller As New AssetDataController
    Private oApplication As New Parameter.Application
    Private m_ControllerApp As New ApplicationController
    Private oAssetMasterPriceController As New AssetMasterPriceController
    Private oApplicationController As New ApplicationController
#End Region

#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(viewstate("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("vwsCmdWhere") = Value
        End Set
    End Property
    Public Property ApplicationID() As String
        Get
            Return (CType(ViewState("ApplicationID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
    Public Property CustomerID() As String
        Get
            Return (CType(ViewState("CustomerID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property
    Public Property AssetDesc() As String
        Get
            Return (CType(ViewState("AssetDesc"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("AssetDesc") = Value
        End Set
    End Property

    Public Property AssetCode() As String
        Get
            Return (CType(ViewState("AssetCode"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("AssetCode") = Value
        End Set
    End Property

    Public Property DP() As String
        Get
            Return (CType(ViewState("DP"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("DP") = Value
        End Set
    End Property

    Public Property OTR() As String
        Get
            Return (CType(ViewState("OTR"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("OTR") = Value
        End Set
    End Property

    Public Property NU() As String
        Get
            Return (CType(ViewState("NU"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("NU") = Value
        End Set
    End Property

    Public Property App() As String
        Get
            Return (CType(ViewState("App"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("App") = Value
        End Set
    End Property

    Public Property SupplierID() As String
        Get
            Return (CType(ViewState("SupplierID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("SupplierID") = Value
        End Set
    End Property


    Public Property CustName() As String
        Get
            Return (CType(ViewState("CustName"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("CustName") = Value
        End Set
    End Property
    Public Property Asset() As String
        Get
            Return (CType(ViewState("Asset"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("Asset") = Value
        End Set
    End Property
    Public Property ProductOfferingID() As String
        Get
            Return (CType(ViewState("ProductOfferingID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("ProductOfferingID") = Value
        End Set
    End Property

    Public Property ProductID() As String
        Get
            Return (CType(ViewState("ProductID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("ProductID") = Value
        End Set
    End Property

    Public Property Mode() As String
        Get
            Return (CType(ViewState("Mode"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("Mode") = Value
        End Set
    End Property

    Public Property SalesID() As String
        Get
            Return (CType(ViewState("SalesID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("SalesID") = Value
        End Set
    End Property

    Public Property SupervisorID() As String
        Get
            Return (CType(ViewState("SupervisorID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("SupervisorID") = Value
        End Set
    End Property

    Public Property AdminID() As String
        Get
            Return (CType(ViewState("AdminID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("AdminID") = Value
        End Set
    End Property

    Property PageSource() As String
        Get
            Return ViewState("PageSource").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("PageSource") = Value
        End Set
    End Property
    Property style() As String
        Get
            Return ViewState("style").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("style") = Value
        End Set
    End Property

    Property CustomerType() As String
        Get
            Return ViewState("CustomerType").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerType") = Value
        End Set
    End Property
    Property Origination() As String
        Get
            Return ViewState("Origination").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("Origination") = Value
        End Set
    End Property
#End Region

#Region "Controls"
    Protected WithEvents ucViewApplication1 As New ucViewApplication
    Protected WithEvents ucViewCustomerDetail1 As New ucViewCustomerDetail
    'Protected WithEvents ucSupplier1 As ucLookUpSupplier
    Protected WithEvents ucSupplier1 As New ucSupplier
    Protected WithEvents ucSupplierKaroseri As New ucSupplier
    'Protected WithEvents ucSupplierKaroseri As ucLookUpSupplier
    'Protected WithEvents ucLookupAsset1 As UcLookupAsset
    Protected WithEvents ucLookupAsset1 As New ucAsset
    Protected WithEvents ucAO1 As New ucCMO
    'Protected WithEvents ucAO1 As ucAO
    Protected WithEvents UCAddress As New ucAddress
    'Protected WithEvents UCAddress As UcCompanyAddress
    Protected WithEvents ucApplicationTab1 As New ucApplicationTab
    Protected WithEvents ucOTR As New ucNumberFormat
    Protected WithEvents ucHargakaroseri As New ucNumberFormat
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        If Not Page.IsPostBack Then
            Me.ApplicationID = Request("ApplicationID")
            Me.CustomerID = Request("CustID")


            BindGrid()

        End If
    End Sub
    Sub BindUC()
        With ucViewApplication1
            .ApplicationID = Me.ApplicationID
            .initControls("OnNewApplication")
            .bindData()

            Me.CustomerID = .CustomerID
            Me.CustName = .CustomerName
            Me.Asset = .AssetTypeID
            Me.ProductID = .ProductID
            Me.ProductOfferingID = .ProductOfferingID
            Me.SupplierID = .SupplierID
            Me.CustomerType = .CustomerType
            Me.Origination = .Origination
        End With
        ucViewCustomerDetail1.CustomerID = Me.CustomerID
        ucViewCustomerDetail1.bindCustomerDetail()

        ' lblSupplierDealer.Text = Me.ApplicationID
    End Sub
    Sub BindGrid()
        BindUC()
        BindAssetEdit()
        BindAssetRegistration()
        GetDefaultAssetData()
    End Sub
    Sub BindAssetEdit()
        Dim oAssetData As New Parameter.AssetData
        Dim oData As New DataTable
        ''Me.CmdWhere = " AgreementAsset.ApplicationID  = '" & Me.ApplicationID & "' and AssetDocumentList.AssetTypeID = 'ALATBERAT'"
        Me.CmdWhere = " AgreementAsset.ApplicationID  = '" & Me.ApplicationID & "' and AssetDocumentList.AssetTypeID = '" & Me.Asset & "'"

        oAssetData.strConnection = GetConnectionString()
        'oAssetData.AppID = Me.ApplicationID
        'oAssetData.AssetID = Me.Asset
        'oAssetData.CustomerType = Me.CustomerType
        'oAssetData.Origination = Me.Origination
        oAssetData.WhereCond = Me.CmdWhere
        oAssetData.SpName = "spEditAssetDataAssetDoc"
        oAssetData = m_controller.EditAssetDataDocument(oAssetData)
        oData = oAssetData.ListData
        dtgAssetDoc.DataSource = oData
        dtgAssetDoc.DataBind()

    End Sub

    Sub BindAssetRegistration()
        Dim entitiesAssetData As New Parameter.AssetData
        Dim oAssetDataController As New AssetDataController
        Dim oDataRegistration As New DataTable

        With entitiesAssetData
            .AppID = Me.ApplicationID
            .strConnection = GetConnectionString()
            .SpName = "spEditAssetDataAssetRegistration"
        End With

        entitiesAssetData = oAssetDataController.EditGetAssetRegistration(entitiesAssetData)

        If Not entitiesAssetData Is Nothing Then
            oDataRegistration = entitiesAssetData.ListData
            If oDataRegistration.Rows.Count > 0 Then
                lblNamaBPKB.Text = oDataRegistration.Rows(0).Item("ownerasset").ToString
                rboBPKBanBadanUsaha.SelectedIndex = rboBPKBanBadanUsaha.Items.IndexOf(rboBPKBanBadanUsaha.Items.FindByValue(oDataRegistration.Rows(0).Item("OwnerAssetCompany").ToString.Trim))
                lblBPKBBadanUsaha.Text = rboBPKBanBadanUsaha.SelectedItem.Text
                LBLKeteranganBPKB.Text = oDataRegistration.Rows(0).Item("notes").ToString
                lblalamat.Text = oDataRegistration.Rows(0).Item("owneraddress").ToString
                lblRt.Text = oDataRegistration.Rows(0).Item("ownerrt").ToString
                lblRw.Text = oDataRegistration.Rows(0).Item("ownerrw").ToString
                lblKelurahan.Text = (oDataRegistration.Rows(0).Item("ownerkelurahan").ToString)
                lblKecamatan.Text = oDataRegistration.Rows(0).Item("ownerkecamatan").ToString
                lblKota.Text = oDataRegistration.Rows(0).Item("ownercity").ToString
                lblKodePos.Text = oDataRegistration.Rows(0).Item("ownerzipcode").ToString
                lblTanggalPajakSTNK.Text = oDataRegistration.Rows(0).Item("taxdate").ToString
            End If
        End If
    End Sub
    Sub GetDefaultAssetData()
        Try
            Dim controllerassetData As New EditApplicationController
            Dim EntitiesAssetData As New Parameter.Application
            Dim oDataAsset As New DataTable
            Dim WhereSup As String
            With EntitiesAssetData
                .ApplicationID = Me.ApplicationID
                .strConnection = GetConnectionString()
                .SpName = "EditAssetDataGetInfoAssetData"
            End With
            EntitiesAssetData = controllerassetData.EditAssetDataGetInfoAssetData(EntitiesAssetData)
            If Not EntitiesAssetData Is Nothing Then
                oDataAsset = EntitiesAssetData.ListData
                If oDataAsset.Rows.Count > 0 Then
                    Me.SupplierID = oDataAsset.Rows(0).Item("SupplierID").ToString
                    lblSupplierDealer.Text = oDataAsset.Rows(0).Item("SupplierName").ToString
                    lblSupplierDealerKaroseri.Text = oDataAsset.Rows(0).Item("SupplierNameKaroseri").ToString
                    lblKendaraan.Text = oDataAsset.Rows(0).Item("Description").ToString
                    lblHargaKaroseri.Text = FormatNumber(oDataAsset.Rows(0).Item("HargaKaroseri"), 0)
                    lblTahunProduksi.Text = oDataAsset.Rows(0).Item("ManufacturingYear").ToString
                    lblKategoriAsset.Text = oDataAsset.Rows(0).Item("CategoryID").ToString
                    lblWarna.Text = oDataAsset.Rows(0).Item("Color").ToString
                    lblHargaOTR.Text = FormatNumber(oDataAsset.Rows(0).Item("OTRPrice"), 0)
                    lblhideOTRPrice.Text = FormatNumber(oDataAsset.Rows(0).Item("OTRPrice"), 0)
                    'chkSplitBayar.Checked = CBool(oDataAsset.Rows(0).Item("SplitPembayaran").ToString)
                    lblhideHargaKaroseri.Text = FormatNumber(oDataAsset.Rows(0).Item("HargaKaroseri"), 0)
                    lblTotalHargaOTR.Text = FormatNumber(CDbl(lblhideOTRPrice.Text) + CDbl(lblhideHargaKaroseri.Text), 0)
                    lblNoChasis.Text = oDataAsset.Rows(0).Item("SerialNo1").ToString
                    lblNoMesin.Text = oDataAsset.Rows(0).Item("SerialNo2").ToString
                    lblNoPolisi.Text = oDataAsset.Rows(0).Item("Licplate").ToString

                    WhereSup = "SupplierID = '" & Me.SupplierID & "'"
                    FillCboEmp("SupplierEmployee", cboSalesman, WhereSup)
                    FillCboEmp("SupplierEmployee", cboSalesSpv, WhereSup)
                    FillCboEmp("SupplierEmployee", cboSupplierAdm, WhereSup)
                    cboSalesman.SelectedIndex = cboSalesman.Items.IndexOf(cboSalesman.Items.FindByValue(oDataAsset.Rows(0).Item("SalesmanID").ToString))
                    'lblSalesman.Text = cboSalesman.SelectedItem.Text
                    lblSalesman.Text = oDataAsset.Rows(0).Item("Salesman").ToString
                    cboSalesSpv.SelectedIndex = cboSalesSpv.Items.IndexOf(cboSalesSpv.Items.FindByValue(oDataAsset.Rows(0).Item("SalesSupervisorID").ToString))
                    lblSalesSupervisor.Text = cboSalesSpv.SelectedItem.Text
                    cboSupplierAdm.SelectedIndex = cboSupplierAdm.Items.IndexOf(cboSupplierAdm.Items.FindByValue(oDataAsset.Rows(0).Item("SupplierAdminID").ToString))
                    lblSupplierAdmin.Text = cboSupplierAdm.SelectedItem.Text

                    FillCbo(cboUsage, "tblAssetUsage")
                    FillCbo(cboInsuredBy, "tblInsuredBy")
                    FillCbo(cboPaidBy, "tblPaidBy")
                    cboUsage.SelectedIndex = cboUsage.Items.IndexOf(cboUsage.Items.FindByValue(oDataAsset.Rows(0).Item("AssetUsage").ToString))
                    lblPenggunaan.Text = cboUsage.SelectedItem.Text
                    cboInsuredBy.SelectedIndex = cboInsuredBy.Items.IndexOf(cboInsuredBy.Items.FindByValue(oDataAsset.Rows(0).Item("InsAssetInsuredBy").ToString))
                    lblDiAsuransiOleh.Text = cboInsuredBy.SelectedItem.Text
                    cboPaidBy.SelectedIndex = cboPaidBy.Items.IndexOf(cboPaidBy.Items.FindByValue(oDataAsset.Rows(0).Item("InsAssetPaidBy").ToString))
                    lblDibayaroleh.Text = cboPaidBy.SelectedItem.Text
                    lblUangMuka.Text = cboUangMukaBayar.SelectedItem.Text
                    cboPencairanKe.SelectedIndex = cboPencairanKe.Items.IndexOf(cboPencairanKe.Items.FindByValue(oDataAsset.Rows(0).Item("PencairanKe").ToString))
                    lblPencairanKe.Text = cboPencairanKe.SelectedItem.Text
                    rboKaroseri.SelectedValue = oDataAsset.Rows(0).Item("isKaroseri").ToString
                    lblKaroseri.Text = rboKaroseri.SelectedItem.Text
                    rboSplitPembayaran.SelectedValue = oDataAsset.Rows(0).Item("SplitPembayaran").ToString
                    lblSplitPembayaran.Text = rboSplitPembayaran.SelectedItem.Text
                    If oDataAsset.Rows(0).Item("TaxDate").ToString <> "" Then
                        lblTanggalPajakSTNK.Text = Format(oDataAsset.Rows(0).Item("TaxDate"), "dd/MM/yyyy")
                    Else
                        lblTanggalPajakSTNK.Text = ""
                    End If
                    lblBPKBsamadenganKontrak.Text = rboNamaBPKBSamaKontrak.SelectedItem.Text
                    rboBPKBPengganti.SelectedValue = oDataAsset.Rows(0)("isBPKBPengganti").ToString
                    lblBPKBPengganti.Text = rboBPKBPengganti.SelectedItem.Text
                    If lblTanggalPajakSTNK.Text <> "" Then
                        If ConvertDate2(lblTanggalPajakSTNK.Text) <= Me.BusinessDate Then
                            '  divTglSTNKNotes.Visible = True
                            lblTanggalPajakSTNK.Text = oDataAsset.Rows(0)("AlasanSTNKExpired").ToString
                        End If
                    End If
                    LBLKeteranganBPKB.Text = oDataAsset.Rows(0).Item("Notes").ToString
                    cboInsuredBy.SelectedIndex = cboInsuredBy.Items.IndexOf(cboInsuredBy.Items.FindByValue(oDataAsset.Rows(0).Item("InsAssetInsuredBy").ToString))
                    cboPaidBy.SelectedIndex = cboPaidBy.Items.IndexOf(cboPaidBy.Items.FindByValue(oDataAsset.Rows(0).Item("InsAssetPaidBy").ToString))
                    lblCMO.Text = oDataAsset.Rows(0).Item("AOName").ToString
                End If
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub

#Region "FillCbo"
    Sub FillCboEmp(ByVal Table As String, ByVal cboName As DropDownList, ByVal Where As String)
        Dim oAssetData As New Parameter.AssetData
        Dim oData As New DataTable
        oAssetData.strConnection = GetConnectionString()
        oAssetData.Where = Where
        oAssetData.Table = Table
        oAssetData = m_controller.GetCboEmp(oAssetData)
        oData = oAssetData.ListData
        cboName.DataSource = oData
        cboName.DataTextField = "Name"
        cboName.DataValueField = "ID"
        cboName.DataBind()
        cboName.Items.Insert(0, "Select One")
        cboName.Items(0).Value = "Select One"
    End Sub
    Sub FillCbo(ByVal cboName As DropDownList, ByVal Table As String)
        Dim oAssetData As New Parameter.AssetData
        Dim oData As New DataTable
        oAssetData.strConnection = GetConnectionString()
        oAssetData.Table = Table
        oAssetData = m_controller.GetCbo(oAssetData)
        oData = oAssetData.ListData
        cboName.DataSource = oData
        cboName.DataTextField = "Description"
        cboName.DataValueField = "ID"
        cboName.DataBind()
        cboName.Items.Insert(0, "Select One")
        cboName.Items(0).Value = "Select One"
    End Sub
#End Region

    
    Private Sub dtgAssetDoc_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgAssetDoc.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            CType(e.Item.FindControl("lblNo"), Label).Text = CStr(e.Item.ItemIndex + 1)
        End If
    End Sub

    Sub hideRT()
        HdnRT.Attributes("style") = "display: none;"
    End Sub
    Sub hideRW()
        HdnRW.Attributes("style") = "display: none;"
    End Sub
    Sub HideKelurahan()
        HdnKelurahan.Attributes("style") = "display: none;"
    End Sub
    Sub HideKecamatan()
        HdnKecamatan.Attributes("style") = "display: none;"
    End Sub
    Sub Hidexx()
        HdnSalesSupervisor.Attributes("style") = "display: none;"
        HdnSupplierAdmin.Attributes("style") = "display: none;"
        lblSalesSupervisor.Visible = False
        lblSupplierAdmin.Visible = False
    End Sub
End Class