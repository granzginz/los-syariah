﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UcAssetData.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.UcAssetData" %>
<%--<%@ Register TagPrefix="uc1" TagName="UcViewInsuranceDetail" Src="../UcViewInsuranceDetail.ascx" %>--%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcCompanyAdress" Src="../ucCompanyAddress.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ValidDate" Src="../ValidDate.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UCAO" Src="../UCAO.ascx" %>
<%@ Register TagPrefix="uc1" TagName="uccmo" Src="../uccmo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucLookUpAsset" Src="../ucLookUpAsset.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucAsset" Src="../ucAsset.ascx" %>
<%@ Register Src="../ucViewApplication.ascx" TagName="ucViewApplication" TagPrefix="uc3" %>
<%@ Register Src="../ucViewCustomerDetail.ascx" TagName="ucViewCustomerDetail" TagPrefix="uc2" %>
<%@ Register TagPrefix="uc1" TagName="UcBankAccount" Src="../UcBankAccount.ascx" %>
<%@ Register Src="../ucLookUpSupplier.ascx" TagName="ucLookUpSupplier" TagPrefix="uc1" %>
<%@ Register Src="../ucSupplier.ascx" TagName="ucSupplier" TagPrefix="uc1" %>
<%@ Register Src="../ucApplicationTab.ascx" TagName="ucApplicationTab" TagPrefix="uc7" %>
<%@ Register Src="../ucNumberFormat.ascx" TagName="ucNumberFormat" TagPrefix="uc7" %>
<%@ Register TagPrefix="uc1" TagName="ucAddress" Src="../ucAddress.ascx" %>
<asp:Label ID="lblMessage" runat="server" EnableViewState="False"></asp:Label>
<div class="form_box">
    <div class="title_strip">
    </div>
    <div class="form_left">
        <uc3:ucviewapplication id="ucViewApplication1" runat="server" />
    </div>
    <div class="form_right">
        <uc2:ucviewcustomerdetail id="ucViewCustomerDetail1" runat="server" />
    </div>
</div>
<div class="form_box_header">
    <div class="form_single">
        <h4>
            DATA ASSET</h4>
    </div>
</div>
<div class="form_box">
    <%--<div class="form_single">--%>
    <div class="form_left">
        <label class="label_general">
            Supplier / Dealer</label>
        <asp:Label runat="server" ID="lblSupplierDealer"></asp:Label>
    </div>
    <div class="form_right">
        <label class="label_general">
            Supplier / Dealer Karoseri</label>
        <asp:Label runat="server" ID="lblSupplierDealerKaroseri"></asp:Label>
    </div>
</div>
<div class="form_box">
    <%--<div class="form_single">--%>
    <div class="form_left">
        <label class="label_general">
            Nama Asset
        </label>
        <asp:Label runat="server" ID="lblKendaraan"></asp:Label>
    </div>
    <div class="form_right">
        <label class="label_general">
            Harga Karoseri</label>
        <asp:Label runat="server" ID="lblHargaKaroseri"></asp:Label>
    </div>
</div>
<div class="form_box">
    <%--<div class="form_single">--%>
    <div class="form_left">
        <label class="label_general">
            Tahun Produksi
        </label>
        <asp:Label runat="server" ID="lblTahunProduksi"></asp:Label>
    </div>
    <div class="form_right">
        <label class="label_general">
            Kategori Asset</label>
        <asp:Label runat="server" ID="lblKategoriAsset"></asp:Label>
    </div>
</div>
<div class="form_box">
    <%--<div class="form_single">--%>
    <div class="form_left">
        <label class="label_general">
            Karoseri</label>
        <asp:RadioButtonList runat="server" ID="rboKaroseri" Visible="false" CssClass="opt_single"
            RepeatDirection="Horizontal">
            <asp:ListItem Value="True" Text="Ya"></asp:ListItem>
            <asp:ListItem Value="False" Text="Tidak"></asp:ListItem>
        </asp:RadioButtonList>
        <asp:Label runat="server" ID="lblKaroseri"></asp:Label>
        <asp:Label runat="server" ID="lblhideOTRPrice" Visible="false"></asp:Label>
        <asp:Label runat="server" ID="lblhideHargaKaroseri" Visible="false"></asp:Label>
    </div>
    <div class="form_right">
        <label class="label_general">
            Split Pembayaran
        </label>
        <asp:RadioButtonList runat="server" ID="rboSplitPembayaran" Visible="false" CssClass="opt_single"
            RepeatDirection="Horizontal">
            <asp:ListItem Value="True" Text="Ya"></asp:ListItem>
            <asp:ListItem Value="False" Text="Tidak"></asp:ListItem>
        </asp:RadioButtonList>
        <asp:Label runat="server" ID="lblSplitPembayaran"></asp:Label>
    </div>
</div>
<div class="form_box">
    <div class="form_left">
        <label class="label_general">
            Harga OTR
        </label>
        <asp:Label runat="server" ID="lblHargaOTR"></asp:Label>
    </div>
    <div class="form_right">
        <label class="label_general">
            Uang Muka Di Bayar
        </label>
        <asp:DropDownList ID="cboUangMukaBayar" runat="server" Visible="false">
            <asp:ListItem Selected="True" Text="SUPPLIER" Value="S"></asp:ListItem>
            <asp:ListItem Text="ANDALAN FINANCE" Value="A"></asp:ListItem>
        </asp:DropDownList>
        <asp:Label runat="server" ID="lblUangMuka"></asp:Label>
    </div>
</div>
<div class="form_box">
    <div class="form_left">
        <label class="label_general">
            Total Harga OTR
        </label>
        <asp:Label runat="server" ID="lblTotalHargaOTR"></asp:Label>
    </div>
    <div class="form_right">
        <label class="label_general">
            Penggunaan
        </label>
        <asp:DropDownList ID="cboUsage" runat="server" Visible="false">
        </asp:DropDownList>
        <asp:Label runat="server" ID="lblPenggunaan"></asp:Label>
    </div>
</div>
<div class="form_box">
    <div class="form_left">
        <label class="label_general">
            MRP / Harga Pasar
        </label>
        <asp:Label runat="server" ID="lblHargaPasar"></asp:Label>
    </div>
    <div class="form_right">
        <label class="label_general">
            Pencairan Ke
        </label>
        <asp:DropDownList ID="cboPencairanKe" runat="server" Visible="false">
            <asp:ListItem Text="SUPPLIER" Value="S" Selected="True"></asp:ListItem>
            <asp:ListItem Text="CUSTOMER" Value="A"></asp:ListItem>
        </asp:DropDownList>
        <asp:Label runat="server" ID="lblPencairanKe"></asp:Label>
    </div>
</div>
<div class="form_box_header">
    <div class="form_single">
        <h4>
            ASSET INFO</h4>
    </div>
</div>
<div class="form_box">
    <div class="form_left">
        <label class="label_general">
            Warna
        </label>
        <asp:Label runat="server" ID="lblWarna"></asp:Label>
    </div>
    <div class="form_right">
        <label class="label_general">
            No Chasis
        </label>
        <asp:Label runat="server" ID="lblNoChasis"></asp:Label>
    </div>
</div>
<div class="form_box">
    <div class="form_left">
        <label class="label_general">
            No Polisi
        </label>
        <asp:Label runat="server" ID="lblNoPolisi"></asp:Label>
    </div>
    <div class="form_right">
        <label class="label_general">
            No Mesin
        </label>
        <asp:Label runat="server" ID="lblNoMesin"></asp:Label>
    </div>
</div>
<div class="form_box_header">
    <div class="form_single">
        <h4>
            REGISTRASI DOKUMEN ASSET</h4>
    </div>
</div>
<div class="form_box">
    <div class="form_left">
        <label class="label_general">
            Nama Dokumen Asset sama dengan Kontrak
        </label>
        <asp:RadioButtonList ID="rboNamaBPKBSamaKontrak" runat="server" Visible="false" AutoPostBack="True"
            CssClass="opt_single" RepeatDirection="Horizontal">
            <asp:ListItem Value="True" Text="Ya"></asp:ListItem>
            <asp:ListItem Value="False" Text="Tidak" Selected="True"></asp:ListItem>
        </asp:RadioButtonList>
        <asp:Label runat="server" ID="lblBPKBsamadenganKontrak"></asp:Label>
    </div>
    <div class="form_right">
        <label class="label_general">
            Dokumen Asset Pengganti
        </label>
        <asp:RadioButtonList ID="rboBPKBPengganti" runat="server" AutoPostBack="True" Visible="false"
            CssClass="opt_single" RepeatDirection="Horizontal">
            <asp:ListItem Value="True" Text="Ya"></asp:ListItem>
            <asp:ListItem Value="False" Text="Bukan" Selected="True"></asp:ListItem>
        </asp:RadioButtonList>
        <asp:Label runat="server" ID="lblBPKBPengganti"></asp:Label>
    </div>
</div>
<div class="form_box">
    <div class="form_left">
        <label class="label_general">
            Nama Dokumen Asset
        </label>
        <asp:Label runat="server" ID="lblNamaBPKB"></asp:Label>
    </div>
    <div class="form_right">
        <label class="label_general">
            Dokumen Asset an Badan Usaha
        </label>
        <asp:RadioButtonList ID="rboBPKBanBadanUsaha" runat="server" Visible="false" AutoPostBack="True"
            CssClass="opt_single" RepeatDirection="Horizontal">
            <asp:ListItem Value="True" Text="Ya"></asp:ListItem>
            <asp:ListItem Value="False" Text="Bukan" Selected="True"></asp:ListItem>
        </asp:RadioButtonList>
        <asp:Label runat="server" ID="lblBPKBBadanUsaha"></asp:Label>
    </div>
</div>
<div class="form_box">
    <div class="form_single">
        <label class="label_general">
            Alamat
        </label>
        <asp:Label runat="server" ID="lblalamat"></asp:Label>
    </div>
</div>
<div class="form_box" runat="server" id="HdnRT">
    <div class="form_single">
        <label class="label_general">
            RT
        </label>
        <asp:Label runat="server" ID="lblRt"></asp:Label>
    </div>
</div>
<div class="form_box" runat="server" id="HdnRW">
    <div class="form_single">
        <label class="label_general">
            RW
        </label>
        <asp:Label runat="server" ID="lblRw"></asp:Label>
    </div>
</div>
<div class="form_box" runat="server" id="HdnKelurahan">
    <div class="form_single">
        <label class="label_general">
            Kelurahan
        </label>
        <asp:Label runat="server" ID="lblKelurahan"></asp:Label>
    </div>
</div>
<div class="form_box" runat="server" id="HdnKecamatan">
    <div class="form_single">
        <label class="label_general">
            Kecamatan
        </label>
        <asp:Label runat="server" ID="lblKecamatan"></asp:Label>
    </div>
</div>
<div class="form_box">
    <div class="form_single">
        <label class="label_general">
            Kota
        </label>
        <asp:Label runat="server" ID="lblKota"></asp:Label>
    </div>
</div>
<div class="form_box">
    <div class="form_single">
        <label class="label_general">
            Kode Pos
        </label>
        <asp:Label runat="server" ID="lblKodePos"></asp:Label>
    </div>
</div>
<div class="form_box">
    <div class="form_single">
        <label class="label_general">
            Tanggal Pajak STNK
        </label>
        <asp:Label runat="server" ID="lblTanggalPajakSTNK"></asp:Label>
    </div>
</div>
<div class="form_box">
    <div class="form_single">
        <label class="label_general">
            Keterangan Dokumen Asset
        </label>
        <asp:Label runat="server" ID="LBLKeteranganBPKB"></asp:Label>
    </div>
</div>
<div class="form_box_header">
    <div class="form_single">
        <h4>
            ASURANSI ASSET
        </h4>
    </div>
</div>
<div class="form_box">
    <div class="form_single">
        <label class="label_general">
            Di Asuransi Oleh
        </label>
        <asp:DropDownList ID="cboInsuredBy" runat="server" Visible="false">
        </asp:DropDownList>
        <asp:Label runat="server" ID="lblDiAsuransiOleh"></asp:Label>
    </div>
</div>
<div class="form_box">
    <div class="form_single">
        <label class="label_general">
            Di bayar oleh
        </label>
        <asp:DropDownList ID="cboPaidBy" runat="server" Visible="False">
        </asp:DropDownList>
        <asp:Label ID="lblDibayaroleh" runat="server"></asp:Label>
    </div>
</div>
<div class="form_box_header">
    <div class="form_left">
        <h4>
            KARYAWAN
        </h4>
    </div>
    <div class="form_right">
        <h4>
            KARYAWAN SUPPLIER
        </h4>
    </div>
</div>
<div class="form_box">
    <div>
        <div class="form_left">
            <label class="label_general">
                CMO
            </label>
            <asp:Label ID="lblCMO" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Salesman
            </label>
            <asp:Label ID="lblSalesman" runat="server"></asp:Label>
            <asp:DropDownList ID="cboSalesman" runat="server" AutoPostBack="true" Visible="false">
            </asp:DropDownList>
        </div>
    </div>
</div>
<div class="form_box" runat="server" id="HdnSalesSupervisor">
    <div>
        <div class="form_left">
        </div>
        <div class="form_right">
            <label>
                Sales Supervisor
            </label>
            <asp:DropDownList ID="cboSalesSpv" runat="server" Visible="false">
            </asp:DropDownList>
            <asp:Label runat="server" ID="lblSalesSupervisor"></asp:Label>
        </div>
    </div>
</div>
<div class="form_box" runat="server" id="HdnSupplierAdmin">
    <div>
        <div class="form_left">
            <label>
            </label>
        </div>
        <div class="form_right">
            <label>
                Supplier Admin
            </label>
            <asp:DropDownList ID="cboSupplierAdm" runat="server" Visible="false">
            </asp:DropDownList>
            <asp:Label runat="server" ID="lblSupplierAdmin"></asp:Label>
        </div>
    </div>
</div>
<div class="form_box_title">
    <div class="form_single">
        <h4>
            DOKUMEN ASSET</h4>
    </div>
</div>
<div class="form_box_header">
    <div class="form_single">
        <asp:DataGrid ID="dtgAssetDoc" runat="server" AutoGenerateColumns="False" BorderWidth="0"
            BorderStyle="None" CssClass="grid_general">
            <HeaderStyle CssClass="th" />
            <ItemStyle CssClass="item_grid" />
            <Columns>
                <asp:TemplateColumn HeaderText="NO">
                    <ItemStyle />
                    <ItemTemplate>
                        <asp:Label ID="lblNo" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:BoundColumn DataField="AssetDocName" HeaderText="DOKUMEN"></asp:BoundColumn>
                <asp:TemplateColumn HeaderText="NO DOKUMEN">
                    <ItemStyle />
                    <ItemTemplate>
                        <asp:Label ID="lblNumber" runat="server" Text='<%# DataBinder.eval(Container,"DataItem.DocumentNo") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="PERIKSA">
                    <ItemStyle CssClass="short_col" />
                    <ItemTemplate>
                        <asp:CheckBox ID="chk" runat="server" Enabled="false" Visible="true" Checked='<%# DataBinder.eval(Container,"DataItem.IsDocExist") %>'>
                        </asp:CheckBox>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="CATATAN">
                    <ItemTemplate>
                        <asp:Label ID="lblNotes" runat="server" Text='<%# DataBinder.eval(Container,"DataItem.Notes") %>'
                            CssClass="desc_textbox"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:BoundColumn Visible="False" DataField="MandatoryForNewAsset"></asp:BoundColumn>
                <asp:BoundColumn Visible="False" DataField="MandatoryForUsedAsset"></asp:BoundColumn>
                <asp:BoundColumn Visible="False" DataField="AssetDocID"></asp:BoundColumn>
                <asp:BoundColumn Visible="False" DataField="IsMainDoc"></asp:BoundColumn>
                <asp:BoundColumn Visible="False" DataField="IsValueNeeded"></asp:BoundColumn>
            </Columns>
        </asp:DataGrid>
    </div>
</div>
