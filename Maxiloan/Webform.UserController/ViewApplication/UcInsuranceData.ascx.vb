﻿Imports Maxiloan.Controller
Imports System.Data.SqlClient

Public Class UcInsuranceData
    Inherits ControlBased

#Region "Constanta"
    Dim pagesource As String
    Dim back As String

    Protected WithEvents oViewInsuranceDetail As UcViewInsuranceDetail
    Protected WithEvents oViewAssetInsuranceDetail As UcViewAssetInsuranceDetail
    Protected WithEvents oViewAssetInsuranceDetailGridtenor As UcViewAssetInsuranceDetailGridTenor
#End Region

#Region "Property"

    Public Property ApplicationID() As String
        Get
            Return (CType(ViewState("ApplicationID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property

    Public Property BranchID() As String
        Get
            Return (CType(ViewState("BranchID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("BranchID") = Value
        End Set
    End Property

#End Region

    Public Sub BindData()
        Cache.Insert("CacheViewInsuranceDetailParamaterApplicationID", Me.ApplicationID)
        Cache.Insert("CacheViewInsuranceDetailParamaterBranchID", Me.BranchID)
        oViewInsuranceDetail.BindData()
        oViewAssetInsuranceDetail.BindData()
        oViewAssetInsuranceDetailGridtenor.BindData()
    End Sub

End Class