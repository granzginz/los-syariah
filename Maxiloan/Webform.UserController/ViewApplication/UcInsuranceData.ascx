﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UcInsuranceData.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.UcInsuranceData" %>

<%@ Register TagPrefix="uc1" TagName="UcViewInsuranceDetail" Src="../UcViewInsuranceDetail.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcViewAssetInsuranceDetailGridTenor" Src="../UcViewAssetInsuranceDetailGridTenor.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcViewAssetInsuranceDetail" Src="../UcViewAssetInsuranceDetail.ascx" %>

<div class="form_title">
    <div class="title_strip">
    </div>
    <div class="form_single">
        <h3>
            DETAIL INFORMASI ASURANSI
        </h3>
    </div>
</div>
<div class="form_box_uc">
    <uc1:ucviewinsurancedetail id="oViewInsuranceDetail" runat="server"></uc1:ucviewinsurancedetail>
</div>
<div class="form_title">
    <div class="form_single">
        <h4>
            DETAIL INFORMASI ASURANSI ASSET
        </h4>
    </div>
</div>
<div class="form_box_uc">
    <uc1:ucviewassetinsurancedetail id="oViewAssetInsuranceDetail" runat="server"></uc1:ucviewassetinsurancedetail>
</div>
<div class="form_box_uc">
    <uc1:ucviewassetinsurancedetailgridtenor id="oViewAssetInsuranceDetailGridtenor"
        runat="server"> </uc1:ucviewassetinsurancedetailgridtenor>
</div>
