﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UcCreditScoring.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.UcCreditScoring" %>
<div class="form_title">
    <div class="form_single">
        <h3>
            VIEW - SCORING
        </h3>
    </div>
</div>
<div class="form_box">
    <div class="form_single">
        <label>
            No Aplikasi</label>
        <asp:Literal ID="ltlProspectAppId" runat="server"></asp:Literal>
    </div>
</div>
<div class="form_box">
    <div class="form_single">
        <label>
            Nama Customer</label>
        <asp:Literal ID="ltlCustomerName" runat="server"></asp:Literal>
    </div>
</div>
<div class="form_box">
    <div class="form_single">
        <label>
            Scoring Pembiayaan</label>
        <asp:Literal ID="ltlCreditScoring" runat="server"></asp:Literal>
    </div>
</div>
<div class="form_box">
    <div class="form_single">
        <label>
            Hasil</label>
        <asp:Literal ID="ltlResult" runat="server"></asp:Literal>
    </div>
</div>
<div class="form_box_header">
    <div class="form_single">
        <h4>
            HASIL SCORING
        </h4>
    </div>
</div>
<div class="form_box_header">
    <div class="form_single">
        <asp:DataGrid ID="dtgView" runat="server" EnableViewState="False" Width="100%" CssClass="grid_general"
            CellSpacing="1" CellPadding="3" BorderWidth="0px" AutoGenerateColumns="False">
            <ItemStyle CssClass="item_grid" />
            <HeaderStyle CssClass="th" />
            <Columns>
                <asp:TemplateColumn HeaderText="No">
                    <ItemTemplate>
                        <asp:Label ID="lblNumber" runat="server" Text="0"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="KOMPONEN SCORING">
                    <ItemTemplate>
                        <asp:Label ID="lblDescription" runat="server" Text='<%#container.dataitem("Description")%>'>
                        </asp:Label>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="KOMPONEN CONTENT">
                    <ItemTemplate>
                        <asp:Label ID="lblValue" runat="server" Text='<%#container.dataitem("Value")%>'>
                        </asp:Label>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="NILAI">
                    <ItemTemplate>
                        <asp:Label ID="lblScoreValue" runat="server" Text='<%#container.dataitem("ScoreValue")%>'>
                        </asp:Label>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="BOBOT">
                    <ItemTemplate>
                        <asp:Label ID="lblWeight" runat="server" Text='<%#container.dataitem("Weight")%>'>
                        </asp:Label>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="ID" Visible="False">
                    <ItemTemplate>
                        <asp:Label ID="lblID" runat="server" Text='<%#container.dataitem("ID")%>'>
                        </asp:Label>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="SCORING">
                    <ItemTemplate>
                        <asp:Label ID="lblScore" runat="server" Text='<%#container.dataitem("Score")%>'>
                        </asp:Label>
                    </ItemTemplate>
                </asp:TemplateColumn>
            </Columns>
        </asp:DataGrid>
    </div>
</div>
