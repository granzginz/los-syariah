﻿#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports System.Data.SqlClient
#End Region


Public Class UcCreditScoring
    Inherits ControlBased

    Private LStrStatusNote As String
    Private LStrStatus As String

#Region "Properties"
    Private Property myDataTable() As DataTable
        Get
            Return CType(ViewState("myDataTable"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            ViewState("myDataTable") = Value
        End Set
    End Property
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property CSResult_Temp() As String
        Get
            Return CType(ViewState("CSResult_Temp"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CSResult_Temp") = Value
        End Set
    End Property
    Public Property ApplicationID() As String
        Get
            Return CType(ViewState("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
    Private Property CustomerID() As String
        Get
            Return CType(ViewState("CustomerID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property
    Private Property SupplierName() As String
        Get
            Return CType(ViewState("SupplierName"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("SupplierName") = Value
        End Set
    End Property
    Private Property EmployeeName() As String
        Get
            Return CType(ViewState("EmployeeName"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("EmployeeName") = Value
        End Set
    End Property
    Private Property NewApplicationDate() As Date
        Get
            Return CType(ViewState("NewApplicationDate"), Date)
        End Get
        Set(ByVal Value As Date)
            ViewState("NewApplicationDate") = Value
        End Set
    End Property
    Private Property CustomerName() As String
        Get
            Return CType(ViewState("CustomerName"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerName") = Value
        End Set
    End Property
    Private Property CustomerType() As String
        Get
            Return CType(ViewState("CustomerType"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerType") = Value
        End Set
    End Property
    Private Property IsActive() As Boolean
        Get
            Return CType(ViewState("IsActive"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            ViewState("IsActive") = Value
        End Set
    End Property
    Private Property AddEdit() As String
        Get
            Return CType(ViewState("vwsAddEdt"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsAddEdt") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property
    Private Property CreditScoreSchemeID() As String
        Get
            Return CType(ViewState("CreditScoreSchemeID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CreditScoreSchemeID") = Value
        End Set
    End Property
    Private Property CreditScoreComponentID() As String
        Get
            Return CType(ViewState("CreditScoreComponentID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CreditScoreComponentID") = Value
        End Set
    End Property
    Private Property CreditScore() As Decimal
        Get
            Return CType(ViewState("CreditScore"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            ViewState("CreditScore") = Value
        End Set
    End Property
    Private Property CreditScoreResult() As String
        Get
            Return CType(ViewState("CreditScoreResult"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CreditScoreResult") = Value
        End Set
    End Property
    Private Property Name() As String
        Get
            Return CType(ViewState("Name"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Name") = Value
        End Set
    End Property
    Public Property PageSource() As String
        Get
            Return ViewState("PageSource").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("PageSource") = Value
        End Set
    End Property
    Public Property Style() As String
        Get
            Return ViewState("Style").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("Style") = Value
        End Set
    End Property

    Public Property BranchID() As String
        Get
            Return ViewState("BranchID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("BranchID") = Value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
       
    End Sub

    Public Sub BindData()
        ltlProspectAppId.Text = Me.ApplicationID
        ltlCustomerName.Text = Me.Name
        CalculateCreditScoring(Me.BranchID, Me.ApplicationID)
        ltlCreditScoring.Text = Me.CreditScore.ToString
        ltlResult.Text = Me.CreditScoreResult                

        dtgView.DataSource = Me.myDataTable.DefaultView
        dtgView.CurrentPageIndex = 0
        dtgView.DataBind()
    End Sub

    Public Function CalculateCreditScoring(ByVal LStrBranchID As String, ByVal LStrApplicationID As String) As String
        Dim LObjCommand As SqlCommand
        Dim LObjAdpt As New SqlDataAdapter
        Dim LObjDS As New DataSet
        Dim LIntIndex As Integer
        Dim LObjRowCreditScoreSchemeComponent As DataRow
        Dim LObjDataReader As SqlDataReader
        Dim LStrResultQuery As String
        Dim ScoreDesc As String
        Dim LObjRow As DataRow
        Dim ScoreValue As Double = 0
        Dim myFatalScore As Boolean = False
        Dim myWarningScore As Boolean = False
        Dim FatalDesc As String = ""
        Dim WarningDesc As String = ""
        Dim LDblResultCalc As Double = 0
        Dim LDblApproveScore As Double
        Dim LDblRejectScore As Double
        Dim GObjCon As New SqlConnection(GetConnectionString)
        Dim LStrSQL As String
        Dim ReturnResult As String
        Dim myDataColumn As DataColumn
        Dim myDataRow As DataRow

        '----------------------------------
        'cari CustomerID, ProductID, ProductOfferingID dari Agreement
        If GObjCon.State = ConnectionState.Closed Then GObjCon.Open()
        LStrSQL = "spCount_CreditScoring1"
        LObjCommand = New SqlCommand(LStrSQL, GObjCon)
        LObjCommand.CommandType = CommandType.StoredProcedure
        LObjCommand.Parameters.AddWithValue("@BranchID", Me.BranchID)
        LObjCommand.Parameters.AddWithValue("@ApplicationID", Me.ApplicationID)
        LObjAdpt = New SqlDataAdapter(LStrSQL, GObjCon)
        LObjAdpt.SelectCommand = LObjCommand
        LObjAdpt.Fill(LObjDS, "Agreement")
        '----------------------------------

        '----------------------------------
        'cari tipe Customer (Personal/Company)
        LStrSQL = "spCount_CreditScoring2"
        LObjCommand = New SqlCommand(LStrSQL, GObjCon)
        LObjCommand.CommandType = CommandType.StoredProcedure
        LObjCommand.Parameters.AddWithValue("@CustomerID", LObjDS.Tables("Agreement").Rows(0)("CustomerID"))
        LObjAdpt = New SqlDataAdapter(LStrSQL, GObjCon)
        LObjAdpt.SelectCommand = LObjCommand
        LObjAdpt.Fill(LObjDS, "Customer")
        Me.CustomerType = LObjDS.Tables("Customer").Rows(0)("CustomerType").ToString
        '----------------------------------

        '----------------------------------
        'cari CreditScoreSchemeID dari ProductOffering 
        LStrSQL = "spCount_CreditScoring3"
        LObjCommand = New SqlCommand(LStrSQL, GObjCon)
        LObjCommand.CommandType = CommandType.StoredProcedure
        LObjCommand.Parameters.AddWithValue("@BranchID", LStrBranchID)
        LObjCommand.Parameters.AddWithValue("@ProductID", LObjDS.Tables("Agreement").Rows(0)("ProductID"))
        LObjCommand.Parameters.AddWithValue("@ProductOfferingID", LObjDS.Tables("Agreement").Rows(0)("ProductOfferingID"))
        LObjAdpt = New SqlDataAdapter(LStrSQL, GObjCon)
        LObjAdpt.SelectCommand = LObjCommand
        LObjAdpt.Fill(LObjDS, "ProductOffering")
        Me.CreditScoreSchemeID = LObjDS.Tables("ProductOffering").Rows(0)("CreditScoreSchemeID").ToString
        '----------------------------------

        '----------------------------------
        'cari Component Scoring yang sesuai dengan CreditScoreSchemeID        
        LStrSQL = "spCount_CreditScoring4"
        LObjCommand = New SqlCommand(LStrSQL, GObjCon)
        LObjCommand.CommandType = CommandType.StoredProcedure
        LObjCommand.Parameters.AddWithValue("@CreditScoreSchemeID", Me.CreditScoreSchemeID)
        LObjCommand.Parameters.AddWithValue("@ScoringType", Me.CustomerType)
        LObjAdpt = New SqlDataAdapter(LStrSQL, GObjCon)
        LObjAdpt.SelectCommand = LObjCommand
        LObjAdpt.Fill(LObjDS, "CreditScoreSchemeComponent")
        '----------------------------------

        '----------------------------------
        'cari dr tbl CreditScoreComponentContent
        LStrSQL = "spCount_CreditScoring5"
        LObjCommand = New SqlCommand(LStrSQL, GObjCon)
        LObjCommand.CommandType = CommandType.StoredProcedure
        LObjCommand.Parameters.AddWithValue("@CreditScoreSchemeID", Me.CreditScoreSchemeID)
        LObjAdpt = New SqlDataAdapter(LStrSQL, GObjCon)
        LObjAdpt.SelectCommand = LObjCommand
        LObjAdpt.Fill(LObjDS, "CreditScoreComponentContent")
        '----------------------------------

        'buat table
        myDataTable = New DataTable("Result")

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.String")
        myDataColumn.ColumnName = "ID"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.String")
        myDataColumn.ColumnName = "Description"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.String")
        myDataColumn.ColumnName = "Value"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.String")
        myDataColumn.ColumnName = "ScoreValue"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.String")
        myDataColumn.ColumnName = "Weight"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.String")
        myDataColumn.ColumnName = "Score"
        myDataTable.Columns.Add(myDataColumn)

        '----------------------------------
        'looping sebanyak jumlah Component Credit Scoring
        For LIntIndex = 0 To LObjDS.Tables("CreditScoreSchemeComponent").Rows.Count - 1
            LObjRowCreditScoreSchemeComponent = LObjDS.Tables("CreditScoreSchemeComponent").Rows(LIntIndex)
            Me.CreditScoreComponentID = LObjRowCreditScoreSchemeComponent("CreditScoreComponentID").ToString

            LObjCommand = New SqlCommand(LObjRowCreditScoreSchemeComponent("SQLCmd").ToString, GObjCon)

            If GObjCon.State = ConnectionState.Closed Then GObjCon.Open()
            LObjCommand.Parameters.AddWithValue("@BranchID", Me.BranchID)
            LObjCommand.Parameters.AddWithValue("@ApplicationID", Me.ApplicationID)
            LObjDataReader = LObjCommand.ExecuteReader()
            LObjDataReader.Read()
            If Not IsDBNull(LObjDataReader(0)) Then
                LStrResultQuery = Trim(LObjDataReader(0).ToString)
            Else
                LStrResultQuery = ""
            End If
            LObjDataReader.Close()

            'buat row
            myDataRow = myDataTable.NewRow()

            ScoreDesc = LObjRowCreditScoreSchemeComponent("Description").ToString

            If LObjRowCreditScoreSchemeComponent("CalculationType").ToString = "R" Then
                'Range
                Try
                    If Not IsNumeric(LStrResultQuery) Then
                        LStrResultQuery = CDbl(LStrResultQuery).ToString
                    End If
                    LObjRow = LObjDS.Tables("CreditScoreComponentContent").Select("CreditScoreComponentID = '" & Me.CreditScoreComponentID & "' and  ValueFrom <= " & LStrResultQuery & " and ValueTo >= " & LStrResultQuery & "")(0)

                    'LObjRow = LObjDS.Tables("CreditScoreComponentContent").Select("CreditScoreComponentID = '" & Me.CreditScoreComponentID & "' and  ScoreStatus = 'F'")(0)
                    ScoreValue = CDbl(LObjRow("ScoreValue"))
                    If LObjRow("ScoreStatus").ToString.Trim = "F" Then
                        myFatalScore = True
                        FatalDesc &= ScoreDesc & " - "
                    End If
                    'If LObjRow("ScoreStatus").ToString.Trim = "W" Then
                    '    myWarningScore = True
                    '    WarningDesc &= ScoreDesc & " - "
                    'End If
                Catch ex As Exception
                    ScoreValue = 0
                    Exit Try
                End Try
            Else
                'Table
                Try
                    LObjRow = LObjDS.Tables("CreditScoreComponentContent").Select("CreditScoreComponentID = '" & Me.CreditScoreComponentID & "' and valueContent = '" & LStrResultQuery & "'")(0)
                    'LObjRow = LObjDS.Tables("CreditScoreComponentContent").Select("CreditScoreComponentID = '" & Me.CreditScoreComponentID & "' and  ScoreStatus = 'F'")(0)
                    ScoreValue = CDbl(LObjRow("ScoreValue"))
                    If LObjRow("ScoreStatus").ToString.Trim = "F" Then
                        myFatalScore = True
                        FatalDesc &= ScoreDesc & " - "
                    End If
                    'If LObjRow("ScoreStatus").ToString.Trim = "W" Then
                    '    myWarningScore = True
                    '    WarningDesc &= ScoreDesc & " - "
                    'End If
                Catch
                    ScoreValue = 0
                    Exit Try
                End Try
            End If
            LDblResultCalc += ((CDbl(LObjRowCreditScoreSchemeComponent("Weight")) / 100) * ScoreValue)

            'isi row
            myDataRow("ID") = Me.CreditScoreComponentID
            myDataRow("Description") = ScoreDesc
            myDataRow("Value") = LStrResultQuery
            myDataRow("ScoreValue") = ScoreValue
            myDataRow("Weight") = LObjRowCreditScoreSchemeComponent("Weight")
            myDataRow("Score") = CDbl(ScoreValue) * CDbl(LObjRowCreditScoreSchemeComponent("Weight")) / 100

            'tambah row
            myDataTable.Rows.Add(myDataRow)
        Next
        '----------------------------------

        If Me.CustomerType = "P" Then
            LDblApproveScore = CDbl(LObjDS.Tables("CreditScoreSchemeComponent").Rows(0)("PersonalApprovedScore"))
            LDblRejectScore = CDbl(LObjDS.Tables("CreditScoreSchemeComponent").Rows(0)("PersonalRejectScore"))
        Else
            LDblApproveScore = CDbl(LObjDS.Tables("CreditScoreSchemeComponent").Rows(0)("CompanyApprovedScore"))
            LDblRejectScore = CDbl(LObjDS.Tables("CreditScoreSchemeComponent").Rows(0)("CompanyRejectScore"))
        End If

        If myFatalScore = True Then
            LStrStatusNote = "Rejected (Fatal Score - " & FatalDesc & ")"
            LStrStatus = "R"
            LDblResultCalc = 0
            Me.CSResult_Temp = "F"
        ElseIf LDblRejectScore >= LDblResultCalc Then
            LStrStatusNote = "Rejected"
            LStrStatus = "R"
            Me.CSResult_Temp = "R"
        ElseIf LDblApproveScore > LDblResultCalc And LDblRejectScore < LDblResultCalc Then
            LStrStatusNote = "Marginal"
            LStrStatus = "M"
            Me.CSResult_Temp = "M"
        ElseIf LDblApproveScore <= LDblResultCalc Then
            LStrStatusNote = "Approved"
            LStrStatus = "A"
            Me.CSResult_Temp = "A"
        End If

        Me.CreditScore = CDec(LDblResultCalc)
        Me.CreditScoreResult = LStrStatusNote
        Me.myDataTable = myDataTable

        ReturnResult = CStr(LDblResultCalc) + " - " + LStrStatusNote

        If GObjCon.State = ConnectionState.Open Then GObjCon.Close()

        Return ReturnResult
    End Function

    Private Sub dtgView_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgView.ItemDataBound
        Dim lblNumber As New Label

        If e.Item.ItemIndex >= 0 Then
            lblNumber = CType(e.Item.FindControl("lblNumber"), Label)
            lblNumber.Text = (e.Item.ItemIndex + 1).ToString
        End If
    End Sub
End Class