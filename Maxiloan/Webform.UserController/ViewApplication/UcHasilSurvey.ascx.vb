﻿Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Imports System.Data.SqlClient

Public Class UcHasilSurvey
    Inherits ControlBased

    Protected WithEvents ucViewApplication1 As ucViewApplication
    Protected WithEvents ucViewCustomerDetail1 As ucViewCustomerDetail

    Private oAssetDataController As New AssetDataController
    Private oController As New HasilSurveyController
    Private oCustomerController As New CustomerController

#Region "Property"
    Public Property ApplicationID() As String
        Get
            Return (CType(ViewState("ApplicationID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property

    Public Property BranchID() As String
        Get
            Return (CType(ViewState("BranchID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("BranchID") = Value
        End Set
    End Property

    Private Property CustomerID() As String
        Get
            Return (CType(ViewState("CustomerID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property

    Private Property SupplierID() As String
        Get
            Return CType(viewstate("SupplierID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("SupplierID") = Value
        End Set
    End Property

    Private Property CreditScore() As Decimal
        Get
            Return CType(ViewState("CreditScore"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            ViewState("CreditScore") = Value
        End Set
    End Property

    Private Property CreditScoreResult() As String
        Get
            Return CType(ViewState("CreditScoreResult"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CreditScoreResult") = Value
        End Set
    End Property

    Private Property CSResult_Temp() As String
        Get
            Return CType(ViewState("CSResult_Temp"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CSResult_Temp") = Value
        End Set
    End Property


#End Region
    Public Sub BindData()
        InitialPageLoad()    
        getHasilSurvey()
    End Sub
    Private Sub InitialPageLoad()        
        With ucViewApplication1
            .ApplicationID = Me.ApplicationID
            .initControls("OnNewApplication")
            .bindData()
            Me.CustomerID = .CustomerID
            Me.BranchID = .BranchID
            Me.SupplierID = .SupplierID
        End With

        FillCbo("tblBankAccount")

        With ucViewCustomerDetail1
            .CustomerID = Me.CustomerID
            .bindCustomerDetail()
        End With

        Dim WhereBranch As String = "BranchID = '" & Replace(Me.sesBranchId, "'", "") & "'"
        Dim WhereSup As String = "SupplierID = '" & Me.SupplierID & "'"

        FillCboEmp("BranchEmployee", cboCreditAnalyst, WhereBranch + " and (EmployeePosition='CA' or EmployeePosition='AO') ")
        FillCboEmp("BranchEmployee", cboSurveyor, WhereBranch + " and (EmployeePosition='SVY' or EmployeePosition='AO') ")
    End Sub
    Sub getHasilSurvey()
        Dim oPar As New Parameter.HasilSurvey

        With oPar
            .strConnection = GetConnectionString()
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationID
        End With

        oPar = oController.getHasilSurvey(oPar)

        With oPar
            lblTanggalSurvey.Text = .AgreementSurveyDate.ToString("dd/MM/yyyy")
            lblTglKonfirmasiSurvey.Text = IIf(.AgreementSurveyConfirmationDate.ToString("dd/MM/yyyy").Contains("1900"), "", .AgreementSurveyConfirmationDate.ToString("dd/MM/yyyy")).ToString
            If oPar.SurveyorID.Trim <> "" Then
                lblSurveyor.Text = cboSurveyor.Items.FindByValue(oPar.SurveyorID).Text
            Else
                lblSurveyor.Text = ""
            End If
            If oPar.CAID.Trim <> "" Then
                lblCreditAnalyst.Text = cboCreditAnalyst.Items.FindByValue(oPar.CAID).Text
            Else
                lblCreditAnalyst.Text = ""
            End If

            lblSaldoRataRata.Text = FormatNumber(.SaldoRataRata, 0)
            lblSaldoAwal.Text = FormatNumber(.SaldoAwal, 0)
            lblSaldoAkhir.Text = FormatNumber(.SaldoAkhir, 0)
            lblJumlahPemasukan.Text = FormatNumber(.JumlahPemasukan, 0)
            lblJumlahPengeluaran.Text = FormatNumber(.JumlahPengeluaran, 0)
            lblJumlahHariTransaksi.Text = FormatNumber(.JumlahHariTransaksi, 0)
            lblSurveyorNotes.Text = .SurveyorNotes.ToString
            lblLamaSurvey.Text = .LamaSurvey.ToString
            lblJumlahKendaraan.Text = FormatNumber(.JumlahKendaraan, 0)
            lblGarasi.Text = rboGarasi.Items.FindByValue(oPar.Garasi.ToString.Trim).Text
            If oPar.PertamaKredit.ToString.Trim <> "" Then
                lblPertamaKredit.Text = cboPertamaKredit.Items.FindByValue(oPar.PertamaKredit.ToString).Text
            Else
                lblPertamaKredit.Text = cboPertamaKredit.Items.FindByValue("YA").Text
            End If
            lblBankAccType.Text = cboBankAccType.Items.FindByValue(oPar.JenisRekening.ToString).Text
            lblOrderKe.Text = FormatNumber(oPar.OrderKe.ToString, 0)

            If oPar.creditScore <> 0 Then
                SetScoring()
            End If
        End With
    End Sub
    Protected Sub FillCboEmp(ByVal Table As String, ByVal cboName As DropDownList, ByVal Where As String)
        Dim oAssetData As New Parameter.AssetData
        Dim oData As New DataTable

        oAssetData.strConnection = GetConnectionString()
        oAssetData.Where = Where
        oAssetData.Table = Table
        oAssetData = oAssetDataController.GetCboEmp(oAssetData)

        oData = oAssetData.ListData
        cboName.DataSource = oData
        cboName.DataTextField = "Name"
        cboName.DataValueField = "ID"
        cboName.DataBind()
        cboName.Items.Insert(0, "Select One")
        cboName.Items(0).Value = "Select One"
    End Sub
#Region "Scoring"
    Private Sub SetScoring()
        Dim scoringData As New Parameter.CreditScoring_calculate

        With scoringData
            .strConnection = GetConnectionString()
            .BranchId = Me.BranchID
            .ApplicationId = Me.ApplicationID
        End With
        scoringData = CalculateCreditScoring(scoringData)

        'set result        
        lblGrade.Text = scoringData.lblGrade
        lblResult.Text = scoringData.ReturnResult
        Me.CreditScore = scoringData.CreditScore
        Me.CreditScoreResult = scoringData.CreditScoreResult
        Me.CSResult_Temp = scoringData.CSResult_Temp

        If lblResult.Text <> "" Then
            pnlScoring.Visible = True
        End If
    End Sub
#End Region
    Private Function CalculateCreditScoring(ByVal scoringData As CreditScoring_calculate) As CreditScoring_calculate
        Dim LObjCommand As SqlCommand
        Dim LObjAdpt As New SqlDataAdapter
        Dim LObjDS As New DataSet
        Dim LIntIndex As Integer
        Dim LObjRowCreditScoreSchemeComponent As DataRow
        Dim LObjDataReader As SqlDataReader
        Dim LStrResultQuery As String
        Dim ScoreDesc As String
        Dim LObjRow As DataRow
        Dim ScoreValue As Double = 0
        Dim myFatalScore As Boolean = False
        Dim myWarningScore As Boolean = False
        Dim FatalDesc As String = ""
        Dim WarningDesc As String = ""
        Dim LDblResultCalc As Double = 0
        Dim LDblApproveScore As Double
        Dim LDblRejectScore As Double
        Dim GObjCon As New SqlConnection(scoringData.strConnection)
        Dim LStrSQL As String
        Dim ReturnResult As String
        Dim myDataColumn As DataColumn
        Dim myDataRow As DataRow
        Dim LDblGradeTotal As Double = 0

        Dim LStrStatusNote As String
        Dim LStrStatus As String
        Dim mydatatable As DataTable = scoringData.DT

        'cari CustomerID, ProductID, ProductOfferingID dari Agreement
        If GObjCon.State = ConnectionState.Closed Then GObjCon.Open()
        LStrSQL = "spCount_CreditScoring1"
        LObjCommand = New SqlCommand(LStrSQL, GObjCon)
        LObjCommand.CommandType = CommandType.StoredProcedure
        LObjCommand.Parameters.AddWithValue("@BranchID", scoringData.BranchId)
        LObjCommand.Parameters.AddWithValue("@ApplicationID", scoringData.ApplicationId)
        LObjAdpt = New SqlDataAdapter(LStrSQL, GObjCon)
        LObjAdpt.SelectCommand = LObjCommand
        LObjAdpt.Fill(LObjDS, "Agreement")

        'cari tipe Customer (Personal/Company)
        LStrSQL = "spCount_CreditScoring2"
        LObjCommand = New SqlCommand(LStrSQL, GObjCon)
        LObjCommand.CommandType = CommandType.StoredProcedure
        LObjCommand.Parameters.AddWithValue("@CustomerID", LObjDS.Tables("Agreement").Rows(0)("CustomerID"))
        LObjAdpt = New SqlDataAdapter(LStrSQL, GObjCon)
        LObjAdpt.SelectCommand = LObjCommand
        LObjAdpt.Fill(LObjDS, "Customer")
        scoringData.CustomerType = LObjDS.Tables("Customer").Rows(0)("CustomerType").ToString

        'cari CreditScoreSchemeID dari ProductOffering 
        LStrSQL = "spCount_CreditScoring3"
        LObjCommand = New SqlCommand(LStrSQL, GObjCon)
        LObjCommand.CommandType = CommandType.StoredProcedure
        LObjCommand.Parameters.AddWithValue("@BranchID", scoringData.BranchId)
        LObjCommand.Parameters.AddWithValue("@ProductID", LObjDS.Tables("Agreement").Rows(0)("ProductID"))
        LObjCommand.Parameters.AddWithValue("@ProductOfferingID", LObjDS.Tables("Agreement").Rows(0)("ProductOfferingID"))
        LObjAdpt = New SqlDataAdapter(LStrSQL, GObjCon)
        LObjAdpt.SelectCommand = LObjCommand
        LObjAdpt.Fill(LObjDS, "ProductOffering")
        scoringData.CreditScoreSchemeID = LObjDS.Tables("ProductOffering").Rows(0)("CreditScoreSchemeID").ToString

        'cari Component Scoring yang sesuai dengan CreditScoreSchemeID        
        LStrSQL = "spCount_CreditScoring4"
        LObjCommand = New SqlCommand(LStrSQL, GObjCon)
        LObjCommand.CommandType = CommandType.StoredProcedure
        LObjCommand.Parameters.AddWithValue("@CreditScoreSchemeID", scoringData.CreditScoreSchemeID)
        LObjCommand.Parameters.AddWithValue("@ScoringType", scoringData.CustomerType)
        LObjAdpt = New SqlDataAdapter(LStrSQL, GObjCon)
        LObjAdpt.SelectCommand = LObjCommand
        LObjAdpt.Fill(LObjDS, "CreditScoreSchemeComponent")

        'cari dr tbl CreditScoreComponentContent
        LStrSQL = "spCount_CreditScoring5"
        LObjCommand = New SqlCommand(LStrSQL, GObjCon)
        LObjCommand.CommandType = CommandType.StoredProcedure
        LObjCommand.Parameters.AddWithValue("@CreditScoreSchemeID", scoringData.CreditScoreSchemeID)
        LObjAdpt = New SqlDataAdapter(LStrSQL, GObjCon)
        LObjAdpt.SelectCommand = LObjCommand
        LObjAdpt.Fill(LObjDS, "CreditScoreComponentContent")
        '----------------------------------

        mydatatable = New DataTable("Result")

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.String")
        myDataColumn.ColumnName = "ID"
        mydatatable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.String")
        myDataColumn.ColumnName = "Description"
        mydatatable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.String")
        myDataColumn.ColumnName = "Value"
        mydatatable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.String")
        myDataColumn.ColumnName = "ScoreValue"
        mydatatable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.String")
        myDataColumn.ColumnName = "Weight"
        mydatatable.Columns.Add(myDataColumn)

        For LIntIndex = 0 To LObjDS.Tables("CreditScoreSchemeComponent").Rows.Count - 1
            LObjRowCreditScoreSchemeComponent = LObjDS.Tables("CreditScoreSchemeComponent").Rows(LIntIndex)
            scoringData.CreditScoreComponentID = LObjRowCreditScoreSchemeComponent("CreditScoreComponentID").ToString

            LObjCommand = New SqlCommand(LObjRowCreditScoreSchemeComponent("SQLCmd").ToString, GObjCon)

            If GObjCon.State = ConnectionState.Closed Then GObjCon.Open()

            LObjCommand.Parameters.AddWithValue("@BranchID", scoringData.BranchId)
            LObjCommand.Parameters.AddWithValue("@ApplicationID", scoringData.ApplicationId)

            LObjDataReader = LObjCommand.ExecuteReader()
            LObjDataReader.Read()

            If Not IsDBNull(LObjDataReader(0)) Then
                LStrResultQuery = Trim(LObjDataReader(0).ToString)
            Else
                LStrResultQuery = ""
            End If

            LObjDataReader.Close()
            myDataRow = mydatatable.NewRow()
            ScoreDesc = LObjRowCreditScoreSchemeComponent("Description").ToString

            If LObjRowCreditScoreSchemeComponent("CalculationType").ToString = "R" Then
                Try
                    If Not IsNumeric(LStrResultQuery) Then
                        LStrResultQuery = CDbl(LStrResultQuery).ToString
                    End If

                    LObjRow = LObjDS.Tables("CreditScoreComponentContent").Select("CreditScoreComponentID = '" & scoringData.CreditScoreComponentID & "' and  ValueFrom <= " & LStrResultQuery & " and ValueTo >= " & LStrResultQuery & "")(0)
                    ScoreValue = CDbl(LObjRow("ScoreValue"))

                    If LObjRow("ScoreStatus").ToString.Trim = "F" Then
                        myFatalScore = True
                        FatalDesc &= ScoreDesc & " - "
                    End If
                Catch ex As Exception
                    ScoreValue = 0
                    Exit Try
                End Try
            Else
                Try
                    LObjRow = LObjDS.Tables("CreditScoreComponentContent").Select("CreditScoreComponentID = '" & scoringData.CreditScoreComponentID & "' and valueContent = '" & LStrResultQuery & "'")(0)
                    ScoreValue = CDbl(LObjRow("ScoreValue"))
                    If LObjRow("ScoreStatus").ToString.Trim = "F" Then
                        myFatalScore = True
                        FatalDesc &= ScoreDesc & " - "
                    End If
                Catch
                    ScoreValue = 0
                    Exit Try
                End Try
            End If

            LDblResultCalc += ((CDbl(LObjRowCreditScoreSchemeComponent("Weight")) / 100) * ScoreValue)
            LDblGradeTotal += ScoreValue

            myDataRow("ID") = scoringData.CreditScoreComponentID
            myDataRow("Description") = ScoreDesc
            myDataRow("Value") = LStrResultQuery
            myDataRow("ScoreValue") = ScoreValue
            myDataRow("Weight") = LObjRowCreditScoreSchemeComponent("Weight")
            mydatatable.Rows.Add(myDataRow)
        Next

        If scoringData.CustomerType = "P" Then
            LDblApproveScore = CDbl(LObjDS.Tables("CreditScoreSchemeComponent").Rows(0)("PersonalApprovedScore"))
            LDblRejectScore = CDbl(LObjDS.Tables("CreditScoreSchemeComponent").Rows(0)("PersonalRejectScore"))
        Else
            LDblApproveScore = CDbl(LObjDS.Tables("CreditScoreSchemeComponent").Rows(0)("CompanyApprovedScore"))
            LDblRejectScore = CDbl(LObjDS.Tables("CreditScoreSchemeComponent").Rows(0)("CompanyRejectScore"))
        End If

        If myFatalScore = True Then
            LStrStatusNote = "Rejected (Fatal Score - " & FatalDesc & ")"
            LStrStatus = "R"
            LDblResultCalc = 0
            scoringData.CSResult_Temp = "F"
        ElseIf LDblRejectScore >= LDblResultCalc Then
            LStrStatusNote = "Rejected"
            LStrStatus = "R"
            scoringData.CSResult_Temp = "R"
        ElseIf LDblApproveScore > LDblResultCalc And LDblRejectScore < LDblResultCalc Then
            LStrStatusNote = "Marginal"
            LStrStatus = "M"
            scoringData.CSResult_Temp = "M"
        ElseIf LDblApproveScore <= LDblResultCalc Then
            LStrStatusNote = "Approved"
            LStrStatus = "A"
            scoringData.CSResult_Temp = "A"
        End If

        scoringData.DT = mydatatable

        ReturnResult = CStr(LDblResultCalc) + " - " + LStrStatusNote

        scoringData.CreditScore = CDec(LDblGradeTotal)
        scoringData.ReturnResult = ReturnResult

        'LStrSQL = "spCreditScoringGrade"
        'LObjCommand = New SqlCommand(LStrSQL, GObjCon)
        'LObjCommand.CommandType = CommandType.StoredProcedure
        'LObjCommand.Parameters.AddWithValue("@GradeValue", scoringData.CreditScore)
        'LObjCommand.Parameters.AddWithValue("@CreditScoreSchemeID", scoringData.CreditScoreSchemeID)
        'LObjCommand.Parameters.AddWithValue("@CustType", scoringData.CustomerType)
        'LObjCommand.Parameters.Add("@GradeDescription", SqlDbType.VarChar, 50)
        'LObjCommand.Parameters.Add("@GradeID", SqlDbType.VarChar, 50)
        'LObjCommand.Parameters("@GradeDescription").Direction = ParameterDirection.Output
        'LObjCommand.Parameters("@GradeID").Direction = ParameterDirection.Output
        'LObjCommand.ExecuteNonQuery()
        scoringData = SetGradeValue(scoringData)

        If GObjCon.State = ConnectionState.Open Then GObjCon.Close()
        'Return ReturnResult

        Return scoringData
    End Function

    Sub FillCbo(ByVal table As String)
        Dim dtEntity As DataTable = Nothing
        Dim oCustomer As New Parameter.Customer

        oCustomer.strConnection = GetConnectionString
        oCustomer.Table = table
        oCustomer = oCustomerController.CustomerPersonalAdd(oCustomer)

        If Not oCustomer Is Nothing Then
            dtEntity = oCustomer.listdata
        End If
        Select Case table
            Case "tblBankAccount"
                With cboBankAccType
                    .DataSource = dtEntity
                    .DataTextField = "Description"
                    .DataValueField = "ID"
                    .DataBind()
                    .Items.Insert(0, "Select One")
                    .Items(0).Value = ""
                End With
        End Select
    End Sub

    Private Function SetGradeValue(ByVal data As CreditScoring_calculate) As CreditScoring_calculate
        Dim LObjCommand As SqlCommand
        Dim LStrSQL As String
        Dim GObjCon As New SqlConnection(data.strConnection)

        If GObjCon.State = ConnectionState.Closed Then GObjCon.Open()

        LStrSQL = "spCreditScoringGrade"
        LObjCommand = New SqlCommand(LStrSQL, GObjCon)
        LObjCommand.CommandType = CommandType.StoredProcedure
        LObjCommand.Parameters.AddWithValue("@GradeValue", data.CreditScore)
        LObjCommand.Parameters.AddWithValue("@CreditScoreSchemeID", data.CreditScoreSchemeID)
        LObjCommand.Parameters.AddWithValue("@CustType", data.CustomerType)
        LObjCommand.Parameters.Add("@GradeDescription", SqlDbType.VarChar, 50)
        LObjCommand.Parameters.Add("@GradeID", SqlDbType.VarChar, 50)
        LObjCommand.Parameters("@GradeDescription").Direction = ParameterDirection.Output
        LObjCommand.Parameters("@GradeID").Direction = ParameterDirection.Output
        LObjCommand.ExecuteNonQuery()

        Dim strGrade As String = IIf(IsDBNull(LObjCommand.Parameters("@GradeDescription").Value), "Tidak ada grade!", LObjCommand.Parameters("@GradeDescription").Value).ToString
        'lblGrade.Text = " " & strGrade & " (" & LDblGradeTotal & ")"
        data.lblGrade = " " & strGrade & " (" & data.CreditScore & ")"
        data.CreditScoreResult = IIf(IsDBNull(LObjCommand.Parameters("@GradeID").Value), "0", LObjCommand.Parameters("@GradeID").Value).ToString

        If GObjCon.State = ConnectionState.Open Then GObjCon.Close()

        Return data
    End Function
End Class