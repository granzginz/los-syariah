<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UcFindEmployee.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.UcFindEmployee" %>

<script language="javascript" type ="text/javascript" >
    function OpenWinEmployee(pEmployeeID, pEmployeeName, pStyle, pBranchID) {
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/General/LookupEmployee.aspx?style=' + pStyle + '&employeeid=' + pEmployeeID + '&employeename=' + pEmployeeName + '&branchid=' + pBranchID, 'UserLookup', 'left=50, top=10, width=600, height=480, menubar=0, scrollbars=yes');
    }
</script>
<div class="form_box_usercontrol">
    <div class="form_left_usercontrol">
        <asp:Label ID="lblText" runat="server" CssClass="label_general" >Collection Group Head</asp:Label>
        <asp:TextBox ID="txtEmployeeName" runat="server" MaxLength="5" ReadOnly="True" CssClass ="long_text"></asp:TextBox>
        <asp:hyperlink runat="server" ID="hpLookup" CausesValidation="false" Text="..." CssClass="small buttongo blue" />
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Width="152px" runat="server"
            ErrorMessage="Please select Employee" ControlToValidate="txtEmployeeName" CssClass="validator_general"></asp:RequiredFieldValidator>
    </div>
</div>
<input type="hidden" runat="server" name="txtEmployeeID" id="txtEmployeeID" />
<input id="hdnBranchID" type="hidden" name="hdnBranchID" runat="server" />