﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UcIncentiveGridView.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.UcIncentiveGridView" %>
<%@ Register Src="ucNumberFormat.ascx" TagName="ucNumberFormat" TagPrefix="uc1" %>
<asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
    onclick="hideMessage();"></asp:Label>
<script type="text/javascript">

    var handleddlJabatan_Change = function (e, penerimaID) {
        $('#' + penerimaID).empty();
        $('#' + penerimaID).append($("<option></option>").val('0').html('Select One'));

        var paramArg = JSON.stringify({ id: $(e).val() });
        $.ajax({
            url: "IncentiveData.aspx/GetEmployee",
            data: paramArg,
            type: "POST",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            async: false,
            success: function (dt) {
                $.each(dt.d, function (key, value) {
                    $('#' + penerimaID).append($("<option></option>").val(value.SupplierEmployeeID).html(value.SupplierEmployeeName));
                });
            }
        });
    }
    function total(tableID) {

        var spliIDTable = tableID.split("_");
        var tbl = spliIDTable[0] + "_" + spliIDTable[1];



        var grd = document.getElementById(tbl);
        var row = grd.rows.length - 1;

        var totalP = 0;
        var totalI = 0;
        var totalPID;
        var totalIID;
        $('#' + tbl).find('tr').each(function (i, el) {
            if (i > 0) {
                var $tds = $(this).find('td');
                var persentase_ = $tds[3].getElementsByTagName('input')[0];
                var persentase = persentase_.value;
                var insentif_ = $tds[4].getElementsByTagName('input')[0];
                var insentif = insentif_.value;

                if (persentase == "") {
                    persentase = "0";
                }
                if (insentif == "") {
                    insentif = "0";
                }
                if (i < row) {
                    totalP = parseFloat(totalP) + parseFloat(persentase.replace(/\s*,\s*/g, ''));
                    totalI = parseFloat(totalI) + parseFloat(insentif.replace(/\s*,\s*/g, ''));
                }
                if (i == row) {
                    totalPID = persentase_.id;
                    totalIID = insentif_.id;
                }
            }
        });
        $('#' + totalPID).val(number_format(totalP, 2));
        $('#' + totalIID).val(number_format(totalI));

//        if (spliIDTable[0] == "UcRefundPremi") {
//            var lblAlokasiPremiAsuransi = $('#lblRefundPremi').html();
//            var Nil_internal = parseFloat(lblAlokasiPremiAsuransi.replace(/\s*,\s*/g, '')) - parseFloat(totalI)
//            $('#lblAlokasiPremiAsuransiInsentif').html(number_format(totalI));
//            $('#lblAlokasiPremiAsuransiInternal').html(number_format(Nil_internal));
//        }

        if (totalP > 100) {
            $('#' + totalPID).attr('style', 'color:red');
        } else {
            $('#' + totalPID).removeAttr('style');
        }
        var lblalokasi = getlblAlokasi(tbl);
        if (totalI > lblalokasi) {
            $('#' + totalIID).attr('style', 'color:red');
        } else {
            $('#' + totalIID).removeAttr('style');
        }
    }
    function getlblAlokasi(grd) {
        var tbl = {};
        tbl["UcPremiAsuransi_dtgIncentive"] = "lblAlokasiPremiAsuransiInsentif";
        tbl["UcProgresifPremiAsuransi_dtgIncentive"] = "lblProgresifPremiAsuransi";
        tbl["UcRefundBunga_dtgIncentive"] = "lblRefundBunga";
        tbl["UcPremiProvisi_dtgIncentive"] = "lblPremiProvisi";
        tbl["UcBiayaLainnya_dtgIncentive"] = "lblBiayaLainnya";
        var lbl = $('#' + tbl[grd]).html();
        return parseInt(lbl.replace(/\s*,\s*/g, ''));

    }
    function GenerateTotal() {
        $('#dtgSum').find("tr:gt(0)").remove();

        var dtTable = ["UcRefundPremi_dtgIncentive", "UcRefundBunga_dtgIncentive", "UcPremiProvisi_dtgIncentive", "UcBiayaLainnya_dtgIncentive"];

        for (i = 0; i <= dtTable.length - 1; i++) {
            if (getlblAlokasi(dtTable[i]) != 0) {
                var grd = document.getElementById(dtTable[i]);
                if (grd != null) {
                    var row = grd.rows.length - 1;

                    $('#' + dtTable[i]).find('tr').each(function (i, el) {
                        if (i > 0 && i < row) {
                            var $tds = $(this).find('td');

                            var jabatan_ = $tds[1].getElementsByTagName('select')[0];
                            var jabatan = jabatan_.options[jabatan_.selectedIndex].innerHTML

                            var employee_ = $tds[2].getElementsByTagName('select')[0];
                            var employee = employee_.options[employee_.selectedIndex].innerHTML;

                            var insentif_ = $tds[4].getElementsByTagName('input')[0];
                            var insentif = insentif_.value;

                            if (jabatan != "Select One" && employee != "Select One" && (insentif != "0" && insentif != "")) {
                                CreateGridDTSUM(jabatan, employee, insentif);
                            }
                        }
                    });

                }
            }
        }
    }
    function CreateGridDTSUM(jabatan, employee, insentif) {
        var grd = document.getElementById("dtgSum");
        var row = grd.rows.length - 1;
        var html = "";

        if (row == 0) {
            html += "<tr class=\"item_grid\">";
            html += "	 <td>" + jabatan + "</td><td>" + employee + "</td><td class=\"th_right\">" + insentif + "</td>";
            html += "</tr>";
            $('#dtgSum tbody').append(html);

        } else {
            var exist = false;
            $('#dtgSum').find('tr').each(function (i, el) {
                if (i > 0) {
                    var $tds = $(this).find('td');
                    var jabatan_ = $tds[0].innerHTML;
                    var employee_ = $tds[1].innerHTML;
                    var insentif_ = $tds[2].innerHTML;
                    if ((jabatan == jabatan_) && (employee == employee_)) {
                        var totalInsentif = parseFloat(insentif.replace(/\s*,\s*/g, '')) + parseFloat(insentif_.replace(/\s*,\s*/g, ''));
                        $tds[2].innerHTML = number_format(totalInsentif);
                        exist = true;
                    }
                }
            });
            if (exist == false) {
                html += "<tr class=\"item_grid\">";
                html += "	 <td>" + jabatan + "</td><td>" + employee + "</td><td class=\"th_right\">" + insentif + "</td>";
                html += "</tr>";
                $('#dtgSum tbody').append(html);
            }
        }


    }
    var handletxtProsentase_Change = function (x, n, NilID) {
        var nilai = parseFloat(n.replace(/\s*,\s*/g, '')) * (x / 100);
        $('#' + NilID).val(number_format(parseFloat((nilai * 10) / 10)));

        var lblInsentifGrossID = NilID.replace("txtInsentif", "lblInsentifGross");
        $('#' + lblInsentifGrossID).html($('#' + NilID).val());

        var TarifPajakID = NilID.replace("txtInsentif", "lblTarifPajak");
        var lblNilaiPajakID = NilID.replace("txtInsentif", "lblNilaiPajak");
        var lblInsentifNetID = NilID.replace("txtInsentif", "lblInsentifNet");

        if (!$('#' + TarifPajakID).html() == "") {
            var lblInsentifGross = $('#' + lblInsentifGrossID).html();
            var lblTarifPajak = $('#' + TarifPajakID).html();
            $('#' + lblNilaiPajakID).html(number_format(parseInt(lblInsentifGross.replace(/\s*,\s*/g, '')) * (parseFloat(lblTarifPajak.replace(/\s*,\s*/g, '')) / 100)));
            var lblNilaiPajak = $('#' + lblNilaiPajakID).html();
            $('#' + lblInsentifNetID).html(number_format(parseInt(lblInsentifGross.replace(/\s*,\s*/g, '')) - parseInt(lblNilaiPajak.replace(/\s*,\s*/g, ''))));
        }

        GenerateTotal();
        total(NilID);
    }
    var handletxtInsentif_Change = function (x, n, PersenID) {
        var nilai = x / (parseFloat(n.replace(/\s*,\s*/g, '')) / 100);
        $('#' + PersenID).val(number_format(nilai, 2));

        var lblInsentifGrossID = PersenID.replace("txtProsentase", "lblInsentifGross");
        $('#' + lblInsentifGrossID).html(number_format(x));

        var TarifPajakID = PersenID.replace("txtProsentase", "lblTarifPajak");
        var lblNilaiPajakID = PersenID.replace("txtProsentase", "lblNilaiPajak");
        var lblInsentifNetID = PersenID.replace("txtProsentase", "lblInsentifNet");

        if (!$('#' + TarifPajakID).html() == "") {
            var lblInsentifGross = $('#' + lblInsentifGrossID).html();
            var lblTarifPajak = $('#' + TarifPajakID).html();
            $('#' + lblNilaiPajakID).html(number_format(parseInt(lblInsentifGross.replace(/\s*,\s*/g, '')) * (parseFloat(lblTarifPajak.replace(/\s*,\s*/g, '')) / 100)));
            var lblNilaiPajak = $('#' + lblNilaiPajakID).html();
            $('#' + lblInsentifNetID).html(number_format(parseInt(lblInsentifGross.replace(/\s*,\s*/g, '')) - parseInt(lblNilaiPajak.replace(/\s*,\s*/g, ''))));
        }
        GenerateTotal();
        total(PersenID);
    }
    var handleddlpenerima_Change = function (x, y, TarifPajakID) {
        var paramArg = JSON.stringify({ supplierEmployeeID: x, supplierEmployeePosition: $('#' + y).val() });
        $.ajax({
            url: "IncentiveData.aspx/GetTarifPajak",
            data: paramArg,
            type: "POST",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            async: false,
            success: function (dt) {
                $('#' + TarifPajakID).html(dt.d);
            }
        });

        var lblInsentifGrossID = TarifPajakID.replace("lblTarifPajak", "lblInsentifGross");
        var lblNilaiPajakID = TarifPajakID.replace("lblTarifPajak", "lblNilaiPajak");
        var lblInsentifNetID = TarifPajakID.replace("lblTarifPajak", "lblInsentifNet");

        if (!$('#' + lblInsentifGrossID).html() == "") {
            var lblInsentifGross = $('#' + lblInsentifGrossID).html();
            var lblTarifPajak = $('#' + TarifPajakID).html();
            $('#' + lblNilaiPajakID).html(number_format(parseInt(lblInsentifGross.replace(/\s*,\s*/g, '')) * parseFloat(lblTarifPajak.replace(/\s*,\s*/g, '')) / 100));
            var lblNilaiPajak = $('#' + lblNilaiPajakID).html();
            $('#' + lblInsentifNetID).html(number_format(parseInt(lblInsentifGross.replace(/\s*,\s*/g, '')) - parseInt(lblNilaiPajak.replace(/\s*,\s*/g, ''))));
        }
        GenerateTotal();
        total(TarifPajakID);
    }
    function getPPH(id, PphID, TarifPajakID) {
        if (id == "CO") {
            $('#' + PphID).html('23');
        } else {
            $('#' + PphID).html('21');
        }

        $('#' + TarifPajakID).html('3');

    }
    function number_format(number, decimals, dec_point, thousands_sep) {

        number = (number + '')
            .replace(/[^0-9+\-Ee.]/g, '');
        var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function (n, prec) {
                var k = Math.pow(10, prec);
                return '' + (Math.round(n * k) / k)
                .toFixed(prec);
            };
        // Fix for IE parseFloat(0.55).toFixed(0) = 0;
        s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
            .split('.');
        if (s[0].length > 3) {
            s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
        }
        if ((s[1] || '')
            .length < prec) {
            s[1] = s[1] || '';
            s[1] += new Array(prec - s[1].length + 1)
              .join('0');
        }
        return s.join(dec);
    }
    function deleteRow(ClientID, row) {
        var i = row.parentNode.parentNode.rowIndex;
        document.getElementById(ClientID).deleteRow(i);
        GenerateTotal();
        total(ClientID);
    }
    function Apply(tbl) {
        var a = tbl.split("_");
        var uc = a[0];
        $('#' + uc + '_pesan').html("Mohon tunggu, Sedang dalam proses!");

        var stat = true;
        var grd = document.getElementById(tbl);
        var row = grd.rows.length - 1;
        TrnID = getTransID(tbl); // -- ga kepake
        $('#' + tbl).find('tr').each(function (i, el) {
            if (i > 0 && i < row) {
                var $tds = $(this).find('td');

                var jabatan = $tds[1].getElementsByTagName('select')[0];
                var employee = $tds[2].getElementsByTagName('select')[0];
                var persen = $tds[3].getElementsByTagName('input')[0];
                var insentif = $tds[4].getElementsByTagName('input')[0];
                var pph = $tds[6].getElementsByTagName('span')[0];
                var tarifpajak = $tds[7].getElementsByTagName('span')[0];
                var nilaipajak = $tds[8].getElementsByTagName('span')[0];
                var insentifnet = $tds[9].getElementsByTagName('span')[0];
                var TransID = $tds[10].getElementsByTagName('select')[0];

                var idJabatan = jabatan.id;
                var idemployee = employee.id;
                var idpersen = persen.id;
                var idinsentif = insentif.id;

                $('#' + idJabatan).attr('style', 'background-color:white');
                $('#' + idemployee).attr('style', 'background-color:white;width:150px');
                $('#' + idpersen).attr('style', 'background-color:white');
                $('#' + idinsentif).attr('style', 'background-color:white');


                //                if (jabatan.value == "" || jabatan.value == 0) {
                //                    stat = false;
                //                    $('#' + idJabatan).attr('style', 'background-color:red');
                //                }
                //                if (employee.value == "" || employee.value == 0) {
                //                    stat = false;
                //                    $('#' + idemployee).attr('style', 'background-color:red;width:150px');
                //                }
                //                if (persen.value == "" || persen.value == 0) {
                //                    stat = false;
                //                    $('#' + idpersen).attr('style', 'background-color:red');
                //                }
                //                if (insentif.value == "" || persen.value == 0) {
                //                    stat = false;
                //                    $('#' + idinsentif).attr('style', 'background-color:red');
                //                }


                if (insentif.value != "" || insentif.value != "0" || insentif.value != "0.00" || insentif.value > 0) {
                    if ((jabatan.value == "" || jabatan.value == 0)) {
                        stat = false;
                        $('#' + idJabatan).attr('style', 'background-color:red');
                    }

                    //                    if (employee.value == "" || employee.value == 0 ) {
                    //                        stat = false;
                    //                        $('#' + idemployee).attr('style', 'background-color:red;width:150px');
                    //                    }
                    if (stat == true) {
                        var paramArg = JSON.stringify({
                            jabatan: jabatan.value,
                            employee: employee.value,
                            persen: persen.value,
                            insentif: insentif.value,
                            pph: pph.innerHTML,
                            tarifpajak: tarifpajak.innerHTML,
                            nilaipajak: nilaipajak.innerHTML,
                            insentifnet: insentifnet.innerHTML,
                            TransID: TransID.value,
                            seq: i
                        });
                        $.ajax({
                            url: "IncentiveData.aspx/Apply",
                            data: paramArg,
                            type: "POST",
                            dataType: "json",
                            contentType: "application/json; charset=utf-8",
                            async: false,
                            success: function (dt) {

                            }
                        });
                    }
                }


            } else if (i == row) {
                var lblTotal = getlblAlokasi(tbl);
                var txtTotal;
                var $tds = $(this).find('td');
                txtTotal = $tds[4].getElementsByTagName('input')[0];
                var valTotal = txtTotal.value;
                var idTotal = txtTotal.id;

                if (lblTotal != valTotal.replace(/\s*,\s*/g, '')) {
                    $('#' + idTotal).attr('style', 'color:red');
                    stat = false;
                } else {
                    $('#' + idTotal).removeAttr('style');
                }

            }
        });
        if (stat == true) {
            var paramArg2 = JSON.stringify({ GrouptransID: TrnID });
            $.ajax({
                url: "IncentiveData.aspx/SaveApply",
                data: paramArg2,
                type: "POST",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                async: false,
                success: function (response) {
                    if (response.d == "D") {
                        $('#ucApplicationTab1_hypHasilSurvey').attr('href', 'HasilSurvey.aspx?CustomerID=<%= SupplierID %>&ApplicationID=<%= ApplicationID %>');
                    } else {
                        $('#ucApplicationTab1_hypHasilSurvey').removeAttr('href');
                    }
                    $('#' + uc + '_pesan').html('Proses Berhasil, Terima kasih!');
                    $('#' + uc + '_pesan').attr('style', 'padding-top:10px;font-size:10px;width:80%');
                },
                error: function (xhr, status, error) {
                    var err = eval("(" + xhr.responseText + ")");
                    $('#' + uc + '_pesan').html('Proses Error : ' + err.Message);
                    $('#' + uc + '_pesan').attr('style', 'padding-top:10px;font-size:10px;width:80%;color:red');
                }
            });

        } else {
            $('#' + uc + '_pesan').html('Proses Gagal, Periksa inputannya kembali!');
            $('#' + uc + '_pesan').attr('style', 'padding-top:10px;font-size:10px;width:80%;color:red');
        }
        return false;
    }
    function getTransID(grd) {   // -- ga kepake

        var tbl_ = {};
        tbl_["UcPremiAsuransi_dtgIncentive"] = "TDI";
        tbl_["UcProgresifPremiAsuransi_dtgIncentive"] = "PTP";
        tbl_["UcRefundBunga_dtgIncentive"] = "NPV";
        tbl_["UcPremiProvisi_dtgIncentive"] = "PRS";
        tbl_["UcBiayaLainnya_dtgIncentive"] = "TDS";
        return tbl_[grd]
    }
</script>
<div class="form_box_header">
    <div class="form_single">
        <asp:DataGrid ID="dtgIncentive" runat="server" Width="100%" CellSpacing="1" CellPadding="3"
            BorderWidth="0px" AutoGenerateColumns="False" AllowSorting="True" CssClass="grid_general"
            ShowFooter="true">
            <HeaderStyle CssClass="th" />
            <ItemStyle CssClass="item_grid" />
            <Columns>
                <asp:TemplateColumn HeaderText="NO">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblNomor"></asp:Label>
                    </ItemTemplate>
                    <FooterStyle BorderStyle="None" HorizontalAlign="Right" />
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="JABATAN" ItemStyle-CssClass="item_grid_right" HeaderStyle-CssClass="th_right">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblddlJabatan"></asp:Label>
                    </ItemTemplate>
                    <FooterStyle BorderStyle="None" HorizontalAlign="Right" />
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="PENERIMA" ItemStyle-CssClass="item_grid_right" HeaderStyle-CssClass="th_right">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblddlPenerima" Style="width: 150px"></asp:Label>
                    </ItemTemplate>
                    <FooterStyle BorderStyle="None" HorizontalAlign="Right" />
                    <FooterTemplate>
                        <asp:Label runat="server" ID="lblTotal" Text="TOTAL"></asp:Label>
                    </FooterTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="PROSENTASE" ItemStyle-CssClass="item_grid_right"
                    HeaderStyle-CssClass="th_right">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblProsentase">0</asp:Label>
                    </ItemTemplate>
                    <FooterStyle BorderStyle="None" HorizontalAlign="Right" />
                    <FooterTemplate>
                        <asp:Label runat="server" ID="lblTotalProsentase" Enabled="false">0</asp:Label>
                    </FooterTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="INSENTIF" ItemStyle-CssClass="item_grid_right" HeaderStyle-CssClass="th_right">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblInsentif">0</asp:Label>
                    </ItemTemplate>
                    <FooterStyle BorderStyle="None" HorizontalAlign="Right" />
                    <FooterTemplate>
                        <asp:Label runat="server" ID="lblTotalInsentif" Enabled="false">0</asp:Label>
                    </FooterTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="INSENTIF GROSS" ItemStyle-CssClass="item_grid_right"
                    HeaderStyle-CssClass="th_right">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblInsentifGross">test</asp:Label>
                    </ItemTemplate>
                    <FooterStyle BorderStyle="None" HorizontalAlign="Right" />
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="PPH" ItemStyle-CssClass="item_grid_right" HeaderStyle-CssClass="th_right">
                    <ItemTemplate>
                        <%--<asp:Label runat="server" ID="lblPPH"></asp:Label>--%>
                        <asp:Label runat="server" ID="lblddlPPH"></asp:Label>
                    </ItemTemplate>
                    <FooterStyle BorderStyle="None" HorizontalAlign="Right" />
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="TARIF PAJAK (%)" ItemStyle-CssClass="item_grid_right"
                    HeaderStyle-CssClass="th_right">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblTarifPajak"></asp:Label>
                    </ItemTemplate>
                    <FooterStyle BorderStyle="None" HorizontalAlign="Right" />
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="NILAI PAJAK" ItemStyle-CssClass="item_grid_right"
                    HeaderStyle-CssClass="th_right">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblNilaiPajak"></asp:Label>
                    </ItemTemplate>
                    <FooterStyle BorderStyle="None" HorizontalAlign="Right" />
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="INSENTIF NET" ItemStyle-CssClass="item_grid_right"
                    HeaderStyle-CssClass="th_right">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblInsentifNet"></asp:Label>
                    </ItemTemplate>
                    <FooterStyle BorderStyle="None" HorizontalAlign="Right" />
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="TRANSID" ItemStyle-CssClass="item_grid_right" HeaderStyle-CssClass="th_right">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblddlTransID"></asp:Label>
                    </ItemTemplate>
                    <FooterStyle BorderStyle="None" HorizontalAlign="Right" />
                </asp:TemplateColumn>

                 <asp:TemplateColumn HeaderText="Potong Pajak" ItemStyle-CssClass="item_grid_right" HeaderStyle-CssClass="th_right"> 
                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblIsDiscountPPH"></asp:Label> 
                        </ItemTemplate>
                      <FooterStyle BorderStyle="None" HorizontalAlign="Right" />
                     </asp:TemplateColumn>

                <asp:TemplateColumn HeaderText="DELETE" Visible="false">
                    <ItemStyle CssClass="command_col"></ItemStyle>
                    <ItemTemplate>
                        <asp:ImageButton ID="imgDelete" runat="server" CausesValidation="False" ImageUrl="../Images/icondelete.gif">
                        </asp:ImageButton>
                        <asp:Label runat="server" ID="lblSupplierEmployeeID" Visible="false"></asp:Label>
                        <asp:Label runat="server" ID="lblSupplierEmployeePosition" Visible="false"></asp:Label>
                        <asp:Label runat="server" ID="lblEmployeeName" Visible="false"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateColumn>
            </Columns>
        </asp:DataGrid>
    </div>
</div>
<div style="display: none;">
    <asp:Button ID="btnAdd" runat="server" Text="Add" CausesValidation="False" CssClass="small button blue" />
    <asp:Button ID="btnSave" runat="server" Text="Save" CausesValidation="False" CssClass="small button blue" />
    <label id="pesan" runat="server" style="padding-top: 10px; font-size: 10px; width: 80%">
    </label>
</div>
