﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucYear.ascx.vb" Inherits="Maxiloan.Webform.UserController.ucYear" %>
<asp:TextBox runat="server" ID="txtYear" MaxLength="4" CssClass="small_text" ></asp:TextBox>
<asp:RequiredFieldValidator runat="server" ID="reqYear" ErrorMessage="Isi Tahun!"
    Display="Dynamic" ControlToValidate="txtYear" CssClass="validator_general"  >
</asp:RequiredFieldValidator>
<asp:RegularExpressionValidator runat="server" ID="regexYear" ErrorMessage="Input Tahun Salah!"
    Display="Dynamic" ControlToValidate="txtYear" CssClass="validator_general" ValidationExpression="[0-9]{4}">
</asp:RegularExpressionValidator>
