﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
#End Region

Public Class UcAgreeExposure
    Inherits AccMntControlBased

#Region "Property"

    Public Property Style() As Double
        Get
            Return CType(Viewstate("Style"), Double)
        End Get
        Set(ByVal Value As Double)
            viewstate("Style") = Value
        End Set
    End Property

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Public Sub AgreementExposure()
        Me.ApplicationID = Request("ApplicationID").ToString
        Dim oController As New UCExposureController
        Dim oCustomClass As New Parameter.AccMntBase
        With oCustomClass
            .strConnection = GetConnectionString
            .ApplicationID = Me.ApplicationID
        End With
        oCustomClass = oController.GetAgreeExposure(oCustomClass)
        With oCustomClass
            lblMaxOSBalance.Text = FormatNumber(.MaxOutstandingBalance, 2)
            lblOSBalance.Text = FormatNumber(.OutstandingBalanceAR, 2)
            lblOSPrincipal.Text = FormatNumber(.OutstandingPrincipal, 2)
            lblOSInterest.Text = FormatNumber(.OutStandingInterest, 2)
            lblAssetRepossed.Text = CStr(.NumOfAssetRepossed)
            lblAssetInventoried.Text = CStr(.NumOfAssetInventoried)
            lblRescheduling.Text = CStr(.NumOfRescheduling)
            lblAgreementTransfer.Text = CStr(.NumOfAgreementTransfer)
            lblAssetReplacement.Text = CStr(.NumOfAssetReplacement)
            lblBounceCheque.Text = CStr(.NumOfBounceCheque)
            lblBucket1text.Text = .Bucket1Principle
            lblBucket2text.Text = .Bucket2Principle
            lblBucket3text.Text = .Bucket3Principle
            lblBucket4text.Text = .Bucket4Principle
            lblBucket5text.Text = .Bucket5Principle
            lblBucket6Text.Text = .Bucket6Principle
            lblBucket7text.Text = .Bucket7Principle
            lblBucket8text.Text = .Bucket8Principle
            lblBucket9text.Text = .Bucket9Principle
            lblBucket10text.Text = .Bucket10Principle

            lblBucket1.Text = FormatNumber(.Bucket1_gross, 2)
            lblBucket2.Text = FormatNumber(.Bucket2_gross, 2)
            lblBucket3.Text = FormatNumber(.Bucket3_gross, 2)
            lblBucket4.Text = FormatNumber(.Bucket4_gross, 2)
            lblBucket5.Text = FormatNumber(.Bucket5_gross, 2)
            lblBucket6.Text = FormatNumber(.Bucket6_gross, 2)
            lblBucket7.Text = FormatNumber(.Bucket7_gross, 2)
            lblBucket8.Text = FormatNumber(.Bucket8_gross, 2)
            lblBucket9.Text = FormatNumber(.Bucket9_gross, 2)
            lblBucket10.Text = FormatNumber(.Bucket10_gross, 2)
            lblTotalAllBucket.Text = FormatNumber(.TotalAllBucket, 2)
        End With
        'Dim DtUserList As New DataTable
        'Dim DvUserList As New DataView
        'oCustomClass = oController.GetListAgree(oCustomClass)

        'DtUserList = oCustomClass.listdata
        'DvUserList = DtUserList.DefaultView
        'dtg.DataSource = DvUserList
        'dtg.DataBind()

    End Sub

End Class