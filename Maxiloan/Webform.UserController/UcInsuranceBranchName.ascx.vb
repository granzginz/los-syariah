﻿#Region "Import"
Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.Controller
Imports Maxiloan.Parameter

#End Region
Public Class UcInsuranceBranchName
    Inherits ControlBased

#Region " Private Const "
    Private m_controller As New DataUserControlController
    Private m_controller_ins As New InsCoSelectionController
#End Region

#Region "Properties"
    'Public Property BranchID() As String
    '    Get
    '        Return CType(ViewState("BranchID"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        ViewState("BranchID") = Value
    '    End Set
    'End Property

    'Public Property BranchName() As String
    '    Get
    '        Return CType(ViewState("BranchName"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        ViewState("BranchName") = Value
    '    End Set
    'End Property

    Public Property SelectedValue() As String
        Get
            Return DDLInsuranceBranch.SelectedValue.Trim
        End Get
        Set(ByVal SelectedValue As String)
            DDLInsuranceBranch.SelectedIndex = DDLInsuranceBranch.Items.IndexOf(DDLInsuranceBranch.Items.FindByValue(SelectedValue))
        End Set
    End Property

    Public ReadOnly Property SelectedText() As String
        Get
            Return DDLInsuranceBranch.SelectedItem.Text
        End Get
    End Property

    Public Property FillRequired() As Boolean
        Get
            Return ReqValField.Enabled
        End Get
        Set(ByVal FillRequired As Boolean)
            ReqValField.Enabled = FillRequired
        End Set
    End Property

    Public Property Enabled() As Boolean
        Get
            Return DDLInsuranceBranch.Enabled
        End Get
        Set(ByVal Enabled As Boolean)
            DDLInsuranceBranch.Enabled = Enabled
        End Set
    End Property





#End Region


    'Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
    '    If Not IsPostBack Then
    '        'Dim DtInsuranceBranchName As New DataTable

    '        'DtInsuranceBranchName = CType(Me.Cache.Item(CACHE_INSURANCE_BRANCH & Me.sesBranchId), DataTable)

    '        'If DtInsuranceBranchName Is Nothing Then
    '        '    Dim DTCache As New DataTable
    '        '    DTCache = m_controller.GetInsuranceBranchName(GetConnectionString, Me.sesBranchId)
    '        '    Me.Cache.Insert(CACHE_INSURANCE_BRANCH & Me.sesBranchId, DTCache, Nothing, DateTime.Now.AddHours(DEFAULT_TIME_DURATION_CACHE_HOUR), TimeSpan.Zero)
    '        '    DtInsuranceBranchName = CType(Me.Cache.Item(CACHE_INSURANCE_BRANCH & Me.sesBranchId), DataTable)
    '        'End If

    '        DDLInsuranceBranch.DataValueField = "ID"
    '        DDLInsuranceBranch.DataTextField = "Name"
    '        DDLInsuranceBranch.DataSource = m_controller.GetInsuranceBranchName(GetConnectionString, Me.sesBranchId, Me.Loginid)
    '        DDLInsuranceBranch.DataBind()
    '        DDLInsuranceBranch.Items.Insert(0, "Select One")


    '        If ReqValField.Enabled = True Then
    '            DDLInsuranceBranch.Items(0).Value = ""
    '        Else
    '            DDLInsuranceBranch.Items(0).Value = "0"
    '        End If
    '    End If

    '    'Me.BranchID = DDLInsuranceBranch.SelectedItem.Value.Trim
    '    'Me.BranchName = DDLInsuranceBranch.SelectedItem.Text.Trim
    'End Sub

    Private Sub btnClearCache_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearCache.Click
        Me.Cache.Remove(CACHE_INSURANCE_BRANCH & Me.sesBranchId)
    End Sub
    Public Sub RemoveById(value As String)
        Dim item = DDLInsuranceBranch.Items.FindByValue(value)
        If Not (item Is Nothing) Then
            DDLInsuranceBranch.Items.Remove(item)
        End If 
    End Sub
    Public Sub loadInsBranchbyProduct(ByVal productID As String)

        With DDLInsuranceBranch
            .DataValueField = "ID"
            .DataTextField = "Name"

            Dim ins As InsCoBranch = New InsCoBranch()
            ins.LoginId = Me.Loginid
            ins.BranchId = Me.sesBranchId.Replace("'", "")
            ins.strConnection = GetConnectionString()
            ins.InsuranceProductID = productID

            .DataSource = m_controller_ins.GetInsuranceComByProduct(ins).ListData

            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = IIf(ReqValField.Enabled = True, "", "0").ToString
        End With

    End Sub
    Public Sub loadInsBranch(ByVal byLogin As Boolean)

        With DDLInsuranceBranch
            .DataValueField = "ID"
            .DataTextField = "Name"

            If byLogin Then
                .DataSource = m_controller.GetInsuranceBranchName(GetConnectionString, Me.sesBranchId, Me.Loginid)
            Else
                .DataSource = m_controller.GetInsuranceBranchName(GetConnectionString, Me.sesBranchId)
            End If

            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = IIf(ReqValField.Enabled = True, "", "0").ToString
        End With

    End Sub
End Class