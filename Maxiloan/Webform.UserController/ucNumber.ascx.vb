﻿Public Class ucNumber
    Inherits System.Web.UI.UserControl

#Region "Properties"
    Public Property MinimumRange() As Decimal
        Get
            Return (CType(viewstate("minimumrange"), Decimal))
        End Get
        Set(ByVal Value As Decimal)
            Viewstate("minimumrange") = Value
        End Set
    End Property
    Public Property ErrorRange() As String
        Get
            Return (CType(viewstate("errorrange"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("errorrange") = Value
        End Set
    End Property
    Public Property RegularValidation() As String
        Get
            Return (CType(viewstate("regularvalidation"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("regularvalidation") = Value
        End Set
    End Property
    Public Property RegularValidationMessage() As String
        Get
            Return (CType(viewstate("regularvalidationmessage"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("regularvalidationmessage") = Value
        End Set
    End Property
    Public Property TypeRangeValidator() As System.Web.UI.WebControls.ValidationDataType
        Get
            Return (CType(viewstate("typerangevalidator"), System.Web.UI.WebControls.ValidationDataType))
        End Get
        Set(ByVal Value As System.Web.UI.WebControls.ValidationDataType)
            viewstate("typerangevalidator") = Value
        End Set
    End Property
    Public Property MaximumRange() As Decimal
        Get
            Return (CType(viewstate("maximumrange"), Decimal))
        End Get
        Set(ByVal Value As Decimal)
            viewstate("maximumrange") = Value
        End Set
    End Property
    Public Property Text() As Double
        Get
            Return CType(viewstate("amount"), Double)
        End Get
        Set(ByVal Value As Double)
            viewstate("amount") = Value
        End Set
    End Property
    Public Property MaxLength() As Integer
        Get
            Return CType(viewstate("MaxLength"), Integer)
        End Get
        Set(ByVal Value As Integer)
            viewstate("MaxLength") = Value
        End Set
    End Property
    Public Property EnabledRangeValidator() As Boolean
        Get
            Return CType(viewstate("EnabledRangeValidator"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            viewstate("EnabledRangeValidator") = Value
        End Set
    End Property
    Public Property DisableReadOnly() As String
        Get
            Return CType(viewstate("DisableReadOnly"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("DisableReadOnly") = Value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load        
        Me.Text = CDbl(IIf(txtAmount.Text.Trim = "", "0", txtAmount.Text.Trim))
    End Sub

    Public Sub BindText()      
        rgValAmount.Enabled = True
        rvfAmount.ValidationExpression = CType(Me.RegularValidation, String)
        rvfAmount.ErrorMessage = CType(Me.RegularValidationMessage, String)
        rvfAmount.Enabled = True
        rgValAmount.Enabled = Me.EnabledRangeValidator
        rgValAmount.Visible = Me.EnabledRangeValidator
        rgValAmount.MinimumValue = CType(Me.MinimumRange, String)
        rgValAmount.MaximumValue = CType(Me.MaximumRange, String)
        rgValAmount.ErrorMessage = CType(Me.ErrorRange, String)
        rgValAmount.Type = Me.TypeRangeValidator
        If Me.DisableReadOnly = "Yes" Then
            txtAmount.ReadOnly = True
        End If
        txtAmount.MaxLength = CType(viewstate("MaxLength"), Integer)
        txtAmount.Columns = CType(viewstate("Column"), Integer)
        txtAmount.Text = CType(Me.Text, String)
    End Sub
End Class