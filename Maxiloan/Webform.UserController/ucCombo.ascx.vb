﻿
#Region "Import"
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
#End Region

Public Class ucCombo
    Inherits ControlBased
    Private oController As New CollectorController
    Dim chrClientID As String
    Dim m_coll As New CollectorProfController
#Region "properties"
    Public Property cgid() As String
        Get
            Return CType(viewstate("CGID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CGID") = Value
        End Set
    End Property

    Public Property CollectorTypeID() As String
        Get
            Return CType(cboParent.SelectedItem.Value, String)
            'Return CType(cboParent.SelectedItem.Value, String)
        End Get
        Set(ByVal Value As String)
            cboParent.SelectedIndex = cboParent.Items.IndexOf(cboParent.Items.FindByValue(Value))
        End Set
    End Property

    Public Property CollectorTypeName() As String
        Get
            Return CType(viewstate("CollectorTypeName"), String)
            'Return CType(cboParent.SelectedItem.Text, String)
        End Get
        Set(ByVal Value As String)
            viewstate("CollectorTypeName") = Value
        End Set
    End Property


    Public Property SupervisorID() As String
        Get
            Return hdnSP.Value
        End Get
        Set(ByVal Value As String)
            viewstate("SupervisorID") = Value
        End Set
    End Property

    Public Property SupervisorName() As String
        Get
            Return CType(viewstate("SupervisorName"), String)
            'Return CType(cboChild.SelectedItem.Text, String)
        End Get
        Set(ByVal Value As String)
            viewstate("SupervisorName") = Value
            'cboChild.SelectedIndex = cboChild.Items.IndexOf(cboChild.Items.FindByText(Value))
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not IsPostBack Then
            chrClientID = Trim(cboChild.ClientID)

            Dim dtParent As New DataTable
            Dim Coll As New Parameter.Collector
            Dim CollList As New Parameter.Collector

            With Coll
                .strConnection = GetConnectionString()
                .CGID = Me.GroubDbID
            End With

            CollList = oController.GetCollectorTypeCombo(Coll)
            dtParent = CollList.ListData

            cboParent.DataTextField = "Description"
            cboParent.DataValueField = "CollectorType"
            cboParent.DataSource = dtParent
            cboParent.DataBind()
            cboParent.Items.Insert(0, "Select One")
            cboParent.Items(0).Value = "0"
            cboParent.SelectedIndex = 0
            BindChild()

        End If

    End Sub

    Public Sub BindChild()
        Dim dtChild As New DataTable
        Dim Coll As New Parameter.Collector
        Dim CollList As New Parameter.Collector

        With Coll
            .strConnection = getconnectionstring()
            .CGID = Me.GroubDbID
        End With

        CollList = oController.GetSupervisorCombo(Coll)
        dtChild = CollList.ListData
        Header.InnerHtml = GenerateScript(dtChild)
    End Sub

    Public Function WOPonChange() As String
        Return "WOPChange('" & Trim(cboParent.ClientID) & "','" & Trim(cboChild.ClientID) & "','" & Trim(hdnSP.ClientID) & "',((this.selectedIndex == -1) ? null : ListData[this.selectedIndex]));"
    End Function

    Public Function SetFocuscboChild() As String
        Return "SetcboChildFocus()"
    End Function

    Private Function GenerateScript(ByVal DtTable As DataTable) As String
        Dim strScript As String
        Dim strScript1 As String
        Dim DataRow As DataRow()
        Dim strType As String
        Dim i As Int32
        Dim j As Int32
        strScript = "<script language=""JavaScript"">" & vbCrLf
        strScript &= "ListData = new Array(" & vbCrLf
        strType = ""
        For j = 0 To cboParent.Items.Count - 1
            DataRow = DtTable.Select(" collectortype = '" & cboParent.Items(j).Value & "'")
            If DataRow.Length > 0 Then
                For i = 0 To DataRow.Length - 1
                    If strType <> CStr(DataRow(i)("CollectorType")).Trim Then
                        strType = CStr(DataRow(i)("CollectorType")).Trim
                        strScript &= "new Array(" & vbCrLf
                        strScript1 = ""
                    End If


                    If strScript1 = "" Then
                        strScript1 = " new Array('" & CStr(DataRow(i)("employeename")).Trim & "','" & CStr(IIf(IsDBNull(DataRow(i)("supervisor")), "-", DataRow(i)("supervisor"))).Trim & "') "
                    Else
                        strScript1 &= "," & vbCrLf & " new Array('" & CStr(DataRow(i)("employeename")).Trim & "','" & CStr(IIf(IsDBNull(DataRow(i)("supervisor")), "-", DataRow(i)("supervisor"))).Trim & "') "
                    End If
                Next
                strScript &= strScript1 & ")," & vbCrLf
            Else
                strScript &= " null," & vbCrLf
            End If
        Next

        If Right(strScript.Trim, 5) = "null," Then
            strScript = Left(strScript.Trim, strScript.Trim.Length - 1)
        Else
            strScript = Left(strScript.Trim, strScript.Trim.Length - 2)
        End If

        If Right(strScript.Trim, 4) = "null" Then
            strScript &= vbCrLf & ");" & vbCrLf
        Else
            strScript &= vbCrLf & "));" & vbCrLf
        End If
        'strScript &= vbCrLf & "));" & vbCrLf

        strScript &= "</script>"
        Return strScript
    End Function

    'Public Sub BindCombo()
    '    cboParent.SelectedIndex = cboParent.Items.IndexOf(cboParent.Items.FindByValue(Me.CollectorTypeID))
    '    cboChild.SelectedIndex = cboChild.Items.IndexOf(cboChild.Items.FindByValue("Flin"))
    'End Sub

    Public Sub GetComboValue()
        Me.CollectorTypeID = cboParent.SelectedItem.Value.Trim
        Me.CollectorTypeName = cboParent.SelectedItem.Text.Trim
    End Sub

    Private Sub cboParent_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboParent.SelectedIndexChanged
        cboChild.Visible = True
    End Sub

    'Public Function GenerateChild() As String

    '    Return "GenerateChild('" & Trim(cboParent.ClientID) & "','" & Trim(cboChild.ClientID) & "','" & Trim(hdnSP.ClientID) & "',((this.selectedIndex == -1) ? null : ListData[this.selectedIndex])" & Me.SupervisorID & ");"
    'End Function

    'Public Sub GoGenerateChild()
    '    Header.InnerHtml = "<script language=""""JavaScript""""> SetcboChildFocus();</script>"
    'End Sub

    Public Sub InsertComboChild()
        Dim coll As New Parameter.Collector
        Dim Supervisor_List As New Parameter.Collector
        Dim i As Integer
        Dim indexSelected As Int16

        Dim dt As New DataTable

        With coll
            .strConnection = getConnectionString
            .CGID = Me.cgid
            .CollectorType = cboParent.SelectedItem.Value
        End With

        Supervisor_List = m_coll.GetSupervisorCombo(coll)
        dt = Supervisor_List.ListData



        With cboChild
            .Items.Clear()

            For i = 0 To dt.Rows.Count - 1
                .Items.Insert(i, CStr(dt.Rows(i).Item("SupervisorName")))
                .Items(i).Value = CStr(dt.Rows(i).Item("SupervisorID"))
                If .Items(i).Value.Trim = Me.SupervisorID.Trim Then indexSelected = CShort(i)
            Next
            .SelectedIndex = indexSelected
        End With
    End Sub

End Class
