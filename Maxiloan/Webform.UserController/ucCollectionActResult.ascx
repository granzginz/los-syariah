﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucCollectionActResult.ascx.vb" Inherits="Maxiloan.Webform.UserController.ucCollectionActResult" %>
<%@ Register TagPrefix="uc1" TagName="UcInstallmentSchedule" Src="UcInstallmentSchedule.ascx" %>
<div class="form_box">
                    <div class="form_left">
                        <label>No Kontrak</label>
                        <asp:HyperLink ID="hypAgreementNo" runat="server"></asp:HyperLink>
                    </div>
                    <div class="form_right">
                        <label>Nama Customer</label>
                        <asp:HyperLink ID="hypCustomerName" runat="server"></asp:HyperLink>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>Alamat</label>
                        <asp:Label ID="lblAddress" runat="server"></asp:Label>
                    </div>
                </div>


                <div class="form_box">
                    <div class="form_left">
                        <label>Asset</label>
                        <asp:Label ID="lblAsset" runat="server"></asp:Label>
                    </div>
                     <div class="form_right">
                        <label>No Polisi</label>
                        <asp:Label ID="lblLicenseNo" runat="server"></asp:Label>
                     </div> 
                </div>

                 
                <div class="form_box">
                    <div class="form_left">
                        <label>Nama CMO</label>
                        <asp:Label ID="lblCMONo" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                      <label>Collector</label>
                         <asp:Label ID="lblCollector" runat="server"></asp:Label>
                    </div>
                </div>

                <div class="form_box">
                     <div class="form_left">
                        <label>No Telepon Alamat KTP</label>
                        <asp:Label ID="lblLegalPhone1" runat="server"></asp:Label>
                        <asp:Label ID="lblLegalPhone2" runat="server"></asp:Label>
                    </div>

                    <div class="form_right"> 
                        <label> No Telepon tempat tinggal</label>
                        <asp:Label ID="lblResidencePhone1" runat="server"></asp:Label>
                        <asp:Label ID="lblResidencePhone2" runat="server"></asp:Label> 
                    </div>
                </div>


               
                <div class="form_box">
                      <div class="form_left">
                        <label>No Telepon Kantor</label>
                        <asp:Label ID="lblCompanyPhone1" runat="server"></asp:Label>
                        <asp:Label ID="lblCompanyPhone2" runat="server"></asp:Label>
                    </div>
                     <div class="form_right">
                        <label> No HandPhone</label>
                        <asp:Label ID="lblMobilePhone" runat="server"></asp:Label>
                     </div>
                </div>
                 
                <div class="form_box">
                    <div class="form_left">
                        <label>No Telepon Alamat Tagih</label>
                        <asp:Label ID="lblMailingPhone1" runat="server"></asp:Label>
                        <asp:Label ID="lblMailingPhone2" runat="server"></asp:Label>
                    </div>
                     <div class="form_right">
                          <label>No Telepon Suami/Istri</label>
                           <asp:Label ID="lblSuamiIstriPhone1" runat="server"></asp:Label>
                           <asp:Label ID="lblSuamiIstriPhone2" runat="server"></asp:Label>
                     </div> 
                </div>


                 <div class="form_box_header">
                    <div class="form_single">
                        <h5> TABEL ANGSURAN </h5>
                    </div>
                    </div>
                 <uc1:ucinstallmentschedule id="oInstallmentSchedule" runat="server" />

                   

    <div class="form_box">
        <div class="form_left">
            <label> Cara Pembayaran</label>
            <asp:Literal ID="ltlWayOfPayment" runat="server"></asp:Literal>
        </div>
                   
            <div class="form_right">
            <label>Jumlah Hari Tunggak</label>
            <asp:Label ID="lblOverDuedays" runat="server"></asp:Label>&nbsp;Hari
        </div>

    </div>