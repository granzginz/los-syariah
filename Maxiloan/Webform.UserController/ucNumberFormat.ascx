﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucNumberFormat.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.ucNumberFormat" %>

<asp:TextBox runat="server" ID="txtNumber" onblur="extractNumber(this,7,true);blockInvalid(this);this.value=numberFormat(this.value);this.value=blankToZero(this.value);"
    onkeyup="extractNumber(this,7,true);" onkeypress="return blockNonNumbers(this, event, true, true);"
    onfocus="this.value=resetNumber(this.value);" CssClass="numberAlign" OnChange="javascript:void(0)" autocomplete="off">0</asp:TextBox>
<asp:RangeValidator runat="server" ID="rv" Display="Dynamic" ErrorMessage="Input hanya boleh 0 s/d 999999999999999"
    ControlToValidate="txtNumber" MaximumValue="999999999999999" MinimumValue="0"
    Type="Currency" Enabled="false" CssClass="validator_general"></asp:RangeValidator>
<asp:RequiredFieldValidator runat="server" ID="rfv" Display="Dynamic" ErrorMessage="Input ini harus diisi!"
    ControlToValidate="txtNumber" CssClass="validator_general" Enabled="false"></asp:RequiredFieldValidator>
<asp:HiddenField runat="server" ID="hdnOnBlur" />
<asp:HiddenField runat="server" ID="hdnOnChange" />