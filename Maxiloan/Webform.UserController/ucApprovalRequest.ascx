﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucApprovalRequest.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.ucApprovalRequest" %>
<%@ Register TagPrefix="uc1" TagName="UCReason" Src="UCReason.ascx" %>
<div class="form_box">
    <div class="form_left">
        <label>
            Alasan</label>
        <uc1:UCReason ID="oReason" runat="server"></uc1:UCReason>
    </div>
    <div class="form_right">
        <label>
            Disetujui Oleh</label>
        <asp:DropDownList ID="cboApprovedBy" runat="server">
        </asp:DropDownList>
        <asp:RequiredFieldValidator ID="reqApprovedBy" runat="server" ControlToValidate="cboApprovedBy"
            ErrorMessage="Harap pilih DiSetujui Oleh" Display="Dynamic" InitialValue="0"
            CssClass="validator_general"></asp:RequiredFieldValidator>
    </div>
</div>
<div class="form_box">
    <div class="form_single">
        <label>
            Catatan
        </label>
        <asp:TextBox ID="txtNotes" runat="server" TextMode="MultiLine" CssClass="multiline_textbox"></asp:TextBox>
    </div>
</div>
