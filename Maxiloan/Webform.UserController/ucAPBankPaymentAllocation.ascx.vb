﻿Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.Controller

Public Class ucAPBankPaymentAllocation
    Inherits ControlBased
    Private m_controller As New DataUserControlController    
    Private dttBankName As New DataTable

    Public Property ComboID() As String
        Get
            Return CType(cboBankName.SelectedItem.Value, String)
        End Get
        Set(ByVal Value As String)
            cboBankName.SelectedIndex = cboBankName.Items.IndexOf(cboBankName.Items.FindByValue(Value))
        End Set
    End Property

    Public Property ComboDescription() As String
        Get
            Return CType(cboBankName.SelectedItem.Text, String)
        End Get
        Set(ByVal Value As String)
            cboBankName.SelectedIndex = cboBankName.Items.IndexOf(cboBankName.Items.FindByText(Value))
        End Set
    End Property

    Public Property FillRequired() As Boolean
        Get
            Return RequiredFieldValidator4.Enabled
        End Get
        Set(ByVal Value As Boolean)
            RequiredFieldValidator4.Enabled = Value
        End Set
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not IsPostBack Then
            cboBankName.Items.Clear()
            Dim DtBankName As DataTable
            DtBankName = CType(Me.Cache.Item(CACHE_APBANK), DataTable)
            If DtBankName Is Nothing Then
                Dim dtBankNameCache As New DataTable
                dtBankNameCache = m_controller.GetAPBank(GetConnectionString)
                Me.Cache.Insert(CACHE_APBANK, dtBankNameCache, Nothing, DateTime.Now.AddHours(DEFAULT_TIME_DURATION_CACHE_HOUR), TimeSpan.Zero)
                DtBankName = CType(Me.Cache.Item(CACHE_APBANK), DataTable)
            End If

            cboBankName.DataValueField = "ID"
            cboBankName.DataTextField = "Name"
            cboBankName.DataSource = DtBankName
            cboBankName.DataBind()
            cboBankName.Items.Insert(0, "Select One")
            cboBankName.Items(0).Value = "0"
            cboBankName.SelectedIndex = 0
        End If

        Me.ComboID = cboBankName.SelectedItem.Value.Trim
        Me.ComboDescription = cboBankName.SelectedItem.Text.Trim
    End Sub

    Public Sub BindData()
        If Me.ComboID = "" Then
            cboBankName.SelectedIndex = 0
        Else
            cboBankName.SelectedIndex = cboBankName.Items.IndexOf(cboBankName.Items.FindByValue(Me.ComboID))
        End If
    End Sub

    Private Sub btnRefresh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRefresh.Click
        Me.Cache.Remove(CACHE_APBANK)
    End Sub
End Class