﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UcCashier.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.UcCashier" %>
<asp:DropDownList ID="cmbCashier" runat="server" AutoPostBack="true" onselectedindexchanged="ddl_SelectedIndexChanged">
</asp:DropDownList>
<asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" InitialValue="0"
    Display="Dynamic" ErrorMessage="Harap pilih Kasir" ControlToValidate="cmbCashier" CssClass ="validator_general"></asp:RequiredFieldValidator>
