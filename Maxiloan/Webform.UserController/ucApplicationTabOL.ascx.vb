﻿Imports System.Data.SqlClient

Public Class ucApplicationTabOL
    Inherits Maxiloan.Webform.ControlBased

    Public Property ApplicationID As String
    Private Property CustomerID As String
    Private Property SupplierGroupID As String

    Public Sub setLink()
        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(GetConnectionString)
        Dim objReader As SqlDataReader

        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()

            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.CommandText = "spGetApplicationTab"
            objCommand.Parameters.Add("@ApplicationID", SqlDbType.VarChar, 20).Value = Me.ApplicationID.Trim
            objCommand.Connection = objConnection
            objReader = objCommand.ExecuteReader()

            If objReader.Read Then
                Me.CustomerID = objReader.Item("CustomerID").ToString
                Me.SupplierGroupID = "" 'objReader.Item("SupplierGroupID").ToString

                If Not IsDBNull(objReader.Item("DateEntryIncentiveData")) Then
                    hypAplikasi.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/OperatingLease/ApplicationOperatingLease_003.aspx?id=" & Me.CustomerID & "&appid=" & Me.ApplicationID & "&page=Edit"
                    hypAsset.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/OperatingLease/AssetDataOperatingLease_002.aspx?appid=" & Me.ApplicationID.ToString.Trim & "&page=Edit"
                    hypAsuransi.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/OperatingLease/AppInsuranceOperatingLease.aspx?ApplicationID=" & Me.ApplicationID.ToString.Trim & "&PageSource=CompanyCustomer&SupplierGroupID=" & Me.SupplierGroupID.ToString.Trim & "&page=Edit"
                    hypFinancial.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/OperatingLease/FinancialDataOperatingLease_003.aspx?CustomerID=" & Me.CustomerID.Trim & "&ApplicationID=" & Me.ApplicationID.Trim & "&page=Edit"
                    hypFinancial2.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/OperatingLease/FinancialDataOperatingLease_004.aspx?CustomerID=" & Me.CustomerID.Trim & "&ApplicationID=" & Me.ApplicationID.Trim & "&page=Edit"
                    '   hypRefund.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/OperatingLease/IncentiveDataOperatingLease.aspx?CustomerID=" & Me.CustomerID.Trim & "&ApplicationID=" & Me.ApplicationID.Trim & "&page=Edit"
                    'hypRincianTransaksi.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewApplication/ApplicationTransactionDetail.aspx?CustomerID=" & Me.CustomerID.Trim & "&ApplicationID=" & Me.ApplicationID.Trim
                    'hypHasilSurvey.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewApplication/HasilSurvey.aspx?CustomerID=" & Me.CustomerID.Trim & "&ApplicationID=" & Me.ApplicationID.Trim
                ElseIf Not IsDBNull(objReader.Item("DateEntryFinancialData2")) Then
                    hypAplikasi.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/OperatingLease/ApplicationOperatingLease_003.aspx?id=" & Me.CustomerID & "&appid=" & Me.ApplicationID & "&page=Edit"
                    hypAsset.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/OperatingLease/AssetDataOperatingLease_002.aspx?appid=" & Me.ApplicationID.ToString.Trim & "&page=Edit"
                    hypAsuransi.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/OperatingLease/AppInsuranceOperatingLease.aspx?ApplicationID=" & Me.ApplicationID.ToString.Trim & "&PageSource=CompanyCustomer&SupplierGroupID=" & Me.SupplierGroupID.ToString.Trim & "&page=Edit"
                    hypFinancial.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/OperatingLease/FinancialDataOperatingLease_003.aspx?CustomerID=" & Me.CustomerID.Trim & "&ApplicationID=" & Me.ApplicationID.Trim & "&page=Edit"
                    hypFinancial2.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/OperatingLease/FinancialDataOperatingLease_004.aspx?CustomerID=" & Me.CustomerID.Trim & "&ApplicationID=" & Me.ApplicationID.Trim & "&page=Edit"
                    '    hypRefund.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/OperatingLease/IncentiveDataOperatingLease.aspx?CustomerID=" & Me.CustomerID.Trim & "&ApplicationID=" & Me.ApplicationID.Trim & "&page=Edit"
                    'hypRincianTransaksi.NavigateUrl = ""
                    'hypHasilSurvey.NavigateUrl = ""
                ElseIf Not IsDBNull(objReader.Item("DateEntryFinancialData")) Then
                    hypAplikasi.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/OperatingLease/ApplicationOperatingLease_003.aspx?id=" & Me.CustomerID & "&appid=" & Me.ApplicationID & "&page=Edit"
                    hypAsset.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/OperatingLease/AssetDataOperatingLease_002.aspx?appid=" & Me.ApplicationID.ToString.Trim & "&page=Edit"
                    hypAsuransi.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/OperatingLease/AppInsuranceOperatingLease.aspx?ApplicationID=" & Me.ApplicationID.ToString.Trim & "&PageSource=CompanyCustomer&SupplierGroupID=" & Me.SupplierGroupID.ToString.Trim & "&page=Edit"
                    hypFinancial.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/OperatingLease/FinancialDataOperatingLease_003.aspx?CustomerID=" & Me.CustomerID.Trim & "&ApplicationID=" & Me.ApplicationID.Trim & "&page=Edit"
                    hypFinancial2.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/OperatingLease/FinancialDataOperatingLease_004.aspx?CustomerID=" & Me.CustomerID.Trim & "&ApplicationID=" & Me.ApplicationID.Trim & "&page=Edit"
                    '   hypRefund.NavigateUrl = ""
                    'hypRincianTransaksi.NavigateUrl = ""
                    'hypHasilSurvey.NavigateUrl = ""
                ElseIf Not IsDBNull(objReader.Item("DateEntryInsuranceData")) Then
                    hypAplikasi.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/OperatingLease/ApplicationOperatingLease_003.aspx?id=" & Me.CustomerID & "&appid=" & Me.ApplicationID & "&page=Edit"
                    hypAsset.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/OperatingLease/AssetDataOperatingLease_002.aspx?appid=" & Me.ApplicationID.ToString.Trim & "&page=Edit"
                    hypAsuransi.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/OperatingLease/AppInsuranceOperatingLease.aspx?ApplicationID=" & Me.ApplicationID.ToString.Trim & "&PageSource=CompanyCustomer&SupplierGroupID=" & Me.SupplierGroupID.ToString.Trim & "&page=Edit"
                    hypFinancial.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/OperatingLease/FinancialDataOperatingLease_003.aspx?CustomerID=" & Me.CustomerID.Trim & "&ApplicationID=" & Me.ApplicationID.Trim 
                    hypFinancial2.NavigateUrl = ""
                    '   hypRefund.NavigateUrl = ""
                    'hypRincianTransaksi.NavigateUrl = ""
                    'hypHasilSurvey.NavigateUrl = ""
                ElseIf Not IsDBNull(objReader.Item("DateEntryAssetData")) Then
                    hypAplikasi.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/OperatingLease/ApplicationOperatingLease_003.aspx?id=" & Me.CustomerID & "&appid=" & Me.ApplicationID & "&page=Edit"
                    hypAsset.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/OperatingLease/AssetDataOperatingLease_002.aspx?appid=" & Me.ApplicationID.ToString.Trim & "&page=Edit"
                    hypAsuransi.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/OperatingLease/AppInsuranceOperatingLease.aspx?ApplicationID=" & Me.ApplicationID.ToString.Trim & "&PageSource=CompanyCustomer&SupplierGroupID=" & Me.SupplierGroupID.ToString.Trim
                    hypFinancial.NavigateUrl = ""
                    hypFinancial2.NavigateUrl = ""
                    '   hypRefund.NavigateUrl = ""
                    'hypRincianTransaksi.NavigateUrl = ""
                    ' hypHasilSurvey.NavigateUrl = ""
                ElseIf Not IsDBNull(objReader.Item("DateEntryApplicationData")) Then
                    hypAplikasi.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/OperatingLease/ApplicationOperatingLease_003.aspx?id=" & Me.CustomerID & "&appid=" & Me.ApplicationID & "&page=Edit"
                    hypAsset.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/OperatingLease/AssetDataOperatingLease_002.aspx?appid=" & Me.ApplicationID.ToString.Trim
                    hypAsuransi.NavigateUrl = ""
                    hypFinancial.NavigateUrl = ""
                    hypFinancial2.NavigateUrl = ""
                    '    hypRefund.NavigateUrl = ""
                    'hypRincianTransaksi.NavigateUrl = ""
                    'hypHasilSurvey.NavigateUrl = ""
                Else
                    hypAplikasi.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/OperatingLease/ApplicationOperatingLease_003.aspx?id=" & Me.CustomerID & "&appid=" & Me.ApplicationID & "&page=Edit"
                    hypAsset.NavigateUrl = ""
                    hypAsuransi.NavigateUrl = ""
                    hypFinancial.NavigateUrl = ""
                    hypFinancial2.NavigateUrl = ""
                    '  hypRefund.NavigateUrl = ""
                    'hypRincianTransaksi.NavigateUrl = ""
                    'hypHasilSurvey.NavigateUrl = ""
                End If

            End If

            objReader.Close()
        Catch ex As Exception
            Throw New Exception(ex.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
            objCommand.Dispose()
        End Try
    End Sub

    Public Sub selectedTab(ByVal strTab As String)
        Select Case strTab
            Case "Application"
                tabAplikasi.Attributes.Add("class", "tab_selected")
                tabAsset.Attributes.Add("class", "tab_notselected")
                tabAsuransi.Attributes.Add("class", "tab_notselected")
                tabFinancial.Attributes.Add("class", "tab_notselected")
                tabFinancial2.Attributes.Add("class", "tab_notselected")
                ' tabRefund.Attributes.Add("class", "tab_notselected")
                'tabRincianTransaksi.Attributes.Add("class", "tab_notselected")
                'tabSurvey.Attributes.Add("class", "tab_notselected")
            Case "Asset"
                tabAplikasi.Attributes.Add("class", "tab_notselected")
                tabAsset.Attributes.Add("class", "tab_selected")
                tabAsuransi.Attributes.Add("class", "tab_notselected")
                tabFinancial.Attributes.Add("class", "tab_notselected")
                tabFinancial2.Attributes.Add("class", "tab_notselected")
                '    tabRefund.Attributes.Add("class", "tab_notselected")
                'tabRincianTransaksi.Attributes.Add("class", "tab_notselected")
                'tabSurvey.Attributes.Add("class", "tab_notselected")
            Case "Asuransi"
                tabAplikasi.Attributes.Add("class", "tab_notselected")
                tabAsset.Attributes.Add("class", "tab_notselected")
                tabAsuransi.Attributes.Add("class", "tab_selected")
                tabFinancial.Attributes.Add("class", "tab_notselected")
                tabFinancial2.Attributes.Add("class", "tab_notselected")
                '    tabRefund.Attributes.Add("class", "tab_notselected")
                'tabRincianTransaksi.Attributes.Add("class", "tab_notselected")
                'tabSurvey.Attributes.Add("class", "tab_notselected")
            Case "Financial"
                tabAplikasi.Attributes.Add("class", "tab_notselected")
                tabAsset.Attributes.Add("class", "tab_notselected")
                tabAsuransi.Attributes.Add("class", "tab_notselected")
                tabFinancial.Attributes.Add("class", "tab_selected")
                tabFinancial2.Attributes.Add("class", "tab_notselected")
                '   tabRefund.Attributes.Add("class", "tab_notselected")
            Case "Financial2"
                tabAplikasi.Attributes.Add("class", "tab_notselected")
                tabAsset.Attributes.Add("class", "tab_notselected")
                tabAsuransi.Attributes.Add("class", "tab_notselected")
                tabFinancial.Attributes.Add("class", "tab_notselected")
                tabFinancial2.Attributes.Add("class", "tab_selected")
                '    tabRefund.Attributes.Add("class", "tab_notselected")
                'tabRincianTransaksi.Attributes.Add("class", "tab_notselected")
                'tabSurvey.Attributes.Add("class", "tab_notselected")
            Case "Refund"
                tabAplikasi.Attributes.Add("class", "tab_notselected")
                tabAsset.Attributes.Add("class", "tab_notselected")
                tabAsuransi.Attributes.Add("class", "tab_notselected")
                tabFinancial.Attributes.Add("class", "tab_notselected")
                tabFinancial2.Attributes.Add("class", "tab_notselected")
                '    tabRefund.Attributes.Add("class", "tab_selected")
                'tabRincianTransaksi.Attributes.Add("class", "tab_notselected")
                'tabSurvey.Attributes.Add("class", "tab_notselected")
                'Case "RincianTransaksi"
                '    tabAplikasi.Attributes.Add("class", "tab_notselected")
                '    tabAsset.Attributes.Add("class", "tab_notselected")
                '    tabAsuransi.Attributes.Add("class", "tab_notselected")
                '    tabFinancial.Attributes.Add("class", "tab_notselected")
                '    'tabRefund.Attributes.Add("class", "tab_notselected")
                '    'tabRincianTransaksi.Attributes.Add("class", "tab_selected")
                '    tabSurvey.Attributes.Add("class", "tab_notselected")

            Case Else
                tabAplikasi.Attributes.Add("class", "tab_notselected")
                tabAsset.Attributes.Add("class", "tab_notselected")
                tabAsuransi.Attributes.Add("class", "tab_notselected")
                tabFinancial.Attributes.Add("class", "tab_notselected")
                '    tabRefund.Attributes.Add("class", "tab_notselected")
                tabFinancial2.Attributes.Add("class", "tab_notselected")
                'tabRincianTransaksi.Attributes.Add("class", "tab_notselected")
                'tabSurvey.Attributes.Add("class", "tab_selected")
        End Select
    End Sub
End Class