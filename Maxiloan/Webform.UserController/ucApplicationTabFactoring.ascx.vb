﻿Imports System.Data.SqlClient

Public Class ucApplicationTabFactoring
    Inherits Maxiloan.Webform.ControlBased

    Public Property ApplicationID As String
    Private Property CustomerID As String
    Private Property SupplierGroupID As String

    Public Sub setLink()
        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(GetConnectionString)
        Dim objReader As SqlDataReader

        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()

            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.CommandText = "spGetApplicationTab"
            objCommand.Parameters.Add("@ApplicationID", SqlDbType.VarChar, 20).Value = Me.ApplicationID.Trim
            objCommand.Connection = objConnection
            objReader = objCommand.ExecuteReader()

            If objReader.Read Then
                Me.CustomerID = objReader.Item("CustomerID").ToString
                Me.SupplierGroupID = "" 'objReader.Item("SupplierGroupID").ToString

                If Not IsDBNull(objReader.Item("DateEntryFinancialData")) Then
                    hypAplikasi.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewFactoring/ApplicationFactoring_003.aspx?id=" & Me.CustomerID & "&appid=" & Me.ApplicationID & "&page=Edit"
                    hypInvoice.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewFactoring/InvoiceData.aspx?appid=" & Me.ApplicationID.ToString.Trim & "&page=Edit"
                    hypFinancial.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewFactoring/FinancialFactoringData.aspx?appid=" & Me.ApplicationID.Trim & "&page=Edit"
                    'hypFinancial2.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewFactoring/FinancialDataFactoring_004.aspx?appid=" & Me.ApplicationID.Trim & "&page=Edit"
                    'hypInsentif.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewFactoring/IncentiveDataFactoring.aspx?appid=" & Me.ApplicationID.Trim & "&page=Edit"
                ElseIf Not IsDBNull(objReader.Item("DateEntryAssetData")) Then
                    hypAplikasi.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewFactoring/ApplicationFactoring_003.aspx?id=" & Me.CustomerID & "&appid=" & Me.ApplicationID & "&page=Edit"
                    hypInvoice.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewFactoring/InvoiceData.aspx?appid=" & Me.ApplicationID.ToString.Trim & "&page=Edit"
                    hypFinancial.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewFactoring/FinancialFactoringData.aspx?appid=" & Me.ApplicationID.Trim & "&page=Edit"
                    'hypFinancial2.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewFactoring/FinancialDataFactoring_004.aspx?appid=" & Me.ApplicationID.Trim & "&page=Edit"
                    'hypInsentif.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewFactoring/IncentiveDataFactoring.aspx?appid=" & Me.ApplicationID.Trim & "&page=Edit"
                ElseIf Not IsDBNull(objReader.Item("DateEntryApplicationData")) Then
                    hypAplikasi.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewFactoring/ApplicationFactoring_003.aspx?id=" & Me.CustomerID & "&appid=" & Me.ApplicationID & "&page=Edit"
                    hypInvoice.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewFactoring/InvoiceData.aspx?appid=" & Me.ApplicationID.ToString.Trim & "&page=Edit"
                    hypFinancial.NavigateUrl = ""
                    'hypFinancial2.NavigateUrl = ""
                    'hypInsentif.NavigateUrl = ""
                Else
                    hypAplikasi.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewFactoring/ApplicationFactoring_003.aspx?id=" & Me.CustomerID & "&appid=" & Me.ApplicationID & "&page=Edit"
                    hypInvoice.NavigateUrl = ""
                    hypFinancial.NavigateUrl = ""
                    'hypFinancial2.NavigateUrl = ""
                    'hypInsentif.NavigateUrl = ""
                End If

            End If

            objReader.Close()
        Catch ex As Exception
            Throw New Exception(ex.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
            objCommand.Dispose()
        End Try
    End Sub

    Public Sub selectedTab(ByVal strTab As String)
        Select Case strTab
            Case "Application"
                tabAplikasi.Attributes.Add("class", "tab_selected")
                tabAsset.Attributes.Add("class", "tab_notselected")
                tabFinancial.Attributes.Add("class", "tab_notselected")
                'tabFinancial2.Attributes.Add("class", "tab_notselected")
                'tabInsentif.Attributes.Add("class", "tab_notselected")
            Case "Invoice"
                tabAplikasi.Attributes.Add("class", "tab_notselected")
                tabAsset.Attributes.Add("class", "tab_selected")
                tabFinancial.Attributes.Add("class", "tab_notselected")
                'tabFinancial2.Attributes.Add("class", "tab_notselected")
                'tabInsentif.Attributes.Add("class", "tab_notselected")
            Case "Financial"
                tabAplikasi.Attributes.Add("class", "tab_notselected")
                tabAsset.Attributes.Add("class", "tab_notselected")
                tabFinancial.Attributes.Add("class", "tab_selected")
                'tabFinancial2.Attributes.Add("class", "tab_notselected")
                'tabInsentif.Attributes.Add("class", "tab_notselected")
                'Case "Financial2"
                '    tabAplikasi.Attributes.Add("class", "tab_notselected")
                '    tabAsset.Attributes.Add("class", "tab_notselected")
                '    tabFinancial.Attributes.Add("class", "tab_notselected")
                '    tabFinancial2.Attributes.Add("class", "tab_selected")
                '    tabInsentif.Attributes.Add("class", "tab_notselected")
                'Case "Insentif"
                '    tabAplikasi.Attributes.Add("class", "tab_notselected")
                '    tabAsset.Attributes.Add("class", "tab_notselected")
                '    tabFinancial.Attributes.Add("class", "tab_notselected")
                '    tabFinancial2.Attributes.Add("class", "tab_notselected")
                '    tabInsentif.Attributes.Add("class", "tab_selected")
            Case Else
                tabAplikasi.Attributes.Add("class", "tab_selected")
                tabAsset.Attributes.Add("class", "tab_notselected")
                tabFinancial.Attributes.Add("class", "tab_notselected")
                'tabFinancial2.Attributes.Add("class", "tab_notselected")
                'tabInsentif.Attributes.Add("class", "tab_notselected")
        End Select
    End Sub
End Class