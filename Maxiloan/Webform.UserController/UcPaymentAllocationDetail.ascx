﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UcPaymentAllocationDetail.ascx.vb" Inherits="Maxiloan.Webform.UserController.UcPaymentAllocationDetail" %>
<%@ Register Src="ucNumberFormat.ascx" TagName="ucNumberFormat" TagPrefix="uc" %>
<script type="text/javascript">
    function toggleVisibility(controlId) {
        //var control = document.getElementById(controlId);
        var control = document.getElementById('<%=panelMore.ClientId %>');
        if (control.style.display == "none")
            control.style.display = "";
        else
            control.style.display = "none";
    }

    function calculateTotal() {
        try {

            //declare all textbox

            var NilaiAngsuran = document.getElementById('<%= txtInstallmentDue0.ClientId %>').value;
            var NilaiTitipan = document.getElementById('<%= txtTtpAngsuran.ClientId %>').value;
            var lblMessage = document.getElementById('<%= txtRnd.ClientId %>');


            if (parseFloat(NilaiTitipan.replace(/\,/g, "")) < 0) {
                alert("Nilai parsial salah");
                document.getElementById('<%= txtTtpAngsuran.ClientId %>').value = "0";
            }

            if (parseFloat(NilaiAngsuran.replace(/\,/g, "")) > 0) {
                    if (parseFloat(NilaiTitipan.replace(/\,/g, "")) >= parseFloat(NilaiAngsuran.replace(/\,/g, ""))) {
                    alert("error: Nilai parsial tidak boleh lebih besar atau sama dengan dari " + number_format(parseFloat(NilaiAngsuran.replace(/\,/g, ""))));
                    document.getElementById('<%= txtTtpAngsuran.ClientId %>').value = "0";
                    }
            }

            var result = document.getElementById('<%= lblTotalDiterima.ClientId %>');
            var txtRnd = document.getElementById('<%= txtRnd.ClientId %>');


            var total = 0;

            var listControls = ['<%= txtInstallmentDue.ClientId %>', '<%= txtTtpAngsuran.ClientId %>', '<%= txtInstallLC.ClientId %>', '<%= txtInstallCollFee.ClientId %>'
                            , '<%= txtInsuranceDue.ClientId %>', '<%= txtInsuranceLC.ClientId %>', '<%= txtInsuranceCollFee.ClientId %>'
                            , '<%= txtPDCBounceFee.ClientId %>', '<%= txtSTNKRenewalFee.ClientId %>'
                            , '<%= txtInsuranceClaimExpense.ClientId %>', '<%= txtRepossessionFee.ClientId %>'
                            , '<%= txtPrepaid.ClientId %>','<%= txtPLL.ClientId %>','<%= txtGracePeriod.ClientId %>'];


            //calculate
            var i = 0;
            var hargaRow = "";
            for (i = 0; i < listControls.length; i++) {
                hargaRow = document.getElementById(listControls[i]).value.replace(/\,/g, "");
                if (hargaRow == "")
                    hargaRow = 0;
                total = total + parseFloat(hargaRow);

            }
            //set all value
            result.innerHTML = numberWithCommas(total);
            $('#<%= lblTotalDiterima.ClientID %>').text(total);
            calculateChange();
        }
        catch (err) {
            var lblMessage = document.getElementById('<%= txtRnd.ClientId %>');
            lblMessage.value = "error:" + err;
        }



    }

    function numberWithCommas(x) {
        var parts = x.toString().split(".");
        return parts[0].replace(/\B(?=(\d{3})+(?=$))/g, ",") + (parts[1] ? "." + parts[1] : "");
    }

    function calculateChange() {
        try {

            var diterima = document.getElementById('<%= txtUangDiterima.ClientId %>').value.replace(",", "");
            var bayar = document.getElementById('<%= lblTotalDiterima.ClientId %>').innerHTML;

            //alert(bayar.replace(/\,/g, ""));
            var kembali = parseFloat(diterima.replace(/\,/g, "")) - parseFloat(bayar.replace(/\,/g, ""));
            var lblKembali = document.getElementById('<%= lblKembali.ClientId %>');
            lblKembali.innerHTML = numberWithCommas(kembali);
        }

        catch (err) {
            var lblMessage = document.getElementById('<%= txtRnd.ClientId %>');
            lblMessage.value = "error:" + err;
        }

    }
    function CekNilaiAngsuran(NilaiInput) {
        
        var NilaiAngsuran = document.getElementById('<%= txtInstallmentDue0.ClientId %>').value;
        var modulus = parseInt(NilaiInput) % parseInt(NilaiAngsuran.replace(/\s*,\s*/g, ''));
        var nilaiPlus = document.getElementById('<%= txtToleransiLebih.ClientId %>').value;
        var nilaiMin = document.getElementById('<%= txtToleransiKurang.ClientId %>').value;
        var AkhirBayar = document.getElementById('<%= txtisAkhirBayar.ClientId %>').value;
        
        document.getElementById('<%= txtTtpAngsuran.ClientId %>').value = "0";

        if (NilaiInput > 0) {
            if (parseInt(NilaiInput) < parseInt(NilaiAngsuran.replace(/\s*,\s*/g, ''))){
                if (parseInt(NilaiAngsuran.replace(/\s*,\s*/g, '')) - parseInt(NilaiInput) > parseInt(nilaiMin)) {
                    alert("Input angsuran lebih kecil dari nilai angsuran, alokasikan ke parsial secara otomatis");
                    document.getElementById('<%= txtInstallmentDue.ClientId %>').value = "0";
                    document.getElementById('<%= txtTtpAngsuran.ClientId %>').value = number_format(NilaiInput);
                }
            } else if (parseInt(NilaiInput) > parseInt(NilaiAngsuran.replace(/\s*,\s*/g, ''))) {
                if (parseInt(NilaiInput) - parseInt(NilaiAngsuran.replace(/\s*,\s*/g, '')) > parseInt(nilaiPlus)) {
                    if (modulus > 0) {
                        if (parseInt(NilaiAngsuran.replace(/\s*,\s*/g, '')) - parseInt(modulus) > parseInt(nilaiPlus)) {
                            if (AkhirBayar == "True") {
                                alert(" Input Angsuran kelebihan " + number_format(modulus) + " dari nilai angsuran dan kelipatannya, sisanya akan di masukkan ke dalam prepaid. ");
                                document.getElementById('<%= txtPrepaid.ClientId %>').value = number_format(modulus);
                                document.getElementById('<%= txtInstallmentDue.ClientId %>').value = parseInt(NilaiInput.replace(/\s*,\s*/g, '')) - parseInt(modulus);
                            } else {
                                alert(" Input Angsuran kelebihan " + number_format(modulus) + " dari nilai angsuran dan kelipatannya, sisanya akan di masukkan ke dalam parsial. ");
                                document.getElementById('<%= txtTtpAngsuran.ClientId %>').value = number_format(modulus);
                                document.getElementById('<%= txtInstallmentDue.ClientId %>').value = parseInt(NilaiInput.replace(/\s*,\s*/g, '')) - parseInt(modulus);
                            }
                        } 
                    }
                } else {
                    //alert(" Ada kelebihan bayar sebesar Rp. " + number_format(modulus) + ", Save jika ingin dijadikan sebangai pendapatan, jika tidak alokasikan secara manual!");
                    if (modulus > 0) {
						if (confirm("Ada kelebihan bayar sebesar Rp. " + number_format(modulus) + ", Jadikan sebagai pendapatan?")) {
							document.getElementById('<%= txtPLL.ClientId %>').value = number_format(modulus);
							//document.getElementById('<%= txtInstallmentDue.ClientId %>').value = parseInt(NilaiAngsuran.replace(/\s*,\s*/g, ''));
							document.getElementById('<%= txtInstallmentDue.ClientId %>').value = parseInt(NilaiInput.replace(/\s*,\s*/g, '')) - parseInt(modulus);
						} else {
							document.getElementById('<%= txtTtpAngsuran.ClientId %>').value = number_format(modulus);
							document.getElementById('<%= txtInstallmentDue.ClientId %>').value = parseInt(NilaiAngsuran.replace(/\s*,\s*/g, ''));
						}
                    }

                }
            }
        }
        
    }
    function number_format(number, decimals, dec_point, thousands_sep) {

        number = (number + '')
            .replace(/[^0-9+\-Ee.]/g, '');
        var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function (n, prec) {
                var k = Math.pow(10, prec);
                return '' + (Math.round(n * k) / k)
                .toFixed(prec);
            };
        // Fix for IE parseFloat(0.55).toFixed(0) = 0;
        s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
            .split('.');
        if (s[0].length > 3) {
            s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
        }
        if ((s[1] || '')
            .length < prec) {
            s[1] = s[1] || '';
            s[1] += new Array(prec - s[1].length + 1)
              .join('0');
        }
        return s.join(dec);
    }
</script>
<div style="display:none">
    <asp:TextBox ID="txtToleransiKurang" runat="server"></asp:TextBox>
    <asp:TextBox ID="txtToleransiLebih" runat="server"></asp:TextBox>
    <asp:TextBox ID="txtisAkhirBayar" runat="server"></asp:TextBox> <!-- True or False -->
</div>
<div class="form_box_usercontrol">
    <div class="form_quarter">
        <h5>
            ALOKASI PEMBAYARAN</h5>
    </div>
    <div class="form_quarter numberAlign">
        <h5>
            TAGIHAN JT TEMPO</h5>
    </div>
    <div class="form_quarter numberAlign">
        <h5>
            ALOKASI BAYAR</h5>
    </div>
    <div class="form_quarter">
        <h5>
            KETERANGAN</h5>
    </div>
</div>
<div class="form_box_usercontrol">
    <div class="form_quarter">
        <label class="label_auto">
            Angsuran</label>
    </div>
    <div class="form_quarter numberAlign">
        <asp:TextBox ID="txtInstallmentDue0" runat="server" CssClass="readonly_text numberAlign"
            ReadOnly="true"></asp:TextBox>
    </div>
    <div class="form_quarter numberAlign">
        <%-- <asp:TextBox ID="txtInstallmentDue" runat="server" Width="100%" onchange="calculateTotal();"></asp:TextBox>
        <asp:RequiredFieldValidator ID="reqInstallment" runat="server" ControlToValidate="txtInstallmentDue"
            ErrorMessage="Harap diisi Jumlahnya" Display="Dynamic" Visible="True" Enabled="True"
            CssClass="validator_general"></asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="regInstallment" runat="server" ControlToValidate="txtInstallmentDue"
            ErrorMessage="Harap diisi dengan Angka" Display="Dynamic" Enabled="True" ValidationExpression="^\d{1,}$|^\d{1,}[.{1}]\d{1,}$"
            Visible="True" CssClass="validator_general"></asp:RegularExpressionValidator>
        <asp:RangeValidator ID="rngInstallment" runat="server" Type="Double" ErrorMessage="<br>Amount Out Of Range"
            Display="Dynamic" ControlToValidate="txtInstallmentDue" MinimumValue="0" MaximumValue="999999"
            Visible="True" CssClass="validator_general"></asp:RangeValidator>--%>
        <uc:UcNumberFormat id="txtInstallmentDue" runat="server" OnChange="CekNilaiAngsuran(this.value);calculateTotal();"
            rangeValidatorEnable="false" RequiredFieldValidator="true" />
    </div>
    <div class="form_quarter">
        <asp:TextBox ID="txtInstallmentDueDesc" runat="server" Width="100%"></asp:TextBox>
    </div>
</div>

<div class="form_box_usercontrol">
    <div class="form_quarter">
        <label class="label_auto"> Angsuran Parsial</label>
    </div>
    <div class="form_quarter numberAlign">
        <asp:TextBox ID="txtTtpAngsuran0" runat="server" CssClass="readonly_text numberAlign" ReadOnly="true"></asp:TextBox>
    </div>
    <div class="form_quarter numberAlign"> 
        <uc:UcNumberFormat id="txtTtpAngsuran" runat="server" OnChange="calculateTotal();" rangeValidatorEnable="false" RequiredFieldValidator="true" />
    </div>
    <div class="form_quarter">
        <asp:TextBox ID="txtTtpAngsuranDesc" runat="server" Width="100%"></asp:TextBox>
    </div>
</div>



<div class="form_box_usercontrol">
    <div class="form_quarter">
        <label class="label_auto">
            Denda Keterlambatan</label>
    </div>
    <div class="form_quarter numberAlign">
        <asp:TextBox ID="txtInstallLC0" runat="server" CssClass="readonly_text numberAlign"></asp:TextBox>
    </div>
    <div class="form_quarter numberAlign">
        <%-- <asp:TextBox ID="txtInstallLC" runat="server" Width="100%" onchange="calculateTotal();"></asp:TextBox>
        <asp:RequiredFieldValidator ID="reqLCInstallment" runat="server" ControlToValidate="txtInstallLC"
            ErrorMessage="Harap diisi Jumlahnya" Display="Dynamic" Visible="True" Enabled="True"
            CssClass="validator_general"></asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="regLCInstallment" runat="server" ControlToValidate="txtInstallLC"
            ErrorMessage="Harap diisi dengan Angka" Display="Dynamic" Enabled="True" ValidationExpression="^\d{1,}$|^\d{1,}[.{1}]\d{1,}$"
            Visible="True" CssClass="validator_general"></asp:RegularExpressionValidator>
        <asp:RangeValidator ID="rngLCInstallment" runat="server" Type="Double" ErrorMessage="<br>Amount Out Of Range"
            Display="Dynamic" ControlToValidate="txtInstallLC" MinimumValue="0" MaximumValue="999999"
            Visible="True" CssClass="validator_general"></asp:RangeValidator>--%>
        <uc:UcNumberFormat id="txtInstallLC" runat="server" OnChange="calculateTotal();"
            rangeValidatorEnable="true" RequiredFieldValidator="true" />
    </div>
    <div class="form_quarter">
        <asp:TextBox ID="txtInstallLCDesc" runat="server" Width="100%"></asp:TextBox>
    </div>
</div>
<div class="form_box_usercontrol">
    <div class="form_quarter">
        <label class="label_auto">
            Biaya Tagih</label>
    </div>
    <div class="form_quarter numberAlign">
        <asp:TextBox ID="txtInstallCollFee0" runat="server" CssClass="readonly_text numberAlign"
            ReadOnly="true"></asp:TextBox>
    </div>
    <div class="form_quarter numberAlign">
        <%--<asp:TextBox ID="txtInstallCollFee" runat="server" Width="100%" onchange="calculateTotal();"></asp:TextBox>
        <asp:RequiredFieldValidator ID="reqInstallCollFee" runat="server" ControlToValidate="txtInstallCollFee"
            ErrorMessage="Harap diisi Jumlahnya" Display="Dynamic" Visible="True" Enabled="True"
            CssClass="validator_general"></asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="regInstallCollFee" runat="server" ControlToValidate="txtInstallCollFee"
            ErrorMessage="Harap diisi dengan Angka" Display="Dynamic" Enabled="True" ValidationExpression="^\d{1,}$|^\d{1,}[.{1}]\d{1,}$"
            Visible="True" CssClass="validator_general"></asp:RegularExpressionValidator>
        <asp:RangeValidator ID="rngInstallCollFee" runat="server" Type="Double" ErrorMessage="<br>Amount Out Of Range"
            Display="Dynamic" ControlToValidate="txtInstallCollFee" MinimumValue="0" MaximumValue="999999"
            Visible="True" CssClass="validator_general"></asp:RangeValidator>--%>
        <uc:UcNumberFormat id="txtInstallCollFee" runat="server" OnChange="calculateTotal();"
            rangeValidatorEnable="true" RequiredFieldValidator="true" />
    </div>
    <div class="form_quarter">
        <asp:TextBox ID="txtInstallCollFeeDesc" runat="server" Width="100%"></asp:TextBox>
    </div>
</div>
<div class="form_box_usercontrol">
    <div class="form_left_usercontrol">
        <a href="#" onclick="toggleVisibility('<%=panelMore.ClientId %>');">Alokasi Pembayaran
            Lainnya...</a>
    </div>
</div>
<asp:Panel ID="panelMore" runat="server" Style="display: none">
    <div class="form_box_usercontrol">
        <div class="form_quarter">
            <label class="label_auto">
                Premi Asuransi</label>
        </div>
        <div class="form_quarter numberAlign">
            <asp:TextBox ID="txtInsuranceDue0" runat="server" CssClass="readonly_text numberAlign"
                ReadOnly="true"></asp:TextBox>
        </div>
        <div class="form_quarter numberAlign">
            <%-- <asp:TextBox ID="txtInsuranceDue" runat="server" Width="100%" onchange="calculateTotal();"></asp:TextBox>
            <asp:RequiredFieldValidator ID="reqInsurance" runat="server" ControlToValidate="txtInsuranceDue"
                ErrorMessage="Harap diisi Jumlahnya" Display="Dynamic" Visible="True" Enabled="True"
                CssClass="validator_general"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="regInsurance" runat="server" ControlToValidate="txtInsuranceDue"
                ErrorMessage="Harap diisi dengan Angka" Display="Dynamic" Enabled="True" ValidationExpression="^\d{1,}$|^\d{1,}[.{1}]\d{1,}$"
                Visible="True" CssClass="validator_general"></asp:RegularExpressionValidator>
            <asp:RangeValidator ID="rngInsurance" runat="server" Type="Double" ErrorMessage="<br>Amount Out Of Range"
                Display="Dynamic" ControlToValidate="txtInsuranceDue" MinimumValue="0" MaximumValue="999999"
                Visible="True" CssClass="validator_general"></asp:RangeValidator>--%>
            <uc:UcNumberFormat id="txtInsuranceDue" runat="server" OnChange="calculateTotal();"
                rangeValidatorEnable="false" RequiredFieldValidator="true" />
        </div>
        <div class="form_quarter">
            <asp:TextBox ID="txtInsuranceDueDesc" runat="server" Width="100%"></asp:TextBox>
        </div>
    </div>
    <div class="form_box_usercontrol">
        <div class="form_quarter">
            <label class="label_auto">
                Denda Keterlambatan Asuransi</label>
        </div>
        <div class="form_quarter numberAlign">
            <asp:TextBox ID="txtInsuranceLC0" runat="server" CssClass="readonly_text numberAlign"
                ReadOnly="true"></asp:TextBox>
        </div>
        <div class="form_quarter numberAlign">
            <%-- <asp:TextBox ID="txtInsuranceLC" runat="server" Width="100%" onchange="calculateTotal();"></asp:TextBox>
            <asp:RequiredFieldValidator ID="reqLCInsurance" runat="server" ControlToValidate="txtInsuranceLC"
                ErrorMessage="Harap diisi Jumlahnya" Display="Dynamic" Visible="True" Enabled="True"
                CssClass="validator_general"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="regLCInsurance" runat="server" ControlToValidate="txtInsuranceLC"
                ErrorMessage="Harap diisi dengan Angka" Display="Dynamic" Enabled="True" ValidationExpression="^\d{1,}$|^\d{1,}[.{1}]\d{1,}$"
                Visible="True" CssClass="validator_general"></asp:RegularExpressionValidator>
            <asp:RangeValidator ID="rngLCInsurance" runat="server" Type="Double" ErrorMessage="<br>Amount Out Of Range"
                Display="Dynamic" ControlToValidate="txtInsuranceLC" MinimumValue="0" MaximumValue="999999"
                Visible="True" CssClass="validator_general"></asp:RangeValidator>--%>
            <uc:UcNumberFormat id="txtInsuranceLC" runat="server" OnChange="calculateTotal();"
                rangeValidatorEnable="true" RequiredFieldValidator="true" />

        </div>
        <div class="form_quarter">
            <asp:TextBox ID="txtInsuranceLCDesc" runat="server" Width="100%"></asp:TextBox>
        </div>
    </div>
    <div class="form_box_usercontrol">
        <div class="form_quarter">
            <label class="label_auto">
                Biaya Tagih Asuransi</label>
        </div>
        <div class="form_quarter numberAlign">
            <asp:TextBox ID="txtInsuranceCollFee0" runat="server" CssClass="readonly_text numberAlign"
                ReadOnly="true"></asp:TextBox>
        </div>
        <div class="form_quarter numberAlign">
            <%-- <asp:TextBox ID="txtInsuranceCollFee" runat="server" Width="100%" onchange="calculateTotal();"></asp:TextBox>
            <asp:RequiredFieldValidator ID="reqInsuranceCollFee" runat="server" ControlToValidate="txtInsuranceCollFee"
                ErrorMessage="Harap diisi Jumlahnya" Display="Dynamic" Visible="True" Enabled="True"
                CssClass="validator_general"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="regInsuranceCollFee" runat="server" ControlToValidate="txtInsuranceCollFee"
                ErrorMessage="Harap diisi dengan Angka" Display="Dynamic" Enabled="True" ValidationExpression="^\d{1,}$|^\d{1,}[.{1}]\d{1,}$"
                Visible="True" CssClass="validator_general"></asp:RegularExpressionValidator>
            <asp:RangeValidator ID="rngInsuranceCollFee" runat="server" Type="Double" ErrorMessage="<br>Amount Out Of Range"
                Display="Dynamic" ControlToValidate="txtInsuranceCollFee" MaximumValue="999999"
                MinimumValue="0" Visible="True" CssClass="validator_general"></asp:RangeValidator>--%>
            <uc:UcNumberFormat id="txtInsuranceCollFee" runat="server" OnChange="calculateTotal();"
                rangeValidatorEnable="true" RequiredFieldValidator="true" />
        </div>
        <div class="form_quarter">
            <asp:TextBox ID="txtInsuranceCollFeeDesc" runat="server" Width="100%"></asp:TextBox>
        </div>
    </div>
    <div class="form_box_usercontrol">
        <div class="form_quarter">
            <label class="label_auto">
                Biaya Tolakan PDC</label>
        </div>
        <div class="form_quarter numberAlign">
            <asp:TextBox ID="txtPDCBounceFee0" runat="server" CssClass="readonly_text numberAlign"
                ReadOnly="true"></asp:TextBox>
        </div>
        <div class="form_quarter numberAlign">
            <%-- <asp:TextBox ID="txtPDCBounceFee" runat="server" Width="100%" onchange="calculateTotal();"></asp:TextBox>
            <asp:RequiredFieldValidator ID="reqPDCBounceFee" runat="server" ControlToValidate="txtPDCBounceFee"
                ErrorMessage="Harap diisi Jumlahnya" Display="Dynamic" Visible="True" Enabled="True"
                CssClass="validator_general"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="regPDCBounceFee" runat="server" ControlToValidate="txtPDCBounceFee"
                ErrorMessage="Harap diisi dengan Angka" Display="Dynamic" Enabled="True" ValidationExpression="^\d{1,}$|^\d{1,}[.{1}]\d{1,}$"
                Visible="True" CssClass="validator_general"></asp:RegularExpressionValidator>
            <asp:RangeValidator ID="rngPDCBounceFee" runat="server" Type="Double" ErrorMessage="<br>Amount Out Of Range"
                Display="Dynamic" ControlToValidate="txtPDCBounceFee" MinimumValue="0" MaximumValue="999999"
                Visible="True" CssClass="validator_general"></asp:RangeValidator>--%>
            <uc:UcNumberFormat id="txtPDCBounceFee" runat="server" OnChange="calculateTotal();"
                rangeValidatorEnable="true" RequiredFieldValidator="true" />
        </div>
        <div class="form_quarter">
            <asp:TextBox ID="txtPDCBounceFeeDesc" runat="server" Width="100%"></asp:TextBox>
        </div>
    </div>
    <div class="form_box_usercontrol">
        <div class="form_quarter">
            <label class="label_auto">
                Biaya Perpanjangan STNK/BBN</label>
        </div>
        <div class="form_quarter numberAlign">
            <asp:TextBox ID="txtSTNKRenewalFee0" runat="server" CssClass="readonly_text numberAlign"
                 ReadOnly="true"></asp:TextBox>
        </div>
        <div class="form_quarter numberAlign">
            <%-- <asp:TextBox ID="txtSTNKRenewalFee" runat="server" Width="100%" onchange="calculateTotal();"></asp:TextBox>
            <asp:RequiredFieldValidator ID="reqSTNKRenewalFee" runat="server" ControlToValidate="txtSTNKRenewalFee"
                ErrorMessage="Harap diisi Jumlahnya" Display="Dynamic" Visible="True" Enabled="True"
                CssClass="validator_general"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="regSTNKRenewalFee" runat="server" ControlToValidate="txtSTNKRenewalFee"
                ErrorMessage="Harap diisi dengan Angka" Display="Dynamic" Enabled="True" ValidationExpression="^\d{1,}$|^\d{1,}[.{1}]\d{1,}$"
                Visible="True" CssClass="validator_general"></asp:RegularExpressionValidator>
            <asp:RangeValidator ID="rngSTNKRenewalFee" runat="server" Type="Double" ErrorMessage="<br>Amount Out Of Range"
                Display="Dynamic" ControlToValidate="txtSTNKRenewalFee" MinimumValue="0" MaximumValue="999999"
                Visible="True" CssClass="validator_general"></asp:RangeValidator>--%>
            <uc:UcNumberFormat id="txtSTNKRenewalFee" runat="server" OnChange="calculateTotal();"
                rangeValidatorEnable="true" RequiredFieldValidator="true" />
        </div>
        <div class="form_quarter">
            <asp:TextBox ID="txtSTNKRenewalFeeDesc" runat="server" Width="100%"></asp:TextBox>
        </div>
    </div>
    <div class="form_box_usercontrol">
        <div class="form_quarter">
            <label class="label_auto">
                Biaya Klaim Asuransi</label>
        </div>
        <div class="form_quarter numberAlign">
            <asp:TextBox ID="txtInsuranceClaimExpense0" runat="server" CssClass="readonly_text numberAlign"
                ReadOnly="true"></asp:TextBox>
        </div>
        <div class="form_quarter numberAlign">
            <%-- <asp:TextBox ID="txtInsuranceClaimExpense" runat="server" Width="100%" onchange="calculateTotal();"></asp:TextBox>
            <asp:RequiredFieldValidator ID="reqInsuranceClaimFee" runat="server" ControlToValidate="txtInsuranceClaimExpense"
                ErrorMessage="Harap diisi Jumlahnya" Display="Dynamic" Visible="True" Enabled="True"
                CssClass="validator_general"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="regInsuranceClaimFee" runat="server" ControlToValidate="txtInsuranceClaimExpense"
                ErrorMessage="Harap diisi dengan Angka" Display="Dynamic" Enabled="True" ValidationExpression="^\d{1,}$|^\d{1,}[.{1}]\d{1,}$"
                Visible="True" CssClass="validator_general"></asp:RegularExpressionValidator>
            <asp:RangeValidator ID="rngInsuranceClaimFee" runat="server" Type="Double" ErrorMessage="<br>Amount Out Of Range"
                Display="Dynamic" ControlToValidate="txtInsuranceClaimExpense" MaximumValue="999999"
                MinimumValue="0" Visible="True" CssClass="validator_general"></asp:RangeValidator>--%>
            <uc:UcNumberFormat id="txtInsuranceClaimExpense" runat="server" OnChange="calculateTotal();"
                rangeValidatorEnable="true" RequiredFieldValidator="true" />
        </div>
        <div class="form_quarter">
            <asp:TextBox ID="txtInsuranceClaimExpenseDesc" runat="server" Width="100%"></asp:TextBox>
        </div>
    </div>
    <div class="form_box_usercontrol">
        <div class="form_quarter">
            <label class="label_auto">
                Biaya Tarik</label>
        </div>
        <div class="form_quarter numberAlign">
            <asp:TextBox ID="txtRepossessionFee0" runat="server" CssClass="readonly_text numberAlign"
                ReadOnly="true"></asp:TextBox>
        </div>
        <div class="form_quarter numberAlign">
            <%--   <asp:TextBox ID="txtRepossessionFee" runat="server" Width="100%" onchange="calculateTotal();"></asp:TextBox>
            <asp:RequiredFieldValidator ID="reqRepossessionFee" runat="server" ControlToValidate="txtRepossessionFee"
                ErrorMessage="Harap diisi Jumlahnya" Display="Dynamic" Visible="True" Enabled="True"
                CssClass="validator_general"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="regRepossessionFee" runat="server" ControlToValidate="txtRepossessionFee"
                ErrorMessage="Harap diisi dengan Angka" Display="Dynamic" Enabled="True" ValidationExpression="^\d{1,}$|^\d{1,}[.{1}]\d{1,}$"
                Visible="True" CssClass="validator_general"></asp:RegularExpressionValidator>
            <asp:RangeValidator ID="rngRepossessionFee" runat="server" Type="Double" ErrorMessage="<br>Amount Out Of Range"
                Display="Dynamic" ControlToValidate="txtRepossessionFee" MaximumValue="999999"
                MinimumValue="0" Visible="True" CssClass="validator_general"></asp:RangeValidator>--%>
            <uc:UcNumberFormat id="txtRepossessionFee" runat="server" OnChange="calculateTotal();"
                rangeValidatorEnable="true" RequiredFieldValidator="true" />
        </div>
        <div class="form_quarter">
            <asp:TextBox ID="txtRepossessionFeeDesc" runat="server" Width="100%"></asp:TextBox>
        </div>
    </div>
    <div class="form_box_usercontrol">
        <div class="form_quarter">
            <label class="label_auto">
                Bunga Grace Period</label>
        </div>
        <div class="form_quarter numberAlign">
            <asp:TextBox ID="txtGracePeriod0" runat="server" CssClass="readonly_text numberAlign" ReadOnly = "true"></asp:TextBox>
        </div>
        <div class="form_quarter numberAlign">
            <uc:UcNumberFormat id="txtGracePeriod" runat="server" OnChange="calculateTotal();" />
        </div>
        <div class="form_quarter">
            <asp:TextBox ID="txtGracePeriodDesc" runat="server" Width="100%"></asp:TextBox>
        </div>
    </div>
    <div class="form_box_usercontrol">
        <div class="form_quarter">
            <label class="label_auto">
                Prepaid</label>
        </div>
        <div class="form_quarter numberAlign">
            <asp:TextBox ID="txtPrepaid0" runat="server" CssClass="readonly_text numberAlign"
                ReadOnly="true"></asp:TextBox>
        </div>
        <div class="form_quarter numberAlign">
            <%--  <asp:TextBox ID="txtPrepaid" runat="server" Width="100%" onchange="calculateTotal();"></asp:TextBox>
            <asp:RegularExpressionValidator ID="regPrepaidFee" runat="server" ControlToValidate="txtPrepaid"
                ErrorMessage="Harap diisi Jumlahnya" Display="Dynamic" Enabled="True" ValidationExpression="^\d{1,}$|^\d{1,}[.{1}]\d{1,}$"
                Visible="True" CssClass="validator_general"></asp:RegularExpressionValidator>--%>
            <uc:UcNumberFormat id="txtPrepaid" runat="server" OnChange="calculateTotal();" />
        </div>
        <div class="form_quarter">
            <asp:TextBox ID="txtPrepaidDesc" runat="server" Width="100%"></asp:TextBox>
        </div>
    </div>
    <div class="form_box_usercontrol">
        <div class="form_quarter">
            <label class="label_auto">
                Pendapatan Lain-Lain</label>
        </div>
        <div class="form_quarter numberAlign">
            <asp:TextBox ID="txtPLL0" runat="server" CssClass="readonly_text numberAlign" ReadOnly = "true"></asp:TextBox>
        </div>
        <div class="form_quarter numberAlign">
            <uc:UcNumberFormat id="txtPLL" runat="server" OnChange="calculateTotal();" />
        </div>
        <div class="form_quarter">
            <asp:TextBox ID="txtPLLDesc" runat="server" Width="100%"></asp:TextBox>
        </div>
    </div>
    
</asp:Panel>
<div class="form_box_usercontrol">
    <div class="form_quarter">
        <label class="label_auto">
            Total</label>
    </div>
    <div class="form_quarter numberAlign">
        <h5>
            <asp:Label ID="lblTotalTagihan" runat="server" CssClass="numberAlign"></asp:Label></h5>
    </div>
    <div class="form_quarter numberAlign">
        <h5>
            <asp:Label ID="lblTotalDiterima" runat="server" Text="0" CssClass="numberAlign"></asp:Label></h5>
    </div>
    <div class="form_quarter numberAlign" style="display:none">
        <asp:TextBox ID="txtRnd" runat="server" CssClass="numberAlign" Width="100%"></asp:TextBox>
    </div>
</div>
<asp:Panel ID="pnlKasir" runat="server">
    <div class="form_box_usercontrol">
        <div class="form_quarter"></div>
        <div class="form_quarter numberAlign">
            <label class="label_auto">
                Jumlah Uang Diterima</label>
        </div>
        <div class="form_quarter numberAlign">
            <%-- <asp:textbox ID="txtUangDiterima" runat="server" Text="0" onchange="calculateChange();" ></asp:textbox>--%>
            <uc:UcNumberFormat id="txtUangDiterima" runat="server" OnChange="calculateChange();"
                RequiredFieldValidator="true" />
        </div>
    </div>
    <div class="form_box_usercontrol">
        <div class="form_quarter"></div>
        <div class="form_quarter numberAlign">
            <label class="label_auto">
                Kembali</label>
        </div>
        <div class="form_quarter numberAlign">
            <h5>
                <asp:Label ID="lblKembali" runat="server" Text="0" CssClass="numberAlign"></asp:Label></h5>
        </div>
    </div>
    <div class="form_box_usercontrol">
        <div class="form_quarter"></div>
        <div class="form_quarter numberAlign">
            <label class="label_auto">
                Penyetor</label>
        </div>
        <div class="form_quarter numberAlign">
                <asp:textbox ID="txtPenyetor" runat="server" ></asp:textbox>
        </div>
    </div>

</asp:Panel>
