﻿Public Class ucSupplier
    Inherits System.Web.UI.UserControl
   

    Public Property SupplierID() As String
        Get
            Return hdnSupplierID.Value
        End Get
        Set(ByVal Value As String)
            hdnSupplierID.Value = Value
        End Set
    End Property

    Public Property SupplierName() As String
        Get
            Return txtSupplierName.Text
        End Get
        Set(ByVal Value As String)
            txtSupplierName.Text = Value
        End Set
    End Property

    Public Property PrivateSupplier() As String
        Get
            Return lblPF.Text
        End Get
        Set(ByVal Value As String)
            lblPF.Text = Value
        End Set
    End Property

    Public Property Style() As String
        Get
            Return CStr(viewstate("style"))
        End Get
        Set(ByVal Value As String)
            viewstate("style") = Value
        End Set
    End Property

    Public Property ApplicationID() As String
        Get
            Return hdnApplicationID.Value
        End Get
        Set(ByVal Value As String)
            hdnApplicationID.Value = Value
        End Set
    End Property

    Public Property hideLookup() As Boolean
        Get
            Return hpLookup.Visible
        End Get
        Set(ByVal Value As Boolean)
            hpLookup.Visible = Not Value
        End Set
    End Property

    Public Sub BindData()
        txtSupplierName.Attributes.Add("readonly", "true")
        hpLookup.NavigateUrl = "javascript:OpenWinSupplierLookup('" & hdnSupplierID.ClientID & "','" & txtSupplierName.ClientID & "','" & Me.Style & "','" & Me.ApplicationID & "','" & hdnPrivate.ClientID & "')"
    End Sub

End Class