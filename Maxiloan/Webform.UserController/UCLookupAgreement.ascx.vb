﻿Public Class UCLookupAgreement
    Inherits System.Web.UI.UserControl

    Public Property ApplicationID() As String
        Get
            Return hdnApplicationID.Value
        End Get
        Set(ByVal Value As String)
            hdnApplicationID.Value = Value
        End Set
    End Property

    Public Property AgreementNo() As String
        Get
            Return txtAgreementNo.Text
        End Get
        Set(ByVal Value As String)
            txtAgreementNo.Text = Value
        End Set
    End Property

    Public Property BranchAgreement() As String
        Get
            Return hdnBranchAgreement.Value
        End Get
        Set(ByVal Value As String)
            hdnBranchAgreement.Value = Value
        End Set
    End Property

    Public Property Style() As String
        Get
            Return CStr(ViewState("style"))
        End Get
        Set(ByVal Value As String)
            ViewState("style") = Value
        End Set
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        txtAgreementNo.Attributes.Add("readonly", "true")
        hpLookup.NavigateUrl = "javascript:OpenAgreement('" & txtAgreementNo.ClientID & "','" & hdnApplicationID.ClientID & "','" & hdnBranchAgreement.ClientID & "','" & Me.Style & "')"
    End Sub

    Public Sub BindData()
        hpLookup.NavigateUrl = "javascript:OpenAgreement('" & txtAgreementNo.ClientID & "','" & hdnApplicationID.ClientID & "','" & hdnBranchAgreement.ClientID & "','" & Me.Style & "')"
    End Sub


End Class