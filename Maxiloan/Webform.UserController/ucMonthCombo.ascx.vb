﻿Public Class ucMonthCombo
    Inherits System.Web.UI.UserControl

    Public Property SelectedMonth As Integer
        Get
            Return CInt(cboMonth.SelectedValue)
        End Get
        Set(ByVal value As Integer)
            cboMonth.SelectedValue = value.ToString
        End Set
    End Property
    Public Property AutoPostBack As Boolean
        Get
            Return cboMonth.AutoPostBack
        End Get
        Set(ByVal Value As Boolean)
            cboMonth.AutoPostBack = Value
        End Set
    End Property

    Public Event SelectedIndexChanged()

    Private Sub cboMonth_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboMonth.SelectedIndexChanged
        RaiseEvent SelectedIndexChanged()
    End Sub

    Public Sub FeatureEnabled(ByVal status As Boolean)
        If status = True Then
            cboMonth.Enabled = True
        Else
            cboMonth.Enabled = False
        End If
    End Sub

End Class