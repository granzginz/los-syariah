﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucLookUpCustomer.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.ucLookUpCustomer" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%--<style type="text/css">
    .wp
    {
        background-color: #fff;
        width: 700px;        
        border: 1px solid #ccc;
        float: left;
    }
    
    .wp h4
    {
        margin: 0;
        color: #000;
        text-align: left;
    }
    
    .wp .gridwp
    {
        max-height: 300px;
        overflow: auto;
    }
    
    .wpbg
    {
        background-color: #000;
        filter: alpha(opacity=70);
        opacity: 0.7;
    }
    
    .wpbtnexit
    {
        position: absolute;
        float: right;
        top: 0;
        right: 0;
    }
</style>--%>
<asp:UpdatePanel runat="server" ID="updatePanel1">
    <ContentTemplate>
        <asp:Button runat="server" ID="btnLookup" Text="btnLookup" Style="display: none;" />
        <asp:ModalPopupExtender ID="ModalPopupExtender" runat="server" TargetControlID="btnLookup"
            PopupControlID="Panel1" BackgroundCssClass="wpbg" CancelControlID="btnExit">
        </asp:ModalPopupExtender>
        <asp:Panel ID="Panel1" runat="server">
            <div class="wp">
                <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtGoPage" MinimumValue="1"
                    MaximumValue="999" ErrorMessage="Page No. is not valid" Type="Double"></asp:RangeValidator>
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
                <asp:ImageButton runat="server" ID="btnExit" ImageUrl="../Images/exit_lookup00.png"
                    CssClass="wpbtnexit" />
                <div class="form_box_title">
                    <div class="title_strip">
                    </div>
                    <div class="form_single">
                        <h4>
                            DAFTAR CUSTOMER
                        </h4>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ws">
                            <%--<asp:DataGrid ID="dtgPaging" runat="server" CellPadding="0" OnSortCommand="SortGrid"
                                BackColor="White" BorderWidth="1px" BorderStyle="None" BorderColor="#CCCCCC"
                                DataKeyField="CustomerID" AutoGenerateColumns="False" AllowSorting="True" AllowPaging="True"
                                Width="000" Visible="True" CssClass="gridwp">--%>
                            <asp:DataGrid ID="dtgPaging" runat="server" OnSortCommand="SortGrid" BorderStyle="None"
                                    DataKeyField="CustomerID" AutoGenerateColumns="False" AllowSorting="True"
                                    AllowPaging="True" Visible="true" Width="100%" CssClass="gridwp">
                                <ItemStyle CssClass="item_grid"></ItemStyle>
                                <HeaderStyle CssClass="th"></HeaderStyle>
                                <Columns>
                                    <asp:ButtonColumn Text="SELECT" CommandName="Select" ButtonType="LinkButton" ItemStyle-CssClass="command_col">
                                    </asp:ButtonColumn>
                                    <asp:BoundColumn ReadOnly="True" DataField="Name" SortExpression="Name" HeaderText="CUSTOMER NAME"
                                        ItemStyle-CssClass="name_col"></asp:BoundColumn>
                                    <asp:BoundColumn ReadOnly="True" DataField="Address" SortExpression="Address" HeaderText="CUSTOMER ADDRESS">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn ReadOnly="True" DataField="BadType" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn ReadOnly="True" DataField="IDType" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn ReadOnly="True" DataField="IDTypeDescription" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn ReadOnly="True" DataField="IDNumber" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn ReadOnly="True" DataField="BirthPlace" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn ReadOnly="True" DataField="BirthDate" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn ReadOnly="True" DataField="CustomerType" Visible="false"></asp:BoundColumn>
                                </Columns>
                                <PagerStyle Visible="False" HorizontalAlign="Left" ForeColor="#000066" BackColor="White"
                                    Mode="NumericPages"></PagerStyle>
                            </asp:DataGrid>
                        </div>
                        <div class="grid_wrapper_ns">
                            <div class="button_gridnavigation">
                                <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" ImageUrl="../Images/grid_navbutton01.png"
                                    OnCommand="NavigationLink_Click" CommandName="First"></asp:ImageButton>
                                <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" ImageUrl="../Images/grid_navbutton02.png"
                                    OnCommand="NavigationLink_Click" CommandName="Prev"></asp:ImageButton>
                                <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" ImageUrl="../Images/grid_navbutton03.png"
                                    OnCommand="NavigationLink_Click" CommandName="Next"></asp:ImageButton>
                                <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" ImageUrl="../Images/grid_navbutton04.png"
                                    OnCommand="NavigationLink_Click" CommandName="Last"></asp:ImageButton>
                                Page
                                <asp:TextBox ID="txtGoPage" runat="server" Width="34px">1</asp:TextBox>
                                <asp:Button ID="btnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                                    EnableViewState="False" />
                                <asp:RangeValidator ID="Rangevalidator1" runat="server" Type="Integer" MaximumValue="999999999"
                                    ErrorMessage="Page No. is not valid" MinimumValue="1" ControlToValidate="txtGoPage"></asp:RangeValidator>
                                <asp:RequiredFieldValidator ID="rfvGo" runat="server" ErrorMessage="Page No. is not valid"
                                    ControlToValidate="txtGoPage" Display="Dynamic" CssClass ="validator_general"></asp:RequiredFieldValidator>
                            </div>
                            <div class="label_gridnavigation">
                                <asp:Label ID="lblPage" runat="server"></asp:Label>of
                                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                                <asp:Label ID="lblTotRec" runat="server"></asp:Label>record(s)
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_auto" >
                            Find By</label>
                        <asp:DropDownList ID="cboSearchBy" runat="server">
                            <asp:ListItem Value="CustomerName">Customer Name</asp:ListItem>
                            <asp:ListItem Value="CustomerAddress">Customer Address</asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtSearchBy" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="btnSearch" runat="server" CausesValidation="False" Text="Find" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="btnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
                    </asp:Button>
                </div>
            </div>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
