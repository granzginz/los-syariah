﻿Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
Imports Maxiloan.General.CommonCacheHelper

Public Class Reports
    Inherits Maxiloan.Webform.WebBased
    Private m_controller As New DataUserControlController
    Private oAssetDataController As New AssetDataController
    Private oClass As New Parameter.ControlsRS

#Region " Private Const "
    Dim m_Company As New FundingCompanyController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private lnkContractNo As LinkButton

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack Then

            Dim strReportFile As String = "../Webform.UserController/ucRSViewerPopup.aspx?formid=" & Request("formid") & "&rsname=" & Request("rsname")

            Response.Write("<script language = javascript>" & vbCrLf _
            & "var x = screen.width; " & vbCrLf _
            & "var y = screen.height; " & vbCrLf _
            & "window.open('" & strReportFile & "','_blank', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes') " & vbCrLf _
            & "</script>")
        End If

        If Not Page.IsPostBack Then
            Me.FormID = Request("formid")

            bindBranch()

            Dim WhereBranch As String = "BranchID IN (" & selectedBranch() & ")"

            If Me.FormID = "HASILCRDSCOR" Then bindNoAplikasi()
            If Me.FormID = "LAPHASILKUNCOLL" Then bindCollectorType_CL()
            If Me.FormID = "LAPKEGDESKCOLL" Then bindCollectorType_D()
            If Me.FormID = "LAPTUNGGCA" Then FillCboEmp("BranchEmployee", cboCreditAnalist, WhereBranch + " and (EmployeePosition='CA' or EmployeePosition='AO') ")
            If Me.FormID = "LAPTUNGGCMO" Then FillCboEmp("BranchEmployee", cboCMO, WhereBranch + " and (EmployeePosition='AO') ")
            If Me.FormID = "LAPTUNGGSVY" Then FillCboEmp("BranchEmployee", cboCMO, WhereBranch + " and (EmployeePosition='SVY' or EmployeePosition='AO') ")

            If Me.FormID = "DAFTRANREKBANK" Then bindBankAccountbyType("B")
            If Me.FormID = "DAFTRANREKTUNAI" Then bindBankAccountbyType("C")

            If Me.FormID = "LAPTRANHARIKASIR" Then bindKasir()
            If Me.FormID = "LAPTRANKASBANK" Then bindBankAccount()
            If Me.FormID = "DAFJAMTPPOLIS" Or Me.FormID = "ANGSBANKDTL" Then bindComboCompany()
            If Me.FormID = "DAFJAMTPBPKB" Or Me.FormID = "DAFJAMTPPOLIS""" Then bindComboCompany()
            If Me.FormID = "TANDATERIMAJAM" Or Me.FormID = "DAFANGSBANKBATCH" Then
                bindComboCompany()
                bindComboContract()
                bindComboBatch()
            End If
            If Me.FormID = "DAFANGSBANKKONS" Then
                bindComboCompany()
                bindComboContract()
                bindComboBatch()
                bindNamaKonsumen()
            End If
            If Me.FormID = "DAFTERIMAPOLIS" Or Me.FormID = "DAFTANGAS" Then bindChkInsuranceCompany()
            If Me.FormID = "LAPHUTANGAS" Then bindChkInsuranceCompany()
            If Me.FormID = "LAPTAGAS" Then bindcboInsuranceCompany()
            If Me.FormID = "DAFJURNALDTL" Or Me.FormID = "DAFJURNALSUM" Then bindTransactionType()
            If Me.FormID = "HITUNGDENDA" Then bindNoKontrak()


            oClass.isHOBRanch = Me.IsHoBranch
            oClass.FormID = Me.FormID

            oClass = RSConfig.setControls(oClass)

            With oClass
                'dvBranch.Visible = .isFilterMultiBranch
                dvBranch.Visible = .isFilterBranch
                dvDateRange.Visible = .isFilterRangeDate
                dvSingleDate.Visible = .isFilterSingleDate
                dvKondisiSKT.Visible = .isFilterKondisiSKT
                dvPeriodLapARODStatCMO.Visible = .isFilterPeriodLapARODStatCMO
                dvLmNunggak.Visible = .isFilterLmNunggak
                dvCollectorCL.Visible = .isFilterCollectorCL
                dvCollectorD.Visible = .isFilterCollectorD
                dvStatus.Visible = .isFilterStatus
                dvCreditAnalist.Visible = .isFilterCreditAnalist
                dvCMO.Visible = .isFilterCMO
                dvSurveyor.Visible = .isFilterSurveyor
                dvRekBankType_B.Visible = .isFilterRekeningBankType_B
                dvRekBankType_C.Visible = .isFilterRekeningBankType_C
                dvBankAccount.Visible = .isFilterRekeningBankAccount
                dvChkColl_CL.Visible = .isFilterChkColl_CL
                dvKasir.Visible = .isFilterKasir
                dvBankCompany.Visible = .isFilterBankCompany
                dvKontrak.Visible = .isFilterKontrak
                dvNoBatch.Visible = .isFilterNoBatch
                dvNamaKonsumen.Visible = .isFilterNamaKonsumen
                dvchkInsuranceCompany.Visible = .isFilterCabangAsuransi
                dvStatusPolis.Visible = .isFilterStatusPolis
                dvYear.Visible = .isFilterYear
                dvTransactionType.Visible = .isFilterTransactionType
                dvNoKontrak.Visible = .isFilterNoKontrak
                dvMonth.Visible = .isFilterMonth
                dvPilihan.Visible = .isFilterPilihan
                dvInvoice.Visible = .isFilterInvoice
                dvNoAplikasi.Visible = .isFilterNoAplikasi
                dvInsuranceCompany.Visible = .isFilterCabangAsuransi
            End With

        End If

    End Sub

    Private Sub bindBranch()

        Dim DtBranchName As New DataTable
        DtBranchName = CType(Me.Cache.Item(CACHE_BRANCH_ALL), DataTable)
        If DtBranchName Is Nothing Then
            Dim DtBranchNameCache As New DataTable
            DtBranchNameCache = m_controller.GetBranchAll(GetConnectionString)
            Me.Cache.Insert(CACHE_BRANCH_ALL, DtBranchNameCache, Nothing, DateTime.Now.AddHours(DEFAULT_TIME_DURATION_CACHE_HOUR), TimeSpan.Zero)
            DtBranchName = CType(Me.Cache.Item(CACHE_BRANCH_ALL), DataTable)
        End If

        With chkBranch
            .DataTextField = "Name"
            .DataValueField = "ID"
            .DataSource = DtBranchName
            .DataBind()
        End With

    End Sub

    Private Sub bindChkColl()
        Dim oClass As New Parameter.ControlsRS
        With oClass
            .strConnection = GetConnectionString()
            .BranchId = selectedBranch()
        End With
        oClass = m_controller.GetCollectorReportLapHasilKunColl(oClass)
        Dim dt As DataTable
        dt = oClass.ListData

        With chkStatus
            .DataTextField = "NamaCollector"
            .DataValueField = "CollectorID"
            .DataSource = oClass
            .DataBind()
        End With
    End Sub

    ' GetInsuranceCompanyBranchALL
    Private Sub bindChkInsuranceCompany()
        Dim oClass As New Parameter.ControlsRS
        With oClass
            .strConnection = GetConnectionString()
            .BranchId = selectedBranch()
        End With
        oClass = m_controller.GetInsuranceCompanyBranchALL(oClass)
        Dim dt As DataTable
        dt = oClass.ListData

        With chkInsuranceCompany
            .DataTextField = "Name"
            .DataValueField = "ChildID"
            .DataSource = dt
            .DataBind()
        End With
    End Sub
    Private Sub bindcboInsuranceCompany()
        Dim oClass As New Parameter.ControlsRS
        With oClass
            .strConnection = GetConnectionString()
            .BranchId = selectedBranch()
        End With
        oClass = m_controller.GetInsuranceCompanyBranchALL(oClass)
        Dim dt As DataTable
        dt = oClass.ListData

        With cboInsuranceCompany
            .DataTextField = "Name"
            .DataValueField = "ChildID"
            .DataSource = dt
            .DataBind()
        End With
    End Sub


    Private Sub bindCollectorType_CL()

        Dim oClass As New Parameter.ControlsRS
        With oClass
            .strConnection = GetConnectionString()
            .BranchId = selectedBranch()
        End With

        oClass = m_controller.GetCollectorReportLapHasilKunColl(oClass)
        Dim dt As New DataTable
        dt = oClass.ListData

        cboCollectorCL.DataSource = dt
        cboCollectorCL.DataTextField = "NamaCollector"
        cboCollectorCL.DataValueField = "CollectorID"
        cboCollectorCL.DataBind()
        cboCollectorCL.Items.Insert(0, "Select One")
        cboCollectorCL.Items(0).Value = "0"

    End Sub


    Private Sub bindCollectorType_D()

        Dim oClass As New Parameter.ControlsRS

        With oClass
            .strConnection = GetConnectionString()
            .BranchId = selectedBranch()
        End With
        'spGetCollectorReportLapKegDeskColl
        oClass = m_controller.GetCollectorReportLapKegDeskColl(oClass)

        Dim dt As New DataTable
        dt = oClass.ListData

        cboCollectorD.DataSource = dt
        cboCollectorD.DataTextField = "NamaCollector"
        cboCollectorD.DataValueField = "CollectorID"
        cboCollectorD.DataBind()
        cboCollectorD.Items.Insert(0, "Select One")
        cboCollectorD.Items(0).Value = "0"

    End Sub

    '[spGetReportEmployeeLapTunggCMO]
    Protected Sub FillCboEmp(ByVal Table As String, ByVal cboName As DropDownList, ByVal Where As String)
        Dim oAssetData As New Parameter.AssetData
        Dim oData As New DataTable

        oAssetData.strConnection = GetConnectionString()
        oAssetData.Where = Where
        oAssetData.Table = Table
        oAssetData = oAssetDataController.GetCboEmp(oAssetData)

        oData = oAssetData.ListData
        cboName.DataSource = oData
        cboName.DataTextField = "Name"
        cboName.DataValueField = "ID"
        cboName.DataBind()
        cboName.Items.Insert(0, "Select One")
        cboName.Items(0).Value = "Select One"
    End Sub

    Private Sub bindBankAccountbyType(ByVal strType As String)
        'spGetReportDafTranRekBank()       
        cboRekBankType_B.DataSource = m_controller.GetBankAccount(GetConnectionString, selectedBranch, strType, "")
        cboRekBankType_B.DataTextField = "Name"
        cboRekBankType_B.DataValueField = "ID"
        cboRekBankType_B.DataBind()
        cboRekBankType_B.Items.Insert(0, "Select One")
        cboRekBankType_B.Items(0).Value = "0"
    End Sub
    
    Private Sub bindBankAccount()
        'spGetReportBankAccount
        'spGetReportDafTranRekBank()

        cboBankAccount.DataSource = m_controller.GetBankAccountbranch(GetConnectionString(), selectedBranch())
        cboBankAccount.DataTextField = "BankAccountName"
        cboBankAccount.DataValueField = "BankAccountID"
        cboBankAccount.DataBind()
        cboBankAccount.Items.Insert(0, "Select One")
        cboBankAccount.Items(0).Value = "0"
    End Sub

    Private Sub bindKasir()
        Dim oClass As New Parameter.ControlsRS
        With oClass
            .strConnection = GetConnectionString()
            .BranchId = selectedBranch()
        End With
        'spGetReportDafTranRekBank()
        oClass = m_controller.GetReportKasir(oClass)
        Dim dt As New DataTable
        dt = oClass.ListData

        cboRekBankType_C.DataSource = dt
        cboRekBankType_C.DataTextField = "EmployeeName"
        cboRekBankType_C.DataValueField = "LoginId"
        cboRekBankType_C.DataBind()
        cboRekBankType_C.Items.Insert(0, "Select One")
        cboRekBankType_C.Items(0).Value = "0"
    End Sub

    Private Sub bindComboCompany()
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim customClassx As New GeneralPagingController
        Dim customClass As New Parameter.GeneralPaging

        With customClass
            .strConnection = GetConnectionString()
            .WhereCond = ""
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = ""
            .SpName = "spFundingGetComboCompany"
        End With
        customClass = customClassx.GetGeneralPaging(customClass)

        dtsEntity = customClass.ListData
        dtvEntity = dtsEntity.DefaultView
        cboBankCompany.DataValueField = "FundingCoyId"
        cboBankCompany.DataTextField = "FundingCoyName"
        cboBankCompany.DataSource = dtvEntity
        cboBankCompany.DataBind()
    End Sub

    Sub bindComboContract()
        Dim oClass As New Parameter.ControlsRS
        With oClass
            .strConnection = GetConnectionString()
            .CoyID = cboBankCompany.SelectedValue
        End With
        oClass = m_controller.GetReportComboContract(oClass)
        Dim dt As New DataTable
        dt = oClass.ListData
        cboKontrak.DataSource = dt
        cboKontrak.DataTextField = "NamaKontrak"
        cboKontrak.DataValueField = "FundingContractNo"
        cboKontrak.DataBind()
    End Sub

    Sub bindComboBatch()
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim customClassx As New GeneralPagingController
        Dim customClass As New Parameter.GeneralPaging
        With customClass
            .strConnection = GetConnectionString()
            .WhereCond = "WHERE fundingCoyID = '" & cboBankCompany.SelectedValue & "' AND FundingContractNo = '" & cboKontrak.SelectedValue & "' "
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = ""
            .SpName = "spGetReportComboBatch"
        End With

        customClass = customClassx.GetGeneralPaging(customClass)

        dtsEntity = customClass.ListData
        dtvEntity = dtsEntity.DefaultView
        cboNoBatch.DataValueField = "FundingBatchNo"
        cboNoBatch.DataTextField = "FundingBatchNo"
        cboNoBatch.DataSource = dtvEntity
        cboNoBatch.DataBind()
        'cboNoBatch.Items.Insert(0, "All")
        'cboNoBatch.Items(0).Value = "All"
    End Sub

    Sub bindNamaKonsumen()
        Dim oClass As New Parameter.ControlsRS
        With oClass
            .strConnection = GetConnectionString()
            .Kontrak = cboKontrak.SelectedValue
            .NoBatch = cboNoBatch.SelectedValue
        End With
        oClass = m_controller.GetReportNamaKonsumen(oClass)

        Dim dt As DataTable
        dt = oClass.ListData
        With cboNamaKonsumen
            .DataSource = dt
            .DataTextField = "NamaKonsumen"
            .DataValueField = "ApplicationID"
            .DataBind()
        End With
    End Sub

    Private Sub bindTransactionType()
        Dim oClass As New Parameter.ControlsRS
        With oClass
            .strConnection = GetConnectionString()
        End With
        oClass = m_controller.GetReportTransactionType(oClass)

        Dim dt As DataTable
        dt = oClass.ListData

        With chkTransactionType
            .DataTextField = "TransactionType"
            .DataValueField = "transactionid"
            .DataSource = dt
            .DataBind()
        End With
    End Sub

    'bind AgreementNo
    Private Sub bindNoKontrak()
        Dim oClass As New Parameter.ControlsRS
        With oClass
            .strConnection = GetConnectionString()
            .BranchId = selectedBranch()
        End With
        oClass = m_controller.GetReportNoKontrak(oClass)

        Dim dt As DataTable
        dt = oClass.ListData

        With cboNoKontrak
            .DataSource = dt
            .DataTextField = "NoKontrak"
            .DataValueField = "ApplicationID"
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = "0"
        End With

    End Sub
    Private Sub bindNoAplikasi()

        Dim oClass As New Parameter.ControlsRS
        With oClass
            .strConnection = GetConnectionString()
            .BranchId = Me.sesBranchId.Replace("'", "")
        End With
        oClass = m_controller.GetReportNoAplikasi(oClass)
        Dim dt As New DataTable
        dt = oClass.ListData
        With cboNoAplikasi
            .DataSource = dt
            .DataTextField = "NoNama"
            .DataValueField = "ApplicationID"
            .DataBind()
        End With
    End Sub

    Private Sub btnView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnView.Click
        With oClass
            .SelectedBranch = selectedBranch()
            .StartDate = ConvertDate2(txtStartDate.Text).ToString("MM/dd/yyyy")
            .EndDate = ConvertDate2(txtEndDate.Text).ToString("MM/dd/yyyy")
            .KondisiSKT = cboKondisiSKT.SelectedValue
            .StartMonth = cboStartMonth.SelectedValue
            .EndMonth = cboEndMonth.SelectedValue
            .StartYear = txtStartYear.Text
            .EndYear = txtEndYear.Text
            .StartYear2 = txtlnstart.Text
            .EndYear2 = txtlnend.Text
            .CollectorType_D = cboCollectorD.SelectedValue
            .CollectorType_CL = cboCollectorCL.SelectedValue
            .JenisTask = cboJenisTask.SelectedValue
            .StatusRepo = selectedStatusRepo()
            .CreditAnalist = cboCreditAnalist.SelectedValue
            .CMO = cboCMO.SelectedValue
            .Surveyor = cboSurveyor.SelectedValue
            .RekeningBankkType_B = cboRekBankType_B.SelectedValue
            .RekeningBankkType_C = cboRekBankType_C.SelectedValue
            .ChkColl_CL = chkColl.SelectedValue
            .Kasir = cboKasir.SelectedValue
            .RekeningBankAccount = cboBankAccount.SelectedValue
            .BankCompany = cboBankCompany.SelectedValue
            .Kontrak = cboKontrak.SelectedValue
            .NoBatch = cboNoBatch.SelectedValue
            .NamaKonsumen = cboNamaKonsumen.SelectedValue
            .CabangAsuransi = cboInsuranceCompany.SelectedValue
            .StatusPolis = cboStatusPolis.SelectedValue
            .Year = txtYear.Text
            .Month = cboMonth.SelectedValue
            .Invoice = txtInvoice.Text
            .TransactionType = chkTransactionType.SelectedValue
            .NoKontrak = cboNoKontrak.SelectedValue
            .Pilihan = chkPilihan.SelectedValue
            .NoAplikasi = cboNoAplikasi.SelectedValue

        End With

        Response.AppendCookie(RSConfig.setCookies(Request.Cookies("RSCOOKIES"), oClass))
    End Sub

    Private Function selectedBranch() As String
        If IsHoBranch Then
            Dim strID As New StringBuilder

            For Each oItem As ListItem In chkBranch.Items
                If oItem.Selected Then
                    strID.Append(IIf(strID.ToString = "", oItem.Value, "," & oItem.Value))
                End If
            Next
            Return strID.ToString
        Else
            Return Replace(Me.sesBranchId, "'", "")
        End If
    End Function


    Private Function selectedStatusRepo() As String
        If IsHoBranch Then
            Dim strID As New StringBuilder

            For Each oItem As ListItem In chkStatus.Items
                If oItem.Selected Then
                    strID.Append(IIf(strID.ToString = "", oItem.Value, "," & oItem.Value))
                End If
            Next
            Return strID.ToString
        Else
            Return Replace(Me.sesBranchId, "'", "")
        End If
    End Function

    Private Sub cboBankCompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBankCompany.SelectedIndexChanged
        bindComboContract()
        bindComboBatch()
        bindNamaKonsumen()
    End Sub


End Class