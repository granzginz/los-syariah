﻿

Public Class ucApprovalRequest
    Inherits AccMntControlBased

    Public Property Notes() As String
        Get
            Return txtNotes.Text.Trim
        End Get
        Set(ByVal Value As String)
            txtNotes.Text = Value
        End Set
    End Property
    Public Property ReasonID() As String
        Get
            Return oReason.ReasonID
        End Get
        Set(ByVal Value As String)
            oReason.ReasonID = Value
        End Set
    End Property

    Public Property ReasonTypeID() As String
        Get
            Return oReason.ReasonTypeID
        End Get
        Set(ByVal Value As String)
            oReason.ReasonTypeID = Value
        End Set
    End Property

    Public Property ReasonName() As String
        Get
            Return oReason.Description
        End Get
        Set(ByVal Value As String)
            oReason.Description = value
        End Set
    End Property
    Public Property ToBeApprove() As String
        Get
            Return cboApprovedBy.SelectedValue
        End Get
        Set(ByVal Value As String)
            cboApprovedBy.SelectedIndex = cboApprovedBy.Items.IndexOf(cboApprovedBy.Items.FindByText(Value))
        End Set
    End Property

    Public Property ApprovalScheme() As String
        Get
            Return CStr(viewstate("ApprovalScheme"))
        End Get
        Set(ByVal Value As String)
            viewstate("ApprovalScheme") = value
        End Set
    End Property
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not IsPostBack Then
            oReason.ReasonTypeID = Me.ReasonTypeID
            oReason.BindReason()

            cboApprovedBy.DataSource = Me.Get_UserApproval(Me.ApprovalSCheme, Me.sesBranchId.Replace("'", ""))
            cboApprovedBy.DataTextField = "Name"
            cboApprovedBy.DataValueField = "ID"
            cboApprovedBy.DataBind()
            cboApprovedBy.Items.Insert(0, "Select One")
            cboApprovedBy.Items(0).Value = "0"
        End If
    End Sub


End Class