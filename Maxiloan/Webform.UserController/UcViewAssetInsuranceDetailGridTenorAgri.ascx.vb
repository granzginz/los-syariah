﻿#Region "Imports"
Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.General.CommonVariableHelper
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller

Imports System.Text
#End Region

Public Class UcViewAssetInsuranceDetailGridTenorAgri
    Inherits ControlBased

    Private oController As New SPPAController

    Public Sub BindData()
        Dim ParamApplicationId As String = CType(Me.Cache.Item(CACHE_VIEW_INSURANCE_DETAIL_PARAMETER_APPLICATION_ID), String)
        Dim ParamBranchID As String = CType(Me.Cache.Item(CACHE_VIEW_INSURANCE_DETAIL_PARAMETER_BRANCH_ID), String)
        ' Dim CmdWhere As String = " Where dbo.InsuranceAssetDetail.BranchId ='" & Replace(ParamBranchID, "'", "") & "' And dbo.InsuranceAssetDetail.ApplicationID= '" & Replace(ParamApplicationId, "'", "") & "' "
        Dim cmdwherebuilder As New StringBuilder
        cmdwherebuilder.Append("Where dbo.InsuranceAssetDetail.BranchId ='" & Replace(ParamBranchID, "'", "") & "' And dbo.InsuranceAssetDetail.ApplicationID= '" & Replace(ParamApplicationId, "'", "") & "' ")
        cmdwherebuilder.Append(" AND dbo.InsuranceAssetDetail.AssetSeqNo = (select dbo.AmbilAssetSequencenoInsuranceAsset('" & Replace(ParamBranchID, "'", "") & "','" & Replace(ParamApplicationId, "'", "") & "'))  ")
        cmdwherebuilder.Append(" And dbo.InsuranceAssetDetail.InsSequenceNo = (select dbo.AmbilInsSequencenoInsuranceAsset('" & Replace(ParamBranchID, "'", "") & "','" & Replace(ParamApplicationId, "'", "") & "')) ")
        Dim oEntities As New Parameter.SPPA
        With oEntities
            '.WhereCond = CmdWhere
            .WhereCond = cmdwherebuilder.ToString
            .PageSource = "PAGE_SOURCE_GRID_TENOR_VIEW_INSURANCE_ASSET_DETAIL_AGRI"
            .strConnection = GetConnectionString()
        End With

        Context.Trace.Write("CmdWhere = " + cmdwherebuilder.ToString)
        Try
            oEntities = oController.GetListCreateSPPA(oEntities)
        Catch ex As Exception
            Response.Write(ex.Message)
            Dim err As New MaxiloanExceptions
            err.WriteLog("UcViewAssetInsuranceDetailGridTenorAgri.ascx", "Page_Load", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
        End Try

        Dim dtEntity As New DataTable

        If Not oEntities Is Nothing Then
            dtEntity = oEntities.ListData
        End If

        DgridTenor.DataSource = dtEntity
        DgridTenor.DataBind()
    End Sub
End Class