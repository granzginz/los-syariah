﻿Public Class ucHasilSurveyViewTabPhone
    Inherits System.Web.UI.UserControl


#Region "Property"
    Public Property AreaPhoneHome() As String
        Get
            Return CType(ViewState("AreaPhoneHome"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("AreaPhoneHome") = Value
        End Set
    End Property
    Public Property AreaPhoneOffice() As String
        Get
            Return CType(ViewState("AreaPhoneOffice"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("AreaPhoneOffice") = Value
        End Set
    End Property
    Public Property PhoneHome() As String
        Get
            Return CType(ViewState("PhoneHome"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("PhoneHome") = Value
        End Set
    End Property
    Public Property PhoneOffice() As String
        Get
            Return CType(ViewState("PhoneOffice"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("PhoneOffice") = Value
        End Set
    End Property
    Public Property Handphone() As String
        Get
            Return CType(ViewState("Handphone"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Handphone") = Value
        End Set
    End Property
    Public Property EmergencyContact() As String
        Get
            Return CType(ViewState("EmergencyContact"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("EmergencyContact") = Value
        End Set
    End Property
    Public Property Style() As String
        Get
            Return CType(ViewState("Style"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Style") = Value
        End Set
    End Property
#End Region


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not IsPostBack Then
            LblAreaPhoneHome.Text = Me.AreaPhoneHome
            LblAreaPhoneOffice.Text = Me.AreaPhoneOffice
            LblPhoneHome.Text = Me.PhoneHome
            LblPhoneOffice.Text = Me.PhoneOffice
            LblHandphone.Text = Me.Handphone
            LblEmergencyPhone.Text = Me.EmergencyContact
        Else
            Me.AreaPhoneHome = LblAreaPhoneHome.Text
            Me.AreaPhoneOffice = LblAreaPhoneOffice.Text
            Me.PhoneHome = LblPhoneHome.Text
            Me.PhoneOffice = LblPhoneOffice.Text
            Me.Handphone = LblHandphone.Text
            Me.EmergencyContact = LblEmergencyPhone.Text
        End If
    End Sub

    Public Sub showMandatoryAll()
        'bintang1.Visible = True
        'bintang2.Visible = True
        'lblAlamat.Attributes("class") = "label_split_req"
        lblTelepon1.Attributes("class") = "label_split_req"
    End Sub

    Public Sub hideMandatoryAll()
        'bintang1.Visible = True
        'bintang2.Visible = True
        'lblAlamat.Attributes("class") = "label_split"
        lblTelepon1.Attributes("class") = "label_split"
    End Sub

    Public Sub ValidatorFalse()
        'RequiredFieldValidator1.Enabled = False
        'RequiredFieldValidator2.Enabled = False
        'RequiredFieldValidator3.Enabled = False
        'oPopupZipCode.ValidatorFalse()
    End Sub

    Public Sub Phone1ValidatorEnabled(ByVal status As Boolean)
        If status = True Then

            'RequiredFieldValidator2.Enabled = True
            'RequiredFieldValidator3.Enabled = True
        Else
            'RequiredFieldValidator2.Enabled = False
            'RequiredFieldValidator3.Enabled = False
        End If
    End Sub
    Public Sub ValidatorTrue()
        'RequiredFieldValidator1.Enabled = True
        'RequiredFieldValidator2.Enabled = True
        'RequiredFieldValidator3.Enabled = True
    End Sub
    Public Sub ValidatorDefault()
        'RequiredFieldValidator1.Enabled = True
        'RequiredFieldValidator2.Enabled = True
        'RequiredFieldValidator3.Enabled = True
    End Sub
    Public Sub BindAddress()
        LblAreaPhoneHome.Text = Me.AreaPhoneHome
        LblAreaPhoneOffice.Text = Me.AreaPhoneOffice
        LblPhoneHome.Text = Me.PhoneHome
        LblPhoneOffice.Text = Me.PhoneOffice
        LblHandphone.Text = Me.Handphone
        LblEmergencyPhone.Text = Me.EmergencyContact
    End Sub
    Public Sub BorderNone(ByVal Read As Boolean, ByVal Style As BorderStyle)
        'LblAreaPhoneHome.ReadOnly = Read
        'LblAreaPhoneHome.BorderStyle = Style
        'LblAreaPhoneOffice.ReadOnly = Read
        'LblAreaPhoneOffice.BorderStyle = Style
        'LblPhoneHome.ReadOnly = Read
        'LblPhoneHome.BorderStyle = Style
        'LblPhoneOffice.ReadOnly = Read
        'LblPhoneOffice.BorderStyle = Style
        'LblHandphone.ReadOnly = Read
        'LblHandphone.BorderStyle = Style
        'LblEmergencyPhone.ReadOnly = Read
        'LblEmergencyPhone.BorderStyle = Style
        ''oPopupZipCode.LookUpBorder(Read, Style)
        'If Style = BorderStyle.None Then
        '    lblMiring.Visible = False
        '    lblStrip1.Visible = False
        '    lblStrip2.Visible = False
        '    lblStrip3.Visible = False
        '    txtAddress.TextMode = TextBoxMode.SingleLine
        'Else
        '    lblMiring.Visible = True
        '    lblStrip1.Visible = True
        '    lblStrip2.Visible = True
        '    lblStrip3.Visible = True
        '    txtAddress.TextMode = TextBoxMode.MultiLine
        'End If
    End Sub

    Public Sub BPKBView()
        pnlTelepon.Visible = False
    End Sub

End Class