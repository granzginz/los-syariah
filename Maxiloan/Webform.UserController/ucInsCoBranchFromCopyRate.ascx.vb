﻿#Region "Imports"
Imports Maxiloan.General
Imports Maxiloan.Controller
#End Region

Public Class ucInsCoBranchFromCopyRate
    Inherits ControlBased

#Region "Private Const"
    Private m_controller As New DataUserControlController
    Private Const BRANCH_SOURCE_RATE As String = General.CommonVariableHelper.BRANCH_SOURCE_FOR_CUSTOMER
#End Region

#Region "Branch Info"
    Public Property InsCoID() As String
        Get
            Return CType(ViewState("InsCoID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("InsCoID") = Value
        End Set
    End Property


    Public Property InsCoBranchID() As String
        Get
            Return CType(ViewState("InsCoBranchID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("InsCoBranchID") = Value
        End Set
    End Property

    Public Property InsCoBranchName() As String
        Get
            Return CType(ViewState("InsCoBranchName"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("InsCoBranchName") = Value
        End Set
    End Property
    Public Property Description() As String
        Get
            Return CType(cboInsCoBranch.SelectedItem.Text, String)
        End Get
        Set(ByVal Value As String)
            cboInsCoBranch.SelectedIndex = cboInsCoBranch.Items.IndexOf(cboInsCoBranch.Items.FindByText(Value))
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not IsPostBack Then
            Dim DtInsCoBranchNameCopyRate As New DataTable
            'DtInsCoBranchNameCopyRate = CType(Me.Cache.Item(CommonCacheHelper.CACHE_INSCOBRANCH_FROM_COPYRATE), DataTable)
            'If DtInsCoBranchNameCopyRate Is Nothing Then
            Dim DtBranchNameCache As New DataTable
            DtBranchNameCache = m_controller.GetBranchFromCopyRate(GetConnectionString, Me.InsCoID, Me.sesBranchId.Replace("'", ""))
            Me.Cache.Insert(CommonCacheHelper.CACHE_INSCOBRANCH_FROM_COPYRATE, DtBranchNameCache, Nothing, DateTime.Now.AddHours(CommonCacheHelper.DEFAULT_TIME_DURATION_CACHE_HOUR), TimeSpan.Zero)
            'DtInsCoBranchNameCopyRate = CType(Me.Cache.Item(CommonCacheHelper.CACHE_INSCOBRANCH_FROM_COPYRATE), DataTable)
            'End If

            cboInsCoBranch.DataValueField = "ID"
            cboInsCoBranch.DataTextField = "Name"

            cboInsCoBranch.DataSource = DtBranchNameCache
            cboInsCoBranch.DataBind()

            cboInsCoBranch.SelectedIndex = 1

        End If
        Me.InsCoBranchID = cboInsCoBranch.SelectedItem.Value.Trim
        Me.InsCoBranchName = cboInsCoBranch.SelectedItem.Text.Trim

    End Sub

    Public Sub GetInsCoBranch()
        Me.InsCoBranchID = cboInsCoBranch.SelectedItem.Value.Trim
        Me.InsCoBranchName = cboInsCoBranch.SelectedItem.Text.Trim
    End Sub


End Class