﻿
Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.Controller
Public Class UcBranch
    Inherits ControlBased

#Region "Branch Info"
    
    Public Property BranchID As String
        Get
            Return cmbBranchID.SelectedValue
        End Get
        Set(ByVal BranchID As String)
            cmbBranchID.SelectedValue = BranchID
        End Set
    End Property

    Public Property BranchName() As String
        Get
            Return cmbBranchID.SelectedItem.Text
        End Get
        Set(ByVal BranchName As String)
            cmbBranchID.SelectedItem.Text = BranchName
        End Set
    End Property
#End Region

#Region " Private Const "
    Private m_controller As New DataUserControlController
    Protected WithEvents cmbBranchID As System.Web.UI.WebControls.DropDownList
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not IsPostBack Then
            Dim DtBranchName As New DataTable
            'DtBranchName = CType(Me.Cache.Item(CACHE_BRANCH & Me.sesBranchName), DataTable)
            'If DtBranchName Is Nothing Then
            '    Dim DtBranchNameCache As New DataTable
            '    DtBranchNameCache = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
            '    Me.Cache.Insert(CACHE_BRANCH & Me.sesBranchName, DtBranchNameCache, Nothing, DateTime.Now.AddHours(DEFAULT_TIME_DURATION_CACHE_HOUR), TimeSpan.Zero)
            '    DtBranchName = CType(Me.Cache.Item(CACHE_BRANCH & Me.sesBranchName), DataTable)
            'End If

            DtBranchName = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)

            cmbBranchID.DataValueField = "ID"
            cmbBranchID.DataTextField = "Name"

            cmbBranchID.DataSource = DtBranchName
            cmbBranchID.DataBind()
            cmbBranchID.Items.Insert(0, "Select One")
            cmbBranchID.Items(0).Value = "0"

            If cmbBranchID.Items.Count > 1 Then
                cmbBranchID.SelectedIndex = 1
            Else
                cmbBranchID.SelectedIndex = 0
            End If
        End If        
    End Sub

End Class