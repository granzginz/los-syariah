﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucSyaratCair.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.ucSyaratCair" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="ucDateCE.ascx" TagPrefix="uc1" TagName="ucDate" %>
<div class="form_title">
    <div class="form_single">
        <h4>
            DAFTAR PERSYARATAN CAIR</h4>
    </div>
</div>
<div class="form_box_header">
    <div class="form_single">
        <div class="grid_wrapper_ws">
            <asp:DataGrid ID="dtgSyaratCair" runat="server" CellPadding="3" CellSpacing="1" BorderWidth="0px"
                AutoGenerateColumns="False" DataKeyField="ID" CssClass="grid_general" >
                <HeaderStyle CssClass="th" />
                <ItemStyle CssClass="item_grid" />
                <Columns>
                    <asp:TemplateColumn HeaderText="NO" ItemStyle-CssClass="command_col">
                        <ItemTemplate>
                            <asp:Label ID="lblNo" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:BoundColumn DataField="Description" HeaderText="PERSYARATAN" ItemStyle-CssClass="name_col">                        
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="ID" HeaderText="PERSYARATAN" Visible="false"></asp:BoundColumn>
                    <asp:TemplateColumn HeaderText="PERIKSA" ItemStyle-CssClass="command_col">
                        <ItemTemplate>
                            <asp:CheckBox ID="chk" runat="server"></asp:CheckBox>
                            <asp:Label ID="lblChkSyaratCair" runat="server" Visible="false" CssClass="label_req">&nbsp;&nbsp;&nbsp;&nbsp;</asp:Label>
                            <asp:checkbox id="isMandatory" runat="server"  Checked='<%# DataBinder.eval(Container, "DataItem.isMandatory") %>' Visible="false"/>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="CATATAN">
                        <ItemTemplate>
                            <asp:TextBox ID="txtNotes" Text='' runat="server" CssClass="desc_textbox">
                            </asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="TGL JANJI" ItemStyle-CssClass="small_text" Visible="false">
                        <ItemTemplate>
                            <asp:TextBox runat="server" ID="txtPromiseDate" CssClass="small_text"></asp:TextBox>
                            <asp:CalendarExtender runat="server" ID="cePromiseDate" TargetControlID="txtPromiseDate"
                                Format="dd/MM/yyyy">
                            </asp:CalendarExtender>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Penulisan tanggal salah format, input dengan format dd/MM/yyyy!"
                                ValidationExpression="(0[1-9]|[12][0-9]|3[01])/(0[1-9]|1[012])/\d{4}" ControlToValidate="txtPromiseDate"
                                SetFocusOnError="true" Display="Dynamic" CssClass="validator_general"> 
                            </asp:RegularExpressionValidator>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                </Columns>
            </asp:DataGrid>
        </div>
    </div>
</div>
