﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucAO.ascx.vb" Inherits="Maxiloan.Webform.UserController.ucAO"  %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../Webform.UserController/UcSearchBy.ascx" %>

<asp:UpdatePanel runat="server" ID="UpdatePanel1">
    <ContentTemplate>
        <asp:Button runat="server" ID="btnLookup" Style="display: none;" Text="Lookup" />
        <asp:ModalPopupExtender ID="ModalPopupExtender1" runat="server" PopupControlID="Panel0"
            BackgroundCssClass="wpbg" TargetControlID="btnLookup" CancelControlID="btnExit">
        </asp:ModalPopupExtender>
        <asp:Panel runat="server" ID="Panel0">
            <div class="wp">
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
                <asp:ImageButton runat="server" ID="btnExit" ImageUrl="../Images/exit_lookup00.png"
                    CssClass="wpbtnexit" />
                <div class="form_title">
                    <div class="title_strip">
                    </div>
                    <div class="form_single">
                        <h4>
                            DAFTAR CMO
                        </h4>
                    </div>
                </div>
                <asp:Panel ID="Panel1" runat="server">
                    <asp:Panel ID="Panel2" runat="server">
                        <div class="form_box_header">
                            <div class="form_single">
                                <div class="grid_wrapper_ns">
                                    <asp:DataGrid ID="dtgAO" runat="server" Width="100%" AllowSorting="True" OnSortCommand="SortGrid"
                                        AutoGenerateColumns="False" DataKeyField="EmployeeID" CellPadding="0" CssClass="grid_general">
                                        <HeaderStyle CssClass="th" />
                                        <ItemStyle CssClass="item_grid" />
                                        <Columns>
                                            <asp:ButtonColumn ButtonType="LinkButton" CommandName="SELECT" Text="SELECT"></asp:ButtonColumn>
                                            <asp:TemplateColumn SortExpression="EmployeeID" HeaderText="CMO ID">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEmployeeID" runat="server" Text='<%#Container.DataItem("EmployeeID")%>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn SortExpression="EmployeeName" HeaderText="EMPLOYEE NAME">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEmployeeName" runat="server" Text='<%#Container.DataItem("EmployeeName")%>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                        </Columns>
                                        <PagerStyle Visible="False"></PagerStyle>
                                    </asp:DataGrid>
                                </div>
                                <div class="button_gridnavigation">
                                    <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                                        CommandName="First" ImageUrl="../Images/grid_navbutton01.png"></asp:ImageButton>
                                    <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                                        CommandName="Prev" ImageUrl="../Images/grid_navbutton02.png"></asp:ImageButton>
                                    <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                                        CommandName="Next" ImageUrl="../Images/grid_navbutton03.png"></asp:ImageButton>
                                    <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                                        CommandName="Last" ImageUrl="../Images/grid_navbutton04.png"></asp:ImageButton>
                                    Page
                                    <asp:TextBox ID="txtPage" runat="server" Width="34px">1</asp:TextBox>
                                    <asp:Button ID="btnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                                        EnableViewState="False" />
                                    <asp:RangeValidator ID="rgvGo" runat="server" font-name="Verdana" Font-Size="11px"
                                        Type="Integer" MaximumValue="999999999" ErrorMessage="Page No. is not valid"
                                        MinimumValue="1" ControlToValidate="txtPage"></asp:RangeValidator>
                                    <asp:RequiredFieldValidator ID="rfvGo" runat="server" font-name="Verdana" Font-Size="11px"
                                        ErrorMessage="Page No. is not valid" ControlToValidate="txtPage" Display="Dynamic" CssClass ="validator_general"></asp:RequiredFieldValidator>
                                </div>
                                <div class="label_gridnavigation">
                                    <asp:Label ID="lblPage" runat="server"></asp:Label>of
                                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                                    <asp:Label ID="lblRecord" runat="server"></asp:Label>record(s)
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:Panel runat="server" ID="Panel3">
                        <div class="form_box_title">
                            <div class="form_single">
                                <h4>
                                    CARI CMO
                                </h4>
                            </div>
                        </div>
                        <div class="form_box">
                            <div class="form_single">
                                <uc1:ucsearchby id="oSearchBy" runat="server">
                                </uc1:ucsearchby>
                            </div>
                        </div>
                        <div class="form_button">
                            <asp:Button ID="btnSearch" runat="server" CausesValidation="False" Text="Find" CssClass="small button blue" />
                            <asp:Button ID="btnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray" />
                        </div>
                    </asp:Panel>
                </asp:Panel>
            </div>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
