﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucHasilSurveyTab.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.ucHasilSurveyTab" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<div class="tab_container">
    <div id="tabSurvey" runat="server">
        <asp:HyperLink ID="hypHasilSurvey" runat="server">HASIL SURVEY</asp:HyperLink></div>
    <div id="tabCharacter" runat="server">
        <asp:HyperLink ID="hypCharacter" runat="server">CHARACTER</asp:HyperLink></div>
    <div id="tabCapacity" runat="server">
        <asp:HyperLink ID="hypCapacity" runat="server">CAPACITY</asp:HyperLink></div>
    <div id="tabCondition" runat="server">
        <asp:HyperLink ID="hypCondition" runat="server">CONDITION</asp:HyperLink></div>
    <div id="tabCapital" runat="server">
        <asp:HyperLink ID="hypCapital" runat="server">CAPITAL</asp:HyperLink></div>
    <div id="tabCollateral" runat="server">
        <asp:HyperLink ID="hypCollateral" runat="server">COLLATERAL</asp:HyperLink></div>
    <div id="tabCatatan" runat="server">
        <asp:HyperLink ID="hypCatatan" runat="server">CATATAN</asp:HyperLink></div>
    <div id="tabKYC" runat="server">
        <asp:HyperLink ID="hypKYC" runat="server">KYC</asp:HyperLink></div>
</div>
