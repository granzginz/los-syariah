﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UcPaymentInfo.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.UcPaymentInfo" %>
<asp:Panel ID="pnltopi" Visible="True" runat="server">
    <div class="form_box_title">
        <div class="form_single">
            <h5>
                JUMLAH PEMBAYARAN JATUH TEMPO PER TANGGAL -
                <asp:Label ID="labAsOf" runat="server"></asp:Label>
            </h5>
        </div>
    </div>
</asp:Panel>
<div class="form_box">
    <div>
        <div class="form_left">
            <label>
                Angsuran Jatuh Tempo
            </label>
            <asp:Label ID="labInstallmentDue" runat="server" CssClass="numberAlign label"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Premi Asuransi Jatuh Tempo
            </label>
            <asp:Label ID="labInsuranceDue" runat="server" CssClass="numberAlign label"></asp:Label>
        </div>
    </div>
</div>
<div class="form_box">
    <div>
        <div class="form_left">
            <label>
                Denda Keterlambatan Angsuran
            </label>
            <asp:Label ID="labLCInstall" runat="server" CssClass="numberAlign label"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Denda Keterlambatan Asuransi
            </label>
            <asp:Label ID="labLCInsurance" runat="server" CssClass="numberAlign label"></asp:Label>
        </div>
    </div>
</div>
<div class="form_box">
    <div>
        <div class="form_left">
            <label>
                Biaya Tagih
            </label>
            <asp:Label ID="labInstallColl" runat="server" CssClass="numberAlign label"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Biaya Tagih Premi Asuransi
            </label>
            <asp:Label ID="labInsuranceColl" runat="server" CssClass="numberAlign label"></asp:Label>
        </div>
    </div>
</div>
<div class="form_box">
    <div>
        <div class="form_left">
            <label>
                Biaya Tolakan PDC
            </label>
            <asp:Label ID="labPDCBounceFee" runat="server" CssClass="numberAlign label"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Biaya Perpanjangan STNK
            </label>
            <asp:Label ID="labSTNKFee" runat="server" CssClass="numberAlign label"></asp:Label>
        </div>
    </div>
</div>
<div class="form_box">
    <div>
        <div class="form_left">
            <label>
                Biaya Klaim Asuransi
            </label>
            <asp:Label ID="labInsuranceClaim" runat="server" CssClass="numberAlign label"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Biaya Tarik
            </label>
            <asp:Label ID="labRepossessionFee" runat="server" CssClass="numberAlign label"></asp:Label>
        </div>
    </div>
</div>
<div class="form_box">
    <div>
        <div class="form_left">
        </div>
        <div class="form_right">
            <label>
                Jumlah Pembayaran Jatuh Tempo
            </label>
            <asp:Label ID="labTotalOSOverDue" runat="server" CssClass="numberAlign label"></asp:Label>
        </div>
    </div>
</div>
