﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucBGNoDate.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.ucBGNoDate" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="ucDateCE.ascx" TagName="ucDateCE" TagPrefix="uc1" %>

<div class="form_box_usercontrol">
    <div class="form_left_usercontrol">
        <label>
            No. Bilyet Giro</label>
        <asp:DropDownList AutoPostBack="true" ID="cmbBGNo" runat="server">
        </asp:DropDownList>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Harap isi No Bilyet Giro"
            Display="Dynamic" ControlToValidate="cmbBGNo" InitialValue="0" CssClass="validator_general"></asp:RequiredFieldValidator>
    </div>
</div>
<div class="form_box_usercontrol">
    <div class="form_left_usercontrol">
        <label>
            Tanggal Jatuh Tempo</label>
        <uc1:ucdatece id="DueDate" runat="server"></uc1:ucdatece>        
    </div>
</div>
