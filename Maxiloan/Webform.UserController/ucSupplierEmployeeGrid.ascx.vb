﻿Imports Maxiloan.Controller

Public Class ucSupplierEmployeeGrid
    Inherits ControlBased

    Private m_controller As New SupplierController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10

    Public Delegate Sub SelectedSupplierEmployeeForDistribusiNilaiHandler()
    Public Event SelectedSupplierEmployeeForDistribusiNilai As SelectedSupplierEmployeeForDistribusiNilaiHandler

#Region "Properties"
    Public Property FilterStatement1 As String
    Public Property FilterStatement2 As String
    Public Property SortStatement As String
    Public Property SupplierEmployeeID As String
    Public Property SupplierEmployeeName As String
    Public Property SupplierEmployeePosition As String
    Public Property SupplierEmployeePositionID As String
    Public Property SupplierEmployeeAccountNo As String
    Public Property SupplierEmployeeAccountName As String
    Public Property SupplierEmployeeBankID As String
    Public Property SupplierEmployeeBankName As String
    Public Property SupplierEmployeeBankBranchID As String
    Public Property SupplierEmployeeBankBranch As String
    Public Property ValidationGroup As String
        Get
            Return ViewState("validationGroupIncentive").ToString
        End Get
        Set(ByVal value As String)
            ViewState("validationGroupIncentive") = value
        End Set
    End Property

#End Region

    Public Sub getSupplierEmployee()
        Dim dtEntity As DataTable = Nothing
        Dim oCustomClass As New Parameter.Supplier

        oCustomClass.PageSize = pageSize
        oCustomClass.WhereCond = Me.FilterStatement1
        oCustomClass.SortBy = Me.SortStatement
        oCustomClass.WhereCond2 = Me.FilterStatement2
        oCustomClass.CurrentPage = currentPage
        oCustomClass.strConnection = GetConnectionString()
        oCustomClass = m_controller.GetSupplierEmployee(oCustomClass)

        If Not oCustomClass Is Nothing Then
            dtEntity = oCustomClass.ListData
            gridSupplierEmployee.DataSource = dtEntity.DefaultView
            gridSupplierEmployee.CurrentPageIndex = 0
            gridSupplierEmployee.DataBind()
        End If

    End Sub

    Public Sub getSelectedSupplierEmployee(ByVal parSelectedIndex As Integer)
        'NOTE -1 berarti selection menggunakan radio button
        Dim i As Integer = -1

        i = parSelectedIndex

        If i = -1 Then
            'TODO Buat prosedur untuk select by radio button
            Dim rboSelect As RadioButton

            For i = 0 To gridSupplierEmployee.Items.Count
                rboSelect = CType(gridSupplierEmployee.Items(i).FindControl("rboSelect"), RadioButton)
                If rboSelect.Checked Then
                    gridSupplierEmployee.SelectedIndex = i
                    i = gridSupplierEmployee.SelectedIndex
                    Exit For
                End If
            Next

        End If

        Me.SupplierEmployeeID = gridSupplierEmployee.DataKeys.Item(i).ToString
        Me.SupplierEmployeeName = gridSupplierEmployee.Items(i).Cells(1).Text
        Me.SupplierEmployeePositionID = gridSupplierEmployee.Items(i).Cells(2).Text
        Me.SupplierEmployeePosition = gridSupplierEmployee.Items(i).Cells(3).Text
        Me.SupplierEmployeeBankID = gridSupplierEmployee.Items(i).Cells(4).Text
        Me.SupplierEmployeeBankName = gridSupplierEmployee.Items(i).Cells(5).Text
        Me.SupplierEmployeeBankBranchID = gridSupplierEmployee.Items(i).Cells(6).Text
        Me.SupplierEmployeeBankBranch = gridSupplierEmployee.Items(i).Cells(7).Text
        Me.SupplierEmployeeAccountNo = gridSupplierEmployee.Items(i).Cells(8).Text
        Me.SupplierEmployeeAccountName = gridSupplierEmployee.Items(i).Cells(9).Text
    End Sub

    Protected Sub gridSupplierEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles gridSupplierEmployee.SelectedIndexChanged
        getSelectedSupplierEmployee(gridSupplierEmployee.SelectedIndex)
        RaiseEvent SelectedSupplierEmployeeForDistribusiNilai()
    End Sub
End Class