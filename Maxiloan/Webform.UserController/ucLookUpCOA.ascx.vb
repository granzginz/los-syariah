﻿Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports System.Data.SqlClient
Imports Maxiloan.General.CommonCookiesHelper
Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.Framework.SQLEngine

Public Class ucLookUpCOA
    Inherits ControlBased

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Public Delegate Sub CatSelectedHandler(ByVal PaymentAllocationID As String,
                                           ByVal Description As String,
                                           ByVal COA As String)
    Public Event CatSelected As CatSelectedHandler

    Public Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property

    Public Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Text = ""
    End Sub

    Public Sub BindGridEntity(ByVal cmdWhere As String)
        Dim dtEntity As DataTable = Nothing
        Dim oCustom As New Parameter.LookUpTransaction

        oCustom.strConnection = GetConnectionString()
        oCustom.WhereCond = cmdWhere
        oCustom.CurrentPage = currentPage
        oCustom.PageSize = pageSize
        oCustom.SortBy = Me.Sort
        oCustom = GetData(oCustom)

        If Not oCustom Is Nothing Then
            dtEntity = oCustom.ListData
            recordCount = oCustom.TotalRecords
        Else
            recordCount = 0
        End If

        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()
        PagingFooter()
    End Sub

    Public Function GetData(ByVal oCustomClass As Parameter.LookUpTransaction) As Parameter.LookUpTransaction
        Dim oReturnValue As New Parameter.LookUpTransaction
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spPagingTransaction", params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.GetData")
        End Try
    End Function

#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            lblMessage.Text = "Data not found....."
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblTotRec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select

        Popup()
        BindGridEntity(Me.CmdWhere)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGoPage.Click
        If IsNumeric(txtGoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtGoPage.Text, Int32)
                Popup()
                BindGridEntity(Me.CmdWhere)
            End If
        End If
    End Sub
#End Region

    Protected Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If

        Popup()
        BindGridEntity(Me.CmdWhere)
    End Sub

    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Popup()
        BindGridEntity(Me.CmdWhere)
    End Sub

    Private Sub imbSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        If txtSearchBy.Text <> "" Then
            Me.CmdWhere = cboSearchBy.SelectedItem.Value + " like '%" + txtSearchBy.Text + "%'"
        End If
        Popup()
        BindGridEntity(Me.CmdWhere)
    End Sub

    Protected Sub dtgPaging_ItemCommand(ByVal sender As Object, ByVal e As DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        Dim i As Integer = e.Item.ItemIndex
        RaiseEvent CatSelected(dtgPaging.DataKeys.Item(i).ToString.Trim,
                               dtgPaging.Items(i).Cells.Item(1).Text.Trim,
                               dtgPaging.Items(i).Cells.Item(2).Text.Trim)
    End Sub

    Public Sub Popup()
        Me.ModalPopupExtender.Show()
    End Sub
End Class