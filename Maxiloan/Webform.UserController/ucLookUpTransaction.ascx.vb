﻿Public Class ucLookUpTransaction
    Inherits System.Web.UI.UserControl


    Protected WithEvents txtAmount As ucNumberFormat
    Protected WithEvents txtValutaDate As ucDateCE


    Public Property EnableAmount() As Boolean
        Get
            Return txtAmount.Enabled
        End Get
        Set(ByVal Value As Boolean)
            txtAmount.Enabled = Value
        End Set
    End Property
    Public WriteOnly Property EnabledValutaDate As Boolean
        Set(ByVal value As Boolean)
            txtValutaDate.Enabled = value
        End Set
    End Property
    Public Property VisibleAmount() As Boolean
        Get
            Return divAmount.Visible
        End Get
        Set(ByVal Value As Boolean)
            divAmount.Visible = Value
        End Set
    End Property
    Public Property EnableDescription() As Boolean
        Get
            Return txtDescription.Enabled
        End Get
        Set(ByVal Value As Boolean)
            txtDescription.Enabled = Value
        End Set
    End Property
    Public Property VisibleDescription() As Boolean
        Get
            Return divDesc.Visible
        End Get
        Set(ByVal Value As Boolean)
            divDesc.Visible = Value
        End Set
    End Property
    Public Property Transaction() As String
        Get
            Return CType(txtTransaction.Text.Trim, String)
        End Get
        Set(ByVal Value As String)
            txtTransaction.Text = Value
        End Set
    End Property

    Public Property TransactionID() As String
        Get
            Return CType(hdnTransactionID.Value.Trim, String)
        End Get
        Set(ByVal Value As String)
            hdnTransactionID.Value = Value
        End Set
    End Property

    Public Property Amount() As Double
        Get
            Return CType(txtAmount.Text.Trim, Double)
        End Get
        Set(ByVal Value As Double)
            txtAmount.Text = FormatNumber(CStr(Value), 2)
        End Set
    End Property

    Public Property Description() As String
        Get
            Return CType(txtDescription.Text.Trim, String)
        End Get
        Set(ByVal Value As String)
            txtDescription.Text = Value
        End Set
    End Property
    Public Property ValutaDate() As String
        Get
            Return CType(txtValutaDate.Text.Trim, String)
        End Get
        Set(ByVal Value As String)
            txtValutaDate.Text = Value
        End Set
    End Property

    Public Property ProcessID() As String
        Get
            Return CType(ViewState("ProcessID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ProcessID") = Value
        End Set
    End Property

    Public Property IsAgreement() As String
        Get
            Return CType(ViewState("IsAgreement"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("IsAgreement") = Value
        End Set
    End Property
    Public Property IsPettyCash() As String
        Get
            Return CType(ViewState("IsPettyCash"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("IsPettyCash") = Value
        End Set
    End Property
    Public Property IsHOTransaction() As String
        Get
            Return CType(ViewState("IsHOTransaction"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("IsHOTransaction") = Value
        End Set
    End Property
    Public Property IsPaymentReceive() As String
        Get
            Return CType(ViewState("IsPaymentReceive"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("IsPaymentReceive") = Value
        End Set
    End Property
    Public Property IsPaymentRequest() As String
        Get
            Return CType(ViewState("IsPaymentRequest"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("IsPaymentRequest") = Value
        End Set
    End Property
    Public Property Style() As String
        Get
            Return CStr(ViewState("style"))
        End Get
        Set(ByVal Value As String)
            ViewState("style") = Value
        End Set
    End Property
    Public Property COA() As String
        Get
            Return CType(hdnCOA.value.trim, String)
        End Get
        Set(ByVal value As String)
            hdnCOA.value = value
        End Set
    End Property

    Public WriteOnly Property IsCanNeagetive() As Boolean
        Set(ByVal Value As Boolean)
            If Value Then
                txtAmount.RangeValidatorEnable = False
                txtAmount.RequiredFieldValidatorEnable = False
                txtAmount.RangeValidatorMinimumValue = "0"
            End If
        End Set
    End Property
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        txtAmount.TextCssClass = "numberAlign regular_text"
        txtTransaction.Attributes.Add("readonly", "true")
        hdnIsPaymentReceive.Value = Me.IsPaymentReceive

    End Sub
    Public Sub BindData()
        txtTransaction.Text = Me.Transaction
        txtDescription.Text = Me.Description
        txtValutaDate.Text = Me.ValutaDate

        hdnIsPettyCash.Value = Me.IsPettyCash
        hdnIsPaymentRequest.Value = Me.IsPaymentRequest

    End Sub

End Class