﻿Imports Maxiloan.Controller
Imports Maxiloan.General.CommonCacheHelper

Public Class UcBankAccountID
    Inherits ControlBased

#Region "Contanta"
    Private oController As New DataUserControlController

#End Region

    Public Property Filter() As String
        Get
            Return CType(ViewState("Filter"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Filter") = Value
        End Set
    End Property

    Public Property BankAccountName() As String
        Get
            Return CType(cmbBankAccount.SelectedItem.Text, String)
        End Get
        Set(ByVal Value As String)
            cmbBankAccount.SelectedIndex = cmbBankAccount.Items.IndexOf(cmbBankAccount.Items.FindByText(Value))
        End Set
    End Property


    Public Property BankAccountID() As String
        Get
            Return CType(cmbBankAccount.SelectedItem.Value, String)
        End Get
        Set(ByVal Value As String)
            cmbBankAccount.SelectedIndex = cmbBankAccount.Items.IndexOf(cmbBankAccount.Items.FindByValue(Value))
        End Set
    End Property

    Public Property IsAutoPostback() As Boolean
        Get
            Return CType(ViewState("IsPostBack"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            ViewState("IsPostBack") = Value
        End Set
    End Property


    Public Property BankType() As String
        Get
            Return CType(ViewState("BankType"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("BankType") = Value
        End Set
    End Property

    Public Property BankPurpose() As String
        Get
            Return CType(ViewState("BankPurpose"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("BankPurpose") = Value
        End Set
    End Property

    Public Property IsAll() As Boolean
        Get
            Return (CType(ViewState("IsAll"), Boolean))
        End Get
        Set(ByVal Value As Boolean)
            ViewState("IsAll") = Value
        End Set
    End Property
    Public Sub ValidatorFalse()
        RequiredFieldValidator4.Enabled = False
    End Sub

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not IsPostBack Then

        End If
    End Sub
    Public Sub BindBankAccount()
        cmbBankAccount.AutoPostBack = Me.IsAutoPostback
        Dim strCacheName As String
        strCacheName = CACHE_BANKACCOUNT & Me.sesBranchId.Replace("'", "") & Me.BankType & Me.BankPurpose
        Dim dtBankAccount As New DataTable
        dtBankAccount = CType(Me.Cache.Item(strCacheName), DataTable)
        If dtBankAccount Is Nothing Then
            Dim dtBankAccountCache As New DataTable
            dtBankAccountCache = oController.GetBankAccount(GetConnectionString, Me.sesBranchId.Replace("'", ""), _
                                Me.BankType, Me.BankPurpose)
            Me.Cache.Insert(strCacheName, dtBankAccountCache, Nothing, DateTime.Now.AddHours(DEFAULT_TIME_DURATION_CACHE_HOUR), TimeSpan.Zero)
            dtBankAccount = CType(Me.Cache.Item(strCacheName), DataTable)
        End If
        cmbBankAccount.DataTextField = "Name"
        cmbBankAccount.DataValueField = "ID"
        cmbBankAccount.DataSource = dtBankAccount
        cmbBankAccount.DataBind()

        If Me.IsAll Then
            cmbBankAccount.Items.Insert(0, "ALL")
            cmbBankAccount.Items(0).Value = "ALL"
        Else
            cmbBankAccount.Items.Insert(0, "Select One")
            cmbBankAccount.Items(0).Value = "0"
        End If
        If cmbBankAccount.Items.Count > 1 Then
            cmbBankAccount.SelectedIndex = 1
        Else
            cmbBankAccount.SelectedIndex = 0
        End If

        Me.BankAccountID = cmbBankAccount.SelectedItem.Value.Trim
        Me.BankAccountName = cmbBankAccount.SelectedItem.Text.Trim

    End Sub

End Class