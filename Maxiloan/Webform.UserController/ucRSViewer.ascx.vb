﻿Imports Microsoft.Reporting.WebForms

Public Class ucRSViewer
    Inherits Maxiloan.Webform.ControlBased

    Public Property ReportPath As String
        Get
            Return CType(ViewState("ReportPath"), String)
        End Get
        Set(ByVal value As String)
            ViewState("ReportPath") = value
        End Set
    End Property

    Public Property ProcessingMode As ProcessingMode
        Get
            Return CType(ViewState("ProcessingMode"), ProcessingMode)
        End Get
        Set(ByVal value As ProcessingMode)
            ViewState("ProcessingMode") = value
        End Set
    End Property

    Public Property ReportID As String
        Get
            Return CType(ViewState("ReportID"), String)
        End Get
        Set(ByVal value As String)
            ViewState("ReportID") = value
        End Set
    End Property


    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then


            Dim irsc As IReportServerCredentials = New CustomReportCredentials(Me.UIDRS, Me.PassRS, Me.ServerNameRS)
            'Dim irsc As IReportServerCredentials = New CustomReportCredentials("administrator", "afi@12345", "alphard")
            With ReportViewer1                
                .Reset()
                .ProcessingMode = Me.ProcessingMode
                .ServerReport.ReportServerCredentials = irsc
                .ServerReport.ReportServerUrl = New Uri(Me.VirtualDirectoryRS)
                '.ServerReport.ReportServerUrl = New Uri("http://alphard/reportserver")

                .ServerReport.ReportPath = Me.ReportPath            
                .ServerReport.SetParameters(RSConfig.getRSParameters(Me.ReportID, Request.Cookies("RSCOOKIES")))
                .ServerReport.Refresh()
                .ShowToolBar = True
                .ShowReportBody = True
                .ShowParameterPrompts = False
            End With
        End If
    End Sub
End Class