﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucAPType.ascx.vb" Inherits="Maxiloan.Webform.UserController.ucAPType" %>

        <div class="form_box_usercontrol">
            <div class="form_left_usercontrol">
                <label>
                    Jenis Pembayaran</label>
                <asp:DropDownList ID="cmbAPType" runat="server">    
                </asp:DropDownList>
                <asp:TextBox ID="txtAPType" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfv1" runat="server" Display="Dynamic"
                    ErrorMessage="Harap pilih Jenis Pembayaran/Pencairan" ControlToValidate="cmbAPType"
                    InitialValue="0" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
