﻿Public Class UcSearchByWithNoTable
    Inherits System.Web.UI.UserControl

    Public WriteOnly Property ListData() As String
        Set(ByVal Value As String)
            viewstate("listdata") = Value
        End Set
    End Property

    Public Property Text() As String
        Get
            Return (CType(viewstate("text"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("text") = Value
        End Set
    End Property

    Public ReadOnly Property Description() As String
        Get
            Return (CType(viewstate("description"), String))
        End Get
    End Property

    Public ReadOnly Property ValueID() As String
        Get
            Return (CType(viewstate("ID"), String))
        End Get
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Format String yang bisa dikirim contoh 'BANKID,Bank ID-BANKNAME, Bank Name        
        Me.Text = txtSearch.Text
        If Not IsPostBack Then
            BindData()
        End If

        ViewState("description") = CmbSearchList.SelectedItem.Text.Trim
        ViewState("ID") = CmbSearchList.SelectedItem.Value.Trim
    End Sub

    Public Sub BindData()
        Dim oDataTable As DataTable = New DataTable
        Dim oRow As DataRow
        Dim splitListData() As String
        Dim splitRow() As String
        Dim i As Integer

        oDataTable.Columns.Add("ID", System.Type.GetType("System.String"))
        oDataTable.Columns.Add("Description", System.Type.GetType("System.String"))

        oRow = oDataTable.NewRow()
        oRow("ID") = ""
        oRow("Description") = ""

        If Not viewstate("listdata") Is Nothing Then
            splitListData = Split(CStr(viewstate("listdata")), "-")
            For i = 0 To UBound(splitListData)
                splitRow = Split(splitListData(i), ",")
                oRow("ID") = splitRow(0)
                oRow("Description") = splitRow(1)
                oDataTable.Rows.Add(oRow)
                oRow = oDataTable.NewRow()
            Next

            CmbSearchList.DataValueField = "ID"
            CmbSearchList.DataTextField = "Description"
            CmbSearchList.DataSource = oDataTable
            CmbSearchList.DataBind()
            TxtSearch.Text = Me.Text
        End If

    End Sub
End Class