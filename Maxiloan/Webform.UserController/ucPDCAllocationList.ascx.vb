﻿
#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
#End Region

Public Class ucPDCAllocationList
    Inherits ControlBased
    Dim temptotalPDCPA As Double
    Dim temptotalPDCPAClear As Double
    Dim temptotalPDCHis As Double

#Region "Property"
    Public Property GiroNo() As String
        Get
            Return CType(viewstate("GiroNo"), String)
        End Get
        Set(ByVal Value As String)
            Viewstate("GiroNo") = Value
        End Set
    End Property
    Public Property GiroSeqNo() As Integer
        Get
            Return CType(viewstate("GiroSeqNo"), Integer)
        End Get
        Set(ByVal Value As Integer)
            Viewstate("GiroSeqNo") = Value
        End Set
    End Property
    Public Property PDCReceiptNo() As String
        Get
            Return CType(viewstate("PDCReceiptNo"), String)
        End Get
        Set(ByVal Value As String)
            Viewstate("PDCReceiptNo") = Value
        End Set
    End Property
    Public Property BranchID() As String
        Get
            Return CType(viewstate("BranchID"), String)
        End Get
        Set(ByVal Value As String)
            Viewstate("BranchID") = Value
        End Set
    End Property

    Public Property Style() As String
        Get
            Return CStr(viewstate("style"))
        End Get
        Set(ByVal Value As String)
            viewstate("style") = Value
        End Set
    End Property
    Public Property CustomerId() As String
        Get
            Return CType(viewstate("CustomerId"), String)
        End Get
        Set(ByVal Value As String)
            Viewstate("CustomerId") = Value
        End Set
    End Property
#End Region
#Region "Privateconst"
    Private oCustomClass As New Parameter.PDCReceive
    Private oController As New PDCRInquiryController
    Private oRow As DataRow
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load    
        DoBindPA()
    End Sub
    Sub DoBindPA()
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        With oCustomClass
            .PDCReceiptNo = Me.PDCReceiptNo
            .strConnection = GetConnectionString
            .BranchId = Me.BranchID
            .GiroNo = Me.GiroNo
            .GiroSeqNo = Me.GiroSeqNo
        End With
        oCustomClass = oController.PDCInquiryDetailwithGiroSeqNo(oCustomClass)
        DtUserList = oCustomClass.listPDCPA
        oRow = DtUserList.Rows(0)
        lblAgreeBranch.Text = oRow.Item("BranchFullName").ToString.Trim
        HyAgreementNo.Text = oRow.Item("AgreementNo").ToString.Trim
        HyAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(HyAgreementNo.Text) & "')"
        HyCustomerName.Text = oRow.Item("Name").ToString.Trim
        HyCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(Me.CustomerId) & "')"
        DvUserList = DtUserList.DefaultView
        DvUserList.Sort = Me.SortBy
        DtgPA.DataSource = DvUserList

        Try
            DtgPA.DataBind()
        Catch    
            DtgPA.DataBind()
        End Try
    End Sub
    Private Sub DtgPA_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgPA.ItemDataBound
        Dim lPDCAmount As Label
        Dim totalPDCAmount As Label
        Dim lPDCAmountClear As Label
        Dim totalPDCAmountClear As Label        

        If e.Item.ItemIndex >= 0 Then
            lPDCAmount = CType(e.Item.FindControl("lblAmount"), Label)
            temptotalPDCPA += CDbl(lPDCAmount.Text)
            lPDCAmountClear = CType(e.Item.FindControl("lblAmountClear"), Label)
            temptotalPDCPAClear += CDbl(lPDCAmountClear.Text)
        End If


        If e.Item.ItemType = ListItemType.Footer Then
            totalPDCAmount = CType(e.Item.FindControl("lblTotPDCAmt"), Label)
            totalPDCAmount.Text = FormatNumber(temptotalPDCPA.ToString, 2)
            totalPDCAmountClear = CType(e.Item.FindControl("lblTotPDCAmtClear"), Label)
            totalPDCAmountClear.Text = FormatNumber(temptotalPDCPAClear.ToString, 2)
        End If
    End Sub
End Class
