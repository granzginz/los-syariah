﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucMonthCombo.ascx.vb" Inherits="Maxiloan.Webform.UserController.ucMonthCombo" %>
<asp:DropDownList ID="cboMonth" runat="server">
    <asp:ListItem Value="1">Januari</asp:ListItem>
    <asp:ListItem Value="2">Februari</asp:ListItem>
    <asp:ListItem Value="3">Maret</asp:ListItem>
    <asp:ListItem Value="4">April</asp:ListItem>
    <asp:ListItem Value="5">Mei</asp:ListItem>
    <asp:ListItem Value="6">Juni</asp:ListItem>
    <asp:ListItem Value="7">Juli</asp:ListItem>
    <asp:ListItem Value="8">Agustus</asp:ListItem>
    <asp:ListItem Value="9">September</asp:ListItem>
    <asp:ListItem Value="10">Oktober</asp:ListItem>
    <asp:ListItem Value="11">November</asp:ListItem>
    <asp:ListItem Value="12">Desember</asp:ListItem>
</asp:DropDownList>
