﻿Public Class UcLookUpPdctOffering
    Inherits ControlBased
#Region "Properties and Form Scope Variables"
    Public Property BranchID As String
        Get
            Return CType(ViewState("LookupBranchID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("LookupBranchID") = Value
        End Set
    End Property

    Public Property ProductID As String
        Get
            Return CType(hdnProductID.Value.Trim, String)
        End Get
        Set(ByVal Value As String)
            hdnProductID.Value = Value
        End Set
    End Property

    Public Property ProductOfferingID As String
        Get
            Return CType(hdnProductOfferingID.Value.Trim, String)
        End Get
        Set(ByVal Value As String)
            hdnProductOfferingID.Value = Value
        End Set
    End Property

    Public Property KegiatanUsaha As String
        Get
            Return CType(hdnKegiatanUsaha.value.trim, String)
        End Get
        Set(value As String)
            hdnKegiatanUsaha.Value = value
        End Set
    End Property
    Public Property JenisPembiayaan As String
        Get
            Return CType(hdnJenisPembiayaan.Value.Trim, String)
        End Get
        Set(value As String)
            hdnJenisPembiayaan.Value = value
        End Set
    End Property
    Public Property ProductOfferingDescription As String
        Get
            Return CType(txtProductOffering.Text.Trim, String)
        End Get
        Set(ByVal Value As String)
            txtProductOffering.Text = Value
        End Set
    End Property
    Public WriteOnly Property VisiblePnlLookupProdOff As Boolean
        Set(value As Boolean)
            pnlLookupProdOff.Visible = value
        End Set
    End Property
    Public Property Akad As String
        Get
            Return CType(hdnAkad.Value.Trim, String)
        End Get
        Set(value As String)
            hdnAkad.Value = value
        End Set
    End Property
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

End Class