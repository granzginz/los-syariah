﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UcCoverageType.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.UcCoverageType" %>
<asp:DropDownList ID="DDLCoverageType" runat="server">
</asp:DropDownList>
<asp:RequiredFieldValidator ID="ReqValField" runat="server" ErrorMessage="Please Select Coverage Type"
    InitialValue="0" Display="Dynamic" ControlToValidate="DDLCoverageType" CssClass ="validator_general"></asp:RequiredFieldValidator>