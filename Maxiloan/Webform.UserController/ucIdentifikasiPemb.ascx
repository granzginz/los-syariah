﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucIdentifikasiPemb.ascx.vb" Inherits="Maxiloan.Webform.UserController.ucIdentifikasiPemb" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Button ID="ButtonIdentifikasiPemb" runat="server" EnableViewState="False" Text="Identifikasi Pembayaran"
                CssClass="small button blue"></asp:Button>&nbsp;

<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
<asp:ModalPopupExtender runat="server" ID="mpLookupIdentifikasiPemb" PopupControlID="Panel1"
    TargetControlID="ButtonIdentifikasiPemb" BackgroundCssClass="wpbg" CancelControlID="btnExit">
</asp:ModalPopupExtender>
<asp:Panel runat="server" ID="Panel1">
    <div class="wp">
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
        <asp:ImageButton runat="server" ID="btnExit" ImageUrl="../Images/exit_lookup00.png"
            CssClass="wpbtnexit" />
            <asp:Panel ID="pnlLoad" runat="server">
            <div class="form_box_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h4>
                        IDENTIFIKASI PEMBAYARAN
                    </h4>
                </div>
            </div>
             <div class="form_box">
                    <div class="form_single">
                        <label>
                            Load CSV Files
                        </label>
                        <asp:FileUpload runat="server" ID="files" ViewStateMode="Enabled" />
                    </div>  
             </div>
             <div class="form_button">
                <asp:Button ID="ButtonSearch" runat="server" CssClass="small button blue" Text="Load"
                    CausesValidation="False"></asp:Button>&nbsp;
            </div>
            </asp:Panel>
            <asp:Panel ID="pnlGrid" runat="server">
            <div class="form_box_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h4>
                        IDENTIFIKASI PEMBAYARAN
                    </h4>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single" style="overflow:scroll; height:300px" >
                    <div class="grid_wrapper_ws">
                        <asp:DataGrid ID="dtgPaging" runat="server" AutoGenerateColumns="False" 
                            Width="1366px" CssClass="grid_general">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <Columns>
                                <asp:BoundColumn DataField="keterangan" HeaderText="KETERANGAN">
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center"></HeaderStyle>
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="NoLine" HeaderText="LINE">
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center"></HeaderStyle>
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="agreementNo" HeaderText="AGREEMENT NO">
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center"></HeaderStyle>
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="InstallmentAmount" HeaderText="INSTALLMENT AMOUNT">
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center"></HeaderStyle>
                                </asp:BoundColumn>
                                 <asp:BoundColumn DataField="Name" HeaderText="NAMA CUSTOMER">
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center"></HeaderStyle>
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="NamaPasangan" HeaderText="NAMA PASANGAN">
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center"></HeaderStyle>
                                </asp:BoundColumn>  
                                <asp:BoundColumn DataField="NoPlat" HeaderText="PLAT NO">
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center"></HeaderStyle>
                                </asp:BoundColumn>  
                            </Columns>
                        </asp:DataGrid>
                    </div>
                </div>
            </div>
        </asp:Panel>
    </div>
</asp:Panel>