﻿Imports System.Data.SqlClient

Public Class UcPaymentAllocationDetail
    Inherits System.Web.UI.UserControl
    'Inherits ControlBased

#Region "Property"
#Region "PaymentDetail"
#Region "Installment"
    Public Property InstallmentDue() As Double
        Get
            Return CType(txtInstallmentDue.Text, Double)
        End Get
        Set(ByVal Value As Double)
            txtInstallmentDue.Text = FormatNumber(Value.ToString, 0)
        End Set
    End Property

    Public Property InstallmentDueDesc() As String
        Get
            Return CType(txtInstallmentDueDesc.Text, String)
        End Get
        Set(ByVal Value As String)
            txtInstallmentDueDesc.Text = Value
        End Set
    End Property

    Public Property TitipanAngsuran() As Double
        Get
            Return CType(txtTtpAngsuran.Text, Double)
        End Get
        Set(ByVal Value As Double)
            txtTtpAngsuran.Text = FormatNumber(Value.ToString, 0)
        End Set
    End Property

    Public Property TitipanAngsuranDesc() As String
        Get
            Return CType(txtTtpAngsuranDesc.Text, String)
        End Get
        Set(ByVal Value As String)
            txtTtpAngsuranDesc.Text = Value
        End Set
    End Property

    Public Property LCInstallment() As Double
        Get
            Return CType(txtInstallLC.Text, Double)
        End Get
        Set(ByVal Value As Double)
            txtInstallLC.Text = FormatNumber(Value.ToString, 0)
        End Set
    End Property

    Public Property LCInstallmentDesc() As String
        Get
            Return CType(txtInstallLCDesc.Text, String)
        End Get
        Set(ByVal Value As String)
            txtInstallLCDesc.Text = Value
        End Set
    End Property

    Public Property InstallmentCollFee() As Double
        Get
            Return CType(txtInstallCollFee.Text, Double)
        End Get
        Set(ByVal Value As Double)
            txtInstallCollFee.Text = FormatNumber(Value.ToString, 0)
        End Set
    End Property

    Public Property InstallmentCollFeeDesc() As String
        Get
            Return CType(txtInstallCollFeeDesc.Text, String)
        End Get
        Set(ByVal Value As String)
            txtInstallCollFeeDesc.Text = Value
        End Set
    End Property

    Public Property Penyetor() As String
        Get
            Return CType(txtPenyetor.Text, String)
        End Get
        Set(ByVal Value As String)
            txtPenyetor.Text = Value
        End Set
    End Property

    Public Property ToleransiBayarKurang As String
        Get
            Return CType(txtToleransiKurang.Text, String)
        End Get
        Set(value As String)
            txtToleransiKurang.Text = value.ToString
        End Set
    End Property
    Public Property ToleransiBayarLebih As String
        Get
            Return CType(txtToleransiLebih.Text, String)
        End Get
        Set(value As String)
            txtToleransiLebih.Text = value.ToString
        End Set
    End Property
#End Region

#Region "Insurance"
    Public Property InsuranceDue() As Double
        Get
            Return CType(txtInsuranceDue.Text, Double)
        End Get
        Set(ByVal Value As Double)
            txtInsuranceDue.Text = FormatNumber(Value.ToString, 0)
        End Set
    End Property

    Public Property InsuranceDueDesc() As String
        Get
            Return CType(txtInsuranceDueDesc.Text, String)
        End Get
        Set(ByVal Value As String)
            txtInsuranceDueDesc.Text = Value
        End Set
    End Property

    Public Property LCInsurance() As Double
        Get
            Return CType(txtInsuranceLC.Text, Double)
        End Get
        Set(ByVal Value As Double)
            txtInsuranceLC.Text = FormatNumber(Value.ToString, 0)
        End Set
    End Property

    Public Property LCInsuranceDesc() As String
        Get
            Return CType(txtInsuranceLCDesc.Text, String)
        End Get
        Set(ByVal Value As String)
            txtInsuranceLCDesc.Text = CStr(Value)
        End Set
    End Property

    Public Property InsuranceCollFee() As Double
        Get
            Return CType(txtInsuranceCollFee.Text, Double)
        End Get
        Set(ByVal Value As Double)
            txtInsuranceCollFee.Text = FormatNumber(Value.ToString, 0)
        End Set
    End Property

    Public Property InsuranceCollFeeDesc() As String
        Get
            Return CType(txtInsuranceCollFeeDesc.Text, String)
        End Get
        Set(ByVal Value As String)
            txtInsuranceCollFeeDesc.Text = CStr(Value)
        End Set
    End Property
#End Region

#Region "Other"
    Public Property PDCBounceFee() As Double
        Get
            Return CType(txtPDCBounceFee.Text, Double)
        End Get
        Set(ByVal Value As Double)
            txtPDCBounceFee.Text = FormatNumber(Value.ToString, 0)
        End Set
    End Property

    Public Property PDCBounceFeeDesc() As String
        Get
            Return CType(txtPDCBounceFeeDesc.Text, String)
        End Get
        Set(ByVal Value As String)
            txtPDCBounceFeeDesc.Text = CStr(Value)
        End Set
    End Property

    Public Property STNKRenewalFee() As Double
        Get
            Return CType(txtSTNKRenewalFee.Text, Double)
        End Get
        Set(ByVal Value As Double)
            txtSTNKRenewalFee.Text = FormatNumber(Value.ToString, 0)
        End Set
    End Property

    Public Property STNKRenewalDesc() As String
        Get
            Return CType(txtSTNKRenewalFeeDesc.Text, String)
        End Get
        Set(ByVal Value As String)
            txtSTNKRenewalFeeDesc.Text = CStr(Value)
        End Set
    End Property


    Public Property InsuranceClaimExpense() As Double
        Get
            Return CType(txtInsuranceClaimExpense.Text, Double)
        End Get
        Set(ByVal Value As Double)
            txtInsuranceClaimExpense.Text = FormatNumber(Value.ToString, 0)
        End Set
    End Property

    Public Property InsuranceClaimExpenseDesc() As String
        Get
            Return CType(txtInsuranceClaimExpenseDesc.Text, String)
        End Get
        Set(ByVal Value As String)
            txtInsuranceClaimExpenseDesc.Text = CStr(Value)
        End Set
    End Property
    Public Property RepossessionFee() As Double
        Get
            Return CType(txtRepossessionFee.Text, Double)
        End Get
        Set(ByVal Value As Double)
            txtRepossessionFee.Text = FormatNumber(Value.ToString, 0)
        End Set
    End Property

    Public Property RepossessionFeeDesc() As String
        Get
            Return CType(txtRepossessionFeeDesc.Text, String)
        End Get
        Set(ByVal Value As String)
            txtRepossessionFeeDesc.Text = CStr(Value)
        End Set
    End Property

    Public Property Prepaid() As Double
        Get
            Return CType(txtPrepaid.Text, Double)
        End Get
        Set(ByVal Value As Double)
            txtPrepaid.Text = FormatNumber(Value.ToString, 0)
        End Set
    End Property

    Public Property PrepaidDesc() As String
        Get
            Return CType(txtPrepaidDesc.Text, String)
        End Get
        Set(ByVal Value As String)
            txtPrepaidDesc.Text = CStr(Value)
        End Set
    End Property
    Public Property PLL As Double
        Get
            Return CType(txtPLL.Text, Double)
        End Get
        Set(value As Double)
            txtPLL.Text = FormatNumber(value.ToString, 0)
        End Set
    End Property
    Public Property PLLDesc As String
        Get
            Return CType(txtPLLDesc.Text, String)
        End Get
        Set(value As String)
            txtPLLDesc.Text = CStr(value.ToString)
        End Set
    End Property
#End Region

#Region "ApplicationID"
    Public Property DisabledAll() As Boolean
        Get
            Return CType(ViewState("DisabledAll"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            ViewState("DisabledAll") = Value
        End Set
    End Property
    Public Property DisableForPrepaidAllocation As Boolean
        Get
            Return CType(ViewState("DisableForPrepaidAllocation"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            ViewState("DisableForPrepaidAllocation") = Value
        End Set
    End Property
    Public Property DisableForTitipanAngsuran As Boolean
        Get
            Return CType(ViewState("DisableForTitipanAngsuran"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            ViewState("DisableForTitipanAngsuran") = Value
        End Set
    End Property
    Public Property DisableForGracePeriod As Boolean
        Get
            Return CType(ViewState("DisableForGracePeriod"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            ViewState("DisableForGracePeriod") = Value
        End Set
    End Property
#End Region

#End Region

#Region "Property Maximum Value"
    Public Property MaximumInstallment() As Double
        Get
            Return CType(ViewState("MaximumInstallment"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("MaximumInstallment") = Value
        End Set
    End Property

    Public Property MaximumInsurance() As Double
        Get
            Return CType(ViewState("MaximumInsurance"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("MaximumInsurance") = Value
        End Set
    End Property

    Public Property MaximumInstallCollFee() As Double
        Get
            Return CType(ViewState("MaximumInstallCollFee"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("MaximumInstallCollFee") = Value
        End Set
    End Property

    Public Property MaximumInsuranceCollFee() As Double
        Get
            Return CType(ViewState("MaximumInstallCollFee"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("MaximumInstallCollFee") = Value
        End Set
    End Property

    Public Property MaximumLCInstallFee() As Double
        Get
            Return CType(ViewState("MaximumLCInstallFee"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("MaximumLCInstallFee") = Value
        End Set
    End Property

    Public Property MaximumLCInsuranceFee() As Double
        Get
            Return CType(ViewState("MaximumLCInsuranceFee"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("MaximumLCInsuranceFee") = Value
        End Set
    End Property

    Public Property MaximumInsuranceClaimFee() As Double
        Get
            Return CType(ViewState("MaximumInsuranceClaimFee"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("MaximumInsuranceClaimFee") = Value
        End Set
    End Property

    Public Property MaximumSTNKRenewalFee() As Double
        Get
            Return CType(ViewState("MaximumSTNKRenewalFee"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("MaximumSTNKRenewalFee") = Value
        End Set
    End Property

    Public Property MaximumPDCBounceFee() As Double
        Get
            Return CType(ViewState("MaximumPDCBounceFee"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("MaximumPDCBounceFee") = Value
        End Set
    End Property

    Public Property MaximumReposessionFee() As Double
        Get
            Return CType(ViewState("MaximumReposessionFee"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("MaximumReposessionFee") = Value
        End Set
    End Property
#End Region

#Region "Field Tagihan -scl-"
    Public Property InstallmentDue0() As Double
        Get
            Return CType(txtInstallmentDue0.Text, Double)
        End Get
        Set(ByVal Value As Double)
            txtInstallmentDue0.Text = FormatNumber(Value.ToString, 0)
        End Set
    End Property
    Public Property TitipAngsuran0() As Double
        Get
            Return CType(txtTtpAngsuran0.Text, Double)
        End Get
        Set(ByVal Value As Double)
            txtTtpAngsuran0.Text = FormatNumber(Value.ToString, 0)
        End Set
    End Property
    Public Property LCInstallment0() As Double
        Get
            Return CType(txtInstallLC0.Text, Double)
        End Get
        Set(ByVal Value As Double)
            txtInstallLC0.Text = FormatNumber(Value.ToString, 0)
        End Set
    End Property

    Public Property InstallmentCollFee0() As Double
        Get
            Return CType(txtInstallCollFee0.Text, Double)
        End Get
        Set(ByVal Value As Double)
            txtInstallCollFee0.Text = FormatNumber(Value.ToString, 0)
        End Set
    End Property

    Public Property InsuranceDue0() As Double
        Get
            Return CType(txtInsuranceDue0.Text, Double)
        End Get
        Set(ByVal Value As Double)
            txtInsuranceDue0.Text = FormatNumber(Value.ToString, 0)
        End Set
    End Property

    Public Property LCInsurance0() As Double
        Get
            Return CType(txtInsuranceLC0.Text, Double)
        End Get
        Set(ByVal Value As Double)
            txtInsuranceLC0.Text = FormatNumber(Value.ToString, 0)
        End Set
    End Property

    Public Property InsuranceCollFee0() As Double
        Get
            Return CType(txtInsuranceCollFee0.Text, Double)
        End Get
        Set(ByVal Value As Double)
            txtInsuranceCollFee0.Text = FormatNumber(Value.ToString, 0)
        End Set
    End Property

    Public Property PDCBounceFee0() As Double
        Get
            Return CType(txtPDCBounceFee0.Text, Double)
        End Get
        Set(ByVal Value As Double)
            txtPDCBounceFee0.Text = FormatNumber(Value.ToString, 0)
        End Set
    End Property

    Public Property STNKRenewalFee0() As Double
        Get
            Return CType(txtSTNKRenewalFee0.Text, Double)
        End Get
        Set(ByVal Value As Double)
            txtSTNKRenewalFee0.Text = FormatNumber(Value.ToString, 0)
        End Set
    End Property

    Public Property InsuranceClaimExpense0() As Double
        Get
            Return CType(txtInsuranceClaimExpense0.Text, Double)
        End Get
        Set(ByVal Value As Double)
            txtInsuranceClaimExpense0.Text = FormatNumber(Value.ToString, 0)
        End Set
    End Property

    Public Property RepossessionFee0() As Double
        Get
            Return CType(txtRepossessionFee0.Text, Double)
        End Get
        Set(ByVal Value As Double)
            txtRepossessionFee0.Text = FormatNumber(Value.ToString, 0)
        End Set
    End Property

    Public Property Prepaid0() As Double
        Get
            Return CType(txtPrepaid0.Text, Double)
        End Get
        Set(ByVal Value As Double)
            txtPrepaid0.Text = FormatNumber(Value.ToString, 0)
        End Set
    End Property
    Public Property PLL0() As Double
        Get
            Return CType(txtPLL0.Text, Double)
        End Get
        Set(value As Double)
            txtPLL0.Text = FormatNumber(value.ToString, 0)
        End Set
    End Property
    Public Property totalTagihan() As Double
        Get
            Return CType(lblTotalTagihan.Text, Double)
        End Get
        Set(ByVal Value As Double)
            lblTotalTagihan.Text = FormatNumber(Value.ToString, 0)
        End Set
    End Property

    Public Property totalBayar() As Double
        Get
            Return CType(lblTotalDiterima.Text, Double)
        End Get
        Set(ByVal Value As Double)
            lblTotalDiterima.Text = FormatNumber(Value.ToString, 0)
        End Set
    End Property

    Public Property ShowPanelCashier() As Boolean
        Get
            Return CType(pnlKasir.Visible, Boolean)
        End Get
        Set(ByVal Value As Boolean)
            pnlKasir.Visible = Value
        End Set
    End Property
    Public Property isAkhirBayar() As Boolean
        Get
            Return txtisAkhirBayar.Text
        End Get
        Set(value As Boolean)
            txtisAkhirBayar.Text = value
        End Set
    End Property


#End Region

    Public Property PrioritasPembayaran As String
    Private Const PrioritasPembayaran_DE As String = "de"

#End Region

    Private Sub formatTextBoxOnload(ByVal txt As ucNumberFormat)
        If txt.Text <> String.Empty Then FormatNumber(txt.Text, 0)
    End Sub
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not IsPostBack Then
            formatTextBoxOnload(txtInstallmentDue)
            formatTextBoxOnload(txtTtpAngsuran)
            formatTextBoxOnload(txtInstallCollFee)
            formatTextBoxOnload(txtInsuranceDue)
            formatTextBoxOnload(txtInsuranceCollFee)
            formatTextBoxOnload(txtInstallLC)
            formatTextBoxOnload(txtInsuranceLC)
            formatTextBoxOnload(txtInsuranceClaimExpense)
            formatTextBoxOnload(txtRepossessionFee)
            formatTextBoxOnload(txtSTNKRenewalFee)
            formatTextBoxOnload(txtPDCBounceFee)
            formatTextBoxOnload(txtPrepaid)
            formatTextBoxOnload(txtGracePeriod)

            'txtInstallmentDue.Text = FormatNumber(txtInstallmentDue.Text, 0)
            '.Text = FormatNumber(txtInstallCollFee.Text, 0)

            '.Text = FormatNumber(txtInsuranceDue.Text, 0)
            '.Text = FormatNumber(txtInsuranceCollFee.Text, 0)

            '.Text = FormatNumber(txtInstallLC.Text, 0)
            '.Text = FormatNumber(txtInsuranceLC.Text, 0)

            '.Text = FormatNumber(txtInsuranceClaimExpense.Text, 0)
            '.Text = FormatNumber(txtRepossessionFee.Text, 0)
            '.Text = FormatNumber(txtSTNKRenewalFee.Text, 0)
            '.Text = FormatNumber(txtPDCBounceFee.Text, 0)
            '.Text = FormatNumber(txtPrepaid.Text, 0)

            'sementara dicomment dulu karena belum berlaku
            'If Not PrioritasPembayaran Is Nothing AndAlso PrioritasPembayaran.ToLower = PrioritasPembayaran_DE Then
            '    EnabledTxtInstallmentLC(True)

            'End If

            txtGracePeriod.Enabled = False
            txtGracePeriodDesc.Enabled = False
        End If
    End Sub

    Public Sub PaymentAllocationBind()


        'txtInstallmentDue.RangeValidatorMaximumValue = CStr(Me.MaximumInstallment)
        'txtInsuranceDue.RangeValidatorMaximumValue = CStr(Me.MaximumInsurance)
        txtInstallCollFee.RangeValidatorMaximumValue = CStr(Me.MaximumInstallCollFee)
        txtInsuranceCollFee.RangeValidatorMaximumValue = CStr(Me.MaximumInsuranceCollFee)
        txtInsuranceLC.RangeValidatorMaximumValue = CStr(Me.MaximumLCInsuranceFee)
        txtInstallLC.RangeValidatorMaximumValue = CStr(Me.MaximumLCInstallFee)
        'txtInstallLC.RangeValidatorMaximumValue = CStr(Me.LCInstallment)
        txtPDCBounceFee.RangeValidatorMaximumValue = CStr(Me.MaximumPDCBounceFee)
        txtSTNKRenewalFee.RangeValidatorMaximumValue = CStr(Me.MaximumSTNKRenewalFee)
        txtRepossessionFee.RangeValidatorMaximumValue = CStr(Me.MaximumReposessionFee)
        txtInsuranceClaimExpense.RangeValidatorMaximumValue = CStr(Me.MaximumInsuranceClaimFee)

        'txtInstallmentDue.RangeValidatorMaximumValue = Me.MaximumInstallment.ToString
        txtInstallmentDue.TextCssClass = "numberAlign"

        txtInsuranceDue.RangeValidatorMaximumValue = Me.MaximumInsurance.ToString
        txtInsuranceDue.TextCssClass = "numberAlign"

        txtInstallCollFee.RangeValidatorMaximumValue = Me.MaximumInstallCollFee.ToString
        txtInstallCollFee.TextCssClass = "numberAlign"

        txtInsuranceCollFee.RangeValidatorMaximumValue = Me.MaximumInsuranceCollFee.ToString
        txtInsuranceCollFee.TextCssClass = "numberAlign"

        txtInsuranceLC.RangeValidatorMaximumValue = Me.MaximumLCInsuranceFee.ToString
        txtInsuranceLC.TextCssClass = "numberAlign"

        txtInstallLC.RangeValidatorMaximumValue = Me.MaximumLCInstallFee.ToString
        'txtInstallLC.RangeValidatorMaximumValue = Me.LCInstallment.ToString
        txtInstallLC.TextCssClass = "numberAlign"

        txtPDCBounceFee.RangeValidatorMaximumValue = Me.MaximumPDCBounceFee.ToString
        txtPDCBounceFee.TextCssClass = "numberAlign"

        txtSTNKRenewalFee.RangeValidatorMaximumValue = Me.MaximumSTNKRenewalFee.ToString
        txtSTNKRenewalFee.TextCssClass = "numberAlign"

        txtRepossessionFee.RangeValidatorMaximumValue = Me.MaximumReposessionFee.ToString
        txtRepossessionFee.TextCssClass = "numberAlign"

        txtInsuranceClaimExpense.RangeValidatorMaximumValue = Me.MaximumInsuranceClaimFee.ToString
        txtInsuranceClaimExpense.TextCssClass = "numberAlign"

        txtPrepaid.TextCssClass = "numberAlign"
        txtUangDiterima.TextCssClass = "numberAlign"

        txtGracePeriod.TextCssClass = "numberAlign"

        If DisabledAll Then
            'modify nofi 03072018
            'txtTtpAngsuran.Enabled = False
            'txtTtpAngsuranDesc.Enabled = False
            txtTtpAngsuran.Enabled = True
            txtTtpAngsuranDesc.Enabled = True
            'txtInstallmentDue.Enabled = False
            'txtInstallmentDueDesc.Enabled = False
            txtInstallmentDue.Enabled = True
            txtInstallmentDueDesc.Enabled = True
            'txtInstallLC.Enabled = False
            'txtInstallLCDesc.Enabled = False
            txtInstallLC.Enabled = True
            txtInstallLCDesc.Enabled = True
            'txtInstallCollFee.Enabled = False
            'txtInstallCollFeeDesc.Enabled = False
            txtInstallCollFee.Enabled = True
            txtInstallCollFeeDesc.Enabled = True
            txtInsuranceDue.Enabled = False
            txtInsuranceDueDesc.Enabled = False
            txtInsuranceLC.Enabled = False
            txtInsuranceLCDesc.Enabled = False
            txtInsuranceCollFee.Enabled = False
            txtInsuranceCollFeeDesc.Enabled = False
            txtPDCBounceFee.Enabled = False
            txtPDCBounceFeeDesc.Enabled = False
            txtInsuranceClaimExpense.Enabled = False
            txtInsuranceClaimExpenseDesc.Enabled = False
            txtRepossessionFee.Enabled = False
            txtRepossessionFeeDesc.Enabled = False
            txtPrepaid.Enabled = True
            txtPrepaidDesc.Enabled = True
            txtSTNKRenewalFee.Enabled = False
            txtSTNKRenewalFeeDesc.Enabled = False
            txtPenyetor.Enabled = False
        End If
        If DisableForPrepaidAllocation Then
            txtPrepaid.Enabled = False
            txtPrepaidDesc.Enabled = False
        End If
        If DisableForTitipanAngsuran Then
            txtTtpAngsuran.Enabled = False
            txtTtpAngsuranDesc.Enabled = False
            txtPrepaid.Enabled = False
            txtPrepaidDesc.Enabled = False
            txtInstallmentDue.Enabled = False
            txtInstallmentDueDesc.Enabled = False
        End If

        If DisableForGracePeriod Then
            'modify nofi 05092018 karena gina mau bayar angsuran bandung login jakpus
            'txtTtpAngsuran.Enabled = False
            'txtTtpAngsuranDesc.Enabled = False
            'txtInstallmentDue.Enabled = False
            'txtInstallmentDueDesc.Enabled = False
            'txtInstallLC.Enabled = False
            'txtInstallLCDesc.Enabled = False
            'txtInstallCollFee.Enabled = False
            'txtInstallCollFeeDesc.Enabled = False
            'txtInsuranceDue.Enabled = False
            'txtInsuranceDueDesc.Enabled = False
            'txtInsuranceLC.Enabled = False
            'txtInsuranceLCDesc.Enabled = False
            'txtInsuranceCollFee.Enabled = False
            'txtInsuranceCollFeeDesc.Enabled = False
            'txtPDCBounceFee.Enabled = False
            'txtPDCBounceFeeDesc.Enabled = False
            'txtInsuranceClaimExpense.Enabled = False
            'txtInsuranceClaimExpenseDesc.Enabled = False
            'txtRepossessionFee.Enabled = False
            'txtRepossessionFeeDesc.Enabled = False
            'txtPrepaid.Enabled = True
            'txtPrepaidDesc.Enabled = True
            'txtSTNKRenewalFee.Enabled = False
            'txtSTNKRenewalFeeDesc.Enabled = False
            'txtPenyetor.Enabled = False
            'txtPrepaid.Enabled = False
            'txtPrepaidDesc.Enabled = False
            'txtGracePeriod.Enabled = True
            'txtGracePeriodDesc.Enabled = True

            txtTtpAngsuran.Enabled = True
            txtTtpAngsuranDesc.Enabled = True
            txtInstallmentDue.Enabled = True
            txtInstallmentDueDesc.Enabled = True
            txtInstallLC.Enabled = True
            txtInstallLCDesc.Enabled = True
            txtInstallCollFee.Enabled = True
            txtInstallCollFeeDesc.Enabled = True
            txtInsuranceDue.Enabled = True
            txtInsuranceDueDesc.Enabled = True
            txtInsuranceLC.Enabled = True
            txtInsuranceLCDesc.Enabled = True
            txtInsuranceCollFee.Enabled = True
            txtInsuranceCollFeeDesc.Enabled = True
            txtPDCBounceFee.Enabled = True
            txtPDCBounceFeeDesc.Enabled = True
            txtInsuranceClaimExpense.Enabled = True
            txtInsuranceClaimExpenseDesc.Enabled = True
            txtRepossessionFee.Enabled = True
            txtRepossessionFeeDesc.Enabled = True
            txtPrepaid.Enabled = True
            txtPrepaidDesc.Enabled = True
            txtSTNKRenewalFee.Enabled = True
            txtSTNKRenewalFeeDesc.Enabled = True
            txtPenyetor.Enabled = True
            txtPrepaid.Enabled = True
            txtPrepaidDesc.Enabled = True
            txtGracePeriod.Enabled = True
            txtGracePeriodDesc.Enabled = True

        End If

    End Sub

    Private Sub EnabledTxtInstallmentLC(ByVal isDenda As Boolean)
        txtInstallLC.isReadOnly = isDenda
        txtInstallLC.Text = LCInstallment0.ToString
    End Sub



    Public Function GetPDCDetails(pram As Parameter.PDCParam) As IList(Of Parameter.PDCParam)
        Dim result As IList(Of Parameter.PDCParam) = New List(Of Parameter.PDCParam)
        Dim seq As Integer = 1
        Dim ComponentList = New List(Of Components)
        createComponent(ComponentList)
        For Each item In ComponentList
            If (CDec(item.AmountT.Text) > 0) Then
                result.Add(buidParam(seq, item.Amounts, item.Desct.Text, item.InstId, pram))
                seq += 1
            End If
        Next
        Return result
    End Function

    Private Sub createComponent(ComponentList As IList(Of Components))

        ComponentList.Add(New Components() With {.AmountT = txtTtpAngsuran, .Desct = txtTtpAngsuranDesc, .InstId = "INSTDEP"})

        ComponentList.Add(New Components() With {.AmountT = txtInstallmentDue, .Desct = txtInstallmentDueDesc, .InstId = "INSTALLRCV"})
        ComponentList.Add(New Components() With {.AmountT = txtInstallLC, .Desct = txtInstallLCDesc, .InstId = "LCINSTALL"})
        ComponentList.Add(New Components() With {.AmountT = txtInstallCollFee, .Desct = txtInstallCollFeeDesc, .InstId = "INSTCOLFEE"})

        ComponentList.Add(New Components() With {.AmountT = txtInsuranceDue, .Desct = txtInsuranceDueDesc, .InstId = "INSURRCV"})
        ComponentList.Add(New Components() With {.AmountT = txtInsuranceLC, .Desct = txtInsuranceLCDesc, .InstId = "LCINSUR"})
        ComponentList.Add(New Components() With {.AmountT = txtInsuranceCollFee, .Desct = txtInsuranceCollFeeDesc, .InstId = "INSRCOLFEE"})
        ComponentList.Add(New Components() With {.AmountT = txtPDCBounceFee, .Desct = txtPDCBounceFeeDesc, .InstId = "PDCBNCFEE"})
        ComponentList.Add(New Components() With {.AmountT = txtSTNKRenewalFee, .Desct = txtSTNKRenewalFeeDesc, .InstId = "ARSTNK"})
        ComponentList.Add(New Components() With {.AmountT = txtInsuranceClaimExpense, .Desct = txtInsuranceClaimExpenseDesc, .InstId = "INSRCLMRCV"})
        ComponentList.Add(New Components() With {.AmountT = txtRepossessionFee, .Desct = txtRepossessionFeeDesc, .InstId = "REPOFEE"})
        ComponentList.Add(New Components() With {.AmountT = txtPrepaid, .Desct = txtPrepaidDesc, .InstId = "PREPAID"})
        ComponentList.Add(New Components() With {.AmountT = txtPLL, .Desct = txtPLLDesc, .InstId = "PLL"})


    End Sub

    Public Function CountTotalBayar() As Decimal
        Dim ComponentList = New List(Of Components)
        createComponent(ComponentList)
        Return ComponentList.Sum(Function(x) x.Amounts)
    End Function

    Function buidParam(seq As Integer, amount As Decimal, desc As String, payAllId As String, pram As Parameter.PDCParam) As Parameter.PDCParam

        Return New Parameter.PDCParam With {.seq = seq, .Holiday = "", .BankPDC = pram.BankPDC, .BankID = pram.BankID, .PDCNo = pram.PDCNo,
                                            .PDCAmount = amount, .DueDate = pram.DueDate, .IsInkaso = pram.IsInkaso, .IsCummulative = pram.IsCummulative,
                                            .Type = pram.Type, .Description = desc, .PaymentAllocationID = payAllId}
    End Function


End Class


Public Class Components
    Public Property AmountT As ucNumberFormat
    Public ReadOnly Property Amounts As Decimal
        Get
            Return CDec(AmountT.Text)
        End Get
    End Property
    Public Property Desct As TextBox
    Public Property InstId As String
End Class