﻿Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.Parameter

Public Class ucFundingReport
    Inherits ControlBased

    Private m_controller As New DataUserControlController
    Public Event TextChanged As TextChangedHandler
    Public Delegate Sub TextChangedHandler(ByVal sender As Object, ByVal e As System.EventArgs)

#Region " Private Const "    
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region

#Region "Properties"
    Public Property IsKonsumen() As Boolean
        Get
            Return CType(ViewState("Konsumen"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            ViewState("Konsumen") = Value
        End Set
    End Property

    Public Property BankCompany() As String
        Get
            Return CType(cboBankCompany.SelectedItem.Value, String)
        End Get
        Set(ByVal Value As String)
            cboBankCompany.SelectedIndex = cboBankCompany.Items.IndexOf(cboBankCompany.Items.FindByValue(Value))
        End Set
    End Property

    Public Property Kontrak() As String
        Get
            Return CType(cboKontrak.SelectedItem.Value, String)
        End Get
        Set(ByVal Value As String)
            cboKontrak.SelectedIndex = cboKontrak.Items.IndexOf(cboKontrak.Items.FindByValue(Value))
        End Set
    End Property

    Public Property NoBatch() As String
        Get
            Return CType(cboNoBatch.SelectedItem.Value, String)
        End Get
        Set(ByVal Value As String)
            cboNoBatch.SelectedIndex = cboNoBatch.Items.IndexOf(cboNoBatch.Items.FindByValue(Value))
        End Set
    End Property

    Public Property NamaKonsumen As String
        Get
            Return txtNamaKonsumen.Text.Trim
        End Get
        Set(ByVal value As String)
            txtNamaKonsumen.Text = value
        End Set
    End Property

    Public Property NamaID As String
        Get
            Return txtEmployeeID.Text.Trim()
        End Get
        Set(ByVal value As String)
            txtEmployeeID.Text = value
        End Set
    End Property

#End Region

    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then

            bindComboCompany()
            bindComboContract()
            bindComboBatch()

            If Me.IsKonsumen = True Then
                dvkonsumen.Visible = True
            Else
                dvkonsumen.Visible = False
            End If

        End If

    End Sub

    Sub bindComboCompany()
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim customClassx As New GeneralPagingController
        Dim customClass As New Parameter.GeneralPaging

        With customClass
            .strConnection = GetConnectionString()
            .WhereCond = ""
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = ""
            .SpName = "spFundingGetComboCompany"
        End With
        customClass = customClassx.GetGeneralPaging(customClass)

        dtsEntity = customClass.ListData
        dtvEntity = dtsEntity.DefaultView
        cboBankCompany.DataValueField = "FundingCoyId"
        cboBankCompany.DataTextField = "FundingCoyName"
        cboBankCompany.DataSource = dtvEntity
        cboBankCompany.DataBind()
        cboBankCompany.Items.Insert(0, "Select One")
        cboBankCompany.Items(0).Value = "0"

    End Sub

    Sub bindComboContract()
        Dim oClass As New Parameter.ControlsRS
        With oClass
            .strConnection = GetConnectionString()
            .CoyID = cboBankCompany.SelectedValue
        End With
        oClass = m_controller.GetReportComboContract(oClass)
        Dim dt As New DataTable
        dt = oClass.ListData
        cboKontrak.DataSource = dt
        cboKontrak.DataTextField = "NamaKontrak"
        cboKontrak.DataValueField = "FundingContractNo"
        cboKontrak.DataBind()
        cboKontrak.Items.Insert(0, "Select One")
        cboKontrak.Items(0).Value = "0"
    End Sub

    Sub bindComboBatch()
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim customClassx As New GeneralPagingController
        Dim customClass As New Parameter.GeneralPaging
        With customClass
            .strConnection = GetConnectionString()
            .WhereCond = "WHERE fundingCoyID = '" & cboBankCompany.SelectedValue & "' AND FundingContractNo = '" & cboKontrak.SelectedValue & "' "
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = ""
            .SpName = "spGetReportComboBatch"
        End With

        customClass = customClassx.GetGeneralPaging(customClass)

        dtsEntity = customClass.ListData
        dtvEntity = dtsEntity.DefaultView        
        cboNoBatch.DataValueField = "FundingBatchNo"
        cboNoBatch.DataTextField = "FundingBatchNo"
        cboNoBatch.DataSource = dtvEntity
        cboNoBatch.DataBind()
        cboNoBatch.Items.Insert(0, "Select One")
        cboNoBatch.Items(0).Value = "0"
    End Sub

    Private Sub cboBankCompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBankCompany.SelectedIndexChanged
        bindComboContract()
        bindComboBatch()
        Session.Add("ucBank", cboBankCompany.SelectedValue.ToString.Trim())
        Session.Add("ucKontrak", cboKontrak.SelectedValue.ToString.Trim())
        Session.Add("ucBatch", cboNoBatch.SelectedValue.ToString.Trim())        
    End Sub

    Private Sub cboKontrak_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboKontrak.SelectedIndexChanged        
        bindComboBatch()
        Session.Add("ucBank", cboBankCompany.SelectedValue.ToString.Trim())
        Session.Add("ucKontrak", cboKontrak.SelectedValue.ToString.Trim())
        Session.Add("ucBatch", cboNoBatch.SelectedValue.ToString.Trim())
    End Sub

    Private Sub cboNoBatch_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboNoBatch.SelectedIndexChanged
        Session.Add("ucBank", cboBankCompany.SelectedValue.ToString.Trim())
        Session.Add("ucKontrak", cboKontrak.SelectedValue.ToString.Trim())
        Session.Add("ucBatch", cboNoBatch.SelectedValue.ToString.Trim())
    End Sub

    Private Sub txtNamaKonsumen_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNamaKonsumen.TextChanged
        RaiseEvent TextChanged(sender, e)
    End Sub

End Class