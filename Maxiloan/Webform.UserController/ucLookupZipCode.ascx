﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucLookupZipCode.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.ucLookupZipCode" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:UpdatePanel runat="server" ID="upSelectedZipCode">
    <ContentTemplate>
        <div class="form_box_usercontrol">
            <div class="form_left_usercontrol">
                <label class="label_split">
                    Kelurahan</label>
                <asp:TextBox ID="txtKelurahan" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="form_box_usercontrol">
            <div class="form_left_usercontrol">
                <label class="label_split">
                    Kecamatan</label>
                <asp:TextBox ID="txtKecamatan" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="form_box_usercontrol">
            <div class="form_left_usercontrol">
                <label class="label_split">
                    Kota</label>
                <asp:TextBox ID="txtCity" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="form_box_usercontrol">
            <div class="form_left_usercontrol">
                <label class="label_split_req" runat="server" id="lblKodePos">
                    Kode Pos</label>
                <asp:TextBox ID="txtZipCode" runat="server" CssClass="small_text"></asp:TextBox>
                <asp:Button runat="server" ID="btnLookup" CausesValidation="false" Text="..." CssClass="small buttongo blue" />
                <asp:Button runat="server" ID="btnReset" CausesValidation="false" Text="Reset" CssClass="small buttongo gray" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Harap pilih KodePos"
                    ControlToValidate="txtZipCode" CssClass="validator_general" SetFocusOnError="true"></asp:RequiredFieldValidator>
            </div>
        </div>
        <asp:ModalPopupExtender runat="server" ID="mpLookupZipCode" PopupControlID="Panel1"
            TargetControlID="btnLookup" BackgroundCssClass="wpbg" CancelControlID="btnExit">
        </asp:ModalPopupExtender>
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="dtgPaging" EventName="SelectedIndexChanged" />
        <asp:AsyncPostBackTrigger ControlID="buttonReset" EventName="Click" />
    </Triggers>
</asp:UpdatePanel>
<asp:UpdatePanel runat="server" ID="upZipCodeList">
    <ContentTemplate>
        <asp:Panel runat="server" ID="Panel1">
            <div class="wp">
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
                <asp:ImageButton runat="server" ID="btnExit" ImageUrl="../Images/exit_lookup00.png"
                    CssClass="wpbtnexit" />
                <asp:Panel ID="pnlGrid" runat="server">
                    <div class="form_box_title">
                        <div class="title_strip">
                        </div>
                        <div class="form_single">
                            <h4>
                                DAFTAR KODE POS
                            </h4>
                        </div>
                    </div>
                    <div class="form_box_header">
                        <div class="form_single">
                            <div class="grid_wrapper_ws">
                                <asp:DataGrid ID="dtgPaging" runat="server" AutoGenerateColumns="False" DataKeyField="ZIPCODE"
                                    Width="800px" CssClass="grid_general" >
                                    <HeaderStyle CssClass="th" />
                                    <ItemStyle CssClass="item_grid" />
                                    <PagerStyle CssClass="item_grid" />
                                    <Columns>
                                        <asp:ButtonColumn CommandName="SELECT" Text="SELECT" CausesValidation="false"></asp:ButtonColumn>
                                        <asp:BoundColumn DataField="KELURAHAN" HeaderText="KELURAHAN">
                                            <HeaderStyle Font-Bold="True" HorizontalAlign="Center"></HeaderStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="KECAMATAN" HeaderText="KECAMATAN">
                                            <HeaderStyle Font-Bold="True" HorizontalAlign="Center"></HeaderStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="CITY" HeaderText="CITY">
                                            <HeaderStyle Font-Bold="True" HorizontalAlign="Center"></HeaderStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="ZIPCODE" HeaderText="ZIP CODE">
                                            <HeaderStyle Font-Bold="True" HorizontalAlign="Center"></HeaderStyle>
                                        </asp:BoundColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </div>
                            <div style="display: inline">
                                <div class="button_gridnavigation">
                                    <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../Images/grid_navbutton01.png"
                                        CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                                    </asp:ImageButton>
                                    <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../Images/grid_navbutton02.png"
                                        CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                                    </asp:ImageButton>
                                    <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../Images/grid_navbutton03.png"
                                        CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                                    </asp:ImageButton>
                                    <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../Images/grid_navbutton04.png"
                                        CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                                    </asp:ImageButton>
                                    Page
                                    <asp:TextBox ID="txtGoPage" runat="server" Width="34px">1</asp:TextBox>
                                    <asp:Button ID="btnGoPage" runat="server" Text="Go" CssClass="small buttongo blue" CausesValidation="false" 
                                        EnableViewState="False" />                                    
                                </div>
                                <div class="label_gridnavigation">
                                    <asp:Label ID="lblPage" runat="server"></asp:Label>of
                                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                                    <asp:Label ID="lblTotRec" runat="server"></asp:Label>record(s)
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel runat="server" ID="PanelSearch">
                    <div class="form_box_title">
                        <div class="form_single">
                            <h4>
                                CARI KODE POS</h4>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label>
                                Kota</label>
                            <asp:DropDownList ID="CboCity" runat="server">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Please Choose City"
                                Display="Dynamic" ControlToValidate="CboCity" Enabled="False" InitialValue="0"
                                CssClass="validator_general"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label>
                                Cari Berdasarkan</label>
                            <asp:DropDownList ID="cboSearchBy" runat="server">
                                <asp:ListItem Value=""></asp:ListItem>
                                <asp:ListItem Value="Kelurahan">Kelurahan</asp:ListItem>
                                <asp:ListItem Value="Kecamatan">Kecamatan</asp:ListItem>
                                <asp:ListItem Value="ZipCode">Zip Code</asp:ListItem>
                            </asp:DropDownList>
                            <asp:TextBox ID="txtSearch" runat="server" onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form_button">
                        <asp:Button ID="buttonSearch" runat="server" Text="Find" CssClass="small button blue"
                            CausesValidation="false" />
                        <asp:Button ID="buttonReset" runat="server" Text="Reset" CssClass="small button gray"
                            CausesValidation="false" />
                    </div>
                </asp:Panel>
            </div>
        </asp:Panel>
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="buttonSearch" EventName="Click" />
    </Triggers>
</asp:UpdatePanel>
