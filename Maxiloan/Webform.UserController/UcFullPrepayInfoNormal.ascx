﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UcFullPrepayInfoNormal.ascx.vb" Inherits="Maxiloan.Webform.UserController.UcFullPrepayInfoNormal" %>
<div class="form_box_header">
    <div class="form_single">
        <h4>
            TOTAL PELUNASAN NORMAL
        </h4>
    </div>
</div>
<div class="form_box">
    <div>
        <div class="form_left">
            <label>
                Angsuran
            </label>
            <asp:Label ID="lblAngsuran" runat="server" CssClass="numberAlign label"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Biaya Tolakan PDC
            </label>
            <asp:Label ID="lblBiayaTolakanPDC" runat="server" CssClass="numberAlign label"></asp:Label>
        </div>
    </div>
</div>
<div class="form_box">
    <div>
        <div class="form_left">
            <label>
                Denda
            </label>
            <asp:Label ID="lblDenda" runat="server" CssClass="numberAlign label"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                STNK
            </label>
            <asp:Label ID="lblSTNK" runat="server" CssClass="numberAlign label"></asp:Label>
        </div>
    </div>
</div>
<div class="form_box">
    <div>
        <div class="form_left">
            <label>
                Biaya Tagih Angsuran
            </label>
            <asp:Label ID="lblBiayaTagihAngsuran" runat="server" CssClass="numberAlign label"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Biaya Tarik
            </label>
            <asp:Label ID="lblBiayaTarik" runat="server" CssClass="numberAlign label"></asp:Label>
        </div>
    </div>
</div>
<div class="form_box" >
    <div>
        <div class="form_left"></div>
        <div class="form_right" style="background-color:#FEFF51;font-weight:bold">
            <label>
                Total Pelunasan Normal
            </label>
            <asp:Label ID="lblTotalPelunasanNormal" runat="server" CssClass="numberAlign label"></asp:Label>
        </div>
    </div>
</div>