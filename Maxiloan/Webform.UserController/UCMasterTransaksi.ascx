﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UCMasterTransaksi.ascx.vb" Inherits="Maxiloan.Webform.UserController.UCMasterTransaksi" %>

<link href="../include/collection.css" type="text/css" rel="stylesheet"/>
<div id="Header" runat="server">
</div>

<div class="form_box_usercontrol">
    <div class="form_left_usercontrol">
        <label>
            Master Transaksi</label>
        <asp:DropDownList ID="cboParent" runat="server">
        </asp:DropDownList>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" InitialValue="0"
            Display="Dynamic" ControlToValidate="cboParent" ErrorMessage="Fill Master Transaksi ID"
            CssClass="validator_general"></asp:RequiredFieldValidator>
    </div>
</div>