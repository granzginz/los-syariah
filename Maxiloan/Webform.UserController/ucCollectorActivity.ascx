﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucCollectorActivity.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.ucCollectorActivity" %>
<%@ Register TagPrefix="uc1" TagName="ValidDate" Src="ValidDate.ascx" %>
<div id="Header" runat="server">
</div>
<script language="JavaScript" type="text/javascript">
    var hdnDetail;
    var ihdnDetail;
    var ocboChild;

    function WOPChange(pCmbOfPayment, pBankAccount, pHdnDetail, itemArray) {
        hdnDetail = eval('document.forms[0].' + pHdnDetail);

        eval('document.forms[0].' + pBankAccount).disabled = false;

        var i, j;
        for (i = eval('document.forms[0].' + pBankAccount).options.length; i >= 0; i--) {
            eval('document.forms[0].' + pBankAccount).options[i] = null

        };
        if (itemArray == null) {
            j = 0;
        }
        else {
            j = 1;
        };
        eval('document.forms[0].' + pBankAccount).options[0] = new Option('Select One');
        eval('document.forms[0].' + pBankAccount).options[0].value = '0';
        if (itemArray != null) {
            for (i = 0; i < itemArray.length; i++) {
                eval('document.forms[0].' + pBankAccount).options[j] = new Option(itemArray[i][0]);
                if (itemArray[i][1] != null) {
                    eval('document.forms[0].' + pBankAccount).options[j].value = itemArray[i][1];
                };
                j++;
            };
            eval('document.forms[0].' + pBankAccount).selected = true;
        };
    }



    function cboChildonChange(selIndex, phdnIndexResult, l, phdnResult) {
//        alert(phdnResult);
//        alert(phdnIndexResult);
        var xhdnResult = document.getElementById(phdnResult);
//        hdnDetail = eval('document.forms[0].' + phdnResult);
        hdnDetail = eval(xhdnResult);        
        ihdnDetail = eval('document.forms[0].' + phdnIndexResult);
        hdnDetail.value = l;
        eval('document.forms[0].' + hdnIndexResult).value = selIndex;
    }

    function SetcboChildFocus() {
        var SPV;
        SPV = hdnDetail.value;
        alert(SPV);

        for (i = document.forms[0].cboChild.options.length; i >= 0; i--) {
            if (document.forms[0].cboChild.options.options[i].value = SPV) {
                document.forms[0].cboChild.options[i].selected = true;
            }
        };
    }
    function RestoreInsuranceCoIndex(InsuranceIndex, phdnIndexResult, pCboChild) {
        ocboChild = eval('document.forms[0].' + pCboChild);
        if (ocboChild != null)
            if (eval('document.forms[0].' + hdnIndexResult).value != null) {
                if (BankAccountIndex != null)
                    ocboChild.selectedIndex = InsuranceIndex;
            }
    }    
</script>
<input id="hdnSP" runat="server" type="hidden" name="hdnSP" />
<input id="hdnResult" runat="server" type="hidden" name="hdnResult" />
<input id="hdnDetail" runat="server" type="hidden" name="hdnDetail" />
<input id="hdnIndexResult" runat="server" type="hidden" name="hdnIxdResult" />
<div class="form_box_usercontrol">
    <div class="form_left_usercontrol">
        <label>
            Collection Group</label>
        <asp:DropDownList ID="cboParent" runat="server" onchange="<%#WOPonChange()%>">
        </asp:DropDownList>

<%--         <asp:DropDownList ID="cboParent" runat="server" AutoPostBack="true">
        </asp:DropDownList>--%>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" InitialValue="0"
            Display="Dynamic" ControlToValidate="cboParent" ErrorMessage="*" CssClass="validator_general"></asp:RequiredFieldValidator>
    </div>
</div>
<div class="form_box_usercontrol">
    <div class="form_left_usercontrol">
        <label>
            Collector</label>
        <asp:DropDownList ID="cboChild" runat="server" onchange="<%#ChildChange()%>">
        </asp:DropDownList>

<%--        <asp:DropDownList ID="cboChild" runat="server" onchange="<%#ChildChange()%>">
        </asp:DropDownList>--%>
    </div>
</div>
<div id="divInnerHTML" runat="server">
</div>
