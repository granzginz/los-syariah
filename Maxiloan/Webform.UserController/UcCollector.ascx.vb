﻿#Region "Import"

Imports Maxiloan.BusinessProcess
Imports Maxiloan.Interface
Imports Maxiloan.cbse
Imports Maxiloan.Controller
#End Region

Public Class UcCollector
    Inherits ControlBased

#Region "properties"
    Public Property cgid() As String
        Get
            Return CType(ViewState("CGID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CGID") = Value
        End Set
    End Property

    Public Property SupervisorID() As String
        Get
            Return CType(ViewState("SupervisorID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("SupervisorID") = Value
        End Set
    End Property

    Public Property CollectorID() As String
        Get
            Return cboCollector.SelectedItem.Value
        End Get
        Set(ByVal Value As String)
            ViewState("CollectorID") = Value
        End Set
    End Property

    Public Property CollectorName() As String
        Get
            Return cboCollector.SelectedItem.Text
        End Get
        Set(ByVal Value As String)
            ViewState("CollectorName") = Value
        End Set
    End Property

    Public Property CollectorType() As String
        Get
            Return CType(ViewState("CollectorType"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CollectorType") = Value
        End Set
    End Property

    Public Property WhereCollectorType() As String
        Get
            Return CType(ViewState("WhereCollectorType"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("WhereCollectorType") = Value
        End Set
    End Property


    Public Property strKey() As String
        Get
            Return CType(ViewState("strKey"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("strKey") = Value
        End Set
    End Property


#End Region

    Private oController As New DataUserControlController

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
    End Sub

    Public Sub BindCollectorSPV()
        Dim coll As New Parameter.Collector

        With coll
            .strConnection = GetConnectionString()
            .CGID = Me.cgid
            .Supervisor = Me.SupervisorID
            .WhereCollectorType = Me.WhereCollectorType
        End With

        coll = oController.GetCollectorSPVAct(coll)

        Dim dt As New DataTable
        dt = coll.ListData

        cboCollector.DataSource = dt
        cboCollector.DataTextField = "Name"
        cboCollector.DataValueField = "ID"
        cboCollector.DataBind()
        cboCollector.Items.Insert(0, "Select One")
        cboCollector.Items(0).Value = "0"
    End Sub

End Class