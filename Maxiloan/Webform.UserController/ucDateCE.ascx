﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucDateCE.ascx.vb" Inherits="Maxiloan.Webform.UserController.ucDateCE" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:TextBox runat="server" ID="txtDateCE" CssClass="small_text"></asp:TextBox>
<%--<label class="label_auto text_clue">
    (dd/MM/yyyy)
</label>--%>
<asp:CalendarExtender runat="server" ID="ceDate" TargetControlID="txtDateCE" Format="dd/MM/yyyy">
</asp:CalendarExtender>
<asp:RequiredFieldValidator runat="server" ID="rfvDateCE" ControlToValidate="txtDateCE"
    Display="Dynamic" ErrorMessage="Tanggal harus diisi!" CssClass="validator_general"
    SetFocusOnError="true" Enabled="false">
</asp:RequiredFieldValidator>
<%--Custom format "dd/MM/yyyy" wajib pakai ConvertDate2 dan ConvertDate pada code behind dan web.config en-US--%>
<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Penulisan tanggal salah format, input dengan format dd/MM/yyyy!"
    ValidationExpression="(0[1-9]|[12][0-9]|3[01])/(0[1-9]|1[012])/\d{4}" ControlToValidate="txtDateCE"
    SetFocusOnError="true" Display="Dynamic" CssClass="validator_general"> 
</asp:RegularExpressionValidator>
<%--validator untuk format default "dd/MM/yyyy" tidak boleh pakai ConvertDate2 dan web.Config en-GB--%>
<%--<asp:CompareValidator ID="CompareValidator1" runat="server" CultureInvariantValues="True"
    Type="Date" ControlToValidate="txtAOFirstDate" Operator="DataTypeCheck" ErrorMessage="Format tanggal salah!"
    CssClass="validator_general">
</asp:CompareValidator>--%>
