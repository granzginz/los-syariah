﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucRefinancing.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.ucRefinancing" %>
<%@ Register TagPrefix="uc1" TagName="ucNumberFormat" Src="ucNumberFormat.ascx" %>

<script src="../Maxiloan.js" type="text/javascript"></script>
<link href="../include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
<script src="../js/jquery-1.9.1.min.js"></script>
<script src="../js/jquery-ui-1.10.3.custom.min.js"></script>

<div runat="server" id="jlookupContent" />

<asp:HiddenField runat="server" ID="hdnAppID" />
<asp:HiddenField runat="server" ID="hdnRefinancingName" />
<div class="form_left">
    <label class="label_req">
        No Kontrak Pelunasan</label>
    <asp:TextBox ID="txtAgreementNo" runat="server" Enabled="false" CssClass="medium_text"></asp:TextBox>
    <button class="small buttongo blue" onclick="OpenJLookup('<%= ResolveClientUrl("~/webform.Utility/jLookup/Refinancing.aspx?AppID=" & hdnAppID.ClientID & "&nama=" & hdnRefinancingName.ClientID & "&amount=" & txtNilaiPelunasan.ClientID & "&AgreementNo=" & txtAgreementNo.ClientID) %>','Daftar Applikasi','<%= jlookupContent.ClientID %>');return false;">
        ...</button>
</div>
<div class="form_right">
    <div class="form_single">
        <label>
            Nilai Pelunasan</label>        
        <asp:TextBox isReadOnly="true" Enabled="false" runat="server" maxlength="15" ID="txtNilaiPelunasan" 
        onblur="extractNumber(this,2,true);blockInvalid(this);this.value=numberFormat(this.value);this.value=blankToZero(this.value);"
        onkeyup="extractNumber(this,2,true);" onkeypress="return blockNonNumbers(this, event, true, true);"
        onfocus="this.value=resetNumber(this.value);" CssClass="numberAlign" OnChange="javascript:void(0)">0</asp:TextBox>
    </div>
</div>
