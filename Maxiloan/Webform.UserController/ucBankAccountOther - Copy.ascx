﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucBankAccountOther.ascx.vb" Inherits="Maxiloan.Webform.UserController.ucBankAccountOther" %>
    <div runat="server" id="jlookupContent" />
    <div class="form_box_usercontrol">
            <div class="form_left_usercontrol">
                <label class="label_split_req" runat="server" id="lblBankAccountID">
                    Transfer Rekening Cabang</label> <label runat="server" id="lblBankAccountIDNotValidate">
                    Transfer Rekening Cabang</label>
                <asp:TextBox ID="txtBankAccountID" runat="server" CssClass="small_text" Enabled="false" visible="false"></asp:TextBox>
                <asp:TextBox ID="txtBankAccountName" runat="server" CssClass="long_text"></asp:TextBox>
                   <button class="small buttongo blue" 
                            onclick ="OpenJLookup('<%= ResolveClientUrl("~/webform.Utility/jLookup/LookupBankAccountOther.aspx?BankAccountID=" & txtBankAccountID.ClientID & "&BankAccountName=" & txtBankAccountName.ClientID & "&AccountName=" & txtAccountName.ClientID & "&AccountNo=" & txtAccountNo.ClientID) %>','Kode Bank Account','<%= jlookupContent.ClientID %>');return false;">...</button>  
                <asp:Button runat="server" ID="btnReset1" CausesValidation="false" Text="Reset" CssClass="small buttongo gray" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Harap pilih"
                    ControlToValidate="txtBankAccountID" CssClass="validator_general" SetFocusOnError="true"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box_usercontrol">
            <div class="form_left_usercontrol">
                <label class="label_split">
                    Nama Rekening</label>
                <asp:TextBox ID="txtAccountName" runat="server" CssClass="long_text"></asp:TextBox>
            </div>
        </div>
        <div class="form_box_usercontrol">
            <div class="form_left_usercontrol">
                <label class="label_split">
                    Nomor Rekening</label>
                <asp:TextBox ID="txtAccountNo" runat="server" CssClass="long_text"></asp:TextBox>
            </div>
        </div>
        



