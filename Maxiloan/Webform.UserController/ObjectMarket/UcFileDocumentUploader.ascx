﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UcFileDocumentUploader.ascx.vb" Inherits="Maxiloan.Webform.UserController.UcFileDocumentUploader" %>

<asp:FileUpload ID="files" runat="server" ViewStateMode="Enabled" />
<asp:RequiredFieldValidator ID="RequiredFieldValidator" runat="server" Display="Dynamic" ControlToValidate="files" ErrorMessage="File Harus Di Isi !"
	CssClass="validator_general"></asp:RequiredFieldValidator>