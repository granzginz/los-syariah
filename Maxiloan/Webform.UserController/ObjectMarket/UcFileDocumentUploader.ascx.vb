﻿Imports Maxiloan.Controller

Public Class UcFileDocumentUploader
    Inherits System.Web.UI.UserControl

    Public Property pathFolder As String

    Public Property FileUploadObj() As FileUpload
        Get
            Return files
        End Get
        Set(ByVal value As FileUpload)
            files = value
        End Set
    End Property

    Public Function fileUploadCheck() As Boolean
        If files.PostedFile Is Nothing Then Return False
        If files.PostedFile.ContentLength = 0 Then Return False
        Return True
    End Function

    Public Property IsValidationRequired() As Boolean
        Get
            Return RequiredFieldValidator.Enabled
        End Get
        Set(ByVal value As Boolean)
            RequiredFieldValidator.Enabled = value
        End Set
    End Property

    Public Function Upload(ByVal Name As String,ByVal strConnection As String) As Boolean
        If fileUploadCheck() Then
            Try


                'Dim appPath As String = HttpContext.Current.Request.ApplicationPath
                'Dim physicalPath As String = HttpContext.Current.Request.MapPath(appPath)
                'Dim directory As String = physicalPath & "\xml\"
                If files.PostedFile.ContentType = "application/pdf" Then
                'Dim FileName As String = files.PostedFile.FileName
                Dim extension As String = files.PostedFile.FileName.Substring(files.PostedFile.FileName.LastIndexOf(".")).ToLower()
                Dim fullFileName As String = getpathFolder(strConnection) + Name + extension

                files.PostedFile.SaveAs(fullFileName)
                Else
                Return False
                End if
            Catch ex As Exception

            Finally
                files.FileContent.Dispose()
            End Try
        End If
        Return True
    End Function

    Public Function UploadToPathAsset(ByVal Name As String, ByVal strConnection As String, ByVal ApplicationID As String, ByVal branchAgreement As String) As Boolean
        If fileUploadCheck() Then
            Try


                'Dim appPath As String = HttpContext.Current.Request.ApplicationPath
                'Dim physicalPath As String = HttpContext.Current.Request.MapPath(appPath)
                'Dim directory As String = physicalPath & "\xml\"
                If files.PostedFile.ContentType = "application/pdf" Then
                    'Dim FileName As String = files.PostedFile.FileName
                    Dim extension As String = files.PostedFile.FileName.Substring(files.PostedFile.FileName.LastIndexOf(".")).ToLower()
                    Dim fullFileName As String = pathFile("Document", ApplicationID, strConnection, branchAgreement) + Name + extension


                    files.PostedFile.SaveAs(fullFileName)
                Else
                    Return False
                End If
            Catch ex As Exception

            Finally
                files.FileContent.Dispose()
            End Try
        End If
        Return True
    End Function

    Private Function pathFile(ByVal Group As String, ByVal ApplicationID As String, ByVal ConectionString As String, ByVal branchAgreement As String) As String
        Dim strDirectory As String = ""
        strDirectory = getpathFolder(ConectionString) & branchAgreement & "\" & Group & "\" & ApplicationID & "\"
        If Not System.IO.Directory.Exists(strDirectory) Then
            System.IO.Directory.CreateDirectory(strDirectory)
        End If
        Return strDirectory
    End Function

    Private Function getpathFolder(byval ConectionString As String) As String
        If pathFolder = "" Then
            Dim oClass As New Parameter.GeneralSetting
            Dim oCtr As New GeneralSettingController
            With oClass
                .strConnection = ConectionString
                .GSID = "UploadFolder"
                .ModuleID = "GEN"
            End With
            Try
                Return oCtr.GetGeneralSettingValue(oClass).Trim
            Catch ex As Exception
                Return ""
            End Try
        Else
            Return pathFolder
        End If
    End Function

End Class