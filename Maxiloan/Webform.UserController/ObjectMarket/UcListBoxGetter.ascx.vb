﻿Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine

Public Class UcListBoxGetter
    Inherits System.Web.UI.UserControl

    Private ParamListBoxGetter As New ParamListBoxGetter

    Private Enum CaseGetCount
        All        
    End Enum

    Public Property IsAutoPostBack() As Boolean
        Get
            Return cboList.AutoPostBack
        End Get
        Set(ByVal value As Boolean)
            cboList.AutoPostBack = value
        End Set
    End Property

    Private _SelectedValue As String
    Public Property SelectedValue() As String
        Get
            Return cboList.SelectedValue
        End Get
        Set(ByVal value As String)
            cboList.SelectedValue = value
        End Set
    End Property

    Private _SelectedText As String
    Public Property SelectedText() As String
        Get
            Return cboList.Text
        End Get
        Set(ByVal value As String)
            cboList.Text = value
        End Set
    End Property

    Private _SelectedIndex As Integer
    Public Property SelectedIndex() As Integer
        Get
            Return cboList.SelectedIndex
        End Get
        Set(ByVal value As Integer)
            cboList.SelectedIndex = value
        End Set
    End Property

    Private _StrConnection As String
    Public Property StrConnection() As String
        Get
            Return _StrConnection
        End Get
        Set(ByVal value As String)
            _StrConnection = value
        End Set
    End Property

    Private _SpName As String
    Public Property SpName() As String
        Get
            Return _SpName
        End Get
        Set(ByVal value As String)
            _SpName = value
        End Set
    End Property
    
    Private _ParamCount As Integer
    Public Property ParamCount() As Integer
        Get
            Return _ParamCount
        End Get
        Set(ByVal value As Integer)
            _ParamCount = value
        End Set
    End Property

    Private _ArrDictionaryOfSqlType As new ArrayList
    Public Property SqlTypeByDictionaryIndex() As ArrayList
        Get
            Return _ArrDictionaryOfSqlType
        End Get
        Set(ByVal value As ArrayList)
            _ArrDictionaryOfSqlType = value
        End Set
    End Property

    Private _DictionaryOfKeyAndValue As new Dictionary(Of String,String)
    Public Property DictionaryOfKeyAndValue() As Dictionary(Of String,String)
        Get
            Return _DictionaryOfKeyAndValue
        End Get
        Set(ByVal value As Dictionary(Of String,String))
            _DictionaryOfKeyAndValue = value
        End Set
    End Property

    Public Readonly Property Items() As ListItemCollection
        Get
            Return cboList.Items
        End Get
    End Property
    
    Public Property Enabled() As Boolean
        Get
            Return cboList.Enabled
        End Get
        Set(ByVal value As Boolean)
            cboList.Enabled = value
        End Set
    End Property
    
    Public ReadOnly Property SelectedItem() As ListItem
        Get
            Return cboList.SelectedItem
        End Get
    End Property

    Public Property IsVisible() As Boolean
        Get
            Return cboList.Visible
        End Get
        Set(ByVal value As Boolean)
            cboList.Visible = value
        End Set
    End Property

    Public Event SelectedIndexChanged As EventHandler



    Public Sub LoadData()
        Dim dtModel As DataTable


            With ParamListBoxGetter
                .StrConnection = Me.StrConnection
                .SpName = Me.spName
                .ParamCount = Me.ParamCount
                .DictionaryOfKeyAndValue = Me.DictionaryOfKeyAndValue
                .SqlTypeByDictionaryIndex = SqlTypeByDictionaryIndex
            End With
        


        ParamListBoxGetter = GetListFromDB(ParamListBoxGetter)
        dtModel = ParamListBoxGetter.ListData

        cboList.Items.Clear()

        Try

            With cboList
                .AppendDataBoundItems = True
                .DataSource = dtModel
                .DataTextField = "Description"
                .DataValueField = "ID"
                .DataBind()
                .Items.Insert(0, "Select One")
                .Items(0).Value = "Select One"
            End With

        Catch ex As Exception

        End Try
    End Sub
    
    Public Sub LoadDataALL()
        Dim dtModel As DataTable


            With ParamListBoxGetter
                .StrConnection = Me.StrConnection
                .SpName = Me.spName
            End With
        


        ParamListBoxGetter = GetListFromDBALL(ParamListBoxGetter)
        dtModel = ParamListBoxGetter.ListData

        cboList.Items.Clear()

        Try

            With cboList
                .AppendDataBoundItems = True
                .DataSource = dtModel
                .DataTextField = "Description"
                .DataValueField = "ID"
                .DataBind()
                .Items.Insert(0, "Select One")
                .Items(0).Value = "Select One"
            End With

        Catch ex As Exception

        End Try
    End Sub

    Public Function GetListFromDB(CustomClass As ParamListBoxGetter) As ParamListBoxGetter
        Try
            Dim params() As SqlParameter = New SqlParameter(CustomClass.ParamCount) {}
            Try
                
                    For index = 0 To DictionaryOfKeyAndValue.Count - 1
                        params(index) = New SqlParameter("@"+customclass.DictionaryOfKeyAndValue.Keys(index).ToString, CustomClass.SqlTypeByDictionaryIndex.Item(index))
                        params(index).Value = customclass.DictionaryOfKeyAndValue.Values(index).ToString
                    Next
                    CustomClass.ListData = SqlHelper.ExecuteDataset(CustomClass.StrConnection, CommandType.StoredProcedure, CustomClass.SpName,params).Tables(0)
              
            Catch ex As Exception

            End Try
            
            Return CustomClass
        Catch ex As Exception
            Return CustomClass
        End Try
    End Function

     Public Function GetListFromDBALL(CustomClass As ParamListBoxGetter) As ParamListBoxGetter
        Try
            Try
                
                CustomClass.ListData = SqlHelper.ExecuteDataset(CustomClass.StrConnection, CommandType.StoredProcedure, CustomClass.SpName).Tables(0)
              
            Catch ex As Exception

            End Try
            
            Return CustomClass
        Catch ex As Exception
            Return CustomClass
        End Try
    End Function

End Class

Public Class ParamListBoxGetter
    Private _Name As String
    Public Property Name() As String
        Get
            Return _Name
        End Get
        Set(ByVal value As String)
            _Name = value
        End Set
    End Property

    Private _Value As String
    Public Property Value() As String
        Get
            Return _Value
        End Get
        Set(ByVal value As String)
            _Value = value
        End Set
    End Property

    Private _ListData As DataTable
    Public Property ListData() As DataTable
        Get
            Return _ListData
        End Get
        Set(ByVal value As DataTable)
            _ListData = value
        End Set
    End Property

    Private _StrConnection As String
    Public Property StrConnection() As String
        Get
            Return _StrConnection
        End Get
        Set(ByVal value As String)
            _StrConnection = value
        End Set
    End Property

    Private _SpName As String
    Public Property SpName() As String
        Get
            Return _SpName
        End Get
        Set(ByVal value As String)
            _SpName = value
        End Set
    End Property

    Private _ParamCount As Integer
    Public Property ParamCount() As Integer
        Get
            Return _ParamCount
        End Get
        Set(ByVal value As Integer)
            _ParamCount = value
        End Set
    End Property

    Private _DictionaryOfKeyAndValue As Dictionary(Of String,String)
    Public Property DictionaryOfKeyAndValue() As Dictionary(Of String,String)
        Get
            Return _DictionaryOfKeyAndValue
        End Get
        Set(ByVal value As Dictionary(Of String,String))
            _DictionaryOfKeyAndValue = value
        End Set
    End Property

    Private _ArrDictionaryOfSqlType As ArrayList
    Public Property SqlTypeByDictionaryIndex() As ArrayList
        Get
            Return _ArrDictionaryOfSqlType
        End Get
        Set(ByVal value As ArrayList)
            _ArrDictionaryOfSqlType = value
        End Set
    End Property

End Class