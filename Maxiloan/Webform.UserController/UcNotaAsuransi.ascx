﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UcNotaAsuransi.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.UcNotaAsuransi" %>
<%@ Register Src="ucDateCE.ascx" TagName="ucDateCE" TagPrefix="uc1" %>
<%@ Register Src="ucNumberFormat.ascx" TagName="ucNumberFormat" TagPrefix="uc1" %>

<div class="form_box_header">
    <div class="form_single">
        <h4>
            NOTA ASURANSI
        </h4>
    </div>
</div>
<div class="form_box">
    <div class="form_left">
        <label>
            Asuransi</label>
        <asp:Label runat="server" ID="lblAsuransi"></asp:Label>
    </div>
    <div class="form_right">
        <label>
            No.Kontrak</label>
        <asp:Label runat="server" ID="lblNoKontrak"></asp:Label>
    </div>
</div>
<div class="form_box">
    <div class="form_left">
        <label class="label_req">
            No.Nota</label>
        <asp:TextBox runat="server" ID="txtNoNota"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic"
                                    InitialValue="" ControlToValidate="txtNoNota" ErrorMessage="Nomor Nota Harus Diisi!"
                                    CssClass="validator_general"></asp:RequiredFieldValidator>
    </div>
    <div class="form_right">
        <label>
            Nama Konsumen</label>
        <asp:Label runat="server" ID="lblNamaKonsumen"></asp:Label>
    </div>
</div>
<div class="form_box">
    <div class="form_left">
        <label class="label_req">
            Tanggal Nota</label>        
        <uc1:ucdatece id="txtTglNota" runat="server"></uc1:ucdatece>
    </div>
    <div class="form_right">
    </div>
</div>
<div class="form_box">
    <div class="form_left">
        <label>
            Tenor</label>
        <asp:Label runat="server" ID="lblTenor"></asp:Label>
    </div>
    <div class="form_right">
    </div>
</div>
<div class="form_box">
    <div class="form_left">
        <label>
            Periode</label>
        <asp:Label runat="server" ID="lblPeriode"></asp:Label>
    </div>
    <div class="form_right">
    </div>
</div>
<div class="form_box">
    <div class="form_left">
        <label> Attached</label>
        <asp:FileUpload ID="uplPolis" runat="server"/>
        <asp:LinkButton ID="lblfileuploaded" runat="server" Text=""></asp:LinkButton>
    </div>
    <div class="form_right">
            <asp:Button runat="server" ID="btnupload" Text="Upload" CssClass="small button blue"  CausesValidation="False" OnClick="btnupload_Click"/>&nbsp;
            <asp:Button ID="btndownload" runat="server" Text="Download" CausesValidation="False"  CssClass="small button blue" OnClick="btndownload_Click"></asp:Button> 
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"/>
    </div>
</div>
<div class="form_box_header">
    <div class="form_single">
        <h4>
            DAFTAR ITEM
        </h4>
    </div>
</div>
<div class="form_box">
    <div class="form_left">
        <label>
            Perhitungan Sistem</label>
    </div>
    <div class="form_right">
        <label>
            Asuransi</label>
        <asp:Label runat="server" ID="lblAsuransiDetail" Text="Selisih"></asp:Label>
    </div>
</div>
<div class="form_box">
    <div class="form_left">
        <label>
            Premi Gross</label>
        <asp:Label runat="server" ID="lblPremiGross" CssClass="numberAlign regular_text"></asp:Label>
    </div>
    <div class="form_right">        
        <uc1:ucNumberFormat id="txtPremiGross" runat="server"></uc1:ucNumberFormat>
        <asp:Label runat="server" ID="lblPremisGrossSelisih" CssClass="numberAlign regular_text"></asp:Label>
    </div>
</div>
<div class="form_box">
    <div class="form_left border_sum">
        <label>
            Komisi</label>
        <asp:Label runat="server" ID="lblKomisi" CssClass="numberAlign regular_text"></asp:Label>
        <label class="label_calc">
            -
        </label>
    </div>
    <div class="form_right">        
        <uc1:ucNumberFormat id="txtKomisi" runat="server"></uc1:ucNumberFormat>
        <asp:Label runat="server" ID="lblKomisiSelisih" CssClass="numberAlign regular_text"></asp:Label>
    </div>
</div>
<div class="form_box">
    <div class="form_left">
        <label>
            Premi Net</label>
        <asp:Label runat="server" ID="lblPremiNet" CssClass="numberAlign regular_text"></asp:Label>
        <label class="label_calc">
            -
        </label>
    </div>
    <div class="form_right">        
        <uc1:ucNumberFormat id="txtPremiNet" runat="server"></uc1:ucNumberFormat>
        <asp:Label runat="server" ID="lblPremiNetSelisih" CssClass="numberAlign regular_text"></asp:Label>        
    </div>
</div>
<div class="form_box">
    <div class="form_left">
        <label>
            PPN(10%)</label>
        <asp:Label runat="server" ID="lblPPN" CssClass="numberAlign regular_text"></asp:Label>
        <label class="label_calc">
            -
        </label>
    </div>
    <div class="form_right">        
        <uc1:ucNumberFormat id="txtPPN" runat="server"></uc1:ucNumberFormat>
        <asp:Label runat="server" ID="lblPPNSelisih" CssClass="numberAlign regular_text"></asp:Label>
    </div>
</div>
<div class="form_box">
    <div class="form_left border_sum">
        <label>
            PPH</label>
        <asp:Label runat="server" ID="lblPPH" CssClass="numberAlign regular_text"></asp:Label>        
    </div>
    <div class="form_right">        
        <uc1:ucNumberFormat id="txtPPH" runat="server"></uc1:ucNumberFormat>
        <asp:Label runat="server" ID="lblPPHSelisih" CssClass="numberAlign regular_text"></asp:Label>
    </div>
</div>
<div class="form_box">
    <div class="form_left">
        <label>
            Biaya Polis</label>
        <asp:Label runat="server" ID="lblBiayaPolis" CssClass="numberAlign regular_text"></asp:Label>
        <label class="label_calc">
            +
        </label>
    </div>
    <div class="form_right">        
        <uc1:ucNumberFormat id="txtBiayaPolis" runat="server"></uc1:ucNumberFormat>
        <asp:Label runat="server" ID="lblBiayaPolisSelisih" CssClass="numberAlign regular_text"></asp:Label>
    </div>
</div>
<div class="form_box">
    <div class="form_left border_sum">
        <label>
            Biaya Materai</label>
        <asp:Label runat="server" ID="lblBiayaMaterai" CssClass="numberAlign regular_text"></asp:Label>
        <label class="label_calc">
            +
        </label>
    </div>
    <div class="form_right">        
        <uc1:ucNumberFormat id="txtBiayaMaterai" runat="server"></uc1:ucNumberFormat>
        <asp:Label runat="server" ID="lblBiayaMateraiSelisih" CssClass="numberAlign regular_text"></asp:Label>
    </div>
</div>
<div class="form_box">
    <div class="form_left">
        <label>
            Total Tagihan</label>
        <asp:Label runat="server" ID="lblTotalTagihan" CssClass="numberAlign regular_text"></asp:Label>
    </div>
    <div class="form_right">        
        <uc1:ucNumberFormat id="txtTotalTagihan" runat="server"></uc1:ucNumberFormat>
        <asp:Label runat="server" ID="lblTotalTagihanSelisih"  CssClass="numberAlign regular_text"></asp:Label>
    </div>
</div>
<div class="form_box_header">
    <div class="form_single">
        <h4>
            Adjustment Selisih Insurance Payable
        </h4>
    </div>
</div>
<div class="form_box">
    <div class="form_left">
        <label>
            Selisih Insurance Payable</label>
        <asp:Label runat="server" ID="lblAdjSelisihInsPayable" CssClass="numberAlign regular_text" ForeColor="#0099ff" Font-Bold="true"></asp:Label>
    </div>
    <div class="form_right">
        <label  runat="server" id="IdAdjSelsihInsPayable">
            Di Alokasikan ke :
        </label>
        <asp:DropDownList runat="server" ID="cboAdjSelsihInsPayable">
            <asp:ListItem Value="SelectOne">Select One</asp:ListItem>
            <asp:ListItem Value="PLL">Pendapatan Lain-Lain</asp:ListItem>
            <asp:ListItem Value="EXPLL">Biaya Lain-lain</asp:ListItem>
            <asp:ListItem Value="PREPAID">Prepaid</asp:ListItem>
        </asp:DropDownList>
        <asp:RequiredFieldValidator ID="rvAdjSelisihInsPayable" runat="server" ControlToValidate="cboAdjSelsihInsPayable"
            CssClass="validator_general" Display="Dynamic" ErrorMessage="Harap pilih ALokasi!"
            InitialValue="Select One" SetFocusOnError="true"></asp:RequiredFieldValidator>
    </div>
</div>