﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
#End Region

Public Class UcFullPrepayInfo
    Inherits AccMntControlBased

#Region "Property"
    Public WriteOnly Property IsPenaltyTerminationShow() As Boolean
        Set(ByVal Value As Boolean)
            If Not Value Then
                lblTerminationPenaltyFee.Visible = Value
                lblTotalPrepaymentAmount.Visible = Value
                pnlTerminationFee.Visible = Value
            End If
        End Set
    End Property
    Public ReadOnly Property TerminationPenalty() As Double
        Get
            Return CType(lblTerminationPenaltyFee.Text, Double)
        End Get
    End Property
	Public Property InstallmentDueAmount() As Double
		Get
			Return CType(Viewstate("InstallmentDueAmount"), Double)
		End Get
		Set(ByVal Value As Double)
			Viewstate("InstallmentDueAmount") = Value
		End Set
	End Property
	Public Property PrincipalAmountDue() As Double
		Get
			Return CType(ViewState("PrincipalAmountDue"), Double)
		End Get
		Set(ByVal Value As Double)
			ViewState("PrincipalAmountDue") = Value
		End Set
	End Property
	Public Property InterestAmountDue() As Double
		Get
			Return CType(ViewState("InterestAmountDue"), Double)
		End Get
		Set(ByVal Value As Double)
			ViewState("InterestAmountDue") = Value
		End Set
	End Property
	Public Property IsValidPrepayment() As Boolean
        Get
            Return CType(viewstate("isValidPrepayment"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            viewstate("isValidPrepayment") = Value
        End Set
    End Property
    Public Property ContractPrepaidAmount() As Double
        Get
            Return CType(viewstate("ContractPrepaidAmount"), Double)
        End Get
        Set(ByVal Value As Double)
            viewstate("ContractPrepaidAmount") = Value
        End Set
    End Property

    Public Property IsCrossDefault() As Boolean
        Get
            Return CType(Viewstate("IsCrossDefault"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            viewstate("IsCrossDefault") = Value
        End Set
    End Property

    Public Property InsurancePaidBy() As String
        Get
            Return CType(Viewstate("InsurancePaidBy"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("InsurancePaidBy") = Value
        End Set
    End Property

    Public Property BranchAgreement() As String
        Get
            Return CType(Viewstate("BranchAgreement"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("BranchAgreement") = Value
        End Set
    End Property

    Public Property PrepaymentType() As String
        Get
            Return CType(Viewstate("PrepaymentType"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("PrepaymentType") = Value
        End Set
    End Property

    Public Property CustomerType() As String
        Get
            Return CType(Viewstate("CustomerType"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerType") = Value
        End Set
    End Property

	Public Property TotalPrepaymentAmount() As Double
		Get
			Return CType(Viewstate("TotalPrepaymentAmount"), Double)
		End Get
		Set(ByVal Value As Double)
			viewstate("TotalPrepaymentAmount") = Value
		End Set
	End Property

	Public Property TotalPrePaymentAmountResch() As Double
		Get
			Return CType(ViewState("TotalPrePaymentAmountResch"), Double)
		End Get
		Set(ByVal Value As Double)
			ViewState("TotalPrePaymentAmountResch") = Value
		End Set
	End Property

	Public Property CustomerName() As String
        Get
            Return CType(Viewstate("CustomerName"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerName") = Value
        End Set
    End Property

    Public Property DefaultStatus() As String
        Get
            Return CType(Viewstate("DefaultStatus"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("DefaultStatus") = Value
        End Set
    End Property
    Public Property InstallmentAmount() As Double
        Get
            Return CType(Viewstate("InstallmentAmount"), Double)
        End Get
        Set(ByVal Value As Double)
            viewstate("InstallmentAmount") = Value
        End Set
    End Property
    Public Property NextInstallmentNumber() As Integer
        Get
            Return CType(Viewstate("NextInstallmentNumber"), Integer)
        End Get
        Set(ByVal Value As Integer)
            viewstate("NextInstallmentNumber") = Value
        End Set
    End Property
    Public Property NAAMount() As Double
        Get
            Return CType(Viewstate("NAAMount"), Double)
        End Get
        Set(ByVal Value As Double)
            viewstate("NAAMount") = Value
        End Set
    End Property
    Public Property NADate() As Date
        Get
            Return CType(Viewstate("NADate"), Date)
        End Get
        Set(ByVal Value As Date)
            viewstate("NADate") = Value
        End Set
    End Property
    Public Property NextInstallmentDate() As Date
        Get
            Return CType(Viewstate("NextInstallmentDate"), Date)
        End Get
        Set(ByVal Value As Date)
            viewstate("NextInstallmentDate") = Value
        End Set
    End Property
    Public Property FundingPledgeStatus() As String
        Get
            Return CType(Viewstate("FundingPledgeStatus"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("FundingPledgeStatus") = Value
        End Set
    End Property
    Public Property ResiduValue() As Double
        Get
            Return CType(Viewstate("ResiduValue"), Double)
        End Get
        Set(ByVal Value As Double)
            viewstate("ResiduValue") = Value
        End Set
    End Property
    Public Property FundingCoyName() As String
        Get
            Return CType(Viewstate("FundingCoyName"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("FundingCoyName") = Value
        End Set
    End Property
    Public Property ContractStatus() As String
        Get
            Return CType(Viewstate("ContactStatus"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ContactStatus") = Value
        End Set
    End Property
    Public Property BranchID() As String
        Get
            Return CType(Viewstate("BranchID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("BranchID") = Value
        End Set
    End Property

    Public Property AccruedInterest() As Double
        Get
            Return CType(Viewstate("AccruedInterest"), Double)
        End Get
        Set(ByVal Value As Double)
            viewstate("AccruedInterest") = Value
        End Set
    End Property



#Region "Property Maximum Value"
    Public Property MaximumInstallment() As Double
        Get
            Return CType(Viewstate("MaximumInstallment"), Double)
        End Get
        Set(ByVal Value As Double)
            viewstate("MaximumInstallment") = Value
        End Set
    End Property

    Public Property MaximumInsurance() As Double
        Get
            Return CType(Viewstate("MaximumInsurance"), Double)
        End Get
        Set(ByVal Value As Double)
            viewstate("MaximumInsurance") = Value
        End Set
    End Property

    Public Property MaximumInstallCollFee() As Double
        Get
            Return CType(Viewstate("MaximumInstallCollFee"), Double)
        End Get
        Set(ByVal Value As Double)
            viewstate("MaximumInstallCollFee") = Value
        End Set
    End Property

    Public Property MaximumInsuranceCollFee() As Double
        Get
            Return CType(Viewstate("MaximumInsuranceCollFee"), Double)
        End Get
        Set(ByVal Value As Double)
            viewstate("MaximumInsuranceCollFee") = Value
        End Set
    End Property

    Public Property MaximumLCInstallFee() As Double
        Get
            Return CType(Viewstate("MaximumLCInstallFee"), Double)
        End Get
        Set(ByVal Value As Double)
            viewstate("MaximumLCInstallFee") = Value
        End Set
    End Property

    Public Property MaximumLCInsuranceFee() As Double
        Get
            Return CType(Viewstate("MaximumLCInsuranceFee"), Double)
        End Get
        Set(ByVal Value As Double)
            viewstate("MaximumLCInsuranceFee") = Value
        End Set
    End Property

    Public Property MaximumInsuranceClaimFee() As Double
        Get
            Return CType(Viewstate("MaximumInsuranceClaimFee"), Double)
        End Get
        Set(ByVal Value As Double)
            viewstate("MaximumInsuranceClaimFee") = Value
        End Set
    End Property

    Public Property MaximumSTNKRenewalFee() As Double
        Get
            Return CType(Viewstate("MaximumSTNKRenewalFee"), Double)
        End Get
        Set(ByVal Value As Double)
            viewstate("MaximumSTNKRenewalFee") = Value
        End Set
    End Property

    Public Property MaximumPDCBounceFee() As Double
        Get
            Return CType(Viewstate("MaximumPDCBounceFee"), Double)
        End Get
        Set(ByVal Value As Double)
            viewstate("MaximumPDCBounceFee") = Value
        End Set
    End Property

    Public Property MaximumReposessionFee() As Double
        Get
            Return CType(Viewstate("MaximumReposessionFee"), Double)
        End Get
        Set(ByVal Value As Double)
            viewstate("MaximumReposessionFee") = Value
        End Set
    End Property

    Public Property MaximumOutStandingPrincipal() As Double
        Get
            Return CType(Viewstate("MaximumOutStandingPrincipal"), Double)
        End Get
        Set(ByVal Value As Double)
            viewstate("MaximumOutStandingPrincipal") = Value
        End Set
    End Property

    Public Property MaximumOutStandingInterest() As Double
        Get
            Return CType(Viewstate("MaximumOutStandingInterest"), Double)
        End Get
        Set(ByVal Value As Double)
            viewstate("MaximumOutStandingInterest") = Value
        End Set
    End Property

    Public Property MaximumPenaltyRate() As Double
        Get
            Return CType(Viewstate("MaximumPenaltyRate"), Double)
        End Get
        Set(ByVal Value As Double)
            viewstate("MaximumPenaltyRate") = Value
        End Set
    End Property
#End Region
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub
    Public Sub PaymentInfo()

        Dim oController As New UCPaymentInfoController
        Dim oCustomClass As New Parameter.FullPrepay
        With oCustomClass
            .strConnection = GetConnectionString
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationID
            .ValueDate = Me.ValueDate
            .PrepaymentType = Me.PrepaymentType

        End With


        oCustomClass = oController.GetFullPrepaymentInfo(oCustomClass)

        With oCustomClass
            Try
                Me.BranchAgreement = .BranchAgreement
                Me.AgreementNo = .Agreementno

                Me.CustomerID = .CustomerID
                Me.CustomerType = .CustomerType
                Me.ContractStatus = .ContractStatus
                Me.DefaultStatus = .DefaultStatus
                Me.CustomerName = .CustomerName
                Me.ContractPrepaidAmount = .Prepaid

                lblInstallmentDue.Text = FormatNumber(.InstallmentDue, 2)
                lblLCInstall.Text = FormatNumber(.LcInstallment, 2)
                lblInstallColl.Text = FormatNumber(.InstallmentCollFee, 2)

                lblInsuranceDue.Text = FormatNumber(.InsuranceDue, 2)
                lblLCInsurance.Text = FormatNumber(.LcInsurance, 2)
                lblInsuranceColl.Text = FormatNumber(.InsuranceCollFee, 2)

                lblPDCBounceFee.Text = FormatNumber(.PDCBounceFee, 2)
                lblSTNKFee.Text = FormatNumber(.STNKRenewalFee, 2)
                lblInsuranceClaim.Text = FormatNumber(.InsuranceClaimExpense, 2)
                lblRepossessionFee.Text = FormatNumber(.RepossessionFee, 2)
                lblAccruedInterest.Text = FormatNumber(.AccruedInterest, 2)
                If .AccruedInterest < 0 Then
                    lblAccruedKeterangan.Visible = True
                Else
                    lblAccruedKeterangan.Visible = False
                End If
                lblTerminationPenaltyFee.Text = FormatNumber(.TerminationPenalty, 2)
                lblOSPrincipalAmount.Text = FormatNumber(.OutstandingPrincipal, 2)
                lblTotalPrepaymentAmountGross.Text = FormatNumber(.OutstandingPrincipal + .InstallmentDue + .LcInstallment + .InstallmentCollFee +
                                        .InsuranceDue + .InsuranceCollFee + .LcInsurance +
                                        .PDCBounceFee + .STNKRenewalFee + .InsuranceClaimExpense +
                                        .RepossessionFee + .AccruedInterest + .TerminationPenalty, 2)
                lblTotalPrepaymentAmount.Text = FormatNumber(.OutstandingPrincipal + .InstallmentDue + .LcInstallment + .InstallmentCollFee +
                                        .InsuranceDue + .InsuranceCollFee + .LcInsurance +
                                        .PDCBounceFee + .STNKRenewalFee + .InsuranceClaimExpense +
                                        .RepossessionFee + .AccruedInterest + .TerminationPenalty - .FaktorPengurang, 2)
                Me.TotalPrepaymentAmount = .OutstandingPrincipal + .InstallmentDue + .LcInstallment + .InstallmentCollFee +
                                        .InsuranceDue + .InsuranceCollFee + .LcInsurance +
                                        .PDCBounceFee + .STNKRenewalFee + .InsuranceClaimExpense +
                                        .RepossessionFee + .AccruedInterest + .TerminationPenalty - .FaktorPengurang

                '''' add abdi 02-nov-2018 untuk rescheduling
                Me.TotalPrePaymentAmountResch = .OutstandingPrincipal + .InstallmentDue + .LcInstallment + .InstallmentCollFee +
                                        .InsuranceDue + .InsuranceCollFee + .LcInsurance +
                                        .PDCBounceFee + .STNKRenewalFee + .InsuranceClaimExpense +
                                        .RepossessionFee + .AccruedInterest + .TerminationPenalty + - .FaktorPengurang
                ''''

                lblFaktorPengurang.Text = FormatNumber(.FaktorPengurang, 2)
                Me.AccruedInterest = .AccruedInterest
                Me.InstallmentDueAmount = .InstallmentDue
                Me.MaximumPenaltyRate = .TerminationPenalty
                Me.IsCrossDefault = .CrossDefault
                If Me.PrepaymentType = "ED" Then
                    Me.MaximumInstallment = .ECI
                ElseIf Me.PrepaymentType = "PD" Then
                    Me.MaximumInstallment = .OutstandingPrincipal
                End If
                Me.InsurancePaidBy = .InsurancePaidBy
                Me.ContractPrepaidAmount = .Prepaid
                Me.MaximumLCInstallFee = .LcInstallment
                Me.MaximumInstallCollFee = .InstallmentCollFee

                Me.MaximumInsurance = .InsuranceDue
                Me.MaximumLCInsuranceFee = .LcInsurance
                Me.MaximumInsuranceCollFee = .InsuranceCollFee

                Me.MaximumPDCBounceFee = .PDCBounceFee
                Me.MaximumSTNKRenewalFee = .STNKRenewalFee
                Me.MaximumInsuranceClaimFee = .InsuranceClaimExpense
                Me.MaximumReposessionFee = .RepossessionFee

                Me.MaximumOutStandingInterest = .OutStandingInterest
                Me.MaximumOutStandingPrincipal = .OutstandingPrincipal
                Me.IsValidPrepayment = True

                Me.InstallmentAmount = .InstallmentAmount
                Me.NextInstallmentDate = .NextInstallmentDate
                Me.NextInstallmentNumber = .NextInstallmentNumber

                Me.FundingCoyName = .FundingCoyName
                Me.FundingPledgeStatus = .FundingPledgeStatus
                Me.ResiduValue = .ResiduValue
                Me.NADate = .NADate
                Me.NAAMount = .NAAmount

                Me.PrincipalAmountDue = .PrincipalAmountDue
                Me.InterestAmountDue = .InterestAmountDue
            Catch ex As Exception
                ex.Message.ToString()
            End Try


            'If .TerminationPenalty > (.OutStandingInterest - .AccruedInterest) Then
            '    IsValidPrepayment = False
            'Else
            '    IsValidPrepayment = True
            'End If

        End With
    End Sub


    Public Sub PaymentInfoFactAndMDKJ()

        Dim oController As New UCPaymentInfoController
        Dim oCustomClass As New Parameter.FullPrepay
        With oCustomClass
            .strConnection = GetConnectionString()
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationID
            .ValueDate = Me.ValueDate
            .PrepaymentType = Me.PrepaymentType
            .InvoiceNo = Me.InvoiceNo
            .InvoiceSeqNo = Me.InvoiceSeqNo
        End With

        oCustomClass = oController.GetFullPrepaymentInfoFactAndMDKJ(oCustomClass)

        With oCustomClass
            Try
                Me.BranchAgreement = .BranchAgreement
                Me.AgreementNo = .Agreementno

                Me.CustomerID = .CustomerID
                Me.CustomerType = .CustomerType
                Me.ContractStatus = .ContractStatus
                Me.DefaultStatus = .DefaultStatus
                Me.CustomerName = .CustomerName
                Me.ContractPrepaidAmount = .Prepaid

                lblInstallmentDue.Text = FormatNumber(.InstallmentDue, 2)
                lblLCInstall.Text = FormatNumber(.LcInstallment, 2)
                lblInstallColl.Text = FormatNumber(.InstallmentCollFee, 2)

                lblInsuranceDue.Text = FormatNumber(.InsuranceDue, 2)
                lblLCInsurance.Text = FormatNumber(.LcInsurance, 2)
                lblInsuranceColl.Text = FormatNumber(.InsuranceCollFee, 2)

                lblPDCBounceFee.Text = FormatNumber(.PDCBounceFee, 2)
                lblSTNKFee.Text = FormatNumber(.STNKRenewalFee, 2)
                lblInsuranceClaim.Text = FormatNumber(.InsuranceClaimExpense, 2)
                lblRepossessionFee.Text = FormatNumber(.RepossessionFee, 2)
                lblAccruedInterest.Text = FormatNumber(.AccruedInterest, 2)
                If .AccruedInterest < 0 Then
                    lblAccruedKeterangan.Visible = True
                Else
                    lblAccruedKeterangan.Visible = False
                End If
                lblTerminationPenaltyFee.Text = FormatNumber(.TerminationPenalty, 2)
                lblOSPrincipalAmount.Text = FormatNumber(.OutstandingPrincipal, 2)
                lblTotalPrepaymentAmountGross.Text = FormatNumber(.OutstandingPrincipal + .InstallmentDue + .LcInstallment + .InstallmentCollFee +
                                        .InsuranceDue + .InsuranceCollFee + .LcInsurance +
                                        .PDCBounceFee + .STNKRenewalFee + .InsuranceClaimExpense +
                                        .RepossessionFee + .AccruedInterest + .TerminationPenalty, 2)
                lblTotalPrepaymentAmount.Text = FormatNumber(.OutstandingPrincipal + .InstallmentDue + .LcInstallment + .InstallmentCollFee +
                                        .InsuranceDue + .InsuranceCollFee + .LcInsurance +
                                        .PDCBounceFee + .STNKRenewalFee + .InsuranceClaimExpense +
                                        .RepossessionFee + .AccruedInterest + .TerminationPenalty - .FaktorPengurang, 2)
                Me.TotalPrepaymentAmount = .OutstandingPrincipal + .InstallmentDue + .LcInstallment + .InstallmentCollFee +
                                        .InsuranceDue + .InsuranceCollFee + .LcInsurance +
                                        .PDCBounceFee + .STNKRenewalFee + .InsuranceClaimExpense +
                                        .RepossessionFee + .AccruedInterest + .TerminationPenalty - .FaktorPengurang

                Me.TotalPrePaymentAmountResch = .OutstandingPrincipal + .InstallmentDue + .LcInstallment + .InstallmentCollFee +
                                        .InsuranceDue + .InsuranceCollFee + .LcInsurance +
                                        .PDCBounceFee + .STNKRenewalFee + .InsuranceClaimExpense +
                                        .RepossessionFee + .AccruedInterest + .TerminationPenalty + - .FaktorPengurang
                ''''

                lblFaktorPengurang.Text = FormatNumber(.FaktorPengurang, 2)
                Me.AccruedInterest = .AccruedInterest
                Me.InstallmentDueAmount = .InstallmentDue
                Me.MaximumPenaltyRate = .TerminationPenalty
                Me.IsCrossDefault = .CrossDefault
                If Me.PrepaymentType = "ED" Then
                    Me.MaximumInstallment = .ECI
                ElseIf Me.PrepaymentType = "PD" Then
                    Me.MaximumInstallment = .OutstandingPrincipal
                End If
                Me.InsurancePaidBy = .InsurancePaidBy
                Me.ContractPrepaidAmount = .Prepaid
                Me.MaximumLCInstallFee = .LcInstallment
                Me.MaximumInstallCollFee = .InstallmentCollFee

                Me.MaximumInsurance = .InsuranceDue
                Me.MaximumLCInsuranceFee = .LcInsurance
                Me.MaximumInsuranceCollFee = .InsuranceCollFee

                Me.MaximumPDCBounceFee = .PDCBounceFee
                Me.MaximumSTNKRenewalFee = .STNKRenewalFee
                Me.MaximumInsuranceClaimFee = .InsuranceClaimExpense
                Me.MaximumReposessionFee = .RepossessionFee

                Me.MaximumOutStandingInterest = .OutStandingInterest
                Me.MaximumOutStandingPrincipal = .OutstandingPrincipal
                Me.IsValidPrepayment = True

                Me.InstallmentAmount = .installmentamount
                Me.NextInstallmentDate = .NextInstallmentDate
                Me.NextInstallmentNumber = .NextInstallmentNumber

                Me.FundingCoyName = .FundingCoyName
                Me.FundingPledgeStatus = .FundingPledgeStatus
                Me.ResiduValue = .ResiduValue
                Me.NADate = .NADate
                Me.NAAMount = .NAAmount

                Me.PrincipalAmountDue = .PrincipalAmountDue
                Me.InterestAmountDue = .InterestAmountDue
            Catch ex As Exception
                ex.Message.ToString()
            End Try

        End With
    End Sub

    Public Sub PaymentInfoView()

        Dim oController As New UCPaymentInfoController
        Dim oCustomClass As New Parameter.FullPrepay
        With oCustomClass
            .strConnection = GetConnectionString()
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationID
            '.ValueDate = Me.ValueDate
            .PrepaymentType = Me.PrepaymentType
            .RequestNo = Me.RequestNo

        End With


        oCustomClass = oController.GetFullPrepaymentInfoView(oCustomClass)

        With oCustomClass
            Try
                Me.BranchAgreement = .BranchAgreement
                Me.AgreementNo = .Agreementno

                Me.CustomerID = .CustomerID
                Me.CustomerType = .CustomerType
                Me.ContractStatus = .ContractStatus
                Me.DefaultStatus = .DefaultStatus
                Me.CustomerName = .CustomerName
                Me.ContractPrepaidAmount = .Prepaid

                Me.ApplicationID = .ApplicationID
                Me.ApplicationModule = .ApplicationModule
                Me.InvoiceNo = .InvoiceNo
                Me.InvoiceSeqNo = .InvoiceSeqNo

                lblInstallmentDue.Text = FormatNumber(.InstallmentDue, 2)
                lblLCInstall.Text = FormatNumber(.LcInstallment, 2)
                lblInstallColl.Text = FormatNumber(.InstallmentCollFee, 2)

                lblInsuranceDue.Text = FormatNumber(.InsuranceDue, 2)
                lblLCInsurance.Text = FormatNumber(.LcInsurance, 2)
                lblInsuranceColl.Text = FormatNumber(.InsuranceCollFee, 2)

                lblPDCBounceFee.Text = FormatNumber(.PDCBounceFee, 2)
                lblSTNKFee.Text = FormatNumber(.STNKRenewalFee, 2)
                lblInsuranceClaim.Text = FormatNumber(.InsuranceClaimExpense, 2)
                lblRepossessionFee.Text = FormatNumber(.RepossessionFee, 2)
                lblAccruedInterest.Text = FormatNumber(.AccruedInterest, 2)
                If .AccruedInterest < 0 Then
                    lblAccruedKeterangan.Visible = True
                Else
                    lblAccruedKeterangan.Visible = False
                End If
                lblTerminationPenaltyFee.Text = FormatNumber(.TerminationPenalty, 2)
                lblOSPrincipalAmount.Text = FormatNumber(.OutstandingPrincipal, 2)
                lblTotalPrepaymentAmountGross.Text = FormatNumber(.OutstandingPrincipal + .InstallmentDue + .LcInstallment + .InstallmentCollFee +
                                        .InsuranceDue + .InsuranceCollFee + .LcInsurance +
                                        .PDCBounceFee + .STNKRenewalFee + .InsuranceClaimExpense +
                                        .RepossessionFee + .AccruedInterest + .TerminationPenalty, 2)
                lblTotalPrepaymentAmount.Text = FormatNumber(.OutstandingPrincipal + .InstallmentDue + .LcInstallment + .InstallmentCollFee +
                                        .InsuranceDue + .InsuranceCollFee + .LcInsurance +
                                        .PDCBounceFee + .STNKRenewalFee + .InsuranceClaimExpense +
                                        .RepossessionFee + .AccruedInterest + .TerminationPenalty - .FaktorPengurang, 2)
                Me.TotalPrepaymentAmount = .OutstandingPrincipal + .InstallmentDue + .LcInstallment + .InstallmentCollFee +
                                        .InsuranceDue + .InsuranceCollFee + .LcInsurance +
                                        .PDCBounceFee + .STNKRenewalFee + .InsuranceClaimExpense +
                                        .RepossessionFee + .AccruedInterest + .TerminationPenalty - .FaktorPengurang

                '''' add abdi 02-nov-2018 untuk rescheduling
                Me.TotalPrePaymentAmountResch = .OutstandingPrincipal + .InstallmentDue + .LcInstallment + .InstallmentCollFee +
                                        .InsuranceDue + .InsuranceCollFee + .LcInsurance +
                                        .PDCBounceFee + .STNKRenewalFee + .InsuranceClaimExpense +
                                        .RepossessionFee + .AccruedInterest + .TerminationPenalty + - .FaktorPengurang
                ''''

                lblFaktorPengurang.Text = FormatNumber(.FaktorPengurang, 2)
                Me.AccruedInterest = .AccruedInterest
                Me.InstallmentDueAmount = .InstallmentDue
                Me.MaximumPenaltyRate = .TerminationPenalty
                Me.IsCrossDefault = .CrossDefault
                If Me.PrepaymentType = "ED" Then
                    Me.MaximumInstallment = .ECI
                ElseIf Me.PrepaymentType = "PD" Then
                    Me.MaximumInstallment = .OutstandingPrincipal
                End If
                Me.InsurancePaidBy = .InsurancePaidBy
                Me.ContractPrepaidAmount = .Prepaid
                Me.MaximumLCInstallFee = .LcInstallment
                Me.MaximumInstallCollFee = .InstallmentCollFee

                Me.MaximumInsurance = .InsuranceDue
                Me.MaximumLCInsuranceFee = .LcInsurance
                Me.MaximumInsuranceCollFee = .InsuranceCollFee

                Me.MaximumPDCBounceFee = .PDCBounceFee
                Me.MaximumSTNKRenewalFee = .STNKRenewalFee
                Me.MaximumInsuranceClaimFee = .InsuranceClaimExpense
                Me.MaximumReposessionFee = .RepossessionFee

                Me.MaximumOutStandingInterest = .OutStandingInterest
                Me.MaximumOutStandingPrincipal = .OutstandingPrincipal
                Me.IsValidPrepayment = True

                Me.InstallmentAmount = .installmentamount
                Me.NextInstallmentDate = .NextInstallmentDate
                Me.NextInstallmentNumber = .NextInstallmentNumber

                Me.FundingCoyName = .FundingCoyName
                Me.FundingPledgeStatus = .FundingPledgeStatus
                Me.ResiduValue = .ResiduValue
                Me.NADate = .NADate
                Me.NAAMount = .NAAmount

                Me.PrincipalAmountDue = .PrincipalAmountDue
                Me.InterestAmountDue = .InterestAmountDue
            Catch ex As Exception
                ex.Message.ToString()
            End Try


            'If .TerminationPenalty > (.OutStandingInterest - .AccruedInterest) Then
            '    IsValidPrepayment = False
            'Else
            '    IsValidPrepayment = True
            'End If

        End With
    End Sub
End Class