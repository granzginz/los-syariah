﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucBankAccountBranch.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.ucBankAccountBranch" %>
<%@ Register TagPrefix="uc1" TagName="ValidDate" Src="ValidDate.ascx" %>
<asp:DropDownList ID="cmbBank" runat="server">
</asp:DropDownList>
<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Harap pilih rekening Bank"
    Display="Dynamic" ControlToValidate="cmbBank" InitialValue="0" CssClass ="validator_general"></asp:RequiredFieldValidator>
