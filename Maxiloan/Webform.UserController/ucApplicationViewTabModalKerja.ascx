﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucApplicationViewTabModalKerja.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.ucApplicationViewTabModalKerja" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<div class="tab_container">
    <div id="tabAplikasi" runat="server">
        <asp:HyperLink ID="hypAplikasi" runat="server">APLIKASI</asp:HyperLink></div>
    <div id="tabInvoice" runat="server">
        <asp:HyperLink ID="hypInvoice" runat="server">INVOICE</asp:HyperLink></div> 
    <div id="tabFinancial" runat="server">
        <asp:HyperLink ID="hypFinancial" runat="server">FINANCIAL</asp:HyperLink></div>
    <div id="tabAmortisasi" runat="server">
        <asp:HyperLink ID="hypAmortisasi" runat="server">AMORTISASI</asp:HyperLink></div>
    <div id="tabActivityLog" runat="server">
        <asp:HyperLink ID="hypActivityLog" runat="server">ACTIVITY LOG</asp:HyperLink></div>
    <div id="tabPaymentHistory" runat="server">
        <asp:HyperLink ID="hypPaymentHistory" runat="server">HIST PAYMENT</asp:HyperLink></div>
    <div id="tabJournal" runat="server">
        <asp:HyperLink ID="hypJournal" runat="server">JOURNAL</asp:HyperLink></div>
    <div id="tabPlafon" runat="server">
        <asp:HyperLink ID="hypPlafon" runat="server">PLAFON</asp:HyperLink></div>
</div>
