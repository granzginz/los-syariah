﻿Public Class UcViewContactPerson
    Inherits System.Web.UI.UserControl
#Region "WriteOnly Property"

    Public WriteOnly Property ContactPerson() As String
        Set(ByVal Value As String)
            txtContactPerson.Text = Value
        End Set
    End Property

    Public WriteOnly Property ContactPersonTitle() As String
        Set(ByVal Value As String)
            txtContactPersonTitle.Text = Value
        End Set
    End Property

    Public WriteOnly Property MobilePhone() As String
        Set(ByVal Value As String)
            txtMobilePhone.Text = Value
        End Set
    End Property

    Public WriteOnly Property Email() As String
        Set(ByVal Value As String)
            txtEmail.Text = Value
        End Set
    End Property
#End Region
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

End Class