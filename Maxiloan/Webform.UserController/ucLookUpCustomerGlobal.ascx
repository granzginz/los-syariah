﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucLookUpCustomerGlobal.ascx.vb" Inherits="Maxiloan.Webform.UserController.ucLookUpCustomerGlobal" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%--<style type="text/css">
    .wp
    {
        background-color: #fff;
        width: 900px;        
        border: 1px solid #ccc;
        float: left;
    }
    
    .wp h4
    {
        margin: 0;
        color: #000;
        text-align: left;
    }
    
    .wp .gridwp
    {
        max-height: 300px;
        overflow: auto;
    }
    
    .wpbg
    {
        background-color: #000;
        filter: alpha(opacity=70);
        opacity: 0.7;
    }
    
    .wpbtnexit
    {
        position: absolute;
        float: right;
        top: 0;
        right: 0;
    }
</style>--%>
<asp:UpdatePanel runat="server" ID="updatePanel1">
    <ContentTemplate>
        <asp:Button runat="server" ID="btnLookup" Text="btnLookup" Style="display: none;" />
        <asp:ModalPopupExtender ID="ModalPopupExtender" runat="server" TargetControlID="btnLookup"
            PopupControlID="Panel1" BackgroundCssClass="wpbg" CancelControlID="btnExit">
        </asp:ModalPopupExtender>
        <asp:Panel ID="Panel1" runat="server">
            <div class="wp">
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
                <asp:ImageButton runat="server" ID="btnExit" ImageUrl="../Images/exit_lookup00.png"
                    CssClass="wpbtnexit" />
                <div class="form_box_title">
                    <div class="title_strip">
                    </div>
                    <div class="form_single">
                        <h4>
                            DAFTAR CUSTOMER GLOBAL
                        </h4>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ws">
                            <asp:DataGrid ID="dtgPaging" runat="server"  AutoGenerateColumns="False"
                                 DataKeyField="Name" BorderStyle="None" BorderWidth="0"
                                CssClass="grid_general">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="EDIT">
                                        <ItemStyle CssClass="command_col"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" ImageUrl="../Images/iconedit.png"
                                                CommandName="Edit"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="NAMA" SortExpression="Name">
                                        <ItemStyle CssClass="name_col"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkCustomer" runat="server" CausesValidation="false" Text='<%# DataBinder.eval(Container,"DataItem.Name") %>'>
                                            </asp:LinkButton>
                                            <asp:Label ID="lblCust" runat="server" Visible="false" Text='<%# DataBinder.eval(Container,"DataItem.CustomerID") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="CustomerType" ItemStyle-HorizontalAlign="center" SortExpression="CustomerType"
                                        HeaderText="JENIS" ItemStyle-CssClass="short_col"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="Address" SortExpression="Address" HeaderText="ALAMAT">
                                        <ItemStyle CssClass="address_col"></ItemStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="CustomerID" Visible="False" HeaderText="CustomerID">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="BranchFullName" HeaderText="CABANG">
                                    </asp:BoundColumn>
                                   
                                </Columns>
                            </asp:DataGrid>
                        </div>
                        <div class="grid_wrapper_ns">
                            <div class="button_gridnavigation">
                                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../Images/grid_navbutton01.png"
                                    CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../Images/grid_navbutton02.png"
                                    CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../Images/grid_navbutton03.png"
                                    CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../Images/grid_navbutton04.png"
                                    CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                                    EnableViewState="False"></asp:Button>
                                <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtPage"
                                    MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer"
                                    CssClass="validator_general"></asp:RangeValidator>
                                <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                                    ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                            </div>
                            <div class="label_gridnavigation">
                                <asp:Label ID="lblPage" runat="server"></asp:Label>of
                                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_auto">
                            Find By</label>
                        <asp:DropDownList ID="cboSearchBy" runat="server">
                            <asp:ListItem Value="CustomerName">Customer Name</asp:ListItem>
                            <asp:ListItem Value="CustomerAddress">Customer Address</asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtSearchBy" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="btnSearch" runat="server" CausesValidation="False" Text="Find" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="btnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
                    </asp:Button>
                </div>
            </div>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>