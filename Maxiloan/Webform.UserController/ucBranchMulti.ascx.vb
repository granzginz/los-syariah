﻿Imports Maxiloan.Controller
Imports Maxiloan.General.CommonCacheHelper

Public Class ucBranchMulti
    Inherits Maxiloan.Webform.ControlBased
    Private m_controller As New DataUserControlController
    Public Delegate Sub LookupSelectedHandler(ByVal strSelectedID As String)
    Public Event LookupSelected As LookupSelectedHandler

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim DtBranchName As New DataTable
            DtBranchName = CType(Me.Cache.Item(CACHE_BRANCH_ALL), DataTable)
            If DtBranchName Is Nothing Then
                Dim DtBranchNameCache As New DataTable
                DtBranchNameCache = m_controller.GetBranchAll(GetConnectionString)
                Me.Cache.Insert(CACHE_BRANCH_ALL, DtBranchNameCache, Nothing, DateTime.Now.AddHours(DEFAULT_TIME_DURATION_CACHE_HOUR), TimeSpan.Zero)
                DtBranchName = CType(Me.Cache.Item(CACHE_BRANCH_ALL), DataTable)
            End If

            gv1.DataSource = DtBranchName
            gv1.DataBind()
        End If


    End Sub


    Private Sub btnSelect_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSelect.Click
        'comments :tidak jadi buat control ini, lanjutkan dari sini jika perlu

        'Dim strID As StringBuilder
        'For Each oRow As GridViewRow In gv1.Rows
        '    If oRow.Cells(0).      strID.Append(oRow.Cells(0)      
        'Next

        'RaiseEvent LookupSelected(dtgSupplier.DataKeys.Item(i).ToString, lblSupplierName.Text.Trim, isPF)
    End Sub
End Class