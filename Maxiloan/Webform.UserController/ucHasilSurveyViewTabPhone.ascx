﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucHasilSurveyViewTabPhone.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.ucHasilSurveyViewTabPhone" %>
<%@ Register TagPrefix="uc1" TagName="ucNumberFormat" Src="ucNumberFormat.ascx" %>

<script src="../Maxiloan.js" type="text/javascript"></script>
<link href="../include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
<script src="../js/jquery-1.9.1.min.js"></script>
<script src="../js/jquery-ui-1.10.3.custom.min.js"></script>

<div runat="server" id="jlookupContent" />

<asp:HiddenField runat="server" ID="hdnAppID" />
<%--<asp:HiddenField runat="server" ID="hdnName" />--%>
<asp:Panel runat="server" ID="pnlTelepon">
    <div class="form_box_usercontrol">
        <div class="form_left_usercontrol">
            <label class="label_split" runat="server" id="lblTelepon1">
                Telepon Rumah
            </label>
            <asp:Label runat="server" ID="LblAreaPhoneHome"></asp:Label>
            <asp:Label ID="lblStrip1" runat="server">-</asp:Label>
            <asp:Label runat="server" ID="LblPhoneHome"></asp:Label>
        </div>
    </div>
    <div class="form_box_usercontrol">
        <div class="form_left_usercontrol">
            <label class="label_split">
                Telepon Kantor
            </label>
            <asp:Label runat="server" ID="LblAreaPhoneOffice"></asp:Label>
            <asp:Label ID="lblStrip2" runat="server">-</asp:Label>
            <asp:Label runat="server" ID="LblPhoneOffice"></asp:Label>
        </div>
    </div>
    <div class="form_box_usercontrol">
        <div class="form_left_usercontrol">
            <label class="label_split">
                Handphone
            </label>
            <asp:Label runat="server" ID="LblHandphone"></asp:Label>
        </div>
    </div>
    <div class="form_box_usercontrol">
        <div class="form_left_usercontrol">
            <label class="label_split">
                Telp Emergency Contact
            </label>
            <asp:Label runat="server" ID="LblEmergencyPhone"></asp:Label>
        </div>
    </div>
    </asp:Panel>