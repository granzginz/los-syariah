﻿Option Strict On
Public Class ucReferensi
    Inherits ControlBased

#Region "Properties"
    Public Property Name As String
        Get
            'Return CType(ViewState("GuarantorName"), String)
            Return txtNamaReferensi.Text.Trim
        End Get
        Set(ByVal value As String)
            'ViewState("GuarantorName") = value
            txtNamaReferensi.Text = value
        End Set
    End Property

    Public Property Jabatan As String
        Get
            'Return CType(ViewState("GuarantorJabatan"), String)
            Return txtJabatan.Text.Trim
        End Get
        Set(ByVal value As String)
            'ViewState("GuarantorJabatan") = value
            txtJabatan.Text = value
        End Set
    End Property

    Public Property Address As String
        Get
            'Return CType(ViewState("GuarantorAddress"), String)
            Return UcCompanyAddress1.Address
        End Get
        Set(ByVal value As String)
            'ViewState("GuarantorAddress") = value
            UcCompanyAddress1.Address = value
        End Set
    End Property

    Public Property RT As String
        Get
            'Return CType(ViewState("GuarantorRT"), String)
            Return UcCompanyAddress1.RT.Trim
        End Get
        Set(ByVal value As String)
            'ViewState("GuarantorRT") = value
            UcCompanyAddress1.RT = value
        End Set
    End Property

    Public Property RW As String
        Get
            'Return CType(ViewState("GuarantorRW"), String)
            Return UcCompanyAddress1.RW.Trim
        End Get
        Set(ByVal value As String)
            'ViewState("GuarantorRW") = value
            UcCompanyAddress1.RW = value
        End Set
    End Property

    Public Property Kelurahan As String
        'Get
        '    Return CType(ViewState("GuarantorKelurahan"), String)
        'End Get
        'Set(ByVal value As String)
        '    ViewState("GuarantorKelurahan") = value
        'End Set
        Get
            Return UcCompanyAddress1.Kelurahan.Trim
        End Get
        Set(ByVal value As String)
            UcCompanyAddress1.Kelurahan = value
        End Set
    End Property

    Public Property Kecamatan As String
        'Get
        '    Return CType(ViewState("GuarantorKecamatan"), String)
        'End Get
        'Set(ByVal value As String)
        '    ViewState("GuarantorKecamatan") = value
        'End Set
        Get
            Return UcCompanyAddress1.Kecamatan.Trim
        End Get
        Set(ByVal value As String)
            UcCompanyAddress1.Kecamatan = value
        End Set
    End Property

    Public Property Kota As String
        'Get
        '    Return CType(ViewState("GuarantorKota"), String)
        'End Get
        'Set(ByVal value As String)
        '    ViewState("GuarantorKota") = value
        'End Set
        Get
            Return UcCompanyAddress1.City.Trim
        End Get
        Set(ByVal value As String)
            UcCompanyAddress1.City = value
        End Set
    End Property

    Public Property KodePos As String
        'Get
        '    Return CType(ViewState("GuarantorKodePos"), String)
        'End Get
        'Set(ByVal value As String)
        '    ViewState("GuarantorKodePos") = value
        'End Set
        Get
            Return UcCompanyAddress1.ZipCode.Trim
        End Get
        Set(ByVal value As String)
            UcCompanyAddress1.ZipCode = value
        End Set
    End Property

    Public Property Area1 As String
        'Get
        '    Return CType(ViewState("GuarantorArea1"), String)
        'End Get
        'Set(ByVal value As String)
        '    ViewState("GuarantorArea1") = value
        'End Set
        Get
            Return UcCompanyAddress1.AreaPhone1.Trim
        End Get
        Set(ByVal value As String)
            UcCompanyAddress1.AreaPhone1 = value
        End Set
    End Property

    Public Property Phone1 As String
        'Get
        '    Return CType(ViewState("GuarantorPhone1"), String)
        'End Get
        'Set(ByVal value As String)
        '    ViewState("GuarantorPhone1") = value
        'End Set
        Get
            Return UcCompanyAddress1.Phone1.Trim
        End Get
        Set(ByVal value As String)
            UcCompanyAddress1.Phone1 = value
        End Set
    End Property

    Public Property Area2 As String
        'Get
        '    Return CType(ViewState("GuarantorArea2"), String)
        'End Get
        'Set(ByVal value As String)
        '    ViewState("GuarantorArea2") = value
        'End Set
        Get
            Return UcCompanyAddress1.AreaPhone2.Trim
        End Get
        Set(ByVal value As String)
            UcCompanyAddress1.AreaPhone2 = value
        End Set
    End Property

    Public Property Phone2 As String
        'Get
        '    Return CType(ViewState("GuarantorPhone2"), String)
        'End Get
        'Set(ByVal value As String)
        '    ViewState("GuarantorPhone2") = value
        'End Set
        Get
            Return UcCompanyAddress1.Phone2.Trim
        End Get
        Set(ByVal value As String)
            UcCompanyAddress1.Phone2 = value
        End Set
    End Property

    Public Property AreaFax As String
        'Get
        '    Return CType(ViewState("GuarantorAreaFax"), String)
        'End Get
        'Set(ByVal value As String)
        '    ViewState("GuarantorAreaFax") = value
        'End Set
        Get
            Return UcCompanyAddress1.AreaFax.Trim
        End Get
        Set(ByVal value As String)
            UcCompanyAddress1.AreaFax = value
        End Set
    End Property

    Public Property Fax As String
        'Get
        '    Return CType(ViewState("GuarantorFax"), String)
        'End Get
        'Set(ByVal value As String)
        '    ViewState("GuarantorFax") = value
        'End Set
        Get
            Return UcCompanyAddress1.Fax.Trim
        End Get
        Set(ByVal value As String)
            UcCompanyAddress1.Fax = value
        End Set
    End Property

    Public Property NoHP As String
        'Get
        '    Return CType(ViewState("GuarantorNoHP"), String)
        'End Get
        'Set(ByVal value As String)
        '    ViewState("GuarantorNoHP") = value
        'End Set
        Get
            Return txtNoHP.Text.Trim
        End Get
        Set(ByVal value As String)
            txtNoHP.Text = value
        End Set
    End Property

    Public Property Email As String
        'Get
        '    Return CType(ViewState("GuarantorEmail"), String)
        'End Get
        'Set(ByVal value As String)
        '    ViewState("GuarantorEmail") = value
        'End Set
        Get
            Return txtEmail.Text.Trim
        End Get
        Set(ByVal value As String)
            txtEmail.Text = value
        End Set
    End Property

    Public Property Catatan As String
        'Get
        '    Return CType(ViewState("GuarantorCatatan"), String)
        'End Get
        'Set(ByVal value As String)
        '    ViewState("GuarantorCatatan") = value
        'End Set
        Get
            Return txtCatatan.Text.Trim
        End Get
        Set(ByVal value As String)
            txtCatatan.Text = value
        End Set
    End Property

    'Public Property Name As String
    '    Get
    '        Return ViewState("ReferensiName").ToString()
    '    End Get
    '    Set(ByVal value As String)
    '        ViewState("ReferensiName") = value
    '    End Set
    'End Property

    'Public Property Jabatan As String
    '    Get
    '        Return ViewState("ReferensiJabatan").ToString()
    '    End Get
    '    Set(ByVal value As String)
    '        ViewState("ReferensiJabatan") = value
    '    End Set
    'End Property

    'Public Property Address As String
    '    Get
    '        Return ViewState("ReferensiAddress").ToString()
    '    End Get
    '    Set(ByVal value As String)
    '        ViewState("ReferensiAddress") = value
    '    End Set
    'End Property

    'Public Property RT As String
    '    Get
    '        Return ViewState("ReferensiRT").ToString()
    '    End Get
    '    Set(ByVal value As String)
    '        ViewState("ReferensiRT") = value
    '    End Set
    'End Property

    'Public Property RW As String
    '    Get
    '        Return ViewState("ReferensiRW").ToString()
    '    End Get
    '    Set(ByVal value As String)
    '        ViewState("ReferensiRW") = value
    '    End Set
    'End Property

    'Public Property Kelurahan As String
    '    Get
    '        Return ViewState("ReferensiKelurahan").ToString()
    '    End Get
    '    Set(ByVal value As String)
    '        ViewState("ReferensiKelurahan") = value
    '    End Set
    'End Property

    'Public Property Kecamatan As String
    '    Get
    '        Return ViewState("ReferensiKecamatan").ToString()
    '    End Get
    '    Set(ByVal value As String)
    '        ViewState("ReferensiKecamatan") = value
    '    End Set
    'End Property

    'Public Property Kota As String
    '    Get
    '        Return ViewState("ReferensiKota").ToString()
    '    End Get
    '    Set(ByVal value As String)
    '        ViewState("ReferensiKota") = value
    '    End Set
    'End Property

    'Public Property KodePos As String
    '    Get
    '        Return ViewState("ReferensiKodePos").ToString()
    '    End Get
    '    Set(ByVal value As String)
    '        ViewState("ReferensiKodePos") = value
    '    End Set
    'End Property

    'Public Property Area1 As String
    '    Get
    '        Return ViewState("ReferensiArea1").ToString()
    '    End Get
    '    Set(ByVal value As String)
    '        ViewState("ReferensiArea1") = value
    '    End Set
    'End Property

    'Public Property Phone1 As String
    '    Get
    '        Return ViewState("ReferensiPhone1").ToString()
    '    End Get
    '    Set(ByVal value As String)
    '        ViewState("ReferensiPhone1") = value
    '    End Set
    'End Property

    'Public Property Area2 As String
    '    Get
    '        Return ViewState("ReferensiArea2").ToString()
    '    End Get
    '    Set(ByVal value As String)
    '        ViewState("ReferensiArea2") = value
    '    End Set
    'End Property

    'Public Property Phone2 As String
    '    Get
    '        Return ViewState("ReferensiPhone2").ToString()
    '    End Get
    '    Set(ByVal value As String)
    '        ViewState("ReferensiPhone2") = value
    '    End Set
    'End Property

    'Public Property AreaFax As String
    '    Get
    '        Return ViewState("ReferensiAreaFax").ToString()
    '    End Get
    '    Set(ByVal value As String)
    '        ViewState("ReferensiAreaFax") = value
    '    End Set
    'End Property

    'Public Property Fax As String
    '    Get
    '        Return ViewState("ReferensiFax").ToString()
    '    End Get
    '    Set(ByVal value As String)
    '        ViewState("ReferensiFax") = value
    '    End Set
    'End Property

    'Public Property NoHP As String
    '    Get
    '        Return ViewState("ReferensiNoHP").ToString()
    '    End Get
    '    Set(ByVal value As String)
    '        ViewState("ReferensiNoHP") = value
    '    End Set
    'End Property

    'Public Property Email As String
    '    Get
    '        Return ViewState("ReferensiEmail").ToString()
    '    End Get
    '    Set(ByVal value As String)
    '        ViewState("ReferensiEmail") = value
    '    End Set
    'End Property

    'Public Property Catatan As String
    '    Get
    '        Return ViewState("ReferensiCatatan").ToString()
    '    End Get
    '    Set(ByVal value As String)
    '        ViewState("ReferensiCatatan") = value
    '    End Set
    'End Property


#End Region

    'Public Sub ValidatorAddressFalse()
    '    UcCompanyAddress1.
    'End Sub



    Public Sub bindData()
        txtNamaReferensi.Text = Me.Name
        txtJabatan.Text = Me.Jabatan
        With UcCompanyAddress1
            .Address = Me.Address
            .RT = Me.RT
            .RW = Me.RW
            .Kelurahan = Me.Kelurahan
            .Kecamatan = Me.Kecamatan
            .City = Me.Kota
            .ZipCode = Me.KodePos
            .AreaPhone1 = Me.Area1
            .Phone1 = Me.Phone1
            .AreaPhone2 = Me.Area2
            .Phone2 = Me.Phone2
            .AreaFax = Me.AreaFax
            .Fax = Me.Fax
            .BindAddress()            
        End With
        txtNoHP.Text = Me.NoHP
        txtEmail.Text = Me.Email
        txtCatatan.Text = Me.Catatan        
    End Sub

    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            With UcCompanyAddress1
                .ValidatorFalse()
                .hideMandatoryAll()
            End With
        End If
    End Sub
End Class