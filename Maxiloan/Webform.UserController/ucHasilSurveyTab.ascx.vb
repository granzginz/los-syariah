﻿Imports System.Data.SqlClient

Public Class ucHasilSurveyTab
    Inherits Maxiloan.Webform.ControlBased

    Public Property ApplicationID As String
    Private Property CustomerID As String
    Private Property SupplierGroupID As String
    Public Property ProspectAppId As String

    Public Sub setLink()
        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(GetConnectionString)
        Dim objReader As SqlDataReader

        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()

            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.CommandText = "spGetApplicationSurveyTab"
            objCommand.Parameters.Add("@ApplicationID", SqlDbType.VarChar, 20).Value = Me.ApplicationID.Trim
            objCommand.Connection = objConnection
            objReader = objCommand.ExecuteReader()

            If objReader.Read Then
                Me.CustomerID = objReader.Item("CustomerID").ToString.Trim
                Me.SupplierGroupID = "" 'objReader.Item("SupplierGroupID").ToString
                Me.ProspectAppId = objReader.Item("ProspectAppId").ToString.Trim

                If Not IsDBNull(objReader.Item("DateEntryKYC")) Then
                    hypHasilSurvey.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/HasilSurvey_002.aspx?id=" & Me.CustomerID & "&appid=" & Me.ApplicationID & "&prospectappid=" & Me.ProspectAppId & "&page=Edit"
                    hypCharacter.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/HasilSurvey_002Character.aspx?id=" & Me.CustomerID & "&appid=" & Me.ApplicationID & "&prospectappid=" & Me.ProspectAppId & "&page=Edit"
                    hypCapacity.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/HasilSurvey_002Capacity.aspx?id=" & Me.CustomerID & "&appid=" & Me.ApplicationID & "&prospectappid=" & Me.ProspectAppId & "&page=Edit"
                    hypCondition.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/HasilSurvey_002Condition.aspx?id=" & Me.CustomerID & "&appid=" & Me.ApplicationID & "&prospectappid=" & Me.ProspectAppId & "&page=Edit"
                    hypCapital.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/HasilSurvey_002Capital.aspx?id=" & Me.CustomerID & "&appid=" & Me.ApplicationID & "&prospectappid=" & Me.ProspectAppId & "&page=Edit"
                    hypCollateral.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/HasilSurvey_002Collateral.aspx?id=" & Me.CustomerID & "&appid=" & Me.ApplicationID & "&prospectappid=" & Me.ProspectAppId & "&page=Edit"
                    hypCatatan.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/HasilSurvey_002Catatan.aspx?id=" & Me.CustomerID & "&appid=" & Me.ApplicationID & "&prospectappid=" & Me.ProspectAppId & "&page=Edit"
                    hypKYC.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/HasilSurvey_002KYC.aspx?id=" & Me.CustomerID & "&appid=" & Me.ApplicationID & "&prospectappid=" & Me.ProspectAppId & "&page=Edit"
                ElseIf Not IsDBNull(objReader.Item("DateEntryCatatan")) Then
                    hypHasilSurvey.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/HasilSurvey_002.aspx?id=" & Me.CustomerID & "&appid=" & Me.ApplicationID & "&prospectappid=" & Me.ProspectAppId & "&page=Edit"
                    hypCharacter.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/HasilSurvey_002Character.aspx?id=" & Me.CustomerID & "&appid=" & Me.ApplicationID & "&prospectappid=" & Me.ProspectAppId & "&page=Edit"
                    hypCapacity.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/HasilSurvey_002Capacity.aspx?id=" & Me.CustomerID & "&appid=" & Me.ApplicationID & "&prospectappid=" & Me.ProspectAppId & "&page=Edit"
                    hypCondition.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/HasilSurvey_002Condition.aspx?id=" & Me.CustomerID & "&appid=" & Me.ApplicationID & "&prospectappid=" & Me.ProspectAppId & "&page=Edit"
                    hypCapital.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/HasilSurvey_002Capital.aspx?id=" & Me.CustomerID & "&appid=" & Me.ApplicationID & "&prospectappid=" & Me.ProspectAppId & "&page=Edit"
                    hypCollateral.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/HasilSurvey_002Collateral.aspx?id=" & Me.CustomerID & "&appid=" & Me.ApplicationID & "&prospectappid=" & Me.ProspectAppId & "&page=Edit"
                    hypCatatan.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/HasilSurvey_002Catatan.aspx?id=" & Me.CustomerID & "&appid=" & Me.ApplicationID & "&prospectappid=" & Me.ProspectAppId & "&page=Edit"
                    hypKYC.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/HasilSurvey_002KYC.aspx?id=" & Me.CustomerID & "&appid=" & Me.ApplicationID & "&prospectappid=" & Me.ProspectAppId & "&page=Edit"
                ElseIf Not IsDBNull(objReader.Item("DateEntryCollateral")) Then
                    hypHasilSurvey.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/HasilSurvey_002.aspx?id=" & Me.CustomerID & "&appid=" & Me.ApplicationID & "&prospectappid=" & Me.ProspectAppId & "&page=Edit"
                    hypCharacter.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/HasilSurvey_002Character.aspx?id=" & Me.CustomerID & "&appid=" & Me.ApplicationID & "&prospectappid=" & Me.ProspectAppId & "&page=Edit"
                    hypCapacity.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/HasilSurvey_002Capacity.aspx?id=" & Me.CustomerID & "&appid=" & Me.ApplicationID & "&prospectappid=" & Me.ProspectAppId & "&page=Edit"
                    hypCondition.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/HasilSurvey_002Condition.aspx?id=" & Me.CustomerID & "&appid=" & Me.ApplicationID & "&prospectappid=" & Me.ProspectAppId & "&page=Edit"
                    hypCapital.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/HasilSurvey_002Capital.aspx?id=" & Me.CustomerID & "&appid=" & Me.ApplicationID & "&prospectappid=" & Me.ProspectAppId & "&page=Edit"
                    hypCollateral.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/HasilSurvey_002Collateral.aspx?id=" & Me.CustomerID & "&appid=" & Me.ApplicationID & "&prospectappid=" & Me.ProspectAppId & "&page=Edit"
                    hypCatatan.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/HasilSurvey_002Catatan.aspx?id=" & Me.CustomerID & "&appid=" & Me.ApplicationID & "&prospectappid=" & Me.ProspectAppId & "&page=Edit"
                    hypKYC.NavigateUrl = ""
                ElseIf Not IsDBNull(objReader.Item("DateEntryCapital")) Then
                    hypHasilSurvey.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/HasilSurvey_002.aspx?id=" & Me.CustomerID & "&appid=" & Me.ApplicationID & "&prospectappid=" & Me.ProspectAppId & "&page=Edit"
                    hypCharacter.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/HasilSurvey_002Character.aspx?id=" & Me.CustomerID & "&appid=" & Me.ApplicationID & "&prospectappid=" & Me.ProspectAppId & "&page=Edit"
                    hypCapacity.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/HasilSurvey_002Capacity.aspx?id=" & Me.CustomerID & "&appid=" & Me.ApplicationID & "&prospectappid=" & Me.ProspectAppId & "&page=Edit"
                    hypCondition.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/HasilSurvey_002Condition.aspx?id=" & Me.CustomerID & "&appid=" & Me.ApplicationID & "&prospectappid=" & Me.ProspectAppId & "&page=Edit"
                    hypCapital.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/HasilSurvey_002Capital.aspx?id=" & Me.CustomerID & "&appid=" & Me.ApplicationID & "&prospectappid=" & Me.ProspectAppId & "&page=Edit"
                    hypCollateral.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/HasilSurvey_002Collateral.aspx?id=" & Me.CustomerID & "&appid=" & Me.ApplicationID & "&prospectappid=" & Me.ProspectAppId & "&page=Edit"
                    hypCatatan.NavigateUrl = ""
                    hypKYC.NavigateUrl = ""
                ElseIf Not IsDBNull(objReader.Item("DateEntryCondition")) Then
                    hypHasilSurvey.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/HasilSurvey_002.aspx?id=" & Me.CustomerID & "&appid=" & Me.ApplicationID & "&prospectappid=" & Me.ProspectAppId & "&page=Edit"
                    hypCharacter.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/HasilSurvey_002Character.aspx?id=" & Me.CustomerID & "&appid=" & Me.ApplicationID & "&prospectappid=" & Me.ProspectAppId & "&page=Edit"
                    hypCapacity.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/HasilSurvey_002Capacity.aspx?id=" & Me.CustomerID & "&appid=" & Me.ApplicationID & "&prospectappid=" & Me.ProspectAppId & "&page=Edit"
                    hypCondition.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/HasilSurvey_002Condition.aspx?id=" & Me.CustomerID & "&appid=" & Me.ApplicationID & "&prospectappid=" & Me.ProspectAppId & "&page=Edit"
                    hypCapital.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/HasilSurvey_002Capital.aspx?id=" & Me.CustomerID & "&appid=" & Me.ApplicationID & "&prospectappid=" & Me.ProspectAppId & "&page=Edit"
                    hypCollateral.NavigateUrl = ""
                    hypCatatan.NavigateUrl = ""
                    hypKYC.NavigateUrl = ""
                ElseIf Not IsDBNull(objReader.Item("DateEntryCapacity")) Then
                    hypHasilSurvey.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/HasilSurvey_002.aspx?id=" & Me.CustomerID & "&appid=" & Me.ApplicationID & "&prospectappid=" & Me.ProspectAppId & "&page=Edit"
                    hypCharacter.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/HasilSurvey_002Character.aspx?id=" & Me.CustomerID & "&appid=" & Me.ApplicationID & "&prospectappid=" & Me.ProspectAppId & "&page=Edit"
                    hypCapacity.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/HasilSurvey_002Capacity.aspx?id=" & Me.CustomerID & "&appid=" & Me.ApplicationID & "&prospectappid=" & Me.ProspectAppId & "&page=Edit"
                    hypCondition.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/HasilSurvey_002Condition.aspx?id=" & Me.CustomerID & "&appid=" & Me.ApplicationID & "&prospectappid=" & Me.ProspectAppId & "&page=Edit"
                    hypCapital.NavigateUrl = ""
                    hypCollateral.NavigateUrl = ""
                    hypCatatan.NavigateUrl = ""
                    hypKYC.NavigateUrl = ""
                ElseIf Not IsDBNull(objReader.Item("DateEntryCharacter")) Then
                    hypHasilSurvey.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/HasilSurvey_002.aspx?id=" & Me.CustomerID & "&appid=" & Me.ApplicationID & "&prospectappid=" & Me.ProspectAppId & "&page=Edit"
                    hypCharacter.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/HasilSurvey_002Character.aspx?id=" & Me.CustomerID & "&appid=" & Me.ApplicationID & "&prospectappid=" & Me.ProspectAppId & "&page=Edit"
                    hypCapacity.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/HasilSurvey_002Capacity.aspx?id=" & Me.CustomerID & "&appid=" & Me.ApplicationID & "&prospectappid=" & Me.ProspectAppId & "&page=Edit"
                    hypCondition.NavigateUrl = ""
                    hypCapital.NavigateUrl = ""
                    hypCollateral.NavigateUrl = ""
                    hypCatatan.NavigateUrl = ""
                    hypKYC.NavigateUrl = ""
                ElseIf Not IsDBNull(objReader.Item("DateEntryHasilSurvey")) Then
                    hypHasilSurvey.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/HasilSurvey_002.aspx?id=" & Me.CustomerID & "&appid=" & Me.ApplicationID & "&prospectappid=" & Me.ProspectAppId & "&page=Edit"
                    hypCharacter.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/HasilSurvey_002Character.aspx?id=" & Me.CustomerID & "&appid=" & Me.ApplicationID & "&prospectappid=" & Me.ProspectAppId & "&page=Edit"
                    hypCapacity.NavigateUrl = ""
                    hypCondition.NavigateUrl = ""
                    hypCapital.NavigateUrl = ""
                    hypCollateral.NavigateUrl = ""
                    hypCatatan.NavigateUrl = ""
                    hypKYC.NavigateUrl = ""
                Else
                    hypHasilSurvey.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/HasilSurvey_002.aspx?id=" & Me.CustomerID & "&appid=" & Me.ApplicationID & "&prospectappid=" & Me.ProspectAppId & "&page=Edit"
                    hypCharacter.NavigateUrl = ""
                    hypCapacity.NavigateUrl = ""
                    hypCondition.NavigateUrl = ""
                    hypCapital.NavigateUrl = ""
                    hypCollateral.NavigateUrl = ""
                    hypCatatan.NavigateUrl = ""
                    hypKYC.NavigateUrl = ""
                End If

            End If

            objReader.Close()
        Catch ex As Exception
            Throw New Exception(ex.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
            objCommand.Dispose()
        End Try
    End Sub

    Public Sub selectedTab(ByVal strTab As String)
        Select Case strTab
            Case "Survey"
                tabSurvey.Attributes.Add("class", "tab_selected")
                tabCharacter.Attributes.Add("class", "tab_notselected")
                tabCapacity.Attributes.Add("class", "tab_notselected")
                tabCondition.Attributes.Add("class", "tab_notselected")
                tabCapital.Attributes.Add("class", "tab_notselected")
                tabCollateral.Attributes.Add("class", "tab_notselected")
                tabCatatan.Attributes.Add("class", "tab_notselected")
                tabKYC.Attributes.Add("class", "tab_notselected")
            Case "Character"
                tabSurvey.Attributes.Add("class", "tab_notselected")
                tabCharacter.Attributes.Add("class", "tab_selected")
                tabCapacity.Attributes.Add("class", "tab_notselected")
                tabCondition.Attributes.Add("class", "tab_notselected")
                tabCapital.Attributes.Add("class", "tab_notselected")
                tabCollateral.Attributes.Add("class", "tab_notselected")
                tabCatatan.Attributes.Add("class", "tab_notselected")
                tabKYC.Attributes.Add("class", "tab_notselected")
            Case "Capacity"
                tabSurvey.Attributes.Add("class", "tab_notselected")
                tabCharacter.Attributes.Add("class", "tab_notselected")
                tabCapacity.Attributes.Add("class", "tab_selected")
                tabCondition.Attributes.Add("class", "tab_notselected")
                tabCapital.Attributes.Add("class", "tab_notselected")
                tabCollateral.Attributes.Add("class", "tab_notselected")
                tabCatatan.Attributes.Add("class", "tab_notselected")
                tabKYC.Attributes.Add("class", "tab_notselected")
            Case "Condition"
                tabSurvey.Attributes.Add("class", "tab_notselected")
                tabCharacter.Attributes.Add("class", "tab_notselected")
                tabCapacity.Attributes.Add("class", "tab_notselected")
                tabCondition.Attributes.Add("class", "tab_selected")
                tabCapital.Attributes.Add("class", "tab_notselected")
                tabCollateral.Attributes.Add("class", "tab_notselected")
                tabCatatan.Attributes.Add("class", "tab_notselected")
                tabKYC.Attributes.Add("class", "tab_notselected")
            Case "Capital"
                tabSurvey.Attributes.Add("class", "tab_notselected")
                tabCharacter.Attributes.Add("class", "tab_notselected")
                tabCapacity.Attributes.Add("class", "tab_notselected")
                tabCondition.Attributes.Add("class", "tab_notselected")
                tabCapital.Attributes.Add("class", "tab_selected")
                tabCollateral.Attributes.Add("class", "tab_notselected")
                tabCatatan.Attributes.Add("class", "tab_notselected")
                tabKYC.Attributes.Add("class", "tab_notselected")
            Case "Collateral"
                tabSurvey.Attributes.Add("class", "tab_notselected")
                tabCharacter.Attributes.Add("class", "tab_notselected")
                tabCapacity.Attributes.Add("class", "tab_notselected")
                tabCondition.Attributes.Add("class", "tab_notselected")
                tabCapital.Attributes.Add("class", "tab_notselected")
                tabCollateral.Attributes.Add("class", "tab_selected")
                tabCatatan.Attributes.Add("class", "tab_notselected")
                tabKYC.Attributes.Add("class", "tab_notselected")
            Case "Catatan"
                tabSurvey.Attributes.Add("class", "tab_notselected")
                tabCharacter.Attributes.Add("class", "tab_notselected")
                tabCapacity.Attributes.Add("class", "tab_notselected")
                tabCondition.Attributes.Add("class", "tab_notselected")
                tabCapital.Attributes.Add("class", "tab_notselected")
                tabCollateral.Attributes.Add("class", "tab_notselected")
                tabCatatan.Attributes.Add("class", "tab_selected")
                tabKYC.Attributes.Add("class", "tab_notselected")
            Case Else
                tabSurvey.Attributes.Add("class", "tab_notselected")
                tabCharacter.Attributes.Add("class", "tab_notselected")
                tabCapacity.Attributes.Add("class", "tab_notselected")
                tabCondition.Attributes.Add("class", "tab_notselected")
                tabCapital.Attributes.Add("class", "tab_notselected")
                tabCollateral.Attributes.Add("class", "tab_notselected")
                tabCatatan.Attributes.Add("class", "tab_notselected")
                tabKYC.Attributes.Add("class", "tab_selected")
        End Select
    End Sub
End Class