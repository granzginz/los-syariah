﻿Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.Controller
Imports System.IO
Imports System.Data.SqlClient

Public Class UcCashier
    Inherits ControlBased

#Region "Property"
    Public Property CashierID() As String
        Get
            Return cmbCashier.SelectedItem.Value.Trim
        End Get
        Set(ByVal Value As String)
            cmbCashier.SelectedIndex = cmbCashier.Items.IndexOf(cmbCashier.Items.FindByValue(Value))
        End Set
    End Property

    Public Property CashierName() As String
        Get
            Return cmbCashier.SelectedItem.Text.Trim
        End Get
        Set(ByVal Value As String)
            cmbCashier.SelectedIndex = cmbCashier.Items.IndexOf(cmbCashier.Items.FindByText(Value))
        End Set
    End Property

    Public Property HeadCashier() As Boolean
        Get
            Return CType(ViewState("IsHeadCashier"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            ViewState("IsHeadCashier") = Value
        End Set
    End Property

    Public Event SelectedIndexChanged As SelectedIndexChangedEventHandler
    Public Delegate Sub SelectedIndexChangedEventHandler(sender As Object, args As SelectedIndexChangedEventArgs)

    'Public Shared objConnection As New SqlConnection(GetConnectionString)
#End Region

#Region " Private Const "
    Private m_controller As New DataUserControlController
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not IsPostBack Then
            Me.CashierID = ""
            Me.CashierName = ""
            Dim DtCashier As New DataTable
            If Not Me.HeadCashier Then
                DtCashier = CType(Me.Cache.Item(CACHE_CASHIER_LIST & Me.sesBranchId.Replace("'", "")), DataTable)
                If DtCashier Is Nothing Then
                    Dim DtCashierCache As New DataTable
                    DtCashierCache = m_controller.BranchCashierList(GetConnectionString, Me.sesBranchId.Replace("'", ""))
                    Me.Cache.Insert(CACHE_CASHIER_LIST & Me.sesBranchId.Replace("'", ""), DtCashierCache, Nothing, DateTime.Now.AddHours(DEFAULT_TIME_DURATION_CACHE_HOUR), TimeSpan.Zero)
                    DtCashier = CType(Me.Cache.Item(CACHE_CASHIER_LIST & Me.sesBranchId.Replace("'", "")), DataTable)
                End If
            Else
                DtCashier = CType(Me.Cache.Item(CACHE_HEAD_CASHIER & Me.sesBranchId.Replace("'", "")), DataTable)
                If DtCashier Is Nothing Then
                    Dim DtCashierCache As New DataTable
                    DtCashierCache = m_controller.BranchHeadCashierList(GetConnectionString, Me.sesBranchId.Replace("'", ""))
                    Me.Cache.Insert(CACHE_HEAD_CASHIER & Me.sesBranchId.Replace("'", ""), DtCashierCache, Nothing, DateTime.Now.AddHours(DEFAULT_TIME_DURATION_CACHE_HOUR), TimeSpan.Zero)
                    DtCashier = CType(Me.Cache.Item(CACHE_HEAD_CASHIER & Me.sesBranchId.Replace("'", "")), DataTable)
                End If
            End If
            cmbCashier.DataValueField = "ID"
            cmbCashier.DataTextField = "Name"

            cmbCashier.DataSource = DtCashier
            cmbCashier.DataBind()
            cmbCashier.Items.Insert(0, "Select One")
            cmbCashier.Items(0).Value = "0"
        End If
    End Sub

    Protected Sub ddl_SelectedIndexChanged(sender As Object, e As EventArgs)
        RaiseEvent SelectedIndexChanged(Me, New SelectedIndexChangedEventArgs(binddataval(cmbCashier.SelectedItem.Text)))
    End Sub

    Public Class SelectedIndexChangedEventArgs
        Inherits EventArgs
        Private m_selectedItem As String

        Public ReadOnly Property SelectedItem() As String
            Get
                Return m_selectedItem
            End Get
        End Property

        Public Sub New(selectedItem As String)
            m_selectedItem = selectedItem
        End Sub
    End Class

    Public Function binddataval(loginidx As String) As String
        Dim dtGrid As New DataTable
        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(GetConnectionString)
        Dim returnValue As Object
        Dim sda As New SqlDataAdapter()
        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()

            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.CommandText = "spCashierListBalance"
            objCommand.Connection = objConnection
            objCommand.Parameters.Add("@branchid", SqlDbType.VarChar, 3).Value = Me.sesBranchId.Replace("'", "")
            objCommand.Parameters.Add("@LoginId", SqlDbType.VarChar, 12).Value = loginidx.Trim
            sda.SelectCommand = objCommand

            sda.SelectCommand.ExecuteScalar()
            returnValue = objCommand.ExecuteScalar()
            Return returnValue
        Catch ex As Exception
            Response.Write(ex.Message)
        Finally
            objCommand.Dispose()
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try
        Return returnValue
    End Function
End Class