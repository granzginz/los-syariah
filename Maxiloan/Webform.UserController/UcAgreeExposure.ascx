﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UcAgreeExposure.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.UcAgreeExposure" %>
<div class="form_title">
    <div class="form_single">
        <h3>
            EXPOSURE DATA KONTRAK
        </h3>
    </div>
</div>
<div class="form_box">
    <div class="form_single">
        <h5>
            <label>
                OUTSTANDING AMOUNT
            </label>
            <asp:Label ID="labAsOf" runat="server"></asp:Label>
        </h5>
    </div>
</div>
<div class="form_box">
    <div>
        <div class="form_left">
            <label>
                Maximum Sisa Saldo
            </label>
            <asp:Label ID="lblMaxOSBalance" runat="server" CssClass="numberAlign label"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Sisa Saldo (O/S Balance)
            </label>
            <asp:Label ID="lblOSBalance" runat="server" CssClass="numberAlign label"></asp:Label>
        </div>
    </div>
</div>
<div class="form_box">
    <div>
        <div class="form_left">
            <label>
                Sisa Pokok (O/S Principal)
            </label>
            <asp:Label ID="lblOSPrincipal" runat="server" CssClass="numberAlign label"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Sisa Bunga (O/S Interest)
            </label>
            <asp:Label ID="lblOSInterest" runat="server" CssClass="numberAlign label"></asp:Label>
        </div>
    </div>
</div>
<div class="form_box_title">
    <div class="form_single">
        <h5>
            JUMLAH DARI
        </h5>
    </div>
</div>
<div class="form_box">
    <div class="form_left">
        <label>
            Asset Ditarik</label>
        <asp:Label ID="lblAssetRepossed" runat="server"></asp:Label>
    </div>
    <div class="form_right">
        <label>
            Asset masuk Inventori
        </label>
        <asp:Label ID="lblAssetInventoried" runat="server"></asp:Label>
    </div>
</div>
<div class="form_box">
    <div class="form_left">
        <label>
            Rescheduling
        </label>
        <asp:Label ID="lblRescheduling" runat="server"></asp:Label>
    </div>
    <div class="form_right">
        <label>
            Over Kontrak
        </label>
        <asp:Label ID="lblAgreementTransfer" runat="server"></asp:Label>
    </div>
</div>
<div class="form_box">
    <div class="form_left">
        <label>
            Penggantian Asset
        </label>
        <asp:Label ID="lblAssetReplacement" runat="server"></asp:Label>
    </div>
    <div class="form_right">
        <label>
            Tolakan CEK/BG
        </label>
        <asp:Label ID="lblBounceCheque" runat="server"></asp:Label>
    </div>
</div>
<div class="form_box_title">
    <div class="form_quarter">
        <h5>
            BUCKET
        </h5>
    </div>
    <div class="form_quarter numberAlign label">
        <h5>
            TOTAL PAST DUE
        </h5>
    </div>
    <div class="form_quarter">
        <h5>
            BUCKET
        </h5>
    </div>
    <div class="form_quarter numberAlign label">
        <h5>
            TOTAL PAST DUE
        </h5>
    </div>
</div>
<div class="form_box">
    <div class="form_quarter">
        <asp:Label ID="lblBucket1text" runat="server">
        </asp:Label>
    </div>
    <div class="form_quarter">
        <asp:Label ID="lblBucket1" runat="server" CssClass="numberAlign label">
        </asp:Label>
    </div>
    <div class="form_quarter">
        <label>
            <asp:Label ID="lblBucket6Text" runat="server">
            </asp:Label>
        </label>
    </div>
    <div class="form_quarter">
        <asp:Label ID="lblBucket6" runat="server" CssClass="numberAlign label">
        </asp:Label>
    </div>
</div>
<div class="form_box">
    <div class="form_quarter">
        <asp:Label ID="lblBucket2text" runat="server"></asp:Label>
    </div>
    <div class="form_quarter">
        <asp:Label ID="lblBucket2" runat="server" CssClass="numberAlign label"></asp:Label>
    </div>
    <div class="form_quarter">
        <label>
            <asp:Label ID="lblBucket7text" runat="server"></asp:Label></label>
    </div>
    <div class="form_quarter">
        <asp:Label ID="lblBucket7" runat="server" CssClass="numberAlign label"></asp:Label>
    </div>
</div>
<div class="form_box">
    <div class="form_quarter">
        <asp:Label ID="lblBucket3Text" runat="server"></asp:Label>
    </div>
    <div class="form_quarter">
        <asp:Label ID="lblBucket3" runat="server" CssClass="numberAlign label"></asp:Label>
    </div>
    <div class="form_quarter">
        <label>
            <asp:Label ID="lblBucket8Text" runat="server"></asp:Label></label>
    </div>
    <div class="form_quarter">
        <asp:Label ID="lblBucket8" runat="server" CssClass="numberAlign label"></asp:Label>
    </div>
</div>
<div class="form_box">
    <div class="form_quarter">
        <asp:Label ID="lblBucket4Text" runat="server"></asp:Label>
    </div>
    <div class="form_quarter">
        <asp:Label ID="lblBucket4" runat="server" CssClass="numberAlign label"></asp:Label>
    </div>
    <div class="form_quarter">
        <label>
            <asp:Label ID="lblBucket9Text" runat="server"></asp:Label></label>
    </div>
    <div class="form_quarter">
        <asp:Label ID="lblBucket9" runat="server" CssClass="numberAlign label"></asp:Label>
    </div>
</div>
<div class="form_box">
    <div class="form_quarter">
        <asp:Label ID="lblBucket5Text" runat="server"></asp:Label>
    </div>
    <div class="form_quarter">
        <asp:Label ID="lblBucket5" runat="server" CssClass="numberAlign label"></asp:Label>
    </div>
    <div class="form_quarter">
        <label>
            <asp:Label ID="lblBucket10Text" runat="server"></asp:Label></label>
    </div>
    <div class="form_quarter">
        <asp:Label ID="lblBucket10" runat="server" CssClass="numberAlign label"></asp:Label>
    </div>
</div>
<div class="form_box">
    <div class="form_quarter">
        <asp:Label ID="Label3" runat="server"></asp:Label>
    </div>
    <div class="form_quarter">
        <asp:Label ID="Label4" runat="server"></asp:Label>
    </div>
    <div class="form_quarter">
        <h5>
            TOTAL
        </h5>
    </div>
    <div class="form_quarter">
        <asp:Label ID="lblTotalAllBucket" runat="server" CssClass="numberAlign label"></asp:Label>
    </div>
</div>
