﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UcLookupAsset.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.UcLookupAsset" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:UpdatePanel runat="server" ID="updatePanel1">
    <ContentTemplate>
        <asp:Button runat="server" ID="btnLookup" Text="btnLookup" Style="display: none;" />
        <asp:ModalPopupExtender ID="ModalPopupExtender" runat="server" TargetControlID="btnLookup"
            PopupControlID="Panel1" BackgroundCssClass="wpbg" CancelControlID="btnExit">
        </asp:ModalPopupExtender>
        <asp:Panel ID="Panel1" runat="server">
            <div class="wp">
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
                <asp:ImageButton runat="server" ID="btnExit" ImageUrl="../Images/exit_lookup00.png"
                    CssClass="wpbtnexit" />
                <div class="form_title">
                    <div class="title_strip">
                    </div>
                    <div class="form_single">
                        <h4>
                            DAFTAR ASSET</h4>
                    </div>
                </div>
                <asp:Panel ID="panelGrid" runat="server" Visible="false">
                    <div class="form_box_header">
                        <div class="form_single">
                            <div class="grid_wrapper_ns">
                                <asp:DataGrid ID="dtgPaging" runat="server" CellPadding="0" OnSortCommand="SortGrid"
                                    BorderStyle="None" DataKeyField="AssetCode" AutoGenerateColumns="False" AllowSorting="True"
                                    AllowPaging="True" Width="100%" Visible="True" CssClass="grid_general">
                                    <HeaderStyle CssClass="th" />
                                    <ItemStyle CssClass="item_grid" />
                                    <Columns>
                                        <asp:ButtonColumn Text="Select" CommandName="Select" ButtonType="LinkButton"></asp:ButtonColumn>
                                        <asp:BoundColumn ReadOnly="True" DataField="AssetCode" SortExpression="AssetCode"
                                            HeaderText="ASSET CODE"></asp:BoundColumn>
                                        <asp:BoundColumn ReadOnly="True" DataField="Description" SortExpression="Description"
                                            HeaderText="DESCRIPTION"></asp:BoundColumn>
                                        <asp:BoundColumn ReadOnly="True" DataField="Karoseri" SortExpression="Karoseri" HeaderText="KAROSERI"
                                            Visible="false"></asp:BoundColumn>
                                    </Columns>
                                    <PagerStyle Visible="False"></PagerStyle>
                                </asp:DataGrid>
                            </div>
                            <div class="button_gridnavigation">
                                <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" ImageUrl="../Images/grid_navbutton01.png"
                                    OnCommand="NavigationLink_Click" CommandName="First"></asp:ImageButton>
                                <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" ImageUrl="../Images/grid_navbutton02.png"
                                    OnCommand="NavigationLink_Click" CommandName="Prev"></asp:ImageButton>
                                <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" ImageUrl="../Images/grid_navbutton03.png"
                                    OnCommand="NavigationLink_Click" CommandName="Next"></asp:ImageButton>
                                <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" ImageUrl="../Images/grid_navbutton04.png"
                                    OnCommand="NavigationLink_Click" CommandName="Last"></asp:ImageButton>
                                Page
                                <asp:TextBox ID="txtGoPage" runat="server" width="34px">1</asp:TextBox>
                                <asp:Button ID="btnGoPage" runat="server" text="Go" CssClass ="small buttongo blue" EnableViewState="False" />                                
                                <asp:RangeValidator ID="Rangevalidator1" runat="server" Font-Size="11px" Type="Integer"
                                    MaximumValue="999999999" ErrorMessage="Page No. is not valid" MinimumValue="1"
                                    ControlToValidate="txtGoPage"></asp:RangeValidator>
                                <asp:RequiredFieldValidator ID="rfvGo" runat="server" ErrorMessage="Page No. is not valid"
                                    ControlToValidate="txtGoPage" Display="Dynamic" CssClass ="validator_general"></asp:RequiredFieldValidator>
                                <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtGoPage" MinimumValue="1"
                                    MaximumValue="999" ErrorMessage="Page No. is not valid" Type="Double"></asp:RangeValidator>
                            </div>
                            <div class="label_gridnavigation">
                                <asp:Label ID="lblPage" runat="server"></asp:Label>of
                                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                                <asp:Label ID="lblTotRec" runat="server"></asp:Label>record(s)
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel ID="panelSearch" runat="server">
                    <div class="form_box_title">
                        <div class="form_single">
                            <h4>
                                CARI ASSET</h4>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label>
                                Cari Berdasarkan
                            </label>
                            <asp:DropDownList ID="cboSearchBy" runat="server">
                                <asp:ListItem Value="AssetCode">Asset Code</asp:ListItem>
                                <asp:ListItem Value="Description" Selected ="True" >Description</asp:ListItem>
                            </asp:DropDownList>
                            <asp:TextBox ID="txtSearchBy" runat="server"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form_button">
                        <asp:Button ID="btnSearch" runat="server" CausesValidation="False" text= "Find" cssclass="small button blue" />
                        <asp:Button ID="btnReset" runat="server" CausesValidation="False" text= "Reset" cssclass="small button gray" />
                    </div>
                </asp:Panel>
            </div>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
