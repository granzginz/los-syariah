﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucHasilSurveyTabPhone.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.ucHasilSurveyTabPhone" %>
<%@ Register TagPrefix="uc1" TagName="ucNumberFormat" Src="ucNumberFormat.ascx" %>

<script src="../Maxiloan.js" type="text/javascript"></script>
<link href="../include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
<script src="../js/jquery-1.9.1.min.js"></script>
<script src="../js/jquery-ui-1.10.3.custom.min.js"></script>

<div runat="server" id="jlookupContent" />

<asp:HiddenField runat="server" ID="hdnAppID" />
<%--<asp:HiddenField runat="server" ID="hdnName" />--%>
<asp:Panel runat="server" ID="pnlTelepon">
    <div class="form_box_usercontrol">
        <div class="form_left_usercontrol">
            <label class="label_split" runat="server" id="lblTelepon1">
                Telepon Rumah
            </label>
            <asp:TextBox ID="txtAreaPhoneHome" MaxLength="4" runat="server" CssClass="smaller_text" onkeypress="return numbersonly2(event)"></asp:TextBox>
            <asp:Label ID="lblStrip1" runat="server">-</asp:Label>
            <asp:TextBox ID="txtPhoneHome" MaxLength="10" runat="server" CssClass="small_text" onkeypress="return numbersonly2(event)"></asp:TextBox>
        </div>
    </div>
    <div class="form_box_usercontrol">
        <div class="form_left_usercontrol">
            <label class="label_split">
                Telepon Kantor
            </label>
            <asp:TextBox ID="txtAreaPhoneOffice" MaxLength="4" CssClass="smaller_text" runat="server" onkeypress="return numbersonly2(event)"></asp:TextBox>
            <asp:Label ID="lblStrip2" runat="server">-</asp:Label>
            <asp:TextBox ID="txtPhoneOffice" MaxLength="10" runat="server" CssClass="small_text" onkeypress="return numbersonly2(event)"></asp:TextBox>
        </div>
    </div>
    <div class="form_box_usercontrol">
        <div class="form_left_usercontrol">
            <label class="label_split">
                Handphone
            </label>
            <asp:TextBox ID="txtHandphone" MaxLength="15" runat="server" CssClass="medium_text" onkeypress="return numbersonly2(event)"></asp:TextBox>
        </div>
    </div>
    <div class="form_box_usercontrol">
        <div class="form_left_usercontrol">
            <label class="label_split">
                Telp Emergency Contact
            </label>
            <asp:TextBox ID="txtEmergencyPhone" MaxLength="15" runat="server" CssClass="medium_text" onkeypress="return numbersonly2(event)"></asp:TextBox>
        </div>
    </div>
    </asp:Panel>