﻿Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.Controller
Imports Maxiloan.Parameter

Public Class ucBankAccountReconcile
    Inherits ControlBased
    Private oController As New GeneralPagingController

    Public Property BankAccountName() As String
        Get
            Return CType(cmbBankAccount.SelectedItem.Text, String)
        End Get
        Set(ByVal Value As String)
            cmbBankAccount.SelectedIndex = cmbBankAccount.Items.IndexOf(cmbBankAccount.Items.FindByText(Value))
        End Set
    End Property


    Public Property BankAccountID() As String
        Get
            Return CType(cmbBankAccount.SelectedItem.Value, String)
        End Get
        Set(ByVal Value As String)
            cmbBankAccount.SelectedIndex = cmbBankAccount.Items.IndexOf(cmbBankAccount.Items.FindByValue(Value))
        End Set
    End Property

    Public Property IsAutoPostback() As Boolean
        Get
            Return CType(ViewState("IsPostBack"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            ViewState("IsPostBack") = Value
        End Set
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not IsPostBack Then
            BindBankAccountNoCondition()
        End If
    End Sub

    Public Sub BindBankAccountNoCondition()
        Dim oEntities As New Parameter.GeneralPaging
        cmbBankAccount.AutoPostBack = Me.IsAutoPostback
        Dim strCacheName As String
        strCacheName = CACHE_BANK_ACCOUNT_RECONCILE & Me.sesBranchId.Replace("'", "")
        Dim dtBankAccount As New DataSet
        dtBankAccount = CType(Me.Cache.Item(strCacheName), DataSet)
        If dtBankAccount Is Nothing Then
            Dim dtBankAccountCache As New DataSet
            With oEntities
                .SpName = "spGetBankAccountNoCondition"
                .WhereCond = " And BranchID in ('" & Replace(Me.sesBranchId, "'", "") & "') and BankID IS NOT NULL "
                .strConnection = GetConnectionString()
            End With

            oEntities = oController.GetReportWithParameterWhereCond(oEntities)
            dtBankAccountCache = oEntities.ListDataReport

            Me.Cache.Insert(strCacheName, dtBankAccountCache, Nothing, DateTime.Now.AddHours(DEFAULT_TIME_DURATION_CACHE_HOUR), TimeSpan.Zero)
            dtBankAccount = CType(Me.Cache.Item(strCacheName), DataSet)
        End If
        cmbBankAccount.DataTextField = "Name"
        cmbBankAccount.DataValueField = "ID"
        cmbBankAccount.DataSource = dtBankAccount
        cmbBankAccount.DataBind()


        cmbBankAccount.Items.Insert(0, "Select One")
        cmbBankAccount.Items(0).Value = "0"

        If cmbBankAccount.Items.Count > 1 Then
            cmbBankAccount.SelectedIndex = 1
        Else
            cmbBankAccount.SelectedIndex = 0
        End If

        Me.BankAccountID = cmbBankAccount.SelectedItem.Value.Trim
        Me.BankAccountName = cmbBankAccount.SelectedItem.Text.Trim
    End Sub

End Class