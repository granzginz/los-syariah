﻿Public Class ucFlagTransaksi
    Inherits ControlBased

    Public Event flagChanged(i As Integer)
    Public Delegate Sub flagChangedHandler(i As Integer)

    Public Property GridIndex As Integer
        Get
            Return CType(ViewState("GridIndex"), Integer)
        End Get
        Set(value As Integer)
            ViewState("GridIndex") = value
        End Set
    End Property

    Public Property SelectedValue As String
        Get
            Return cboFlag.SelectedValue.Trim
        End Get
        Set(value As String)
            cboFlag.SelectedIndex = cboFlag.Items.IndexOf(cboFlag.Items.FindByValue(value))
        End Set
    End Property

    Public ReadOnly Property SelectedText As String
        Get
            Return cboFlag.SelectedItem.Text.Trim
        End Get
    End Property

    Public WriteOnly Property AutoPostBack As Boolean
        Set(value As Boolean)
            cboFlag.AutoPostBack = value
        End Set
    End Property

    Public Sub flagIsChanged() Handles cboFlag.SelectedIndexChanged
        RaiseEvent flagChanged(Me.GridIndex)
    End Sub

    Public Property cbo As DropDownList
        Get
            Return cboFlag
        End Get
        Set(ByVal value As DropDownList)
            cboFlag = value
        End Set
    End Property
End Class