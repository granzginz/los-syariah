﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UcAgreementListNew.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.UcAgreementListNew" %>
<script language="javascript" type="text/javascript">
    var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
    var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
    var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';			
</script>
<script src="../../Maxiloan.js" type="text/javascript"></script>
<div class="form_box_usercontrol_header">
    <div class="form_left_usercontrol">
        <asp:DataGrid ID="DtgAgreementList" AutoGenerateColumns="False" DataKeyField="ApplicationID"
            BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" BackColor="White"
            CellPadding="0" Width="100%" runat="server" CssClass="grid_general">
            <ItemStyle CssClass="item_grid"></ItemStyle>
            <HeaderStyle CssClass="th"></HeaderStyle>
            <Columns>
                <asp:TemplateColumn HeaderText="No">
                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center" Width="5%"></ItemStyle>
                    <ItemTemplate>
                        <asp:Label ID="lblNomor" runat="server" Text='<%#Container.DataItem("Nomor")%>'>
                        </asp:Label>
                    </ItemTemplate>
                </asp:TemplateColumn>
                  <asp:TemplateColumn HeaderText="PROSPECT ID">
                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                    <ItemStyle HorizontalAlign="center" Width="20%"></ItemStyle>
                    <ItemTemplate>
                        <asp:label ID="lblProspectId" runat="server" Text='<%#Container.DataItem("prospectappid")%>'>
                        </asp:label>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="APPLICATION ID">
                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                    <ItemStyle HorizontalAlign="center" Width="20%"></ItemStyle>
                    <ItemTemplate>
                        <asp:HyperLink ID="lnkApplicationID" runat="server" Text='<%#Container.DataItem("ApplicationID")%>'>
                        </asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="CONTRACT NO.">
                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left" Width="20%"></ItemStyle>
                    <ItemTemplate>
                        <asp:HyperLink ID="lnkAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>'></asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="REALIZATION DATE">
                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center" Width="15%"></ItemStyle>
                    <ItemTemplate>
                        <asp:Label ID="lblAgreementDate" runat="server" Text='<%#Container.DataItem("AgreementDate")%>'>
                        </asp:Label>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="INSTALLMENT">
                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Right" Width="20%"></ItemStyle>
                    <ItemTemplate>
                        <asp:Label ID="lblInstallmentAmount" runat="server" Text='<%#FormatNumber(Container.DataItem("InstallmentAmount"),2)%>'>
                        </asp:Label>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="OS PRINCIPAL">
                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Right" Width="20%"></ItemStyle>
                    <ItemTemplate>
                        <asp:Label ID="lblOutstandingPrincipal" runat="server" Text='<%#FormatNumber(Container.DataItem("OutstandingPrincipal"),2)%>'>
                        </asp:Label>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="STEP">
                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                    <ItemTemplate>
                        <asp:Label ID="lblApplicationStep" runat="server" Text='<%#Container.DataItem("ApplicationStep")%>'>
                        </asp:Label>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="STATUS">
                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                    <ItemTemplate>
                        <asp:Label ID="lblContractStatus" runat="server" Text='<%#Container.DataItem("ContractStatus")%>'>
                        </asp:Label>
                    </ItemTemplate>
                </asp:TemplateColumn>
            </Columns>
            <PagerStyle Visible="False" HorizontalAlign="Left" ForeColor="#000066" BackColor="White"
                Mode="NumericPages"></PagerStyle>
        </asp:DataGrid>
    </div>
</div>
