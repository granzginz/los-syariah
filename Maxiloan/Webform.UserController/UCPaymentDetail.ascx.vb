﻿
#Region "Import"
Imports System.Globalization
Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.Controller
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports System.IO

#End Region
Public Class UCPaymentDetail
    Inherits ControlBased
    Protected WithEvents scriptJava As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents hdnWop As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents hdnWopName As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents rngAmountReceive As System.Web.UI.WebControls.RangeValidator
    Private oController As New DataUserControlController
    Private dtBankAccount As New DataTable
    Protected WithEvents ucAmountReceive As ucNumberFormat

    Public Event TextChanged As TextChangedHandler



    Public Delegate Sub TextChangedHandler(ByVal sender As Object, ByVal e As System.EventArgs)

    'Private Sub txtTglBayar_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtTglBayar.TextChanged
    '    RaiseEvent TextChanged(sender, e)
    'End Sub
    'Public Property AutoPostBack As Boolean
    '    Get
    '        Return txtTglBayar.AutoPostBack
    '    End Get
    '    Set(ByVal value As Boolean)
    '        txtTglBayar.AutoPostBack = value
    '    End Set
    'End Property
#Region "Property"
#Region "PaymentDetail"

    Public Property ReceivedFrom() As String
        Get
            Return CType(txtReceivedFrom.Text.Trim, String)
        End Get
        Set(ByVal Value As String)
            txtReceivedFrom.Text = Value
        End Set
    End Property

    Public Property ReferenceNo() As String
        Get
            Return CType(txtReferenceNo.Text.Trim, String)
        End Get
        Set(ByVal Value As String)
            txtReferenceNo.Text = Value
        End Set
    End Property

    Public Property WayOfPayment() As String
        Get
            Return CType(cmbWayOfPayment.SelectedItem.Value.Trim, String)
        End Get
        Set(ByVal Value As String)
            cmbWayOfPayment.SelectedIndex = cmbWayOfPayment.Items.IndexOf(cmbWayOfPayment.Items.FindByValue(Value))
        End Set
    End Property

    Public ReadOnly Property WayOfPaymentName() As String
        Get
            Return CType(cmbWayOfPayment.SelectedItem.Text.Trim, String)
        End Get
    End Property

    Public ReadOnly Property BankAccount() As String
        Get
            Return CType(cmbBankAccount.SelectedValue.Trim, String)
        End Get
    End Property

    Public ReadOnly Property BankAccountName() As String
        Get
            Return CType(cmbBankAccount.SelectedValue.Trim, String)
        End Get
    End Property

    Public Property ValueDate() As String
        Get
            'Return CType(oValueDate.dateValue, String)
            Return CType(txtValueDate.Text, String)
        End Get
        Set(ByVal Value As String)
            'oValueDate.dateValue = Value
            txtValueDate.Text = Value
        End Set
    End Property

    Public Property AmountReceive() As Double
        Get
            Return CType(IIf(Not IsNumeric(ucAmountReceive.Text.Trim), 0, ucAmountReceive.Text.Trim), Double)
        End Get
        Set(ByVal Value As Double)
            ucAmountReceive.Text = CStr(Value)
        End Set
    End Property

    Public Property Notes() As String
        Get
            Return CType(txtNotes.Text.Trim, String)
        End Get
        Set(ByVal Value As String)
            txtNotes.Text = Value.Trim
        End Set
    End Property


    Public Property BayarDi() As String
        Get
            Return CType(ddlBayarDi.SelectedValue.Trim, String)
        End Get
        Set(ByVal Value As String)
            ddlBayarDi.SelectedValue = Value.Trim
        End Set
    End Property

    Public Property CollectorID() As String
        Get
            Return CType(ddlCollectorID.SelectedValue.Trim, String)
        End Get
        Set(ByVal Value As String)
            ddlCollectorID.SelectedValue = Value.Trim
        End Set
    End Property
    Public Property ReceiptNo() As String
        Get
            Return CType(ddNoKwitansi.SelectedValue.Trim, String)
        End Get
        Set(ByVal Value As String)
            ddNoKwitansi.SelectedValue = Value.Trim
        End Set
    End Property
    Public Property NoTTS() As String
        Get
            Return CType(txtTTS.Text, String)
        End Get
        Set(value As String)
            txtTTS.Text = value
        End Set
    End Property
    Public Property CollectorName() As String
        Get
            Return CType(ddlCollectorID.SelectedItem.Text, String)
        End Get
        Set(ByVal Value As String)
            ddlCollectorID.SelectedItem.Text = Value.Trim
        End Set
    End Property

    Public Property JenisFormPembayaran As String 'payment receive, cash/bank/collection
#End Region
    Private Property ListBankAccount() As DataTable
        Get
            Return (CType(ViewState("ListBankAccount"), DataTable))
        End Get
        Set(ByVal Value As DataTable)
            ViewState("ListBankAccount") = Value
        End Set
    End Property
    Public Property ApplicationID() As String
        Get
            Return (CType(ViewState("ApplicationID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
    Public Property IsTitle() As Boolean
        Get
            Return (CType(ViewState("IsTitle"), Boolean))
        End Get
        Set(ByVal Value As Boolean)
            ViewState("IsTitle") = Value
        End Set
    End Property

    Public Property WithOutPrepaid() As Boolean
        Get
            Return (CType(ViewState("withoutprepaid"), Boolean))
        End Get
        Set(ByVal Value As Boolean)
            ViewState("withoutprepaid") = Value
        End Set
    End Property

    Public WriteOnly Property IsCanNegative() As Boolean
        Set(ByVal Value As Boolean)
            If Value Then
                'reqAmountReceive.Enabled = False
                'regAmountReceive.Enabled = False
            End If
        End Set
    End Property

    Public Property ResultcmbBankAccount() As String
        Get
            'Return hdnResult.Value
            Return CType(cmbBankAccount.SelectedItem.Value, String)
        End Get
        Set(ByVal Value As String)
            'ViewState("SupervisorID") = Value
            cmbBankAccount.SelectedIndex = cmbBankAccount.Items.IndexOf(cmbBankAccount.Items.FindByValue(Value))
        End Set
    End Property

    Public Property isDateMandatory() As Boolean
        Get
            'Return hdnResult.Value
            Return CType(rfvValueDate.Enabled, Boolean)
        End Get
        Set(ByVal Value As Boolean)
            rfvValueDate.Enabled = Value
        End Set
    End Property
    Public Property pnlConll() As Boolean
        Get
            Return CType(pnlColl.Visible, Boolean)
        End Get
        Set(value As Boolean)
            pnlColl.Visible = value
        End Set

    End Property

    Public Property ShowTglBayar() As Boolean
        Get
            Return divTglBayar.Visible = True
        End Get
        Set(ByVal Value As Boolean)
            divTglBayar.Visible = Value
        End Set
    End Property

    Public Property PanelBank() As Boolean
        Get
            Return pnlBank.Visible = True
        End Get
        Set(ByVal Value As Boolean)
            pnlBank.Visible = Value
        End Set
    End Property

    Public Property cmbBankAccountEnabled() As Boolean
        Get
            Return cmbBankAccount.Enabled
        End Get
        Set(ByVal Value As Boolean)
            cmbBankAccount.Enabled = Value
        End Set
    End Property

    'Public Property TglBayarTxt() As TextBox
    '    Get
    '        Return txtTglBayar
    '    End Get
    '    Set(ByVal txtTglBayar As TextBox)
    '        txtTglBayar = txtTglBayar
    '    End Set
    'End Property
    Public Property BankPurpose() As String
        Get
            Return (CType(ViewState("BankPurpose"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("BankPurpose") = Value
        End Set
    End Property


    Public Property NoKwitansiTxt As DropDownList
        Get
            Return ddNoKwitansi
        End Get
        Set(ByVal ddNoKwitansi As DropDownList)
            ddNoKwitansi = ddNoKwitansi
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not IsPostBack Then
            'ucAmountReceive.AutoPostBack = False
            'ucAmountReceive.RangeValidatorMinimumValue = "1"
            'ucAmountReceive.RangeValidatorEnable = True

            hdnBankAccount.Value = ""
            BindWayOfPayment()
            BindBankAccount()
            BindCollector()
        End If
    End Sub


#Region "Bind Bank Account"
    Private Sub BindBankAccount()
        'oValueDate.isCalendarPostBack = False
        dtBankAccount = CType(Me.Cache.Item(CACHE_BANKACCOUNT & Me.sesBranchId.Replace("'", "")), DataTable)
        If dtBankAccount Is Nothing Then
            Dim dtBankAccountCache As New DataTable
            dtBankAccountCache = oController.GetBankAccount(GetConnectionString, Me.sesBranchId, "", "")
            Me.Cache.Insert(CACHE_BANKACCOUNT & Me.sesBranchId.Replace("'", ""), dtBankAccountCache, Nothing, DateTime.Now.AddHours(DEFAULT_TIME_DURATION_CACHE_HOUR), TimeSpan.Zero)
            dtBankAccount = CType(Me.Cache.Item(CACHE_BANKACCOUNT & Me.sesBranchId.Replace("'", "")), DataTable)
        End If
        Me.ListBankAccount = dtBankAccount
        scriptJava.InnerHtml = GenerateScript(dtBankAccount)
    End Sub
#End Region

#Region "Bind WayOfPayment"
    Private Sub BindWayOfPayment()
        Dim oDataTable As DataTable = New DataTable
        Dim oRow As DataRow
        Dim splitListData() As String
        Dim splitRow() As String
        Dim i As Integer
        Dim strListData As String

        oDataTable.Columns.Add("ID", System.Type.GetType("System.String"))
        oDataTable.Columns.Add("Description", System.Type.GetType("System.String"))

        oRow = oDataTable.NewRow()
        oRow("ID") = ""
        oRow("Description") = ""
        If Me.WithOutPrepaid Then
            strListData = "CA,Cash-BA,Bank"
        Else
            strListData = "CA,Cash-BA,Bank-CP,Prepaid"
        End If

        splitListData = Split(strListData, "-")
        For i = 0 To UBound(splitListData)
            splitRow = Split(splitListData(i), ",")
            oRow("ID") = splitRow(0)
            oRow("Description") = splitRow(1)
            oDataTable.Rows.Add(oRow)
            oRow = oDataTable.NewRow()
        Next

        cmbWayOfPayment.DataValueField = "ID"
        cmbWayOfPayment.DataTextField = "Description"
        cmbWayOfPayment.DataSource = oDataTable
        cmbWayOfPayment.DataBind()
        cmbWayOfPayment.Items.Insert(0, "Select One")
        cmbWayOfPayment.Items(0).Value = "0"

    End Sub
#End Region


#Region "Generate Script"
    Public Function WOPonChange() As String
        hdnBankAccount.Value = ""
        Return "WOPChange('" & Trim(cmbWayOfPayment.ClientID) & "','" &
                Trim(cmbBankAccount.ClientID) & "','" &
                Trim(hdnBankAccount.ClientID) & "','" &
                Trim(ucAmountReceive.ClientId) &
                "',((this.selectedIndex == -1) ? null : ListData[this.selectedIndex]));"
    End Function

    Private Function GenerateScript(ByVal DtTable As DataTable) As String
        Dim strScript As String
        Dim strScript1 As String = ""
        Dim strType As String
        Dim DataRow As DataRow()
        Dim i As Int32
        Dim j As Int32
        Dim k As Int32
        strScript = "<script language=""JavaScript"">" & vbCrLf
        strScript &= " var a; " & vbCrLf
        strScript &= "ListData = new Array(" & vbCrLf
        strType = ""
        If Me.WithOutPrepaid Then
            k = cmbWayOfPayment.Items.Count - 1
        Else
            k = cmbWayOfPayment.Items.Count - 2
        End If

        For j = 0 To k
            DataRow = DtTable.Select(" Type = '" & Left(cmbWayOfPayment.Items(j).Value.Trim, 1) & "'")
            If DataRow.Length > 0 Then
                For i = 0 To DataRow.Length - 1
                    If strType <> CStr(DataRow(i)("Type")).Trim Then
                        strType = CStr(DataRow(i)("Type")).Trim
                        strScript &= "new Array(" & vbCrLf
                        strScript1 = ""
                    End If
                    If strScript1 = "" Then
                        strScript1 = " new Array('" & CStr(DataRow(i)("Name")).Trim & "','" & CStr(DataRow(i)("ID")).Trim & "')"
                        'strScript1 &= vbCrLf & ")" & vbCrLf
                    Else
                        strScript1 &= "," & vbCrLf & " new Array('" & CStr(DataRow(i)("Name")).Trim & "','" & CStr(DataRow(i)("ID")).Trim & "') "
                        'strScript1 &= vbCrLf & ")" & vbCrLf
                    End If
                Next

                strScript &= strScript1 & ")," & vbCrLf
                'strScript1 &= vbCrLf & ")," & vbCrLf
            Else
                strScript &= " null," & vbCrLf
            End If
        Next
        If Right(strScript.Trim, 5) = "null," Then
            strScript = Left(strScript.Trim, strScript.Trim.Length - 1)
        Else
            strScript = Left(strScript.Trim, strScript.Trim.Length - 2)
        End If
        'strScript &= vbCrLf & ")" & vbCrLf
        strScript &= vbCrLf & "));" & vbCrLf
        strScript &= "</script>"
        Return strScript
    End Function

#End Region





    Private Sub BindCollector()

        Dim strconn As String = GetConnectionString()
        Dim dt As New DataTable

        dt = oController.popCollector(GetConnectionString(), Me.GroubDbID, "CLE")

        ddlCollectorID.DataSource = dt
        ddlCollectorID.DataTextField = "CollectorName"
        ddlCollectorID.DataValueField = "CollectorID"
        ddlCollectorID.DataBind()
        ddlCollectorID.Items.Insert(0, "Select One")
        ddlCollectorID.Items(0).Value = "0"
    End Sub

#Region "GetSetValue"
    Private Function GetSetValue() As DataTable
        Dim dtsEntity As New DataTable
        Dim cContract As New GeneralPagingController
        Dim oContract As New Parameter.GeneralPaging

        With oContract
            .strConnection = GetConnectionString()
            .WhereCond = " and branchid=" + Me.sesBranchId + ""
            .CurrentPage = 1
            .PageSize = 1000
            .SortBy = ""
            .SpName = "spGetSetValueJenisFormPembayaran"
        End With

        oContract = cContract.GetGeneralPaging(oContract)
        dtsEntity = oContract.ListData
        Return dtsEntity
    End Function
#End Region
    Public Sub DoFormCashModule()
        Me.JenisFormPembayaran = IIf(Me.JenisFormPembayaran Is Nothing, "", Me.JenisFormPembayaran).ToString.Trim

        If JenisFormPembayaran.ToUpper = "CA" Then
            ddlBayarDi.Visible = False
            ddlBayarDi.SelectedValue = "Cabang"
            ddlCollectorID.Visible = False
            cmbWayOfPayment.SelectedValue = "CA"
            cmbWayOfPayment.Enabled = False
            BindRekeningCashModule("C", "")
            'cmbBankAccount.SelectedValue = resultString
            cmbBankAccount.Enabled = False
            Requiredfieldvalidator3.Enabled = False
            rfvCollector.Enabled = False
            lblNoBonMerah.Visible = False
            txtReferenceNo.Visible = False
            pnlBayarMelalui.Visible = False
            pnlBank.Visible = False
            pnlColl.Visible = False
            pnlRest.Visible = False
            pnlWOP.Visible = False


        ElseIf JenisFormPembayaran.ToUpper = "BA" Then
            ddlBayarDi.Visible = False
            ddlBayarDi.SelectedValue = "0"
            ddlCollectorID.Visible = False
            cmbWayOfPayment.SelectedValue = "BA"
            cmbWayOfPayment.Enabled = False
            rfvCollector.Enabled = False
            BindRekeningCashModule("B", "")
            lblNoBonMerah.Visible = False
            txtReferenceNo.Visible = False
            pnlBayarMelalui.Visible = False
            pnlBank.Visible = True
            pnlColl.Visible = False
            pnlRest.Visible = False
            pnlWOP.Visible = False


        ElseIf JenisFormPembayaran.ToUpper = "CP" Then
            ddlBayarDi.Visible = False
            ddlBayarDi.SelectedValue = "Collection"
            ddlCollectorID.Visible = True
            cmbWayOfPayment.SelectedValue = "CA"
            cmbWayOfPayment.Enabled = False

            BindRekeningCashModule("C", "")
            'cmbBankAccount.SelectedValue = resultString
            cmbBankAccount.Enabled = False
            Requiredfieldvalidator3.Enabled = False
            rfvCollector.Enabled = True
            lblNoBonMerah.Visible = False
            txtReferenceNo.Visible = False
            pnlBayarMelalui.Visible = True

            pnlWOP.Visible = False
            pnlBank.Visible = False
            pnlColl.Visible = True
            pnlRest.Visible = False

        Else
            pnlWOP.Visible = True
            pnlBayarMelalui.Visible = False
            lblNoBonMerah.Visible = True
            txtReferenceNo.Visible = True
            pnlRest.Visible = True
            pnlColl.Visible = False
            ddlCollectorID.Visible = False
        End If
    End Sub

    Public Sub BindRekeningCashModule(ByVal bankType As String, ByVal status As String)
        'dtBankAccount = CType(Me.Cache.Item(CACHE_BANKACCOUNT & Me.sesBranchId.Replace("'", "")), DataTable)
        'If dtBankAccount Is Nothing Then
        '    Dim dtBankAccountCache As New DataTable
        '    dtBankAccountCache = oController.GetBankAccount(GetConnectionString, Me.sesBranchId, bankType, "")
        '    Me.Cache.Insert(CACHE_BANKACCOUNT & Me.sesBranchId.Replace("'", ""), dtBankAccountCache, Nothing, DateTime.Now.AddHours(DEFAULT_TIME_DURATION_CACHE_HOUR), TimeSpan.Zero)
        '    dtBankAccount = CType(Me.Cache.Item(CACHE_BANKACCOUNT & Me.sesBranchId.Replace("'", "")), DataTable)
        'End If
        'cmbBankAccount.DataSource = dtBankAccount

        If status.Trim = "AdvIns" Then
            Dim oConInst As New InstallRcvController
            Dim oInstal As New Parameter.InstallRcv
            oInstal.strConnection = GetConnectionString()
            oInstal.SPName = "spGetBankAccountAdvIns"
            oInstal.WhereCond = " BranchID = '" + sesBranchId.Replace("'", "") + "'  "

            cmbBankAccount.DataSource = oConInst.GetSP(oInstal).ListData
            cmbBankAccount.DataTextField = "Name"
            cmbBankAccount.DataValueField = "ID"
            cmbBankAccount.DataBind()
        ElseIf status.Trim = "ALL" Then
            cmbBankAccount.DataSource = oController.GetBankAccount(GetConnectionString, Me.sesBranchId, "", "")
            cmbBankAccount.DataTextField = "Name"
            cmbBankAccount.DataValueField = "ID"
            cmbBankAccount.DataBind()
        Else
            cmbBankAccount.DataSource = oController.GetBankAccount(GetConnectionString, Me.sesBranchId, bankType, "EC")
            cmbBankAccount.DataTextField = "Name"
            cmbBankAccount.DataValueField = "ID"
            cmbBankAccount.DataBind()
        End If



    End Sub



    Private Sub cmbWayOfPayment_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbWayOfPayment.SelectedIndexChanged

        ' Dim bankType As String = ""
        ' dtBankAccount = CType(Me.Cache.Item(CACHE_BANKACCOUNT & Me.sesBranchId.Replace("'", "")), DataTable)
        ' If dtBankAccount Is Nothing Then
        Dim dtBankAccountCache As New DataTable
        dtBankAccountCache = oController.GetBankAccount(GetConnectionString, Me.sesBranchId, cmbWayOfPayment.SelectedValue, "")
        'Me.Cache.Insert(CACHE_BANKACCOUNT & Me.sesBranchId.Replace("'", ""), dtBankAccountCache, Nothing, DateTime.Now.AddHours(DEFAULT_TIME_DURATION_CACHE_HOUR), TimeSpan.Zero)
        ' dtBankAccount = CType(Me.Cache.Item(CACHE_BANKACCOUNT & Me.sesBranchId.Replace("'", "")), DataTable)
        'End If
        cmbBankAccount.DataSource = dtBankAccountCache
        cmbBankAccount.DataTextField = "Name"
        cmbBankAccount.DataValueField = "ID"
        cmbBankAccount.DataBind()
    End Sub



    Public Sub EnableCollector()
        ddlCollectorID.Enabled = False
        ddNoKwitansi.Enabled = False
    End Sub



    Private Sub ddlCollectorID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCollectorID.SelectedIndexChanged
        BindKwitansiCollection(ddlCollectorID.SelectedValue)

    End Sub

    Private Function Get_Combo_ReceiptByCollector(ByVal customClass As Parameter.InstallRcv) As Parameter.InstallRcv
        Dim oReturnValue As New Parameter.InstallRcv
        Dim params As IList(Of SqlParameter) = New List(Of SqlParameter)
        params.Add(New SqlParameter("@CollectorId", SqlDbType.VarChar, 20) With {.Value = customClass.CollectorID})
        params.Add(New SqlParameter("@ApplicationId", SqlDbType.VarChar, 20) With {.Value = customClass.ApplicationID})


        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(customClass.strConnection, CommandType.StoredProcedure, "spCbReceiptByCollector", params.ToArray()).Tables(0)
            Return oReturnValue
        Catch ex As Exception
            'Throw New Exception("Error On DataAccess.Marketing.AssetType.GetAssetType")
        End Try

    End Function



    Public Function BindKwitansiCollection(ByVal collid As String) As String
        Dim builder As New StringWriter()
        Dim dt As New DataTable

        Dim customClassInstallRcv As New Parameter.InstallRcv
        customClassInstallRcv.CollectorID = collid 'ddlCollectorID.SelectedValue
        customClassInstallRcv.ApplicationID = Me.ApplicationID
        customClassInstallRcv.strConnection = GetConnectionString()

        dt = Get_Combo_ReceiptByCollector(customClassInstallRcv).ListData

        ddNoKwitansi.DataSource = dt
        ddNoKwitansi.DataTextField = "Descrn"
        ddNoKwitansi.DataValueField = "Code"
        ddNoKwitansi.DataBind()
        'ddlCollectorID.Items.Insert(0, "None")
        'ddlCollectorID.Items(0).Value = "0"

        'builder.WriteLine("[")
        'If dt.Rows.Count > 0 Then
        '    'builder.WriteLine("{""optionDisplay"":""Select State"",")
        '    'builder.WriteLine("""optionValue"":""0""},")
        '    For i As Integer = 0 To dt.Rows.Count - 1
        '        builder.WriteLine("{""optionDisplay"":""" & Convert.ToString(dt.Rows(i)("Descrn")) & """,")
        '        builder.WriteLine("""optionValue"":""" & Convert.ToString(dt.Rows(i)("Code")) & """},")
        '    Next
        'Else
        '    builder.WriteLine("{""optionDisplay"":""None"",")
        '    builder.WriteLine("""optionValue"":""""},")
        'End If
        'Dim returnjson As String = builder.ToString().Substring(0, builder.ToString().Length - 3)
        'returnjson = returnjson & "]"
        'Return returnjson.Replace(vbCr, "").Replace(vbLf, "")
    End Function
End Class
