﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
#End Region

Public Class UcFullPrepayInfoOL
    Inherits AccMntControlBased

#Region "Property"
    Public WriteOnly Property IsPenaltyTerminationShow() As Boolean
        Set(ByVal Value As Boolean)
            If Not Value Then
                lblTerminationPenaltyFee.Visible = Value
                lblTotalPrepaymentAmount.Visible = Value
                pnlTerminationFee.Visible = Value
            End If
        End Set
    End Property
    Public ReadOnly Property TerminationPenalty() As Double
        Get
            Return CType(lblTerminationPenaltyFee.Text, Double)
        End Get
    End Property
    Public Property InstallmentDueAmount() As Double
        Get
            Return CType(ViewState("InstallmentDueAmount"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("InstallmentDueAmount") = Value
        End Set
    End Property
    Public Property IsValidPrepayment() As Boolean
        Get
            Return CType(ViewState("isValidPrepayment"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            ViewState("isValidPrepayment") = Value
        End Set
    End Property
    Public Property ContractPrepaidAmount() As Double
        Get
            Return CType(ViewState("ContractPrepaidAmount"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("ContractPrepaidAmount") = Value
        End Set
    End Property

    Public Property IsCrossDefault() As Boolean
        Get
            Return CType(ViewState("IsCrossDefault"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            ViewState("IsCrossDefault") = Value
        End Set
    End Property

    Public Property InsurancePaidBy() As String
        Get
            Return CType(ViewState("InsurancePaidBy"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("InsurancePaidBy") = Value
        End Set
    End Property

    Public Property BranchAgreement() As String
        Get
            Return CType(ViewState("BranchAgreement"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("BranchAgreement") = Value
        End Set
    End Property

    Public Property PrepaymentType() As String
        Get
            Return CType(ViewState("PrepaymentType"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("PrepaymentType") = Value
        End Set
    End Property

    Public Property CustomerType() As String
        Get
            Return CType(ViewState("CustomerType"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerType") = Value
        End Set
    End Property

    Public Property TotalPrepaymentAmount() As Double
        Get
            Return CType(ViewState("TotalPrepaymentAmount"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("TotalPrepaymentAmount") = Value
        End Set
    End Property

    Public Property CustomerName() As String
        Get
            Return CType(ViewState("CustomerName"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerName") = Value
        End Set
    End Property

    Public Property DefaultStatus() As String
        Get
            Return CType(ViewState("DefaultStatus"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("DefaultStatus") = Value
        End Set
    End Property
    Public Property InstallmentAmount() As Double
        Get
            Return CType(ViewState("InstallmentAmount"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("InstallmentAmount") = Value
        End Set
    End Property
    Public Property NextInstallmentNumber() As Integer
        Get
            Return CType(ViewState("NextInstallmentNumber"), Integer)
        End Get
        Set(ByVal Value As Integer)
            ViewState("NextInstallmentNumber") = Value
        End Set
    End Property
    Public Property NAAMount() As Double
        Get
            Return CType(ViewState("NAAMount"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("NAAMount") = Value
        End Set
    End Property
    Public Property NADate() As Date
        Get
            Return CType(ViewState("NADate"), Date)
        End Get
        Set(ByVal Value As Date)
            ViewState("NADate") = Value
        End Set
    End Property
    Public Property NextInstallmentDate() As Date
        Get
            Return CType(ViewState("NextInstallmentDate"), Date)
        End Get
        Set(ByVal Value As Date)
            ViewState("NextInstallmentDate") = Value
        End Set
    End Property
    Public Property FundingPledgeStatus() As String
        Get
            Return CType(ViewState("FundingPledgeStatus"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("FundingPledgeStatus") = Value
        End Set
    End Property
    Public Property ResiduValue() As Double
        Get
            Return CType(ViewState("ResiduValue"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("ResiduValue") = Value
        End Set
    End Property
    Public Property FundingCoyName() As String
        Get
            Return CType(ViewState("FundingCoyName"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("FundingCoyName") = Value
        End Set
    End Property
    Public Property ContractStatus() As String
        Get
            Return CType(ViewState("ContactStatus"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ContactStatus") = Value
        End Set
    End Property
    Public Property BranchID() As String
        Get
            Return CType(ViewState("BranchID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("BranchID") = Value
        End Set
    End Property

    Public Property AccruedInterest() As Double
        Get
            Return CType(ViewState("AccruedInterest"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("AccruedInterest") = Value
        End Set
    End Property



#Region "Property Maximum Value"
    Public Property MaximumInstallment() As Double
        Get
            Return CType(ViewState("MaximumInstallment"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("MaximumInstallment") = Value
        End Set
    End Property

    Public Property MaximumInsurance() As Double
        Get
            Return CType(ViewState("MaximumInsurance"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("MaximumInsurance") = Value
        End Set
    End Property

    Public Property MaximumInstallCollFee() As Double
        Get
            Return CType(ViewState("MaximumInstallCollFee"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("MaximumInstallCollFee") = Value
        End Set
    End Property

    Public Property MaximumInsuranceCollFee() As Double
        Get
            Return CType(ViewState("MaximumInsuranceCollFee"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("MaximumInsuranceCollFee") = Value
        End Set
    End Property

    Public Property MaximumLCInstallFee() As Double
        Get
            Return CType(ViewState("MaximumLCInstallFee"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("MaximumLCInstallFee") = Value
        End Set
    End Property

    Public Property MaximumLCInsuranceFee() As Double
        Get
            Return CType(ViewState("MaximumLCInsuranceFee"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("MaximumLCInsuranceFee") = Value
        End Set
    End Property

    Public Property MaximumInsuranceClaimFee() As Double
        Get
            Return CType(ViewState("MaximumInsuranceClaimFee"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("MaximumInsuranceClaimFee") = Value
        End Set
    End Property

    Public Property MaximumSTNKRenewalFee() As Double
        Get
            Return CType(ViewState("MaximumSTNKRenewalFee"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("MaximumSTNKRenewalFee") = Value
        End Set
    End Property

    Public Property MaximumPDCBounceFee() As Double
        Get
            Return CType(ViewState("MaximumPDCBounceFee"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("MaximumPDCBounceFee") = Value
        End Set
    End Property

    Public Property MaximumReposessionFee() As Double
        Get
            Return CType(ViewState("MaximumReposessionFee"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("MaximumReposessionFee") = Value
        End Set
    End Property

    Public Property MaximumOutStandingPrincipal() As Double
        Get
            Return CType(ViewState("MaximumOutStandingPrincipal"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("MaximumOutStandingPrincipal") = Value
        End Set
    End Property

    Public Property MaximumOutStandingInterest() As Double
        Get
            Return CType(ViewState("MaximumOutStandingInterest"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("MaximumOutStandingInterest") = Value
        End Set
    End Property

    Public Property MaximumPenaltyRate() As Double
        Get
            Return CType(ViewState("MaximumPenaltyRate"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("MaximumPenaltyRate") = Value
        End Set
    End Property
#End Region
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub
    Public Sub PaymentInfo()

        Dim oController As New UCPaymentInfoController
        Dim oCustomClass As New Parameter.FullPrepay
        With oCustomClass
            .strConnection = GetConnectionString()
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationID
            .ValueDate = Me.ValueDate
            .PrepaymentType = Me.PrepaymentType
        End With

        oCustomClass = oController.GetFullPrepaymentInfoOL(oCustomClass)

        With oCustomClass
            Try
                Me.BranchAgreement = .BranchAgreement
                Me.AgreementNo = .Agreementno

                Me.CustomerID = .CustomerID
                Me.CustomerType = .CustomerType
                Me.ContractStatus = .ContractStatus
                Me.DefaultStatus = .DefaultStatus
                Me.CustomerName = .CustomerName
                Me.ContractPrepaidAmount = .Prepaid

                lblLCInstall.Text = FormatNumber(.LcInstallment, 2)
                lblPDCBounceFee.Text = FormatNumber(.PDCBounceFee, 2)
                lblRepossessionFee.Text = FormatNumber(.RepossessionFee, 2)
                lblFaktorPengurang.Text = FormatNumber(.FaktorPengurang, 2)
                lblTerminationPenaltyFee.Text = FormatNumber(.TerminationPenalty, 2)
                lblOSPrincipalAmount.Text = FormatNumber(.OutstandingPrincipal, 2)
                lblTotalPrepaymentAmountGross.Text = FormatNumber(.OutstandingPrincipal + .InstallmentDue + .LcInstallment + .InstallmentCollFee + _
                                        .InsuranceDue + .InsuranceCollFee + .LcInsurance + _
                                        .PDCBounceFee + .STNKRenewalFee + .InsuranceClaimExpense + _
                                        .RepossessionFee + .AccruedInterest + .TerminationPenalty, 2)
                lblTotalPrepaymentAmount.Text = FormatNumber(.OutstandingPrincipal + .InstallmentDue + .LcInstallment + .InstallmentCollFee + _
                                        .InsuranceDue + .InsuranceCollFee + .LcInsurance + _
                                        .PDCBounceFee + .STNKRenewalFee + .InsuranceClaimExpense + _
                                        .RepossessionFee + .AccruedInterest + .TerminationPenalty - .FaktorPengurang, 2)
                Me.TotalPrepaymentAmount = .OutstandingPrincipal + .InstallmentDue + .LcInstallment + .InstallmentCollFee + _
                                        .InsuranceDue + .InsuranceCollFee + .LcInsurance + _
                                        .PDCBounceFee + .STNKRenewalFee + .InsuranceClaimExpense + _
                                        .RepossessionFee + .AccruedInterest + .TerminationPenalty - .FaktorPengurang

                Me.AccruedInterest = .AccruedInterest
                Me.InstallmentDueAmount = .InstallmentDue
                Me.MaximumPenaltyRate = .TerminationPenalty
                Me.IsCrossDefault = .CrossDefault
                If Me.PrepaymentType = "ED" Then
                    Me.MaximumInstallment = .ECI
                ElseIf Me.PrepaymentType = "PD" Then
                    Me.MaximumInstallment = .OutstandingPrincipal
                End If
                Me.InsurancePaidBy = .InsurancePaidBy
                Me.ContractPrepaidAmount = .Prepaid
                Me.MaximumLCInstallFee = .LcInstallment
                Me.MaximumInstallCollFee = .InstallmentCollFee

                Me.MaximumInsurance = .InsuranceDue
                Me.MaximumLCInsuranceFee = .LcInsurance
                Me.MaximumInsuranceCollFee = .InsuranceCollFee

                Me.MaximumPDCBounceFee = .PDCBounceFee
                Me.MaximumSTNKRenewalFee = .STNKRenewalFee
                Me.MaximumInsuranceClaimFee = .InsuranceClaimExpense
                Me.MaximumReposessionFee = .RepossessionFee

                Me.MaximumOutStandingInterest = .OutStandingInterest
                Me.MaximumOutStandingPrincipal = .OutstandingPrincipal
                Me.IsValidPrepayment = True

                Me.InstallmentAmount = .InstallmentAmount
                Me.NextInstallmentDate = .NextInstallmentDate
                Me.NextInstallmentNumber = .NextInstallmentNumber

                Me.FundingCoyName = .FundingCoyName
                Me.FundingPledgeStatus = .FundingPledgeStatus
                Me.ResiduValue = .ResiduValue
                Me.NADate = .NADate
                Me.NAAMount = .NAAmount

            Catch ex As Exception
                ex.Message.ToString()
            End Try


            'If .TerminationPenalty > (.OutStandingInterest - .AccruedInterest) Then
            '    IsValidPrepayment = False
            'Else
            '    IsValidPrepayment = True
            'End If

        End With
    End Sub

End Class