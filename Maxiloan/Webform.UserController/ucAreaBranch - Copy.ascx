﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucAreaBranch.ascx.vb" Inherits="Maxiloan.Webform.UserController.ucAreaBranch" %>
<link href="../Include/Lookup.css" rel="stylesheet" type="text/css" />

<div class="form_box_usercontrol">
    <div class="form_left_usercontrol">
        <label>Area</label>
        <asp:DropDownList ID="cboArea" runat="server" AutoPostBack="true" OnSelectedIndexChanged="cboArea_IndexChanged" />
        <asp:RequiredFieldValidator ID="RFVArea" runat="server" InitialValue="0"
                    Display="Dynamic" ControlToValidate="cboArea" ErrorMessage="Harap isi area"
                    CssClass="validator_general"></asp:RequiredFieldValidator>
    </div>
</div>
<div class="form_box_usercontrol">
	<div class="form_left_usercontrol">
        <label>Cabang</label>
        <asp:DropDownList ID="cboBranch" runat="server" AutoPostBack="true"/>
        <asp:RequiredFieldValidator ID="RFVBranch" runat="server" InitialValue="0"
                    Display="Dynamic" ControlToValidate="cboBranch" ErrorMessage="Harap isi cabang"
                    CssClass="validator_general"></asp:RequiredFieldValidator>
	</div>
</div>
