﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucAction.ascx.vb" Inherits="Maxiloan.Webform.UserController.ucAction" %>
<%@ Register TagPrefix="uc1" TagName="ValidDate" Src="ValidDate.ascx" %>
<script language="JavaScript" type="text/javascript">
<!--
    var hdnDetail;
    function WOPChange(pCmbOfPayment, pBankAccount, pHdnDetail, itemArray) {
        hdnDetail = eval('document.forms[0].' + pHdnDetail);
        //alert(eval('document.forms[0].' + pCmbOfPayment).options[eval('document.forms[0].' + pCmbOfPayment).selectedIndex].value);
        eval('document.forms[0].' + pBankAccount).disabled = false;


        var i, j;
        for (i = eval('document.forms[0].' + pBankAccount).options.length; i >= 0; i--) {
            eval('document.forms[0].' + pBankAccount).options[i] = null

        };
        if (itemArray == null) {
            j = 0;
        }
        else {
            j = 1;
        };
        eval('document.forms[0].' + pBankAccount).options[0] = new Option('Select One');
        eval('document.forms[0].' + pBankAccount).options[0].value = '0';
        if (itemArray != null) {
            for (i = 0; i < itemArray.length; i++) {
                eval('document.forms[0].' + pBankAccount).options[j] = new Option(itemArray[i][0]);
                if (itemArray[i][1] != null) {
                    eval('document.forms[0].' + pBankAccount).options[j].value = itemArray[i][1];
                };
                j++;
            };
            eval('document.forms[0].' + pBankAccount).selected = true;
        };


        //if( eval('document.forms[0].' + pCmbOfPayment).selectedIndex == 4){
        //eval('document.forms[0].' + pBankAccount).disabled = true;
        //}
        //if( eval('document.forms[0].' + pCmbOfPayment).selectedIndex  == 5){
        //eval('document.forms[0].' + pBankAccount).disabled = true;
        //}
        //if( eval('document.forms[0].' + pCmbOfPayment).selectedIndex  == 2){
        //eval('document.forms[0].' + pBankAccount).disabled = true;
        //}
        //if( eval('document.forms[0].' + pCmbOfPayment).selectedIndex  == 7){
        //eval('document.forms[0].' + pBankAccount).disabled = true;
        //}
    }



    function cboChildonChange(l) {
//                hdnDetail.value = l;
        var _ActionHasil = $("#oAction_cboChild").val();
        if (_ActionHasil.match(/^PTPY.*$/)) {
            $('#txtPromiseDate').removeAttr('disabled');
            $('#cboPlanActivity').attr('disabled', 'disabled');   
            $('#txtPlanDate').attr('disabled', 'disabled');
            $('#txtHour').attr('disabled', 'disabled');
            $('#txtMinute').attr('disabled', 'disabled');
            document.getElementById("cboPlanActivity").value = ""
            document.getElementById("txtPlanDate").value = ""
            document.getElementById("txtHour").value = ""
            document.getElementById("txtMinute").value = ""
        }
        else {
            $('#txtPromiseDate').attr('disabled', 'disabled');            
            $('#cboPlanActivity').removeAttr('disabled');
            $('#txtPlanDate').removeAttr('disabled');
            $('#txtHour').removeAttr('disabled');
            $('#txtMinute').removeAttr('disabled');
            document.getElementById("txtPromiseDate").value = ""
        }
        hdnDetail.value = l;
    }




    function SetcboChildFocus() {
        var SPV;
        SPV = hdnDetail.value;
        alert(SPV);

        for (i = document.forms[0].cboChild.options.length; i >= 0; i--) {
            if (document.forms[0].cboChild.options.options[i].value = SPV) {
                document.forms[0].cboChild.options[i].selected = true;
            }
        };
    }
-->
</script>
<input id="hdnSP" type="hidden" name="hdnSP" runat="server" />
<input id="hdnResult" type="hidden" name="hdnResult" runat="server" />
<asp:UpdatePanel runat="server" ID="up1">
    <ContentTemplate>
        <div class="form_box_usercontrol">
            <div class="form_left_usercontrol">
                <label>
                    Aktivitas</label>
                <%--<asp:DropDownList ID="cboParent" runat="server" onchange="<%# WOPonChange() %>">
        </asp:DropDownList>--%>
                <asp:DropDownList ID="cboParent" runat="server" AutoPostBack="true">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" InitialValue="0"
                    Display="Dynamic" ControlToValidate="cboParent" ErrorMessage="Harap pilih Aktivitas"
                    CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box_usercontrol">
            <div class="form_left_usercontrol">
                <label>
                    Hasil</label>
                <%--<asp:DropDownList ID="cboChild" runat="server" onchange="cboChildonChange(this.options[this.selectedIndex].value);">
                </asp:DropDownList>--%>
                <asp:DropDownList ID="cboChild" runat="server" onchange="cboChildonChange(this.options[this.selectedIndex].value);">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Harap pilih Hasil"
                    ControlToValidate="cboChild" Display="Dynamic" InitialValue="0" CssClass="validator_general"></asp:RequiredFieldValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage=" Harap pilih Hasil"
                    ControlToValidate="cboChild" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
<div id="Header" runat="server">
</div>
