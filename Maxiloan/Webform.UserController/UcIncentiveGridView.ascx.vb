﻿Imports Maxiloan.Controller

Public Class UcIncentiveGridView
    Inherits ControlBased

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Public Event EventEdit(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
    Private Msg As New Maxiloan.Webform.WebBased
    Private m_Insentif As New RefundInsentifController

    Public WriteOnly Property OnClientClick As String
        Set(ByVal value As String)
            btnAdd.Attributes.Add("OnClick", value)
        End Set
    End Property
    Public WriteOnly Property OnSaveClick As String
        Set(ByVal value As String)
            btnSave.Attributes.Add("OnClick", value)
        End Set
    End Property
    Public Property ApplicationID() As String
        Get
            Return (CType(ViewState("ApplicationID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
    Public Property SupplierID() As String
        Set(ByVal SupplierID As String)
            ViewState("SupplierID") = SupplierID
        End Set
        Get
            Return ViewState("SupplierID").ToString
        End Get
    End Property
    Public Property WhereCond As String
        Set(ByVal WhereCond As String)
            ViewState("WhereCond") = WhereCond
        End Set
        Get
            Return ViewState("WhereCond").ToString
        End Get
    End Property
    Public Property dtg As DataGrid
        Get
            Return dtgIncentive
        End Get
        Set(ByVal dtg As DataGrid)
            dtgIncentive = dtg
        End Set
    End Property

    Public Property AlokasiPremiAsuransi As String
        Set(ByVal AlokasiPremiAsuransi As String)
            ViewState("AlokasiPremiAsuransi") = AlokasiPremiAsuransi
        End Set
        Get
            Return ViewState("AlokasiPremiAsuransi").ToString
        End Get
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub
 
    Public Sub AddRecord()
        Dim objectDataTable As New DataTable
        Dim intLoopGrid As Integer
        Dim oRow As DataRow
        Dim lblNomor As New Label
        Dim lblInsentifGross As New Label
        Dim lblPPH As New Label
        Dim lblTarifPajak As New Label
        Dim lblNilaiPajak As New Label
        Dim lblInsentifNet As New Label
        Dim lblddlJabatan As New Label
        Dim lblddlPenerima As New Label
        Dim lblddlTransID As New Label
        Dim lblProsentase As New Label
        Dim lblInsentif As New Label
        Dim plus As Integer = 0
        Dim imgDelete As New ImageButton
        Dim lblddlPPh As New Label


        With objectDataTable
            .Columns.Add(New DataColumn("Nomor", GetType(String)))
            .Columns.Add(New DataColumn("Jabatan", GetType(String)))
            .Columns.Add(New DataColumn("Penerima", GetType(String)))
            .Columns.Add(New DataColumn("Prosentase", GetType(String)))
            .Columns.Add(New DataColumn("Insentif", GetType(String)))
            .Columns.Add(New DataColumn("InsentifGross", GetType(String)))
            .Columns.Add(New DataColumn("PPH", GetType(String)))
            .Columns.Add(New DataColumn("TarifPajak", GetType(String)))
            .Columns.Add(New DataColumn("NilaiPajak", GetType(String)))
            .Columns.Add(New DataColumn("InsentifNet", GetType(String)))
            .Columns.Add(New DataColumn("TransID", GetType(String)))
        End With

        For intLoopGrid = 0 To dtgIncentive.Items.Count - 1
            lblNomor = CType(dtgIncentive.Items(intLoopGrid).FindControl("lblNomor"), Label)
            lblddlJabatan = CType(dtgIncentive.Items(intLoopGrid).FindControl("lblddlJabatan"), Label)
            lblddlPenerima = CType(dtgIncentive.Items(intLoopGrid).FindControl("lblddlPenerima"), Label)
            lblProsentase = CType(dtgIncentive.Items(intLoopGrid).FindControl("lblProsentase"), Label)
            lblInsentif = CType(dtgIncentive.Items(intLoopGrid).FindControl("lblInsentif"), Label)
            lblInsentifGross = CType(dtgIncentive.Items(intLoopGrid).FindControl("lblInsentifGross"), Label)
            'lblPPH = CType(dtgIncentive.Items(intLoopGrid).FindControl("lblPPH"), Label)
            lblddlPPh = CType(dtgIncentive.Items(intLoopGrid).FindControl("lblddlPPh"), Label)
            lblTarifPajak = CType(dtgIncentive.Items(intLoopGrid).FindControl("lblTarifPajak"), Label)
            lblNilaiPajak = CType(dtgIncentive.Items(intLoopGrid).FindControl("lblNilaiPajak"), Label)
            lblInsentifNet = CType(dtgIncentive.Items(intLoopGrid).FindControl("lblInsentifNet"), Label)
            lblddlTransID = CType(dtgIncentive.Items(intLoopGrid).FindControl("lblddlTransID"), Label)

            '----- Add row -------'
            oRow = objectDataTable.NewRow()
            oRow("Nomor") = CType(lblNomor.Text, String)
            oRow("Jabatan") = CType(lblddlJabatan.Text, String)
            oRow("Penerima") = CType(lblddlPenerima.Text, String)
            oRow("Prosentase") = CType(lblProsentase.Text, String)
            oRow("Insentif") = CType(lblInsentif.Text, String)
            oRow("InsentifGross") = CType(lblInsentifGross.Text, String)
            'oRow("PPH") = CType(lblPPH.Text, String)
            oRow("PPH") = CType(lblddlPPh.Text, String)
            oRow("TarifPajak") = CType(lblTarifPajak.Text, String)
            oRow("NilaiPajak") = CType(lblNilaiPajak.Text, String)
            oRow("InsentifNet") = CType(lblInsentifNet.Text, String)
            oRow("TransID") = CType(lblddlTransID.Text, String)
            objectDataTable.Rows.Add(oRow)


        Next

        oRow = objectDataTable.NewRow()
        oRow("Nomor") = dtgIncentive.Items.Count + 1
        oRow("Jabatan") = ""
        oRow("Penerima") = ""
        oRow("Prosentase") = ""
        oRow("Insentif") = ""
        oRow("InsentifGross") = ""
        oRow("PPH") = ""
        oRow("TarifPajak") = ""
        oRow("NilaiPajak") = ""
        oRow("InsentifNet") = ""
        oRow("TransID") = ""
        objectDataTable.Rows.Add(oRow)

        dtgIncentive.DataSource = objectDataTable
        dtgIncentive.DataBind()

        'BindDDl()

        For intLoopGrid = 0 To dtgIncentive.Items.Count - 1
            imgDelete = CType(dtgIncentive.Items(intLoopGrid).FindControl("imgDelete"), ImageButton)
            lblNomor = CType(dtgIncentive.Items(intLoopGrid).FindControl("lblNomor"), Label)
            lblNomor = CType(dtgIncentive.Items(intLoopGrid).FindControl("lblNomor"), Label)
            lblddlJabatan = CType(dtgIncentive.Items(intLoopGrid).FindControl("lblddlJabatan"), Label)
            lblddlPenerima = CType(dtgIncentive.Items(intLoopGrid).FindControl("lblddlPenerima"), Label)
            lblProsentase = CType(dtgIncentive.Items(intLoopGrid).FindControl("lblProsentase"), Label)
            lblInsentif = CType(dtgIncentive.Items(intLoopGrid).FindControl("lblInsentif"), Label)
            lblInsentifGross = CType(dtgIncentive.Items(intLoopGrid).FindControl("lblInsentifGross"), Label)
            'lblPPH = CType(dtgIncentive.Items(intLoopGrid).FindControl("lblPPH"), Label)
            lblddlPPh = CType(dtgIncentive.Items(intLoopGrid).FindControl("lblddlPPh"), Label)
            lblTarifPajak = CType(dtgIncentive.Items(intLoopGrid).FindControl("lblTarifPajak"), Label)
            lblNilaiPajak = CType(dtgIncentive.Items(intLoopGrid).FindControl("lblNilaiPajak"), Label)
            lblInsentifNet = CType(dtgIncentive.Items(intLoopGrid).FindControl("lblInsentifNet"), Label)
            lblddlTransID = CType(dtgIncentive.Items(intLoopGrid).FindControl("lblddlTransID"), Label)

            lblNomor.Text = objectDataTable.Rows(intLoopGrid).Item(0).ToString.Trim

            If objectDataTable.Rows(intLoopGrid).Item(1).ToString <> "" Then
                lblddlJabatan.Text = objectDataTable.Rows(intLoopGrid).Item(1).ToString.Trim
            End If

            BindlblddlPenerima(lblddlJabatan, intLoopGrid)
            BindLblddlPPH(lblddlPPh, intLoopGrid)

            'onchange="handlelblddlJabatan_Change(this,'lblddlPenerima.ClientID');"

            If objectDataTable.Rows(intLoopGrid).Item(2).ToString <> "" Then
                lblddlPenerima.Text = objectDataTable.Rows(intLoopGrid).Item(2).ToString.Trim
            End If

            lblProsentase.Text = objectDataTable.Rows(intLoopGrid).Item(3).ToString.Trim
            lblInsentif.Text = objectDataTable.Rows(intLoopGrid).Item(4).ToString.Trim
            lblInsentifGross.Text = objectDataTable.Rows(intLoopGrid).Item(5).ToString.Trim
            'lblPPH.Text = objectDataTable.Rows(intLoopGrid).Item(6).ToString.Trim
            lblddlPPh.Text = objectDataTable.Rows(intLoopGrid).Item(6).ToString.Trim
            lblTarifPajak.Text = objectDataTable.Rows(intLoopGrid).Item(7).ToString.Trim
            lblNilaiPajak.Text = objectDataTable.Rows(intLoopGrid).Item(8).ToString.Trim
            lblInsentifNet.Text = objectDataTable.Rows(intLoopGrid).Item(9).ToString.Trim

            imgDelete.Attributes.Add("OnClick", "return deleteRow('" & dtgIncentive.ClientID & "',this);")

            lblProsentase.Attributes.Add("OnChange", "handlelblProsentase_Change(this.value,'" & Me.AlokasiPremiAsuransi & "','" & lblInsentif.ClientID & "')")
            lblInsentif.Attributes.Add("OnChange", "handlelblInsentif_Change(this.value,'" & Me.AlokasiPremiAsuransi & "','" & lblProsentase.ClientID & "')")
            lblddlJabatan.Attributes.Add("OnChange", "handlelblddlJabatan_Change(this,'" & lblddlPenerima.ClientID & "');getPPH(this.value,'" & lblPPH.ClientID & "','" & lblTarifPajak.ClientID & "')")

            lblddlPenerima.Attributes.Add("OnChange", "handlelblddlPenerima_Change(this.value,'" & lblddlJabatan.ClientID & "','" & lblTarifPajak.ClientID & "')")

            
        Next
    End Sub
    
    Private Sub BindDDl()
        Dim dtEntity As New DataTable
        Dim oCustom As New Parameter.RefundInsentif
        Dim lblddlJabatan As New Label
        Dim lblddlPenerima As New Label

        With oCustom
            .strConnection = GetConnectionString()
            .WhereCond = " "
            .SPName = "spGetTblEmployeePosition"
        End With

        oCustom = m_Insentif.GetSPBy(oCustom)
        dtEntity = oCustom.ListDataTable

        For intLoop = 0 To dtgIncentive.Items.Count - 1
            lblddlJabatan = CType(dtgIncentive.Items(intLoop).FindControl("lblddlJabatan"), Label)
            lblddlPenerima = CType(dtgIncentive.Items(intLoop).FindControl("lblddlPenerima"), Label)

            If dtEntity.Rows.Count > 0 Then lblddlJabatan.Text = dtEntity.Rows(0).Item("Name").ToString
            If dtEntity.Rows.Count <= 0 Then lblddlJabatan.Text = ""
        Next
    End Sub
    'Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
    '    AddRecord()
    'End Sub
    'Sub lblddlJabatan_IndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    lblMessage.Visible = False
    '    Dim lblddlJabatan As New label
    '    lblddlJabatan = CType(sender, label)
    '    Dim index As Integer = DirectCast(lblddlJabatan.DataItemContainer, System.Web.UI.WebControls.DataGridItem).DataSetIndex

    '    For indexGrid = 0 To dtgIncentive.Items.Count - 1
    '        Dim A_lblddlJabatan As New label
    '        A_lblddlJabatan = CType(dtgIncentive.Items(indexGrid).FindControl("lblddlJabatan"), label)

    '        If index <> indexGrid And A_lblddlJabatan.SelectedValue.Trim <> "" And lblddlJabatan.SelectedValue.Trim = A_lblddlJabatan.SelectedValue.Trim Then
    '            lblddlJabatan.SelectedIndex = 0
    '            Msg.ShowMessage(lblMessage, "Data Sudah ada!", True)
    '            Exit Sub
    '        Else
    '            lblMessage.Visible = False
    '        End If

    '    Next

    '    BindlblddlPenerima(lblddlPenerima, index)
    '    ScriptManager.RegisterStartupScript(lblddlJabatan, GetType(Page), lblddlJabatan.ClientID, String.Format("(document.getElementById('{0}')).focus();", lblddlJabatan.ClientID), True)
    'End Sub
    Sub BindlblddlPPH(ByVal blbddlPPH As Label, ByVal index As Integer)
        lblMessage.Visible = False
        Dim oCustom As New Parameter.RefundInsentif
        Dim entity As New DataTable
        Dim lblddlPPH As New Label

        lblddlPPH = CType(dtgIncentive.Items(index).FindControl("lblddlPPH"), Label)
        If lblddlPPH.Text = "3.0" Then
            lblddlPPH.Text = "PPh 21 Non Npwp"
        ElseIf lblddlPPH.Text = "2.5" Then
            lblddlPPH.Text = "PPh 21 Npwp"
        ElseIf lblddlPPH.Text = "4.0" Then
            lblddlPPH.Text = "PPh 23 Non Npwp"
        ElseIf lblddlPPH.Text = "2.0" Then
            lblddlPPH.Text = "PPh 23 Npwp"
        Else
            lblddlPPH.Text = "0"
        End If

    End Sub

    Sub BindlblIsDiscountPPH(ByVal lblIsDiscountPPH As Label, ByVal index As Integer)
        lblMessage.Visible = False
        Dim oCustom As New Parameter.RefundInsentif
        Dim entity As New DataTable
        'Dim lblIsDiscountPPH As New Label

        lblIsDiscountPPH = CType(dtgIncentive.Items(index).FindControl("lblIsDiscountPPH"), Label)
        If lblIsDiscountPPH.Text = "NON" Then
            lblIsDiscountPPH.Text = "Tidak Potong Pajak"
        Else
            lblIsDiscountPPH.Text = "Potong Pajak"
        End If

    End Sub

    Sub BindlblddlPenerima(ByVal lblddlJabatan As Label, ByVal index As Integer)
        lblMessage.Visible = False
        Dim oCustom As New Parameter.RefundInsentif
        Dim entity As New DataTable
        Dim lblddlPenerima As New Label

        lblddlPenerima = CType(dtgIncentive.Items(index).FindControl("lblddlPenerima"), Label)
        With oCustom
            .strConnection = GetConnectionString()
            .WhereCond = "  SupplierEmployeePosition='" & lblddlJabatan.Text.Trim & "' and SupplierID='" & Me.SupplierID & "' "
            .SPName = "spGetSupplierEmployee"
        End With

        oCustom = m_Insentif.GetSPBy(oCustom)
        entity = oCustom.ListDataTable

        If entity.Rows.Count > 0 Then
            lblddlPenerima.Text = entity.Rows(0).Item("Name").ToString
        Else
            lblddlPenerima.Text = ""
        End If

    End Sub
    Sub BindlblddlTransID(ByVal index As Integer, ByVal tipe As String)
        Dim oCustom As New Parameter.RefundInsentif
        Dim entity As New DataTable
        Dim lblddlTransID As New Label
        lblddlTransID = CType(dtgIncentive.Items(index).FindControl("lblddlTransID"), Label)
        With oCustom
            .strConnection = GetConnectionString()
            .WhereCond = ""
            If tipe <> "" Then
                .WhereCond = " TransID in (select TransID from tblAgreementMasterTransaction WHERE GroupTransID = '" & tipe & "') "
            End If
            .SPName = "spGetTblAgreementMasterTransaction"
        End With

        oCustom = m_Insentif.GetSPBy(oCustom)
        entity = oCustom.ListDataTable
        If entity.Rows.Count > 0 Then
            lblddlTransID.Text = entity.Rows(0).Item("TransID").ToString.Trim
        else 
            lblddlTransID.Text = ""
        End If
    End Sub
   
    Public Sub BindInsentif(ByVal lblInsentif As Label, ByVal index As Integer)
        Dim A_PremiAsuransi As Decimal = CDec(Me.AlokasiPremiAsuransi)
        Dim Input As Decimal = CDec(IIf(lblInsentif.Text.Trim = "", "0", lblInsentif.Text))
        Dim lblProsentase As New Label
        Dim totalProsentase As Decimal = 0
        Dim totalInsentif As Decimal = 0
        Dim lblInsentifGross As New Label
        Dim lblTarifPajak As New Label
        Dim lblNilaiPajak As New Label
        Dim lblInsentifNet As New Label
        Dim lblddlPenerima As New Label
        Dim lblddlJabatan As New Label
        Dim oCustom As New Parameter.RefundInsentif

        lblddlJabatan = CType(dtgIncentive.Items(index).FindControl("lblddlJabatan"), Label)
        lblddlPenerima = CType(dtgIncentive.Items(index).FindControl("lblddlPenerima"), Label)

        With oCustom
            .strConnection = GetConnectionString()
            .WhereCond = "  SupplierEmployeePosition='" & lblddlJabatan.Text.Trim & "' and rtrim(ltrim(rtrim(ltrim(SupplierID)) + '-' + rtrim(ltrim(SupplierEmployeeID)))) = '" & lblddlPenerima.Text.Trim & "' "
            .SPName = "spGetSupplierEmployee"
        End With

        oCustom = m_Insentif.GetSPBy(oCustom)

        If oCustom.ListDataTable.Rows.Count > 0 Then
            CType(dtgIncentive.Items(index).FindControl("lblSupplierEmployeeID"), Label).Text = oCustom.ListDataTable.Rows(0).Item("SupplierEmployeeID").ToString.Trim
            CType(dtgIncentive.Items(index).FindControl("lblSupplierEmployeePosition"), Label).Text = oCustom.ListDataTable.Rows(0).Item("SupplierEmployeePosition").ToString.Trim
            CType(dtgIncentive.Items(index).FindControl("lblEmployeeName"), Label).Text = oCustom.ListDataTable.Rows(0).Item("Name").ToString.Trim
        End If


        lblProsentase = CType(dtgIncentive.Items(index).FindControl("lblProsentase"), Label)

        lblProsentase.Text = FormatNumber(CType((Input / A_PremiAsuransi) * 100, String), 2)
        lblInsentifGross = CType(dtgIncentive.Items(index).FindControl("lblInsentifGross"), Label)
        lblTarifPajak = CType(dtgIncentive.Items(index).FindControl("lblTarifPajak"), Label)
        lblNilaiPajak = CType(dtgIncentive.Items(index).FindControl("lblNilaiPajak"), Label)
        lblInsentifNet = CType(dtgIncentive.Items(index).FindControl("lblInsentifNet"), Label)

        If lblddlPenerima.Text.Trim = "" Then
            CType(dtgIncentive.Items(index).FindControl("lblProsentase"), Label).Text = ""
            CType(dtgIncentive.Items(index).FindControl("lblInsentif"), Label).Text = ""
            CType(dtgIncentive.Items(index).FindControl("lblInsentifGross"), Label).Text = ""
            CType(dtgIncentive.Items(index).FindControl("lblNilaiPajak"), Label).Text = ""
            CType(dtgIncentive.Items(index).FindControl("lblInsentifNet"), Label).Text = ""
            Msg.ShowMessage(lblMessage, "Penerima Harus dipilih!", True)
            Exit Sub
        Else
            lblMessage.Visible = False
        End If

        lblInsentifGross.Text = lblInsentif.Text
        lblNilaiPajak.Text = FormatNumber(CType((CDec(IIf(lblInsentifGross.Text.Trim = "", "0", lblInsentifGross.Text)) * (CDec(IIf(lblTarifPajak.Text.Trim = "", "0", lblTarifPajak.Text)) / 100)), String), 0)
        lblInsentifNet.Text = FormatNumber(CType((CDec(IIf(lblInsentifGross.Text.Trim = "", "0", lblInsentifGross.Text)) - CDec(IIf(lblNilaiPajak.Text.Trim = "", "0", lblNilaiPajak.Text))), String), 0)
    End Sub
    Public Sub BindTotal()
        Dim A_PremiAsuransi As Decimal = CDec(Me.AlokasiPremiAsuransi)
        Dim totalInsentif As Decimal = 0
        Dim totalProsentase As Decimal = 0

        For i = 0 To dtgIncentive.Items.Count - 1
            Dim Grid_lblInsentif As New Label
            Dim Grid_lblProsentase As New Label

            Grid_lblInsentif = CType(dtgIncentive.Items(i).FindControl("lblInsentif"), Label)
            Grid_lblProsentase = CType(dtgIncentive.Items(i).FindControl("lblProsentase"), Label)
            totalInsentif = totalInsentif + CDec(IIf(Grid_lblInsentif.Text.Trim = "", "0", Grid_lblInsentif.Text))
            totalProsentase = totalProsentase + CDec(IIf(Grid_lblProsentase.Text.Trim = "", "0", Grid_lblProsentase.Text))
        Next

        If totalInsentif > 0 And A_PremiAsuransi > 0 Then
            ScriptManager.RegisterStartupScript(dtgIncentive, GetType(DataGrid), dtgIncentive.ClientID, String.Format(" document.getElementById('{0}' + '_lblTotalProsentase').innerHTML = '" + FormatNumber(totalProsentase, 2).ToString + "';  document.getElementById('{0}' + '_lblTotalInsentif').textContent = '" + FormatNumber(totalInsentif, 0).ToString + "'; ", dtgIncentive.ClientID), True)
        End If



        'If totalInsentif > A_PremiAsuransi Then
        '    Msg.ShowMessage(lblMessage, " Total Insentif : " & FormatNumber(totalInsentif, 0).ToString & " tidak boleh lebih dari : " & FormatNumber(A_PremiAsuransi, 0).ToString & "", True)
        'Else
        '    lblMessage.Visible = False
        'End If
    End Sub

End Class