﻿Imports System.Data.SqlClient

Public Class ucApplicationViewTab
    Inherits Maxiloan.Webform.ControlBased

    Public Property ApplicationID As String
    Private Property CustomerID As String
    Private Property SupplierGroupID As String

    Public Sub setLink()
        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(GetConnectionString)
        Dim objReader As SqlDataReader

        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()

            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.CommandText = "spGetApplicationTab"
            objCommand.Parameters.Add("@ApplicationID", SqlDbType.VarChar, 20).Value = Me.ApplicationID.Trim
            objCommand.Connection = objConnection
            objReader = objCommand.ExecuteReader()

            If objReader.Read Then
                Me.CustomerID = objReader.Item("CustomerID").ToString.Trim
                Me.SupplierGroupID = "" 'objReader.Item("SupplierGroupID").ToString

                If Not IsDBNull(objReader.Item("DateEntryIncentiveData")) Then
                    hypAplikasi.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewApplication/ViewApplication_003.aspx?CustID=" & Me.CustomerID & "&ApplicationID=" & Me.ApplicationID & "&page=Edit"
                    hypAsset.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewApplication/ViewAssetData_002.aspx?CustID=" & Me.CustomerID & "&ApplicationID=" & Me.ApplicationID & "&page=Edit"
                    hypAsuransi.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/New/ViewNewAppInsuranceByCompany.aspx?CustID=" & Me.CustomerID & "&ApplicationID=" & Me.ApplicationID & "&page=Edit"
                    hypFinancial.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewApplication/ViewFinancialData_003.aspx?CustID=" & Me.CustomerID & "&ApplicationID=" & Me.ApplicationID & "&page=Edit"
                    hypFinancial2.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewApplication/ViewFinancialData_004.aspx?CustID=" & Me.CustomerID & "&ApplicationID=" & Me.ApplicationID & "&page=Edit"
                    hypRefund.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewApplication/ViewIncentiveData.aspx?CustID=" & Me.CustomerID & "&ApplicationID=" & Me.ApplicationID & "&page=Edit"
                    hypSurvey.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewApplication/ViewHasilSurvey.aspx?CustID=" & Me.CustomerID & "&ApplicationID=" & Me.ApplicationID & "&page=Edit"
                    hypPlafon.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewApplication/ViewPlafon.aspx?CustID=" & Me.CustomerID & "&ApplicationID=" & Me.ApplicationID & "&page=Edit"
                ElseIf Not IsDBNull(objReader.Item("DateEntryFinancialData2")) Then
                    hypAplikasi.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewApplication/ViewApplication_003.aspx?CustID=" & Me.CustomerID & "&ApplicationID=" & Me.ApplicationID & "&page=Edit"
                    hypAsset.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewApplication/ViewAssetData_002.aspx?CustID=" & Me.CustomerID & "&ApplicationID=" & Me.ApplicationID & "&page=Edit"
                    hypAsuransi.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/New/ViewNewAppInsuranceByCompany.aspx?CustID=" & Me.CustomerID & "&ApplicationID=" & Me.ApplicationID & "&page=Edit"
                    hypFinancial.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewApplication/ViewFinancialData_003.aspx?CustID=" & Me.CustomerID & "&ApplicationID=" & Me.ApplicationID & "&page=Edit"
                    hypFinancial2.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewApplication/ViewFinancialData_004.aspx?CustID=" & Me.CustomerID & "&ApplicationID=" & Me.ApplicationID & "&page=Edit"
                    hypRefund.NavigateUrl = ""
                    hypSurvey.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewApplication/ViewHasilSurvey.aspx?CustID=" & Me.CustomerID & "&ApplicationID=" & Me.ApplicationID & "&page=Edit"
                    hypPlafon.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewApplication/ViewPlafon.aspx?CustID=" & Me.CustomerID & "&ApplicationID=" & Me.ApplicationID & "&page=Edit"
                ElseIf Not IsDBNull(objReader.Item("DateEntryFinancialData")) Then
                    hypAplikasi.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewApplication/ViewApplication_003.aspx?CustID=" & Me.CustomerID & "&ApplicationID=" & Me.ApplicationID & "&page=Edit"
                    hypAsset.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewApplication/ViewAssetData_002.aspx?CustID=" & Me.CustomerID & "&ApplicationID=" & Me.ApplicationID & "&page=Edit"
                    hypAsuransi.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/New/ViewNewAppInsuranceByCompany.aspx?CustID=" & Me.CustomerID & "&ApplicationID=" & Me.ApplicationID & "&page=Edit"
                    hypFinancial.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewApplication/ViewFinancialData_003.aspx?CustID=" & Me.CustomerID & "&ApplicationID=" & Me.ApplicationID & "&page=Edit"
                    hypFinancial2.NavigateUrl = ""
                    hypRefund.NavigateUrl = ""
                    hypSurvey.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewApplication/ViewHasilSurvey.aspx?CustID=" & Me.CustomerID & "&ApplicationID=" & Me.ApplicationID & "&page=Edit"
                    hypPlafon.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewApplication/ViewPlafon.aspx?CustID=" & Me.CustomerID & "&ApplicationID=" & Me.ApplicationID & "&page=Edit"
                ElseIf Not IsDBNull(objReader.Item("DateEntryInsuranceData")) Then
                    hypAplikasi.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewApplication/ViewApplication_003.aspx?CustID=" & Me.CustomerID & "&ApplicationID=" & Me.ApplicationID & "&page=Edit"
                    hypAsset.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewApplication/ViewAssetData_002.aspx?CustID=" & Me.CustomerID & "&ApplicationID=" & Me.ApplicationID & "&page=Edit"
                    hypAsuransi.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/New/ViewNewAppInsuranceByCompany.aspx?CustID=" & Me.CustomerID & "&ApplicationID=" & Me.ApplicationID & "&page=Edit"
                    hypFinancial.NavigateUrl = ""
                    hypFinancial2.NavigateUrl = ""
                    hypRefund.NavigateUrl = ""
                    hypSurvey.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewApplication/ViewHasilSurvey.aspx?CustID=" & Me.CustomerID & "&ApplicationID=" & Me.ApplicationID & "&page=Edit"
                    hypPlafon.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewApplication/ViewPlafon.aspx?CustID=" & Me.CustomerID & "&ApplicationID=" & Me.ApplicationID & "&page=Edit"
                ElseIf Not IsDBNull(objReader.Item("DateEntryAssetData")) Then
                    hypAplikasi.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewApplication/ViewApplication_003.aspx?CustID=" & Me.CustomerID & "&ApplicationID=" & Me.ApplicationID & "&page=Edit"
                    hypAsset.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewApplication/ViewAssetData_002.aspx?CustID=" & Me.CustomerID & "&ApplicationID=" & Me.ApplicationID & "&page=Edit"
                    hypAsuransi.NavigateUrl = ""
                    hypFinancial.NavigateUrl = ""
                    hypFinancial2.NavigateUrl = ""
                    hypRefund.NavigateUrl = ""
                    hypSurvey.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewApplication/ViewHasilSurvey.aspx?CustID=" & Me.CustomerID & "&ApplicationID=" & Me.ApplicationID & "&page=Edit"
                    hypPlafon.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewApplication/ViewPlafon.aspx?CustID=" & Me.CustomerID & "&ApplicationID=" & Me.ApplicationID & "&page=Edit"
                ElseIf Not IsDBNull(objReader.Item("DateEntryApplicationData")) Then
                    hypAplikasi.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewApplication/ViewApplication_003.aspx?CustID=" & Me.CustomerID & "&ApplicationID=" & Me.ApplicationID & "&page=Edit"
                    hypAsset.NavigateUrl = ""
                    hypAsuransi.NavigateUrl = ""
                    hypFinancial.NavigateUrl = ""
                    hypFinancial2.NavigateUrl = ""
                    hypRefund.NavigateUrl = ""
                    hypSurvey.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewApplication/ViewHasilSurvey.aspx?CustID=" & Me.CustomerID & "&ApplicationID=" & Me.ApplicationID & "&page=Edit"
                    hypPlafon.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewApplication/ViewPlafon.aspx?CustID=" & Me.CustomerID & "&ApplicationID=" & Me.ApplicationID & "&page=Edit"
                ElseIf Not IsDBNull(objReader.Item("DateEntryHasilSurvey")) Then
                    hypAplikasi.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewApplication/ViewApplication_003.aspx?CustID=" & Me.CustomerID & "&ApplicationID=" & Me.ApplicationID & "&page=Edit"
                    hypAsset.NavigateUrl = ""
                    hypAsuransi.NavigateUrl = ""
                    hypFinancial.NavigateUrl = ""
                    hypFinancial2.NavigateUrl = ""
                    hypRefund.NavigateUrl = ""
                    hypSurvey.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewApplication/ViewHasilSurvey.aspx?CustID=" & Me.CustomerID & "&ApplicationID=" & Me.ApplicationID & "&page=Edit"
                    hypPlafon.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewApplication/ViewPlafon.aspx?CustID=" & Me.CustomerID & "&ApplicationID=" & Me.ApplicationID & "&page=Edit"
                Else
                    hypAplikasi.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewApplication/ViewApplication_003.aspx?CustID=" & Me.CustomerID & "&ApplicationID=" & Me.ApplicationID & "&page=Edit"
                    hypAsset.NavigateUrl = ""
                    hypAsuransi.NavigateUrl = ""
                    hypFinancial.NavigateUrl = ""
                    hypFinancial2.NavigateUrl = ""
                    hypRefund.NavigateUrl = ""
                    hypSurvey.NavigateUrl = ""
                    hypPlafon.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewApplication/ViewPlafon.aspx?CustID=" & Me.CustomerID & "&ApplicationID=" & Me.ApplicationID & "&page=Edit"
                End If

            End If

            objReader.Close()
        Catch ex As Exception
            Throw New Exception(ex.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
            objCommand.Dispose()
        End Try
    End Sub

    Public Sub selectedTab(ByVal strTab As String)
        Select Case strTab
            Case "Application"
                tabAplikasi.Attributes.Add("class", "tab_selected")
                tabAsset.Attributes.Add("class", "tab_notselected")
                tabAsuransi.Attributes.Add("class", "tab_notselected")
                tabFinancial.Attributes.Add("class", "tab_notselected")
                tabFinancial2.Attributes.Add("class", "tab_notselected")
                tabRefund.Attributes.Add("class", "tab_notselected")
                tabSurvey.Attributes.Add("class", "tab_notselected")
                tabPlafon.Attributes.Add("class", "tab_notselected")
            Case "Asset"
                tabAplikasi.Attributes.Add("class", "tab_notselected")
                tabAsset.Attributes.Add("class", "tab_selected")
                tabAsuransi.Attributes.Add("class", "tab_notselected")
                tabFinancial.Attributes.Add("class", "tab_notselected")
                tabFinancial2.Attributes.Add("class", "tab_notselected")
                tabRefund.Attributes.Add("class", "tab_notselected")
                tabSurvey.Attributes.Add("class", "tab_notselected")
                tabPlafon.Attributes.Add("class", "tab_notselected")
            Case "Asuransi"
                tabAplikasi.Attributes.Add("class", "tab_notselected")
                tabAsset.Attributes.Add("class", "tab_notselected")
                tabAsuransi.Attributes.Add("class", "tab_selected")
                tabFinancial.Attributes.Add("class", "tab_notselected")
                tabFinancial2.Attributes.Add("class", "tab_notselected")
                tabRefund.Attributes.Add("class", "tab_notselected")
                tabSurvey.Attributes.Add("class", "tab_notselected")
                tabPlafon.Attributes.Add("class", "tab_notselected")
            Case "Financial"
                tabAplikasi.Attributes.Add("class", "tab_notselected")
                tabAsset.Attributes.Add("class", "tab_notselected")
                tabAsuransi.Attributes.Add("class", "tab_notselected")
                tabFinancial.Attributes.Add("class", "tab_selected")
                tabFinancial2.Attributes.Add("class", "tab_notselected")
                tabRefund.Attributes.Add("class", "tab_notselected")
                tabSurvey.Attributes.Add("class", "tab_notselected")
                tabPlafon.Attributes.Add("class", "tab_notselected")
            Case "Financial2"
                tabAplikasi.Attributes.Add("class", "tab_notselected")
                tabAsset.Attributes.Add("class", "tab_notselected")
                tabAsuransi.Attributes.Add("class", "tab_notselected")
                tabFinancial.Attributes.Add("class", "tab_notselected")
                tabFinancial2.Attributes.Add("class", "tab_selected")
                tabRefund.Attributes.Add("class", "tab_notselected")
                tabSurvey.Attributes.Add("class", "tab_notselected")
                tabPlafon.Attributes.Add("class", "tab_notselected")
            Case "Refund"
                tabAplikasi.Attributes.Add("class", "tab_notselected")
                tabAsset.Attributes.Add("class", "tab_notselected")
                tabAsuransi.Attributes.Add("class", "tab_notselected")
                tabFinancial.Attributes.Add("class", "tab_notselected")
                tabFinancial2.Attributes.Add("class", "tab_notselected")
                tabRefund.Attributes.Add("class", "tab_selected")
                tabSurvey.Attributes.Add("class", "tab_notselected")
                tabPlafon.Attributes.Add("class", "tab_notselected")
            Case "Survey"
                tabAplikasi.Attributes.Add("class", "tab_notselected")
                tabAsset.Attributes.Add("class", "tab_notselected")
                tabAsuransi.Attributes.Add("class", "tab_notselected")
                tabFinancial.Attributes.Add("class", "tab_notselected")
                tabFinancial2.Attributes.Add("class", "tab_notselected")
                tabRefund.Attributes.Add("class", "tab_notselected")
                tabSurvey.Attributes.Add("class", "tab_selected")
                tabPlafon.Attributes.Add("class", "tab_notselected")
            Case "Plafon"
                tabAplikasi.Attributes.Add("class", "tab_notselected")
                tabAsset.Attributes.Add("class", "tab_notselected")
                tabAsuransi.Attributes.Add("class", "tab_notselected")
                tabFinancial.Attributes.Add("class", "tab_notselected")
                tabFinancial2.Attributes.Add("class", "tab_notselected")
                tabRefund.Attributes.Add("class", "tab_notselected")
                tabSurvey.Attributes.Add("class", "tab_notselected")
                tabPlafon.Attributes.Add("class", "tab_selected")
            Case Else
                tabAplikasi.Attributes.Add("class", "tab_notselected")
                tabAsset.Attributes.Add("class", "tab_notselected")
                tabAsuransi.Attributes.Add("class", "tab_notselected")
                tabFinancial.Attributes.Add("class", "tab_notselected")
                tabRefund.Attributes.Add("class", "tab_notselected")
                tabFinancial2.Attributes.Add("class", "tab_notselected")
                tabSurvey.Attributes.Add("class", "tab_notselected")
                tabPlafon.Attributes.Add("class", "tab_notselected")
        End Select
    End Sub
End Class