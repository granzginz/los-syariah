﻿Option Strict On
Imports System.IO
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.General.CommonVariableHelper

Public Class ucIdentifikasiPemb
    Inherits ControlBased
    Dim dtCSV As New DataTable

    Private oCustomClass As New Parameter.AgreementList
    Private oController As New AgreementListController

    Private currentPage As Integer = DEFAULT_CURRENT_PAGE
    Private pageSize As Integer = DEFAULT_PAGE_SIZE
    Private currentPageNumber As Int32 = DEFAULT_CURRENT_PAGE
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            pnlGrid.Visible = False
        End If
    End Sub

    Protected Sub ButtonSearch_Click(sender As Object, e As EventArgs) Handles ButtonSearch.Click
        collectCSV()
        BindGridEntity()
    End Sub
    Private Sub collectCSV()
        Dim appPath As String = HttpContext.Current.Request.ApplicationPath
        Dim physicalPath As String = HttpContext.Current.Request.MapPath(appPath)
        Dim directory As String = physicalPath & "\xml\"
        Dim FileName As String = files.PostedFile.FileName
        Dim fullFileName As String = directory & FileName
        'Dim dt As New DataTable


        files.PostedFile.SaveAs(fullFileName)
        Dim sr As New IO.StreamReader(fullFileName)
        Try


            Dim newline() As String
            Dim originalLine As String

            For i As Integer = 1 To 7
                newline = sr.ReadLine.Split(","c)
            Next

            dtCSV.Columns.Add("lineNo")
            dtCSV.Columns.Add("keterangan")
            dtCSV.Columns.Add("jumlah")

            Dim x As Integer = 1
            While (Not sr.EndOfStream)
                originalLine = sr.ReadLine.ToString
                Dim OriSplit() As String = Split(originalLine, ",", """", False)

                Dim newrow As DataRow = dtCSV.NewRow
                newrow.ItemArray = {x, OriSplit(1).Replace("""", ""), OriSplit(3).Replace("""", "")}
                dtCSV.Rows.Add(newrow)

                x += 1
            End While


            'dtCSV = dt


        Catch ex As Exception

        Finally
            sr.Dispose()
            File.Delete(fullFileName)
        End Try
    End Sub
    Public Function Split( _
    ByVal expression As String, _
    ByVal delimiter As String, _
    ByVal qualifier As String, _
    ByVal ignoreCase As Boolean) As String()
        Dim _Statement As String = String.Format("{0}(?=(?:[^{1}]*{1}[^{1}]*{1})*(?![^{1}]*{1}))", Regex.Escape(delimiter), Regex.Escape(qualifier))

        Dim _Options As RegexOptions = RegexOptions.Compiled Or RegexOptions.Multiline
        If ignoreCase Then _Options = _Options Or RegexOptions.IgnoreCase

        Dim _Expression As Regex = New Regex(_Statement, _Options)
        Return _Expression.Split(expression)
    End Function
    Public Sub BindGridEntity()
        Dim dttEntity As DataTable = Nothing

        oCustomClass.strConnection = Me.GetConnectionString
        oCustomClass.ListData = oController.AgreementIdentifikasiPembayaran(oCustomClass, dtCSV).ListAgreement

        If Not oCustomClass Is Nothing Then
            dttEntity = oCustomClass.ListData
        End If
        dtgPaging.DataSource = dttEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()

        pnlGrid.Visible = True
        Me.mpLookupIdentifikasiPemb.Show()
    End Sub

End Class