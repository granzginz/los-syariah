﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UcViewContactPerson.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.UcViewContactPerson" %>
<div class="form_box_usercontrol">
    <div class="form_left_usercontrol">
        <label>
            Kontak</label>
        <asp:Label ID="txtContactPerson" runat="server"></asp:Label>
    </div>
</div>
<div class="form_box_usercontrol">
    <div class="form_left_usercontrol">
        <label>
            Jabatan</label>
        <asp:Label ID="txtContactPersonTitle" runat="server"></asp:Label>
    </div>
</div>
<div class="form_box_usercontrol">
    <div class="form_left_usercontrol">
        <label>
            Handphone</label>
        <asp:Label ID="txtMobilePhone" runat="server"></asp:Label>
    </div>
</div>
<div class="form_box_usercontrol">
    <div class="form_left_usercontrol">
        <label>
            e-Mail</label>
        <asp:Label ID="txtEmail" runat="server"></asp:Label>
    </div>
</div>
