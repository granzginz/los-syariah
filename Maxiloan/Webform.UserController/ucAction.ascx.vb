﻿
#Region "Import"

Imports Maxiloan.Parameter
Imports Maxiloan.Controller
#End Region

Public Class ucAction
    Inherits ControlBased
    Dim chrClientID As String
    Private oController As New CLActivityController

#Region "properties"
    Public Property cgid() As String
        Get
            Return CType(ViewState("CGID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CGID") = Value
        End Set
    End Property

    Public Property ActionID() As String
        Get
            Return CType(cboParent.SelectedItem.Value, String)
            'Return CType(cboParent.SelectedItem.Value, String)
        End Get
        Set(ByVal Value As String)
            cboParent.SelectedIndex = cboParent.Items.IndexOf(cboParent.Items.FindByValue(Value))
        End Set
    End Property

    Public Property ActionDescription() As String
        Get
            Return CType(ViewState("CollectorTypeName"), String)
            'Return CType(cboParent.SelectedItem.Text, String)
        End Get
        Set(ByVal Value As String)
            ViewState("CollectorTypeName") = Value
        End Set
    End Property


    Public Property ResultID() As String
        Get
            'Return hdnResult.Value
            Return CType(cboChild.SelectedItem.Value, String)
        End Get
        Set(ByVal Value As String)
            'ViewState("SupervisorID") = Value
            cboChild.SelectedIndex = cboChild.Items.IndexOf(cboChild.Items.FindByValue(Value))
        End Set
    End Property

    Public Property SupervisorName() As String
        Get
            Return CType(ViewState("SupervisorName"), String)
            'Return CType(cboChild.SelectedItem.Text, String)
        End Get
        Set(ByVal Value As String)
            ViewState("SupervisorName") = Value
            'cboChild.SelectedIndex = cboChild.Items.IndexOf(cboChild.Items.FindByText(Value))
        End Set
    End Property


#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not IsPostBack Then
            chrClientID = Trim(cboChild.ClientID)

            Dim dtParent As New DataTable
            Dim Coll As New Parameter.CLActivity
            'Dim CollList As New Parameter.Collector

            With Coll
                .strConnection = GetConnectionString()
                .CGID = Me.GroubDbID
                .strKey = ""
                .LoginId = Me.Loginid
            End With

            Coll = oController.CLActivityActionResult(Coll)
            dtParent = Coll.ListCLActivity

            cboParent.DataTextField = "ActionDescription"
            cboParent.DataValueField = "ActionID"
            cboParent.DataSource = dtParent
            cboParent.DataBind()
            cboParent.Items.Insert(0, "Select One")
            cboParent.Items(0).Value = "0"
            cboParent.SelectedIndex = 0
            'BindChild()

        End If

    End Sub

    Public Sub BindChild()
        Dim dtChild As New DataTable
        Dim Coll As New Parameter.CLActivity
        Dim CollList As New Parameter.CLActivity

        With Coll
            .strConnection = GetConnectionString()
            .CGID = Me.GroubDbID
            .ActivityID = ""
        End With

        CollList = oController.GetResult(Coll)
        dtChild = CollList.ListCLActivity
        'Header.InnerHtml = GenerateScript(dtChild)
    End Sub

    Protected Function WOPonChange() As String
        Return "WOPChange('" & Trim(cboParent.ClientID) & "','" & Trim(cboChild.ClientID) & "','" & Trim(hdnResult.ClientID) & "',((this.selectedIndex == -1) ? null : ListData[this.selectedIndex]));"
    End Function

    Public Function SetFocuscboChild() As String
        Return "SetcboChildFocus()"
    End Function

    Private Function GenerateScript(ByVal DtTable As DataTable) As String
        Dim strScript As String
        Dim strScript1 As String
        Dim ints As Integer
        Dim DataRow As DataRow()
        Dim strType As String
        Dim i As Int32
        Dim j As Int32
        ints = DtTable.Rows.Count
        strScript = "<script language=""JavaScript"">" & vbCrLf
        strScript &= "ListData = new Array(" & vbCrLf
        strType = ""
        For j = 0 To cboParent.Items.Count - 1
            DataRow = DtTable.Select(" ActionID = '" & cboParent.Items(j).Value & "'")
            If DataRow.Length > 0 Then
                For i = 0 To DataRow.Length - 1
                    If strType <> CStr(DataRow(i)("ActionID")).Trim Then
                        strType = CStr(DataRow(i)("ActionID")).Trim
                        strScript &= "new Array(" & vbCrLf
                        strScript1 = ""
                    End If


                    If strScript1 = "" Then
                        strScript1 = " new Array('" & CStr(DataRow(i)("Description")).Trim & "','" & CStr(IIf(IsDBNull(DataRow(i)("ResultID")), "-", DataRow(i)("ResultID"))).Trim & "') "
                    Else
                        strScript1 &= "," & vbCrLf & " new Array('" & CStr(DataRow(i)("Description")).Trim & "','" & CStr(IIf(IsDBNull(DataRow(i)("ResultID")), "-", DataRow(i)("ResultID"))).Trim & "') "
                    End If
                Next
                strScript &= strScript1 & ")," & vbCrLf
            Else
                strScript &= " null," & vbCrLf
            End If
        Next

        If Right(strScript.Trim, 5) = "null," Then
            strScript = Left(strScript.Trim, strScript.Trim.Length - 1)
        Else
            strScript = Left(strScript.Trim, strScript.Trim.Length - 2)
        End If

        strScript &= vbCrLf & "));" & vbCrLf
        strScript &= "</script>"
        Return strScript
    End Function

    'Public Sub BindCombo()
    '    cboParent.SelectedIndex = cboParent.Items.IndexOf(cboParent.Items.FindByValue(Me.CollectorTypeID))
    '    cboChild.SelectedIndex = cboChild.Items.IndexOf(cboChild.Items.FindByValue("Flin"))
    'End Sub

    Public Sub GetComboValue()
        Me.ActionID = cboParent.SelectedItem.Value.Trim
        Me.ActionDescription = cboParent.SelectedItem.Text.Trim
    End Sub

    Private Sub cboParent_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboParent.SelectedIndexChanged
        'cboChild.Visible = True
        InsertComboChild()
    End Sub

    'Public Function GenerateChild() As String

    '    Return "GenerateChild('" & Trim(cboParent.ClientID) & "','" & Trim(cboChild.ClientID) & "','" & Trim(hdnSP.ClientID) & "',((this.selectedIndex == -1) ? null : ListData[this.selectedIndex])" & Me.SupervisorID & ");"
    'End Function

    'Public Sub GoGenerateChild()
    '    Header.InnerHtml = "<script language=""""JavaScript""""> SetcboChildFocus();</script>"
    'End Sub

    Public Sub InsertComboChild()
        Dim coll As New Parameter.CLActivity
        Dim Supervisor_List As New Parameter.CLActivity
        Dim i As Integer
        Dim indexSelected As Int16

        Dim dt As New DataTable

        With coll
            .strConnection = GetConnectionString()
            .CGID = Me.cgid
            .ActivityID = cboParent.SelectedItem.Value.Trim
        End With

        Supervisor_List = oController.GetResult(coll)
        dt = Supervisor_List.ListCLActivity



        With cboChild
            .Items.Clear()

            For i = 0 To dt.Rows.Count - 1
                .Items.Insert(i, CStr(dt.Rows(i).Item("Description")))
                .Items(i).Value = CStr(dt.Rows(i).Item("ResultID"))
                If .Items(i).Value.Trim = Me.ResultID.Trim Then indexSelected = CShort(i)
            Next
            .SelectedIndex = indexSelected
        End With
    End Sub
End Class