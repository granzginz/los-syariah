﻿
Public Class ucLookupKodePos

    Inherits ControlBased

    
#Region "Properties"
    Public Property ZipCode() As String
        Get
            Return CType(txtZipCode.Text.Trim, String)
        End Get
        Set(ByVal Value As String)
            txtZipCode.Text = Value
        End Set
    End Property
    Public Property Kelurahan() As String
        Get
            Return CType(txtKelurahan.Text.Trim, String)
        End Get
        Set(ByVal Value As String)
            txtKelurahan.Text = Value
        End Set
    End Property
    Public Property Kecamatan() As String
        Get
            Return CType(txtKecamatan.Text.Trim, String)
        End Get
        Set(ByVal Value As String)
            txtKecamatan.Text = Value
        End Set
    End Property
    Public Property City() As String
        Get
            Return CType(txtCity.Text.Trim, String)
        End Get
        Set(ByVal Value As String)
            txtCity.Text = Value
        End Set
    End Property
    Public Property Style() As String
        Get
            Return CStr(ViewState("style"))
        End Get
        Set(ByVal Value As String)
            ViewState("style") = Value
        End Set
    End Property

    Public Property IsRequired As Boolean
        Get
            Return CBool(ViewState("Validation"))
        End Get
        Set(ByVal Value As Boolean)
            ViewState("Validation") = Value
        End Set
    End Property

#End Region
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If IsRequired = False Then
                RequiredFieldValidator1.Enabled = False
                lblKodePos.Visible = False
                lblKodePosNotValidate.Visible = True
            Else
                RequiredFieldValidator1.Enabled = True
                lblKodePos.Visible = True
                lblKodePosNotValidate.Visible = False
            End If
        End If
    End Sub
    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        txtCity.Text = ""
        txtKecamatan.Text = ""
        txtKelurahan.Text = ""
        txtZipCode.Text = ""


    End Sub
End Class