﻿Public Class ucLookUpCOA2
    Inherits ControlBased

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Public Delegate Sub CatSelectedHandler(ByVal CoaId As String,
                                          ByVal CoaName As String,
                                          ByVal CoaBranch As String)
    Public Event CatSelected As CatSelectedHandler

    Public Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Public Sub BindGridEntity(ByVal cmdWhere As String)
        'Dim dttEntity As DataTable = Nothing
        'Dim COAdata As New Parameter.MasterAcc
        'Dim COAcontroller As New Controller.COAController
        'Dim dt As DataTable

        'With COAdata
        '    .strConnection = GetConnectionString()
        '    .WhereCond = cmdWhere
        '    .CurrentPage = currentPage
        '    .PageSize = pageSize 
        'End With

        'dt = COAcontroller.SelectData2(COAdata)

        'DtgAsset.DataSource = dt
        'DtgAsset.DataBind()
        'PagingFooter() 
        'Popup()
    End Sub

#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            lblMessage.Text = "Data not found....."
            lblMessage.Visible = True
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridEntity(Me.CmdWhere)
    End Sub

    Private Sub btnPageNumb_click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int32)
                BindGridEntity(Me.CmdWhere)
            End If
        End If
    End Sub

#End Region

    Protected Sub DtgAsset_ItemCommand(ByVal sender As Object, ByVal e As DataGridCommandEventArgs) Handles DtgAsset.ItemCommand
        If e.CommandName = "Select" Then
            Dim i As Integer = e.Item.ItemIndex
            With DtgAsset
                RaiseEvent CatSelected(.DataKeys.Item(i).ToString.Trim,
                                    .Items(i).Cells.Item(2).Text.Trim,
                                    .Items(i).Cells.Item(8).Text.Trim)

                'coaId, coaName, coaBranchs
            End With
            
        End If
    End Sub

    Public Sub Popup()
        Me.ModalPopupExtender.Show()
    End Sub

    Protected Sub imbExit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Me.ModalPopupExtender.Hide()
    End Sub

    Public Function AccountTypeToValue(ByVal accType As Integer) As String
        Select Case accType
            Case 1
                Return "Cash/Bank"
            Case 2
                Return "Account Receivable"
            Case Else
                Return String.Empty
        End Select
    End Function

    Public Function SubAccountToValue(ByVal isSubAccount As Boolean) As String
        Select Case isSubAccount
            Case True
                Return "Detail"
            Case False
                Return "Parent"
            Case Else
                Return String.Empty
        End Select
    End Function

End Class