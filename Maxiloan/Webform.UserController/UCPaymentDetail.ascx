﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UCPaymentDetail.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.UCPaymentDetail" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="aspajax" %>
<%--<%@ Register TagPrefix="uc1" TagName="ValidDate" Src="ValidDate.ascx" %>--%>
<%@ Register TagPrefix="uc1" TagName="ucBGNumber" Src="ucBGNumber.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucNumberFormat" Src="ucnumberformat.ascx" %>
<div id="scriptJava" runat="server">
</div>
<%--<script type="text/javascript" src="http://code.jquery.com/jquery-1.8.2.js"></script>--%>
<script language="JavaScript" type="text/javascript">
    /*
    var hidBankAccount;
    function WOPChange(pCmbOfPayment, pBankAccount, pHidDetail, pAmountReceive, itemArray) {
    hidBankAccount = eval('document.forms[0].' + pHidDetail);
    var a = eval('document.forms[0].' + pCmbOfPayment).options[eval('document.forms[0].' + pCmbOfPayment).selectedIndex].value;
    if (a == 'CP') {
    for (i = eval('document.forms[0].' + pBankAccount).options.length; i >= 0; i--) {
    eval('document.forms[0].' + pBankAccount).options[i] = null;
    }
    eval('document.forms[0].' + pAmountReceive).disabled = true;
    eval('document.forms[0].' + pAmountReceive).value = 0;
    eval('document.forms[0].' + pBankAccount).disabled = true;
    hidBankAccount.value = '';
    }
    else {
    eval('document.forms[0].' + pAmountReceive).disabled = false;
    eval('document.forms[0].' + pBankAccount).disabled = false;
    var i, j;
    for (i = eval('document.forms[0].' + pBankAccount).options.length; i >= 0; i--) {
    eval('document.forms[0].' + pBankAccount).options[i] = null;
    }
    j = 0;
    eval('document.forms[0].' + pBankAccount).options[j++] = new Option('Select One', '0');
    var a;
    if (itemArray != null) {
    for (i = 0; i < itemArray.length; i++) {
    eval('document.forms[0].' + pBankAccount).options[j++] = new Option(itemArray[i][0], itemArray[i][1]);
    }
    eval('document.forms[0].' + pBankAccount).selected = true;
    }
    }
    }
    
    

    function setCboDetlVal(l) {
    hidBankAccount.value = l;

    }*/
    function cmbKwitansi(receiptNo) {

        $.ajax({
            type: "POST",
            url: "InstallRCV_Collection.aspx/BindKwitansiCollection",
            data: '{collid: "' + receiptNo + '" }',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg) {
                var j = jQuery.parseJSON(msg.d);
                var options;
                for (var i = 0; i < j.length; i++) {
                    options += '<option value="' + j[i].optionValue + '">' + j[i].optionDisplay + '</option>'
                }
                $('#<%=ddNoKwitansi.ClientID %>').html(options)
            },
            error: function (data) {
                alert('Something Went Wrong')
            }
        });
    }
    function OnSuccess(response) {
//        $('#lblAdminFee_txtNumber').val(response.d);
//        perkiraanTotal();
    }

    function GetKwitansi(ddlId) {

        var Collector = document.getElementById(ddlId.id);
        cmbKwitansi(Collector.value);
      

    }


   
</script>
<%--<asp:ScriptManager runat="server" ID="ScriptManager1">
</asp:ScriptManager>--%>
<input id="hdnBankAccount" type="hidden" name="hdnBankAccount" runat="server" />
<input id="hdnBankAccountName" type="hidden" name="hdnBankAccountName" runat="server" />
<input id="HidDetail" name="HidDetail" type="hidden" runat="server" />
<asp:Panel runat="server" ID="pnlWOP" Visible="false">
    <div class="form_box_usercontrol">
        <div class="form_left">
            <label class="label_split">
                Cara Pembayaran
            </label>
            <%-- <asp:DropDownList ID="cmbWayOfPayment" runat="server" OnTextChanged="<%#WOPonChange()%>">
        </asp:DropDownList>--%>
            <asp:DropDownList ID="cmbWayOfPayment" runat="server" AutoPostBack="True" >
            </asp:DropDownList>
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" ControlToValidate="cmbWayOfPayment"
                Visible="True" Enabled="False" Display="Dynamic" ErrorMessage="Please Fill Way Of Payment"
                InitialValue="0" CssClass="validator_general"></asp:RequiredFieldValidator>
        </div>
        <div class="form_right">
            <label class="label_split">
                Jumlah Terima
            </label>
            <uc1:ucnumberformat runat="server" id="ucAmountReceive"></uc1:ucnumberformat>
            <%-- <asp:TextBox ID="txtAmountReceive" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="reqAmountReceive" runat="server" ControlToValidate="txtAmountReceive"
                ErrorMessage="You have to fill Amount" Display="Dynamic" Visible="True" Enabled="False"
                CssClass="validator_general"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="regAmountReceive" runat="server" ControlToValidate="txtAmountReceive"
                ErrorMessage="You have to fill numeric value" Display="Dynamic" Enabled="False"
                ValidationExpression="^\d{1,}$|^\d{1,}[.{1}]\d{1,}$" Visible="True"></asp:RegularExpressionValidator>
            <asp:RangeValidator ID="rngValidator" runat="server" ControlToValidate="txtAmountReceive"
                ErrorMessage="You have to fill numeric value" Display="Dynamic" MinimumValue="-9999999999"
                MaximumValue="999999999999" Type="Double" Enabled="False" Visible="True"></asp:RangeValidator>--%>
        </div>
    </div>
</asp:Panel>
<asp:Panel ID="pnlBank" runat="server">
    <%--<div class="form_box_usercontrol">--%>
    <div class="form_left">
        <label class="label_split">
            Rekening
        </label>
        <%--                <asp:DropDownList ID="cmbBankAccount" onchange="setCboDetlVal(this.options[this.selectedIndex].value);"
                    runat="server">
                </asp:DropDownList>--%>
        <asp:DropDownList ID="cmbBankAccount" runat="server">
        </asp:DropDownList>
        <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" ControlToValidate="cmbBankAccount"
            Visible="True" Enabled="True" Display="Dynamic" ErrorMessage="Please Fill Bank Account"
            InitialValue="0" CssClass="validator_general"></asp:RequiredFieldValidator>
    </div>
    <div class="form_right" runat="server" id="divTglBayar">
        <label class="label_split">
            Tanggal Bayar
        </label>
        <%--<uc1:ValidDate ID="oValueDate" runat="server"></uc1:ValidDate>--%>
        <asp:TextBox ID="txtValueDate" runat="server" CssClass="small_text" />
        <aspajax:CalendarExtender ID="ceValueDate" runat="server" TargetControlID="txtValueDate"
            Format="dd/MM/yyyy" />
        <asp:RequiredFieldValidator ID="rfvValueDate" runat="server" ControlToValidate="txtValueDate"
            ErrorMessage="Harap isi dengan tanggal jatuh tempo" Enabled="false" Display="Dynamic" />
    </div>
    <%--</div>--%>
</asp:Panel>
<asp:Panel ID="pnlColl" runat="server">
    <div class="form_box_usercontrol">
        <div class="form_left">
            <label>
                Collector
            </label>
            <asp:DropDownList ID="ddlCollectorID" runat="server"> <%-- AutoPostBack="true"> --%>
            </asp:DropDownList>
            <asp:RequiredFieldValidator ID="rfvCollector" runat="server" ErrorMessage="Pilih Collector!"
                Display="Dynamic" Enabled="True" Visible="True" ControlToValidate="ddlCollectorID"
                InitialValue="0" CssClass="validator_general"></asp:RequiredFieldValidator>
        </div>
        <%--<div class="form_right" runat="server" id="divTglBayarColl">
            <label>
                Tanggal Bayar
            </label>        
            <asp:TextBox ID="txtTglBayar" runat="server" CssClass="small_text" />
            <aspajax:CalendarExtender ID="CEtxtTglBayar" runat="server" TargetControlID="txtTglBayar"
                Format="dd/MM/yyyy" />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtTglBayar"
                ErrorMessage="Harap isi dengan tanggal jatuh tempo" Enabled="false" Display="Dynamic" />
        </div>--%>
    </div>    
    <div class="form_box_usercontrol">
        <div class="form_left" runat="server" id="divNoKwitColl">
            <label>
                No Kwitansi Sementara
            </label>
             <asp:DropDownList ID="ddNoKwitansi" runat="server" Visible="false"> 
            </asp:DropDownList>
            <asp:TextBox runat="server" ID="txtTTS" />
         
            <asp:RequiredFieldValidator ID="RFVNoKwitansi" runat="server" ControlToValidate="txtTTS"
            Visible="True" Enabled="True" Display="Dynamic" ErrorMessage="Masukkan No. TTS" CssClass="validator_general"></asp:RequiredFieldValidator>
        </div>
    </div>
</asp:Panel>
<asp:Panel ID="pnlRest" runat="server" Visible="false">
    <asp:Panel runat="server" ID="pnlRefNo" Visible="false">
        <div class="form_box_usercontrol_title_nobg">        
            <div class="form_left">
                <label class="label_split">
                    Terima Dari
                </label>
                <asp:TextBox ID="txtReceivedFrom" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvText" runat="server" ControlToValidate="txtReceivedFrom"
                Visible="True" Enabled="True" Display="Dynamic" ErrorMessage="You have to fill this
                field" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
            <div class="form_right">
                <asp:Label ID="lblNoBonMerah" runat="server" Text="Nomor Bon Merah" class="label_split" />
                <asp:TextBox ID="txtReferenceNo" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvRef" runat="server" ControlToValidate="txtReferenceNo"
                    Visible="True" Enabled="False" Display="Dynamic" ErrorMessage="You have to fill this field"
                    CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlBayarMelalui" runat="server">
        <div class="form_box_usercontrol">
            <div class="form_left">
                <label>
                    Bayar Melalui
                </label>
                <asp:DropDownList ID="ddlBayarDi" runat="server">
                    <asp:ListItem Value="0">Select One</asp:ListItem>
                    <asp:ListItem Value="Cabang">Bayar di Cabang</asp:ListItem>
                    <asp:ListItem Value="Collection">Hasil Collection</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator3" runat="server" ErrorMessage="Select bayar melalui apa"
                    Display="Dynamic" Enabled="False" Visible="True" ControlToValidate="ddlBayarDi"
                    InitialValue="0" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
    </asp:Panel>
    <div class="form_box_usercontrol_title_nobg">
        <%--<div class="form_box_usercontrol">--%>
        <div class="form_left_usercontrol">
            <label class="label_general">
                Keterangan
            </label>
            <asp:TextBox ID="txtNotes" runat="server" TextMode="MultiLine" CssClass="multiline_textbox_uc"></asp:TextBox>
        </div>
    </div>
</asp:Panel>
