﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucFindEmpployee2.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.ucFindEmpployee2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<div class="form_box_usercontrol">
    <div class="form_left_usercontrol">
        <asp:Label ID="lblText" runat="server" CssClass="label_split_req">Collection Group Head</asp:Label>
        <asp:TextBox ID="txtEmployeeName" runat="server" MaxLength="5" ReadOnly="True" CssClass="long_text"></asp:TextBox>
        <asp:Button runat="server" ID="btnLookup" CausesValidation="false" Text="..." CssClass="small buttongo blue" />
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Width="152px" runat="server"
            ErrorMessage="Please select Employee" ControlToValidate="txtEmployeeName" CssClass="validator_general"></asp:RequiredFieldValidator>
    </div>
</div>
<asp:ModalPopupExtender runat="server" ID="ModalPopupExtender1" TargetControlID="btnLookup"
    PopupControlID="Panel1" CancelControlID="btnExit" BackgroundCssClass="wpbg">
</asp:ModalPopupExtender>
<asp:Panel runat="server" ID="Panel1">
    <div class="wp">
        <asp:Label runat="server" ID="lblMessage" Visible="false"></asp:Label>
        <asp:ImageButton runat="server" ID="btnExit" ImageUrl="../Images/exit_lookup00.png"
            CssClass="wpbtnexit" />
        <div class="form_box_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h4>
                    DAFTAR EMPLOYEE
                </h4>
            </div>
        </div>
        <asp:Panel ID="pnlEmployeeList" runat="server" Visible="false">
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ws">
                        <asp:DataGrid ID="dtg" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                            OnSortCommand="SortGrid" BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <Columns>
                                <asp:ButtonColumn CommandName="SELECT" Text="SELECT" CausesValidation="false"></asp:ButtonColumn>
                                <asp:TemplateColumn SortExpression="EmployeeID" HeaderText="ID">
                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblEmployeeID" runat="server" Visible="true" Text='<%# DataBinder.eval(Container,"DataItem.EmployeeID")%>'> </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="EmployeeName" HeaderText="NAMA KARYAWAN">
                                    <ItemTemplate>
                                        <asp:Label ID="lblEmployeeName" runat="server" Visible="true" Text='<%# DataBinder.eval(Container,"DataItem.EmployeeName")%>'> </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn DataField="EmployeePosition" SortExpression="EmployeePosition" HeaderText="POSISI">
                                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                </asp:BoundColumn>
                                <asp:TemplateColumn Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblBranchID" runat="server" Visible="true" Text='<%# DataBinder.eval(Container,"DataItem.BranchID")%>'> </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                    </div>
                    <div class="button_gridnavigation">
                        <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" ImageUrl="../Images/grid_navbutton01.png"
                            OnCommand="NavigationLink_Click" CommandName="First"></asp:ImageButton>
                        <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" ImageUrl="../Images/grid_navbutton02.png"
                            OnCommand="NavigationLink_Click" CommandName="Prev"></asp:ImageButton>
                        <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" ImageUrl="../Images/grid_navbutton03.png"
                            OnCommand="NavigationLink_Click" CommandName="Next"></asp:ImageButton>
                        <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" ImageUrl="../Images/grid_navbutton04.png"
                            OnCommand="NavigationLink_Click" CommandName="Last"></asp:ImageButton>
                        Page
                        <asp:TextBox ID="txtGoPage" runat="server" Width="34px">1</asp:TextBox>
                        <asp:Button ID="btnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                            EnableViewState="False" />
                        <asp:RangeValidator ID="Rangevalidator1" runat="server" Font-Size="11px" Type="Integer"
                            MaximumValue="999999999" ErrorMessage="Page No. is not valid" MinimumValue="1"
                            ControlToValidate="txtGoPage" Display="Dynamic"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="rfvGo" runat="server" Font-Size="11px" ErrorMessage="Page No. is not valid"
                            ControlToValidate="txtGoPage" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtGoPage" MinimumValue="1"
                            MaximumValue="999" ErrorMessage="Page No. is not valid!" Type="Double" Display="Dynamic"></asp:RangeValidator>
                    </div>
                    <div class="label_gridnavigation">
                        <asp:Label ID="lblPage" runat="server"></asp:Label>of
                        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                        <asp:Label ID="lblTotRec" runat="server"></asp:Label>record(s)
                    </div>
                </div>
            </div>
            <asp:TextBox ID="txtEmployeeID" runat="server" Visible="false"></asp:TextBox>
            <asp:TextBox ID="txtBranchID" runat="server" Visible="false"></asp:TextBox>
        </asp:Panel>
        <asp:Panel runat="server" ID="PanelSearch">
            <div class="form_box">
                <div class="form_single">
                    <h4>
                        CARI EMPLOYEE</h4>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Cari Berdasarkan
                    </label>
                    <asp:DropDownList ID="cboSearchBy" runat="server">
                        <asp:ListItem Value="EmployeeName">Employee Name</asp:ListItem>
                    </asp:DropDownList>
                    <asp:TextBox ID="txtSearchBy" runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="btnSearch" runat="server" CausesValidation="False" Text="Find" CssClass="small button blue" />
                <asp:Button ID="btnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray" />
            </div>
        </asp:Panel>
    </div>
</asp:Panel>
