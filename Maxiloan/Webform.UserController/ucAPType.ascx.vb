﻿Imports Maxiloan.Controller
Imports Maxiloan.Parameter

Public Class ucAPType
    Inherits ControlBased

#Region " Private Const "
    Private m_controller As New ImplementasiControler    
#End Region

    Public ReadOnly Property selectedAPType As String
        Get
            Return cmbAPType.SelectedItem.Text
        End Get
    End Property

    Public Property Filter() As String
        Get
            Return CType(ViewState("Filter"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Filter") = Value
        End Set
    End Property

    Public Property selectedAPTypeID() As String
        Get
            Return CType(cmbAPType.SelectedItem.Value, String)
        End Get
        Set(ByVal Value As String)
            cmbAPType.SelectedIndex = cmbAPType.Items.IndexOf(cmbAPType.Items.FindByValue(Value))
        End Set
    End Property

    Public Property APType As String
        Get
            Return txtAPType.Text.Trim
        End Get
        Set(ByVal value As String)
            txtAPType.Text = value
        End Set
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not IsPostBack Then
        End If
    End Sub

    Public Sub BindAPType()

        cmbAPType.Items.Clear()
        Dim DtApType As New DataTable
        Dim dvAPType As New DataView        
        Dim customClass As New Parameter.Implementasi

        With customClass
            .strConnection = GetConnectionString()            
            .SPName = "spGetAPType"
            .WhereCond = Filter            
        End With

        customClass = m_controller.GetSP(customClass)
        DtApType = customClass.Listdata
        dvAPType = DtApType.DefaultView

        With cmbAPType
            .DataValueField = "ID"
            .DataTextField = "Description"
            .DataSource = dvAPType
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = "0"
        End With
    End Sub

End Class