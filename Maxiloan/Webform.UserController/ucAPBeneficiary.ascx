﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucAPBeneficiary.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.ucAPBeneficiary" %>
<div class="form_box_title">
    <div class="form_single">
        <h4>
            REKENING PENERIMA</h4>
    </div>
</div>
<div class="form_box">
    <div class="form_left">
        <label>
            Nama Rekening</label>
        <asp:Label ID="lblAccName" runat="server"></asp:Label>
    </div>
    <div class="form_right">
        <label>
            No Rekening</label>
        <asp:Label ID="lblAccNo" runat="server"></asp:Label>
    </div>
</div>
<div class="form_box">
    <div class="form_left">
        <label>
            Nama Bank</label>
        <asp:Label ID="lblBankName" runat="server"></asp:Label>
    </div>
    <div class="form_right">
        <label>
            Nama Cabang</label>
        <asp:Label ID="lblBankBran" runat="server"></asp:Label>
    </div>
</div>
