﻿Imports Maxiloan.Controller
Imports Maxiloan.Parameter

Public Class ucGuarantor
    Inherits ControlBased

    Private m_Controll As New DataUserControlController
    Protected WithEvents txtPenghasilan As ucNumberFormat
    Protected WithEvents UcAddress As UcCompanyAddress
#Region "Properties"
    Public Property Name As String
        Get
            'Return CType(ViewState("GuarantorName"), String)
            Return txtNamaGuarantor.Text.Trim
        End Get
        Set(ByVal value As String)
            'ViewState("GuarantorName") = value
            txtNamaGuarantor.Text = value
        End Set
    End Property
    Public Property txtNamaGuarantorEnabled As Boolean
        Get
            Return txtNamaGuarantor.Enabled
        End Get
        Set(ByVal value As Boolean)
            txtNamaGuarantor.Enabled = value
        End Set
    End Property
    Public Property rfvNamaGuarantorVisible As Boolean
        Get
            Return rfvNamaGuarantor.Visible
        End Get
        Set(ByVal value As Boolean)
            rfvNamaGuarantor.Visible = value
        End Set
    End Property
    Public Property Jabatan As String
        Get
            'Return CType(ViewState("GuarantorJabatan"), String)
            Return txtJabatan.Text.Trim
        End Get
        Set(ByVal value As String)
            'ViewState("GuarantorJabatan") = value
            txtJabatan.Text = value
        End Set
    End Property

    Public Property Address As String
        Get
            'Return CType(ViewState("GuarantorAddress"), String)
            Return UcCompanyAddress1.Address
        End Get
        Set(ByVal value As String)
            'ViewState("GuarantorAddress") = value
            UcCompanyAddress1.Address = value
        End Set
    End Property

    Public Property RT As String
        Get
            'Return CType(ViewState("GuarantorRT"), String)
            Return UcCompanyAddress1.RT.Trim
        End Get
        Set(ByVal value As String)
            'ViewState("GuarantorRT") = value
            UcCompanyAddress1.RT = value
        End Set
    End Property

    Public Property RW As String
        Get
            'Return CType(ViewState("GuarantorRW"), String)
            Return UcCompanyAddress1.RW.Trim
        End Get
        Set(ByVal value As String)
            'ViewState("GuarantorRW") = value
            UcCompanyAddress1.RW = value
        End Set
    End Property

    Public Property Kelurahan As String
        'Get
        '    Return CType(ViewState("GuarantorKelurahan"), String)
        'End Get
        'Set(ByVal value As String)
        '    ViewState("GuarantorKelurahan") = value
        'End Set
        Get
            Return UcCompanyAddress1.Kelurahan.Trim
        End Get
        Set(ByVal value As String)
            UcCompanyAddress1.Kelurahan = value
        End Set
    End Property

    Public Property Kecamatan As String
        'Get
        '    Return CType(ViewState("GuarantorKecamatan"), String)
        'End Get
        'Set(ByVal value As String)
        '    ViewState("GuarantorKecamatan") = value
        'End Set
        Get
            Return UcCompanyAddress1.Kecamatan.Trim
        End Get
        Set(ByVal value As String)
            UcCompanyAddress1.Kecamatan = value
        End Set
    End Property

    Public Property Kota As String
        'Get
        '    Return CType(ViewState("GuarantorKota"), String)
        'End Get
        'Set(ByVal value As String)
        '    ViewState("GuarantorKota") = value
        'End Set
        Get
            Return UcCompanyAddress1.City.Trim
        End Get
        Set(ByVal value As String)
            UcCompanyAddress1.City = value
        End Set
    End Property

    Public Property KodePos As String
        'Get
        '    Return CType(ViewState("GuarantorKodePos"), String)
        'End Get
        'Set(ByVal value As String)
        '    ViewState("GuarantorKodePos") = value
        'End Set
        Get
            Return UcCompanyAddress1.ZipCode.Trim
        End Get
        Set(ByVal value As String)
            UcCompanyAddress1.ZipCode = value
        End Set
    End Property

    Public Property Area1 As String
        'Get
        '    Return CType(ViewState("GuarantorArea1"), String)
        'End Get
        'Set(ByVal value As String)
        '    ViewState("GuarantorArea1") = value
        'End Set
        Get
            Return UcCompanyAddress1.AreaPhone1.Trim
        End Get
        Set(ByVal value As String)
            UcCompanyAddress1.AreaPhone1 = value
        End Set
    End Property

    Public Property Phone1 As String
        'Get
        '    Return CType(ViewState("GuarantorPhone1"), String)
        'End Get
        'Set(ByVal value As String)
        '    ViewState("GuarantorPhone1") = value
        'End Set
        Get
            Return UcCompanyAddress1.Phone1.Trim
        End Get
        Set(ByVal value As String)
            UcCompanyAddress1.Phone1 = value
        End Set
    End Property

    Public Property Area2 As String
        'Get
        '    Return CType(ViewState("GuarantorArea2"), String)
        'End Get
        'Set(ByVal value As String)
        '    ViewState("GuarantorArea2") = value
        'End Set
        Get
            Return UcCompanyAddress1.AreaPhone2.Trim
        End Get
        Set(ByVal value As String)
            UcCompanyAddress1.AreaPhone2 = value
        End Set
    End Property

    Public Property Phone2 As String
        'Get
        '    Return CType(ViewState("GuarantorPhone2"), String)
        'End Get
        'Set(ByVal value As String)
        '    ViewState("GuarantorPhone2") = value
        'End Set
        Get
            Return UcCompanyAddress1.Phone2.Trim
        End Get
        Set(ByVal value As String)
            UcCompanyAddress1.Phone2 = value
        End Set
    End Property

    Public Property AreaFax As String
        'Get
        '    Return CType(ViewState("GuarantorAreaFax"), String)
        'End Get
        'Set(ByVal value As String)
        '    ViewState("GuarantorAreaFax") = value
        'End Set
        Get
            Return UcCompanyAddress1.AreaFax.Trim
        End Get
        Set(ByVal value As String)
            UcCompanyAddress1.AreaFax = value
        End Set
    End Property

    Public Property Fax As String
        'Get
        '    Return CType(ViewState("GuarantorFax"), String)
        'End Get
        'Set(ByVal value As String)
        '    ViewState("GuarantorFax") = value
        'End Set
        Get
            Return UcCompanyAddress1.Fax.Trim
        End Get
        Set(ByVal value As String)
            UcCompanyAddress1.Fax = value
        End Set
    End Property

    Public Property NoHP As String
        'Get
        '    Return CType(ViewState("GuarantorNoHP"), String)
        'End Get
        'Set(ByVal value As String)
        '    ViewState("GuarantorNoHP") = value
        'End Set
        Get
            Return txtNoHP.Text.Trim
        End Get
        Set(ByVal value As String)
            txtNoHP.Text = value
        End Set
    End Property

    Public Property Email As String
        'Get
        '    Return CType(ViewState("GuarantorEmail"), String)
        'End Get
        'Set(ByVal value As String)
        '    ViewState("GuarantorEmail") = value
        'End Set
        Get
            Return txtEmail.Text.Trim
        End Get
        Set(ByVal value As String)
            txtEmail.Text = value
        End Set
    End Property

    Public Property Catatan As String
        'Get
        '    Return CType(ViewState("GuarantorCatatan"), String)
        'End Get
        'Set(ByVal value As String)
        '    ViewState("GuarantorCatatan") = value
        'End Set
        Get
            Return txtCatatan.Text.Trim
        End Get
        Set(ByVal value As String)
            txtCatatan.Text = value
        End Set
    End Property

    Public Property Penghasilan As Decimal
        'Get
        '    Return CType(ViewState("GuarantorPenghasilan"), Decimal)
        'End Get
        'Set(ByVal value As Decimal)
        '    ViewState("GuarantorPenghasilan") = value
        'End Set
        Get
            Return CDec(IIf(IsNumeric(txtPenghasilan.Text.Trim), txtPenghasilan.Text, "0"))
        End Get
        Set(ByVal value As Decimal)
            txtPenghasilan.Text = value.ToString
        End Set
    End Property

    Public Property ApplicationID As String
        Get
            Return CType(ViewState("ApplicationID"), String)
        End Get
        Set(ByVal value As String)
            ViewState("ApplicationID") = value
        End Set
    End Property

    Public WriteOnly Property EnabledGuarantorName As Boolean
        Set(ByVal value As Boolean)
            txtNamaGuarantor.Enabled = value
        End Set
    End Property
#End Region

    Public Sub bindData(ByVal mode As String)
        Dim oCustomClass As New AgreementGuarantor

        If mode.ToUpper.Trim = "REQ" Then
            If Not Me.ApplicationID = "" Then
                oCustomClass.ApplicationID = Me.ApplicationID.Trim
                oCustomClass.strConnection = GetConnectionString()
                oCustomClass = m_Controll.getAgreementGuarantor(oCustomClass)
            End If

            txtNamaGuarantor.Text = oCustomClass.GuarantorName
            txtJabatan.Text = oCustomClass.GuarantorJobTitle

            divAddress.Visible = False
            divHP.Visible = False
            divEmail.Visible = False
            divCatatan.Visible = False
            divPenghasilan.Visible = False

            UcCompanyAddress1.Visible = False
            txtNoHP.Visible = False
            txtEmail.Visible = False
            txtCatatan.Visible = False
            txtPenghasilan.Visible = False

            lblHandphone.Visible = False
            lblEmail.Visible = False
            lblCatatan.Visible = False
            lblPenghasilan.Visible = False



            Me.Name = oCustomClass.GuarantorName
            Me.Jabatan = oCustomClass.GuarantorJobTitle
            Me.Address = oCustomClass.GuarantorAddress
            Me.RT = oCustomClass.GuarantorRT
            Me.RW = oCustomClass.GuarantorRW
            Me.Kelurahan = oCustomClass.GuarantorKelurahan
            Me.Kecamatan = oCustomClass.GuarantorKecamatan
            Me.Kota = oCustomClass.GuarantorCity
            Me.KodePos = oCustomClass.GuarantorZipCode
            Me.Area1 = oCustomClass.GuarantorAreaPhone1
            Me.Phone1 = oCustomClass.GuarantorPhone1
            Me.Area2 = oCustomClass.GuarantorAreaPhone2
            Me.Phone2 = oCustomClass.GuarantorPhone2
            Me.AreaFax = oCustomClass.GuarantorAreaFax
            Me.Fax = oCustomClass.GuarantorFax
            Me.NoHP = oCustomClass.GuarantorMobilePhone
            Me.Email = oCustomClass.GuarantorEmail
            Me.Catatan = oCustomClass.GuarantorNotes
            Me.Penghasilan = oCustomClass.GuarantorPenghasilan
        Else
            txtNamaGuarantor.Text = Me.Name
            txtJabatan.Text = Me.Jabatan
            With UcCompanyAddress1
                .Address = Me.Address
                .RT = Me.RT
                .RW = Me.RW
                .Kelurahan = Me.Kelurahan
                .Kecamatan = Me.Kecamatan
                .City = Me.Kota
                .ZipCode = Me.KodePos
                .AreaPhone1 = Me.Area1
                .Phone1 = Me.Phone1
                .AreaPhone2 = Me.Area2
                .Phone2 = Me.Phone2
                .AreaFax = Me.AreaFax
                .Fax = Me.Fax
                .BindAddress()
                .ValidatorTrue()
                .showMandatoryAll()
            End With
            txtNoHP.Text = Me.NoHP
            txtEmail.Text = Me.Email
            txtCatatan.Text = Me.Catatan
            txtPenghasilan.Text = FormatNumber(Me.Penghasilan, 0)
        End If

    End Sub

    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            With UcCompanyAddress1
                .ValidatorFalse()
                .hideMandatoryAll()
            End With
        End If
    End Sub
End Class