﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UcBankAccountID.ascx.vb" Inherits="Maxiloan.Webform.UserController.UcBankAccountID" %>
<asp:dropdownlist id="cmbBankAccount"  runat="server"></asp:dropdownlist>
<asp:requiredfieldvalidator id="RequiredFieldValidator4" runat="server" initialvalue="0" display="Dynamic" errormessage="Harap Pilih Rekening Bank"
	controltovalidate="cmbBankAccount" CssClass ="validator_general"></asp:RequiredFieldValidator>