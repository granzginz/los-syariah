﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
#End Region

Public Class UcAgreementListNew
    Inherits ControlBased

#Region "Property"
    Public Property CustomerID() As String
        Get
            Return CType(ViewState("CustomerID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property
    Public Property Style() As String
        Get
            Return CStr(ViewState("style"))
        End Get
        Set(ByVal Value As String)
            ViewState("style") = Value
        End Set
    End Property
    Public Property ApplicationModule() As String
        Get
            Return CStr(ViewState("ApplicationModule"))
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationModule") = Value
        End Set
    End Property
#End Region

#Region "Agreement List Load"
    Private oController As New UcAgreementListController
    Private oCustomClass As New Parameter.AccMntBase

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    'sorting di comment dulu
    'Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
    '    If InStr(Me.SortBy, "DESC") > 0 Then
    '        Me.SortBy = e.SortExpression
    '    Else
    '        Me.SortBy = e.SortExpression + " DESC"
    '    End If
    '    BindAgreement()
    'End Sub

    Public Sub BindAgreement()
        Dim DvAgreementList As New DataView

        With oCustomClass
            .strConnection = GetConnectionString()
            .CustomerID = Me.CustomerID
            .SortBy = SortBy
        End With

        DvAgreementList = oController.UcAgreementList(oCustomClass).DefaultView
        DvAgreementList.Sort = Me.SortBy
        DtgAgreementList.DataSource = DvAgreementList
        DtgAgreementList.DataBind()

    End Sub
#End Region

    Function DtgRows() As Integer
        Return DtgAgreementList.Items.Count
    End Function
    Private Sub DtgAgreementList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgreementList.ItemDataBound
        Dim lnkAgreementNo As HyperLink
        Dim lnkApplicationID As HyperLink
        If e.Item.ItemIndex >= 0 Then
            lnkAgreementNo = CType(e.Item.FindControl("lnkAgreementNo"), HyperLink)
            lnkApplicationID = CType(e.Item.FindControl("lnkApplicationID"), HyperLink)

            If lnkAgreementNo.Text.Trim.Length > 0 Then
                lnkAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccAcq" & "', '" & Server.UrlEncode(lnkApplicationID.Text.Trim) & "')"
            End If

            If lnkApplicationID.Text.Trim.Length > 0 Then
                lnkApplicationID.NavigateUrl = "javascript:OpenApplicationId('" & "AccAcq" & "', '" & Server.UrlEncode(lnkApplicationID.Text.Trim) & "')"
            End If
        End If
    End Sub
End Class