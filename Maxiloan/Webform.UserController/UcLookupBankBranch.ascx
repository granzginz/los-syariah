﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UcLookupBankBranch.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.UcLookupBankBranch" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%--<style type="text/css">
    .wp
    {
        background-color: #fff;
        width: 700px;        
        border: 1px solid #ccc;
        float: left;
    }
    
    .wp h4
    {
        margin: 0;
        color: #000;
        text-align: left;
    }
    
    .wp .gridwp
    {
        max-height: 300px;
        overflow: auto;
    }
    
    .wpbg
    {
        background-color: #000;
        filter: alpha(opacity=70);
        opacity: 0.7;
    }
    
    .wpbtnexit
    {
        position: absolute;
        float: right;
        top: 0;
        right: 0;
    }
</style>--%>
<asp:UpdatePanel runat="server" ID="updatePanel1">
    <ContentTemplate>
        <asp:Button runat="server" ID="btnLookup" Text="btnLookup" Style="display: none;" CausesValidation="false" />
        <asp:ModalPopupExtender ID="ModalPopupExtender" runat="server" TargetControlID="btnLookup"
            PopupControlID="Panel1" BackgroundCssClass="wpbg" CancelControlID="btnExit">
        </asp:ModalPopupExtender>
        <asp:Panel ID="Panel1" runat="server">
            <div class="wp">
               <%-- <asp:Label ID="lblMessage" Visible="false" runat="server"></asp:Label>--%>
                <asp:ImageButton runat="server" ID="btnExit" ImageUrl="../Images/exit_lookup00.png"
                    CssClass="wpbtnexit" />
                <div class="form_box_title">
                    <div class="title_strip">
                    </div>
                    <div class="form_single">
                        <h4>
                            DAFTAR BANK
                        </h4>
                    </div>
                </div>
                <asp:Panel ID="pnlList" runat="server">
                    <asp:Panel ID="pnlSubList" runat="server" Visible="false">
                        <div class="form_box_header">
                            <div class="form_single">
                                <div class="grid_wrapper_ns">
                                    <asp:DataGrid ID="dtgPaging" runat="server" CellPadding="0" OnSortCommand="SortGrid"
                                        BorderStyle="None" DataKeyField="BankBranchID" AutoGenerateColumns="False" AllowSorting="True"
                                        AllowPaging="True" Width="100%" CssClass="grid_general">
                                        <ItemStyle CssClass="item_grid"></ItemStyle>
                                        <HeaderStyle CssClass="th"></HeaderStyle>
                                        <Columns>
                                            <asp:ButtonColumn Text="Select" CommandName="Select" ButtonType="LinkButton"></asp:ButtonColumn>
                                            <asp:BoundColumn ReadOnly="True" DataField="BankCode" SortExpression="BankCode" HeaderText="Bank Code">
                                            </asp:BoundColumn>
                                            <asp:BoundColumn ReadOnly="True" DataField="BankBranchName" SortExpression="BankBranchName"
                                                HeaderText="Bank Branch Name"></asp:BoundColumn>
                                            <asp:BoundColumn ReadOnly="True" DataField="Address" SortExpression="Address" HeaderText="Address">
                                            </asp:BoundColumn>
                                            <asp:BoundColumn ReadOnly="True" DataField="City" SortExpression="City" HeaderText="City">
                                            </asp:BoundColumn>
                                            <asp:BoundColumn ReadOnly="True" DataField="BankID" Visible="False"></asp:BoundColumn>
                                        </Columns>
                                        <PagerStyle Visible="False" HorizontalAlign="Left" ForeColor="#000066" BackColor="White"
                                            Mode="NumericPages"></PagerStyle>
                                    </asp:DataGrid>
                                </div>
                                <div class="button_gridnavigation">
                                    <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../Images/grid_navbutton01.png"
                                        CausesValidation="False" CommandName="First" OnCommand="NavigationLink_Click">
                                    </asp:ImageButton>
                                    <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../Images/grid_navbutton02.png"
                                        CausesValidation="False" CommandName="Prev" OnCommand="NavigationLink_Click">
                                    </asp:ImageButton>
                                    <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../Images/grid_navbutton03.png"
                                        CausesValidation="False" CommandName="Next" OnCommand="NavigationLink_Click">
                                    </asp:ImageButton>
                                    <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../Images/grid_navbutton04.png"
                                        CausesValidation="False" CommandName="Last" OnCommand="NavigationLink_Click">
                                    </asp:ImageButton>
                                    &nbsp;&nbsp;Page&nbsp;&nbsp;
                                    <asp:TextBox ID="txtGoPage" runat="server" Width="34px" CssClass="navigasi_txt">1</asp:TextBox>
                                    <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtGoPage"
                                        MinimumValue="1" MaximumValue="999" ErrorMessage="Page No. is not valid" Type="Double"
                                        Display="Dynamic"></asp:RangeValidator>
                                    <asp:Button ID="btnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                                        EnableViewState="False" />
                                    <asp:RangeValidator ID="Rangevalidator2" runat="server" Type="Integer" MaximumValue="999999999"
                                        Display="Dynamic" ErrorMessage="Page No. is not valid" MinimumValue="1" ControlToValidate="txtGoPage"></asp:RangeValidator>
                                    <asp:RequiredFieldValidator ID="rfvGo" runat="server" ErrorMessage="Page No. is not valid"
                                        ControlToValidate="txtGoPage" Display="Dynamic" CssClass ="validator_general"></asp:RequiredFieldValidator>
                                    <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtGoPage" MinimumValue="1"
                                        MaximumValue="999" ErrorMessage="Page No. is not valid" Type="Double"></asp:RangeValidator>
                                </div>
                                <div class="label_gridnavigation">
                                    <asp:Label ID="lblPage" runat="server"></asp:Label>of
                                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                                    <asp:Label ID="lblTotRec" runat="server"></asp:Label>record(s)
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <div class="form_box_title">
                        <div class="form_single">
                            <h4>
                                Find By Bank
                            </h4>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <asp:DropDownList ID="cboSearchBy" runat="server">
                                <asp:ListItem Value="BankCode">Bank Code</asp:ListItem>
                                <asp:ListItem Value="BankBranchName">Bank Branch Name</asp:ListItem>
                                <asp:ListItem Value="Address">Address</asp:ListItem>
                                <asp:ListItem Value="City">City</asp:ListItem>
                            </asp:DropDownList>
                            <asp:TextBox ID="txtSearchBy" runat="server"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form_button">
                        <asp:Button ID="btnSearch" runat="server" CausesValidation="False" Text="Find" CssClass="small button blue" />
                        <asp:Button ID="btnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray" />
                    </div>
                </asp:Panel>
            </div>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
