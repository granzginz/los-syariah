﻿Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.Controller

Public Class ucPaymentLocation
    Inherits ControlBased

#Region "Properti"
    Public Property BranchID() As String
        Get
            Return CType(ViewState("BranchID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("BranchID") = Value
        End Set
    End Property

    Public Property BranchName() As String
        Get
            Return CType(ViewState("BranchName"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("BranchName") = Value
        End Set
    End Property
#End Region
#Region " Private Const "
    Private m_controller As New DataUserControlController
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not IsPostBack Then
            Dim DtBranchName As New DataTable
            DtBranchName = CType(Me.Cache.Item(CACHE_BRANCH & Me.sesBranchName), DataTable)
            If DtBranchName Is Nothing Then
                Dim DtBranchNameCache As New DataTable
                DtBranchNameCache = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
                Me.Cache.Insert(CACHE_BRANCH & Me.sesBranchName, DtBranchNameCache, Nothing, DateTime.Now.AddHours(DEFAULT_TIME_DURATION_CACHE_HOUR), TimeSpan.Zero)
                DtBranchName = CType(Me.Cache.Item(CACHE_BRANCH & Me.sesBranchName), DataTable)
            End If

            cmbBranch.DataValueField = "ID"
            cmbBranch.DataTextField = "Name"

            cmbBranch.DataSource = DtBranchName
            cmbBranch.DataBind()
            cmbBranch.Items.Insert(0, "All")
            cmbBranch.Items(0).Value = "0"


            'Finding HO....
            Dim DtBranch As New DataTable
            DtBranch = m_controller.GetHOBranch(GetConnectionString)

            With cmbBranch
                Dim ID, Name As String
                If DtBranch.Rows.Count = 1 Then
                    ID = CType(DtBranch.Rows(0)("ID"), String)
                    Name = CType(DtBranch.Rows(0)("Name"), String)
                End If
                .Items.Insert(1, Name)
                .Items(1).Value = ID
            End With
        End If
        Me.BranchID = cmbBranch.SelectedItem.Value.Trim
        Me.BranchName = cmbBranch.SelectedItem.Text.Trim
    End Sub


End Class