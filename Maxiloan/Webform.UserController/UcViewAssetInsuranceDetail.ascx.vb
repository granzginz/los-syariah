﻿#Region "Imports"
Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Exceptions
#End Region

Public Class UcViewAssetInsuranceDetail
    Inherits ControlBased

#Region "Constanta'"


    Private oCustomClass As New Parameter.InsuranceAssetDetail

#End Region

#Region "Property"


#End Region

#Region "PageLoad"

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        


    End Sub

    Public Sub BindData()
        LblApplicationID.Text = CType(Me.Cache.Item(CACHE_VIEW_INSURANCE_DETAIL_PARAMETER_APPLICATION_ID), String)
        LblBranchID.Text = CType(Me.Cache.Item(CACHE_VIEW_INSURANCE_DETAIL_PARAMETER_BRANCH_ID), String)
        Dim debugstring As String
        Dim strCmdWhereData As String = ""

     

        'strCmdWhereData = " Where dbo.InsuranceAsset.BranchId = '" & Replace(LblBranchID.Text.Trim, "'", "") & "' And dbo.InsuranceAsset.ApplicationID = '" & Replace(LblApplicationID.Text.Trim, "'", "") & "' "
        strCmdWhereData = " Where  dbo.InsuranceAsset.ApplicationID = '" & Replace(LblApplicationID.Text.Trim, "'", "") & "' "

            Dim oController As New InsuranceAssetDetailController
            With oCustomClass
                .WhereCond = strCmdWhereData
                .PageSource = Maxiloan.General.CommonVariableHelper.PAGE_SOURCE_ASSET_INSURANCE_DETAIL_INFORMATION
                .strConnection = GetConnectionString()
            End With
            Context.Trace.Write("strCmdWhere = " + strCmdWhereData)
        oCustomClass = oController.GetViewInsuranceDetail(oCustomClass)

        Dim debugstringbuilder As New System.Text.StringBuilder

            debugstringbuilder.Append("..ApplicationID = " & LblApplicationID.Text)
            debugstringbuilder.Append("...BranchId = " & LblBranchID.Text)
            debugstringbuilder.Append("...strCmdWhere = " & strCmdWhereData)
            debugstringbuilder.Append("...PageSource = " & Maxiloan.General.CommonVariableHelper.PAGE_SOURCE_ASSET_INSURANCE_DETAIL_INFORMATION)
            debugstring = debugstringbuilder.ToString()

     


        If Not oCustomClass Is Nothing Then

            With oCustomClass
                lblAgreement.Text = .AgreementNo
                lblAsset.Text = CStr(.AssetseqNo)
                lblIns.Text = CStr(.InsseqNo)
                LblInsuranceCompany.Text = .InsuranceComBranchName
                LblInsuredByDescr.Text = .InsuredByDescr
                LblPaidByDescr.Text = .PaidByDescr
                LblSumInsured.Text = FormatNumber(.SumInsured, 2, , , )
                LblSPPANo.Text = .SPPANo
                lnkPolicyNo.Text = .PolicyNumber
                lnkPolicyNo.NavigateUrl = "javascript:OpenWinViewPolicyNo(" & Server.UrlEncode(Me.sesBranchId.Trim) & ",'" & lblAgreement.Text.Trim & " ','" & lblAsset.Text.Trim & "','" & lblIns.Text.Trim & "','" & LblApplicationID.Text.Trim & "')"

                Dim strstartdate As String = ConvertDate(CType(.StartDate, String))

                If .StartDate.ToString = "1/1/0001 12:00:00 AM" Or .StartDate.ToString = "" Then
                    lblStartDate.Text = ""
                Else
                    lblStartDate.Text = .StartDate.ToString("dd/MM/yyyy")
                End If
                If .EndDate.ToString = "1/1/0001 12:00:00 AM" Or .EndDate.ToString = "" Then
                    lblEndDate.Text = ""
                Else
                    lblEndDate.Text = .EndDate.ToString("dd/MM/yyyy")
                End If
                If .InsCoSelectionDate.ToString = "1/1/0001 12:00:00 AM" Or .InsCoSelectionDate.ToString = "" Then
                    LblInscoSelectionDate.Text = ""
                Else
                    LblInscoSelectionDate.Text = .InsCoSelectionDate.ToString("dd/MM/yyyy")

                End If
                If .SPPADate.ToString = "1/1/0001 12:00:00 AM" Or .SPPADate.ToString = "" Then
                    LblSPPADate.Text = ""
                Else
                    LblSPPADate.Text = .SPPADate.ToString("dd/MM/yyyy")
                End If
                If .InsActivateDate.ToString = "1/1/0001 12:00:00 AM" Or .InsActivateDate.ToString = "" Then
                    lblActivationDate.Text = ""
                Else
                    lblActivationDate.Text = .InsActivateDate.ToString("dd/MM/yyyy")
                End If
                If .PolicyReceiveDate.ToString = "1/1/0001 12:00:00 AM" Or .PolicyReceiveDate.ToString = "" Then
                    lblPolicyReceivedDate.Text = ""
                Else
                    lblPolicyReceivedDate.Text = .PolicyReceiveDate.ToString("dd/MM/yyyy")
                End If
                lblRefundToSupplier.Text = CType(.PremiumBaseForRefundSupp, String)
                lblAccNotes.Text = .AccNotes
                lblInsNotes.Text = .InsNotes

                lblPremiUtamaCus.Text = FormatNumber((.MainPremiumToCust - .SRCCToCust - .TPLToCust - .FloodToCust - .PAToCust - .EQVETToCust - .TERRORISMToCust - .PADriverToCust), 2, , , )
                lblPremiSRCCCus.Text = FormatNumber(.SRCCToCust, 2, , , )
                lblPremiPTLCus.Text = FormatNumber(.TPLToCust, 2, , , )
                lblPremiFloodCus.Text = FormatNumber(.FloodToCust, 2, , , )
                lblPremiPACus.Text = FormatNumber(.PAToCust, 2, , , )
                lblPremiEQVETCus.Text = FormatNumber(.EQVETToCust, 2, , , )
                lblPremiTerorismeCus.Text = FormatNumber(.TERRORISMToCust, 2, , , )
                lblPremiPADriverCus.Text = FormatNumber(.PADriverToCust, 2, , , )

                lblTotalPremiCus.Text = FormatNumber(.MainPremiumToCust, 2, , , )


                Dim nDec As Decimal

                If CDec(lblTotalPremiCus.Text) = 0 Then
                    nDec = CDec(1)
                Else
                    nDec = CDec(lblTotalPremiCus.Text.Trim)
                End If

                'lblIndicationRefundPercentage.Text = CStr((CDec(lblRefundToSupplier.Text.Trim) / CDec(lblTotalPremium.Text.Trim)) * 100%)
                lblIndicationRefundPercentage.Text = CStr((CDec(lblRefundToSupplier.Text.Trim) / nDec) * 100%)

                lblPremiUtamaCom.Text = FormatNumber(.MainPremiumToInsco, 2, , , )
                lblPremiSRCCCom.Text = FormatNumber(.RSCCToInsco, 2, , , )
                lblPremiFloodCom.Text = FormatNumber(.FloodToInsco, 2, , , )
                lblPremiPTLCom.Text = FormatNumber(.TPLToInsco, 2, , , )
                lblBiayaAdminCom.Text = FormatNumber(.AdminFee, 2, , , )
                lblBiayaMateraiCom.Text = FormatNumber(.MeteraiFee, 2, , , )
                lblPremiPACom.Text = FormatNumber(.PAToInsco, 2, , , )
                lblPremiEQVETCom.Text = FormatNumber(.EQVETToInsco, 2, , , )
                lblPremiTerorismeCom.Text = FormatNumber(.TERRORISMToInsco, 2, , , )
                lblPremiPADriverCom.Text = FormatNumber(.PADriverToInsco, 2, , , )

                lblTotalPremiCom.Text = FormatNumber((.MainPremiumToInsco + .RSCCToInsco + .FloodToInsco + .TPLToInsco + .AdminFee + .MeteraiFee + .PAToInsco + .EQVETToInsco + .TERRORISMToInsco + .PADriverToInsco), 2, , , )


                LblInvoiceNo.Text = .InvoiceNo
                If .InvoiceDate.ToString = "1/1/0001 12:00:00 AM" Or .InvoiceDate.ToString = "" Then
                    LblInvoiceDate.Text = ""
                Else
                    LblInvoiceDate.Text = .InvoiceDate.ToString("dd/MM/yyyy")
                End If
                lblTotalRatePremi.Text = FormatNumber(.TotalRate, 6, , , )
                lblTotalRatePremiCP.Text = FormatNumber(.TotalPremiCP, 2, , , )
                lblTotalRatePremiJK.Text = FormatNumber(.TotalPremiJK, 2, , , )
            End With
        End If
    End Sub
#End Region

End Class