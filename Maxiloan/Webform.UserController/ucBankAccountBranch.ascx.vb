﻿Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.Controller

Public Class ucBankAccountBranch
    Inherits ControlBased

#Region " Private Const "
    Private m_controller As New DataUserControlController
#End Region
#Region "Property"


    Public Property BankAccountName() As String
        Get
            Return (CType(cmbBank.SelectedItem.Text, String))
        End Get
        Set(ByVal Value As String)
            cmbBank.SelectedIndex = cmbBank.Items.IndexOf(cmbBank.Items.FindByText(Value))
        End Set
    End Property


    Public Property BankAccountID() As String
        Get
            Return (CType(cmbBank.SelectedItem.Value, String))
        End Get
        Set(ByVal Value As String)
            cmbBank.SelectedIndex = cmbBank.Items.IndexOf(cmbBank.Items.FindByValue(Value))
        End Set
    End Property
    Public Property isPostbackcmd() As Boolean
        Get
            Return cmbBank.AutoPostBack
        End Get
        Set(value As Boolean)
            cmbBank.AutoPostBack = value
        End Set
    End Property
#End Region

    Public Delegate Sub cmbbankhandeler(ByVal cmbbankValue As String)
    Public Event cmbbankselected As cmbbankhandeler



    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not IsPostBack Then

        End If
    End Sub

    Public Sub BindBankAccountBranch(ByVal strBranch As String)
        cmbBank.Items.Clear()
        Dim DtBankAccountbranch As New DataTable
        Dim dvBankAccountbranch As New DataView
        DtBankAccountbranch = m_controller.GetBankAccountbranch(GetConnectionString, strBranch)
        dvBankAccountbranch = DtBankAccountbranch.DefaultView
        With cmbBank
            .DataValueField = "ID"
            .DataTextField = "Name"
            .DataSource = dvBankAccountbranch
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = "0"
        End With
    End Sub

    Public Sub BindBankHOTransfer(ByVal BankAccId As String)
        'GetBankAccountHOTransfer
        Dim cImp As New ImplementasiControler
        cmbBank.Items.Clear()
        Dim DtBankAccountbranch As New DataTable
        Dim dvBankAccountbranch As New DataView
        DtBankAccountbranch = cImp.GetBankAccountHOTransfer(GetConnectionString, BankAccId)
        dvBankAccountbranch = DtBankAccountbranch.DefaultView
        With cmbBank
            .DataValueField = "ID"
            .DataTextField = "Name"
            .DataSource = dvBankAccountbranch
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = "0"
        End With
    End Sub

    Private Sub cmbBank_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbBank.SelectedIndexChanged
        RaiseEvent cmbbankselected(cmbBank.SelectedValue)
    End Sub
End Class