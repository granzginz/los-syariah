
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Public Class AccMntControlBased : Inherits ControlBased
#Region "Property"
    Public Property ApplicationID() As String
        Get
            Return CType(Viewstate("applicationid"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("applicationid") = Value
        End Set
    End Property

    Public Property AgreementNo() As String
        Get
            Return CType(Viewstate("AgreementNo"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AgreementNo") = Value
        End Set
    End Property

    Public Property CustomerID() As String
        Get
            Return CType(Viewstate("CustomerID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerID") = Value
        End Set
    End Property

    Public Property Prepaid() As Double
        Get
            Return CType(Viewstate("Prepaid"), Double)
        End Get
        Set(ByVal Value As Double)
            viewstate("Prepaid") = Value
        End Set
    End Property

#Region "Payment Info"
    Public Property InstallmentDue() As Double
        Get
            Return CType(Viewstate("InstallmentDue"), Double)
        End Get
        Set(ByVal Value As Double)
            viewstate("InstallmentDue") = Value
        End Set
    End Property

    Public Property InsuranceDue() As Double
        Get
            Return CType(Viewstate("InsuranceDue"), Double)
        End Get
        Set(ByVal Value As Double)
            viewstate("InsuranceDue") = Value
        End Set
    End Property

    Public Property InstallLC() As Double
        Get
            Return CType(Viewstate("InstallLC"), Double)
        End Get
        Set(ByVal Value As Double)
            viewstate("InstallLC") = Value
        End Set
    End Property

    Public Property InsuranceLC() As Double
        Get
            Return CType(Viewstate("InsuranceLC"), Double)
        End Get
        Set(ByVal Value As Double)
            viewstate("InsuranceLC") = Value
        End Set
    End Property

    Public Property InstallCollFee() As Double
        Get
            Return CType(Viewstate("InstallCollFee"), Double)
        End Get
        Set(ByVal Value As Double)
            viewstate("InstallCollFee") = Value
        End Set
    End Property

    Public Property InsuranceCollFee() As Double
        Get
            Return CType(Viewstate("InsuranceCollFee"), Double)
        End Get
        Set(ByVal Value As Double)
            viewstate("InsuranceCollFee") = Value
        End Set
    End Property

    Public Property PDCBounceFee() As Double
        Get
            Return CType(Viewstate("PDCBounceFee"), Double)
        End Get
        Set(ByVal Value As Double)
            viewstate("PDCBounceFee") = Value
        End Set
    End Property

    Public Property STNKRenewalFee() As Double
        Get
            Return CType(Viewstate("STNKRenewalFee"), Double)
        End Get
        Set(ByVal Value As Double)
            viewstate("STNKRenewalFee") = Value
        End Set
    End Property

    Public Property InsuranceClaim() As Double
        Get
            Return CType(Viewstate("InsuranceClaim"), Double)
        End Get
        Set(ByVal Value As Double)
            viewstate("InsuranceClaim") = Value
        End Set
    End Property

    Public Property ReposessionFee() As Double
        Get
            Return CType(Viewstate("ReposessionFee"), Double)
        End Get
        Set(ByVal Value As Double)
            viewstate("ReposessionFee") = Value
        End Set
    End Property

    Public Property TotalOSOverDue() As Double
        Get
            Return CType(Viewstate("TotalOSOverDue"), Double)
        End Get
        Set(ByVal Value As Double)
            viewstate("TotalOSOverDue") = Value
        End Set
    End Property

    Public Property ValueDate() As Date
        Get
            Return CType(Viewstate("ValueDate"), Date)
        End Get
        Set(ByVal Value As Date)
            viewstate("ValueDate") = Value
        End Set
    End Property

    Public Property InvoiceNo() As String
        Get
            Return CType(ViewState("InvoiceNo"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("InvoiceNo") = Value
        End Set
    End Property
    Public Property InvoiceSeqNo() As Int64
        Get
            Return CType(ViewState("InvoiceSeqNo"), Int64)
        End Get
        Set(ByVal Value As Int64)
            ViewState("InvoiceSeqNo") = Value
        End Set
    End Property

    Public Property RequestNo() As String
        Get
            Return CType(ViewState("RequestNo"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("RequestNo") = Value
        End Set
    End Property
    Public Property ApplicationModule() As String
        Get
            Return CType(ViewState("ApplicationModule"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationModule") = Value
        End Set
    End Property

#End Region

#End Region

#Region "Approval"
    Public Function Get_UserApproval(ByVal SchemeID As String, _
                                     ByVal BranchID As String, _
                                     Optional ByVal Amount As Double = 0) As DataTable
        Dim oController As New ApprovalController
        Dim oCustomClass As New Parameter.Approval
        With oCustomClass
            .strConnection = GetConnectionString()
            .SchemeID = SchemeID
            .BranchId = BranchID
            .AmountApproval = Amount
        End With
        Return oController.GetUserApproval(oCustomClass)
    End Function
#End Region

End Class
