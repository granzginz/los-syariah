﻿Imports Maxiloan.Controller
Public Class UcApplicationType
    Inherits ControlBased

#Region "Private Const"
    Private m_controller As New DataUserControlController
    Private Const CACHE_APPLICATION_TYPE As String = "CacheApplicationType"
#End Region

#Region "Property"
    Public Property AppTypeID() As String
        Get
            Return CType(viewstate("AppTypeID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AppTypeID") = Value
        End Set
    End Property

    Public Property AppTypeName() As String
        Get
            Return CType(viewstate("AppTypeName"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AppTypeName") = Value
        End Set
    End Property

    Public Property SupplierGroupID() As String
        Get
            Return CType(viewstate("SupplierGroupID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("SupplierGroupID") = Value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not IsPostBack Then
            'Dim DtApplicationType As New DataTable
            'DtApplicationType = CType(Me.Cache.Item(CACHE_APPLICATION_TYPE), DataTable)
            'If DtApplicationType Is Nothing Then
            '    Dim DtAppTypeCache As New DataTable
            '    DtAppTypeCache = m_controller.GetApplicationType(GetConnectionString)
            '    Me.Cache.Insert(CACHE_APPLICATION_TYPE, DtAppTypeCache, Nothing, DateTime.Now.AddHours(10), TimeSpan.Zero)
            '    DtApplicationType = CType(Me.Cache.Item(CACHE_APPLICATION_TYPE), DataTable)
            'End If


            'Dim DtApplicationType As New DataTable
            'DtApplicationType = CType(Me.Cache.Item(CACHE_APPLICATION_TYPE), DataTable)
            'If DtApplicationType Is Nothing Then
            '    Dim DtAppTypeCache As New DataTable
            '    DtAppTypeCache = m_controller.GetApplicationType(GetConnectionString)
            '    Me.Cache.Insert(CACHE_APPLICATION_TYPE, DtAppTypeCache, Nothing, DateTime.Now.AddHours(10), TimeSpan.Zero)
            '    DtApplicationType = CType(Me.Cache.Item(CACHE_APPLICATION_TYPE), DataTable)
            'End If


            CmpApplicationType.DataValueField = "ID"
            CmpApplicationType.DataTextField = "Name"

            CmpApplicationType.DataSource = m_controller.GetApplicationType(GetConnectionString)
            CmpApplicationType.DataBind()

        End If
        Me.AppTypeID = CmpApplicationType.SelectedItem.Value.Trim
        Me.AppTypeName = CmpApplicationType.SelectedItem.Text.Trim

        If Not IsNothing(Me.SupplierGroupID) Then
            If Me.SupplierGroupID.Trim <> "" Then
                If Me.SupplierGroupID = "G" Or Me.SupplierGroupID = "A" Then
                    CmpApplicationType.Items.Remove(CmpApplicationType.Items.FindByValue("NG"))
                ElseIf Me.SupplierGroupID = "N" Then
                    CmpApplicationType.Items.Remove(CmpApplicationType.Items.FindByValue("G"))
                End If
            End If
        End If
    End Sub
End Class