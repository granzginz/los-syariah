﻿Public Class UcCompanyAddress
    Inherits System.Web.UI.UserControl
#Region "Property"
    Public Property Address() As String
        Get
            Return CType(viewstate("address"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("address") = Value
        End Set
    End Property
    Public Property RT() As String
        Get
            Return CType(viewstate("RT"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("RT") = Value
        End Set
    End Property
    Public Property RW() As String
        Get
            Return CType(viewstate("RW"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("RW") = Value
        End Set
    End Property
    Public Property Kecamatan() As String
        Get
            Return CType(viewstate("Kecamatan"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Kecamatan") = Value
        End Set
    End Property
    Public Property Kelurahan() As String
        Get
            Return CType(viewstate("Kelurahan"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Kelurahan") = Value
        End Set
    End Property
    Public Property City() As String
        Get
            Return CType(viewstate("City"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("City") = Value
        End Set
    End Property
    Public Property ZipCode() As String
        Get
            Return CType(viewstate("ZipCode"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ZipCode") = Value
        End Set
    End Property
    Public Property AreaPhone1() As String
        Get
            Return CType(viewstate("AreaPhone1"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AreaPhone1") = Value
        End Set
    End Property
    Public Property AreaPhone2() As String
        Get
            Return CType(viewstate("AreaPhone2"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AreaPhone2") = Value
        End Set
    End Property
    Public Property Phone1() As String
        Get
            Return CType(viewstate("Phone1"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Phone1") = Value
        End Set
    End Property
    Public Property Phone2() As String
        Get
            Return CType(viewstate("Phone2"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Phone2") = Value
        End Set
    End Property
    Public Property AreaFax() As String
        Get
            Return CType(viewstate("AreaFax"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AreaFax") = Value
        End Set
    End Property
    Public Property Fax() As String
        Get
            Return CType(viewstate("Fax"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Fax") = Value
        End Set
    End Property
    Public Property Style() As String
        Get
            Return CType(viewstate("Style"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Style") = Value
        End Set
    End Property
    Public WriteOnly Property IsRequiredZipcode As Boolean
        Set(ByVal value As Boolean)
            oLookUpZipCode.IsRequired = value
        End Set
    End Property

    Public Property IsRequiredCompanyAddress As Boolean
        Get
            Return CBool(ViewState("Validation"))
        End Get
        Set(ByVal Value As Boolean)
            ViewState("Validation") = Value
        End Set
    End Property
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not IsPostBack Then
            Me.Address = txtAddress.Text
            Me.RT = txtRT.Text
            Me.RW = txtRW.Text
            Me.Kelurahan = oLookUpZipCode.Kelurahan
            Me.Kecamatan = oLookUpZipCode.Kecamatan
            Me.ZipCode = oLookUpZipCode.ZipCode
            Me.City = oLookUpZipCode.City
            Me.AreaPhone1 = txtAreaPhone1.Text
            Me.AreaPhone2 = txtAreaPhone2.Text
            Me.Phone1 = txtPhone1.Text
            Me.Phone2 = txtPhone2.Text
            Me.AreaFax = txtAreaFax.Text
            Me.Fax = txtFax.Text
        End If
        Me.Address = txtAddress.Text
        Me.RT = txtRT.Text
        Me.RW = txtRW.Text
        Me.Kelurahan = oLookUpZipCode.Kelurahan
        Me.Kecamatan = oLookUpZipCode.Kecamatan
        Me.ZipCode = oLookUpZipCode.ZipCode
        Me.City = oLookUpZipCode.City
        Me.AreaPhone1 = txtAreaPhone1.Text
        Me.AreaPhone2 = txtAreaPhone2.Text
        Me.Phone1 = txtPhone1.Text
        Me.Phone2 = txtPhone2.Text
        Me.AreaFax = txtAreaFax.Text
        Me.Fax = txtFax.Text
    End Sub

    Public Sub showMandatoryAll()
        'bintang1.Visible = True
        'bintang2.Visible = True
        lblAlamat.Attributes("class") = "label_split_req"
        lblTelepon1.Attributes("class") = "label_split_req"
    End Sub

    Public Sub hideMandatoryAll()
        'bintang1.Visible = True
        'bintang2.Visible = True
        lblAlamat.Attributes("class") = "label_split"
        lblTelepon1.Attributes("class") = "label_split"
    End Sub

    Public Sub ValidatorFalse()
        RequiredFieldValidator1.Enabled = False
        RequiredFieldValidator3.Enabled = False
        RequiredFieldValidator2.Enabled = False

        lblAlamat.Attributes("class") = "label_split"
        lblTelepon1.Attributes("class") = "label_split"

        oLookUpZipCode.ValidatorFalse()
    End Sub

    Public Sub Phone1ValidatorEnabled(ByVal status As Boolean)
        If status = True Then
            'RequiredFieldValidator2.Enabled = True
            'RequiredFieldValidator3.Enabled = True
            RequiredFieldValidator2.Enabled = True
            RequiredFieldValidator3.Enabled = True
            lblTelepon1.Attributes("class") = "label_split_req"
        Else
            'RequiredFieldValidator2.Enabled = False
            'RequiredFieldValidator3.Enabled = False
            lblTelepon1.Attributes("class") = "label_split"
        End If

    End Sub
    Public Sub ValidatorTrue()
        RequiredFieldValidator1.Enabled = True
        'RequiredFieldValidator2.Enabled = True
        'RequiredFieldValidator3.Enabled = True
        RequiredFieldValidator2.Enabled = True
        RequiredFieldValidator3.Enabled = True


        lblAlamat.Attributes("class") = "label_split_req"
        lblTelepon1.Attributes("class") = "label_split_req"

        revRT.Enabled = True
        revRW.Enabled = True
        oLookUpZipCode.ValidatorTrue()
    End Sub
    Public Sub ValidatorDefault()
        RequiredFieldValidator1.Enabled = True
        'RequiredFieldValidator2.Enabled = True
        'RequiredFieldValidator3.Enabled = True
        RequiredFieldValidator2.Enabled = True
        RequiredFieldValidator3.Enabled = True

        lblAlamat.Attributes("class") = "label_split_req"
        lblTelepon1.Attributes("class") = "label_split_req"

        revRT.Enabled = True
        revRW.Enabled = True
        oLookUpZipCode.ValidatorTrue()
    End Sub
    Public Sub BindAddress()
        txtAddress.Text = Me.Address
        txtRT.Text = Me.RT
        txtRW.Text = Me.RW
        oLookUpZipCode.ZipCode = Me.ZipCode
        oLookUpZipCode.Kecamatan = Me.Kecamatan
        oLookUpZipCode.Kelurahan = Me.Kelurahan
        oLookUpZipCode.City = Me.City
        oLookUpZipCode.BindData()
        txtAreaPhone1.Text = Me.AreaPhone1
        txtAreaPhone2.Text = Me.AreaPhone2
        txtPhone1.Text = Me.Phone1
        txtPhone2.Text = Me.Phone2
        txtAreaFax.Text = Me.AreaFax
        txtFax.Text = Me.Fax
    End Sub
    Public Sub BorderNone(ByVal Read As Boolean, ByVal Style As BorderStyle)
        txtAddress.ReadOnly = Read
        txtAddress.BorderStyle = Style
        txtRT.ReadOnly = Read
        txtRT.BorderStyle = Style
        txtRW.ReadOnly = Read
        txtRW.BorderStyle = Style
        txtAreaPhone1.ReadOnly = Read
        txtAreaPhone1.BorderStyle = Style
        txtAreaPhone2.ReadOnly = Read
        txtAreaPhone2.BorderStyle = Style
        txtPhone1.ReadOnly = Read
        txtPhone1.BorderStyle = Style
        txtPhone2.ReadOnly = Read
        txtPhone2.BorderStyle = Style
        txtAreaFax.ReadOnly = Read
        txtAreaFax.BorderStyle = Style
        txtFax.ReadOnly = Read
        txtFax.BorderStyle = Style
        'oLookUpZipCode.LookUpBorder(Read, Style)
        If Style = BorderStyle.None Then
            lblMiring.Visible = False
            lblStrip1.Visible = False
            lblStrip2.Visible = False
            lblStrip3.Visible = False
            txtAddress.TextMode = TextBoxMode.SingleLine
        Else
            lblMiring.Visible = True
            lblStrip1.Visible = True
            lblStrip2.Visible = True
            lblStrip3.Visible = True
            txtAddress.TextMode = TextBoxMode.MultiLine
        End If
    End Sub

    Public Sub BPKBView()
        pnlTelepon.Visible = False
    End Sub
End Class