﻿Public Class ucAddressCity
    Inherits System.Web.UI.UserControl

#Region "Property"
    Public Property Address() As String
        Get
            Return CType(ViewState("address"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("address") = Value
        End Set
    End Property

    Public Property Kecamatan() As String
        Get
            Return CType(ViewState("Kecamatan"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Kecamatan") = Value
        End Set
    End Property
    Public Property Kelurahan() As String
        Get
            Return CType(ViewState("Kelurahan"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Kelurahan") = Value
        End Set
    End Property
    Public Property RT() As String
        Get
            Return CType(ViewState("RT"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("RT") = Value
        End Set
    End Property
    Public Property RW() As String
        Get
            Return CType(ViewState("RW"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("RW") = Value
        End Set
    End Property
    Public Property City() As String
        Get
            Return CType(ViewState("City"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("City") = Value
        End Set
    End Property
    Public Property CityCode() As String
        Get
            Return CType(ViewState("CityCode"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CityCode") = Value
        End Set
    End Property
    Public Property ZipCode() As String
        Get
            Return CType(ViewState("ZipCode"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ZipCode") = Value
        End Set
    End Property
    Public Property AreaPhone1() As String
        Get
            Return CType(ViewState("AreaPhone1"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("AreaPhone1") = Value
        End Set
    End Property
    Public Property AreaPhone2() As String
        Get
            Return CType(ViewState("AreaPhone2"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("AreaPhone2") = Value
        End Set
    End Property
    Public Property Phone1() As String
        Get
            Return CType(ViewState("Phone1"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Phone1") = Value
        End Set
    End Property
    Public Property Phone2() As String
        Get
            Return CType(ViewState("Phone2"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Phone2") = Value
        End Set
    End Property
    Public Property AreaFax() As String
        Get
            Return CType(ViewState("AreaFax"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("AreaFax") = Value
        End Set
    End Property
    Public Property Fax() As String
        Get
            Return CType(ViewState("Fax"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Fax") = Value
        End Set
    End Property
    Public Property HP() As String
        Get
            Return CType(ViewState("HP"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("HP") = Value
        End Set
    End Property
    Public Property Style() As String
        Get
            Return CType(ViewState("Style"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Style") = Value
        End Set
    End Property
    Public Property HPEnabled() As Boolean
        Get
            Return CType(ViewState("HPEnabled"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            ViewState("HPEnabled") = Value
        End Set
    End Property
 
#End Region

    Protected WithEvents ucLookupKabupaten1 As ucLookupKabupaten

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        txtKotaKabupaten.Attributes.Add("readonly", "readonly")
        If Not IsPostBack Then
            'Me.Address = txtAddress.Text
            Me.ZipCode = txtZipCode.Text
            '    Me.City = txtKotaKabupaten.Text
            '    Me.CityCode = hdfKodeKotaKabupaten.Value
            '    Me.AreaPhone1 = txtAreaPhone1.Text
            '    Me.AreaPhone2 = txtAreaPhone2.Text
            '    Me.Phone1 = txtPhone1.Text
            '    Me.Phone2 = txtPhone2.Text
            '    Me.AreaFax = txtAreaFax.Text
            '    Me.Fax = txtFax.Text
            'Else
        End If
        Me.ZipCode = txtZipCode.Text
        Me.Address = txtAddress.Text
        Me.City = txtKotaKabupaten.Text
        Me.CityCode = hdfKodeKotaKabupaten.Value
        Me.AreaPhone1 = txtAreaPhone1.Text
        Me.AreaPhone2 = txtAreaPhone2.Text
        Me.Phone1 = txtPhone1.Text
        Me.Phone2 = txtPhone2.Text
        Me.AreaFax = txtAreaFax.Text
        Me.Fax = txtFax.Text
        'End If
        Me.HP = txtHPhone.Text

        reqHP.Enabled = HPEnabled
    End Sub

    Public Sub showMandatoryAll() 
       ValidatorTrue()
    End Sub

    Public Sub hideMandatoryAll()
        ValidatorFalse()
    End Sub

    Public Sub ValidatorFalse()
        lblAlamat.Attributes("class") = "label_split"
        lblTelepon1.Attributes("class") = "label_split"
        RequiredFieldValidator1.Enabled = False
        'RequiredFieldValidator2.Enabled = False
        'RequiredFieldValidator3.Enabled = False

    End Sub

    Public Sub Phone1ValidatorEnabled(ByVal status As Boolean)
        If status = True Then
            lblTelepon1.Attributes("class") = "label_split_req"
            RequiredFieldValidator2.Enabled = True
            RequiredFieldValidator3.Enabled = True
        Else
            lblTelepon1.Attributes("class") = "label_split"
            RequiredFieldValidator2.Enabled = False
            RequiredFieldValidator3.Enabled = False
        End If
    End Sub

    Public Sub SetHPValidator(ByVal status As Boolean)
        lblHPNo.Attributes("class") = IIf(status = True, "label_split_req", "label_split").ToString()
        reqHP.Enabled = status
        HPEnabled = status
    End Sub

    Public Sub ValidatorTrue2()
        lblAlamat.Attributes("class") = "label_split_req"
        RequiredFieldValidator1.Enabled = True
    End Sub

    'modify by ario 3feb2018
    Public Sub ValidatorTrue()
        lblAlamat.Attributes("class") = "label_split_req"
        lblTelepon1.Attributes("class") = "label_split"
        RequiredFieldValidator1.Enabled = False
        RequiredFieldValidator2.Enabled = False
        RequiredFieldValidator3.Enabled = False
        'Regularexpressionvalidator1.Enabled = False
        'Regularexpressionvalidator2.Enabled = False
        'revRT.Enabled = True
        'revRW.Enabled = True
        'oPopupZipCode.ValidatorTrue()
    End Sub
    Public Sub ValidatorDefault()
        ValidatorTrue()
    End Sub
    Public Sub BindAddress()
        txtAddress.Text = Me.Address
        'txtAddress.Text = Me.Address + " " + Me.RT + " " + Me.RW + " " + Me.Kelurahan + " " + Me.Kecamatan
        txtZipCode.Text = Me.ZipCode
        txtKotaKabupaten.Text = Me.City
        txtAreaPhone1.Text = Me.AreaPhone1
        txtAreaPhone2.Text = Me.AreaPhone2
        txtPhone1.Text = Me.Phone1
        txtPhone2.Text = Me.Phone2
        txtAreaFax.Text = Me.AreaFax
        txtFax.Text = Me.Fax
        txtHPhone.Text = Me.HP
    End Sub
    Public Sub BorderNone(ByVal Read As Boolean, ByVal Style As BorderStyle)
        txtAddress.ReadOnly = Read
        txtAddress.BorderStyle = Style
        'txtRT.ReadOnly = Read
        'txtRT.BorderStyle = Style
        'txtRW.ReadOnly = Read
        'txtRW.BorderStyle = Style
        txtAreaPhone1.ReadOnly = Read
        txtAreaPhone1.BorderStyle = Style
        txtAreaPhone2.ReadOnly = Read
        txtAreaPhone2.BorderStyle = Style
        txtPhone1.ReadOnly = Read
        txtPhone1.BorderStyle = Style
        txtPhone2.ReadOnly = Read
        txtPhone2.BorderStyle = Style
        txtAreaFax.ReadOnly = Read
        txtAreaFax.BorderStyle = Style
        txtFax.ReadOnly = Read
        txtFax.BorderStyle = Style
        txtHPhone.ReadOnly = Read
        txtHPhone.BorderStyle = Style
        txtZipCode.ReadOnly = Read
        txtZipCode.BorderStyle = Style
        'oPopupZipCode.LookUpBorder(Read, Style)
        If Style = BorderStyle.None Then
            'lblMiring.Visible = False
            lblStrip1.Visible = False
            lblStrip2.Visible = False
            lblStrip3.Visible = False
            txtAddress.TextMode = TextBoxMode.SingleLine
        Else
            'lblMiring.Visible = True
            lblStrip1.Visible = True
            lblStrip2.Visible = True
            lblStrip3.Visible = True
            txtAddress.TextMode = TextBoxMode.MultiLine
        End If
    End Sub

    Public Sub TeleponFalse()
        pnlTelepon.Visible = False
    End Sub

    Public Sub KodePosFalse()
        DivKdPos.Visible = False
    End Sub

    Public Sub DivKdPosHide()
        DivKdPos.Attributes("style") = "display: none;"
    End Sub

    Public Sub DivHPHide()
        divHp.Attributes("style") = "display: none;"
    End Sub
End Class