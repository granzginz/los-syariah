﻿Imports Maxiloan.Controller
Imports System.Data.SqlClient

Public Class ucViewApplicationMK
    Inherits ControlBased

    Public Property CustomerID As String
    Public Property Style As String
    Public Property CustomerName As String
    Public Property ApprovalNo As String
    Public Property ApplicationID As String
    Public Property AgreementNo As String
    Public Property AssetTypeID As String
    Public Property ProductOfferingID As String
    Public Property ProductID As String
    Public Property InterestType As String
    Public Property InstallmentScheme As String
    Public Property BranchID As String
    Public Property SupplierID As String
    Public Property CustomerType As String
    Public Property Origination As String
    Public Property PolaTransaksi As String
    Public Property TanggalAplikasi As String
        Get
            Return CType(ViewState("TanggalAplikasi"), String)
        End Get
        Set(ByVal value As String)
            ViewState("TanggalAplikasi") = value
        End Set
    End Property
    Public Property IsAppTimeLimitReached As Boolean
    Public Property AppTimeLimit As Integer
    Public Property DPPercentage As Integer
        Get
            Return CType(ViewState("DPPercentage"), Integer)
        End Get
        Set(value As Integer)
            ViewState("DPPercentage") = value
        End Set
    End Property

    Public Property DownPayment As Double
        Get
            Return CType(ViewState("DownPayment"), Double)
        End Get
        Set(value As Double)
            ViewState("DownPayment") = value
        End Set
    End Property
    Public Property KondisiAsset As String
        Get
            Return CType(ViewState("KondisiAsset"), String)
        End Get
        Set(value As String)
            ViewState("KondisiAsset") = value
        End Set
    End Property
    Public Property isPAAvailable As Boolean
        Get
            Return CType(ViewState("isPAAvailable"), Boolean)
        End Get
        Set(value As Boolean)
            ViewState("isPAAvailable") = value
        End Set
    End Property
    Public Property PaketProgram As String
        Get
            Return CType(ViewState("PaketProgram"), String)
        End Get
        Set(value As String)
            ViewState("PaketProgram") = value
        End Set
    End Property
    Public Property isProceed As Boolean
        Set(value As Boolean)
            ViewState("isProceed") = value
        End Set
        Get
            Return CType(ViewState("isProceed"), Boolean)
        End Get
    End Property
    Public Property SupplierName As String
    Public Property desProduct As String
    Public Property AssetUsage As String
        Get
            Return CType(ViewState("AssetUsage"), String)
        End Get
        Set(value As String)
            ViewState("AssetUsage") = value
        End Set
    End Property
    Property OTR() As Double
        Get
            Return CType(ViewState("OTR"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("OTR") = Value
        End Set
    End Property
    Public Property ManufacturingYear() As String
        Get
            Return ViewState("ManufacturingYear").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ManufacturingYear") = Value
        End Set
    End Property
    Public Property NoFasilitas() As String
        Get
            Return ViewState("NoFasilitas").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("NoFasilitas") = Value
        End Set
    End Property
#Region "LinkTo"
    Function LinkTo(ByVal strApplicationID As String, ByVal strStyle As String) As String
        Return "javascript:OpenWinApplication('" & strApplicationID & "','" & strStyle & "')"
    End Function
    Function LinkToSupplier(ByVal strStyle As String, ByVal strSupplierID As String) As String
        Return "javascript:OpenWinSupplier('" & strStyle & "','" & strSupplierID & "')"
    End Function
#End Region

    Public Sub initControls(ByVal strInit As String)
        Select Case strInit
            Case "OnNewApplication"
                PanelBiaya.Visible = False
                PanelCatatan.Visible = False
                PanelDataEntriAPK.Visible = True
                PanelDataLainnya.Visible = False
                PanelHeader.Visible = False
                PanelKontrakLain.Visible = False
                PanelProsesKredit.Visible = False
                PanelApprovalHistory.Visible = False
                PanelButtons.Visible = False
            Case Else
                PanelBiaya.Visible = True
                PanelCatatan.Visible = True
                PanelDataEntriAPK.Visible = False
                PanelDataLainnya.Visible = True
                PanelHeader.Visible = True
                PanelKontrakLain.Visible = True
                PanelProsesKredit.Visible = True
                PanelApprovalHistory.Visible = True
                PanelButtons.Visible = True
        End Select
    End Sub

    Public Sub bindData()
        Bindgrid()
        'BindgridApprovalHistory()
        lblCustomerID.Attributes.Add("onclick", "return OpenViewCustomer('" & Me.CustomerID & "','" & Me.Style & "')")
    End Sub

    Private Sub Bindgrid()
        Dim dt As New DataTable
        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(GetConnectionString)
        Dim objReader As SqlDataReader
        Dim objProsCommand As New SqlCommand
        Dim objProsReader As SqlDataReader
        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.CommandText = "spViewCreditProcess"

            If Not Me.ApplicationID Is Nothing Or Me.ApplicationID <> "" Then
                objCommand.Parameters.Add("@ApplicationID", SqlDbType.VarChar, 20).Value = Me.ApplicationID.Trim
                objCommand.Connection = objConnection
                objReader = objCommand.ExecuteReader()

                If objReader.Read Then
                    Me.AgreementNo = objReader.Item("AgreementNo").ToString.Trim
                    Me.ApprovalNo = objReader.Item("ApprovalNo").ToString.Trim
                    Me.AssetTypeID = objReader.Item("AssetTypeID").ToString.Trim
                    Me.CustomerID = objReader.Item("CustomerID").ToString.Trim
                    Me.CustomerName = objReader.Item("CustomerName").ToString.Trim
                    Me.ProductID = objReader.Item("ProductID").ToString.Trim
                    Me.ProductOfferingID = objReader.Item("ProductOfferingID").ToString.Trim
                    Me.InterestType = objReader.Item("InterestType").ToString.Trim
                    Me.InstallmentScheme = objReader.Item("InstallmentScheme").ToString.Trim
                    Me.BranchID = objReader.Item("BranchID").ToString.Trim
                    Me.SupplierID = objReader.Item("SupplierID").ToString.Trim
                    Me.TanggalAplikasi = Format(objReader.Item("DateEntryApplicationData"), "dd/MM/yyyy")
                    Me.AppTimeLimit = CInt(objReader.Item("appTimeLimit"))
                    Me.IsAppTimeLimitReached = CBool(IIf(DateDiff(DateInterval.Day, CDate(objReader.Item("DateEntryApplicationData")), Me.BusinessDate) >= Me.AppTimeLimit, True, False))
                    Me.CustomerType = objReader.Item("CustomerType").ToString.Trim
                    Me.Origination = objReader.Item("Origination").ToString.Trim
                    Me.PolaTransaksi = objReader.Item("PolaTransaksi").ToString.Trim
                    Me.DPPercentage = CInt(objReader.Item("DPPercentage").ToString.Trim)
                    'Me.DownPayment = CInt(objReader.Item("DownPayment").ToString.Trim)
                    Me.DownPayment = CDec(objReader.Item("DownPayment").ToString.Trim)
                    Me.KondisiAsset = objReader.Item("KondisiAsset").ToString.Trim
                    Me.isPAAvailable = CBool(objReader.Item("isPAAvailable").ToString.Trim)
                    Me.PaketProgram = objReader.Item("PaketProgram").ToString.Trim
                    Me.isProceed = CBool(objReader.Item("isProceed").ToString.Trim)
                    Me.SupplierName = objReader.Item("SupplierName").ToString.Trim
                    Me.desProduct = objReader.Item("desProduct").ToString.Trim
                    Me.AssetUsage = objReader.Item("AssetUsage").ToString.Trim
                    Me.OTR = CDec(objReader.Item("OTR").ToString.Trim)
                    Me.ManufacturingYear = objReader.Item("ManufacturingYear").ToString.Trim
                    Me.NoFasilitas = objReader.Item("NoFasilitas").ToString.Trim

                    lblTanggalAPK.Text = Me.TanggalAplikasi
                    hyNoAPK.Text = objReader.Item("ApplicationID").ToString.Trim
                    hyNoAPK.NavigateUrl = LinkTo(hyNoAPK.Text, "AccAcq")

                    lblProductPaket.Text = objReader.Item("Paket").ToString.Trim
                    lblNamaProduct.Text = objReader.Item("desProduct").ToString.Trim

                    lblAgreementNo.Text = Me.AgreementNo
                    lblCustomerID.Text = Me.CustomerName
                    lblApplicationDaTE.Text = objReader.Item("NewApplicationDate").ToString.Trim
                    lblSourceApplication.Text = objReader.Item("ApplicationSource").ToString.Trim
                    lblReqSurveyDate.Text = objReader.Item("RequestSurveyDate").ToString.Trim
                    lblCreditBy.Text = objReader.Item("CreditScoringResult").ToString.Trim
                    lblSurveyDate.Text = objReader.Item("SurveyDate").ToString.Trim
                    lblRCADate.Text = objReader.Item("RCADate").ToString.Trim
                    lblApprovalDate.Text = objReader.Item("ApprovalDate").ToString.Trim
                    lblApprovalNo.Text = objReader.Item("ApprovalNo").ToString.Trim

                    lblAgreementDate.Text = objReader.Item("AgreementDate").ToString.Trim
                    lblPODate.Text = objReader.Item("PurchaseOrderDate").ToString.Trim
                    lblDODate.Text = objReader.Item("DeliveryOrderDate").ToString.Trim
                    lblSupplierDate.Text = objReader.Item("SupplierInvoiceDate").ToString.Trim
                    lblGoLiveDate.Text = objReader.Item("GoLiveDate").ToString.Trim
                    lblSurveyor.Text = objReader.Item("SurveyorName").ToString.Trim
                    lblAO.Text = objReader.Item("AOName").ToString.Trim
                    lblCA.Text = objReader.Item("CAName").ToString.Trim
                    lblAdminFee.Text = FormatNumber(objReader.Item("AdminFee").ToString.Trim, 0)
                    lblSurveyFee.Text = FormatNumber(objReader.Item("SurveyFee").ToString.Trim, 0)
                    lblFiduciaFee.Text = FormatNumber(objReader.Item("FiduciaFee").ToString.Trim, 0)
                    lblBBNFee.Text = FormatNumber(objReader.Item("BBNFee").ToString.Trim, 0)
                    lblProvisionFee.Text = FormatNumber(objReader.Item("ProvisionFee").ToString.Trim, 0)
                    lblOtherFee.Text = FormatNumber(objReader.Item("OtherFee").ToString.Trim, 0)
                    lblNotaryFee.Text = FormatNumber(objReader.Item("NotaryFee").ToString.Trim, 0)
                    lblExConversion.Text = objReader.Item("isExConversion").ToString.Trim
                    lblFloatingPeriod.Text = objReader.Item("FloatingPeriod").ToString.Trim
                    lblAvalist.Text = objReader.Item("isAvalist").ToString.Trim
                    lblFloatingReviewDate.Text = objReader.Item("FloatingNextReviewDate").ToString.Trim
                    lblSyndication.Text = objReader.Item("isSyndication").ToString.Trim
                    lblLastFloatAdj.Text = objReader.Item("LastFloatingAdjustment").ToString.Trim
                    lblBuyBackValidDate.Text = objReader.Item("BuyBackGuaranteeValidDate").ToString.Trim
                    lblLastFloatEx.Text = objReader.Item("LastFloatExecution").ToString.Trim
                    LblNST.Text = objReader.Item("IsNst").ToString.Trim



                End If
                objReader.Close()

                If objConnection.State = ConnectionState.Closed Then objConnection.Open()
                objCommand.Parameters.Clear()
                objCommand.CommandType = CommandType.StoredProcedure
                objCommand.CommandText = "spViewCrossDefault"
                objCommand.Parameters.Add("@ApplicationID", SqlDbType.Char, 20).Value = Me.ApplicationID.Trim
                objReader = objCommand.ExecuteReader()
                dtgCrossDefault.DataSource = objReader
                dtgCrossDefault.DataBind()
                objReader.Close()
                'modify by Wira 
            ElseIf (Me.ApplicationID Is Nothing Or Me.ApplicationID = "") And Me.CustomerID <> "" Then
                If objConnection.State = ConnectionState.Closed Then objConnection.Open()
                objProsCommand.Parameters.Clear()
                objProsCommand.CommandType = CommandType.StoredProcedure
                objProsCommand.CommandText = "spConsumerProspect2"
                objProsCommand.Parameters.Add("@CustomerID", SqlDbType.Char, 20).Value = Me.CustomerID.Trim
                objProsCommand.Connection = objConnection
                objProsReader = objProsCommand.ExecuteReader()

                If objProsReader.Read Then
                    lblTanggalAPK.Text = Me.BusinessDate
                    Me.SupplierID = objProsReader.Item("SupplierID").ToString.Trim
                End If
                objProsReader.Close()

                '-----------End Modify
            Else
                lblTanggalAPK.Text = "-"
                hyNoAPK.Text = "-"
                lblAgreementNo.Text = "-"
                lblCustomerID.Text = "-"
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
            objCommand.Dispose()
        End Try
    End Sub

    Private Sub BindgridApprovalHistory()
        Dim objCommand As New SqlCommand
        Dim objconnection As New SqlConnection(GetConnectionString)
        Dim objread As SqlDataReader
        Try
            If objconnection.State = ConnectionState.Closed Then objconnection.Open()
            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.Connection = objconnection
            objCommand.CommandText = "spApprovalHistoryList"
            objCommand.Parameters.Add("@ApprovalNo", SqlDbType.VarChar, 50).Value = Me.ApprovalNo
            objCommand.Parameters.Add("@BusinessDate", SqlDbType.DateTime).Value = Me.BusinessDate
            objread = objCommand.ExecuteReader()
            dtgHistory.DataSource = objread
            dtgHistory.DataBind()
            objread.Close()
            If dtgHistory.Items.Count > 0 Then
                dtgHistory.Visible = True
            Else
                dtgHistory.Visible = False
            End If
        Catch ex As Exception
            Trace.Write(ex.Message)
        Finally
            objCommand.Parameters.Clear()
            objCommand.Dispose()
            If objconnection.State = ConnectionState.Open Then objconnection.Close()
            objconnection.Dispose()
        End Try
    End Sub
End Class