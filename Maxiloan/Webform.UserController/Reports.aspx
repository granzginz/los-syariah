﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Reports.aspx.vb" Inherits="Maxiloan.Webform.UserController.Reports" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%--<%@ Register Src="../Webform.UserController/ucRSViewer.ascx" TagName="ucRSViewer"
    TagPrefix="uc1" %>--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Reports</title>
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="sm1">
    </asp:ScriptManager>
    <%--<uc1:ucrsviewer id="ucRSViewer1" runat="server" />--%>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h4>
                FILTER REPORT</h4>
        </div>
    </div>
    <div runat="server" id="dvBranch" class="form_box">
        <div class="form_left">
            <label class="label_general">
                Cabang
            </label>
            <div class="checkbox_wrapper_ws">
                <asp:CheckBoxList runat="server" ID="chkBranch" RepeatDirection="Vertical" RepeatLayout="Table"
                    CssClass="opt_single">
                </asp:CheckBoxList>
            </div>
        </div>
    </div>
    <div runat="server" id="dvDateRange" class="form_box">
        <div class="form_single">
            <label class="label_general">
                Mulai Tanggal
            </label>
            <asp:TextBox runat="server" ID="txtStartDate" CssClass="small_text"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ID="rfvDate1" ControlToValidate="txtStartDate"
                Display="Dynamic" ErrorMessage="Isi tanggal!" CssClass="validator_general"></asp:RequiredFieldValidator>
            <asp:CalendarExtender runat="server" ID="ce1" TargetControlID="txtStartDate" Format="dd/MM/yyyy">
            </asp:CalendarExtender>
            <label class="label_auto">
                s/d</label>
            <asp:TextBox runat="server" ID="txtEndDate" CssClass="small_text"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ID="rfvDate2" ControlToValidate="txtEndDate"
                Display="Dynamic" ErrorMessage="Isi tanggal!" CssClass="validator_general"></asp:RequiredFieldValidator>
            <asp:CalendarExtender runat="server" ID="ce2" TargetControlID="txtEndDate" Format="dd/MM/yyyy">
            </asp:CalendarExtender>
        </div>
    </div>
    <div runat="server" id="dvSingleDate" class="form_box">
        <div class="form_single">
            <label class="label_general">
                Tanggal
            </label>
            <asp:TextBox runat="server" ID="txtSingleDate" CssClass="small_text"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtSingleDate"
                Display="Dynamic" ErrorMessage="Isi tanggal!" CssClass="validator_general"></asp:RequiredFieldValidator>
            <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtSingleDate"
                Format="dd/MM/yyyy">
            </asp:CalendarExtender>
        </div>
    </div>
    <div runat="server" id="dvKondisiSKT" class="form_box">
        <div class="form_single">
            <label class="label_general">
                Kondisi
            </label>
            <asp:DropDownList runat="server" ID="cboKondisiSKT">
                <asp:ListItem Text="SKT - HARI INI" Value="TD" Selected="true"></asp:ListItem>
                <asp:ListItem Text="SKT - OUSTANDING" Value="OP"></asp:ListItem>
                <asp:ListItem Text="SKT - BERHASIL DITARIK" Value="CL"></asp:ListItem>
            </asp:DropDownList>
        </div>
    </div>
    <div runat="server" id="dvPeriodLapARODStatCMO" class="form_box">
        <div class="form_single">
            <label class="label_general">
                Periode</label>
            <asp:DropDownList runat="server" ID="cboStartMonth">
                <asp:ListItem Text="Januari" Value="1" Selected="True"></asp:ListItem>
                <asp:ListItem Text="Februari" Value="2"></asp:ListItem>
                <asp:ListItem Text="Maret" Value="3"></asp:ListItem>
                <asp:ListItem Text="April" Value="4"></asp:ListItem>
                <asp:ListItem Text="Mei" Value="5"></asp:ListItem>
                <asp:ListItem Text="Juni" Value="6"></asp:ListItem>
                <asp:ListItem Text="Juli" Value="7"></asp:ListItem>
                <asp:ListItem Text="Agustus" Value="8"></asp:ListItem>
                <asp:ListItem Text="September" Value="9"></asp:ListItem>
                <asp:ListItem Text="Oktober" Value="10"></asp:ListItem>
                <asp:ListItem Text="November" Value="11"></asp:ListItem>
                <asp:ListItem Text="Desember" Value="12"></asp:ListItem>
            </asp:DropDownList>
            <asp:TextBox runat="server" ID="txtStartYear" CssClass="small_text"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ControlToValidate="txtStartYear"
                Display="Dynamic" ErrorMessage="Isi Tahun" CssClass="validator_general"></asp:RequiredFieldValidator>
            <label class="label_auto">
                s/d
            </label>
            <asp:DropDownList runat="server" ID="cboEndMonth">
                <asp:ListItem Text="Januari" Value="1" Selected="True"></asp:ListItem>
                <asp:ListItem Text="Februari" Value="2"></asp:ListItem>
                <asp:ListItem Text="Maret" Value="3"></asp:ListItem>
                <asp:ListItem Text="April" Value="4"></asp:ListItem>
                <asp:ListItem Text="Mei" Value="5"></asp:ListItem>
                <asp:ListItem Text="Juni" Value="6"></asp:ListItem>
                <asp:ListItem Text="Juli" Value="7"></asp:ListItem>
                <asp:ListItem Text="Agustus" Value="8"></asp:ListItem>
                <asp:ListItem Text="September" Value="9"></asp:ListItem>
                <asp:ListItem Text="Oktober" Value="10"></asp:ListItem>
                <asp:ListItem Text="November" Value="11"></asp:ListItem>
                <asp:ListItem Text="Desember" Value="12"></asp:ListItem>
            </asp:DropDownList>
            <asp:TextBox runat="server" ID="txtEndYear" CssClass="small_text"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ControlToValidate="txtEndYear" Display="dynamic"
                ErrorMessage="isi tahun" CssClass="validator_general" ID="RequiredFieldValidator3"></asp:RequiredFieldValidator>
        </div>
    </div>
    <div runat="server" id="dvLmNunggak" class="form_box">
        <div class="form_single">
            <label class="label_general">
                Lama Nunggak Dari</label>
            <asp:TextBox runat="server" ID="txtlnstart" CssClass="small_text"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator4" ControlToValidate="txtStartYear"
                Display="Dynamic" ErrorMessage="Isi Tahun" CssClass="validator_general"></asp:RequiredFieldValidator>
            <label class="label_auto">
                Sampai Hari ke
            </label>
            <asp:TextBox runat="server" ID="txtlnend" CssClass="small_text"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ControlToValidate="txtEndYear" Display="dynamic"
                ErrorMessage="isi tahun" CssClass="validator_general" ID="RequiredFieldValidator5"></asp:RequiredFieldValidator>
        </div>
    </div>
    <div runat="server" id="dvCollectorCL" class="form_box">
        <div class="form_single">
            <label class="label_general">
                Collector</label>
            <asp:DropDownList runat="server" ID="cboCollectorCL">
            </asp:DropDownList>
        </div>
    </div>
    <div runat="server" id="dvCollectorD" class="form_box">
        <div class="form_single">
            <label class="label_general">
                Collector</label>
            <asp:DropDownList runat="server" ID="cboCollectorD">
            </asp:DropDownList>
            <label class="label_auto">
                Jenis Kegiatan
            </label>
            <asp:DropDownList runat="server" ID="cboJenisTask">
                <asp:ListItem Text="Remind Installment" Value="ri" Selected="True"></asp:ListItem>
                <asp:ListItem Text="OverDue Call" Value="oc">
                </asp:ListItem>
            </asp:DropDownList>
        </div>
    </div>
    <div runat="server" id="dvStatus" class="form_box">
        <div class="form_left">
            <label class="label_general">
                Status</label>
            <asp:CheckBoxList runat="server" ID="chkStatus" RepeatDirection="Vertical" RepeatLayout="Table"
                CssClass="opt_single">
                <asp:ListItem Text="Repossess" Value="r" Selected="True"></asp:ListItem>
                <asp:ListItem Text="Inventory" Value="i"></asp:ListItem>
                <asp:ListItem Text="Sold" Value="s"></asp:ListItem>
                <asp:ListItem Text="Paid" Value="p"></asp:ListItem>
            </asp:CheckBoxList>
        </div>
    </div>
    <div runat="server" id="dvCreditAnalist" class="form_box">
        <div class="form_single">
            <label class="label_general">
                Credit Analyst</label>
            <asp:DropDownList runat="server" ID="cboCreditAnalist">
            </asp:DropDownList>
        </div>
    </div>
    <div runat="server" id="dvCMO" class="form_box">
        <div class="form_single">
            <label class="label_general">
                Nama CMO</label>
            <asp:DropDownList runat="server" ID="cboCMO">
            </asp:DropDownList>
        </div>
    </div>
    <div runat="server" id="dvSurveyor" class="form_box">
        <div class="form_single">
            <label class="label_general">
                Nama Surveyor</label>
            <asp:DropDownList runat="server" ID="cboSurveyor">
            </asp:DropDownList>
        </div>
    </div>
    <div runat="server" id="dvRekBankType_B" class="form_box">
        <div class="form_single">
            <label class="label_general">
                Rekening Bank</label>
            <asp:DropDownList runat="server" ID="cboRekBankType_B">
            </asp:DropDownList>
        </div>
    </div>
    <div runat="server" id="dvRekBankType_C" class="form_box">
        <div class="form_single">
            <label class="label_general">
                Rekening Bank</label>
            <asp:DropDownList runat="server" ID="cboRekBankType_C">
            </asp:DropDownList>
        </div>
    </div>
    <div runat="server" id="dvBankAccount" class="form_box">
        <div class="form_single">
            <label class="label_general">
                Bank Account</label>
            <asp:DropDownList runat="server" ID="cboBankAccount">
            </asp:DropDownList>
        </div>
    </div>
    <div runat="server" id="dvChkColl_CL" class="form_box">
        <div class="form_single">
            <label class="label_general">
                Collector
            </label>
            <asp:CheckBoxList runat="server" ID="chkColl" RepeatDirection="Vertical" RepeatLayout="Table"
                CssClass="opt_single">
            </asp:CheckBoxList>
        </div>
    </div>
    <div runat="server" id="dvKasir" class="form_box">
        <div class="form_single">
            <label class="label_general">
                Kasir</label>
            <asp:DropDownList runat="server" ID="cboKasir">
            </asp:DropDownList>
        </div>
    </div>
    <div runat="server" id="dvBankCompany" class="form_box">
        <div class="form_single">
            <label class="label_general">
                Bank</label>
            <asp:DropDownList runat="server" ID="cboBankCompany" AutoPostBack="true">
            </asp:DropDownList>
        </div>
    </div>
    <div runat="server" id="dvKontrak" class="form_box">
        <div class="form_single">
            <label class="label_general">
                No Kontrak
            </label>
            <asp:DropDownList runat="server" ID="cboKontrak" AutoPostBack="true" OnSelectedIndexChanged="bindComboBatch">
            </asp:DropDownList>
        </div>
    </div>
    <div runat="server" id="dvNoBatch" class="form_box">
        <div class="form_single">
            <label class="label_general">
                No Batch
            </label>
            <asp:DropDownList runat="server" ID="cboNoBatch" AutoPostBack="true" OnSelectedIndexChanged="bindNamaKonsumen">
            </asp:DropDownList>
        </div>
    </div>
    <div runat="server" id="dvNamaKonsumen" class="form_box">
        <div class="form_single">
            <label class="label_general">
                Nama Konsumen
            </label>
            <asp:DropDownList runat="server" ID="cboNamaKonsumen">
            </asp:DropDownList>
        </div>
    </div>
    <div runat="server" id="dvchkInsuranceCompany" class="form_box">
        <div class="form_single">
            <label class="label_general">
                Perusahaan Asuransi
            </label>
            <div class="checkbox_wrapper_ws">
                <asp:CheckBoxList runat="server" ID="chkInsuranceCompany" RepeatDirection="Vertical"
                    RepeatLayout="Table" CssClass="opt_single">
                </asp:CheckBoxList>
            </div>
        </div>
    </div>
    <div runat="server" id="dvStatusPolis" class="form_box">
        <div class="form_single">
            <label class="label_general">
                Status Polis
            </label>
            <asp:DropDownList runat="server" ID="cboStatusPolis">
                <asp:ListItem Text="SUDAH TERIMA" Value="O"></asp:ListItem>
                <asp:ListItem Text="BELUM TERIMA" Value="N"></asp:ListItem>
            </asp:DropDownList>
        </div>
    </div>
    <div runat="server" id="dvYear" class="form_box">
        <div class="form_single">
            <label class="label_general">
                Tahun
            </label>
            <asp:TextBox runat="server" ID="txtYear" CssClass="small_text"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator6" ControlToValidate="txtYear"
                Display="Dynamic" ErrorMessage="Isi Tahun" CssClass="validator_general"></asp:RequiredFieldValidator>
        </div>
    </div>
    <div runat="server" id="dvInvoice" class="form_box">
        <div class="form_single">
            <label class="label_general">
                Invoice No
            </label>
            <asp:TextBox runat="server" ID="txtInvoice" CssClass="small_text"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator7" ControlToValidate="txtInvoice"
                Display="Dynamic" ErrorMessage="Isi Nomor Tagihan" CssClass="validator_general"></asp:RequiredFieldValidator>
        </div>
    </div>
    <div runat="server" id="dvInsuranceCompany" class="form_box">
        <div class="form_single">
            <label class="label_general">
                Asuransi
            </label>
            <asp:DropDownList runat="server" ID="cboInsuranceCompany">
            </asp:DropDownList>
        </div>
    </div>
    <div runat="server" id="dvTransactionType" class="form_box">
        <div class="form_left">
            <label class="label_general">
                Jenis Transaksi
            </label>
            <div class="checkbox_wrapper_ws">
                <asp:CheckBoxList runat="server" ID="chkTransactionType" RepeatDirection="Vertical"
                    RepeatLayout="Table" CssClass="opt_single">
                </asp:CheckBoxList>
            </div>
        </div>
    </div>
    <div runat="server" id="dvNoKontrak" class="form_box">
        <div class="form_single">
            <label class="label_general">
                No Kontrak
            </label>
            <asp:DropDownList ID="cboNoKontrak" runat="server">
            </asp:DropDownList>
        </div>
    </div>
    <%-- <div runat="server" id="Div1" class="form_box">
        <div class="form_single">
            <label class="label_general">
                Kondisi
            </label>
            <asp:DropDownList runat="server" ID="DropDownList1">
                <asp:ListItem Text="SKT - HARI INI" Value="TD" Selected="true"></asp:ListItem>
                <asp:ListItem Text="SKT - OUSTANDING" Value="OP"></asp:ListItem>
                <asp:ListItem Text="SKT - BERHASIL DITARIK" Value="CL"></asp:ListItem>
            </asp:DropDownList>
        </div>
    </div>--%>
    <div runat="server" id="dvMonth" class="form_box">
        <div class="form_single">
            <label class="label_general">
                Bulan</label>
            <asp:DropDownList runat="server" ID="cboMonth">
                <asp:ListItem Text="Januari" Value="1" Selected="True"></asp:ListItem>
                <asp:ListItem Text="Februari" Value="2"></asp:ListItem>
                <asp:ListItem Text="Maret" Value="3"></asp:ListItem>
                <asp:ListItem Text="April" Value="4"></asp:ListItem>
                <asp:ListItem Text="Mei" Value="5"></asp:ListItem>
                <asp:ListItem Text="Juni" Value="6"></asp:ListItem>
                <asp:ListItem Text="Juli" Value="7"></asp:ListItem>
                <asp:ListItem Text="Agustus" Value="8"></asp:ListItem>
                <asp:ListItem Text="September" Value="9"></asp:ListItem>
                <asp:ListItem Text="Oktober" Value="10"></asp:ListItem>
                <asp:ListItem Text="November" Value="11"></asp:ListItem>
                <asp:ListItem Text="Desember" Value="12"></asp:ListItem>
            </asp:DropDownList>
        </div>
    </div>
    <div runat="server" id="dvNoAplikasi" class="form_box">
        <div class="form_single">
            <label class="label_general">
                No Aplikasi</label>
            <asp:DropDownList runat="server" ID="cboNoAplikasi">
            </asp:DropDownList>
        </div>
    </div>
    <div runat="server" id="dvPilihan" class="form_box">
        <div class="form_left">
            <label class="label_general">
                Pilihan</label>
            <asp:CheckBoxList runat="server" ID="chkPilihan" RepeatDirection="Vertical" RepeatLayout="Table"
                CssClass="opt_single">
                <asp:ListItem Text="DAILY" Value="D" Selected="True"></asp:ListItem>
                <asp:ListItem Text="MONTHLY" Value="M"></asp:ListItem>
            </asp:CheckBoxList>
        </div>
    </div>
    <div class="form_button">
        <asp:Button runat="server" ID="btnView" Text="View Report" CssClass="small button blue" />
    </div>
    </form>
</body>
</html>
