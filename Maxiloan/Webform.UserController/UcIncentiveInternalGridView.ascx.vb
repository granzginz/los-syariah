﻿Imports Maxiloan.Controller

Public Class UcIncentiveInternalGridView
    Inherits ControlBased

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Public Event EventEdit(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
    Private Msg As New Maxiloan.Webform.WebBased
    Private m_Insentif As New RefundInsentifController

    Public WriteOnly Property OnClientClick As String
        Set(ByVal value As String)
            btnAdd.Attributes.Add("OnClick", value)
        End Set
    End Property
    Public WriteOnly Property OnSaveClick As String
        Set(ByVal value As String)
            btnSave.Attributes.Add("OnClick", value)
        End Set
    End Property
    Public Property ApplicationID() As String
        Get
            Return (CType(ViewState("ApplicationID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
    Public Property SupplierID() As String
        Set(ByVal SupplierID As String)
            ViewState("SupplierID") = SupplierID
        End Set
        Get
            Return ViewState("SupplierID").ToString
        End Get
    End Property
    Public Property WhereCond As String
        Set(ByVal WhereCond As String)
            ViewState("WhereCond") = WhereCond
        End Set
        Get
            Return ViewState("WhereCond").ToString
        End Get
    End Property
    Public Property dtg As DataGrid
        Get
            Return dtgIncentive
        End Get
        Set(ByVal dtg As DataGrid)
            dtgIncentive = dtg
        End Set
    End Property
    Public Property AlokasiPremiAsuransiInternal As String
        Set(ByVal AlokasiPremiAsuransiInternal As String)
            ViewState("AlokasiPremiAsuransiInternal") = AlokasiPremiAsuransiInternal
        End Set
        Get
            Return ViewState("AlokasiPremiAsuransiInternal").ToString
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub
    'Private Sub dtgIncentive_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgIncentive.ItemCommand
    '    If e.CommandName = "Delete" Then
    '        DeleteBaris(e.Item.ItemIndex)
    '    End If
    'End Sub
    Public Sub AddRecord()
        Dim objectDataTable As New DataTable
        Dim intLoopGrid As Integer
        Dim oRow As DataRow
        Dim lblNomor As New Label
        Dim lblddlPenggunaan As New label
        Dim lblProsentase As New Label
        Dim lblInsentif As New Label
        Dim plus As Integer = 0
        Dim imgDelete As New ImageButton



        With objectDataTable
            .Columns.Add(New DataColumn("Nomor", GetType(String)))
            .Columns.Add(New DataColumn("Penggunaan", GetType(String)))
            .Columns.Add(New DataColumn("Prosentase", GetType(String)))
            .Columns.Add(New DataColumn("Insentif", GetType(String)))
        End With

        For intLoopGrid = 0 To dtgIncentive.Items.Count - 1
            lblNomor = CType(dtgIncentive.Items(intLoopGrid).FindControl("lblNomor"), Label)
            lblddlPenggunaan = CType(dtgIncentive.Items(intLoopGrid).FindControl("lblddlPenggunaan"), label)
            lblProsentase = CType(dtgIncentive.Items(intLoopGrid).FindControl("lblProsentase"), label)
            lblInsentif = CType(dtgIncentive.Items(intLoopGrid).FindControl("lblInsentif"), label)

            '----- Add row -------'
            oRow = objectDataTable.NewRow()
            oRow("Nomor") = CType(lblNomor.Text, String)
            oRow("Penggunaan") = CType(lblddlPenggunaan.Text, String)
            oRow("Prosentase") = CType(lblProsentase.Text, String)
            oRow("Insentif") = CType(lblInsentif.Text, String)

            objectDataTable.Rows.Add(oRow)


        Next

        oRow = objectDataTable.NewRow()
        oRow("Nomor") = dtgIncentive.Items.Count + 1
        oRow("Penggunaan") = ""
        oRow("Prosentase") = ""
        oRow("Insentif") = ""

        objectDataTable.Rows.Add(oRow)

        dtgIncentive.DataSource = objectDataTable
        dtgIncentive.DataBind()

        ' BindDDl()

        For intLoopGrid = 0 To dtgIncentive.Items.Count - 1
            imgDelete = CType(dtgIncentive.Items(intLoopGrid).FindControl("imgDelete"), ImageButton)
            lblNomor = CType(dtgIncentive.Items(intLoopGrid).FindControl("lblNomor"), Label)
            lblddlPenggunaan = CType(dtgIncentive.Items(intLoopGrid).FindControl("lblddlPenggunaan"), Label)
            lblProsentase = CType(dtgIncentive.Items(intLoopGrid).FindControl("lblProsentase"), label)
            lblInsentif = CType(dtgIncentive.Items(intLoopGrid).FindControl("lblInsentif"), label)

            lblNomor.Text = objectDataTable.Rows(intLoopGrid).Item(0).ToString.Trim

            If objectDataTable.Rows(intLoopGrid).Item(1).ToString <> "" Then
                lblddlPenggunaan.Text = objectDataTable.Rows(intLoopGrid).Item(1).ToString.Trim
            End If

            lblProsentase.Text = objectDataTable.Rows(intLoopGrid).Item(2).ToString.Trim
            lblInsentif.Text = objectDataTable.Rows(intLoopGrid).Item(3).ToString.Trim

            lblProsentase.Attributes.Add("OnChange", "handlelblProsentaseInternal_Change(this.value,'" & Me.AlokasiPremiAsuransiInternal & "','" & lblInsentif.ClientID & "')")
            lblInsentif.Attributes.Add("OnChange", "handlelblInsentifInternal_Change(this.value,'" & Me.AlokasiPremiAsuransiInternal & "','" & lblProsentase.ClientID & "')")

            imgDelete.Attributes.Add("OnClick", "return deleteRowInternal('" & dtgIncentive.ClientID & "',this);")
        Next
    End Sub
    Public Sub BindDDl(ByVal tipe As String, ByVal index As Integer)
        Dim dtEntity As New DataTable
        Dim oCustom As New Parameter.RefundInsentif
        Dim lblddlPenggunaan As New Label
        lblddlPenggunaan = CType(dtgIncentive.Items(index).FindControl("lblddlPenggunaan"), Label)

        With oCustom
            .strConnection = GetConnectionString()
            .WhereCond = ""
            If tipe <> "" Then
                .WhereCond = " TransID in (select TransID from tblAgreementMasterTransaction WHERE GroupTransID = '" & tipe & "') "
            End If

            .SPName = "spGetPenggunaanInsentif"
        End With

        oCustom = m_Insentif.GetSPBy(oCustom)
        dtEntity = oCustom.ListDataTable

        If dtEntity.Rows.Count > 0 Then lblddlPenggunaan.Text = dtEntity.Rows(0).Item("Name").ToString
        If dtEntity.Rows.Count <= 0 Then lblddlPenggunaan.Text = ""
        
    End Sub
    Public Sub BindTotal()
        Dim A_PremiAsuransi As Decimal = CDec(Me.AlokasiPremiAsuransiInternal)
        Dim totalInsentif As Decimal = 0

        For i = 0 To dtgIncentive.Items.Count - 1
            Dim Grid_lblInsentif As New label

            Grid_lblInsentif = CType(dtgIncentive.Items(i).FindControl("lblInsentif"), label)
            totalInsentif = totalInsentif + CDec(IIf(Grid_lblInsentif.Text.Trim = "", "0", Grid_lblInsentif.Text))
        Next

        If totalInsentif > 0 And A_PremiAsuransi > 0 Then
            ScriptManager.RegisterStartupScript(dtgIncentive, GetType(DataGrid), dtgIncentive.ClientID, String.Format(" document.getElementById('{0}' + '_lblTotalProsentase').innerHTML = '" + FormatNumber((totalInsentif / A_PremiAsuransi) * 100, 2).ToString + "';  document.getElementById('{0}' + '_lblTotalInsentif').innerHTML = '" + FormatNumber(totalInsentif, 0).ToString + "'; ", dtgIncentive.ClientID), True)
        End If

    End Sub
End Class