﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UCRegional.ascx.vb" Inherits="Maxiloan.Webform.UserController.UCRegional" %>
<div class="form_box_usercontrol">
    <div class="form_left_usercontrol">
        <label>
            Regional</label>
        <asp:DropDownList ID="cboParent" runat="server">
        </asp:DropDownList>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" InitialValue="0"
            Display="Dynamic" ControlToValidate="cboParent" ErrorMessage="Fill Master Regional ID"
            CssClass="validator_general" Enabled="false"  ></asp:RequiredFieldValidator>
    </div>
</div>