﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UCCollectorRpt2.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.UCCollectorRpt2" %>
<div id="Header" runat="server">
</div>
<script language="JavaScript" type="text/javascript">
<!--
    var hdnDetail;
    function WOPChange(pCmbOfPayment, pBankAccount, pHdnDetail, itemArray) {
        hdnDetail = eval('document.forms[0].' + pHdnDetail);
        hdnDetail.value = '0';
        //alert(eval('document.forms[0].' + pCmbOfPayment).options[eval('document.forms[0].' + pCmbOfPayment).selectedIndex].value);
        eval('document.forms[0].' + pBankAccount).disabled = false;


        var i, j;
        for (i = eval('document.forms[0].' + pBankAccount).options.length; i >= 0; i--) {
            eval('document.forms[0].' + pBankAccount).options[i] = null

        };
        if (itemArray == null) {
            j = 0;
        }
        else {
            j = 1;
        };
        eval('document.forms[0].' + pBankAccount).options[0] = new Option('Select One');
        eval('document.forms[0].' + pBankAccount).options[0].value = '0';
        if (itemArray != null) {
            for (i = 0; i < itemArray.length; i++) {
                eval('document.forms[0].' + pBankAccount).options[j] = new Option(itemArray[i][0]);
                if (itemArray[i][1] != null) {
                    eval('document.forms[0].' + pBankAccount).options[j].value = itemArray[i][1];
                };
                j++;
            };
            eval('document.forms[0].' + pBankAccount).selected = true;
        };


        //if( eval('document.forms[0].' + pCmbOfPayment).selectedIndex == 4){
        //eval('document.forms[0].' + pBankAccount).disabled = true;
        //}
        //if( eval('document.forms[0].' + pCmbOfPayment).selectedIndex  == 5){
        //eval('document.forms[0].' + pBankAccount).disabled = true;
        //}
        //if( eval('document.forms[0].' + pCmbOfPayment).selectedIndex  == 2){
        //eval('document.forms[0].' + pBankAccount).disabled = true;
        //}
        //if( eval('document.forms[0].' + pCmbOfPayment).selectedIndex  == 7){
        //eval('document.forms[0].' + pBankAccount).disabled = true;
        //}
    }



    function cboChildonChange(l, phdnResult) {
        hdnDetail = eval('document.forms[0].' + phdnResult);
        if (l != '0')
        { hdnDetail.value = l }
        else
        { hdnDetail.value = '0'; }

    }



    function SetcboChildFocus() {
        var SPV;
        SPV = hdnDetail.value;
        alert(SPV);

        for (i = document.forms[0].cboChild.options.length; i >= 0; i--) {
            if (document.forms[0].cboChild.options.options[i].value = SPV) {
                document.forms[0].cboChild.options[i].selected = true;
            }
        };
    }
-->
</script>
<input id="hdnSP" type="hidden" name="hdnSP" runat="server" />
<input id="hdnResult" type="hidden" name="hdnResult" runat="server" />
<table class="tablegrid" id="Table1" cellspacing="1" cellpadding="3" width="95%"
    align="center" border="0">
    <tr>
        <td class="tdgenap" style="height: 22px" width="25%">
            Collection Group
        </td>
        <td class="tdganjil" style="height: 22px" width="75%">
            <asp:DropDownList ID="cboParent" runat="server" onchange="<%#WOPonChange()%>" >
            </asp:DropDownList>
            &nbsp;<font color="#ff3300">*)<asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                runat="server" InitialValue="0" Display="Dynamic" ControlToValidate="cboParent"
                ErrorMessage="Harap pilih Collection Group" CssClass ="validator_general"></asp:RequiredFieldValidator></font>
        </td>
    </tr>
    <tr>
        <td class="tdgenap" width="25%">
            Collector
        </td>
        <td class="tdganjil" width="25%">
            <asp:DropDownList ID="cboChild" runat="server" onchange="<%#ChildChange()%>" >
            </asp:DropDownList>
            &nbsp;<font color="#ff3300">*)<asp:RequiredFieldValidator ID="Requiredfieldvalidator2"
                runat="server" InitialValue="0" Display="Dynamic" ControlToValidate="cboChild"
                ErrorMessage="Harap pilih Collector" CssClass ="validator_general"></asp:RequiredFieldValidator></font>
        </td>
    </tr>
</table>
