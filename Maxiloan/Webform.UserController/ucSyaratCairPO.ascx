﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucSyaratCairPO.ascx.vb" Inherits="Maxiloan.Webform.UserController.ucSyaratCairPO" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<div class="form_title">
    <div class="form_single">
        <h4>
            DAFTAR PERSYARATAN PO</h4>
    </div>
</div>
<div class="form_box_header">
    <div class="form_single">
        <div class="grid_wrapper">
            <asp:DataGrid ID="dtgSyaratCair" runat="server" CellPadding="3" CellSpacing="1" BorderWidth="0px"
                AutoGenerateColumns="False" DataKeyField="ID" CssClass="grid_general" >
                <HeaderStyle CssClass="th" />
                <ItemStyle CssClass="item_grid" />
                <Columns>
                    <asp:TemplateColumn HeaderText="NO" ItemStyle-CssClass="command_col">
                        <ItemTemplate>
                            <asp:Label ID="lblNo" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:BoundColumn DataField="Description" HeaderText="PERSYARATAN" ItemStyle-CssClass="name_col">                        
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="ID" HeaderText="PERSYARATAN" Visible="false"></asp:BoundColumn>
                    <asp:TemplateColumn HeaderText="PERIKSA" ItemStyle-CssClass="command_col">
                        <ItemTemplate>
                            <asp:CheckBox ID="chk" runat="server"></asp:CheckBox>
                            <asp:Label ID="lblChkSyaratCair" runat="server" Visible="false">&nbsp;&nbsp;&nbsp;&nbsp;</asp:Label>
                            <asp:checkbox id="isMandatory" runat="server"  Checked='<%# DataBinder.eval(Container, "DataItem.isMandatory") %>' Visible="false"/>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                </Columns>
            </asp:DataGrid>
        </div>
    </div>
</div>
