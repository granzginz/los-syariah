﻿Public Class ucCoverageTypeApk
    Inherits ControlBased

    Public Event CoverageChanged(ByVal GridIndex As Integer)
    Public Delegate Sub CoverageChangedHandler(ByVal GridIndex As Integer)

    Public Property GridIndex As Integer
        Get
            Return CType(ViewState("GridIndex"), Integer)
        End Get
        Set(ByVal value As Integer)
            ViewState("GridIndex") = value
        End Set
    End Property

    Public WriteOnly Property DataSource As Object
        Set(ByVal value As Object)
            ddlCoverageTypeAPK.DataSource = value
        End Set
    End Property

    Public WriteOnly Property DataTextField As String
        Set(ByVal value As String)
            ddlCoverageTypeAPK.DataTextField = value
        End Set
    End Property

    Public WriteOnly Property DataValueField As String
        Set(ByVal value As String)
            ddlCoverageTypeAPK.DataValueField = value
        End Set
    End Property

    Public Property SelectedValue As String
        Get
            Return ddlCoverageTypeAPK.SelectedValue
        End Get
        Set(ByVal value As String)
            ddlCoverageTypeAPK.SelectedIndex = ddlCoverageTypeAPK.Items.IndexOf(ddlCoverageTypeAPK.Items.FindByValue(value))
        End Set
    End Property

    Public Property Enabled As Boolean
        Get
            Return ddlCoverageTypeAPK.Enabled
        End Get
        Set(ByVal value As Boolean)
            ddlCoverageTypeAPK.Enabled = value
        End Set
    End Property

    Public Sub DataBind()
        ddlCoverageTypeAPK.DataBind()
    End Sub

    Public Sub ClearSelection()
        ddlCoverageTypeAPK.ClearSelection()
    End Sub

    Public Sub ChangeCoverage() Handles ddlCoverageTypeAPK.SelectedIndexChanged
        RaiseEvent CoverageChanged(Me.GridIndex)
    End Sub
End Class