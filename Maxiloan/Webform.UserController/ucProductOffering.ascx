﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucProductOffering.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.ucProductOffering" %>
<div class="form_box">
    <div class="form_single">
        <label class="label_req">Produk Jual</label>
        <asp:TextBox ID="txtProductOfferingDescription" runat="server" CssClass="long_text"></asp:TextBox>
        <asp:HyperLink ID="hpLookup" runat="server" ImageUrl="../images/IconDetail.gif"></asp:HyperLink>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtProductOfferingDescription"
            ErrorMessage="Please Fill In ProductOffering" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
        <input type="hidden" id="hdnProductID" runat="server" name="hdnProductID" />
        <input type="hidden" id="hdnProductOfferingID" runat="server" name="hdnProductOfferingID" />
        <input type="hidden" id="hdnAssetTypeID" runat="server" name="hdnAssetTypeID" />
    </div>
</div>
