﻿Imports Maxiloan.General.CommonCacheHelper

Public Class ucAPPVDetail
    Inherits ControlBased

#Region "Properti"
    Public Property Branch() As String
        Get
            Return CType(ViewState("Branch"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Branch") = Value
        End Set
    End Property
    Public Property PVNo() As String
        Get
            Return CType(ViewState("PVNo"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("PVNo") = Value
        End Set
    End Property
    Public Property WOP() As String
        Get
            Return CType(ViewState("WOP"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("WOP") = Value
        End Set
    End Property
    Public Property RequestBy() As String
        Get
            Return CType(ViewState("RequestBy"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("RequestBy") = Value
        End Set
    End Property
    Public Property APType() As String
        Get
            Return CType(ViewState("APType"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("APType") = Value
        End Set
    End Property
    Public Property PVDate() As String
        Get
            Return CType(ViewState("PVDate"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("PVDate") = Value
        End Set
    End Property
    Public Property BankAccount() As String
        Get
            Return CType(ViewState("BankAccount"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("BankAccount") = Value
        End Set
    End Property
    Public Property Balance() As String
        Get
            Return CType(ViewState("Balance"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Balance") = Value
        End Set
    End Property
    Public Property Notes() As String
        Get
            Return CType(ViewState("Notes"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Notes") = Value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Public Sub FillTextBox()
        Me.Notes = txtNotes.Text
    End Sub

    Public Sub BindAPPVDetail()
        lblBranch.Text = Me.Branch
        Select Case Me.WOP
            Case "BA"
                lblWOP.Text = "Bank"
            Case "CA"
                lblWOP.Text = "Cash"
            Case "GT"
                lblWOP.Text = "e-Banking"
        End Select

        lblPVNo.Text = Me.PVNo
        lblRequest.Text = Me.RequestBy
        Select Case Me.APType
            Case "SPPL" : lblAPType.Text = "PO Supplier"
            Case "INSR" : lblAPType.Text = "Asuransi"
            Case "INCS" : lblAPType.Text = "Incentive Supplier"
            Case "INCE" : lblAPType.Text = "Incentive Karyawan"
            Case "STNK" : lblAPType.Text = "Birojasa STNK"
            Case "RCST" : lblAPType.Text = "Refund ke Customer"
            Case "ASRD" : lblAPType.Text = "PO Ganti Asset"
            Case "ATFC" : lblAPType.Text = "AP Funding Bank"
            Case "AFTN" : lblAPType.Text = "Fiducia Notaris"
        End Select

        lblPVDate.Text = Me.PVDate
        lblBankAccount.Text = Me.BankAccount
        lblBalance.Text = Me.Balance
        txtNotes.Text = Me.Notes
    End Sub


End Class