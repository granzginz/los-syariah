﻿Public Class UcDeskCollTab
    Inherits System.Web.UI.UserControl

    Property RICI As String
        Set(value As String)
            hypDeskColl.Text = value
        End Set
        Get
            Return hypDeskColl.Text
        End Get
    End Property
    Property CustomerID() As String
        Get
            Return ViewState("CustomerID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property

    Property CustomerName() As String
        Get
            Return ViewState("CustomerName").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerName") = Value
        End Set
    End Property

    Property ApplicationID() As String
        Get
            Return ViewState("ApplicationID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property

    Property AgreementNo() As String
        Get
            Return ViewState("AgreementNo").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("AgreementNo") = Value
        End Set
    End Property

    Public Sub selectedTab(ByVal strTab As String)
        Select Case strTab
            Case "DeskColl"
                tabDeskColl.Attributes.Add("class", "tab_selected")
                tabCollAct.Attributes.Add("class", "tab_notselected")
                tabCollHis.Attributes.Add("class", "tab_notselected")
            Case "CollAct"
                tabDeskColl.Attributes.Add("class", "tab_notselected")
                tabCollAct.Attributes.Add("class", "tab_selected")
                tabCollHis.Attributes.Add("class", "tab_notselected")
            Case "CollHis"
                tabDeskColl.Attributes.Add("class", "tab_notselected")
                tabCollAct.Attributes.Add("class", "tab_notselected")
                tabCollHis.Attributes.Add("class", "tab_selected")
        End Select
    End Sub
    Public Sub setLink()
        hypDeskColl.NavigateUrl = "~/Webform.ARMgt/Inquiry/InqCollActivityResult.aspx?AgreementNo=" + AgreementNo + "&CustomerName=" + CustomerName + "&CustomerID=" + CustomerID
        hypCollAct.NavigateUrl = "~/Webform.ARMgt/Inquiry/InqCollActivityResult.aspx?AgreementNo=" + AgreementNo + "&CustomerName=" + CustomerName + "&CustomerID=" + CustomerID + "&Style=Collection&Referrer=Activity"
        hypCollHis.NavigateUrl = "~/Webform.ARMgt/Inquiry/InqCollActivityHistory.aspx?AgreementNo=" + AgreementNo + "&CustomerName=" + CustomerName + "&CustomerID=" + CustomerID + "&Style=Collection"
    End Sub
End Class