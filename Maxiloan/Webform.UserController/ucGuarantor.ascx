﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucGuarantor.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.ucGuarantor" %>
<%--<%@ Register TagPrefix="uc1" TagName="ucAddress" Src="ucAddress.ascx" %>--%>
<%@ Register TagPrefix="uc1" TagName="ucAddress" Src="UcCompanyAddress.ascx" %>
<%@ Register Src="ucNumberFormat.ascx" TagName="ucNumberFormat" TagPrefix="uc1" %>

<%--<%@ Register Src="UcCompanyAddress.ascx" TagName="UcCompanyAddress" TagPrefix="uc1" %>--%>
<%--<div class="form_box_usercontrol">
    <div class="form_left_usercontrol">
        <h4>
            PENJAMIN</h4>
    </div>
</div>--%>
<div class="form_box_usercontrol">
    <div class="form_left_usercontrol">
        <label class="label_split">
            Nama
        </label>
        <asp:TextBox runat="server" ID="txtNamaGuarantor"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server" ID="rfvNamaGuarantor" ControlToValidate="txtNamaGuarantor"
        InitialValue="" CssClass="validator_general" ErrorMessage="Nama harus diisi!"
        Display="Dynamic"></asp:RequiredFieldValidator>
    </div>
</div>
<div class="form_box_usercontrol">
    <div class="form_left_usercontrol">
        <label class="label_split">
            Jabatan
        </label>
        <asp:TextBox runat="server" ID="txtJabatan"></asp:TextBox>
    </div>
</div>
<div runat="server" id="divAddress">
    <%--<uc1:uccompanyaddress id="UcCompanyAddress1" runat="server" />--%>
    <uc1:ucAddress id="UcCompanyAddress1" runat="server"></uc1:ucAddress>
</div>
<div runat="server" id="divHP" class="form_box_usercontrol">
    <div class="form_left_usercontrol">
        <asp:Label runat="server" ID="lblHandphone" Text="No. Handphone" class="label_split"></asp:Label>
        <asp:TextBox runat="server" ID="txtNoHP"></asp:TextBox>
    </div>
</div>
<div runat="server" id="divEmail" class="form_box_usercontrol">
    <div class="form_left_usercontrol">
        <asp:Label runat="server" ID="lblEmail" Text="Email" CssClass="label_split"></asp:Label>
        <asp:TextBox runat="server" ID="txtEmail"></asp:TextBox>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Format alamat email salah!"
            ControlToValidate="txtEmail" Display="Dynamic" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
    </div>
</div>
<div runat="server" id="divCatatan" class="form_box_usercontrol">
    <div class="form_left_usercontrol">
        <asp:Label runat="server" ID="lblCatatan" Text="Catatan" CssClass="label_split"></asp:Label>
        <% 'TODO txtCatatan belum number format  %>
        <asp:TextBox runat="server" ID="txtCatatan" CssClass="multiline_textbox_uc" TextMode="MultiLine"></asp:TextBox>
    </div>
</div>
<div runat="server" runat="server" id="divPenghasilan" class="form_box_usercontrol">
    <div class="form_left_usercontrol">
        <asp:Label runat="server" ID="lblPenghasilan" Text="Penghasilan" CssClass="label_split"></asp:Label>
        <% 'TODO TxtPenghasilan belum number format  %>        
        <uc1:ucNumberFormat id="txtPenghasilan" runat="server"></uc1:ucNumberFormat>
    </div>
</div>
