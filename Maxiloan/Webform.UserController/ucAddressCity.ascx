﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucAddressCity.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.ucAddressCity" %>

<%@ Register TagPrefix="uc1" TagName="ucPopupZipCode" Src="ucPopupZipCode.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucLookupKabupaten" Src="ucLookupKabupaten.ascx"%>

<script src="../Maxiloan.js" type="text/javascript"></script>
<link href="../include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
<script src="../js/jquery-1.9.1.min.js"></script>
<script src="../js/jquery-ui-1.10.3.custom.min.js"></script>

<div runat="server" id="jlookupContent" />
<div class="form_box_usercontrol"> 
    <div class="form_left_usercontrol">
        <label class="label_split" runat="server" id="lblAlamat">
            Alamat
        </label>
        <asp:TextBox ID="txtAddress" runat="server" TextMode="MultiLine" CssClass="multiline_textbox"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic"
            ErrorMessage="Harap isi Alamat" ControlToValidate="txtAddress" CssClass="validator_general"></asp:RequiredFieldValidator>
    </div>
</div>
<div class="form_box_usercontrol">
    <div class="form_left_usercontrol">
        <label class="label_split">
            Kota</label>                
        <asp:HiddenField runat="server" ID="hdfKodeKotaKabupaten" />           
        <asp:TextBox runat="server" ID="txtKotaKabupaten" CssClass="medium_text"></asp:TextBox>
        <button class="small buttongo blue" onclick="OpenJLookup('<%= ResolveClientUrl("~/webform.Utility/jLookup/Kabupaten.aspx?kode=" & hdfKodeKotaKabupaten.ClientID & "&nama=" & txtKotaKabupaten.ClientID) %>','Daftar Kota/Kabupaten','<%= jlookupContent.ClientID %>');return false;">
            ...</button>             
    </div>
</div>
<div class="form_box_usercontrol" runat="server" id="DivKdPos">
    <div class="form_left_usercontrol">
        <label runat="server" id="lblKodePos">
            Kode Pos</label>
        <asp:TextBox ID="txtZipCode" runat="server" CssClass="small_text"></asp:TextBox>        
    </div>
</div>
<asp:Panel runat="server" ID="pnlTelepon">
    <div class="form_box_usercontrol">
        <div class="form_left_usercontrol">
            <label class="label_split_req" runat="server" id="lblTelepon1">
                Telepon 1
            </label>
            <asp:TextBox ID="txtAreaPhone1" MaxLength="4" runat="server" CssClass="smaller_text" onkeypress="return numbersonly2(event)"></asp:TextBox>
            <asp:Label ID="lblStrip1" runat="server">-</asp:Label>
            <asp:TextBox ID="txtPhone1" MaxLength="10" runat="server" CssClass="small_text" onkeypress="return numbersonly2(event)"></asp:TextBox>
           <%-- <asp:RegularExpressionValidator ID="Regularexpressionvalidator1" runat="server" Display="Dynamic"
                ValidationExpression="[0-9]{0,4}" ErrorMessage="Harap isi Area Telepon1 dengan Angka"
                ControlToValidate="txtAreaPhone1" CssClass="validator_general"></asp:RegularExpressionValidator>
            <asp:RegularExpressionValidator ID="Regularexpressionvalidator2" runat="server" Display="Dynamic"
                ValidationExpression="[0-9]{0,10}" ErrorMessage="Harap isi Telepon dengan Angka"
                ControlToValidate="txtPhone1" CssClass="validator_general"></asp:RegularExpressionValidator>--%>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtAreaPhone1"
                ErrorMessage="Harap isi Area Telepon1" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtPhone1"
                ErrorMessage="harap isi Telepone1" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
        </div>
    </div>
    <div class="form_box_usercontrol">
        <div class="form_left_usercontrol">
            <label class="label_split">
                Telepon 2
            </label>
            <asp:TextBox ID="txtAreaPhone2" MaxLength="4" CssClass="smaller_text" runat="server" onkeypress="return numbersonly2(event)"></asp:TextBox>
            <asp:Label ID="lblStrip2" runat="server">-</asp:Label>
            <asp:TextBox ID="txtPhone2" MaxLength="10" runat="server" CssClass="small_text" onkeypress="return numbersonly2(event)"></asp:TextBox><asp:RegularExpressionValidator
                ID="revAreaPhone2" runat="server" Display="Dynamic" ValidationExpression="[0-9]{0,4}"
                ErrorMessage="Harap isi Area Telepon2 dengan Angka" ControlToValidate="txtAreaPhone2"
                CssClass="validator_general"></asp:RegularExpressionValidator>
            <asp:RegularExpressionValidator ID="revPhone2" runat="server" Display="Dynamic" ValidationExpression="[0-9]{0,10}"
                ErrorMessage="Harap isi Telepon2 dengan angka" ControlToValidate="txtPhone2"
                CssClass="validator_general"></asp:RegularExpressionValidator>
        </div>
    </div>
    <div class="form_box_usercontrol">
        <div class="form_left_usercontrol">
            <label class="label_split">
                Fax
            </label>
            <asp:TextBox ID="txtAreaFax" MaxLength="4" CssClass="smaller_text" runat="server" onkeypress="return numbersonly2(event)"></asp:TextBox>
            <asp:Label ID="lblStrip3" runat="server">-</asp:Label>
            <asp:TextBox ID="txtFax" MaxLength="10" runat="server" CssClass="small_text" onkeypress="return numbersonly2(event)"></asp:TextBox><asp:RegularExpressionValidator
                ID="revAreaFax" runat="server" Display="Dynamic" ValidationExpression="[0-9]{0,4}"
                ErrorMessage="Harap isi Area Fax dengan angka" ControlToValidate="txtAreaFax"
                CssClass="validator_general"></asp:RegularExpressionValidator>
            <asp:RegularExpressionValidator ID="revFax" runat="server" Display="Dynamic" ValidationExpression="[0-9]{0,10}"
                ErrorMessage="Harap isi No Fax dengan angka" ControlToValidate="txtFax" CssClass="validator_general"></asp:RegularExpressionValidator>
        </div>
    </div>

    <%--modify by ario--%>
    <div class="form_box_usercontrol" runat="server" id="divHp">
    <div class="form_left_usercontrol">
        <label runat="server" id="lblHPNo" class="label_split_req" MaxLength="12" onkeypress="return numbersonly2(event)"> No Hand Phone</label>
        <asp:TextBox ID="txtHPhone" runat="server" CssClass="medium_text"></asp:TextBox>     
          <asp:RequiredFieldValidator ID="reqHP" runat="server" ControlToValidate="txtHPhone"
                ErrorMessage="harap isi  No Hand Phone" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" Display="Dynamic" ValidationExpression="[0-9]{0,15}"
                ErrorMessage="Harap isi No HP dengan angka" ControlToValidate="txtHPhone" CssClass="validator_general"></asp:RegularExpressionValidator>
               
    </div>
</div>
</asp:Panel>
