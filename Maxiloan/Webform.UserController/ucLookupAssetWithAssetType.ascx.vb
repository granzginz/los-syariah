﻿
Public Class ucLookupAssetWithAssetType
    Inherits System.Web.UI.UserControl

    Public Property ApplicationId() As String
        Get
            Return CType(ViewState("applicationid"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("applicationid") = Value
        End Set
    End Property

    Public Property AssetCode() As String
        Get
            Return hdnAssetCode.Value
        End Get
        Set(ByVal Value As String)
            hdnAssetCode.Value = Value
        End Set
    End Property

    Public Property Description() As String
        Get
            Return txtDescription.Text
        End Get
        Set(ByVal Value As String)
            txtDescription.Text = Value
        End Set
    End Property

    Public Property AssetTypeId() As String
        Get
            Return hdnAssetTypeID.Value
        End Get
        Set(ByVal Value As String)
            hdnAssetTypeID.Value = Value
        End Set
    End Property
    Public Property BranchID() As String
        Get
            Return hdnBranchID.Value
        End Get
        Set(ByVal Value As String)
            hdnBranchID.Value = Value
        End Set
    End Property
    Public Property Style() As String
        Get
            Return CStr(ViewState("style"))
        End Get
        Set(ByVal Value As String)
            ViewState("style") = Value
        End Set
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        hpLookup.NavigateUrl = "javascript:OpenWinAssetCode('" & hdnAssetCode.ClientID & "','" & txtDescription.ClientID & "', '" & Me.AssetTypeId & "','" & Me.Style & "','" & Me.ApplicationId & "' ,  '" & hdnBranchID.ClientID & "')"
    End Sub

    Public Sub BindData()
        'hdnProductOfferingID.Value = Me.ProductOfferingID
        'txtProductOfferingDescription.Text = Me.ProductOfferingDescription
        'hdnProductID.Value = Me.ProductID
        hpLookup.NavigateUrl = "javascript:OpenWinAssetCode('" & hdnAssetCode.ClientID & "','" & txtDescription.ClientID & "', '" & Me.AssetTypeId & "','" & Me.Style & "','" & Me.ApplicationId & "',  '" & hdnBranchID.ClientID & "')"
    End Sub

End Class