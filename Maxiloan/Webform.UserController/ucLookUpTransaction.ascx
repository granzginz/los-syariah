﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucLookUpTransaction.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.ucLookUpTransaction" %>
<script language="javascript" type="text/javascript">
    function OpenTransaction(pTransaction, pDescription, pIsAgreement, pIsPettyCash, pIsHOTransaction, pProcessID, pStyle, pIsPaymentReceive) {
        var AppInfo = window.location.pathname;
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
        var ServerName = window.location.protocol + '//' + window.location.host + '/';
        var x = screen.width;
        var y = screen.height;
        window.open(ServerName + App + '/General/LookUpTransaction.aspx?Transaction=' + pTransaction + '&Description=' + pDescription + '&IsAgreeMent=' + pIsAgreement + '&IsPettyCash=' + pIsPettyCash + '&IsHOTransaction=' + pIsHOTransaction + '&ProcessID=' + pProcessID + '&Style=' + pStyle + '&IsPaymentReceive=' + pIsPaymentReceive, 'TransactionLookup', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
    }
</script>
<%@ Register src="ucNumberFormat.ascx" tagname="ucNumberFormat" tagprefix="uc1" %>
<%@ Register src="ucDateCE.ascx" tagname="ucDateCE" tagprefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<div runat="server" id="jlookupContent" />
<input type="hidden" id="hdnTransactionID" runat="server" name="hdnSupplierID" class="inptype" />
<input type="hidden" id="hdnIsPaymentReceive" runat="server" name="hdnIsPaymentReceive" class="inptype" />
<input type="hidden" id="hdnIsPettyCash" runat="server" name="hdnIsPettyCash" class="inptype" />
<input type="hidden" id="hdnIsPaymentRequest" runat="server" name="hdnIsPaymentRequest" class="inptype" />
<input type="hidden" id="hdnCOA" runat="server" name="hdnCOA" class="inptype" />
<div class="form_box_usercontrol">
    <div class="form_left_usercontrol">
        <label>
            Alokasi COA / Transaksi
        </label>
        <asp:TextBox ID="txtTransaction" runat="server"
            CssClass="medium_text"></asp:TextBox>
             <button class="small buttongo blue" 
                             onclick ="OpenJLookup('<%= ResolveClientUrl("~/webform.Utility/jLookup/LookUpTransaction.aspx?transactionID=" & hdnTransactionID.ClientID & "&transaction=" & txtTransaction.ClientID & "&isPaymentReceive=" & hdnIsPaymentReceive.Value & "&IsPettyCash=" & hdnIsPettyCash.Value & "&IsPaymentRquest=" & hdnIsPaymentRequest.Value  )%>','Daftar Transaction','<%= jlookupContent.ClientID %>');return false;">...</button>  
        <%--<asp:HyperLink ID="hpLookup" runat="server" ImageUrl="../images/IconDetail.gif"></asp:HyperLink>--%>
        <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" ControlToValidate="txtTransaction"
            ErrorMessage="Harap dipilih Transaksi" Visible="True" Enabled="True" CssClass="validator_general"></asp:RequiredFieldValidator>
    </div>
</div>
<div class="form_box_usercontrol"  runat="server" id="divValutaDate">
    <div class="form_left_usercontrol">
        <label>
            Tanggal Valuta</label>        
        <uc1:ucDateCE id="txtValutaDate" runat="server" />      
    </div>
</div>
<div class="form_box_usercontrol"  runat="server" id="divAmount">
    <div class="form_left_usercontrol">
        <label>
            Jumlah</label>        
        <uc1:ucNumberFormat ID="txtAmount" runat="server" />       
    </div>
</div>
<div class="form_box_usercontrol" runat="server" id="divDesc">
    <div class="form_left_usercontrol">
        <label>
            Keterangan</label>
        <asp:TextBox ID="txtDescription"  runat="server" CssClass="multiline_textbox_uc"
            MaxLength="35" TextMode="MultiLine" ></asp:TextBox>
        <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" ControlToValidate="txtDescription"
            ErrorMessage="Harap isi Keterangan" Visible="True" Enabled="True" CssClass="validator_general"></asp:RequiredFieldValidator>
    </div>
</div>
