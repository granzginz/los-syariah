﻿Imports Maxiloan.Controller

Public Class UcIncentiveInternalGrid
    Inherits ControlBased

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Public Event EventEdit(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
    Private Msg As New Maxiloan.Webform.WebBased
    Private m_Insentif As New RefundInsentifController

    Public WriteOnly Property OnClientClick As String
        Set(ByVal value As String)
            btnAdd.Attributes.Add("OnClick", value)
        End Set
    End Property
    Public WriteOnly Property OnSaveClick As String
        Set(ByVal value As String)
            btnSave.Attributes.Add("OnClick", value)
        End Set
    End Property
    Public Property ApplicationID() As String
        Get
            Return (CType(ViewState("ApplicationID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
    Public Property SupplierID() As String
        Set(ByVal SupplierID As String)
            ViewState("SupplierID") = SupplierID
        End Set
        Get
            Return ViewState("SupplierID").ToString
        End Get
    End Property
    Public Property BranchID() As String
        Set(ByVal BranchID As String)
            ViewState("BranchID") = BranchID
        End Set
        Get
            Return ViewState("BranchID").ToString
        End Get
    End Property
    Public Property BussinessDate() As String
        Set(ByVal BussinessDate As String)
            ViewState("BussinessDate") = BussinessDate
        End Set
        Get
            Return ViewState("BussinessDate").ToString
        End Get
    End Property

    Public Property WhereCond As String
        Set(ByVal WhereCond As String)
            ViewState("WhereCond") = WhereCond
        End Set
        Get
            Return ViewState("WhereCond").ToString
        End Get
    End Property
    Public Property dtg As DataGrid
        Get
            Return dtgIncentive
        End Get
        Set(ByVal dtg As DataGrid)
            dtgIncentive = dtg
        End Set
    End Property
    Public Property AlokasiPremiAsuransiInternal As String
        Set(ByVal AlokasiPremiAsuransiInternal As String)
            ViewState("AlokasiPremiAsuransiInternal") = AlokasiPremiAsuransiInternal
        End Set
        Get
            Return ViewState("AlokasiPremiAsuransiInternal").ToString
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        txtSupplier.Text = Me.SupplierID
        txtApplicationID.Text = Me.ApplicationID
        txtBranchID.Text = Me.BranchID
        txtBussinessDate.Text = Me.BussinessDate
    End Sub
    'Private Sub dtgIncentive_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgIncentive.ItemCommand
    '    If e.CommandName = "Delete" Then
    '        DeleteBaris(e.Item.ItemIndex)
    '    End If
    'End Sub
    Public Sub AddRecord()
        Dim objectDataTable As New DataTable
        Dim intLoopGrid As Integer
        Dim oRow As DataRow
        Dim lblNomor As New Label
        Dim ddlPenggunaan As New DropDownList
        Dim txtProsentase As New TextBox
        Dim txtInsentif As New TextBox
        Dim plus As Integer = 0
        Dim imgDelete As New ImageButton



        With objectDataTable
            .Columns.Add(New DataColumn("Nomor", GetType(String)))
            .Columns.Add(New DataColumn("Penggunaan", GetType(String)))
            .Columns.Add(New DataColumn("Prosentase", GetType(String)))
            .Columns.Add(New DataColumn("Insentif", GetType(String)))
        End With

        For intLoopGrid = 0 To dtgIncentive.Items.Count - 1
            lblNomor = CType(dtgIncentive.Items(intLoopGrid).FindControl("lblNomor"), Label)
            ddlPenggunaan = CType(dtgIncentive.Items(intLoopGrid).FindControl("ddlPenggunaan"), DropDownList)
            txtProsentase = CType(dtgIncentive.Items(intLoopGrid).FindControl("txtProsentase"), TextBox)
            txtInsentif = CType(dtgIncentive.Items(intLoopGrid).FindControl("txtInsentif"), TextBox)

            '----- Add row -------'
            oRow = objectDataTable.NewRow()
            oRow("Nomor") = CType(lblNomor.Text, String)
            oRow("Penggunaan") = CType(ddlPenggunaan.SelectedValue, String)
            oRow("Prosentase") = CType(txtProsentase.Text, String)
            oRow("Insentif") = CType(txtInsentif.Text, String)

            objectDataTable.Rows.Add(oRow)


        Next

        oRow = objectDataTable.NewRow()
        oRow("Nomor") = dtgIncentive.Items.Count + 1
        oRow("Penggunaan") = ""
        oRow("Prosentase") = ""
        oRow("Insentif") = ""

        objectDataTable.Rows.Add(oRow)

        dtgIncentive.DataSource = objectDataTable
        dtgIncentive.DataBind()

        ' BindDDl()

        For intLoopGrid = 0 To dtgIncentive.Items.Count - 1
            imgDelete = CType(dtgIncentive.Items(intLoopGrid).FindControl("imgDelete"), ImageButton)
            lblNomor = CType(dtgIncentive.Items(intLoopGrid).FindControl("lblNomor"), Label)
            ddlPenggunaan = CType(dtgIncentive.Items(intLoopGrid).FindControl("ddlPenggunaan"), DropDownList)
            txtProsentase = CType(dtgIncentive.Items(intLoopGrid).FindControl("txtProsentase"), TextBox)
            txtInsentif = CType(dtgIncentive.Items(intLoopGrid).FindControl("txtInsentif"), TextBox)

            lblNomor.Text = objectDataTable.Rows(intLoopGrid).Item(0).ToString.Trim

            If objectDataTable.Rows(intLoopGrid).Item(1).ToString <> "" Then
                ddlPenggunaan.SelectedIndex = ddlPenggunaan.Items.IndexOf(ddlPenggunaan.Items.FindByValue(objectDataTable.Rows(intLoopGrid).Item(1).ToString.Trim))
            End If

            txtProsentase.Text = objectDataTable.Rows(intLoopGrid).Item(2).ToString.Trim
            txtInsentif.Text = objectDataTable.Rows(intLoopGrid).Item(3).ToString.Trim

            txtProsentase.Attributes.Add("OnChange", "handletxtProsentaseInternal_Change(this.value,'" & Me.AlokasiPremiAsuransiInternal & "','" & txtInsentif.ClientID & "')")
            txtInsentif.Attributes.Add("OnChange", "handletxtInsentifInternal_Change(this.value,'" & Me.AlokasiPremiAsuransiInternal & "','" & txtProsentase.ClientID & "')")

            imgDelete.Attributes.Add("OnClick", "return deleteRowInternal('" & dtgIncentive.ClientID & "',this);")
        Next
    End Sub
    Public Sub BindDDl(ByVal tipe As String, ByVal index As Integer)
        Dim dtEntity As New DataTable
        Dim oCustom As New Parameter.RefundInsentif
        Dim ddlPenggunaan As New DropDownList
        ddlPenggunaan = CType(dtgIncentive.Items(index).FindControl("ddlPenggunaan"), DropDownList)

        With oCustom
            .strConnection = GetConnectionString()
            .WhereCond = ""
            If tipe <> "" Then
                .WhereCond = " TransID in (select TransID from tblAgreementMasterTransaction WHERE GroupTransID = '" & tipe & "') "
            End If

            .SPName = "spGetPenggunaanInsentif"
        End With

        oCustom = m_Insentif.GetSPBy(oCustom)
        dtEntity = oCustom.ListDataTable

        ddlPenggunaan.DataSource = dtEntity.DefaultView
        ddlPenggunaan.DataTextField = "Name"
        ddlPenggunaan.DataValueField = "TransID"
        ddlPenggunaan.DataBind()
        ddlPenggunaan.SelectedIndex = 0
        ddlPenggunaan.Items.Insert(0, "Select One")
        ddlPenggunaan.Items(0).Value = ""

    End Sub
    Public Sub BindTotal()
        Dim A_PremiAsuransi As Decimal = CDec(Me.AlokasiPremiAsuransiInternal)
        Dim totalInsentif As Decimal = 0

        For i = 0 To dtgIncentive.Items.Count - 1
            Dim Grid_txtInsentif As New TextBox

            Grid_txtInsentif = CType(dtgIncentive.Items(i).FindControl("txtInsentif"), TextBox)
            totalInsentif = totalInsentif + CDec(IIf(Grid_txtInsentif.Text.Trim = "", "0", Grid_txtInsentif.Text))
        Next

        If totalInsentif > 0 And A_PremiAsuransi > 0 Then
            ScriptManager.RegisterStartupScript(dtgIncentive, GetType(DataGrid), dtgIncentive.ClientID, String.Format(" document.getElementById('{0}' + '_txtTotalProsentase').value = '" + FormatNumber((totalInsentif / A_PremiAsuransi) * 100, 2).ToString + "';  document.getElementById('{0}' + '_txtTotalInsentif').value = '" + FormatNumber(totalInsentif, 0).ToString + "'; ", dtgIncentive.ClientID), True)
        End If

        'If totalInsentif > A_PremiAsuransi Then
        '    Msg.ShowMessage(lblMessage, " Total Insentif : " & FormatNumber(totalInsentif, 0).ToString & " tidak boleh lebih dari : " & FormatNumber(A_PremiAsuransi, 0).ToString & "", True)
        'Else
        '    lblMessage.Visible = False
        'End If
    End Sub
End Class