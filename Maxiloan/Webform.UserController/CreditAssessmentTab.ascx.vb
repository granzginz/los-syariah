﻿Imports System.Data.SqlClient
Public Class CreditAssessmentTab
    Inherits Maxiloan.Webform.ControlBased


    Public Property ApplicationID As String
    Private Property CustomerID As String
    Private Property SupplierGroupID As String

    Public Sub setLink(Optional enableRek As Boolean = False)
        'Dim objCommand As New SqlCommand
        'Dim objConnection As New SqlConnection(GetConnectionString)
        'Dim objReader As SqlDataReader

        'Try
        '    If objConnection.State = ConnectionState.Closed Then objConnection.Open()

        '    objCommand.CommandType = CommandType.StoredProcedure
        '    objCommand.CommandText = "spGetApplicationTab"
        '    objCommand.Parameters.Add("@ApplicationID", SqlDbType.VarChar, 20).Value = Me.ApplicationID.Trim
        '    objCommand.Connection = objConnection
        '    objReader = objCommand.ExecuteReader()

        'If objReader.Read Then
        'Me.CustomerID = objReader.Item("CustomerID").ToString
        'Me.SupplierGroupID = "" 'objReader.Item("SupplierGroupID").ToString

        'If Not IsDBNull(objReader.Item("DateEntryIncentiveData")) Then
        hyVerifikasi.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/CreditAssesment.aspx?id=" & Me.CustomerID & "&appid=" & Me.ApplicationID & "&page=Edit"

        If (enableRek = False) Then
            hyRekening.NavigateUrl = "" 
        Else
            hyRekening.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/CreditAssesment_002.aspx?id=" & Me.CustomerID & "&appid=" & Me.ApplicationID & "&page=Edit"
        End If
        hyRekening.Enabled = enableRek


        'ElseIf Not IsDBNull(objReader.Item("DateEntryFinancialData")) Then
        'hyVerifikasi.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditAssesment.aspx?id=" & Me.CustomerID & "&appid=" & Me.ApplicationID & "&page=Edit"
        'hyRekening.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditAssesment_002.aspx?id=" & Me.CustomerID & "&appid=" & Me.ApplicationID & "&page=Edit"
        'End If
        'End If

        '    objReader.Close()
        'Catch ex As Exception
        '    Throw New Exception(ex.Message)
        'Finally
        '    If objConnection.State = ConnectionState.Open Then objConnection.Close()
        '    objConnection.Dispose()
        '    objCommand.Dispose()
        'End Try
    End Sub

    Public Sub selectedTab(ByVal strTab As String)
        Select Case strTab
            Case "VerifikasiPhone"
                tabVerifikasiPhone.Attributes.Add("class", "tab_selected")
                tabRekening.Attributes.Add("class", "tab_notselected")
            Case "Rekening"
                tabVerifikasiPhone.Attributes.Add("class", "tab_notselected")
                tabRekening.Attributes.Add("class", "tab_selected")
        End Select
    End Sub

End Class