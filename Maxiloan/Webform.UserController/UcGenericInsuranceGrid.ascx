﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UcGenericInsuranceGrid.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.UcGenericInsuranceGrid" %>
<p>
    <asp:RangeValidator ID="rgvGo" runat="server" ErrorMessage="No Halaman Salah"
        MinimumValue="1" Type="integer" ControlToValidate="txtGoPage"></asp:RangeValidator></p>
<p>
    <asp:DataGrid ID="DtgPaging" runat="server" HorizontalAlign="Center">
    </asp:DataGrid></p>
<p>
    <table id="Table5" height="25" cellspacing="0" cellpadding="0" width="95%" align="center"
        border="0">
        <tr>
            <td align="center" width="30">
                <asp:ImageButton ID="imbFirstPage" runat="server" CommandName="First" OnCommand="NavigationLink_Click"
                    CausesValidation="False" ImageUrl="../Images/butkiri1.gif"></asp:ImageButton>
            </td>
            <td align="center" width="30">
                <asp:ImageButton ID="imbPrevPage" runat="server" CommandName="Prev" OnCommand="NavigationLink_Click"
                    CausesValidation="False" ImageUrl="../Images/butkiri.gif"></asp:ImageButton>
            </td>
            <td align="center" width="30">
                <asp:ImageButton ID="imbNextPage" runat="server" CommandName="Next" OnCommand="NavigationLink_Click"
                    CausesValidation="False" ImageUrl="../Images/butkanan.gif"></asp:ImageButton>
            </td>
            <td align="center" width="30">
                <asp:ImageButton ID="imbLastPage" runat="server" CommandName="Last" OnCommand="NavigationLink_Click"
                    CausesValidation="False" ImageUrl="../Images/butkanan1.gif"></asp:ImageButton>
            </td>
            <td style="width: 163px" align="center" width="163">
                Page
                <asp:TextBox ID="txtGoPage" runat="server" MaxLength="3"  Width="34px"></asp:TextBox>
                <asp:ImageButton ID="imbGoPage" runat="server" ImageUrl="../Images/butgo.gif" ImageAlign="AbsBottom"
                    EnableViewState="False"></asp:ImageButton>
            </td>
            <td align="right">
                <font color="#999999">Page
                    <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                    <asp:Label ID="lblTotRec" runat="server"></asp:Label>&nbsp;record(s)</font>
            </td>
        </tr>
    </table>
</p>
