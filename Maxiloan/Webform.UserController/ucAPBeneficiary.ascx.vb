﻿Imports Maxiloan.General.CommonCacheHelper

Public Class ucAPBeneficiary
    Inherits ControlBased

#Region "Properti"
    Public Property AccountName() As String
        Get
            Return CType(viewstate("AccountName"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AccountName") = Value
        End Set
    End Property
    Public Property BankName() As String
        Get
            Return CType(viewstate("BankName"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("BankName") = Value
        End Set
    End Property
    Public Property AccountNumber() As String
        Get
            Return CType(viewstate("AccountNumber"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AccountNumber") = Value
        End Set
    End Property
    Public Property BankBranch() As String
        Get
            Return CType(viewstate("BankBranch"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("BankBranch") = Value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Public Sub BindAPBeneficiary()
        lblAccName.Text = Me.AccountName
        lblAccNo.Text = Me.AccountNumber
        lblBankBran.Text = Me.BankBranch
        lblBankName.Text = Me.BankName
    End Sub

End Class