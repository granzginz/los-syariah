﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucLookupGroupCust.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.ucLookupGroupCust" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%--<style type="text/css">
    .wp
    {
        background-color: #fff;
        width: 700px;        
        border: 1px solid #ccc;
        float: left;
    }
    
    .wp h4
    {
        margin: 0;
        color: #000;
        text-align: left;
    }
    
    .wp .gridwp
    {
        max-height: 300px;
        overflow: auto;
    }
    
    .wpbg
    {
        background-color: #000;
        filter: alpha(opacity=70);
        opacity: 0.7;
    }
    
    .wpbtnexit
    {
        position: absolute;
        float: right;
        top: 0;
        right: 0;
    }
</style>--%>
<asp:UpdatePanel runat="server" ID="updatePanel1">
    <ContentTemplate>
        <asp:Button runat="server" ID="btnLookup" Text="btnLookup" Style="display: none;" />
        <asp:ModalPopupExtender ID="mpLookupGroupCust" runat="server" TargetControlID="btnLookup"
            PopupControlID="Panel1" BackgroundCssClass="wpbg" CancelControlID="btnExit">
        </asp:ModalPopupExtender>
        <asp:Panel ID="Panel1" runat="server">
            <div class="wp">
                <asp:ImageButton runat="server" ID="btnExit" ImageUrl="../Images/exit_lookup00.png"
                    CssClass="wpbtnexit" />
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
                <div class="form_box_title">
                    <div class="title_strip">
                    </div>
                    <div class="form_single">
                        <h4>
                            KELOMPOK USAHA
                        </h4>
                    </div>
                </div>
                <asp:Panel runat="server" ID="pnlList">
                    <div class="form_box_header">
                        <div class="form_single">
                            <div class="grid_wrapper_ns">
                                <asp:DataGrid ID="dtGroupCust" runat="server" OnSortCommand="SortGrid" BorderStyle="None"
                                    DataKeyField="CustomerGroupID" AutoGenerateColumns="False" AllowSorting="True"
                                    AllowPaging="True" Visible="true" Width="100%" CssClass="gridwp">
                                    <HeaderStyle CssClass="th" />
                                    <ItemStyle CssClass="item_grid" />
                                    <Columns>
                                        <asp:ButtonColumn Text="Select" CommandName="Select" ButtonType="LinkButton" ItemStyle-CssClass="command_col">
                                        </asp:ButtonColumn>
                                        <asp:BoundColumn DataField="CustomerGroupNm" SortExpression="CustomerGroupNm" HeaderText="CUSTOMER GROUP">
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="CustomerGroupID" HeaderText="ID" Visible="false"></asp:BoundColumn>
                                    </Columns>
                                    <PagerStyle Visible="false" />
                                </asp:DataGrid>
                            </div>
                            <div class="button_gridnavigation">
                                <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" ImageUrl="../Images/grid_navbutton01.png"
                                    OnCommand="NavigationLink_Click" CommandName="First"></asp:ImageButton>
                                <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" ImageUrl="../Images/grid_navbutton02.png"
                                    OnCommand="NavigationLink_Click" CommandName="Prev"></asp:ImageButton>
                                <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" ImageUrl="../Images/grid_navbutton03.png"
                                    OnCommand="NavigationLink_Click" CommandName="Next"></asp:ImageButton>
                                <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" ImageUrl="../Images/grid_navbutton04.png"
                                    OnCommand="NavigationLink_Click" CommandName="Last"></asp:ImageButton>
                                Page
                                <asp:TextBox ID="txtGoPage" CssClass="gopage" runat="server">1</asp:TextBox>
                                <asp:Button ID="btnGoPage" runat="server" EnableViewState="False" Text="Go" CssClass="small buttongo blue">
                                </asp:Button>
                                <asp:RangeValidator ID="rgvGo" runat="server" Type="Integer" MaximumValue="999999999"
                                    ErrorMessage="Page No. is not valid" MinimumValue="1" ControlToValidate="txtGoPage"></asp:RangeValidator>
                                <asp:RequiredFieldValidator ID="rfvGo" runat="server" ErrorMessage="Page No. is not valid"
                                    ControlToValidate="txtGoPage" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                            </div>
                            <div class="label_gridnavigation">
                                <asp:Label ID="lblPage" runat="server"></asp:Label>of
                                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                                <asp:Label ID="lblTotRec" runat="server"></asp:Label>record(s)
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Find By</label>
                        <asp:DropDownList ID="cboSearchBy" runat="server">
                            <asp:ListItem Value="CustomerGroupNm">Customer Group Name</asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtSearchBy" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="btnSearch" runat="server" CausesValidation="False" Text="Find" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="btnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
                    </asp:Button>
                </div>
            </div>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
