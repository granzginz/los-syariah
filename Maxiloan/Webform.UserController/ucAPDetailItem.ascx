﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucAPDetailItem.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.ucAPDetailItem" %>
<%@ Register src="../webform.UserController/ucNumberFormat.ascx" tagname="ucNumberFormat" tagprefix="uc1" %>
<script src="../../Maxiloan.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript">
    var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';
    var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
    var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
    function OpenViewInsCoBranch(pInsCoID, pInsCoBranchID, pStyle) {
        window.open(ServerName + App + '/Webform.Asuransi/Setting/InsCoView.aspx?InsCoID=' + pInsCoID + '&InsCoBranchID=' + pInsCoBranchID + '&Style=' + pStyle, 'InsuranceCo', 'left=15, top=10, width=985, height=350, menubar=0, scrollbars=1')
    }		
</script>
<div class="form_box">
    <div class="form_left">
        <label>
            Dibayar Kepada</label>
        <asp:Label ID="lblAPTo" runat="server"></asp:Label>
    </div>
    <div class="form_right">
        <label>
            Tanggal Rencana Bayar
        </label>
        <asp:Label ID="lblDueDate" runat="server"></asp:Label>
    </div>
</div>
<div class="form_box_header">
    <div class="form_single">
        <div class="grid_wrapper_ns">
            <asp:DataGrid ID="dtgAP" runat="server" Width="100%" CssClass="grid_general" BorderStyle="None"
                BorderWidth="0" AutoGenerateColumns="False" ShowFooter="true">
                <HeaderStyle CssClass="th" />
                <ItemStyle CssClass="item_grid" />
                <FooterStyle CssClass="item_grid" />
                <Columns>
                    <asp:TemplateColumn HeaderText="DETAIL A/P">
                        <ItemTemplate>
                            <asp:HyperLink ID="lnkAPDetailDesc" runat="server" CommandName="apno" Text='<%# DataBinder.Eval(Container, "DataItem.AccountPayableDescription") %>'>
                            </asp:HyperLink>
                            <asp:Label ID="lnkAPDeta" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.AccountPayableNo") %>'>
                            </asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            Total
                        </FooterTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="SALDO A/P" ItemStyle-CssClass="item_grid_right" FooterStyle-CssClass="item_grid_right" HeaderStyle-CssClass="item_grid_right">
                        <ItemTemplate>
                            <asp:Label ID="lblAPBala" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.APBalance") %>'>
                            </asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="lblSumBalance" runat="server"></asp:Label>
                        </FooterTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="JUMLAH DIBAYAR" ItemStyle-CssClass="item_grid_right"  FooterStyle-CssClass="item_grid_right" HeaderStyle-CssClass="item_grid_right">
                        <ItemTemplate>
                            <asp:Label ID="lblPaymAmou" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PaymentAmount") %>'>
                            </asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="lblSumAmount" runat="server" ></asp:Label>
                        </FooterTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn  ItemStyle-CssClass="item_grid_right"  FooterStyle-CssClass="item_grid_right" HeaderStyle-CssClass="item_grid_right">
                        <HeaderTemplate >
                            <asp:Label ID="lblJumlahAdjust" runat="server" Text="JUMLAH DIBAYAR ADJUST"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <uc1:ucNumberFormat ID="txtPaymentAmount" runat="server"  />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="NO INVOICE">
                        <ItemTemplate>
                            <asp:Label ID="Label3" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.InvoiceNo") %>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="TGL INVOICE">
                        <ItemTemplate>
                            <asp:Label ID="lblInvoDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.InvoiceDate") %>'>
                            </asp:Label>
                            <asp:Label ID="lblAssetSeqNo" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.AssetSeqNo") %>'>
                            </asp:Label>
                            <asp:Label ID="lblAssetRepSeqNo" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.AssetRepSeqNo") %>'>
                            </asp:Label>
                            <asp:Label ID="lblCustID" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.CustomerID") %>'>
                            </asp:Label>
                            <asp:Label ID="lblReffNo" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.ReferenceNo") %>'>
                            </asp:Label>
                            <asp:Label ID="lblMaskAssID" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.MaskAssID") %>'>
                            </asp:Label>
                            <asp:Label ID="lblApplicationID" runat="server" Visible="false" Text='<%#Container.DataItem("ApplicationID")%>'>
                            </asp:Label>
                            <asp:Label ID="lblInsBranchID" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.InsuranceBranchID") %>'>
                            </asp:Label>
                            <asp:Label ID="lblSupplierID" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.SupplierID") %>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                </Columns>
            </asp:DataGrid>
        </div>
    </div>
</div>
