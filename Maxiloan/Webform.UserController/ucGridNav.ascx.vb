﻿Public Delegate Sub TextChangedHandler(ByVal sender As Object, ByVal e As PageChangedEventArgs)
Public Class ucGridNav
    Inherits UserControl
    Protected _currentPage As Integer
    Protected _recordCount As Integer
    Protected _pageSize As Integer
    Protected _totalPage As Integer


    Private _branchId As String
    Private _type As Integer
    Private _opW As Integer
    Private _where As String

    Private _isFind As Boolean = False


    Public Event PageChanged As TextChangedHandler
    Public ReadOnly Property TotalPage As Integer
        Get
            Return _totalPage
        End Get
    End Property

    Public ReadOnly Property CurrentPage As Integer
        Get
            Return _currentPage
        End Get
    End Property


    Protected Sub NavigationFirstLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        updateState()
        _currentPage = 1 
        rEvent()
    End Sub
    Protected Sub NavigationPrevLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        updateState()
        _currentPage -= 1

        rEvent()
    End Sub
    Protected Sub NavigationNextLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        updateState()
        _currentPage += 1
        rEvent()
    End Sub
    Protected Sub NavigationLastLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        updateState()
        _currentPage = _totalPage 
        rEvent()
    End Sub
    Private Sub btnPageNumb_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPageNumb.Click
        updateState()
        _currentPage = CType(PageToGo.Text, Integer)
        rEvent()
    End Sub
    Private Sub rEvent()
        RaiseEvent PageChanged(New Object(), New PageChangedEventArgs(_currentPage, _recordCount, _totalPage, _branchId, _type, _opW, _where, _isFind))
    End Sub

    Public Sub Initialize(recordCount As Integer, pageSize As Integer)
        _currentPage = 1
        _recordCount = recordCount
        _pageSize = pageSize
        _totalPage = CType(Math.Ceiling(recordCount / pageSize), Integer)
        If (_totalPage = 0) Then _totalPage = 1
        RangeValidator.MaximumValue = _totalPage.ToString()
        checkState()
    End Sub

    Public Sub Initialize(recordCount As Integer, pageSize As Integer, branchId As String, type As Integer, opW As Integer, where As String, isFind As Boolean)
        _branchId = branchId
        _type = type
        _opW = opW
        _where = where
        _isFind = isFind
        Initialize(recordCount, pageSize) 
    End Sub

    Public Sub ReInitialize(currentPage As Integer, recordCount As Integer, totalPage As Integer)
        _currentPage = currentPage
        _recordCount = recordCount
        _totalPage = totalPage
         
        checkState()
    End Sub


    Public Sub ReInitialize(currentPage As Integer, recordCount As Integer, totalPage As Integer, branchId As String, type As Integer, opW As Integer, where As String, isFind As Boolean)
          
        _branchId = branchId
        _type = type
        _opW = opW
        _where = where
        _isFind = isFind


        ReInitialize(currentPage, recordCount, totalPage)
    End Sub


    Sub updateState()
        _currentPage = Integer.Parse(lblPage.Text)
        _recordCount = Integer.Parse(lblrecord.Text)
        _totalPage = Integer.Parse(lblTotPage.Text)

        _branchId = hdBranch.Value
        _type = Integer.Parse(hdType.Value)
        _opW = Integer.Parse(hdWOpt.Value)
        _where = hdWhere.Value
        _isFind = Boolean.Parse(hdIsFind.Value)


    End Sub

    Private ReadOnly Property PreviousNav As ImageButton
        Get
            Return imbPrevPage
        End Get
    End Property

    Private ReadOnly Property FirstNav As ImageButton
        Get
            Return imbFirstPage
        End Get
    End Property

    Private ReadOnly Property NextNav As ImageButton
        Get
            Return imbNextPage
        End Get
    End Property

    Private ReadOnly Property LastNav As ImageButton
        Get
            Return imbLastPage
        End Get
    End Property

    Private ReadOnly Property PageToGo As TextBox
        Get
            Return txtPage
        End Get
    End Property
    Private ReadOnly Property RangeValidator As RangeValidator
        Get
            Return rgvGo
        End Get
    End Property
    Private ReadOnly Property Go As Button
        Get
            Return btnPageNumb
        End Get
    End Property

    Private Sub SetGridNavInfo() 'curPage As String, totalPage As String, rowCount As String)
        lblPage.Text = _currentPage.ToString()
        lblTotPage.Text = _totalPage.ToString()
        lblrecord.Text = _recordCount.ToString()
        RangeValidator.MaximumValue = _totalPage.ToString()



        hdBranch.Value = _branchId
        hdType.Value = _type.ToString
        hdWOpt.Value = _opW.ToString()
        hdWhere.Value = _where
        hdIsFind.Value = _isFind.ToString()

    End Sub

    Private Sub checkState()
        PageToGo.Text = _currentPage.ToString()
        SetGridNavInfo()

        If _totalPage = 1 Then
            PreviousNav.Enabled = False
            FirstNav.Enabled = False
            NextNav.Enabled = False
            LastNav.Enabled = False
            PageToGo.Enabled = False
            Go.Visible = False
            Return
        End If
        PageToGo.Enabled = _totalPage > 1
        Go.Visible = _totalPage > 1
        PreviousNav.Enabled = _currentPage > 1
        FirstNav.Enabled = _currentPage > 1
        NextNav.Enabled = (_currentPage <> _totalPage)
        LastNav.Enabled = (_currentPage <> _totalPage)
    End Sub



End Class

Public Class PageChangedEventArgs
    Inherits EventArgs
    Private _currentPage As Integer
    Private _recordCount As Integer
    Private _totalPage As Integer


    Private _branchId As String
    Private _type As Integer
    Private _opW As Integer
    Private _where As String

    Private _isFind As Boolean = False

    Public Sub New(cPage As Integer, rCount As Integer, tPage As Integer)
        _currentPage = cPage
        _recordCount = rCount
        _totalPage = tPage
    End Sub 


    Public Sub New(cPage As Integer, rCount As Integer, tPage As Integer, branch As String, tp As Integer, opW As Integer, where As String, isf As Boolean)
        _currentPage = cPage
        _recordCount = rCount
        _totalPage = tPage
        _branchId = branch
        _type = tp
        _opW = opW
        _where = where
        _isFind = isf
    End Sub


    Public ReadOnly Property CurrentPage As Integer
        Get
            Return _currentPage
        End Get
    End Property
    Public ReadOnly Property RecordCount As Integer
        Get
            Return _recordCount
        End Get
    End Property

    Public ReadOnly Property TotalPage As Integer
        Get
            Return _totalPage
        End Get
    End Property


    Public ReadOnly Property IsFind As Boolean
        Get
            Return _isFind
        End Get
    End Property

    Public ReadOnly Property BranchId As String
        Get
            Return _branchId
        End Get
    End Property
     
    Public ReadOnly Property Type As Integer
        Get
            Return _type
        End Get
    End Property

    Public ReadOnly Property WhereOption As Integer
        Get
            Return _opw
        End Get
    End Property

    Public ReadOnly Property Where As String
        Get
            Return _where
        End Get
    End Property
End Class

