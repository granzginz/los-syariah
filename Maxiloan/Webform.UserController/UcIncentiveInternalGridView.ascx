﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UcIncentiveInternalGridView.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.UcIncentiveInternalGridView" %>
<%@ Register Src="ucNumberFormat.ascx" TagName="ucNumberFormat" TagPrefix="uc1" %>
<asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
    onclick="hideMessage();"></asp:Label>
<script type="text/javascript">
    function deleteRowInternal(ClientID, row) {
        var i = row.parentNode.parentNode.rowIndex;
        document.getElementById(ClientID).deleteRow(i);
        totalInternal(ClientID);
    }
    var handletxtProsentaseInternal_Change = function (x, n, NilID) {
        var nilai;
        var SplitTbl = NilID.split("_");

        if (SplitTbl[0] == "ucTitipanPremiAsuransi") {
            n = $('#lblAlokasiPremiAsuransiInternal').html();
            nilai = parseFloat(n.replace(/\s*,\s*/g, '')) * (x / 100);
        } else {
            nilai = parseFloat(n.replace(/\s*,\s*/g, '')) * (x / 100);
        }
        $('#' + NilID).val(number_format(parseInt((nilai * 10) / 10)));
        totalInternal(NilID);
    }
    var handletxtInsentifInternal_Change = function (x, n, PersenID) {
        var nilai;
        var SplitTbl = PersenID.split("_");

        if (SplitTbl[0] == "ucTitipanPremiAsuransi") {
            n = $('#lblAlokasiPremiAsuransiInternal').html();
            nilai = x / (parseInt(n.replace(/\s*,\s*/g, '')) / 100);

        } else {
            nilai = x / (parseInt(n.replace(/\s*,\s*/g, '')) / 100);
        }
        $('#' + PersenID).val(number_format(nilai, 2));
        totalInternal(PersenID);
    }
    function totalInternal(tableID) {

        var spliIDTable = tableID.split("_");
        var tbl = spliIDTable[0] + "_" + spliIDTable[1];

        var grd = document.getElementById(tbl);
        var row = grd.rows.length - 1;

        var totalP = 0;
        var totalI = 0;
        var totalPID;
        var totalIID;
        $('#' + tbl).find('tr').each(function (i, el) {
            if (i > 0) {
                var $tds = $(this).find('td');
                var persentase_ = $tds[2].getElementsByTagName('input')[0];
                var persentase = persentase_.value;
                var insentif_ = $tds[3].getElementsByTagName('input')[0];
                var insentif = insentif_.value;

                if (persentase == "") {
                    persentase = "0";
                }
                if (insentif == "") {
                    insentif = "0";
                }
                if (i < row) {
                    totalP = parseFloat(totalP) + parseFloat(persentase.replace(/\s*,\s*/g, ''));
                    totalI = parseFloat(totalI) + parseFloat(insentif.replace(/\s*,\s*/g, ''));
                }
                if (i == row) {
                    totalPID = persentase_.id;
                    totalIID = insentif_.id;
                }
            }
        });
        $('#' + totalPID).val(number_format(totalP, 2));
        $('#' + totalIID).val(number_format(totalI));
        if (totalP > 100) {
            $('#' + totalPID).attr('style', 'color:red');
        } else {
            $('#' + totalPID).removeAttr('style');
        }
        var lblalokasi = getlblAlokasiInternal(tbl);
        if (totalI > lblalokasi) {
            $('#' + totalIID).attr('style', 'color:red');
        } else {
            $('#' + totalIID).removeAttr('style');
        }
    }
    function number_format(number, decimals, dec_point, thousands_sep) {

        number = (number + '')
            .replace(/[^0-9+\-Ee.]/g, '');
        var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function (n, prec) {
                var k = Math.pow(10, prec);
                return '' + (Math.round(n * k) / k)
                .toFixed(prec);
            };
        // Fix for IE parseFloat(0.55).toFixed(0) = 0;
        s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
            .split('.');
        if (s[0].length > 3) {
            s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
        }
        if ((s[1] || '')
            .length < prec) {
            s[1] = s[1] || '';
            s[1] += new Array(prec - s[1].length + 1)
              .join('0');
        }
        return s.join(dec);
    }
    function getlblAlokasiInternal(grd) {
        var tbl = {};
        tbl["ucTitipanPremiAsuransi_dtgIncentive"] = "lblAlokasiPremiAsuransiInternal";
        tbl["UcTitipanRefundBunga_dtgIncentive"] = "lblTitipanRefundBunga";
        tbl["UcTitipanProvisi_dtgIncentive"] = "lblTitipanProvisi";
        tbl["UcTitipanBiayalainnya_dtgIncentive"] = "lblTitipanBiayaLainnya";

        var lbl = $('#' + tbl[grd]).html();
        return parseInt(lbl.replace(/\s*,\s*/g, ''));

    }
    function ApplyInternal(tbl) {
        var a = tbl.split("_");
        var uc = a[0];
        $('#' + uc + '_pesan').html("Mohon tunggu, Sedang dalam proses!");

        var stat = true;
        var grd = document.getElementById(tbl);
        var row = grd.rows.length - 1;
        TrnID = getTransIDInternal(tbl);
        $('#' + tbl).find('tr').each(function (i, el) {
            if (i > 0 && i < row) {
                var $tds = $(this).find('td');

                var penggunaan = $tds[1].getElementsByTagName('select')[0];
                var persen = $tds[2].getElementsByTagName('input')[0];
                var insentif = $tds[3].getElementsByTagName('input')[0];

                var idpenggunaan = penggunaan.id;
                var idpersen = persen.id;
                var idinsentif = insentif.id;

                $('#' + idpenggunaan).attr('style', 'background-color:white;width:250px');
                $('#' + idpersen).attr('style', 'background-color:white');
                $('#' + idinsentif).attr('style', 'background-color:white');


                if (penggunaan.value == "") {
                    stat = false;
                    $('#' + idpenggunaan).attr('style', 'background-color:red;width:250px');
                }

                if (persen.value == "" || persen.value == 0) {
                    stat = false;
                    $('#' + idpersen).attr('style', 'background-color:red');
                }
                if (insentif.value == "" || persen.value == 0) {
                    stat = false;
                    $('#' + idinsentif).attr('style', 'background-color:red');
                }

                var paramArg = JSON.stringify({
                    penggunaan: penggunaan.value,
                    persen: persen.value,
                    insentif: insentif.value,
                    TransID: penggunaan.value,
                    seq: i
                });
                $.ajax({
                    url: "IncentiveData.aspx/ApplyInternal",
                    data: paramArg,
                    type: "POST",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    async: false,
                    success: function (dt) {

                    }
                });

            } else if (i == row) {
                var lblTotal = getlblAlokasiInternal(tbl);
                var $tds = $(this).find('td');

                var txtTotal = $tds[3].getElementsByTagName('input')[0];
                var valTotal = txtTotal.value;
                var idTotal = txtTotal.id;

                if (lblTotal != valTotal.replace(/\s*,\s*/g, '')) {
                    $('#' + idTotal).attr('style', 'color:red');
                    stat = false;
                } else {
                    $('#' + idTotal).removeAttr('style');
                }

            }
        });
        if (stat == true) {
            var paramArg2 = JSON.stringify({ GrouptransID: TrnID });
            $.ajax({
                url: "IncentiveData.aspx/SaveApplyInternal",
                type: "POST",
                data: paramArg2,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                async: false,
                success: function (response) {
                    if (response.d == "D") {
                        $('#ucApplicationTab1_hypHasilSurvey').attr('href', 'HasilSurvey.aspx?CustomerID=<%= SupplierID %>&ApplicationID=<%= ApplicationID %>');
                    } else {
                        $('#ucApplicationTab1_hypHasilSurvey').removeAttr('href');
                    }
                    $('#' + uc + '_pesan').html('Proses Berhasil, Terima kasih!');
                    $('#' + uc + '_pesan').attr('style', 'padding-top:10px;font-size:10px;width:80%');
                },
                error: function (xhr, status, error) {
                    var err = eval("(" + xhr.responseText + ")");
                    $('#' + uc + '_pesan').html('Proses Error : ' + err.Message);
                    $('#' + uc + '_pesan').attr('style', 'padding-top:10px;font-size:10px;width:80%;color:red');
                }
            });

        } else {
            $('#' + uc + '_pesan').html('Proses Gagal, Periksa inputannya kembali!');
        }
        return false;
    }
    function getTransIDInternal(grd) {
        var tbl = {};
        tbl["ucTitipanPremiAsuransi_dtgIncentive"] = "TDI";
        tbl["UcTitipanRefundBunga_dtgIncentive"] = "NPV";

        tbl["UcTitipanProvisi_dtgIncentive"] = "PRS";
        tbl["UcTitipanBiayalainnya_dtgIncentive"] = "TDS";

        return tbl[grd]
    }
</script>
<div class="form_box_header">
    <div class="form_single">
        <asp:DataGrid ID="dtgIncentive" runat="server" Width="100%" AutoGenerateColumns="False"
            BorderStyle="None" BorderWidth="0" CssClass="grid_general" ShowFooter="True">
            <HeaderStyle CssClass="th" />
            <ItemStyle CssClass="item_grid" />
            <Columns>
                <asp:TemplateColumn HeaderText="NO">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblNomor"></asp:Label>
                    </ItemTemplate>
                    <FooterStyle BorderStyle="None" HorizontalAlign="Right" />
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="PENGGUNAAN"  ItemStyle-CssClass="item_grid_right" HeaderStyle-CssClass="th_right">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblddlPenggunaan"></asp:Label>
                    </ItemTemplate>
                    <FooterStyle BorderStyle="None" HorizontalAlign="Right" />
                    <FooterTemplate>
                        <asp:Label runat="server" ID="lblTotal" Text="TOTAL"></asp:Label>
                    </FooterTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="PROSENTASE"  ItemStyle-CssClass="item_grid_right" HeaderStyle-CssClass="th_right">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblProsentase">0</asp:Label>
                    </ItemTemplate>
                    <FooterStyle BorderStyle="None" HorizontalAlign="Right" />
                    <FooterTemplate>
                        <asp:Label runat="server" ID="lblTotalProsentase">0</asp:Label>
                    </FooterTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="INSENTIF"  ItemStyle-CssClass="item_grid_right" HeaderStyle-CssClass="th_right">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblInsentif">0</asp:Label>
                    </ItemTemplate>
                    <FooterStyle BorderStyle="None" HorizontalAlign="Right" />
                    <FooterTemplate>
                        <asp:Label runat="server" ID="lblTotalInsentif">0</asp:Label>
                    </FooterTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="DELETE" Visible="false">
                    <ItemStyle CssClass="command_col"></ItemStyle>
                    <ItemTemplate>
                        <asp:ImageButton ID="imgDelete" runat="server" CausesValidation="False" ImageUrl="../Images/icondelete.gif"
                            CommandName="Delete"></asp:ImageButton>
                        <asp:Label runat="server" ID="lblSupplierEmployeeID" Visible="false"></asp:Label>
                        <asp:Label runat="server" ID="lblSupplierEmployeePosition" Visible="false"></asp:Label>
                        <asp:Label runat="server" ID="lblEmployeeName" Visible="false"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateColumn>
            </Columns>
        </asp:DataGrid>
    </div>
</div>
<div style="display: none;">
    <asp:Button ID="btnAdd" runat="server" Text="Add" CausesValidation="False" CssClass="small button blue" />
    <asp:Button ID="btnSave" runat="server" Text="Save" CausesValidation="False" CssClass="small button blue" />
    <label id="pesan" runat="server" style="padding-top: 10px; font-size: 10px; width: 80%">
    </label>
</div>
