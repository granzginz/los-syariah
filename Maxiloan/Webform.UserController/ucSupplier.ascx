﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucSupplier.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.ucSupplier" %>
<label class="label_req">
    Supplier/Dealer</label>
<asp:TextBox ID="txtSupplierName" runat="server" CssClass="long_text"></asp:TextBox>
<asp:HyperLink ID="hpLookup" runat="server" ImageUrl="../images/IconDetail.gif"></asp:HyperLink>
<asp:Label ID="lblPF" runat="server"></asp:Label>
<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtSupplierName"
    ErrorMessage="Pilih supplier/dealer!" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
<input type="hidden" id="hdnSupplierID" runat="server" name="hdnSupplierID" />
<input type="hidden" id="hdnSupplierName" runat="server" name="hdnSupplierName" />
<input type="hidden" id="hdnApplicationID" runat="server" name="hdnApplicationID" />
<input type="hidden" id="hdnPrivate" runat="server" name="hdnPrivate" />
