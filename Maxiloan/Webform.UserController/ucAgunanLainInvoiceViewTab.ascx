﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucAgunanLainInvoiceViewTab.ascx.vb" Inherits="Maxiloan.Webform.UserController.ucAgunanLainInvoiceViewTab" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<div class="tab_container_view">
<div class="tab_container">
    <div id="tabLandBuilding" runat="server">
        <asp:HyperLink ID="hyplandbuilding" runat="server">TANAH</asp:HyperLink></div>
    <div id="tabBPKB" runat="server">
        <asp:HyperLink ID="hypbpkb" runat="server">BPKB</asp:HyperLink></div> 
    <div id="tabInvoice" runat="server">
        <asp:HyperLink ID="hypinvoice" runat="server">INVOICE</asp:HyperLink></div>
<div class="form_box_hide">
    <div id="tabSertifikat" runat="server">
        <asp:HyperLink ID="hypsertifikat" runat="server" Enabled="false">SERTIFIKAT</asp:HyperLink></div>
    <div id="tabPinjaman" runat="server">
        <asp:HyperLink ID="hyppinjaman" runat="server" Enabled="false">PINJAMAN LAINNYA</asp:HyperLink></div>
</div>
    <div id="tabLinkage" runat="server">
        <asp:HyperLink ID="hyplinkage" runat="server">LINKAGE</asp:HyperLink></div>
</div>
</div>