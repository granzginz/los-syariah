﻿Imports Maxiloan.Controller

Public Class UcIncentiveGrid
    Inherits ControlBased

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Public Event EventEdit(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
    Private Msg As New Maxiloan.Webform.WebBased
    Private m_Insentif As New RefundInsentifController

    Public WriteOnly Property OnClientClick As String
        Set(ByVal value As String)
            btnAdd.Attributes.Add("OnClick", value)
        End Set
    End Property
    Public WriteOnly Property OnSaveClick As String
        Set(ByVal value As String)
            btnSave.Attributes.Add("OnClick", value)
        End Set
    End Property
    Public Property ApplicationID() As String
        Get
            Return (CType(ViewState("ApplicationID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
    Public Property SupplierID() As String
        Set(ByVal SupplierID As String)
            ViewState("SupplierID") = SupplierID
        End Set
        Get
            Return ViewState("SupplierID").ToString
        End Get
    End Property
    Public Property BranchID() As String
        Set(ByVal BranchID As String)
            ViewState("BranchID") = BranchID
        End Set
        Get
            Return ViewState("BranchID").ToString
        End Get
    End Property
    Public Property BussinessDate() As String
        Set(ByVal BussinessDate As String)
            ViewState("BussinessDate") = BussinessDate
        End Set
        Get
            Return ViewState("BussinessDate").ToString
        End Get
    End Property

    Public Property WhereCond As String
        Set(ByVal WhereCond As String)
            ViewState("WhereCond") = WhereCond
        End Set
        Get
            Return ViewState("WhereCond").ToString
        End Get
    End Property
    Public Property dtg As DataGrid
        Get
            Return dtgIncentive
        End Get
        Set(ByVal dtg As DataGrid)
            dtgIncentive = dtg
        End Set
    End Property

    Public Property AlokasiPremiAsuransi As String
        Set(ByVal AlokasiPremiAsuransi As String)
            ViewState("AlokasiPremiAsuransi") = AlokasiPremiAsuransi
        End Set
        Get
            Return ViewState("AlokasiPremiAsuransi").ToString
        End Get
    End Property
    Public Property OTR As String
        Set(value As String)
            txtOTR.Text = value
        End Set
        Get
            Return txtOTR.Text
        End Get
    End Property
    Public Property Tenor As String
        Set(value As String)
            txtTenor.Text = value
        End Set
        Get
            Return txtTenor.Text
        End Get
    End Property
    Public Property FlatRate As String
        Set(value As String)
            txtFlatRate.Text = value
        End Set
        Get
            Return txtFlatRate.Text
        End Get
    End Property
    Public Property Admin As String
        Set(value As String)
            txtAdmin.Text = value
        End Set
        Get
            Return txtAdmin.Text
        End Get
    End Property
    Public Property AsuransiCoy As String
        Set(value As String)
            txtAsuransiCom.Text = value
        End Set
        Get
            Return txtAsuransiCom.Text
        End Get
    End Property
    Public Property Provisi As String
        Set(value As String)
            txtProvisi.Text = value
        End Set
        Get
            Return txtProvisi.Text
        End Get
    End Property
    Public Property NTF As String
        Set(value As String)
            txtNTF.Text = value
        End Set
        Get
            Return txtNTF.Text
        End Get
    End Property
    Public Property TotalBunga As String
        Set(value As String)
            txtTotalBunga.Text = value
        End Set
        Get
            Return txtTotalBunga.Text
        End Get
    End Property
    Public Property Other As String
        Set(value As String)
            txtOther.Text = value
        End Set
        Get
            Return txtOther.Text
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        txtSupplier.Text = Me.SupplierID
        txtApplicationID.Text = Me.ApplicationID
        txtBranchID.Text = Me.BranchID
        txtBussinessDate.Text = Me.BussinessDate
    End Sub


    'Private Sub dtgIncentive_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgIncentive.ItemCommand
    '    If e.CommandName = "Delete" Then
    '        DeleteBaris(e.Item.ItemIndex)
    '    End If
    'End Sub
    Public Sub AddRecord()
        Dim objectDataTable As New DataTable
        Dim intLoopGrid As Integer
        Dim oRow As DataRow
        Dim lblNomor As New Label
        Dim lblInsentifGross As New Label
        Dim lblPPH As New Label
        Dim lblTarifPajak As New Label
        Dim lblNilaiPajak As New Label
        Dim lblInsentifNet As New Label
        Dim ddlJabatan As New DropDownList
        Dim ddlPenerima As New DropDownList
        Dim ddlTransID As New DropDownList
        Dim txtProsentase As New TextBox
        Dim txtInsentif As New TextBox
        Dim plus As Integer = 0
        Dim imgDelete As New ImageButton
        Dim ddlPPh As New DropDownList
        Dim txtInsentifGross As New TextBox

        With objectDataTable
            .Columns.Add(New DataColumn("Nomor", GetType(String)))
            .Columns.Add(New DataColumn("Jabatan", GetType(String)))
            .Columns.Add(New DataColumn("Penerima", GetType(String)))
            .Columns.Add(New DataColumn("Prosentase", GetType(String)))
            .Columns.Add(New DataColumn("Insentif", GetType(String)))
            .Columns.Add(New DataColumn("InsentifGross", GetType(String)))
            .Columns.Add(New DataColumn("PPH", GetType(String)))
            .Columns.Add(New DataColumn("TarifPajak", GetType(String)))
            .Columns.Add(New DataColumn("NilaiPajak", GetType(String)))
            .Columns.Add(New DataColumn("InsentifNet", GetType(String)))
            .Columns.Add(New DataColumn("TransID", GetType(String)))
        End With

        For intLoopGrid = 0 To dtgIncentive.Items.Count - 1
            lblNomor = CType(dtgIncentive.Items(intLoopGrid).FindControl("lblNomor"), Label)
            ddlJabatan = CType(dtgIncentive.Items(intLoopGrid).FindControl("ddlJabatan"), DropDownList)
            ddlPenerima = CType(dtgIncentive.Items(intLoopGrid).FindControl("ddlPenerima"), DropDownList)
            txtProsentase = CType(dtgIncentive.Items(intLoopGrid).FindControl("txtProsentase"), TextBox)
            txtInsentif = CType(dtgIncentive.Items(intLoopGrid).FindControl("txtInsentif"), TextBox)
            lblInsentifGross = CType(dtgIncentive.Items(intLoopGrid).FindControl("lblInsentifGross"), Label)
            'lblPPH = CType(dtgIncentive.Items(intLoopGrid).FindControl("lblPPH"), Label)
            ddlPPh = CType(dtgIncentive.Items(intLoopGrid).FindControl("ddlPPh"), DropDownList)
            lblTarifPajak = CType(dtgIncentive.Items(intLoopGrid).FindControl("lblTarifPajak"), Label)
            lblNilaiPajak = CType(dtgIncentive.Items(intLoopGrid).FindControl("lblNilaiPajak"), Label)
            lblInsentifNet = CType(dtgIncentive.Items(intLoopGrid).FindControl("lblInsentifNet"), Label)
            ddlTransID = CType(dtgIncentive.Items(intLoopGrid).FindControl("ddlTransID"), DropDownList)

            '----- Add row -------'
            oRow = objectDataTable.NewRow()
            oRow("Nomor") = CType(lblNomor.Text, String)
            oRow("Jabatan") = CType(ddlJabatan.SelectedValue, String)
            oRow("Penerima") = CType(ddlPenerima.SelectedValue, String)
            oRow("Prosentase") = CType(txtProsentase.Text, String)
            oRow("Insentif") = CType(txtInsentif.Text, String)
            oRow("InsentifGross") = CType(lblInsentifGross.Text, String)
            'oRow("PPH") = CType(lblPPH.Text, String)
            oRow("PPH") = CType(ddlPPh.SelectedValue, String)
            oRow("TarifPajak") = CType(lblTarifPajak.Text, String)
            oRow("NilaiPajak") = CType(lblNilaiPajak.Text, String)
            oRow("InsentifNet") = CType(lblInsentifNet.Text, String)
            oRow("TransID") = CType(ddlTransID.SelectedValue, String)
            objectDataTable.Rows.Add(oRow)


        Next

        oRow = objectDataTable.NewRow()
        oRow("Nomor") = dtgIncentive.Items.Count + 1
        oRow("Jabatan") = ""
        oRow("Penerima") = ""
        oRow("Prosentase") = ""
        oRow("Insentif") = ""
        oRow("InsentifGross") = ""
        oRow("PPH") = ""
        oRow("TarifPajak") = ""
        oRow("NilaiPajak") = ""
        oRow("InsentifNet") = ""
        oRow("TransID") = ""
        objectDataTable.Rows.Add(oRow)

        dtgIncentive.DataSource = objectDataTable
        dtgIncentive.DataBind()

        'BindDDl()

        For intLoopGrid = 0 To dtgIncentive.Items.Count - 1
            imgDelete = CType(dtgIncentive.Items(intLoopGrid).FindControl("imgDelete"), ImageButton)
            lblNomor = CType(dtgIncentive.Items(intLoopGrid).FindControl("lblNomor"), Label)
            lblNomor = CType(dtgIncentive.Items(intLoopGrid).FindControl("lblNomor"), Label)
            ddlJabatan = CType(dtgIncentive.Items(intLoopGrid).FindControl("ddlJabatan"), DropDownList)
            ddlPenerima = CType(dtgIncentive.Items(intLoopGrid).FindControl("ddlPenerima"), DropDownList)
            txtProsentase = CType(dtgIncentive.Items(intLoopGrid).FindControl("txtProsentase"), TextBox)
            txtInsentif = CType(dtgIncentive.Items(intLoopGrid).FindControl("txtInsentif"), TextBox)
            lblInsentifGross = CType(dtgIncentive.Items(intLoopGrid).FindControl("lblInsentifGross"), Label)
            'lblPPH = CType(dtgIncentive.Items(intLoopGrid).FindControl("lblPPH"), Label)
            ddlPPh = CType(dtgIncentive.Items(intLoopGrid).FindControl("ddlPPh"), DropDownList)
            lblTarifPajak = CType(dtgIncentive.Items(intLoopGrid).FindControl("lblTarifPajak"), Label)
            lblNilaiPajak = CType(dtgIncentive.Items(intLoopGrid).FindControl("lblNilaiPajak"), Label)
            lblInsentifNet = CType(dtgIncentive.Items(intLoopGrid).FindControl("lblInsentifNet"), Label)
            ddlTransID = CType(dtgIncentive.Items(intLoopGrid).FindControl("ddlTransID"), DropDownList)


            lblNomor.Text = objectDataTable.Rows(intLoopGrid).Item(0).ToString.Trim

            If objectDataTable.Rows(intLoopGrid).Item(1).ToString <> "" Then
                ddlJabatan.SelectedIndex = ddlJabatan.Items.IndexOf(ddlJabatan.Items.FindByValue(objectDataTable.Rows(intLoopGrid).Item(1).ToString.Trim))
            End If

            BindddlPenerima(ddlJabatan, intLoopGrid)

            'onchange="handleddlJabatan_Change(this,'ddlPenerima.ClientID');"

            If objectDataTable.Rows(intLoopGrid).Item(2).ToString <> "" Then
                ddlPenerima.SelectedIndex = ddlPenerima.Items.IndexOf(ddlPenerima.Items.FindByValue(objectDataTable.Rows(intLoopGrid).Item(2).ToString.Trim))
            End If

            txtProsentase.Text = objectDataTable.Rows(intLoopGrid).Item(3).ToString.Trim
            txtInsentif.Text = objectDataTable.Rows(intLoopGrid).Item(4).ToString.Trim
            lblInsentifGross.Text = objectDataTable.Rows(intLoopGrid).Item(5).ToString.Trim
            'lblPPH.Text = objectDataTable.Rows(intLoopGrid).Item(6).ToString.Trim
            ddlPPh.Text = objectDataTable.Rows(intLoopGrid).Item(6).ToString.Trim
            lblTarifPajak.Text = objectDataTable.Rows(intLoopGrid).Item(7).ToString.Trim
            lblNilaiPajak.Text = objectDataTable.Rows(intLoopGrid).Item(8).ToString.Trim
            lblInsentifNet.Text = objectDataTable.Rows(intLoopGrid).Item(9).ToString.Trim
            imgDelete.Attributes.Add("OnClick", "return deleteRow('" & dtgIncentive.ClientID & "',this);")

            txtProsentase.Attributes.Add("OnChange", "handletxtProsentase_Change(this.value,'" & ddlTransID.ClientID & "','" & txtInsentif.ClientID & "')")
            txtInsentif.Attributes.Add("OnChange", "handletxtInsentif_Change(this.value,'" & ddlTransID.ClientID & "','" & txtProsentase.ClientID & "')")
            ddlJabatan.Attributes.Add("OnChange", "handleddlJabatan_Change(this,'" & ddlPenerima.ClientID & "')")

        Next
    End Sub

    Private Sub BindDDl()
        Dim dtEntity As New DataTable
        Dim oCustom As New Parameter.RefundInsentif
        Dim ddlJabatan As New DropDownList
        Dim ddlPenerima As New DropDownList

        With oCustom
            .strConnection = GetConnectionString()
            .WhereCond = " "
            .SPName = "spGetTblEmployeePosition"
        End With

        oCustom = m_Insentif.GetSPBy(oCustom)
        dtEntity = oCustom.ListDataTable

        For intLoop = 0 To dtgIncentive.Items.Count - 1
            ddlJabatan = CType(dtgIncentive.Items(intLoop).FindControl("ddlJabatan"), DropDownList)
            ddlPenerima = CType(dtgIncentive.Items(intLoop).FindControl("ddlPenerima"), DropDownList)

            ddlJabatan.DataSource = dtEntity.DefaultView
            ddlJabatan.DataTextField = "Name"
            ddlJabatan.DataValueField = "ID"
            ddlJabatan.DataBind()
            ddlJabatan.Items.Insert(0, "Select One")
            ddlJabatan.Items(0).Value = ""

            ddlPenerima.Items.Insert(0, "Select One")
            ddlPenerima.Items(0).Value = ""
        Next
    End Sub
    'Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
    '    AddRecord()
    'End Sub
    Sub ddlJabatan_IndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        lblMessage.Visible = False
        Dim ddlJabatan As New DropDownList
        ddlJabatan = CType(sender, DropDownList)
        Dim index As Integer = DirectCast(ddlJabatan.DataItemContainer, System.Web.UI.WebControls.DataGridItem).DataSetIndex

        For indexGrid = 0 To dtgIncentive.Items.Count - 1
            Dim A_ddlJabatan As New DropDownList
            A_ddlJabatan = CType(dtgIncentive.Items(indexGrid).FindControl("ddlJabatan"), DropDownList)

            If index <> indexGrid And A_ddlJabatan.SelectedValue.Trim <> "" And ddlJabatan.SelectedValue.Trim = A_ddlJabatan.SelectedValue.Trim Then
                ddlJabatan.SelectedIndex = 0
                Msg.ShowMessage(lblMessage, "Data Sudah ada!", True)
                Exit Sub
            Else
                lblMessage.Visible = False
            End If

        Next

        BindddlPenerima(ddlJabatan, index)
        ScriptManager.RegisterStartupScript(ddlJabatan, GetType(Page), ddlJabatan.ClientID, String.Format("(document.getElementById('{0}')).focus();", ddlJabatan.ClientID), True)
    End Sub
    Sub BindddlPenerima(ByVal ddlJabatan As DropDownList, ByVal index As Integer)
        lblMessage.Visible = False
        Dim oCustom As New Parameter.RefundInsentif
        Dim entity As New DataTable
        Dim ddlPenerima As New DropDownList

        ddlPenerima = CType(dtgIncentive.Items(index).FindControl("ddlPenerima"), DropDownList)
        With oCustom
            .strConnection = GetConnectionString()
            .WhereCond = "  SupplierEmployeePosition='" & ddlJabatan.SelectedValue.Trim & "' and SupplierID='" & Me.SupplierID & "' "
            .SPName = "spGetSupplierEmployee"
        End With

        oCustom = m_Insentif.GetSPBy(oCustom)
        entity = oCustom.ListDataTable

        ddlPenerima.DataSource = entity.DefaultView
        ddlPenerima.DataTextField = "Name"
        ddlPenerima.DataValueField = "SupplierEmployeeID"
        ddlPenerima.DataBind()
        ddlPenerima.SelectedIndex = 0
        ddlPenerima.Items.Insert(0, "Select One")
        ddlPenerima.Items(0).Value = ""
    End Sub
    Sub BindddlTransID(ByVal index As Integer, ByVal tipe As String)
        Dim oCustom As New Parameter.RefundInsentif
        Dim entity As New DataTable
        Dim ddlTransID As New DropDownList
        ddlTransID = CType(dtgIncentive.Items(index).FindControl("ddlTransID"), DropDownList)
        With oCustom
            .strConnection = GetConnectionString()
            .WhereCond = ""
            If tipe <> "" Then
                .WhereCond = " TransID in (select TransID from tblAgreementMasterTransaction WHERE GroupTransID = '" & tipe & "') "
            End If
            .SPName = "spGetTblAgreementMasterTransaction"
        End With

        oCustom = m_Insentif.GetSPBy(oCustom)
        entity = oCustom.ListDataTable

        ddlTransID.DataSource = entity.DefaultView
        ddlTransID.DataTextField = "TransID"
        ddlTransID.DataValueField = "TransID"
        ddlTransID.DataBind()
        ddlTransID.Items.Insert(0, "--")
        ddlTransID.Items(0).Value = ""
    End Sub

    Public Sub BindInsentif(ByVal txtInsentif As TextBox, ByVal index As Integer)
        Dim A_PremiAsuransi As Decimal = CDec(IIf(Me.AlokasiPremiAsuransi = "", 0, Me.AlokasiPremiAsuransi))
        Dim Input As Decimal = CDec(IIf(txtInsentif.Text.Trim = "", "0", txtInsentif.Text))
        Dim txtProsentase As New TextBox
        Dim totalProsentase As Decimal = 0
        Dim totalInsentif As Decimal = 0
        Dim lblInsentifGross As New Label
        Dim lblTarifPajak As New Label
        Dim lblNilaiPajak As New Label
        Dim lblInsentifNet As New Label
        Dim ddlPenerima As New DropDownList
        Dim ddlJabatan As New DropDownList
        Dim oCustom As New Parameter.RefundInsentif
        Dim txtInsentifGross As New TextBox

        ddlJabatan = CType(dtgIncentive.Items(index).FindControl("ddlJabatan"), DropDownList)
        ddlPenerima = CType(dtgIncentive.Items(index).FindControl("ddlPenerima"), DropDownList)

        With oCustom
            .strConnection = GetConnectionString()
            .WhereCond = "  SupplierEmployeePosition='" & ddlJabatan.SelectedValue.Trim & "' and rtrim(ltrim(rtrim(ltrim(SupplierID)) + '-' + rtrim(ltrim(SupplierEmployeeID)))) = '" & ddlPenerima.SelectedValue.Trim & "' "
            .SPName = "spGetSupplierEmployee"
        End With

        oCustom = m_Insentif.GetSPBy(oCustom)

        If oCustom.ListDataTable.Rows.Count > 0 Then
            CType(dtgIncentive.Items(index).FindControl("lblSupplierEmployeeID"), Label).Text = oCustom.ListDataTable.Rows(0).Item("SupplierEmployeeID").ToString.Trim
            CType(dtgIncentive.Items(index).FindControl("lblSupplierEmployeePosition"), Label).Text = oCustom.ListDataTable.Rows(0).Item("SupplierEmployeePosition").ToString.Trim
            CType(dtgIncentive.Items(index).FindControl("lblEmployeeName"), Label).Text = oCustom.ListDataTable.Rows(0).Item("Name").ToString.Trim
        End If


        txtProsentase = CType(dtgIncentive.Items(index).FindControl("txtProsentase"), TextBox)

        txtProsentase.Text = FormatNumber(CType((Input / A_PremiAsuransi) * 100, String), 2)
        lblInsentifGross = CType(dtgIncentive.Items(index).FindControl("lblInsentifGross"), Label)
        lblTarifPajak = CType(dtgIncentive.Items(index).FindControl("lblTarifPajak"), Label)
        lblNilaiPajak = CType(dtgIncentive.Items(index).FindControl("lblNilaiPajak"), Label)
        lblInsentifNet = CType(dtgIncentive.Items(index).FindControl("lblInsentifNet"), Label)


        If ddlPenerima.SelectedValue.Trim = "" Then
            CType(dtgIncentive.Items(index).FindControl("txtProsentase"), TextBox).Text = ""
            CType(dtgIncentive.Items(index).FindControl("txtInsentif"), TextBox).Text = ""
            CType(dtgIncentive.Items(index).FindControl("lblInsentifGross"), Label).Text = ""
            CType(dtgIncentive.Items(index).FindControl("lblNilaiPajak"), Label).Text = ""
            CType(dtgIncentive.Items(index).FindControl("lblInsentifNet"), Label).Text = ""
            Msg.ShowMessage(lblMessage, "Penerima Harus dipilih!", True)
            Exit Sub
        Else
            lblMessage.Visible = False
        End If

        lblInsentifGross.Text = txtInsentif.Text
        lblNilaiPajak.Text = FormatNumber(CType((CDec(IIf(lblInsentifGross.Text.Trim = "", "0", lblInsentifGross.Text)) * (CDec(IIf(lblTarifPajak.Text.Trim = "", "0", lblTarifPajak.Text)) / 100)), String), 0)
        lblInsentifNet.Text = FormatNumber(CType((CDec(IIf(lblInsentifGross.Text.Trim = "", "0", lblInsentifGross.Text)) - CDec(IIf(lblNilaiPajak.Text.Trim = "", "0", lblNilaiPajak.Text))), String), 0)

    End Sub
    Public Sub BindTotal()
        Dim A_PremiAsuransi As Decimal = CDec(IIf(Me.AlokasiPremiAsuransi = "", 0, Me.AlokasiPremiAsuransi))
        Dim totalInsentif As Decimal = 0
        Dim totalPersen As Decimal = 0

        For i = 0 To dtgIncentive.Items.Count - 1
            Dim Grid_txtInsentif As New TextBox
            Dim txtProsentase As New TextBox

            Grid_txtInsentif = CType(dtgIncentive.Items(i).FindControl("txtInsentif"), TextBox)
            txtProsentase = CType(dtgIncentive.Items(i).FindControl("txtProsentase"), TextBox)

            totalInsentif = totalInsentif + CDec(IIf(Grid_txtInsentif.Text.Trim = "", "0", Grid_txtInsentif.Text))
            totalPersen = totalPersen + CDec(IIf(IsNumeric(txtProsentase.Text), txtProsentase.Text, "0"))

        Next

        'If totalInsentif > 0 And A_PremiAsuransi > 0 Then
        ScriptManager.RegisterStartupScript(dtgIncentive, GetType(DataGrid), dtgIncentive.ClientID, String.Format(" document.getElementById('{0}' + '_txtTotalProsentase').value = '" + FormatNumber(totalPersen, 2).ToString + "';  document.getElementById('{0}' + '_txtTotalInsentif').value = '" + FormatNumber(totalInsentif, 0).ToString + "'; ", dtgIncentive.ClientID), True)
        'End If


    End Sub


End Class