﻿Imports System.Data.SqlClient

Public Class ucAgunanLainInvoiceViewTab
	Inherits Maxiloan.Webform.ControlBased

	Public Property CollateralID As String
	Public Property CustomerID As String

	Public Sub setLink()
		hyplandbuilding.NavigateUrl = "~/Webform.AgunanLain/Transaksi/ViewCollateralLandBuilding.aspx?Style=accacq&CollateralID=" & Me.CollateralID & "&CustomerID=" & Me.CustomerID
		hypbpkb.NavigateUrl = "~/Webform.AgunanLain/Transaksi/ViewCollateralBPKB.aspx?Style=accacq&CollateralID=" & Me.CollateralID & "&CustomerID=" & Me.CustomerID
		hypinvoice.NavigateUrl = "~/Webform.AgunanLain/Transaksi/ViewCollateralInvoice.aspx?Style=accacq&CollateralID=" & Me.CollateralID & "&CustomerID=" & Me.CustomerID
		hypsertifikat.NavigateUrl = "~/Webform.AgunanLain/Transaksi/ViewCollateralSertifikat.aspx?Style=accacq&CollateralID=" & Me.CollateralID & "&CustomerID=" & Me.CustomerID
		hyppinjaman.NavigateUrl = "~/Webform.AgunanLain/Transaksi/ViewCollateralPinjaman.aspx?Style=accacq&CollateralID=" & Me.CollateralID & "&CustomerID=" & Me.CustomerID
		hyplinkage.NavigateUrl = "~/Webform.AgunanLain/Transaksi/ViewLinkAgeJT.aspx?Style=accacq&CollateralID=" & Me.CollateralID & "&CustomerID=" & Me.CustomerID
	End Sub

	Public Sub selectedTab(ByVal strTab As String)
		Select Case strTab
			Case "LandBuilding"
				tabLandBuilding.Attributes.Add("class", "tab_selected")
				tabBPKB.Attributes.Add("class", "tab_notselected")
				tabInvoice.Attributes.Add("class", "tab_notselected")
				tabSertifikat.Attributes.Add("class", "tab_notselected")
				tabPinjaman.Attributes.Add("class", "tab_notselected")
				tabLinkage.Attributes.Add("class", "tab_notselected")
			Case "BPKB"
				tabLandBuilding.Attributes.Add("class", "tab_notselected")
				tabBPKB.Attributes.Add("class", "tab_selected")
				tabInvoice.Attributes.Add("class", "tab_notselected")
				tabSertifikat.Attributes.Add("class", "tab_notselected")
				tabPinjaman.Attributes.Add("class", "tab_notselected")
				tabLinkage.Attributes.Add("class", "tab_notselected")
			Case "Invoice"
				tabLandBuilding.Attributes.Add("class", "tab_notselected")
				tabBPKB.Attributes.Add("class", "tab_notselected")
				tabInvoice.Attributes.Add("class", "tab_selected")
				tabSertifikat.Attributes.Add("class", "tab_notselected")
				tabPinjaman.Attributes.Add("class", "tab_notselected")
				tabLinkage.Attributes.Add("class", "tab_notselected")
			Case "Sertifikat"
				tabLandBuilding.Attributes.Add("class", "tab_notselected")
				tabBPKB.Attributes.Add("class", "tab_notselected")
				tabInvoice.Attributes.Add("class", "tab_notselected")
				tabSertifikat.Attributes.Add("class", "tab_selected")
				tabPinjaman.Attributes.Add("class", "tab_notselected")
				tabLinkage.Attributes.Add("class", "tab_notselected")
			Case "PinjamanLain"
				tabLandBuilding.Attributes.Add("class", "tab_notselected")
				tabBPKB.Attributes.Add("class", "tab_notselected")
				tabInvoice.Attributes.Add("class", "tab_notselected")
				tabSertifikat.Attributes.Add("class", "tab_notselected")
				tabPinjaman.Attributes.Add("class", "tab_selected")
				tabLinkage.Attributes.Add("class", "tab_notselected")
			Case "Linkage"
				tabLandBuilding.Attributes.Add("class", "tab_notselected")
				tabBPKB.Attributes.Add("class", "tab_notselected")
				tabInvoice.Attributes.Add("class", "tab_notselected")
				tabSertifikat.Attributes.Add("class", "tab_notselected")
				tabPinjaman.Attributes.Add("class", "tab_notselected")
				tabLinkage.Attributes.Add("class", "tab_selected")
			Case Else
				tabLandBuilding.Attributes.Add("class", "tab_selected")
				tabBPKB.Attributes.Add("class", "tab_notselected")
				tabInvoice.Attributes.Add("class", "tab_notselected")
				tabSertifikat.Attributes.Add("class", "tab_notselected")
				tabPinjaman.Attributes.Add("class", "tab_notselected")
				tabLinkage.Attributes.Add("class", "tab_notselected")
		End Select
	End Sub
End Class