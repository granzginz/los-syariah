﻿Public Class ucAsset
    Inherits System.Web.UI.UserControl

    Public Property AssetCode() As String
        Get
            Return hdnAssetCode.Value
        End Get
        Set(ByVal Value As String)
            hdnAssetCode.Value = Value
        End Set
    End Property

    Public Property AssetName() As String
        Get
            Return txtAssetName.Text
        End Get
        Set(ByVal Value As String)
            txtAssetName.Text = Value
        End Set
    End Property

    Public Property AssetTypeID() As String
        Get
            Return CStr(ViewState("AssetTypeID"))
        End Get
        Set(ByVal Value As String)
            ViewState("AssetTypeID") = Value
        End Set
    End Property

    Public Property Style() As String
        Get
            Return CStr(viewstate("style"))
        End Get
        Set(ByVal Value As String)
            viewstate("style") = Value
        End Set
    End Property


    Public Property hideLookup() As Boolean
        Get
            Return hpLookup.Visible
        End Get
        Set(ByVal Value As Boolean)
            hpLookup.Visible = Not Value
        End Set
    End Property

    Public Sub BindData()
        txtAssetName.Attributes.Add("readonly", "true")
        hpLookup.NavigateUrl = "javascript:OpenWinAssetLookup('" & hdnAssetCode.ClientID & "','" & txtAssetName.ClientID & "','" & Me.Style & "','" & Me.AssetTypeID & "')"
    End Sub

End Class