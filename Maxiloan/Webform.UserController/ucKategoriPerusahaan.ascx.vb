﻿Public Class ucKategoriPerusahaan
    Inherits System.Web.UI.UserControl

    Public Property SelectedKategori As String
        Get
            Return cboKategoriPerusahaan.SelectedValue
        End Get
        Set(ByVal value As String)
            cboKategoriPerusahaan.SelectedValue = value
        End Set
    End Property
End Class