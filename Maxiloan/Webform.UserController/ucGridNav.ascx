﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucGridNav.ascx.vb" Inherits="Maxiloan.Webform.UserController.ucGridNav" %>
<div>
     <div class="button_gridnavigation">
         <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../Images/grid_navbutton01.png" CommandName="First" OnCommand="NavigationFirstLink_Click" CausesValidation="False" /> 
         <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../Images/grid_navbutton02.png" CommandName="Prev" OnCommand="NavigationPrevLink_Click" CausesValidation="False" /> 
         <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../Images/grid_navbutton03.png" CommandName="Next" OnCommand="NavigationNextLink_Click" CausesValidation="False"/> 
         <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../Images/grid_navbutton04.png" CommandName="Last" OnCommand="NavigationLastLink_Click" CausesValidation="False"/>         
         <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
         <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue" EnableViewState="False"/>
         <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general" Display="Dynamic"/>
         <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage" ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general"/>         
    </div>
    <div class="label_gridnavigation">
        <asp:Label ID="lblPage" runat="server"/>of
        <asp:Label ID="lblTotPage" runat="server"/>, Total
        <asp:Label ID="lblrecord" runat="server"/>record(s)
    </div>
    <asp:HiddenField id="hdIsFind" runat="server" Value="0"/>
    <asp:HiddenField id="hdBranch" runat="server" Value=""/>
    <asp:HiddenField id="hdType" runat="server" Value="0"/>
    <asp:HiddenField id="hdWOpt" runat="server" Value="0"/>
    <asp:HiddenField id="hdWhere" runat="server" Value=""/>
</div>