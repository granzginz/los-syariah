﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucBranchMulti.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.ucBranchMulti" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%--<style type="text/css">
    .wp
    {
        background-color: #fff;
        width: 700px;        
        border: 1px solid #ccc;
        float: left;
    }
    
    .wp h4
    {
        margin: 0;
        color: #000;
        text-align: left;
    }
    
    .wp .gridwp
    {
        max-height: 300px;
        overflow: auto;
    }
    
    .wpbg
    {
        background-color: #000;
        filter: alpha(opacity=70);
        opacity: 0.7;
    }
    
    .wpbtnexit
    {
        position: absolute !important;
        float: right !important;
        top: 0 !important;
        right: 0 !important;
    }
</style>--%>
<asp:UpdatePanel runat="server" ID="updatePanel1">
    <ContentTemplate>
        <asp:Button runat="server" ID="btnLookup" Text="btnLookup" Style="display: none;" />
        <asp:ModalPopupExtender ID="ModalPopupExtender" runat="server" TargetControlID="btnLookup"
            PopupControlID="Panel1" BackgroundCssClass="wpbg" CancelControlID="btnExit">
        </asp:ModalPopupExtender>
        <asp:Panel ID="Panel1" runat="server">
            <div class="wp">
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
                <asp:ImageButton runat="server" ID="btnExit" ImageUrl="../Images/exit_lookup00.png"
                    CssClass="wpbtnexit" />
                <div class="form_title">
                    <div class="title_strip">
                    </div>
                    <div class="form_single">
                        <h4>
                            DAFTAR CABANG</h4>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ws">
                            <asp:GridView runat="server" ID="gv1" AllowPaging="false" AutoGenerateColumns="false"
                                DataKeyNames="BranchID" CssClass="grid_general">
                                <HeaderStyle CssClass="th" />
                                <RowStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:CheckBox runat="server" ID="chk"  />
                                        </ItemTemplate> 
                                    </asp:TemplateField> 
                                    <asp:BoundField DataField="BranchFullName" HeaderText="CABANG" />
                                    <asp:BoundField DataField="BranchID" Visible="false" />
                                </Columns>
                            </asp:GridView>                            
                        </div>
                    </div>
                </div>
                <div class ="form_button">
                    <asp:Button runat="server" ID="btnSelect" Text ="SELECT" CssClass ="small button blue" /> 
                </div>
            </div>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
