﻿Public Class ucRefinancing
    Inherits System.Web.UI.UserControl    

    Public Property RefinancingName() As String
        Get
            Return hdnRefinancingName.Value
        End Get
        Set(ByVal Value As String)
            hdnRefinancingName.Value = Value
        End Set
    End Property

    Public Property ApplicationID() As String
        Get
            Return hdnAppID.Value
        End Get
        Set(ByVal Value As String)
            hdnAppID.Value = Value
        End Set
    End Property

    Public Property AgreementNo() As String
        Get
            Return txtAgreementNo.Text
        End Get
        Set(ByVal Value As String)
            txtAgreementNo.Text = Value
        End Set
    End Property

    Public Property NilaiPelunasan() As String
        Get
            Return txtNilaiPelunasan.Text
        End Get
        Set(ByVal Value As String)
            txtNilaiPelunasan.Text = Value
        End Set
    End Property

    'Public Sub BindData()
    '    txtRefinancingName.Attributes.Add("readonly", "true")
    '    hpLookup.NavigateUrl = "javascript:OpenWinReFinancingLookup('" & hdnApplicationID.ClientID & "','" & txtRefinancingName.ClientID & "','" & Me.Style & "')"
    'End Sub
End Class