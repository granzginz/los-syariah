﻿Imports System.Data.SqlClient

Public Class ucApplicationViewTabFactoring
    Inherits Maxiloan.Webform.ControlBased

    Public Property ApplicationID As String
    Private Property CustomerID As String
    Private Property SupplierGroupID As String
    Private Property NoFasilitas As String

    Public Sub setLink()
        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(GetConnectionString)
        Dim objReader As SqlDataReader

        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()

            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.CommandText = "spGetApplicationTab"
            objCommand.Parameters.Add("@ApplicationID", SqlDbType.VarChar, 20).Value = Me.ApplicationID.Trim
            objCommand.Connection = objConnection
            objReader = objCommand.ExecuteReader()

            If objReader.Read Then
                Me.CustomerID = objReader.Item("CustomerID").ToString.Trim
                Me.SupplierGroupID = "" 'objReader.Item("SupplierGroupID").ToString
                If Not IsDBNull(objReader.Item("DateEntryFinancialData")) Then
                    hypAplikasi.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewFactoring/ViewApplicationFactoring_003.aspx?id=" & Me.CustomerID & "&ApplicationID=" & Me.ApplicationID & "&page=Edit"
                    hypInvoice.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewFactoring/ViewInvoiceData.aspx?ApplicationID=" & Me.ApplicationID.ToString.Trim & "&page=Edit"
                    hypFinancial.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewFactoring/ViewFinancialFactoringData.aspx?ApplicationID=" & Me.ApplicationID.Trim & "&page=Edit"
					hypJournal.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewFactoring/ViewJournalFactoringData.aspx?ApplicationID=" & Me.ApplicationID.Trim & "&page=Edit"
                    hypActivityLog.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewFactoring/ViewActivityLogFact.aspx?ApplicationID=" & Me.ApplicationID.Trim & "&page=Edit"
                    hypPlafon.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewFactoring/ViewPlafonFact.aspx?facno=" & Me.NoFasilitas & "&CustID=" & Me.CustomerID & "&ApplicationID=" & Me.ApplicationID & "&page=Edit"
                    hypInvoicePaid.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewFactoring/ViewInvoicePaid.aspx?ApplicationID=" & Me.ApplicationID.ToString.Trim & "&page=Edit"
                    hypInvoiceChangeDueDate.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewFactoring/ViewChangeDueDate.aspx?ApplicationID=" & Me.ApplicationID.ToString.Trim & "&page=Edit"
                ElseIf Not IsDBNull(objReader.Item("DateEntryAssetData")) Then
					hypAplikasi.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewFactoring/ViewApplicationFactoring_003.aspx?id=" & Me.CustomerID & "&ApplicationID=" & Me.ApplicationID & "&page=Edit"
					hypInvoice.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewFactoring/ViewInvoiceData.aspx?ApplicationID=" & Me.ApplicationID.ToString.Trim & "&page=Edit"
					hypFinancial.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewFactoring/ViewFinancialFactoringData.aspx?ApplicationID=" & Me.ApplicationID.Trim & "&page=Edit"
					hypJournal.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewFactoring/ViewJournalFactoringData.aspx?ApplicationID=" & Me.ApplicationID.Trim & "&page=Edit"
                    hypActivityLog.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewFactoring/ViewActivityLogFact.aspx?ApplicationID=" & Me.ApplicationID.Trim & "&page=Edit"
                    hypPlafon.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewFactoring/ViewPlafonFact.aspx?facno=" & Me.NoFasilitas & "&CustID=" & Me.CustomerID & "&ApplicationID=" & Me.ApplicationID & "&page=Edit"
                    hypInvoicePaid.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewFactoring/ViewInvoicePaid.aspx?ApplicationID=" & Me.ApplicationID.ToString.Trim & "&page=Edit"
                    hypInvoiceChangeDueDate.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewFactoring/ViewChangeDueDate.aspx?ApplicationID=" & Me.ApplicationID.ToString.Trim & "&page=Edit"
                ElseIf Not IsDBNull(objReader.Item("DateEntryApplicationData")) Then
                    hypAplikasi.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewFactoring/ViewApplicationFactoring_003.aspx?id=" & Me.CustomerID & "&ApplicationID=" & Me.ApplicationID & "&page=Edit"
                    hypInvoice.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewFactoring/ViewInvoiceData.aspx?ApplicationID=" & Me.ApplicationID.ToString.Trim & "&page=Edit"
					hypFinancial.NavigateUrl = ""
					hypActivityLog.NavigateUrl = ""
                    hypJournal.NavigateUrl = ""
                    hypPlafon.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewFactoring/ViewPlafonFact.aspx?facno=" & Me.NoFasilitas & "&CustID=" & Me.CustomerID & "&ApplicationID=" & Me.ApplicationID & "&page=Edit"
                    hypInvoicePaid.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewFactoring/ViewInvoicePaid.aspx?ApplicationID=" & Me.ApplicationID.ToString.Trim & "&page=Edit"
                    hypInvoiceChangeDueDate.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewFactoring/ViewChangeDueDate.aspx?ApplicationID=" & Me.ApplicationID.ToString.Trim & "&page=Edit"
                Else
                    hypAplikasi.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewFactoring/ViewApplication_003.aspx?id=" & Me.CustomerID & "&ApplicationID=" & Me.ApplicationID & "&page=Edit"
                    hypInvoice.NavigateUrl = ""
					hypFinancial.NavigateUrl = ""
					hypActivityLog.NavigateUrl = ""
                    hypJournal.NavigateUrl = ""
                    hypPlafon.NavigateUrl = ""
                    hypInvoicePaid.NavigateUrl = ""
                    hypInvoiceChangeDueDate.NavigateUrl = ""
                End If
            End If

            objReader.Close()
        Catch ex As Exception
            Throw New Exception(ex.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
            objCommand.Dispose()
        End Try
    End Sub

    Public Sub selectedTab(ByVal strTab As String)
        Select Case strTab
            Case "Application"
                tabAplikasi.Attributes.Add("class", "tab_selected")
                tabAsset.Attributes.Add("class", "tab_notselected")
                tabFinancial.Attributes.Add("class", "tab_notselected")
				TabJournal.Attributes.Add("class", "tab_notselected")
                tabActivityLog.Attributes.Add("class", "tab_notselected")
                tabPlafon.Attributes.Add("class", "tab_notselected")
                tabInvoicePaid.Attributes.Add("class", "tab_notselected")
                tabChangeDueDate.Attributes.Add("class", "tab_notselected")
            Case "Invoice"
                tabAplikasi.Attributes.Add("class", "tab_notselected")
                tabAsset.Attributes.Add("class", "tab_selected")
                tabFinancial.Attributes.Add("class", "tab_notselected")
				TabJournal.Attributes.Add("class", "tab_notselected")
                tabActivityLog.Attributes.Add("class", "tab_notselected")
                tabPlafon.Attributes.Add("class", "tab_notselected")
                tabInvoicePaid.Attributes.Add("class", "tab_notselected")
                tabChangeDueDate.Attributes.Add("class", "tab_notselected")
            Case "Financial"
                tabAplikasi.Attributes.Add("class", "tab_notselected")
                tabAsset.Attributes.Add("class", "tab_notselected")
                tabFinancial.Attributes.Add("class", "tab_selected")
				TabJournal.Attributes.Add("class", "tab_notselected")
                tabActivityLog.Attributes.Add("class", "tab_notselected")
                tabPlafon.Attributes.Add("class", "tab_notselected")
                tabInvoicePaid.Attributes.Add("class", "tab_notselected")
                tabChangeDueDate.Attributes.Add("class", "tab_notselected")
            Case "Journal"
                tabAplikasi.Attributes.Add("class", "tab_notselected")
                tabAsset.Attributes.Add("class", "tab_notselected")
                tabFinancial.Attributes.Add("class", "tab_notselected")
				TabJournal.Attributes.Add("class", "tab_selected")
                tabActivityLog.Attributes.Add("class", "tab_notselected")
                tabPlafon.Attributes.Add("class", "tab_notselected")
                tabInvoicePaid.Attributes.Add("class", "tab_notselected")
                tabChangeDueDate.Attributes.Add("class", "tab_notselected")
            Case "ActivityLog"
				tabAplikasi.Attributes.Add("class", "tab_notselected")
				tabAsset.Attributes.Add("class", "tab_notselected")
				tabFinancial.Attributes.Add("class", "tab_notselected")
				TabJournal.Attributes.Add("class", "tab_notselected")
                tabActivityLog.Attributes.Add("class", "tab_selected")
                tabPlafon.Attributes.Add("class", "tab_notselected")
                tabInvoicePaid.Attributes.Add("class", "tab_notselected")
                tabChangeDueDate.Attributes.Add("class", "tab_notselected")
            Case "Plafon"
                tabAplikasi.Attributes.Add("class", "tab_notselected")
                tabAsset.Attributes.Add("class", "tab_notselected")
                tabFinancial.Attributes.Add("class", "tab_notselected")
                TabJournal.Attributes.Add("class", "tab_notselected")
                tabActivityLog.Attributes.Add("class", "tab_notselected")
                tabPlafon.Attributes.Add("class", "tab_selected")
                tabInvoicePaid.Attributes.Add("class", "tab_notselected")
                tabChangeDueDate.Attributes.Add("class", "tab_notselected")
            Case "InvoicePaid"
                tabAplikasi.Attributes.Add("class", "tab_notselected")
                tabAsset.Attributes.Add("class", "tab_notselected")
                tabFinancial.Attributes.Add("class", "tab_notselected")
                TabJournal.Attributes.Add("class", "tab_notselected")
                tabActivityLog.Attributes.Add("class", "tab_notselected")
                tabPlafon.Attributes.Add("class", "tab_notselected")
                tabInvoicePaid.Attributes.Add("class", "tab_selected")
                tabChangeDueDate.Attributes.Add("class", "tab_notselected")
            Case "ChangeDueDate"
                tabAplikasi.Attributes.Add("class", "tab_notselected")
                tabAsset.Attributes.Add("class", "tab_notselected")
                tabFinancial.Attributes.Add("class", "tab_notselected")
                TabJournal.Attributes.Add("class", "tab_notselected")
                tabActivityLog.Attributes.Add("class", "tab_notselected")
                tabPlafon.Attributes.Add("class", "tab_notselected")
                tabInvoicePaid.Attributes.Add("class", "tab_notselected")
                tabChangeDueDate.Attributes.Add("class", "tab_selected")
            Case Else
                tabAplikasi.Attributes.Add("class", "tab_selected")
                tabAsset.Attributes.Add("class", "tab_notselected")
                tabFinancial.Attributes.Add("class", "tab_notselected")
				TabJournal.Attributes.Add("class", "tab_notselected")
                tabActivityLog.Attributes.Add("class", "tab_notselected")
                tabPlafon.Attributes.Add("class", "tab_notselected")
        End Select
    End Sub
End Class