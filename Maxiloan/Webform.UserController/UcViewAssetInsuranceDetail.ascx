﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UcViewAssetInsuranceDetail.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.UcViewAssetInsuranceDetail" %>
<script src="../Maxiloan.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript">
    function OpenWinViewPolicyNo(BranchID, AgreementNo, strasset, strins, strApplication) {
        window.open('ViewPolicyDetail.aspx?BranchID=' + BranchID + '&AgreementNo=' + AgreementNo + '&AssetSeqNo=' + strasset + '&InsSeqNo=' + strins + '&ApplicationID=' + strApplication + '&Back=InsDetail' + '&style=Insurance', 'UserLookup', 'left=50, top=10, width=600, height=480, menubar=0, scrollbars=yes');
    }
</script>
<asp:Label ID="LblApplicationID" Visible="False" runat="server"></asp:Label>
<asp:Label ID="LblBranchID" Visible="False" runat="server"></asp:Label>
<asp:Label ID="lblAgreement" runat="server" Visible="False">lblAgreement</asp:Label>
<asp:Label ID="lblAsset" runat="server" Visible="False">lblAsset</asp:Label>
<asp:Label ID="lblIns" runat="server" Visible="False">lblIns</asp:Label>
<div class="form_box_usercontrol">
    <div class="form_l_usercontrol">
        <label class="label_split">
            Perusahaan Asuransi
        </label>
        <asp:Label ID="LblInsuranceCompany" runat="server"></asp:Label>
    </div>
    <div class="form_r_usercontrol">
        <label class="label_split">
            Nilai Pertanggungan
        </label>
        <asp:Label ID="LblSumInsured" runat="server"></asp:Label>
    </div>
</div>
<div class="form_box_usercontrol">
    <div class="form_l_usercontrol">
        <label class="label_split">
            DiAsuransi Oleh
        </label>
        <asp:Label ID="LblInsuredByDescr" runat="server"></asp:Label>
    </div>
    <div class="form_r_usercontrol">
        <label class="label_split">
            DiBayar Oleh
        </label>
        <asp:Label ID="LblPaidByDescr" runat="server"></asp:Label>
    </div>
</div>
<div class="form_box_usercontrol">
    <div class="form_l_usercontrol">
        <label class="label_split">
            Periode Cover
        </label>
        <asp:Label ID="lblStartDate" runat="server"></asp:Label>&nbsp;S/D&nbsp;
        <asp:Label ID="lblEndDate" runat="server"></asp:Label>
    </div>
    <div class="form_r_usercontrol">
        <label class="label_split">
            Tanggal Pemiliah Pers Asuransi
        </label>
        <asp:Label ID="LblInscoSelectionDate" runat="server"></asp:Label>
    </div>
</div>
<div class="form_box_usercontrol">
    <div class="form_l_usercontrol">
        <label class="label_split">
            No SPPA
        </label>
        <asp:Label ID="LblSPPANo" runat="server"></asp:Label>
    </div>
    <div class="form_r_usercontrol">
        <label class="label_split">
            Tanggal SPPA
        </label>
        <asp:Label ID="LblSPPADate" runat="server"></asp:Label>
    </div>
</div>
<div class="form_box_usercontrol">
    <div class="form_l_usercontrol">
        <label class="label_split">
            No Invoice
        </label>
        <asp:Label ID="LblInvoiceNo" runat="server"></asp:Label>
    </div>
    <div class="form_r_usercontrol">
        <label class="label_split">
            Tanggal Invoice
        </label>
        <asp:Label ID="LblInvoiceDate" runat="server"></asp:Label>
    </div>
</div>
<div class="form_box_usercontrol">
    <div class="form_l_usercontrol">
        <label class="label_split">
            Tanggal Aktivasi
        </label>
        <asp:Label ID="lblActivationDate" runat="server"></asp:Label>
    </div>
</div>
<div class="form_box_usercontrol">
    <div class="form_l_usercontrol">
        <label class="label_split">
            No Polis
        </label>
        <asp:HyperLink ID="lnkPolicyNo" runat="server"></asp:HyperLink>
    </div>
    <div class="form_r_usercontrol">
        <label class="label_split">
            Tanggal Terima Polis
        </label>
        <asp:Label ID="lblPolicyReceivedDate" runat="server"></asp:Label>
    </div>
</div>
<div class="form_box_usercontrol">
    <div class="form_l_usercontrol">
        <label class="label_split">
            Refund Ke Supplier
        </label>
        <asp:Label ID="lblRefundToSupplier" runat="server"></asp:Label>
    </div>
    <div class="form_r_usercontrol">
        <label class="label_split">
            Indikasi Prosentase Refund
        </label>
        <asp:Label ID="lblIndicationRefundPercentage" runat="server"></asp:Label>%
    </div>
</div>
<div class="form_box_usercontrol">
    <div class="form_l_usercontrol">
        <label class="label_split">
            Catatan Accessories
        </label>
        <asp:Label ID="lblAccNotes" runat="server"></asp:Label>
    </div>
    <div class="form_r_usercontrol">
        <label class="label_split">
            Catatan Asuransi
        </label>
        <asp:Label ID="lblInsNotes" runat="server"></asp:Label>
    </div>
</div>
<div style="display:none">
<div class="form_box_usercontrol_header">
    <div class="form_l_usercontrol">
        <h4>
            PREMI KE CUSTOMER
        </h4>
    </div>
    <div class="form_r_usercontrol">
        <h4>
            PREMI KE PERUSAHAAN ASURANSI
        </h4>
    </div>
</div>
<div class="form_box_usercontrol">
    <div class="form_l_usercontrol">
        <label class="label_split">
            Premi Utama
        </label>
        <asp:Label ID="lblPremiUtamaCus" runat="server"></asp:Label>
    </div>
    <div class="form_r_usercontrol">
        <label class="label_split">
            Premi Utama
        </label>
        <asp:Label ID="lblPremiUtamaCom" runat="server"></asp:Label>
    </div>
</div>
<div class="form_box_usercontrol">
    <div class="form_l_usercontrol">
        <label class="label_split">
            Premi PTL
        </label>
        <asp:Label ID="lblPremiPTLCus" runat="server"></asp:Label>
    </div>
    <div class="form_r_usercontrol">
        <label class="label_split">
            Premi PTL
        </label>
        <asp:Label ID="lblPremiPTLCom" runat="server"></asp:Label>
    </div>
</div>
<div class="form_box_usercontrol">
    <div class="form_l_usercontrol">
        <label class="label_split">
            Premi Flood
        </label>
        <asp:Label ID="lblPremiFloodCus" runat="server"></asp:Label>
    </div>
    <div class="form_r_usercontrol">
        <label class="label_split">
            Premi Flood
        </label>
        <asp:Label ID="lblPremiFloodCom" runat="server"></asp:Label>
    </div>
</div>
<div class="form_box_usercontrol">
    <div class="form_l_usercontrol">
        <label class="label_split">
            Premi EQVET
        </label>
        <asp:Label ID="lblPremiEQVETCus" runat="server"></asp:Label>
    </div>
    <div class="form_r_usercontrol">
        <label class="label_split">
            Premi EQVET
        </label>
        <asp:Label ID="lblPremiEQVETCom" runat="server"></asp:Label>
    </div>
</div>
<div class="form_box_usercontrol">
    <div class="form_l_usercontrol">
        <label class="label_split">
            Premi SRCC
        </label>
        <asp:Label ID="lblPremiSRCCCus" runat="server"></asp:Label>
    </div>
    <div class="form_r_usercontrol">
        <label class="label_split">
            Premi SRCC
        </label>
        <asp:Label ID="lblPremiSRCCCom" runat="server"></asp:Label>
    </div>
</div>
<div class="form_box_usercontrol">
    <div class="form_l_usercontrol">
        <label class="label_split">
            Premi Terorisme
        </label>
        <asp:Label ID="lblPremiTerorismeCus" runat="server"></asp:Label>
    </div>
    <div class="form_r_usercontrol">
        <label class="label_split">
            Premi Terorisme
        </label>
        <asp:Label ID="lblPremiTerorismeCom" runat="server"></asp:Label>
    </div>
</div>
<div class="form_box_usercontrol">
    <div class="form_l_usercontrol">
        <label class="label_split">
            Premi PA
        </label>
        <asp:Label ID="lblPremiPACus" runat="server"></asp:Label>
    </div>
    <div class="form_r_usercontrol">
        <label class="label_split">
            Premi PA
        </label>
        <asp:Label ID="lblPremiPACom" runat="server"></asp:Label>
    </div>
</div>
<div class="form_box_usercontrol">
    <div class="form_l_usercontrol">
        <label class="label_split">
            Premi PA Driver
        </label>
        <asp:Label ID="lblPremiPADriverCus" runat="server"></asp:Label>
    </div>
    <div class="form_r_usercontrol">
        <label class="label_split">
            Premi PA Driver
        </label>
        <asp:Label ID="lblPremiPADriverCom" runat="server"></asp:Label>
    </div>
</div>
<div class="form_box_usercontrol">
    <div class="form_l_usercontrol">
        <label class="label_split">
            
        </label>
        <asp:Label ID="Label1" runat="server"></asp:Label>
    </div>
    <div class="form_r_usercontrol">
        <label class="label_split">
            Biaya Admin
        </label>
        <asp:Label ID="lblBiayaAdminCom" runat="server"></asp:Label>
    </div>
</div>
<div class="form_box_usercontrol">
    <div class="form_l_usercontrol">
        <label class="label_split">
            
        </label>
        <asp:Label ID="Label3" runat="server"></asp:Label>
    </div>
    <div class="form_r_usercontrol">
        <label class="label_split">
            Biaya Materai
        </label>
        <asp:Label ID="lblBiayaMateraiCom" runat="server"></asp:Label>
    </div>
</div>
<div class="form_box_usercontrol">
    
    <div class="form_r_usercontrol">
        <label class="label_split">
            Total Premi
        </label>
        <asp:Label ID="lblTotalPremiCom" runat="server"></asp:Label>
    </div>
</div>
</div>
<div class="form_box_usercontrol">
    <div class="form_l_usercontrol">
        <label class="label_split">
            Total Premi
        </label>
        <asp:Label ID="lblTotalPremiCus" runat="server"></asp:Label>
    </div>
</div>
<div class="form_box_usercontrol">
    <div class="form_l_usercontrol">
        <label class="label_split">
            Total Rate Premi
        </label>
        <asp:Label ID="lblTotalRatePremi" runat="server"></asp:Label>
    </div>
    <div class="form_r_usercontrol">
    </div>
</div>
<div class="form_box_usercontrol">
    <div class="form_l_usercontrol">
        <label class="label_split">
            Premi CP
        </label>
        <asp:Label ID="lblTotalRatePremiCP" runat="server"></asp:Label>
    </div>
    <div class="form_r_usercontrol">
    </div>
</div>
<div class="form_box_usercontrol">
    <div class="form_l_usercontrol">
        <label class="label_split">
            Premi JK
        </label>
        <asp:Label ID="lblTotalRatePremiJK" runat="server"></asp:Label>
    </div>
    <div class="form_r_usercontrol">
    </div>
</div>
<%--<div class="form_box_usercontrol_header">
    <div class="form_single">
        <h4>
            PREMI KE CUSTOMER
        </h4>
    </div>
</div>
<div class="form_box_usercontrol">
    <div class="form_l_usercontrol">
        <label class="label_split">
            Premi Utama
        </label>
        <asp:Label ID="lblMainPremiumToCust" runat="server"></asp:Label>
    </div>
    <div class="form_r_usercontrol">
        <label class="label_split">
            Loading Fee
        </label>
        <asp:Label ID="lblloadingfeetocust" runat="server"></asp:Label>
    </div>
</div>
<div class="form_box_usercontrol">
    <div class="form_l_usercontrol">
        <label class="label_split">
            Premi SRCC
        </label>
        <asp:Label ID="lblSRCCPremiumToCust" runat="server"></asp:Label>
    </div>
    <div class="form_r_usercontrol">
        <label class="label_split">
            Premi Flood
        </label>
        <asp:Label ID="lblfloodpremiumtocust" runat="server"></asp:Label>
    </div>
</div>
<div class="form_box_usercontrol">
    <div class="form_l_usercontrol">
        <label class="label_split">
            Premi TPL
        </label>
        <asp:Label ID="lblTPLPremiumToCust" runat="server"></asp:Label>
    </div>
    <div class="form_r_usercontrol">
        <label class="label_split">
            Jumlah DiBayar Customer
        </label>
        <asp:Label ID="LblPaidAmountbyCust" runat="server"></asp:Label>
    </div>
</div>
<div class="form_box_usercontrol">
    <div class="form_l_usercontrol">
        <label class="label_split">
            Biaya Admin
        </label>
        <asp:Label ID="lblAdminFeeToCust" runat="server"></asp:Label>
    </div>
    <div class="form_r_usercontrol">
        <label class="label_split">
            Biaya Meterai
        </label>
        <asp:Label ID="lblMeteraiFeeToCust" runat="server"></asp:Label>
    </div>
</div>
<div class="form_box_usercontrol">
    <div class="form_l_usercontrol">
        <label class="label_split">
            Premi
        </label>
        <asp:Label ID="lblPremiumToCust" runat="server"></asp:Label>
    </div>
    <div class="form_r_usercontrol">
        <label class="label_split">
            Discount Premi
        </label>
        <asp:Label ID="lblDiscToCustAmount" runat="server"></asp:Label>
    </div>
</div>
<div class="form_box_usercontrol">
    <div class="form_l_usercontrol">
        <label class="label_split">
            Total Premi
        </label>
        <asp:Label ID="lblTotalPremium" runat="server"></asp:Label>
    </div>
</div>
<div class="form_box_usercontrol_header">
    <div class="form_single">
        <h4>
            PREMI KE PERUSAHAAN ASURANSI
        </h4>
    </div>
</div>
<div class="form_box_usercontrol">
    <div class="form_l_usercontrol">
        <label class="label_split">
            Premi Utama
        </label>
        <asp:Label ID="lblMainPremiumToInsCo" runat="server"></asp:Label>
    </div>
    <div class="form_r_usercontrol">
        <label class="label_split">
            Loading Fee
        </label>
        <asp:Label ID="lblLoadingFeeToInsCo" runat="server"></asp:Label>
    </div>
</div>
<div class="form_box_usercontrol">
    <div class="form_l_usercontrol">
        <label class="label_split">
            Premi SRCC
        </label>
        <asp:Label ID="lblSRCCToInsCo" runat="server"></asp:Label>
    </div>
    <div class="form_r_usercontrol">
        <label class="label_split">
            Premi Flood
        </label>
        <asp:Label ID="lblFloodToInsco" runat="server"></asp:Label>
    </div>
</div>
<div class="form_box_usercontrol">
    <div class="form_l_usercontrol">
        <label class="label_split">
            Premi TPL
        </label>
        <asp:Label ID="lblTPLToInsco" runat="server"></asp:Label>
    </div>
    <div class="form_r_usercontrol">
        <label class="label_split">
            Jumlah DiBayar ke Pers Asuransi
        </label>
        <asp:Label ID="LblPaidAmountByInsco" runat="server"></asp:Label>
    </div>
</div>
<div class="form_box_usercontrol">
    <div class="form_l_usercontrol">
        <label class="label_split">
            Biaya Admin
        </label>
        <asp:Label ID="lblAdminFee" runat="server"></asp:Label>
    </div>
    <div class="form_r_usercontrol">
        <label class="label_split">
            Biaya Materai
        </label>
        <asp:Label ID="LblMeteraIFee" runat="server"></asp:Label>
    </div>
</div>
<div class="form_box_usercontrol">
    <div class="form_l_usercontrol">
        <label class="label_split">
            Total Premi
        </label>
        <asp:Label ID="lblPremiumAmountToInsco" runat="server"></asp:Label>
    </div>
</div>--%>
