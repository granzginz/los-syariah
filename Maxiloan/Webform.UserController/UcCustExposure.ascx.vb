﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
#End Region

Public Class UcCustExposure
    Inherits AccMntControlBased

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub
    Public Sub CustomerExposure()

        Dim oController As New UCExposureController
        Dim oCustomClass As New Parameter.AccMntBase
        With oCustomClass
            .strConnection = GetConnectionString
            .CustomerID = Me.CustomerID
        End With
        oCustomClass = oController.GetCustExposure(oCustomClass)
        With oCustomClass

            lblOSBalance.Text = FormatNumber(.OutstandingBalanceAR, 2)
            lblOSPrincipal.Text = FormatNumber(.OutstandingPrincipal, 2)
            lblOSInterest.Text = FormatNumber(.OutStandingInterest, 2)

            LblMaxOfOSBalance.Text = FormatNumber(.MaxOutstandingBalance, 2)
            LblMaxOfOverDueDays.Text = CStr(.MaxOverdueDays)
            LblInstallmentAmount.Text = FormatNumber(.MaximumInstallment, 2)
            LblOverdueAmount.Text = FormatNumber(.MaxOverDueAmount, 2)

            lblActiveAgreement.Text = CStr(.NumofActiveAgreement)
            lblAssetRepossessed.Text = CStr(.NumOfAssetRepossed)
            lblInProcessAgreement.Text = CStr(.NumofInProcessAgreement)
            lblAssetInventoried.Text = CStr(.NumOfAssetInventoried)
            lblAssetInFinancing.Text = CStr(.NumOfAssetInFinancing)
            lblWrittenOffAgreement.Text = CStr(.NumOfWrittenOffAgreement)
            LblRejectedAgreement.Text = CStr(.NumOfRejectedAgreement)
            LblNonAccrualAgreement.Text = CStr(.NumOfNonAccrualAgreement)
            LblCancelledAgreement.Text = CStr(.NumOfCancelledAgreement)
            LblBounceCheque.Text = CStr(.NumOfBounceCheque)

            lblBucket1text.Text = .Bucket1Principle
            lblBucket2text.Text = .Bucket2Principle
            lblBucket3text.Text = .Bucket3Principle
            lblBucket4text.Text = .Bucket4Principle
            lblBucket5text.Text = .Bucket5Principle
            lblBucket6Text.Text = .Bucket6Principle
            lblBucket7text.Text = .Bucket7Principle
            lblBucket8text.Text = .Bucket8Principle
            lblBucket9text.Text = .Bucket9Principle
            lblBucket10Text.Text = .Bucket10Principle

            lblBucket1.Text = FormatNumber(.Bucket1_gross, 2)
            lblBucket2.Text = FormatNumber(.Bucket2_gross, 2)
            lblBucket3.Text = FormatNumber(.Bucket3_gross, 2)
            lblBucket4.Text = FormatNumber(.Bucket4_gross, 2)
            lblBucket5.Text = FormatNumber(.Bucket5_gross, 2)
            lblBucket6.Text = FormatNumber(.Bucket6_gross, 2)
            lblBucket7.Text = FormatNumber(.Bucket7_gross, 2)
            lblBucket8.Text = FormatNumber(.Bucket8_gross, 2)
            lblBucket9.Text = FormatNumber(.Bucket9_gross, 2)
            lblBucket10.Text = FormatNumber(.Bucket10_gross, 2)
            lblTotalAllBucket.Text = FormatNumber(.TotalAllBucket, 2)
        End With

    End Sub

End Class