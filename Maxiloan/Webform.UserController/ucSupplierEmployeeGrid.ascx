﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucSupplierEmployeeGrid.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.ucSupplierEmployeeGrid" %>
<asp:UpdatePanel runat="server" ID="UpdatePanel">
    <ContentTemplate>
        <asp:DataGrid ID="gridSupplierEmployee" runat="server" Width="100%" DataKeyField="SupplierEmployeeID"
            AutoGenerateColumns="False" BorderWidth="0px" CellPadding="3" CellSpacing="1" CssClass="grid_general">
            <HeaderStyle CssClass="th"/> 
            <ItemStyle CssClass ="item_grid" /> 
            <Columns>
                <asp:ButtonColumn ButtonType="LinkButton" CommandName="SELECT" CausesValidation="true"
                    Text="SELECT" ValidationGroup="<%= validationGroup %>"></asp:ButtonColumn>
                <asp:BoundColumn HeaderText="NAMA" DataField="SupplierEmployeeName"></asp:BoundColumn>
                <asp:BoundColumn Visible="False" DataField="SupplierEmployeePositionID"></asp:BoundColumn>
                <asp:BoundColumn HeaderText="JABATAN" DataField="SupplierEmployeePosition"></asp:BoundColumn>
                <asp:BoundColumn Visible="False" DataField="SupplierEmployeeBankID"></asp:BoundColumn>
                <asp:BoundColumn Visible="False" DataField="SupplierEmployeeBankName"></asp:BoundColumn>
                <asp:BoundColumn Visible="False" DataField="SupplierEmployeeBankBranchId"></asp:BoundColumn>
                <asp:BoundColumn Visible="False" DataField="SupplierEmployeeBankBranch"></asp:BoundColumn>
                <asp:BoundColumn Visible="False" DataField="SupplierEmployeeAccountNo"></asp:BoundColumn>
                <asp:BoundColumn Visible="False" DataField="SupplierEmployeeAccountName"></asp:BoundColumn>
                <asp:BoundColumn Visible="False" DataField="IsActive"></asp:BoundColumn>
            </Columns>
        </asp:DataGrid>
    </ContentTemplate>
</asp:UpdatePanel>
