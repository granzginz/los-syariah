﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UcCustExposure.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.UcCustExposure" %>
<div class="form_title">
    <div class="form_single">
        <h3>
            EXPOSURE DATA CUSTOMER
        </h3>
    </div>
</div>
<div class="form_box">
    <div class="form_single">
        <h5>
            OUTSTANDING AMOUNT
        </h5>
    </div>
</div>
<div class="form_box">
    <div class="form_single">
        <label>
            Sisa Saldo A/R</label>
        <asp:Label ID="lblOSBalance" runat="server"></asp:Label>
    </div>
</div>
<div class="form_box">
    <div class="form_single">
        <label>
            Sisa Pokok
        </label>
        <asp:Label ID="lblOSPrincipal" runat="server"></asp:Label>
    </div>
</div>
<div class="form_box">
    <div class="form_single">
        <label>
            Sisa Bunga
        </label>
        <asp:Label ID="lblOSInterest" runat="server"></asp:Label>
    </div>
</div>
<div class="form_box_title">
    <div class="form_single">
        <h5>
            MAX DARI
        </h5>
    </div>
</div>
<div class="form_box">
    <div>
        <div class="form_left">
            <label>
                Sisa Saldo
            </label>
            <asp:Label ID="LblMaxOfOSBalance" runat="server" CssClass="numberAlign label"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Jumlah Hari Telat
            </label>
            <asp:Label ID="LblMaxOfOverDueDays" runat="server" CssClass="numberAlign label"></asp:Label>
        </div>
    </div>
</div>
<div class="form_box">
    <div>
        <div class="form_left">
            <label>
                Jumlah Angsuran
            </label>
            <asp:Label ID="LblInstallmentAmount" runat="server" CssClass="numberAlign label"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Jumlah Tunggakan
            </label>
            <asp:Label ID="LblOverdueAmount" runat="server" CssClass="numberAlign label"></asp:Label>
        </div>
    </div>
</div>
<div class="form_box_title">
    <div class="form_single">
        <h5>
            NO DARI
        </h5>
    </div>
</div>
<div class="form_box">
    <div>
        <div class="form_left">
            <label>
                Kontrak yg Aktif</label>
            <asp:Label ID="lblActiveAgreement" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Asset DiTarik
            </label>
            <asp:Label ID="lblAssetRepossessed" runat="server"></asp:Label>
        </div>
    </div>
</div>
<div class="form_box">
    <div>
        <div class="form_left">
            <label>
                Kontrak dalam Proses
            </label>
            <asp:Label ID="lblInProcessAgreement" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Asset Masuk Inventori
            </label>
            <asp:Label ID="lblAssetInventoried" runat="server"></asp:Label>
        </div>
    </div>
</div>
<div class="form_box">
    <div>
        <div class="form_left">
            <label>
                Asset Dalam Financing
            </label>
            <asp:Label ID="lblAssetInFinancing" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Kontrak Written Off
            </label>
            <asp:Label ID="lblWrittenOffAgreement" runat="server"></asp:Label>
        </div>
    </div>
</div>
<div class="form_box">
    <div>
        <div class="form_left">
            <label>
                Kontrak Rejected
            </label>
            <asp:Label ID="LblRejectedAgreement" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Kontrak Stop Accrued
            </label>
            <asp:Label ID="LblNonAccrualAgreement" runat="server"></asp:Label>
        </div>
    </div>
</div>
<div class="form_box">
    <div>
        <div class="form_left">
            <label>
                Kontrak Batal
            </label>
            <asp:Label ID="LblCancelledAgreement" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Tolakan CEK/BG
            </label>
            <asp:Label ID="LblBounceCheque" runat="server"></asp:Label>
        </div>
    </div>
</div>

<div class="form_box_title">
    <div class="form_quarter">
        <h5>
            BUCKET
        </h5>
    </div>
    <div class="form_quarter numberAlign label">
        <h5>
            TOTAL PAST DUE
        </h5>
    </div>
    <div class="form_quarter">
        <h5>
            BUCKET
        </h5>
    </div>
    <div class="form_quarter numberAlign label">
        <h5>
            TOTAL PAST DUE
        </h5>
    </div>
</div>
<div class="form_box">
    <div class="form_quarter">
        <asp:Label ID="lblBucket1text" runat="server"></asp:Label>
    </div>
    <div class="form_quarter">
        <asp:Label ID="lblBucket1" runat="server" CssClass = "numberAlign label"></asp:Label>
    </div>
    <div class="form_quarter">
        <label>
            <asp:Label ID="lblBucket6Text" runat="server"></asp:Label></label>
    </div>
    <div class="form_quarter">
        <asp:Label ID="lblBucket6" runat="server"  CssClass = "numberAlign label"></asp:Label>
    </div>
</div>
<div class="form_box">
    <div class="form_quarter">
        <asp:Label ID="lblBucket2text" runat="server"></asp:Label>
    </div>
    <div class="form_quarter">
        <asp:Label ID="lblBucket2" runat="server"  CssClass = "numberAlign label"></asp:Label>
    </div>
    <div class="form_quarter">
        <label>
            <asp:Label ID="lblBucket7text" runat="server"></asp:Label></label>
    </div>
    <div class="form_quarter">
        <asp:Label ID="lblBucket7" runat="server"  CssClass = "numberAlign label"></asp:Label>
    </div>
</div>
<div class="form_box">
    <div class="form_quarter">
        <asp:Label ID="lblBucket3Text" runat="server"></asp:Label>
    </div>
    <div class="form_quarter">
        <asp:Label ID="lblBucket3" runat="server"  CssClass = "numberAlign label"></asp:Label>
    </div>
    <div class="form_quarter">
        <label>
            <asp:Label ID="lblBucket8Text" runat="server"></asp:Label></label>
    </div>
    <div class="form_quarter">
        <asp:Label ID="lblBucket8" runat="server"  CssClass = "numberAlign label"></asp:Label>
    </div>
</div>
<div class="form_box">
    <div class="form_quarter">
        <asp:Label ID="lblBucket4Text" runat="server"></asp:Label>
    </div>
    <div class="form_quarter">
        <asp:Label ID="lblBucket4" runat="server"  CssClass = "numberAlign label"></asp:Label>
    </div>
    <div class="form_quarter">
        <label>
            <asp:Label ID="lblBucket9Text" runat="server"></asp:Label></label>
    </div>
    <div class="form_quarter">
        <asp:Label ID="lblBucket9" runat="server"  CssClass = "numberAlign label"></asp:Label>
    </div>
</div>
<div class="form_box">
    <div class="form_quarter">
        <asp:Label ID="lblBucket5Text" runat="server"></asp:Label>
    </div>
    <div class="form_quarter">
        <asp:Label ID="lblBucket5" runat="server"  CssClass = "numberAlign label"></asp:Label>
    </div>
    <div class="form_quarter">
        <label>
            <asp:Label ID="lblBucket10Text" runat="server"></asp:Label></label>
    </div>
    <div class="form_quarter">
        <asp:Label ID="lblBucket10" runat="server"  CssClass = "numberAlign label"></asp:Label>
    </div>
</div>
<div class="form_box">
    <div class="form_quarter">
        <asp:Label ID="Label1" runat="server"></asp:Label>
    </div>
    <div class="form_quarter">
        <asp:Label ID="Label2" runat="server"></asp:Label>
    </div>
    <div class="form_quarter">
        <h5>
            TOTAL
        </h5>
    </div>
    <div class="form_quarter">
        <asp:Label ID="lblTotalAllBucket" runat="server"  CssClass = "numberAlign label"></asp:Label>
    </div>
</div>
