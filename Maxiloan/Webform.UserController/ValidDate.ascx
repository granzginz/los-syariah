﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ValidDate.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.ValidDate" %>
<%
    Dim AppInfo As String, App As String
    AppInfo = Request.ServerVariables("PATH_INFO")
    App = AppInfo.substring(1, AppInfo.indexOf("/", 1) - 1)
%>
<script language="javascript" type="text/javascript">   
    function isValidDate(source, arguments) {
        try {
            var ArrDate = arguments.Value.split("/");

            if (ArrDate.length != 3)
                arguments.IsValid = false;
            else {
                if (ArrDate[2].length != 4) {
                    arguments.IsValid = false;
                }
                else {
                    if (isNaN(ArrDate[2])) {
                        arguments.IsValid = false;
                    }
                    else {
                        var objDate1 = new Date();
                        objDate1.setFullYear(ArrDate[2]);
                        objDate1.setMonth(0);
                        objDate1.setDate(1);

                        objDate1.setDate(parseInt(ArrDate[0], 10));
                        objDate1.setMonth(parseInt(ArrDate[1], 10) - 1);

                        if (objDate1.getDate() == parseInt(ArrDate[0], 10) && objDate1.getMonth() == parseInt(ArrDate[1], 10) - 1 && objDate1.getFullYear() == parseInt(ArrDate[2], 10)) {
                            arguments.IsValid = true;
                        } else {
                            arguments.IsValid = false;
                        }
                    }
                }
            }
        }
        catch (e) {
            arguments.IsValid = false;
        }

        return arguments.IsValid;
    }

    function OpenWin(strParam, X, Y) {
        //var objWindow = window.open('http://<%=Request.servervariables("SERVER_NAME")%>/<%=App%>/webform.usercontroller/Calendar.asp?parent=' + strParam + '&sesBusinessDate=<%=session("sesBusinessDate")%>&postback=<%=lcase(txtDate.AutoPostBack)%>', 'Calender', 'height=200,width=190,left=' + X + ',top=' + Y + ',menubar=no,status=no,toolbar=no,scrollbars=no,resizable=no,titlebar=no');
        var objWindow = window.open('~/webform.usercontroller/Calendar.asp?parent=' + strParam + '&sesBusinessDate=<%=session("sesBusinessDate")%>&postback=<%=lcase(txtDate.AutoPostBack)%>', 'Calender', 'height=200,width=190,left=' + X + ',top=' + Y + ',menubar=no,status=no,toolbar=no,scrollbars=no,resizable=no,titlebar=no');       
        objWindow.focus();
        return false;
    }

    function dosubmit() {
        document.forms[0].submit();
    }
</script>
<asp:TextBox ID="txtDate" Visible="true"  Columns="10" runat="server"
    MaxLength="10" Width="70px"></asp:TextBox>
<span id="divButton" runat="server">
    <%
        Dim AppInfo As String, App As String
        AppInfo = Request.ServerVariables("PATH_INFO")
        App = AppInfo.substring(1, AppInfo.indexOf("/", 1) - 1)
    %>
    <input type="image" id="<%= getClientID() %>_imgCalender" src="http://<%=(Request.servervariables("SERVER_NAME") & "/" & App )%>/Images/calendar02_icon.gif"
        onclick="javascript:return OpenWin('<%= getClientID() %>',window.event.clientX,window.event.clientY);" />
</span>
<input type="hidden" id="hidFlag"  />
<input type="hidden" id="isCalendarPostBack" name="isCalendarPostBack" />
<asp:CustomValidator ID="cvlDate" Visible="true" runat="server" ClientValidationFunction="isValidDate"
    OnServerValidate="isValidDate" ControlToValidate="txtDate" Display="dynamic"
    Enabled="true"></asp:CustomValidator>
<asp:RequiredFieldValidator ID="rfvRequired" Visible="true" runat="server" ControlToValidate="txtDate"
    Display="dynamic" Enabled="true" CssClass ="validator_general"></asp:RequiredFieldValidator>