﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
#End Region

Public Class UcPaymentInfo
    Inherits AccMntControlBased

#Region "Property"
#Region "Property Maximum Value"
    Public ReadOnly Property MaximumInstallment() As Double
        Get
            Return oCustomClass.MaximumInstallment
        End Get 
    End Property

    Public ReadOnly Property MaximumInstallmentDue() As Double
        Get
            Return oCustomClass.InstallmentDue
        End Get
    End Property

    Public ReadOnly Property MaximumInsurance() As Double
        Get
            Return oCustomClass.InsuranceDue
        End Get

    End Property

    Public ReadOnly Property MaximumInstallCollFee() As Double
        Get
            Return oCustomClass.InstallmentCollFee
        End Get
    End Property

    Public ReadOnly Property MaximumInsuranceCollFee() As Double
        Get
            Return oCustomClass.InsuranceCollFee
        End Get 
    End Property

    Public ReadOnly Property MaximumLCInstallFee() As Double
        Get
            Return oCustomClass.LcInstallment
        End Get 
    End Property

    Public ReadOnly Property MaximumLCInsuranceFee() As Double
        Get
            Return oCustomClass.LcInsurance
        End Get 
    End Property

    Public ReadOnly Property MaximumInsuranceClaimFee() As Double
        Get
            Return oCustomClass.InsuranceClaimExpense
        End Get 
    End Property

    Public ReadOnly Property MaximumSTNKRenewalFee() As Double
        Get
            Return oCustomClass.STNKRenewalFee
        End Get 
    End Property

    Public ReadOnly Property MaximumPDCBounceFee() As Double
        Get
            Return oCustomClass.PDCBounceFee
        End Get 
    End Property

    Public ReadOnly Property MaximumReposessionFee() As Double
        Get
            Return oCustomClass.RepossessionFee
        End Get 
    End Property

    Public ReadOnly Property MaximumOutStandingPrincipal() As Double
        Get
            Return oCustomClass.OutstandingPrincipal
        End Get 
    End Property

    Public ReadOnly Property MaximumOutStandingInterest() As Double
        Get
            Return oCustomClass.OutStandingInterest
        End Get 
    End Property

    Public Property IsTitle() As Boolean
        Get
            Return (CType(ViewState("IsTitle"), Boolean))
        End Get
        Set(ByVal Value As Boolean)
            ViewState("IsTitle") = Value
        End Set
    End Property

    Public ReadOnly Property PrepaidHoldStatus() As String
        Get
            Return oCustomClass.PrepaidHoldStatus
        End Get
    End Property

    Public ReadOnly Property ContractStatus() As String
        Get
            Return oCustomClass.ContractStatus
        End Get 
    End Property


    Public ReadOnly Property TotOSOverDue() As Double
        Get
            Return oCustomClass.GetTotalOSOverDue
        End Get
    End Property





#End Region
#End Region

    Public Property oCustomClass() As Parameter.AccMntBase
        Get
            Return CType(ViewState("OCUSTOMCLASS"), Parameter.AccMntBase)
        End Get
        Set(ByVal Value As Parameter.AccMntBase)
            ViewState("OCUSTOMCLASS") = Value
        End Set
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub
    Public Sub PaymentInfo()
        Dim oController As New UCPaymentInfoController
        Dim oCustomClass As New Parameter.AccMntBase
        With oCustomClass
            .strConnection = GetConnectionString()
            .ApplicationID = Me.ApplicationID
            .ValueDate = Me.ValueDate
        End With
        oCustomClass = oController.GetPaymentInfo(oCustomClass)
        PaymentInfo(oCustomClass)
    End Sub

    Public Sub PaymentInfoFactAndMDKJ()
        Dim oController As New UCPaymentInfoController
        Dim oCustomClass As New Parameter.AccMntBase
        With oCustomClass
            .strConnection = GetConnectionString()
            .ApplicationID = Me.ApplicationID
            .ValueDate = Me.ValueDate
            .InvoiceNo = Me.InvoiceNo
            .InvoiceSeqNo = Me.InvoiceSeqNo
        End With
        oCustomClass = oController.GetPaymentInfoFactAndMDKJ(oCustomClass)
        PaymentInfo(oCustomClass)
    End Sub

    Public Sub PaymentInfo(param As Parameter.AccMntBase)
        If Me.IsTitle Then
            pnltopi.Visible = False
        Else
            pnltopi.Visible = True
        End If
        oCustomClass = param 
        With oCustomClass
            labAsOf.Text = .ValueDate.ToString("dd/MM/yyyy")
            labInstallmentDue.Text = FormatNumber(.InstallmentDue, 2)
            labLCInstall.Text = FormatNumber(.LcInstallment, 2)
            labInstallColl.Text = FormatNumber(.InstallmentCollFee, 2)

            labInsuranceDue.Text = FormatNumber(.InsuranceDue, 2)
            labLCInsurance.Text = FormatNumber(.LcInsurance, 2)
            labInsuranceColl.Text = FormatNumber(.InsuranceCollFee, 2)

            labPDCBounceFee.Text = FormatNumber(.PDCBounceFee, 2)
            labSTNKFee.Text = FormatNumber(.STNKRenewalFee, 2)
            labInsuranceClaim.Text = FormatNumber(.InsuranceClaimExpense, 2)
            labRepossessionFee.Text = FormatNumber(.RepossessionFee, 2)

            labTotalOSOverDue.Text = FormatNumber(.GetTotalOSOverDue(), 2)
            'FormatNumber(.InstallmentDue + .LcInstallment + .InstallmentCollFee + _
            '                    .InsuranceDue + .InsuranceCollFee + .LcInsurance + _
            '                    .PDCBounceFee + .STNKRenewalFee + .InsuranceClaimExpense + _
            '                    .RepossessionFee, 2)
            Me.Prepaid = .Prepaid
            Me.InstallmentDue = .InstallmentDue

            ' Me.MaximumInstallment = .MaximumInstallment
            'Me.MaximumInstallmentDue = .InstallmentDue
            'Me.MaximumLCInstallFee = .LcInstallment
            'Me.MaximumInstallCollFee = .InstallmentCollFee

            'Me.MaximumInsurance = .InsuranceDue
            'Me.MaximumLCInsuranceFee = .LcInsurance
            'Me.MaximumInsuranceCollFee = .InsuranceCollFee

            'Me.MaximumPDCBounceFee = .PDCBounceFee
            'Me.MaximumSTNKRenewalFee = .STNKRenewalFee
            'Me.MaximumInsuranceClaimFee = .InsuranceClaimExpense
            'Me.MaximumReposessionFee = .RepossessionFee
            'Me.ContractStatus = .ContractStatus
            'Me.MaximumOutStandingInterest = .OutStandingInterest
            'Me.MaximumOutStandingPrincipal = .OutstandingPrincipal
            'Me.TotOSOverDue = .InstallmentDue + .LcInstallment + .InstallmentCollFee + _
            '                        .InsuranceDue + .InsuranceCollFee + .LcInsurance + _
            '                        .PDCBounceFee + .STNKRenewalFee + .InsuranceClaimExpense + _
            '                        .RepossessionFee
        End With
    End Sub

End Class