﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UcViewAssetInsuranceDetailGridTenor.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.UcViewAssetInsuranceDetailGridTenor" %>
<div class="form_box_usercontrol_header">
    <div class="form_single">
        <div class="grid_wrapper_ws">
            <asp:DataGrid ID="DgridTenor" runat="server" AutoGenerateColumns="False" BorderWidth="0px"
                CellPadding="3" CellSpacing="1" BorderStyle="None" DataKeyField="YearNum" CssClass="grid_general">
                <HeaderStyle CssClass="th"></HeaderStyle>
                <ItemStyle CssClass="item_grid" />
                <Columns>
                    <asp:TemplateColumn HeaderText="YEAR">
                        <ItemTemplate>
                            <asp:Label ID="lblGridInscoDYearNum" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.YearNum") %>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="PAID">
                        <ItemTemplate>
                            <asp:Label ID="lblPaidByCust" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PaidByCust") %>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="JENIS COVER">
                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblGridInscoDCoverage" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Description") %>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="RATE PREMI">
                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblRatePremi" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.InsuranceRate") %>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="PREMI UTAMA">
                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblGridInscoDMainPremium" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MainPremiumToInsCo", "{0:###,###,##0}") %>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="PLUS/MINUS">
                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblamountplusminus" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PlusMinus") & DataBinder.Eval(Container, "DataItem.AmountPlusMinus", "{0:###,###,##0}") %>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="PREMI TPL">
                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblGridInscoDTPLPremium" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TPLAmountToCust", "{0:###,###,##0}") %>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="PREMI FLOOD">
                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblGridInscoDFloodPremium" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FloodPremiumToCust", "{0:###,###,##0}") %>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="PREMI EQVET">
                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblGridInscoDEQVETPremium" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.EQVETPremiumToCust", "{0:###,###,##0}") %>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="PREMI SRCC">
                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblGridInscoDSRCCPremium" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SRCCPremiumToCust", "{0:###,###,##0}") %>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="PREMI TERORISME">
                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblGridInscoDTerrorismPremium" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TERRORISMPremiumToCust", "{0:###,###,##0}") %>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="LOADING FEE">
                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblGridInscoDLoadingFee" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.LoadingFeeToCust", "{0:###,###,##0}") %>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="PA PASS">
                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblGridInscoDPAPass" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PAAmountToCust", "{0:###,###,##0}") %>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="PA DRIVER">
                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblGridInscoDPADriver" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PADriverAmountToCust", "{0:###,###,##0}") %>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                </Columns>
            </asp:DataGrid>
        </div>
    </div>
</div>
