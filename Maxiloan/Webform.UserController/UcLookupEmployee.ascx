﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UcLookupEmployee.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.UcLookupEmployee" %>
<script language="javascript" type="text/javascript">
    function OpenWinEmployee(pEmployeeID, pEmployeeName, pStyle, pBranchID) {
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var x = screen.width;
        var y = screen.height;
        window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/General/LookUpEmployee.aspx?style=' + pStyle + '&employeeid=' + pEmployeeID + '&employeename=' + pEmployeeName + '&branchid=' + pBranchID, 'UserLookup', 'left=0, top=0, width=' + x +', height=' + y + ', menubar=0, scrollbars=yes');
    }
</script>
<input type="hidden" runat="server" id="hdnEmployeeID" />
<asp:Panel runat="server" ID="pnlEmployeeID">
    <div class="form_box_usercontrol">
        <div class="form_left_usercontrol">
            <label>
                ID Karyawan
            </label>
            <asp:TextBox ID="txtEmployeeID" runat="server" MaxLength="5"></asp:TextBox>
            <asp:HyperLink ID="hpLookup" runat="server" ImageUrl="../images/IconDetail.gif"></asp:HyperLink>
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator3" runat="server" Display="Dynamic"
                ErrorMessage="Please Fill Employee ID" ControlToValidate="txtEmployeeID" CssClass="validator_general"></asp:RequiredFieldValidator>
        </div>
    </div>
</asp:Panel>
<div class="form_box_usercontrol">
    <div class="form_left_usercontrol">
        <label>
            Nama Karyawan
        </label>
        <asp:TextBox ID="txtEmployeeName" runat="server" MaxLength="50" ReadOnly="True"></asp:TextBox>
        <asp:HyperLink ID="hpLookup1" runat="server" ImageUrl="../images/IconDetail.gif"></asp:HyperLink>
    </div>
</div>
