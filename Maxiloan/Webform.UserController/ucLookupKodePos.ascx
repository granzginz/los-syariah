﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucLookupKodePos.ascx.vb" Inherits="Maxiloan.Webform.UserController.ucLookupKodePos" %>
    <div runat="server" id="jlookupContent" />
        <div class="form_box_usercontrol">
            <div class="form_left_usercontrol">
                <label class="label_split">
                    Kelurahan</label>
                <asp:TextBox ID="txtKelurahan" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="form_box_usercontrol">
            <div class="form_left_usercontrol">
                <label class="label_split">
                    Kecamatan</label>
                <asp:TextBox ID="txtKecamatan" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="form_box_usercontrol">
            <div class="form_left_usercontrol">
                <label class="label_split">
                    Kota</label>
                <asp:TextBox ID="txtCity" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="form_box_usercontrol">
            <div class="form_left_usercontrol">
                <label class="label_split_req" runat="server" id="lblKodePos">
                    Kode Pos</label> <label runat="server" id="lblKodePosNotValidate">
                    Kode Pos</label>
                <asp:TextBox ID="txtZipCode" runat="server" CssClass="small_text"></asp:TextBox>
                   <button class="small buttongo blue" 
                            onclick ="OpenJLookup('<%= ResolveClientUrl("~/webform.Utility/jLookup/LookupKodePos.aspx?kode=" & txtZipCode.ClientID & "&city=" & txtCity.ClientID & "&kecamatan=" & txtKecamatan.ClientID  & "&kelurahan=" & txtKelurahan.ClientID) %>','Kode Pos','<%= jlookupContent.ClientID %>');return false;">...</button>  
                <asp:Button runat="server" ID="btnReset" CausesValidation="false" Text="Reset" CssClass="small buttongo gray" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Harap pilih KodePos"
                    ControlToValidate="txtZipCode" CssClass="validator_general" SetFocusOnError="true"></asp:RequiredFieldValidator>
            </div>
        </div>
 


