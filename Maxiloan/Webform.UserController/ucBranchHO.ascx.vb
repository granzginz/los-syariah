﻿Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.General.CommonCookiesHelper
Imports Maxiloan.Controller

Public Class ucBranchHO
    Inherits ControlBased

#Region " Private Const "
    Private m_controller As New DataUserControlController
#End Region

#Region "Property"


    Public Property BranchName() As String
        Get
            Return (CType(cmbbranch.SelectedItem.Text, String))
        End Get
        Set(ByVal Value As String)
            cmbbranch.SelectedIndex = cmbbranch.Items.IndexOf(cmbbranch.Items.FindByText(Value))
        End Set
    End Property


    Public Property BranchID() As String
        Get
            Return (CType(cmbbranch.SelectedItem.Value, String))
        End Get
        Set(ByVal Value As String)
            cmbbranch.SelectedIndex = cmbbranch.Items.IndexOf(cmbbranch.Items.FindByValue(Value))
        End Set
    End Property

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not IsPostBack Then

        End If
    End Sub

    Public Sub BindBranchHO(ByVal strWhere As String)
        'cmbbranch.Items.Clear()

        'Dim DtBranchHO As New DataTable
        'Dim dvBranchHO As New DataView
        'Dim strIsBranchHo As String = Me.sesBranchId & "BranchListView"

        'DtBranchHO = CType(Me.Cache.Item(strIsBranchHo), DataTable)

        'If DtBranchHO Is Nothing Then
        '    Dim dtBranchHOCache As New DataTable

        '    dtBranchHOCache = m_controller.GetBranchHO(GetConnectionString, strWhere)
        '    Me.Cache.Insert(strIsBranchHo, dtBranchHOCache, Nothing, DateTime.Now.AddHours(DEFAULT_TIME_DURATION_CACHE_HOUR), TimeSpan.Zero)
        '    DtBranchHO = CType(Me.Cache.Item(strIsBranchHo), DataTable)
        'End If

        'dvBranchHO = DtBranchHO.DefaultView

        'not using cache
        cmbbranch.Items.Clear()

        Dim DtBranchHO As New DataTable
        Dim dvBranchHO As New DataView

        DtBranchHO = m_controller.GetBranchHO(GetConnectionString, strWhere)
        dvBranchHO = DtBranchHO.DefaultView


        With cmbbranch
            .DataValueField = "ID"
            .DataTextField = "Name"
            .DataSource = dvBranchHO
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = "0"
        End With
    End Sub


End Class