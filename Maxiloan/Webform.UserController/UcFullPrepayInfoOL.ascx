﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UcFullPrepayInfoOL.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.UcFullPrepayInfoOL" %>
<div class="form_box_header">
    <div class="form_single">
        <h4>
            TOTAL PELUNASAN OPERATING LEASE
        </h4>
    </div>
</div>
<div class="form_box">
    <div>
        <div class="form_left">
            <label>
                Sisa Nilai Kontrak
            </label>
            <asp:Label ID="lblOSPrincipalAmount" runat="server" CssClass="numberAlign label"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Biaya Tarik
            </label>
            <asp:Label ID="lblRepossessionFee" runat="server" CssClass="numberAlign label"></asp:Label>    
        </div>
    </div>
</div>
<div class="form_box">
    <div>
        <div class="form_left">
            <label>
                Biaya Tolakan PDC
            </label>
            <asp:Label ID="lblPDCBounceFee" runat="server" CssClass="numberAlign label"></asp:Label>
        </div>
        <div class="form_right">
        <label>
                Denda keterlambatan Angsuran
            </label>
            <asp:Label ID="lblLCInstall" runat="server" CssClass="numberAlign label"></asp:Label>
        </div>
    </div>
</div>
<div class="form_box">
    <div>
        <div class="form_left">
            <label>
                Penalty & Biaya Administrasi
            </label>
            <asp:Label ID="lblTerminationPenaltyFee" runat="server" CssClass="numberAlign label"></asp:Label>
        </div>
        <div class="form_right" style="font-weight:bold">
            <label>
                TOTAL
            </label>
            <asp:Label ID="lblTotalPrepaymentAmountGross" runat="server" CssClass="numberAlign label"></asp:Label>
        </div>
    </div>
</div>
<div class="form_box">
    <div>
        <div class="form_left">
        </div>
        <div class="form_right" style="font-weight:bold">
            <label>
                Faktor Pengurang
            </label>
            <asp:Label ID="lblFaktorPengurang" runat="server" CssClass="numberAlign label"></asp:Label>
        </div>
    </div>
</div>
<asp:Panel ID="pnlTerminationFee" runat="server">
    <div class="form_box">
        <div>
            <div class="form_left">
                
            </div>
            <div class="form_right" style="background-color:#FEFF51;font-weight:bold">
                <label>
                    Total yang harus dibayarkan
                </label>
                <asp:Label ID="lblTotalPrepaymentAmount" runat="server" CssClass="numberAlign label"></asp:Label>
            </div>
        </div>
    </div>
</asp:Panel>
