﻿Imports Maxiloan.Controller

Public Class ucAPTypeNew
    Inherits ControlBased
#Region " Private Const "
    Private m_controller As New ImplementasiControler
#End Region

    Public ReadOnly Property selectedAPType As String
        Get
            Return cmbAPTypeNew.SelectedItem.Text
        End Get
    End Property

    Public Property Filter() As String
        Get
            Return CType(ViewState("Filter"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Filter") = Value
        End Set
    End Property

    Public Property selectedAPTypeID() As String
        Get
            Return CType(cmbAPTypeNew.SelectedItem.Value, String)
        End Get
        Set(ByVal Value As String)
            cmbAPTypeNew.SelectedIndex = cmbAPTypeNew.Items.IndexOf(cmbAPTypeNew.Items.FindByValue(Value))
        End Set
    End Property


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not IsPostBack Then
            BindAPTypeNew()
        End If
    End Sub

    Public Sub BindAPTypeNew()

        cmbAPTypeNew.Items.Clear()
        Dim DtApType As New DataTable
        Dim dvAPType As New DataView
        Dim customClass As New Parameter.Implementasi

        With customClass
            .strConnection = GetConnectionString()
            .SPName = "spPembPencairanList"
            .WhereCond = Filter
        End With

        customClass = m_controller.GetSP(customClass)
        DtApType = customClass.Listdata
        dvAPType = DtApType.DefaultView

        With cmbAPTypeNew
            .DataValueField = "ID"
            .DataTextField = "Description"
            .DataSource = dvAPType
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = "0"
            .Items.Insert(1, "ALL")
            .Items(1).Value = "ALL"
        End With


    End Sub
End Class