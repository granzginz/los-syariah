﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UcFullPrepayInfo.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.UcFullPrepayInfo" %>
<div class="form_box_header">
    <div class="form_single">
        <h4>
            TOTAL PELUNASAN DIPERCEPAT
        </h4>
    </div>
</div>
<div class="form_box">
    <div>
        <div class="form_left">
            <label>
                Sisa Pokok
            </label>
            <asp:Label ID="lblOSPrincipalAmount" runat="server" CssClass="numberAlign label"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Accrued Interest
            </label>
            <asp:label ID ="lblAccruedKeterangan" runat="server" Visible="false" >(Advance Interest)</asp:label>
            <asp:Label ID="lblAccruedInterest" runat="server" CssClass="numberAlign label"></asp:Label>
        </div>
    </div>
</div>
<div class="form_box">
    <div>
        <div class="form_left">
            <label>
                <%--Angsuran Jatuh Tempo--%>
                Bunga Jatuh Tempo
            </label>
            <asp:Label ID="lblInstallmentDue" runat="server" CssClass="numberAlign label"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Premi Asuransi Jatuh Tempo
            </label>
            <asp:Label ID="lblInsuranceDue" runat="server" CssClass="numberAlign label"></asp:Label>
        </div>
    </div>
</div>
<div class="form_box">
    <div>
        <div class="form_left">
            <label>
                Denda keterlambatan Angsuran
            </label>
            <asp:Label ID="lblLCInstall" runat="server" CssClass="numberAlign label"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Denda Keterlambatan AsuInransi
            </label>
            <asp:Label ID="lblLCInsurance" runat="server" CssClass="numberAlign label"></asp:Label>
        </div>
    </div>
</div>
<div class="form_box">
    <div>
        <div class="form_left">
            <label>
                Biaya Tagih Angsuran
            </label>
            <asp:Label ID="lblInstallColl" runat="server" CssClass="numberAlign label"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Biaya Tagih Asuransi
            </label>
            <asp:Label ID="lblInsuranceColl" runat="server" CssClass="numberAlign label"></asp:Label>
        </div>
    </div>
</div>
<div class="form_box">
    <div>
        <div class="form_left">
            <label>
                Biaya Tolakan PDC
            </label>
            <asp:Label ID="lblPDCBounceFee" runat="server" CssClass="numberAlign label"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Biaya Perpanjangan STNK/BBN
            </label>
            <asp:Label ID="lblSTNKFee" runat="server" CssClass="numberAlign label"></asp:Label>
        </div>
    </div>
</div>
<div class="form_box">
    <div>
        <div class="form_left">
            <label>
                Biaya Klaim Asuransi
            </label>
            <asp:Label ID="lblInsuranceClaim" runat="server" CssClass="numberAlign label"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Biaya Tarik
            </label>
            <asp:Label ID="lblRepossessionFee" runat="server" CssClass="numberAlign label"></asp:Label>
        </div>
    </div>
</div>
<div class="form_box">
    <div>
        <div class="form_left">
            <label>
                Penalty & Biaya Administrasi
            </label>
            <asp:Label ID="lblTerminationPenaltyFee" runat="server" CssClass="numberAlign label"></asp:Label>
        </div>
        <div class="form_right" style="font-weight:bold">
            <label>
                TOTAL
            </label>
            <asp:Label ID="lblTotalPrepaymentAmountGross" runat="server" CssClass="numberAlign label"></asp:Label>
        </div>
    </div>
</div>
<div class="form_box">
    <div>
        <div class="form_left">
            
        </div>
        <div class="form_right" style="font-weight:bold">
            <label>
                Faktor Pengurang
            </label>
            <asp:Label ID="lblFaktorPengurang" runat="server" CssClass="numberAlign label"></asp:Label>
        </div>
    </div>
</div>
<asp:Panel ID="pnlTerminationFee" runat="server">
    <div class="form_box">
        <div>
            <div class="form_left">
                
            </div>
            <div class="form_right" style="background-color:#FEFF51;font-weight:bold">
                <label>
                    Total Pelunasan Dipercepat
                </label>
                <asp:Label ID="lblTotalPrepaymentAmount" runat="server" CssClass="numberAlign label"></asp:Label>
            </div>
        </div>
    </div>
</asp:Panel>
