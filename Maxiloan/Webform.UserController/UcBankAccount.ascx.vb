﻿Imports Maxiloan.General
Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.Controller

Public Class UcBankAccount
    Inherits ControlBased
    Private Msg As New Maxiloan.Webform.WebBased
    Protected WithEvents UcLookupBankBranch As UcLookupBankBranch

#Region "Bank Account"
    Public Property BankID() As String
        Get

            If cboBankName.SelectedItem Is Nothing OrElse cboBankName.SelectedValue = "0" Then
                Return ""
            Else
                Return CType(cboBankName.SelectedItem.Value, String)
            End If

        End Get
        Set(ByVal BankID As String)
            cboBankName.SelectedIndex = cboBankName.Items.IndexOf(cboBankName.Items.FindByValue(BankID))
        End Set
    End Property
    Public Property BankName() As String
        Get
            Return CType(cboBankName.SelectedItem.Text, String)
        End Get
        Set(ByVal BankName As String)
            cboBankName.SelectedIndex = cboBankName.Items.IndexOf(cboBankName.Items.FindByText(BankName))
        End Set
    End Property
    Public Property AccountNo() As String
        Get
            Return CType(txtAccountNo.Text, String)
        End Get
        Set(ByVal AccountNo As String)
            txtAccountNo.Text = AccountNo
        End Set
    End Property
    Public Property AccountName() As String
        Get
            Return CType(txtAccountName.Text, String)
        End Get
        Set(ByVal AccountName As String)
            txtAccountName.Text = AccountName
        End Set
    End Property
    Public Property BankBranch() As String
        Get
            Return CType(txtBankBranchName.Text, String)
        End Get
        Set(ByVal BankBranch As String)
            txtBankBranchName.Text = BankBranch
        End Set
    End Property
    Public Property BankBranchId() As String
        Get
            Return hdnBankBranchId.Value
        End Get
        Set(ByVal BankBranchId As String)
            hdnBankBranchId.Value = BankBranchId
        End Set
    End Property
    Public Property BankCodeCabang() As String
        Get
            Return CType(txtBankCodeCabang.Text.Trim, String)
        End Get
        Set(ByVal BankCodeCabang As String)
            txtBankCodeCabang.Text = BankCodeCabang
        End Set
    End Property
    Public Property BankCodeBank() As String
        Get
            Return CType(txtBankCodeBank.Text.Trim, String)
        End Get
        Set(ByVal BankCodeBank As String)
            txtBankCodeBank.Text = BankCodeBank
        End Set
    End Property
    Public Property City() As String
        Get
            Return CType(txtCity.Text.Trim, String)
        End Get
        Set(ByVal City As String)
            txtCity.Text = City
        End Set
    End Property
    Public Property BankBranchName() As String
        Get
            Return CType(txtBankBranchName.Text.Trim, String)
        End Get
        Set(ByVal BankBranchName As String)
            txtBankBranchName.Text = BankBranchName
        End Set
    End Property
    Private Property InsCoBranchID() As String
        Get
            Return CStr(viewstate("InsCoBranchID"))
        End Get
        Set(ByVal InsCoBranchID As String)
            ViewState("InsCoBranchID") = InsCoBranchID
        End Set
    End Property
    Private Property BankNameState() As String
        Get
            Return CStr(viewstate("BankNameState"))
        End Get
        Set(ByVal BankNameState As String)
            ViewState("BankNameState") = BankNameState
        End Set
    End Property
    Public Property Style() As String
        Get
            Return CType(viewstate("Style"), String)
        End Get
        Set(ByVal Style As String)
            ViewState("Style") = Style
        End Set
    End Property
    Public Property BranchMandatory As Boolean
        Get
            Return CType(ViewState("BranchMandatory"), Boolean)
        End Get
        Set(BranchMandatory As Boolean)
            ViewState("BranchMandatory") = BranchMandatory
        End Set
    End Property

#End Region

#Region " Private Const "
    Private m_controller As New DataUserControlController
    Private dttBankName As New DataTable
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not Page.IsPostBack Then
           
            If Me.BranchMandatory = True Then
                rfvBranchName.Visible = True
            Else
                rfvBranchName.Visible = False
                Me.BankBranchId = "0"
            End If
            initLookupBankBrach()
        End If
    End Sub

    Public Sub BindBankAccount()
        Dim DtBankName As DataTable

        cboBankName.Items.Clear()
        'DtBankName = CType(Me.Cache.Item(CACHE_BANK_MASTER), DataTable)

        'If DtBankName Is Nothing Then
        'Dim dtBankNameCache As New DataTable

        'dtBankNameCache = m_controller.GetBankName(GetConnectionString)
        ' Me.Cache.Insert(CACHE_BANK_MASTER, dtBankNameCache, Nothing, DateTime.Now.AddHours(DEFAULT_TIME_DURATION_CACHE_HOUR), TimeSpan.Zero)
        'DtBankName = CType(Me.Cache.Item(CACHE_BANK_MASTER), DataTable)
        'End If

        DtBankName = m_controller.GetBankName(GetConnectionString)

        cboBankName.DataValueField = "ID"
        cboBankName.DataTextField = "Name"
        cboBankName.DataSource = DtBankName
        cboBankName.DataBind()
        cboBankName.Items.Insert(0, "Select One")
        cboBankName.Items(0).Value = "0"
        cboBankName.SelectedIndex = 0

        BindData()
    End Sub

    Sub BindData()
        Dim m_controller As New LookUpBankBranchController
        Dim oCustomClass As New Parameter.LookUpBankBranch

        oCustomClass = m_controller.GetListData(GetConnectionString, "BankBranchId = '" & hdnBankBranchId.Value & "'", 1, 10, "City ASC")

        If Not oCustomClass Is Nothing Then
            If oCustomClass.ListData.Rows.Count > 0 Then
                Me.BankCodeBank = oCustomClass.ListData.Rows(0).Item("BankCode").ToString.Substring(0, 3)
                Me.BankCodeCabang = oCustomClass.ListData.Rows(0).Item("BankCode").ToString
                Me.City = oCustomClass.ListData.Rows(0).Item("City").ToString
                Me.BankBranchName = oCustomClass.ListData.Rows(0).Item("BankBranchName").ToString
                Me.BankBranchId = oCustomClass.ListData.Rows(0).Item("BankBranchId").ToString
            End If
        End If
    End Sub

    Public Sub ShowMandatoryAll()
        Bintang1.Visible = True
        bintang3.Visible = True
        bintang4.Visible = True
    End Sub

    Public Sub ValidatorTrue()
        RequiredFieldValidator5.Enabled = True
        RequiredFieldValidator8.Enabled = True
        RequiredFieldValidator7.Enabled = True
        rfvBranchName.Enabled = False

        lblNamaBank.Visible = False
        lblNamaBankReq.Visible = True
        lblNoRekening.Visible = False
        lblNoRekeningReq.Visible = True
        lblNamaRekening.Visible = False
        lblNamaRekeningReq.Visible = True
        lblNamaCabangBank.Visible = True
        lblNamaCabangBankReq.Visible = False

    End Sub
  
    Public Sub RequiredBankName()
        RequiredFieldValidator5.Enabled = True
        RequiredFieldValidator7.Enabled = False
        RequiredFieldValidator8.Enabled = False

        lblNamaBank.Visible = False
        lblNamaBankReq.Visible = True
        lblNoRekening.Visible = True
        lblNamaRekening.Visible = True
        lblNamaRekeningReq.Visible = False


    End Sub

    Public Sub GetCookiesBankName()
        Dim cookie As HttpCookie = Request.Cookies(CommonCookiesHelper.COOKIES_BANK_NAME)
        Me.InsCoBranchID = cookie.Values("InsCoBranchID")
        Me.BankNameState = cookie.Values("BankName")
        Context.Trace.Write("UcBanckAccount.Ascx GetCookies Me.InsCoBranchID = " & Me.InsCoBranchID)
        Context.Trace.Write("UcBanckAccount.Ascx GetCookies Me.BankNameState = " & Me.BankNameState)
    End Sub

    Private Sub cboBankName_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboBankName.SelectedIndexChanged
        initLookupBankBrach()
    End Sub

    Private Sub initLookupBankBrach()
        If cboBankName.SelectedValue.Trim = "CASH" Or cboBankName.SelectedValue.Trim = "0" Then
            setBankBranchDefault()
        Else
            If Page.IsPostBack Then clearBankBranch()
        End If
    End Sub

    Public Sub setBankBranchDefault()
        Me.BankCodeBank = "-"
        Me.BankCodeCabang = "-"
        Me.BankBranchName = "-"
        Me.City = "-"
        Me.BankBranchId = "1"
    End Sub

    Public Sub clearBankBranch()
        Me.BankCodeBank = ""
        Me.BankCodeCabang = ""
        Me.BankBranchName = ""
        Me.City = ""
        Me.BankBranchId = ""
    End Sub

    Private Sub btnClearBankAccountChache_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearBankAccountChache.Click
        Try
            Me.Cache.Remove(CACHE_BANK_MASTER)
        Catch ex As Exception
            Dim lblMessage As Label

            lblMessage = CType(Me.FindControl("lblMessage"), Label)
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Public Sub LookupBankBrand()
        UcLookupBankBranch.BankID = BankID
        UcLookupBankBranch.Sort = "BankCode ASC"
        UcLookupBankBranch.CmdWhere = ""
        UcLookupBankBranch.Search()
        UcLookupBankBranch.Popup()
    End Sub

    Protected Sub btnLookupBankBrand_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnLookupBankBrand.Click
        LookupBankBrand()
    End Sub

    Public Sub CatSelectedBankBranch(ByVal BankBranchID As String,
                                           ByVal PBankCode As String,
                                           ByVal BankBranchName As String,
                                           ByVal Address As String,
                                           ByVal City As String,
                                           ByVal BankID As String)

        Dim TBankCodeBank As String = Nothing
        If ((PBankCode <> "-" Or PBankCode <> "") And PBankCode.Length > 2) Then
            TBankCodeBank = PBankCode.Substring(0, 3)
        End If

        BankBranchID = BankBranchID
        BankBranchName = BankBranchName
        BankCodeBank = TBankCodeBank
        BankCodeCabang = PBankCode
        City = City
        BankID = BankID
        txtBankBranchName.Text = BankBranchName
        txtBankCodeBank.Text = TBankCodeBank
        txtBankCodeCabang.Text = PBankCode
        txtCity.Text = City
        hdnBankBranchId.Value = BankBranchID
    End Sub

    Public Sub cboBankDisabled()
        cboBankName.Enabled = False
    End Sub

    Public Sub cboBankEnabled()
        cboBankName.Enabled = True
    End Sub
End Class