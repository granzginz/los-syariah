﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucCollectorRpt.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.ucCollectorRpt" %>
<%@ Register TagPrefix="uc1" TagName="ValidDate" Src="ValidDate.ascx" %>
<div id="Header" runat="server">
</div>
<script language="JavaScript">
    var hdnDetail;
    function WOPChange(pCmbOfPayment, pBankAccount, pHdnDetail, itemArray) {
        hdnDetail = eval('document.forms[0].' + pHdnDetail);
        hdnDetail.value = '0';
        //alert(eval('document.forms[0].' + pCmbOfPayment).options[eval('document.forms[0].' + pCmbOfPayment).selectedIndex].value);
        eval('document.forms[0].' + pBankAccount).disabled = false;


        var i, j;
        for (i = eval('document.forms[0].' + pBankAccount).options.length; i >= 0; i--) {
            eval('document.forms[0].' + pBankAccount).options[i] = null

        };
        if (itemArray == null) {
            j = 0;
        }
        else {
            j = 1;
        };
        eval('document.forms[0].' + pBankAccount).options[0] = new Option('Select One');
        eval('document.forms[0].' + pBankAccount).options[0].value = '0';
        if (itemArray != null) {
            for (i = 0; i < itemArray.length; i++) {
                eval('document.forms[0].' + pBankAccount).options[j] = new Option(itemArray[i][0]);
                if (itemArray[i][1] != null) {
                    eval('document.forms[0].' + pBankAccount).options[j].value = itemArray[i][1];
                };
                j++;
            };
            eval('document.forms[0].' + pBankAccount).selected = true;
        };


        //if( eval('document.forms[0].' + pCmbOfPayment).selectedIndex == 4){
        //eval('document.forms[0].' + pBankAccount).disabled = true;
        //}
        //if( eval('document.forms[0].' + pCmbOfPayment).selectedIndex  == 5){
        //eval('document.forms[0].' + pBankAccount).disabled = true;
        //}
        //if( eval('document.forms[0].' + pCmbOfPayment).selectedIndex  == 2){
        //eval('document.forms[0].' + pBankAccount).disabled = true;
        //}
        //if( eval('document.forms[0].' + pCmbOfPayment).selectedIndex  == 7){
        //eval('document.forms[0].' + pBankAccount).disabled = true;
        //}
    }



    function cboChildonChange(l, phdnResult) {
        hdnDetail = eval('document.forms[0].' + phdnResult);
        if (l != '0')
        { hdnDetail.value = l }
        else
        { hdnDetail.value = '0'; }

    }



    function SetcboChildFocus() {
        var SPV;
        SPV = hdnDetail.value;
        alert(SPV);

        for (i = document.forms[0].cboChild.options.length; i >= 0; i--) {
            if (document.forms[0].cboChild.options.options[i].value = SPV) {
                document.forms[0].cboChild.options[i].selected = true;
            }
        };
    }
</script>
<input id="hdnSP" type="hidden" name="hdnSP" runat="server" />
<input id="hdnResult" type="hidden" name="hdnResult" runat="server" />
<div class="form_box_usercontrol">
    <div class="form_left_usercontrol">
        <label class="label_req">
            Collection Group</label>
        <asp:DropDownList ID="cboParent" runat="server" onchange="<%#WOPonChange()%>">
        </asp:DropDownList>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" InitialValue="0"
            Display="Dynamic" ControlToValidate="cboParent" ErrorMessage="Harap isi ID Collection Group"
            CssClass="validator_general"></asp:RequiredFieldValidator>
    </div>
</div>
<div class="form_box_usercontrol">
    <div class="form_left_usercontrol">
        <label class="label_req">
            Collector</label>
        <asp:DropDownList ID="cboChild" runat="server" onchange="<%#ChildChange()%>">
        </asp:DropDownList>
    </div>
</div>
