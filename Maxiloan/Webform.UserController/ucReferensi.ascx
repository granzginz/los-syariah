﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucReferensi.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.ucReferensi" %>
<%--<%@ Register TagPrefix="uc1" TagName="ucAddress" Src="ucAddress.ascx" %>--%>
<%@ Register TagPrefix="uc1" TagName="ucAddress" Src="UcCompanyAddress.ascx" %>
<%--<%@ Register Src="UcCompanyAddress.ascx" TagName="UcCompanyAddress" TagPrefix="uc1" %>--%>
<%--<div class="form_box_usercontrol">
    <div class="form_left_usercontrol">
        <h4>
            REFERENSI</h4>
    </div>
</div>--%>
<div class="form_box_usercontrol">
    <div class="form_left_usercontrol">
        <label class="label_split">
            Nama
        </label>
        <asp:TextBox runat="server" ID="txtNamaReferensi"></asp:TextBox>
    </div>
</div>
<div class="form_box_usercontrol">
    <div class="form_left_usercontrol">
        <label class="label_split">
            Hubungan
        </label>
        <asp:TextBox runat="server" ID="txtJabatan"></asp:TextBox>
    </div>
</div>
<%--<uc1:UcCompanyAddress ID="UcCompanyAddress1" runat="server" />--%>
<uc1:ucAddress ID="UcCompanyAddress1" runat="server" />
<div class="form_box_usercontrol">
    <div class="form_left_usercontrol">
        <label class="label_split">
            No. Handphone
        </label>
        <asp:TextBox runat="server" ID="txtNoHP"></asp:TextBox>
    </div>
</div>
<div class="form_box_usercontrol">
    <div class="form_left_usercontrol">
        <label class="label_split">
            Email
        </label>
        <asp:TextBox runat="server" ID="txtEmail"></asp:TextBox>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtEmail"
            Display="Dynamic" ErrorMessage="Format alamat email salah!" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
    </div>
</div>
<div class="form_box_usercontrol">
    <div class="form_left_usercontrol">
        <label class="label_split">
            Catatan
        </label>
        <asp:TextBox runat="server" ID="txtCatatan" TextMode="MultiLine" CssClass="multiline_textbox_uc"></asp:TextBox>
    </div>
</div>
