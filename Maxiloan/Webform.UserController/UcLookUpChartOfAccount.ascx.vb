﻿Public Class UcLookUpChartOfAccount
    Inherits System.Web.UI.UserControl


    Public Property Coa() As String
        Get
            Return CType(txtCoaName.Text.Trim, String)
        End Get
        Set(ByVal Value As String)
            txtCoaName.Text = Value
        End Set
    End Property

    Public Property CoAId() As String
        Get
            Return CType(hdnCoAId.Value.Trim, String)
        End Get
        Set(ByVal Value As String)
            hdnCoAId.Value = Value
        End Set
    End Property

    Public Property Style() As String
        Get
            Return CStr(ViewState("style"))
        End Get
        Set(ByVal Value As String)
            ViewState("style") = Value
        End Set
    End Property


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        txtCoaName.Attributes.Add("readonly", "true")

    End Sub

End Class