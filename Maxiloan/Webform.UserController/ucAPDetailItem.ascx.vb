﻿Public Class ucAPDetailItem
    Inherits ControlBased
    Dim Total As Double
    Dim Total2 As Double
#Region "Properties"
    Public Property APType() As String
        Get
            Return CType(ViewState("APType"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("APType") = Value
        End Set
    End Property
    Public Property APTo() As String
        Get
            Return CType(ViewState("APTo"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("APTo") = Value
        End Set
    End Property

    Public Property DueDate() As String
        Get
            Return CType(ViewState("DueDate"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("DueDate") = Value
        End Set
    End Property

    Public Property ListData() As DataTable
        Get
            Return CType(ViewState("ListData"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            ViewState("ListData") = Value
        End Set
    End Property
    Public Property TotalAP() As Decimal
        Set(value As Decimal)
            ViewState("TotalAP") = value
        End Set
        Get
            Return CType(ViewState("TotalAP"), Decimal)
        End Get

    End Property
   
    Public Property FormID() As String
        Set(value As String)
            ViewState("FormID") = value
        End Set
        Get
            Return CType(ViewState("FormID"), String)
        End Get

    End Property

    Private Tot As Decimal = 0


#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub DtgAP_ItemCommand(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgAP.ItemCommand
    End Sub

    Public Sub BindAPDetail()
        lblAPTo.Text = Me.APTo
        lblDueDate.Text = Me.DueDate
        Dim dsDtgAP As DataTable
        Dim dvDtgAP As DataView
        dsDtgAP = Me.ListData
        dvDtgAP = dsDtgAP.DefaultView

        dtgAP.DataSource = dvDtgAP
        dtgAP.DataBind()
    End Sub

    Public Function getGridAP() As DataGrid
        Return dtgAP
    End Function

    Private Sub DtgAP_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgAP.ItemDataBound
        Dim lblAPBala, lblSumBalance, lblJumlahAdjust As Label
        Dim lblPaymAmou, lblSumAmount, lblInvoDate As Label

        Dim lnkAPDetailDesc As New HyperLink
        Dim lnkAPDeta As New Label
        Dim nSupplierID As New Label
        Dim lblInsBranchID As New Label
        Dim lblMaskAssID As New Label
        Dim nApplicationID As New Label
        Dim nReffNo As New Label
        Dim nCustID As New Label
        Dim lblAssetSeqNo As New Label
        Dim lblAssetRepSeqNo As New Label
        Dim txtPaymentAmount As ucNumberFormat

        lnkAPDetailDesc = CType(e.Item.FindControl("lnkAPDetailDesc"), HyperLink)
        lnkAPDeta = CType(e.Item.FindControl("lnkAPDeta"), Label)

        nSupplierID = CType(e.Item.FindControl("lblSupplierID"), Label)
        lblInsBranchID = CType(e.Item.FindControl("lblInsBranchID"), Label)
        lblMaskAssID = CType(e.Item.FindControl("lblMaskAssID"), Label)
        nApplicationID = CType(e.Item.FindControl("lblApplicationID"), Label)
        nReffNo = CType(e.Item.FindControl("lblReffNo"), Label)
        nCustID = CType(e.Item.FindControl("lblCustID"), Label)
        lblAssetSeqNo = CType(e.Item.FindControl("lblAssetSeqNo"), Label)
        lblAssetRepSeqNo = CType(e.Item.FindControl("lblAssetRepSeqNo"), Label)

        If e.Item.ItemIndex >= 0 Then
            'Select Case Me.APType
            '    Case "SPPL"
            '        lnkAPDetailDesc.NavigateUrl = "javascript:OpenViewApSupplier('" & "Finance" & "', '" & lnkAPDeta.Text.Trim & "','" & Server.UrlEncode(Me.sesBranchId.Replace("'", "").Trim) & "')"
            '    Case "INSR"
            '        lnkAPDetailDesc.NavigateUrl = "javascript:OpenViewApInsurance('" & "Finance" & "', '" & Server.UrlEncode(nApplicationID.Text.Trim) & "','" & Server.UrlEncode(Me.sesBranchId.Replace("'", "").Trim) & "')"
            '    Case "RADV"
            '        lnkAPDetailDesc.NavigateUrl = "javascript:OpenViewRequestNoRefund('" & Server.UrlEncode(Me.sesBranchId.Replace("'", "").Trim) & "', '" & Server.UrlEncode(nReffNo.Text.Trim) & "','" & "Finance" & "')"
            '    Case "RCST"
            '        lnkAPDetailDesc.NavigateUrl = "javascript:OpenCustomer('" & "Finance" & "', '" & Server.UrlEncode(nCustID.Text.Trim) & "')"
            '    Case "ASRP"
            '        lnkAPDetailDesc.NavigateUrl = "javascript:OpenViewAssetReplacament('" & "Finance" & "', '" & Server.UrlEncode(Me.sesBranchId.Replace("'", "").Trim) & "' , '" & Server.UrlEncode(nApplicationID.Text.Trim) & "' , '" & Server.UrlEncode(lblAssetRepSeqNo.Text.Trim) & "','" & Server.UrlEncode(lblAssetSeqNo.Text.Trim) & "')"
            'End Select

            lnkAPDetailDesc.NavigateUrl = "javascript:OpenViewApSupplier('" & "Finance" & "', '" & lnkAPDeta.Text.Trim & "','" & Server.UrlEncode(Me.sesBranchId.Replace("'", "").Trim) & "')"

            lblAPBala = CType(e.Item.FindControl("lblAPBala"), Label)
            Total += CDbl(lblAPBala.Text)
            lblAPBala.Text = FormatNumber(CDbl(lblAPBala.Text), 2)

            lblPaymAmou = CType(e.Item.FindControl("lblPaymAmou"), Label)
            Total2 += CDbl(lblPaymAmou.Text)
            lblPaymAmou.Text = FormatNumber(CDbl(lblPaymAmou.Text), 2)

            lblInvoDate = CType(e.Item.FindControl("lblInvoDate"), Label)
            lblInvoDate.Text = lblInvoDate.Text.Trim

            txtPaymentAmount = CType(e.Item.FindControl("txtPaymentAmount"), ucNumberFormat)
            If Me.FormID = "APAPPROVEHO" Then
                txtPaymentAmount.Enabled = CBool(IIf(APType = "SPPL", False, True))
                txtPaymentAmount.Text = lblPaymAmou.Text
                txtPaymentAmount.RequiredFieldValidatorEnable = True
                txtPaymentAmount.RangeValidatorMinimumValue = "1"
                txtPaymentAmount.RangeValidatorMaximumValue = lblPaymAmou.Text
            Else
                txtPaymentAmount.Visible = False
            End If

        End If

        If e.Item.ItemType = ListItemType.Header Then
            lblJumlahAdjust = CType(e.Item.FindControl("lblJumlahAdjust"), Label)
            If Me.FormID = "APAPPROVEHO" Then
                lblJumlahAdjust.Visible = True
            Else
                lblJumlahAdjust.Visible = False
            End If
        End If

        If e.Item.ItemType = ListItemType.Footer Then
            lblSumBalance = CType(e.Item.FindControl("lblSumBalance"), Label)
            lblSumBalance.Text = FormatNumber(Total.ToString, 2)

            lblSumAmount = CType(e.Item.FindControl("lblSumAmount"), Label)
            lblSumAmount.Text = FormatNumber(Total2.ToString, 2)
        End If
        TotalAP = CDec(Total.ToString)
    End Sub

End Class