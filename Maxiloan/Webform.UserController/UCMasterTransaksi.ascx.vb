﻿
#Region "Import"
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
#End Region

Public Class UCMasterTransaksi
    Inherits ControlBased

    Dim chrClientID As String

#Region "properties"
    Public Property TransID() As String
        Get
                Return CType(cboParent.SelectedItem.Value, String)
        End Get
        Set(ByVal Value As String)
            cboParent.SelectedIndex = cboParent.Items.IndexOf(cboParent.Items.FindByValue(Value))
        End Set
    End Property
#End Region

    Sub cboTransID()
        Dim cPaging As New GeneralPagingController
        Dim oPaging As New Parameter.GeneralPaging

        Dim dtParent As New DataTable
        Dim PagingList As New Parameter.GeneralPaging

        With oPaging
            .strConnection = GetConnectionString()
            .WhereCond = "IsActive = 1 AND isDistributable = 1"
            .CurrentPage = 1
            .PageSize = 100
            .SortBy = "TransDesc ASC"
            .SpName = "spGetAgreementMasterTransaction"
        End With

        oPaging = cPaging.GetGeneralPaging(oPaging)

        cboParent.DataTextField = "TransDesc"
        cboParent.DataValueField = "TransID"
        cboParent.DataSource = oPaging.ListData
        cboParent.DataBind()
        cboParent.Items.Insert(0, "Select One")
        cboParent.Items(0).Value = "0"
    End Sub
End Class
