﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UcViewAddress.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.UcViewAddress" %>
<div class="form_box_usercontrol">
    <div class="form_left_usercontrol">
        <label>
            Alamat</label>
        <asp:Label ID="lblAddress" runat="server"></asp:Label>
    </div>
</div>
<div class="form_box_usercontrol">
    <div class="form_left_usercontrol">
        <label>
            RT / RW</label>
        <asp:Label ID="lblRT" runat="server"></asp:Label>&nbsp;/
        <asp:Label ID="lblRW" runat="server"></asp:Label>
    </div>
</div>
<div class="form_box_usercontrol">
    <div class="form_left_usercontrol">
        <label>
            Kelurahan</label>
        <asp:Label ID="lblKelurahan" runat="server"></asp:Label>
    </div>
</div>
<div class="form_box_usercontrol">
    <div class="form_left_usercontrol">
        <label>
            Kecamatan</label>
        <asp:Label ID="lblKecamatan" runat="server"></asp:Label>
    </div>
</div>
<div class="form_box_usercontrol">
    <div class="form_left_usercontrol">
        <label>
            Kota</label>
        <asp:Label ID="lblCity" runat="server"></asp:Label>
    </div>
</div>
<div class="form_box_usercontrol">
    <div class="form_left_usercontrol">
        <label>
            Kode Pos</label>
        <asp:Label ID="lblZipCode" runat="server"></asp:Label>
    </div>
</div>
<div class="form_box_usercontrol">
    <div class="form_left_usercontrol">
        <label>
            Telepon-1</label>
        <asp:Label ID="lblAreaPhone1" runat="server"></asp:Label>&nbsp;-
        <asp:Label ID="lblPhone1" runat="server"></asp:Label>
    </div>
</div>
<div class="form_box_usercontrol">
    <div class="form_left_usercontrol">
        <label>
            Telepon-2</label>
        <asp:Label ID="lblAreaPhone2" runat="server"></asp:Label>&nbsp;-
        <asp:Label ID="lblPhone2" runat="server"></asp:Label>
    </div>
</div>
<div class="form_box_usercontrol">
    <div class="form_left_usercontrol">
        <label>
            Fax</label>
        <asp:Label ID="lblAreaFax" runat="server"></asp:Label>&nbsp;-
        <asp:Label ID="lblFax" runat="server"></asp:Label>
    </div>
</div>
