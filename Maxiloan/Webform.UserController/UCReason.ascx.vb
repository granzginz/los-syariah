﻿Imports Maxiloan.Controller
Public Class UCReason
    Inherits ControlBased
    Private m_controller As New DataUserControlController
    Private dttBankName As New DataTable
    Public Property ReasonTypeID() As String
        Get
            Return CType(ViewState("ReasonTypeID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ReasonTypeID") = Value
        End Set
    End Property
    Public Property ReasonID() As String
        Get
            Return CType(cboReason.SelectedItem.Value, String)
        End Get
        Set(ByVal Value As String)
            cboReason.SelectedIndex = cboReason.Items.IndexOf(cboReason.Items.FindByValue(Value))
        End Set
    End Property
    

    Public Property Description() As String
        Get
            Return CType(cboReason.SelectedItem.Text, String)
        End Get
        Set(ByVal Value As String)
            cboReason.SelectedIndex = cboReason.Items.IndexOf(cboReason.Items.FindByText(Value))
        End Set
    End Property

    Public Property AutoPostBack() As Boolean
        Get
            Return cboReason.AutoPostBack
        End Get
        Set(ByVal Value As Boolean)
            cboReason.AutoPostBack = Value
        End Set

    End Property

    Public Delegate Sub ReasonSelectedHandler(ByVal reasonValue As String)
    Public Event ReasonSelected As ReasonSelectedHandler

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub


    Public Sub BindReason()
        cboReason.Items.Clear()
        Dim DtReason As DataTable
        'disable cache karena combo reason tidak diperbaharui
        'DtReason = CType(Me.Cache.Item("Reason" & Me.ReasonTypeID.Trim), DataTable)
        'If DtReason Is Nothing Then
        'Dim dtBankNameCache As New DataTable
        'dtBankNameCache = m_controller.GetReason(GetConnectionString, Me.ReasonTypeID.Trim)
        'Me.Cache.Insert("Reason" & Me.ReasonTypeID.Trim, dtBankNameCache, Nothing, DateTime.Now.AddHours(10), TimeSpan.Zero)
        'DtReason = CType(Me.Cache.Item("Reason" & Me.ReasonTypeID.Trim), DataTable)
        'End If
        DtReason = m_controller.GetReason(GetConnectionString, Me.ReasonTypeID.Trim)
        cboReason.DataValueField = "ID"
        cboReason.DataTextField = "Name"
        cboReason.DataSource = DtReason
        cboReason.DataBind()
        cboReason.Items.Insert(0, "Select One")
        cboReason.Items(0).Value = "0"
        Me.ReasonID = cboReason.SelectedItem.Value.Trim
        Me.Description = cboReason.SelectedItem.Text.Trim
    End Sub

    Private Sub cboReason_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboReason.SelectedIndexChanged
        RaiseEvent ReasonSelected(cboReason.SelectedValue)

    End Sub
End Class