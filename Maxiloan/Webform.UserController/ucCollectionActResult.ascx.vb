﻿Public Class ucCollectionActResult
    Inherits System.Web.UI.UserControl
    Protected WithEvents oInstallmentSchedule As ucInstallmentSchedule
    Public Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub


    Public Sub DoBindInstallmentSchedule(appId As String, valueDate As Date)
        oInstallmentSchedule.ApplicationId = appId
        oInstallmentSchedule.ValueDate = valueDate
        oInstallmentSchedule.DoBind_Angsuran(String.Empty)
    End Sub

    Public Sub ClearVariable()
        hypAgreementNo.Text = ""
        hypCustomerName.Text = ""
        lblAddress.Text = ""
        lblAddress.Text = ""
        lblAsset.Text = ""
        lblLicenseNo.Text = ""
        lblCMONo.Text = ""

        lblSuamiIstriPhone1.Text = ""
        lblSuamiIstriPhone2.Text = ""

        lblOverDuedays.Text = ""
        ltlWayOfPayment.Text = ""
        lblCollector.Text = ""




        lblLegalPhone1.Text = ""
        lblLegalPhone2.Text = ""
        lblResidencePhone1.Text = ""
        lblResidencePhone2.Text = ""
        lblCompanyPhone1.Text = ""
        lblCompanyPhone2.Text = ""
        lblMobilePhone.Text = ""
        lblMailingPhone1.Text = ""
        lblMailingPhone2.Text = ""


    End Sub






    '''<summary>
    '''hypAgreementNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Public WithEvents hypAgreementNo As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''hypCustomerName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Public WithEvents hypCustomerName As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''lblAddress control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Public WithEvents lblAddress As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblAsset control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Public WithEvents lblAsset As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblLicenseNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Public WithEvents lblLicenseNo As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblCMONo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Public WithEvents lblCMONo As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''ltlCollector control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Public WithEvents lblCollector As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblLegalPhone1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Public WithEvents lblLegalPhone1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblLegalPhone2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Public WithEvents lblLegalPhone2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblResidencePhone1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Public WithEvents lblResidencePhone1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblResidencePhone2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Public WithEvents lblResidencePhone2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblCompanyPhone1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Public WithEvents lblCompanyPhone1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblCompanyPhone2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Public WithEvents lblCompanyPhone2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblMobilePhone control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Public WithEvents lblMobilePhone As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblMailingPhone1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Public WithEvents lblMailingPhone1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblMailingPhone2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Public WithEvents lblMailingPhone2 As Global.System.Web.UI.WebControls.Label
    '''<summary>
    '''lblSuamiIstriPhone1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Public WithEvents lblSuamiIstriPhone1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblSuamiIstriPhone2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Public WithEvents lblSuamiIstriPhone2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''ltlWayOfPayment control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Public WithEvents ltlWayOfPayment As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''lblOverDuedays control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Public WithEvents lblOverDuedays As Global.System.Web.UI.WebControls.Label
End Class