﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucPaymentAllocate.ascx.vb" Inherits="Maxiloan.Webform.UserController.ucPaymentAllocate" %>
<%@ Register src="ucNumberFormat.ascx" tagname="ucNumberFormat" tagprefix="uc1" %>
  <style>
        .tsl
        {
               border-top: 2px solid;
        }
        .nb { border-bottom: 0 solid #e5e5e5;
              display: flex !important;}
    </style>
     <script type="text/javascript">
         var allocate;
         var fee;
         var oth;
         $(document).ready(function () {

             allocate = parseFloat($("#oViewPaymentAllocate_txtAmountAllocate_txtNumber").value);
             $("#oViewPaymentAllocate_txtAmountAllocate_txtNumber").change(function (e) {
                 allocate = parseFloat(this.value)
                 fee = 0;
                 oth = 0;
                 $("#oViewPaymentAllocate_txtAmountInstFee_txtNumber").val("0");
                 $("#oViewPaymentAllocate_txtAmountOther_txtNumber").val("0");
             });

             $("#oViewPaymentAllocate_txtAmountInstFee_txtNumber").change(function (e) {
                 var nn = parseFloat(this.value);
                 if (nn > allocate) {
                     alert('Jumlah Alokasi Angsuran tidak valid');
                     $("#oViewPaymentAllocate_txtAmountInstFee_txtNumber").val(fee);
                     return;
                 }
                 fee = nn;
                 oth = allocate - fee;
                 $("#oViewPaymentAllocate_txtAmountOther_txtNumber").val(oth);
             });

             $("#oViewPaymentAllocate_txtAmountOther_txtNumber").change(function (e) {
                 var nn = parseFloat(this.value);
                 if (nn > allocate) {
                     alert('Jumlah Alokasi Angsuran Lain valid');
                     $("#oViewPaymentAllocate_txtAmountOther_txtNumber").val(oth);
                     return;
                 }
                 oth = nn;
                 fee = allocate - oth;
                 $("#oViewPaymentAllocate_txtAmountInstFee_txtNumber").val(fee);
             });


             $('#oViewPaymentAllocate_btnAllocate').click(function () {
              
                 if (allocate == 0 || (allocate - (oth + fee)) != 0) {
                     alert('Nilai Alokasi tidak valid.');
                     return false;
                 }
                 return true;
             });
         }); 
        </script>
     <asp:Panel ID="pnlAlokasi" runat="server">
     <div class="form_box">
            <div class="form_left th">
               <h5>ANGSURAN dan DENDA</h5> 
            </div>
            <div class="form_right th">
                <h5>KEWAJIBAN LAINNYA</h5>
            </div>
        </div>
          <div class="form_box nb">
            <div class="form_left">
                <label> Angsuran Jatuh Tempo </label>
               <asp:label ID="labInstallmentDue" runat="server" CssClass="numberAlign label" />
            </div>
            <div class="form_right">
                <label > Biaya Tagih Angsuran</label>
                <asp:label ID="labInstallColl" runat="server" CssClass="numberAlign label"/>
            </div>
        </div>

         <div class="form_box nb">
            <div class="form_left">
                <label> Denda Keterlambatan</label>
               <asp:label ID="labLCInstall" runat="server" CssClass="numberAlign label" />
            </div>
            <div class="form_right">
                <label >Premi Asuransi</label>
                <asp:label ID="labInsuranceDue" runat="server" CssClass="numberAlign label" />
            </div>
        </div>

          <div class="form_box nb">
            <div class="form_left tsl">
                <label> Total</label>
               <asp:label ID="labTotalInstall" runat="server" CssClass="numberAlign label"/>
            </div>
            <div class="form_right">
                <label >Denda Keterlambatan Asuransi</label>
                <asp:label ID="labLCInsurance" runat="server" CssClass="numberAlign label"/>
            </div>
        </div>

        
          <div class="form_box nb">
            <div class="form_left"> 
            </div>
            <div class="form_right">
                <label >Biaya Tagih Asuransi</label>
                <asp:label ID="labInsuranceColl" runat="server" CssClass="numberAlign label"/>
            </div>
        </div>

        
          <div class="form_box nb">
            <div class="form_left"> 
            </div>
            <div class="form_right">
                <label >Biaya Tolakan PDC</label>
                <asp:label ID="labPDCBounceFee" runat="server" CssClass="numberAlign label"/>
            </div>
        </div>

        <div class="form_box nb">
            <div class="form_left"> 
            </div>
            <div class="form_right">
                <label >Biaya Perpanjangan STNK</label>
                <asp:label ID="labSTNKFee" runat="server" CssClass="numberAlign label"/>
            </div>
        </div>

         <div class="form_box nb">
            <div class="form_left"> 
            </div>
            <div class="form_right">
                <label >Biaya Klaim Asuransi</label>
                <asp:label ID="labInsuranceClaim" runat="server" CssClass="numberAlign label" />
            </div>
        </div>

         <div class="form_box nb">
            <div class="form_left"> 
            </div>
            <div class="form_right">
                <label >Biaya Tarik</label>
                <asp:label ID="labRepossessionFee" runat="server" CssClass="numberAlign label"/>
            </div>
        </div>

         <div class="form_box nb">
            <div class="form_left"> 
            </div>
            <div class="form_right tsl">
                <label >Total</label>
                <asp:label ID="labTotalOSOverDue" runat="server" CssClass="numberAlign label"/>
            </div>
        </div>
         <div class="form_box nb tsl">
             <div class="form_single">
                    <label >Jumlah dibayar</label>
                    <uc1:ucNumberFormat ID="txtAmountAllocate" runat="server" />         
             </div>
         </div>

         <div class="form_box nb ">
             <div class="form_single">
                    <label >Alokasi Angsuran dan Denda</label>
                    <uc1:ucNumberFormat ID="txtAmountInstFee" runat="server" />          
             </div>
         </div>
          <div class="form_box">
             <div class="form_single">
                    <label >Alokasi Pembayaran Lainnya</label>
                    <uc1:ucNumberFormat ID="txtAmountOther" runat="server" />        
             </div>
         </div>

          <div class="form_button">
            <asp:Button ID="btnAllocate" runat="server" EnableViewState="False" Text="Alokasi" CssClass="small button blue"  />
            <asp:Button ID="btnCancel" runat="server" EnableViewState="False" Text="Cancel" CssClass="small button gray" CausesValidation="False" />  
        </div>

    </asp:Panel>