﻿Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Public Delegate Sub PayAllocateChangedHandler(ByVal sender As Object, ByVal e As PayAllocateEventArgs)
Public Class ucPaymentAllocate
    Inherits System.Web.UI.UserControl
    Public Event PaymentAllocateEvent As PayAllocateChangedHandler
    Public Property oCustomClass() As Parameter.AccMntBase
        Get
            Return CType(ViewState("OCUSTOMCLASS"), Parameter.AccMntBase)
        End Get
        Set(ByVal Value As Parameter.AccMntBase)
            ViewState("OCUSTOMCLASS") = Value
        End Set
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load 
    End Sub

    Private Sub CancelClick(ByVal sender As System.Object, ByVal e As EventArgs) Handles btnCancel.Click
        RaiseEvent PaymentAllocateEvent(New Object(), New PayAllocateEventArgs(Nothing, False))
    End Sub

    Private Sub DoAllocateClick(ByVal sender As System.Object, ByVal e As EventArgs) Handles btnAllocate.Click
        oCustomClass.ToAmountAllocate = CDbl(txtAmountAllocate.Text)
        oCustomClass.ToAmountInstFee = CDbl(txtAmountInstFee.Text)
        oCustomClass.ToAmountOther = CDbl(txtAmountOther.Text)

        RaiseEvent PaymentAllocateEvent(New Object(), New PayAllocateEventArgs(oCustomClass, True))
    End Sub

    Public Sub PaymentInfo(param As Parameter.AccMntBase)
        oCustomClass = param

        With oCustomClass
            labInstallmentDue.Text = FormatNumber(.InstallmentDue, 0)
            labLCInstall.Text = FormatNumber(.LcInstallment, 0)
            labTotalInstall.Text = FormatNumber(.GetTotalInstall(), 0)

            labInstallColl.Text = FormatNumber(.InstallmentCollFee, 0)
            labInsuranceDue.Text = FormatNumber(.InsuranceDue, 0)
            labLCInsurance.Text = FormatNumber(.LcInsurance, 0)
            labInsuranceColl.Text = FormatNumber(.InsuranceCollFee, 0)
            labPDCBounceFee.Text = FormatNumber(.PDCBounceFee, 0)
            labSTNKFee.Text = FormatNumber(.STNKRenewalFee, 0)
            labInsuranceClaim.Text = FormatNumber(.InsuranceClaimExpense, 0)
            labRepossessionFee.Text = FormatNumber(.RepossessionFee, 0)

            labTotalOSOverDue.Text = FormatNumber(.GetTotalOSOverDue(), 0)
            If .ContractStatus = "LNS" Or .ContractStatus = "SSD" Then
                txtAmountInstFee.Enabled = False
            Else
                txtAmountInstFee.Enabled = True
            End If
        End With

        

        ' txtAmountAllocate.Text = FormatNumber(oCustomClass.GetTotalOSOverDue() + oCustomClass.GetTotalInstall(), 0)
    End Sub
End Class

Public Class PayAllocateEventArgs
    Inherits EventArgs
    Private _param As Parameter.AccMntBase
    Private _ok As Boolean = False
    Public Sub New(param As Parameter.AccMntBase, ok As Boolean)
        _param = param
        _ok = ok
    End Sub
 
    Public ReadOnly Property OClass As Parameter.AccMntBase
        Get
            Return _param
        End Get
    End Property
    Public ReadOnly Property Ok As Boolean
        Get
            Return _ok
        End Get
    End Property

End Class
