﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UcViewInsuranceDetail.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.UcViewInsuranceDetail" %>
<div class="form_box_usercontrol">
    <div class="form_left_usercontrol">
        <label class="label_split">
            No Aplikasi
        </label>
        <asp:Label ID="LblApplicationID" runat="server"></asp:Label>
        <asp:Label ID="lblBranchID" runat="server" Visible="False"></asp:Label>
    </div>
</div>
<div class="form_box_usercontrol">
    <div class="form_left_usercontrol">
        <label class="label_split">
            No Kontrak
        </label>
        <asp:Label ID="LblAgreementNo" runat="server"></asp:Label>
    </div>
</div>
<div class="form_box_usercontrol">
    <div class="form_left_usercontrol">
        <label class="label_split">
            Nama Customer
        </label>
        <asp:Label ID="LblCustomerName" runat="server"></asp:Label>
    </div>
</div>
<div class="form_box_usercontrol">
    <div class="form_left_usercontrol">
        <label class="label_split">
            Nama Asset
        </label>
        <asp:Label ID="LblAssetDescription" runat="server"></asp:Label>
    </div>
</div>
