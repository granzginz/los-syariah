﻿

<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="lookupAgreement.aspx.vb"
    Inherits="Maxiloan.Webform.UserController.lookupAgreement" %>

<%@ Register TagPrefix="uc1" TagName="UcBranchAll" Src="UcBranchAll.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="UcSearchBy.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function Close_Window() {
            window.close();
        }
        function Agreement_Checked(pApplicationID, pAgreementNo, pBranchAgreement, pTotalOTR, pNTF) {
            with (document.forms['form1']) {
                hdnApplicationID.value = pApplicationID;
                hdnAgreementNo.value = pAgreementNo;
                hdnBranchAgreement.value = pBranchAgreement;
                hdnTotalOTR.value = pTotalOTR;
                hdnNTF.value = pNTF;
            }
        }

        function Select_Click() {
            with (document.forms['form1']) {
                var lObjName = '<%= Request.QueryString("ApplicationID")%>';
                if (eval('opener.document.forms["form1"].' + lObjName)) {
                    eval('opener.document.forms["form1"].' + lObjName).value = hdnApplicationID.value;
                }
                var lObjName = '<%= Request.QueryString("AgreementNo")%>';
                if (eval('opener.document.forms["form1"].' + lObjName)) {
                    eval('opener.document.forms["form1"].' + lObjName).value = hdnAgreementNo.value;
                }
                var lObjName = '<%= Request.QueryString("BranchAgreement")%>';
                if (eval('opener.document.forms["form1"].' + lObjName)) {
                    eval('opener.document.forms["form1"].' + lObjName).value = hdnBranchAgreement.value;
                }
                var lObjName = '<%= Request.QueryString("TotalOTR")%>';
                if (eval('opener.document.forms["form1"].' + lObjName)) {
                    eval('opener.document.forms["form1"].' + lObjName).value = hdnTotalOTR.value;
                }
                var lObjName = '<%= Request.QueryString("NTF")%>';
                if (eval('opener.document.forms["form1"].' + lObjName)) {
                    eval('opener.document.forms["form1"].' + lObjName).value = hdnNTF.value;
                }
            }
            window.close();
        }	
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <input id="hdnApplicationID" type="hidden" name="hdnApplicationID" runat="server" />
    <input id="hdnAgreementNo" type="hidden" name="hdnAgreementNo" runat="server" />
    <input id="hdnBranchAgreement" type="hidden" name="hdnBranchAgreement" runat="server" />
    <input id="hdnTotalOTR" type="hidden" name="hdnTotalOTR" runat="server" />
    <input id="hdnNTF" type="hidden" name="hdnTotalOTR" runat="server" />
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
    <asp:Panel ID="pnlList" runat="server">
        <asp:Panel ID="pnlDatagrid" runat="server" Visible="false">
            <div class="form_title">
                <div class="form_single">
                    <h3>
                        DAFTAR KONTRAK</h3>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ns">
                        <asp:DataGrid ID="DtgAgree" runat="server" Width="100%" CellPadding="0" BackColor="White"
                            BorderWidth="1px" BorderStyle="None" BorderColor="#CCCCCC" DataKeyField="Applicationid"
                            AutoGenerateColumns="False" OnSortCommand="SortGrid" AllowSorting="True" Visible="False">
                            <ItemStyle Height="30px" CssClass="item_grid"></ItemStyle>
                            <HeaderStyle Height="22px" CssClass="th"></HeaderStyle>
                            <Columns>
                                <asp:TemplateColumn HeaderText="PILIH">
                                    <HeaderStyle HorizontalAlign="Center" Width="4%"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center" Width="4%"></ItemStyle>
                                    <ItemTemplate>
                                        <%# Get_Radio(Container.DataItem("ApplicationID"), Container.DataItem("AgreementNo"), FormatNumber(Container.DataItem("TotalOTR"), 0), FormatNumber(Container.DataItem("NTF"), 0))%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="ApplicationId" HeaderText="NO APLIKASI">
                                    <HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Left" Width="10%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:HyperLink ID="hyApplicationId" runat="server" Text='<%#Container.DataItem("ApplicationId")%>'>
                                        </asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">
                                    <HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Left" Width="10%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:HyperLink ID="hyAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>'>
                                        </asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn Visible="False" HeaderText="CUSTOMER ID">
                                    <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblCustomerId" runat="server" Text='<%#Container.DataItem("CustomerID")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="Name" HeaderText="NAMA KONSUMEN">
                                    <HeaderStyle HorizontalAlign="Center" Width="22%"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Left" Width="22%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:HyperLink ID="hyCustomerName" runat="server" Text='<%#Container.DataItem("Name")%>'>
                                        </asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn Visible="False" SortExpression="CustomerType" HeaderText="CUSTOMER TYPE">
                                    <HeaderStyle HorizontalAlign="Center" Width="25%"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Left" Width="25%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblCustomerType" runat="server" Text='<%#Container.DataItem("CustomerType")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="Address" HeaderText="ALAMAT">
                                    <HeaderStyle HorizontalAlign="Center" Width="55%"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Left" Width="55%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblAddress" runat="server" Text='<%#Container.DataItem("Address")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn visible = "false" SortExpression="ApplicationStep"   HeaderText="STEP">
                                    <HeaderStyle HorizontalAlign="Center" Width="5%"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center" Width="5%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblApplicationStep" runat="server" Text='<%#Container.DataItem("ApplicationStep")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                
                                <asp:TemplateColumn SortExpression="InstallmentAmount" HeaderText="ANGSURAN">
                                    <HeaderStyle CssClass="th_right" Width="10%"></HeaderStyle>
                                    <ItemStyle CssClass="item_grid_right" Width="10%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblInstallment" runat="server" Text='<%#formatnumber(Container.DataItem("InstallmentAmount"),0)%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="ContractStatus" HeaderText="STS">
                                    <HeaderStyle HorizontalAlign="Center" Width="3%"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center" Width="3%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblContractStatus" runat="server" Text='<%#Container.DataItem("ContractStatus")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="TotalOTR" HeaderText="OTR">
                                    <HeaderStyle CssClass="th_right" Width="10%"></HeaderStyle>
                                    <ItemStyle CssClass="item_grid_right" Width="10%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblTotalOTR" runat="server" Text='<%#FormatNumber(Container.DataItem("TotalOTR"), 0)%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="NTF" HeaderText="NTF">
                                    <HeaderStyle CssClass="th_right" Width="10%"></HeaderStyle>
                                    <ItemStyle CssClass="item_grid_right" Width="10%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblNTF" runat="server" Text='<%#FormatNumber(Container.DataItem("NTF"), 0)%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                    </div>
                    <div>
                    </div>
                    <div class="button_gridnavigation">
                        <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../Images/grid_navbutton01.png"
                            CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../Images/grid_navbutton02.png"
                            CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../Images/grid_navbutton03.png"
                            CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../Images/grid_navbutton04.png"
                            CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False" />
                        Page
                        <asp:TextBox ID="txtPage" runat="server" CssClass="small_text">1</asp:TextBox>
                        <asp:Button ID="imbGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                            EnableViewState="False" />
                        <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" MinimumValue="1"
                            ErrorMessage="Page No. is not valid" MaximumValue="999999999" Type="Integer"
                            CssClass="validator_general"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                            ErrorMessage="Page No. is not valid" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                    <div class="label_gridnavigation">
                        <asp:Label ID="lblPage" runat="server"></asp:Label>of
                        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                        <asp:Label ID="lblRecord" runat="server"></asp:Label>record(s)
                    </div>
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="ImbSelect" runat="server" Text="Select" CssClass="small button blue"
                    CausesValidation="False" />
                <asp:Button ID="imbExit" runat="server" Text="Exit" CssClass="small button gray"
                    CausesValidation="False" OnClientClick="javascript:window.close();"></asp:Button>
            </div>
        </asp:Panel>
        <div class="form_box_title">
            <div class="form_single">
                <h4>
                    Agreement Lookup
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Branch
                </label>
                <uc1:UcBranchAll ID="oBranch" runat="server"></uc1:UcBranchAll>
                <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <uc1:UcSearchBy ID="oSearchBy" runat="server"></uc1:UcSearchBy>
        </div>
        <div>
        </div>
        <div class="form_button">
            <asp:Button ID="imgsearch" runat="server" CssClass="small button blue" Text="Search" />
            <asp:Button ID="imbReset" runat="server" CssClass="small button gray" Text="Reset" />
        </div>
        <input id="hdnTransaction" type="hidden" name="hdnTransaction" runat="server" />
        <input id="hdnDescription" type="hidden" name="hdnDescription" runat="server" />
    </asp:Panel>
    </form>
</body>
</html>
