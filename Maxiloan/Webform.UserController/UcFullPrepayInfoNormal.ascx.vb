﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
#End Region
Public Class UcFullPrepayInfoNormal
    Inherits AccMntControlBased
    Public Property BranchID() As String
        Get
            Return CType(Viewstate("BranchID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("BranchID") = Value
        End Set
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub
    Public Sub PaymentInfoNormal()
        Dim oController As New UCPaymentInfoController
        Dim oCustomClass As New Parameter.FullPrepay
        With oCustomClass
            .strConnection = GetConnectionString
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationID
            .ValueDate = Me.ValueDate
        End With

        oCustomClass = oController.GetFullPrepaymentInfoNormal(oCustomClass)
        With oCustomClass
            lblAngsuran.Text = FormatNumber(.Angsuran, 2)
            lblDenda.Text = FormatNumber(.Denda, 2)
            lblBiayaTarik.Text = FormatNumber(.BiayaTarik, 2)
            lblSTNK.Text = FormatNumber(.BiayaSTNK, 2)
            lblBiayaTolakanPDC.Text = FormatNumber(.BiayaTolakanPDC, 2)
            lblBiayaTagihAngsuran.Text = FormatNumber(.BiayaTagihAngsuran, 2)

            lblTotalPelunasanNormal.Text = FormatNumber(CDbl(.Angsuran) + CDbl(.Denda) + CDbl(.BiayaTarik) + CDbl(.BiayaTolakanPDC) + CDbl(.BiayaTagihAngsuran) + CDbl(.BiayaSTNK), 2)
        End With



    End Sub
End Class