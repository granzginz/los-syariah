﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucPDCAllocationList.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.ucPDCAllocationList" %>
<script language="javascript" type="text/javascript">
    var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
    var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
    var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';			
</script>
<script src="../../Maxiloan.js" type="text/javascript"></script>
<div class="form_box_title">
    <div class="form_single">
        <h4>
            PDC ALLOCATION</h4>
    </div>
</div>
<div class="form_box">
    <div>
        <div class="form_left">
            <label>
                Agreement Branch
            </label>
            <asp:Label ID="lblAgreeBranch" runat="server" EnableViewState="False"></asp:Label>
        </div>
        <div class="form_right">
            <label>
            </label>
        </div>
    </div>
</div>
<div class="form_box">
    <div>
        <div class="form_left">
            <label>
                Agreement No
            </label>
            <asp:HyperLink ID="HyAgreementNo" runat="server" EnableViewState="False"></asp:HyperLink>
        </div>
        <div class="form_right">
            <label>
                Customer Name
            </label>
            <asp:HyperLink ID="HyCustomerName" runat="server" EnableViewState="False"></asp:HyperLink>
        </div>
    </div>
</div>
<div class="form_box_header">
    <div class="form_single">
        <div class="grid_wrapper_ns">
            <asp:DataGrid ID="DtgPA" runat="server" EnableViewState="False" AutoGenerateColumns="False"
                AllowSorting="True" ShowFooter="True" Width="100%" CssClass="grid_general">
                <ItemStyle CssClass="item_grid"></ItemStyle>
                <HeaderStyle CssClass="th"></HeaderStyle>
                <FooterStyle CssClass="item_grid"></FooterStyle>
                <Columns>
                    <asp:TemplateColumn HeaderText="TRANSACTION" Visible="false">
                        <ItemStyle Width="37%"></ItemStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblPA" runat="server" EnableViewState="False" Text='<%#Container.DataItem("PaymentAllocationID")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="TRANSACTION">
                        <ItemStyle Width="30%"></ItemStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblPA2" runat="server" EnableViewState="False" Text='<%#Container.DataItem("PaymentAllocationDesc")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="DESCRIPTION">
                        <ItemStyle Width="30%"></ItemStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblDescription" runat="server" EnableViewState="False" Text='<%#Container.DataItem("Description")%>'>
                            </asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="Label2" runat="server" EnableViewState="False" Text="Total"></asp:Label>
                        </FooterTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="AMOUNT ALLOCATION">
                        <HeaderStyle CssClass="th_right" />
                        <ItemStyle CssClass ="item_grid_right" Width ="20%"></ItemStyle>
                        <FooterStyle CssClass ="item_grid_right" />
                        <ItemTemplate>
                            <asp:Label ID="lblAmount" runat="server" EnableViewState="False" Text='<%#formatnumber(Container.DataItem("Amount"),2)%>'>
                            </asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="lblTotPDCAmt" runat="server" EnableViewState="False"></asp:Label>
                        </FooterTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="AMOUNT CLEAR">
                        <HeaderStyle CssClass="th_right" />
                        <ItemStyle CssClass ="item_grid_right" Width="19%"></ItemStyle>
                        <FooterStyle CssClass ="item_grid_right" />
                        <ItemTemplate>
                            <asp:Label ID="lblAmountClear" runat="server" EnableViewState="False" Text='<%#formatnumber(Container.DataItem("AmountClear"),2)%>'>
                            </asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="lblTotPDCAmtClear" runat="server" EnableViewState="False"></asp:Label>
                        </FooterTemplate>
                    </asp:TemplateColumn>
                </Columns>
            </asp:DataGrid>
        </div>
    </div>
</div>
