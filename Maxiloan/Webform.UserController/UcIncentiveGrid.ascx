﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UcIncentiveGrid.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.UcIncentiveGrid" %>

<%@ Register Src="ucNumberFormat.ascx" TagName="ucNumberFormat" TagPrefix="uc1" %>
<asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
        onclick="hideMessage();"></asp:Label>
<script type="text/javascript">

    var handleddlJabatan_Change = function (e, penerimaID) {
        $('#' + penerimaID).empty();
        $('#' + penerimaID).append($("<option></option>").val('0').html('Select One'));

        var paramArg = JSON.stringify({ id: $(e).val(), 
                                        supplierID: $('#<%= txtSupplier.ClientID %>').val(),
                                        ApplicationID: $('#<%= txtApplicationID.ClientID %>').val() });
        $.ajax({
            url: "IncentiveData.aspx/GetEmployee",
            data: paramArg,
            type: "POST",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            async:false,
            success: function (dt) {
                $.each(dt.d, function (key, value) {
                    $('#' + penerimaID).append($("<option></option>").val(value.SupplierEmployeeID).html(value.SupplierEmployeeName));
                });
            },
            error: function (xhr, status, error) {
                var err = eval("(" + xhr.responseText + ")");
                alert('Proses Error : ' + err.Message);
                
            }
        });
    }
    function total(tableID) { 
        var spliIDTable = tableID.split("_");
        var tbl = spliIDTable[0] + "_" + spliIDTable[1];

        
        
        var grd = document.getElementById(tbl);
        var row = grd.rows.length - 1;

        var totalP = 0;
        var totalI = 0;
        var totalPID;
        var totalIID;
        $('#' + tbl).find('tr').each(function (i, el) {
            if (i > 0) {
                var $tds = $(this).find('td');
                var persentase_ = $tds[4].getElementsByTagName('input')[0];
                var persentase = persentase_.value;
                var insentif_ = $tds[5].getElementsByTagName('input')[0];
                var insentif = insentif_.value;
                
                if (persentase == "") {
                    persentase = "0";
                }
                if (insentif == "") {
                    insentif = "0";
                }
                if (i < row) {
                    totalP = parseFloat(totalP) + parseFloat(persentase.replace(/\s*,\s*/g, ''));
                    totalI = parseFloat(totalI) + parseFloat(insentif.replace(/\s*,\s*/g, ''));
                }
                if (i == row) {
                    totalPID = persentase_.id;
                    totalIID = insentif_.id;
                }
            }
        });
        $('#' + totalPID).val(number_format(totalP,2));
        $('#' + totalIID).val(number_format(totalI));

        if (spliIDTable[0] == "UcPremiAsuransi") {
            var lblAlokasiPremiAsuransi = $('#lblAlokasiPremiAsuransi').html();
            var Nil_internal = parseFloat(lblAlokasiPremiAsuransi.replace(/\s*,\s*/g, '')) - parseFloat(totalI)
            $('#lblAlokasiPremiAsuransiInsentif').html(number_format(totalI));
            $('#lblAlokasiPremiAsuransiInternal').html(number_format(Nil_internal));
        }

        if (totalP > 100) {
            $('#' + totalPID).attr('style', 'color:red');
        } else {
            $('#' + totalPID).removeAttr('style');
        }
        var lblalokasi = getlblAlokasi(tbl);
        if (totalI > lblalokasi) {
            $('#' + totalIID).attr('style', 'color:red');
        } else {
            $('#' + totalIID).removeAttr('style');
        }
    }
    function getlblAlokasi(grd) {
        var tbl = {};
        tbl["UcRefundPremi_dtgIncentive"] = "lblRefundPremi";
        tbl["UcRefundPremiCP_dtgIncentive"] = "lblRefundPremiCP";
        tbl["UcRefundPremiJK_dtgIncentive"] = "lblRefundPremiJK";
        tbl["UcRefundBunga_dtgIncentive"] = "lblRefundBunga";
        tbl["UcRefundUppingBunga_dtgIncentive"] = "lblRefundUppingBunga";
        tbl["UcRefundBiayaProvisi_dtgIncentive"] = "lblRefundBiayaProvisi";
        //tbl["UcRefundBiayaAdmin_dtgIncentive"] = "lblRefundBiayaAdmin";
        tbl["UcRefundBiayaLain_dtgIncentive"] = "lblRefundBiayaLain";
        tbl["UcRefundUppingBunga_dtgIncentive"] = "lblRefundUppingBunga";

        tbl["UcRefundPremiCP_dtgIncentive"] = "lblRefundPremiCP";
        tbl["UcRefundPremiJK_dtgIncentive"] = "lblRefundPremiJK";
        
        var lbl = $('#' + tbl[grd]).html();
        return parseInt(lbl.replace(/\s*,\s*/g, ''));

    }
    function getPPH(id, PphID, TarifPajakID) {
        if (id == "CO") {
            $('#' + PphID).html('23');
        } else {
            $('#' + PphID).html('21');
        }

        $('#' + TarifPajakID).html('3');

    }
    function GenerateTotal() {
        $('#dtgSum').find("tr:gt(0)").remove();

        var dtTable = ["UcRefundPremi_dtgIncentive", "UcRefundPremiCP_dtgIncentive", "UcRefundPremiJK_dtgIncentive", "UcRefundBunga_dtgIncentive", "UcRefundUppingBunga_dtgIncentive", "UcRefundBiayaProvisi_dtgIncentive", "UcRefundBiayaLain_dtgIncentive"];

        for (i = 0; i <= dtTable.length - 1; i++) {
            if (getlblAlokasi(dtTable[i]) != 0) {
                var grd = document.getElementById(dtTable[i]);
                if (grd != null) {
                    var row = grd.rows.length - 1;

                    $('#' + dtTable[i]).find('tr').each(function (i, el) {
                        if (i > 0 && i < row) {
                            var $tds = $(this).find('td');

                            var jabatan_ = $tds[1].getElementsByTagName('select')[0];
                            var jabatan = jabatan_.options[jabatan_.selectedIndex].innerHTML

                            var employee_ = $tds[2].getElementsByTagName('select')[0];
                            var employee = employee_.options[employee_.selectedIndex].innerHTML;

                            var insentif_ = $tds[5].getElementsByTagName('input')[0];
                            var insentif = insentif_.value;
                              
                            
                            if (jabatan != "Select One" && employee != "Select One" && (insentif != "0" && insentif != "")) {
                                CreateGridDTSUM(jabatan, employee, insentif);   
                            }
                        }
                    });

                }
            }
        }
    }
    function CreateGridDTSUM(jabatan, employee, insentif) {
        var grd = document.getElementById("dtgSum");
        var row = grd.rows.length - 1;
        var html = "";

        if (row == 0) {
            html += "<tr class=\"item_grid\">";
            html += "	 <td>" + jabatan + "</td><td>" + employee + "</td><td class=\"th_right\">" + insentif + "</td>";
            html += "</tr>";
            $('#dtgSum tbody').append(html);
        
        }else {
            var exist = false;
            $('#dtgSum').find('tr').each(function (i, el) {
                if (i > 0) {
                    var $tds = $(this).find('td');
                    var jabatan_ = $tds[0].innerHTML;
                    var employee_ = $tds[1].innerHTML;
                    var insentif_ = $tds[2].innerHTML;

                    //alert(insentif_);
                    
                    if ((jabatan == jabatan_) && (employee == employee_)) {
                        var totalInsentif = parseFloat(insentif.replace(/\s*,\s*/g, '')) + parseFloat(insentif_.replace(/\s*,\s*/g, ''));
                        $tds[2].innerHTML = number_format(totalInsentif);
                        exist = true;
                    }
                }
            });
            if (exist == false) {
                html += "<tr class=\"item_grid\">";
                html += "	 <td>" + jabatan + "</td><td>" + employee + "</td><td class=\"th_right\">" + insentif + "</td>";
                html += "</tr>";
                $('#dtgSum tbody').append(html);
            }
        }

		
    }
    var handletxtProsentase_Change = function (x, TypeClientID, NilID) {
        var nilai = 0;
        var type = $('#' + TypeClientID).val();
         
        if (type == "RB1" || type == "RB2" || type == "RB3" || type == "RB4" || type == "RB5" || type == "RB6" || type == "RB7") {
//            var ntf = $('#UcRefundBunga_txtNTF').val();
//            var Tenor = $('#UcRefundBunga_txtTenor').val();
//            var FlatRate = $('#UcRefundBunga_txtFlatRate').val();            
//            // npv = (ntf * TenorYear * (refundRate / 100)) / (1 + (flatRate / 100))
//            nilai = (parseInt(ntf) * parseInt(Tenor) * (parseFloat(x) / 100)) / (1 + ((parseFloat(FlatRate) / 100)));
//            nilai = Math.round(parseFloat(nilai), 0) / 1000;
            //            nilai = Math.ceil(parseFloat(nilai)) * 1000;
            var bunga = $('#UcRefundPremi_txtTotalBunga').val();
            nilai = parseFloat(bunga) * (parseFloat(x) / 100);
        } else if (type == "UP1" || type == "UP2" || type == "UP3" || type == "UP4" || type == "UP5" || type == "UP6" || type == "UP7") {
            var Premi = $('#UcRefundUppingBunga_txtTotalBunga').val();
            nilai = parseFloat(Premi) * (parseFloat(x) / 100);
        } else if (type == "TP1" || type == "TP2" || type == "TP3" || type == "TP4" || type == "TP5" || type == "TP6" || type == "TP7") {
            var Premi = $('#UcRefundPremi_txtAsuransiCom').val();
            nilai = parseFloat(Premi) * (parseFloat(x) / 100);
        } else if (type == "CP1" || type == "CP2" || type == "CP3" || type == "CP4" || type == "CP5" || type == "CP6" || type == "CP7") {
            var Premi = $('#UcRefundPremiCP_txtAsuransiCom').val();
            nilai = parseFloat(Premi) * (parseFloat(x) / 100);
        } else if (type == "JK1" || type == "JK2" || type == "JK3" || type == "JK4" || type == "JK5" || type == "JK6" || type == "JK7") {
            var Premi = $('#UcRefundPremiJK_txtAsuransiCom').val();
            nilai = parseFloat(Premi) * (parseFloat(x) / 100);
        } else if (type == "TD1" || type == "TD2" || type == "TD3" || type == "TD4" || type == "TD5" || type == "TD6" || type == "TD7") {
            //var Admin = $('#UcRefundBiayaAdmin_txtAdmin').val();
            //nilai = parseFloat(Admin) * (parseFloat(x) / 100);
            var Other = $('#UcRefundBiayaLain_txtOther').val();
            nilai = parseFloat(Other) * (parseFloat(x) / 100);
        } else if (type == "PR1" || type == "PR2" || type == "PR3" || type == "PR4" || type == "PR5" || type == "PR6" || type == "PR7") {
            var Provisi = $('#UcRefundBiayaProvisi_txtProvisi').val();
            nilai = parseFloat(Provisi) * (parseFloat(x) / 100);
        }

        $('#' + NilID).val(number_format(nilai));

        var lblInsentifGrossID = NilID.replace("txtInsentif", "lblInsentifGross");
     
        $('#' + lblInsentifGrossID).html($('#' + NilID).val());
       
   
        var TarifPajakID = NilID.replace("txtInsentif", "lblTarifPajak");
        var lblNilaiPajakID = NilID.replace("txtInsentif", "lblNilaiPajak");
        var lblInsentifNetID = NilID.replace("txtInsentif", "lblInsentifNet"); 
          
        if (!$('#' + TarifPajakID).html() == "") {
            var lblInsentifGross = $('#' + lblInsentifGrossID).html();  
            var lblTarifPajak = $('#' + TarifPajakID).html();
            $('#' + lblNilaiPajakID).html(number_format(parseInt(lblInsentifGross.replace(/\s*,\s*/g, '')) * (parseFloat(lblTarifPajak.replace(/\s*,\s*/g, '')) / 100))); 
            var lblNilaiPajak = $('#' + lblNilaiPajakID).html();
            $('#' + lblInsentifNetID).html(number_format(parseInt(lblInsentifGross.replace(/\s*,\s*/g, '')) - parseInt(lblNilaiPajak.replace(/\s*,\s*/g, ''))));  
        }
        
        GenerateTotal();
        total(NilID);
    }
	//modify ario
    var handletxtInsentif_Change = function (x, TypeClientID, PersenID) {
        var nilai = 0;
        var type = $('#' + TypeClientID).val();
         
        if (type == "RB1" || type == "RB2" || type == "RB3" || type == "RB4" || type == "RB5" || type == "RB6" || type == "RB7") {
//            var ntf = $('#UcRefundBunga_txtNTF').val();
//            var Tenor = $('#UcRefundBunga_txtTenor').val();
//            var FlatRate = $('#UcRefundBunga_txtFlatRate').val();

//            var n = (1 + (FlatRate / 100))
//            var result = ((parseInt(x.replace(/\s*,\s*/g, '')) * n) / Tenor / parseInt(ntf.replace(/\s*,\s*/g, '')));

//            nilai = parseFloat(Math.floor(result * 100) / 100);
            var bunga = $('#UcRefundPremi_txtTotalBunga').val();
            nilai = parseFloat(x) / parseFloat(bunga) * 100;
        } else if (type == "UP1" || type == "UP2" || type == "UP3" || type == "UP4" || type == "UP5" || type == "UP6" || type == "UP7") {
            var bunga = $('#UcRefundUppingBunga_txtTotalBunga').val();
            nilai = parseFloat(x) / parseFloat(bunga) * 100;
        } else if (type == "TP1" || type == "TP2" || type == "TP3" || type == "TP4" || type == "TP5" || type == "TP6" || type == "TP7") {
            var Premi = $('#UcRefundPremi_txtAsuransiCom').val();
            nilai = parseFloat(x) / parseFloat(Premi) * 100;
        } else if (type == "CP1" || type == "CP2" || type == "CP3" || type == "CP4" || type == "CP5" || type == "CP6" || type == "CP7") {
            var Premi = $('#UcRefundPremiCP_txtAsuransiCom').val();
            nilai = parseFloat(x) / parseFloat(Premi) * 100;
        } else if (type == "JK1" || type == "JK2" || type == "JK3" || type == "JK4" || type == "JK5" || type == "JK6" || type == "JK7") {
            var Premi = $('#UcRefundPremiJK_txtAsuransiCom').val();
            nilai = parseFloat(x) / parseFloat(Premi) * 100;
        } else if (type == "TD1" || type == "TD2" || type == "TD3" || type == "TD4" || type == "TD5" || type == "TD6" || type == "TD7") {
            //var Admin = $('#UcRefundBiayaAdmin_txtAdmin').val();
            //nilai = parseFloat(x) / parseFloat(Admin) *  100;
            var Other = $('#UcRefundBiayaLain_txtOther').val();
            nilai = parseFloat(x) / parseFloat(Other) * 100;
        } else if (type == "PR1" || type == "PR2" || type == "PR3" || type == "PR4" || type == "PR5" || type == "PR6" || type == "PR7") {
            var Provisi = $('#UcRefundBiayaProvisi_txtProvisi').val();
            nilai = parseFloat(x) / parseFloat(Provisi) * 100;
        }

        $('#' + PersenID).val(number_format(nilai, 2));

        var lblInsentifGrossID = PersenID.replace("txtProsentase", "lblInsentifGross"); 
        $('#' + lblInsentifGrossID).html(number_format(x));

        var TarifPajakID = PersenID.replace("txtProsentase", "lblTarifPajak");
        var lblNilaiPajakID = PersenID.replace("txtProsentase", "lblNilaiPajak");
        var lblInsentifNetID = PersenID.replace("txtProsentase", "lblInsentifNet");

         


        if (!$('#' + TarifPajakID).html() == "") {
            var lblInsentifGross = $('#' + lblInsentifGrossID).html(); 
            var lblTarifPajak = $('#' + TarifPajakID).html();
            $('#' + lblNilaiPajakID).html(number_format(parseInt(lblInsentifGross.replace(/\s*,\s*/g, '')) * (parseFloat(lblTarifPajak.replace(/\s*,\s*/g, '')) / 100))); 
            var lblNilaiPajak = $('#' + lblNilaiPajakID).html();
            $('#' + lblInsentifNetID).html(number_format(parseInt(lblInsentifGross.replace(/\s*,\s*/g, '')) - parseInt(lblNilaiPajak.replace(/\s*,\s*/g, '')))); 
             
        }
        GenerateTotal();
        total(PersenID); 
    }
    var handleddlpenerima_Change = function (x, y, TarifPajakID) {
        
        var lblInsentifGrossID = TarifPajakID.replace("lblTarifPajak", "lblInsentifGross"); 
        var lblNilaiPajakID = TarifPajakID.replace("lblTarifPajak", "lblNilaiPajak");
        var lblInsentifNetID = TarifPajakID.replace("lblTarifPajak", "lblInsentifNet");
         

        if (!$('#' + lblInsentifGrossID).html() == "") {
            var lblInsentifGross = $('#' + lblInsentifGrossID).html(); 
            var lblTarifPajak = $('#' + TarifPajakID).html();
            $('#' + lblNilaiPajakID).html(number_format(parseInt(lblInsentifGross.replace(/\s*,\s*/g, '')) * parseFloat(lblTarifPajak.replace(/\s*,\s*/g, '')) / 100)); 
            var lblNilaiPajak = $('#' + lblNilaiPajakID).html();
            $('#' + lblInsentifNetID).html(number_format(parseInt(lblInsentifGross.replace(/\s*,\s*/g, '')) - parseInt(lblNilaiPajak.replace(/\s*,\s*/g, '')))); 
             
        }
        GenerateTotal();
        total(TarifPajakID);
    }
    
    function number_format(number, decimals, dec_point, thousands_sep) {

        number = (number + '')
            .replace(/[^0-9+\-Ee.]/g, '');
        var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function (n, prec) {
                var k = Math.pow(10, prec);
                return '' + (Math.round(n * k) / k)
                .toFixed(prec);
            };
        // Fix for IE parseFloat(0.55).toFixed(0) = 0;
        s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
            .split('.');
        if (s[0].length > 3) {
            s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
        }
        if ((s[1] || '')
            .length < prec) {
            s[1] = s[1] || '';
            s[1] += new Array(prec - s[1].length + 1)
              .join('0');
        }
        return s.join(dec);
    }
    function deleteRow(ClientID,row) {
        var i = row.parentNode.parentNode.rowIndex;
        document.getElementById(ClientID).deleteRow(i);
        GenerateTotal();
        total(ClientID);
    }
    function Apply(tbl) {
        var a = tbl.split("_");
        var uc = a[0];
        $('#' + uc + '_pesan').html("Mohon tunggu, Sedang dalam proses!");

        var stat = true;
        var grd = document.getElementById(tbl);
        var row = grd.rows.length - 1;
        TrnID = getTransID(tbl); // -- ga kepake

        var _ApplicationID = $('#<%= txtApplicationID.ClientID %>').val();
        var _BranchID = $('#<%= txtBranchID.ClientID %>').val();
        var _BussinessDate = $('#<%= txtBussinessDate.ClientID %>').val();
        var _SupplierID = $('#<%= txtSupplier.ClientID %>').val();

        $('#' + tbl).find('tr').each(function (i, el) {
            if (i > 0 && i < row) {
                var $tds = $(this).find('td');

                var jabatan = $tds[1].getElementsByTagName('select')[0];
                var employee = $tds[2].getElementsByTagName('select')[0];
                var TransID = $tds[3].getElementsByTagName('select')[0];

                var persen = $tds[4].getElementsByTagName('input')[0];
                var insentif = $tds[5].getElementsByTagName('input')[0]; 
                var pph = $tds[6].getElementsByTagName('select')[0];
                //var tarifpajak = $tds[7].getElementsByTagName('span')[0];
                //var nilaipajak = $tds[8].getElementsByTagName('span')[0];
                //var pph = 0;
                var tarifpajak = 0;
                var nilaipajak = 0;
                var insentifnet = $tds[7].getElementsByTagName('span')[0]; 
                
                var IsDiscountPPH = $tds[8].getElementsByTagName('select')[0];
                 
                var idJabatan = jabatan.id;
                var idemployee = employee.id;
                var idpersen = persen.id;
                var idinsentif = insentif.id;
                var idpph = pph.id;
                var idIsDiscountPPH = IsDiscountPPH.id;
                 

                $('#' + idJabatan).attr('style', 'background-color:white');
                $('#' + idemployee).attr('style', 'background-color:white;width:150px');
                $('#' + idpersen).attr('style', 'background-color:white');
                $('#' + idinsentif).attr('style', 'background-color:white');
                $('#' + idpph).attr('style', 'background-color:white');
                $('#' + idIsDiscountPPH).attr('style', 'background-color:white');


                if (insentif.value != "" || insentif.value != "0" || insentif.value != "0.00" || insentif.value > 0) {
                    if ((jabatan.value == "" || jabatan.value == 0 || employee.value == "" || employee.value == 0)) {
                        stat = false;
                        $('#' + idJabatan).attr('style', 'background-color:red');
                    }
                    if (stat == true) {
                        var paramArg = JSON.stringify({
                            jabatan: jabatan.value,
                            employee: employee.value,
                            persen: persen.value,
                            insentif: insentif.value,
                            //pph: '0',
                            pph: pph.value,
                            tarifpajak: '0',
                            nilaipajak: '0',
                            insentifnet: insentifnet.innerHTML,
                            TransID: TransID.value,
                            seq: i,
                            ApplicationID: _ApplicationID,
                            BranchID: _BranchID,
                            BussinessDate: _BussinessDate,
                            SupplierID: _SupplierID,
                            IsDiscountPPH: IsDiscountPPH.value
                        });
                        $.ajax({
                            url: "IncentiveData.aspx/Apply",
                            data: paramArg,
                            type: "POST",
                            dataType: "json",
                            contentType: "application/json; charset=utf-8",
                            async: false,
                            success: function (dt) {

                            }
                        });
                    }
                }


            } else if (i == row) {
                var lblTotal = getlblAlokasi(tbl);
                var txtTotal;
                var $tds = $(this).find('td');
                txtTotal = $tds[5].getElementsByTagName('input')[0];
                var valTotal = txtTotal.value;
                var idTotal = txtTotal.id;

                if (lblTotal != valTotal.replace(/\s*,\s*/g, '')) {
                    $('#' + idTotal).attr('style', 'color:red');
                    stat = false;
                } else {
                    $('#' + idTotal).removeAttr('style');
                }

            }
        });
        if (stat == true) {
            var paramArg2 = JSON.stringify({ GrouptransID: TrnID, 
                                                ApplicationID: _ApplicationID,
                                                BranchID: _BranchID,
                                                BussinessDate: _BussinessDate,
                                                SupplierID: _SupplierID
                                           });
            $.ajax({
                url: "IncentiveData.aspx/SaveApply",
                data: paramArg2,
                type: "POST",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                async: false,
                success: function (response) {
//                    if (response.d == "D") {
//                        $('#ucApplicationTab1_hypHasilSurvey').attr('href', 'HasilSurvey.aspx?CustomerID=<%= SupplierID %>&ApplicationID=<%= ApplicationID %>');
//                    } else {
//                        $('#ucApplicationTab1_hypHasilSurvey').removeAttr('href');
//                    }
                    $('#' + uc + '_pesan').html('Proses Berhasil, Terima kasih!');
                    $('#' + uc + '_pesan').attr('style', 'padding-top:10px;font-size:10px;width:80%');
                },
                error: function (xhr, status, error) {
                    var err = eval("(" + xhr.responseText + ")");
                    $('#' + uc + '_pesan').html('Proses Error : ' + err.Message);
                    $('#' + uc + '_pesan').attr('style', 'padding-top:10px;font-size:10px;width:80%;color:red');
                }
            });
            
        } else {
            $('#' + uc + '_pesan').html('Proses Gagal, Periksa inputannya kembali!');
            $('#' + uc + '_pesan').attr('style', 'padding-top:10px;font-size:10px;width:80%;color:red');
        }
        return false;
    }
    function getTransID(grd) {   // -- ga kepake

        var tbl_ = {};
        tbl_["UcRefundPremi_dtgIncentive"] = "TDI";
        tbl_["UcRefundPremiCP_dtgIncentive"] = "TCP";
        tbl_["UcRefundPremiJK_dtgIncentive"] = "TJK";
        tbl_["UcRefundBunga_dtgIncentive"] = "NPV";
        tbl_["UcRefundUppingBunga_dtgIncentive"] = "NPU";
        tbl_["UcRefundBiayaProvisi_dtgIncentive"] = "PRS";
        tbl_["UcRefundBiayaLain_dtgIncentive"] = "TDS";
        return tbl_[grd]
    }
</script>
<div class="form_box_header">
    <div class="form_single">
        <div class="grid_wrapper_ns">
            <asp:TextBox ID="txtSupplier" runat="server" style="display:none" />
            <asp:TextBox ID="txtApplicationID" runat="server" style="display:none" />
            <asp:TextBox ID="txtBranchID" runat="server" style="display:none" />
            <asp:TextBox ID="txtBussinessDate" runat="server" style="display:none" />
            <asp:TextBox ID="txtOTR" runat="server" style="display:none" />
            <asp:TextBox ID="txtTenor" runat="server" style="display:none" />
            <asp:TextBox ID="txtFlatRate" runat="server" style="display:none" />
            <asp:TextBox ID="txtAdmin" runat="server" style="display:none" />
            <asp:TextBox ID="txtAsuransiCom" runat="server" style="display:none" />
            <asp:TextBox ID="txtProvisi" runat="server" style="display:none" />
            <asp:TextBox ID="txtNTF" runat="server" style="display:none" />
            <asp:TextBox ID="txtTotalBunga" runat="server" style="display:none" />
            <asp:TextBox ID="txtOther" runat="server" style="display:none" />
            
            <asp:DataGrid ID="dtgIncentive" runat="server" Width="100%" AutoGenerateColumns="False" BorderStyle="None" BorderWidth="0" CssClass="grid_general" 
            ShowFooter="True">
                <HeaderStyle CssClass="th" />
                <ItemStyle CssClass="item_grid" />
                <FooterStyle CssClass="item_grid" />
                <Columns>
                    <asp:TemplateColumn HeaderText="NO">
                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblNomor"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="JABATAN">
                        <ItemTemplate>
                            <asp:DropDownList runat="server" ID="ddlJabatan" >
                                <%--<asp:ListItem Value="">All</asp:ListItem>--%>
                                <asp:ListItem Value="GM">General Manager</asp:ListItem>
                                <asp:ListItem Value="OM">Operation Manager</asp:ListItem>
                                <asp:ListItem Value="BM">Branch Manager</asp:ListItem>
                                <asp:ListItem Value="SM">Senior Manager</asp:ListItem>
                                <asp:ListItem Value="SV">Sales Supervisor</asp:ListItem>
                                <asp:ListItem Value="SL">Sales Person</asp:ListItem>
                                <asp:ListItem Value="AM">Supplier Admin</asp:ListItem>
                                <asp:ListItem Value="FI">F & I</asp:ListItem>
                                <asp:ListItem Value="SO">Supplier Office</asp:ListItem>
                                <asp:ListItem Value="FM">F & I Manager</asp:ListItem>
                                <asp:ListItem Value="OT">Other</asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator runat="server" ID="rfvddlJabatan" Display="Dynamic" ErrorMessage="*"
                                ControlToValidate="ddlJabatan" InitialValue="" CssClass="validator_general"></asp:RequiredFieldValidator>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="PENERIMA">
                        <ItemTemplate>
                            <asp:DropDownList runat="server" ID="ddlPenerima" style="width:150px">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator runat="server" ID="rfvddlPenerima" Display="Dynamic" ErrorMessage="*"
                                ControlToValidate="ddlPenerima" InitialValue="" CssClass="validator_general"></asp:RequiredFieldValidator>
                        </ItemTemplate>
                        <FooterTemplate>                            
                            <asp:Label runat="server" ID="lblTotal" Text="TOTAL"></asp:Label>
                        </FooterTemplate>
                    </asp:TemplateColumn>
					<%--modify ario--%>
                    <asp:TemplateColumn HeaderText="Trans ID" HeaderStyle-Width="80px"> 
                        <ItemTemplate>
                            <asp:DropDownList runat="server" ID="ddlTransID" style="width:70px">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator runat="server" ID="rfvddlTransID" Display="Dynamic" ErrorMessage="*"
                                ControlToValidate="ddlTransID" InitialValue="" CssClass="validator_general"></asp:RequiredFieldValidator>
                        </ItemTemplate>
                    </asp:TemplateColumn>
					<%--end modify--%>
                    <asp:TemplateColumn HeaderText="PROSENTASE" HeaderStyle-CssClass="th_right">
                        <ItemTemplate>
                            <asp:TextBox runat="server" ID="txtProsentase" onblur="extractNumber(this,2,true);blockInvalid(this);this.value=numberFormat(this.value);this.value=blankToZero(this.value);"
                                onkeyup="extractNumber(this,2,true);" onkeypress="return blockNonNumbers(this, event, true, true);"
                                onfocus="this.value=resetNumber(this.value);"
                                CssClass="numberAlign small_text">0</asp:TextBox>
                        </ItemTemplate>
                        <FooterTemplate>                                                        
                            <asp:TextBox runat="server" ID="txtTotalProsentase" CssClass="numberAlign small_text readonly_text" Enabled="false">0</asp:TextBox>
                        </FooterTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="INSENTIF" HeaderStyle-CssClass="th_right">
                        <ItemTemplate>
                            <asp:TextBox runat="server" ID="txtInsentif" onblur="extractNumber(this,2,true);blockInvalid(this);this.value=numberFormat(this.value);this.value=blankToZero(this.value);"
                                onkeyup="extractNumber(this,2,true);" onkeypress="return blockNonNumbers(this, event, true, true);"
                                onfocus="this.value=resetNumber(this.value);"
                                CssClass="numberAlign regular_text">0</asp:TextBox>
                        </ItemTemplate>
                        <FooterTemplate>                            
                            <asp:TextBox runat="server" ID="txtTotalInsentif" CssClass="numberAlign regular_text readonly_text" Enabled="false">0</asp:TextBox>
                        </FooterTemplate>
                    </asp:TemplateColumn> 
                    <asp:TemplateColumn HeaderText="PPH" HeaderStyle-CssClass="th_right" HeaderStyle-Width="150px"> 
                        <ItemStyle CssClass="th_center"></ItemStyle>
                        <ItemTemplate>
                              <asp:DropDownList runat="server" ID="ddlPPh" style="width:150px"> 
                                  <asp:ListItem Value="0"> </asp:ListItem>
                                <asp:ListItem Value="3.0">PPh 21 Non Npwp</asp:ListItem>
                                <asp:ListItem Value="2.5">PPh 21 Npwp</asp:ListItem>
                                <asp:ListItem Value="4.0">PPh 23 Non Npwp</asp:ListItem> 
                                <asp:ListItem Value="2.0">PPh 23</asp:ListItem> 
                            </asp:DropDownList>
                         <%--   <asp:Label runat="server" ID="lblPPH"></asp:Label>--%>
                        </ItemTemplate>
                    </asp:TemplateColumn>    
                   <asp:TemplateColumn HeaderText="INSENTIF GROSS"> 
                        <ItemStyle CssClass="th_right"></ItemStyle>
                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblInsentifGross" style="text-align:right" >test</asp:Label> 
                        </ItemTemplate>
                    </asp:TemplateColumn>
                   <%--  <asp:TemplateColumn HeaderText="PPH" Visible="false" > 
                        <ItemStyle CssClass="th_center"></ItemStyle>
                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblPPH"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn> --%>

                      <asp:TemplateColumn HeaderText="Potong Pajak" HeaderStyle-CssClass="th_right" HeaderStyle-Width="150px">
                         <ItemStyle CssClass="th_center"></ItemStyle>
                        <ItemTemplate>
                              <asp:DropDownList runat="server" ID="ddlIsDiscountPPH" style="width:150px">  
                                <asp:ListItem Value="NON">Tidak Potong Pajak</asp:ListItem>
                                <asp:ListItem Value="POT">Potong Pajak</asp:ListItem> 
                            </asp:DropDownList> 
                        </ItemTemplate>
                     </asp:TemplateColumn>

                    <asp:TemplateColumn HeaderText="TARIF PAJAK (%)" Visible="false"> 
                        <ItemStyle CssClass="th_center"></ItemStyle>
                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblTarifPajak"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="NILAI PAJAK" Visible="false"> 
                        <ItemStyle CssClass="th_right"></ItemStyle>
                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblNilaiPajak"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="INSENTIF NET" Visible="false"> 
                        <ItemStyle CssClass="th_right"></ItemStyle>
                        <ItemTemplate> 
                            <asp:Label runat="server" ID="lblInsentifNet" visible="false"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                     <%--  <asp:TemplateColumn HeaderText="" HeaderStyle-Width="80px"> 
                        <ItemTemplate>
                            <asp:DropDownList runat="server" ID="ddlTransID" style="width:70px">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator runat="server" ID="rfvddlTransID" Display="Dynamic" ErrorMessage="*"
                                ControlToValidate="ddlTransID" InitialValue="" CssClass="validator_general"></asp:RequiredFieldValidator>
                        </ItemTemplate>
                    </asp:TemplateColumn>--%>
                    <asp:TemplateColumn HeaderText="DELETE">
                        <ItemStyle CssClass="command_col"></ItemStyle>
                        <ItemTemplate>
                            <asp:ImageButton ID="imgDelete" runat="server" CausesValidation="False" ImageUrl="../Images/icondelete.gif"></asp:ImageButton>
                            <asp:Label runat="server" ID="lblSupplierEmployeeID" Visible="false"></asp:Label>
                            <asp:Label runat="server" ID="lblSupplierEmployeePosition" Visible="false"></asp:Label>
                            <asp:Label runat="server" ID="lblEmployeeName" Visible="false"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                </Columns>
            </asp:DataGrid>
        </div>
    </div>    
</div>
<div class="form_button">
    <asp:Button ID="btnAdd" runat="server" Text="Add"  CausesValidation="False"  CssClass="small button blue" />
    <asp:Button ID="btnSave" runat="server" Text="Save" CausesValidation="False" CssClass="small button blue" />
    <label id="pesan" runat="server" style="padding-top:10px;font-size:10px;width:80%"></label>
</div>
