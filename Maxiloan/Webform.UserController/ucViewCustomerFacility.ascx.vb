﻿Imports Maxiloan.Controller

Public Class ucViewCustomerFacility
    Inherits ControlBased

    Public Property CustomerID As String
	Public Property FacilityNo As String
	Public Property FacilityMaturityDate As Date
	Public Property FasilitasInfo As Parameter.CustomerFacility
        Get
            Return CType(ViewState("FasilitasInfo"), Parameter.CustomerFacility)
        End Get
        Set(value As Parameter.CustomerFacility)
            ViewState("FasilitasInfo") = value
        End Set
    End Property
    Public Property ApplicationModule As String
        Get
            Return lblApplicationModule.Text
        End Get
        Set(value As String)
            lblApplicationModule.Text = value
        End Set
    End Property

    Dim oController As New CustomerFacilityController

    Public Sub LoadCustomerFacility()
        Dim oCustomClass As New Parameter.CustomerFacility

        Dim oTable As New DataTable
        With oCustomClass
            .NoFasilitas = Me.FacilityNo
            .strConnection = GetConnectionString()
        End With
        oCustomClass = oController.GetFacilityDetail(oCustomClass)

        lblNamaFasilitas.Text = oCustomClass.NamaFasilitas
        lblNoFasilitas.Text = oCustomClass.NoFasilitas
        lblFasilitasType.Text = GetFasilitasTypeDescription(oCustomClass.FasilitasType)
        lblPeriode.Text = oCustomClass.DrawDownStartDate & " - " & oCustomClass.DrawDownMaturityDate
        lblPlafond.Text = FormatNumber(oCustomClass.FasilitasAmount, 0)
        lblSisaPlafond.Text = FormatNumber(oCustomClass.AvailableAmount, 0)
        lblMinimum.Text = FormatNumber(oCustomClass.MinimumPencairan, 0)
        lblApplicationModule.Text = oCustomClass.ApplicationModule
        lbltglakhirfasilitas.Text = oCustomClass.FacilityMaturityDate
        If oCustomClass.FacilityMaturityDate IsNot Nothing Then
            FacilityMaturityDate = ConvertDate2(oCustomClass.FacilityMaturityDate)
        End If
        FasilitasInfo = oCustomClass
	End Sub

    Function GetFasilitasTypeDescription(ByVal facilityType As String) As String
        Select Case facilityType
            Case "R"
                Return "Revolving"
            Case "N"
                Return "Non Revolving"
            Case Else
                Return ""
        End Select
    End Function

End Class