﻿Public Class ucProductOffering
    Inherits System.Web.UI.UserControl

    Public Property BranchID() As String
        Get
            Return CType(viewstate("branchid"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("branchid") = Value
        End Set
    End Property

    Public Property ProductOfferingID() As String
        Get
            Return hdnProductOfferingID.Value
        End Get
        Set(ByVal Value As String)
            hdnProductOfferingID.Value = Value
        End Set
    End Property

    Public Property ProductOfferingDescription() As String
        Get
            Return txtProductOfferingDescription.Text
        End Get
        Set(ByVal Value As String)
            txtProductOfferingDescription.Text = Value
        End Set
    End Property
    Public Property assetTypeID() As String
        Get
            Return hdnAssetTypeID.Value
        End Get
        Set(ByVal Value As String)
            hdnAssetTypeID.Value = Value
        End Set
    End Property
    Public Property ProductID() As String
        Get
            Return hdnProductID.Value
        End Get
        Set(ByVal Value As String)
            hdnProductID.Value = Value
        End Set
    End Property

    Public Property Style() As String
        Get
            Return CStr(viewstate("style"))
        End Get
        Set(ByVal Value As String)
            viewstate("style") = Value
        End Set
    End Property

    Public Property hideLookup() As Boolean
        Get
            Return hpLookup.Visible
        End Get
        Set(ByVal Value As Boolean)
            hpLookup.Visible = Value
        End Set
    End Property

    Public Sub BindData()
        txtProductOfferingDescription.Attributes.Add("readonly", "true")
        hpLookup.NavigateUrl = "javascript:OpenWinProductOfferingLookup('" & hdnProductOfferingID.ClientID & "','" & txtProductOfferingDescription.ClientID & "', '" & hdnProductID.ClientID & "', '" & hdnAssetTypeID.ClientID & "','" & Me.Style & "','" & Me.BranchID & "')"
    End Sub
End Class