﻿#Region "Import"
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
#End Region

Public Class ucComboBranchAndInsurance
    Inherits ControlBased
    Dim chrClientID As String = ""
#Region "properties"
    Public Property BranchID() As String
        Get
            Return CType(viewstate("BranchID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("BranchID") = Value
        End Set
    End Property

    Public Property BranchIDParent() As String
        Get
            Return CType(cboParent.SelectedItem.Value, String)
        End Get
        Set(ByVal Value As String)
            cboParent.SelectedIndex = cboParent.Items.IndexOf(cboParent.Items.FindByValue(Value))
        End Set
    End Property

    Public Property BranchFullName() As String
        Get
            Return CType(viewstate("BranchFullName"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("BranchFullName") = Value
        End Set
    End Property


    Public Property InsuranceComBranchID() As String
        Get
            Return hdnResult.Value
        End Get
        Set(ByVal Value As String)
            viewstate("ID") = Value
        End Set
    End Property

    Public Property CollectorName() As String
        Get
            Return CType(viewstate("SupervisorName"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("SupervisorName") = Value
        End Set
    End Property


    Private Property oData() As DataTable
        Get
            Return CType(viewstate("oData"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            viewstate("oData") = Value
        End Set
    End Property
#End Region

    Private oController As New GeneralPagingController


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not IsPostBack Then
            chrClientID = Trim(cboChild.ClientID)

            Dim dtParent As New DataTable
            Dim oEntites As New Parameter.InsCoAllocationDetailList

            With oEntites
                .strConnection = GetConnectionString
                If IsHoBranch Then
                    .BranchId = "All"
                Else
                    .BranchId = Me.sesBranchId
                End If
            End With

            oEntites = oController.GetBranchCombo(oEntites)
            dtParent = oEntites.ListData

            cboParent.DataTextField = "Name"
            cboParent.DataValueField = "ID"
            cboParent.DataSource = dtParent
            cboParent.DataBind()
            cboParent.Items.Insert(0, "Select One")
            cboParent.Items(0).Value = "0"
            BindChild()
        End If
    End Sub

    Public Sub BindChild()
        Dim dtChild As New DataTable
        Dim intr As Integer = 0
        dtChild = GetDTBranch()

        Me.oData = dtChild
        intr = Me.oData.Rows.Count
        Header.InnerHtml = GenerateScript(dtChild)

    End Sub

    Private Function GetDTBranch() As DataTable
        Dim dtChild As New DataTable

        Dim oEntitesChild As New Parameter.InsCoAllocationDetailList
        With oEntitesChild
            .strConnection = GetConnectionString()
            If IsHoBranch Then
                .BranchId = "All"
            Else
                .BranchId = Me.sesBranchId
            End If
        End With

        oEntitesChild = oController.GetInsuranceBranchCombo(oEntitesChild)
        dtChild = oEntitesChild.ListData
        Return dtChild
    End Function


    Public Function WOPonChange() As String
        Return "WOPChange('" & Trim(cboParent.ClientID) & "','" & Trim(cboChild.ClientID) & "','" & Trim(hdnResult.ClientID) & "',((this.selectedIndex == -1) ? null : ListData[this.selectedIndex]));"
    End Function

    Protected Function ChildChange() As String
        Return "cboChildonChange(this.options[this.selectedIndex].value,'" & Trim(hdnResult.ClientID) & "');"
    End Function

    Public Function SetFocuscboChild() As String
        Return "SetcboChildFocus()"
    End Function

    Private Function GenerateScript(ByVal DtTable As DataTable) As String
        Dim strScript As String = ""
        Dim strScript1 As String
        Dim ints As Integer
        Dim DataRow As DataRow()
        Dim strType As String
        Dim i As Int32
        Dim j As Int32
        ints = DtTable.Rows.Count
        strScript = "<script language=""JavaScript"">" & vbCrLf
        strScript &= "ListData = new Array(" & vbCrLf
        strType = ""
        For j = 0 To cboParent.Items.Count - 1
            DataRow = DtTable.Select(" BranchID = '" & cboParent.Items(j).Value & "'")
            If DataRow.Length > 0 Then
                For i = 0 To DataRow.Length - 1
                    If strType <> CStr(DataRow(i)("BranchID")).Trim Then
                        strType = CStr(DataRow(i)("BranchID")).Trim
                        strScript &= "new Array(" & vbCrLf
                        strScript1 = ""
                    End If


                    If strScript1 = "" Then
                        strScript1 = " new Array('" & CStr(DataRow(i)("Name")).Trim & "','" & CStr(IIf(IsDBNull(DataRow(i)("ChildID")), "-", DataRow(i)("ChildID"))).Trim & "') "
                    Else
                        strScript1 &= "," & vbCrLf & " new Array('" & CStr(DataRow(i)("Name")).Trim & "','" & CStr(IIf(IsDBNull(DataRow(i)("ChildID")), "-", DataRow(i)("ChildID"))).Trim & "') "
                    End If
                Next
                strScript &= strScript1 & ")," & vbCrLf
            Else
                strScript &= " null," & vbCrLf
            End If
        Next

        If Right(strScript.Trim, 5) = "null," Then
            strScript = Left(strScript.Trim, strScript.Trim.Length - 1)
        Else
            strScript = Left(strScript.Trim, strScript.Trim.Length - 2)
        End If
        If Right(strScript.Trim, 4) = "null" Then
            strScript &= vbCrLf & ");" & vbCrLf
        Else
            strScript &= vbCrLf & "));" & vbCrLf
        End If

        strScript &= "</script>"
        Return strScript
    End Function


    Public Sub GetComboValue()
        Me.BranchIDParent = cboParent.SelectedItem.Value.Trim
        Me.BranchFullName = cboParent.SelectedItem.Text.Trim
    End Sub

    Private Sub cboParent_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboParent.SelectedIndexChanged
        cboChild.Visible = True
    End Sub

    Private Sub cboChild_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboChild.Load
        If cboParent.SelectedItem.Value <> "0" Then
            Dim intr As Integer
            Dim oTable As New DataTable
            Dim tblRow As DataRow
            Dim oRow As DataRow()
            With oTable
                .Columns.Add(New DataColumn("ID", GetType(String)))
                .Columns.Add(New DataColumn("Name", GetType(String)))
            End With
            oRow = Me.oData.Select(" ChildID='" & cboParent.SelectedItem.Value.Trim & "' ")
            If oRow.Length > 0 Then
                For intr = 0 To oRow.Length - 1
                    tblRow = oTable.NewRow
                    tblRow("ChildID") = CStr(oRow(intr)("ChildID")).Trim
                    tblRow("Name") = CStr(oRow(intr)("Name")).Trim
                    oTable.Rows.Add(tblRow)
                Next
            End If
            With cboChild
                .DataSource = oTable
                .DataTextField = "Name"
                .DataValueField = "ChildID"
                .DataBind()
                .Items.Insert(0, "Select One")
                .Items(0).Value = "0"
            End With
        End If
    End Sub

    Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)
        Dim DtChild As DataTable = GetDTBranch()
        Dim i As Integer

        For i = 0 To DtChild.Rows.Count - 1
            Page.ClientScript.RegisterForEventValidation(cboChild.UniqueID, CStr(DtChild.Rows(i).Item("ChildID")).Trim)
        Next

        Page.ClientScript.RegisterForEventValidation(cboChild.UniqueID, "0")

        MyBase.Render(writer)
    End Sub
End Class