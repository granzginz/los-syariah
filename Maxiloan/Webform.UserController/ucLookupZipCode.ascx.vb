﻿Option Strict On
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.General.CommonVariableHelper
Imports System.Linq

Public Class ucLookupZipCode
    Inherits ControlBased

    Private oController As New LookUpZipCodeController
    Private currentPage As Integer = DEFAULT_CURRENT_PAGE
    Private pageSize As Integer = DEFAULT_PAGE_SIZE
    Private currentPageNumber As Int32 = DEFAULT_CURRENT_PAGE
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private ZipCodeDS As DataTable

#Region "Properties"
    Public Property ZipCode() As String
        Get
            Return CType(txtZipCode.Text.Trim, String)
        End Get
        Set(ByVal Value As String)
            txtZipCode.Text = Value
        End Set
    End Property
    Public Property Kelurahan() As String
        Get
            Return CType(txtKelurahan.Text.Trim, String)
        End Get
        Set(ByVal Value As String)
            txtKelurahan.Text = Value
        End Set
    End Property
    Public Property Kecamatan() As String
        Get
            Return CType(txtKecamatan.Text.Trim, String)
        End Get
        Set(ByVal Value As String)
            txtKecamatan.Text = Value
        End Set
    End Property
    Public Property City() As String
        Get
            Return CType(txtCity.Text.Trim, String)
        End Get
        Set(ByVal Value As String)
            txtCity.Text = Value
        End Set
    End Property
    Public Property Style() As String
        Get
            Return CStr(ViewState("style"))
        End Get
        Set(ByVal Value As String)
            ViewState("style") = Value
        End Set
    End Property
    Public Property IsRequired As Boolean
        Get
            Return CBool(ViewState("Validation"))
        End Get
        Set(ByVal Value As Boolean)
            ViewState("Validation") = Value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not Me.IsPostBack Then
            txtKelurahan.Attributes.Add("readOnly", "true")
            txtKecamatan.Attributes.Add("readOnly", "true")
            txtCity.Attributes.Add("readOnly", "true")
            txtZipCode.Attributes.Add("readOnly", "true")
            txtGoPage.Text = "1"
            Me.SortBy = "City ASC"
            If Request("cond") <> "" Then
                Me.SearchBy = Request("cond")
            Else
                Me.SearchBy = "ALL"
            End If

            Dim dttCitySearch As New DataTable

            dttCitySearch = oController.GetCitySearch(GetConnectionString)
            CboCity.DataValueField = "City"
            CboCity.DataTextField = "City"
            CboCity.DataSource = dttCitySearch.DefaultView
            CboCity.DataBind()
            CboCity.Items.Insert(0, "ALL")
            CboCity.Items(0).Value = "ALL"
            'CboCity.Items.Insert(0, "Select One")
            'CboCity.Items(0).Value = "0"

        End If
    End Sub

    Public Sub ValidatorFalse()
        RequiredFieldValidator1.Enabled = False
        lblKodePos.Attributes("class") = "label_split"
    End Sub

    Public Sub ValidatorTrue()
        RequiredFieldValidator1.Enabled = True
        lblKodePos.Attributes("class") = "label_split_req"
    End Sub

    Public Sub BindData()
        txtKelurahan.Text = Me.Kelurahan
        txtKecamatan.Text = Me.Kecamatan
        txtCity.Text = Me.City
        txtZipCode.Text = Me.ZipCode

        pnlGrid.Visible = False
    End Sub

    Protected Sub BindGridEntity(ByVal cmdWhere As String, Optional ByVal PageIndex As Integer = 0)
        Dim dttEntity As DataTable = Nothing
        Dim oCustomClass As New Maxiloan.Parameter.LookUpZipCode
        pnlGrid.Visible = True
        oCustomClass = oController.GetListData(GetConnectionString, cmdWhere, currentPage, pageSize, Me.SortBy)

        If Not oCustomClass Is Nothing Then
            dttEntity = oCustomClass.ListData
            recordCount = oCustomClass.TotalRecords
        Else
            recordCount = 0
        End If
        dtgPaging.DataSource = dttEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()
        PagingFooter()
        Me.mpLookupZipCode.Show()


        'ZipCodeDS = CType(Cache.Item("CACHE_ZIPCODE"), DataTable)

        'If ZipCodeDS Is Nothing Then
        '    Dim dtCache As New DataTable
        '    dtCache = oController.GetListData(GetConnectionString, "ALL", 1, 80000, Me.SortBy).ListData
        '    Me.Cache.Insert("CACHE_ZIPCODE", dtCache, Nothing, DateTime.Now.AddHours(DEFAULT_TIME_DURATION_CACHE_HOUR), TimeSpan.Zero)
        '    ZipCodeDS = CType(Me.Cache.Item("CACHE_ZIPCODE"), DataTable)
        'End If


        'Dim dv As DataView = New DataView(ZipCodeDS)

        'dv.RowFilter = cmdWhere.Replace("ALL", "")

        'dtgPaging.DataSource = dv
        'dtgPaging.CurrentPageIndex = PageIndex
        'dtgPaging.DataBind()

        'pnlGrid.Visible = True
        'Me.mpLookupZipCode.Show()



        'cmdWhere = " WHERE " & cmdWhere.Replace("ALL", " 1 = 1")
        'SqlDataSource1.ConnectionString = Me.GetConnectionString
        'SqlDataSource1.SelectCommand = "SELECT Kelurahan, Kecamatan, City, ZipCode FROM Kelurahan" & cmdWhere

        'dtgPaging.DataSource = SqlDataSource1
        'dtgPaging.CurrentPageIndex = PageIndex
        'dtgPaging.DataBind()

        'pnlGrid.Visible = True
        'Me.mpLookupZipCode.Show()

    End Sub

#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            lblMessage.Text = "Data tidak ditemukan ....."
            lblTotPage.Text = "1"            
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()            
        End If
        lblTotRec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridEntity(Me.SearchBy)
    End Sub
    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGoPage.Click
        If IsNumeric(txtGoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtGoPage.Text, Int32)
                BindGridEntity(Me.SearchBy)
            Else
                txtGoPage.Text = String.Empty                
            End If
        End If
    End Sub
#End Region

    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonSearch.Click
        Me.SearchBy = "ALL"
        ' If CboCity.SelectedIndex > 0 Then
        If CboCity.SelectedValue = "ALL" Then
            If txtSearch.Text.Trim <> "" Then

                'If Right(txtSearch.Text.Trim, 1) = "%" Then
                '    Me.SearchBy = cboSearchBy.SelectedItem.Value + " like '" + Replace(txtSearch.Text.Trim, "'", "''") + "'"
                'Else
                '    Me.SearchBy = cboSearchBy.SelectedItem.Value + " = '" + Replace(txtSearch.Text.Trim, "'", "''") + "'"
                'End If

                Me.SearchBy = cboSearchBy.SelectedItem.Value + " LIKE '%" + Replace(txtSearch.Text.Trim, "'", "''") + "%'"
            End If
        Else
            Me.SearchBy = " City = '" & CboCity.SelectedItem.Value & "'"
            If txtSearch.Text.Trim <> "" And cboSearchBy.SelectedValue <> "" Then
                'If Right(txtSearch.Text.Trim, 1) = "%" Then
                '    Me.SearchBy &= " And " & cboSearchBy.SelectedItem.Value + " like '" + Replace(txtSearch.Text.Trim, "'", "''") + "'"
                'Else
                '    Me.SearchBy &= " And " & cboSearchBy.SelectedItem.Value + " = '" + Replace(txtSearch.Text.Trim, "'", "''") + "'"
                'End If

                Me.SearchBy &= " AND " & cboSearchBy.SelectedItem.Value + " LIKE '%" + Replace(txtSearch.Text.Trim, "'", "''") + "%'"

            End If
        End If
        BindGridEntity(Me.SearchBy)
        ' End If
    End Sub

    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonReset.Click
        CboCity.SelectedIndex = 0
        cboSearchBy.SelectedIndex = 0
        txtSearch.Text = ""
        BindGridEntity("ALL")
        pnlGrid.Visible = False
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        txtCity.Text = ""
        txtKecamatan.Text = ""
        txtKelurahan.Text = ""
        txtZipCode.Text = ""
    End Sub

    Private Sub dtgPaging_PageIndexChanged(source As Object, e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dtgPaging.PageIndexChanged        
        BindGridEntity(Me.SearchBy, e.NewPageIndex)
        Me.mpLookupZipCode.Show()
    End Sub

    Private Sub dtgPaging_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtgPaging.SelectedIndexChanged
        Dim i As Integer = dtgPaging.SelectedIndex
        txtKelurahan.Text = dtgPaging.Items(i).Cells(1).Text.Trim
        txtKecamatan.Text = dtgPaging.Items(i).Cells(2).Text.Trim
        txtCity.Text = dtgPaging.Items(i).Cells(3).Text.Trim
        txtZipCode.Text = dtgPaging.Items(i).Cells(4).Text.Trim
        Me.mpLookupZipCode.Hide()
    End Sub

    Public Sub FeatureEnabled(ByVal status As Boolean)
        If status = True Then
            txtZipCode.Enabled = True
            txtKecamatan.Enabled = True
            txtKelurahan.Enabled = True
            txtCity.Enabled = True
        Else
            txtZipCode.Enabled = False
            txtKecamatan.Enabled = False
            txtKelurahan.Enabled = False
            txtCity.Enabled = False
        End If
    End Sub
End Class