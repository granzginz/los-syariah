﻿#Region "Import"
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Interface
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
#End Region

Public Class UCCollectorRpt2
    Inherits ControlBased
    Dim chrClientID As String
    Private oController As New RptCollActController

#Region "properties"
    Public Property cgid() As String
        Get
            Return CType(ViewState("CGID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CGID") = Value
        End Set
    End Property

    Public Property CGIDParent() As String
        Get
            Return CType(cboParent.SelectedItem.Value, String)
            'Return CType(cboParent.SelectedItem.Value, String)
        End Get
        Set(ByVal Value As String)
            cboParent.SelectedIndex = cboParent.Items.IndexOf(cboParent.Items.FindByValue(Value))
        End Set
    End Property

    Public Property CGIDParentName() As String
        Get
            Return CType(cboParent.SelectedItem.Text, String)
            'Return CType(cboParent.SelectedItem.Value, String)
        End Get
        Set(ByVal Value As String)
            cboParent.SelectedIndex = cboParent.Items.IndexOf(cboParent.Items.FindByText(Value))
        End Set
    End Property

    Public Property CollectorIDChildName() As String
        Get
            Return CType(cboChild.SelectedItem.Text, String)
        End Get
        Set(ByVal Value As String)
            cboChild.SelectedIndex = cboChild.Items.IndexOf(cboChild.Items.FindByText(Value))
        End Set
    End Property

    Public Property CGName() As String
        Get
            Return CType(ViewState("CollectorTypeName"), String)
            'Return CType(cboParent.SelectedItem.Text, String)
        End Get
        Set(ByVal Value As String)
            ViewState("CollectorTypeName") = Value
        End Set
    End Property


    Public Property CollectorID() As String
        Get
            Return hdnResult.Value
        End Get
        Set(ByVal Value As String)
            hdnResult.Value = Value
        End Set
    End Property



    Public Property CollectorName() As String
        Get
            Return CType(ViewState("SupervisorName"), String)
            'Return CType(cboChild.SelectedItem.Text, String)
        End Get
        Set(ByVal Value As String)
            ViewState("SupervisorName") = Value
            'cboChild.SelectedIndex = cboChild.Items.IndexOf(cboChild.Items.FindByText(Value))
        End Set
    End Property

    Public Property CollectorType() As String
        Get
            Return CType(ViewState("CollectorType"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CollectorType") = Value
        End Set
    End Property
    Private Property oData() As DataTable
        Get
            Return CType(ViewState("oData"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            ViewState("oData") = Value
        End Set
    End Property

    Private Property isPostBackChild() As Boolean
        Get
            Return cboChild.AutoPostBack
        End Get
        Set(ByVal Value As Boolean)
            cboChild.AutoPostBack = Value
            ViewState("ispostbackchild") = Value
        End Set
    End Property

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not IsPostBack Then
            chrClientID = Trim(cboChild.ClientID)

            Dim dtParent As New DataTable
            Dim Coll As New Parameter.RptCollAct

            With Coll
                .strConnection = GetConnectionString()
                .CGID = Me.GroubDbID
                .strKey = "CG"
                .CollectorType = ""
            End With

            Coll = oController.ViewDataCollector(Coll)
            dtParent = Coll.ListCollector

            cboParent.DataTextField = "CGName"
            cboParent.DataValueField = "CGID"
            cboParent.DataSource = dtParent
            cboParent.DataBind()
            cboParent.Items.Insert(0, "Select One")
            cboParent.Items(0).Value = "0"
            If dtParent.Rows.Count = 1 Then
                cboParent.Items.FindByValue(Me.GroubDbID).Selected = True
            End If
            BindChild()
        End If
    End Sub

    Public Sub BindChild()
        Dim dtChild As New DataTable
        Dim Coll As New Parameter.RptCollAct
        Dim CollList As New Parameter.RptCollAct

        With Coll
            .strConnection = GetConnectionString()
            .CGID = Me.GroubDbID
            .CollectorType = Me.CollectorType
            .strKey = ""
        End With

        CollList = oController.ViewDataCollector(Coll)
        dtChild = CollList.ListCollector
        Me.oData = dtChild
        Header.InnerHtml = GenerateScript(dtChild)
    End Sub

    Protected Function WOPonChange() As String
        Return "WOPChange('" & Trim(cboParent.ClientID) & "','" & Trim(cboChild.ClientID) & "','" & Trim(hdnResult.ClientID) & "',((this.selectedIndex == -1) ? null : ListData[this.selectedIndex]));"
    End Function

    Protected Function ChildChange() As String
        Return "cboChildonChange(this.options[this.selectedIndex].value,'" & Trim(hdnResult.ClientID) & "');"
    End Function

    Public Function SetFocuscboChild() As String
        Return "SetcboChildFocus()"
    End Function

    Private Function GenerateScript(ByVal DtTable As DataTable) As String
        Dim strScript As String
        Dim strScript1 As String
        Dim ints As Integer
        Dim DataRow As DataRow()
        Dim strType As String
        Dim i As Int32
        Dim j As Int32
        ints = DtTable.Rows.Count
        strScript = "<script language=""JavaScript"">" & vbCrLf
        strScript &= "ListData = new Array(" & vbCrLf
        strType = ""
        For j = 0 To cboParent.Items.Count - 1
            DataRow = DtTable.Select(" CGID = '" & cboParent.Items(j).Value & "'")
            If DataRow.Length > 0 Then
                For i = 0 To DataRow.Length - 1
                    If strType <> CStr(DataRow(i)("CGID")).Trim Then
                        strType = CStr(DataRow(i)("CGID")).Trim
                        strScript &= "new Array(" & vbCrLf
                        strScript1 = ""
                    End If


                    If strScript1 = "" Then
                        strScript1 = " new Array('" & CStr(DataRow(i)("Name")).Trim & "','" & CStr(IIf(IsDBNull(DataRow(i)("CollectorID")), "-", DataRow(i)("CollectorID"))).Trim & "') "
                    Else
                        strScript1 &= "," & vbCrLf & " new Array('" & CStr(DataRow(i)("Name")).Trim & "','" & CStr(IIf(IsDBNull(DataRow(i)("CollectorID")), "-", DataRow(i)("CollectorID"))).Trim & "') "
                    End If
                Next
                strScript &= strScript1 & ")," & vbCrLf
            Else
                strScript &= " null," & vbCrLf
            End If
        Next



        If Right(strScript.Trim, 5) = "null," Then
            strScript = Left(strScript.Trim, strScript.Trim.Length - 1)
        Else
            strScript = Left(strScript.Trim, strScript.Trim.Length - 2)
        End If

        If Right(strScript.Trim, 4) = "null" Then
            strScript &= vbCrLf & ");" & vbCrLf
        Else
            strScript &= vbCrLf & "));" & vbCrLf
        End If
        strScript &= "</script>"
        Return strScript
    End Function

    Public Sub GetComboValue()
        Me.CGIDParent = cboParent.SelectedItem.Value.Trim
        Me.CGName = cboParent.SelectedItem.Text.Trim
    End Sub

    Private Sub cboParent_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboParent.SelectedIndexChanged
        cboChild.Visible = True
    End Sub

    Public Sub InsertComboChild()
        Dim coll As New Parameter.RptCollAct
        Dim Supervisor_List As New Parameter.RptCollAct
        Dim i As Integer
        Dim indexSelected As Int16

        Dim dt As New DataTable

        With coll
            .strConnection = GetConnectionString()
            .CGID = Me.cgid
            .strKey = ""
            .CollectorType = ""
        End With

        Supervisor_List = oController.ViewDataCollector(coll)
        dt = Supervisor_List.ListCollector



        With cboChild
            .Items.Clear()

            For i = 0 To dt.Rows.Count - 1
                .Items.Insert(i, CStr(dt.Rows(i).Item("Name")))
                .Items(i).Value = CStr(dt.Rows(i).Item("CollectorID"))
                If .Items(i).Value.Trim = Me.CollectorID.Trim Then indexSelected = CShort(i)
            Next
            .SelectedIndex = indexSelected
        End With
    End Sub

    Private Sub cboChild_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboChild.Load
        If Not Page.IsPostBack Then
            If cboParent.SelectedItem.Value <> "0" Then
                Dim intr As Integer
                Dim oTable As New DataTable
                Dim tblRow As DataRow
                Dim oRow As DataRow()
                With oTable
                    .Columns.Add(New DataColumn("CollectorID", GetType(String)))
                    .Columns.Add(New DataColumn("Name", GetType(String)))
                End With
                oRow = Me.oData.Select(" CGID='" & cboParent.SelectedItem.Value.Trim & "' ")
                If oRow.Length > 0 Then
                    For intr = 0 To oRow.Length - 1
                        tblRow = oTable.NewRow
                        tblRow("CollectorID") = CStr(oRow(intr)("CollectorID")).Trim
                        tblRow("Name") = CStr(oRow(intr)("Name")).Trim
                        oTable.Rows.Add(tblRow)
                    Next
                End If
                With cboChild
                    .DataSource = oTable
                    .DataTextField = "Name"
                    .DataValueField = "CollectorID"
                    .DataBind()
                    .Items.Insert(0, "Select One")
                    .Items(0).Value = "0"
                End With
            End If
        End If
    End Sub

    Public Sub cboCollectorRefresh()
        With cboChild
            .Items.Insert(0, "Select One")
            .Items(0).Value = "0"
        End With
    End Sub

End Class