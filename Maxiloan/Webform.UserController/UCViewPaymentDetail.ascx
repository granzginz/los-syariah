﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UCViewPaymentDetail.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.UCViewPaymentDetail" %>
<table class="tablegrid" cellspacing="1" cellpadding="3" width="100%" align="center"
    border="0">
    <tr>
        <td class="tdjudul" colspan="4">
            RINCIAN PEMBAYARAN
        </td>
    </tr>
</table>
<table class="tablegrid" cellspacing="1" cellpadding="3" width="100%" align="center"
    border="0">
    <tr>
        <td class="tdgenap" width="25%">
            Terima Dari
        </td>
        <td class="tdganjil" width="25%">
            <asp:Label ID="lblReceivedFrom" runat="server"></asp:Label>
        </td>
        <td class="tdgenap" width="25%" style="color: red; font-weight: bold">
            No Bukti Kas Masuk
        </td>
        <td class="tdganjil" width="25%">
            <asp:Label ID="lblReferenceNo" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td class="tdgenap" width="25%">
            Cara Pembayaran
        </td>
        <td class="tdganjil" width="25%">
            <asp:Label ID="lblWayOfPayment" runat="server"></asp:Label>
        </td>
        <td class="tdgenap" width="25%">
            Tanggal Valuta
        </td>
        <td class="tdganjil" width="25%">
            <asp:Label ID="lblValueDate" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td class="tdgenap" width="25%">
            Rekening
        </td>
        <td class="tdganjil" width="25%">
            <asp:Label ID="lblBankAccount" runat="server"></asp:Label>
        </td>
        <td class="tdgenap" width="25%">
            Jumlah Di Terima
        </td>
        <td class="tdganjil" width="25%" align="right">
            <asp:Label ID="lblAmountReceive" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
		<td class="tdgenap" width="25%">Bayar di</td>
		<td class="tdganjil" width="25%"><asp:label id="lblBayarDi" runat="server"></asp:label></td>
		<td class="tdgenap" width="25%">Collector</td>
		<td class="tdganjil" width="25%"><asp:label id="lblCollector" runat="server"></asp:label></td>
	</tr>
    <tr>
        <td class="tdgenap" width="25%">
            Keterangan
        </td>
        <td class="tdganjil" width="75%" colspan="3">
            <asp:Label ID="lblNotes" runat="server" Width="100%"></asp:Label>
        </td>
    </tr>
</table>
