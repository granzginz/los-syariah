﻿
#Region "Import"
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
#End Region

Public Class ucCollectorActivity
    Inherits ControlBased

#Region "properties"
    Public Property cgid() As String
        Get
            Return CType(viewstate("CGID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CGID") = Value
        End Set
    End Property

    Public Property CGIDParent() As String
        Get
            Return CType(cboParent.SelectedItem.Value, String)            
        End Get
        Set(ByVal Value As String)
            cboParent.SelectedIndex = cboParent.Items.IndexOf(cboParent.Items.FindByValue(Value))
        End Set
    End Property

    Public Property CGName() As String
        Get
            Return CType(viewstate("CollectorTypeName"), String)            
        End Get
        Set(ByVal Value As String)
            viewstate("CollectorTypeName") = Value
        End Set
    End Property


    Public Property CollectorID() As String
        Get
            Return hdnResult.Value
        End Get
        Set(ByVal Value As String)
            hdnResult.Value = Value
        End Set
    End Property

    Public Property CollectorName() As String
        Get
            Return CType(viewstate("SupervisorName"), String)

        End Get
        Set(ByVal Value As String)
            viewstate("SupervisorName") = Value
        End Set
    End Property

    Public Property CollectorType() As String
        Get
            Return CType(viewstate("CollectorType"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CollectorType") = Value
        End Set
    End Property

    Public Property strKey() As String
        Get
            Return CType(viewstate("strKey"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("strKey") = Value
        End Set
    End Property

    Public Property strAll() As String
        Get
            Return CType(viewstate("strAll"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("strAll") = Value
        End Set
    End Property

    Private Property oData() As DataTable
        Get
            Return CType(viewstate("oData"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            viewstate("oData") = Value
        End Set
    End Property
#End Region

    Dim chrClientID As String
    Private cboChildValue As String
    Private oController As New CLActivityController


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load        
        If Not IsPostBack Then
            chrClientID = Trim(cboChild.ClientID)

            Dim dtParent As New DataTable
            Dim Coll As New Parameter.RptCollAct
            Dim ControllerCG As New RptCollActController
           
            If Me.strAll = "1" Then
                With Coll
                    .strConnection = getconnectionstring()
                    .CGID = Me.GroubDbID
                    .strKey = "CG_ALL"
                    .CollectorType = ""
                End With
            Else
                With Coll
                    .strConnection = getconnectionstring()
                    .CGID = Me.GroubDbID
                    .strKey = "CG"
                    .CollectorType = ""
                End With
            End If
            Coll = ControllerCG.ViewDataCollector(Coll)
            dtParent = Coll.ListCollector

            cboParent.DataTextField = "CGName"
            cboParent.DataValueField = "CGID"
            cboParent.DataSource = dtParent
            cboParent.DataBind()
            cboParent.Items.Insert(0, "Select One")
            cboParent.Items(0).Value = "0"
            'cboParent.SelectedIndex = 0
            If dtParent.Rows.Count = 1 Then
                cboParent.Items.FindByValue(Me.GroubDbID).Selected = True
            End If
            BindChild()
        End If
    End Sub

    Public Sub BindChild()
        Dim dtChild As New DataTable
        Dim Coll As New Parameter.CLActivity
        Dim CollList As New Parameter.CLActivity
        Dim intr As Integer
        With Coll
            .strConnection = getconnectionstring()
            .CGID = Me.GroubDbID
            .LoginId = Me.Loginid
            .strKey = Me.strKey
        End With
        CollList = oController.CLActivityListCollector(Coll)
        dtChild = CollList.ListCLActivity
        Me.oData = dtChild
        intr = Me.oData.Rows.Count
        Header.InnerHtml = GenerateScript(dtChild)
        Dim strScript As String
        strScript = "<script language=""JavaScript"">" & vbCrLf
        strScript &= "WOPChange('" & Trim(cboParent.ClientID) & "','" & Trim(cboChild.ClientID) & "','" & Trim(hdnResult.ClientID) & "',((eval('document.forms[0]." & cboParent.ClientID & "').selectedIndex  == -1) ? null : ListData[eval('document.forms[0]." & cboParent.ClientID & "').selectedIndex]));"

        strScript &= "RestoreInsuranceCoIndex(" & Request("hdnResult") & ");"
        strScript &= "</script>"
        divInnerHTML.InnerHtml = strScript
    End Sub

    Public Function WOPonChange() As String
        Return "WOPChange('" & Trim(cboParent.ClientID) & "','" & Trim(cboChild.ClientID) & "','" & Trim(hdnResult.ClientID) & "',((this.selectedIndex == -1) ? null : ListData[this.selectedIndex]));"
    End Function

    Public Function ChildChange() As String
        Return "cboChildonChange(this.selectedIndex,'" & hdnIndexResult.ClientID & "', this.options[this.selectedIndex].value,'" & Trim(hdnResult.ClientID) & "');"
    End Function

    Public Function SetFocuscboChild() As String
        Return "SetcboChildFocus()"
    End Function

    Private Function GenerateScript(ByVal DtTable As DataTable) As String
        Dim strScript As String
        Dim strScript1 As String
        Dim ints As Integer
        Dim DataRow As DataRow()
        Dim strType As String
        Dim i As Int32
        Dim j As Int32
        ints = DtTable.Rows.Count
        strScript = "<script language=""JavaScript"">" & vbCrLf
        strScript &= "ListData = new Array(" & vbCrLf
        strType = ""
        For j = 0 To cboParent.Items.Count - 1
            DataRow = DtTable.Select(" CGID = '" & cboParent.Items(j).Value & "'")
            If DataRow.Length > 0 Then
                For i = 0 To DataRow.Length - 1
                    If strType <> CStr(DataRow(i)("CGID")).Trim Then
                        strType = CStr(DataRow(i)("CGID")).Trim
                        strScript &= "new Array(" & vbCrLf
                        strScript1 = ""
                    End If


                    If strScript1 = "" Then
                        strScript1 = " new Array('" & CStr(DataRow(i)("Name")).Trim & "','" & CStr(IIf(IsDBNull(DataRow(i)("CollectorID")), "-", DataRow(i)("CollectorID"))).Trim & "') "
                    Else
                        strScript1 &= "," & vbCrLf & " new Array('" & CStr(DataRow(i)("Name")).Trim & "','" & CStr(IIf(IsDBNull(DataRow(i)("CollectorID")), "-", DataRow(i)("CollectorID"))).Trim & "') "
                    End If


                Next
                strScript &= strScript1 & ")," & vbCrLf
            Else
                strScript &= " null," & vbCrLf
            End If
        Next

        If Right(strScript.Trim, 5) = "null," Then
            strScript = Left(strScript.Trim, strScript.Trim.Length - 1)
        Else
            strScript = Left(strScript.Trim, strScript.Trim.Length - 2)
        End If
        If Right(strScript.Trim, 4) = "null" Then
            strScript &= vbCrLf & ");" & vbCrLf
        Else
            strScript &= vbCrLf & "));" & vbCrLf
        End If

        strScript &= "</script>"
        Return strScript
    End Function

    'Public Sub BindCombo()
    '    cboParent.SelectedIndex = cboParent.Items.IndexOf(cboParent.Items.FindByValue(Me.CollectorTypeID))
    '    cboChild.SelectedIndex = cboChild.Items.IndexOf(cboChild.Items.FindByValue("Flin"))
    'End Sub

    Public Sub GetComboValue()
        Me.CGIDParent = cboParent.SelectedItem.Value.Trim
        Me.CGName = cboParent.SelectedItem.Text.Trim
    End Sub

    Private Sub cboParent_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboParent.SelectedIndexChanged
        cboChild.Visible = True
    End Sub

    'Public Function GenerateChild() As String

    '    Return "GenerateChild('" & Trim(cboParent.ClientID) & "','" & Trim(cboChild.ClientID) & "','" & Trim(hdnSP.ClientID) & "',((this.selectedIndex == -1) ? null : ListData[this.selectedIndex])" & Me.SupervisorID & ");"
    'End Function

    'Public Sub GoGenerateChild()
    '    Header.InnerHtml = "<script language=""""JavaScript""""> SetcboChildFocus();</script>"
    'End Sub

    Public Sub InsertComboChild()
        Dim coll As New Parameter.CLActivity
        Dim Supervisor_List As New Parameter.CLActivity
        Dim i As Integer
        Dim indexSelected As Int16

        Dim dt As New DataTable

        With coll
            .strConnection = getConnectionString
            .CGID = Me.cgid
            .strKey = Me.strKey
            .LoginId = Me.Loginid
        End With

        Supervisor_List = oController.CLActivityListCollector(coll)
        dt = Supervisor_List.ListCLActivity



        With cboChild
            .Items.Clear()

            For i = 0 To dt.Rows.Count - 1
                .Items.Insert(i, CStr(dt.Rows(i).Item("Name")))
                .Items(i).Value = CStr(dt.Rows(i).Item("CollectorID"))
                If .Items(i).Value.Trim = Me.CollectorID.Trim Then indexSelected = CShort(i)
            Next
            .SelectedIndex = indexSelected
        End With
    End Sub

    Private Sub cboChild_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboChild.Load
        If cboParent.SelectedItem.Value <> "0" Then
            Dim intr As Integer
            Dim oTable As New DataTable
            Dim tblRow As DataRow
            Dim oRow As DataRow()
            With oTable
                .Columns.Add(New DataColumn("CollectorID", GetType(String)))
                .Columns.Add(New DataColumn("Name", GetType(String)))
            End With
            oRow = Me.oData.Select(" CGID='" & cboParent.SelectedItem.Value.Trim & "' ")
            If oRow.Length > 0 Then
                For intr = 0 To oRow.Length - 1
                    tblRow = oTable.NewRow
                    tblRow("CollectorID") = CStr(oRow(intr)("CollectorID")).Trim
                    tblRow("Name") = CStr(oRow(intr)("Name")).Trim
                    oTable.Rows.Add(tblRow)
                Next
            End If
            With cboChild
                .DataSource = oTable
                .DataTextField = "Name"
                .DataValueField = "CollectorID"
                .DataBind()
                .Items.Insert(0, "Select One")
                .Items(0).Value = "0"
            End With
        End If
    End Sub
End Class
