﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UcLookUpPdctOffering.ascx.vb" Inherits="Maxiloan.Webform.UserController.UcLookUpPdctOffering" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<div runat="server" id="jlookupContent" />
  <div class="form_box_usercontrol">
            <div class="form_left_usercontrol">
                <div style="float:left">
                <label class="label_req">Produk Jual</label>
                  <asp:HiddenField runat="server" ID="hdnProductID" />  
                  <asp:HiddenField runat="server" ID="hdnProductOfferingID" />  
                  <asp:HiddenField runat="server" ID="hdnAssetTypeID" /> 
                  <asp:HiddenField runat="server" ID="hdnKegiatanUsaha" />
                  <asp:HiddenField runat="server" ID="hdnJenisPembiayaan" />
                  <asp:HiddenField runat="server" ID="hdnAkad" /> 
                <asp:TextBox runat="server" ID="txtProductOffering" Enabled="false" CssClass="medium_text"></asp:TextBox>
               </div>
               <div  style="margin-top:2px">
                     <asp:Panel ID="pnlLookupProdOff" runat="server">
                     <button class="small buttongo blue" style="display:inherit"
                             onclick ="OpenJLookup('<%= ResolveClientUrl("~/webform.Utility/jLookup/ProductOffering.aspx?productID=" & hdnProductID.ClientID & "&productOfferingID=" & hdnProductOfferingID.ClientID & "&assetTypeID=" & hdnAssetTypeID.ClientID & "&desc=" & txtProductOffering.ClientID & "&KegiatanUsaha=" & hdnKegiatanUsaha.Value & "&JenisPembiayaan=" & hdnJenisPembiayaan.Value & "&Akad=" & hdnAkad.Value)%>','Daftar Product','<%= jlookupContent.ClientID %>');return false;">...</button>  
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Pilih product offering!"
                        Display="Dynamic" ControlToValidate="txtProductOffering" SetFocusOnError="True"
                        CssClass="validator_general"></asp:RequiredFieldValidator>
                        </asp:Panel>
               </div>
            </div>
        </div>
