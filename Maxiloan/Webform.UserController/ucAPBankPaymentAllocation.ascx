﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucAPBankPaymentAllocation.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.ucAPBankPaymentAllocation" %>
<asp:DropDownList ID="cboBankName" runat="server">
</asp:DropDownList>
<asp:Button ID="btnRefresh" runat="server" Text="Refresh" CausesValidation="False">
</asp:Button>
<asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" InitialValue="0"
    Display="Dynamic" ErrorMessage="Please Select AP Bank Payment Allocation" ControlToValidate="cboBankName" CssClass ="validator_general"></asp:RequiredFieldValidator>