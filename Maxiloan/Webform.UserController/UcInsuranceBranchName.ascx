﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UcInsuranceBranchName.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.UcInsuranceBranchName" %>
<asp:DropDownList ID="DDLInsuranceBranch" runat="server">
</asp:DropDownList>
<asp:RequiredFieldValidator ID="ReqValField" runat="server" ErrorMessage="Pilih Asuransi!"
    ControlToValidate="DDLInsuranceBranch" Display="Dynamic" CssClass ="validator_general"></asp:RequiredFieldValidator>
<asp:Button ID="btnClearCache" runat="server" Text="Refresh" CausesValidation="False" CssClass="small buttongo blue" Visible ="false" >
</asp:Button>
