﻿Imports System.Data.SqlClient

Public Class ucProspectTab
    Inherits Maxiloan.Webform.ControlBased

    Public Property ProspectAppID As String

    Public Sub setLink()
        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(GetConnectionString)
        Dim objReader As SqlDataReader

        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()

            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.CommandText = "spGetProspectTab"
            objCommand.Parameters.Add("@ProspectAppID", SqlDbType.VarChar, 20).Value = Me.ProspectAppID.Trim
            objCommand.Connection = objConnection
            objReader = objCommand.ExecuteReader()

            If objReader.Read Then
                If Not IsDBNull(objReader.Item("DateEntryFinancialData")) Then
                    hypAplikasi.NavigateUrl = "~/Webform.SalesMarketing/ProspectApplicationMain.aspx?id=" & Me.ProspectAppID.Trim & "&page=Edit"
                    hypAsset.NavigateUrl = "~/Webform.SalesMarketing/ProspectAsset.aspx?id=" & Me.ProspectAppID.Trim & "&page=Edit"
                    hypDemografi.NavigateUrl = "~/Webform.SalesMarketing/ProspectDemografi.aspx?id=" & Me.ProspectAppID.Trim & "&page=Edit"
                    hypFinancial.NavigateUrl = "~/Webform.SalesMarketing/ProspectFinancial.aspx?id=" & Me.ProspectAppID.Trim & "&page=Edit"
                ElseIf Not IsDBNull(objReader.Item("DateEntryDemografiData")) Then
                    hypAplikasi.NavigateUrl = "~/Webform.SalesMarketing/ProspectApplicationMain.aspx?id=" & Me.ProspectAppID.Trim & "&page=Edit"
                    hypAsset.NavigateUrl = "~/Webform.SalesMarketing/ProspectAsset.aspx?id=" & Me.ProspectAppID.Trim & "&page=Edit"
                    hypDemografi.NavigateUrl = "~/Webform.SalesMarketing/ProspectDemografi.aspx?id=" & Me.ProspectAppID.Trim & "&page=Edit"
                    hypFinancial.NavigateUrl = "~/Webform.SalesMarketing/ProspectFinancial.aspx?id=" & Me.ProspectAppID.Trim & "&page=Edit"
                ElseIf Not IsDBNull(objReader.Item("DateEntryAssetData")) Then
                    hypAplikasi.NavigateUrl = "~/Webform.SalesMarketing/ProspectApplicationMain.aspx?id=" & Me.ProspectAppID.Trim & "&page=Edit"
                    hypAsset.NavigateUrl = "~/Webform.SalesMarketing/ProspectAsset.aspx?id=" & Me.ProspectAppID.Trim & "&page=Edit"
                    hypDemografi.NavigateUrl = "~/Webform.SalesMarketing/ProspectDemografi.aspx?id=" & Me.ProspectAppID.Trim & "&page=Edit"
                    hypFinancial.NavigateUrl = ""
                ElseIf Not IsDBNull(objReader.Item("DateEntryApplicationData")) Then
                    hypAplikasi.NavigateUrl = "~/Webform.SalesMarketing/ProspectApplicationMain.aspx?id=" & Me.ProspectAppID.Trim & "&page=Edit"
                    hypAsset.NavigateUrl = "~/Webform.SalesMarketing/ProspectAsset.aspx?id=" & Me.ProspectAppID.Trim & "&page=Edit"
                    hypDemografi.NavigateUrl = ""
                    hypFinancial.NavigateUrl = ""
                Else
                    hypAplikasi.NavigateUrl = "~/Webform.SalesMarketing/ProspectApplicationMain.aspx?id=" & Me.ProspectAppID.Trim
                    hypAsset.NavigateUrl = ""
                    hypDemografi.NavigateUrl = ""
                    hypFinancial.NavigateUrl = ""
                End If
            End If
            objReader.Close()
        Catch ex As Exception
            Throw New Exception(ex.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
            objCommand.Dispose()
        End Try
    End Sub

    Public Sub selectedTab(ByVal strTab As String)
        Select Case strTab
            Case "Application"
                tabAplikasi.Attributes.Add("class", "tab_selected")
                tabAsset.Attributes.Add("class", "tab_notselected")
                tabFinancial.Attributes.Add("class", "tab_notselected")
                tabDemografi.Attributes.Add("class", "tab_notselected")
            Case "Asset"
                tabAplikasi.Attributes.Add("class", "tab_notselected")
                tabAsset.Attributes.Add("class", "tab_selected")
                tabFinancial.Attributes.Add("class", "tab_notselected")
                tabDemografi.Attributes.Add("class", "tab_notselected")
            Case "Demografi"
                tabAplikasi.Attributes.Add("class", "tab_notselected")
                tabAsset.Attributes.Add("class", "tab_notselected")
                tabFinancial.Attributes.Add("class", "tab_notselected")
                tabDemografi.Attributes.Add("class", "tab_selected")
            Case "Financial"
                tabAplikasi.Attributes.Add("class", "tab_notselected")
                tabAsset.Attributes.Add("class", "tab_notselected")
                tabFinancial.Attributes.Add("class", "tab_selected")
                tabDemografi.Attributes.Add("class", "tab_notselected")
            Case Else
                tabAplikasi.Attributes.Add("class", "tab_notselected")
                tabAsset.Attributes.Add("class", "tab_notselected")
                tabFinancial.Attributes.Add("class", "tab_notselected")
                tabDemografi.Attributes.Add("class", "tab_notselected")
        End Select
    End Sub
End Class