﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucBatchVoucher.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.ucBatchVoucher" %>
<%--<div id="scriptJava" runat="server">
</div>
<script language="JavaScript" type="text/javascript" >
    var hidBankAccount;
    function WOPChange(pCmbOfPayment, pBankAccount, pHidDetail, itemArray) {
        hidBankAccount = eval('document.forms[0].' + pHidDetail);
        var a = eval('document.forms[0].' + pCmbOfPayment).options[eval('document.forms[0].' + pCmbOfPayment).selectedIndex].value;
        if (a == 'CP') {
            for (i = eval('document.forms[0].' + pBankAccount).options.length; i >= 0; i--) {
                eval('document.forms[0].' + pBankAccount).options[i] = null;
            }
            eval('document.forms[0].' + pBankAccount).disabled = true;
            hidBankAccount.value = '';
        }
        else {
            eval('document.forms[0].' + pBankAccount).disabled = false;
            var i, j;
            for (i = eval('document.forms[0].' + pBankAccount).options.length; i >= 0; i--) {
                eval('document.forms[0].' + pBankAccount).options[i] = null
            };
            j = 0;
            eval('document.forms[0].' + pBankAccount).options[j++] = new Option('Select One', '0');
            //		eval('document.forms[0].' + pBankAccount).options[j++].value = '0';
            var a;
            if (itemArray != null) {
                for (i = 0; i < itemArray.length; i++) {
                    eval('document.forms[0].' + pBankAccount).options[j++] = new Option(itemArray[i][0], itemArray[i][1]);
                };
                eval('document.forms[0].' + pBankAccount).selected = true;
            };
        };
    }

    function setCboDetlVal(l) {
        hidBankAccount.value = l;
    }


</script>--%>
<input id="hdnBankAccount" type="hidden" name="hdnBankAccount" runat="server" />
<input id="hdnBankAccountName" type="hidden" name="hdnBankAccountName" runat="server" />
<input id="HidDetail" name="HidDetail" type="hidden" runat="server" />
<div class="form_box">
    <div class="form_single">
        <h4>
        DETAIL PEMBAYARAN
        </h4>
    </div>
</div>
                
<div class="form_box">
    <div class="form_single">
        <label>Cara Pembayaran
        </label>
        <asp:DropDownList ID="cmbWayOfPayment" runat="server" AutoPostBack ="true" >
            </asp:DropDownList>
            <asp:Label ID="lblWayOfPayment" runat="server"></asp:Label>
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" ControlToValidate="cmbWayOfPayment"
                Visible="True" Enabled="True" Display="Dynamic" ErrorMessage="Harap pilih Cara Pembayaran"
                InitialValue="0" CssClass ="validator_general"></asp:RequiredFieldValidator>
    </div>
</div>
<div class="form_box">
    <div class="form_single">
        <label>Rekening Bank
        </label>
         <asp:DropDownList ID="cmbBankAccount"
                runat="server" >
            </asp:DropDownList>
            <asp:Label ID="lblBankAccountID" runat="server"></asp:Label>
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" ControlToValidate="cmbBankAccount"
                Visible="True" Enabled="True" Display="Dynamic" ErrorMessage="Harap Pilih Rekening Bank"
                InitialValue="0" CssClass ="validator_general"></asp:RequiredFieldValidator>
    </div>
</div>
            
<div class="form_box">
    <div class="form_single">
        <label>Catatan
        </label>
        <asp:TextBox ID="txtNotes" runat="server"  TextMode="MultiLine" CssClass ="multiline_textbox"></asp:TextBox>
    </div>
</div>

<%--<asp:Panel ID="pnltopi" runat="server" Visible="true">
    <table class="tablegrid" cellspacing="1" cellpadding="3" width="100%" align="center"
        border="0">
        <tr>
            <td class="tdjudul" colspan="4">
                DETAIL PEMBAYARAN
            </td>
        </tr>
    </table>
</asp:Panel>
<table class="tablegrid" cellspacing="1" cellpadding="3" width="100%" align="center"
    border="0">
    <tr>
        <td class="tdgenap" width="20%">
            Cara Pembayaran
        </td>
        <td class="tdganjil" width="80%">
            <asp:DropDownList ID="cmbWayOfPayment" runat="server"  onchange="<%#WOPonChange()%>">
            </asp:DropDownList>
            <asp:Label ID="lblWayOfPayment" runat="server"></asp:Label>
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" ControlToValidate="cmbWayOfPayment"
                Visible="True" Enabled="True" Display="Dynamic" ErrorMessage="Harap pilih Cara Pembayaran"
                InitialValue="0" CssClass ="validator_general"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td class="tdgenap" width="20%">
            Rekening Bank
        </td>
        <td class="tdganjil" width="80%">
            <asp:DropDownList ID="cmbBankAccount" onchange="setCboDetlVal(this.options[this.selectedIndex].value);"
                runat="server" >
            </asp:DropDownList>
            <asp:Label ID="lblBankAccountID" runat="server"></asp:Label>
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" ControlToValidate="cmbBankAccount"
                Visible="True" Enabled="True" Display="Dynamic" ErrorMessage="Harap Pilih Rekening Bank"
                InitialValue="0" CssClass ="validator_general"></asp:RequiredFieldValidator>
    </tr>
</table>
<table class="tablegrid" cellspacing="1" cellpadding="3" width="100%" align="center"
    border="0">
    <tr>
        <td class="tdgenap" width="20%">
            Catatan
        </td>
        <td class="tdganjil" width="80%">
            <asp:TextBox ID="txtNotes" runat="server"  TextMode="MultiLine"
                Width="100%"></asp:TextBox>
        </td>
    </tr>
</table>--%>
