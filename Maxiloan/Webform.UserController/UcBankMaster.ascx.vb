﻿Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.Controller

Public Class UcBankMaster
    Inherits ControlBased

#Region "Bank Account"
    Public Property BankID() As String
        Get
            Return CType(viewstate("BankID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("BankID") = Value
        End Set
    End Property

    Public Property BankName() As String
        Get
            Return CType(viewstate("BankName"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("BankName") = Value
        End Set
    End Property

    Public Property BankBranch() As String
        Get
            Return CType(viewstate("BankBranch"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("BankBranch") = Value
        End Set
    End Property

    Public Property FillRequired() As Boolean
        Get
            Return RequiredFieldValidator4.Enabled
        End Get
        Set(ByVal Value As Boolean)
            RequiredFieldValidator4.Enabled = Value
        End Set
    End Property
#End Region

#Region " Private Const "
    Private m_controller As New DataUserControlController
    Private dttBankName As New DataTable
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load        
        If Not IsPostBack Then
            cboBankName.Items.Clear()
            Dim DtBankName As DataTable
            DtBankName = CType(Me.Cache.Item(CACHE_BANK_MASTER), DataTable)
            If DtBankName Is Nothing Then
                Dim dtBankNameCache As New DataTable
                dtBankNameCache = m_controller.GetBankName(GetConnectionString)
                Me.Cache.Insert(CACHE_BANK_MASTER, dtBankNameCache, Nothing, DateTime.Now.AddHours(10), TimeSpan.Zero)
                DtBankName = CType(Me.Cache.Item(CACHE_BANK_MASTER), DataTable)
            End If

            cboBankName.DataValueField = "ID"
            cboBankName.DataTextField = "Name"
            cboBankName.DataSource = DtBankName
            cboBankName.DataBind()
            cboBankName.Items.Insert(0, "ALL")
            cboBankName.Items(0).Value = "ALL"
        End If

        Me.BankID = cboBankName.SelectedItem.Value.Trim
        Me.BankName = cboBankName.SelectedItem.Text.Trim
    End Sub

    Public Sub BindData()
        If Me.BankID = "" Then
            cboBankName.SelectedIndex = 0
        End If
    End Sub

End Class
