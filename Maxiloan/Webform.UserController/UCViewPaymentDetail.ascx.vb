﻿Public Class UCViewPaymentDetail
    Inherits System.Web.UI.UserControl

#Region "Property"
#Region "PaymentDetail"

    Public Property ReceivedFrom() As String
        Get
            Return CType(lblReceivedFrom.Text.Trim, String)
        End Get
        Set(ByVal Value As String)
            lblReceivedFrom.Text = Value
        End Set
    End Property

    Public Property ReferenceNo() As String
        Get
            Return CType(lblReferenceNo.Text.Trim, String)
        End Get
        Set(ByVal Value As String)
            lblReferenceNo.Text = Value
        End Set
    End Property

    Public Property WayOfPayment() As String
        Get
            Return CType(lblWayOfPayment.Text, String)
        End Get
        Set(ByVal Value As String)
            lblWayOfPayment.Text = Value
        End Set
    End Property

    Public Property BankAccount() As String
        Get
            Return CType(lblBankAccount.Text, String)
        End Get
        Set(ByVal Value As String)
            lblBankAccount.Text = Value
        End Set
    End Property

    Public Property ValueDate() As String
        Get
            Return CType(lblValueDate.Text, String)
        End Get
        Set(ByVal Value As String)
            lblValueDate.Text = Value
        End Set
    End Property

    Public Property AmountReceive() As Double
        Get
            Return CType(lblAmountReceive.Text.Trim, Double)
        End Get
        Set(ByVal Value As Double)
            lblAmountReceive.Text = FormatNumber(Value, 2)
        End Set
    End Property

    Public Property Notes() As String
        Get
            Return CType(lblNotes.Text.Trim, String)
        End Get
        Set(ByVal Value As String)
            lblNotes.Text = Value.Trim
        End Set
    End Property

    Public Property BayarDI() As String
        Get
            Return CType(lblBayarDi.Text.Trim, String)
        End Get
        Set(ByVal Value As String)
            lblBayarDi.Text = Value.Trim
        End Set
    End Property

    Public Property CollectorName() As String
        Get
            Return CType(lblCollector.Text.Trim, String)
        End Get
        Set(ByVal Value As String)
            lblCollector.Text = Value.Trim
        End Set
    End Property
#End Region
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub
End Class