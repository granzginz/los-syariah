﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucInstallmentSchedule.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.ucInstallmentSchedule" %>
<div class="form_box_header">
    <div class="form_single">
        <div class="grid_wrapper_ns">
            <asp:DataGrid ID="dtgAngsuran" runat="server" AutoGenerateColumns="false" BorderWidth="0"
                OnSortCommand="SortGrid_angsuran" BorderStyle="None" CssClass="grid_general"
                DataKeyField="Applicationid" AllowSorting="True">
                <HeaderStyle CssClass="th" />
                <ItemStyle CssClass="item_grid" />
                <Columns>
                    <asp:BoundColumn DataField="InsSeqNo" HeaderText="ANGSURAN KE" />
                    <asp:BoundColumn DataField="DueDate" HeaderText="JATUH TEMPO" DataFormatString="{0:dd/MM/yyyy}" />
                    <asp:BoundColumn DataField="InstallmentAmount" HeaderText="ANGSURAN/BLN" DataFormatString="{0:N0}" />
                    <asp:BoundColumn DataField="PaidAmount" HeaderText="PARSIAL" DataFormatString="{0:N0}" />
                    <asp:BoundColumn DataField="SaldoAngsuranKe" HeaderText="SISA" DataFormatString="{0:N0}" />
                    <asp:BoundColumn DataField="LCAmount" HeaderText="DENDA" DataFormatString="{0:N0}" />
                </Columns>
            </asp:DataGrid>
            <div class="button_gridnavigation">
                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../Images/grid_navbutton01.png"
                    CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="First">
                </asp:ImageButton>
                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../Images/grid_navbutton02.png"
                    CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Prev">
                </asp:ImageButton>
                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../Images/grid_navbutton03.png"
                    CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Next">
                </asp:ImageButton>
                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../Images/grid_navbutton04.png"
                    CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Last">
                </asp:ImageButton>
                Page&nbsp;
                <asp:TextBox ID="txtPage" runat="server" CssClass ="smaller_text">1</asp:TextBox>
                <asp:Button ID="imgbtnPageNumb" runat="server" CssClass="small buttongo blue" Text="Go"
                    EnableViewState="False"></asp:Button>
                <asp:RangeValidator ID="rgvGo" runat="server" ErrorMessage="No Halaman Salah" ControlToValidate="txtPage"
                    Type="Integer" MaximumValue="999999999" MinimumValue="1" CssClass="validator_general"
                    Display="Dynamic"></asp:RangeValidator>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" Display="Dynamic"
                    ErrorMessage="No Halaman Salah" ControlToValidate="txtPage" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
            <div class="label_gridnavigation">
                <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                <asp:Label ID="lblrecord" runat="server"></asp:Label>&nbsp;record(s)
            </div>
        </div>
    </div>
</div>
