﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class ucHasilSurveyViewTabPhone

    '''<summary>
    '''jlookupContent control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents jlookupContent As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''hdnAppID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnAppID As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''pnlTelepon control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlTelepon As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''lblTelepon1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblTelepon1 As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''LblAreaPhoneHome control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents LblAreaPhoneHome As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblStrip1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblStrip1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''LblPhoneHome control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents LblPhoneHome As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''LblAreaPhoneOffice control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents LblAreaPhoneOffice As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblStrip2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblStrip2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''LblPhoneOffice control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents LblPhoneOffice As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''LblHandphone control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents LblHandphone As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''LblEmergencyPhone control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents LblEmergencyPhone As Global.System.Web.UI.WebControls.Label
End Class
