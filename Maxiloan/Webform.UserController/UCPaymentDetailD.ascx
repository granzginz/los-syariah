﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UCPaymentDetailD.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.UCPaymentDetailD" %>
<%@ Register TagPrefix="uc1" TagName="ucBGNumber" Src="ucBGNumber.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" TagPrefix="asp" Namespace="AjaxControlToolkit" %>
<%@ Register Src="UcBankAccount.ascx" TagName="UcBankAccount" TagPrefix="uc2" %>
<%@ Register src="ucNumberFormat.ascx" tagname="ucNumberFormat" tagprefix="uc1" %>
<%@ Register Src="ucDateCE.ascx" TagName="ucDateCE" TagPrefix="uc1" %>

<%--<div id="scriptJava" runat="server">
</div>
<script language="JavaScript" type="text/javascript">
    var hidBankAccount;

    function WOPChange(pCmbOfPayment, pBankAccount, pHidDetail, pAmountReceive, itemArray) {
        hidBankAccount = eval('document.forms[0].' + pHidDetail);
        var a = eval('document.forms[0].' + pCmbOfPayment).options[eval('document.forms[0].' + pCmbOfPayment).selectedIndex].value;
        var oPnlJenisTransfer = document.getElementById('<%= pnlJenisTransfer.ClientID %>');

        if (a == 'CP') {
            for (i = eval('document.forms[0].' + pBankAccount).options.length; i >= 0; i--) {
                eval('document.forms[0].' + pBankAccount).options[i] = null;
            }
            eval('document.forms[0].' + pAmountReceive).disabled = true;
            eval('document.forms[0].' + pAmountReceive).value = 0;
            eval('document.forms[0].' + pBankAccount).disabled = true;
            hidBankAccount.value = '';
        }
        else {
            eval('document.forms[0].' + pAmountReceive).disabled = false;
            eval('document.forms[0].' + pBankAccount).disabled = false;
            var i, j;
            for (i = eval('document.forms[0].' + pBankAccount).options.length; i >= 0; i--) {
                eval('document.forms[0].' + pBankAccount).options[i] = null
            };
            j = 0;
            eval('document.forms[0].' + pBankAccount).options[j++] = new Option('Select One', '0');
            //		eval('document.forms[0].' + pBankAccount).options[j++].value = '0';
            var a;
            if (itemArray != null) {
                for (i = 0; i < itemArray.length; i++) {
                    eval('document.forms[0].' + pBankAccount).options[j++] = new Option(itemArray[i][0], itemArray[i][1]);
                };
                eval('document.forms[0].' + pBankAccount).selected = true;
            };
        };

        if (a !== 'GT') {
            oPnlJenisTransfer.style.display = 'none';
        } else {
            oPnlJenisTransfer.style.display = 'inherit';
        }
    }

    function setCboDetlVal(l) {
        hidBankAccount.value = l;
    }


</script>--%>
<%--<input id="hdnBankAccount" type="hidden" name="hdnBankAccount" runat="server" />
<input id="hdnBankAccountName" type="hidden" name="hdnBankAccountName" runat="server" />
<input id="HidDetail" name="HidDetail" type="hidden" runat="server" />--%>
<div class="form_box_usercontrol_header">
    <div class="form_left_usercontrol">
        <h4>
            DETAIL PEMBAYARAN
        </h4>
    </div>
</div>
<div class="form_box_usercontrol">
    <div class="form_left_usercontrol">
        <label>
            Nama Penerima
        </label>
        <asp:TextBox ID="txtReceivedFrom" runat="server" CssClass="long_text" Width="180px"></asp:TextBox><asp:RequiredFieldValidator
            ID="rfvText" runat="server" ControlToValidate="txtReceivedFrom" Visible="True"
            Enabled="True" Display="Dynamic" ErrorMessage="Harap isi Nama Penerima" CssClass="validator_general"></asp:RequiredFieldValidator>
    </div>
</div>
<div class="form_box_usercontrol">
    <div class="form_left_usercontrol">
        <label>
            Cara Pembayaran
        </label>
        <asp:DropDownList ID="cmbWayOfPayment" runat="server" AutoPostBack ="true" >
        </asp:DropDownList>
        <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" ControlToValidate="cmbWayOfPayment"
            Visible="True" Enabled="True" Display="Dynamic" ErrorMessage="Harap Pilih Cara Pembayaran"
            InitialValue="0" CssClass="validator_general"></asp:RequiredFieldValidator>
    </div>
</div>
<asp:Panel runat="server" ID="pnlJenisTransfer" visible="false">
    <div class="form_box_usercontrol">
        <div class="form_left_usercontrol">
            <label class="label_general">
                Jenis Transfer
            </label>
            <asp:RadioButtonList runat="server" ID="rboJenisTransfer" RepeatDirection="Horizontal"
                CssClass="opt_single">
                <asp:ListItem Selected="True" Text="SKN" Value="SKN"></asp:ListItem>
                <asp:ListItem Text="RTGS" Value="RTGS"></asp:ListItem>
            </asp:RadioButtonList>
        </div>
    </div>
    <div class="form_box_usercontrol_header">
        <div class="form_left_usercontrol">
            <h4>
                BENEFICIARY BANK ACCOUNT
            </h4>
        </div>
    </div>
    <uc2:UcBankAccount ID="UcBankAccount1" runat="server" />
</asp:Panel>
<div class="form_box_usercontrol_header">
    <div class="form_left_usercontrol">
        <h4>
            TRANSAKSI PEMBAYARAN
        </h4>
    </div>
</div>
<div class="form_box_usercontrol">
    <div class="form_left_usercontrol">
        <label>
            No Bukti Kas Keluar
        </label>
        <asp:TextBox ID="txtReferenceNo" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator ID="rfvRef" Visible="false" runat="server" ControlToValidate="txtReferenceNo"
            Enabled="True" Display="Dynamic" ErrorMessage="Harap isi No Bukti Kas Keluar"
            CssClass="validator_general"></asp:RequiredFieldValidator>
    </div>
</div>
<div class="form_box_usercontrol">
    <div class="form_left_usercontrol">
        <label>
            Tanggal Valuta
        </label>        
        <uc1:ucdatece id="txtValueDate" runat="server"></uc1:ucdatece>             
    </div>
</div>
<div class="form_box_usercontrol">
    <div class="form_left_usercontrol">
        <label>
            Jumlah</label>
        <uc1:ucNumberFormat ID="txtAmountReceive" runat="server" />        
    </div>
</div>
<div class="form_box_usercontrol">
    <div class="form_left_usercontrol">
        <label>
            Dari Rekening
        </label>
        <asp:DropDownList ID="cmbBankAccount" 
            runat="server">
        </asp:DropDownList>
        <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" ControlToValidate="cmbBankAccount"
            Visible="True" Enabled="True" Display="Dynamic" ErrorMessage="Harap pilih Rekening Bank"
            InitialValue="0" CssClass="validator_general"></asp:RequiredFieldValidator>
    </div>
</div>
<div class="form_box_usercontrol">
    <div class="form_left_usercontrol">
        <label class="label_general">
            Catatan</label>
        <asp:TextBox ID="txtNotes" runat="server" CssClass="multiline_textbox_uc" TextMode="MultiLine"></asp:TextBox>
    </div>
</div>
