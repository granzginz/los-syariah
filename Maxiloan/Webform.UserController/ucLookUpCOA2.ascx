﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucLookUpCOA2.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.ucLookUpCOA2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%--<style type="text/css">
    .wp
    {
        background-color: #fff;
        width: 700px;        
        border: 1px solid #ccc;
        float: left;
    }
    
    .wp h4
    {
        margin: 0;
        color: #000;
        text-align: left;
    }
    
    .wp .gridwp
    {
        max-height: 300px;
        overflow: auto;
    }
    
    .wpbg
    {
        background-color: #000;
        filter: alpha(opacity=70);
        opacity: 0.7;
    }
    
    .wpbtnexit
    {
        position: absolute;
        float: right;
        top: 0;
        right: 0;
    }
</style>--%>
<asp:UpdatePanel runat="server" ID="updatePanel1">
    <ContentTemplate>
        <asp:Button runat="server" ID="btnLookup" Text="btnLookup" Style="display: none;" />
        <asp:ModalPopupExtender ID="ModalPopupExtender" runat="server" TargetControlID="btnLookup"
            PopupControlID="Panel1" BackgroundCssClass="wpbg" CancelControlID="btnExit">
        </asp:ModalPopupExtender>
        <asp:Panel ID="Panel1" runat="server">
            <div class="wp">
                <asp:Label ID="lblMessage" Visible="false" runat="server"></asp:Label>
                <asp:ImageButton runat="server" ID="btnExit" ImageUrl="../Images/exit_lookup00.png"
                    CssClass="wpbtnexit" />
                <div class="form_box_title">
                    <div class="title_strip">
                    </div>
                    <div class="form_single">
                        <h4>
                            REGISTRASI AKUN (CHART OF ACCOUNT)
                        </h4>
                    </div>
                </div>
                <asp:Panel ID="pnlList" runat="server">
                    <asp:Panel ID="pnlSubList" runat="server" Visible="false">
                    </asp:Panel>
                    <div class="form_box_header">
                        <div class="form_single">
                            <div class="grid_wrapper_ns">
                                <asp:DataGrid ID="DtgAsset" runat="server" AllowSorting="False" AutoGenerateColumns="false"
                                    DataKeyField="GLAccountNo" CssClass="grid_general">
                                    <HeaderStyle CssClass="th" />
                                    <ItemStyle CssClass="item_grid" />
                                    <Columns>
                                        <asp:ButtonColumn Text="Select" CommandName="Select" ButtonType="LinkButton"></asp:ButtonColumn>
                                        <asp:BoundColumn DataField="GLAccountNo" HeaderText="Account No" />
                                        <asp:BoundColumn DataField="GLAccountName" HeaderText="Account Name" />
                                        <asp:BoundColumn DataField="BranchFullName" HeaderText="Branch" />
                                        <asp:TemplateColumn HeaderText="Account Type">
                                            <ItemTemplate>
                                                <%#AccountTypeToValue(Eval("AccountType")) %>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="P/D">
                                            <ItemTemplate>
                                                <%# SubAccountToValue(Eval("SubAccount"))%>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:BoundColumn DataField="ParentAccount" HeaderText="Parent Acct" />
                                        <asp:BoundColumn DataField="CurrencyId" HeaderText="Currency" />
                                         <asp:BoundColumn DataField="BranchId" HeaderText="BranchId" />
                                    </Columns>
                                </asp:DataGrid>
                                <div class="button_gridnavigation">
                                    <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../Images/grid_navbutton01.png"
                                        CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                                    </asp:ImageButton>
                                    <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../Images/grid_navbutton02.png"
                                        CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                                    </asp:ImageButton>
                                    <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../Images/grid_navbutton03.png"
                                        CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                                    </asp:ImageButton>
                                    <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../Images/grid_navbutton04.png"
                                        CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                                    </asp:ImageButton>
                                    <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                                    <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                                        EnableViewState="False"></asp:Button>
                                    <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" MinimumValue="1"
                                        ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"
                                        Display="Dynamic"></asp:RangeValidator>
                                    <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                                        ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general">
                                    </asp:RequiredFieldValidator>
                                </div>
                                <div class="label_gridnavigation">
                                    <asp:Label ID="lblPage" runat="server"></asp:Label>of
                                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                                    <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
            </div>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
 
