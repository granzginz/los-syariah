﻿Public Class ucYear
    Inherits ControlBased

    Public Property Text As String
        Get
            Return txtYear.Text.Trim
        End Get
        Set(ByVal value As String)
            txtYear.Text = value
        End Set
    End Property

End Class