﻿Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.Controller

Public Class UcBranchCollection
    Inherits ControlBased

#Region "Branch Info"
    Public Property BranchID() As String
        Get
            Return CType(cmbBranchID.SelectedItem.Value.Trim, String)
        End Get
        Set(ByVal Value As String)
            cmbBranchID.SelectedIndex = cmbBranchID.Items.IndexOf(cmbBranchID.Items.FindByText(Value))
        End Set

    End Property

    Public ReadOnly Property BranchName() As String
        Get
            Return CType(cmbBranchID.SelectedItem.Text.Trim, String)
        End Get
    End Property
#End Region

#Region " Private Const "
    Private m_controller As New DataUserControlController    
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        LoadData
    End Sub

    Public Sub LoadData()
        If Not IsPostBack Then
            Dim DtBranchName As DataTable
            
            DtBranchName = m_controller.GetBranchCollectionName(GetConnectionString, Me.GroubDbID)            

            cmbBranchID.DataValueField = "ID"
            cmbBranchID.DataTextField = "Name"

            cmbBranchID.DataSource = DtBranchName
            cmbBranchID.DataBind()
            cmbBranchID.Items.Insert(0, "Select One")
            cmbBranchID.Items(0).Value = "0"
            If cmbBranchID.Items.Count > 1 Then
                cmbBranchID.SelectedIndex = 1
            Else
                cmbBranchID.SelectedIndex = 0
            End If
        End If
    End Sub

    Public Sub BindBranch()
        Dim DtBranchName As DataTable
        DtBranchName = CType(Me.Cache.Item(CACHE_BRANCH_COLLECTION & Me.GroubDbID), DataTable)
        If DtBranchName Is Nothing Then
            Dim DtBranchNameCache As New DataTable
            DtBranchNameCache = m_controller.GetBranchCollectionName(GetConnectionString, Me.GroubDbID)
            Me.Cache.Insert(CACHE_BRANCH_COLLECTION & Me.GroubDbID, DtBranchNameCache, Nothing, DateTime.Now.AddHours(DEFAULT_TIME_DURATION_CACHE_HOUR), TimeSpan.Zero)
            DtBranchName = CType(Me.Cache.Item(CACHE_BRANCH_COLLECTION & Me.GroubDbID), DataTable)
        End If

        cmbBranchID.DataValueField = "ID"
        cmbBranchID.DataTextField = "Name"

        cmbBranchID.DataSource = DtBranchName
        cmbBranchID.DataBind()
        cmbBranchID.Items.Insert(0, "Select One")
        cmbBranchID.Items(0).Value = "0"
        If cmbBranchID.Items.Count > 1 Then
            cmbBranchID.SelectedIndex = 1
        Else
            cmbBranchID.SelectedIndex = 0
        End If
    End Sub

End Class