﻿Option Strict On
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.General.CommonVariableHelper
Public Class ucFindEmpployee2
    Inherits ControlBased

    Dim m_Employee As New EmployeeController
#Region "Variable & Const"
    Private m_controller As New LookUpSupplierController
    Private currentPage As Integer = DEFAULT_CURRENT_PAGE
    Private pageSize As Int16 = 5
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region
#Region "Properties"
    Public Property Where() As String
        Get
            Return CType(ViewState("LookupSupplierWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("LookupSupplierWhere") = Value
        End Set
    End Property

    Public Property BranchID() As String
        Get
            Return CType(txtBranchID.Text.Trim, String)
        End Get
        Set(ByVal Value As String)
            txtBranchID.Text = Value
        End Set
    End Property

    Public Property EmployeeName() As String
        Get
            Return CType(txtEmployeeName.Text.Trim, String)
        End Get
        Set(ByVal Value As String)
            txtEmployeeName.Text = Value
        End Set
    End Property
    Public Property EmployeeID() As String
        Get
            Return txtEmployeeID.Text
        End Get
        Set(ByVal Value As String)
            txtEmployeeID.Text = Value
        End Set
    End Property
    Public Property Text() As String
        Get
            Return CType(viewstate("Text"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Text") = Value
        End Set
    End Property

    Public Property Sort As String
        Get
            Return CType(ViewState("LookupSupplierSort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("LookupSupplierSort") = Value
        End Set
    End Property
#End Region
#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            lblMessage.Text = "Data not found ....."
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblTotRec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        bindData()
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGoPage.Click
        If IsNumeric(txtGoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtGoPage.Text, Int32)
                bindData()
            End If
        End If
    End Sub
#End Region
    Public Sub bindData()
        Dim dttEntity As DataTable = Nothing
        Dim oCustomClass As New Maxiloan.Parameter.Employee

        pnlEmployeeList.Visible = True
        ' lblMessage.Text = ""

        oCustomClass.PageSize = pageSize
        oCustomClass.WhereCond = Me.Where
        oCustomClass.CurrentPage = currentPage
        oCustomClass.SortBy = "EmployeeName"
        oCustomClass.strConnection = GetConnectionString()
        oCustomClass.BranchId = Replace(Me.sesBranchId, "'", "")

        oCustomClass = m_Employee.ListEmployee(oCustomClass)

        If Not oCustomClass Is Nothing Then
            dttEntity = oCustomClass.ListData
            recordCount = oCustomClass.TotalRecord
        Else
            recordCount = 0
        End If

        dtg.DataSource = dttEntity.DefaultView
        dtg.CurrentPageIndex = 0
        dtg.DataBind()

        PagingFooter()

        Me.ModalPopupExtender1.Show()
    End Sub
#Region "SortGrid"
    Protected Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        bindData()
    End Sub
#End Region

#Region "imbReset"
    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        cboSearchBy.SelectedIndex = 0
        txtSearchBy.Text = ""
        Me.Where = txtSearchBy.Text
        pnlEmployeeList.Visible = False
        bindData()
    End Sub
#End Region
#Region "imbSearch"
    Private Sub imbSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        If txtSearchBy.Text.Trim <> "" Then
            Me.Where = " EmployeePositionID = 'CH' AND " & cboSearchBy.SelectedItem.Value & " LIKE '%" & txtSearchBy.Text & "%'"
        Else
            Me.Where = " EmployeePositionID = 'CH'"
        End If
        'pnlSupplierList.Visible = True
        bindData()

    End Sub
#End Region

    Protected Sub imbExit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Me.ModalPopupExtender1.Hide()
    End Sub

    Private Sub dtg_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtg.SelectedIndexChanged
        Dim i As Integer = dtg.SelectedIndex
        txtEmployeeID.Text = CType(dtg.Items(i).FindControl("lblEmployeeID"), Label).Text
        txtBranchID.Text = CType(dtg.Items(i).FindControl("lblBranchID"), Label).Text
        txtEmployeeName.Text = CType(dtg.Items(i).FindControl("lblEmployeeName"), Label).Text
        Me.ModalPopupExtender1.Hide()
    End Sub
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        pnlEmployeeList.Visible = False
    End Sub
End Class