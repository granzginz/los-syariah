﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucViewApplicationMK.ascx.vb" Inherits="Maxiloan.Webform.UserController.ucViewApplicationMK" %>
 
    <asp:Panel runat="server" ID="PanelHeader">

        <h3> VIEW - PROSES KREDIT</h3>

     <div  >
            <label> Nama Customer</label>
            <asp:LinkButton ID="lblCustomerID" runat="server" EnableViewState="False"></asp:LinkButton>
        </div>
         <div class="form_box">
            <label>
                No Kontrak</label>
            <span id="lblAgrNumber">
                <asp:Label ID="lblAgreementNo" runat="server" EnableViewState="False"></asp:Label>
            </span>
        </div>
    </asp:Panel>

    <asp:Panel runat="server" ID="PanelDataEntriAPK">
        <div class="form_box_usercontrol">
        <div class="form_left_usercontrol">
            <label>
                Tanggal APK</label>
            <asp:Label runat="server" ID="lblTanggalAPK"></asp:Label>
        </div>
         </div>
         <div class="form_box_usercontrol">
        <div class="form_left_usercontrol">
            <label>
                Data ID</label>                        
            <asp:HyperLink ID="hyNoAPK" runat="server"></asp:HyperLink>
        </div>
        </div>

   <div class="form_box_usercontrol">
        <div class="form_left_usercontrol">
            <label>
                Nama Produk</label>
            <asp:Label runat="server" ID="lblNamaProduct"></asp:Label>
        </div></div>
         <div class="form_box_usercontrol">
        <div class="form_left_usercontrol">
            <label>
                Paket Produk</label>
            <asp:Label runat="server" ID="lblProductPaket"></asp:Label>
        </div></div>
    </asp:Panel>


    <asp:Panel runat="server" ID="PanelProsesKredit">
        <h4>
            PROSES KREDIT</h4>
        <div class="form_box_usercontrol">
            <label>
                Tanggal Aplikasi Baru</label>
            <asp:Label ID="lblApplicationDaTE" runat="server" EnableViewState="False"></asp:Label>
        </div>
        <div class="form_box_usercontrol">
            <label>
                Sumber Aplikasi
            </label>
            <asp:Label ID="lblSourceApplication" runat="server" EnableViewState="False"></asp:Label>
        </div>
         <div class="form_box_usercontrol">
            <label>
                Tanggal Permintaan Survey</label>
            <asp:Label ID="lblReqSurveyDate" runat="server" EnableViewState="False"></asp:Label>
        </div>
        <div class="form_box_usercontrol">
            <label>
                Credit Scoring</label><asp:Label ID="lblCreditBy" runat="server" EnableViewState="False"></asp:Label></div>
        <div class="form_box_usercontrol">
            <label>
                Tanggal Survey</label>
            <asp:Label ID="lblSurveyDate" runat="server" EnableViewState="False"></asp:Label>
        </div>
         <div class="form_box_usercontrol">
            <label>
                Tanggal CAR</label>
            <asp:Label ID="lblRCADate" runat="server" EnableViewState="False"></asp:Label>
        </div>
        <div class="form_box_usercontrol">
            <label>
                Tanggal Approval
            </label>
            <asp:Label ID="lblApprovalDate" runat="server" EnableViewState="False"></asp:Label>
            <label>
                No Approval
            </label>
            <asp:Label ID="lblApprovalNo" runat="server" EnableViewState="False"></asp:Label>
        </div>
        <div class="form_box_usercontrol">
            <label>
                Tanggal Kontrak
            </label>
            <asp:Label ID="lblAgreementDate" runat="server" EnableViewState="False"></asp:Label>
            <label>
                Tanggal Purchase Order
            </label>
            <asp:Label ID="lblPODate" runat="server" EnableViewState="False"></asp:Label>
        </div>
        <div class="form_box_usercontrol">
            <label>
                Tanggal Verifikasi Pra Pencairan
            </label>
            <asp:Label ID="lblDODate" runat="server" EnableViewState="False"></asp:Label>
            <label>
                Tanggal Invoice Supplier
            </label>
            <asp:Label ID="lblSupplierDate" runat="server" EnableViewState="False"></asp:Label>
        </div>
        <div class="form_box_usercontrol">
            <label>
                Tanggal Loan Activation
            </label>
            <asp:Label ID="lblGoLiveDate" runat="server" EnableViewState="False"></asp:Label>
            <label>
                Surveyor
            </label>
            <asp:Label ID="lblSurveyor" runat="server" EnableViewState="False"></asp:Label>
        </div>
       <div class="form_box_usercontrol">
            <label>
                CMO
            </label>
            <asp:Label ID="lblAO" runat="server" EnableViewState="False"></asp:Label>
            <label>
                Credit Analyst
            </label>
            <asp:Label ID="lblCA" runat="server" EnableViewState="False"></asp:Label>
        </div>
    </asp:Panel>
    <asp:Panel runat="server" ID="PanelBiaya">
        <h4>
            BIAYA-BIAYA
        </h4>
        <div>
            <label>
                Biaya Admin
            </label>
            <asp:Label ID="lblAdminFee" runat="server" EnableViewState="False"></asp:Label>
            <label>
                Biaya Survey
            </label>
            <asp:Label ID="lblSurveyFee" runat="server" EnableViewState="False"></asp:Label>
        </div>
        <div>
            <label>
                Biaya Fiducia
            </label>
            <asp:Label ID="lblFiduciaFee" runat="server" EnableViewState="False"></asp:Label>
            <label>
                Biaya BBN
            </label>
            <asp:Label ID="lblBBNFee" runat="server" EnableViewState="False"></asp:Label>
        </div>
        <div>
            <label>
                Biaya Provisi
            </label>
            <asp:Label ID="lblProvisionFee" runat="server" EnableViewState="False"></asp:Label>
            <label>
                Biaya Lainnya
            </label>
            <asp:Label ID="lblOtherFee" runat="server" EnableViewState="False"></asp:Label>
        </div>
        <div>
            <label>
                Biaya Notaris
            </label>
            <asp:Label ID="lblNotaryFee" runat="server" EnableViewState="False"></asp:Label>
        </div>
    </asp:Panel>
    <asp:Panel runat="server" ID="PanelDataLainnya">
        <h4>
            DATA LAINNYA
        </h4>
        <div>
            <label>
                Ex Konversi
            </label>
            <asp:Label ID="lblExConversion" runat="server" EnableViewState="False"></asp:Label>
            <label>
                Periode Floating
            </label>
            <asp:Label ID="lblFloatingPeriod" runat="server" EnableViewState="False"></asp:Label>
        </div>
        <div>
            <label>
                Penjamin
            </label>
            <asp:Label ID="lblAvalist" runat="server" EnableViewState="False"></asp:Label>
            <label>
                Tgl Review berkut Floating</label>
            <asp:Label ID="lblFloatingReviewDate" runat="server" EnableViewState="False"></asp:Label>
        </div>
        <div>
            <label>
                Sindikasi
            </label>
            <asp:Label ID="lblSyndication" runat="server" EnableViewState="False"></asp:Label>
            <label>
                Adjustment Floating Terakhir
            </label>
            <asp:Label ID="lblLastFloatAdj" runat="server" EnableViewState="False"></asp:Label>
        </div>
        <div>
            <label>
                Tgl Buy Back Guarantee
            </label>
            <asp:Label ID="lblBuyBackValidDate" runat="server" EnableViewState="False"></asp:Label>
            <label>
                Eksekusi Floating Terakhir
            </label>
            <asp:Label ID="lblLastFloatEx" runat="server" EnableViewState="False"></asp:Label>
        </div>
        <div>
            <label>
                Transaksi Non Standard
            </label>
            <label>
                <asp:Label ID="LblNST" runat="server" EnableViewState="False"></asp:Label>
            </label>
        </div>
    </asp:Panel>
    <asp:Panel runat="server" ID="PanelCatatan">
        <h4>
            CATATAN</h4>
        <div>
            <label>
                <asp:Label ID="lblNotes" runat="server" EnableViewState="False"></asp:Label>
            </label>
        </div>
    </asp:Panel>
    <asp:Panel runat="server" ID="PanelKontrakLain">
        <h4>
            KONTRAK LAIN DIMILIKI
        </h4>
        <div>
            <asp:DataGrid ID="dtgCrossDefault" runat="server" EnableViewState="False" CssClass="tablegrid"
                CellSpacing="1" CellPadding="3" BorderWidth="0px" AutoGenerateColumns="False">
                <AlternatingItemStyle CssClass="tdgenap"></AlternatingItemStyle>
                <ItemStyle CssClass="tdganjil"></ItemStyle>
                <HeaderStyle HorizontalAlign="Center" CssClass="tdjudul"></HeaderStyle>
                <Columns>
                    <asp:TemplateColumn HeaderText="NO">
                        <HeaderStyle Width="10%"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblNo" runat="server" EnableViewState="False"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:BoundColumn DataField="AGREEMENTNO" HeaderText="NO KONTRAK"></asp:BoundColumn>
                    <asp:BoundColumn DataField="NAME" HeaderText="NAMA"></asp:BoundColumn>
                    <asp:BoundColumn DataField="AGREEMENTDATE" HeaderText="TGL KONTRAK"></asp:BoundColumn>
                    <asp:BoundColumn DataField="DEFAULTSTATUS" HeaderText="STATUS DEFAULT"></asp:BoundColumn>
                    <asp:BoundColumn DataField="CONTRACTSTATUS" HeaderText="STATUS KONTRAK"></asp:BoundColumn>
                </Columns>
            </asp:DataGrid>
        </div>
    </asp:Panel>
    <asp:Panel runat="server" ID="PanelApprovalHistory">
        <h4>
            APPROVAL HISTORY</h4>
        <asp:DataGrid ID="dtgHistory" runat="server" EnableViewState="False" Width="100%"
            CssClass="tablegrid" CellSpacing="1" CellPadding="3" BorderWidth="0px" AutoGenerateColumns="False">
            <AlternatingItemStyle CssClass="tdgenap"></AlternatingItemStyle>
            <ItemStyle CssClass="tdganjil"></ItemStyle>
            <HeaderStyle HorizontalAlign="Center" CssClass="tdjudul"></HeaderStyle>
            <Columns>
                <asp:BoundColumn HeaderText="NO">
                    <HeaderStyle Width="5%"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:BoundColumn>
                <asp:BoundColumn DataField="UserApproval" HeaderText="NAMA">
                    <HeaderStyle Width="20%"></HeaderStyle>
                </asp:BoundColumn>
                <asp:BoundColumn DataField="ApprovalDate" HeaderText="TGL APPROVED">
                    <HeaderStyle Width="10%"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:BoundColumn>
                <asp:BoundColumn DataField="WOA" HeaderText="JENIS">
                    <HeaderStyle Width="10%"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:BoundColumn>
                <asp:BoundColumn DataField="Duration" HeaderText="LAMA">
                    <HeaderStyle Width="10%"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:BoundColumn>
                <asp:BoundColumn DataField="SecurityCode" HeaderText="SECURITY CODE">
                    <HeaderStyle Width="15%"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:BoundColumn>
                <asp:BoundColumn DataField="ApprovalNote" HeaderText="CATATAN APPROVAL">
                    <HeaderStyle Width="30%"></HeaderStyle>
                </asp:BoundColumn>
            </Columns>
        </asp:DataGrid>
    </asp:Panel>
    <asp:Panel runat ="server" ID= "PanelButtons">
        <a href="javascript:window.close();">
            <img height="20" src="../Images/ButtonClose.gif" width="100"  alt="Close" /></a>
        <a href='viewStatementOfAccount.aspx?AgreementNo=<%=me.agreementNo%>&amp;Style=<%=request("style")%>&amp;ApplicationID=<%=request("ApplicationID")%>'>
            <img height="20" src="../Images/ButtonBack.gif" width="100" alt="Back" /></a>
    </asp:Panel>
 
