﻿Public Class ucPopupZipCode
    Inherits System.Web.UI.UserControl

    Public Property ZipCode() As String
        Get
            Return CType(txtZipCode.Text.Trim, String)
        End Get
        Set(ByVal Value As String)
            txtZipCode.Text = Value
        End Set
    End Property
    Public Property Kelurahan() As String
        Get
            Return CType(txtKelurahan.Text.Trim, String)
        End Get
        Set(ByVal Value As String)
            txtKelurahan.Text = Value
        End Set
    End Property
    Public Property Kecamatan() As String
        Get
            Return CType(txtKecamatan.Text.Trim, String)
        End Get
        Set(ByVal Value As String)
            txtKecamatan.Text = Value
        End Set
    End Property
    Public Property City() As String
        Get
            Return CType(txtCity.Text.Trim, String)
        End Get
        Set(ByVal Value As String)
            txtCity.Text = Value
        End Set
    End Property
    Public Property Style() As String
        Get
            Return CStr(ViewState("style"))
        End Get
        Set(ByVal Value As String)
            ViewState("style") = Value
        End Set
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not Page.IsPostBack Then
            hpLookup.NavigateUrl = "javascript:OpenWinZipCode('" & txtZipCode.ClientID & "','" & txtKelurahan.ClientID & "','" & txtKecamatan.ClientID & "','" & txtCity.ClientID & "','" & Me.Style & "')"

            txtCity.Attributes.Add("readonly", "true")
            txtKecamatan.Attributes.Add("readonly", "true")
            txtKelurahan.Attributes.Add("readonly", "true")
            txtZipCode.Attributes.Add("readonly", "true")
        End If
    End Sub

    Public Sub ValidatorFalse()
        RequiredFieldValidator1.Enabled = False
        lblKodePos.Attributes("class") = "label_split"
    End Sub

    Public Sub ValidatorTrue()
        RequiredFieldValidator1.Enabled = True
        lblKodePos.Attributes("class") = "label_split_req"
    End Sub
    Public Sub BindData()
        txtKelurahan.Text = Me.Kelurahan
        txtKecamatan.Text = Me.Kecamatan
        txtCity.Text = Me.City
        txtZipCode.Text = Me.ZipCode

        hpLookup.NavigateUrl = "javascript:OpenWinZipCode('" & txtZipCode.ClientID & "','" & txtKelurahan.ClientID & "','" & txtKecamatan.ClientID & "','" & txtCity.ClientID & "','" & Me.Style & "')"
    End Sub


    Private Sub btnReset_Click(sender As Object, e As System.EventArgs) Handles btnReset.Click
        txtCity.Text = ""
        txtKecamatan.Text = ""
        txtKelurahan.Text = ""
        txtZipCode.Text = ""
    End Sub
End Class