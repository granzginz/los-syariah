﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucAreaBranchCeklis.ascx.vb" Inherits="Maxiloan.Webform.UserController.ucAreaBranchCeklis" %>
<link href="../Include/Lookup.css" rel="stylesheet" type="text/css" />
    <title></title>
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/ReportingService.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <script src="../js/jquery-1.8.3.js" type="text/javascript" ></script>
    <script type="text/javascript">
        $(function () {
            $("[id*=chkAll]").bind("click", function () {
                if ($(this).is(":checked")) {
                    $("[id*=cboBranch] input").attr("checked", "checked");
                } else {
                    $("[id*=cboBranch] input").removeAttr("checked");
                }
            });
            $("[id*=chkBranch] input").bind("click", function () {
                if ($("[id*=cboBranch] input:checked").length == $("[id*=cboBranch] input").length) {
                    $("[id*=chkAll]").attr("checked", "checked");
                } else {
                    $("[id*=chkAll]").removeAttr("checked");
                }
            });
        });
    </script>
<div class="form_box_usercontrol">
    <div class="form_left_usercontrol">
        <label>Area</label>
        <asp:DropDownList ID="cboArea" runat="server" AutoPostBack="true" OnSelectedIndexChanged="cboArea_IndexChanged" />
        <asp:RequiredFieldValidator ID="RFVArea" runat="server" InitialValue="0"
                    Display="Dynamic" ControlToValidate="cboArea" ErrorMessage="Harap isi area"
                    CssClass="validator_general"></asp:RequiredFieldValidator>
    </div>
</div>
<div class="form_box_usercontrol">
	<div class="form_left_usercontrol">
        <label class="label_general">
                Cabang
            </label>
            <div class="checkbox_wrapper_ws" runat="server" id="cboBranch1">
            <asp:CheckBox ID="chkAll" runat="server" Text="ALL" AutoPostBack="true"/><br/>
                <asp:CheckBoxList runat="server" ID="cboBranch" AutoPostBack="true" RepeatDirection="Vertical" RepeatLayout="Table"
                    CssClass="opt_single"/>
	</div>
</div>
<div class="form_right_usercontrol">
</div>
</div>