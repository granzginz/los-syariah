﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucLookupPaymentAllocation.ascx.vb" Inherits="Maxiloan.Webform.UserController.ucLookupPaymentAllocation" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

    <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtGoPage"
        MinimumValue="1" MaximumValue="999" ErrorMessage="Page No. is not valid" Type="Double"></asp:RangeValidator>
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
   <ContentTemplate>
        <asp:Button runat="server" ID="btnLookup" Style="display: none;" Text="Lookup" />
        <asp:ModalPopupExtender ID="ModalPopupExtender" runat="server" PopupControlID="Panel1"
            BackgroundCssClass="wpbg" TargetControlID="btnLookup" CancelControlID="imbExit">
        </asp:ModalPopupExtender>
        <asp:Panel ID="Panel1" runat="server">
            <div class="wp">
                <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtGoPage" MinimumValue="1"
                    MaximumValue="999" ErrorMessage="Page No. is not valid" Type="Double"></asp:RangeValidator>
                <asp:Label ID="Label1" runat="server"></asp:Label>
                <asp:ImageButton runat="server" ID="imbExit" ImageUrl="../Images/exit_lookup00.png"
                    CssClass="wpbtnexit" />
                <div class="form_box_title">
                    <div class="title_strip">
                    </div>
                    <div class="form_single">
                        <h4>
                            PAYMENT ALLOCATION SUPPLIER 
                        </h4>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ws">
                            <asp:DataGrid ID="dtgPaging" runat="server" CellPadding="0" OnSortCommand="SortGrid"
                                BackColor="White" BorderWidth="1px" BorderStyle="None" BorderColor="#CCCCCC"
                                DataKeyField="PaymentAllocationID" AutoGenerateColumns="False" AllowSorting="True" AllowPaging="True"
                                Visible="True" Width="100%" CssClass="gridwp">
                                <ItemStyle CssClass="item_grid"></ItemStyle>
                                <HeaderStyle CssClass="th"></HeaderStyle>
                                <Columns>
                                    <asp:ButtonColumn Text="SELECT" CommandName="Select" ButtonType="LinkButton" ItemStyle-CssClass="command_col">
                                    </asp:ButtonColumn>
                                    <asp:BoundColumn ReadOnly="True" DataField="PaymentAllocationDescription" SortExpression="PaymentAllocationDescription"
                                        HeaderText="DESCRIPTION" ItemStyle-CssClass="name_col"></asp:BoundColumn>
                                    <asp:BoundColumn ReadOnly="True" Visible="false" DataField="COA">
                                    </asp:BoundColumn>
                                </Columns>
                                <PagerStyle Visible="False" HorizontalAlign="Left" ForeColor="#000066" BackColor="White"
                                    Mode="NumericPages"></PagerStyle>
                            </asp:DataGrid>
                        </div>
                        <div class="grid_wrapper_ns">
                            <div class="button_gridnavigation">
                                <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" ImageUrl="../Images/grid_navbutton01.png"
                                    OnCommand="NavigationLink_Click" CommandName="First"></asp:ImageButton>
                                <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" ImageUrl="../Images/grid_navbutton02.png"
                                    OnCommand="NavigationLink_Click" CommandName="Prev"></asp:ImageButton>
                                <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" ImageUrl="../Images/grid_navbutton03.png"
                                    OnCommand="NavigationLink_Click" CommandName="Next"></asp:ImageButton>
                                <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" ImageUrl="../Images/grid_navbutton04.png"
                                    OnCommand="NavigationLink_Click" CommandName="Last"></asp:ImageButton>
                                Page
                                <asp:TextBox ID="txtGoPage" runat="server" Width="34px">1</asp:TextBox>
                                <asp:Button ID="btnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                                    EnableViewState="False" />
                                <asp:RangeValidator ID="Rangevalidator2" runat="server" Type="Integer" MaximumValue="999999999"
                                    ErrorMessage="Page No. is not valid" MinimumValue="1" ControlToValidate="txtGoPage"></asp:RangeValidator>
                                <asp:RequiredFieldValidator ID="rfvGo" runat="server" ErrorMessage="Page No. is not valid"
                                    ControlToValidate="txtGoPage" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                            </div>
                            <div class="label_gridnavigation">
                                <asp:Label ID="lblPage" runat="server"></asp:Label>of
                                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                                <asp:Label ID="lblTotRec" runat="server"></asp:Label>record(s)
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_auto">
                            Description</label>
                        <asp:TextBox ID="txtSearchBy" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="btnSearch" runat="server" CausesValidation="False" Text="Find" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="btnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
                    </asp:Button>
                </div>
            </div>
        </asp:Panel>
    </ContentTemplate>
    </asp:UpdatePanel>


