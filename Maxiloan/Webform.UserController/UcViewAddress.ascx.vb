﻿Public Class UcViewAddress
    Inherits System.Web.UI.UserControl
    Public WriteOnly Property Address() As String
        Set(ByVal Value As String)
            lblAddress.Text = Value
        End Set
    End Property

    Public WriteOnly Property RT() As String
        Set(ByVal Value As String)
            lblRT.Text = Value
        End Set
    End Property

    Public WriteOnly Property RW() As String
        Set(ByVal Value As String)
            lblRW.Text = Value
        End Set
    End Property

    Public WriteOnly Property Kecamatan() As String
        Set(ByVal Value As String)
            lblKecamatan.Text = Value
        End Set
    End Property

    Public WriteOnly Property Kelurahan() As String
        Set(ByVal Value As String)
            lblKelurahan.Text = Value
        End Set
    End Property

    Public WriteOnly Property City() As String
        Set(ByVal Value As String)
            lblCity.Text = Value
        End Set
    End Property

    Public WriteOnly Property ZipCode() As String
        Set(ByVal Value As String)
            lblZipCode.Text = Value
        End Set
    End Property

    Public WriteOnly Property AreaPhone1() As String
        Set(ByVal Value As String)
            lblAreaPhone1.Text = Value
        End Set
    End Property

    Public WriteOnly Property AreaPhone2() As String
        Set(ByVal Value As String)
            lblAreaPhone2.Text = Value
        End Set
    End Property

    Public WriteOnly Property Phone1() As String
        Set(ByVal Value As String)
            lblPhone1.Text = Value
        End Set
    End Property

    Public WriteOnly Property Phone2() As String
        Set(ByVal Value As String)
            lblPhone2.Text = Value
        End Set
    End Property


    Public WriteOnly Property AreaFax() As String
        Set(ByVal Value As String)
            lblAreaFax.Text = Value
        End Set
    End Property

    Public WriteOnly Property Fax() As String
        Set(ByVal Value As String)
            lblFax.Text = Value
        End Set
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

End Class