﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UcMaxAccCollList.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.UcMaxAccCollList" %>
<div class="form_box_header">
    <div class="form_single">
        <div class="grid_wrapper_ns">
            <asp:DataGrid ID="dtgColl" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                OnSortCommand="Sorting" DataKeyField="CollectorID" BorderStyle="None" BorderWidth="0"
                CssClass="grid_general">
                <HeaderStyle CssClass="th" />
                <ItemStyle CssClass="item_grid" />
                <Columns>
                    <asp:TemplateColumn HeaderText="EDIT">
                        <ItemStyle CssClass="command_col"></ItemStyle>
                        <ItemTemplate>
                            <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" ImageUrl="../Images/iconedit.gif"
                                CommandName="Edit"></asp:ImageButton>
                        </ItemTemplate>
                    </asp:TemplateColumn>                    
                    <asp:BoundColumn DataField="CollectorID" HeaderText="ID"></asp:BoundColumn>
                    <asp:BoundColumn DataField="CollectorName" HeaderText="NAMA"></asp:BoundColumn>
                    <asp:BoundColumn DataField="MaksimalKontrak" HeaderText="MAKSIMAL KONTRAK" DataFormatString="{0:N0}"></asp:BoundColumn>
                    <asp:BoundColumn DataField="CGID" Visible="false"></asp:BoundColumn>
                </Columns>
            </asp:DataGrid>
            <div class="button_gridnavigation">
                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../Images/grid_navbutton01.png"
                    CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                </asp:ImageButton>
                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../Images/grid_navbutton02.png"
                    CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                </asp:ImageButton>
                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../Images/grid_navbutton03.png"
                    CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                </asp:ImageButton>
                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../Images/grid_navbutton04.png"
                    CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                </asp:ImageButton>
                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                    EnableViewState="False"></asp:Button>
                <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" MinimumValue="1"
                    ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"></asp:RangeValidator>
                <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                    ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
            <div class="label_gridnavigation">
                <asp:Label ID="lblPage" runat="server"></asp:Label>of
                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
            </div>
        </div>
    </div>
</div>
