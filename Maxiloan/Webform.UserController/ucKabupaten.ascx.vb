﻿Public Class ucKabupaten
    Inherits System.Web.UI.UserControl

    Public Property DatiID() As String
        Get
            Return hdnProductOfferingID.Value
        End Get
        Set(ByVal Value As String)
            hdnProductOfferingID.Value = Value
        End Set
    End Property

    Public Property NamaKotaKab() As String
        Get
            Return txtProductOfferingDescription.Text
        End Get
        Set(ByVal Value As String)
            txtProductOfferingDescription.Text = Value
        End Set
    End Property
   
    Public Property ProductID() As String
        Get
            Return hdnProductID.Value
        End Get
        Set(ByVal Value As String)
            hdnProductID.Value = Value
        End Set
    End Property

    Public Property Style() As String
        Get
            Return CStr(viewstate("style"))
        End Get
        Set(ByVal Value As String)
            viewstate("style") = Value
        End Set
    End Property

    Public Sub BindData()
        hpLookup.NavigateUrl = "javascript:OpenWinKabupaten('" & hdnProductOfferingID.ClientID & "','" & txtProductOfferingDescription.ClientID & "', '" & hdnProductID.ClientID & "','" & Me.Style & "')"
    End Sub

End Class