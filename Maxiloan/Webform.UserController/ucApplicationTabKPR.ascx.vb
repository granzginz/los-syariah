﻿Imports System.Data.SqlClient

Public Class ucApplicationTabKPR
    Inherits Maxiloan.Webform.ControlBased

    Public Property ApplicationID As String
    Private Property CustomerID As String
    Private Property SupplierGroupID As String

    Public WriteOnly Property visibled As Boolean
        Set(ByVal value As Boolean)
            tabPenjamin.Visible = value
        End Set
    End Property




    Public Sub setLink()
        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(GetConnectionString)
        Dim objReader As SqlDataReader

        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()

            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.CommandText = "spGetApplicationTab"
            objCommand.Parameters.Add("@ApplicationID", SqlDbType.VarChar, 20).Value = Me.ApplicationID.Trim
            objCommand.Connection = objConnection
            objReader = objCommand.ExecuteReader()

            If objReader.Read Then
                Me.CustomerID = objReader.Item("CustomerID").ToString
                Me.SupplierGroupID = "" 'objReader.Item("SupplierGroupID").ToString
                If Not IsDBNull(objReader.Item("DateEntryHasilSurvey")) Then
                    hyphasilsurvey.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewApplication/HasilSurveyKPR.aspx?CustomerID=" & Me.CustomerID.Trim & "&ApplicationID=" & Me.ApplicationID.Trim
                    hypAplikasi.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewApplication/Application_003KPR.aspx?id=" & Me.CustomerID & "&appid=" & Me.ApplicationID & "&page=Edit"
                    hypAsset.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewApplication/AssetData_002KPR.aspx?appid=" & Me.ApplicationID.ToString.Trim & "&page=Edit"
                    hypAsuransi.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/New/NewAppInsuranceByCompanyKPR.aspx?ApplicationID=" & Me.ApplicationID.ToString.Trim & "&PageSource=CompanyCustomer&SupplierGroupID=" & Me.SupplierGroupID.ToString.Trim & "&page=Edit"
                    hypFinancial.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewApplication/FinancialData_003KPR.aspx?CustomerID=" & Me.CustomerID.Trim & "&ApplicationID=" & Me.ApplicationID.Trim & "&page=Edit"
                    hypPenjamin.NavigateUrl = "~/Webform.LoanOrg/Credit/CustomerPersonalGuarantorKPR.aspx?CustomerID=" & Me.CustomerID.Trim & "&ApplicationID=" & Me.ApplicationID.Trim & "&page=Edit"
                    hypRefund.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewApplication/IncentiveDataKPR.aspx?CustomerID=" & Me.CustomerID.Trim & "&ApplicationID=" & Me.ApplicationID.Trim & "&page=Edit"
                    hypFinancial2.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewApplication/FinancialData_004KPR.aspx?CustomerID=" & Me.CustomerID.Trim & "&ApplicationID=" & Me.ApplicationID.Trim & "&page=Edit"
                    hypappraisal.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewApplication/AppraisalKPR.aspx?CustomerID=" & Me.CustomerID.Trim & "&ApplicationID=" & Me.ApplicationID.Trim & "&page=Edit"
                ElseIf Not IsDBNull(objReader.Item("DateEntryAppraisal")) Then
                    hyphasilsurvey.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewApplication/HasilSurveyKPR.aspx?CustomerID=" & Me.CustomerID.Trim & "&ApplicationID=" & Me.ApplicationID.Trim
                    hypAplikasi.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewApplication/Application_003KPR.aspx?id=" & Me.CustomerID & "&appid=" & Me.ApplicationID & "&page=Edit"
                    hypAsset.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewApplication/AssetData_002KPR.aspx?appid=" & Me.ApplicationID.ToString.Trim & "&page=Edit"
                    hypAsuransi.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/New/NewAppInsuranceByCompanyKPR.aspx?ApplicationID=" & Me.ApplicationID.ToString.Trim & "&PageSource=CompanyCustomer&SupplierGroupID=" & Me.SupplierGroupID.ToString.Trim & "&page=Edit"
                    hypFinancial.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewApplication/FinancialData_003KPR.aspx?CustomerID=" & Me.CustomerID.Trim & "&ApplicationID=" & Me.ApplicationID.Trim & "&page=Edit"
                    hypPenjamin.NavigateUrl = "~/Webform.LoanOrg/Credit/CustomerPersonalGuarantorKPR.aspx?CustomerID=" & Me.CustomerID.Trim & "&ApplicationID=" & Me.ApplicationID.Trim & "&page=Edit"
                    hypRefund.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewApplication/IncentiveDataKPR.aspx?CustomerID=" & Me.CustomerID.Trim & "&ApplicationID=" & Me.ApplicationID.Trim & "&page=Edit"
                    hypFinancial2.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewApplication/FinancialData_004KPR.aspx?CustomerID=" & Me.CustomerID.Trim & "&ApplicationID=" & Me.ApplicationID.Trim & "&page=Edit"
                    hypappraisal.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewApplication/AppraisalKPR.aspx?CustomerID=" & Me.CustomerID.Trim & "&ApplicationID=" & Me.ApplicationID.Trim & "&page=Edit"

                ElseIf Not IsDBNull(objReader.Item("DateEntryIncentiveData")) Then
                    hypAplikasi.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewApplication/Application_003KPR.aspx?id=" & Me.CustomerID & "&appid=" & Me.ApplicationID & "&page=Edit"
                    hypAsset.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewApplication/AssetData_002KPR.aspx?appid=" & Me.ApplicationID.ToString.Trim & "&page=Edit"
                    hypAsuransi.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/New/NewAppInsuranceByCompanyKPR.aspx?ApplicationID=" & Me.ApplicationID.ToString.Trim & "&PageSource=CompanyCustomer&SupplierGroupID=" & Me.SupplierGroupID.ToString.Trim & "&page=Edit"
                    hypFinancial.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewApplication/FinancialData_003KPR.aspx?CustomerID=" & Me.CustomerID.Trim & "&ApplicationID=" & Me.ApplicationID.Trim & "&page=Edit"
                    hypPenjamin.NavigateUrl = "~/Webform.LoanOrg/Credit/CustomerPersonalGuarantorKPR.aspx?CustomerID=" & Me.CustomerID.Trim & "&ApplicationID=" & Me.ApplicationID.Trim & "&page=Edit"
                    hypRefund.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewApplication/IncentiveDataKPR.aspx?CustomerID=" & Me.CustomerID.Trim & "&ApplicationID=" & Me.ApplicationID.Trim & "&page=Edit"
                    hypFinancial2.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewApplication/FinancialData_004KPR.aspx?CustomerID=" & Me.CustomerID.Trim & "&ApplicationID=" & Me.ApplicationID.Trim & "&page=Edit"
                    hyphasilsurvey.NavigateUrl = ""
                    hypappraisal.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewApplication/AppraisalKPR.aspx?CustomerID=" & Me.CustomerID.Trim & "&ApplicationID=" & Me.ApplicationID.Trim & "&page=Edit"
                ElseIf Not IsDBNull(objReader.Item("DateEntryFinancialData2")) Then
                    hypAplikasi.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewApplication/Application_003KPR.aspx?id=" & Me.CustomerID & "&appid=" & Me.ApplicationID & "&page=Edit"
                    hypAsset.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewApplication/AssetData_002KPR.aspx?appid=" & Me.ApplicationID.ToString.Trim & "&page=Edit"
                    hypAsuransi.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/New/NewAppInsuranceByCompanyKPR.aspx?ApplicationID=" & Me.ApplicationID.ToString.Trim & "&PageSource=CompanyCustomer&SupplierGroupID=" & Me.SupplierGroupID.ToString.Trim
                    hypFinancial.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewApplication/FinancialData_003KPR.aspx?CustomerID=" & Me.CustomerID.Trim & "&ApplicationID=" & Me.ApplicationID.Trim & "&page=Edit"
                    hypPenjamin.NavigateUrl = "~/Webform.LoanOrg/Credit/CustomerPersonalGuarantorKPR.aspx?CustomerID=" & Me.CustomerID.Trim & "&ApplicationID=" & Me.ApplicationID.Trim & "&page=Edit"
                    hypRefund.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewApplication/IncentiveDataKPR.aspx?CustomerID=" & Me.CustomerID.Trim & "&ApplicationID=" & Me.ApplicationID.Trim & "&page=Edit"
                    hypFinancial2.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewApplication/FinancialData_004KPR.aspx?CustomerID=" & Me.CustomerID.Trim & "&ApplicationID=" & Me.ApplicationID.Trim & "&page=Edit"
                    hypappraisal.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewApplication/AppraisalKPR.aspx?CustomerID=" & Me.CustomerID.Trim & "&ApplicationID=" & Me.ApplicationID.Trim & "&page=Edit"

                    hyphasilsurvey.NavigateUrl = ""
                ElseIf Not IsDBNull(objReader.Item("DateEntryFinancialData")) Then
                    hypAplikasi.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewApplication/Application_003KPR.aspx?id=" & Me.CustomerID & "&appid=" & Me.ApplicationID & "&page=Edit"
                    hypAsset.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewApplication/AssetData_002KPR.aspx?appid=" & Me.ApplicationID.ToString.Trim & "&page=Edit"
                    hypAsuransi.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/New/NewAppInsuranceByCompanyKPR.aspx?ApplicationID=" & Me.ApplicationID.ToString.Trim & "&PageSource=CompanyCustomer&SupplierGroupID=" & Me.SupplierGroupID.ToString.Trim
                    hypFinancial.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewApplication/FinancialData_003KPR.aspx?CustomerID=" & Me.CustomerID.Trim & "&ApplicationID=" & Me.ApplicationID.Trim & "&page=Edit"
                    hypPenjamin.NavigateUrl = "~/Webform.LoanOrg/Credit/CustomerPersonalGuarantorKPR.aspx?CustomerID=" & Me.CustomerID.Trim & "&ApplicationID=" & Me.ApplicationID.Trim & "&page=Edit"
                    hypFinancial2.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewApplication/FinancialData_004KPR.aspx?CustomerID=" & Me.CustomerID.Trim & "&ApplicationID=" & Me.ApplicationID.Trim & "&page=Edit"

                    hypRefund.NavigateUrl = ""
                    hypappraisal.NavigateUrl = ""
                    hyphasilsurvey.NavigateUrl = ""
                ElseIf Not IsDBNull(objReader.Item("DateEntryInsuranceData")) Then
                    hypAplikasi.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewApplication/Application_003KPR.aspx?id=" & Me.CustomerID & "&appid=" & Me.ApplicationID & "&page=Edit"
                    hypAsset.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewApplication/AssetData_002KPR.aspx?appid=" & Me.ApplicationID.ToString.Trim & "&page=Edit"
                    hypAsuransi.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/New/NewAppInsuranceByCompanyKPR.aspx?CustomerID=" & Me.CustomerID.Trim & "&ApplicationID=" & Me.ApplicationID.ToString.Trim & "&PageSource=CompanyCustomer&SupplierGroupID=" & Me.SupplierGroupID.ToString.Trim & "&page=Edit"
                    hypFinancial.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewApplication/FinancialData_003KPR.aspx?CustomerID=" & Me.CustomerID.Trim & "&ApplicationID=" & Me.ApplicationID.Trim
                    hypPenjamin.NavigateUrl = ""
                    hypRefund.NavigateUrl = ""
                    hypFinancial2.NavigateUrl = ""
                    hypappraisal.NavigateUrl = ""
                    hyphasilsurvey.NavigateUrl = ""
                ElseIf Not IsDBNull(objReader.Item("DateEntryAssetData")) Then
                    hypAplikasi.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewApplication/Application_003KPR.aspx?id=" & Me.CustomerID & "&appid=" & Me.ApplicationID & "&page=Edit"
                    hypAsset.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewApplication/AssetData_002KPR.aspx?appid=" & Me.ApplicationID.ToString.Trim & "&page=Edit"
                    hypAsuransi.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/New/NewAppInsuranceByCompanyKPR.aspx?ApplicationID=" & Me.ApplicationID.ToString.Trim & "&PageSource=CompanyCustomer&SupplierGroupID=" & Me.SupplierGroupID.ToString.Trim
                    hypFinancial.NavigateUrl = ""
                    hypPenjamin.NavigateUrl = ""
                    hypRefund.NavigateUrl = ""
                    hypFinancial2.NavigateUrl = ""
                    hypappraisal.NavigateUrl = ""
                    hyphasilsurvey.NavigateUrl = ""
                ElseIf Not IsDBNull(objReader.Item("DateEntryApplicationData")) Then
                    hypAplikasi.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewApplication/Application_003KPR.aspx?id=" & Me.CustomerID & "&appid=" & Me.ApplicationID & "&page=Edit"
                    hypAsset.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewApplication/AssetData_002KPR.aspx?appid=" & Me.ApplicationID.ToString.Trim
                    hypAsuransi.NavigateUrl = ""
                    hypFinancial.NavigateUrl = ""
                    hypPenjamin.NavigateUrl = ""
                    hypRefund.NavigateUrl = ""
                    hypFinancial2.NavigateUrl = ""
                    hypappraisal.NavigateUrl = ""
                    hyphasilsurvey.NavigateUrl = ""
                Else
                    hypAplikasi.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewApplication/Application_003KPR.aspx?id=" & Me.CustomerID & "&appid=" & Me.ApplicationID
                    hypAsset.NavigateUrl = ""
                    hypAsuransi.NavigateUrl = ""
                    hypFinancial.NavigateUrl = ""
                    hypPenjamin.NavigateUrl = ""
                    hypRefund.NavigateUrl = ""
                    hypFinancial2.NavigateUrl = ""
                    hypappraisal.NavigateUrl = ""
                    hyphasilsurvey.NavigateUrl = ""
                End If

            End If

            objReader.Close()
        Catch ex As Exception
            Throw New Exception(ex.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
            objCommand.Dispose()
        End Try
    End Sub

    Public Sub selectedTab(ByVal strTab As String)
        Select Case strTab
            Case "Application"
                tabAplikasi.Attributes.Add("class", "tab_selected")
                tabAsset.Attributes.Add("class", "tab_notselected")
                tabAsuransi.Attributes.Add("class", "tab_notselected")
                tabFinancial.Attributes.Add("class", "tab_notselected")
                hypPenjamin.Attributes.Add("class", "tab_notselected")
                tabRefund.Attributes.Add("class", "tab_notselected")
                tabFinancial2.Attributes.Add("class", "tab_notselected")
                'tabRincianTransaksi.Attributes.Add("class", "tab_notselected")
                tabappraisal.Attributes.Add("class", "tab_notselected")
                tabhasilsurvey.Attributes.Add("class", "tab_notselected")
            Case "Asset"
                tabAplikasi.Attributes.Add("class", "tab_notselected")
                tabAsset.Attributes.Add("class", "tab_selected")
                tabAsuransi.Attributes.Add("class", "tab_notselected")
                tabFinancial.Attributes.Add("class", "tab_notselected")
                hypPenjamin.Attributes.Add("class", "tab_notselected")
                tabRefund.Attributes.Add("class", "tab_notselected")
                tabFinancial2.Attributes.Add("class", "tab_notselected")
                'tabRincianTransaksi.Attributes.Add("class", "tab_notselected")
                'tabSurvey.Attributes.Add("class", "tab_notselected")
                tabappraisal.Attributes.Add("class", "tab_notselected")
                tabhasilsurvey.Attributes.Add("class", "tab_notselected")
            Case "Asuransi"
                tabAplikasi.Attributes.Add("class", "tab_notselected")
                tabAsset.Attributes.Add("class", "tab_notselected")
                tabAsuransi.Attributes.Add("class", "tab_selected")
                tabFinancial.Attributes.Add("class", "tab_notselected")
                hypPenjamin.Attributes.Add("class", "tab_notselected")
                tabRefund.Attributes.Add("class", "tab_notselected")
                tabFinancial2.Attributes.Add("class", "tab_notselected")
                tabappraisal.Attributes.Add("class", "tab_notselected")
                tabhasilsurvey.Attributes.Add("class", "tab_notselected")
                'tabRincianTransaksi.Attributes.Add("class", "tab_notselected")
                'tabSurvey.Attributes.Add("class", "tab_notselected")
            Case "Financial"
                tabAplikasi.Attributes.Add("class", "tab_notselected")
                tabAsset.Attributes.Add("class", "tab_notselected")
                tabAsuransi.Attributes.Add("class", "tab_notselected")
                tabFinancial.Attributes.Add("class", "tab_selected")
                hypPenjamin.Attributes.Add("class", "tab_notselected")
                tabRefund.Attributes.Add("class", "tab_notselected")
                tabRefund.Attributes.Add("class", "tab_notselected")
                tabFinancial2.Attributes.Add("class", "tab_notselected")
                tabappraisal.Attributes.Add("class", "tab_notselected")
                tabhasilsurvey.Attributes.Add("class", "tab_notselected")
            Case "Financial2"
                tabAplikasi.Attributes.Add("class", "tab_notselected")
                tabAsset.Attributes.Add("class", "tab_notselected")
                tabAsuransi.Attributes.Add("class", "tab_notselected")
                tabFinancial.Attributes.Add("class", "tab_notselected")
                tabFinancial2.Attributes.Add("class", "tab_selected")
                tabRefund.Attributes.Add("class", "tab_notselected")
                hypPenjamin.Attributes.Add("class", "tab_notselected")
                tabappraisal.Attributes.Add("class", "tab_notselected")
                tabhasilsurvey.Attributes.Add("class", "tab_notselected")
                'tabRincianTransaksi.Attributes.Add("class", "tab_notselected")
                'tabSurvey.Attributes.Add("class", "tab_notselected")
            Case "Penjamin"
                tabAplikasi.Attributes.Add("class", "tab_notselected")
                tabAsset.Attributes.Add("class", "tab_notselected")
                tabAsuransi.Attributes.Add("class", "tab_notselected")
                tabFinancial.Attributes.Add("class", "tab_notselected")
                hypPenjamin.Attributes.Add("class", "tab_selected")
                tabRefund.Attributes.Add("class", "tab_notselected")
                tabFinancial2.Attributes.Add("class", "tab_notselected")
                tabappraisal.Attributes.Add("class", "tab_notselected")
                tabhasilsurvey.Attributes.Add("class", "tab_notselected")
                'tabRincianTransaksi.Attributes.Add("class", "tab_notselected")
                'tabSurvey.Attributes.Add("class", "tab_notselected")
            Case "Refund"
                tabAplikasi.Attributes.Add("class", "tab_notselected")
                tabAsset.Attributes.Add("class", "tab_notselected")
                tabAsuransi.Attributes.Add("class", "tab_notselected")
                tabFinancial.Attributes.Add("class", "tab_notselected")
                hypPenjamin.Attributes.Add("class", "tab_notselected")
                tabRefund.Attributes.Add("class", "tab_selected")
                tabFinancial2.Attributes.Add("class", "tab_notselected")
                tabappraisal.Attributes.Add("class", "tab_notselected")
                tabhasilsurvey.Attributes.Add("class", "tab_notselected")
                'tabRincianTransaksi.Attributes.Add("class", "tab_notselected")
                'tabSurvey.Attributes.Add("class", "tab_notselected")
                'Case "RincianTransaksi"
                '    tabAplikasi.Attributes.Add("class", "tab_notselected")
                '    tabAsset.Attributes.Add("class", "tab_notselected")
                '    tabAsuransi.Attributes.Add("class", "tab_notselected")
                '    tabFinancial.Attributes.Add("class", "tab_notselected")
                '    'tabRefund.Attributes.Add("class", "tab_notselected")
                '    'tabRincianTransaksi.Attributes.Add("class", "tab_selected")
                '    tabSurvey.Attributes.Add("class", "tab_notselected")
            Case "Appraisal"
                tabAplikasi.Attributes.Add("class", "tab_notselected")
                tabAsset.Attributes.Add("class", "tab_notselected")
                tabAsuransi.Attributes.Add("class", "tab_notselected")
                tabFinancial.Attributes.Add("class", "tab_notselected")
                hypPenjamin.Attributes.Add("class", "tab_notselected")
                tabRefund.Attributes.Add("class", "tab_notselected")
                tabFinancial2.Attributes.Add("class", "tab_notselected")
                tabappraisal.Attributes.Add("class", "tab_selected")
                tabhasilsurvey.Attributes.Add("class", "tab_notselected")
                'tabRincianTransaksi.Attributes.Add("class", "tab_notselected")
                'tabSurvey.Attributes.Add("class", "tab_notselected")
                'Case "RincianTransaksi"
                '    tabAplikasi.Attributes.Add("class", "tab_notselected")
                '    tabAsset.Attributes.Add("class", "tab_notselected")
                '    tabAsuransi.Attributes.Add("class", "tab_notselected")
                '    tabFinancial.Attributes.Add("class", "tab_notselected")
                '    'tabRefund.Attributes.Add("class", "tab_notselected")
                '    'tabRincianTransaksi.Attributes.Add("class", "tab_selected")
                '    tabSurvey.Attributes.Add("class", "tab_notselected")
            Case "HasilSurvey"
                tabAplikasi.Attributes.Add("class", "tab_notselected")
                tabAsset.Attributes.Add("class", "tab_notselected")
                tabAsuransi.Attributes.Add("class", "tab_notselected")
                tabFinancial.Attributes.Add("class", "tab_notselected")
                hypPenjamin.Attributes.Add("class", "tab_notselected")
                tabRefund.Attributes.Add("class", "tab_notselected")
                tabFinancial2.Attributes.Add("class", "tab_notselected")
                tabappraisal.Attributes.Add("class", "tab_notselected")
                tabhasilsurvey.Attributes.Add("class", "tab_selected")
                'tabRincianTransaksi.Attributes.Add("class", "tab_notselected")
                'tabSurvey.Attributes.Add("class", "tab_notselected")
                'Case "RincianTransaksi"
                '    tabAplikasi.Attributes.Add("class", "tab_notselected")
                '    tabAsset.Attributes.Add("class", "tab_notselected")
                '    tabAsuransi.Attributes.Add("class", "tab_notselected")
                '    tabFinancial.Attributes.Add("class", "tab_notselected")
                '    'tabRefund.Attributes.Add("class", "tab_notselected")
                '    'tabRincianTransaksi.Attributes.Add("class", "tab_selected")
                '    tabSurvey.Attributes.Add("class", "tab_notselected")

            Case Else
                tabAplikasi.Attributes.Add("class", "tab_notselected")
                tabAsset.Attributes.Add("class", "tab_notselected")
                tabAsuransi.Attributes.Add("class", "tab_notselected")
                tabFinancial.Attributes.Add("class", "tab_notselected")
                tabRefund.Attributes.Add("class", "tab_notselected")
                hypPenjamin.Attributes.Add("class", "tab_notselected")
                tabappraisal.Attributes.Add("class", "tab_notselected")
                tabFinancial2.Attributes.Add("class", "tab_notselected")
                tabhasilsurvey.Attributes.Add("class", "tab_notselected")
                'tabRincianTransaksi.Attributes.Add("class", "tab_notselected")
                'tabSurvey.Attributes.Add("class", "tab_selected")
        End Select
    End Sub
End Class