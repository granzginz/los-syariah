﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UcContactPerson.ascx.vb" Inherits="Maxiloan.Webform.UserController.UcContactPerson" %>
<div class="form_box_usercontrol">
    <div class="form_left_usercontrol">
        <label class="label_req">Kontak Person</label>
        <asp:textbox id="txtContactPerson" runat="server" maxlength="50" width="291px" ></asp:textbox>			
	    <asp:requiredfieldvalidator id="rfvContactPerson" runat="server" display="Dynamic" errormessage="Harap isi Kontak"
	    controltovalidate="txtContactPerson" Enabled="False" Visible="False" CssClass ="validator_general"></asp:RequiredFieldValidator></td>
    </div>
</div>
<div class="form_box_usercontrol">
    <div class="form_left_usercontrol">
        <label>Jabatan</label>
        <asp:textbox id="txtContactPersonTitle" runat="server" maxlength="50" width="291px" ></asp:textbox>
	    <asp:requiredfieldvalidator id="rfvContactPersonTitle" runat="server" errormessage="harap isi Jabatan"
	    display="Dynamic" controltovalidate="txtContactPersonTitle" Enabled="False" Visible="False" CssClass ="validator_general"></asp:RequiredFieldValidator>
    </div>
</div>
<div class="form_box_usercontrol">
    <div class="form_left_usercontrol">
    <label class="label_req">Handphone</label>
    <asp:textbox id="txtMobilePhone" runat="server" maxlength="20" width="120px" ></asp:textbox>
    <asp:requiredfieldvalidator id="rfvHandPhone" runat="server" display="Dynamic" errormessage="Harap isi No HandPhone"
	    controltovalidate="txtMobilePhone" Enabled="False" Visible="False" CssClass ="validator_general"></asp:RequiredFieldValidator>
    </div>
</div>
<div class="form_box_usercontrol">
    <div class="form_left_usercontrol">
    <label>e-Mail</label>
    <asp:textbox id="txtEmail" runat="server" maxlength="30" width="200px" ></asp:textbox>
    </div>
</div>
