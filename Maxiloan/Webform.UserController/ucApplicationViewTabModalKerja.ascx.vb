﻿Imports System.Data.SqlClient

Public Class ucApplicationViewTabModalKerja
    Inherits Maxiloan.Webform.ControlBased

    Public Property ApplicationID As String
    Private Property CustomerID As String
    Private Property SupplierGroupID As String

    Public Sub setLink()
        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(GetConnectionString)
        Dim objReader As SqlDataReader

        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()

            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.CommandText = "spGetApplicationTab"
            objCommand.Parameters.Add("@ApplicationID", SqlDbType.VarChar, 20).Value = Me.ApplicationID.Trim
            objCommand.Connection = objConnection
            objReader = objCommand.ExecuteReader()

            If objReader.Read Then
                Me.CustomerID = objReader.Item("CustomerID").ToString
                Me.SupplierGroupID = "" 'objReader.Item("SupplierGroupID").ToString

                If Not IsDBNull(objReader.Item("DateEntryFinancialData")) Then
                    hypAplikasi.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewModalKerja/ViewApplicationModalKerja_003.aspx?id=" & Me.CustomerID & "&ApplicationID=" & Me.ApplicationID & "&page=Edit"
                    hypInvoice.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewModalKerja/ViewInvoiceModalKerja.aspx?ApplicationID=" & Me.ApplicationID.ToString.Trim & "&page=Edit"
                    hypFinancial.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewModalKerja/ViewFinancialModalKerjaData.aspx?ApplicationID=" & Me.ApplicationID.Trim & "&page=Edit"
                    hypAmortisasi.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewModalKerja/ViewAmortisasiModalKerjaData.aspx?ApplicationID=" & Me.ApplicationID.Trim & "&page=Edit"
                    hypPaymentHistory.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewModalKerja/ViewPaymentHistoryModalKerjaData.aspx?ApplicationID=" & Me.ApplicationID.Trim & "&page=Edit"
					hypJournal.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewModalKerja/ViewJournalModalKerjaData.aspx?ApplicationID=" & Me.ApplicationID.Trim & "&page=Edit"
                    hypActivityLog.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewModalKerja/ViewActivityLogMdkj.aspx?ApplicationID=" & Me.ApplicationID.Trim & "&page=Edit"
                    hypPlafon.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewModalKerja/ViewPlafonMdKj.aspx?CustID=" & Me.CustomerID & "&ApplicationID=" & Me.ApplicationID & "&page=Edit"
                ElseIf Not IsDBNull(objReader.Item("DateEntryApplicationData")) Then
                    hypAplikasi.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewModalKerja/ViewApplicationModalKerja_003.aspx?id=" & Me.CustomerID & "&ApplicationID=" & Me.ApplicationID & "&page=Edit"
                    hypInvoice.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewModalKerja/ViewInvoiceModalKerja.aspx?ApplicationID=" & Me.ApplicationID.ToString.Trim & "&page=Edit"
                    hypFinancial.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewModalKerja/ViewFinancialModalKerjaData.aspx?ApplicationID=" & Me.ApplicationID.Trim & "&page=Edit"
                    hypAmortisasi.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewModalKerja/ViewAmortisasiModalKerjaData.aspx?ApplicationID=" & Me.ApplicationID.Trim & "&page=Edit"
                    hypPaymentHistory.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewModalKerja/ViewPaymentHistoryModalKerjaData.aspx?ApplicationID=" & Me.ApplicationID.Trim & "&page=Edit"
					hypJournal.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewModalKerja/ViewJournalModalKerjaData.aspx?ApplicationID=" & Me.ApplicationID.Trim & "&page=Edit"
                    hypActivityLog.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewModalKerja/ViewActivityLogMdkj.aspx?ApplicationID=" & Me.ApplicationID.Trim & "&page=Edit"
                    hypPlafon.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewModalKerja/ViewPlafonMdKj.aspx?CustID=" & Me.CustomerID & "&ApplicationID=" & Me.ApplicationID & "&page=Edit"
                Else
                    hypAplikasi.NavigateUrl = "~/Webform.LoanOrg/Credit/CreditProcess/NewModalKerja/ViewApplication_003.aspx?id=" & Me.CustomerID & "&ApplicationID=" & Me.ApplicationID & "&page=Edit"
                    hypInvoice.NavigateUrl = ""
                    hypFinancial.NavigateUrl = ""
                    hypAmortisasi.NavigateUrl = ""
                    hypPaymentHistory.NavigateUrl = ""
					hypJournal.NavigateUrl = ""
					hypActivityLog.NavigateUrl = ""
                    hypPlafon.NavigateUrl = ""
                End If

            End If

                objReader.Close()
        Catch ex As Exception
            Throw New Exception(ex.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
            objCommand.Dispose()
        End Try
    End Sub

    Public Sub selectedTab(ByVal strTab As String)
        Select Case strTab
            Case "Application"
                tabAplikasi.Attributes.Add("class", "tab_selected")
                tabInvoice.Attributes.Add("class", "tab_notselected")
                tabFinancial.Attributes.Add("class", "tab_notselected")
                tabAmortisasi.Attributes.Add("class", "tab_notselected")
                tabPaymentHistory.Attributes.Add("class", "tab_notselected")
				tabJournal.Attributes.Add("class", "tab_notselected")
                tabActivityLog.Attributes.Add("class", "tab_notselected")
                tabPlafon.Attributes.Add("class", "tab_notselected")
            Case "Invoice"
                tabAplikasi.Attributes.Add("class", "tab_notselected")
                tabInvoice.Attributes.Add("class", "tab_selected")
                tabFinancial.Attributes.Add("class", "tab_notselected")
                tabAmortisasi.Attributes.Add("class", "tab_notselected")
                tabPaymentHistory.Attributes.Add("class", "tab_notselected")
				tabJournal.Attributes.Add("class", "tab_notselected")
                tabActivityLog.Attributes.Add("class", "tab_notselected")
                tabPlafon.Attributes.Add("class", "tab_notselected")
            Case "Financial"
                tabAplikasi.Attributes.Add("class", "tab_notselected")
                tabInvoice.Attributes.Add("class", "tab_notselected")
                tabFinancial.Attributes.Add("class", "tab_selected")
                tabAmortisasi.Attributes.Add("class", "tab_notselected")
                tabPaymentHistory.Attributes.Add("class", "tab_notselected")
				tabJournal.Attributes.Add("class", "tab_notselected")
                tabActivityLog.Attributes.Add("class", "tab_notselected")
                tabPlafon.Attributes.Add("class", "tab_notselected")
            Case "Amortisasi"
                tabAplikasi.Attributes.Add("class", "tab_notselected")
                tabInvoice.Attributes.Add("class", "tab_notselected")
                tabFinancial.Attributes.Add("class", "tab_notselected")
                tabAmortisasi.Attributes.Add("class", "tab_selected")
                tabPaymentHistory.Attributes.Add("class", "tab_notselected")
				tabJournal.Attributes.Add("class", "tab_notselected")
                tabActivityLog.Attributes.Add("class", "tab_notselected")
                tabPlafon.Attributes.Add("class", "tab_notselected")
            Case "PaymentHistory"
                tabAplikasi.Attributes.Add("class", "tab_notselected")
                tabInvoice.Attributes.Add("class", "tab_notselected")
                tabFinancial.Attributes.Add("class", "tab_notselected")
                tabAmortisasi.Attributes.Add("class", "tab_notselected")
                tabPaymentHistory.Attributes.Add("class", "tab_selected")
				tabJournal.Attributes.Add("class", "tab_notselected")
                tabActivityLog.Attributes.Add("class", "tab_notselected")
                tabPlafon.Attributes.Add("class", "tab_notselected")
            Case "Journal"
                tabAplikasi.Attributes.Add("class", "tab_notselected")
                tabInvoice.Attributes.Add("class", "tab_notselected")
                tabFinancial.Attributes.Add("class", "tab_notselected")
                tabAmortisasi.Attributes.Add("class", "tab_notselected")
                tabPaymentHistory.Attributes.Add("class", "tab_notselected")
				tabJournal.Attributes.Add("class", "tab_selected")
                tabActivityLog.Attributes.Add("class", "tab_notselected")
                tabPlafon.Attributes.Add("class", "tab_notselected")
            Case "ActivityLog"
				tabAplikasi.Attributes.Add("class", "tab_notselected")
				tabInvoice.Attributes.Add("class", "tab_notselected")
				tabFinancial.Attributes.Add("class", "tab_notselected")
				tabAmortisasi.Attributes.Add("class", "tab_notselected")
				tabPaymentHistory.Attributes.Add("class", "tab_notselected")
				tabJournal.Attributes.Add("class", "tab_notselected")
                tabActivityLog.Attributes.Add("class", "tab_selected")
                tabPlafon.Attributes.Add("class", "tab_notselected")
            Case "Plafon"
                tabAplikasi.Attributes.Add("class", "tab_notselected")
                tabInvoice.Attributes.Add("class", "tab_notselected")
                tabFinancial.Attributes.Add("class", "tab_notselected")
                tabAmortisasi.Attributes.Add("class", "tab_notselected")
                tabPaymentHistory.Attributes.Add("class", "tab_notselected")
                tabJournal.Attributes.Add("class", "tab_notselected")
                tabActivityLog.Attributes.Add("class", "tab_notselected")
                tabPlafon.Attributes.Add("class", "tab_selected")
            Case Else
                tabAplikasi.Attributes.Add("class", "tab_selected")
                tabInvoice.Attributes.Add("class", "tab_notselected")
                tabFinancial.Attributes.Add("class", "tab_notselected")
                tabAmortisasi.Attributes.Add("class", "tab_notselected")
                tabPaymentHistory.Attributes.Add("class", "tab_notselected")
				tabJournal.Attributes.Add("class", "tab_notselected")
                tabActivityLog.Attributes.Add("class", "tab_notselected")
                tabPlafon.Attributes.Add("class", "tab_notselected")
        End Select
    End Sub
End Class