﻿Public Class ucCMO
    Inherits System.Web.UI.UserControl

    Public Property LookupID() As String
        Get
            Return hdn1.Value
        End Get
        Set(ByVal Value As String)
            hdn1.Value = Value
        End Set
    End Property
    Public Property LookupName() As String
        Get
            Return txtLookup.Text
        End Get
        Set(ByVal Value As String)
            txtLookup.Text = Value
        End Set
    End Property

    Public Property hideLookup() As Boolean
        Get
            Return hpLookup.Visible
        End Get
        Set(ByVal Value As Boolean)
            hpLookup.Visible = Value
        End Set
    End Property

    Public Sub BindData()
        txtLookup.Attributes.Add("readonly", "true")
        hpLookup.NavigateUrl = "javascript:OpenWinCMOLookup('" & hdn1.ClientID & "','" & txtLookup.ClientID & "')"
    End Sub


End Class