﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UcBankAccount.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.UcBankAccount" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcLookupBankBranch" Src="UcLookupBankBranch.ascx" %>
<asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
        onclick="hideMessage();"></asp:Label>
<%--<div runat="server" id="jlookupContent" />--%>
<asp:UpdatePanel runat="server" ID="updatePanel2">
    <ContentTemplate>
        <div class="form_box_usercontrol">
            <div class="form_left_usercontrol">
                <asp:Label runat="server" ID="lblNamaBank" Width="230px" CssClass="label_req">
                    Nama Bank
                </asp:Label>
                <asp:Label runat="server" ID="lblNamaBankReq" Width="230px" CssClass="label_req" Visible="false">
                    Nama Bank
                </asp:Label>
                <asp:DropDownList ID="cboBankName" runat="server" AutoPostBack="True">
                </asp:DropDownList>
                <asp:Label ID="Bintang1" runat="server" ForeColor="Red" Visible="False">* )</asp:Label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Please Choose Bank Name"
                    Display="Dynamic" ControlToValidate="cboBankName" InitialValue="0"
                    CssClass="validator_general"></asp:RequiredFieldValidator>
                <asp:Label ID="LblDefaultBankName" runat="server" Visible="False"></asp:Label>
                <asp:Button ID="btnClearBankAccountChache" runat="server" Text="Refresh" ToolTip="Refresh Bank Account"
                    CausesValidation="False" CssClass="small buttongo blue"></asp:Button>
            </div>
        </div>
        <div class="form_box_usercontrol">
            <div class="form_left_usercontrol">
                <label CssClass="label_split_req">
                    Sandi Bank</label>
                <asp:TextBox ID="txtBankCodeBank" Enabled="false" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="form_box_usercontrol">
            <div class="form_left_usercontrol">
                <asp:Label runat="server" ID="lblNamaCabangBank" Width="230px" CssClass ="label_req" >
                    Nama Cabang Bank
                </asp:Label>
                <asp:Label runat="server" ID="lblNamaCabangBankReq" CssClass ="label_req" Visible="false">
                    Nama Cabang Bank
                </asp:Label>
                <asp:TextBox ID="txtBankBranchName" Enabled="false" runat="server" CssClass="medium_text"  ></asp:TextBox>
                <asp:Button ID="btnLookupBankBrand" runat="server" CausesValidation="False" Text="..."
                    CssClass="small buttongo blue" />
                  <%--    <button class="small buttongo blue" 
                             onclick ="OpenJLookup('<%= ResolveClientUrl("~/webform.Utility/jLookup/LookupBankBranch.aspx?bankCodeBank=" & txtBankCodeBank.ClientID & "&bankBranchName=" & txtBankBranchName.ClientID & "&bankCodeCabang=" & txtBankCodeCabang.ClientID & "&hdnBankBranchId=" & hdnBankBranchId.ClientID & "&city=" & txtCity.ClientID )%>','Daftar Transaction','<%= jlookupContent.ClientID %>');return false;">...</button>  --%>
                <asp:RequiredFieldValidator runat="server" ID="rfvBranchName" ControlToValidate="txtBankBranchName"
                    ErrorMessage="Pilih cabang!" CssClass="validator_general"></asp:RequiredFieldValidator>
                <uc1:UcLookupBankBranch ID="UcLookupBankBranch" runat="server" OnCatSelected="CatSelectedBankBranch" />
            </div>
        </div>
        <div class="form_box_usercontrol">
            <div class="form_left_usercontrol">
                <label CssClass ="label_req" >
                    Sandi Cabang</label>
                <asp:TextBox ID="txtBankCodeCabang" Enabled="false" runat="server"></asp:TextBox>
                <input id="hdnBankBranchId" type="hidden" name="hdnBankBranchId" runat="server" />
            </div>
        </div>
        <div class="form_box_usercontrol">
            <div class="form_left_usercontrol">
                <label CssClass ="label_req" >
                    Kota</label>
                <asp:TextBox ID="txtCity" CssClass="medium_text" Enabled="false" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="form_box_usercontrol">
            <div class="form_left_usercontrol">
                <asp:Label runat="server" ID="lblNoRekening" Width="230px">
                    No Rekening
                </asp:Label>
                <asp:Label runat="server" ID="lblNoRekeningReq" CssClass="label_split_req" Visible="false">
                    No Rekening
                </asp:Label>
                <asp:TextBox ID="txtAccountNo" runat="server" Width="200px" MaxLength="50" onkeypress="return numbersonly2(event)"></asp:TextBox>
    
                <%--<input name="Nama" type="text" id="txtAccountNo" name="txtAccountNo" Width="200px" MaxLength="50" onKeyPress="return goodchars(event,'1234567890',this)" />--%>
                <asp:Label ID="bintang3" runat="server" ForeColor="Red" Visible="False">* )</asp:Label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="Isi no. rekening!" Enabled="false"
                    ControlToValidate="txtAccountNo" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box_usercontrol">
            <div class="form_left_usercontrol">
                <asp:Label runat="server" ID="lblNamaRekening" Width="230px">
                    Nama Rekening
                </asp:Label>
                <asp:Label runat="server" ID="lblNamaRekeningReq" CssClass="label_split_req" Visible="false">
                    Nama Rekening
                </asp:Label>
                <asp:TextBox ID="txtAccountName" runat="server" CssClass="medium_text" MaxLength="50"></asp:TextBox>
                <asp:Label ID="bintang4" runat="server" ForeColor="Red" Visible="False">* )</asp:Label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="Isi nama rekening!"
                    Display="Dynamic" ControlToValidate="txtAccountName" Enabled="False" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
