﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UcViewAssetInsuranceDetailGridTenorAgri.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.UcViewAssetInsuranceDetailGridTenorAgri" %>
<div class="form_box_usercontrol_header">
    <div class="form_single">
        <div class="grid_wrapper_ws">
            <asp:DataGrid ID="DgridTenor" runat="server" AutoGenerateColumns="False" BorderWidth="0px"
                CellPadding="3" CellSpacing="1" BorderStyle="None" DataKeyField="YearNum" CssClass="grid_general">
                <HeaderStyle CssClass="th"></HeaderStyle>
                <ItemStyle CssClass="item_grid" />
                <Columns>
                    <asp:TemplateColumn HeaderText="YEAR">
                        <ItemTemplate>
                            <asp:Label ID="lblGridInscoDYearNum" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.YearNum") %>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="JENIS COVER">
                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblGridInscoDCoverage" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Description") %>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="NILAI ASSET">
                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblGridInscoDNilaiAsset" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.NilaiAsset", "{0:###,###,##0}") %>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="PREMI UTAMA">
                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblGridInscoDMainPremium" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MainPremiumToInsCo", "{0:###,###,##0}") %>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                </Columns>
            </asp:DataGrid>
        </div>
    </div>
</div>
