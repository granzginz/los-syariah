﻿Public Class ValidDate
    Inherits System.Web.UI.UserControl
    Public Event TextChanged As EventHandler
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not IsPostBack Then
            If Not CStr(viewstate("ispostback")) <> "" Then
                cvlDate.ErrorMessage = "Invalid Format Date"
                cvlDate.Display = ValidatorDisplay.None
                rfvRequired.ErrorMessage = "You have to fill Date"
                rfvRequired.Display = ValidatorDisplay.None               
                rfvRequired.Enabled = False
            End If
        End If
        Try
            If txtDate.Text.Trim <> "" Then cvlDate.Validate()
        Catch exp As Exception
            Response.Write("Please Fill date with dd/MM/yyyy")
        End Try
    End Sub

#Region "Properties"
    Public Property isCalendarPostBack() As Boolean
        Get
            Return txtDate.AutoPostBack
        End Get
        Set(ByVal blnValue As Boolean)
            txtDate.AutoPostBack = blnValue
            viewstate("ispostback") = blnValue
        End Set
    End Property

    Public Property isEnabled() As Boolean
        Get
            Return txtDate.Enabled
        End Get
        Set(ByVal Value As Boolean)
            With txtDate
                If Value = False Then
                    .BorderStyle = BorderStyle.None
                Else
                    .BorderStyle = BorderStyle.NotSet
                End If
                .ReadOnly = Not Value
            End With
            divButton.Visible = Value
        End Set
    End Property

    Public Property ValidationErrMessage() As String
        Get
            Return cvlDate.ErrorMessage
        End Get
        Set(ByVal StrErrMsg As String)
            cvlDate.ErrorMessage = StrErrMsg
        End Set
    End Property

    Public Property FillRequired() As Boolean
        Get
            Return rfvRequired.Enabled
        End Get
        Set(ByVal Value As Boolean)
            rfvRequired.Enabled = Value
        End Set
    End Property

    Public Property FieldRequiredMessage() As String
        Get
            Return rfvRequired.ErrorMessage
        End Get
        Set(ByVal Value As String)
            rfvRequired.ErrorMessage = Value
        End Set
    End Property

    Public Property dateValue() As String
        Get
            Return txtDate.Text
        End Get
        Set(ByVal Value As String)
            txtDate.Text = Value
        End Set
    End Property

    Public Property Display() As String
        Get
            Return CType(rfvRequired.Display, String)
        End Get
        Set(ByVal Value As String)
            Select Case UCase(Value)
                Case "NONE" : rfvRequired.Display = ValidatorDisplay.None
                    cvlDate.Display = ValidatorDisplay.None
                Case Else
                    rfvRequired.Display = ValidatorDisplay.Dynamic
                    cvlDate.Display = ValidatorDisplay.Dynamic
            End Select
        End Set
    End Property

    Public ReadOnly Property isValidFormatDate() As Boolean
        Get
            Return Check_Date(txtDate.Text.Trim)
        End Get
    End Property
#End Region

    Private Function Check_Date(ByVal lStrDate As String) As Boolean
        Dim lArrDate() As String
        lArrDate = Split(lStrDate, "/")
        Dim lDteDate As Date
        Dim lBlnResult As Boolean = False
        If UBound(lArrDate) <> 2 Then
            lBlnResult = False
        Else
            If lArrDate(2).Length <> 4 Then
                lBlnResult = False
            Else

                lDteDate = New Date(CType(lArrDate(2), Integer), CType(lArrDate(1), Integer), CType(lArrDate(0), Integer))
                lBlnResult = True
            End If
        End If
        Return lBlnResult
    End Function

    Public Sub isValidDate(ByVal source As Object, ByVal e As ServerValidateEventArgs)
        e.IsValid = Check_Date(e.Value)
    End Sub

    Private Sub txtDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDate.TextChanged
        RaiseEvent TextChanged(sender, e)
    End Sub

    Public Function getClientID() As String
        Return txtDate.ClientID.Trim
    End Function

    Public Sub Disable()
        txtDate.Enabled = False
    End Sub

    Public Sub Enable()
        txtDate.Enabled = True
    End Sub

End Class