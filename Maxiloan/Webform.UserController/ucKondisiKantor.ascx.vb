﻿Public Class ucKondisiKantor
    Inherits System.Web.UI.UserControl

    Public Property SelectedKondisi As String
        Get
            Return cboKondisiKantor.SelectedValue
        End Get
        Set(ByVal value As String)
            cboKondisiKantor.SelectedValue = value
        End Set
    End Property
End Class