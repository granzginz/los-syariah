﻿#Region "Imports"
Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
#End Region

Public Class UcViewInsuranceDetail
    Inherits ControlBased

#Region "Constanta'"

    Private oController As New InsuranceAssetDetailController
    Private oCustomClass As New Maxiloan.Parameter.InsuranceAssetDetail

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load



    End Sub

    Public Sub BindData()

        LblApplicationID.Text = CType(Me.Cache.Item(CACHE_VIEW_INSURANCE_DETAIL_PARAMETER_APPLICATION_ID), String)
        lblBranchID.Text = CType(Me.Cache.Item(CACHE_VIEW_INSURANCE_DETAIL_PARAMETER_BRANCH_ID), String)

        'Cache.Remove(CACHE_VIEW_INSURANCE_DETAIL_PARAMETER_APPLICATION_ID)
        'Cache.Remove(CACHE_VIEW_INSURANCE_DETAIL_PARAMETER_BRANCH_ID)

        Dim strCmdWhere As String = " Where  dbo.Agreement.ApplicationID = '" & Replace(LblApplicationID.Text.Trim, "'", "") & "' "
        With oCustomClass
            .WhereCond = strCmdWhere
            .PageSource = Maxiloan.General.CommonVariableHelper.PAGE_SOURCE_INSURANCE_DETAIL_INFORMATION
            .strConnection = GetConnectionString()

        End With
        Context.Trace.Write("strCmdWhere = " + strCmdWhere)
        oCustomClass = oController.GetViewInsuranceDetail(oCustomClass)

        If Not oCustomClass Is Nothing Then
            With oCustomClass
                LblAgreementNo.Text = .AgreementNo
                LblCustomerName.Text = .CustomerName
                LblAssetDescription.Text = .AssetMasterDescription
            End With
        End If
    End Sub

End Class