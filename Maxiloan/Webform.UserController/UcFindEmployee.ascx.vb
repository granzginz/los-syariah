Option Strict On
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.General.CommonVariableHelper
Public Class UcFindEmployee
    Inherits ControlBased
    Private oLookupEmployee As New LookupEmployee
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents hpLookup As System.Web.UI.WebControls.HyperLink
    Protected WithEvents Requiredfieldvalidator3 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents txtEmployeeName As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtEmployeeID As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblText As System.Web.UI.WebControls.Label
    Protected WithEvents RequiredFieldValidator1 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents hdnBranchID As System.Web.UI.HtmlControls.HtmlInputHidden


    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Public Property BranchID() As String
        Get
            Return CType(hdnBranchID.Value, String)
        End Get
        Set(ByVal Value As String)
            hdnBranchID.Value = Value
        End Set
    End Property

    Public Property Text() As String
        Get
            Return CType(viewstate("Text"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Text") = Value
        End Set
    End Property



    Public Property EmployeeName() As String
        Get
            Return CType(txtEmployeeName.Text.Trim, String)
        End Get
        Set(ByVal Value As String)
            txtEmployeeName.Text = Value
        End Set
    End Property

    Public Property EmployeeID() As String
        Get
            Return txtEmployeeID.Value
        End Get
        Set(ByVal Value As String)
            txtEmployeeID.Value = Value
        End Set
    End Property

    Public Property Style() As String
        Get
            Return CStr(viewstate("style"))
        End Get
        Set(ByVal Value As String)
            viewstate("style") = Value
        End Set
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        hpLookup.NavigateUrl = "javascript:OpenWinEmployee('" & txtEmployeeID.ClientID & "','" & txtEmployeeName.ClientID & "','" & Me.Style & "','" & hdnBranchID.ClientID & "')"
        Me.EmployeeID = txtEmployeeID.Value
        Me.BranchID = hdnBranchID.Value



    End Sub

    Public Sub BindData()
        txtEmployeeID.Value = Me.EmployeeID
        txtEmployeeName.Text = Me.EmployeeName
        lblText.Text = Me.Text
        hpLookup.NavigateUrl = "javascript:OpenWinEmployee('" & txtEmployeeID.ClientID & "','" & txtEmployeeName.ClientID & "','" & Me.Style & "','" & hdnBranchID.ClientID & "')"
    End Sub

End Class
