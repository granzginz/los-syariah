﻿Imports Maxiloan.Controller
Imports Maxiloan.General.CommonCacheHelper

Public Class UcBranch2
    Inherits ControlBased

#Region "Branch Info"
    Public Property BranchID() As String
        Get
            Return CType(viewstate("BranchID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("BranchID") = Value
        End Set
    End Property

    Public Property BranchName() As String
        Get
            Return CType(viewstate("BranchName"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("BranchName") = Value
        End Set
    End Property

    Public Property IsAll() As Boolean
        Get
            Return (CType(Viewstate("IsAll"), Boolean))
        End Get
        Set(ByVal Value As Boolean)
            viewstate("IsAll") = Value
        End Set
    End Property
#End Region

#Region " Private Const "
    Private m_controller As New DataUserControlController
    Protected WithEvents cmbBranchID As System.Web.UI.WebControls.DropDownList
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not IsPostBack Then
            Dim DtBranchName As New DataTable
            DtBranchName = CType(Me.Cache.Item(CACHE_BRANCH_ALL), DataTable)
            If DtBranchName Is Nothing Then
                Dim DtBranchNameCache As New DataTable
                DtBranchNameCache = m_controller.GetBranchName2(GetConnectionString, Me.sesBranchId.Replace("'", "").Trim)
                Me.Cache.Insert(CACHE_BRANCH_ALL, DtBranchNameCache, Nothing, DateTime.Now.AddHours(DEFAULT_TIME_DURATION_CACHE_HOUR), TimeSpan.Zero)
                DtBranchName = CType(Me.Cache.Item(CACHE_BRANCH_ALL), DataTable)
            End If

            cmbBranchID.DataValueField = "ID"
            cmbBranchID.DataTextField = "Name"

            cmbBranchID.DataSource = DtBranchName
            cmbBranchID.DataBind()

            If Me.IsAll Then
                cmbBranchID.Items.Insert(0, "ALL")
                cmbBranchID.Items(0).Value = "ALL"
            Else
                cmbBranchID.Items.Insert(0, "Select One")
                cmbBranchID.Items(0).Value = "0"
            End If
            If cmbBranchID.Items.Count > 1 Then
                cmbBranchID.SelectedIndex = 1
            Else
                cmbBranchID.SelectedIndex = 0
            End If
            cmbBranchID.SelectedIndex = cmbBranchID.Items.IndexOf(cmbBranchID.Items.FindByValue(Me.sesBranchId.Replace("'", "")))
        End If

        Me.BranchID = cmbBranchID.SelectedItem.Value.Trim
        Me.BranchName = cmbBranchID.SelectedItem.Text.Trim
    End Sub

End Class