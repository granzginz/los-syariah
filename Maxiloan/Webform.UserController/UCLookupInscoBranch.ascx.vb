﻿Public Class UCLookupInscoBranch
    Inherits System.Web.UI.UserControl

    Public Property InscoCoyID() As String
        Get
            Return hdnMaskAssID.Value
        End Get
        Set(ByVal Value As String)
            hdnMaskAssID.Value = Value
        End Set
    End Property

    Public Property InscoCoyBranchID() As String
        Get
            Return hdnInsuranceComBranchId.Value
        End Get
        Set(ByVal Value As String)
            hdnInsuranceComBranchId.Value = Value
        End Set
    End Property

    Public Property BranchIDFrom() As String
        Get
            Return hdnBranchID.Value
        End Get
        Set(ByVal Value As String)
            hdnBranchID.Value = Value
        End Set
    End Property

    Public Property BranchName() As String
        Get
            Return txtBranchFullName.Text.Trim
        End Get
        Set(ByVal Value As String)
            txtBranchFullName.Text = Value
        End Set
    End Property

    Public Property Style() As String
        Get
            Return CStr(viewstate("style"))
        End Get
        Set(ByVal Value As String)
            viewstate("style") = Value
        End Set
    End Property

    Public Property InscoID() As String
        Get
            Return CStr(viewstate("InscoID"))
        End Get
        Set(ByVal Value As String)
            viewstate("InscoID") = Value
        End Set
    End Property


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        '  hpLookup.NavigateUrl = "javascript:OpenWinInsBranch('" & Me.Style & "','" & hdnInscoCoyID.ClientID & "','" & hdnInscoCoyBranchID.ClientID & "','" & hdnBranchID.ClientID & "','" & txtInscoBranch.ClientID & "','" & Me.InscoID & "')"
    End Sub

    Public Sub BindDataNih()

        hpLookup.NavigateUrl = "javascript:OpenWinInsBranch('" & Me.Style & "','" & hdnMaskAssID.ClientID & "','" & hdnInsuranceComBranchId.ClientID & "','" & hdnBranchID.ClientID & "','" & txtBranchFullName.ClientID & "','" & Me.InscoID & "')"
    End Sub

End Class