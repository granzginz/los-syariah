﻿#Region "Imports"
Imports Maxiloan.General
Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.Controller
#End Region
Public Class UcCoverageType
    Inherits ControlBased
#Region "Property"
    Public Property FillRequired() As Boolean
        Get
            Return ReqValField.Enabled
        End Get
        Set(ByVal Value As Boolean)
            ReqValField.Enabled = Value
        End Set
    End Property

    Public Property CoverageTypeID() As String
        Get
            Return CType(viewstate("CoverageTypeID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CoverageTypeID") = Value
        End Set
    End Property

    Public Property CoverageTypeName() As String
        Get
            Return CType(viewstate("CoverageTypeName"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CoverageTypeName") = Value
        End Set
    End Property
#End Region
#Region " Private Const "
    Private m_controller As New DataUserControlController    
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load        
        If Not IsPostBack Then
            Dim DtCoverageTypeName As New DataTable
            DtCoverageTypeName = CType(Me.Cache.Item(CommonCacheHelper.CACHE_COVERAGE_TYPE), DataTable)
            If DtCoverageTypeName Is Nothing Then
                Dim DtCoverageTypeNameCache As New DataTable
                DtCoverageTypeNameCache = m_controller.GetCoverageType(GetConnectionString)
                Me.Cache.Insert(CACHE_COVERAGE_TYPE, DtCoverageTypeNameCache, Nothing, DateTime.Now.AddHours(DEFAULT_TIME_DURATION_CACHE_HOUR), TimeSpan.Zero)
                DtCoverageTypeName = CType(Me.Cache.Item(CACHE_COVERAGE_TYPE), DataTable)
            End If
            DDLCoverageType.DataValueField = "ID"
            DDLCoverageType.DataTextField = "Name"
            DDLCoverageType.DataSource = DtCoverageTypeName
            DDLCoverageType.DataBind()
            DDLCoverageType.Items.Insert(0, "Select One")
            DDLCoverageType.Items(0).Value = "0"
        End If
        Me.CoverageTypeID = DDLCoverageType.SelectedItem.Value.Trim
        Me.CoverageTypeName = DDLCoverageType.SelectedItem.Text.Trim

    End Sub
End Class