﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucViewBankAccount.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.ucViewBankAccount" %>
<div class="form_box_usercontrol">
    <div class="form_left_usercontrol">
        <label>
            Nama Bank</label>
        <asp:Label ID="lblBankName" runat="server"></asp:Label>
    </div>
</div>
<div class="form_box_usercontrol">
    <div class="form_left_usercontrol">
        <label>
            Cabang Bank
        </label>
        <asp:Label ID="lblBankBranch" runat="server"></asp:Label>
    </div>
</div>
<div class="form_box_usercontrol">
    <div class="form_left_usercontrol">
        <label>
            No Rekening
        </label>
        <asp:Label ID="lblAccountNo" runat="server"></asp:Label>
    </div>
</div>
<div class="form_box_usercontrol">
    <div class="form_left_usercontrol">
        <label>
            Nama Rekening
        </label>
        <asp:Label ID="lblAccountName" runat="server"></asp:Label>
    </div>
</div>
