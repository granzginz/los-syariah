﻿Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.Controller

Public Class ucBGNumber
    Inherits ControlBased

#Region " Private Const "
    Private m_controller As New DataUserControlController

    Public Property BGNumber() As String
        Get
            Return cmbBGNumber.SelectedItem.Text
        End Get
        Set(ByVal Value As String)
            cmbBGNumber.SelectedIndex = cmbBGNumber.Items.IndexOf(cmbBGNumber.Items.FindByText(Value))
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not IsPostBack Then

        End If
    End Sub

    'Public Sub GetBGNo()
    '    If cmbBGNumber.SelectedIndex > 0 Then
    '        Me.BGNumber = cmbBGNumber.SelectedItem.Text
    '    Else
    '        Me.BGNumber = ""
    '    End If
    'End Sub

    Public Sub BindData(ByVal strWhere As String)
        cmbBGNumber.Items.Clear()
        Dim DtBGNumber As New DataTable
        Dim dvBGNumber As New DataView
        DtBGNumber = m_controller.GetBGNumber(GetConnectionString, strWhere)
        'DtBGNumber = CType(Me.Cache.Item("BGNumber"), DataTable)
        'If DtBGNumber Is Nothing Then
        '    Dim dtBGNumberCache As New DataTable
        '    dtBGNumberCache = m_controller.GetBGNumber(GetConnectionString, strWhere)
        '    Me.Cache.Insert("BGNumber", dtBGNumberCache, Nothing, DateTime.Now.AddHours(DEFAULT_TIME_DURATION_CACHE_HOUR), TimeSpan.Zero)
        '    DtBGNumber = CType(Me.Cache.Item("BGNumber"), DataTable)
        'End If
        dvBGNumber = DtBGNumber.DefaultView

        With cmbBGNumber
            .DataValueField = "Name"
            .DataTextField = "ID"
            .DataSource = dvBGNumber
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = "0"
            .Items.Insert(1, "None")
            .Items(1).Value = "None"

        End With
    End Sub


End Class