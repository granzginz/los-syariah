﻿Public Class ucCoverageTypeRate
    Inherits ControlBased

    Public Event CoverageChanged(ByVal GridIndex As Integer)
    Public Delegate Sub CoverageChangedHandler(ByVal GridIndex As Integer)

    Public Property GridIndex As Integer
        Get
            Return CType(ViewState("GridIndex"), Integer)
        End Get
        Set(ByVal value As Integer)
            ViewState("GridIndex") = value
        End Set
    End Property

    Public WriteOnly Property DataSource As Object
        Set(ByVal value As Object)
            ddlCoverageTypeRate.DataSource = value
        End Set
    End Property

    Public WriteOnly Property DataTextField As String
        Set(ByVal value As String)
            ddlCoverageTypeRate.DataTextField = value
        End Set
    End Property

    Public WriteOnly Property DataValueField As String
        Set(ByVal value As String)
            ddlCoverageTypeRate.DataValueField = value
        End Set
    End Property

    Public Property SelectedValue As String
        Get
            Return ddlCoverageTypeRate.SelectedValue
        End Get
        Set(ByVal value As String)
            ddlCoverageTypeRate.SelectedIndex = ddlCoverageTypeRate.Items.IndexOf(ddlCoverageTypeRate.Items.FindByValue(value))
        End Set
    End Property

    Public Property Enabled As Boolean
        Get
            Return ddlCoverageTypeRate.Enabled
        End Get
        Set(ByVal value As Boolean)
            ddlCoverageTypeRate.Enabled = value
        End Set
    End Property

    Public Sub DataBind()
        ddlCoverageTypeRate.DataBind()
    End Sub

    Public Sub ClearSelection()
        ddlCoverageTypeRate.ClearSelection()
    End Sub

    Public Sub ChangeCoverage() Handles ddlCoverageTypeRate.SelectedIndexChanged
        RaiseEvent CoverageChanged(Me.GridIndex)
    End Sub
End Class