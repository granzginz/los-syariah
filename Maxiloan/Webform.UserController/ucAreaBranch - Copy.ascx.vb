﻿Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.Controller

Public Class ucAreaBranch
    Inherits ControlBased

    Public Event DropDownListCabang_SelectedIndexChanged()

#Region "Branch Info"
    Public Property AdditionalBranchFilter() As String
        Get
            Return CType(ViewState("AdditionalBranchFilter"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("AdditionalBranchFilter") = Value
        End Set
    End Property
    Public ReadOnly Property Branchs() As String()
        Get
            Dim dt As DataTable
            Dim m_Controller As New Controller.ImplementasiControler
            Dim mWhere As String

            If cboArea.SelectedValue = "ALL" Then
                mWhere = ""
            Else
                mWhere = "AreaID = '" + cboArea.SelectedValue + "'"
            End If
            dt = m_Controller.Branch2List(GetConnectionString, mWhere)
            Return (From _dt As DataRow In dt
                        Select New With {
                            .Id = _dt("ID").ToString()
                        }
                    ).Select(Function(x) x.Id).ToArray()
        End Get
    End Property
    Public Property BranchID() As String
        Get
            Return CType(ViewState("BranchID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("BranchID") = Value
        End Set
    End Property

    Public Property BranchName() As String
        Get
            Return CType(ViewState("BranchName"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("BranchName") = Value
        End Set
    End Property

    Public Property IsAll() As Boolean
        Get
            Return CType(ViewState("IsAll"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            ViewState("IsAll") = Value
        End Set
    End Property
    Public Property DropDownListArea As DropDownList
        Get
            Return cboArea
        End Get
        Set(ByVal value As DropDownList)
            cboArea = value
        End Set
    End Property

    Public Property DropDownListCabang As DropDownList
        Get
            Return cboBranch
        End Get
        Set(ByVal value As DropDownList)
            cboBranch = value
        End Set
    End Property

    Public Property SetArea() As String
        Get
            Return CType(ViewState("SetArea"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("SetArea") = Value
        End Set
    End Property

    Public Property SetCabang() As String
        Get
            Return CType(ViewState("SetCabang"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("SetCabang") = Value
        End Set
    End Property

    Public Property Enabled() As Boolean
        Get
            Return CType(ViewState("Enabled"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            ViewState("Enabled") = Value
        End Set
    End Property

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindArea()
            If IsAll Then
                cboBranch.Items.Insert(0, "ALL")
                cboBranch.Items(0).Value = "ALL"
            Else
                cboBranch.Items.Insert(0, "Select One")
                cboBranch.Items(0).Value = "0"
            End If
        End If

    End Sub
#Region "COMBO"
    Sub BindArea()
        Dim m_Controller As New Controller.ImplementasiControler
        Dim dt As DataTable
        dt = m_Controller.AreaList(GetConnectionString, "")
        cboArea.DataSource = dt
        cboArea.DataTextField = "Name"
        cboArea.DataValueField = "ID"
        cboArea.DataBind()

        If SetArea <> "" Then
            cboArea.SelectedIndex = cboArea.Items.IndexOf(cboArea.Items.FindByValue(SetArea))
            cboArea.Enabled = Enabled
            BindBranch("AreaID = '" + cboArea.SelectedValue + "'" & IIf(AdditionalBranchFilter = "", "", " and " & AdditionalBranchFilter).ToString)
        End If

        If IsAll Then
            If cboArea.Items(0).Value <> "ALL" Then
                cboArea.Items.Insert(0, "ALL")
                cboArea.Items(0).Value = "ALL"
            End If
        Else
            If cboArea.Items(0).Value <> "0" Then
                cboArea.Items.Insert(0, "Select One")
                cboArea.Items(0).Value = "0"
            End If
        End If

    End Sub
    Sub BindBranch(ByVal mWhere As String)

        Dim m_Controller As New Controller.ImplementasiControler
        Dim dt As DataTable
        dt = m_Controller.Branch2List(GetConnectionString, mWhere)
        cboBranch.DataSource = dt
        cboBranch.DataTextField = "Name"
        cboBranch.DataValueField = "ID"
        cboBranch.DataBind()

        If IsAll Then
            If cboArea.Items(0).Value <> "ALL" Then
                cboArea.Items.Insert(0, "ALL")
                cboArea.Items(0).Value = "ALL"
            End If
        Else
            If cboArea.Items(0).Value <> "0" Then
                cboArea.Items.Insert(0, "Select One")
                cboArea.Items(0).Value = "0"
            End If
        End If

        If SetCabang <> "" Then
            If (SetCabang.Equals("ALL")) Then
                cboBranch.Items.Insert(0, "ALL")
                cboBranch.Items(0).Value = "ALL"
                cboBranch.Items(0).Selected = True
            Else
                cboBranch.SelectedIndex = cboBranch.Items.IndexOf(cboBranch.Items.FindByValue(SetCabang))
                BranchID = cboBranch.SelectedValue
                cboBranch.Enabled = Enabled
            End If
        Else
            cboBranch.Enabled = Not (mWhere = "")
        End If

    End Sub
    Sub cboArea_IndexChanged()
        If cboArea.SelectedValue = "ALL" Then
            BindBranch(AdditionalBranchFilter)
        Else
            BindBranch("AreaID = '" + cboArea.SelectedValue + "'" & IIf(sesBranchId = "900", "", " or AreaID='ALL' ").ToString & IIf(AdditionalBranchFilter = "", "", " and " & AdditionalBranchFilter).ToString)
        End If
    End Sub
#End Region

    Protected Sub cboBranch_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboBranch.SelectedIndexChanged
        RaiseEvent DropDownListCabang_SelectedIndexChanged()
    End Sub

End Class