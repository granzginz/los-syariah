﻿
Public Class ucBankAccountOther

    Inherits ControlBased


#Region "Properties"
    Public Property BankAccountID() As String
        Get
            Return CType(txtBankAccountID.Text.Trim, String)
        End Get
        Set(ByVal Value As String)
            txtBankAccountID.Text = Value
        End Set
    End Property
    Public Property BankAccountName() As String
        Get
            Return CType(txtBankAccountName.Text.Trim, String)
        End Get
        Set(ByVal Value As String)
            txtBankAccountName.Text = Value
        End Set
    End Property
    Public Property AccountName() As String
        Get
            Return CType(txtAccountName.Text.Trim, String)
        End Get
        Set(ByVal Value As String)
            txtAccountName.Text = Value
        End Set
    End Property
    Public Property AccountNo() As String
        Get
            Return CType(txtAccountNo.Text.Trim, String)
        End Get
        Set(ByVal Value As String)
            txtAccountNo.Text = Value
        End Set
    End Property
    Public Property Style() As String
        Get
            Return CStr(ViewState("style"))
        End Get
        Set(ByVal Value As String)
            ViewState("style") = Value
        End Set
    End Property

    Public Property IsRequired As Boolean
        Get
            Return CBool(ViewState("Validation"))
        End Get
        Set(ByVal Value As Boolean)
            ViewState("Validation") = Value
        End Set
    End Property

#End Region
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If IsRequired = False Then
                RequiredFieldValidator1.Enabled = False
                lblBankAccountID.Visible = False
                lblBankAccountIDNotValidate.Visible = True
            Else
                RequiredFieldValidator1.Enabled = True
                lblBankAccountID.Visible = True
                lblBankAccountIDNotValidate.Visible = False
            End If
        End If
    End Sub

    Private Sub btnReset2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset1.Click
        txtBankAccountName.Text = ""
        txtAccountNo.Text = ""
        txtBankAccountID.Text = ""
        txtBankAccountName.Text = ""
    End Sub
End Class