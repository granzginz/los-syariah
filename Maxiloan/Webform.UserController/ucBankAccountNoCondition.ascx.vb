﻿#Region "Import"

Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.Controller
Imports Maxiloan.Parameter

#End Region

Public Class ucBankAccountNoCondition
    Inherits ControlBased

#Region "Contanta"
    Private oController As New GeneralPagingController
    Private oCustomClass As New Parameter.BankAccount
    Private oBankController As New BankController
#End Region
#Region "Property"
    Public Property BankAccountName() As String
        Get
            Return CType(cmbBankAccount.SelectedItem.Text, String)
        End Get
        Set(ByVal Value As String)
            cmbBankAccount.SelectedIndex = cmbBankAccount.Items.IndexOf(cmbBankAccount.Items.FindByText(Value))
        End Set
    End Property
    Public Property BankAccountID() As String
        Get
            Return CType(cmbBankAccount.SelectedItem.Value, String)
        End Get
        Set(ByVal Value As String)
            cmbBankAccount.SelectedIndex = cmbBankAccount.Items.IndexOf(cmbBankAccount.Items.FindByValue(Value))
        End Set
    End Property
    Public Property IsAutoPostback() As Boolean
        Get
            Return CType(ViewState("IsPostBack"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            ViewState("IsPostBack") = Value
        End Set
    End Property
    Public Property Type() As String
        Get
            Return CType(ViewState("Type"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Type") = Value
        End Set
    End Property
#End Region
#Region "PageLoad"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not IsPostBack Then
            BindBankAccountNoCondition()
        End If
    End Sub

#End Region
#Region "BindBankAccountNoCondition"

    Public Sub BindBankAccountNoCondition()
        Dim oEntities As New Parameter.GeneralPaging
        Dim dtTable As DataTable = Nothing

        cmbBankAccount.AutoPostBack = Me.IsAutoPostback
        Dim strCacheName As String
        strCacheName = CACHE_BANK_ACCOUNT_NO_CONDITION & Me.sesBranchId.Replace("'", "")
        Dim dtBankAccount As New DataSet
        dtBankAccount = CType(Me.Cache.Item(strCacheName), DataSet)
        If dtBankAccount Is Nothing Then
            Dim dtBankAccountCache As New DataSet
            With oEntities
                .SpName = "spGetBankAccountNoCondition"
                .WhereCond = " And BranchID in ('" & Replace(Me.sesBranchId, "'", "") & "') "
                .strConnection = GetConnectionString()
            End With

            oEntities = oController.GetReportWithParameterWhereCond(oEntities)
            dtBankAccountCache = oEntities.ListDataReport

            Me.Cache.Insert(strCacheName, dtBankAccountCache, Nothing, DateTime.Now.AddHours(DEFAULT_TIME_DURATION_CACHE_HOUR), TimeSpan.Zero)
            dtBankAccount = CType(Me.Cache.Item(strCacheName), DataSet)
        End If
        cmbBankAccount.DataTextField = "Name"
        cmbBankAccount.DataValueField = "ID"
        cmbBankAccount.DataSource = dtBankAccount
        cmbBankAccount.DataBind()


        cmbBankAccount.Items.Insert(0, "Select One")
        cmbBankAccount.Items(0).Value = "0"

        With oCustomClass
            .strConnection = GetConnectionString()
            .BranchId = Replace(Me.sesBranchId, "'", "")
            .BankAccountType = Me.Type
        End With
        dtTable = oBankController.BankAccountListByType(oCustomClass)

        If dtTable.Rows.Count > 0 Then
            For index = 0 To dtTable.Rows.Count - 1
                If CBool(dtTable.Rows(index).Item("BankAccountDefault").ToString) Then                    
                    cmbBankAccount.SelectedIndex = cmbBankAccount.Items.IndexOf(cmbBankAccount.Items.FindByValue(dtTable.Rows(index).Item("BankAccountID").ToString))
                End If
            Next
        Else
            If cmbBankAccount.Items.Count > 1 Then
                cmbBankAccount.SelectedIndex = 1
            Else
                cmbBankAccount.SelectedIndex = 0
            End If
        End If


        'Me.BankAccountID = cmbBankAccount.SelectedItem.Value.Trim
        'Me.BankAccountName = cmbBankAccount.SelectedItem.Text.Trim

    End Sub
#End Region


End Class