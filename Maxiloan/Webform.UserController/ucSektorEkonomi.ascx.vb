﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports System.Data.SqlClient
Imports Maxiloan.General.CommonCookiesHelper
Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.Framework.SQLEngine
#End Region

Partial Class ucSektorEkonomi
    Inherits ControlBased

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Public Delegate Sub CatSelectedHandler(ByVal Kode As String, ByVal SektorEkonomi As String)

    Public Event CatSelected As CatSelectedHandler

    Public Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property

    Public Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property

    Public Property Kode As String
    Public Property SektorEkonomi As String

    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            lblMessage.Text = ""
            'lblMessage.Visible = False
            'pnlList.Visible = False
        End If
    End Sub


    Public Sub BindGridEntity(ByVal cmdWhere As String, Optional ByVal PageIndex As Integer = 0)
        Dim dtEntity As DataTable = Nothing
        Dim oCustomClass As New Parameter.D01

        oCustomClass.strConnection = GetConnectionString()
        oCustomClass.WhereCond = cmdWhere
        oCustomClass.CurrentPage = currentPage
        oCustomClass.PageSize = pageSize
        oCustomClass.SortBy = Me.Sort
        oCustomClass = GetData(oCustomClass)

        If Not oCustomClass Is Nothing Then
            dtEntity = oCustomClass.ListData
            recordCount = oCustomClass.TotalRecord
        Else
            recordCount = 0
        End If

        dtSektorEkonomi.DataSource = dtEntity.DefaultView
        dtSektorEkonomi.CurrentPageIndex = 0
        dtSektorEkonomi.DataBind()
        PagingFooter()
        'pnlList.Visible = True
        'Me.mpSektorEkonomi.Show()
    End Sub

    Public Function GetData(ByVal oCustomClass As Parameter.D01) As Parameter.D01
        Dim oReturnValue As New Parameter.D01
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar, 50)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spPagingSektorEkonomi", params).Tables(0)
            oReturnValue.TotalRecord = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            lblMessage.Text = "Data not found....."
            lblMessage.Visible = True
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblTotRec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select

        BindGridEntity(Me.CmdWhere)
        Popup()
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGoPage.Click
        'If IsNumeric(txtGoPage.Text) Then
        '    If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
        '        currentPage = CType(txtGoPage.Text, Int32)
        '        Popup()
        '        BindGridEntity(Me.CmdWhere)
        '    Else
        '        txtGoPage.Text = String.Empty
        '    End If
        'End If
        If IsNumeric(txtGoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtGoPage.Text, Int32)
                Popup()
                BindGridEntity(Me.CmdWhere)
            End If
        End If
    End Sub
#End Region

    Protected Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If


        BindGridEntity(Me.CmdWhere)
        Popup()
    End Sub

    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        'cboSearchBy.SelectedIndex = 0
        'txtSearchBy.Text = " "
        ''pnlList.Visible = False
        'lblMessage.Visible = False
        ''Popup()
        'BindGridEntity(Me.CmdWhere)
        txtSearchBy.Text = String.Empty
        Me.CmdWhere = cboSearchBy.SelectedItem.Value + " LIKE '%" + txtSearchBy.Text + "%'"
        lblMessage.Visible = False
        BindGridEntity(Me.CmdWhere)
        Popup()
    End Sub

    Private Sub imbSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        If txtSearchBy.Text <> " " Then
            Me.CmdWhere = cboSearchBy.SelectedItem.Value + " LIKE '%" + txtSearchBy.Text + "%'"
        End If

        If lblTotPage.Text >= "1" Then
            lblMessage.Visible = False
        End If

        BindGridEntity(Me.CmdWhere)
        Popup()
    End Sub

    Protected Sub dtSektorEkonomi_ItemCommand(ByVal sender As Object, ByVal e As DataGridCommandEventArgs) Handles dtSektorEkonomi.ItemCommand
        Dim i As Integer = e.Item.ItemIndex
        RaiseEvent CatSelected(dtSektorEkonomi.DataKeys.Item(i).ToString.Trim,
                               dtSektorEkonomi.Items(i).Cells.Item(1).Text.Trim)
    End Sub

    Public Sub Popup()
        Me.mpSektorEkonomi.Show()
    End Sub

End Class