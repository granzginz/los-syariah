﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucApplicationViewTabFactoring.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.ucApplicationViewTabFactoring" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<div class="tab_container_view">
<div class="tab_container">
    <div id="tabAplikasi" runat="server">
        <asp:HyperLink ID="hypAplikasi" runat="server">APLIKASI</asp:HyperLink></div>
    <div id="tabAsset" runat="server">
        <asp:HyperLink ID="hypInvoice" runat="server">INVOICE</asp:HyperLink></div> 
    <div id="tabFinancial" runat="server">
        <asp:HyperLink ID="hypFinancial" runat="server">FINANCIAL</asp:HyperLink></div>
    <div id="tabActivityLog" runat="server">
        <asp:HyperLink ID="hypActivityLog" runat="server">ACTIVITY LOG</asp:HyperLink></div>
    <div id="TabJournal" runat="server">
        <asp:HyperLink ID="hypJournal" runat="server">JOURNAL</asp:HyperLink></div>
    <div id="tabPlafon" runat="server">
        <asp:HyperLink ID="hypPlafon" runat="server">PLAFON</asp:HyperLink></div>
    <div id="tabInvoicePaid" runat="server">
        <asp:HyperLink ID="hypInvoicePaid" runat="server">History Pembayaran</asp:HyperLink></div>
    <div id="tabChangeDueDate" runat="server">
        <asp:HyperLink ID="hypInvoiceChangeDueDate" runat="server">History Ganti Jatuh Tempo</asp:HyperLink></div>
</div>
</div>