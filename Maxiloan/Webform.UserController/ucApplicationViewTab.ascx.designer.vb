﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class ucApplicationViewTab
    
    '''<summary>
    '''tabAplikasi control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tabAplikasi As Global.System.Web.UI.HtmlControls.HtmlGenericControl
    
    '''<summary>
    '''hypAplikasi control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hypAplikasi As Global.System.Web.UI.WebControls.HyperLink
    
    '''<summary>
    '''tabAsset control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tabAsset As Global.System.Web.UI.HtmlControls.HtmlGenericControl
    
    '''<summary>
    '''hypAsset control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hypAsset As Global.System.Web.UI.WebControls.HyperLink
    
    '''<summary>
    '''tabAsuransi control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tabAsuransi As Global.System.Web.UI.HtmlControls.HtmlGenericControl
    
    '''<summary>
    '''hypAsuransi control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hypAsuransi As Global.System.Web.UI.WebControls.HyperLink
    
    '''<summary>
    '''tabFinancial control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tabFinancial As Global.System.Web.UI.HtmlControls.HtmlGenericControl
    
    '''<summary>
    '''hypFinancial control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hypFinancial As Global.System.Web.UI.WebControls.HyperLink
    
    '''<summary>
    '''tabFinancial2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tabFinancial2 As Global.System.Web.UI.HtmlControls.HtmlGenericControl
    
    '''<summary>
    '''hypFinancial2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hypFinancial2 As Global.System.Web.UI.WebControls.HyperLink
    
    '''<summary>
    '''tabRefund control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tabRefund As Global.System.Web.UI.HtmlControls.HtmlGenericControl
    
    '''<summary>
    '''hypRefund control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hypRefund As Global.System.Web.UI.WebControls.HyperLink
    
    '''<summary>
    '''tabSurvey control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tabSurvey As Global.System.Web.UI.HtmlControls.HtmlGenericControl
    
    '''<summary>
    '''hypSurvey control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hypSurvey As Global.System.Web.UI.WebControls.HyperLink
    
    '''<summary>
    '''tabPlafon control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tabPlafon As Global.System.Web.UI.HtmlControls.HtmlGenericControl
    
    '''<summary>
    '''hypPlafon control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hypPlafon As Global.System.Web.UI.WebControls.HyperLink
End Class
