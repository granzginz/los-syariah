﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucFundingReport.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.ucFundingReport" %>
<asp:UpdatePanel runat="server" ID="updpanel" UpdateMode="Conditional">
    <ContentTemplate>
        <div class="form_box_usercontrol">
            <div class="form_left_usercontrol">
                <label>
                    Bank</label>
                <asp:DropDownList ID="cboBankCompany" runat="server" AutoPostBack="true">
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_box_usercontrol">
            <div class="form_left_usercontrol">
                <label>
                    No Kontrak</label>
                <asp:DropDownList ID="cboKontrak" runat="server" AutoPostBack="true">
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_box_usercontrol">
            <div class="form_left_usercontrol">
                <label>
                    No Batch</label>
                <asp:DropDownList ID="cboNoBatch" runat="server" AutoPostBack="true">
                </asp:DropDownList>
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
<div class="form_box_usercontrol" runat="server" id="dvkonsumen">
    <div class="form_left_usercontrol">
        <label class="label_split" runat="server" id="lblAlamat">
            Nama Konsumen
        </label>
        <asp:TextBox ID="txtNamaKonsumen" runat="server" Style="width: 500px" placeholder="Masukan nama karyawan"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="Dynamic"
            ControlToValidate="txtNamaKonsumen" ErrorMessage="Harap pilih Karyawan" Visible="true"
            CssClass="validator_general"></asp:RequiredFieldValidator>
        <asp:TextBox ID="txtEmployeeID" runat="server" placeholder="ID Karyawan" />
    </div>
</div>
