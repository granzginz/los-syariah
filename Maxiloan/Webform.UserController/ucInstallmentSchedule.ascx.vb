﻿Imports Maxiloan.Controller

Public Class ucInstallmentSchedule
    Inherits ControlBased

    Private currentPage_angs As Integer = 1
    Private totalPages_angs As Integer
    Private pageSize_angs As Short = 3
    Private recordCount_angs As Integer


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Public Property ApplicationId As String
        Set(ByVal value As String)
            ViewState("ucApplicationId") = value
        End Set
        Get
            Return ViewState("ucApplicationId").ToString
        End Get
    End Property


    Public Property ValueDate As Date
        Set(ByVal value As Date)
            ViewState("ucValueDate") = value
        End Set
        Get
            Return CType(ViewState("ucValueDate"), Date)
        End Get
    End Property

#Region "bind and navigation angsuran"

    Public Sub DoBind_Angsuran(ByVal strSort As String)        
        Dim cPaging As New InstallRcvController
        Dim oPaging As New Parameter.GeneralPaging

        With oPaging
            .strConnection = GetConnectionString()
            '.WhereCond = " Agreement.applicationId = '" & Me.ApplicationId & "' and Agreement.branchId = " & sesBranchId
            .WhereCond = " Agreement.applicationId = '" & Me.ApplicationId & "' and Installmentschedule.InsSeqNo > 0 "
            .CurrentPage = currentPage_angs
            .PageSize = pageSize_angs
            .SortBy = strSort
            .BusinessDate = Me.ValueDate
            .SpName = "spInstallmentSchedulePagingOutStanding"
        End With

        oPaging = cPaging.InstallRcvInstallmentSchedulePaging(oPaging)

        recordCount_angs = CInt(oPaging.TotalRecords)
        dtgAngsuran.DataSource = oPaging.ListData
        dtgAngsuran.DataBind()

        PagingFooter_angsuran()

    End Sub


    Public Function GetAllAngsuranUnPaid() As DataTable
        Dim cPaging As New InstallRcvController
        Dim oPaging As New Parameter.GeneralPaging

        With oPaging
            .strConnection = GetConnectionString()
            .WhereCond = " Agreement.applicationId = '" & Me.ApplicationId & "' and Installmentschedule.InsSeqNo > 0  "
            .CurrentPage = 1
            .PageSize = 200
            .SortBy = " InstallmentSchedule.InsSeqNo  "
            .BusinessDate = Me.ValueDate
            .SpName = "spInstallmentSchedulePagingOutStanding"
        End With

        oPaging = cPaging.InstallRcvInstallmentSchedulePaging(oPaging)
        Return oPaging.ListData
    End Function


    Protected Sub SortGrid_angsuran(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dtgAngsuran.SortCommand
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
            DoBind_Angsuran(e.SortExpression)
        Else
            Me.SortBy = e.SortExpression + " DESC"
            DoBind_Angsuran(e.SortExpression + " DESC")
        End If

    End Sub

    Private Sub PagingFooter_angsuran()
        lblPage.Text = currentPage_angs.ToString()
        totalPages_angs = CType(Math.Ceiling(CType((recordCount_angs / CType(pageSize_angs, Integer)), Integer)), Integer)
        If totalPages_angs = 0 Then

            'ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages_angs, String)
            rgvGo.MaximumValue = CType(totalPages_angs, String)
        End If
        lblrecord.Text = CType(recordCount_angs, String)

        If currentPage_angs = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages_angs > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage_angs = totalPages_angs Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage_angs = 1
            Case "Last" : currentPage_angs = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage_angs = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage_angs = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind_Angsuran(Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage_angs = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind_Angsuran(Me.SortBy)
            End If
        End If
    End Sub
#End Region
End Class