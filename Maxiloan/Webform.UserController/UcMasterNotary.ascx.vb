﻿#Region "Import"
Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.Controller
#End Region

Public Class UcMasterNotary
    Inherits ControlBased

#Region " Private Const "
    Private m_controller As New DataUserControlController

#End Region
#Region "Property"
    Public Property FillRequired() As Boolean
        Get
            Return RequiredFieldValidator1.Enabled
        End Get
        Set(ByVal Value As Boolean)
            RequiredFieldValidator1.Enabled = Value
        End Set
    End Property
    Public Property NotaryID() As String
        Get
            Return CType(cboMasterNotary.SelectedItem.Value, String)
        End Get
        Set(ByVal Value As String)
            cboMasterNotary.SelectedIndex = cboMasterNotary.Items.IndexOf(cboMasterNotary.Items.FindByValue(Value))
        End Set
    End Property

    Public Property NotaryName() As String
        Get
            Return CType(cboMasterNotary.SelectedItem.Text, String)
        End Get
        Set(ByVal Value As String)
            cboMasterNotary.SelectedIndex = cboMasterNotary.Items.IndexOf(cboMasterNotary.Items.FindByText(Value))
        End Set
    End Property

#End Region

    Public Sub BindNotary()
        Try

            'Dim DtMasterNotary As New DataTable

            'DtMasterNotary = CType(Me.Cache.Item(CACHE_MASTER_NOTARY), DataTable)

            'If DtMasterNotary Is Nothing Then
            '    Dim DTCache As New DataTable

            '    DTCache = m_controller.GetMasterNotary(GetConnectionString)
            '    Me.Cache.Insert(CACHE_MASTER_NOTARY, DTCache, Nothing, DateTime.Now.AddHours(DEFAULT_TIME_DURATION_CACHE_HOUR), TimeSpan.Zero)
            '    DtMasterNotary = CType(Me.Cache.Item(CACHE_MASTER_NOTARY), DataTable)
            'End If

            cboMasterNotary.DataValueField = "ID"
            cboMasterNotary.DataTextField = "Name"

            'cboMasterNotary.DataSource = DtMasterNotary


            cboMasterNotary.DataSource = m_controller.GetMasterNotary(GetConnectionString)
            cboMasterNotary.DataBind()

            cboMasterNotary.Items.Insert(0, "Select One")


            If RequiredFieldValidator1.Enabled = True Then
                cboMasterNotary.Items(0).Value = ""
            Else
                cboMasterNotary.Items(0).Value = "0"
            End If

            Me.NotaryID = cboMasterNotary.SelectedItem.Value.Trim
            Me.NotaryName = cboMasterNotary.SelectedItem.Text.Trim


        Catch ex As Exception
            Response.Write(ex.Message + ex.StackTrace + ex.TargetSite.Name)
        End Try

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Cache.Remove(CACHE_MASTER_NOTARY)
    End Sub
End Class
