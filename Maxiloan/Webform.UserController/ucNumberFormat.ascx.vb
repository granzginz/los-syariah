﻿Public Class ucNumberFormat
    Inherits ControlBased

    Public Delegate Sub TextChangedHandler(ByVal sender As Object, ByVal e As System.EventArgs)
    'Public Event TextChanged As TextChangedHandler
    Public Event TextChanged As TextChangedHandler
    Public WithEvents txtNumber As TextBox
    Public WithEvents rv As RangeValidator

    Public Delegate Sub TextChangedGridHandler(ByVal index As Integer)
    Public Event TextChangedGrid(ByVal index As Integer)


#Region "Properties"
    Public Property RangeValidatorEnable As Boolean
        Get
            Return rv.Enabled
        End Get
        Set(ByVal value As Boolean)
            rv.Enabled = value
        End Set
    End Property
    Public Property OnClientBlur() As String
        Get
            Return hdnOnBlur.Value
        End Get
        Set(ByVal value As String)
            hdnOnBlur.Value = value
        End Set
    End Property
    Public Property OnClientChange() As String
        Get
            Return hdnOnChange.Value
        End Get
        Set(ByVal value As String)
            hdnOnChange.Value = value
        End Set
    End Property

    Public Property RangeValidatorMinimumValue As String
        Get
            Return rv.MinimumValue
        End Get
        Set(ByVal value As String)
            rv.MinimumValue = value
        End Set
    End Property

    Public Property RangeValidatorErrorMessage As String
        Get
            Return rv.ErrorMessage
        End Get
        Set(ByVal value As String)
            rv.ErrorMessage = value
        End Set
    End Property

    Public Property RangeValidatorMaximumValue As String
        Get
            Return rv.MaximumValue
        End Get
        Set(ByVal value As String)
            rv.MaximumValue = value
        End Set
    End Property

    Public Property RequiredFieldValidatorEnable As Boolean
        Get
            Return rfv.Enabled
        End Get
        Set(ByVal value As Boolean)
            rfv.Enabled = value
        End Set
    End Property

    Public Property Text As String
        Get
            Return txtNumber.Text
        End Get
        Set(ByVal value As String)
            txtNumber.Text = value
        End Set
    End Property

    Public Property Enabled As Boolean
        Get
            Return txtNumber.Enabled
        End Get
        Set(ByVal value As Boolean)
            txtNumber.Enabled = value
        End Set
    End Property

    Public Property AutoPostBack As Boolean
        Get
            Return txtNumber.AutoPostBack
        End Get
        Set(ByVal value As Boolean)
            txtNumber.AutoPostBack = value
        End Set
    End Property

    Public WriteOnly Property TextCssClass As String
        Set(ByVal value As String)
            txtNumber.CssClass = value
        End Set
    End Property

    Public Overrides ReadOnly Property ClientId As String
        Get
            Return txtNumber.ClientID
        End Get
    End Property

    Public WriteOnly Property OnChange As String
        Set(ByVal value As String)
            txtNumber.Attributes.Add("onchange", value)
        End Set
    End Property

    Public WriteOnly Property Width As Integer
        Set(value As Integer)
            txtNumber.Width = Unit.Pixel(value)
        End Set
    End Property

    Public Property isReadOnly As Boolean
        Get
            Return txtNumber.ReadOnly
        End Get
        Set(ByVal value As Boolean)
            txtNumber.ReadOnly = value
            txtNumber.Attributes.Add("style", "background-color:#EBEBEB")
        End Set
    End Property

    Public Property GridIndex As Integer
        Set(ByVal value As Integer)
            ViewState("GridIndex") = value
        End Set
        Get
            Return CType(ViewState("GridIndex"), Integer)
        End Get
    End Property

    Public Property IsGrid() As Boolean
        Set(ByVal value As Boolean)
            ViewState("IsGrid") = value
        End Set
        Get
            Return CType(ViewState("IsGrid"), Boolean)
        End Get
    End Property


#End Region
    Private Sub txtNumber_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNumber.PreRender
        txtNumber.Attributes.Remove("onblur")
        txtNumber.Attributes.Add("onblur", "extractNumber(this,7,true);blockInvalid(this);this.value=numberFormat(this.value);this.value=blankToZero(this.value);" & OnClientBlur)
        'txtNumber.Attributes.Remove("OnChange")
        If OnClientChange <> "" Then
            txtNumber.Attributes.Add("OnChange", OnClientChange)
        End If

    End Sub
    Private Sub txtNumber_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNumber.TextChanged
        If IsGrid Then
            RaiseEvent TextChangedGrid(Me.GridIndex)
        Else
            RaiseEvent TextChanged(sender, e)
        End If

    End Sub


    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            setErrMsg()
        End If
    End Sub

    Public Sub setErrMsg()
        rv.ErrorMessage = "Input angka antara " & FormatNumber(rv.MinimumValue, 2) & " s/d " & FormatNumber(rv.MaximumValue, 2)
    End Sub

    Public Sub setBehaviour(ByVal ColumnName As String, ByVal oRow As DataRow)
        Select Case oRow(ColumnName + "Patern").ToString.Trim
            Case "D"
                rv.MinimumValue = "0"
                rv.MaximumValue = "999999999999999"
                rv.ErrorMessage = "Input Salah"
                rv.Enabled = True
                txtNumber.ReadOnly = False
            Case "L"
                rv.MinimumValue = "0"
                rv.MaximumValue = "999999999999999"
                rv.ErrorMessage = "Input Salah"
                rv.Enabled = True
                txtNumber.ReadOnly = True
            Case "N"
                rv.MinimumValue = CInt(oRow(ColumnName)).ToString.Trim
                rv.MaximumValue = "999999999999999"
                rv.ErrorMessage = "Input harus >= " & oRow(ColumnName).ToString.Trim & "!"
                rv.Enabled = True
                txtNumber.ReadOnly = False
            Case "X"
                rv.MaximumValue = CInt(oRow(ColumnName)).ToString.Trim
                rv.MinimumValue = "0"
                rv.ErrorMessage = "Input harus <= " & oRow(ColumnName).ToString.Trim & "!"
                rv.Enabled = True
                txtNumber.ReadOnly = False
        End Select
    End Sub

End Class