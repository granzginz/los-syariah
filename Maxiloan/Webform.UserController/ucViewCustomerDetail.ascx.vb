﻿Imports Maxiloan.Controller

Public Class ucViewCustomerDetail
    Inherits ControlBased

    Public Property CustomerID As String
    Public Property CustomerName As String
        Get
            Return hyCustomerName.Text
        End Get
        Set(ByVal value As String)
            hyCustomerName.Text = value
        End Set
    End Property
    Public Property CustomerType As String
    Public Property PersonalCustomerType As String
        Get
            Return CType(ViewState("PersonalCustomerType"), String)
        End Get
        Set(value As String)
            ViewState("PersonalCustomerType") = value
        End Set
    End Property
    Public Property IsFleet As Boolean
    Dim oController As New DataUserControlController

#Region "LinkTo"
    Function LinkToCustomer(ByVal strCustomerID As String, ByVal strStyle As String) As String
        Return "javascript:OpenWinCustomer('" & strCustomerID & "','" & strStyle & "')"
    End Function   
#End Region

    Public Sub bindCustomerDetail()
        Dim oCustomClass As New Parameter.Customer
        Dim oTable As New DataTable
        With oCustomClass
            .CustomerID = Me.CustomerID
            .strConnection = GetConnectionString()
        End With
        oCustomClass = oController.viewCustomerDetailShort(oCustomClass)
        oTable = oCustomClass.listdata        
        With oTable
            hyCustomerName.Text = oTable.Rows(0).Item("Name").ToString
            hyCustomerName.NavigateUrl = LinkToCustomer(oTable.Rows(0).Item("CustomerID").ToString.Trim, "AccAcq")

            lblAlamat.Text = .Rows(0).Item("Address").ToString
            lblKota.Text = .Rows(0).Item("LegalCity").ToString
            lblNoHP.Text = .Rows(0).Item("MobilePhone").ToString
            lblNoTelp.Text = .Rows(0).Item("NoTelepon").ToString
            Me.CustomerType = .Rows(0).Item("CustomerType").ToString
            Me.PersonalCustomerType = .Rows(0).Item("PersonalCustomerType").ToString
            Me.IsFleet = CBool(.Rows(0).Item("IsFleet"))
            End With
    End Sub
End Class