﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucLookupAssetWithAssetType.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.ucLookupAssetWithAssetType" %>
<script language="javascript" type="text/javascript">
    function OpenWinAssetCode(pAssetCode, pDescription, pAssetTypeID, pStyle, pApplicationID) {
        //var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        //var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
        //window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/General/LookUpAssetWithAssetType.aspx?style=' + pStyle + '&assetcode=' + pAssetCode + '&description=' + pDescription + '&assettypeid=' + pAssetTypeID + '&applicationid=' + pApplicationID, 'UserLookup', 'left=50, top=10, width=600, height=480, menubar=0, scrollbars=yes');
        window.open('http://localhost:1056/General/LookUpAssetWithAssetType.aspx?style=' + pStyle + '&assetcode=' + pAssetCode + '&description=' + pDescription + '&assettypeid=' + pAssetTypeID + '&applicationid=' + pApplicationID, 'UserLookup', 'left=50, top=10, width=600, height=480, menubar=0, scrollbars=yes');
    }
</script>
<table class="tablegrid" cellspacing="0" cellpadding="0" align="left" border="0">
    <tr>
        <td class="tdganjil">
            <asp:TextBox ID="txtDescription"  MaxLength="5" runat="server"
                Width="280px" ReadOnly="True"></asp:TextBox>&nbsp;
            <asp:HyperLink ID="hpLookup" runat="server" ImageUrl="../images/IconDetail.gif"></asp:HyperLink>&nbsp;
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic"
                ErrorMessage="Harap Pilih Asset" ControlToValidate="txtDescription" CssClass ="validator_general"></asp:RequiredFieldValidator>
            <input type="hidden" id="hdnAssetCode" runat="server" name="hdnAssetCode" class="inptype" />
            <input type="hidden" id="hdnAssetTypeID" runat="server" name="hdnAssetTypeID" class="inptype" />
            <input type="hidden" id="hdnBranchID" runat="server" name="hdnBranchID" class="inptype" />
        </td>
    </tr>
</table>
