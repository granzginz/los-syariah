﻿Imports Maxiloan.Controller

Public Class UcMaxAccCollList
    Inherits ControlBased

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Public Event EventEdit(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)

    Public Property ApplicationId As String
        Set(ByVal value As String)
            ViewState("ucApplicationId") = value
        End Set
        Get
            Return ViewState("ucApplicationId").ToString
        End Get
    End Property
    Public Property CGID As String
        Set(ByVal value As String)
            ViewState("CGID") = value
        End Set
        Get
            Return ViewState("CGID").ToString
        End Get
    End Property
    Public Property Sort As String
        Set(ByVal value As String)
            ViewState("Sort") = value
        End Set
        Get
            Return ViewState("Sort").ToString
        End Get
    End Property    

    Public Property Edit As Boolean
        Set(ByVal value As Boolean)
            dtgColl.Columns(0).Visible = value
        End Set
        Get
            Return dtgColl.Columns(0).Visible
        End Get
    End Property

    Public Property WhereCond As String
        Set(ByVal value As String)
            ViewState("WhereCond") = value
        End Set
        Get
            Return ViewState("WhereCond").ToString
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub
    Public Sub DoBind()
        Dim cPaging As New GeneralPagingController
        Dim oPaging As New Parameter.GeneralPaging
        Dim dtEntity As System.Data.DataTable = Nothing

        With oPaging
            .strConnection = GetConnectionString()            
            .WhereCond = Me.WhereCond
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = Me.Sort
            .SpName = "spCollectorAccountPaging"
        End With

        oPaging = cPaging.GetGeneralPaging(oPaging)

        If Not oPaging Is Nothing Then
            dtEntity = oPaging.ListData
            recordCount = oPaging.TotalRecords
        Else
            recordCount = 0
        End If

        dtgColl.DataSource = dtEntity.DefaultView
        dtgColl.CurrentPageIndex = 0
        dtgColl.DataBind()
        PagingFooter()

    End Sub
#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            'ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        DoBind()
    End Sub
    Private Sub imgbtnPageNumb_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int32)
                DoBind()
            End If
        End If
    End Sub
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgColl.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        DoBind()
    End Sub
#End Region
    Private Sub dtgColl_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgColl.ItemCommand                
        If e.CommandName = "Edit" Then
            RaiseEvent EventEdit(source, e)
        End If
    End Sub
    
End Class