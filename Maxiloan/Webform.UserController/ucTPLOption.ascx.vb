﻿Imports Maxiloan.Controller
Imports Maxiloan.Parameter

Public Class ucTPLOption
    Inherits ControlBased

    Public Event SelectedIndexChanged(ByVal index As Integer)
    Public Delegate Sub SelectedIndexChangedHandler(ByVal index As Integer)

    Public Property GridIndex As Integer
        Set(ByVal value As Integer)
            ViewState("GridIndex") = value
        End Set
        Get
            Return CType(ViewState("GridIndex"), Integer)
        End Get
    End Property

    Public Property SelectedValue As String
        Set(ByVal value As String)
            ddlTPLOption.SelectedIndex = ddlTPLOption.Items.IndexOf(ddlTPLOption.Items.FindByValue(value))
        End Set
        Get
            Return ddlTPLOption.SelectedValue
        End Get
    End Property

    Private Sub ddlTPL_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTPLOption.SelectedIndexChanged
        RaiseEvent SelectedIndexChanged(Me.GridIndex)
    End Sub


    Public Function getDefaultTPLAmount(ByVal OTRAmount As Double) As Double
        Dim generalSet As New Parameter.GeneralSetting
        Dim conGeneral As New GeneralSettingController

        If Me.SelectedValue = "0" Then Return 0

        With generalSet
            .strConnection = GetConnectionString()

            If OTRAmount <= 150000000 Then
                .GSID = "TPLOTR<150"
            Else
                .GSID = "TPLOTR>150"
            End If
        End With

        generalSet = conGeneral.GetGeneralSettingByID(generalSet)

        If generalSet.ListData.Rows.Count > 0 Then
            Return CDbl(generalSet.ListData.Rows(0).Item("GSValue").ToString.Trim)
        Else
            Return 0
        End If
    End Function

End Class