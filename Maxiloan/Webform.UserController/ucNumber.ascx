﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucNumber.ascx.vb" Inherits="Maxiloan.Webform.UserController.ucNumber" %>
<asp:TextBox ID="txtAmount" runat="server" Width="128px" ></asp:TextBox>
<asp:RequiredFieldValidator ID="rfvAmount" runat="server" ControlToValidate="txtAmount"
    ErrorMessage="You have to fill Amount" Visible="True" Enabled="True" Display="Dynamic" CssClass ="validator_general"></asp:RequiredFieldValidator>
<asp:RegularExpressionValidator ID="rvfAmount" runat="server" ControlToValidate="txtAmount"
    ErrorMessage="You have to fill numeric value" Enabled="True" ValidationExpression="^\d{1,}$|^\d{1,}[.{1}]\d{1,}$"
    Visible="True" Display="Dynamic"></asp:RegularExpressionValidator>
<asp:RangeValidator ID="rgValAmount" runat="server" font-name="Verdana" Font-Size="11px"
    Type="Double" ControlToValidate="txtAmount" MinimumValue="0" MaximumValue="9999"
    Visible="True" Display="Dynamic"></asp:RangeValidator>
