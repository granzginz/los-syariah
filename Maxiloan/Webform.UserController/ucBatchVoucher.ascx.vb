﻿#Region "Import"
Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Interface
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
#End Region

Public Class ucBatchVoucher
    Inherits ControlBased

    Private oController As New DataUserControlController
    Private dtBankAccount As New DataTable

#Region "Property"
#Region "PaymentDetail"
    Public Property WayOfPayment() As String
        Get
            Return CType(cmbWayOfPayment.SelectedItem.Value.Trim, String)
        End Get
        Set(ByVal Value As String)
            cmbWayOfPayment.SelectedIndex = cmbWayOfPayment.Items.IndexOf(cmbWayOfPayment.Items.FindByValue(Value))
        End Set
    End Property

    Public ReadOnly Property WayOfPaymentName() As String
        Get
            Return CType(cmbWayOfPayment.SelectedItem.Text.Trim, String)
        End Get
    End Property

    Public ReadOnly Property BankAccount() As String
        Get
            'Return CType(hdnBankAccount.Value.Trim, String)
            Return CType(cmbBankAccount.SelectedItem.Value.Trim, String)
        End Get
    End Property

    Public ReadOnly Property BankAccountName() As String
        Get
            'Return CType(ViewState("BankAccountName"), String)
            Return CType(cmbBankAccount.SelectedItem.Text.Trim, String)
        End Get
    End Property

    Public Property Notes() As String
        Get
            Return CType(txtNotes.Text.Trim, String)
        End Get
        Set(ByVal Value As String)
            txtNotes.Text = Value.Trim
        End Set
    End Property

    Public Property ViewWayOfPayment() As String
        Get
            Return lblWayOfPayment.Text.Trim
        End Get
        Set(ByVal Value As String)
            lblWayOfPayment.Text = Value
        End Set
    End Property

    Public Property ViewBankAccountID() As String
        Get
            Return lblBankAccountID.Text.Trim
        End Get
        Set(ByVal Value As String)
            lblBankAccountID.Text = Value
        End Set
    End Property
#End Region
    Private Property ListBankAccount() As DataTable
        Get
            Return (CType(ViewState("ListBankAccount"), DataTable))
        End Get
        Set(ByVal Value As DataTable)
            ViewState("ListBankAccount") = Value
        End Set
    End Property

    Public Property IsTitle() As Boolean
        Get
            Return (CType(ViewState("IsTitle"), Boolean))
        End Get
        Set(ByVal Value As Boolean)
            ViewState("IsTitle") = Value
        End Set
    End Property

    Public Property WithOutPrepaid() As Boolean
        Get
            Return (CType(ViewState("withoutprepaid"), Boolean))
        End Get
        Set(ByVal Value As Boolean)
            ViewState("withoutprepaid") = Value
        End Set
    End Property

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Dim objrow As DataRow()

        If Not IsPostBack Then
            'If Me.IsTitle Then
            '    pnltopi.Visible = False
            'Else
            '    pnltopi.Visible = True
            'End If
            BindWayOfPayment()
            BindBankAccount()
        End If
        'If Me.IsTitle Then
        '    pnltopi.Visible = False
        'Else
        '    pnltopi.Visible = True
        'End If
        'ViewState("BankAccountName") = ""
        'If Not IsNothing(hdnBankAccount.Value) Then

        '    objrow = Me.ListBankAccount.Select(" ID = '" & hdnBankAccount.Value.Trim & "'")
        '    If objrow.Length > 0 Then
        '        ViewState("BankAccountName") = CStr(objrow(0)("Name"))
        '    End If
        'End If
    End Sub

    Public Sub resetBankAccount()
        cmbWayOfPayment.SelectedIndex = 0
    End Sub

#Region "Bind Bank Account"
    Private Sub BindBankAccount()
        dtBankAccount = CType(Me.Cache.Item(CACHE_BANKACCOUNT & Me.sesBranchId.Replace("'", "")), DataTable)
        If dtBankAccount Is Nothing Then
            Dim dtBankAccountCache As New DataTable
            dtBankAccountCache = oController.GetBankAccount(GetConnectionString, Me.sesBranchId, "", "")
            Me.Cache.Insert(CACHE_BANKACCOUNT & Me.sesBranchId.Replace("'", ""), dtBankAccountCache, Nothing, DateTime.Now.AddHours(DEFAULT_TIME_DURATION_CACHE_HOUR), TimeSpan.Zero)
            dtBankAccount = CType(Me.Cache.Item(CACHE_BANKACCOUNT & Me.sesBranchId.Replace("'", "")), DataTable)
        End If
        Me.ListBankAccount = dtBankAccount
        'scriptJava.InnerHtml = GenerateScript(dtBankAccount)
    End Sub
#End Region

#Region "Bind WayOfPayment"
    Private Sub BindWayOfPayment()
        Dim oDataTable As DataTable = New DataTable
        Dim oRow As DataRow
        Dim splitListData() As String
        Dim splitRow() As String
        Dim i As Integer
        Dim strListData As String

        oDataTable.Columns.Add("ID", System.Type.GetType("System.String"))
        oDataTable.Columns.Add("Description", System.Type.GetType("System.String"))

        oRow = oDataTable.NewRow()
        oRow("ID") = ""
        oRow("Description") = ""
        If Me.WithOutPrepaid Then
            strListData = "CA,Cash-BA,Bank"
        Else
            strListData = "CA,Cash-BA,Bank-CP,Prepaid"
        End If

        splitListData = Split(strListData, "-")
        For i = 0 To UBound(splitListData)
            splitRow = Split(splitListData(i), ",")
            oRow("ID") = splitRow(0)
            oRow("Description") = splitRow(1)
            oDataTable.Rows.Add(oRow)
            oRow = oDataTable.NewRow()
        Next

        cmbWayOfPayment.DataValueField = "ID"
        cmbWayOfPayment.DataTextField = "Description"
        cmbWayOfPayment.DataSource = oDataTable
        cmbWayOfPayment.DataBind()
        cmbWayOfPayment.Items.Insert(0, "Select One")
        cmbWayOfPayment.Items(0).Value = "0"
    End Sub
#End Region

#Region "Generate Script"
    Protected Function WOPonChange() As String
        hdnBankAccount.Value = ""
        Return "WOPChange('" & Trim(cmbWayOfPayment.ClientID) & "','" & _
                Trim(cmbBankAccount.ClientID) & "','" & _
                Trim(hdnBankAccount.ClientID) & "'" & _
                ",((this.selectedIndex == -1) ? null : ListData[this.selectedIndex]));"
    End Function

    Private Function GenerateScript(ByVal DtTable As DataTable) As String
        Dim strScript As String
        Dim strScript1 As String
        Dim strType As String
        Dim DataRow As DataRow()
        Dim i As Int32
        Dim j As Int32
        Dim k As Int32
        strScript = "<script language=""JavaScript"">" & vbCrLf
        strScript &= " var a; " & vbCrLf
        strScript &= "ListData = new Array(" & vbCrLf
        strType = ""
        If Me.WithOutPrepaid Then
            k = cmbWayOfPayment.Items.Count - 1
        Else
            k = cmbWayOfPayment.Items.Count - 2
        End If

        For j = 0 To k
            DataRow = DtTable.Select(" Type = '" & Left(cmbWayOfPayment.Items(j).Value.Trim, 1) & "'")
            If DataRow.Length > 0 Then
                For i = 0 To DataRow.Length - 1
                    If strType <> CStr(DataRow(i)("Type")).Trim Then
                        strType = CStr(DataRow(i)("Type")).Trim
                        strScript &= "new Array(" & vbCrLf
                        strScript1 = ""
                    End If
                    If strScript1 = "" Then
                        strScript1 = " new Array('" & CStr(DataRow(i)("Name")).Trim & "','" & CStr(DataRow(i)("ID")).Trim & "') "
                    Else
                        strScript1 &= "," & vbCrLf & " new Array('" & CStr(DataRow(i)("Name")).Trim & "','" & CStr(DataRow(i)("ID")).Trim & "') "
                    End If
                Next
                strScript &= strScript1 & ")," & vbCrLf
            Else
                strScript &= " null," & vbCrLf
            End If
        Next
        If Right(strScript.Trim, 5) = "null," Then
            strScript = Left(strScript.Trim, strScript.Trim.Length - 1)
        Else
            strScript = Left(strScript.Trim, strScript.Trim.Length - 2)
        End If

        strScript &= vbCrLf & "));" & vbCrLf
        strScript &= "</script>"
        Return strScript
    End Function

#End Region

    Private Sub cmbWayOfPayment_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbWayOfPayment.SelectedIndexChanged
        ' Dim bankType As String = ""
        ' dtBankAccount = CType(Me.Cache.Item(CACHE_BANKACCOUNT & Me.sesBranchId.Replace("'", "")), DataTable)
        ' If dtBankAccount Is Nothing Then
        Dim dtBankAccountCache As New DataTable
        dtBankAccountCache = oController.GetBankAccount(GetConnectionString, Me.sesBranchId, cmbWayOfPayment.SelectedValue, "")
        'Me.Cache.Insert(CACHE_BANKACCOUNT & Me.sesBranchId.Replace("'", ""), dtBankAccountCache, Nothing, DateTime.Now.AddHours(DEFAULT_TIME_DURATION_CACHE_HOUR), TimeSpan.Zero)
        ' dtBankAccount = CType(Me.Cache.Item(CACHE_BANKACCOUNT & Me.sesBranchId.Replace("'", "")), DataTable)
        'End If
        cmbBankAccount.DataSource = dtBankAccountCache
        cmbBankAccount.DataTextField = "Name"
        cmbBankAccount.DataValueField = "ID"
        cmbBankAccount.DataBind()
    End Sub
End Class