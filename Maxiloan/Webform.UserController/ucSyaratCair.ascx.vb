﻿Imports Maxiloan.Controller
Public Class ucSyaratCair
    Inherits ControlBased
    Private oController As New ApplicationController
    Protected WithEvents ucDate As ucDateCE
    Dim oRow As DataRow
    Public Property oData() As DataTable
        Get
            Return CType(ViewState("oData"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            ViewState("oData") = Value
        End Set
    End Property
    Public Property UseNew() As String
        Get
            Return CType(ViewState("UseNew"), String)
        End Get
        Set(value As String)
            ViewState("UseNew") = value
        End Set
    End Property
    Public Sub BindData()
        Dim oApplication As New Parameter.Application
        Dim oData As New DataTable

        oApplication.strConnection = GetConnectionString()
        oApplication.UseNew = UseNew
        oApplication = oController.GetSyaratCair(oApplication)

        If Not oApplication Is Nothing Then
            oData = oApplication.ListData
        End If

        dtgSyaratCair.DataSource = oData.DefaultView
        dtgSyaratCair.DataBind()
    End Sub
    Private Sub dtgSyaratCair_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgSyaratCair.ItemDataBound
        Dim lblIsDocExist As New Label
        Dim lblNo As New Label
        Dim today As Date = Date.Today
        Dim lblChkSyaratCair As Label
        Dim isMandatory As CheckBox

        lblNo = CType(e.Item.FindControl("lblNo"), Label)
        lblChkSyaratCair = CType(e.Item.FindControl("lblChkSyaratCair"), Label)
        isMandatory = CType(e.Item.FindControl("isMandatory"), CheckBox)

        If e.Item.ItemIndex >= 0 Then
            lblNo.Text = (e.Item.ItemIndex + 1).ToString
            If isMandatory.Checked = True Then
                lblChkSyaratCair.Visible = True
            Else
                lblChkSyaratCair.Visible = False
            End If
        End If
    End Sub
    Public Function ChkValidator() As Boolean
        Dim chk As CheckBox
        Dim lblChkSyaratCair As Label
        Dim status As Boolean = True
        Dim oTable As New DataTable
        Dim isMandatory As CheckBox

        oTable.Columns.Add("IdSyaratCair", GetType(String))
        oTable.Columns.Add("Notes", GetType(String))
        oTable.Columns.Add("PromiseDate", GetType(String))


        For intLoop = 0 To dtgSyaratCair.Items.Count - 1
            chk = CType(dtgSyaratCair.Items(intLoop).FindControl("chk"), CheckBox)
            lblChkSyaratCair = CType(dtgSyaratCair.Items(intLoop).FindControl("lblChkSyaratCair"), Label)
            isMandatory = CType(dtgSyaratCair.Items(intLoop).FindControl("isMandatory"), CheckBox)

            If isMandatory.Checked = True Then
                If chk.Checked = True Then
                    lblChkSyaratCair.Visible = False
                    oRow = oTable.NewRow
                    oRow("IdSyaratCair") = dtgSyaratCair.Items(intLoop).Cells(2).Text.Trim
                    oRow("Notes") = CType(dtgSyaratCair.Items(intLoop).FindControl("txtNotes"), TextBox).Text.Trim
                    oRow("PromiseDate") = CType(dtgSyaratCair.Items(intLoop).FindControl("txtPromiseDate"), TextBox).Text.Trim
                    oTable.Rows.Add(oRow)
                    oData = oTable
                Else
                    lblChkSyaratCair.Visible = True
                    status = False
                End If
            Else
                If chk.Checked = True Then
                    lblChkSyaratCair.Visible = False
                    oRow = oTable.NewRow
                    oRow("IdSyaratCair") = dtgSyaratCair.Items(intLoop).Cells(2).Text.Trim
                    oRow("Notes") = CType(dtgSyaratCair.Items(intLoop).FindControl("txtNotes"), TextBox).Text.Trim
                    oRow("PromiseDate") = CType(dtgSyaratCair.Items(intLoop).FindControl("txtPromiseDate"), TextBox).Text.Trim
                    oTable.Rows.Add(oRow)
                    oData = oTable
                End If
            End If

        Next
        Return status
    End Function
End Class