﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucLookupProductOffering.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.ucLookupProductOffering" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:UpdatePanel runat="server" ID="upProdOff1" UpdateMode="Conditional">
    <ContentTemplate>
        <div class="form_box_usercontrol">
            <div class="form_left_usercontrol">
                <label class="label_req">Produk Jual</label>
                <asp:TextBox runat="server" ID="txtProductOffering" CssClass="long_text"></asp:TextBox>
                <asp:Button runat="server" ID="btnLookupProducOffering" Text="..." CausesValidation="false"
                    CssClass="small buttongo blue"></asp:Button>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Pilih product offering!"
                    Display="Dynamic" ControlToValidate="txtProductOffering" SetFocusOnError="True"
                    CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <asp:Button runat="server" ID="btnLookup" Style="display: none;" Text="Lookup" />
        <asp:ModalPopupExtender ID="ModalPopupExtender1" runat="server" PopupControlID="Panel1"
            BackgroundCssClass="wpbg" TargetControlID="btnLookup" CancelControlID="btnExit">
        </asp:ModalPopupExtender>
        <asp:Panel runat="server" ID="Panel1">
            <div class="wp">
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
                <asp:ImageButton runat="server" ID="btnExit" ImageUrl="../Images/exit_lookup00.png"
                    CssClass="wpbtnexit" />
                <div class="form_title">
                    <div class="title_strip">
                    </div>
                    <div class="form_single">
                        <h4>
                            DAFTAR PRODUCT OFFERING
                        </h4>
                    </div>
                </div>
                <asp:Panel ID="pnlList" runat="server">
                    <asp:Panel ID="pnlProductOfferingList" runat="server">
                        <div class="form_box_header">
                            <div class="form_single">
                                <div class="grid_wrapper_ws">
                                    <asp:DataGrid ID="dtgProductOffering" runat="server" Width="800px" AllowPaging="True"
                                        AllowSorting="True" AutoGenerateColumns="False" DataKeyField="productofferingid"
                                        BorderStyle="None" BorderWidth="1px" OnSortCommand="SortGrid" CellPadding="0"
                                        CssClass="grid_general">
                                        <HeaderStyle CssClass="th" />
                                        <ItemStyle CssClass="item_grid" />
                                        <Columns>
                                            <asp:ButtonColumn CommandName="Select" Text="SELECT"></asp:ButtonColumn>
                                            <asp:TemplateColumn SortExpression="productofferingid" HeaderText="PRODUCT OFFERING">
                                                <ItemStyle HorizontalAlign="Left" Width="30%"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="hynProductOffering" runat="server" Text='<%#Container.DataItem("ProductOfferingId")%>'>
                                                    </asp:HyperLink>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn SortExpression="description" HeaderText="DESCRIPTION">
                                                <ItemStyle HorizontalAlign="Left" Width="60%"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDescription" runat="server" Text='<%#Container.DataItem("Description")%>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="PRODUCT OFFERING" Visible="False">
                                                <ItemStyle HorizontalAlign="Left" Width="30%"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblProductOfferingID" runat="server" Text='<%#Container.DataItem("ProductOfferingId")%>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="PRODUCT ID" Visible="False">
                                                <ItemStyle HorizontalAlign="Left" Width="30%"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblProductID" runat="server" Text='<%#Container.DataItem("ProductID")%>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="AssetTypeID" Visible="False">
                                                <ItemStyle HorizontalAlign="Left" Width="30%"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAssetTypeID" runat="server" Text='<%#Container.DataItem("AssetTypeID")%>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                        </Columns>
                                        <PagerStyle Visible="False" HorizontalAlign="Left" ForeColor="#000066" BackColor="White"
                                            Mode="NumericPages"></PagerStyle>
                                    </asp:DataGrid>
                                </div>
                                <div class="button_gridnavigation">
                                    <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../Images/grid_navbutton01.png"
                                        CausesValidation="False" CommandName="First" OnCommand="NavigationLink_Click">
                                    </asp:ImageButton>
                                    <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../Images/grid_navbutton02.png"
                                        CausesValidation="False" CommandName="Prev" OnCommand="NavigationLink_Click">
                                    </asp:ImageButton>
                                    <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../Images/grid_navbutton03.png"
                                        CausesValidation="False" CommandName="Next" OnCommand="NavigationLink_Click">
                                    </asp:ImageButton>
                                    <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../Images/grid_navbutton04.png"
                                        CausesValidation="False" CommandName="Last" OnCommand="NavigationLink_Click">
                                    </asp:ImageButton>
                                    Page
                                    <asp:TextBox ID="txtGoPage" runat="server" Width="34px" CssClass="txtPage">1</asp:TextBox>
                                    <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtGoPage" MinimumValue="1"
                                        MaximumValue="999" ErrorMessage="Page No. is not valid" Type="Double" Display="Dynamic"></asp:RangeValidator>
                                    <asp:Button ID="btnGoPage" runat="server" EnableViewState="False" Text="Go" CssClass="small buttongo blue" />
                                    <asp:RangeValidator ID="Rangevalidator1" runat="server" Type="Integer" MaximumValue="999999999"
                                        ErrorMessage="Page No. is not valid" MinimumValue="1" ControlToValidate="txtGoPage"
                                        Display="Dynamic"></asp:RangeValidator>
                                    <asp:RequiredFieldValidator ID="rfvGo" runat="server" ErrorMessage="Page No. is not valid"
                                        ControlToValidate="txtGoPage" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                                </div>
                                <div class="navigasi_totalrecord">
                                    <asp:Label ID="lblPage" runat="server"></asp:Label>of
                                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                                    <asp:Label ID="lblTotRec" runat="server"></asp:Label>record(s)
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:Panel runat="server" ID="PanelSearch">
                        <div class="form_box_title">
                            <div class="form_single">
                                <h4>
                                    CARI PRODUCT OFFERING</h4>
                            </div>
                        </div>
                        <div class="form_box">
                            <div class="form_single">
                                <label>
                                    Product</label>
                                <asp:DropDownList ID="cboProductBranch" runat="server">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="form_button">
                            <asp:Button ID="btnSearch" runat="server" Text="Find" CssClass="small button blue"
                                CausesValidation="False" />
                            <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="small button gray"
                                CausesValidation="False" />
                            <asp:UpdateProgress runat="server" ID="upgLookupProdOff1" AssociatedUpdatePanelID="upProdOff1">
                                <ProgressTemplate>
                                    <img alt="Loading..." src="../Images/pic-loader.gif" />
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                        </div>
                    </asp:Panel>
                </asp:Panel>
            </div>
        </asp:Panel>
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
        <asp:AsyncPostBackTrigger ControlID="imbFirstPage" EventName="Command" />
        <asp:AsyncPostBackTrigger ControlID="imbPrevPage" EventName="Command" />
        <asp:AsyncPostBackTrigger ControlID="imbNextPage" EventName="Command" />
        <asp:AsyncPostBackTrigger ControlID="imbLastPage" EventName="Command" />
        <asp:AsyncPostBackTrigger ControlID="btnGoPage" EventName="Click" />
        <asp:AsyncPostBackTrigger ControlID="btnLookupProducOffering" EventName="Click" />
    </Triggers>
</asp:UpdatePanel>
