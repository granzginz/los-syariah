﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucCombo.ascx.vb" Inherits="Maxiloan.Webform.UserController.ucCombo" %>
<%@ Register TagPrefix="uc1" TagName="ValidDate" Src="ValidDate.ascx" %>
<link href="../include/collection.css" type="text/css" rel="stylesheet">
<div id="Header" runat="server">
</div>
<script language="JavaScript" type="text/javascript">
    var hdnDetail;
    function WOPChange(pCmbOfPayment, pBankAccount, pHdnDetail, itemArray) {
        hdnDetail = eval('document.forms[0].' + pHdnDetail);

        eval('document.forms[0].' + pBankAccount).disabled = false;


        var i, j;
        for (i = eval('document.forms[0].' + pBankAccount).options.length; i >= 0; i--) {
            eval('document.forms[0].' + pBankAccount).options[i] = null

        };
        if (itemArray == null) {
            j = 0;
        }
        else {
            j = 1;
        };
        eval('document.forms[0].' + pBankAccount).options[0] = new Option('Select One');
        eval('document.forms[0].' + pBankAccount).options[0].value = '0';
        if (itemArray != null) {
            for (i = 0; i < itemArray.length; i++) {
                eval('document.forms[0].' + pBankAccount).options[j] = new Option(itemArray[i][1]);
                if (itemArray[i][1] != null) {
                    eval('document.forms[0].' + pBankAccount).options[j].value = itemArray[i][1];
                };
                j++;
            };
            eval('document.forms[0].' + pBankAccount).selected = true;
        };


        if (eval('document.forms[0].' + pCmbOfPayment).selectedIndex == 4) {
            eval('document.forms[0].' + pBankAccount).disabled = true;
        }
        if (eval('document.forms[0].' + pCmbOfPayment).selectedIndex == 5) {
            eval('document.forms[0].' + pBankAccount).disabled = true;
        }
        if (eval('document.forms[0].' + pCmbOfPayment).selectedIndex == 2) {
            eval('document.forms[0].' + pBankAccount).disabled = true;
        }
        if (eval('document.forms[0].' + pCmbOfPayment).selectedIndex == 7) {
            eval('document.forms[0].' + pBankAccount).disabled = true;
        }
    }



    function cboChildonChange(l) {
        hdnDetail.value = l;
    }



    function SetcboChildFocus() {
        var SPV;
        SPV = hdnDetail.value;
        alert(SPV);

        for (i = document.forms[0].cboChild.options.length; i >= 0; i--) {
            if (document.forms[0].cboChild.options.options[i].value = SPV) {
                document.forms[0].cboChild.options[i].selected = true;
            }
        };
    }
</script>
<input id="hdnSP" type="hidden" name="hdnSP" runat="server" />
<table class="tablegrid" id="Table1" cellspacing="1" cellpadding="3" width="95%"
    align="center" border="0">
    <tr>
        <td class="tdgenap" style="height: 10px" width="25%">
            Collector Type
        </td>
        <td class="tdganjil" style="height: 10px" width="75%">
            <asp:DropDownList ID="cboParent" runat="server" onchange="<%#WOPonChange()%>" >
            </asp:DropDownList>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" InitialValue="0"
                Display="Dynamic" ControlToValidate="cboParent" Width="296px" ErrorMessage="Please select Collector Type" CssClass ="validator_general"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td class="tdgenap" width="25%">
            Supervisor ID
        </td>
        <td class="tdganjil" width="25%">
            <asp:DropDownList ID="cboChild" runat="server" onchange="cboChildonChange(this.options[this.selectedIndex].value);"
                >
            </asp:DropDownList>
        </td>
    </tr>
</table>
