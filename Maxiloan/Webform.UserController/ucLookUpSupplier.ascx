﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucLookUpSupplier.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.ucLookUpSupplier" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:UpdatePanel runat="server" ID="UpdatePanel1">
    <ContentTemplate>
        <asp:Button runat="server" ID="btnLookup" Text="Lookup" Style="display: none;" />
        <asp:ModalPopupExtender runat="server" ID="ModalPopupExtender1" TargetControlID="btnLookup"
            PopupControlID="Panel1" CancelControlID="btnExit" BackgroundCssClass="wpbg">
        </asp:ModalPopupExtender>
        <asp:Panel runat="server" ID="Panel1">
            <div class="wp">
                <asp:Label runat="server" ID="lblMessage"></asp:Label>
                <asp:ImageButton runat="server" ID="btnExit" ImageUrl="../Images/exit_lookup00.png"
                    CssClass="wpbtnexit" />
                <div class="form_box_title">
                    <div class="title_strip">
                    </div>
                    <div class="form_single">
                        <h4>
                            DAFTAR SUPPLIER
                        </h4>
                    </div>
                </div>
                <asp:Panel ID="pnlList" runat="server">
                    <asp:Panel ID="pnlSupplierList" runat="server" Visible="false">
                        <div class="form_box_header">
                            <div class="form_single">
                                <div class="grid_wrapper_ws">
                                    <asp:DataGrid ID="dtgSupplier" runat="server" CellPadding="0" OnSortCommand="SortGrid"
                                        BorderStyle="None" DataKeyField="SupplierID" AutoGenerateColumns="False" AllowSorting="True"
                                        AllowPaging="True" Visible="true" Width="1000px" CssClass="grid_general">
                                        <HeaderStyle CssClass="th" />
                                        <ItemStyle CssClass="item_grid" />
                                        <Columns>
                                            <asp:ButtonColumn ButtonType="LinkButton" CommandName="Select" Text="SELECT" ItemStyle-CssClass="command_col" ></asp:ButtonColumn>
                                            <asp:TemplateColumn SortExpression="SupplierId" HeaderText="SUPPLIER ID" Visible="False">
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="hynSupplier" runat="server" Text='<%#Container.DataItem("SupplierId")%>'>
                                                    </asp:HyperLink>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn SortExpression="SupplierInitialName" HeaderText="SUPPLIER INITIAL NAME"
                                                Visible="false">
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="hynSupplierInitialName" runat="server" Text='<%#Container.DataItem("SupplierInitialName")%>'>
                                                    </asp:HyperLink>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn SortExpression="suppliername" HeaderText="SUPPLIER NAME" ItemStyle-CssClass="name_col" >
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSupplierName" runat="server" Text='<%#Container.DataItem("SupplierName")%>' >
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn SortExpression="address" HeaderText="SUPPLIER ADDRESS">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblsupplieraddress" runat="server" Text='<%#Container.DataItem("Address")%>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn SortExpression="phone" HeaderText="KONTAK">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblContactPerson1" runat="server" Text='<%#Container.DataItem("ContactPersonName")%>'>
                                                    </asp:Label>
                                                    <label class="label_auto" >
                                                        Telp.
                                                    </label>
                                                    <asp:Label ID="lblsupplierphone" runat="server" Text='<%#Container.DataItem("Phone")%>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn SortExpression="contactpersonname" HeaderText="CONTACT PERSON"
                                                Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblContactPerson" runat="server" Text='<%#Container.DataItem("ContactPersonName")%>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="SUPPLIER ID" Visible="False">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSupplierID" runat="server" Text='<%#Container.DataItem("SupplierId")%>'>
                                                    </asp:Label>
                                                    <asp:Label ID="lblNewAmountFee" runat="server" Text='<%#Container.DataItem("NewAmountFee")%>'>
                                                    </asp:Label>
                                                    <asp:Label ID="lblUsedAmountFee" runat="server" Text='<%#Container.DataItem("UsedAmountFee")%>'>
                                                    </asp:Label>
                                                    <asp:Label ID="lblPF" runat="server" Text='<%#Container.DataItem("PF")%>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                        </Columns>
                                        <PagerStyle Visible="false" />
                                    </asp:DataGrid>
                                </div>
                                <div class="button_gridnavigation">
                                    <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" ImageUrl="../Images/grid_navbutton01.png"
                                        OnCommand="NavigationLink_Click" CommandName="First"></asp:ImageButton>
                                    <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" ImageUrl="../Images/grid_navbutton02.png"
                                        OnCommand="NavigationLink_Click" CommandName="Prev"></asp:ImageButton>
                                    <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" ImageUrl="../Images/grid_navbutton03.png"
                                        OnCommand="NavigationLink_Click" CommandName="Next"></asp:ImageButton>
                                    <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" ImageUrl="../Images/grid_navbutton04.png"
                                        OnCommand="NavigationLink_Click" CommandName="Last"></asp:ImageButton>
                                    Page
                                    <asp:TextBox ID="txtGoPage" runat="server" Width="34px">1</asp:TextBox>
                                    <asp:Button ID="btnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                                        EnableViewState="False" />
                                    <asp:RangeValidator ID="Rangevalidator1" runat="server" Font-Size="11px" Type="Integer"
                                        MaximumValue="999999999" ErrorMessage="Page No. is not valid" MinimumValue="1"
                                        ControlToValidate="txtGoPage" Display="Dynamic"></asp:RangeValidator>
                                    <asp:RequiredFieldValidator ID="rfvGo" runat="server" Font-Size="11px" ErrorMessage="Page No. is not valid"
                                        ControlToValidate="txtGoPage" Display="Dynamic" CssClass ="validator_general"></asp:RequiredFieldValidator>
                                    <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtGoPage" MinimumValue="1"
                                        MaximumValue="999" ErrorMessage="Page No. is not valid!" Type="Double" Display="Dynamic"></asp:RangeValidator>
                                </div>
                                <div class="label_gridnavigation">
                                    <asp:Label ID="lblPage" runat="server"></asp:Label>of
                                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                                    <asp:Label ID="lblTotRec" runat="server"></asp:Label>record(s)
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:Panel runat="server" ID="PanelSearch">
                        <div class="form_box">
                            <div class="form_single">
                                <h4>
                                    CARI SUPPLIER</h4>
                            </div>
                        </div>
                        <div class="form_box">
                            <div class="form_single">
                                <label>
                                    Cari Berdasarkan
                                </label>
                                <asp:DropDownList ID="cboSearchBy" runat="server">
                                    <asp:ListItem Value="SupplierInitialName">Supplier Initial Name</asp:ListItem>
                                    <asp:ListItem Value="SupplierName">Supplier Name</asp:ListItem>
                                </asp:DropDownList>
                                <asp:TextBox ID="txtSearchBy" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form_button">
                            <asp:Button ID="btnSearch" runat="server" CausesValidation="False" Text="Find" CssClass="small button blue" />
                            <asp:Button ID="btnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray" />
                        </div>
                    </asp:Panel>
                </asp:Panel>
            </div>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
