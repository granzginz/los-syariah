﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucKabupaten.ascx.vb"
    Inherits="Maxiloan.Webform.UserController.ucKabupaten" %>
<div class="form_single">
    <label class="label_req">
        Kota/Kabupaten DATI II</label>
    <asp:TextBox ID="txtProductOfferingDescription" runat="server" CssClass="medium_text"></asp:TextBox>
    <asp:HyperLink ID="hpLookup" runat="server" ImageUrl="../images/IconDetail.gif"></asp:HyperLink>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtProductOfferingDescription"
        ErrorMessage="Pilih Kota/Kabupaten DATI II" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
    <input type="hidden" id="hdnProductID" runat="server" name="hdnProductID" />
    <input type="hidden" id="hdnProductOfferingID" runat="server" name="hdnProductOfferingID" />    
</div>
