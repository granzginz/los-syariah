﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UcLookUpChartOfAccount.ascx.vb" 
    Inherits="Maxiloan.Webform.UserController.UcLookUpChartOfAccount" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<div runat="server" id="jlookupContent" />
<input type="hidden" id="hdnCoAId" runat="server" class="inptype" />
<div>
    <div>
        <label class="label_req">
            Chart of Account
        </label>
        <asp:TextBox ID="txtCoaName" runat="server"
            CssClass="medium_text"></asp:TextBox>
             <button class="small buttongo blue" 
                             onclick ="OpenJLookup('<%= ResolveClientUrl("~/webform.Utility/jLookup/ChatOfAccount.aspx?kode=" & hdnCoAId.ClientID & "&nama=" & txtCoaName.ClientID)%>','Daftar Chart Of Account','<%= jlookupContent.ClientID %>');return false;">...</button>  
        <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" ControlToValidate="txtCoaName"
            ErrorMessage="Harap dipilih Unit" Visible="True" Enabled="True" CssClass="validator_general"></asp:RequiredFieldValidator>
    </div>
</div>
