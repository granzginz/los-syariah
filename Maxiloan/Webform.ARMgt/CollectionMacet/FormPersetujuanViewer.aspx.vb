﻿#Region "Imports"
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class FormPersetujuanViewer
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"

#End Region
#Region "Property"
 
    Private Property ApplicationId() As String
        Get
            Return CType(ViewState("ApplicationId"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationId") = Value
        End Set
    End Property
   
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not Page.IsPostBack Then
            Me.FormID = "FormPersetujuanPrint"
            BindReport()
        End If
    End Sub


    Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies("FormPersetujuanPrint")
        Me.ApplicationId = cookie.Values("ApplicationId")
    End Sub


    Sub BindReport()
        Dim m_controller As New InventoryAppraisalController
        Dim oCustomClass As New Parameter.InventoryAppraisal
        Dim oCustomClassSub As New Parameter.InventoryAppraisal

        Dim objReport As FormPersetujuanPrint = New FormPersetujuanPrint
        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions

        GetCookies()

        With oCustomClass
            .strConnection = GetConnectionString()
            .ApplicationID = Me.ApplicationId
            .BusinessDate = Me.BusinessDate
        End With

        m_controller.PrintFormPersetujuanAppraisal(oCustomClass)

        objReport.SetDataSource(oCustomClass.ListReport)
        With oCustomClassSub
            .strConnection = GetConnectionString()
            .ApplicationID = Me.ApplicationId

        End With

        m_controller.PrintFormPersetujuanAppraisalSubBid(oCustomClassSub)
        objReport.Subreports(0).SetDataSource(oCustomClassSub.ListReport)
        crPrintBPK.ReportSource = objReport
        crPrintBPK.Visible = True
        crPrintBPK.DataBind()


        objReport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
        objReport.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat

        Dim strFileLocation As String
        strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
        strFileLocation += Me.Session.SessionID + Me.Loginid + "FormPersetujuanAppraisal.pdf"
        DiskOpts.DiskFileName = strFileLocation
        'oPPK.PrintOptions.PaperSize = PaperSize.PaperA4
        objReport.ExportOptions.DestinationOptions = DiskOpts
        objReport.Export()
        objReport.Close()
        objReport.Dispose()
        Response.Redirect("FormPersetujuan.aspx?file=" & Me.Session.SessionID & Me.Loginid & "FormPersetujuanAppraisal.pdf")

    End Sub
End Class