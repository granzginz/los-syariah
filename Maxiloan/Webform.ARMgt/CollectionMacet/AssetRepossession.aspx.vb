﻿#Region "Imports"
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Parameter.AssetRepo
#End Region

Public Class AssetRepossession
    Inherits Maxiloan.Webform.WebBased    
    Protected WithEvents oCGID As UcBranchCollection
    Protected WithEvents txtReposessExpense As ucNumberFormat
    Protected WithEvents txtKMAsset As ucNumberFormat
    Protected WithEvents GridNavigator As ucGridNav
#Region "properties"

    Public Property CustomerID() As String
        Get
            Return CType(viewstate("CustomerID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerID") = Value
        End Set
    End Property

    Public Property RepossesSeqNo() As Integer
        Get
            Return CType(Viewstate("RepossesSeqNo"), Integer)
        End Get
        Set(ByVal Value As Integer)
            Viewstate("RepossesSeqNo") = Value
        End Set
    End Property

    Public Property AssetSeqNo() As Integer
        Get
            Return CType(Viewstate("AssetSeqNo"), Integer)
        End Get
        Set(ByVal Value As Integer)
            Viewstate("AssetSeqNo") = Value
        End Set
    End Property

    Public Property UntilDate() As Date
        Get
            Return CType(Viewstate("UntilDate"), Date)
        End Get
        Set(ByVal Value As Date)
            Viewstate("UntilDate") = Value
        End Set
    End Property

    Public Property RALDate() As Date
        Get
            Return CType(Viewstate("RALDate"), Date)
        End Get
        Set(ByVal Value As Date)
            Viewstate("RALDate") = Value
        End Set
    End Property

    Public Property RALEndDate() As Date
        Get
            Return CType(Viewstate("RALEndDate"), Date)
        End Get
        Set(ByVal Value As Date)
            Viewstate("RALEndDate") = Value
        End Set
    End Property

    Public Property AssetTypeID() As String
        Get
            Return CType(Viewstate("AssetTypeID"), String)
        End Get
        Set(ByVal Value As String)
            Viewstate("AssetTypeID") = Value
        End Set
    End Property

    Public Property AllPage() As String
        Get
            Return CType(Viewstate("AllPage"), String)
        End Get
        Set(ByVal Value As String)
            Viewstate("AllPage") = Value
        End Set
    End Property

    Public Property cgid() As String
        Get
            Return CType(viewstate("CGID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CGID") = Value
        End Set
    End Property

    Public Property AgreementNo() As String
        Get
            Return CType(viewstate("AgreementNo"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AgreementNo") = Value
        End Set
    End Property

    Public Property ApplicationID() As String
        Get
            Return CType(viewstate("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property


    Public Property RALNo() As String
        Get
            Return CType(viewstate("RALNo"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("RALNo") = Value
        End Set
    End Property

    Public Property CollectorID() As String
        Get
            Return CType(viewstate("CollectorID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CollectorID") = Value
        End Set
    End Property

    Public Property CollectorName() As String
        Get
            Return CType(viewstate("CollectorName"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CollectorName") = Value
        End Set
    End Property

    Public Property CollectorType() As String
        Get
            Return CType(viewstate("CollectorType"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CollectorType") = Value
        End Set
    End Property

    Public Property FormType() As String
        Get
            Return CType(ViewState("FormType"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("FormType") = Value
        End Set
    End Property
#End Region

#Region " Private Const "
    Dim m_AssetRepo As New AssetRepoController

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private m_controller As New DataUserControlController
#End Region

#Region " Navigation "

    'Private Sub PagingFooter()
    '    lblPage.Text = currentPage.ToString()
    '    totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
    '    If totalPages = 0 Then

    '        lblTotPage.Text = "1"
    '        rgvGo.MaximumValue = "1"
    '    Else
    '        lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
    '        rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
    '    End If
    '    lblrecord.Text = recordCount.ToString

    '    If currentPage = 1 Then
    '        imbPrevPage.Enabled = False
    '        imbFirstPage.Enabled = False
    '        If totalPages > 1 Then
    '            imbNextPage.Enabled = True
    '            imbLastPage.Enabled = True
    '        Else
    '            imbPrevPage.Enabled = False
    '            imbNextPage.Enabled = False
    '            imbLastPage.Enabled = False
    '            imbFirstPage.Enabled = False
    '        End If
    '    Else
    '        imbPrevPage.Enabled = True
    '        imbFirstPage.Enabled = True
    '        If currentPage = totalPages Then
    '            imbNextPage.Enabled = False
    '            imbLastPage.Enabled = False
    '        Else
    '            imbLastPage.Enabled = True
    '            imbNextPage.Enabled = True
    '        End If
    '    End If

    'End Sub

    'Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
    '    Select Case e.CommandName
    '        Case "First" : currentPage = 1
    '        Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
    '        Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
    '        Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
    '    End Select
    '    Bindgrid(Me.SearchBy, Me.SortBy)
    'End Sub

    'Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click

    '    If txtPage.Text = "" Then
    '        txtPage.Text = "0"
    '    Else
    '        If IsNumeric(txtPage.Text) Then
    '            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
    '                currentPage = CType(txtPage.Text, Int32)
    '                Bindgrid(Me.SearchBy, Me.SortBy)
    '            End If
    '        End If
    '    End If


    'End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        Bindgrid(Me.SearchBy, Me.SortBy)
    End Sub


    Protected Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        currentPage = e.CurrentPage
        Bindgrid(Me.SearchBy, Me.SortBy, True)
        GridNavigator.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If
        AddHandler GridNavigator.PageChanged, AddressOf PageNavigation
        If Not Me.IsPostBack Then

            Me.FormID = "CollRepo"
            Me.cgid = Me.GroubDbID

            With oBranch
                'If Me.IsHoBranch Then
                .DataSource = m_controller.GetBranchAll(GetConnectionString)
                .DataValueField = "ID"
                .DataTextField = "Name"
                .DataBind()
                'Else
                '    .DataSource = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
                '    .DataValueField = "ID"
                '    .DataTextField = "Name"
                '    .DataBind()
                'End If
                .Items.Insert(0, "Select One")
                .Items(0).Value = "0"
            End With

            Dim c_Class As New Controller.AssetRepoController
            Dim p_Class As New Parameter.AssetRepo

            p_Class.strConnection = GetConnectionString()
            c_Class.GetLokasiPenyimpanan(p_Class)
            With ddlLokasiPenyimpanan
                .DataSource = p_Class.listData
                .DataValueField = "ID"
                .DataTextField = "Description"
                .DataBind()
                .Items.Insert(0, "Select One")
                .Items(0).Value = "0"
            End With

            'With ddlGrade
            '    .DataSource = c_Class.AssetGrade(GetConnectionString)
            '    .DataValueField = "Value"
            '    .DataTextField = "Text"
            '    .DataBind()
            'End With

            InitialDefaultPanel()
        End If
    End Sub

#Region " InitialDefaultPanel "
    Private Sub InitialDefaultPanel()
        PnlSearch.Visible = True
        PnlList.Visible = False
        PnlListDetail.Visible = False
        With txtReposessExpense
            .RequiredFieldValidatorEnable = True
            .RangeValidatorEnable = False
        End With
        With txtKMAsset
            .RequiredFieldValidatorEnable = False
            .RangeValidatorEnable = False
        End With
    End Sub
#End Region

    Sub Bindgrid(ByVal cmdWhere As String, ByVal cmSort As String, Optional isFrNav As Boolean = False)
        Dim dtvEntity As New DataView
        Dim dtsEntity As New DataTable
        Dim oAssetRepo As New Parameter.AssetRepo

        With oAssetRepo
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oAssetRepo = m_AssetRepo.AssetRepoList(oAssetRepo)
        recordCount = oAssetRepo.TotalRecord
        'With oAssetRepo
        '    lblrecord.Text = CType(.TotalRecord, String)
        '    lblTotPage.Text = CType(.PageSize, String)
        '    lblPage.Text = CType(.CurrentPage, String)
        '    recordCount = .TotalRecord
        'End With

        dtsEntity = oAssetRepo.listData
        dtsEntity.TableName = "Table"

        dtvEntity = dtsEntity.DefaultView
        dtvEntity.Sort = Me.SortBy

        Dtg.DataSource = dtvEntity

        Try
            Dtg.DataBind()
        Catch ex As Exception

        End Try

        If (isFrNav = False) Then
            GridNavigator.Initialize(recordCount, pageSize)
        End If
    End Sub

    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        InitialDefaultPanel()
        cboSearchBy.ClearSelection()
        txtSearchBy.Text = ""
        Me.SearchBy = ""
        Me.SortBy = ""
        Bindgrid(Me.SearchBy, Me.SortBy)        
    End Sub

    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Buttonsearch.Click

        Me.SearchBy = ""
        Me.SortBy = ""        

        If oCGID.BranchID.Trim <> "" Then
            Me.SearchBy = "CGID='" & oCGID.BranchID & "'"
            Me.cgid = oCGID.BranchID
        End If

        If cboSearchBy.SelectedIndex <> 0 And txtSearchBy.Text <> "" Then
            If Me.SearchBy = "" Then
                Me.SearchBy = cboSearchBy.SelectedItem.Value.Trim & " like '%" + txtSearchBy.Text.Trim + "%'"
            Else
                Me.SearchBy = Me.SearchBy & " and " & cboSearchBy.SelectedItem.Value.Trim & " like '%" + txtSearchBy.Text.Trim + "%'"
            End If
        End If


        Bindgrid(Me.SearchBy, Me.SortBy)
        PnlList.Visible = True
        PnlListDetail.Visible = False
    End Sub

    Private Sub Dtg_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles Dtg.ItemCommand
        Me.AgreementNo = e.Item.Cells(1).Text.Trim
        Me.RALNo = e.Item.Cells(9).Text.Trim
        Me.CustomerID = e.Item.Cells(2).Text.Trim
        Context.Trace.Write("Me.AgreementNo = " & Me.AgreementNo)
        Context.Trace.Write("Me.RALNo = " & Me.RALNo)
        Select Case e.CommandName
            Case "REPOSSESS"
                FormType = "N" 
                If CheckFeature(Me.Loginid, Me.FormID, "Repo", Me.AppId) Then
                    If SessionInvalid() Then
                        Exit Sub
                    End If
                End If
                viewRepoDetail(Me.AgreementNo, Me.RALNo)
                viewCheckList(Me.AssetTypeID)
               
            Case "EDITREPOSSESS"
                FormType = "J"
                viewRepoDetail(Me.AgreementNo, Me.RALNo)
                viewCheckList(Me.AssetTypeID, ApplicationID)
        End Select
    End Sub

    Private Sub viewRepoDetail(ByVal agreementno As String, ByVal ralNo As String)
        Dim oAssetRepo As New Parameter.AssetRepo
        Dim dt As New DataTable


        With oAssetRepo
            .strConnection = getConnectionString()
            .AgreementNo = agreementno
            .BusinessDate = Me.BusinessDate
            .RALNo = ralNo
        End With

        oAssetRepo = m_AssetRepo.AssetRepoDetail(oAssetRepo)

        dt = oAssetRepo.listData

        hypAgreementNo.Text = CStr(dt.Rows(0).Item("AgreementNo"))

        hypAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "Collection" & "', '" & CStr(dt.Rows(0).Item("ApplicationID")).Trim & "')"

        hypCustomerName.Text = CStr(dt.Rows(0).Item("CustomerName"))

        hypCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "Collection" & "', '" & Server.UrlEncode(Me.CustomerID.Trim) & "')"

        lblAddress.Text = CStr(dt.Rows(0).Item("MailingAddress"))
        lblDuedate.Text = CStr(dt.Rows(0).Item("Duedate"))
        lblOverDueDays.Text = CStr(dt.Rows(0).Item("EndPastDuedays"))
        lblInstallmentNo.Text = CStr(dt.Rows(0).Item("InstallmentNo"))
        lblInstallmentAmount.Text = FormatNumber(CStr(dt.Rows(0).Item("InstallmentAmount")), 0)
        lblODAmount.Text = FormatNumber(CStr(dt.Rows(0).Item("EndPastDueAmt")), 0)
        lblLateCharges.Text = FormatNumber(CStr(dt.Rows(0).Item("LC")), 0)
        lblOSBallance.Text = FormatNumber(CStr(dt.Rows(0).Item("OS_Gross")), 0)
        'lblCollExpense.Text = FormatNumber(CStr(dt.Rows(0).Item("CollectionExpense")), 0)
        lblRALNo.Text = CStr(dt.Rows(0).Item("RALNo"))
        lblRALPeriod.Text = CStr(dt.Rows(0).Item("RALPeriod"))
        'lblRALStatus.Text = CStr(dt.Rows(0).Item("RALStatus"))

        lblExecutor.Text = IIf(IsDBNull(dt.Rows(0).Item("CollectorID")), "", dt.Rows(0).Item("CollectorID").ToString).ToString
        lblOnHand.Text = CStr(dt.Rows(0).Item("OnHand"))
        lblAsset.Text = CStr(dt.Rows(0).Item("Description"))
        lblLicensePlate.Text = CStr(dt.Rows(0).Item("LicensePlate"))
        lblChassisNo.Text = CStr(dt.Rows(0).Item("ChassisNo"))
        lblEngineNo.Text = CStr(dt.Rows(0).Item("EngineNo"))
        lblYear.Text = CStr(dt.Rows(0).Item("Year")).Trim
        lblColor.Text = CStr(dt.Rows(0).Item("Color")).Trim

        'txtRepossesDate.Text = CStr(dt.Rows(0).Item("RepossesDate"))
        txtAssetLocation.Text = CStr(dt.Rows(0).Item("AssetLocation")).Trim
        txtAssetCondition.Text = CStr(dt.Rows(0).Item("AssetCondition")).Trim
        lblBAPK.Text = CStr(dt.Rows(0).Item("BAPK")).Trim
        txtKMAsset.Text = FormatNumber(IIf(IsDBNull(dt.Rows(0).Item("KMAsset")) = True, "0", dt.Rows(0).Item("KMAsset")), 0)
        txtColor.Text = IIf(IsDBNull(dt.Rows(0).Item("KMAsset")) = True, "", dt.Rows(0).Item("Warna")).ToString

        txtlisencePlate.Text = IIf(FormType = "N", IIf(IsDBNull(dt.Rows(0).Item("KMAsset")) = True, "", dt.Rows(0).Item("NoPolisi")), dt.Rows(0).Item("LicensePlate").ToString)

        lblChassisNo.Text = IIf(IsDBNull(dt.Rows(0).Item("KMAsset")) = True, "", dt.Rows(0).Item("NoRangka")).ToString
        lblEngineNo.Text = IIf(IsDBNull(dt.Rows(0).Item("KMAsset")) = True, "", dt.Rows(0).Item("NoMesin")).ToString


        LblRepossessFee.Text = FormatNumber(CStr(dt.Rows(0).Item("ReposessFee")).Trim, 0)

        Me.AssetTypeID = CStr(dt.Rows(0).Item("AssetTypeID")).Trim

        Me.RALDate = CDate(dt.Rows(0).Item("RALDate"))
        Me.RALEndDate = CDate(dt.Rows(0).Item("RALEndDate"))
        Me.AssetSeqNo = CInt(dt.Rows(0).Item("AssetSeqNo"))
        Me.UntilDate = CDate(dt.Rows(0).Item("UntilDate"))
        Me.ApplicationID = CStr(dt.Rows(0).Item("ApplicationID")).Trim
        Me.BranchID = CStr(dt.Rows(0).Item("BranchID")).Trim

        If IsDBNull(dt.Rows(0).Item("STNKEndDate")) Then
            txtSTNKEndDate.Text = ""
        Else
            txtSTNKEndDate.Text = CDate(dt.Rows(0).Item("STNKEndDate")).ToString("dd/MM/yyyy")
        End If

        txtRepossesDate.Text = Me.BusinessDate.ToString("dd/MM/yyyy")


        txtCheckDate.Text = Me.BusinessDate.ToString("dd/MM/yyyy")


        ddlSTNK.SelectedValue = CStr(dt.Rows(0).Item("AdaSTNK")).Trim

        'If IsDBNull(dt.Rows(0).Item("Grade")) Then
        '    'ddlGrade.SelectedValue = "0"
        'Else
        '    ddlGrade.SelectedValue = IIf(CStr(dt.Rows(0).Item("Grade")).Trim = "", "0", CStr(dt.Rows(0).Item("Grade")).Trim)
        'End If
        ddlLokasiPenyimpanan.SelectedValue = IIf(CStr(dt.Rows(0).Item("IDLokasiAsset")).Trim = "", "0", CStr(dt.Rows(0).Item("IDLokasiAsset")).Trim)
        txtlisencePlate.Text = ""
        txtReposessExpense.Text = FormatNumber(CStr(dt.Rows(0).Item("std")), 0)
        If (FormType = "J") Then
            txtlisencePlate.Text = CStr(dt.Rows(0).Item("AssetRepossessionOrderLicensePlate")).Trim
            txtColor.Text = CStr(dt.Rows(0).Item("AssetRepossessionOrderColor")).Trim
            txtChecker.Text = CStr(dt.Rows(0).Item("CheckListPIC")).Trim
            txtCheckerJob.Text = CStr(dt.Rows(0).Item("JobTitlePIC")).Trim
            txtCheckDate.Text = CDate(dt.Rows(0).Item("CheckerListDate")).ToString("dd/MM/yyyy")
            txtNotes.Text = CStr(dt.Rows(0).Item("Notes")).Trim
            lblBAPK.Text = CStr(dt.Rows(0).Item("BAPKNO")).Trim
        End If


        PnlSearch.Visible = False
        PnlList.Visible = False
        PnlListDetail.Visible = True



        If lblExecutor.Text.Trim = "" Then
            ButtonSave.Visible = False
            ShowMessage(lblMessage, "Harap pilih Eksekutor", True)
        Else
            ButtonSave.Visible = True
        End If
    End Sub

    Private Sub viewCheckList(ByVal AssetTypeID As String, Optional appid As String = "")
        Dim oAssetRepo As New Parameter.AssetRepo
        Dim dt As New DataTable


        With oAssetRepo
            .strConnection = GetConnectionString()
            .AssetTypeID = AssetTypeID
            .ApplicationID = appid
        End With

        oAssetRepo = m_AssetRepo.AssetRepoCheckList(oAssetRepo)

        dt = oAssetRepo.listData
        dtgCheckList.DataSource = dt.DefaultView
        dtgCheckList.DataBind()

        PnlSearch.Visible = False
        PnlList.Visible = False
        PnlListDetail.Visible = True

    End Sub

    Private Sub imbCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        PnlListDetail.Visible = False
        Me.SearchBy = ""
        Me.SortBy = ""
        Me.Bindgrid(Me.SearchBy, Me.SortBy)
        PnlSearch.Visible = True
        PnlList.Visible = True
        ClearAllTextBox()
    End Sub

    Private Sub dtgCheckList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgCheckList.ItemDataBound
        Dim txtAnswer As New TextBox
        Dim rbAnswer As New RadioButtonList

        If e.Item.ItemIndex >= 0 Then
            txtAnswer = CType(e.Item.FindControl("txtAnswer"), TextBox)
            rbAnswer = CType(e.Item.FindControl("rbAnswer"), RadioButtonList)

            If e.Item.Cells(4).Text.Trim = "YesNo" Then
                txtAnswer.Visible = False
                rbAnswer.Visible = True
                rbAnswer.Items.FindByValue("N").Selected = (e.Item.Cells(6).Text.Trim() = "0")
            Else
                txtAnswer.Visible = True
                rbAnswer.Visible = False
            End If
        End If
    End Sub

    Private Sub ButtonSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
        'Validasi 

        If Me.BusinessDate < ConvertDate2(txtRepossesDate.Text) Then            
            ShowMessage(lblMessage, "Tanggal Tarik tidak boleh > tanggal hari ini", True)
            Exit Sub
        End If

        If Me.BusinessDate < ConvertDate2(txtCheckDate.Text) Then            
            ShowMessage(lblMessage, "Tanggal Pemeriksaan tidak boleh > tanggal hari ini", True)
            Exit Sub
        End If

        'Ambil semua data yang diperlukan dari layar detail

        Dim oAssetRepo As New Parameter.AssetRepo

        With oAssetRepo
            .strConnection = getConnectionString()
            .CGID = Me.cgid
            .AssetSeqNo = Me.AssetSeqNo
            .RepossesDate = ConvertDate2(txtRepossesDate.Text.Trim)

            .AgreementNo = hypAgreementNo.Text.Trim
            .AssetDescription = lblAsset.Text.Trim
            .EndpastDueDays = CInt(lblOverDueDays.Text.Trim)
            .CollectorID = lblExecutor.Text.Trim
            .AssetLocation = txtAssetLocation.Text.Trim
            .AssetCondition = txtAssetCondition.Text.Trim
            .Checker = txtChecker.Text.Trim
            .CheckerJob = txtCheckerJob.Text.Trim
            .Checkdate = ConvertDate2(CStr(IIf(txtCheckDate.Text.Trim = "", "01/01/1900", txtCheckDate.Text.Trim)))
            .UntilDate = Me.UntilDate

            .Notes = txtNotes.Text.Trim
            .RALNo = lblRALNo.Text.Trim
            .BussinessDate = Me.BusinessDate
            .RALDate = Me.RALDate
            .RALEndDate = Me.RALEndDate
            .BAPKNo = lblBAPK.Text.Trim

            .ApplicationID = Me.ApplicationID
            .BranchId = Me.BranchID
            .Color = txtColor.Text
            .LisencePlate = txtlisencePlate.Text
            .STNKEndDate = txtSTNKEndDate.Text
            .STNKName = txtSTNKName.Text
            '.BBM = txtBBM.Text
            .BBM = ""
            .DebitAmount = CDbl(txtReposessExpense.Text)
            .BranchAssetReposes = oBranch.SelectedValue.Trim
            .KMAsset = CInt(txtKMAsset.Text)
            .AdaAsset = ddlSTNK.SelectedValue
            .IDLokasiAsset = ddlLokasiPenyimpanan.SelectedValue
            '.Grade = ddlGrade.SelectedValue
        End With

     

        Dim assetRepoChecks = New List(Of Parameter.AssetRepo)

        '==============================
        ' Check List Save
        '==============================

        'Dim txtbox As New TextBox
        'Dim rb As New RadioButtonList
        'Dim txtboxDesc As New TextBox

        Dim i As Integer
        For i = 0 To dtgCheckList.Items.Count - 1

            Dim txtbox = CType(dtgCheckList.Items(i).FindControl("txtAnswer"), TextBox)
            Dim rb = CType(dtgCheckList.Items(i).FindControl("rbAnswer"), RadioButtonList)
            Dim txtboxDesc = CType(dtgCheckList.Items(i).FindControl("txtDescription"), TextBox)
            Dim oAssetRepo_ = New Parameter.AssetRepo
            With oAssetRepo_
                ' .strConnection = getConnectionString()
                .BAPKNo = lblBAPK.Text.Trim
                .CGID = Me.cgid
                .AssetSeqNo = Me.AssetSeqNo
                .ApplicationID = Me.ApplicationID
                .AssetTypeID = Me.AssetTypeID
                .AgreementNo = hypAgreementNo.Text.Trim 
                .BranchId = Me.BranchID

                .CheckListID = dtgCheckList.Items(i).Cells(5).Text.Trim


                If dtgCheckList.Items(i).Cells(4).Text.Trim = "YesNo" Then
                    .YNQuestion = rb.SelectedItem.Value
                    .Quantity = ""
                Else
                    .YNQuestion = ""
                    .Quantity = txtbox.Text.Trim
                End If 
                .Notes = txtboxDesc.Text.Trim
            End With
            assetRepoChecks.Add(oAssetRepo_)

            'Try
            '    m_AssetRepo.AssetRepoCheckSave(oAssetRepo)                
            '    ShowMessage(lblMessage, "Simpan Data Berhasil", False)
            'Catch ex As Exception                
            '    ShowMessage(lblMessage, "Simpan Data Gagal", True)
            'End Try 
        Next 
        Try
            oAssetRepo.AssetRepoChecks = assetRepoChecks
            m_AssetRepo.AssetRepoSave(oAssetRepo)
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
            Exit Sub
        End Try
         
        Bindgrid(Me.SearchBy, Me.SortBy)
        PnlSearch.Visible = True
        PnlList.Visible = True
        PnlListDetail.Visible = False
        ClearAllTextBox()
    End Sub

    Private Sub ClearAllTextBox()
        txtChecker.Text = ""
        txtCheckerJob.Text = ""
        txtAssetCondition.Text = ""
        txtAssetLocation.Text = ""
        txtNotes.Text = ""
        txtColor.Text = ""
        txtSTNKName.Text = ""
        txtSTNKEndDate.Text = ""
        txtKMAsset.Text = ""
        'txtBBM.Text = "" 
    End Sub
    Private Sub Dtg_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles Dtg.ItemDataBound
        Me.AgreementNo = e.Item.Cells(1).Text.Trim
        Me.CustomerID = e.Item.Cells(2).Text.Trim
        Dim lbTemp As HyperLink

        If e.Item.ItemIndex >= 0 Then 
        '*** Add agreement view
        lbTemp = CType(e.Item.FindControl("hlAgreement"), HyperLink)
        lbTemp.NavigateUrl = "javascript:OpenAgreementNo('" & "Collection" & "', '" & Me.AgreementNo & "')"

        '*** Add customer  view
        lbTemp = CType(e.Item.FindControl("hlCustomer"), HyperLink)
        lbTemp.NavigateUrl = "javascript:OpenCustomer('" & "Collection" & "', '" & Me.CustomerID & "')"

        CType(e.Item.FindControl("lnkReposses"), LinkButton).Enabled = e.Item.Cells(12).Text.Trim() = ""
        CType(e.Item.FindControl("lnkEditReposses"), LinkButton).Enabled = e.Item.Cells(12).Text.Trim() <> ""
        End If
    End Sub

End Class