﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CollExpense.aspx.vb" Inherits="Maxiloan.Webform.ARMgt.CollExpense" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ucApprovalRequest" Src="../../Webform.UserController/ucApprovalRequest.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcBranchCollection" Src="../../Webform.UserController/UcBranchCollection.ascx" %>
<%@ Register TagName="ucNumberFormat" TagPrefix="uc1" Src="../../webform.UserController/ucNumberFormat.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>CollExpense</title>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;

        var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%# Request.servervariables("SERVER_NAME")%>/';

        function OpenWinHistory(pCGID, pExID, pExName) {
            window.open('../ExecutorHistory.aspx?CGID=' + pCGID + '&ExecutorID=' + pExID + '&ExecutorName=' + pExName + '', 'UserLookup', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenWinCollector(pCollID, pCGID) {
            window.open('../../Webform.Setup/CollectorView.aspx?CGID=' + pCGID + '&CollectorID=' + pCollID + '', 'UserLookup', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenWinCustomer(pID) {
            window.open('../../../Webform.LoanOrg/Credit/ViewPersonalCustomer.aspx?CustomerID=' + pID + '&style=Collection', 'UserLookup', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="sm1">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
        onclick="hideMessage();"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                PENGAJUAN BIAYA COLLECTION
            </h3>
        </div>
    </div>
    <asp:Panel ID="pnlsearch" runat="server">
        <div class="form_box">
            <div class="form_single">
                <label>
                    Collection
                </label>
                &nbsp;<uc1:ucbranchcollection id="oCGID" runat="server"></uc1:ucbranchcollection>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cari Berdasarkan</label>
                <asp:DropDownList ID="cboSearchBy" runat="server">
                    <asp:ListItem Value="Select One">Select One</asp:ListItem>
                    <asp:ListItem Value="CollectionAgreement.AgreementNo">No Kontrak</asp:ListItem>
                    <asp:ListItem Value="Customer.Name">Nama Konsumen</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearchBy" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="Buttonsearch" runat="server" Text="Search" CssClass="small button blue">
            </asp:Button>&nbsp;
            <asp:Button ID="ButtonReset" runat="server" Text="Reset" CssClass="small button gray"
                CausesValidation="False"></asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlDtGrid" runat="server">
        <div class="form_box_title">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgCollExpenseList" runat="server" Width="100%" AllowSorting="True"
                        AutoGenerateColumns="False" DataKeyField="AgreementNo" OnSortCommand="Sorting"
                        CssClass="grid_general" BorderStyle="None" BorderWidth="0">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:BoundColumn DataField="AgreementNo" Visible="False"></asp:BoundColumn>
                            <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">
                                <ItemTemplate>
                                    <asp:Label ID="lblBranchID" Text='<%# Container.DataItem("BranchID")%>' runat="server"
                                        Visible="false">
                                    </asp:Label>
                                    <a href="javascript:OpenAgreementNo('Collection','<%# Container.DataItem("AgreementNo")%>')">
                                        <%# Container.DataItem("AgreementNo")%></a>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="CustomerName" HeaderText="NAMA KONSUMEN">
                                <ItemTemplate>
                                    <asp:Label ID="lblCollectorID" Text='<%# Container.DataItem("CollectorID")%>' runat="server" Visible="false"></asp:Label>
                                    <asp:Label ID="lblCustomerID" Text='<%# Container.DataItem("CustomerID")%>' runat="server" Visible="false"></asp:Label>
                                    <a href="javascript:OpenCustomer('Collection','<%# Container.DataItem("CustomerID")%>')">
                                        <%# Container.DataItem("CustomerName")%></a>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="ENDPASTDUEDAYS" SortExpression="EndPastDueDays" HeaderText="HARI TELAT">
                            </asp:BoundColumn>
                            <%--<asp:BoundColumn DataField="RALNO" SortExpression="RALNo" HeaderText="NO SKT"></asp:BoundColumn>--%>
                            <asp:BoundColumn DataField="RALNO" SortExpression="RALNo" HeaderText="NO SKE"></asp:BoundColumn>
                            <asp:BoundColumn DataField="RAlPeriod" SortExpression="RAlPeriod" HeaderText="PERIODE SKE">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="CollectorName" SortExpression="CollectorName" HeaderText="EKSEKUTOR">
                            </asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="PILIH">
                                <ItemTemplate>
                                    <asp:ImageButton CommandName="Request" ID="imbRequest" runat="server" ImageUrl="../../Images/IconRequest.gif">
                                    </asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                    <div class="button_gridnavigation">
                        <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                            CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                            CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                            CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                            CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                        <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                            EnableViewState="False"></asp:Button>
                        <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" MinimumValue="1"
                            ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                            ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general">
                        </asp:RequiredFieldValidator>
                    </div>
                    <div class="label_gridnavigation">
                        <asp:Label ID="lblPage" runat="server"></asp:Label>of
                        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                        <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlSelect" runat="server">
        <div class="form_box">
            <div class="form_left">
                <label>
                    No Kontrak</label>
                <asp:HyperLink ID="hypAgreementNoSlct" runat="server"></asp:HyperLink>
            </div>
            <div class="form_right">
                <label>
                    Nama Konsumen</label>
                <asp:HyperLink ID="hypCustomerNameSlct" runat="server"></asp:HyperLink>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Alamat</label>
                <asp:Label ID="lblAddress" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Nama Asset</label>
                <asp:Label ID="lblAsset" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Angsuran Ke</label>
                <asp:Label ID="lblInstallmentNo" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Jumlah Angsuran</label>
                <asp:Label ID="lblInstallmentAmount" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Tanggal Jatuh Tempo</label>
                <asp:Label ID="lblDuedate" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Jumlah Hari Telat</label>
                <asp:Label ID="lblOverDueDays" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Jumlah Tunggakan</label>
                <asp:Label ID="lblODAmount" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Denda Keterlambatan</label>
                <asp:Label ID="lblLateCharges" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Biaya Tagih</label>
                <asp:Label ID="lblOSBillingCharges" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Biaya Collection</label>
                <asp:Label ID="lblCollExpense" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Sisa A/R</label>
                <asp:Label ID="lblOSBallance" runat="server"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    <%--No SKT--%>
                    No SKE
                </label>
                <asp:Label ID="lblRALNo" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    <%--Periode SKT--%>
                    Periode SKE
                </label>
                <asp:Label ID="lblRALPeriod1" runat="server"></asp:Label>&nbsp;S/D&nbsp;
                <asp:Label ID="lblRALPeriod2" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    <%--Status SKT--%>
                    Status SKE
                </label>
                <asp:Label ID="lblRALStatus" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    ID Eksekutor</label>
                <asp:Label ID="lblExecutor" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Nama Eksekutor</label>
                <asp:Label runat="server" ID="lblNamaEksekutor"></asp:Label>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlSelect2" runat="server">
        <div class="form_box_title">
            <div class="form_single">
                <h4>
                    BIAYA COLLECTION
                </h4>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="PnlSelect3" runat="server">
        <%--<div class="form_box_hide">
            <div class="form_single">
                <label>
                    Standard Fee Eksekutor</label>
                <asp:TextBox ID="txtStandarExecutorFee" runat="server" ReadOnly="True"></asp:TextBox>
            </div>
        </div>
        <asp:UpdatePanel runat="server" ID="up1">
            <ContentTemplate>--%>
        <%--<div class="form_box">
                    <div class="form_single">
                        <label>
                            Tambahan Fee Eksekutor
                        </label>
                        <uc1:ucnumberformat id="txtERA" runat="server" ontextchanged="txtERA_TextChanged" />
                    </div>
                </div>--%>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Total Fee Eksekutor</label>
                <%--<asp:TextBox ID="txtTotalExecutorFee" runat="server"></asp:TextBox>--%>
                <uc1:ucnumberformat id="txtTotalExecutorFee" runat="server" /> 
            </div>
        </div>
        <%--</ContentTemplate>
        </asp:UpdatePanel>--%>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Nama Bank</label>
                <%--<asp:DropDownList ID="cboBankAccount" runat="server">
                </asp:DropDownList>--%>
                <asp:Label runat="server" ID="lblNamaBank"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Nama Cabang Bank</label>
                <asp:Label runat="server" ID="lblNamaCabangBank"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Nama Rekening</label>
                <asp:Label runat="server" ID="lblNamaRekening"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    No Rekening</label>
                <asp:Label runat="server" ID="lblNoRekening"></asp:Label>
                <asp:HiddenField ID="hdfBankAccountID" runat="server" />
            </div>
        </div>
        <uc1:ucapprovalrequest id="oApprovalRequest" runat="server"></uc1:ucapprovalrequest>
        <div class="form_button">
            <asp:Button ID="ButtonSave" runat="server" Text="Save" CssClass="small button blue">
            </asp:Button>&nbsp;
            <asp:Button ID="ButtonCancel" runat="server" Text="Cancel" CssClass="small button gray"
                CausesValidation="False"></asp:Button>
        </div>
        <div class="form_box">
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgSelect" runat="server" Width="100%" OnSortCommand="Sorting"
                        CssClass="grid_general" BorderStyle="None" BorderWidth="0" AutoGenerateColumns="False">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:BoundColumn DataField="RequestNo" HeaderText="NO REQUEST"></asp:BoundColumn>
                            <asp:BoundColumn DataField="RequestDate" HeaderText="TGL REQUEST"></asp:BoundColumn>
                            <asp:BoundColumn DataField="InstallmentNo" HeaderText="ANGS KE"></asp:BoundColumn>
                            <%--<asp:BoundColumn DataField="RalNo" HeaderText="NO SKT"></asp:BoundColumn>--%>
                            <asp:BoundColumn DataField="RalNo" HeaderText="NO SKE"></asp:BoundColumn>
                            <asp:BoundColumn DataField="ExpenseAmount" HeaderText="JUMLAH BIAYA"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Notes" HeaderText="CATATAN"></asp:BoundColumn>
                            <asp:BoundColumn DataField="CollectorId" HeaderText="ID COLLECTOR"></asp:BoundColumn>
                            <asp:BoundColumn DataField="ApprovalStatus" HeaderText="STATUS APPROVAL"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
