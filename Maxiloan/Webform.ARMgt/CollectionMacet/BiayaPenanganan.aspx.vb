﻿#Region "Import"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCacheHelper
#End Region

Public Class BiayaPenanganan
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oCGID As UcBranchCollection    
    ' Protected WithEvents txtBiayaPenanganan As ucNumberFormat
    Protected WithEvents txtBiayaCollection As ucNumberFormat
    Protected WithEvents txtBiayaTebus As ucNumberFormat
    Protected WithEvents txtJumlahBiayaBOP As ucNumberFormat

#Region "properties"
    Public Property CustomerID() As String
        Get
            Return CType(ViewState("CustomerID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property
    Public Property BankType() As String
        Get
            Return CType(ViewState("BankType"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("BankType") = Value
        End Set
    End Property

    Public Property BankPurpose() As String
        Get
            Return CType(ViewState("BankPurpose"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("BankPurpose") = Value
        End Set
    End Property
    Public Property cgid() As String
        Get
            Return CType(ViewState("cgid"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("cgid") = Value
        End Set
    End Property
    Public Property AgreementNo() As String
        Get
            Return CType(ViewState("AgreementNo"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("AgreementNo") = Value
        End Set
    End Property
    Public Property RALNo() As String
        Get
            Return CType(ViewState("RALNo"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("RALNo") = Value
        End Set
    End Property
    Public Property ApplicationID() As String
        Get
            Return CType(ViewState("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property


    Public Property JenisBiayaBOP() As IList(Of Parameter.CommonValueText)
        Get
            Return CType(ViewState("JenisBiayaBOP"), IList(Of Parameter.CommonValueText))
        End Get
        Set(ByVal Value As IList(Of Parameter.CommonValueText))
            ViewState("JenisBiayaBOP") = Value
        End Set
    End Property


    Public Property AssetRepoBOPs() As IList(Of Parameter.AssetRepoBOP)
        Get
            Return CType(ViewState("AssetRepoBOPs"), IList(Of Parameter.AssetRepoBOP))
        End Get
        Set(ByVal Value As IList(Of Parameter.AssetRepoBOP))
            ViewState("AssetRepoBOPs") = Value
        End Set
    End Property

#End Region

#Region " Private Const "
    Dim m_CollExpense As New CollExpenseController
    Private oController As New DataUserControlController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region
#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If

    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        Bindgrid(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        If txtPage.Text = "" Then
            txtPage.Text = "0"
        Else
            If IsNumeric(txtPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                    currentPage = CType(txtPage.Text, Int32)
                    Bindgrid(Me.SearchBy, Me.SortBy)
                End If
            End If
        End If


    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        Bindgrid(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        Page.Header.DataBind()

        If SessionInvalid() Then
            Exit Sub
        End If

        If Not Page.IsPostBack Then
            Me.FormID = "CollHandling"
            Me.cgid = Me.GroubDbID

            If CheckForm(Me.Loginid, Me.FormID, "Maxiloan") Then
                InitialDefaultPanel()
            Else
                Dim strHTTPServer As String
                Dim strHTTPApp As String
                Dim strNameServer As String
                strHTTPServer = Request.ServerVariables("PATH_INFO")
                strNameServer = Request.ServerVariables("SERVER_NAME")
                strHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                Response.Redirect("http://" & strNameServer & "/" & strHTTPApp & "/NotAuthorized.html")
            End If
        End If
    End Sub


    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Buttonsearch.Click
        Me.SearchBy = ""
        Me.SortBy = ""
        If oCGID.BranchID.Trim <> "" Then
            Me.SearchBy = "CGID='" & oCGID.BranchID & "' and CollectionAgreement.Repossess='N' and qryral.RALStatus = 'OP' and qryral.isPrint = 1"
            Me.cgid = oCGID.BranchID
        End If
        If cboSearchBy.SelectedIndex <> 0 And txtSearchBy.Text <> "" Then
            If Me.SearchBy = "" Then
                Me.SearchBy = cboSearchBy.SelectedItem.Value.Trim & " like '%" + txtSearchBy.Text.Trim + "%'"
            Else
                Me.SearchBy = Me.SearchBy & " and " & cboSearchBy.SelectedItem.Value.Trim & " like '%" + txtSearchBy.Text.Trim + "%'"
            End If
        End If

        Bindgrid(Me.SearchBy, Me.SortBy)
    End Sub
    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        InitialDefaultPanel()
        cboSearchBy.ClearSelection()
        txtSearchBy.Text = ""
    End Sub
    Private Sub InitialDefaultPanel()
        pnlDtGrid.Visible = False
        pnlsearch.Visible = True
        pnlSelect.Visible = False        
        pnlAddJenisBOP.Visible = False
         
        With txtBiayaCollection
            .RequiredFieldValidatorEnable = True
            .RangeValidatorEnable = True
            .TextCssClass = "numberAlign medium_text"
        End With

        With txtBiayaTebus
            .RequiredFieldValidatorEnable = True
            .RangeValidatorEnable = True
            .TextCssClass = "numberAlign medium_text"
        End With


        txtTanggalPembebanan.Text = BusinessDate.ToString("dd/MM/yyyy")

        JenisBiayaBOP = New AssetRepoController().JenisBiayaBOP(GetConnectionString())
        AssetRepoBOPs = New List(Of Parameter.AssetRepoBOP)

    End Sub
    Sub Bindgrid(ByVal cmdWhere As String, ByVal cmSort As String)
        Dim dtvEntity As New DataView
        Dim dtsEntity As New DataTable
        Dim oCollExpense As New Parameter.CollExpense

        With oCollExpense
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oCollExpense = m_CollExpense.CollExpenseList(oCollExpense)

        With oCollExpense
            lblrecord.Text = CType(.TotalRecord, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecord
        End With

        dtsEntity = oCollExpense.listData
        dtsEntity.TableName = "Table"

        dtvEntity = dtsEntity.DefaultView

        dtgCollExpenseList.DataSource = dtvEntity

        Try
            dtgCollExpenseList.DataBind()
        Catch ex As Exception

        End Try

        PagingFooter()
        pnlDtGrid.Visible = True
    End Sub

    Private Sub dtgCollExpenseList_itemcommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgCollExpenseList.ItemCommand
       
        If e.CommandName = "Request" Then
            Me.AgreementNo = CType(e.Item.FindControl("lblAgreementNo"), Label).Text.Trim
            Me.RALNo = CType(e.Item.FindControl("lblRALNo"), Label).Text.Trim

            If CheckFeature(Me.Loginid, Me.FormID, "Req", Me.AppId) Then
                If SessionInvalid() Then
                    Dim strHTTPServer As String
                    Dim strHTTPApp As String
                    Dim strNameServer As String
                    strHTTPServer = Request.ServerVariables("PATH_INFO")
                    strNameServer = Request.ServerVariables("SERVER_NAME")
                    strHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                    Response.Redirect("http://" & strNameServer & "/" & strHTTPApp & "/NotAuthorized.html")
                Else

                    Dim lblBranchID, lblCustomerID As Label
                    If e.Item.ItemIndex >= 0 Then
                        lblBranchID = CType(e.Item.FindControl("lblBranchID"), Label)
                        lblCustomerID = CType(e.Item.FindControl("lblCustomerID"), Label)
                        Me.BranchID = lblBranchID.Text
                        Me.CustomerID = lblCustomerID.Text
                    End If
                    viewRepoDetail(Me.AgreementNo, Me.RALNo)
                End If
            End If
        End If
    End Sub
    Private Sub viewRepoDetail(ByVal agreementno As String, ByVal ralNo As String)

        pnlDtGrid.Visible = False
        pnlsearch.Visible = False
        pnlSelect.Visible = True        

        Dim oAssetRepo As New Parameter.AssetRepo
        Dim oCollExpense As New Parameter.CollExpense
        Dim oCollExpenseReqList As New Parameter.CollExpense
        Dim m_AssetRepo As New AssetRepoController
        Dim dtsEntity As New DataTable
        Dim dtvEntity As New DataView
        Dim aprStatus As String
        Dim dt As New DataTable

        Dim dtBankAccount As New DataTable
        Dim dtBankAccountCache As New DataTable

        Me.BankType = "B"        

        With oAssetRepo
            .strConnection = GetConnectionString()
            .AgreementNo = agreementno
            .BusinessDate = Me.BusinessDate
            .RALNo = ralNo
        End With

        oAssetRepo = m_AssetRepo.AssetRepoDetail(oAssetRepo)

        dt = oAssetRepo.listData

        Me.ApplicationID = CStr(dt.Rows(0).Item("ApplicationID")).Trim
        Me.BranchID = CStr(dt.Rows(0).Item("BranchID")).Trim
        hypAgreementNoSlct.Text = CStr(dt.Rows(0).Item("AgreementNo"))
        hypAgreementNoSlct.NavigateUrl = "javascript:OpenAgreementNo('" & "Collection" & "', '" & Server.UrlEncode(Me.ApplicationID.Trim) & "')"

        hypCustomerNameSlct.Text = CStr(dt.Rows(0).Item("CustomerName"))
        hypCustomerNameSlct.NavigateUrl = "javascript:OpenCustomer('" & "Collection" & "', '" & Server.UrlEncode(Me.CustomerID.Trim) & "')"
        lblAddress.Text = CStr(dt.Rows(0).Item("MailingAddress"))
        lblDuedate.Text = CStr(dt.Rows(0).Item("Duedate"))
        lblOverDueDays.Text = CStr(dt.Rows(0).Item("EndPastDuedays"))
        lblInstallmentNo.Text = CStr(dt.Rows(0).Item("InstallmentNo"))
        lblInstallmentAmount.Text = FormatNumber(CStr(dt.Rows(0).Item("InstallmentAmount")), 0)
        lblODAmount.Text = FormatNumber(CStr(dt.Rows(0).Item("EndPastDueAmt")), 0)
        lblLateCharges.Text = FormatNumber(CStr(dt.Rows(0).Item("LC")), 0)
        lblOSBillingCharges.Text = FormatNumber(CStr(dt.Rows(0).Item("OSBillingCharges")), 0)
        lblOSBallance.Text = FormatNumber(CStr(dt.Rows(0).Item("OS_Gross")), 0)
        lblCollExpense.Text = FormatNumber(CStr(dt.Rows(0).Item("CollectionExpense")), 0)
        lblRALNo.Text = CStr(dt.Rows(0).Item("RALNo"))
        lblRALPeriod1.Text = CStr(dt.Rows(0).Item("RALPeriod"))
        lblSktDitangai.Text = CStr(dt.Rows(0).Item("OnHand"))


        lblExecutor.Text = CStr(dt.Rows(0).Item("CollectorID"))

        lblAsset.Text = CStr(dt.Rows(0).Item("Description"))
        lblNoPolisi.Text = CStr(dt.Rows(0).Item("LicensePlate"))
        lblTahunProduksi.Text = CStr(dt.Rows(0).Item("Year"))
        lblNoRangka.Text = CStr(dt.Rows(0).Item("ChassisNo"))

        lblWarna.Text = CStr(dt.Rows(0).Item("Color"))
        lblNoMesin.Text = CStr(dt.Rows(0).Item("EngineNo"))
        lblBAPK.Text = CStr(dt.Rows(0).Item("BAPK"))


        'txtBiayaPenanganan.Text = FormatNumber(CStr(dt.Rows(0).Item("AgreementCollectionExpense")), 0)
        '==============================================================

        With oCollExpense
            .strConnection = GetConnectionString()
            .WhereCond = "CollExpense.ApplicationID = '" & Me.ApplicationID & "'"
        End With

        oCollExpenseReqList = m_CollExpense.CollExpenseReqList(oCollExpense)
        dtsEntity = oCollExpenseReqList.listData
        
        If lblExecutor.Text.Trim = "" Then
            ButtonSave.Visible = False
            ShowMessage(lblMessage, "Harap pilih Eksekutor", True)
        Else
            ButtonSave.Visible = True
            lblMessage.Visible = False
        End If
        If aprStatus = "REQ" Then
            ButtonSave.Visible = False
            ShowMessage(lblMessage, "Permintaan Sebelumnya belum disetujui", True)
        End If


        dtgBOP.DataSource = AssetRepoBOPs
        dtgBOP.DataBind()

    End Sub

    Private Sub ButtonSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
        Dim oAssetRepo As New Parameter.AssetRepo        
        Dim m_AssetRepo As New AssetRepoController        

        With oAssetRepo
            .strConnection = GetConnectionString()
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationID
            .TanggalPembebanan = ConvertDate2(txtTanggalPembebanan.Text.Trim)
            .BiayaCollection = CDec(txtBiayaCollection.Text.Trim)
            .BiayaTebus = CDec(txtBiayaTebus.Text.Trim)
            .BOPs = AssetRepoBOPs
            ' .BiayaPenanganan = CDec(txtBiayaPenanganan.Text.Trim)
        End With

        Try
            m_AssetRepo.BiayaPenangananSave(oAssetRepo)
            ShowMessage(lblMessage, "Update Data Berhasil ", False)
            Bindgrid(Me.SearchBy, Me.SortBy)
            pnlsearch.Visible = True
            pnlSelect.Visible = False            
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try

    End Sub

    Private Sub imbCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        Response.Redirect("BiayaPenanganan.aspx")
    End Sub

    Private Sub dtgCollExpenseList_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtgCollExpenseList.SelectedIndexChanged

    End Sub
     

    Private Sub btnAddNewBOP_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        divAdd.Visible = False
        pnlAddJenisBOP.Visible = True

        With txtJumlahBiayaBOP
            .RequiredFieldValidatorEnable = True
            .RangeValidatorEnable = True
            .TextCssClass = "numberAlign medium_text"
        End With
        txtJumlahBiayaBOP.Text = 0



        With cboJenisBiayaBOP
            .DataTextField = "Text"
            .DataValueField = "Value"
            .DataSource = JenisBiayaBOP
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = ""
            .SelectedIndex = 0
        End With

        cboJenisBiayaBOP_RequiredFieldValidator.Enabled = True

    End Sub

    Sub refreshBopGrid()
        Dim seq = 1
        For Each item In AssetRepoBOPs
            item.Seq = seq
            seq += 1
        Next

        dtgBOP.DataSource = AssetRepoBOPs
        dtgBOP.DataBind()

    End Sub


    Private Sub btnAddOKBOP_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddOK.Click
         
        If (cboJenisBiayaBOP.SelectedValue = "0" Or CDec(txtJumlahBiayaBOP.Text) <= 0) Then
            ShowMessage(lblMessage, "Pilih Jenis Biaya dan masukan jumlahnya.", True)
            Return
        End If

        If (Not ReferenceEquals(Nothing, AssetRepoBOPs.Where(Function(x) x.BOPId.Trim = cboJenisBiayaBOP.SelectedValue.Trim).SingleOrDefault())) Then
            ShowMessage(lblMessage, "Jenis Biaya sudah dipilih.", True)
            Return
        End If


        Dim asst = New Parameter.AssetRepoBOP() With {.Seq = 0, .BOPId = cboJenisBiayaBOP.SelectedValue, .BOPDescription = cboJenisBiayaBOP.SelectedItem.Text, .BOPAmount = CDec(txtJumlahBiayaBOP.Text)}
        AssetRepoBOPs.Add(asst)

        refreshBopGrid()

        divAdd.Visible = True
        pnlAddJenisBOP.Visible = False

        With txtJumlahBiayaBOP
            .RequiredFieldValidatorEnable = False
            .RangeValidatorEnable = False
            .TextCssClass = "numberAlign medium_text"
        End With

        cboJenisBiayaBOP_RequiredFieldValidator.Enabled = False

    End Sub


    Private Sub btnCancelAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelAdd.Click

        divAdd.Visible = True
        pnlAddJenisBOP.Visible = False

        With txtJumlahBiayaBOP
            .RequiredFieldValidatorEnable = False
            .RangeValidatorEnable = False
            .TextCssClass = "numberAlign medium_text"
        End With

        cboJenisBiayaBOP_RequiredFieldValidator.Enabled = False
    End Sub


    Private Sub dtgEntity_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgBOP.ItemCommand
        If e.CommandName = "Delete" Then
            Dim id = dtgBOP.DataKeys.Item(e.Item.ItemIndex).ToString()

            Dim sel = AssetRepoBOPs.Where(Function(x) x.Seq = id).SingleOrDefault()
            AssetRepoBOPs.Remove(sel)
            refreshBopGrid()
        End If
    End Sub
End Class
