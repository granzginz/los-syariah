﻿#Region "Imports"
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Parameter.AssetRelease
#End Region

Public Class AssetRelease
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oCGID As ucBranchCollection

#Region "properties"


    Public Property RepossesSeqNo() As Integer
        Get
            Return CType(Viewstate("RepossesSeqNo"), Integer)
        End Get
        Set(ByVal Value As Integer)
            Viewstate("RepossesSeqNo") = Value
        End Set
    End Property

    Public Property AssetSeqNo() As Integer
        Get
            Return CType(Viewstate("AssetSeqNo"), Integer)
        End Get
        Set(ByVal Value As Integer)
            Viewstate("AssetSeqNo") = Value
        End Set
    End Property

    Public Property UntilDate() As Date
        Get
            Return CType(Viewstate("UntilDate"), Date)
        End Get
        Set(ByVal Value As Date)
            Viewstate("UntilDate") = Value
        End Set
    End Property

    Public Property RALDate() As Date
        Get
            Return CType(Viewstate("RALDate"), Date)
        End Get
        Set(ByVal Value As Date)
            Viewstate("RALDate") = Value
        End Set
    End Property

    Public Property RALEndDate() As Date
        Get
            Return CType(Viewstate("RALEndDate"), Date)
        End Get
        Set(ByVal Value As Date)
            Viewstate("RALEndDate") = Value
        End Set
    End Property

    Public Property AssetTypeID() As String
        Get
            Return CType(Viewstate("AssetTypeID"), String)
        End Get
        Set(ByVal Value As String)
            Viewstate("AssetTypeID") = Value
        End Set
    End Property

    Public Property AllPage() As String
        Get
            Return CType(Viewstate("AllPage"), String)
        End Get
        Set(ByVal Value As String)
            Viewstate("AllPage") = Value
        End Set
    End Property



    Public Property cgid() As String
        Get
            Return CType(viewstate("CGID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CGID") = Value
        End Set
    End Property


    Public Property AgreementNo() As String
        Get
            Return CType(viewstate("AgreementNo"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AgreementNo") = Value
        End Set
    End Property

    Public Property CustomerID() As String
        Get
            Return CType(viewstate("CustomerID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerID") = Value
        End Set
    End Property

    Public Property ApplicationID() As String
        Get
            Return CType(viewstate("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property


    Public Property RALNo() As String
        Get
            Return CType(viewstate("RALNo"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("RALNo") = Value
        End Set
    End Property

    Public Property CollectorID() As String
        Get
            Return CType(viewstate("CollectorID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CollectorID") = Value
        End Set
    End Property

    Public Property CollectorName() As String
        Get
            Return CType(viewstate("CollectorName"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CollectorName") = Value
        End Set
    End Property

    Public Property CollectorType() As String
        Get
            Return CType(viewstate("CollectorType"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CollectorType") = Value
        End Set
    End Property


#End Region

#Region " Private Const "
    Dim m_AssetRelease As New AssetReleaseController

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region

#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If

    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        Bindgrid(Me.SearchBy, Me.SortBy)
    End Sub

    Protected Sub btnPageNumb_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPageNumb.Click

        If txtPage.Text = "" Then
            txtPage.Text = "0"
        Else
            If IsNumeric(txtPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                    currentPage = CType(txtPage.Text, Int32)
                    Bindgrid(Me.SearchBy, Me.SortBy)
                End If
            End If
        End If

    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        Bindgrid(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        'Put user code to initialize the page here
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not Me.IsPostBack Then
            Me.FormID = "CollAssetRelease"
            Me.cgid = Me.GroubDbID
            InitialDefaultPanel()
        End If

    End Sub

#Region " InitialDefaultPanel "
    Private Sub InitialDefaultPanel()
        PnlSearch.Visible = True
        PnlList.Visible = False
        PnlListDetail.Visible = False
    End Sub
#End Region

    Sub Bindgrid(ByVal cmdWhere As String, ByVal cmSort As String)
        Dim dtvEntity As New DataView
        Dim dtsEntity As New DataTable
        Dim oAssetRelease As New Parameter.AssetRelease

        With oAssetRelease
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oAssetRelease = m_AssetRelease.AssetReleaseList(oAssetRelease)

        With oAssetRelease
            lblrecord.Text = CType(.TotalRecord, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecord
        End With

        dtsEntity = oAssetRelease.listData
        dtsEntity.TableName = "Table"

        dtvEntity = dtsEntity.DefaultView
        dtvEntity.Sort = Me.SortBy

        dtg.DataSource = dtvEntity

        Try
            dtg.DataBind()
        Catch ex As Exception

        End Try

        PagingFooter()
    End Sub

    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        InitialDefaultPanel()
        cboSearchBy.ClearSelection()
        txtSearchBy.Text = ""
        Me.SearchBy = ""
        Me.SortBy = ""
        Me.Bindgrid(Me.SearchBy, Me.SortBy)
        PnlSearch.Visible = True
        PnlList.Visible = False
        lblMessage.Text = ""
    End Sub

    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Buttonsearch.Click
        Me.SearchBy = ""
        Me.SortBy = ""
        lblMessage.Text = ""

        If oCGID.BranchID.Trim <> "" Then
            Me.SearchBy = "CGID='" & oCGID.BranchID & "'"
            Me.cgid = oCGID.BranchID
        End If

        If cboSearchBy.SelectedIndex <> 0 And txtSearchBy.Text <> "" Then
            If Me.SearchBy = "" Then
                Me.SearchBy = cboSearchBy.SelectedItem.Value.Trim & " like '%" + txtSearchBy.Text.Trim + "%'"
            Else
                Me.SearchBy = Me.SearchBy & " and " & cboSearchBy.SelectedItem.Value.Trim & " like '%" + txtSearchBy.Text.Trim + "%'"
            End If
        End If

        Bindgrid(Me.SearchBy, Me.SortBy)
        PnlList.Visible = True
        PnlListDetail.Visible = False
    End Sub

    Private Sub viewReleaseDetail(ByVal agreementno As String, ByVal ralNo As String)
        Dim oAssetRelease As New Parameter.AssetRelease
        Dim dt As New DataTable
        Context.Trace.Write("viewReleaseDetail")
        With oAssetRelease
            .strConnection = getConnectionString()
            .AgreementNo = agreementno
            .BusinessDate = Me.BusinessDate
        End With

        oAssetRelease = m_AssetRelease.AssetReleaseDetail(oAssetRelease)

        If Not oAssetRelease Is Nothing Then
            Context.Trace.Write("viewReleaseDetail - oAssetRelease is not nothing !")
            dt = oAssetRelease.listData

            hypAgreementNo.Text = CStr(dt.Rows(0).Item("AgreementNo"))
            hypCustomerName.Text = CStr(dt.Rows(0).Item("CustomerName"))
            lblAddress.Text = CStr(dt.Rows(0).Item("MailingAddress"))
            lblDuedate.Text = CStr(dt.Rows(0).Item("Duedate"))
            lblOverDueDays.Text = CStr(dt.Rows(0).Item("EndPastDuedays"))
            lblInstallmentNo.Text = CStr(dt.Rows(0).Item("InstallmentNo"))
            lblInstallmentAmount.Text = CStr(FormatNumber(dt.Rows(0).Item("InstallmentAmount"), 0))
            lblODAmount.Text = CStr(FormatNumber(dt.Rows(0).Item("EndPastDueAmt"), 0))
            lblLateCharges.Text = CStr(FormatNumber(dt.Rows(0).Item("LC"), 0))
            lblOSBallance.Text = CStr(FormatNumber(dt.Rows(0).Item("OS_Gross"), 0))
            lblCollExpense.Text = CStr(FormatNumber(dt.Rows(0).Item("CollectionExpense"), 0))

            lblAsset.Text = CStr(dt.Rows(0).Item("Description"))
            lblLicensePlate.Text = CStr(dt.Rows(0).Item("LicensePlate"))
            lblChassisNo.Text = CStr(dt.Rows(0).Item("ChassisNo"))
            lblEngineNo.Text = CStr(dt.Rows(0).Item("EngineNo"))
            lblYear.Text = CStr(dt.Rows(0).Item("Year")).Trim
            lblColor.Text = CStr(dt.Rows(0).Item("Color")).Trim

            lblRepossesDate.Text = CStr(dt.Rows(0).Item("RepossesDate"))
            lblRepossesBy.Text = CStr(dt.Rows(0).Item("RepossesBy"))
            lblAssetStatus.Text = CStr(dt.Rows(0).Item("AssetStatus"))
            lblSellingPrice.Text = CStr(FormatNumber(dt.Rows(0).Item("SellingPrice"), 0))
            lblSellingDate.Text = CStr(dt.Rows(0).Item("SellingDate"))

            txtReleaseDate.Text = Format(Me.BusinessDate, "dd/MM/yyyy")            

            Me.AssetTypeID = CStr(dt.Rows(0).Item("AssetTypeID")).Trim
            Me.AssetSeqNo = CInt(dt.Rows(0).Item("AssetSeqNo"))
            Me.UntilDate = ConvertDate2(CStr(dt.Rows(0).Item("UntilDate")))
            Me.ApplicationID = CStr(dt.Rows(0).Item("ApplicationID")).Trim
            Me.BranchID = CStr(dt.Rows(0).Item("BranchID")).Trim
            Me.RepossesSeqNo = CInt(dt.Rows(0).Item("RepossesSeqNo"))

        End If

        PnlSearch.Visible = False
        PnlList.Visible = False
        PnlListDetail.Visible = True
    End Sub

    Private Sub viewCheckList(ByVal AssetTypeID As String)
        Dim oAssetRelease As New Parameter.AssetRelease
        Dim dt As New DataTable
        Context.Trace.Write("viewCheckList")

        With oAssetRelease
            .strConnection = getConnectionString()
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationID
            .AssetSeqNo = Me.AssetSeqNo
            .RepossesSeqNo = Me.RepossesSeqNo
        End With

        oAssetRelease = m_AssetRelease.AssetReleaseCheckList(oAssetRelease)

        If Not oAssetRelease Is Nothing Then
            Context.Trace.Write("viewCheckList - oAssetRelease is not nothing !")
            dt = oAssetRelease.listData
            dtgCheckList.DataSource = dt
            dtgCheckList.DataBind()

            PnlSearch.Visible = False
            PnlList.Visible = False
            PnlListDetail.Visible = True
        End If


    End Sub

    Private Sub imbCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        PnlListDetail.Visible = False
        Me.Bindgrid(Me.SearchBy, Me.SortBy)
        PnlSearch.Visible = True
        PnlList.Visible = True
    End Sub

    Private Sub dtgCheckList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgCheckList.ItemDataBound
        Dim txtAnswer As New TextBox
        Dim rbAnswer As New RadioButtonList
        Dim txtAnswerReceive As New TextBox
        Dim rbAnswerReceive As New RadioButtonList
        Dim txtDesc As New TextBox
        Dim lbTemp As New LinkButton

        Context.Trace.Write("Proses dtgCheckList_ItemDataBound !!")
        Me.AgreementNo = e.Item.Cells(1).Text.Trim
        Me.CustomerID = e.Item.Cells(2).Text.Trim


        If e.Item.ItemIndex >= 0 Then
            Context.Trace.Write("e.Item.ItemIndex >= 0 ")
            Context.Trace.Write("Add customer view")
            '*** Add customer view
            '            lbTemp = CType(e.Item.FindControl("lbCustomer"), LinkButton)
            lbTemp.Attributes.Add("onclick", "OpenViewCustomer('" & Me.CustomerID.Trim & "')")

            Context.Trace.Write("Add agreement view")
            '*** Add agreement view
            ' lbTemp = CType(e.Item.FindControl("lbCustomer"), LinkButton)
            lbTemp.Attributes.Add("onclick", "OpenViewAgreement('" & Me.AgreementNo.Trim & "')")

            Context.Trace.Write("Answer")
            txtAnswer = CType(e.Item.FindControl("txtAnswer"), TextBox)
            rbAnswer = CType(e.Item.FindControl("rbAnswer"), RadioButtonList)
            txtDesc = CType(e.Item.FindControl("txtDescription"), TextBox)

            Context.Trace.Write("AnswerRecieve")
            txtAnswerReceive = CType(e.Item.FindControl("txtAnswerReceive"), TextBox)
            rbAnswerReceive = CType(e.Item.FindControl("rbAnswerReceive"), RadioButtonList)

            'If e.Item.Cells(7).Text.Trim <> "" Then         ' klo pertanyaannya jenis Y/N
            '    Context.Trace.Write("klo pertanyaannya jenis Y/N")
            '    txtAnswer.Visible = False
            '    rbAnswer.Visible = True
            '    rbAnswer.SelectedIndex = rbAnswer.Items.IndexOf(rbAnswer.Items.FindByValue(CStr(e.Item.Cells(7).Text)))
            '    rbAnswer.Enabled = False
            '    txtAnswerReceive.Visible = False
            '    rbAnswerReceive.Visible = True
            'Else
            '    Context.Trace.Write("klo pertanyaannya Bukan jenis Y/N")
            '    txtAnswer.Visible = True
            '    txtAnswer.Text = CStr(e.Item.Cells(8).Text)
            '    rbAnswer.Visible = False
            '    txtAnswer.Enabled = False
            '    txtAnswerReceive.Visible = True
            '    rbAnswerReceive.Visible = False

            'End If

            If e.Item.Cells(7).Text.Trim <> "" Then         ' klo pertanyaannya jenis Y/N
                Context.Trace.Write("klo pertanyaannya jenis Y/N")
                txtAnswer.Visible = False
                rbAnswer.Visible = True
                rbAnswer.SelectedIndex = rbAnswer.Items.IndexOf(rbAnswer.Items.FindByValue(CStr(e.Item.Cells(7).Text)))
                rbAnswer.Attributes.Add("readonly", "true")
                txtAnswerReceive.Visible = False
                rbAnswerReceive.Visible = True
            Else
                Context.Trace.Write("klo pertanyaannya Bukan jenis Y/N")
                txtAnswer.Visible = True
                txtAnswer.Text = CStr(e.Item.Cells(8).Text)
                rbAnswer.Visible = False
                txtAnswer.Enabled = False
                txtAnswerReceive.Visible = True
                rbAnswerReceive.Visible = False

            End If

            'Context.Trace.Write("txtDesc")
            'txtDesc.Text = CStr(e.Item.Cells(9).Text).Trim
            'txtDesc.Enabled = False

        End If
    End Sub

    Private Sub imbSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSave.Click

        'Validasi 

        If Me.BusinessDate < ConvertDate2(txtReleaseDate.Text) Then
            ShowMessage(lblMessage, "Tanggal Pelepasan Asset tidak boleh > tanggal hari ini", True)            
            Exit Sub
        End If

        lblMessage.Text = ""

        'Ambil semua data yang diperlukan dari layar detail

        Dim oAssetRelease As New Parameter.AssetRelease

        With oAssetRelease
            .strConnection = GetConnectionString()
            .CGID = Me.cgid
            .AssetSeqNo = Me.AssetSeqNo
            .RepossesDate = ConvertDate2(lblRepossesDate.Text.Trim)

            .AgreementNo = hypAgreementNo.Text.Trim
            .AssetDescription = lblAsset.Text.Trim
            .EndpastDueDays = CInt(lblOverDueDays.Text.Trim)

            .ReleaseDate = ConvertDate2(txtReleaseDate.Text.Trim)
            .ReleaseTo = txtReleaseTo.Text.Trim
            .ReleaseBy = txtReleaseBy.Text.Trim
            .ReleaseNotes = txtNotes.Text.Trim

            .RepossesSeqNo = Me.RepossesSeqNo
            .BussinessDate = Me.BusinessDate
            .ApplicationID = Me.ApplicationID
            .BranchId = Me.BranchID
            .CollectorID = lblRepossesBy.Text.Trim

        End With




        '==============================
        ' Check List Save
        '==============================

        Dim txtbox As New TextBox
        Dim rb As New RadioButtonList
        Dim txtboxDesc As New TextBox
        Dim dtdetail As New DataTable("tbldetail")
        Dim drdetail As DataRow
        dtdetail.Columns.Add("CollectorID")
        dtdetail.Columns.Add("CheckListID")
        dtdetail.Columns.Add("Notes")
        dtdetail.Columns.Add("YNQuestion")
        dtdetail.Columns.Add("Quantity")

        Dim i As Integer
        For i = 0 To dtgCheckList.Items.Count - 1
            drdetail = dtdetail.NewRow
            txtbox = CType(dtgCheckList.Items(i).FindControl("txtAnswerReceive"), TextBox)
            rb = CType(dtgCheckList.Items(i).FindControl("rbAnswerReceive"), RadioButtonList)
            txtboxDesc = CType(dtgCheckList.Items(i).FindControl("txtDescriptionReceive"), TextBox)

            With oAssetRelease

                .strConnection = GetConnectionString()
                .CGID = Me.cgid
                .AssetSeqNo = Me.AssetSeqNo
                .RepossesSeqNo = Me.RepossesSeqNo
                .ApplicationID = Me.ApplicationID
                .BranchId = Me.BranchID
                .BussinessDate = Me.BusinessDate
                '.CollectorID = lblRepossesBy.Text.Trim
                drdetail("CollectorID") = lblRepossesBy.Text.Trim
                '.CheckListID = dtgCheckList.Items(i).Cells(5).Text.Trim
                drdetail("CheckListID") = dtgCheckList.Items(i).Cells(5).Text.Trim

                If dtgCheckList.Items(i).Cells(7).Text.Trim <> "" Then
                    '.YNQuestion = rb.SelectedItem.Value
                    drdetail("YNQuestion") = rb.SelectedItem.Value
                    '.Quantity = ""
                    drdetail("Quantity") = ""
                Else
                    '.YNQuestion = ""
                    drdetail("YNQuestion") = ""
                    '.Quantity = txtbox.Text.Trim
                    drdetail("Quantity") = txtbox.Text.Trim
                End If

                '.Notes = txtboxDesc.Text.Trim
                drdetail("Notes") = txtboxDesc.Text.Trim
            End With
            dtdetail.Rows.Add(drdetail)
        Next
        oAssetRelease.listData = dtdetail
        If Not m_AssetRelease.AssetReleaseCheckSave(oAssetRelease) Then
            ShowMessage(lblMessage, "Simpan data gagal!", True)
        Else
            ShowMessage(lblMessage, "Simpan data berhasil!", False)
        End If

        Bindgrid(Me.SearchBy, Me.SortBy)
        PnlSearch.Visible = True
        PnlList.Visible = True
        PnlListDetail.Visible = False
        clear()
    End Sub

    Private Sub dtg_ItemCommand1(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtg.ItemCommand
        Me.AgreementNo = e.Item.Cells(1).Text.Trim
        Me.RALNo = e.Item.Cells(6).Text.Trim
        Me.AssetTypeID = e.Item.Cells(7).Text.Trim
        Me.CollectorID = e.Item.Cells(1).Text.Trim

        If e.CommandName = "RELEASE" Then
            If checkFeature(Me.Loginid, Me.FormID, "Relea", Me.AppId) Then
                If sessioninvalid() Then
                    Exit Sub
                End If
            End If
            Context.Trace.Write("Me.AgreementNo = " & Me.AgreementNo)
            Context.Trace.Write("Me.RALNo = " & Me.RALNo)
            Context.Trace.Write("Me.AssetTypeID = " & Me.AssetTypeID)
            viewReleaseDetail(Me.AgreementNo, Me.RALNo)
            viewCheckList(Me.AssetTypeID)
        End If
    End Sub

    Private Sub dtg_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtg.ItemDataBound
        Me.AgreementNo = e.Item.Cells(0).Text.Trim
        Me.CustomerID = e.Item.Cells(1).Text.Trim
        Dim lbTemp As LinkButton

        If e.Item.ItemIndex >= 0 Then

            '*** Add customer view
            lbTemp = CType(e.Item.FindControl("lbCustomer"), LinkButton)
            lbTemp.Attributes.Add("onclick", "OpenViewCustomer('" & Me.CustomerID.Trim & "')")


            '*** Add agreement view
            lbTemp = CType(e.Item.FindControl("lbAgreement"), LinkButton)
            lbTemp.Attributes.Add("onclick", "OpenViewAgreement('" & Me.AgreementNo.Trim & "')")
        End If
    End Sub
    Private Sub clear()
        txtReleaseTo.Text = ""
        txtReleaseBy.Text = ""
        txtNotes.Text = ""
    End Sub
End Class