﻿#Region "Imports"
Imports System.Text
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region


Public Class InsBlokir
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private Dcontroller As New DataUserControlController
    Private m_controller As New InsBlokirController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region
#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(viewstate("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("vwsCmdWhere") = Value
        End Set
    End Property    
    Private Property Sort() As String
        Get
            Return CType(viewstate("Sort"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Sort") = Value
        End Set
    End Property
#End Region
#Region "LinkTo"    
    Function LinkToCustomer(ByVal strCustomerID As String, ByVal strStyle As String) As String
        Return "javascript:OpenWinCustomer('" & strCustomerID & "','" & strStyle & "')"
    End Function
    Function LinkTo(ByVal strApplicationID As String, ByVal strStyle As String) As String
        Return "javascript:OpenWinApplication('" & strApplicationID & "','" & strStyle & "')"
    End Function
#End Region

#Region "Page Load"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.FormID = "BLOKIRBYRRLS"
            If CheckForm(Me.Loginid, Me.FormID, "MAXILOAN") Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            Me.Sort = "ApplicationId ASC"
            Me.CmdWhere = "All"
            InitialPanel()

 
            With cboBranch
                .DataSource = Dcontroller.GetBranchName(GetConnectionString, sesBranchId)
                .DataValueField = "ID"
                .DataTextField = "Name"
                .DataBind()
                .Items.Insert(0, "Select One")
                .Items(0).Value = "0"
                .SelectedValue = sesBranchId.Replace("'", "")
            End With

            ' BindGrid(Me.CmdWhere)

            If Request.QueryString("Page") = "Release" Then

            End If

        End If
    End Sub
#End Region
    Sub BindGrid(ByVal cmdWhere As String)
        Dim dtEntity As DataTable = Nothing
        Dim oCustomClass As New Parameter.InsBlokir
        pnlGrid.Visible = True
        oCustomClass.PageSize = pageSize
        oCustomClass.WhereCond = cmdWhere
        oCustomClass.SortBy = Me.Sort
        oCustomClass.CurrentPage = currentPage
        oCustomClass.strConnection = GetConnectionString()
        oCustomClass = m_controller.GetInsBlokir(oCustomClass)
        If Not oCustomClass Is Nothing Then
            dtEntity = oCustomClass.ListData
            recordCount = oCustomClass.TotalRecords            
        Else
            recordCount = 0
        End If
        dtgList.DataSource = dtEntity.DefaultView
        dtgList.CurrentPageIndex = 0
        dtgList.DataBind()
        PagingFooter()
    End Sub
#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGrid(Me.CmdWhere)
    End Sub
    Private Sub imgbtnPageNumb_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int32)
                BindGrid(Me.CmdWhere)
            End If
        End If
    End Sub
#End Region
    Private Sub dtgList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgList.ItemDataBound
        Dim lblName As New HyperLink
        Dim lblAgreementNo As New HyperLink
        If e.Item.ItemIndex >= 0 Then
            Dim TglDate As DateTime = CDate(e.Item.Cells(4).Text.Trim)
            e.Item.Cells(4).Text = TglDate.ToString("dd-MM-yyyyy")

            lblName = CType(e.Item.FindControl("lblName"), HyperLink)
            lblAgreementNo = CType(e.Item.FindControl("lblAgreementNo"), HyperLink)

            lblName.NavigateUrl = LinkToCustomer(e.Item.Cells(11).Text.Trim, "AccAcq")
            lblAgreementNo.NavigateUrl = LinkTo(e.Item.Cells(10).Text.Trim, "AccAcq")

            Dim AmountOverdue As String = CStr(FormatNumber(e.Item.Cells(5).Text, 2))
            Dim LateCharges As String = CStr(FormatNumber(e.Item.Cells(6).Text, 2))

            e.Item.Cells(5).Text = AmountOverdue
            e.Item.Cells(6).Text = LateCharges
        End If
    End Sub
    Private Sub dtgList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgList.ItemCommand
        Dim err As String = ""
        Dim oGroupSupplierAccount As New Parameter.InsBlokir
        If e.CommandName = "Edit" Then
            If CheckFeature(Me.Loginid, Me.FormID, "Edit", "MAXILOAN") Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            pnlList.Visible = False
            pnlRelease.Visible = True
            ButtonCancel.CausesValidation = False

            lblNoSP.Text = e.Item.Cells(3).Text
            lblTglSP.Text = e.Item.Cells(4).Text
            lblAmountOverdue.Text = e.Item.Cells(5).Text
            lblLateCharges.Text = e.Item.Cells(6).Text
            lblCollector.Text = e.Item.Cells(7).Text
            lblNamaCustomer.Text = e.Item.Cells(8).Text
            lblNoKontrak.Text = e.Item.Cells(9).Text
            hdnApplicationId.Value = e.Item.Cells(10).Text
        End If
    End Sub
    Sub InitialPanel()
        pnlList.Visible = True
        pnlGrid.Visible = False
        pnlRelease.Visible = False
        txtPage.Text = "1"
    End Sub

    Protected Sub ButtonSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonSave.Click
        Dim customClass As New Parameter.InsBlokir
        Dim ErrMessage As String = ""
        Dim status As Boolean = False

        With customClass
            .ApplicationId = hdnApplicationId.Value.Trim
            .SPNo = lblNoSP.Text.Trim
            .strConnection = GetConnectionString()
        End With

        m_controller.ReleaseBlokirBayar(customClass)
        ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
        BindGrid(Me.CmdWhere)
        pnlList.Visible = True
        pnlRelease.Visible = False
    End Sub

    Protected Sub ButtonCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonCancel.Click
        Response.Redirect("InsBlokir.aspx")
    End Sub

    Protected Sub ButtonSearch_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonSearch.Click
        Dim search As String = ""

        If cboBranch.SelectedValue = "ALL" And
            txtSearch.Text = "" Then
            CmdWhere = "All"
        Else
            If Not cboBranch.SelectedValue = "ALL" Then
                CmdWhere = " Agreement.BranchId = '" & cboBranch.SelectedValue & "'"
            Else
                CmdWhere = ""
            End If

            If txtSearch.Text.Contains("%") Then                
                search = "'%" & txtSearch.Text.Replace("%", " ").Trim & "%'"
            Else
                search = "'%" & txtSearch.Text.Trim & "%'"
            End If


            If txtSearch.Text = "" Then
                If CmdWhere = "" Then
                    CmdWhere = "All"
                End If
            Else
                If CmdWhere = "" Then
                    CmdWhere = "" & cboSearch.SelectedValue & " = " & search & ""
                Else
                    CmdWhere += " and " & cboSearch.SelectedValue & " = " & search & ""
                End If
            End If

        End If

        BindGrid(CmdWhere)
    End Sub

    Protected Sub ButtonReset_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonReset.Click
        Response.Redirect("InsBlokir.aspx")
    End Sub
End Class