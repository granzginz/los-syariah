﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PrintRepossesCL.aspx.vb"
    Inherits="Maxiloan.Webform.ARMgt.PrintRepossesCL" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcBranchCollection" Src="../../Webform.UserController/UcBranchCollection.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PrintRepossesCL</title>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;

        var ServerName = '<%#Request.ServerVariables("SERVER_NAME")%>';
        var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);

        function fback() {
            history.go(-1);
            return false;
        }

        function OpenWinCust(pStrCustID, pStrType) {
            var lStrAddress;
            if (pStrType == 'P')
                lStrAddress = 'ViewSearch.aspx';
            else
                lStrAddress = 'ViewSearchCompany.aspx';

            window.open('http://' + ServerName + '/' + App + '/Application/SmartSearch/' + lStrAddress + '?custid=' + pStrCustID, 'WinLink', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0,scrollbars=1,resizable=1,center=1');
        }

        function OpenWinAsset(pStrApplicationID, pStrAgreementNo, asq) {
            window.open('http://' + ServerName + '/' + App + '/Application/SmartSearch/perAsset.aspx?applicationid=' + pStrApplicationID + '&agreementno=' + pStrAgreementNo + '&assetseqno=' + asq, 'WinLink', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=1,resizable=1,center=1');
        }

        function PlanRALChange() {
            var myID = 'UcRALDate_txtDate';
            var objDate = eval('document.forms[0].' + myID);
            var result

            if (document.forms[0].rbRAL_0.checked = true) {
                result = 'Plan';
            }
            if (document.forms[0].rbRAL_1.checked = true) {
                result = 'Undo Plan';
            }

            alert(result);
            if (result == 'Undo Plan') {
                objDate.disabled = true;
                document.forms[0].all.UcRALDate_txtDate_imgCalender.disabled = true;
                document.forms[0].all.UcRALDate_txtDate_imgCalender.disabled = true;
                document.forms[0].UcRALDate_txtDate.value = '';
            }

        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>    
        <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">           
                <h3>
                    CETAK CHECKLIST DAN SURAT PELUNASAN
                </h3>
            </div>
        </div>  
        <asp:Panel ID="PnlSearch" runat="server" Width="100%">
        <div class="form_box">
	        <div class="form_single">
                <label  class ="label_req">Collection Group</label>
                <uc1:UcBranchCollection id="oCGID" runat="server"></uc1:UcBranchCollection>	        
            </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Cari Berdasarkan</label>
                <asp:DropDownList ID="cboSearchBy" runat="server">
                    <asp:ListItem Value="0">Select One</asp:ListItem>
                    <asp:ListItem Value="Agreement.AgreementNo">No Kontrak</asp:ListItem>
                    <asp:ListItem Value="Customer.Name">Nama Customer</asp:ListItem>
                    <asp:ListItem Value="AgreementAsset.LicensePlate">No Polisi</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearchBy" runat="server" ></asp:TextBox>
	        </div>
        </div>
        <div class="form_button">
            <asp:Button ID="Buttonsearch" runat="server"  Text="Search" CssClass ="small button blue"></asp:Button>&nbsp;
            <asp:Button ID="ButtonReset" runat="server"  Text="Reset" CssClass ="small button gray"
                CausesValidation="False"></asp:Button>
	    </div>              
        </asp:Panel>
        <asp:Panel ID="PnlList" runat="server" Width="100%">
        <div class="form_box_title">
            <div class="form_single">              
                <h4>
                    DAFTAR ASSET DITARIK
                </h4>
            </div>
        </div>
        <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtg" runat="server" Width="100%" OnSortCommand="sorting"
                        AutoGenerateColumns="False" AllowSorting="True" CssClass="grid_general" BorderStyle="None" BorderWidth="0">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="hynAgreementNo" runat="server" Text='<%#container.dataitem("AgreementNo")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Name" HeaderText="NAMA CUSTOMER">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="hynCustomerName" runat="server" Text='<%# container.dataitem("Name")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" SortExpression="Customer.CustomerID" HeaderText="CUSTOMER ID">
                                <ItemTemplate>
                                    <asp:Label ID="lblCustomerID" runat="server" Text='<%# container.dataitem("CustomerID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" SortExpression="Agreement.ApplicationID" HeaderText="APPLICATIONID">
                                <ItemTemplate>
                                    <asp:Label ID="lblApplicationID" runat="server" Text='<%# container.dataitem("ApplicationID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" SortExpression="AssetRepossessionOrder.CollectorId"
                                HeaderText="ExecutorID">
                                <ItemTemplate>
                                    <asp:Label ID="lblExecutorID" runat="server" Text='<%# container.dataitem("CollectorId")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="Description" SortExpression="Description" HeaderText="NAMA ASSET">                                
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="RepossesDate" SortExpression="RepossesDate" HeaderText="TGL TARIK">                                
                            </asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="CETAK BASTK">                                
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbPrintCheckList" runat="server" CommandName="PrintCheckList"
                                        ImageUrl="../../images/IconPrinter.gif"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="CETAK SURAT BATAS AKHIR PENYELESAIAN">                                
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbPrintPrepaymentLetter" runat="server" CommandName="PrintPrepaymentLetter"
                                        ImageUrl="../../images/IconPrinter.gif"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>                        
                </asp:DataGrid>         
                <div class="button_gridnavigation">
	                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
		                CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
		                CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
		                CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
		                CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
	                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
		                EnableViewState="False"></asp:Button>
	                <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage"
				        MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer"
				        CssClass="validator_general"></asp:RangeValidator>
			        <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
				        ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general">
			        </asp:RequiredFieldValidator>              
                </div>
                <div class="label_gridnavigation">
	                <asp:Label ID="lblPage" runat="server"></asp:Label>of
	                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
	                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                </div>
            </div>            
        </div>
        </div>
    </asp:Panel>           
    </form>
</body>
</html>
