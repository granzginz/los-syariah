﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="InqContinueCreditRequest.aspx.vb"
    Inherits="Maxiloan.Webform.ARMgt.InqContinueCreditRequest" %>

<%@ Register TagPrefix="uc1" TagName="UcBranchCollection" Src="../../../Webform.UserController/UcBranchCollection.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Inquiry ContinueCreditRequest</title>
    <link rel="Stylesheet" href="../../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../../Include/General.css" type="text/css" />
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;
        var ServerName = '<%=Request.ServerVariables("SERVER_NAME")%>';
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);

        function fback() {
            history.go(-1);
            return false;
        }

        function OpenWinCust(pStrCustID, pStrType) {
            var lStrAddress;
            if (pStrType == 'P')
                lStrAddress = 'ViewSearch.aspx';
            else
                lStrAddress = 'ViewSearchCompany.aspx';

            window.open('http://' + ServerName + '/' + App + '/Application/SmartSearch/' + lStrAddress + '?custid=' + pStrCustID, 'WinLink', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0,scrollbars=1,resizable=1,center=1');
        }

        function OpenWinAgreement(pStrAgreementNo) {
            window.open('http://' + ServerName + '/' + App + '/Application/SmartSearch/ViewAgreementDetail.aspx?AgreementNo=' + pStrAgreementNo, 'WinLink', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0,scrollbars=1,resizable=1,center=1');
        }

        function OpenWinAsset(pStrApplicationID, pStrAgreementNo, asq) {
            window.open('http://' + ServerName + '/' + App + '/Application/SmartSearch/perAsset.aspx?applicationid=' + pStrApplicationID + '&agreementno=' + pStrAgreementNo + '&assetseqno=' + asq, 'WinLink', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=1,resizable=1,center=1');
        }

        function PlanRALChange() {
            var myID = 'UcRALDate_txtDate';
            var objDate = eval('document.forms[0].' + myID);
            var result

            if (document.forms[0].rbRAL_0.checked = true) {
                result = 'Plan';
            }
            if (document.forms[0].rbRAL_1.checked = true) {
                result = 'Undo Plan';
            }

            alert(result);
            if (result == 'Undo Plan') {
                objDate.disabled = true;
                document.forms[0].all.UcRALDate_txtDate_imgCalender.disabled = true;
                document.forms[0].all.UcRALDate_txtDate_imgCalender.disabled = true;
                document.forms[0].UcRALDate_txtDate.value = '';
            }
        }

        function OpenViewAgreement(pAgreementNo) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.LoanOrg/Credit/ViewStatementofAccount.aspx?AgreementNo=' + pAgreementNo + '&Style=Collection', 'AgreementView', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenViewCustomer(pCustomerID) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.LoanOrg/Credit/ViewPersonalCustomer.aspx?CustomerID=' + pCustomerID + '&Style=Collection', 'CustomerView', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">           
            <h3>                
                INQUIRY AKTIVASI ULANG PEMBIAYAAN
            </h3>
        </div>
    </div>      
    <asp:Panel ID="PnlSearch" runat="server">
        <div class="form_box">
	        <div class="form_single">
                <label  class ="label_req">Collection Group</label>
                <uc1:UcBranchCollection id="oCGID" runat="server"></uc1:UcBranchCollection>	        
            </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Cari Berdasarkan</label>
                <asp:DropDownList ID="cboSearchBy" runat="server">
                    <asp:ListItem Value="Select One">Select One</asp:ListItem>
                    <asp:ListItem Value="a.AgreementNo">No Kontrak</asp:ListItem>
                    <asp:ListItem Value="c.NAME">Nama Customer</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearchBy" runat="server" ></asp:TextBox>
	        </div>
        </div>
        <div class="form_button">
            <asp:Button ID="Buttonsearch" runat="server"  Text="Search" CssClass ="small button blue"></asp:Button>&nbsp;
            <asp:Button ID="ButtonReset" runat="server"  Text="Reset" CssClass ="small button gray"
                CausesValidation="False"></asp:Button>
	    </div>             
        <!--<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<DATA GRID>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>-->
        </asp:Panel>
        <asp:Panel ID="PnlList" runat="server">
        <div class="form_box_title">
            <div class="form_single">              
                <h4>
                    DAFTAR KONTRAK
                </h4>
            </div>
        </div>                 
        <!--<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<PANEL PAGING>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>-->
        <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtg" runat="server" Width="100%" OnSortCommand="sorting"
                        BorderStyle="None" BorderWidth="0"
                        AllowSorting="True" AutoGenerateColumns="False" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:BoundColumn Visible="False" DataField="AgreementNo"></asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="ApplicationID"></asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="BranchID"></asp:BoundColumn>
                            <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyAgreementNo" runat="server" Text='<%#container.dataitem("AgreementNo")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="CustomerName" HeaderText="NAMA CUSTOMER">
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyCustomerName" runat="server" Text='<%# container.dataitem("CustomerName")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="RequestDate" SortExpression="RequestDate" HeaderText="REQUEST DATE">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="RequestNo" SortExpression="RequestNo" HeaderText="REQUEST NO">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="AssetDescription" SortExpression="AssetDescription" HeaderText="NAMA ASSET">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="ApprovalStatus" SortExpression="ApprovalStatus" HeaderText="APR STS">
                            </asp:BoundColumn>
<%--                            <asp:TemplateColumn HeaderText="PILIH">                                
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbRequest" runat="server" ImageUrl="../../../images/iconrelease.gif"
                                        CommandName="REQUEST"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
--%>                            <asp:TemplateColumn Visible="False" HeaderText="CUSTOMER ID">
                                <ItemTemplate>
                                    <asp:Label ID="lblCustomerId" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CustomerID") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>                        
                </asp:DataGrid>                    
                <div class="button_gridnavigation">
	                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../../Images/grid_navbutton01.png"
		                CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../../Images/grid_navbutton02.png"
		                CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../../Images/grid_navbutton03.png"
		                CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../../Images/grid_navbutton04.png"
		                CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
	                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
		                EnableViewState="False"></asp:Button>
	                <asp:RangeValidator   CssClass="validator_general"   ID="rgvGo" runat="server" ControlToValidate="txtPage"
                        MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer"></asp:RangeValidator>
                    <asp:RequiredFieldValidator    CssClass="validator_general"   ID="rfvGo" runat="server" ControlToValidate="txtPage"
                        ErrorMessage="No Halaman Salah" Display="Dynamic"></asp:RequiredFieldValidator>   
                </div>
                <div class="label_gridnavigation">
	                <asp:Label ID="lblPage" runat="server"></asp:Label>of
	                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
	                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                </div>
            </div>
        </div>            
        </div> 
        </asp:Panel>    
        <asp:Panel ID="PnlListDetail" runat="server">        
        <div class="form_box">	        
		        <div class="form_left">
                    <label>No Kontrak</label>
                    <asp:HyperLink ID="hypAgreementNo" runat="server"></asp:HyperLink>
		        </div>
		        <div class="form_right">	
                    <label>Nama Customer</label>		
                    <asp:HyperLink ID="hypCustomerName" runat="server"></asp:HyperLink>
		        </div>	        
        </div>
        <div class="form_box">
	        <div class="form_single">
		        <label>Alamat</label>
                <asp:Label ID="lblAddress" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">	        
		        <div class="form_left">
                    <label>Angsuran Ke</label>
                    <asp:Label ID="lblInstallmentNo" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">	
                    <label>Jumlah Angsuran</label>		
                    <asp:Label ID="lblInstallmentAmount" runat="server"></asp:Label>
		        </div>	        
        </div>  
        <div class="form_box">	        
		        <div class="form_left">
                    <label>Tanggal Jatuh Tempo</label>
                    <asp:Label ID="lblDuedate" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">		
                    <label>Jumlah Hari Telat</label>	
                    <asp:Label ID="lblOverDueDays" runat="server"></asp:Label>
		        </div>	        
        </div>  
        <div class="form_box">	        
		        <div class="form_left">
                    <label>Jumlah Tunggakan</label>
                    <asp:Label ID="lblODAmount" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">	
                    <label>Denda Keterlambatan</label>		
                    <asp:Label ID="lblLateCharges" runat="server"></asp:Label>
		        </div>	        
        </div>  
        <div class="form_box">	        
		        <div class="form_left">
                    <label>Biaya Tagih</label>
                    <asp:Label ID="lblOSBillingCharges" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">	
                    <label>Biaya Collection</label>		
                    <asp:Label ID="lblCollExpense" runat="server"></asp:Label>
		        </div>	        
        </div>  
        <div class="form_box">	       
		        <div class="form_left">
                    <label>Sisa A/R</label>
                    <asp:Label ID="lblOSBallance" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">			
		        </div>	        
        </div>  
        <div class="form_box">
	        <div class="form_single">
                <label>Nama Asset</label>
		        <asp:Label ID="lblAsset" runat="server"></asp:Label>
	        </div>
        </div>  
        <div class="form_box">	        
		        <div class="form_left">
                    <label>No Rangka</label>
                    <asp:Label ID="lblChassisNo" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">		
                    <label>No Mesin</label>
                    <asp:Label ID="lblEngineNo" runat="server"></asp:Label>
		        </div>	        
        </div>
        <div class="form_box">	        
		        <div class="form_left">
                    <label>No Polisi</label>
                    <asp:Label ID="lblLicensePlate" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">	
                    <label>Tahun Produksi</label>		
                    <asp:Label ID="lblYear" runat="server"></asp:Label>
		        </div>	        
        </div>
        <div class="form_box">	        
		        <div class="form_left">
                    <label>Warna</label>
                    <asp:Label ID="lblColor" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">			
		        </div>	        
        </div>
        <div class="form_box">	        
		        <div class="form_left">
                    <label>Tanggal Tarik</label>
                    <asp:Label ID="lblRepossesDate" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">	
                    <label>Tanggal Masuk Inventory</label>		
                    <asp:Label ID="lblExpectedDate" runat="server"></asp:Label>
		        </div>	        
        </div>        
        <div class="form_box">
	        <div class="form_single">
		        <label class ="label_req">Alasan</label>
                <asp:DropDownList ID="cboReason" runat="server" AutoPostBack="true" OnSelectedIndexChanged="cboReason_IndexChanged">
                </asp:DropDownList>                
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"  CssClass="validator_general"
                    Display="Dynamic" ControlToValidate="cboReason" ErrorMessage="Harap Pilih Alasan"></asp:RequiredFieldValidator>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
		        <label class ="label_req">Disetujui Oleh</label>
                <asp:DropDownList ID="cboApproveBy" runat="server">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"  CssClass="validator_general"
                Display="Dynamic" ControlToValidate="cboApproveBy" ErrorMessage="Harap pilih Disetujui Oleh"
                InitialValue="0"></asp:RequiredFieldValidator>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">		        
                <label class="label_general">Catatan</label>
                <asp:TextBox ID="txtNotes" runat="server" CssClass="multiline_textbox" TextMode="MultiLine"></asp:TextBox>
	        </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonSave" runat="server"  Text="Save" CssClass ="small button blue"></asp:Button>&nbsp;
            <asp:Button ID="ButtonCancel" runat="server"  Text="Cancel" CssClass ="small button gray" CausesValidation="False"></asp:Button>
	    </div>       
    </asp:Panel>
    </form>
</body>
</html>
