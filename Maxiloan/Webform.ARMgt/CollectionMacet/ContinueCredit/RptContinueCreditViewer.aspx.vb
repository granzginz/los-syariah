
#Region "Imports"
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.BusinessProcess
Imports Maxiloan.webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
Imports Custom.Controller.ARMgt
#End Region

Public Class RptContinueCreditViewer
    Inherits Maxiloan.webform.WebBased

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents CRVContinueCredit As CrystalDecisions.Web.CrystalReportViewer

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
#Region "Constanta"

    Private m_controller As New CCRequestController
    Private oCustomClass As New Parameter.CCRequest
#End Region
#Region "Property"
    Private Property ApplicationId() As String
        Get
            Return CType(viewstate("ApplicationId"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationId") = Value
        End Set
    End Property
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If sessioninvalid() Then
            Exit Sub
        End If
        BindReportContinueCredit()
    End Sub
    Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies(COOKIESCOLLECTIONCONTINUECREDIT)
        Me.BranchID = cookie.Values("BranchID")
        Me.ApplicationId = cookie.Values("ApplicationId")
        Me.Loginid = cookie.Values("LoginID")
    End Sub
    Sub BindReportContinueCredit()
        GetCookies()
        Dim dtvEntity As New DataView
        Dim dtsEntity As New DataTable
        Dim dsPrintContinueCredit As New DataSet
        Dim PrintingContinueCredit As New ReportDocument
        With oCustomClass
            .strConnection = GetConnectionString
            .ApplicationID = Me.ApplicationId
            .BranchId = Me.BranchID
        End With

        PrintingContinueCredit = New RptContinueCredit

        oCustomClass = m_controller.ContinueCreditReqReport(oCustomClass)
        dsPrintContinueCredit = oCustomClass.ListReport

        Dim numCopies As Int16 = 1
        Dim Collate As Boolean = False
        Dim startpage As Int16 = 1
        PrintingContinueCredit.SetDataSource(dsPrintContinueCredit)

        CRVContinueCredit.ReportSource = PrintingContinueCredit
        CRVContinueCredit.Visible = True
        CRVContinueCredit.DataBind()
        ''=====================================================================================================
        Dim discrete As ParameterDiscreteValue
        Dim ParamFields As ParameterFields
        Dim ParamField As ParameterFieldDefinition
        Dim CurrentValue As ParameterValues


        ParamFields = New ParameterFields
        ParamField = PrintingContinueCredit.DataDefinition.ParameterFields("LoginID")
        discrete = New ParameterDiscreteValue
        discrete.Value = Me.Loginid
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)
        ''=====================================================================================================


        Dim strHTTPServer As String
        Dim StrHTTPApp As String
        Dim strNameServer As String
        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions

        PrintingContinueCredit.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
        PrintingContinueCredit.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat

        Dim strFileLocation As String
        strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
        strFileLocation += Me.Session.SessionID + Me.Loginid + "ContinueCredit.PDF"
        DiskOpts.DiskFileName = strFileLocation
        'oPPK.PrintOptions.PaperSize = PaperSize.PaperA4
        PrintingContinueCredit.ExportOptions.DestinationOptions = DiskOpts
        PrintingContinueCredit.Export()
        Response.Redirect("ContinueCreditRequest.aspx?strFileLocation=" & Me.Session.SessionID & Me.Loginid & "ContinueCredit")

        '    '=====================================
        '    'strHTTPServer = Request.ServerVariables("PATH_INFO")
        '    'strNameServer = Request.ServerVariables("SERVER_NAME")
        '    'StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
        '    'strFileLocation = "http://" & strNameServer & "/" & StrHTTPApp & "/XML/" + Me.Session.SessionID + Me.Loginid + "rpt_RAL.pdf"
        '    ''Call file PDF 

        '    'Response.Write("<script language = javascript>" & vbCrLf _
        '    '            & "history.back(-1) " & vbCrLf _
        '    '            & "window.open('" & strFileLocation & "') " & vbCrLf _
        '    '            & "</script>")
    End Sub
End Class
