﻿#Region "Imports"
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Parameter.CCRequest
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class InqContinueCreditRequest
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oCGID As UcBranchCollection

#Region "properties"


    Public Property RepossesSeqNo() As Integer
        Get
            Return CType(ViewState("RepossesSeqNo"), Integer)
        End Get
        Set(ByVal Value As Integer)
            ViewState("RepossesSeqNo") = Value
        End Set
    End Property

    Public Property AssetSeqNo() As Integer
        Get
            Return CType(ViewState("AssetSeqNo"), Integer)
        End Get
        Set(ByVal Value As Integer)
            ViewState("AssetSeqNo") = Value
        End Set
    End Property

    Public Property UntilDate() As Date
        Get
            Return CType(ViewState("UntilDate"), Date)
        End Get
        Set(ByVal Value As Date)
            ViewState("UntilDate") = Value
        End Set
    End Property

    Public Property RALDate() As Date
        Get
            Return CType(ViewState("RALDate"), Date)
        End Get
        Set(ByVal Value As Date)
            ViewState("RALDate") = Value
        End Set
    End Property

    Public Property RALEndDate() As Date
        Get
            Return CType(ViewState("RALEndDate"), Date)
        End Get
        Set(ByVal Value As Date)
            ViewState("RALEndDate") = Value
        End Set
    End Property

    Public Property AssetTypeID() As String
        Get
            Return CType(ViewState("AssetTypeID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("AssetTypeID") = Value
        End Set
    End Property

    Public Property AllPage() As String
        Get
            Return CType(ViewState("AllPage"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("AllPage") = Value
        End Set
    End Property

    Public Property cgid() As String
        Get
            Return CType(ViewState("CGID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CGID") = Value
        End Set
    End Property

    Public Property AgreementNo() As String
        Get
            Return CType(ViewState("AgreementNo"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("AgreementNo") = Value
        End Set
    End Property

    Public Property ApplicationID() As String
        Get
            Return CType(ViewState("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property


    Public Property RALNo() As String
        Get
            Return CType(ViewState("RALNo"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("RALNo") = Value
        End Set
    End Property

    Public Property CollectorID() As String
        Get
            Return CType(ViewState("CollectorID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CollectorID") = Value
        End Set
    End Property

    Public Property CollectorName() As String
        Get
            Return CType(ViewState("CollectorName"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CollectorName") = Value
        End Set
    End Property

    Public Property CollectorType() As String
        Get
            Return CType(ViewState("CollectorType"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CollectorType") = Value
        End Set
    End Property


#End Region

#Region " Private Const "
    Dim m_CCRequest As New CCRequestController

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region

#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If

    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        Bindgrid(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click

        If txtPage.Text = "" Then
            txtPage.Text = "0"
        Else
            If IsNumeric(txtPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                    currentPage = CType(txtPage.Text, Int32)
                    Bindgrid(Me.SearchBy, Me.SortBy)
                End If
            End If
        End If


    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        Bindgrid(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

    Private Sub getCombo()
        Dim oCCRequest As New Parameter.CCRequest
        With oCCRequest
            .strConnection = GetConnectionString()
        End With


        Dim dt As New DataTable
        Try
            dt = m_CCRequest.GetComboReason(oCCRequest)
        Catch ex As Exception

        End Try

        cboReason.DataSource = dt
        cboReason.DataTextField = "Name"
        cboReason.DataValueField = "ID"
        cboReason.DataBind()
        cboReason.Items.Insert(0, "Select One")
        cboReason.Items(0).Value = ""
        cboReason.SelectedIndex = 0

        cboReason_IndexChanged()
    End Sub

    Sub cboReason_IndexChanged()
        '------------------------------------
        'Combo ApproveBy
        '------------------------------------

        Dim dtApproved As New DataTable
        Dim dvApproved As New DataView


        'If cboReason.SelectedValue.Contains("PELUNASAN") Then
        '    dtApproved = Me.Get_UserApproval("CC2", Me.sesBranchId.Replace("'", ""))
        'Else
        '    dtApproved = Me.Get_UserApproval("CC1", Me.sesBranchId.Replace("'", ""))
        'End If


        dtApproved = Me.Get_UserApproval("CC1", Me.sesBranchId.Replace("'", ""))


        dvApproved = dtApproved.DefaultView
        With cboApproveBy
            .DataValueField = "ID"
            .DataTextField = "Name"
            .DataSource = dvApproved
            .DataBind()
        End With
        cboApproveBy.Items.Insert(0, "Select One")
        cboApproveBy.Items(0).Value = "0"
    End Sub

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        'Put user code to initialize the page here
        If SessionInvalid() Then
            Exit Sub
        End If
        Me.FormID = "InqCollContinueCrReq"
        Me.cgid = Me.GroubDbID
        'If Not Me.IsPostBack Then
        If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
            If Request.QueryString("strFileLocation") <> "" Then
                Dim strHTTPServer As String
                Dim StrHTTPApp As String
                Dim strNameServer As String
                Dim strFileLocation As String

                strHTTPServer = Request.ServerVariables("PATH_INFO")
                strNameServer = Request.ServerVariables("SERVER_NAME")
                StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                strFileLocation = "http://" & strNameServer & "/" & StrHTTPApp & "/XML/" & Request.QueryString("strFileLocation") & ".pdf"
                '& "history.back(-1) " & vbCrLf _
                'Call file PDF 
                Response.Write("<script language = javascript>" & vbCrLf _
                & "window.open('" & strFileLocation & "','Collection', 'left=0, top=0, width=800, height=600, menubar=0, scrollbars=yes') " & vbCrLf _
                & "</script>")
            End If
            InitialDefaultPanel()
            getCombo()
        End If
        'End If
    End Sub

#Region " InitialDefaultPanel "
    Private Sub InitialDefaultPanel()
        PnlSearch.Visible = True
        'PnlList.Visible = False        
        PnlListDetail.Visible = False
    End Sub
#End Region

    Sub Bindgrid(ByVal cmdWhere As String, ByVal cmSort As String)
        Dim dtvEntity As New DataView
        Dim dtsEntity As New DataTable
        Dim oCCRequest As New Parameter.CCRequest

        With oCCRequest
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oCCRequest = m_CCRequest.CCRequestListInq(oCCRequest)

        With oCCRequest
            lblrecord.Text = CType(.TotalRecord, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecord
        End With

        dtsEntity = oCCRequest.listData

        dtvEntity = dtsEntity.DefaultView
        dtvEntity.Sort = Me.SortBy

        dtg.DataSource = dtvEntity

        Try
            dtg.DataBind()

        Catch ex As Exception

        End Try

        PagingFooter()
    End Sub

    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        InitialDefaultPanel()
        cboSearchBy.ClearSelection()
        txtSearchBy.Text = ""
        Me.SearchBy = ""
        Me.SortBy = ""
        Me.Bindgrid(Me.SearchBy, Me.SortBy)
        PnlSearch.Visible = True
        PnlList.Visible = True
    End Sub

    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Buttonsearch.Click
        Me.SearchBy = ""
        Me.SortBy = ""

        If oCGID.BranchID.Trim <> "" Then
            Me.SearchBy = "CGID='" & oCGID.BranchID & "'"
            Me.cgid = oCGID.BranchID
        End If

        If cboSearchBy.SelectedIndex <> 0 And txtSearchBy.Text <> "" Then
            If Me.SearchBy = "" Then
                Me.SearchBy = cboSearchBy.SelectedItem.Value.Trim & " like '%" + txtSearchBy.Text.Trim + "%'"
            Else
                Me.SearchBy = Me.SearchBy & " and " & cboSearchBy.SelectedItem.Value.Trim & " like '%" + txtSearchBy.Text.Trim + "%'"
            End If
        End If

        Bindgrid(Me.SearchBy, Me.SortBy)
        PnlList.Visible = True
        PnlListDetail.Visible = False
    End Sub

    Private Sub viewRequestDetail(ByVal BranchID As String, ByVal ApplicationID As String)
        Dim oCCRequest As New Parameter.CCRequest
        Dim dt As New DataTable
        Dim strApprovalStatus As String

        With oCCRequest
            .strConnection = GetConnectionString()
            .AgreementNo = AgreementNo
            .BusinessDate = Me.BusinessDate
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationID
        End With

        oCCRequest = m_CCRequest.CCRequestDetail(oCCRequest)

        dt = oCCRequest.listData

        hypAgreementNo.Text = CStr(dt.Rows(0).Item("AgreementNo"))
        hypCustomerName.Text = CStr(dt.Rows(0).Item("CustomerName"))


        hypAgreementNo.NavigateUrl = "javascript:OpenViewAgreement('" & CStr(dt.Rows(0).Item("AgreementNo")) & "')"
        hypCustomerName.NavigateUrl = "javascript:OpenViewCustomer('" & CStr(dt.Rows(0).Item("CustomerId")) & "')"


        lblAddress.Text = CStr(dt.Rows(0).Item("MailingAddress"))
        lblDuedate.Text = CStr(dt.Rows(0).Item("Duedate"))
        lblOverDueDays.Text = CStr(dt.Rows(0).Item("EndPastDuedays"))
        lblInstallmentNo.Text = CStr(dt.Rows(0).Item("InstallmentNo"))
        lblInstallmentAmount.Text = CStr(FormatNumber(dt.Rows(0).Item("InstallmentAmount"), 0))
        lblODAmount.Text = CStr(FormatNumber(dt.Rows(0).Item("Amount_Overdue_Gross"), 0))
        lblLateCharges.Text = CStr(FormatNumber(dt.Rows(0).Item("LC"), 0))
        lblOSBallance.Text = CStr(FormatNumber(dt.Rows(0).Item("OS_Gross"), 0))
        lblCollExpense.Text = CStr(FormatNumber(dt.Rows(0).Item("CollectionExpense"), 0))

        lblAsset.Text = CStr(dt.Rows(0).Item("Description"))
        lblLicensePlate.Text = CStr(dt.Rows(0).Item("LicensePlate"))
        lblChassisNo.Text = CStr(dt.Rows(0).Item("ChassisNo"))
        lblEngineNo.Text = CStr(dt.Rows(0).Item("EngineNo"))
        lblYear.Text = CStr(dt.Rows(0).Item("Year")).Trim
        lblColor.Text = CStr(dt.Rows(0).Item("Color")).Trim

        lblRepossesDate.Text = CStr(dt.Rows(0).Item("RepossesDate"))
        lblExpectedDate.Text = CStr(dt.Rows(0).Item("InventoryExpectedDate"))


        Me.AssetTypeID = CStr(dt.Rows(0).Item("AssetTypeID")).Trim
        Me.AssetSeqNo = CInt(dt.Rows(0).Item("AssetSeqNo"))

        Me.ApplicationID = CStr(dt.Rows(0).Item("ApplicationID")).Trim
        Me.BranchID = CStr(dt.Rows(0).Item("BranchID")).Trim
        Me.RepossesSeqNo = CInt(dt.Rows(0).Item("RepossesSeqNo"))
        strApprovalStatus = CStr(dt.Rows(0).Item("ApprovalStatus")).Trim

        PnlSearch.Visible = False
        'PnlList.Visible = False
        PnlListDetail.Visible = True
        ClearAllTextbox()
        If strApprovalStatus = "REQ" Then
            ButtonSave.Visible = False
        End If
    End Sub

    Private Sub ClearAllTextbox()
        txtNotes.Text = ""
        cboApproveBy.SelectedIndex = 0
        cboReason.SelectedIndex = 0
    End Sub
    Private Sub imbCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        PnlListDetail.Visible = False
        Me.SearchBy = ""
        Me.SortBy = ""
        Me.Bindgrid(Me.SearchBy, Me.SortBy)
        PnlSearch.Visible = True
        PnlList.Visible = True
    End Sub

    Private Sub imbSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSave.Click

        'Ambil semua data yang diperlukan dari layar detail

        Dim oCCRequest As New Parameter.CCRequest

        With oCCRequest
            .strConnection = GetConnectionString()
            .CGID = Me.cgid
            .AssetSeqNo = Me.AssetSeqNo
            .RepossesDate = ConvertDate2(lblRepossesDate.Text.Trim)

            .AgreementNo = hypAgreementNo.Text.Trim
            .AssetDescription = lblAsset.Text.Trim
            .EndpastDueDays = CInt(lblOverDueDays.Text.Trim)

            .Reason = cboReason.SelectedItem.Value.Trim
            .ApproveBy = cboApproveBy.SelectedItem.Value.Trim
            .RequestBy = Me.Loginid

            .ApproveVia = ""
            .FaxNo = ""
            .Notes = txtNotes.Text.Trim

            .RepossesSeqNo = CInt(Me.RepossesSeqNo)

            .BussinessDate = Me.BusinessDate
            .ApplicationID = Me.ApplicationID
            .BranchId = Me.BranchID

        End With

        Try
            m_CCRequest.CCRequestSave(oCCRequest)
            ShowMessage(lblMessage, "Simpan Data Berhasil", False)
            SendCookiesToReportViewer()
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message.Trim, True)
        End Try


        Bindgrid(Me.SearchBy, Me.SortBy)
        PnlSearch.Visible = True
        PnlList.Visible = True
        PnlListDetail.Visible = False
    End Sub

    Private Sub dtg_ItemCommand1(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtg.ItemCommand
        Me.AgreementNo = e.Item.Cells(0).Text.Trim
        Me.ApplicationID = e.Item.Cells(1).Text.Trim
        Me.BranchID = e.Item.Cells(2).Text.Trim

        If e.CommandName = "REQUEST" Then
            'If checkFeature(Me.Loginid, Me.FormID, "Req", Me.AppId) Then
            '    If sessioninvalid() Then
            '        Exit Sub
            '    End If
            'End If
            viewRequestDetail(Me.BranchID, Me.ApplicationID)
        End If
    End Sub

    Private Sub dtg_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtg.ItemDataBound
        Dim lblTemp As Label
        Dim hyTemp As HyperLink

        If e.Item.ItemIndex >= 0 Then
            '*** Add customer view
            hyTemp = CType(e.Item.FindControl("hyCustomerName"), HyperLink)
            lblTemp = CType(e.Item.FindControl("lblCustomerId"), Label)
            If (Not lblTemp Is Nothing) And (Not hyTemp Is Nothing) Then
                hyTemp.NavigateUrl = "javascript:OpenViewCustomer('" & lblTemp.Text.Trim & "')"
            End If

            '*** Add agreement view
            hyTemp = CType(e.Item.FindControl("hyAgreementNo"), HyperLink)
            If Not hyTemp Is Nothing Then
                hyTemp.NavigateUrl = "javascript:OpenViewAgreement('" & hyTemp.Text.Trim & "')"
            End If
        End If
    End Sub
    Sub SendCookiesToReportViewer()
        Dim cookie As HttpCookie = Request.Cookies(COOKIESCOLLECTIONCONTINUECREDIT)
        If Not cookie Is Nothing Then
            cookie.Values("BranchId") = Me.BranchID
            cookie.Values("ApplicationID") = Me.ApplicationID
            cookie.Values("LoginID") = Me.Loginid
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie(COOKIESCOLLECTIONCONTINUECREDIT)
            cookieNew.Values.Add("BranchId", Me.BranchID)
            cookieNew.Values.Add("ApplicationID", Me.ApplicationID)
            cookieNew.Values.Add("LoginID", Me.Loginid)
            Response.AppendCookie(cookieNew)
        End If
        'Response.Redirect("RptContinueCreditViewer.aspx")
    End Sub

End Class