﻿#Region "Imports"
Imports System.Text
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonVariableHelper
Imports Maxiloan.General.CommonCookiesHelper
Imports System.Web.Services

#End Region

Public Class InventoryAppraisal
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents GridNavigator As ucGridNav
    Protected WithEvents txtEstimationPrice, txtbidderamount, txtMRP, txtHargaInternetKoran, txtHargaPenawarTerbaik, txtNilaiDepresiasiWajar,
        txtRepossessFee, txtKoordinasiFee, txtMobilisasiFee As ucNumberFormat
    ' txtBiayaPerbaikan, txtBiayaPengurusanSTNKKIR, txtRefundPremi



#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.InventoryAppraisal
    Private oController As New InventoryAppraisalController
    Private CounterColumn As Integer = 0

    Private currentPageChange As Int32 = 1
    Private pageSizeChange As Int16 = 10
    Private currentPageNumberChange As Int16 = 1
    Private totalPagesChange As Double = 1
    Private recordCountChange As Int64 = 1


#End Region

#Region "Property"
    Private Property ApplicationID() As String
        Get
            Return CStr(ViewState("ApplicationID"))
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
    Private Property oData() As DataTable
        Get
            Return CType(ViewState("oData"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            ViewState("oData") = Value
        End Set
    End Property
    Private Property TableBidder() As DataTable
        Get
            Return (CType(ViewState("TableBidder"), DataTable))
        End Get
        Set(ByVal Value As DataTable)
            ViewState("TableBidder") = Value
        End Set
    End Property

#End Region
    Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)

        Page.ClientScript.RegisterForEventValidation(ddlPenawarTerbaik.UniqueID)
        Page.ClientScript.RegisterForEventValidation(ddlRekeningBank.UniqueID)
        MyBase.Render(writer)
    End Sub
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If
        AddHandler GridNavigator.PageChanged, AddressOf PageNavigation
        If Not IsPostBack Then
            Me.FormID = "CollInvAppraisal"
            If CheckForm(Me.Loginid, Me.FormID, "Maxiloan") Then
                ModVar.StrConn = GetConnectionString()
                ModVar.BranchId = Me.sesBranchId.Replace("'", "")
                InitialDefaultPanel()
                FillCbo()
                txtEstimationDate.Text = CType(Me.BusinessDate.ToString("dd/MM/yyyy"), String)
                txtBidderDate.Text = CType(Me.BusinessDate.ToString("dd/MM/yyyy"), String)
                PenawarTerbaikItemsRefresh()

                Dim c_Class As New Controller.AssetRepoController
                With ddlGrade
                    .DataSource = c_Class.AssetGradeValue(GetConnectionString)
                    .DataValueField = "Value"
                    .DataTextField = "Text"
                    .DataBind()
                    .Items.Insert(0, "Select One")
                    .Items(0).Value = "0"
                End With
            End If
        End If
    End Sub
    Protected Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        currentPage = e.CurrentPage
        DoBind(Me.SearchBy, Me.SortBy, True)
        GridNavigator.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
    End Sub
    Private Sub InitialDefaultPanel()
        pnlDtGrid.Visible = False
        pnlUsulanPenjualan.Visible = False
        pnlsearch.Visible = True
        pnlBidder.Visible = False
        pnlAppraisalEntry.Visible = False
        pnlBidderGrid.Visible = False
        ButtonExit.Visible = True
        With txtEstimationPrice
            .RequiredFieldValidatorEnable = True
            .RangeValidatorEnable = False
        End With
    End Sub
    Private Sub FillCbo()

        Dim oDataTable As New DataTable
        If Me.IsHoBranch = False Then
            With oCustomClass
                .strConnection = GetConnectionString()
                .strKey = "CG"
                .CGID = Me.GroubDbID
                .CollectorType = ""
            End With
        Else
            With oCustomClass
                .strConnection = GetConnectionString()
                .strKey = "CG_ALL"
                .CGID = Me.GroubDbID
                .CollectorType = ""
            End With
        End If

        oCustomClass = oController.InventoryAppraisalListCG(oCustomClass)
        oDataTable = oCustomClass.ListRAL
        With cboCG
            .DataSource = oDataTable
            .DataTextField = "CGName"
            .DataValueField = "CGID"
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = ""
            If oDataTable.Rows.Count = 1 Then
                .Items.FindByValue(Me.GroubDbID).Selected = True
            End If
        End With
    End Sub
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#Region "Navigation"
   
#End Region
    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String, Optional isFrNav As Boolean = False)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView 
        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oCustomClass = oController.InventoryAppraisalList(oCustomClass)

        DtUserList = oCustomClass.ListRAL
        DvUserList = DtUserList.DefaultView
        recordCount = oCustomClass.TotalRecord
        DvUserList.Sort = Me.SortBy
        dtgInventoryAppraisal.DataSource = DvUserList
        With oCustomClass
            .strConnection = GetConnectionString()
        End With
        oCustomClass = oController.DataInventoryAppraisal(oCustomClass)
        Me.oData = oCustomClass.ListRAL
        Try
            dtgInventoryAppraisal.DataBind()
        Catch
            dtgInventoryAppraisal.CurrentPageIndex = 0
            dtgInventoryAppraisal.DataBind()
        End Try

        If (isFrNav = False) Then
            GridNavigator.Initialize(recordCount, pageSize)

        End If
        pnlDtGrid.Visible = True
        pnlsearch.Visible = True
    End Sub
    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Buttonsearch.Click
        Me.SearchBy = "CollectionAgreement.CGID='" & cboCG.SelectedItem.Value.Trim & "'"
        If cboSearchBy.SelectedItem.Value.Trim <> "Select One" Then
            Me.SearchBy = Me.SearchBy & " and " & cboSearchBy.SelectedItem.Value.Trim & " LIKE '%" & txtSearchBy.Text.Trim & "%'"
        End If

        Me.SortBy = ""
        DoBind(Me.SearchBy, Me.SortBy)

        Dim dtApproved As New DataTable
        Dim dvApproved As New DataView
        dtApproved = Me.Get_UserApproval("SELL", Me.GroubDbID.Replace("'", ""))
        dvApproved = dtApproved.DefaultView
        With cboApprove
            .DataValueField = "ID"
            .DataTextField = "Name"
            .DataSource = dvApproved
            .DataBind()
        End With
        cboApprove.Items.Insert(0, "Select One")
        cboApprove.Items(0).Value = "0"
    End Sub
    Private Sub dtgInventoryAppraisal_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgInventoryAppraisal.ItemCommand
        Select Case e.CommandName
            Case "App"
                Dim lblbranchid As Label
                lblbranchid = CType(dtgInventoryAppraisal.Items(e.Item.ItemIndex).FindControl("lblBranchId"), Label)
                Dim lblApplicationId As Label

                Dim oDataTable As New DataTable
                Dim lblassetseqno As Label
                Dim lblRepossesseqno As Label

                Dim lblAssetCode As Label
                Dim lblManufacturingYear As Label

                lblAssetCode = CType(e.Item.FindControl("lblAssetCode"), Label)
                lblManufacturingYear = CType(e.Item.FindControl("lblManufacturingYear"), Label)
                lblassetseqno = CType(e.Item.FindControl("lblAssetSeqNo"), Label)
                lblRepossesseqno = CType(e.Item.FindControl("lblRepossesseqno"), Label)
                Dim custId = CType(e.Item.FindControl("lblCustomerId"), Label).Text
                lblApplicationId = CType(dtgInventoryAppraisal.Items(e.Item.ItemIndex).FindControl("lblApplicationId"), Label)
                If CheckFeature(Me.Loginid, Me.FormID, "App", "Maxiloan") Then
                    With oCustomClass
                        .strConnection = GetConnectionString()
                        .ApplicationID = lblApplicationId.Text.Trim
                        .BusinessDate = Me.BusinessDate
                        .BranchId = lblbranchid.Text.Trim
                        .AssetSeqno = CType(lblassetseqno.Text.Trim, Integer)
                        .RepossessseqNo = CType(lblRepossesseqno.Text.Trim, Integer)
                    End With

                    lblassetseqno1.Text = CType(lblassetseqno.Text.Trim, String)
                    lblrepossesseqno1.Text = CType(lblRepossesseqno.Text.Trim, String)
                    oCustomClass = oController.RequestDataSelectInventory(oCustomClass)
                    oDataTable = oCustomClass.ListRAL
                    Me.BranchID = lblbranchid.Text.Trim
                    Me.ApplicationID = lblApplicationId.Text



                    If Not IsDBNull(oDataTable.Rows(0)("AgreementNo")) Then
                        hypAgreementNoSlct.Text = CType(oDataTable.Rows(0)("AgreementNo"), String)
                        hypAgreementNoSlct.NavigateUrl = "javascript:OpenAgreementNo('" & "Collection" & "', '" & lblApplicationId.Text.Trim & "')"
                    End If
                    If Not IsDBNull(oDataTable.Rows(0)("CustomerName")) Then
                        hypCustomerNameSlct.Text = CType(oDataTable.Rows(0)("CustomerName"), String)
                        hypCustomerNameSlct.NavigateUrl = "javascript:OpenCustomer('" & "Collection" & "', '" & Server.UrlEncode(custId) & "')"
                    End If
                    If Not IsDBNull(oDataTable.Rows(0)("Description")) Then
                        lblAsset.Text = CType(oDataTable.Rows(0)("Description"), String)
                    End If

                    If Not IsDBNull(oDataTable.Rows(0)("RepossesDate")) Then
                        lblRepossessDate.Text = Format(oDataTable.Rows(0)("RepossesDate"), "dd/MM/yyyy")
                    End If
                    If Not IsDBNull(oDataTable.Rows(0)("CollectorID")) Then
                        lblrepossessby.Text = CType(oDataTable.Rows(0)("CollectorID"), String)
                    End If

                    If Not IsDBNull(oDataTable.Rows(0)("InventoryDate")) Then
                        lblInventoryDate.Text = Format(oDataTable.Rows(0)("InventoryDate"), "dd/MM/yyyy")
                    End If
                    txtPerkiraanRL.Text = "0"
                    If Not IsDBNull(oDataTable.Rows(0)("OutstandingPrincipal")) Then
                        LblInventoryAmount.Text = FormatNumber(oDataTable.Rows(0)("OutstandingPrincipal"), 0)
                        txtPerkiraanRL.Text = "( " + FormatNumber(oDataTable.Rows(0)("OutstandingPrincipal"), 0) + " )"
                    End If


                    txtMarginDealer.Text = "0"
                    txtRepossessFee.Text = "0"
                    txtKoordinasiFee.Text = "0"
                    txtMobilisasiFee.text = "0"
                    txtTotalMinSalesPrice.Text = "0"
                    txtNilaiPenyesuaian.Text = "0"


                    txtbidderamount.Text = "0"
                    pnlsearch.Visible = False
                    pnlDtGrid.Visible = False
                    pnlBidder.Visible = True
                    pnlUsulanPenjualan.Visible = True
                    pnlAppraisalEntry.Visible = True

                    txtEstimationPrice.Text = "0"
                    txtEstimatedBy.Text = ""
                    txtNotes.Text = ""
                    txtbiddername.Text = ""

                  

                    Dim p_Class As New Parameter.InventoryAppraisal
                    Dim c_Class As New Controller.InventoryAppraisalController

                    With p_Class
                        .strConnection = GetConnectionString()
                        .AssetCode = lblAssetCode.Text
                        .BranchId = Me.BranchID.Replace("'", "")
                        .ManufacturingYear = CInt(lblManufacturingYear.Text)
                    End With
                    'c_Class.GetMRP(p_Class)
                    'txtMRP.Text = p_Class.MRP


                End If
        End Select
    End Sub
    Private Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        
        Response.Redirect("InventoryAppraisal.aspx")

    End Sub
    Public Function isVisiblePrint(ByVal strKey As String) As Boolean
        If strKey.Trim = "" Then
            Return False
        Else
            Return True
        End If
    End Function
#Region "SendCookies"
    Sub SendCookies(ByVal AggrementChecked As String)
        'Context.Trace.Write("Send Cookies !")

        Dim cookie As HttpCookie = Request.Cookies(General.CommonCookiesHelper.COOKIES_INVENTORY_APPRAISAL)
        Dim strWhere As New StringBuilder
        strWhere.Append(Me.SearchBy)
        If Not cookie Is Nothing Then
            cookie.Values("SearchBy") = strWhere.ToString
            cookie.Values("WhereAggNo") = AggrementChecked.Trim
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie(General.CommonCookiesHelper.COOKIES_INVENTORY_APPRAISAL)
            cookieNew.Values("SearchBy") = strWhere.ToString
            cookieNew.Values("WhereAggNo") = AggrementChecked.Trim
            Response.AppendCookie(cookieNew)
        End If
        'Context.Trace.Write("strWhere = " & strWhere.ToString)

    End Sub

#End Region
    Private Sub imbReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        InitialDefaultPanel()
        cboSearchBy.ClearSelection()
        cboCG.ClearSelection()
        txtSearchBy.Text = ""

    End Sub
    Private Sub dtgInventoryAppraisal_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgInventoryAppraisal.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim hyTemp As HyperLink
            Dim lblApplicationId As Label
            Dim approvalstatus As Label

            lblApplicationId = CType(e.Item.FindControl("lblApplicationId"), Label)
            '*** Agreement No link
            hyTemp = CType(e.Item.FindControl("hypAgreementNodtg"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenAgreementNo('" & "Collection" & "', '" & Server.UrlEncode(lblApplicationId.Text.Trim) & "')"

            Dim app As ImageButton
            app = CType(e.Item.FindControl("imgselect"), ImageButton)

            approvalstatus = CType(e.Item.FindControl("ApprovalStatus"), Label)
            If Not (approvalstatus.Text.Trim = "-" Or approvalstatus.Text.Trim = "REJ") Then
                app.Enabled = False
                app.Visible = False

            End If
        End If
    End Sub
#Region "FillViewStateHalaman"

    Private Sub FillViewStateHalaman()
        'Isi ViewState per halaman grid
        Context.Trace.Write("FillViewStateHalaman")
        Dim irecord As Int16
        Dim cb As CheckBox
        Dim hyAgreementGrid As HyperLink
        Dim strAgreement As String
        Dim koma As String
        Dim recordDataGrid As Int16


        recordDataGrid = CType(dtgInventoryAppraisal.Items.Count, Int16)
        ' recordDataGrid = 10
        strAgreement = ""
        ViewState("hal" & CStr(currentPage).Trim) = ""
        Context.Trace.Write("recordDataGrid :" + CType(recordDataGrid, String))
        Try
            For irecord = 0 To CType(recordDataGrid - 1, Int16)
                Context.Trace.Write("Var Irecord :" + CType(irecord, String))
                Context.Trace.Write("Var PageSize :" + CType(pageSize, String))
                cb = CType(dtgInventoryAppraisal.Items(irecord).FindControl("chkPrint"), CheckBox)
                ' hyAgreementGrid = CType(dtgPaging.Items(irecord).FindControl("hyAgreementGrid"), Label)
                hyAgreementGrid = CType(dtgInventoryAppraisal.Items(irecord).Cells(0).FindControl("hypAgreementNodtg"), HyperLink)

                If cb.Checked = True Then
                    If strAgreement = "" Then koma = "" Else koma = ","
                    strAgreement = strAgreement & koma & hyAgreementGrid.Text.Trim
                    ViewState("hal" & CStr(currentPage).Trim) = strAgreement.Trim
                End If
            Next

            Context.Trace.Write("strAgreement = " & strAgreement)

        Catch ex As Exception
            Response.Write(ex.Message)
            Dim oErr As New MaxiloanExceptions
            oErr.WriteLog("InventoryAppraisal.aspx", "Sub FillViewStateHalaman", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
        End Try

    End Sub


#End Region
#Region "JoinAllViewstateHalaman"

    Private Function JoinAllViewstateHalaman() As String
        Dim totalpages As Integer = GridNavigator.TotalPage ' CType(lblTotPage.Text, Integer)
        Dim strAgreement As String
        Dim koma As String
        Dim ihal As Integer
        Dim hal As String
        Context.Trace.Write("JoinAllViewstateHalaman")

        strAgreement = ""
        For ihal = 1 To totalpages
            hal = CStr("hal" & CStr(ihal))
            If strAgreement = "" Then koma = "" Else koma = ","
            strAgreement = strAgreement & koma & CType(ViewState(hal), String)
            ViewState("AllPage") = strAgreement
        Next
        Context.Trace.Write("strAgreement.Trim = " & strAgreement.Trim)
        Return strAgreement.Trim

    End Function

#End Region
#Region "Check apakah agrement ini pernah di centang atau tidak"

    Public Sub CheckIfEverChecked()
        Context.Trace.Write("CheckIfEverChecked()")
        Dim agreementchecked(pageSize) As String
        Dim chkAgreement As CheckBox
        Dim loopingchecked As Int16
        Dim hyAgreementGrid As HyperLink
        Dim recordDataGrid As Int16
        recordDataGrid = CType(dtgInventoryAppraisal.Items.Count, Int16)


        If CType(ViewState("hal" & CStr(currentPage).Trim), String) <> "" Then
            agreementchecked = Split(CType(ViewState("hal" & CStr(currentPage).Trim), String), ",")
            For loopingchecked = 0 To CType(recordDataGrid - 1, Int16)
                Context.Trace.Write("Var loopingchecked :" + CType(loopingchecked, String))
                chkAgreement = CType(dtgInventoryAppraisal.Items(loopingchecked).FindControl("chkPrint"), CheckBox)
                hyAgreementGrid = CType(dtgInventoryAppraisal.Items(loopingchecked).FindControl("hypAgreementNodtg"), HyperLink)

                For periksa As Int16 = 0 To CType(UBound(agreementchecked), Int16)
                    If chkAgreement.Checked = False Then
                        If hyAgreementGrid.Text.Trim = agreementchecked(periksa).Trim Then
                            chkAgreement.Checked = True
                        End If
                    End If

                Next
            Next


        End If

    End Sub

#End Region
#Region "Clear ViewState"

    Private Sub ClearViewState()
        Dim totalpages As Integer = dtgInventoryAppraisal.PageCount
        Dim strAgreement As String
        Dim koma As String
        Dim i As Integer
        Dim hal As String

        strAgreement = ""
        For i = 0 To totalpages
            hal = CStr("hal" & CStr(i))
            ViewState(hal) = ""
        Next
        ViewState("AllPage") = ""
    End Sub

#End Region
#Region "Ketika Tombol Select Dipijit!"


    Private Sub ImbSelect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonPrint.Click
        FillViewStateHalaman()
        Dim AggrementChecked As String = JoinAllViewstateHalaman().Trim

        If AggrementChecked = "" Then
            Exit Sub
        End If
        'Context.Trace.Write("AggrementChecked sebelum replace= " & AggrementChecked)
        AggrementChecked = Replace(AggrementChecked, ",", "','").Trim
        AggrementChecked = " and CollectionAgreement.AgreementNo in ('" & AggrementChecked & "')".Trim
        ''Context.Trace.Write("AggrementChecked sesudah replace = " & AggrementChecked)

        'Response.Write("Aggrementchecked = " & AggrementChecked)
        'Response.Redirect("InsuranceBillingSelect.aspx?InsCo=" & Me.InsCoySelect & "&InsCoName=" & Me.InsCoySelectName & "&TermOfPayment=" & CType(Me.TermOfPayment, String) & "&WhereAggNo=" & AggrementChecked & "")
        'Me.SearchBy = "ReceiptNotes.CGID='" & cboCG.SelectedItem.Value.Trim & "'"
        'Me.SearchBy = Me.SearchBy & " and ReceiptNotes.InstallmentNo='" & CType(cboSelectInstallmentNo.SelectedItem.Value, Integer) & "' and ReceiptNotes.ApplicationId='" & Me.ApplicationID.Trim & "'"

        SendCookies(AggrementChecked)
        Response.Redirect("inventoryappraisalviewer.aspx")
    End Sub

#End Region

    Private Sub imbadd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Buttonadd.Click
        If (txtbiddername.Text = "") Then
            ShowMessage(lblMessage, "Harap isi nama penawar", True)
            Exit Sub
        End If
        txtbiddername.Text = txtbiddername.Text.ToUpper()
        If (txtbidderamount.Text = "") Or (txtbidderamount.Text = "0") Then
            ShowMessage(lblMessage, "Harap isi harga penawaran", True)
            Exit Sub
        End If
        If (txtBidderDate.Text = "") Then
            ShowMessage(lblMessage, "Harap isi Tanggal Penawaran", True)
            Exit Sub
        End If
        If Me.TableBidder Is Nothing Then
            Me.TableBidder = GenerateTableBidder()
        End If
        pnlBidderGrid.Visible = True

        If ConvertDate2(txtBidderDate.Text) > Me.BusinessDate Then
            ShowMessage(lblMessage, "Tanggal Penawaran harus < tanggal hari ini", True)
            Exit Sub
        End If

        If Not CheckDuplikasiBidderName(txtbiddername.Text.Trim) Then
            ShowMessage(lblMessage, "Harap isi Nama Penawar yg lain", True)
            Exit Sub
        End If

        SaveDataGridToDataTable()
        dtgbidder.DataSource = Me.TableBidder.DefaultView
        dtgbidder.DataBind()
        txtbiddername.Text = ""
        txtbidderamount.Text = ""
        txtNoTelepon.Text = ""
        ButtonExit.Visible = False
          
    End Sub
#Region "Generate Table"
    Private Function GenerateTableBidder() As DataTable
        Dim lObjDataTable As New DataTable("TableBidder")
        With lObjDataTable
            .Columns.Add("BidderName", System.Type.GetType("System.String"))
            .Columns.Add("BidderAmount", System.Type.GetType("System.String"))
            .Columns.Add("BidderDate", System.Type.GetType("System.DateTime"))
            .Columns.Add("FormBidder", System.Type.GetType("System.Boolean"))
            .Columns.Add("NoTelepon", System.Type.GetType("System.String"))
        End With
        Return lObjDataTable
    End Function
#End Region
    Private Sub SaveDataGridToDataTable()
        Dim lObjDataRow As DataRow

        lObjDataRow = Me.TableBidder.NewRow()
        lObjDataRow("BidderName") = txtbiddername.Text.Trim
        lObjDataRow("BidderAmount") = txtbidderamount.Text
        lObjDataRow("BidderDate") = ConvertDate2(txtBidderDate.Text)
        lObjDataRow("FormBidder") = CType(ddlFormBidder.SelectedValue, Boolean)
        lObjDataRow("NoTelepon") = txtNoTelepon.Text
        Me.TableBidder.Rows.Add(lObjDataRow)
        PenawarTerbaikItemsRefresh()

    End Sub
    Private Class DTOPenawarTerbaik
        Public Property BidderName As String
    End Class
    Private Sub PenawarTerbaikItemsRefresh()
        Dim ArrList As New List(Of DTOPenawarTerbaik)
        ArrList.Add(New DTOPenawarTerbaik() With {.BidderName = "Select One"})
        If Me.TableBidder Is Nothing Then
            Me.TableBidder = GenerateTableBidder()
        End If
        For Each dt As DataRowView In TableBidder.DefaultView
            ArrList.Add(New DTOPenawarTerbaik() With {.BidderName = dt("BidderName").ToString()})
        Next
        ddlPenawarTerbaik.Items.Clear()
        ddlPenawarTerbaik.DataTextField = "BidderName"
        ddlPenawarTerbaik.DataValueField = "BidderName"
        ddlPenawarTerbaik.DataSource = ArrList
        ddlPenawarTerbaik.DataBind()
    End Sub
    Private Sub dtgbidder_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgbidder.ItemCommand
        If e.CommandName = "DELETE" Then
            Me.TableBidder.Rows.RemoveAt(CInt(e.Item.ItemIndex()))
            dtgbidder.DataSource = Me.TableBidder.DefaultView
            dtgbidder.DataBind()
            PenawarTerbaikItemsRefresh()
        End If
    End Sub
    Private Sub dtgbidder_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgbidder.ItemDataBound

        If SessionInvalid() Then
            Exit Sub
        End If
        Dim lblbidderno As Label
        If e.Item.ItemIndex >= 0 Then
            lblbidderno = CType(e.Item.FindControl("lblbidderno"), Label)
            CounterColumn += 1
            lblbidderno.Text = CStr(CounterColumn)
        End If
    End Sub
    Private Function CheckDuplikasiBidderName(ByVal strBidderName As String) As Boolean
        Dim i As Integer
        Dim isValid As Boolean = True
        Dim lblbiddername As Label
        For i = 0 To dtgbidder.Items.Count - 1
            lblbiddername = CType(dtgbidder.Items(i).FindControl("lblbiddername"), Label)
            If lblbiddername.Text.Trim = strBidderName.Trim Then
                isValid = False
            End If
        Next
        Return isValid
    End Function
    'Private Function CompareBidderwithEstimationPrice(ByVal intEstimation As Integer) As Boolean
    Private Function CompareBidderwithEstimationPrice(ByVal intEstimation As Double) As Boolean
        Dim j As Integer
        oCustomClass.flag = 0
        Dim isValid As Boolean = False
        Dim lblbidderamount As Label
        For j = 0 To dtgbidder.Items.Count - 1
            lblbidderamount = CType(dtgbidder.Items(j).FindControl("lblbidderamount"), Label)

            If CDbl(lblbidderamount.Text.Trim) >= intEstimation Then
                isValid = True
                oCustomClass.flag = 1
            End If
        Next
        Return isValid
    End Function
    Private Sub ImbSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonSave.Click

        If (dtgbidder.Items.Count < 3) Then
            ShowMessage(lblMessage, "Minimun 3 penawaran", True)
            Exit Sub
        End If

        If (txtEstimationDate.Text = "") Then
            ShowMessage(lblMessage, "Harap isi Tanggal Estimasi", True)
            Exit Sub
        End If

        If (txtEstimationPrice.Text = "") Then
            ShowMessage(lblMessage, "Harap isi Estimasi Harga", True)
            Exit Sub
        End If
        If ConvertDate2(txtEstimationDate.Text) > Me.BusinessDate Then
            ShowMessage(lblMessage, "Tanggal estimasi harus < tanggal hari ini", True)
            Exit Sub
        End If

        'If (Not CompareBidderwithEstimationPrice(CType(txtEstimationPrice.Text, Integer))) And cboApprove.SelectedItem.Value = "0" Then
        If (Not CompareBidderwithEstimationPrice(CDbl(txtEstimationPrice.Text))) And cboApprove.SelectedItem.Value = "0" Then
            ShowMessage(lblMessage, "Harap isi Disetujui Oleh Siapa, Karena Harga Penawaran < Harga Estimasi", True)
            Exit Sub
        End If


        Dim out As String
        out = cboApprove.SelectedItem.Value.Trim()
        ComputeHigherbidder()
        With oCustomClass
            .ListBidder = Me.TableBidder
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationID
            .CGID = Me.GroubDbID
            .approveby = cboApprove.SelectedItem.Value.Trim
            .EstimationPrice = CDbl(txtEstimationPrice.Text)
            .EstimationDate = ConvertDate2(txtEstimationDate.Text)
            .EstimationBy = txtEstimatedBy.Text.Trim
            .strConnection = GetConnectionString()
            .EstimationNotes = txtNotes.Text.Trim
            .AssetSeqno = CType(lblassetseqno1.Text.Trim, Integer)
            .RepossessseqNo = CType(lblrepossesseqno1.Text.Trim, Integer)
            .BusinessDate = Me.BusinessDate
            .LoginId = Me.Loginid

            .MRP = CDbl(Page.Request.Form("txtEstimationPrice$txtNumber").ToString())
            .Grade = ddlGrade.SelectedItem.Text
            .GradeValue = ddlGrade.SelectedItem.Value 
            '.NilaiPenyesuaian = CDbl(txtNilaiPenyesuaian.Text)


            .BiayaRepossess = CDbl(txtRepossessFee.Text)
            .BiayaKoordinasi = CDbl(txtKoordinasiFee.Text)
            .BiayaMobilisasi = CDbl(txtMobilisasiFee.Text)

            '.MRP = CDbl(txtMRP.Text)
            '.HargaInternetKoran = CDbl(txtHargaInternetKoran.Text)
            '.NilaiDepresiasiWajar = CDbl(txtNilaiDepresiasiWajar.Text)

            .PenawarTerbaik = ddlPenawarTerbaik.SelectedValue
            .PenawarHarga = CDbl(txtHargaPenawarTerbaik.Text)
            .CaraPembayaran = ddlCaraPembayaran.SelectedValue
            .AccountID = ddlRekeningBank.SelectedValue

            '.BiayaPerbaikan = CDbl(txtBiayaPerbaikan.Text)
            '.BiayaPengurusanSTNKKIR = CDbl(txtBiayaPengurusanSTNKKIR.Text)
            '.RefundPremi = CDbl(txtRefundPremi.Text)


        End With

        oCustomClass = oController.SaveInventoryAppraisal(oCustomClass)

        pnlsearch.Visible = True
        pnlDtGrid.Visible = False
        pnlBidder.Visible = False
        pnlAppraisalEntry.Visible = False
        pnlBidderGrid.Visible = False





    End Sub
    Private Sub ComputeHigherbidder()
        Dim j As Integer

        Dim highbidder As Double
        Dim lblbidderamount As Label
        For j = 0 To dtgbidder.Items.Count - 1
            lblbidderamount = CType(dtgbidder.Items(j).FindControl("lblbidderamount"), Label)

            If CDbl(lblbidderamount.Text.Trim) >= highbidder Then
                highbidder = CDbl(lblbidderamount.Text.Trim)
            End If
        Next
        oCustomClass.bidderamount = highbidder
    End Sub


    Private Sub ButtonExit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonExit.Click
        Response.Redirect("InventoryAppraisal.aspx")
    End Sub


#Region "CheckStatus"

    Public Sub ChkStatus_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim loopitem As Int16
        Dim chkPrint As CheckBox

        For loopitem = 0 To CType(dtgInventoryAppraisal.Items.Count - 1, Int16)
            Context.Trace.Write("loopitem = " & loopitem)
            chkPrint = New CheckBox
            chkPrint = CType(dtgInventoryAppraisal.Items(loopitem).FindControl("chkPrint"), CheckBox)

            If chkPrint.Enabled Then
                If CType(sender, CheckBox).Checked = True Then
                    Context.Trace.Write("CType(sender, CheckBox).Checked  = True ")
                    chkPrint.Checked = True
                Else
                    Context.Trace.Write("CType(sender, CheckBox).Checked  = False ")
                    chkPrint.Checked = False
                End If
            Else
                Context.Trace.Write("chkPrint.Checked = False")
                chkPrint.Checked = False
            End If
        Next

    End Sub
#End Region
    <Serializable()> _
    Public Class DTORekeningBank
        Public Property AccountNo As String
        Public Property Bank As String
    End Class
    <WebMethod()> _
    Public Shared Function GetRekeningBank(id As String) As List(Of DTORekeningBank)
        Dim c_Class As New DataUserControlController
        'Dim p_Class As New Parameter.PPK
        Dim ret As New List(Of DTORekeningBank)
        'With p_Class
        '    .strConnection = ModVar.StrConn
        '    .BranchId = "000" 'ModVar.BranchId   ' hanya menampilkan rekening pusat
        '    .BankAccountType = id
        '    .BankPurpose = "EC"
        'End With
        Dim dtBankAccountCache As New DataTable
        dtBankAccountCache = c_Class.GetBankAccount(ModVar.StrConn, ModVar.BranchId, id, "HB")

        For Each dt As DataRowView In dtBankAccountCache.DefaultView
            ret.Add(New DTORekeningBank() With {
                    .AccountNo = dt("ID").ToString(),
                    .Bank = dt("Name").ToString()
                })
        Next
        Return ret
    End Function

    Private Sub ddlCaraPembayaran_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlCaraPembayaran.SelectedIndexChanged
        Dim c_Class As New DataUserControlController
        'Dim p_Class As New Parameter.PPK
        'Dim ret As New List(Of DTORekeningBank)
        'With p_Class
        '    .strConnection = ModVar.StrConn
        '    .BranchId = "000" 'ModVar.BranchId   ' hanya menampilkan rekening pusat
        '    .BankAccountType = id
        '    .BankPurpose = "EC"
        'End With
        Dim dtBankAccountCache As New DataTable
        dtBankAccountCache = c_Class.GetBankAccount(ModVar.StrConn, ModVar.BranchId, ddlCaraPembayaran.SelectedValue, "HB")
        ddlRekeningBank.DataSource = dtBankAccountCache
        ddlRekeningBank.DataTextField = "Name"
        ddlRekeningBank.DataValueField = "ID"
        ddlRekeningBank.DataBind()

        'For Each dt As DataRowView In dtBankAccountCache.DefaultView
        '    ret.Add(New DTORekeningBank() With {
        '            .AccountNo = dt("ID").ToString(),
        '            .Bank = dt("Name").ToString()
        '        })
        'Next
        'ddlRekeningBank.DataSource = ret
        'ddlRekeningBank.DataBind()
    End Sub
End Class