﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PrintBPK.aspx.vb" Inherits="Maxiloan.Webform.ARMgt.PrintBPK" %>

<%@ Register TagPrefix="uc1" TagName="UcBranchCollection" Src="../../Webform.UserController/UcBranchCollection.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PrintBPK</title>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;

        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';

        function OpenWinHistory(pCGID, pExID, pExName) {
            window.open('../ExecutorHistory.aspx?CGID=' + pCGID + '&ExecutorID=' + pExID + '&ExecutorName=' + pExName + '', 'UserLookup', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenWinCollector(pCollID, pCGID) {
            window.open('../../Setting/CollectorView.aspx?CGID=' + pCGID + '&CollectorID=' + pCollID + '', 'UserLookup', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenWinCustomer(pID) {
            window.open('../../../Webform.LoanOrg/Credit/ViewPersonalCustomer.aspx?CustomerID=' + pID + '&style=Collection', 'UserLookup', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }
        function OpenWinPrintBPKReport(app, col, branch, RepossesSeqNo) {
            window.open('PrinBPKViewer.aspx?ApplicationID=' + app + '&ColectorID=' + col + '&branch=' + branch + '&RepossesSeqNo=' + RepossesSeqNo, 'UserLookup', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }		
		
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                CETAK BPK (BUKTI PENGELUARAN ASSET YANG DIBIAYAI)
            </h3>
        </div>
    </div>
    <asp:Panel ID="pnlsearch" runat="server">
        <div class="form_box">
            <div class="form_single">
                <label>
                    Collection Group
                </label>
                &nbsp;<uc1:ucbranchcollection id="oCGID" runat="server"></uc1:ucbranchcollection>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cari Berdasarkan</label>
                <asp:DropDownList ID="cboSearchBy" runat="server">
                    <asp:ListItem Value="Select One">Select One</asp:ListItem>
                    <asp:ListItem Value="AgreementNo">No Kontrak</asp:ListItem>
                    <asp:ListItem Value="Customer.Name">Nama Konsumen</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearchBy" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="Buttonsearch" runat="server" Text="Search" CssClass="small button blue">
            </asp:Button>&nbsp;
            <asp:Button ID="ButtonReset" runat="server" Text="Reset" CssClass="small button gray"
                CausesValidation="False"></asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlDtGrid" runat="server">
        <div class="form_box_title">
            <div class="form_single">
                <h4>
                    CETAK BPK
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgCollPrinBPKList" runat="server" Width="100%" AllowSorting="True"
                        AutoGenerateColumns="False" DataKeyField="AgreementNo" CssClass="grid_general"
                        BorderStyle="None" BorderWidth="0" OnSortCommand="Sorting">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="CETAK BPK">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbPrintBPK" runat="server" CommandName="printBPK" ImageUrl="../../Images/IconRelease.gif">
                                    </asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">
                                <ItemTemplate>
                                    <a href="javascript:OpenAgreementNo('Collection','<%#Container.DataItem("AgreementNo")%>')">
                                        <%# Container.DataItem("AgreementNo")%>
                                    </a>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Name" HeaderText="NAMA CUSTOMER">
                                <ItemTemplate>
                                    <a href="javascript:OpenCustomer('Collection','<%# Container.DataItem("CustomerID")%>')">
                                        <%# Container.DataItem("Name")%>
                                    </a>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="DESCRIPTION" HeaderText="NAMA ASSET"></asp:BoundColumn>
                            <asp:BoundColumn DataField="REPOSSESDATE" HeaderText="TGL TARIK"></asp:BoundColumn>
                            <asp:BoundColumn DataField="ASSETSTATUS" HeaderText="STATUS ASSET"></asp:BoundColumn>
                            <%--<asp:TemplateColumn HeaderText="CETAK BPK">
                                <ItemTemplate>
                                    <a href="javascript:OpenWinPrintBPKReport('<%# Container.DataItem("ApplicationID")%>','<%# Container.DataItem("CollectorID")%>','<%# Container.DataItem("BranchID")%>','<%# Container.DataItem("RepossesSeqNo")%>')">
                                        <img src="../../Images/IconRelease.gif" width="19" height="15" border="0"></a>
                                </ItemTemplate>
                            </asp:TemplateColumn>--%>
                            
                            <asp:BoundColumn DataField="ApplicationID" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="CollectorID" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="BranchID" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="RepossesSeqNo" Visible="false"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <div class="button_gridnavigation">
                        <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                            CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                            CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                            CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                            CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                        <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                            EnableViewState="False"></asp:Button>
                        <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" MinimumValue="1"
                            ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                            ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general">
                        </asp:RequiredFieldValidator>
                    </div>
                    <div class="label_gridnavigation">
                        <asp:Label ID="lblPage" runat="server"></asp:Label>of
                        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                        <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
