﻿#Region "Import"
Imports System.Text
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports System.Data.SqlClient
#End Region
Public Class InventoryReturn
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oCGID As UcBranchCollection
    Public Property ApplicationId() As String
        Get
            Return CType(viewstate("ApplicationId"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationId") = Value
        End Set
    End Property
    Public Property cgid() As String
        Get
            Return CType(viewstate("cgid"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("cgid") = Value
        End Set
    End Property
    Public Property RepossesSeqNo() As String
        Get
            Return CType(viewstate("RepossesSeqNo"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("RepossesSeqNo") = Value
        End Set
    End Property
#Region " Private Const "
    Dim m_CollInvReturn As New CollInvReturnController
    Dim oCollInvSelling As New Parameter.CollInvSelling
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not Page.IsPostBack Then
            Me.FormID = "CollInvReturn"
            InitialDefaultPanel()
            'txtsellingprice.txtNumber.Enabled = False
            'txtbuyer.Enabled = False
        End If
        If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
            InitialDefaultPanel()
        End If
    End Sub
    Private Sub InitialDefaultPanel()
        pnlDtGrid.Visible = False
        pnlsearch.Visible = True
        pnlSelect.Visible = False
    End Sub
    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Buttonsearch.Click
        Dim strSearch As New StringBuilder
        Me.SearchBy = ""
        Me.SortBy = ""
        lblMessage.Visible = False
        If oCGID.BranchID.Trim <> "" Then
            strSearch.Append("CGID='" & oCGID.BranchID & "'")
            Me.cgid = oCGID.BranchID
        End If
        If cboSearchBy.SelectedIndex <> 0 And txtSearchBy.Text <> "" Then
            strSearch.Append(" and " & cboSearchBy.SelectedItem.Value.Trim & " like '%" & txtSearchBy.Text.Trim & "%'")
        End If
        Me.SearchBy = strSearch.ToString
        Bindgrid(Me.SearchBy, Me.SortBy)
    End Sub
    Sub Bindgrid(ByVal cmdWhere As String, ByVal cmSort As String)
        Dim dtvEntity As New DataView
        Dim dtsEntity As New DataTable

        With oCollInvSelling
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oCollInvSelling = m_CollInvReturn.InvReturnList(oCollInvSelling)

        With oCollInvSelling
            lblRecord.Text = CType(.TotalRecord, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecord
        End With

        dtsEntity = oCollInvSelling.listData
        dtsEntity.TableName = "Table"

        dtvEntity = dtsEntity.DefaultView
        dtvEntity.Sort = Me.SortBy

        dtgCollInvSell.DataSource = dtvEntity

        If Not dtvEntity Is Nothing Then
            dtgCollInvSell.DataBind()
            pnlDtGrid.Visible = True
        End If

        PagingFooter()
    End Sub


#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblRecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If

    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        Bindgrid(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click

        If txtPage.Text = "" Then
            txtPage.Text = "0"
        Else
            If IsNumeric(txtPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                    currentPage = CType(txtPage.Text, Int32)
                    Bindgrid(Me.SearchBy, Me.SortBy)
                End If
            End If
        End If


    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        Bindgrid(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

    Private Sub dtgCollInvSell_ItemCommand(source As Object, e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgCollInvSell.ItemCommand
        Select Case e.CommandName
            Case "Return"
                

                Dim lblbranchid As Label
                Dim lblApplicationId As Label
                Dim lblassetseqno As Label
                Dim lblRepossesseqno As Label
                Dim oDataTable As New DataTable

                lblassetseqno = CType(e.Item.FindControl("lblAssetSeqNo"), Label)
                lblRepossesseqno = CType(e.Item.FindControl("lblRepossesseqno"), Label)
                lblbranchid = CType(dtgCollInvSell.Items(e.Item.ItemIndex).FindControl("lblBranchId"), Label)
                lblApplicationId = CType(dtgCollInvSell.Items(e.Item.ItemIndex).FindControl("lblApplicationId"), Label)


                'If CheckFeature(Me.Loginid, Me.FormID, "Sell", Me.AppId) Then
                With oCollInvSelling
                    .strConnection = GetConnectionString()
                    .ApplicationID = lblApplicationId.Text.Trim
                    .BusinessDate = Me.BusinessDate
                    .BranchId = lblbranchid.Text.Trim
                    .RepossesSeqNo = lblRepossesseqno.Text.Trim
                End With

                oCollInvSelling = m_CollInvReturn.InvReturnDetail(oCollInvSelling)
                oDataTable = oCollInvSelling.listData
                Me.BranchID = lblbranchid.Text.Trim
                Me.ApplicationId = lblApplicationId.Text
                Me.RepossesSeqNo = lblRepossesseqno.Text


                If Not IsDBNull(oDataTable.Rows(0)("AgreementNo")) Then
                    hypAgreementNoSlct.Text = CType(oDataTable.Rows(0)("AgreementNo"), String)
                End If
                If Not IsDBNull(oDataTable.Rows(0)("CustomerName")) Then
                    hypCustomerNameSlct.Text = CType(oDataTable.Rows(0)("CustomerName"), String)
                End If
                If Not IsDBNull(oDataTable.Rows(0)("Description")) Then
                    lblAsset.Text = CType(oDataTable.Rows(0)("Description"), String)
                End If

                If Not IsDBNull(oDataTable.Rows(0)("RepossesDate")) Then
                    lblRepossessDate.Text = Format(oDataTable.Rows(0)("RepossesDate"), "dd/MM/yyyy")
                End If

                If Not IsDBNull(oDataTable.Rows(0)("InventoryDate")) Then
                    lblInventoryDate.Text = Format(oDataTable.Rows(0)("InventoryDate"), "dd/MM/yyyy")
                End If
                If Not IsDBNull(oDataTable.Rows(0)("InventoryAmount")) Then
                    LblInventoryAmount.Text = FormatNumber(oDataTable.Rows(0)("InventoryAmount"), 2)
                End If

                pnlDtGrid.Visible = False
                pnlsearch.Visible = False
                pnlSelect.Visible = True


                'End If
        End Select
    End Sub

    Private Sub ButtonCancel_Click(sender As Object, e As System.EventArgs) Handles ButtonCancel.Click
        InitialDefaultPanel()
        pnlDtGrid.Visible = True
    End Sub

    Private Sub ButtonSave_Click(sender As Object, e As System.EventArgs) Handles ButtonSave.Click
        With oCollInvSelling
            .strConnection = GetConnectionString()
            .ApplicationID = Me.ApplicationId
            .BranchId = Me.BranchID
            .BusinessDate = Me.BusinessDate
            .RepossesSeqNo = Me.RepossesSeqNo
        End With

        If m_CollInvReturn.InvReturnSave(oCollInvSelling) Then
            ShowMessage(lblMessage, "Simpan Data Berhasil", False)
            pnlsearch.Visible = True
            pnlDtGrid.Visible = False
            pnlSelect.Visible = False
        Else
            ShowMessage(lblMessage, "Simpan Data Gagal", True)
        End If
    End Sub
End Class