﻿#Region "Import"
Imports System.Text
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports System.Data.SqlClient
#End Region

Public Class InventorySelling
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oCGID As UcBranchCollection
    Protected WithEvents txtsellingprice As ucNumberFormat

#Region "properties"
    Public Property cgid() As String
        Get
            Return CType(ViewState("cgid"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("cgid") = Value
        End Set
    End Property


    Public Property ApplicationId() As String
        Get
            Return CType(ViewState("ApplicationId"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationId") = Value
        End Set
    End Property
    Public Property CustomerName() As String
        Get
            Return CType(ViewState("CustomerName"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerName") = Value
        End Set
    End Property
    Public Property AgreementNo() As String
        Get
            Return CType(ViewState("AgreementNo"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("AgreementNo") = Value
        End Set
    End Property
    Public Property customerID() As String
        Get
            Return CType(ViewState("customerID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("customerID") = Value
        End Set
    End Property
    Public Property RepossesSeqNo() As String
        Get
            Return CType(ViewState("RepossesSeqNo"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("RepossesSeqNo") = Value
        End Set
    End Property
#End Region

#Region " Private Const "
    Dim m_CollInvSelling As New CollInvSellingController
    Dim oCollInvSelling As New Parameter.CollInvSelling
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region

    Private Sub InitialDefaultPanel()
        pnlDtGrid.Visible = False
        pnlsearch.Visible = True
        pnlSelect.Visible = False
        With txtsellingprice
            .RequiredFieldValidatorEnable = True
            .RangeValidatorEnable = False
        End With
    End Sub

#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If

    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        Bindgrid(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click

        If txtPage.Text = "" Then
            txtPage.Text = "0"
        Else
            If IsNumeric(txtPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                    currentPage = CType(txtPage.Text, Int32)
                    Bindgrid(Me.SearchBy, Me.SortBy)
                End If
            End If
        End If


    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        Bindgrid(Me.SearchBy, Me.SortBy)
    End Sub
#End Region


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        Page.Header.DataBind()

        'Put user code to initialize the page here
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not Page.IsPostBack Then
            Me.FormID = "CollInvSelling"
            Me.cgid = Me.GroubDbID
            InitialDefaultPanel()
            'txtsellingprice.txtNumber.Enabled = False
            'txtbuyer.Enabled = False
        End If
        If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
            InitialDefaultPanel()
        End If
    End Sub
    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Buttonsearch.Click
        Dim strSearch As New StringBuilder
        Me.SearchBy = ""
        Me.SortBy = ""
        If oCGID.BranchID.Trim <> "" Then
            strSearch.Append("CGID='" & oCGID.BranchID & "'")
            Me.cgid = oCGID.BranchID
        End If
        If cboSearchBy.SelectedIndex <> 0 And txtSearchBy.Text <> "" Then
            If strSearch.ToString = "" Then
                strSearch.Append(cboSearchBy.SelectedItem.Value.Trim & " like '%" + txtSearchBy.Text.Trim + "%'")
            Else
                strSearch.Append(" and " & cboSearchBy.SelectedItem.Value.Trim & " like '%" + txtSearchBy.Text.Trim + "%'")
            End If
        End If
        Me.SearchBy = strSearch.ToString
        Bindgrid(Me.SearchBy, Me.SortBy)
    End Sub
    Sub Bindgrid(ByVal cmdWhere As String, ByVal cmSort As String)
        Dim dtvEntity As New DataView
        Dim dtsEntity As New DataTable


        With oCollInvSelling
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oCollInvSelling = m_CollInvSelling.InvSellingList(oCollInvSelling)

        With oCollInvSelling
            lblrecord.Text = CType(.TotalRecord, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecord
        End With

        dtsEntity = oCollInvSelling.listData
        dtsEntity.TableName = "Table"

        dtvEntity = dtsEntity.DefaultView
        dtvEntity.Sort = Me.SortBy

        dtgCollInvSell.DataSource = dtvEntity

        If Not dtvEntity Is Nothing Then
            dtgCollInvSell.DataBind()
            pnlDtGrid.Visible = True
        End If

        PagingFooter()
    End Sub
    Private Sub imbSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
        pnlSelect.Visible = True
        If (txtSellingDate.Text = "") Then
            ShowMessage(lblMessage, "Harap isi Tanggal Jual", True)
            Exit Sub
        ElseIf (txtbuyer.Text = "") Then
            ShowMessage(lblMessage, "Harap isi Nama Pembeli", True)
            pnlsearch.Visible = False
            Exit Sub

        ElseIf (txtsellingprice.Text = "" Or txtsellingprice.Text = "0") Then
            ShowMessage(lblMessage, "Harap isi Harga Jual", True)
            Exit Sub

        ElseIf ConvertDate2(txtSellingDate.Text) > Me.BusinessDate Then
            ShowMessage(lblMessage, "Tanggal Estimasi harus <= Tanggal hari ini", True)
            Exit Sub
        End If


        With oCollInvSelling
            .strConnection = GetConnectionString()
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationId
            .RepossesSeqNo = CType(Me.RepossesSeqNo, Integer)
            .SellingPrice = CDbl(txtsellingprice.Text)
            .SellingDate = ConvertDate2(txtSellingDate.Text)
            .SellingNotes = txtNotes.Text
            .Buyer = txtbuyer.Text
        End With

        If m_CollInvSelling.InvSellingSave(oCollInvSelling) Then
            ShowMessage(lblMessage, "Simpan Data Berhasil", False)
            pnlsearch.Visible = True
            pnlDtGrid.Visible = False
            pnlSelect.Visible = False
        Else
            ShowMessage(lblMessage, "Simpan Data Gagal", True)
        End If
    End Sub

    Private Sub dtgCollInvSell_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgCollInvSell.ItemCommand
        Me.BranchID = e.Item.Cells(0).Text.Trim
        Me.ApplicationId = e.Item.Cells(1).Text.Trim
        Me.customerID = e.Item.Cells(2).Text.Trim
        Me.AgreementNo = e.Item.Cells(3).Text.Trim
        Me.CustomerName = e.Item.Cells(4).Text.Trim
        Me.RepossesSeqNo = e.Item.Cells(5).Text.Trim
        If e.CommandName = "Sell" Then
            If CheckFeature(Me.Loginid, Me.FormID, "Sell", Me.AppId) Then
                If SessionInvalid() Then
                    Dim strHTTPServer As String
                    Dim strHTTPApp As String
                    Dim strNameServer As String
                    strHTTPServer = Request.ServerVariables("PATH_INFO")
                    strNameServer = Request.ServerVariables("SERVER_NAME")
                    strHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                    Response.Redirect("http://" & strNameServer & "/" & strHTTPApp & "/NotAuthorized.html")
                Else
                    viewRepoDetail()
                End If
            End If
        End If
    End Sub
    Private Sub viewRepoDetail()
        clearfield()
        pnlDtGrid.Visible = False
        pnlsearch.Visible = False
        pnlSelect.Visible = True

        Dim dtsEntity As New DataTable
        Dim dtvEntity As New DataView

        Dim dt As New DataTable
        Dim dt1 As New DataTable
        Dim dv As New DataView

        With oCollInvSelling
            .strConnection = GetConnectionString()
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationId
            .RepossesSeqNo = CType(Me.RepossesSeqNo, Integer)
        End With

        oCollInvSelling = m_CollInvSelling.InvSellingDetail(oCollInvSelling)
        ButtonSave.Visible = True
        dt = oCollInvSelling.listData
        If dt.Rows.Count = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan", True)
            ButtonSave.Visible = False
            Exit Sub
        End If
        With dt.Rows(0)
            lblColor.Text = CStr(IIf(IsDBNull(.Item("Color")), "", .Item("Color")))
            lblARInsurance.Text = CStr(IIf(IsDBNull(.Item("ARInsurance")), "", .Item("ARInsurance")))
            lblAssetdesc.Text = CStr(IIf(IsDBNull(.Item("Description")), "", .Item("Description")))
            lblAssetYear.Text = CStr(IIf(IsDBNull(.Item("AssetYear")), "", .Item("AssetYear")))
            lblChasisNo.Text = CStr(IIf(IsDBNull(.Item("ChassisNo")), "", .Item("ChassisNo")))
            lblEngineNo.Text = CStr(IIf(IsDBNull(.Item("EngineNo")), "", .Item("EngineNo")))
            lblLateCharges.Text = FormatNumber(CStr(IIf(IsDBNull(.Item("LC")), "", .Item("LC"))), 0)
            lblInventoryAmount.Text = FormatNumber(CStr(IIf(IsDBNull(.Item("InventoryAmount")), "", .Item("InventoryAmount"))), 0)
            lblInventoryDate.Text = CStr(IIf(IsDBNull(.Item("InventoryDate")), "", .Item("InventoryDate")))
            lblLisencePlate.Text = CStr(IIf(IsDBNull(.Item("LicensePlate")), "", .Item("LicensePlate")))
            lblBillingCharges.Text = FormatNumber(IIf(IsDBNull(.Item("BillingCharges")), "", .Item("BillingCharges")), 0)
            lblAccruedInterest.Text = FormatNumber(IIf(IsDBNull(.Item("AccruedInterest")), "", .Item("AccruedInterest")), 0)
            lblRepossessFee.Text = FormatNumber(IIf(IsDBNull(.Item("RepossesFee")), "", .Item("RepossesFee")), 0)
            lblRepossessDate.Text = CStr(IIf(IsDBNull(.Item("RepossesDate")), "", .Item("RepossesDate")))
            lblARInsurance.Text = FormatNumber(IIf(IsDBNull(.Item("ARInsurance")), "", .Item("ARInsurance")), 0)
            lblLateCharges.Text = FormatNumber(IIf(IsDBNull(.Item("LateCharges")), "", .Item("LateCharges")), 0)
            lblestimationprice1.Text = FormatNumber(IIf(IsDBNull(.Item("EstimationAmount")), "", .Item("EstimationAmount")), 0)
            lblestimationdate1.Text = Format(IIf(IsDBNull(.Item("EstimationDate")), "", .Item("EstimationDate")), "dd/MM/yyyy")
            txtsellingprice.Text = FormatNumber(CStr(IIf(IsDBNull(.Item("Bestbiddervalue")), "", .Item("Bestbiddervalue"))), 0)
            txtbuyer.Text = CStr(IIf(IsDBNull(.Item("Bestbiddername")), "", .Item("Bestbiddername")))

        End With
        lblTotalAR.Text = FormatNumber(CDbl(dt.Rows(0)("AccruedInterest")) + _
                                CDbl(dt.Rows(0)("RepossesFee")) + _
                                CDbl(dt.Rows(0)("BillingCharges")) + _
                                CDbl(dt.Rows(0)("LateCharges")) + _
                                CDbl(dt.Rows(0)("ARInsurance")), 0)


        With oCollInvSelling
            .strConnection = GetConnectionString()
            .BusinessDate = Me.BusinessDate
            .ApplicationID = Me.ApplicationId
            .BranchId = Me.BranchID

        End With
        ' txtsellingprice.Text = "0"

        oCollInvSelling = m_CollInvSelling.ViewAppraisalbidder(oCollInvSelling)
        dt = oCollInvSelling.ListBidder
        dv = dt.DefaultView
        dtgbidder.DataSource = dv
        dtgbidder.DataBind()

    End Sub
    Sub clearfield()
        lblColor.Text = ""
        lblARInsurance.Text = ""
        lblAssetdesc.Text = ""
        lblAssetYear.Text = ""
        lblChasisNo.Text = ""
        lblEngineNo.Text = ""
        lblLateCharges.Text = ""
        lblInventoryAmount.Text = ""
        lblInventoryDate.Text = ""
        lblLisencePlate.Text = ""
        'lblPickUpFee.Text = ""
        lblRepossessFee.Text = ""
        lblRepossessDate.Text = ""
    End Sub
    Private Sub imbCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        InitialDefaultPanel()
        pnlDtGrid.Visible = True
    End Sub


End Class