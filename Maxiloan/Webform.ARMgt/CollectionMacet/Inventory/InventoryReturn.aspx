﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="InventoryReturn.aspx.vb" Inherits="Maxiloan.Webform.ARMgt.InventoryReturn" %>

<%@ Register TagPrefix="uc1" TagName="UcBranchCollection" Src="../../../Webform.UserController/UcBranchCollection.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Pembatalan Asset Tarikan</title>
    <script src="../../../maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../../Include/General.css" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>        
                <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
                <div class="form_title">
                    <div class="title_strip">
                    </div>
                    <div class="form_single">
                        <h3>
                            PEMBATALAN ASSET DARI INVENTORY
                        </h3>
                    </div>
                </div>
                <asp:Panel ID="pnlsearch" runat="server">
                    <div class="form_box">
                        <div class="form_single">
                            <label>
                                Collection Group
                            </label>
                            <uc1:ucbranchcollection id="oCGID" runat="server"></uc1:ucbranchcollection>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label>
                                Cari Berdasarkan</label>
                            <asp:DropDownList ID="cboSearchBy" runat="server">
                                <asp:ListItem Value="Select One">Select One</asp:ListItem>
                                <asp:ListItem Value="AgreementNo">No Kontrak</asp:ListItem>
                                <asp:ListItem Value="Customer.Name">Nama Konsumen</asp:ListItem>
                            </asp:DropDownList>
                            <asp:TextBox ID="txtSearchBy" runat="server"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form_button">
                        <asp:Button ID="Buttonsearch" runat="server" Text="Search" CssClass="small button blue">
                        </asp:Button>&nbsp;
                        <asp:Button ID="ButtonReset" runat="server" Text="Reset" CssClass="small button gray"
                            CausesValidation="False"></asp:Button>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlDtGrid" runat="server">
                    <div class="form_box_title">
                        <div class="form_single">
                            <div class="grid_wrapper_ns">
                                <asp:DataGrid ID="dtgCollInvSell" runat="server" Width="100%" OnSortCommand="Sorting"
                                    BorderStyle="None" BorderWidth="0" DataKeyField="AgreementNo" AutoGenerateColumns="False"
                                    AllowSorting="True" CssClass="grid_general">
                                    <HeaderStyle CssClass="th" />
                                    <ItemStyle CssClass="item_grid" />
                                    <Columns>
                                        <asp:TemplateColumn HeaderText="...">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="ImbSell" CommandName="Return" ImageUrl="../../../Images/iconsale.gif"
                                                    runat="server"></asp:ImageButton>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">
                                            <ItemTemplate>
                                                <asp:HyperLink ID="hypAgreementNodtg" runat="server" Text='<%# Container.Dataitem("AgreementNo")%>'>
                                                </asp:HyperLink>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn SortExpression="CustomerName" HeaderText="NAMA KONSUMEN">
                                            <ItemTemplate>
                                                <a href="javascript:OpenWinCustomer('<%# Container.Dataitem("CustomerID")%>');">
                                                    <asp:Label ID="hypCustomerNamedtg" runat="server" Text='<%# Container.Dataitem("CustomerName")%>'>
                                                    </asp:Label></a>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:BoundColumn DataField="Description" SortExpression="Description" HeaderText="NAMA ASSET">
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="RepossesDate" SortExpression="RepossesDate" HeaderText="TGL TARIK"
                                            DataFormatString="{0:dd/MM/yyyy}">
                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center" Width="8%"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="InventoryDate" SortExpression="InventoryDate" HeaderText="TGL INVENTORY"
                                            DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
                                        <asp:TemplateColumn Visible="False" SortExpression="ApplicationId" HeaderText="ApplicationId">
                                            <ItemTemplate>
                                                <asp:Label ID="lblApplicationId" runat="server" Text='<%# Container.Dataitem("ApplicationId")%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn Visible="False" HeaderText="BranchId">
                                            <ItemTemplate>
                                                <asp:Label ID="lblBranchId" runat="server" Text='<%# Container.Dataitem("BranchID")%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn>
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblRALNo" Text='<%# Container.Dataitem("RALNo")%>'
                                                    Visible="false">
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn Visible="false" HeaderText="RepossesSeqNo">
                                        <ItemTemplate>
                                            <asp:Label ID="lblrepossesseqno" runat="server" Text='<%# Container.Dataitem("repossesseqNo")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                                <div class="button_gridnavigation">
                                    <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../../Images/grid_navbutton01.png"
                                        CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                                    </asp:ImageButton>
                                    <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../../Images/grid_navbutton02.png"
                                        CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                                    </asp:ImageButton>
                                    <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../../Images/grid_navbutton03.png"
                                        CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                                    </asp:ImageButton>
                                    <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../../Images/grid_navbutton04.png"
                                        CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                                    </asp:ImageButton>
                                    <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                                    <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                                        EnableViewState="False"></asp:Button>
                                    <asp:RangeValidator CssClass="validator_general" ID="rgvGo" runat="server" ControlToValidate="txtPage"
                                        MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer"></asp:RangeValidator>
                                    <asp:RequiredFieldValidator CssClass="validator_general" ID="rfvGo" runat="server"
                                        ControlToValidate="txtPage" ErrorMessage="No Halaman Salah" Display="Dynamic"></asp:RequiredFieldValidator>
                                </div>
                                <div class="label_gridnavigation">
                                    <asp:Label ID="lblPage" runat="server"></asp:Label>of
                                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                                    <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlSelect" runat="server">
                    <div class="form_box">
                        <div class="form_left">
                            <label>
                                No Kontrak</label>
                            <asp:HyperLink ID="hypAgreementNoSlct" runat="server"></asp:HyperLink>
                        </div>
                        <div class="form_right">
                            <label>
                                Nama Customer</label>
                            <asp:HyperLink ID="hypCustomerNameSlct" runat="server"></asp:HyperLink>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_left">
                            <label>
                                Nama Asset</label>
                            <asp:Label ID="lblAsset" runat="server"></asp:Label>
                        </div>
                        <div class="form_right">
                            <label>
                                Tanggal Tarik</label>
                            <asp:Label ID="lblRepossessDate" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_left">
                            <label>
                                Ditarik Oleh</label>
                            <asp:Label ID="lblrepossessby" runat="server"></asp:Label>
                        </div>
                        <div class="form_right">
                            <label>
                                Tanggal Inventory</label>
                            <asp:Label ID="lblInventoryDate" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_left">
                            <label>
                                Nilai Inventory</label>
                            <asp:Label ID="LblInventoryAmount" runat="server"></asp:Label>
                        </div>
                        <div class="form_right">
                            
                        </div>
                    </div>
                    <div class="form_button">
                        <asp:Button ID="ButtonSave" runat="server" Text="Save" CssClass="small button blue"
                            CausesValidation="True"></asp:Button>&nbsp;
                        <asp:Button ID="ButtonCancel" runat="server" Text="Cancel" CssClass="small button gray"
                            CausesValidation="False"></asp:Button>
                    </div>
                </asp:Panel>             
    </form>
</body>
</html>
