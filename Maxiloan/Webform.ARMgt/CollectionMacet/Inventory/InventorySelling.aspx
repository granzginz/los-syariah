﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="InventorySelling.aspx.vb"
    Inherits="Maxiloan.Webform.ARMgt.InventorySelling" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcBranchCollection" Src="../../../Webform.UserController/UcBranchCollection.ascx" %>
<%@ Register TagName="ucNumberFormat" TagPrefix="uc1" Src="../../../webform.UserController/ucNumberFormat.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>InventorySelling</title>
    <script src="../../../maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../../Include/General.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;
        var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%#Request.servervariables("SERVER_NAME")%>/';

        function OpenWinPrintBPKReport(app, col, branch, RepossesSeqNo) {
            window.open('PrinBPKViewer.aspx?ApplicationID=' + app + '&ColectorID=' + col + '&branch=' + branch + '&RepossesSeqNo=' + RepossesSeqNo, 'UserLookup', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }		
		
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>    
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>
                        PENJUALAN ASSET DARI INVENTORY
                    </h3>
                </div>
            </div>
            <asp:Panel ID="pnlsearch" runat="server">
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Collection Group
                        </label>
                        <uc1:ucbranchcollection id="oCGID" runat="server"></uc1:ucbranchcollection>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Cari Berdasarkan</label>
                        <asp:DropDownList ID="cboSearchBy" runat="server">
                            <asp:ListItem Value="Select One">Select One</asp:ListItem>
                            <asp:ListItem Value="AgreementNo">No Kontrak</asp:ListItem>
                            <asp:ListItem Value="Customer.Name">Nama Konsumen</asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtSearchBy" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="Buttonsearch" runat="server" Text="Search" CssClass="small button blue">
                    </asp:Button>&nbsp;
                    <asp:Button ID="ButtonReset" runat="server" Text="Reset" CssClass="small button gray"
                        CausesValidation="False"></asp:Button>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlDtGrid" runat="server">
                <div class="form_box_title">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtgCollInvSell" runat="server" Width="100%" OnSortCommand="Sorting"
                                BorderStyle="None" BorderWidth="0" DataKeyField="AgreementNo" AutoGenerateColumns="False"
                                AllowSorting="True" CssClass="grid_general">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:BoundColumn Visible="False" DataField="BranchID"></asp:BoundColumn>
                                    <asp:BoundColumn Visible="False" DataField="ApplicationID"></asp:BoundColumn>
                                    <asp:BoundColumn Visible="False" DataField="CustomerID"></asp:BoundColumn>
                                    <asp:BoundColumn Visible="False" DataField="AgreementNo"></asp:BoundColumn>
                                    <asp:BoundColumn Visible="False" DataField="Name"></asp:BoundColumn>
                                    <asp:BoundColumn Visible="False" DataField="RepossesSeqNo"></asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="JUAL">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ImbSell" CommandName="Sell" ImageUrl="../../../Images/iconsale.gif"
                                                runat="server"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>

                                    <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">
                                        <ItemTemplate>
                                            <a href="javascript:OpenAgreementNo('Collection','<%# Container.DataItem("ApplicationID")%>')">
                                                <%# Container.DataItem("AgreementNo")%>
                                            </a>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="CustomerName" HeaderText="NAMA CUSTOMER">
                                        <ItemTemplate>
                                            <a href="javascript:OpenCustomer('Collection','<%# Container.DataItem("CustomerID")%>')">
                                                <%# Container.DataItem("Name")%>
                                            </a>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="DESCRIPTION" HeaderText="NAMA ASSET"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="ESTIMATIONAMOUNT" HeaderText="NILAI ESTIMASI" DataFormatString="{0:N2}">
                                    </asp:BoundColumn>
                                </Columns>
                            </asp:DataGrid>
                            <div class="button_gridnavigation">
                                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../../Images/grid_navbutton01.png"
                                    CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../../Images/grid_navbutton02.png"
                                    CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../../Images/grid_navbutton03.png"
                                    CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../../Images/grid_navbutton04.png"
                                    CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                                    EnableViewState="False"></asp:Button>
                                <asp:RangeValidator CssClass="validator_general" ID="rgvGo" runat="server" ControlToValidate="txtPage"
                                    MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer"></asp:RangeValidator>
                                <asp:RequiredFieldValidator CssClass="validator_general" ID="rfvGo" runat="server"
                                    ControlToValidate="txtPage" ErrorMessage="No Halaman Salah" Display="Dynamic"></asp:RequiredFieldValidator>
                            </div>
                            <div class="label_gridnavigation">
                                <asp:Label ID="lblPage" runat="server"></asp:Label>of
                                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlSelect" runat="server">
                <div class="form_box_title">
                    <div class="form_single">
                        <label>
                            No Kontrak</label>
                        <a href="javascript:OpenAgreementNo('Collection','<%=me.applicationID%>')">
                            <%=me.AgreementNo%></a>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Nama Customer</label>
                        <a href="javascript:OpenCustomer('Collection','<%=me.CustomerID%>')">
                            <%=me.CustomerName%></a>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Nama Asset</label>
                        <asp:Label ID="lblAssetdesc" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            No Rangka</label>
                        <asp:Label ID="lblChasisNo" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            No Mesin</label>
                        <asp:Label ID="lblEngineNo" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            No Polisi</label>
                        <asp:Label ID="lblLisencePlate" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Tahun Produksi</label>
                        <asp:Label ID="lblAssetYear" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Warna</label>
                        <asp:Label ID="lblColor" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Tanggal Tarik</label>
                        <asp:Label ID="lblRepossessDate" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Tanggal Inventory</label>
                        <asp:Label ID="lblInventoryDate" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Accrued Interest</label>
                        <asp:Label ID="lblAccruedInterest" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            A/R Asuransi</label>
                        <asp:Label ID="lblARInsurance" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Biaya Tarik</label>
                        <asp:Label ID="lblRepossessFee" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Biaya Tagih</label>
                        <asp:Label ID="lblBillingCharges" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Denda Keterlambatan</label>
                        <asp:Label ID="lblLateCharges" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            <strong>Nilai Inventory</strong></label>
                        <asp:Label ID="lblInventoryAmount" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            <strong>Total A/R Customer</strong></label>
                        <asp:Label ID="lblTotalAR" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box_title">
                    <div class="form_single">
                        <h4>
                            DETAIL APPRAISAL
                        </h4>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtgbidder" runat="server" Width="100%" BorderStyle="None" BorderWidth="0"
                                AutoGenerateColumns="false" CssClass="grid_general">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="NO">
                                        <ItemTemplate>
                                            <asp:Label ID="lblbidderno" runat="server" Text='<%#Container.DataItem("BidderNo")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="NAMA PENAWAR">
                                        <ItemTemplate>
                                            <asp:Label ID="lblbiddername" runat="server" Text='<%#Container.DataItem("BidderName")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="NILAI DITAWAR">
                                        <ItemTemplate>
                                            <asp:Label ID="Lblbidderamount" runat="server" Text='<%#FormatNumber(Container.DataItem("BidderAmount"),2)%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="BidderDate" HeaderText="TGL TAWAR" DataFormatString="{0:dd/MM/yyyy}">
                                    </asp:BoundColumn>
                                </Columns>
                                
                            </asp:DataGrid>
                        </div>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Harga Estimasi</label>
                        <asp:Label ID="lblestimationprice1" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Tanggal Estimasi</label>
                        <asp:Label ID="lblestimationdate1" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Harga Jual</label>
                        <%--    <asp:TextBox ID="txtsellingprice" runat="server" ></asp:TextBox>
                    <asp:RegularExpressionValidator ID="Regularexpressionvalidator4" runat="server" ErrorMessage="Harap isi Harga Jual" CssClass="validator_general"
                        ControlToValidate="txtsellingprice" ValidationExpression="^\d{1,}$|^\d{1,}[.{1}]\d{1,}$"
                        Display="Dynamic"></asp:RegularExpressionValidator>--%>
                        <uc1:ucnumberformat id="txtsellingprice" runat="server" />
                        </uc1:ucnumberformat>
                    </div>
                    <div class="form_right">
                        <label class="label_req">
                            Tanggal Jual</label>
                        <asp:TextBox ID="txtSellingDate" runat="server"></asp:TextBox>
                        <asp:CalendarExtender ID="txtSellingDate_CalendarExtender" runat="server" Enabled="True"
                            TargetControlID="txtSellingDate" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                            CssClass="validator_general" ControlToValidate="txtSellingDate"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Pembeli</label>
                        <asp:TextBox ID="txtbuyer" runat="server" Width="345px"></asp:TextBox>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_general">
                            Catatan</label>
                        <asp:TextBox ID="txtNotes" runat="server" CssClass="multiline_textbox" TextMode="MultiLine"></asp:TextBox>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonSave" runat="server" Text="Save" CssClass="small button blue">
                    </asp:Button>&nbsp;
                    <asp:Button ID="ButtonCancel" runat="server" Text="Cancel" CssClass="small button gray"
                        CausesValidation="False"></asp:Button>
                </div>
            </asp:Panel>        
    </form>
</body>
</html>
