﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="RemidialPlan.aspx.vb" Inherits="Maxiloan.Webform.ARMgt.RemidialPlan" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="CollActResult" Src="../../../Webform.UserController/ucCollectionActResult.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>DCRPlan</title>
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%#Request.servervariables("SERVER_NAME")%>/';			
    </script>    
    <script language="JavaScript" type="text/javascript">
						
				<!--
				var hdnDetail;
				var hdndetailvalue;
				var cboBankAccount = null;
				function ParentChange(pCmbOfPayment,pBankAccount,pHdnDetail,pHdnDetailValue, itemArray)
				{
						hdnDetail = eval('document.forms[0].' + pHdnDetail);
						HdnDetailValue = eval('document.forms[0].'+pHdnDetailValue);
						var i, j;
						for(i= eval('document.forms[0].' + pBankAccount).options.length;i>=0;i--)
						{   
							eval('document.forms[0].' + pBankAccount).options[i] = null
						
						}  ;
						if (itemArray==null) 
						{ 
							j = 0 ;
						}
						else
						{  
							j=1;
						};
						eval('document.forms[0].' + pBankAccount).options[0] = new Option('Select One','0');
						if (itemArray!=null)
						{
							for(i=0;i<itemArray.length;i++)
							{	
								eval('document.forms[0].' + pBankAccount).options[j++]=new Option(itemArray[i][0], itemArray[i][1]);
								
							};
							eval('document.forms[0].' + pBankAccount).selected=true;
						}
						};
						
						function cboChildonChange(selIndex,l,j)
							{
								hdnDetail.value = l; 
								HdnDetailValue.value = j;
								eval('document.forms[0].hdnBankAccount').value = selIndex;
							}
						function RestoreInsuranceCoIndex(BankAccountIndex)
						{								
							cboChild  = eval('document.forms[0].cboChild');
							if (cboChild != null)
								if(eval('document.forms[0].hdnBankAccount').value != null)
								{
									if(BankAccountIndex != null)
									cboChild.selectedIndex = BankAccountIndex;
								}
						}
				}-->
    </script>
    <script language="javascript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;
        function OpenActivity() {
            window.open('../Inquiry/InqCollActivityResult.aspx?AgreementNo=' + hdnAgreement.value + '&CustomerName=' + hdnCustName.value + '&CustomerID=' + hdnCustID.value + '&Style=Collection&Referrer=CLActivity', 'UserLookup', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }
        function OpenCollection() {
            window.open('../Inquiry/InqCollActivityHistory.aspx?AgreementNo=' + hdnAgreement.value + '&CustomerName=' + hdnCustName.value + '&CustomerID=' + hdnCustID.value + '', 'UserLookup', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }
    </script>
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../../Include/General.css" type="text/css" />
</head>
<body>
    <input id="hdnAgreement" type="hidden" name="hdnAgreement" runat="server" />
    <input id="hdnCustName" type="hidden" name="hdnCustName" runat="server" />
    <input id="hdnCustID" type="hidden" name="hdnCustID" runat="server" />
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>    
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlResult" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">           
                <h3>
                    HASIL Remidial
                </h3>
            </div>
        </div>         
        <asp:HiddenField  ID="hdnCollector" runat="server" />
          <uc1:CollActResult id="ucCollActResult" runat="server" ></uc1:CollActResult> 
     <%--   <div class="form_box">
	        <div class="form_single">
                <label>No Kontrak</label>
                <asp:HyperLink ID="hypAgreementNo" runat="server"></asp:HyperLink>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Nama Customer</label>
                <asp:HyperLink ID="hypCustomerName" runat="server"></asp:HyperLink>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Alamat</label>
                <asp:Label ID="lblAddress" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Kendaraan</label>
                <asp:Label ID="lblAsset" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>No Polisi</label>
                <asp:Label ID="lblLicenseNo" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Kolektor</label>
                <asp:Label ID="lblCollector" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Nama CMO</label>
                <asp:Label ID="lblCMOName" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Nama Marketing Head</label>
                <asp:Label ID="lblCMOSPVNo" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>No Telepon - Alamat Tagihan</label>
                <asp:Label ID="lblMailingPhone1" runat="server"></asp:Label>&nbsp;,
                <asp:Label ID="lblMailingPhone2" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>No Telepon - Rumah</label>
                <asp:Label ID="lblResidencePhone1" runat="server"></asp:Label>&nbsp;,
                <asp:Label ID="lblResidencePhone2" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>No HandPhone</label>
                <asp:Label ID="lblMobilePhone" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>No Telepon - Kantor</label>
                <asp:Label ID="lblCompanyPhone1" runat="server"></asp:Label>&nbsp;,
                <asp:Label ID="lblCompanyPhone2" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>No Telepon - Alamat KTP</label>
                <asp:Label ID="lblLegalPhone1" runat="server"></asp:Label>&nbsp;,
                <asp:Label ID="lblLegalPhone2" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">	        
		        <div class="form_left">
                    <label>Status SMS Terkirim</label>
                    <asp:Label ID="lblisSMS" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">	
                    <label>Permintaan PDC</label>		
                    <asp:Label ID="lblPDCRequest" runat="server"></asp:Label>
		        </div>	        
        </div> 
        <div class="form_box">	        
		        <div class="form_left">
                    <label>Jumlah Hari Tidak Telat</label>
                    <asp:Label ID="lblPreviousOverDue" runat="server" Visible="False"></asp:Label>&nbsp;days
		        </div>
		        <div class="form_right">		
                    <label>Jumlah Hari Telat</label>                    	
                    <asp:Label ID="lblOverDueDays" runat="server"></asp:Label>&nbsp;days
		        </div>	        
        </div>
        <div class="form_box">	        
		        <div class="form_left">
                    <label>Tanggal Jatuh Tempo</label>
                    <asp:Label ID="lblDueDate" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">			
                    <label>Angsuran Ke</label>
                    <asp:Label ID="lblInstallmentNo" runat="server"></asp:Label>
		        </div>	        
        </div>--%>
        <div class="form_box_header">
            <div class="form_single">              
                <h4>
                    DATA KEUANGAN
                </h4>
            </div>
        </div>
        <div class="form_box">	        
		        <div class="form_left">
                    <label>Jumlah Angsuran</label>
                    <asp:Label ID="lblInstallmentAmt" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">		
                    <label>Sisa Pinjaman</label>	
                    <asp:Label ID="lblOSBalance" runat="server"></asp:Label>
		        </div>	        
        </div>
        <div class="form_box">	        
		        <div class="form_left">
                    <label>Jumlah Angsuran Jatuh Tempo</label>
                    <asp:Label ID="lblOverDueAmt" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">		
                    <label>Jumlah Denda Asuransi</label>	
                    <asp:Label ID="lblOSInsurance" runat="server"></asp:Label></div>
		        </div>	        
        </div>
        <div class="form_box">	        
		        <div class="form_left">
                    <label>Jumnlah Denda Keterlambatan</label>
                    <asp:Label ID="lblOSInstallmentAmt" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">	
                    <label>Jumlah Biaya Tolakan PDC</label>		
                    <asp:Label ID="lblPDCBounceFee" runat="server"></asp:Label>
		        </div>	        
        </div>
        <div class="form_box">
	        <div class="form_single">
		        <label>Jumlah Biaya Tagih</label>
                <asp:Label ID="lblOSBillingCharge" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">              
                <h4>
                    HASIL KEGIATAN
                </h4>
            </div>
        </div>        
        <div class="form_box">
	        <div class="form_single">
                <label>Rencana Kegiatan Sebelumnya</label>
                <asp:Label ID="LblPreviousPlanActivity" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Rencana Kegiatan</label>
                <asp:DropDownList ID="cboPlanActivity" runat="server">
                </asp:DropDownList>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label class ="label_req">Tanggal Rencana</label>                

                <asp:TextBox ID="txtPlanDate" runat="server"></asp:TextBox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Hour&nbsp;                   
                <asp:CalendarExtender ID="txtPlanDate_CalendarExtender" runat="server" 
                    Enabled="True" TargetControlID="txtPlanDate" Format ="dd/MM/yyyy"></asp:CalendarExtender>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*" CssClass="validator_general"
                ControlToValidate="txtPlanDate"></asp:RequiredFieldValidator>

                <asp:TextBox ID="txtHour" runat="server" MaxLength="2" Width="36px" ></asp:TextBox>&nbsp;
                <asp:RangeValidator ID="rgvHour" runat="server" ControlToValidate="txtHour" Display="Dynamic" CssClass="validator_general"
                    MaximumValue="24" MinimumValue="0" Type="Integer" ErrorMessage="*"></asp:RangeValidator>&nbsp;
                :&nbsp;&nbsp;&nbsp;
                <asp:TextBox ID="txtMinute" runat="server" MaxLength="2" Width="36px" ></asp:TextBox>&nbsp;
                <asp:RangeValidator ID="rgvMinute" runat="server" ControlToValidate="txtMinute" Display="Dynamic" CssClass="validator_general"
                    MaximumValue="60" MinimumValue="0" Type="Integer" ErrorMessage="*"></asp:RangeValidator>24-Hours
	        </div>
        </div>
        <%--<div class="form_box">
	        <div class="form_single">
                <a href="javascript:OpenActivity()">
                <asp:Label ID="lblActivityHistory" runat="server" Visible="True">HISTORY KEGIATAN</asp:Label></a>
                <a href="javascript:OpenCollection()">
                <asp:Label ID="lblCollectionHistory" runat="server" Visible="True">HISTORY COLLECTION</asp:Label></a></b>
	        </div>
        </div>--%>
        <div class="form_button">
            <asp:Button ID="ButtonSave" runat="server"  Text="Save" CssClass ="small button blue">
            </asp:Button>&nbsp;
            <asp:Button ID="ButtonCancel" runat="server"  Text="Cancel" CssClass ="small button gray"
                CausesValidation="False"></asp:Button>&nbsp;<a href="javascript:history.go(-1);"></a>
	    </div>                    
    </asp:Panel>                           
    </form>
</body>
</html>
