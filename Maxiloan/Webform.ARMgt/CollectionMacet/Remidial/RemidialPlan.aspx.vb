﻿#Region "Imports"
Imports System.Data.SqlClient
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class RemidialPlan
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents ucCollActResult As ucCollectionActResult
#Region "Property"
    Private Property ValidDueDate() As String
        Get
            Return CType(ViewState("ValidDueDate"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ValidDueDate") = Value
        End Set
    End Property

    Private Property ApplicationID() As String
        Get
            Return CType(ViewState("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property


    Private Property CGID() As String
        Get
            Return CType(ViewState("CGID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CGID") = Value
        End Set
    End Property

    Private Property NextDueDate() As String
        Get
            Return CType(ViewState("NextDueDate"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("NextDueDate") = Value
        End Set
    End Property

    Private Property AgreementNo() As String
        Get
            Return CType(ViewState("AgreementNo"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("AgreementNo") = Value
        End Set
    End Property
    Private Property CustomerID() As String
        Get
            Return CType(ViewState("CustomerID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property

    Private Property CustomerName() As String
        Get
            Return CType(ViewState("CustomerName"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerName") = Value
        End Set
    End Property
    Private Property SearchBy2() As String
        Get
            Return CType(ViewState("SearchBy2"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("SearchBy2") = Value
        End Set
    End Property
    Private Property SelectedCG() As String
        Get
            Return CType(ViewState("SelectedCG"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("SelectedCG") = Value
        End Set
    End Property

    Private Property CollectorID() As String
        Get
            Return CType(ViewState("CollectorID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CollectorID") = Value
        End Set
    End Property
    Private Property SearchByText() As String
        Get
            Return CType(ViewState("SearchByText"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("SearchByText") = Value
        End Set
    End Property
#End Region
#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.CLActivity
    Private oController As New CLActivityController
    Private SearchCond As String
    Private Collector As String


#End Region

#Region "PageLoad"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If
        Me.FormID = "DCRPLAN"
        If Not IsPostBack Then
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then

                txtPlanDate.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
                Dim oDataTable As New DataTable
                Me.ApplicationID = Request.QueryString("ApplicationId")
                If Request.QueryString("WhereCond1") <> "" Then
                    Me.SearchBy = Request.QueryString("WhereCond1")
                End If
                If Request.QueryString("WhereCond2") <> "" Then
                    Me.SearchBy2 = Request.QueryString("WhereCond2")
                End If
                If Request.QueryString("SelectedCG") <> "" Then
                    Me.SelectedCG = Request.QueryString("SelectedCG")
                End If
                If Request.QueryString("CollectorID") <> "" Then
                    Me.CollectorID = Request.QueryString("CollectorID")
                End If
                If Request.QueryString("SearchByText") <> "" Then
                    Me.SearchByText = Request.QueryString("SearchByText")
                End If

                With oCustomClass
                    .strConnection = GetConnectionString()
                    .ApplicationID = Me.ApplicationID.Trim
                End With
                oCustomClass = oController.CLActivityDataAgreement(oCustomClass)
                oDataTable = oCustomClass.ListCLActivity
                FillData(oDataTable)
            End If
        End If
    End Sub
#End Region

#Region "FillData"
    Private Sub FillData(ByVal oDataTable As DataTable)
        Dim oRow As DataRow
        Dim inInstallment As String
        Dim oResult As New DataTable
        Dim dtAction As New DataTable
        If oDataTable.Rows.Count > 0 Then

            cboPlanActivity.ClearSelection()
            txtPlanDate.Text = ""
            txtHour.Text = CStr(Hour(Now()))
            txtMinute.Text = CStr(Minute(Now))
            oRow = oDataTable.Rows(0)
            ucCollActResult.hypAgreementNo.Text = CType(oRow("AgreementNo"), String)
            ucCollActResult.hypCustomerName.Text = CType(oRow("Name"), String)
            hdnAgreement.Value = CType(oRow("AgreementNo"), String)
            hdnCustName.Value = CType(oRow("Name"), String)
            hdnCustID.Value = CType(oRow("CustomerID"), String)

            ucCollActResult.hypAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "Collection" & "', '" & Me.ApplicationID & "')"
            ucCollActResult.hypCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "Collection" & "', '" & Me.CustomerID & "')"

            If Not IsDBNull(oRow("Address")) Then
                ucCollActResult.lblAddress.Text = CType(oRow("Address"), String)
            End If
            If Not IsDBNull(oRow("Description")) Then
                ucCollActResult.lblAsset.Text = CType(oRow("Description"), String)
            End If
            If Not IsDBNull(oRow("LicensePlate")) Then
                ucCollActResult.lblLicenseNo.Text = CType(oRow("LicensePlate"), String)
            End If
            If Not IsDBNull(oRow("EmployeeName")) Then
                ucCollActResult.lblCMONo.Text = CType(oRow("EmployeeName"), String)
            End If
            'If Not IsDBNull(oRow("aospv")) Then
            '    lblCMOSPVNo.Text = CType(oRow("aospv"), String)
            'End If
            If CType(oRow("LegalPhoneYN"), String) = "Y" Then
                ucCollActResult.lblLegalPhone1.ForeColor = System.Drawing.Color.Blue
                ucCollActResult.lblLegalPhone2.ForeColor = System.Drawing.Color.Blue
            End If
            If Not IsDBNull(oRow("LegalPhone1")) Then
                ucCollActResult.lblLegalPhone1.Text = CType(oRow("LegalPhone1"), String)
            End If
            If Not IsDBNull(oRow("LegalPhone2")) Then
                ucCollActResult.lblLegalPhone2.Text = CType(oRow("LegalPhone2"), String)
            End If
            If CType(oRow("HomePhoneYN"), String) = "Y" Then
                ucCollActResult.lblResidencePhone1.ForeColor = System.Drawing.Color.Blue
                ucCollActResult.lblResidencePhone2.ForeColor = System.Drawing.Color.Blue
            End If
            If Not IsDBNull(oRow("ResidencePhone1")) Then
                ucCollActResult.lblResidencePhone1.Text = CType(oRow("ResidencePhone1"), String)
            End If
            If Not IsDBNull(oRow("ResidencePhone2")) Then
                ucCollActResult.lblResidencePhone2.Text = CType(oRow("ResidencePhone2"), String)
            End If
            If CType(oRow("CompanyPhoneYN"), String) = "Y" Then
                ucCollActResult.lblCompanyPhone1.ForeColor = System.Drawing.Color.Blue
                ucCollActResult.lblCompanyPhone2.ForeColor = System.Drawing.Color.Blue
            End If
            If Not IsDBNull(oRow("CompanyPhone1")) Then
                ucCollActResult.lblCompanyPhone1.Text = CType(oRow("CompanyPhone1"), String)
            End If
            If Not IsDBNull(oRow("CompanyPhone2")) Then
                ucCollActResult.lblCompanyPhone2.Text = CType(oRow("CompanyPhone2"), String)
            End If
            If Not IsDBNull(oRow("MobilePhone")) Then
                ucCollActResult.lblMobilePhone.Text = CType(oRow("MobilePhone"), String)
            End If
            If CType(oRow("MailingPhoneYN"), String) = "Y" Then
                ucCollActResult.lblMailingPhone1.ForeColor = System.Drawing.Color.Blue
                ucCollActResult.lblMailingPhone2.ForeColor = System.Drawing.Color.Blue
            End If
            If Not IsDBNull(oRow("MailingPhone1")) Then
                ucCollActResult.lblMailingPhone1.Text = CType(oRow("MailingPhone1"), String)
            End If
            If Not IsDBNull(oRow("MailingPhone2")) Then
                ucCollActResult.lblMailingPhone2.Text = CType(oRow("MailingPhone2"), String)
            End If
            If Not IsDBNull(oRow("SIPhone1")) Then
                ucCollActResult.lblSuamiIstriPhone1.Text = CType(oRow("SIPhone1"), String)
            End If
            If Not IsDBNull(oRow("SIPhone2")) Then
                ucCollActResult.lblSuamiIstriPhone2.Text = CType(oRow("SIPhone2"), String)
            End If


            'If Not IsDBNull(oRow("isSMSSent")) Then
            '    lblisSMS.Text = CType(oRow("isSMSSent"), String)
            'End If
            'If Not IsDBNull(oRow("LastPastDueDays")) Then
            '    lblPreviousOverDue.Text = CType(oRow("LastPastDueDays"), String)
            'End If
            'If Not IsDBNull(oRow("NextInstallmentDate")) Then
            '    lblDueDate.Text = Format(CDate(oRow("NextInstallmentDate")), "dd/MM/yyyy")
            'End If
            'If Not IsDBNull(oRow("StatusPDC")) Then
            '    lblPDCRequest.Text = CType(oRow("StatusPDC"), String)
            'End If
            If Not IsDBNull(oRow("EndPastDueDays")) Then
                ucCollActResult.lblOverDuedays.Text = CType(oRow("EndPastDueDays"), String)
            End If
            'If Not IsDBNull(oRow("NextInstallmentNumber")) Then
            '    lblInstallmentNo.Text = CType(oRow("NextInstallmentNumber"), String)
            'End If
            If Not IsDBNull(oRow("CollectorID")) Then
                hdnCollector.Value = CType(oRow("CollectorID"), String)
            End If
            If Not IsDBNull(oRow("InstallmentAmount")) Then
                lblInstallmentAmt.Text = FormatNumber(oRow("InstallmentAmount"), 2)
            End If
            If Not IsDBNull(oRow("EndPastDueAmt")) Then
                lblOverDueAmt.Text = FormatNumber(oRow("EndPastDueAmt"), 2)
            End If
            If Not IsDBNull(oRow("OSLCInstallment")) Then
                lblOSInstallmentAmt.Text = FormatNumber(oRow("OSLCInstallment"), 2)
            End If
            If Not IsDBNull(oRow("OSBillingCharges")) Then
                lblOSBillingCharge.Text = FormatNumber(oRow("OSBillingCharges"), 2)
            End If
            If Not IsDBNull(oRow("OSBalance")) Then
                lblOSBalance.Text = FormatNumber(oRow("OSBalance"), 2)
            End If
            If Not IsDBNull(oRow("OSLCInsurance")) Then
                lblOSInsurance.Text = FormatNumber(oRow("OSLCInsurance"), 2)
            End If
            If Not IsDBNull(oRow("OSPDCBounceFee")) Then
                lblPDCBounceFee.Text = FormatNumber(oRow("OSPDCBounceFee"), 2)
            End If

            If CDbl(lblOverDueAmt.Text) <= 0 Then
                ShowMessage(lblMessage, "Kontrak ini tidak Telat", True)
            End If
            LblPreviousPlanActivity.Text = CType(oRow("PlanActivity"), String)

            Me.ValidDueDate = CType(oRow("ValidDueDate"), String)
            Me.ApplicationID = CType(oRow("ApplicationID"), String)
            Me.NextDueDate = Format(CDate(oRow("NextInstallmentDueDate")), "dd/MM/yyyy")


            ucCollActResult.ltlWayOfPayment.Text = CStr(IIf(IsDBNull(oRow("WayOfPayment")), "", oRow("WayOfPayment")))
            ucCollActResult.lblCollector.Text = CStr(IIf(IsDBNull(oRow("Collector")), "", oRow("Collector")))
            ucCollActResult.DoBindInstallmentSchedule(Me.ApplicationID, Me.BusinessDate)

        Else
            cboPlanActivity.ClearSelection()
            txtPlanDate.Text = ""
            txtHour.Text = CStr(Hour(Now()))
            txtMinute.Text = CStr(Minute(Now))
            ucCollActResult.ClearVariable()
            'hypAgreementNo.Text = ""
            'hypCustomerName.Text = ""
            hdnAgreement.Value = ""
            hdnCustName.Value = ""
            hdnCustID.Value = ""
            'lblAddress.Text = ""
            'lblAsset.Text = ""
            'lblLicenseNo.Text = ""
            'lblCMOName.Text = ""
            'lblLegalPhone1.Text = ""
            'lblLegalPhone2.Text = ""
            'lblResidencePhone1.Text = ""
            'lblResidencePhone2.Text = ""
            'lblCompanyPhone1.Text = ""
            'lblCompanyPhone2.Text = ""
            'lblMobilePhone.Text = ""
            'lblMailingPhone1.Text = ""
            'lblMailingPhone2.Text = ""
            'lblisSMS.Text = ""
            'lblPreviousOverDue.Text = ""
            'lblDueDate.Text = ""
            'lblPDCRequest.Text = ""
            'lblOverDueDays.Text = ""
            'lblInstallmentNo.Text = ""
            'lblCollector.Text = ""
            lblInstallmentAmt.Text = ""
            lblOverDueAmt.Text = ""
            lblOSInstallmentAmt.Text = ""
            lblOSBillingCharge.Text = ""
            lblOSBalance.Text = ""
            lblOSInsurance.Text = ""
            lblPDCBounceFee.Text = ""
            Me.ValidDueDate = ""
            Me.ApplicationID = ""
            Me.NextDueDate = ""

        End If
        With oCustomClass
            .strConnection = GetConnectionString()
            .strKey = ""
        End With
        oCustomClass = oController.CLActivityActionResult(oCustomClass)
        dtAction = oCustomClass.ListCLActivity

        With cboPlanActivity
            .DataSource = dtAction
            .DataTextField = "ActionDescription"
            .DataValueField = "ActionID"
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = "0"
        End With
    End Sub

#End Region

#Region "Cancel"
    Private Sub ImbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        'Response.Redirect("remidialplan.aspx?WhereCond1=" & Me.SearchBy.Trim & "&WhereCond2=" & Me.SearchBy2.Trim & "&CGID=" & Me.cgid & "&collector=" & collector.trim & "&SearchBy=" & SearchCond.trim & "")
        Response.Redirect("RemidialPlanList.aspx?WhereCond1=" & Me.SearchBy.Trim & "&WhereCond2=" & Me.SearchBy2.Trim & "")
        'Response.Redirect("DCRPlanList.aspx")
        'Response.Redirect("DCRPlanList.aspx?WhereCond1=" & Me.SearchBy.Trim & "&WhereCond2=" & Me.SearchBy2.Trim)
    End Sub
#End Region

#Region "Save"
    Private Sub BtnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
        If CheckFeature(Me.Loginid, Me.FormID, "SAVE", Me.AppId) Then
            Dim oDate As New Maxiloan.Webform.WebBased
            Dim oDataTable As New DataTable
            With oCustomClass
                '           @branchid  Varchar(3),
                '@Applicationid varchar(20),
                '@PlanDate datetime,
                '@PlanActivity varchar(20),
                '@Businessdate datetime
                If DateDiff(DateInterval.Day, Me.BusinessDate, ConvertDate2(txtPlanDate.Text)) < 0 Then
                    ShowMessage(lblMessage, "Tanggal Rencana harus >= Tgl hari ini", True)
                    Exit Sub
                End If
                .strConnection = GetConnectionString()
                .ApplicationID = Me.ApplicationID
                .AgreementNo = ucCollActResult.hypAgreementNo.Text.Trim
                .BranchId = Me.sesBranchId.Replace("'", "")
                If txtPlanDate.Text <> "" Then
                    Context.Trace.Write("Kondisi 10")
                    .PlanDate = oDate.ConvertDate(txtPlanDate.Text) & " " & txtHour.Text.Trim & ":" & txtMinute.Text.Trim
                Else
                    Context.Trace.Write("Kondisi 11")
                    .PlanDate = Nothing
                End If
                If cboPlanActivity.SelectedItem.Value = "0" Then
                    .PlanActivityID = Nothing
                Else
                    .PlanActivityID = cboPlanActivity.SelectedItem.Value.Trim
                End If
                .BusinessDate = Me.BusinessDate


                Try
                    oController.DCRSave(oCustomClass)
                    With oCustomClass
                        .strConnection = GetConnectionString()
                        .ApplicationID = Me.ApplicationID.Trim
                    End With
                    oCustomClass = oController.CLActivityDataAgreement(oCustomClass)
                    ShowMessage(lblMessage, "Simpan data Berhasil", False)
                    'oDataTable = oCustomClass.ListCLActivity
                    'FillData(oDataTable)
                    'BtnSave.Visible = False
                    'Server.Transfer("DCRPlanList.aspx?message=" & MessageHelper.MESSAGE_INSERT_SUCCESS)
                    Response.Redirect("RemidialPlanList.aspx?Message=" & lblMessage.Text.Trim & "&WhereCond1=" & Me.SearchBy.Trim & "&WhereCond2=" & Me.SearchBy2.Trim & "&SelectedCG=" & Me.SelectedCG & "&CollectorID=" & Me.CollectorID & "&SearchByText=" & Me.SearchByText.Trim & "")
                Catch exp As Exception
                    ShowMessage(lblMessage, exp.Message, True)
                End Try

            End With
        End If

    End Sub

#End Region

End Class