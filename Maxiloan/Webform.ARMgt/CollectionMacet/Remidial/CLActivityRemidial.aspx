﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CLActivityRemidial.aspx.vb" Inherits="Maxiloan.Webform.ARMgt.CLActivityRemidial" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ucAction" Src="../../../Webform.UserController/ucAction.ascx" %>
<%@ Register TagPrefix="uc1" TagName="CollActResult" Src="../../../Webform.UserController/ucCollectionActResult.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>CLActivity</title>
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
    <script src="../../../js/jquery-1.9.1.min.js"></script>
    <script src="../../../js/jquery-ui-1.10.3.custom.min.js"></script>
    <link rel="Stylesheet" href="../../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../../Include/General.css" type="text/css" />

    <script language="javascript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;
        var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%#Request.servervariables("SERVER_NAME")%>/';
        var hdnDetail;
        var hdndetailvalue;
        var cboBankAccount = null;
        function ParentChange(pCmbOfPayment, pBankAccount, pHdnDetail, pHdnDetailValue, itemArray) {
            hdnDetail = eval('document.forms[0].' + pHdnDetail);
            HdnDetailValue = eval('document.forms[0].' + pHdnDetailValue);
            var i, j;
            for (i = eval('document.forms[0].' + pBankAccount).options.length; i >= 0; i--) {
                eval('document.forms[0].' + pBankAccount).options[i] = null

            };
            if (itemArray == null) {
                j = 0;
            }
            else {
                j = 2;
            };
            eval('document.forms[0].' + pBankAccount).options[0] = new Option('Select One', '0');
            eval('document.forms[0].' + pBankAccount).options[1] = new Option('ALL', 'ALL');
            if (itemArray != null) {
                for (i = 0; i < itemArray.length; i++) {
                    eval('document.forms[0].' + pBankAccount).options[j++] = new Option(itemArray[i][0], itemArray[i][1]);

                };
                eval('document.forms[0].' + pBankAccount).selected = true;
            }
        };

        function cboChildonChange(selIndex, l, j) {
            hdnDetail.value = l;
            HdnDetailValue.value = j;
            eval('document.forms[0].hdnBankAccount').value = selIndex;
        };
        function RestoreInsuranceCoIndex(BankAccountIndex) {
            cboChild = eval('document.forms[0].cboChild');
            if (cboChild != null)
                if (eval('document.forms[0].hdnBankAccount').value != null) {
                    if (BankAccountIndex != null)
                        cboChild.selectedIndex = BankAccountIndex;
                }
            };


          

            $(document).ready(function () {
                var _ActionHasil = $("#oAction_cboChild").val();
                if (_ActionHasil.match(/^PTPY.*$/)) {
                    $('#txtPromiseDate').removeAttr('disabled');
                    $('#cboPlanActivity').attr('disabled', 'disabled');
                    $('#txtPlanDate').attr('disabled', 'disabled');
                    $('#txtHour').attr('disabled', 'disabled');
                    $('#txtMinute').attr('disabled', 'disabled');                    
//                    document.getElementById("cboPlanActivity").value = ""
//                    document.getElementById("txtPlanDate").value = ""
//                    document.getElementById("txtHour").value = ""
//                    document.getElementById("txtMinute").value = ""
                }
                else {
                    $('#txtPromiseDate').attr('disabled', 'disabled');
                    $('#cboPlanActivity').removeAttr('disabled');
                    $('#txtPlanDate').removeAttr('disabled');
                    $('#txtHour').removeAttr('disabled');
                    $('#txtMinute').removeAttr('disabled');
                    document.getElementById("txtPromiseDate").value = ""
                }
            }); 
              
    </script>
    <script language="javascript" type="text/javascript">
        function OpenActivity() {
            window.open('../Inquiry/InqCollActivityResult.aspx?AgreementNo=' + hdnAgreement.value + '&CustomerName=' + hdnCustName.value + '&CustomerID=' + hdnCustID.value + '&Style=Collection&Referrer=Activity', 'UserLookup', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenCollection() {
            window.open('../Inquiry/InqCollActivityHistory.aspx?AgreementNo=' + hdnAgreement.value + '&CustomerName=' + hdnCustName.value + '&CustomerID=' + hdnCustID.value + '', 'UserLookup', 'left=0, top=0, width=' + x + ', height = ' + y + ', scrollbars=yes');
        }
    </script>
   
</head>
<body>
    <input id="hdnAgreement" type="hidden" name="hdnAgreement" runat="server" />
    <input id="hdnCustName" type="hidden" name="hdnCustName" runat="server" />
    <input id="hdnCustID" type="hidden" name="hdnCustID" runat="server" />
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>    
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>
                        AKTIVITAS COLLECTION LANCAR
                    </h3>
                </div>
            </div>
            <asp:Panel ID="pnlsearch" runat="server">
                <input id="hdnChildValue" type="hidden" name="hdnSP" runat="server" />
                <input id="hdnChildName" type="hidden" name="hdnSP" runat="server" />
                <input id="hdnBankAccount" type="hidden" name="hdSP" runat="server" />
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Collection Group</label>
                        <asp:DropDownList ID="cboParent" runat="server" onchange="<%#CollectionGroupIDChange()%>">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator CssClass="validator_general" ID="RequiredFieldValidator4"
                            runat="server" ErrorMessage="Harap pilih collection group!" Display="Dynamic"
                            ControlToValidate="cboParent" InitialValue="0"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Collector
                        </label>
                        <asp:DropDownList ID="cboChild" runat="server" onchange="cboChildonChange(this.selectedIndex,this.options[this.selectedIndex].value,this.options[this.selectedIndex].text);">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator CssClass="validator_general" ID="rfvCollector" runat="server"
                            ErrorMessage="Harap pilih Collector!" Display="Dynamic" ControlToValidate="cboChild"
                            InitialValue="0"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Cari Berdasarkan</label>
                        <asp:DropDownList ID="cboSearchBy" runat="server">
                            <asp:ListItem>Select One</asp:ListItem>
                            <asp:ListItem Value="Agreement.AgreementNo">No Kontrak</asp:ListItem>
                            <asp:ListItem Value="Customer.Name">Nama Customer</asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtSearchBy" runat="server"></asp:TextBox>
                    </div>
                </div>
                <%--<div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Cetak Tanggal Rencana</label>
                        <asp:TextBox ID="txtoPlanDate" runat="server"></asp:TextBox>
                        <asp:CalendarExtender ID="txtoPlanDate_CalendarExtender" runat="server" Enabled="True"
                            TargetControlID="txtoPlanDate" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                        <asp:RequiredFieldValidator CssClass="validator_general" ID="RequiredFieldValidator1"
                            runat="server" ErrorMessage="Harap isi tanggal rencana!" ControlToValidate="txtoPlanDate"></asp:RequiredFieldValidator>
                    </div>
                </div>--%>
                <div class="form_button">
                    <asp:Button ID="Buttonsearch" runat="server" Text="Search" CssClass="small button blue">
                    </asp:Button>&nbsp;
                    <asp:Button ID="ButtonReset" runat="server" Text="Reset" CssClass="small button gray"
                        CausesValidation="False"></asp:Button></div>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlDtGrid" runat="server">
                <div class="form_box_title">
                    <div class="form_single">
                        <h4>
                            DAFTAR AKTIVITAS COLLECTION LANCAR
                        </h4>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtgCLActivity" runat="server" Width="100%" AllowSorting="True"
                                AutoGenerateColumns="False" BorderStyle="None" BorderWidth="0" OnSortCommand="SortGrid"
                                CssClass="grid_general">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="HASIL">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbResult" runat="server" ImageUrl="../../../images/IconReceived.gif"
                                                CommandName="result"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hypAgreementNodtg" runat="server" Text='<%# Container.Dataitem("AgreementNo")%>'>
                                            </asp:HyperLink>
                                            <asp:Label ID="hypApplicationID" Visible="False" runat="server" Text='<%# Container.Dataitem("ApplicationID")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="CustomerName" HeaderText="NAMA CUSTOMER">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hypCustomerNamedtg" runat="server" Text='<%# Container.Dataitem("CustomerName")%>'>
                                            </asp:HyperLink>
                                            <asp:Label ID="lblCustomerId" runat="server" Text='<%#Container.DataItem("CustomerID")%>'
                                                Visible="false">
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="Days" SortExpression="Days" HeaderText="DAYS">                                
                                    </asp:BoundColumn>
                                    <asp:TemplateColumn SortExpression="LegalPhone1" HeaderText="NO TELP ALM KTP">
                                        <ItemTemplate>
                                            <asp:Label ID="lblLegalPhone" runat="server" Text='<%#Container.DataItem("LegalPhone1")%>'
                                                ForeColor='<%# getColor(Container.DataItem("LegalPhoneYN"))%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="ResidencePhone1" HeaderText="No TELEPON RUMAH">
                                        <ItemTemplate>
                                            <asp:Label ID="lblResidencePhone" runat="server" Text='<%#Container.DataItem("ResidencePhone1")%>'
                                                ForeColor='<%# getColor(Container.DataItem("HomePhoneYN"))%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="CompanyPhone1" HeaderText="NO TELEPON KANTOR">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCompanyPhone" runat="server" Text='<%#Container.DataItem("CompanyPhone1")%>'
                                                ForeColor='<%# getColor(Container.DataItem("CompanyPhoneYN"))%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="MobilePhone" SortExpression="MobilePhone" HeaderText="NO HANDPHONE">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="PDCEmptyStatus" SortExpression="PDCEmptyStatus" HeaderText="PDC">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="NextInstallmentDuedate" SortExpression="NextInstallmentDuedate"
                                        HeaderText="TGL JATUH TEMPO" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
                                    <asp:TemplateColumn Visible="False" SortExpression="ApplicationId" HeaderText="ApplicationId">
                                        <ItemTemplate>
                                            <asp:Label ID="lblApplicationId" runat="server" Text='<%# Container.Dataitem("ApplicationId")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                            <div class="button_gridnavigation">
                                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../../Images/grid_navbutton01.png"
                                    CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../../Images/grid_navbutton02.png"
                                    CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../../Images/grid_navbutton03.png"
                                    CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../../Images/grid_navbutton04.png"
                                    CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                                    EnableViewState="False"></asp:Button>
                                <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" MinimumValue="1"
                                    ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"
                                    Display="Dynamic"></asp:RangeValidator>
                                <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                                    ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general">
                                </asp:RequiredFieldValidator>
                            </div>
                            <div class="label_gridnavigation">
                                <asp:Label ID="lblPage" runat="server"></asp:Label>of
                                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlResult" runat="server">
                <div class="form_box_title">
                    <div class="form_single">
                        <h4>
                            AKTIVITAS DAN HASIL
                        </h4>
                    </div>
                </div>
                <asp:HiddenField  ID="hdnCollector" runat="server" />
                   <uc1:CollActResult id="ucCollActResult1" runat="server" ></uc1:CollActResult>
              <%--  <div class="form_box">
                    <div class="form_single">
                        <label>
                            No Kontrak</label>
                        <asp:HyperLink ID="hypAgreementNo" runat="server"></asp:HyperLink>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Nama Customer</label>
                        <asp:HyperLink ID="hypCustomerName" runat="server"></asp:HyperLink>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Alamat</label>
                        <asp:Label ID="lblAddress" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Asset</label>
                        <asp:Label ID="lblAsset" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            No Polisi</label>
                        <asp:Label ID="lblLicenseNo" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Nama CMO</label>
                        <asp:Label ID="lblCMOName" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Supervisor CMO</label>
                        <asp:Label ID="lblCMOSPVNo" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Collector</label>
                        <asp:Label ID="lblCollector" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            No Telepon Alamat KTP</label>
                        <asp:Label ID="lblLegalPhone1" runat="server"></asp:Label>
                        <asp:Label ID="lblLegalPhone2" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            No Telepon Tempat Tinggal</label>
                        <asp:Label ID="lblResidencePhone1" runat="server"></asp:Label>
                        <asp:Label ID="lblResidencePhone2" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            No Telepon Kantor</label>
                        <asp:Label ID="lblCompanyPhone1" runat="server"></asp:Label>
                        <asp:Label ID="lblCompanyPhone2" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            No HandPhone</label>
                        <asp:Label ID="lblMobilePhone" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            No Telepon Alamat Tagih</label>
                        <asp:Label ID="lblMailingPhone1" runat="server"></asp:Label>
                        <asp:Label ID="lblMailingPhone2" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Status Kirim SMS</label>
                        <asp:Label ID="lblisSMS" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            PDC</label>
                        <asp:Label ID="lblPDCRequest" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Jumlah Hari tidak Telat</label>
                        <asp:Label ID="lblPreviousOverDue" runat="server" Visible="False"></asp:Label>&nbsp;days
                    </div>
                    <div class="form_right">
                        <label>
                            Jumlah Hari Telat</label>
                        <asp:Label ID="lblOverDueDays" runat="server"></asp:Label>&nbsp;days
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Tanggal jatuh Tempo</label>
                        <asp:Label ID="lblDueDate" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Angsuran Ke</label>
                        <asp:Label ID="lblInstallmentNo" runat="server"></asp:Label>
                    </div>
                </div>--%>
                <div class="form_box_header">
                    <div class="form_single">
                        <h4>
                            DATA FINANCIAL
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Jumlah Angsuran</label>
                        <asp:Label ID="lblInstallmentAmt" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Sisa Saldo</label>
                        <asp:Label ID="lblOSBalance" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Jumlah Tunggakan</label>
                        <asp:Label ID="lblOverDueAmt" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Denda Keterlambatan Angsuran</label>
                        <asp:Label ID="lblOSInstallmentAmt" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Denda Keterlambatan Asuransi</label>
                        <asp:Label ID="lblOSInsurance" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Biaya Tagih</label>
                        <asp:Label ID="lblOSBillingCharge" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Biaya Tolakan PDC</label>
                        <asp:Label ID="lblPDCBounceFee" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <h4>
                            AKTIVITAS dan HASIL
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Rencana Aktivitas Sebelumnya</label>
                        <asp:Label ID="LblPreviousPlanActivity" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            <%--Tgl Input Hasil Kunjungan </label>--%>
                            Tgl Aktivitas </label>
                        <asp:TextBox ID="txtInputDate" runat="server"></asp:TextBox>
                        <asp:CalendarExtender ID="txtInputDate_CalendarExtender" runat="server" Enabled="True"
                            TargetControlID="txtInputDate" Format="dd/MM/yyyy"></asp:CalendarExtender>
                    </div>
                </div>
                <div class="form_box_uc" id="divoAction">
                    <uc1:ucaction id="oAction1" runat="server" ></uc1:ucaction>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Tanggal Janji Bayar</label>
                        <asp:TextBox ID="txtPromiseDate" runat="server"></asp:TextBox>
                        <asp:CalendarExtender ID="txtPromiseDate_CalendarExtender" runat="server" Enabled="True"
                            TargetControlID="txtPromiseDate" Format="dd/MM/yyyy"></asp:CalendarExtender>
                        <%--<asp:RequiredFieldValidator CssClass="validator_general" ID="RequiredFieldValidator2"
                            runat="server" ErrorMessage="*" ControlToValidate="txtPromiseDate"></asp:RequiredFieldValidator>--%>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Rencana Aktivitas</label>
                        <asp:DropDownList ID="cboPlanActivity" runat="server">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Tanggal Rencana</label>
                        <asp:TextBox ID="txtPlanDate" runat="server"></asp:TextBox>
                        <asp:CalendarExtender ID="txtPlanDate_CalendarExtender" runat="server" Enabled="True"
                            TargetControlID="txtPlanDate" Format="dd/MM/yyyy"></asp:CalendarExtender>
                        <%--<asp:RequiredFieldValidator CssClass="validator_general" ID="RequiredFieldValidator3"
                            runat="server" ErrorMessage="*" ControlToValidate="txtPlanDate"></asp:RequiredFieldValidator>--%>
                        <label class="label_auto">
                            Hour</label>
                        <asp:TextBox ID="txtHour" runat="server" Width="36px" MaxLength="2"></asp:TextBox>&nbsp;
                        <asp:RangeValidator CssClass="validator_general" ID="rgvHour" runat="server" ErrorMessage="*"
                            Display="Dynamic" ControlToValidate="txtHour" Enabled="False" Type="Integer"
                            MinimumValue="0" MaximumValue="24"></asp:RangeValidator>&nbsp; :&nbsp;&nbsp;&nbsp;
                        <asp:TextBox ID="txtMinute" runat="server" Width="36px" MaxLength="2"></asp:TextBox>&nbsp;
                        <asp:RangeValidator CssClass="validator_general" ID="rgvMinute" runat="server" ErrorMessage="*"
                            Display="Dynamic" ControlToValidate="txtMinute" Enabled="False" Type="Integer"
                            MinimumValue="0" MaximumValue="60"></asp:RangeValidator>24-Hours
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Perlu Perhatian Supervisor</label>
                        <asp:CheckBox ID="chkRequestSpv" runat="server"></asp:CheckBox>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_general">
                            Catatan</label>
                        <asp:TextBox ID="txtNotes" runat="server" CssClass="multiline_textbox" TextMode="MultiLine"></asp:TextBox>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Telepon Berhasil dihubungi</label>
                        <asp:DropDownList ID="cboSuccess" runat="server">
                            <asp:ListItem Value="0">Select One</asp:ListItem>
                            <asp:ListItem Value="LegalPhone">No Telepon Alamat KTP</asp:ListItem>
                            <asp:ListItem Value="ResidencePhone">No Telepon Tempat Tinggal</asp:ListItem>
                            <asp:ListItem Value="CompanyPhone">No Telepon Kantor</asp:ListItem>
                            <asp:ListItem Value="MailingPhone">No Telepon Alamat Tagih</asp:ListItem>
                            <asp:ListItem Value="MobilePhone">No HandPhone</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <%--<div class="form_box">
                    <div class="form_single">
                        <label>
                            <a href="javascript:OpenActivity()">
                                <asp:Label ID="lblActivityHistory" runat="server" Visible="True">HISTORY AKTIVITAS</asp:Label></a></label>
                        <label>
                            <b><a href="javascript:OpenCollection()">
                                <asp:Label ID="lblCollectionHistory" runat="server" Visible="True">HISTORY COLLECTION</asp:Label></a></b></div>
                    </label>
                </div>
                </div>--%>
                <div class="form_button">
                    <asp:Button ID="ButtonSave" runat="server" Text="Save" CssClass="small button blue">
                    </asp:Button>&nbsp;
                    <asp:Button ID="ButtonCancel" runat="server" CausesValidation="False" Text="Cancel"
                        CssClass="small button gray"></asp:Button>&nbsp;<a href="javascript:history.go(-1);"></a>
                </div>
            </asp:Panel>
            <div id="divInnerHTML" runat="server">
            </div>        
    </form>
</body>
</html>
