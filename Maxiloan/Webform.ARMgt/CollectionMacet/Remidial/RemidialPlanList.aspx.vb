﻿#Region "Imports"
Imports System.Data.SqlClient
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class RemidialPlanList
    Inherits Maxiloan.Webform.WebBased

#Region "Property "
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property FilterBy() As String
        Get
            Return CType(ViewState("FilterBy"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("FilterBy") = Value
        End Set
    End Property

    Private Property SearchBy2() As String
        Get
            Return CType(ViewState("SearchBy2"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("SearchBy2") = Value
        End Set
    End Property
    Public Property strAll() As String
        Get
            Return CType(ViewState("strAll"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("strAll") = Value
        End Set
    End Property
    Private Property SelectedCG() As String
        Get
            Return CType(ViewState("SelectedCG"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("SelectedCG") = Value
        End Set
    End Property

    Private Property CollectorID() As String
        Get
            Return CType(ViewState("CollectorID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CollectorID") = Value
        End Set
    End Property
    Private Property SearchByText() As String
        Get
            Return CType(ViewState("SearchByText"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("SearchByText") = Value
        End Set
    End Property
#End Region

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private oController1 As New CLActivityController
    Private oCustomClass As New Parameter.AgreementList
    Private oController As New AgreementListController
    Private m_CollZipCode As New CollZipCodeController
#End Region

#Region "PageLoad"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        'Put user code to initialize the page here
        If SessionInvalid() Then
            Exit Sub
        End If
        Me.FormID = "RMDPLAN"
        If Not IsPostBack Then
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If Request.QueryString("message") <> "" Then
                    ShowMessage(lblMessage, Request.QueryString("message"), True)
                End If

                pnlDtGrid.Visible = False

                pnlsearch.Visible = True
                'oCLActivity.strKey = "CL"
                If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                    Me.SearchBy = ""
                    Me.SortBy = ""

                    txtPlanDate.Text = Format(Me.BusinessDate, "dd/MM/yyyy")
                    txtPlanDateTo.Text = Format(Me.BusinessDate, "dd/MM/yyyy")

                    '-------------gantinya usercontrol------------------
                    Dim dtParent As New DataTable
                    Dim Coll As New Parameter.RptCollAct
                    Dim ControllerCG As New RptCollActController
                    'Dim CollList As New Parameter.Collector
                    'RequiredFieldValidator1.Enabled = False
                    If Me.strAll = "1" Then
                        With Coll
                            .strConnection = GetConnectionString()
                            .CGID = Me.GroubDbID
                            .strKey = "CG_ALL"
                            .CollectorType = ""
                        End With
                    Else
                        With Coll
                            .strConnection = GetConnectionString()
                            .CGID = Me.GroubDbID
                            .strKey = "CG"
                            .CollectorType = ""
                        End With
                    End If
                    Coll = ControllerCG.ViewDataCollector(Coll)
                    dtParent = Coll.ListCollector

                    cboParent.DataTextField = "CGName"
                    cboParent.DataValueField = "CGID"
                    cboParent.DataSource = dtParent
                    cboParent.DataBind()
                    cboParent.Items.Insert(0, "Select One")
                    cboParent.Items(0).Value = "0"
                    'cboParent.SelectedIndex = 0
                    'If dtParent.Rows.Count = 1 Then
                    '    CollectionGroupIDChange()
                    '    cboParent.Items.FindByValue(Me.GroubDbID).Selected = True
                    'End If

                    'BindChild()
                    'getchildcombo()
                    BindCollector()

                    If Request.QueryString("WhereCond1") <> "" Then
                        pnlsearch.Visible = False
                        PnlSearchDetail.Visible = True
                        pnlDtGrid.Visible = True
                        Me.SearchBy = Request.QueryString("WhereCond1")
                        Me.SearchBy2 = Request.QueryString("WhereCond2")
                        DoBind(Me.SearchBy, Me.SearchBy2, Me.SortBy)
                        LblCollector.Text = Request.QueryString("CollectorID")
                        lblCG.Text = Request.QueryString("SelectedCG")
                        ShowMessage(lblMessage, Request.QueryString("Message"), True)
                        lblSearchBy.Text = Request.QueryString("SearchByText")
                    Else
                        ClearSearchDetail()
                        PnlSearchDetail.Visible = False
                    End If


                End If
            End If
        End If
    End Sub
#End Region

    Private Sub BindCollector()

        Dim oCollZipCode As New Parameter.CollZipCode
        Dim strconn As String = GetConnectionString()
        Dim dt As New DataTable

        With oCollZipCode
            .strConnection = GetConnectionString()
            .CGID = Me.GroubDbID
            .CollectorType = "CL"
        End With

        dt = m_CollZipCode.GetCollectorCombo(oCollZipCode)

        cboChild.DataSource = dt
        cboChild.DataTextField = "CollectorName"
        cboChild.DataValueField = "CollectorID"
        cboChild.DataBind()
        cboChild.Items.Insert(0, "Select One")
        cboChild.Items(0).Value = "0"

    End Sub

#Region "DoBind"
    Sub DoBind(ByVal cmdWhere As String, ByVal cmdwhere2 As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        Dim intloop As Integer
        Dim hypID As HyperLink
        Dim oPaging As New Parameter.CLActivity
        Dim m_controller As New CLActivityController

        With oPaging
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .WhereCondForCA = cmdwhere2
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
            .SpName = "spRemidialPlanPaging"
            .BusinessDate = Me.BusinessDate
        End With

        Try
            oPaging = m_controller.GetDCRList(oPaging)
        Catch ex As Exception
            Dim err As New MaxiloanExceptions
            err.WriteLog("RemidialPlanList.aspx.vb", "DoBind", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
        End Try



        If Not oPaging Is Nothing Then
            DtUserList = oPaging.ListData
            recordCount = oPaging.TotalRecords
        Else
            recordCount = 0
        End If
        dtgCLActivity.DataSource = DtUserList.DefaultView
        dtgCLActivity.CurrentPageIndex = 0
        dtgCLActivity.DataBind()

        ''------tambahan----------
        'getchildcombo()
        'Dim strScript As String
        'strScript = "<script language=""JavaScript"">" & vbCrLf
        'strScript &= "ParentChange('" & Trim(cboParent.ClientID) & "','" & Trim(cboChild.ClientID) & "','" & Trim(hdnChildValue.ClientID) & "','" & Trim(hdnChildName.ClientID) & "',((eval('document.forms[0]." & cboParent.ClientID & "').selectedIndex  == -1) ? null : ListData[eval('document.forms[0]." & cboParent.ClientID & "').selectedIndex]));"
        'strScript &= "RestoreInsuranceCoIndex(" & Request("hdnBankAccount") & ");"
        'strScript &= "</script>"
        'divInnerHTML.InnerHtml = strScript

        ''------------------------

        PagingFooter()
        pnlDtGrid.Visible = True
    End Sub

#End Region

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SearchBy, Me.SearchBy2, Me.SortBy)
    End Sub
#End Region

#Region "databound"
    Private Sub dtgCLActivity_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgCLActivity.ItemDataBound
        Dim lblTemp As Label
        Dim lbbranch As Label
        Dim hyTemp As HyperLink

        If SessionInvalid() Then
            Exit Sub
        End If
        Dim m As Int32
        Dim hypCancel As HyperLink
        Dim inHyView As HyperLink
        Dim hyApplicationId As Label
        Dim lblInStatus As Label
        Dim inlblSeqNo As New Label
        Dim instyle As String = "AccMnt"

        If e.Item.ItemIndex >= 0 Then
            lblInStatus = CType(e.Item.FindControl("lblStatus"), Label)
            lbbranch = CType(e.Item.FindControl("lbranchid"), Label)
            lblTemp = CType(e.Item.FindControl("lblCustomerID"), Label)

            hyApplicationId = CType(e.Item.FindControl("lblApplicationId"), Label)


            '*** Customer Link
            'lblTemp = CType(e.Item.FindControl("lblCustomerId"), Label)
            hyTemp = CType(e.Item.FindControl("hypCustomerNamedtg"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(lblTemp.Text.Trim) & "')"
            '*** Agreement No link
            hyTemp = CType(e.Item.FindControl("hypAgreementNodtg"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(hyApplicationId.Text.Trim) & "')"
            '*** ApplicationId link
            'hyTemp = CType(e.Item.FindControl("hypApplicationID"), HyperLink)
            'hyTemp.NavigateUrl = "javascript:OpenApplicationId('" & "AccMnt" & "', '" & Server.UrlEncode(hyTemp.Text.Trim) & "')"

        End If
    End Sub
#End Region

#Region "Reset"

    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        Server.Transfer("remidialplanList.aspx")
    End Sub
#End Region

#Region "Search"
    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Buttonsearch.Click
        If SessionInvalid() Then
            Exit Sub
        End If

        If CheckFeature(Me.Loginid, Me.FormID, "SRCH", Me.AppId) Then
            Dim cmdwhere, cmdwhere2 As String
            Dim filterBy As String
            filterBy = ""

            Me.SearchBy = ""
            Me.SearchBy2 = ""
            cmdwhere = ""

            LblPlanDate.Text = ConvertDate(txtPlanDate.Text)
            'Remark 
            'Me.SearchBy2 = Me.SearchBy & "  ((CollectionAgreement.plandate  >= '" & Me.BusinessDate & "')    and (CollectionAgreement.plandate < (dateadd(day,1,'" & Me.BusinessDate & "'))) and not exists(select ApplicationId from DCR where DCRDate= ' " & Me.BusinessDate & "' and ApplicationId=CollectionAgreement.ApplicationId))"
            'Me.SearchBy = Me.SearchBy & "   ((DCR.DCRDate = '" & Me.BusinessDate & "')   and  (DCR.DCRDate < (dateadd(day,1,'" & Me.BusinessDate & "'))))  "

            'Me.SearchBy2 = Me.SearchBy & "  ((CollectionAgreement.plandate  >= '" & ConvertDate(txtPlanDate.Text) & "')    and (CollectionAgreement.plandate < (dateadd(day,1,'" & ConvertDate(txtPlanDate.Text) & "'))) and not exists(select ApplicationId from DCR where DCRDate= ' " & ConvertDate(txtPlanDate.Text) & "' and ApplicationId=CollectionAgreement.ApplicationId))"
            'Me.SearchBy2 = Me.SearchBy & "  ( EndPastDueDays < 180 and not exists(select ApplicationId from DCR where DCRDate= ' " & ConvertDate(txtPlanDate.Text) & "' and ApplicationId=CollectionAgreement.ApplicationId))"
            Me.SearchBy2 = Me.SearchBy & "  ( not exists(select ApplicationId from DCR where DCRDate= ' " & ConvertDate(txtPlanDate.Text) & "' and ApplicationId=CollectionAgreement.ApplicationId))"
            Me.SearchBy = Me.SearchBy & "   ((DCR.DCRDate = '" & Me.BusinessDate & "')   and  (DCR.DCRDate < (dateadd(day,1,'" & Me.BusinessDate & "'))))  "


            If cboSearchBy.SelectedItem.Value <> "Select One" Then
                If txtSearchBy.Text.Trim <> "" Then
                    If Right(txtSearchBy.Text.Trim, 1) = "%" Then
                        Me.SearchBy = Me.SearchBy & " and " & cboSearchBy.SelectedItem.Value & " like '" & txtSearchBy.Text.Trim & "'"
                        Me.SearchBy2 = Me.SearchBy2 & " and " & cboSearchBy.SelectedItem.Value & " like '" & txtSearchBy.Text.Trim & "'"

                        If filterBy <> "" Then
                            filterBy = filterBy & " , " & cboSearchBy.SelectedItem.Value & "  like " & txtSearchBy.Text.Trim.Replace("'", "''") & " "
                        Else
                            filterBy = filterBy & "" & cboSearchBy.SelectedItem.Value & "  like " & txtSearchBy.Text.Trim.Replace("'", "''") & " "
                        End If
                        lblSearchBy.Text = "" & cboSearchBy.SelectedItem.Value & " like '" & txtSearchBy.Text.Trim & "'"
                        Me.SearchByText = "" & cboSearchBy.SelectedItem.Value & " like '" & txtSearchBy.Text.Trim & "'"
                    Else
                        Me.SearchBy = Me.SearchBy & " and " & cboSearchBy.SelectedItem.Value & "='" & txtSearchBy.Text.Trim & "'"
                        Me.SearchBy2 = Me.SearchBy2 & " and " & cboSearchBy.SelectedItem.Value & "='" & txtSearchBy.Text.Trim & "'"
                        If filterBy <> "" Then
                            filterBy = filterBy & " , " & cboSearchBy.SelectedItem.Value & "  : " & txtSearchBy.Text.Trim.Replace("'", "''") & " "
                        Else
                            filterBy = filterBy & "" & cboSearchBy.SelectedItem.Value & "  : " & txtSearchBy.Text.Trim.Replace("'", "''") & " "
                        End If
                        lblSearchBy.Text = "" & cboSearchBy.SelectedItem.Value & " = '" & txtSearchBy.Text.Trim & "'"
                        Me.SearchByText = "" & cboSearchBy.SelectedItem.Value & " = '" & txtSearchBy.Text.Trim & "'"
                    End If
                Else
                    lblSearchBy.Text = "Not Specified"
                    Me.SearchByText = "Not Specified"
                End If
            Else
                lblSearchBy.Text = "Not Specified"
                Me.SearchByText = "Not Specified"
            End If

            ''----------------------------------------------------------------------------------------------
            ''Dapetin Collection type Id dari Supervisor yasng dipilih buat validasi save
            ''----------------------------------------------------------------------------------------------
            'Dim objCommand As New SqlCommand
            'Dim objConnection As New SqlConnection(getConnectionString)
            'Dim objReader As SqlDataReader
            'Dim Colltype As String
            'Dim SPVType As String
            'Try
            '    If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            '    objCommand.CommandType = CommandType.StoredProcedure
            '    objCommand.CommandText = "spGetCollIdSPV"
            '    objCommand.Connection = objConnection
            '    objCommand.Parameters.Add("@SPVName", SqlDbType.Char, 10).Value = Me.Loginid.Trim
            '    objReader = objCommand.ExecuteReader()
            '    If objReader.Read Then
            '        SPVType = objReader.Item("CollType").ToString.Trim
            '    End If
            '    objReader.Close()
            'Catch ex As Exception
            '    Response.Write(ex.Message)
            '    objReader.Close()
            'End Try
            ''----------------------------------------------------------------------------------------------
            ''----------------------------------------------------------------------------------------------
            Try

                Me.SearchBy = Me.SearchBy & "  and  COllectionAgreement.CGID='" & cboParent.SelectedItem.Value.Trim & "' "
                Me.SearchBy2 = Me.SearchBy2 & "  and  COllectionAgreement.CGID='" & cboParent.SelectedItem.Value.Trim & "' "

                If filterBy <> "" Then
                    filterBy = filterBy & " , Collection Group : " & cboParent.SelectedItem.Text.Trim & " "
                Else
                    filterBy = filterBy & " Collection Group : " & cboParent.SelectedItem.Text.Trim & " "
                End If

                lblCG.Text = "" & cboParent.SelectedItem.Text.Trim & ""
                Me.SelectedCG = "" & cboParent.SelectedItem.Text.Trim & ""


                'If SPVType = "CH" Or SPVType = "COLADM" Then
                '    If hdnChildValue.Value.Trim <> "0" And hdnChildValue.Value.Trim <> "ALL" Then
                '        Me.SearchBy = Me.SearchBy & " and COllectionAgreement.COllectorID='" & hdnChildValue.Value.Trim & "'"
                '        Me.SearchBy2 = Me.SearchBy2 & " and COllectionAgreement.COllectorID='" & hdnChildValue.Value.Trim & "'"

                '        LblCollector.Text = "" & hdnChildValue.Value.Trim & ""
                '        Me.CollectorID = "" & hdnChildValue.Value.Trim & ""
                '    End If
                'ElseIf SPVType = "COL" Then
                '    Me.SearchBy = Me.SearchBy & " and Collector.Supervisor = '" & Me.Loginid & "'"
                '    Me.SearchBy2 = Me.SearchBy2 & " and Collector.Supervisor = '" & Me.Loginid & "'"
                '    If filterBy <> "" Then
                '        filterBy = filterBy & " , Collector Supervisor : " & Me.Loginid.Trim & " "
                '    Else
                '        filterBy = filterBy & "Collector Supervisor : " & Me.Loginid.Trim & " "
                '    End If
                '    If hdnChildValue.Value.Trim <> "0" And hdnChildValue.Value.Trim <> "ALL" Then
                '        Me.SearchBy = Me.SearchBy & " and CollectionAgreement.CollectorID='" & hdnChildValue.Value.Trim & "'"
                '        Me.SearchBy2 = Me.SearchBy2 & " and CollectionAgreement.CollectorID='" & hdnChildValue.Value.Trim & "'"
                '        LblCollector.Text = "" & hdnChildValue.Value.Trim & ""
                '        Me.CollectorID = "" & hdnChildValue.Value.Trim & ""                    
                '    End If
                'ElseIf SPVType = "CL" Then
                '    Me.SearchBy = Me.SearchBy & " and COllectionAgreement.COllectorID='" & Me.Loginid & "'"
                '    Me.SearchBy2 = Me.SearchBy2 & " and COllectionAgreement.COllectorID='" & Me.Loginid & "'"
                '    If filterBy <> "" Then
                '        filterBy = filterBy & " , Collector : " & hdnChildValue.Value.Trim & " "
                '    Else
                '        filterBy = filterBy & "Collector : " & hdnChildValue.Value.Trim & " "
                '    End If
                '    LblCollector.Text = "" & hdnChildValue.Value.Trim & ""
                '    Me.CollectorID = "" & hdnChildValue.Value.Trim & ""
                'Else
                '    lblMessage.Text = "You are not allowed to view the list"
                '    lblMessage.Visible = True
                '    Exit Sub
                'End If

                Me.SearchBy = Me.SearchBy & " and COllectionAgreement.COllectorID='" & cboChild.SelectedValue.Trim & "'"
                Me.SearchBy2 = Me.SearchBy2 & " and COllectionAgreement.COllectorID='" & cboChild.SelectedValue.Trim & "'"

                If filterBy <> "" Then
                    filterBy = filterBy & " , Collector : " & cboChild.SelectedItem.Text.Trim & " "
                Else
                    filterBy = filterBy & "Collector : " & cboChild.SelectedItem.Text.Trim & " "
                End If

                LblCollector.Text = "" & cboChild.SelectedItem.Text.Trim & ""
                Me.CollectorID = "" & cboChild.SelectedValue.Trim & ""

                Me.FilterBy = filterBy
                Me.CmdWhere = Me.SearchBy

                If lblCG.Text.Trim = "" Then
                    lblCG.Text = "Not Specified"
                    Me.SelectedCG = "Not Specified"
                End If
                If LblCollector.Text.Trim = "" Then
                    LblCollector.Text = "Not Specified"
                    Me.CollectorID = "Not Specified"
                End If
                pnlDtGrid.Visible = True
                PnlSearchDetail.Visible = True
                dtgCLActivity.Visible = True
                pnlsearch.Visible = False

                DoBind(Me.SearchBy, Me.SearchBy2, Me.SortBy)

            Catch ex As Exception
                ShowMessage(lblMessage, ex.Message, True)
                Dim err As New MaxiloanExceptions
                err.WriteLog("remidialplanList.aspx.vb", "Search", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
                Response.Write(err.StackTrace)
            End Try


        End If
    End Sub
#End Region

#Region "Sort"
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SearchBy2, Me.SortBy)
    End Sub
#End Region

#Region "Cookies"
    Sub Cookies()
        Dim cookie As HttpCookie = Request.Cookies("remidialplan")
        If Not cookie Is Nothing Then
            cookie.Values("cmdwhere") = "CollectionAgreement.CGID='" & cboParent.SelectedItem.Value.Trim & "' " &
                                        "and COllectionAgreement.COllectorID='" & cboChild.SelectedValue.Trim & "' " &
                                        "and convert(char(8), DCR.PlanDate , 112) between '" & ConvertDate(txtPlanDate.Text) & "' And  '" & ConvertDate(txtPlanDateTo.Text) & "'"
            cookie.Values("cmdwhere2") = Me.SearchBy2
            cookie.Values("FilterBy") = Me.FilterBy
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie("remidialplan")
            cookieNew.Values.Add("cmdwhere", "CollectionAgreement.CGID='" & cboParent.SelectedItem.Value.Trim & "' " &
                                        "and COllectionAgreement.COllectorID='" & cboChild.SelectedValue.Trim & "' " &
                                        "and convert(char(8), DCR.PlanDate , 112) between '" & ConvertDate(txtPlanDate.Text) & "' And  '" & ConvertDate(txtPlanDateTo.Text) & "'")
            cookieNew.Values.Add("cmdwhere2", Me.SearchBy2)
            cookieNew.Values.Add("FilterBy", Me.FilterBy)
            Response.AppendCookie(cookieNew)
        End If
    End Sub

#End Region


#Region "ItemCommand"
    Private Sub dtgCLActivity_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgCLActivity.ItemCommand
        Select Case e.CommandName
            Case "result"
                Dim oDataTable As New DataTable
                Dim lblAgreementNo As HyperLink
                Dim lblApplicationid As Label
                Dim lblCustomerID As Label
                Dim lblCustomerName As HyperLink
                If lblSearchBy.Text = "Not Specified" Then
                    Me.SearchByText = "Not Specified"
                End If
                lblAgreementNo = CType(dtgCLActivity.Items(e.Item.ItemIndex).FindControl("hypAgreementNodtg"), HyperLink)
                lblApplicationid = CType(dtgCLActivity.Items(e.Item.ItemIndex).FindControl("lblApplicationId"), Label)
                lblCustomerID = CType(dtgCLActivity.Items(e.Item.ItemIndex).FindControl("lblCustomerID"), Label)
                lblCustomerName = CType(dtgCLActivity.Items(e.Item.ItemIndex).FindControl("hypCustomerNamedtg"), HyperLink)
                Response.Redirect("RemidialPlan.aspx?Applicationid=" & lblApplicationid.Text.Trim & "&WhereCond1=" & Me.SearchBy & "&WhereCond2=" & Me.SearchBy2 & "&SelectedCG=" & Me.SelectedCG & "&CollectorID=" & Me.CollectorID & "&SearchByText=" & Me.SearchByText)
        End Select
    End Sub
#End Region


#Region "Print"
    'Semenentara di hide
    'Private Sub imbPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonPrint.Click
    '    If CheckFeature(Me.Loginid, Me.FormID, "PRINT", Me.AppId) Then
    '        Cookies()
    '        Response.Redirect("remidialplanReport.aspx")

    '    End If
    'End Sub
#End Region



    Private Sub ClearSearchDetail()
        lblCG.Text = ""
        LblCollector.Text = ""
        lblSearchBy.Text = ""
        Me.SearchByText = ""
    End Sub

    Private Sub ImbExit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonExit.Click
        'Response.Redirect("remidialplanList.aspx?WhereCond1=" & Me.SearchBy & "&WhereCond2=" & Me.SearchBy2 & "")
        Response.Redirect("RemidialPlanList.aspx")
    End Sub
#Region "GenerateScriptreplaceUserController"
    Protected Function getchildcombo() As String
        Dim dtChild As New DataTable
        Dim Coll As New Parameter.CLActivity
        Dim CollList As New Parameter.CLActivity
        Dim intr As Integer
        With Coll
            .strConnection = GetConnectionString()
            .CGID = Me.GroubDbID
            .LoginId = Me.Loginid
            .strKey = "CL"
        End With
        CollList = oController1.CLActivityListCollector(Coll)
        dtChild = CollList.ListCLActivity
        Response.Write(GenerateScript(dtChild))
    End Function
    Protected Function CollectionGroupIDChange() As String
        Return "ParentChange('" & Trim(cboParent.ClientID) & "','" & Trim(cboChild.ClientID) & "','" & Trim(hdnChildValue.ClientID) & "','" & Trim(hdnChildName.ClientID) & "',((this.selectedIndex == -1) ? null : ListData[this.selectedIndex]));"
    End Function

    Private Function GenerateScript(ByVal DtTable As DataTable) As String
        Dim strScript As String
        Dim strScript1 As String
        Dim DataRow As DataRow()
        Dim strType As String
        Dim i As Int32
        Dim j As Int32
        strScript = "<script language=""JavaScript"">" & vbCrLf
        strScript &= "ListData = new Array(" & vbCrLf
        strType = ""
        For j = 0 To cboParent.Items.Count - 1
            DataRow = DtTable.Select(" CGID = '" & cboParent.Items(j).Value & "'")
            If DataRow.Length > 0 Then
                For i = 0 To DataRow.Length - 1
                    If strType <> CStr(DataRow(i)("CGID")).Trim Then
                        strType = CStr(DataRow(i)("CGID")).Trim
                        strScript &= "new Array(" & vbCrLf
                        strScript1 = ""
                    End If


                    If strScript1 = "" Then
                        strScript1 = " new Array('" & CStr(DataRow(i)("Name")).Trim & "','" & CStr(IIf(IsDBNull(DataRow(i)("CollectorID")), "null", DataRow(i)("CollectorID"))).Trim & "') "
                    Else
                        strScript1 &= "," & vbCrLf & " new Array('" & CStr(DataRow(i)("Name")).Trim & "','" & CStr(IIf(IsDBNull(DataRow(i)("CollectorID")), "null", DataRow(i)("CollectorID"))).Trim & "') "
                    End If
                Next
                strScript &= strScript1 & ")," & vbCrLf
            Else
                strScript &= " null," & vbCrLf
            End If
        Next

        If Right(strScript.Trim, 5) = "null," Then
            strScript = Left(strScript.Trim, strScript.Trim.Length - 1)
        Else
            strScript = Left(strScript.Trim, strScript.Trim.Length - 2)
        End If

        If Right(strScript.Trim, 4) = "null" Then
            strScript &= vbCrLf & ");" & vbCrLf
        Else
            strScript &= vbCrLf & "));" & vbCrLf
        End If
        'strScript &= vbCrLf & "));" & vbCrLf

        strScript &= "</script>"
        Return strScript
    End Function
#End Region

    Protected Sub btnPageNumb_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SearchBy2, Me.SortBy)
            End If
        End If
    End Sub
End Class