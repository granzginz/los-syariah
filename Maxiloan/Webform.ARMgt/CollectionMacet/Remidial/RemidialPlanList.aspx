﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="RemidialPlanList.aspx.vb" Inherits="Maxiloan.Webform.ARMgt.RemidialPlanList" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>RemidialPlanList</title>
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%#Request.servervariables("SERVER_NAME")%>/';			
    </script>
    <script language="javascript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;
        function OpenActivity() {
            window.open('../Inquiry/InqCollActivityResult.aspx?AgreementNo=' + hdnAgreement.value + '&CustomerName=' + hdnCustName.value + '&CustomerID=' + hdnCustID.value + '&Style=Collection&Referrer=CLActivity', 'UserLookup', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }
        function OpenCollection() {
            window.open('../Inquiry/InqCollActivityHistory.aspx?AgreementNo=' + hdnAgreement.value + '&CustomerName=' + hdnCustName.value + '&CustomerID=' + hdnCustID.value + '', 'UserLookup', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }
    </script>
    <script type="text/javascript" src="../../../Maxiloan.js"></script>
    <link rel="Stylesheet" href="../../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../../Include/General.css" type="text/css" />
    <script language="JavaScript" type="text/javascript">
        var hdnDetail;
        var hdndetailvalue;
        var cboBankAccount = null;
        function ParentChange(pCmbOfPayment, pBankAccount, pHdnDetail, pHdnDetailValue, itemArray) {
            hdnDetail = eval('document.forms[0].' + pHdnDetail);
            HdnDetailValue = eval('document.forms[0].' + pHdnDetailValue);
            var i, j;
            for (i = eval('document.forms[0].' + pBankAccount).options.length; i >= 0; i--) {
                eval('document.forms[0].' + pBankAccount).options[i] = null

            };
            if (itemArray == null) {
                j = 0;
            }
            else {
                j = 2;
            };
            eval('document.forms[0].' + pBankAccount).options[0] = new Option('Select One', '0');
            eval('document.forms[0].' + pBankAccount).options[1] = new Option('ALL', 'ALL');
            if (itemArray != null) {
                for (i = 0; i < itemArray.length; i++) {
                    eval('document.forms[0].' + pBankAccount).options[j++] = new Option(itemArray[i][0], itemArray[i][1]);

                };
                eval('document.forms[0].' + pBankAccount).selected = true;
            }
        };
        function cboChildonChange(selIndex, l, j) {
            hdnDetail.value = l;
            HdnDetailValue.value = j;
            eval('document.forms[0].hdnBankAccount').value = selIndex;
        }
        function RestoreInsuranceCoIndex(BankAccountIndex) {
            cboChild = eval('document.forms[0].cboChild');
            if (cboChild != null)
                if (eval('document.forms[0].hdnBankAccount').value != null) {
                    if (BankAccountIndex != null)
                        cboChild.selectedIndex = BankAccountIndex;
                }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>    
    <input id="hdnAgreement" type="hidden" name="hdnAgreement" runat="server" />
    <input id="hdnCustName" type="hidden" name="hdnCustName" runat="server" />
    <input id="hdnCustID" type="hidden" name="hdnCustID" runat="server" />
    <input id="hdnChildValue" type="hidden" name="hdnSP" runat="server" />
    <input id="hdnChildName" type="hidden" name="hdnSP" runat="server" />
    <input id="hdnBankAccount" type="hidden" name="hdSP" runat="server" />
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>         
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">           
            <h3>
                PERENCANAAN KEGIATAN HARIAN COLLECTOR
            </h3>
        </div>
    </div>  
    <asp:Panel ID="pnlsearch" runat="server">     
        <div class="form_box">
	        <div class="form_single">
                <label class ="label_req">Collection Group </label>
                <asp:DropDownList ID="cboParent" runat="server" onchange="<%#CollectionGroupIDChange()%>">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="rfvcboCollectionGroup" runat="server" ControlToValidate="cboParent" CssClass="validator_general"
                    ErrorMessage="Harap pilih Collection Group" Display="Dynamic" InitialValue="0"></asp:RequiredFieldValidator>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
             <label class ="label_req">Collector </label>
                <asp:DropDownList ID="cboChild" runat="server" onchange="cboChildonChange(this.selectedIndex,this.options[this.selectedIndex].value,this.options[this.selectedIndex].text);">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="rfvCollector" runat="server" ControlToValidate="cboChild" CssClass="validator_general"
                    ErrorMessage="Harap pilih Collector" Display="Dynamic" InitialValue="0"></asp:RequiredFieldValidator>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Cari Berdasarkan</label>
                <asp:DropDownList ID="cboSearchBy" runat="server">
                    <asp:ListItem>Select One</asp:ListItem>
                    <asp:ListItem Value="Agreement.AgreementNo">No Kontrak</asp:ListItem>
                    <asp:ListItem Value="Customer.Name">Nama Customer</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearchBy" runat="server" ></asp:TextBox>
	        </div>
        </div>
        <div class="form_button">
	        <asp:Button ID="Buttonsearch" runat="server"  Text="Search" CssClass ="small button blue"></asp:Button>
            <asp:Button ID="ButtonReset" runat="server"  Text="Reset" CssClass ="small button gray" CausesValidation="False"></asp:Button>
        </div>                 
    </asp:Panel>
    <asp:Panel ID="PnlSearchDetail" runat="server">        
        <div class="form_box_title">
	        <div class="form_single">
                <label>Collection Group</label>
                <asp:Label ID="lblCG" runat="server"></asp:Label>
	        </div>
        </div> 
        <div class="form_box">
	        <div class="form_single">
                <label>Collector</label>
                <asp:Label ID="LblCollector" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Cari Berdasarkan</label>
                <asp:Label ID="lblSearchBy" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
               <label class ="label_req">Cetak Tanggal Rencana</label>                

                <asp:TextBox ID="txtPlanDate" runat="server"></asp:TextBox>                   
                <asp:CalendarExtender ID="txtPlanDate_CalendarExtender" runat="server" 
                    Enabled="True" TargetControlID="txtPlanDate" Format ="dd/MM/yyyy"></asp:CalendarExtender>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*" CssClass="validator_general"
                ControlToValidate="txtPlanDate"></asp:RequiredFieldValidator>


                <asp:Label ID="LblPlanDate" runat="server" Visible="False"> </asp:Label>&nbsp;S/D&nbsp;                

                <asp:TextBox ID="txtPlanDateTo" runat="server"></asp:TextBox>                   
                <asp:CalendarExtender ID="txtPlanDateTo_CalendarExtender" runat="server" 
                    Enabled="True" TargetControlID="txtPlanDateTo" Format ="dd/MM/yyyy"></asp:CalendarExtender>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*" CssClass="validator_general"
                ControlToValidate="txtPlanDateTo"></asp:RequiredFieldValidator>

	        </div>
        </div>
        <%--<div class="form_button">
            <asp:Button ID="ButtonPrint" runat="server"  Text="Print" CssClass ="small button blue"
            CausesValidation="False" ></asp:Button>
	    </div>        --%>              
    </asp:Panel>
    <br/>
    <asp:Panel ID="pnlDtGrid" runat="server">
        <div class="form_box_title">
            <div class="form_single">              
                <h4>
                    DAFTAR KONTRAK PERENCANAAN KEGIATAN HARIAN COLLECTOR
                </h4>
            </div>
        </div>
        <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgCLActivity" runat="server" Width="100%" AllowSorting="True"
                        AutoGenerateColumns="False" DataKeyField="AgreementNo" BorderStyle="None" BorderWidth="0" OnSortCommand="SortGrid"
                        CssClass="grid_general">
                       <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                        <asp:TemplateColumn HeaderText="PILIH">                                
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbResult" runat="server" ImageUrl="../../../images/IconReceived.gif"
                                        CommandName="result"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="hypAgreementNodtg" runat="server" Text='<%# Container.Dataitem("AgreementNo")%>'>
                                    </asp:HyperLink>
                                    <asp:Label ID="hypApplicationID" Visible="False" runat="server" Text='<%# Container.Dataitem("ApplicationID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="CustomerName" HeaderText="NAMA CUSTOMER">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="hypCustomerNamedtg" runat="server" Text='<%# Container.Dataitem("CustomerName")%>'>
                                    </asp:HyperLink>
                                    <asp:Label ID="lblCustomerId" runat="server" Text='<%#Container.DataItem("CustomerID")%>'
                                        Visible="false">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="Address" SortExpression="Address" HeaderText="ALAMAT">                                
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="DueDate" SortExpression="DueDate" HeaderText="TGL JATUH TEMPO"
                                DataFormatString="{0:dd/MM/yyyy}">                                
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="DaysOverdue" SortExpression="DaysOverdue" HeaderText="HARI TELAT">                                
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="Plandate" SortExpression="Plandate" HeaderText="TGL RENCANA"
                                DataFormatString="{0:dd/MM/yyyy}">                                
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="PlanActivityID" SortExpression="PlanActivityID" HeaderText="KEGIATAN">                                
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="PdcRequest" SortExpression="PdcRequest" HeaderText="PDC REQUEST">
                            </asp:BoundColumn>
                            
                            <asp:TemplateColumn Visible="False" SortExpression="ApplicationId" HeaderText="ApplicationId">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblApplicationId" runat="server" Text='<%# Container.Dataitem("ApplicationId")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>                        
                    </asp:DataGrid>      
                <div class="button_gridnavigation">
	                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../../Images/grid_navbutton01.png"
		                CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../../Images/grid_navbutton02.png"
		                CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../../Images/grid_navbutton03.png"
		                CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../../Images/grid_navbutton04.png"
		                CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
	                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
		                EnableViewState="False"></asp:Button>
	                <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage"
		                MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer"
		                CssClass="validator_general"></asp:RangeValidator>
	                <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
		                ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>
                <div class="label_gridnavigation">
	                <asp:Label ID="lblPage" runat="server"></asp:Label>of
	                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
	                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                </div>
            </div> 
        </div>                       
        </div>     
        <div class="form_button">
            <asp:Button ID="ButtonExit" runat="server" Text="Exit" CssClass ="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    <div id="divInnerHTML" runat="server">
    </div>            
    </form>
</body>
</html>
