﻿
#Region "Imports"
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class PrintCheckListViewer
    Inherits Maxiloan.webform.WebBased

#Region "Constanta"
    Private m_controller As New AssetRepoController
    Private oCustomClass As New Parameter.AssetRepo
#End Region
#Region "Property"
    Private Property ExecutorID() As String
        Get
            Return CType(viewstate("ExecutorID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ExecutorID") = Value
        End Set
    End Property

    Private Property RALNo() As String
        Get
            Return CType(viewstate("RALNo"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("RALNo") = Value
        End Set
    End Property

    Private Property AgreementNo() As String
        Get
            Return CType(viewstate("AgreementNo"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AgreementNo") = Value
        End Set
    End Property
    Private Property ApplicationId() As String
        Get
            Return CType(viewstate("ApplicationId"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationId") = Value
        End Set
    End Property
    Private Property PageBack() As String
        Get
            Return CType(viewstate("PageBack"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("PageBack") = Value
        End Set
    End Property
    Public Property ReportType() As String
        Get
            Return CType(viewstate("ReportType"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ReportType") = Value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If
        BindReportCheckList()
    End Sub

    Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies(COOKIES_ASSETREPOCHECKLISTRPT)
        Me.ApplicationId = cookie.Values("ApplicationId")
        Me.ExecutorID = cookie.Values("ExecutorID")
        Me.ReportType = cookie.Values("ReportType")
    End Sub

    Sub BindReportCheckList()
        GetCookies()
        Dim dtvEntity As New DataView
        Dim dtsEntity As New DataTable
        Dim dsPrintCheckList As New DataSet
        Dim dsPrintPrepaymentLetter As New DataSet
        Dim PrintCheckListPrint As ReportDocument = Nothing
        Dim strFileLocation As String
        Dim PDFFile As String = ""


        With oCustomClass
            .strConnection = GetConnectionString
            .ApplicationID = Me.ApplicationId
            .ExecutorID = Me.ExecutorID
            .BusinessDate = Me.BusinessDate
        End With

        If Me.ReportType = "PrintCheckList" Then
            PrintCheckListPrint = New PrintCheckListPrint
            oCustomClass = m_controller.AssetRepoCLPrintReport(oCustomClass)
            PrintCheckListPrint.SetDataSource(oCustomClass.ListReport)
            PrintCheckListPrint.SetParameterValue(0, Me.sesCompanyName)
        ElseIf Me.ReportType = "PrintPrepaymentLetter" Then
            PrintCheckListPrint = New PrepaymentPrinting
            oCustomClass = m_controller.AssetRepoPrepaymentLetterPrint(oCustomClass)
            PrintCheckListPrint.SetDataSource(oCustomClass.ListReport)
        End If

        crvPrintCheckList.ReportSource = PrintCheckListPrint
        crvPrintCheckList.DataBind()

        With PrintCheckListPrint.ExportOptions
            .ExportDestinationType = ExportDestinationType.DiskFile
            .ExportFormatType = ExportFormatType.PortableDocFormat
        End With

        Dim doctoprint As New System.Drawing.Printing.PrintDocument
        Dim i As Integer
        'doctoprint.PrinterSettings.PrinterName = "Maxiloan Printer Server"


        If Me.ReportType = "PrintCheckList" Then
            PDFFile = "PrintCheckList.pdf"
        ElseIf Me.ReportType = "PrintPrepaymentLetter" Then
            PDFFile = "PrintPrepaymentLetter.pdf"
        End If

        For i = 0 To doctoprint.PrinterSettings.PaperSizes.Count - 1
            Dim rawKind As Integer
            If doctoprint.PrinterSettings.PaperSizes(i).PaperName = PDFFile Then
                rawKind = CInt(doctoprint.PrinterSettings.PaperSizes(i).GetType().GetField("kind", Reflection.BindingFlags.Instance Or Reflection.BindingFlags.NonPublic).GetValue(doctoprint.PrinterSettings.PaperSizes(i)))
                PrintCheckListPrint.PrintOptions.PaperSize = CType(rawKind, CrystalDecisions.Shared.PaperSize)
                Exit For
            End If
        Next

        strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
        
        strFileLocation += Me.Session.SessionID + Me.Loginid + PDFFile

        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = _
        New CrystalDecisions.Shared.DiskFileDestinationOptions


        DiskOpts.DiskFileName = strFileLocation

        With PrintCheckListPrint
            .ExportOptions.DestinationOptions = DiskOpts
            .Export()
            .Close()
            .Dispose()
        End With

        Response.Redirect("PrintRepossesCL.aspx?file=" & Me.Session.SessionID + Me.Loginid + PDFFile)
    End Sub
End Class
