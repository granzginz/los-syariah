﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ExecutorHistory.aspx.vb"
    Inherits="Maxiloan.Webform.ARMgt.ExecutorHistory" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ExecutorHistory</title>
    <script type="text/javascript" src="../../Maxiloan.js"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <script type="text/javascript" language="javascript">
        var x = screen.width;
        var y = screen.height - 100;
        function OpenWinCustomer(pID) {
            window.open('../../Webform.LoanOrg/Credit/ViewPersonalCustomer.aspx?CustomerID=' + pID + '&style=Collection', 'UserLookup', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }
        function fClose() {
            window.close();
            return false;
        }	
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">           
                <h3>
                    HISTORY EKSEKUTOR
                </h3>
            </div>
        </div>  
        <div class="form_box">
	        <div class="form_single">
                <label>Collection Group</label>
                <asp:Label ID="lblCGID" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>ID Eksekutor</label>
                <asp:Label ID="lblExecutorID" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box_title">
            <div class="form_single">              
                <h4>
                    <%--HISTORY SKT--%>
                    HISTORY SKE
                </h4>
            </div>
        </div>
        <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgRALPrinting" runat="server" Width="100%"
                    AutoGenerateColumns="False"
                    AllowSorting="True" CssClass="grid_general" BorderStyle="None" BorderWidth="0">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="NO KONTRAK">                            
                            <ItemTemplate>
                                <asp:HyperLink ID="hypAgreementNodtg" runat="server" Text='<%# Container.Dataitem("AgreementNo")%>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="NAMA CUSTOMER">                            
                            <ItemTemplate>
                                <a href="javascript:OpenWinCustomer('<%# Container.DataItem("CustomerID")%>');">
                                    <asp:Label ID="hypCustomerNamedtg" runat="server" Text='<%# Container.Dataitem("CustomerName")%>'>
                                    </asp:Label></a>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="INSTALLMENTNO" HeaderText="ANGS KE">                            
                        </asp:BoundColumn>
                        <%--<asp:BoundColumn DataField="RALPeriod" HeaderText="PERIODE SKT">                           --%>
                        <asp:BoundColumn DataField="RALPeriod" HeaderText="PERIODE SKE">
                        </asp:BoundColumn>
                        <%--<asp:BoundColumn DataField="RALSTATUS" HeaderText="STATUS SKT">                            --%>
                        <asp:BoundColumn DataField="RALSTATUS" HeaderText="STATUS SKE">
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="Description" HeaderText="ALASAN BATAL">                            
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="REPOSSESs" HeaderText="TARIK">                            
                        </asp:BoundColumn>
                    </Columns>                    
                </asp:DataGrid>
            </div>
        </div>
        </div>
        <div class="form_button">
            <asp:Button runat="server" ID="ButtonClose" OnClientClick="fClose();"  Text="Close" CssClass ="small button gray"/>
	    </div>                      
    </form>
</body>
</html>

