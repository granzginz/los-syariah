﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewAppraisal.aspx.vb"
    Inherits="Maxiloan.Webform.ARMgt.ViewAppraisal" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ViewAppraisal</title>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;

        function Close() {
            window.close();
        }
        function fback() {
            history.go(-1);
            return false;
        }

        function OpenViewAgreement(pAgreementNo) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.LoanOrg/Credit/ViewStatementofAccount.aspx?AgreementNo=' + pAgreementNo + '&Style=Collection', 'AgreementView', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenViewCustomer(pCustomerID) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.LoanOrg/Credit/ViewPersonalCustomer.aspx?CustomerID=' + pCustomerID + '&Style=Collection', 'CustomerView', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">           
                <h3>
                    VIEW - PENAWARAN INVENTORY
                </h3>
            </div>
        </div>  
        <asp:Panel ID="PnlListDetail" runat="server">    
        <div class="form_box">	        
                <div class="form_left">
                    <label>No Kontrak</label>
                    <asp:HyperLink ID="hypAgreementNo" runat="server"></asp:HyperLink>
		        </div>
		        <div class="form_right">		
                    <label>Nama Customer</label>	
                    <asp:HyperLink ID="hypCustomerName" runat="server"></asp:HyperLink>
		        </div>	       
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Nama Asset</label>
                <asp:Label ID="lblAsset" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <label>Estimasi Harga</label>
                    <asp:Label ID="lblestimationamount" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">	
                    <label>Tanggal Estimasi</label>		
                    <asp:Label ID="lblestimationdate" runat="server"></asp:Label>
		        </div>	        
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <label>Accrued Interest</label>
                    <asp:Label ID="lblaccruedinterest" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">		
                    <label>A/R Asuransi</label>	
                    <asp:Label ID="lblARInsurance" runat="server"></asp:Label>
		        </div>	        
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <label>Biaya Tarik</label>
                    <asp:Label ID="lblrepossessfee" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">	
                    <label>Biaya Tagih</label>		
                    <asp:Label ID="lblbillingcharges" runat="server"></asp:Label>
		        </div>	        
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <label>Denda keterlambatan Angsuran</label>
                    <asp:Label ID="lblLateCharges" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">			
		        </div>	        
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <label><strong>Nilai Inventory</strong></label>
                    <asp:Label ID="lblinvamount" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">			
                    <label><strong>Total A/R Customer</strong></label>
                    <asp:Label ID="lbltotalAR" runat="server"></asp:Label>
		        </div>	        
        </div>
        <div class="form_box_title">
            <div class="form_single">              
                <h4>
                    DAFTAR PENAWARAN
                </h4>
            </div>
        </div>
        <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgbidder" runat="server" AutoGenerateColumns="false" CssClass="grid_general" BorderStyle="None" BorderWidth="0" Width="100%">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="NO.">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblbidderno" runat="server" Text='<%#Container.DataItem("BidderNo")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="NAMA PENAWAR">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblbiddername" runat="server" Text='<%#Container.DataItem("BidderName")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="HARGA PENAWARAN">                                
                                <ItemTemplate>
                                    <asp:Label ID="Lblbidderamount" runat="server" Text='<%#FormatNumber(Container.DataItem("BidderAmount"),2)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="BidderDate" HeaderText="TGL PENAWARAN" DataFormatString="{0:dd/MM/yyyy}">                                
                            </asp:BoundColumn>
                        </Columns>                        
                </asp:DataGrid>
            </div>
        </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonClose" OnClientClick="Close();" runat="server"  Text="Close" CssClass ="small button gray">
            </asp:Button>
	    </div>                                      
    </asp:Panel>
    </form>
</body>
</html>
