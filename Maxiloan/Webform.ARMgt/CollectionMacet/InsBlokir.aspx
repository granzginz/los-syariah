﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="InsBlokir.aspx.vb"
    Inherits="Maxiloan.Webform.ARMgt.InsBlokir" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>ReleaseBlokirAngsuran</title>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <script language="JavaScript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';
    </script>
</head>
<body>
    <form id="form1" runat="server">    
        <asp:Label ID="lblMessage" runat="server" Visible ="false" ></asp:Label>  
        <input id="hdnApplicationId" type="hidden" name="hdnApplicationId" runat="server" />
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">                       
                <h3>                                        
                    RELEASE BLOKIR ANGSURAN
                </h3>
            </div>
        </div>        
        <asp:Panel ID="pnlList" runat="server">
        <asp:Panel ID="pnlGrid" runat="server">
        <div class="form_box_header">
            <div class="form_single">              
                <h4>
                    DAFTAR KONTRAK PEMBAYARAN DIBLOKIR
                </h4>
            </div>
        </div>
        <div class="form_box_header">
        <div class="form_single">
        <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgList" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                            DataKeyField="ApplicationId" BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <Columns>                                                                
                                <asp:TemplateColumn HeaderText="PILIH">                                    
                                    <ItemTemplate>                                                                                
                                        <asp:LinkButton ID="idRelease" runat="server" CausesValidation="False" Text="RELEASE"
                                        CommandName="Edit"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="Name" HeaderText="NAMA KONSUMEN">                                    
                                    <ItemTemplate>                                                                                    
                                            <asp:HyperLink runat="server" ID="lblName" Text='<%#Container.Dataitem("Name")%>'/>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">                                    
                                    <ItemTemplate>                                                                                    
                                            <asp:HyperLink runat="server" ID="lblAgreementNo" Text='<%#Container.Dataitem("AgreementNo")%>'/>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn DataField="SPNo" SortExpression="SPNo" HeaderText="NO SP">                                   
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="SPDate" SortExpression="SPDate" HeaderText="TGL SP">                                    
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="AmountOverdue" SortExpression="AmountOverdue" HeaderText="TUNGGAKAN">                                    
                                <HeaderStyle cssClass="th_right"/> 
                                <ItemStyle cssClass="item_grid_right"/>
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="LateCharges" SortExpression="LateCharges" HeaderText="DENDA">                                    
                                <HeaderStyle cssClass="th_right"/> 
                                <ItemStyle cssClass="item_grid_right"/>
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="CollectorName" SortExpression="CollectorName" HeaderText="COLLECTOR">                                    
                                </asp:BoundColumn> 
                                <asp:BoundColumn DataField="Name" Visible="false">                                    
                                </asp:BoundColumn> 
                                <asp:BoundColumn DataField="AgreementNo" Visible="false">                                    
                                </asp:BoundColumn>  
                                <asp:BoundColumn DataField="ApplicationId" Visible="false">                                    
                                </asp:BoundColumn>                                                                  
                                <asp:BoundColumn DataField="CustomerID" Visible="false">                                    
                                </asp:BoundColumn>  
                            </Columns>
                        </asp:DataGrid>                                       
                <div class="button_gridnavigation">
	                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
		                CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
		                CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
		                CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
		                CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
	                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
		                EnableViewState="False"></asp:Button>
	                <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage"
		                MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer"
		                CssClass="validator_general"></asp:RangeValidator>
	                <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
		                ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>
                <div class="label_gridnavigation">
	                <asp:Label ID="lblPage" runat="server"></asp:Label>of
	                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
	                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                </div>
        </div>
        </div>
        </div>  
        </asp:Panel>       
        <div class="form_box_title">
            <div class="form_single">              
                <h4>
                    CARI KONTRAK BLOKIR BAYAR
                </h4>
            </div>
        </div> 
        <div class="form_box">
	        <div class="form_single">
                <label>Cabang</label>
                <asp:DropDownList ID="cboBranch" runat="server">
                </asp:DropDownList>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Cari Berdasarkan</label>
                <asp:DropDownList ID="cboSearch" runat="server">
                    <asp:ListItem Value="Name">Nama</asp:ListItem>
                    <asp:ListItem Value="AgreementNo">No Kontrak</asp:ListItem>
                </asp:DropDownList>                        
                <asp:TextBox ID="txtSearch" runat="server" ></asp:TextBox>
	        </div>
        </div>             
        <div class="form_button">
            <asp:Button ID="ButtonSearch" runat="server" CausesValidation="False" CssClass ="small button blue" Text="Search">
            </asp:Button>
            <asp:Button ID="ButtonReset" runat="server" CausesValidation="False" Text="Reset" CssClass ="small button gray">
            </asp:Button>
        </div>                                        
        </asp:Panel>
        <asp:Panel ID="pnlRelease" runat="server">        
        <div class="form_box_title">
            <div class="form_single">              
                <h4>
                    PROSES PENERIMAAN INVOICE
                </h4>
            </div>
        </div>                        
        <div class="form_box">
	        <div class="form_single">
                <label>No Kontrak</label>                
                <asp:Label ID="lblNoKontrak" runat="server"></asp:Label>                
	        </div>
        </div>                             
        <div class="form_box">
	        <div class="form_single">
                <label>Nama Customer</label>                
                <asp:Label ID="lblNamaCustomer" runat="server"></asp:Label>         
	        </div>
        </div>                           
        <div class="form_box">
	        <div class="form_single">
                <label>No Surat Tugas</label>                
                <asp:Label ID="lblNoSP" runat="server"></asp:Label>         
	        </div>
        </div>                           
        <div class="form_box">
	        <div class="form_single">
                <label>Tanggal Surat Tugas</label>                
                <asp:Label ID="lblTglSP" runat="server"></asp:Label>         
	        </div>
        </div>                           
        <div class="form_box">
	        <div class="form_single">
                <label>Jumlah Tunggakan</label>                
                <asp:Label ID="lblAmountOverdue" runat="server"></asp:Label>      
	        </div>
        </div>                           
        <div class="form_box">
	        <div class="form_single">
                <label>Jumlah Denda</label>                
                <asp:Label ID="lblLateCharges" runat="server"></asp:Label>    
	        </div>
        </div>                           
        <div class="form_box">
	        <div class="form_single">
                <label>Collector</label>                
                <asp:Label ID="lblCollector" runat="server"></asp:Label> 
	        </div>
        </div>                           
        <div class="form_button">
            <asp:Button ID="ButtonSave" runat="server" CausesValidation="true" Text="Save" CssClass ="small button blue">
            </asp:Button>
            <asp:Button ID="ButtonCancel" runat="server" CausesValidation="false" Text="Cancel" CssClass ="small button gray">
            </asp:Button>
        </div>                  
        </asp:Panel>       
    </form>
</body>
</html>

