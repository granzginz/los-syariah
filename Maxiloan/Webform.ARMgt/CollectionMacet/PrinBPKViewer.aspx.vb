﻿#Region "Imports"
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class PrinBPKViewer
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private m_controller As New CollPrintBPKController
    Private oCustomClass As New Parameter.CollPrintBPK
#End Region
#Region "Property"
    Private Property ExecutorID() As String
        Get
            Return CType(viewstate("ExecutorID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ExecutorID") = Value
        End Set
    End Property


    Private Property ApplicationId() As String
        Get
            Return CType(viewstate("ApplicationId"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationId") = Value
        End Set
    End Property
    Private Property RepossesSeqNo() As String
        Get
            Return CType(viewstate("RepossesSeqNo"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("RepossesSeqNo") = Value
        End Set
    End Property
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load        
        If Not Page.IsPostBack Then
            Me.FormID = "CollPrintBPK"
            BindReport()
        End If
    End Sub


    Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies("CollPrintBPK")
        Me.ApplicationId = cookie.Values("ApplicationId")
        Me.ExecutorID = cookie.Values("ExecutorID")
        Me.BranchID = cookie.Values("BranchID")
        Me.RepossesSeqNo = cookie.Values("RepossesSeqNo")
    End Sub


    Sub BindReport()

        Dim dtvEntity As New DataView
        Dim dtsEntity As New DataTable
        Dim dsPrintBPK As New DataSet
        Dim PrintingBPK As PrintBPKPrint = New PrintBPKPrint


        GetCookies()


        With oCustomClass
            .strConnection = GetConnectionString()
            .ApplicationID = Me.ApplicationId
            .ExecutorID = Me.ExecutorID
            .BusinessDate = Me.BusinessDate
            .BranchId = Me.BranchID
            .RepossesSeqNo = CType(Me.RepossesSeqNo, Integer)
        End With

        oCustomClass = m_controller.PrintBPKReport(oCustomClass)

        PrintingBPK.SetDataSource(oCustomClass.ListReport)        
        crPrintBPK.ReportSource = PrintingBPK
        crPrintBPK.DataBind()

        With PrintingBPK.ExportOptions
            .ExportDestinationType = ExportDestinationType.DiskFile
            .ExportFormatType = ExportFormatType.PortableDocFormat
        End With

        Dim doctoprint As New System.Drawing.Printing.PrintDocument
        Dim i As Integer
        'doctoprint.PrinterSettings.PrinterName = "Maxiloan Printer Server"

        For i = 0 To doctoprint.PrinterSettings.PaperSizes.Count - 1
            Dim rawKind As Integer
            If doctoprint.PrinterSettings.PaperSizes(i).PaperName = "PrintingBPK" Then
                rawKind = CInt(doctoprint.PrinterSettings.PaperSizes(i).GetType().GetField("kind", Reflection.BindingFlags.Instance Or Reflection.BindingFlags.NonPublic).GetValue(doctoprint.PrinterSettings.PaperSizes(i)))
                PrintingBPK.PrintOptions.PaperSize = CType(rawKind, CrystalDecisions.Shared.PaperSize)
                Exit For
            End If
        Next

        Dim strFileLocation As String

        strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
        strFileLocation += Me.Session.SessionID + Me.Loginid + "BPK.pdf"

        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = _
        New CrystalDecisions.Shared.DiskFileDestinationOptions

        DiskOpts.DiskFileName = strFileLocation

        With PrintingBPK
            .ExportOptions.DestinationOptions = DiskOpts
            .Export()
            .Close()
            .Dispose()
        End With

        Response.Redirect("PrintBPK.aspx?file=" & Me.Session.SessionID + Me.Loginid + "BPK.pdf")
    End Sub
End Class