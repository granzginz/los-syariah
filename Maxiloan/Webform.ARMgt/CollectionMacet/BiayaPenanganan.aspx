﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="BiayaPenanganan.aspx.vb"
    Inherits="Maxiloan.Webform.ARMgt.BiayaPenanganan" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ucApprovalRequest" Src="../../Webform.UserController/ucApprovalRequest.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcBranchCollection" Src="../../Webform.UserController/UcBranchCollection.ascx" %>
<%@ Register TagName="ucNumberFormat" TagPrefix="uc1" Src="../../webform.UserController/ucNumberFormat.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>BiayaPenanganan</title>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;

        var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%# Request.servervariables("SERVER_NAME")%>/';

        function OpenWinHistory(pCGID, pExID, pExName) {
            window.open('../ExecutorHistory.aspx?CGID=' + pCGID + '&ExecutorID=' + pExID + '&ExecutorName=' + pExName + '', 'UserLookup', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenWinCollector(pCollID, pCGID) {
            window.open('../../Webform.Setup/CollectorView.aspx?CGID=' + pCGID + '&CollectorID=' + pCollID + '', 'UserLookup', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenWinCustomer(pID) {
            window.open('../../../Webform.LoanOrg/Credit/ViewPersonalCustomer.aspx?CustomerID=' + pID + '&style=Collection', 'UserLookup', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="sm1">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
        onclick="hideMessage();"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                BIAYA PENANGANAN
            </h3>
        </div>
    </div>
    <asp:Panel ID="pnlsearch" runat="server">
        <div class="form_box">
            <div class="form_single">
                <label>
                    Collection
                </label>
                &nbsp;<uc1:ucbranchcollection id="oCGID" runat="server"></uc1:ucbranchcollection>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">  
                <label>
                    Cari Berdasarkan</label>
                <asp:DropDownList ID="cboSearchBy" runat="server">
                    <asp:ListItem Value="Select One">Select One</asp:ListItem>
                    <asp:ListItem Value="CollectionAgreement.AgreementNo">No Kontrak</asp:ListItem>
                    <asp:ListItem Value="Customer.Name">Nama Customer</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearchBy" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="Buttonsearch" runat="server" Text="Search" CssClass="small button blue">
            </asp:Button>&nbsp;
            <asp:Button ID="ButtonReset" runat="server" Text="Reset" CssClass="small button gray"
                CausesValidation="False"></asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlDtGrid" runat="server">
        <div class="form_box_title">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgCollExpenseList" runat="server" Width="100%" AllowSorting="True"
                        AutoGenerateColumns="False" DataKeyField="AgreementNo" OnSortCommand="Sorting"
                        CssClass="grid_general" BorderStyle="None" BorderWidth="0">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="PILIH">
                                <ItemTemplate>
                                    <asp:LinkButton CommandName="Request" ID="imbRequest" runat="server" Text="BIAYA"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="AgreementNo" Visible="False"></asp:BoundColumn>
                            <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">
                                <ItemTemplate>
                                    <asp:Label ID="lblBranchID" Text='<%# Container.DataItem("BranchID")%>' runat="server"
                                        Visible="false"></asp:Label>
                                    <asp:Label ID="lblAgreementNo" Text='<%# Container.DataItem("AgreementNo")%>' runat="server"
                                        Visible="false"></asp:Label>
                                    <asp:Label ID="lblRALNo" Text='<%# Container.DataItem("RALNo")%>' runat="server"
                                        Visible="false"></asp:Label>
                                    <a href="javascript:OpenAgreementNo('Collection','<%# Container.DataItem("AgreementNo")%>')">
                                        <%# Container.DataItem("AgreementNo")%></a>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="CustomerName" HeaderText="NAMA CUSTOMER">
                                <ItemTemplate>
                                    <asp:Label ID="lblCollectorID" Text='<%# Container.DataItem("CollectorID")%>' runat="server"
                                        Visible="false"></asp:Label>
                                    <asp:Label ID="lblCustomerID" Text='<%# Container.DataItem("CustomerID")%>' runat="server"
                                        Visible="false"></asp:Label>
                                    <a href="javascript:OpenCustomer('Collection','<%# Container.DataItem("CustomerID")%>')">
                                        <%# Container.DataItem("CustomerName")%></a>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="AssetDescription" SortExpression="AssetDescription" HeaderText="NAMA ASSET">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="HariTelat" SortExpression="HariTelat" HeaderText="HARI TELAT">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="Collectorid" SortExpression="Collectorid" HeaderText="EKSEKUTOR" />
                            <asp:BoundColumn DataField="CollectorName" SortExpression="CollectorName" HeaderText="NAMA EKSEKUTOR">
                            </asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <div class="button_gridnavigation">
                        <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                            CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                            CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                            CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                            CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                        <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                            EnableViewState="False"></asp:Button>
                        <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" MinimumValue="1"
                            ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                            ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general">
                        </asp:RequiredFieldValidator>
                    </div>
                    <div class="label_gridnavigation">
                        <asp:Label ID="lblPage" runat="server"></asp:Label>of
                        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                        <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlSelect" runat="server">
       <div class="form_title"> <div class="form_single"> <h5>INFORMASI KONSUMEN DAN ASSET</h5> </div> </div>

        <div class="form_box">
            <div class="form_left">
                <label>
                    No Kontrak</label>
                <asp:HyperLink ID="hypAgreementNoSlct" runat="server"></asp:HyperLink>
            </div>
            <div class="form_right">
                <label>
                    Nama Customer</label>
                <asp:HyperLink ID="hypCustomerNameSlct" runat="server"></asp:HyperLink>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Alamat</label>
                <asp:Label ID="lblAddress" runat="server"></asp:Label>
            </div>
        </div>
    
        <div class="form_box">
            <div class="form_left">
                <label>
                    Angsuran Ke</label>
                <asp:Label ID="lblInstallmentNo" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Jumlah Angsuran</label>
                <asp:Label ID="lblInstallmentAmount" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Tanggal Jatuh Tempo</label>
                <asp:Label ID="lblDuedate" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Jumlah Hari Telat</label>
                <asp:Label ID="lblOverDueDays" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Jumlah Tunggakan</label>
                <asp:Label ID="lblODAmount" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Denda Keterlambatan</label>
                <asp:Label ID="lblLateCharges" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>Sisa A/R</label>
                <asp:Label ID="lblOSBallance" runat="server"></asp:Label> 
            </div>
            <div class="form_right">
                <label>Biaya Tarik (Respossess Fee)</label>
                <asp:Label ID="lblOSBillingCharges" runat="server" visible="false"></asp:Label>
                <asp:Label ID="lblCollExpense" runat="server"></asp:Label>
            </div>
        </div>
        
        <div class="form_box">
            <div class="form_left">
                <%--<label>No SKT</label>--%>
                <label>No SKE</label>
                <asp:Label ID="lblRALNo" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <%--<label>Periode SKT</label>--%>
                <label>Periode SKE</label>
                <asp:Label ID="lblRALPeriod1" runat="server"></asp:Label>&nbsp;S/D&nbsp;
                <asp:Label ID="lblRALPeriod2" runat="server"></asp:Label>
            </div>
        </div>
        <%--<div class="form_box">
            <div class="form_single">
                <label>Status SKT</label>
                <asp:Label ID="lblRALStatus" runat="server"></asp:Label>
            </div>
        </div>--%>
        <div class="form_box">
            <div class="form_left">
                <label>Eksekutor</label>
                <asp:Label ID="lblExecutor" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <%--<label>SKT Ditangani</label>--%>
                <label>SKE Ditangani</label>
                <asp:Label runat="server" ID="lblSktDitangai"></asp:Label>&nbsp;hari
            </div>
        </div>
        
        <div class="form_box">
            <div class="form_single">
                <label>Nama Asset</label>
                <asp:Label ID="lblAsset" runat="server"></asp:Label>
            </div>
        </div>

        <div class="form_box">
             <div class="form_left">
                <label>No Polisi</label>
                <asp:Label ID="lblNoPolisi" runat="server"></asp:Label>
            </div>

              <div class="form_right">
                <label>Tahun Produksi</label>
                <asp:Label ID="lblTahunProduksi" runat="server"></asp:Label>
              </div>
        </div>

         <div class="form_box">
            <div class="form_left">
                <label>No Rangka</label>
                <asp:Label ID="lblNoRangka" runat="server"></asp:Label>
            </div>

              <div class="form_right">
                <label>Warna</label>
                <asp:Label ID="lblWarna" runat="server"></asp:Label>
            </div> 
        </div>


        <div class="form_box">
            <div class="form_left">
               <label>No Mesin</label>
                <asp:Label ID="lblNoMesin" runat="server"></asp:Label>
            </div>

              <div class="form_right">
                <label>BAPK</label>
                <asp:Label ID="lblBAPK" runat="server"></asp:Label>
            </div> 
        </div>
        
         <div class="form_title">
         <div class="form_single">
            <h5> BIAYA REPO </h5>
        </div>
        </div>
        
        <div class="form_box">
	        <div class="form_single">                
                <label class ="label_req">Tanggal Pembebanan</label>               
                <asp:TextBox ID="txtTanggalPembebanan" runat="server"></asp:TextBox>                   
                <asp:CalendarExtender ID="txtNewInvExpDate_CalendarExtender" runat="server" Enabled="True" TargetControlID="txtTanggalPembebanan" Format ="dd/MM/yyyy"></asp:CalendarExtender>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*" CssClass="validator_general" ControlToValidate="txtTanggalPembebanan"></asp:RequiredFieldValidator>       
	        </div>
        </div>

        <div class="form_box">
            <div class="form_single">
                <label>Biaya Collection</label>
                <uc1:ucnumberformat id="txtBiayaCollection" runat="server" />
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>Biaya Tebus</label>
                <uc1:ucnumberformat id="txtBiayaTebus" runat="server" />
            </div>
        </div>
         <div class="form_title">
             <div class="form_single">
                <h5>BOP</h5>
            </div>
         </div>

         <div class="form_single">
                <div class="grid_wrapper_ns"> 
                    <asp:DataGrid  ID="dtgBOP" runat="server"  CssClass="grid_general"  BorderStyle="None" BorderWidth="0" 
                    AutoGenerateColumns="False"  Width="70%"  DataKeyField="Seq" >
                    <HeaderStyle CssClass="th" /> 
                    <ItemStyle CssClass="item_grid" /> 
                         <Columns>  
                            <asp:BoundColumn  Visible="True" DataField="Seq" HeaderText="NO" />
                            <asp:BoundColumn  Visible="True" DataField="BOPDescription" HeaderText="JENIS BIAYA"/>
                            <asp:BoundColumn  Visible="True" DataField="BOPAmount"  ItemStyle-HorizontalAlign="left"  DataFormatString= "{0:#,0}" HeaderText="JUMLAH"/>
                              
                             
                             <asp:TemplateColumn ItemStyle-Width="9" HeaderText="DEL" >
                                 <ItemStyle Width="10%" HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbDelete" runat="server" CausesValidation="False" ImageUrl="../../Images/iconDelete.gif"  CommandName="Delete"></asp:ImageButton>
                                </ItemTemplate>
                              </asp:TemplateColumn> 
                          </Columns>
                          </asp:DataGrid >
                          
                          <asp:Panel ID="pnlAddJenisBOP" runat="server">
                           <div class="form_box">
                                <div class="form_single">
                                    <label>Jenis Biaya</label>
                                     <asp:DropDownList ID="cboJenisBiayaBOP" runat="server" />  
                                     <asp:RequiredFieldValidator ID="cboJenisBiayaBOP_RequiredFieldValidator" runat="server" 
                                     CssClass="validator_general" Display="Dynamic" InitialValue="0" ControlToValidate="cboJenisBiayaBOP" Enabled="True" ErrorMessage="Harap pilih Jenis Biaya"  ValidationGroup="JenisBOPValiGroup" ></asp:RequiredFieldValidator>
                                </div>
                            </div>

                             <div class="form_box">
                             <div class="form_single">
                                <label>Jumlah</label>
                                <uc1:ucnumberformat id="txtJumlahBiayaBOP" runat="server" />
                              </div>
                              </div>
                               
                              <div class="form_button">
                                  <asp:Button ID="btnAddOK" runat="server" Text="Add" CssClass="small button blue" ValidationGroup="JenisBOPValiGroup"/> &nbsp;
                                  <asp:Button ID="btnCancelAdd" runat="server" Text="Cancel" CssClass="small button gray" CausesValidation="False" />
                              </div>

                          </asp:Panel>

                          <div class="form_button" id="divAdd" runat="server">
                                <asp:Button ID="btnAdd" runat="server" Text="Add" CssClass="small button blue" />
                          </div>

                </div>
            </div> 

        <div class="form_button">
            <asp:Button ID="ButtonSave" runat="server" Text="Save" CssClass="small button blue">
            </asp:Button>&nbsp;
            <asp:Button ID="ButtonCancel" runat="server" Text="Cancel" CssClass="small button gray"
                CausesValidation="False"></asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
