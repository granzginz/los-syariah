﻿#Region "Import"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class FormPersetujuan
    Inherits Maxiloan.Webform.WebBased


#Region "properties"
    Public Property cgid() As String
        Get
            Return CType(ViewState("cgid"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("cgid") = Value
        End Set
    End Property
    Private Property ExecutorID() As String
        Get
            Return CType(ViewState("ExecutorID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ExecutorID") = Value
        End Set
    End Property


    Private Property ApplicationId() As String
        Get
            Return CType(ViewState("ApplicationId"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationId") = Value
        End Set
    End Property
#End Region

#Region " Private Const "


    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region

    Private Sub InitialDefaultPanel()
        pnlDtGrid.Visible = False
        pnlsearch.Visible = True
    End Sub

#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If

    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        Bindgrid(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click

        If txtPage.Text = "" Then
            txtPage.Text = "0"
        Else
            If IsNumeric(txtPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                    currentPage = CType(txtPage.Text, Int32)
                    Bindgrid(Me.SearchBy, Me.SortBy)
                End If
            End If
        End If


    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        Bindgrid(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not Page.IsPostBack Then
            Me.FormID = "FormPersetujuan"
            Me.cgid = Me.GroubDbID
            If CheckForm(Me.Loginid, Me.FormID, "Maxiloan") Then

                InitialDefaultPanel()

                If Request("file") <> "" Then
                    Dim strFileLocation As String = "../../XML/" & Request.QueryString("file")
                    Response.Write("<script language = javascript>" & vbCrLf _
                                    & "var x = screen.width; " & vbCrLf _
                                    & "var y = screen.height; " & vbCrLf _
                   & "window.open('" & strFileLocation & "','accacq', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes, resizable=1') " & vbCrLf _
                   & "</script>")
                End If

            Else
                Dim strHTTPServer As String
                Dim strHTTPApp As String
                Dim strNameServer As String
                strHTTPServer = Request.ServerVariables("PATH_INFO")
                strNameServer = Request.ServerVariables("SERVER_NAME")
                strHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                Response.Redirect("http://" & strNameServer & "/" & strHTTPApp & "/NotAuthorized.html")
            End If

        End If
       
    End Sub

    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Buttonsearch.Click
        Me.SearchBy = ""
        Me.SortBy = ""
  
        If cboSearchBy.SelectedIndex <> 0 And txtSearchBy.Text <> "" Then
            Me.SearchBy = Me.SearchBy & " and " & cboSearchBy.SelectedItem.Value.Trim & " like '%" & txtSearchBy.Text.Trim & "%'"
        End If

        Bindgrid(Me.SearchBy, Me.SortBy)
    End Sub
    Sub Bindgrid(ByVal cmdWhere As String, ByVal cmSort As String)
        Dim dtvEntity As New DataView
        Dim dtsEntity As New DataTable
        Dim oCollInv As New Parameter.InventoryAppraisal
        Dim m_CollInv As New Controller.InventoryAppraisalController
        With oCollInv
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        m_CollInv.PrintFormPersetujuanList(oCollInv)

        With oCollInv
            lblrecord.Text = CType(.TotalRecord, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecord
        End With

        dtsEntity = oCollInv.Listappraisal
        dtsEntity.TableName = "Table"

        dtvEntity = dtsEntity.DefaultView
        dtvEntity.Sort = Me.SortBy

        dtgPrintList.DataSource = dtvEntity

        If Not dtvEntity Is Nothing Then
            dtgPrintList.DataBind()
            pnlDtGrid.Visible = True
        End If

        PagingFooter()
    End Sub

    Private Sub dtgPrintList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPrintList.ItemCommand
        If e.CommandName = "print" Then
            Dim cookie As HttpCookie = Request.Cookies("FormPersetujuanPrint")
            If Not cookie Is Nothing Then
                cookie.Values("ApplicationId") = dtgPrintList.Items(e.Item.ItemIndex).Cells(6).Text
            Else
                Dim cookieNew As New HttpCookie("FormPersetujuanPrint")
                cookieNew.Values.Add("ApplicationId", dtgPrintList.Items(e.Item.ItemIndex).Cells(6).Text)
                Response.AppendCookie(cookieNew)
            End If
            Response.Redirect("FormPersetujuanViewer.aspx")
        End If
    End Sub

    Private Sub dtgPrintList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPrintList.ItemDataBound

    End Sub
End Class