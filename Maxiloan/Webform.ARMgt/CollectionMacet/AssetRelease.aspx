﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AssetRelease.aspx.vb"
    Inherits="Maxiloan.Webform.ARMgt.AssetRelease" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcBranchCollection" Src="../../Webform.UserController/UcBranchCollection.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>AssetRelease</title>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;
        var ServerName = '<%#Request.ServerVariables("SERVER_NAME")%>';
        var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);

        function fback() {
            history.go(-1);
            return false;
        }

        function OpenWinCust(pStrCustID, pStrType) {
            var lStrAddress;
            if (pStrType == 'P')
                lStrAddress = 'ViewSearch.aspx';
            else
                lStrAddress = 'ViewSearchCompany.aspx';

            window.open('http://' + ServerName + '/' + App + '/Application/SmartSearch/' + lStrAddress + '?custid=' + pStrCustID, 'WinLink', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0,scrollbars=1,resizable=1,center=1');
        }

        function OpenWinAgreement(pStrAgreementNo) {
            window.open('http://' + ServerName + '/' + App + '/Application/SmartSearch/ViewAgreementDetail.aspx?AgreementNo=' + pStrAgreementNo, 'WinLink', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0,scrollbars=1,resizable=1,center=1');
        }

        function OpenWinAsset(pStrApplicationID, pStrAgreementNo, asq) {
            window.open('http://' + ServerName + '/' + App + '/Application/SmartSearch/perAsset.aspx?applicationid=' + pStrApplicationID + '&agreementno=' + pStrAgreementNo + '&assetseqno=' + asq, 'WinLink', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=1,resizable=1,center=1');
        }

        function PlanRALChange() {
            var myID = 'UcRALDate_txtDate';
            var objDate = eval('document.forms[0].' + myID);
            var result

            if (document.forms[0].rbRAL_0.checked = true) {
                result = 'Plan';
            }
            if (document.forms[0].rbRAL_1.checked = true) {
                result = 'Undo Plan';
            }

            alert(result);
            if (result == 'Undo Plan') {
                objDate.disabled = true;
                document.forms[0].all.UcRALDate_txtDate_imgCalender.disabled = true;
                document.forms[0].all.UcRALDate_txtDate_imgCalender.disabled = true;
                document.forms[0].UcRALDate_txtDate.value = '';
            }

            function OpenViewAgreement(pAgreementNo) {
                var AppInfo = '<%#Request.servervariables("PATH_INFO")%>';
                var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
                window.open('http://<%#Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.LoanOrg/Credit/ViewStatementofAccount.aspx?AgreementNo=' + pAgreementNo + '&Style=Collection', 'AgreementView', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
            }

            function OpenViewCustomer(pCustomerID) {
                var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
                var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
                window.open('http://<%#Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.LoanOrg/Credit/ViewPersonalCustomer.aspx?CustomerID=' + pCustomerID + '&Style=Collection', 'CustomerView', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
            }

        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>    
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>
                        PELEPASAN ASSET
                    </h3>
                </div>
            </div>
            <asp:Panel ID="PnlSearch" runat="server" Width="100%">
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Collection Group
                        </label>
                        &nbsp;<uc1:ucbranchcollection id="oCGID" runat="server"></uc1:ucbranchcollection>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Cari berdasarkan</label>
                        <asp:DropDownList ID="cboSearchBy" runat="server">
                            <asp:ListItem Value="Select One">Select One</asp:ListItem>
                            <asp:ListItem Value="CollectionAgreement.AgreementNo">No Kontrak</asp:ListItem>
                            <asp:ListItem Value="Customer.Name">Nama Konsumen</asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtSearchBy" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="Buttonsearch" runat="server" Text="Search" CssClass="small button blue">
                    </asp:Button>&nbsp;
                    <asp:Button ID="ButtonReset" runat="server" Text="Reset" CssClass="small button gray"
                        CausesValidation="False"></asp:Button>
                </div>
            </asp:Panel>
            <asp:Panel ID="PnlList" runat="server" Width="100%">
                <div class="form_box_title">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtg" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                                OnSortCommand="Sorting" CssClass="grid_general" BorderStyle="None" BorderWidth="0">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="LEPAS">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbRelease" runat="server" CommandName="RELEASE" ImageUrl="../../images/iconrelease.gif">
                                            </asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn Visible="False" DataField="AgreementNo"></asp:BoundColumn>
                                    <asp:BoundColumn Visible="False" DataField="CustomerID"></asp:BoundColumn>
                                    <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbAgreement" runat="server" Text='<%#container.dataitem("AgreementNo")%>'>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="CustomerName" HeaderText="NAMA CUSTOMER">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbCustomer" runat="server" Text='<%# container.dataitem("CustomerName")%>'>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="AssetDescription" SortExpression="AssetDescription" HeaderText="NAMA ASSET">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn Visible="False" DataField="RALNO" HeaderText="NO SKE"></asp:BoundColumn>
                                    <asp:BoundColumn Visible="False" DataField="ASSETTYPEID" HeaderText="ASSETTYPEID">
                                    </asp:BoundColumn>
                                </Columns>
                            </asp:DataGrid>
                            <div class="button_gridnavigation">
                                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                                    CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                                    CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                                    CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                                    CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                                    EnableViewState="False"></asp:Button>
                                <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" MinimumValue="1"
                                    ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"></asp:RangeValidator>
                                <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                                    ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general">
                                </asp:RequiredFieldValidator>
                            </div>
                            <div class="label_gridnavigation">
                                <asp:Label ID="lblPage" runat="server"></asp:Label>of
                                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel ID="PnlListDetail" Style="z-index: 102; top: 448px; left: 8px" runat="server">
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            No Kontrak</label>
                        <asp:HyperLink ID="hypAgreementNo" runat="server"></asp:HyperLink>
                    </div>
                    <div class="form_right">
                        <label>
                            Nama Customer</label>
                        <asp:HyperLink ID="hypCustomerName" runat="server"></asp:HyperLink>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Alamat</label>
                        <asp:Label ID="lblAddress" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            No Angsuran</label>
                        <asp:Label ID="lblInstallmentNo" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Jumlah Angsuran</label>
                        <asp:Label ID="lblInstallmentAmount" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Tanggal Jatuh Tempo</label>
                        <asp:Label ID="lblDuedate" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Jumlah Hari Telat</label>
                        <asp:Label ID="lblOverDueDays" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Jumlah Tunggakan</label>
                        <asp:Label ID="lblODAmount" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Denda Keterlambatan</label>
                        <asp:Label ID="lblLateCharges" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Biaya Tagih</label>
                        <asp:Label ID="lblOSBillingCharges" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Biaya Collection</label>
                        <asp:Label ID="lblCollExpense" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Sisa A/R</label>
                        <asp:Label ID="lblOSBallance" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Nama Asset</label>
                        <asp:Label ID="lblAsset" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            No Rangka</label>
                        <asp:Label ID="lblChassisNo" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            No Mesin</label>
                        <asp:Label ID="lblEngineNo" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            No Polisi</label>
                        <asp:Label ID="lblLicensePlate" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Tahun Produksi</label>
                        <asp:Label ID="lblYear" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Warna</label>
                        <asp:Label ID="lblColor" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Tanggal Tarik</label>
                        <asp:Label ID="lblRepossesDate" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Ditarik Oleh</label>
                        <asp:Label ID="lblRepossesBy" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Status Asset</label>
                        <asp:Label ID="lblAssetStatus" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Harga Jual</label>
                        <asp:Label ID="lblSellingPrice" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Tanggal Jual</label>
                        <asp:Label ID="lblSellingDate" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label class="label_req">
                            Di Serahkan Kepada</label>
                        <asp:TextBox ID="txtReleaseTo" runat="server" MaxLength="50"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Width="296px"
                            CssClass="validator_general" ErrorMessage="Harap isi Diserahkan Kepada" ControlToValidate="txtReleaseTo"
                            Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>
                    <div class="form_right">
                        <label class="label_req">
                            Di Serahkan Oleh</label>
                        <asp:TextBox ID="txtReleaseBy" runat="server" MaxLength="50"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Width="296px"
                            CssClass="validator_general" ErrorMessage="Harap isi Diserahkan Oleh" ControlToValidate="txtReleaseTo"
                            Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_general">
                            Catatan</label>
                        <asp:TextBox ID="txtNotes" runat="server" CssClass="multiline_textbox" TextMode="MultiLine"></asp:TextBox>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Tanggal Lepas</label>
                        <asp:TextBox ID="txtReleaseDate" runat="server"></asp:TextBox>
                        <asp:CalendarExtender ID="txtReleaseDate_CalendarExtender" runat="server" Enabled="True"
                            TargetControlID="txtReleaseDate" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                            CssClass="validator_general" ControlToValidate="txtReleaseDate"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box_title">
                    <div class="form_single">
                        <h4>
                            ASSET CHECK LIST
                        </h4>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtgCheckList" runat="server" Width="100%" AutoGenerateColumns="False"
                                CssClass="grid_general" BorderStyle="None" BorderWidth="0" AllowSorting="True">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:BoundColumn DataField="id" HeaderText="NO"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="Question" HeaderText="PERTANYAAN"></asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="JAWABAN">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtAnswer" runat="server"></asp:TextBox>
                                            <asp:RadioButtonList ID="rbAnswer" runat="server" RepeatDirection="Horizontal" class="opt_single" >
                                                <asp:ListItem Value="Y" Selected="True">Yes</asp:ListItem>
                                                <asp:ListItem Value="N">No</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="Notes" HeaderText="KETERANGAN"></asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="CATATAN TERIMA">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtAnswerReceive" runat="server" Width="100px"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Harap Jawab"
                                                CssClass="validator_general" ControlToValidate="txtAnswerReceive" Display="Dynamic"></asp:RequiredFieldValidator>
                                            <asp:RadioButtonList ID="rbAnswerReceive" class="opt_single" runat="server" RepeatDirection="Horizontal">
                                                <asp:ListItem Value="Y">Yes</asp:ListItem>
                                                <asp:ListItem Value="N" Selected="True">No</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn Visible="False" DataField="CheckListID"></asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="KETERANGAN TERIMA">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtDescriptionReceive" runat="server"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn Visible="False" DataField="ynquestion"></asp:BoundColumn>
                                    <asp:BoundColumn Visible="False" DataField="quantity"></asp:BoundColumn>
                                    <asp:TemplateColumn Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# replace(DataBinder.Eval(Container, "DataItem.notes"),"&nbsp;","") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                        </div>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonSave" runat="server" Text="Save" CssClass="small button blue">
                    </asp:Button>&nbsp;
                    <asp:Button ID="ButtonCancel" runat="server" Text="Cancel" CssClass="small button gray"
                        CausesValidation="False"></asp:Button>
                </div>
            </asp:Panel>        
    </form>
</body>
</html>
