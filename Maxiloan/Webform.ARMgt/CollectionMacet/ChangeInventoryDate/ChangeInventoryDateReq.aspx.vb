﻿#Region "Imports"
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Parameter.InvChgExpDate
#End Region

Public Class ChangeInventoryDateReq
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oCG As UcBranchCollection
    Protected WithEvents GridNavigator As ucGridNav
#Region "properties"
    Public Property PrevInvExpDate() As Date
        Get
            Return CType(Viewstate("PrevInvExpDate"), Date)
        End Get
        Set(ByVal Value As Date)
            Viewstate("PrevInvExpDate") = Value
        End Set
    End Property

    Public Property RepossesSeqNo() As Integer
        Get
            Return CType(Viewstate("RepossesSeqNo"), Integer)
        End Get
        Set(ByVal Value As Integer)
            Viewstate("RepossesSeqNo") = Value
        End Set
    End Property

    Public Property AssetSeqNo() As Integer
        Get
            Return CType(Viewstate("AssetSeqNo"), Integer)
        End Get
        Set(ByVal Value As Integer)
            Viewstate("AssetSeqNo") = Value
        End Set
    End Property

    Public Property UntilDate() As Date
        Get
            Return CType(Viewstate("UntilDate"), Date)
        End Get
        Set(ByVal Value As Date)
            Viewstate("UntilDate") = Value
        End Set
    End Property

    Public Property RALDate() As Date
        Get
            Return CType(Viewstate("RALDate"), Date)
        End Get
        Set(ByVal Value As Date)
            Viewstate("RALDate") = Value
        End Set
    End Property

    Public Property RALEndDate() As Date
        Get
            Return CType(Viewstate("RALEndDate"), Date)
        End Get
        Set(ByVal Value As Date)
            Viewstate("RALEndDate") = Value
        End Set
    End Property

    Public Property AssetTypeID() As String
        Get
            Return CType(Viewstate("AssetTypeID"), String)
        End Get
        Set(ByVal Value As String)
            Viewstate("AssetTypeID") = Value
        End Set
    End Property

    Public Property CustomerID() As String
        Get
            Return CType(Viewstate("CustomerID"), String)
        End Get
        Set(ByVal Value As String)
            Viewstate("CustomerID") = Value
        End Set
    End Property

    Public Property AllPage() As String
        Get
            Return CType(Viewstate("AllPage"), String)
        End Get
        Set(ByVal Value As String)
            Viewstate("AllPage") = Value
        End Set
    End Property

    Public Property cgid() As String
        Get
            Return CType(viewstate("CGID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CGID") = Value
        End Set
    End Property

    Public Property AgreementNo() As String
        Get
            Return CType(viewstate("AgreementNo"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AgreementNo") = Value
        End Set
    End Property

    Public Property ApplicationID() As String
        Get
            Return CType(viewstate("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property


    Public Property RALNo() As String
        Get
            Return CType(viewstate("RALNo"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("RALNo") = Value
        End Set
    End Property

    Public Property CollectorID() As String
        Get
            Return CType(viewstate("CollectorID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CollectorID") = Value
        End Set
    End Property

    Public Property CollectorName() As String
        Get
            Return CType(viewstate("CollectorName"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CollectorName") = Value
        End Set
    End Property

    Public Property CollectorType() As String
        Get
            Return CType(viewstate("CollectorType"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CollectorType") = Value
        End Set
    End Property

    Public Property RALExtension() As String
        Get
            Return CType(ViewState("RALExtension"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("RALExtension") = Value
        End Set
    End Property
#End Region

#Region " Private Const "
    Dim m_InvChgExpDate As New ChgInvExpDateController

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region

#Region " Navigation "
     

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        Bindgrid(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

    Private Sub getCombo()
        Dim oInvChgExpDate As New Parameter.InvChgExpDate
        With oInvChgExpDate
            .strConnection = GetConnectionString()
        End With


        Dim dt As New DataTable
        Try
            dt = m_InvChgExpDate.GetComboReason(oInvChgExpDate)
        Catch ex As Exception

        End Try

        cboReason.DataSource = dt
        cboReason.DataTextField = "Name"
        cboReason.DataValueField = "ID"
        cboReason.DataBind()
        cboReason.Items.Insert(0, "Select One")
        cboReason.Items(0).Value = "0"
        cboReason.SelectedIndex = 0


        '------------------------------------
        'Combo ApproveBy
        '------------------------------------

        Dim dtApproved As New DataTable
        Dim dvApproved As New DataView
        'dtApproved = Me.Get_UserApproval("CHINV", Me.sesBranchId.Replace("'", ""))
        dtApproved = Me.Get_UserApproval("CH1", Me.sesBranchId.Replace("'", ""))
        dvApproved = dtApproved.DefaultView
        With cboApproveBy
            .DataValueField = "ID"
            .DataTextField = "Name"
            .DataSource = dvApproved
            .DataBind()
        End With
        cboApproveBy.Items.Insert(0, "Select One")
        cboApproveBy.Items(0).Value = "0"

    End Sub

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        'Put user code to initialize the page here
        If SessionInvalid() Then
            Exit Sub
        End If
        AddHandler GridNavigator.PageChanged, AddressOf PageNavigation
        If Not Me.IsPostBack Then
            Me.FormID = "CollChangeInvReq"
            Me.cgid = Me.GroubDbID
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                InitialDefaultPanel()
                getCombo()
            End If
        End If
    End Sub
    Protected Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        currentPage = e.CurrentPage
        Bindgrid(Me.SearchBy, Me.SortBy, True)
        GridNavigator.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
    End Sub
#Region " InitialDefaultPanel "
    Private Sub InitialDefaultPanel()
        PnlSearch.Visible = True
        PnlList.Visible = False
        PnlListDetail.Visible = False
    End Sub
#End Region

    Sub Bindgrid(ByVal cmdWhere As String, ByVal cmSort As String, Optional isFrNav As Boolean = False)
        Dim dtvEntity As New DataView
        Dim dtsEntity As New DataTable
        Dim oInvChgExpDate As New Parameter.InvChgExpDate

        With oInvChgExpDate
            .strConnection = GetConnectionString()
            .CGID = Me.cgid
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oInvChgExpDate = m_InvChgExpDate.InvChgExpDateList(oInvChgExpDate)

        With oInvChgExpDate 
            recordCount = .TotalRecord
        End With

        dtsEntity = oInvChgExpDate.listData

        dtvEntity = dtsEntity.DefaultView
        dtvEntity.Sort = Me.SortBy

        dtg.DataSource = dtvEntity

        Try
            dtg.DataBind()
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
            dtg.CurrentPageIndex = 0
            dtg.DataBind()
        End Try

        If (isFrNav = False) Then
            GridNavigator.Initialize(recordCount, pageSize)
        End If
    End Sub

    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        InitialDefaultPanel()
        cboSearchBy.ClearSelection()
        txtSearchBy.Text = ""
        Me.SearchBy = ""
        Me.SortBy = ""
        Me.Bindgrid(Me.SearchBy, Me.SortBy)
        PnlSearch.Visible = True
        PnlList.Visible = True        
    End Sub

    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Buttonsearch.Click
        Me.SearchBy = ""
        Me.SortBy = ""

        If oCG.BranchID.Trim <> "" Then
            Me.SearchBy = "CGID='" & oCG.BranchID & "'"
            Me.cgid = oCG.BranchID
        End If

        If cboSearchBy.SelectedIndex <> 0 And txtSearchBy.Text <> "" Then
            If Me.SearchBy = "" Then
                Me.SearchBy = cboSearchBy.SelectedItem.Value.Trim & "='" & txtSearchBy.Text.Trim & "'"
            Else
                Me.SearchBy = Me.SearchBy & " and " & cboSearchBy.SelectedItem.Value.Trim & "='" & txtSearchBy.Text.Trim & "'"
            End If
        End If

        Bindgrid(Me.SearchBy, Me.SortBy)
        PnlList.Visible = True
        PnlListDetail.Visible = False
    End Sub

    Private Sub viewRequestDetail(ByVal BranchID As String, ByVal ApplicationID As String)
        Dim oInvChgExpDate As New Parameter.InvChgExpDate
        Dim dt As New DataTable

        With oInvChgExpDate
            .strConnection = getConnectionString()
            .AgreementNo = AgreementNo
            .BusinessDate = Me.BusinessDate
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationID
            .CGID = Me.cgid
        End With

        oInvChgExpDate = m_InvChgExpDate.InvChgExpDateDetail(oInvChgExpDate)

        dt = oInvChgExpDate.listData

        hypAgreementNo.Text = CStr(dt.Rows(0).Item("AgreementNo"))
        hypCustomerName.Text = CStr(dt.Rows(0).Item("CustomerName"))

        hypAgreementNo.Attributes.Add("onclick", "OpenViewAgreement(" & hypAgreementNo.Text & ")")
        hypCustomerName.Attributes.Add("onclick", "OpenViewCustomer(" & CStr(dt.Rows(0).Item("CustomerID")) & ")")

        lblAddress.Text = CStr(dt.Rows(0).Item("MailingAddress"))
        lblDuedate.Text = CStr(dt.Rows(0).Item("Duedate"))
        lblOverDueDays.Text = CStr(dt.Rows(0).Item("EndPastDuedays"))
        lblInstallmentNo.Text = CStr(dt.Rows(0).Item("InstallmentNo"))
        lblInstallmentAmount.Text = CStr(FormatNumber(dt.Rows(0).Item("InstallmentAmount"), 0))
        lblODAmount.Text = CStr(FormatNumber(dt.Rows(0).Item("EndPastDueAmt"), 0))
        lblLateCharges.Text = CStr(FormatNumber(dt.Rows(0).Item("LC"), 0))
        lblOSBallance.Text = CStr(FormatNumber(dt.Rows(0).Item("OS_Gross"), 0))
        lblCollExpense.Text = CStr(FormatNumber(dt.Rows(0).Item("CollectionExpense"), 0))

        lblAsset.Text = CStr(dt.Rows(0).Item("Description"))
        lblLicensePlate.Text = CStr(dt.Rows(0).Item("LicensePlate"))
        lblChassisNo.Text = CStr(dt.Rows(0).Item("ChassisNo"))
        lblEngineNo.Text = CStr(dt.Rows(0).Item("EngineNo"))
        lblYear.Text = CStr(dt.Rows(0).Item("Year")).Trim
        lblColor.Text = CStr(dt.Rows(0).Item("Color")).Trim

        lblRepossesDate.Text = CStr(dt.Rows(0).Item("RepossesDate"))
        lblExpectedDate.Text = Me.PrevInvExpDate.ToString("dd/MM/yyyy")


        Me.AssetTypeID = CStr(dt.Rows(0).Item("AssetTypeID")).Trim
        Me.AssetSeqNo = CInt(dt.Rows(0).Item("AssetSeqNo"))

        Me.ApplicationID = CStr(dt.Rows(0).Item("ApplicationID")).Trim
        Me.BranchID = CStr(dt.Rows(0).Item("BranchID")).Trim
        Me.RepossesSeqNo = CInt(dt.Rows(0).Item("RepossesSeqNo"))
        Me.CustomerID = CStr(dt.Rows(0).Item("CustomerID"))


        '===================================
        'Bersihkan semua textbox, dsb
        '===================================
        txtNotes.Text = ""
        cboReason.SelectedIndex = 0
        cboApproveBy.SelectedIndex = 0
        txtNewInvExpDate.Text = Me.BusinessDate.ToString("dd/MM/yyyy")        
        PnlSearch.Visible = False
        PnlList.Visible = False
        PnlListDetail.Visible = True

        RALExtension = CStr(dt.Rows(0).Item("RALExtension"))
        If (CInt(dt.Rows(0).Item("FreqPerpanjanganSKT")) >= CInt(dt.Rows(0).Item("PerpanjanganSKT"))) Then
            ButtonSave.Visible = False
            'ShowMessage(lblMessage, String.Format("Maksimum perpanjangan SKT {0} kali.", dt.Rows(0).Item("PerpanjanganSKT")), False)
            ShowMessage(lblMessage, String.Format("Maksimum perpanjangan SKE {0} kali.", dt.Rows(0).Item("PerpanjanganSKT")), False)
        End If


    End Sub

    Private Sub imbCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        PnlListDetail.Visible = False
        Me.SearchBy = ""
        Me.SortBy = ""
        Me.Bindgrid(Me.SearchBy, Me.SortBy)
        PnlSearch.Visible = True
        PnlList.Visible = True        
    End Sub

    Private Sub imbSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSave.Click

        'Validasi 

        If Me.BusinessDate > ConvertDate2(txtNewInvExpDate.Text) Then        
            ShowMessage(lblMessage, "Tanggal Masuk Inventory baru harus >= Tanggal hari ini", True)
            Exit Sub
        End If

        Dim maxDateAllowed = Me.BusinessDate.AddDays(CDbl(RALExtension))

        If ConvertDate2(txtNewInvExpDate.Text) > maxDateAllowed Then
            ShowMessage(lblMessage, String.Format("Maksimum tanggal Masuk Inventory {0} hari ({1:dd/MM/yyyy}) dari Tgl {2:dd/MM/yyyy} ", RALExtension, maxDateAllowed, BusinessDate), True)
            Exit Sub
        End If

        'If lblExpectedDate.Text.Trim <> "" Then
        '    If ConvertDate2(lblExpectedDate.Text.Trim) > ConvertDate2(txtNewInvExpDate.Text) Then
        '        lblMessage.Text = "New Expexted Date must be greater than Expected Date before"
        '        lblMessage.Visible = True
        '        Exit Sub
        '    End If
        'End If

        'Ambil semua data yang diperlukan dari layar detail

        Dim oInvChgExpDate As New Parameter.InvChgExpDate

        With oInvChgExpDate
            .strConnection = GetConnectionString()
            .CGID = Me.cgid
            .AssetSeqNo = Me.AssetSeqNo
            .RepossesDate = ConvertDate2(lblRepossesDate.Text.Trim)

            .AgreementNo = hypAgreementNo.Text.Trim
            .AssetDescription = lblAsset.Text.Trim
            .EndpastDueDays = CInt(lblOverDueDays.Text.Trim)

            .Reason = cboReason.SelectedItem.Value.Trim

            .PrevInvExpDate = Me.PrevInvExpDate
            .NewInvExpDate = ConvertDate2(txtNewInvExpDate.Text)
            .LoginId = Me.Loginid

            .ApproveBy = cboApproveBy.SelectedItem.Value.Trim
            .RequestBy = Me.Loginid

            '.ApproveVia = ""
            '.FaxNo = txtFaxNo.Text.Trim

            .Notes = txtNotes.Text.Trim

            .RepossesSeqNo = CInt(Me.RepossesSeqNo)

            .BussinessDate = Me.BusinessDate
            .ApplicationID = Me.ApplicationID
            .BranchId = Me.BranchID

        End With

        Try
            m_InvChgExpDate.InvChgExpDateSave(oInvChgExpDate)            
            ShowMessage(lblMessage, "Simpan Data Berhasil", False)
        Catch ex As Exception            
            ShowMessage(lblMessage, ex.Message.Trim, True)
        End Try


        Bindgrid(Me.SearchBy, Me.SortBy)
        PnlSearch.Visible = True
        PnlList.Visible = True
        PnlListDetail.Visible = False
    End Sub

    Private Sub dtg_ItemCommand1(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtg.ItemCommand
        Me.AgreementNo = e.Item.Cells(0).Text.Trim
        Me.ApplicationID = e.Item.Cells(1).Text.Trim
        Me.BranchID = e.Item.Cells(2).Text.Trim

        If e.CommandName = "REQUEST" Then
            If checkFeature(Me.Loginid, Me.FormID, "Req", Me.AppId) Then
                If sessioninvalid() Then
                    Exit Sub
                End If
            End If
            Me.PrevInvExpDate = ConvertDate2(e.Item.Cells(7).Text)
            viewRequestDetail(Me.BranchID, Me.ApplicationID)
        End If
    End Sub

    Private Sub dtg_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtg.ItemDataBound

    End Sub
End Class