﻿#Region "Imports"
'Imports CrystalDecisions.Shared
'Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
'Imports Maxiloan.BusinessProcess
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Parameter.InvChgExpDate
#End Region

Public Class ChangeInvDateDetail
    Inherits Maxiloan.Webform.WebBased

#Region " Private Const "
    Dim m_InvChgExpDate As New ChgInvExpDateController
#End Region

    Public Property ApplicationID() As String
        Get
            Return CStr(viewstate("ApplicationID"))
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property
End Class