﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ChangeInventoryDateReq.aspx.vb"
    Inherits="Maxiloan.Webform.ARMgt.ChangeInventoryDateReq" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcBranchCollection" Src="../../../Webform.UserController/UcBranchCollection.ascx" %>
<%@ Register Src="../../../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ChangeInventoryDateReq</title>
    <script language="javascript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;
        var ServerName = '<%#Request.ServerVariables("SERVER_NAME")%>';
        var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);

        function fback() {
            history.go(-1);
            return false;
        }

        function OpenWinCust(pStrCustID, pStrType) {
            var lStrAddress;
            if (pStrType == 'P')
                lStrAddress = 'ViewSearch.aspx';
            else
                lStrAddress = 'ViewSearchCompany.aspx';

            window.open('http://' + ServerName + '/' + App + '/Application/SmartSearch/' + lStrAddress + '?custid=' + pStrCustID, 'WinLink', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0,scrollbars=1,resizable=1,center=1');
        }

        function OpenWinAgreement(pStrAgreementNo) {
            window.open('http://' + ServerName + '/' + App + '/Application/SmartSearch/ViewAgreementDetail.aspx?AgreementNo=' + pStrAgreementNo, 'WinLink', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0,scrollbars=1,resizable=1,center=1');
        }

        function OpenWinAsset(pStrApplicationID, pStrAgreementNo, asq) {
            window.open('http://' + ServerName + '/' + App + '/Application/SmartSearch/perAsset.aspx?applicationid=' + pStrApplicationID + '&agreementno=' + pStrAgreementNo + '&assetseqno=' + asq, 'WinLink', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=1,resizable=1,center=1');
        }
         

        function OpenViewAgreement(pAgreementNo) {
            var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            window.open('http://<%#Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.LoanOrg/Credit/ViewStatementofAccount.aspx?AgreementNo=' + pAgreementNo + '&Style=Collection', 'AgreementView', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenViewCustomer(pCustomerID) {
            var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            window.open('http://<%#Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.LoanOrg/Credit/ViewPersonalCustomer.aspx?CustomerID=' + pCustomerID + '&Style=Collection', 'CustomerView', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }

    </script>
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../../Include/General.css" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>    
        <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"/>
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">           
                <h3>
                    PENGAJUAN GANTI TANGGAL MASUK PERSEDIAAN
                </h3>
            </div>
        </div>  
        <asp:Panel ID="PnlSearch" runat="server">  
        <div class="form_box">
	        <div class="form_single">
                <label>Collection Group </label>
                <uc1:UcBranchCollection id="oCG" runat="server"></uc1:UcBranchCollection>	        
            </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Cari Berdasarkan</label>
                <asp:DropDownList ID="cboSearchBy" runat="server">
                    <asp:ListItem Value="Select One">Select One</asp:ListItem>
                    <asp:ListItem Value="CollectionAgreement.AgreementNo">No Kontrak</asp:ListItem>
                    <asp:ListItem Value="Customer.Name">Nama Customer</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearchBy" runat="server" ></asp:TextBox>
	        </div>
        </div>
        <div class="form_button">
             <asp:Button ID="Buttonsearch" runat="server"  Text="Search" CssClass ="small button blue">
            </asp:Button>&nbsp;
            <asp:Button ID="ButtonReset" runat="server"  Text="Reset" CssClass ="small button gray"
                CausesValidation="False"></asp:Button>
	    </div>                                    
        <!--<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<DATA GRID>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>-->
        </asp:Panel>
        <asp:Panel ID="PnlList" runat="server">
        <div class="form_box_title">
            <div class="form_single">              
                <h4>
                    PENGAJUAN GANTI TANGGAL MASUK INVENTORY
                </h4>
            </div>
        </div>                    
     
        <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtg" runat="server" Width="100%" BorderStyle="None" BorderWidth="0"
                        AllowSorting="True" AutoGenerateColumns="False" BorderColor="#CCCCCC" OnSortCommand="Sorting" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                        <asp:BoundColumn Visible="False" DataField="AgreementNo"  />
                        <asp:BoundColumn Visible="False" DataField="ApplicationID" />
                        <asp:BoundColumn Visible="False" DataField="BranchID" />
                        <asp:TemplateColumn HeaderText="PILIH">                    
                            <ItemTemplate>
                                <asp:ImageButton ID="imbRequest" runat="server" ImageUrl="../../../images/iconrelease.gif" CommandName="REQUEST" Visible='<%#iif(container.dataitem("ApprovalStatus")<>"REQ",true,false)%>' />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">                    
                            <ItemTemplate>
                                <asp:LinkButton ID="hpAgreementNo" runat="server" Text='<%#container.dataitem("AgreementNo")%>' Enabled="True" Visible="True" />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="CustomerName" HeaderText="NAMA CUSTOMER">
                            <ItemTemplate>
                                <asp:LinkButton ID="hpCustomer" runat="server" Text='<%# container.dataitem("CustomerName")%>' Enabled="True" Visible="True" />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="AssetDescription" SortExpression="AssetDescription" HeaderText="NAMA ASSET" />
                        <asp:BoundColumn DataField="InvExpdate" SortExpression="InvExpdate" HeaderText="TGL MASUK INV" />
                        <asp:BoundColumn DataField="ApprovalStatus" SortExpression="ApprovalStatus" HeaderText="STATUS REQ" />
                          <asp:BoundColumn Visible="False" DataField="PerpanjanganSKE"  />
                          <asp:BoundColumn Visible="False" DataField="RALExtension"  />
                          <asp:BoundColumn Visible="False" DataField="FreqPerpanjanganSKE"  />
                    </Columns>            
                </asp:DataGrid>   
                 <uc2:ucGridNav id="GridNavigator" runat="server"/>   
              <%--       
                <div class="button_gridnavigation">
	                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../../Images/grid_navbutton01.png"
		                CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../../Images/grid_navbutton02.png"
		                CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../../Images/grid_navbutton03.png"
		                CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../../Images/grid_navbutton04.png"
		                CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
	                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
		                EnableViewState="False"></asp:Button>
	                <asp:RangeValidator   CssClass="validator_general"   ID="rgvGo" runat="server" ControlToValidate="txtPage"
                        MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer"></asp:RangeValidator>
                    <asp:RequiredFieldValidator    CssClass="validator_general"   ID="rfvGo" runat="server" ControlToValidate="txtPage"
                        ErrorMessage="No Halaman Salah" Display="Dynamic"></asp:RequiredFieldValidator>    
                </div>
                <div class="label_gridnavigation">
	                <asp:Label ID="lblPage" runat="server"/>of
	                <asp:Label ID="lblTotPage" runat="server"/>, Total
	                <asp:Label ID="lblrecord" runat="server"/>record(s)
                </div>--%>
            </div>
        </div>       
        </div> 
        </asp:Panel>
        <asp:Panel ID="PnlListDetail" runat="server">
        <div class="form_box_title">
            <div class="form_single">              
                <h4> PENGAJUAN GANTI TANGGAL MASUK INVENTORY </h4>
            </div>
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <label>No Kontrak</label>
                    <asp:HyperLink ID="hypAgreementNo" runat="server" />
		        </div>
		        <div class="form_right">
                    <label>Nama Customer</label>	
                    <asp:HyperLink ID="hypCustomerName" runat="server" />
		        </div>	       
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Alamat</label>
                <asp:Label ID="lblAddress" runat="server"/>
	        </div>
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <label>Angsuran Ke</label>
                    <asp:Label ID="lblInstallmentNo" runat="server"/>
		        </div>
		        <div class="form_right">			
                    <label>Jumlah Angsuran</label>
                    <asp:Label ID="lblInstallmentAmount" runat="server"/>
		        </div>	        
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <label>Tanggal Jatuh Tempo</label>
                    <asp:Label ID="lblDuedate" runat="server"/>
		        </div>
		        <div class="form_right">	
                    <label>Jumlah Hari Telat</label>		
                    <asp:Label ID="lblOverDueDays" runat="server"/>
		        </div>	        
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <label>Jumlah Tunggakan</label>
                    <asp:Label ID="lblODAmount" runat="server"/>
		        </div>
		        <div class="form_right">	
                    <label>Denda Keterlambatan</label>		
                    <asp:Label ID="lblLateCharges" runat="server"/>
		        </div>	        
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <label>Biaya Tagih</label>
                    <asp:Label ID="lblOSBillingCharges" runat="server"/>
		        </div>
		        <div class="form_right">	
                    <label>Biaya Collection</label>		
                    <asp:Label ID="lblCollExpense" runat="server"/>
		        </div>	        
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <label>Sisa A/R</label>
                    <asp:Label ID="lblOSBallance" runat="server"/>
		        </div>
		        <div class="form_right">			
		        </div>	        
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Nama Asset</label>
                <asp:Label ID="lblAsset" runat="server"/>
	        </div>
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <label>No Rangka</label>
                    <asp:Label ID="lblChassisNo" runat="server"/>
		        </div>
		        <div class="form_right">	
                    <label>No Mesin</label>		
                    <asp:Label ID="lblEngineNo" runat="server"/>
		        </div>	        
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <label>No Polisi</label>
                    <asp:Label ID="lblLicensePlate" runat="server"/>
		        </div>
		        <div class="form_right">		
                    <label>Tahun Produksi</label>	
                    <asp:Label ID="lblYear" runat="server"/>
		        </div>	        
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <label>Warna</label>
                    <asp:Label ID="lblColor" runat="server"/>
		        </div>
		        <div class="form_right">			
		        </div>	        
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <label>Tanggal Tarik</label>
                    <asp:Label ID="lblRepossesDate" runat="server"/>
		        </div>
		        <div class="form_right">	
                    <label>Tanggal Masuk Inventory</label>	
                    <asp:Label ID="lblExpectedDate" runat="server"/>	
		        </div>	        
        </div>
        <div class="form_box">
	        <div class="form_single">                
                <label class ="label_req">Tgl Masuk Inventory Baru </label>               
                <asp:TextBox ID="txtNewInvExpDate" runat="server"></asp:TextBox>                   
                <asp:CalendarExtender ID="txtNewInvExpDate_CalendarExtender" runat="server" 
                    Enabled="True" TargetControlID="txtNewInvExpDate" Format ="dd/MM/yyyy"></asp:CalendarExtender>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*" CssClass="validator_general"
                ControlToValidate="txtNewInvExpDate"></asp:RequiredFieldValidator>       
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label class ="label_req">Alasan </label>
                <asp:DropDownList ID="cboReason" runat="server">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" CssClass="validator_general"
                Display="Dynamic" InitialValue="0" ControlToValidate="cboReason" Enabled="True"
                ErrorMessage="Harap pilih Alasan"></asp:RequiredFieldValidator>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label class ="label_req">Disetujui Oleh </label>
                <asp:DropDownList ID="cboApproveBy" runat="server">
                </asp:DropDownList>
               <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"  CssClass="validator_general"
                    Display="Dynamic" InitialValue="0" ControlToValidate="cboApproveBy" ErrorMessage="Harap pilih disetujui Oleh"></asp:RequiredFieldValidator>--%>
	        </div>
        </div>
<%--        <div class="form_box">
	        <div class="form_single">
                <label>Approval Dikirim melalui</label>
                <asp:CheckBoxList ID="chkApprovalVia" runat="server" Width="192px" RepeatDirection="Horizontal">
                    <asp:ListItem Value="SMS">SMS</asp:ListItem>
                    <asp:ListItem Value="E-mail">E-mail</asp:ListItem>
                    <asp:ListItem Value="Fax">Fax No.</asp:ListItem>
                </asp:CheckBoxList>
                <asp:TextBox ID="txtFaxNo" runat="server" ></asp:TextBox>
	        </div>
        </div>--%>
        <div class="form_box">
	        <div class="form_single">                            
                <label class="label_general">Catatan</label>
                <asp:TextBox ID="txtNotes" runat="server" CssClass="multiline_textbox" TextMode="MultiLine"></asp:TextBox>
	        </div>
        </div>       
        <div class="form_button">
            <asp:Button ID="ButtonSave" runat="server"  Text="Save" CssClass ="small button blue" />            
            <asp:Button ID="ButtonCancel" runat="server"  Text="Cancel" CssClass ="small button gray" CausesValidation="False" />
	    </div>
    </asp:Panel>              
    </form>
</body>
</html>
