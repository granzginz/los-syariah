﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ChangeInvDateDetail.aspx.vb"
    Inherits="Maxiloan.Webform.ARMgt.ChangeInvDateDetail" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ChangeInvDateDetail</title>
    <script language="javascript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;
        function windowClose() {
            window.close();
        }
        function fback() {
            history.go(-1);
            return false;
        }
        function OpenViewAgreement(pAgreementNo) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.LoanOrg/Credit/ViewStatementofAccount.aspx?AgreementNo=' + pAgreementNo + '&Style=Collection', 'AgreementView', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenViewCustomer(pCustomerID) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.LoanOrg/Credit/ViewPersonalCustomer.aspx?CustomerID=' + pCustomerID + '&Style=Collection', 'CustomerView', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }
    </script>
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../../Include/General.css" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:Panel ID="PnlListDetail" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">           
                <h3>
                    PENGAJUAN GANTI TANGGAL MASUK PERSEDIAAN
                </h3>
            </div>
        </div>  
        <div class="form_box">	        
                <div class="form_left">
                    <label>No Kontrak</label>
                    <asp:HyperLink ID="hypAgreementNo" runat="server"></asp:HyperLink>                        
		        </div>
		        <div class="form_right">	
                    <label>Nama Customer</label>		
                    <asp:HyperLink ID="hypCustomerName" runat="server"></asp:HyperLink>
		        </div>	        
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Alamat</label>
                <asp:Label ID="lblAddress" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">	       
                <div class="form_left">
                    <label>Angsuran Ke</label>
                    <asp:Label ID="lblInstallmentNo" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">		
                    <label>Jumlah Angsuran</label>	
                    <asp:Label ID="lblInstallmentAmount" runat="server"></asp:Label>
		        </div>	        
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <label>Tanggak Jatuh Tempo</label>
                    <asp:Label ID="lblDuedate" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">	
                    <label>Jumlah Hari Telat</label>	
                    <asp:Label ID="lblOverDueDays" runat="server"></asp:Label>	
		        </div>	        
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <label>Jumlah Tunggakan</label>
                    <asp:Label ID="lblODAmount" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">		
                    <label>Denda Keterlambatan</label>	
                    <asp:Label ID="lblLateCharges" runat="server"></asp:Label>
		        </div>	        
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <label>Biaya Tagih</label>
                    <asp:Label ID="lblOSBillingCharges" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">			
                    <label>Biaya Tarik</label>
                    <asp:Label ID="lblCollExpense" runat="server"></asp:Label>
		        </div>	        
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <label>Sisa A/R</label>
                    <asp:Label ID="lblOSBallance" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">			
		        </div>	        
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Nama Asset</label>
                <asp:Label ID="lblAsset" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <label>No Rangka</label>
                    <asp:Label ID="lblChassisNo" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">	
                    <label>No Mesin</label>		
                    <asp:Label ID="lblEngineNo" runat="server"></asp:Label>
		        </div>	        
        </div>
        <div class="form_box">
	        <div class="form_single">
                <div class="form_left">
                    <label>No Polisi</label>
                    <asp:Label ID="lblLicensePlate" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">		
                    <label>Tahun Produksi</label>
                    <asp:Label ID="lblYear" runat="server"></asp:Label>	
		        </div>
	        </div>
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <label>Warna</label>
                    <asp:Label ID="lblColor" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">			
		        </div>	        
        </div>
        <div class="form_box">	        
                <div class="form_left"> 
                    <label>Tanggal Tarik</label>
                    <asp:Label ID="lblRepossesDate" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">			
                    <label>Tanggal Masuk Inventory</label>
                    <asp:Label ID="lblExpectedDate" runat="server"></asp:Label>
		        </div>	        
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Tanggal Masuk Inventory Baru</label>
                <asp:Label ID="lblNewExpDate" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Alasan</label>
                <asp:Label ID="lblReason" runat="server" ForeColor="Black"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Disetujui Oleh</label>
                <asp:Label ID="lblApproveBy" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Catatan</label>
                <asp:Label ID="lblNotes" runat="server" ForeColor="Black"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Tanggal Pengajuan</label>
                <asp:Label ID="lblRequestDate" runat="server" ForeColor="Black"></asp:Label>
	        </div>
        </div>
        <div class="form_button">
            <asp:Button OnClientClick="windowClose();" ID="ButtonClose" runat="server"  Text="Close" CssClass ="small button gray"
            CausesValidation="False"></asp:Button>
	    </div>        
    </asp:Panel>
    </form>
</body>
</html>
