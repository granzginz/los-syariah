﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class ExecutorHistory
    Inherits Maxiloan.Webform.WebBased

    Private Property CGID() As String
        Get
            Return CType(viewstate("CGID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CGID") = Value
        End Set
    End Property

    Private Property ExecutorID() As String
        Get
            Return CType(viewstate("ExecutorID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ExecutorID") = Value
        End Set
    End Property
    Private Property ExecutorName() As String
        Get
            Return CType(viewstate("ExecutorName"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ExecutorName") = Value
        End Set
    End Property

    Private oCustomClass As New Parameter.RALPrinting
    Private oController As New RALPrintingController

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            'If checkfeature(Me.Loginid, Me.FormID, "Hist", "Maxiloan") Then
            If Request.QueryString("CGID") <> "" Then Me.CGID = Request.QueryString("CGID")
            If Request.QueryString("ExecutorID") <> "" Then Me.ExecutorID = Request.QueryString("ExecutorID")
            If Request.QueryString("ExecutorName") <> "" Then Me.ExecutorName = Request.QueryString("ExecutorName")
            lblCGID.Text = Me.CGID
            lblExecutorID.Text = Me.ExecutorID.Trim & " - " & Me.ExecutorName.Trim
            FillData()
            'End If
        End If
    End Sub
    Private Sub FillData()
        Dim oDataTable As New DataTable
        With oCustomClass
            .strConnection = getConnectionString
            .ExecutorID = Me.ExecutorID.Trim
        End With
        oCustomClass = oController.RALViewHistoryExecutor(oCustomClass)
        oDataTable = oCustomClass.ListRAL
        dtgRALPrinting.DataSource = oDataTable
        dtgRALPrinting.DataBind()
    End Sub

End Class