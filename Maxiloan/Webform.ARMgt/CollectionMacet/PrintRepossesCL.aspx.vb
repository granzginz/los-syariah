﻿#Region "Imports"
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
Imports Maxiloan.Parameter.AssetRelease
#End Region

Public Class PrintRepossesCL
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oCGID As UcBranchCollection

#Region "properties"


    Public Property RepossesSeqNo() As Integer
        Get
            Return CType(Viewstate("RepossesSeqNo"), Integer)
        End Get
        Set(ByVal Value As Integer)
            Viewstate("RepossesSeqNo") = Value
        End Set
    End Property

    Public Property AssetSeqNo() As Integer
        Get
            Return CType(Viewstate("AssetSeqNo"), Integer)
        End Get
        Set(ByVal Value As Integer)
            Viewstate("AssetSeqNo") = Value
        End Set
    End Property

    Public Property UntilDate() As Date
        Get
            Return CType(Viewstate("UntilDate"), Date)
        End Get
        Set(ByVal Value As Date)
            Viewstate("UntilDate") = Value
        End Set
    End Property

    Public Property RALDate() As Date
        Get
            Return CType(Viewstate("RALDate"), Date)
        End Get
        Set(ByVal Value As Date)
            Viewstate("RALDate") = Value
        End Set
    End Property

    Public Property RALEndDate() As Date
        Get
            Return CType(Viewstate("RALEndDate"), Date)
        End Get
        Set(ByVal Value As Date)
            Viewstate("RALEndDate") = Value
        End Set
    End Property

    Public Property AssetTypeID() As String
        Get
            Return CType(Viewstate("AssetTypeID"), String)
        End Get
        Set(ByVal Value As String)
            Viewstate("AssetTypeID") = Value
        End Set
    End Property

    Public Property AllPage() As String
        Get
            Return CType(Viewstate("AllPage"), String)
        End Get
        Set(ByVal Value As String)
            Viewstate("AllPage") = Value
        End Set
    End Property



    Public Property cgid() As String
        Get
            Return CType(viewstate("CGID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CGID") = Value
        End Set
    End Property


    Public Property AgreementNo() As String
        Get
            Return CType(viewstate("AgreementNo"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AgreementNo") = Value
        End Set
    End Property

    Public Property CustomerID() As String
        Get
            Return CType(viewstate("CustomerID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerID") = Value
        End Set
    End Property

    Public Property ApplicationID() As String
        Get
            Return CType(viewstate("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property


    Public Property RALNo() As String
        Get
            Return CType(viewstate("RALNo"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("RALNo") = Value
        End Set
    End Property

    Public Property CollectorID() As String
        Get
            Return CType(viewstate("CollectorID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CollectorID") = Value
        End Set
    End Property

    Public Property CollectorName() As String
        Get
            Return CType(viewstate("CollectorName"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CollectorName") = Value
        End Set
    End Property

    Public Property CollectorType() As String
        Get
            Return CType(viewstate("CollectorType"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CollectorType") = Value
        End Set
    End Property
    Public Property ReportType() As String
        Get
            Return CType(viewstate("ReportType"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ReportType") = Value
        End Set
    End Property



#End Region
#Region " Private Const "
    Dim m_Controller As New AssetRepoController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region
#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If

    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        Bindgrid(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click

        If txtPage.Text = "" Then
            txtPage.Text = "0"
        Else
            If IsNumeric(txtPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                    currentPage = CType(txtPage.Text, Int32)
                    Bindgrid(Me.SearchBy, Me.SortBy)
                End If
            End If
        End If


    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        Bindgrid(Me.SearchBy, Me.SortBy)
    End Sub


#End Region
#Region "Page_Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        'Put user code to initialize the page here
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not Me.IsPostBack Then
            If Request("file") <> "" Then
                Dim strFileLocation As String = "../../XML/" & Request.QueryString("file")
                Response.Write("<script language = javascript>" & vbCrLf _
                                & "var x = screen.width; " & vbCrLf _
                                & "var y = screen.height; " & vbCrLf _
               & "window.open('" & strFileLocation & "','accacq', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes, resizable=1') " & vbCrLf _
               & "</script>")
            End If
            Me.FormID = "PRepossesCL"
            Me.cgid = Me.GroubDbID
            InitialDefaultPanel()
        End If
    End Sub
#End Region
#Region " InitialDefaultPanel "
    Private Sub InitialDefaultPanel()
        PnlSearch.Visible = True
        PnlList.Visible = False
    End Sub
#End Region
#Region "Bindgrid"
    Sub Bindgrid(ByVal cmdWhere As String, ByVal cmSort As String)
        Dim dtvEntity As New DataView
        Dim dtsEntity As New DataTable
        Dim oAssetRepoCL As New Parameter.AssetRepo

        With oAssetRepoCL
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = cmSort
        End With

        oAssetRepoCL = m_Controller.AssetRepoCLPrintList(oAssetRepoCL)

        With oAssetRepoCL
            lblrecord.Text = CType(.TotalRecord, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecord
        End With


        dtsEntity = oAssetRepoCL.listData

        dtvEntity = dtsEntity.DefaultView
        dtvEntity.Sort = cmSort
        dtg.DataSource = dtvEntity
        Try
            dtg.DataBind()
        Catch ex As Exception
            dtg.CurrentPageIndex = 0
            dtg.DataBind()
        End Try
        PnlList.Visible = True
        PagingFooter()
    End Sub
#End Region
#Region "imbReset"
    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        InitialDefaultPanel()
        cboSearchBy.ClearSelection()
        txtSearchBy.Text = ""
        Me.SearchBy = ""
        Me.SortBy = ""
        Me.Bindgrid(Me.SearchBy, Me.SortBy)
        PnlSearch.Visible = True
        PnlList.Visible = True        
    End Sub
#End Region
#Region "imgSearch"
    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Buttonsearch.Click
        Me.SearchBy = ""
        Me.SortBy = ""        

        If oCGID.BranchID.Trim <> "" Then
            Me.SearchBy = "AssetRepossessionOrder.CGID='" & oCGID.BranchID & "'"
            Me.cgid = oCGID.BranchID
        End If

        If cboSearchBy.SelectedIndex <> 0 And txtSearchBy.Text <> "" Then
            If Me.SearchBy = "" Then
                Me.SearchBy = cboSearchBy.SelectedItem.Value.Trim & " like '%" + txtSearchBy.Text.Trim + "%'"
            Else
                Me.SearchBy = Me.SearchBy & " and " & cboSearchBy.SelectedItem.Value.Trim & " like '%" + txtSearchBy.Text.Trim + "%'"
            End If
        End If

        Bindgrid(Me.SearchBy, Me.SortBy)
        PnlList.Visible = True
    End Sub
#End Region
#Region "dtg_ItemCommand"
    Private Sub dtg_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtg.ItemCommand
        'If sessioninvalid() Then
        '    Exit Sub
        'End If

        If e.CommandName = "PrintCheckList" Then

            'If e.Item.ItemIndex >= 0 Then
            Dim hynAgreementNo As HyperLink
            Dim lblExecutorID As Label
            Dim lblApplicationID As Label
            hynAgreementNo = CType(dtg.Items(e.Item.ItemIndex).FindControl("hynAgreementNo"), HyperLink)
            lblExecutorID = CType(dtg.Items(e.Item.ItemIndex).FindControl("lblExecutorID"), Label)
            lblApplicationID = CType(dtg.Items(e.Item.ItemIndex).FindControl("lblApplicationID"), Label)
            Me.AgreementNo = hynAgreementNo.Text.Trim

            If checkFeature(Me.Loginid, Me.FormID, "Print", Me.AppId) Then
                Me.ReportType = "PrintCheckList"
                Dim cookie As HttpCookie = Request.Cookies(COOKIES_ASSETREPOCHECKLISTRPT)
                If Not cookie Is Nothing Then
                    'cookie.Values("ExecutorID") = CType(dtg.Items(e.Item.ItemIndex).FindControl("lblExecutorID"), Label).Text.Trim
                    'cookie.Values("ApplicationId") = CType(dtg.Items(e.Item.ItemIndex).FindControl("lblApplicationID"), Label).Text.Trim
                    cookie.Values("ExecutorID") = lblExecutorID.Text.Trim
                    cookie.Values("ApplicationId") = lblApplicationID.Text.Trim
                    cookie.Values("ReportType") = Me.ReportType.Trim
                    Response.AppendCookie(cookie)
                Else
                    Dim cookieNew As New HttpCookie(COOKIES_ASSETREPOCHECKLISTRPT)
                    'cookieNew.Values.Add("ExecutorID", CType(dtg.Items(e.Item.ItemIndex).FindControl("lblExecutorID"), Label).Text.Trim)
                    'cookieNew.Values.Add("ApplicationId", CType(dtg.Items(e.Item.ItemIndex).FindControl("lblApplicationID"), Label).Text.Trim)
                    cookieNew.Values.Add("ExecutorID", lblExecutorID.Text.Trim)
                    cookieNew.Values.Add("ApplicationId", lblApplicationID.Text.Trim)
                    cookieNew.Values.Add("ReportType", Me.ReportType.Trim)
                    Response.AppendCookie(cookieNew)
                End If
                Response.Redirect("PrintCheckListViewer.aspx")
            End If

            'End If
        ElseIf e.CommandName = "PrintPrepaymentLetter" Then
            'If e.Item.ItemIndex >= 0 Then
            Dim hynAgreementNo As HyperLink
            Dim lblExecutorID As Label
            Dim lblApplicationID As Label
            hynAgreementNo = CType(dtg.Items(e.Item.ItemIndex).FindControl("hynAgreementNo"), HyperLink)
            lblExecutorID = CType(dtg.Items(e.Item.ItemIndex).FindControl("lblExecutorID"), Label)
            lblApplicationID = CType(dtg.Items(e.Item.ItemIndex).FindControl("lblApplicationID"), Label)
            Me.AgreementNo = hynAgreementNo.Text.Trim

            If checkFeature(Me.Loginid, Me.FormID, "Print", Me.AppId) Then

                Me.ReportType = "PrintPrepaymentLetter"
                Dim cookie As HttpCookie = Request.Cookies(COOKIES_ASSETREPOCHECKLISTRPT)
                If Not cookie Is Nothing Then
                    'cookie.Values("ExecutorID") = CType(dtg.Items(e.Item.ItemIndex).FindControl("lblExecutorID"), Label).Text.Trim
                    'cookie.Values("ApplicationId") = CType(dtg.Items(e.Item.ItemIndex).FindControl("lblApplicationID"), Label).Text.Trim
                    cookie.Values("ExecutorID") = lblExecutorID.Text.Trim
                    cookie.Values("ApplicationId") = lblApplicationID.Text.Trim
                    cookie.Values("ReportType") = Me.ReportType.Trim
                    Response.AppendCookie(cookie)
                Else
                    Dim cookieNew As New HttpCookie(COOKIES_ASSETREPOCHECKLISTRPT)
                    'cookieNew.Values.Add("ExecutorID", CType(dtg.Items(e.Item.ItemIndex).FindControl("lblExecutorID"), Label).Text.Trim)
                    'cookieNew.Values.Add("ApplicationId", CType(dtg.Items(e.Item.ItemIndex).FindControl("lblApplicationID"), Label).Text.Trim)
                    cookieNew.Values.Add("ExecutorID", lblExecutorID.Text.Trim)
                    cookieNew.Values.Add("ApplicationId", lblApplicationID.Text.Trim)
                    cookieNew.Values.Add("ReportType", Me.ReportType.Trim)
                    Response.AppendCookie(cookieNew)
                End If
                Response.Redirect("PrintCheckListViewer.aspx")
            End If
        End If
    End Sub
#End Region
#Region "dtg_ItemDataBound"
    Private Sub dtg_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtg.ItemDataBound
        'Me.AgreementNo = CType(e.Item.FindControl("hynAgreementNo"), HyperLink).Text.Trim
        'Me.CustomerID = CType(e.Item.FindControl("lblCustomerID"), Label).Text.Trim
        'Me.ApplicationID = CType(e.Item.FindControl("lblApplicationID"), Label).Text.Trim

        Dim hynAgreementNo As New HyperLink
        Dim hynCustomerName As New HyperLink
        Dim lblCustomerID As Label
        Dim lblApplicationID As Label
        If e.Item.ItemIndex >= 0 Then

            '*** Add customer view
            hynCustomerName = CType(e.Item.FindControl("hynCustomerName"), HyperLink)
            lblCustomerID = CType(e.Item.FindControl("lblCustomerID"), Label)
            hynCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "Collection" & "','" & lblCustomerID.Text.Trim & "')"

            hynAgreementNo = CType(e.Item.FindControl("hynAgreementNo"), HyperLink)
            lblApplicationID = CType(e.Item.FindControl("lblApplicationID"), Label)
            hynAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccAcq" & "', '" & lblApplicationID.Text.Trim & "')"
        End If
    End Sub
#End Region

End Class