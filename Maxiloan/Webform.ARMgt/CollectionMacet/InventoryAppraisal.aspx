﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="InventoryAppraisal.aspx.vb" EnableEventValidation="false" Inherits="Maxiloan.Webform.ARMgt.InventoryAppraisal" %>

<%@ Register TagName="ucNumberFormat" TagPrefix="uc1" Src="../../webform.UserController/ucNumberFormat.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>InventoryAppraisal</title>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../js/jquery-1.9.1.js" type="text/javascript"></script>
    <script src="../../js/jquery-ui-1.10.3.custom.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
   
    <script language="javascript" type="text/javascript">
        var bankAccountList;
        var cashAccountList;
        var rebindBankAccount = function (e) {

            $.each(e, function (key, value) {
                $('#<%# ddlRekeningBank.ClientID %>').append($("<option></option>").val(value.AccountNo).html(value.Bank));
            });
        }

        var handleCaraPembayaran_Change = function (e) {
            $('#<%# ddlRekeningBank.ClientID %>').empty();
            $('#<%# ddlRekeningBank.ClientID %>').append($("<option></option>").val('0').html('Select One'));
            switch ($(e).val()) {
                case '0':
                    return;
                default:

                    var paramArg = JSON.stringify({ id: $(e).val() });
                    if ($(e).val() == 'C') {
                        if (cashAccountList && cashAccountList.length > 0) {
                            rebindBankAccount(cashAccountList);
                            return;
                        }
                    } else {
                        if (bankAccountList && bankAccountList.length > 0) {
                            rebindBankAccount(bankAccountList);
                            return;
                        }
                    }

                    $.ajax({
                        url: "InventoryAppraisal.aspx/GetRekeningBank",
                        data: paramArg,
                        type: "POST",
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (dt) {
                            if (dt.d.length != 0) {
                                dtList = dt.d;
                                if ($(e).val() == 'C') {
                                    cashAccountList = dt.d;
                                } else {
                                    bankAccountList = dt.d;
                                }

                                rebindBankAccount(dt.d);
                            }
                        }
                    });

                    break;
            }


        }
        var handlePenawarTerbaik_Change = function (e) {
            var dataRow = $('#<%# dtgBidder.ClientID %> tr:has(td) .NamaPenawar:contains(' + $(e).val() + ')').parent().parent();
            $('#<%# txtHargaPenawarTerbaik.ClientID %>').val(dataRow.find('td')[3].textContent.trim());
        }
        var handleHitungNilaiPenyesuaian = function () {
            var resultValue = 0;
            resultValue += convertToFloat($('#<%#txtEstimationPrice.ClientID%>').val());
    
            resultValue -= convertToFloat($('#<%#txtMarginDealer.ClientID%>').val());
            $('#txtNilaiPenyesuaian').val(numberFormat(resultValue.toString()));
        }
        var convertToFloat = function (mValue) {
            if (mValue == '') {
                mValue = '0';
            }
            var strTemp = mValue.replace(/,/g, '');
            return parseFloat(strTemp);
        }

        function toCommas(yourNumber) {
            var n = yourNumber.toString().split(".");
            n[0] = n[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            return n.join(".");
        }

        function SisaPenghasilan() {
            var inventoryAmount = parseFloat($('#LblInventoryAmount').text().replace(/,/g, ""));

            var estimationPrice = parseFloat($('#txtEstimationPrice_txtNumber').val().replace(/,/g, "")); 
            var grade = $('#ddlGrade option:selected').val();


            var marginDealer = estimationPrice - ((grade / 100) * estimationPrice)
            $('#txtMarginDealer').val(toCommas(marginDealer));
           
            var xx = estimationPrice - marginDealer

            $('#txtNilaiPenyesuaian').val(toCommas(xx));

          var repossessFee  =  parseFloat($('#txtRepossessFee_txtNumber').val().replace(/,/g, ""));
          var koordinasiFee =  parseFloat( $('#txtKoordinasiFee_txtNumber').val().replace(/,/g, ""));
          var mobilisasiFee = parseFloat($('#txtMobilisasiFee_txtNumber').val().replace(/,/g, ""));
          
          var salePrice = xx + repossessFee + koordinasiFee + mobilisasiFee
          
          $('#txtTotalMinSalesPrice').val(toCommas(salePrice));

          var perkiraanRL = inventoryAmount - salePrice; 
          var perkiraanRLtxt = '';
          if (perkiraanRL > 0) {
             
              perkiraanRLtxt = '( ' + toCommas(perkiraanRL)  + ' )';
            
          }
          else {
              perkiraanRLtxt = toCommas(perkiraanRL * -1);
          }

          
            $('#txtPerkiraanRL').val(perkiraanRLtxt)
        }

        $(document).ready(function () {

            $('#txtEstimationPrice_txtNumber').change(function (e) { SisaPenghasilan(); });


            $('#txtRepossessFee_txtNumber').change(function (e) { SisaPenghasilan(); });
            $('#txtKoordinasiFee_txtNumber').change(function (e) { SisaPenghasilan(); });
            $('#txtMobilisasiFee_txtNumber').change(function (e) { SisaPenghasilan(); });

            $("#ddlGrade").change(function () { SisaPenghasilan(); });

            if ($('#pnlAppraisalEntry').is(':visible')) { 
                SisaPenghasilan();
            } 
            
        });


    </script>
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%#Request.servervariables("SERVER_NAME")%>/';
    </script>
    <script language="javascript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;
        function OpenWinCustomer(pID) {
            window.open('../../Webform.LoanOrg/Credit/ViewPersonalCustomer.aspx?CustomerID=' + pID + '&style=Collection', 'UserLookup', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div id="jLookupContent" />    
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"/>
            <div class="form_title">
                <div class="title_strip"> </div>
                <div class="form_single">
                    <h3> PENILAIAN ASSET </h3>
                </div>
            </div>
            <asp:Panel ID="pnlsearch" runat="server">
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Collection Group
                        </label>
                        <asp:DropDownList ID="cboCG" runat="server" />
                        <asp:RequiredFieldValidator ID="rfvCG" runat="server" ErrorMessage="*" ControlToValidate="cboCG"
                            CssClass="validator_general" Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Cari Berdasarkan</label>
                        <asp:DropDownList ID="cboSearchBy" runat="server">
                            <asp:ListItem Value="Select One">Select One</asp:ListItem>
                            <asp:ListItem Value="CollectionAgreement.AgreementNo">No Kontrak</asp:ListItem>
                            <asp:ListItem Value="Customer.Name">Nama Customer</asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtSearchBy" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="Buttonsearch" runat="server" Text="Search" CssClass="small button blue">
                    </asp:Button>&nbsp;
                    <asp:Button ID="ButtonReset" runat="server" Text="Reset" CssClass="small button gray"
                        CausesValidation="False"></asp:Button>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlDtGrid" runat="server">
                <div class="form_box_title">
                    <div class="form_single">
                        <h4>
                            DAFTAR PENAWARAN ASSET
                        </h4>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtgInventoryAppraisal" runat="server" Width="100%" AllowSorting="True"
                                AutoGenerateColumns="False" DataKeyField="AgreementNo" OnSortCommand="SortGrid"
                                CssClass="grid_general" BorderStyle="None" BorderWidth="0">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="CETAK">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="ChkAll" runat="server" OnCheckedChanged="ChkStatus_CheckedChanged" AutoPostBack="True" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkPrint" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PENAWARAN">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgSelect" runat="server" CommandName="App" ImageUrl="../../Images/IconDocument.gif" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hypAgreementNodtg" runat="server" Text='<%# Container.Dataitem("AgreementNo")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                      
                                    <asp:TemplateColumn SortExpression="CustomerName" HeaderText="NAMA CUSTOMER">
                                        <ItemTemplate>
                                            <a href="javascript:OpenWinCustomer('<%# Container.Dataitem("CustomerID")%>');">
                                                <asp:Label ID="hypCustomerNamedtg" runat="server" Text='<%# Container.Dataitem("CustomerName")%>' /></a>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="AssetDescription" SortExpression="AssetDescription" HeaderText="NAMA ASSET" />
                                    <asp:BoundColumn DataField="RepossesDate" SortExpression="RepossesDate" HeaderText="TGL TARIK" DataFormatString="{0:dd/MM/yyyy}">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center" Width="8%"></ItemStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="InventoryDate" SortExpression="InventoryDate" HeaderText="TGL INVENTORY" DataFormatString="{0:dd/MM/yyyy}" />
                                    <asp:TemplateColumn Visible="False" SortExpression="ApplicationId" HeaderText="ApplicationId">
                                        <ItemTemplate>
                                            <asp:Label ID="lblApplicationId" runat="server" Text='<%# Container.Dataitem("ApplicationId")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn Visible="False" HeaderText="BranchId">
                                        <ItemTemplate>
                                            <asp:Label ID="lblBranchId" runat="server" Text='<%# Container.Dataitem("BranchID")%>' />
                                            <asp:Label ID="lblCustomerId" runat="server" Text='<%# Container.Dataitem("CustomerID")%>' /> 
                                            <asp:Label ID="ApprovalStatus" runat="server" Text='<%# Container.Dataitem("ApprovalStatus")%>' /> 
                                            <asp:Label ID="lblAssetSeqNo" runat="server" Text='<%# Container.Dataitem("AssetSeqNo")%>' /> 
                                            <asp:Label ID="lblrepossesseqno" runat="server" Text='<%# Container.Dataitem("repossesseqNo")%>' /> 
                                            <asp:Label runat="server" ID="lblAssetCode" Text='<%# Container.Dataitem("AssetCode")%>' Visible="false" />
                                            <asp:Label runat="server" ID="lblManufacturingYear" Text='<%# Container.Dataitem("ManufacturingYear")%>' Visible="false" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                             <uc2:ucGridNav id="GridNavigator" runat="server"/>    
                        </div>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonPrint" Visible="false" runat="server" Text="Print" CssClass="small button blue" CommandName="Print" />
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlAppraisalEntry" runat="server">
                <div class="form_box">
                    <div class="form_left">
                        <label>No Kontrak</label>
                        <asp:HyperLink ID="hypAgreementNoSlct" runat="server" />
                    </div>
                    <div class="form_right">
                        <label>Nama Customer</label>
                        <asp:HyperLink ID="hypCustomerNameSlct" runat="server" />
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>Nama Asset</label>
                        <asp:Label ID="lblAsset" runat="server"/>
                    </div>
                    <div class="form_right">
                        <label>
                            Tanggal Tarik</label>
                        <asp:Label ID="lblRepossessDate" runat="server"/>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Ditarik Oleh</label>
                        <asp:Label ID="lblrepossessby" runat="server"/>
                    </div>
                    <div class="form_right">
                        <label>
                            Tanggal Inventory</label>
                        <asp:Label ID="lblInventoryDate" runat="server"/>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Nilai Inventory</label>
                        <asp:Label ID="LblInventoryAmount" runat="server"/>
                    </div>
                    <div class="form_right">
                        <asp:Label ID="lblassetseqno1" runat="server" Visible="False"/>
                        <asp:Label ID="lblrepossesseqno1" runat="server" Visible="False"/>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <h4>
                            PERKIRAAN HARGA JUAL
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>Estimasi Market Value</label> 
                        <uc1:ucnumberformat id="txtEstimationPrice" runat="server" /> <%--autopostback="false" onclientblur="handleHitungNilaiPenyesuaian();" />--%>
                    </div>
                    <div class="form_right">
                        <label class="label_req">Tanggal Estimasi</label>
                        <asp:TextBox ID="txtEstimationDate" runat="server" />
                        <asp:CalendarExtender ID="txtEstimationDate_CalendarExtender" runat="server" Enabled="True" TargetControlID="txtEstimationDate" Format="dd/MM/yyyy" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*" CssClass="validator_general" ControlToValidate="txtEstimationDate" />
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left" >
                      <label  class="label_req">Asset Grade   <asp:DropDownList runat="server" ID="ddlGrade"   style='margin-left: 10px; width: 120px;' /> </label>
                       <div style ='border-bottom: 2px solid; display: inline; padding-bottom: 6px;' > 
                       <asp:TextBox runat="server"  ID="txtMarginDealer" Enabled="false" CssClass="th_right" />
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*" ControlToValidate="ddlGrade" CssClass="validator_general" InitialValue="0" Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>
                    <div class="form_right">
                        <label>Estimasi Oleh</label>
                        <asp:TextBox ID="txtEstimatedBy" runat="server"></asp:TextBox>
                        <%--<label>MRP</label>
                        <uc1:ucnumberformat id="txtMRP" runat="server" />--%>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                      <label> </label>
                        <asp:TextBox runat="server" ID="txtNilaiPenyesuaian" Enabled="false" CssClass="th_right" /> 
                    </div>
                    <div class="form_right" >
                        <label class="label_req">Disetujui Oleh</label>
                        <asp:DropDownList ID="cboApprove" runat="server" /> 
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4"  runat="server" ErrorMessage="*" ControlToValidate="cboApprove" CssClass="validator_general" InitialValue="0" Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                      <label>Biaya Repossess</label> 
                        <uc1:ucnumberformat id="txtRepossessFee" runat="server" />
                    </div>
                    <div class="form_right">
                     <label> Perkiraan Rugi Laba</label>
                      <asp:TextBox runat="server" ID="txtPerkiraanRL" Enabled="false" CssClass="th_right" />    
                    </div>
                </div> 
                <div class="form_box">
                    <div class="form_left">
                      <label>Biaya Koordinasi</label> 
                        <uc1:ucnumberformat id="txtKoordinasiFee" runat="server" />
                    </div>
                    <div class="form_right">
                    </div>
                </div> 
                  <div class="form_box">
                    <div class="form_left">
                      <label>Biaya Mobiliasasi</label> 
                       <div style ='border-bottom: 2px solid; display: inline; padding-bottom: 6px;' >   <uc1:ucnumberformat id="txtMobilisasiFee" runat="server" /> </div>
                    </div>
                    <div class="form_right">
                    </div>
                </div> 
                 <div class="form_box">
                    <div class="form_left">
                      <label>Total Minimum Sales Price</label> 
                      <asp:TextBox runat="server" ID="txtTotalMinSalesPrice" Enabled="false" CssClass="th_right" />   
                    </div>
                    <div class="form_right">
                    </div>
                </div> 
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_general">Catatan</label>
                        <asp:TextBox ID="txtNotes" runat="server" CssClass="multiline_textbox" TextMode="MultiLine"></asp:TextBox>
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlBidder" runat="server">
                <div class="form_box_header">
                    <div class="form_single">
                        <h4>
                            PENAWARAN MASUK
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Nama Penawar</label>
                        <asp:TextBox ID="txtbiddername" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            No. Telepon</label>
                        <asp:TextBox runat="server" ID="txtNoTelepon" />
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label> Harga Penawaran</label> 
                        <uc1:ucnumberformat id="txtbidderamount" runat="server" />
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Form Bidder</label>
                        <asp:DropDownList runat="server" ID="ddlFormBidder">
                            <asp:ListItem Value="1" Text="ADA" Selected="False"></asp:ListItem>
                            <asp:ListItem Value="0" Text="TIDAK ADA"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req"> Tanggal Penawaran</label>
                        <asp:TextBox ID="txtBidderDate" runat="server"></asp:TextBox>
                        <asp:CalendarExtender ID="txtBidderDate_CalendarExtender" runat="server" Enabled="True" TargetControlID="txtBidderDate" Format="dd/MM/yyyy" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*" CssClass="validator_general" ControlToValidate="txtBidderDate" />
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="Buttonadd" runat="server" Text="Add" CssClass="small button blue" CausesValidation="False"></asp:Button>&nbsp;
                    <asp:Button ID="ButtonExit" runat="server" Text="Exit" CssClass="small button gray" CausesValidation="False"></asp:Button>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlBidderGrid" runat="server">
                <div class="form_box_title">
                    <div class="form_single">
                        <h4> DAFTAR PENAWARAN </h4>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtgbidder" runat="server" Width="100%" AutoGenerateColumns="False"
                                CssClass="grid_general" BorderStyle="None" BorderWidth="0">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="NO PENAWARAN">
                                        <ItemTemplate>
                                            <asp:Label ID="lblbidderno" runat="server"/>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="NAMA PENAWAR">
                                        <ItemTemplate>
                                            <asp:Label ID="lblbiddername" runat="server" Text='<%#Container.DataItem("BidderName")%>'
                                                CssClass="NamaPenawar">
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="NO. TELEPON">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNoTelepon" runat="server" Text='<%# Container.DataItem("NoTelepon") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="HARGA PENAWARAN">
                                        <ItemTemplate>
                                            <asp:Label ID="Lblbidderamount" runat="server" Text='<%#FormatNumber(Container.DataItem("BidderAmount"),2)%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="FORM BIDDER">
                                        <ItemTemplate>
                                            <asp:Label ID="lblFormBidder" runat="server" Text='<%# iif(Container.DataItem("FormBidder")="0","TIDAK ADA","ADA") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="BidderDate" HeaderText="TGL PENAWARAN" DataFormatString="{0:dd/MM/yyyy}">
                                    </asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="HAPUS">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbDelete" runat="server" ImageUrl="../../Images/IconDelete.gif"
                                                CausesValidation="False" CommandName="DELETE"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                        </div>
                    </div>
                </div>
                <asp:Panel runat="server" ID="pnlUsulanPenjualan">
                    <div class="form_box_header">
                        <div class="form_single">
                            <h4>
                                USULAN PENJUALAN
                            </h4>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_left">
                            <label>
                                Penawar Terbaik</label>
                            <asp:DropDownList runat="server" ID="ddlPenawarTerbaik" onchange="handlePenawarTerbaik_Change(this);">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="rfvPenawarTerbaik" runat="server" ErrorMessage="*"
                                ControlToValidate="ddlPenawarTerbaik" CssClass="validator_general" Display="Dynamic"
                                InitialValue="Select One"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form_right">
                            <label>
                                Cara Pembayaran</label>
                            <asp:DropDownList runat="server" ID="ddlCaraPembayaran" AutoPostBack="true">  <%--onchange="handleCaraPembayaran_Change(this);"--%>
                                <asp:ListItem Value="0" Text="Select One" Selected="True"></asp:ListItem>
                                <asp:ListItem Value="C" Text="CASH"></asp:ListItem>
                                <asp:ListItem Value="B" Text="BANK"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="rfvCaraPembayaran" runat="server" ErrorMessage="*"
                                ControlToValidate="ddlCaraPembayaran" CssClass="validator_general" Display="Dynamic"
                                InitialValue="0"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_left">
                            <label>Harga</label>
                            <uc1:ucnumberformat id="txtHargaPenawarTerbaik" runat="server" />
                        </div>
                        <div class="form_right">
                            <label>Rekening Bank</label>
                            <asp:DropDownList runat="server" ID="ddlRekeningBank" />
                            <asp:RequiredFieldValidator ID="rfvRekeningBank" runat="server" ErrorMessage="*" ControlToValidate="ddlRekeningBank" CssClass="validator_general" Display="Dynamic" InitialValue="0" />
                        </div>
                    </div>
                </asp:Panel>
                <div class="form_button">
                    <asp:Button ID="ButtonSave" runat="server" Text="Save" CssClass="small button blue" CausesValidation="True" />
                    <asp:Button ID="ButtonCancel" runat="server" Text="Cancel" CssClass="small button gray" CausesValidation="False" />
                </div>
            </asp:Panel>        
    </form>
</body>
</html>
