﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewAppraisalApproval.aspx.vb"
    Inherits="Maxiloan.Webform.ARMgt.ViewAppraisalApproval" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ViewAppraisalApproval</title>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;

        function Close() {
            window.close();
        }
        function fback() {
            history.go(-1);
            return false;
        }

        function OpenViewAgreement(pAgreementNo) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.LoanOrg/Credit/ViewStatementofAccount.aspx?AgreementNo=' + pAgreementNo + '&Style=Collection', 'AgreementView', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenViewCustomer(pCustomerID) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.LoanOrg/Credit/ViewPersonalCustomer.aspx?CustomerID=' + pCustomerID + '&Style=Collection', 'CustomerView', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">           
                <h3>
                    VIEW - PENAWARAN INVENTORY
                </h3>
            </div>
        </div>  
        <asp:Panel ID="PnlListDetail" runat="server">    
        <div class="form_box">
                    <div class="form_left">
                        <label>No Kontrak</label>
                        <asp:HyperLink ID="hypAgreementNoSlct" runat="server" />
                    </div>
                    <div class="form_right">
                        <label>Nama Customer</label>
                        <asp:HyperLink ID="hypCustomerNameSlct" runat="server" />
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>Nama Asset</label>
                        <asp:Label ID="lblAsset" runat="server"/>
                    </div>
                    <div class="form_right">
                        <label>
                            Tanggal Tarik</label>
                        <asp:Label ID="lblRepossessDate" runat="server"/>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Ditarik Oleh</label>
                        <asp:Label ID="lblrepossessby" runat="server"/>
                    </div>
                    <div class="form_right">
                        <label>
                            Tanggal Inventory</label>
                        <asp:Label ID="lblInventoryDate" runat="server"/>
                    </div>
                </div>
               <%-- <div class="form_box">
                    <div class="form_left">
                        <label>
                            Nilai Inventory</label>
                        <asp:Label ID="LblInventoryAmount" runat="server"/>
                    </div>
                    <div class="form_right">
                        <asp:Label ID="lblassetseqno1" runat="server" Visible="False"/>
                        <asp:Label ID="lblrepossesseqno1" runat="server" Visible="False"/>
                    </div>
                </div>--%>
                <div class="form_box_header">
                    <div class="form_single">
                        <h4>
                            PERKIRAAN HARGA JUAL
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>Estimasi Market Value</label> 
                        <asp:Label ID="lblEstimationPrice" runat="server" />
                    </div>
                    <div class="form_right">
                        <label class="">Tanggal Estimasi</label>
                        <asp:Label ID="lblEstimationDate" runat="server" />
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left" >
                      <label >Asset Grade  <asp:Label ID="lblGrade" runat="server" /></label>
                       <asp:Label ID="lblMarginDealer" runat="server"  CssClass="th_right" />
                    </div>
                    <div class="form_right">
                        <label>Estimasi Oleh</label>
                        <asp:Label ID="lblEstimatedBy" runat="server" />
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                      <label>Nilai Penyesuaian</label>
                        <asp:Label ID="lblNilaiPenyesuaian" runat="server" />
                    </div>
                    <div class="form_right">
                        <label>Disetujui Oleh</label>
                        <asp:Label ID="lblApprove" runat="server" />
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                      <label>Biaya Repossess</label> 
                        <asp:Label ID="lblRepossessFee" runat="server" />
                    </div>
                    <div class="form_right">
                  <%--   <label> Perkiraan Rugi Laba</label>
                     <asp:Label ID="lblPerkiraanRL" runat="server" />--%>
                    </div>
                </div> 
                <div class="form_box">
                    <div class="form_left">
                      <label>Biaya Koordinasi</label> 
                        <asp:Label ID="lblKoordinasiFee" runat="server" />
                    </div>
                    <div class="form_right">
                    </div>
                </div> 
                  <div class="form_box">
                    <div class="form_left">
                      <label>Biaya Mobiliasasi</label> 
                       <asp:Label ID="lblMobilisasiFee" runat="server" />
                    </div>
                    <div class="form_right">
                    </div>
                </div> 
                 <div class="form_box">
                    <div class="form_left">
                      <label>Total Minimum Sales Price</label> 
                      <asp:Label ID="lblTotalMinSalesPrice" runat="server" />
                    </div>
                    <div class="form_right">
                    </div>
                </div> 
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_general">Catatan</label>
                        <asp:Label ID="lblNotes" runat="server" />
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlBidder" runat="server">
                <div class="form_box_title">
                    <div class="form_single">              
                        <h4>
                            DAFTAR PENAWARAN
                        </h4>
                    </div>
                </div>
                <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ns">
                        <asp:DataGrid ID="dtgbidder" runat="server" AutoGenerateColumns="false" CssClass="grid_general" BorderStyle="None" BorderWidth="0" Width="100%">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="NO.">                                
                                        <ItemTemplate>
                                            <asp:Label ID="lblbidderno" runat="server" Text='<%#Container.DataItem("BidderNo")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="NAMA PENAWAR">                                
                                        <ItemTemplate>
                                            <asp:Label ID="lblbiddername" runat="server" Text='<%#Container.DataItem("BidderName")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="HARGA PENAWARAN">                                
                                        <ItemTemplate>
                                            <asp:Label ID="Lblbidderamount" runat="server" Text='<%#FormatNumber(Container.DataItem("BidderAmount"),2)%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="BidderDate" HeaderText="TGL PENAWARAN" DataFormatString="{0:dd/MM/yyyy}">                                
                                    </asp:BoundColumn>
                                </Columns>                        
                        </asp:DataGrid>
                    </div>
                </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <h4>
                            USULAN PENJUALAN
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Penawaran Terbaik</label>
                        <asp:Label ID="lblPenawarTerbaik" runat="server" />
                    </div>
                    <div class="form_right">
                        <label>
                            Cara Pembayaran</label>
                            <asp:Label ID="lblCaraPembayaran" runat="server" />
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>Nilai Inventory</label>
                        <asp:Label ID="LblInventoryAmount" runat="server" />
                    </div>
                    <div class="form_right">
                        <label>Rekening Bank</label>
                        <asp:Label ID="lblRekeningBank" runat="server" />
                        <asp:DropDownList runat="server" ID="ddlRekeningBank" Visible="false" />
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>Harga Penawaran</label>
                        <asp:Label ID="lblHargaPenawarTerbaik" runat="server" />
                    </div>
                    <div class="form_right">
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>Nilai Perlunasan Awal</label>
                     <asp:Label ID="lblPerlunasanAwal" runat="server" />
                    </div>
                    <div class="form_right">
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>Perkiraan Rugi Laba</label>
                     <asp:Label ID="lblPerkiraanRL" runat="server" />
                    </div>
                    <div class="form_right">
                    </div>
                </div>
                   
        </asp:Panel>
<div class="form_button">
<asp:Button ID="ButtonClose" OnClientClick="Close();" runat="server"  Text="Close" CssClass ="small button gray">
</asp:Button>
</div>                                      
    
    </form>
</body>
</html>
