﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AssetRepossessionApprove.aspx.vb" Inherits="Maxiloan.Webform.ARMgt.AssetRepossessionApprove" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcBranchCollection" Src="../../Webform.UserController/UcBranchCollection.ascx" %>
<%@ Register TagName="ucNumberFormat" TagPrefix="uc1" Src="../../webform.UserController/ucNumberFormat.ascx" %> 
<%@ Register Src="../../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %> 
<%@ Register TagPrefix="uc6" TagName="ucDateCE" Src="../../Webform.UserController/ucDateCE.ascx" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Asset Repossiesion Approve</title>
     <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
     <script language="javascript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;

        var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%#Request.servervariables("SERVER_NAME")%>/';


        function OpenWinCust(pStrCustID, pStrType) {
            var lStrAddress;
            if (pStrType == 'P')
                lStrAddress = 'ViewSearch.aspx';
            else
                lStrAddress = 'ViewSearchCompany.aspx';

            window.open('http://' + ServerName + '/' + App + '/Application/SmartSearch/' + lStrAddress + '?custid=' + pStrCustID, 'WinLink', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0,scrollbars=1,resizable=1,center=1');
        }

        function OpenWinAgreement(pStrAgreementNo) {
            window.open('http://' + ServerName + '/' + App + '/Application/SmartSearch/ViewAgreementDetail.aspx?AgreementNo=' + pStrAgreementNo, 'WinLink', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0,scrollbars=1,resizable=1,center=1');
        }
</script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" />
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
            <asp:Panel ID="PnlSearch" runat="server" Width="100%">
            <div class="form_title">
                <div class="title_strip"> </div>
                <div class="form_single">
                    <h3> PENARIKAN ASSET </h3>
                </div>
            </div>
            
                <div class="form_box">
                    <div class="form_single">
                        <label>Collection Group </label>
                        &nbsp;<uc1:ucbranchcollection id="oCGID" runat="server"></uc1:ucbranchcollection>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>Cari Berdasarkan</label> 
                        <asp:DropDownList ID="cboSearchBy" runat="server">
                            <asp:ListItem Value="Select One">Select One</asp:ListItem>
                            <asp:ListItem Value="CollectionAgreement.AgreementNo">No Kontrak</asp:ListItem>
                            <asp:ListItem Value="Customer.Name">Nama Customer</asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtSearchBy" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>Tanggal STNK</label>
                        <uc6:ucdatece id="txtTglSTNK" runat="server" />
                    </div>
                </div>

                  
                <div class="form_button">
                    <asp:Button ID="Buttonsearch" runat="server" Text="Search" CssClass="small button blue" /> &nbsp;
                    <asp:Button ID="ButtonReset" runat="server" Text="Reset" CssClass="small button gray" CausesValidation="False" />
                </div>
            </asp:Panel>




             <asp:Panel ID="PnlList" runat="server" Width="100%">
                <div class="form_box_title">
                    <div class="form_single">
                        <h4>
                            DAFTAR PENARIKAN ASSET
                        </h4>
                    </div>
                </div>

                
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="Dtg" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False" OnSortCommand="Sorting" 
                            CssClass="grid_general" BorderStyle="None" BorderWidth="0">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                 
                                 <asp:TemplateColumn HeaderText="PILIH">
                                        <ItemTemplate>  
                                            <asp:LinkButton   ID="hyAction" runat="server" CommandArgument='<%#container.dataitem("AgreementNo")%>'  CommandName="REPOSSESS" Text='APPROVE' CausesValidation='false'></asp:LinkButton >
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                     
                                    <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlAgreement" runat="server" Enabled="True" Text='<%#container.dataitem("AgreementNo")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="CustomerName" HeaderText="NAMA CUSTOMER">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlCustomer" runat="server" Enabled="True" Text='<%# container.dataitem("CustomerName")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateColumn> 
                                    <asp:BoundColumn DataField="AssetDescription" SortExpression="AssetDescription" HeaderText="NAMA ASSET" />
                                    <asp:BoundColumn DataField="EndPastDueDays" SortExpression="EndPastDueDays" HeaderText="HARI TELAT" />
                                    <asp:BoundColumn DataField="Collectorid" SortExpression="Collectorid" HeaderText="EKSEKUTOR" />
                                    <asp:BoundColumn DataField="CollectorName" SortExpression="CollectorName" HeaderText="NAMA EKSEKUTOR" />


                                    <asp:BoundColumn Visible="False" DataField="AgreementNo" />
                                    <asp:BoundColumn Visible="False" DataField="CustomerID" /> 
                                    <asp:BoundColumn Visible="False" DataField="AgreementNo" />
                                    <asp:BoundColumn Visible="False" DataField="RALNo" />
                                    <asp:BoundColumn Visible="False" DataField="AssetSeqNo" />
                                   <asp:BoundColumn Visible="False" DataField="ApplicationID" />
                                   <asp:BoundColumn Visible="False" DataField="BranchId" />
                                   <asp:TemplateColumn Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCheckListPic" runat="server" Text='<%# container.dataitem("CheckListPic")%>' />
                                            <asp:Label ID="lblJobTitlePIC" runat="server" Text='<%# container.dataitem("JobTitlePIC")%>' />
                                            <asp:Label ID="lblCheckerListDate" runat="server" Text='<%# container.dataitem("CheckerListDate")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                             <uc2:ucGridNav id="GridNavigator" runat="server"/>   
                        </div>
                    </div>
                </div>
            </asp:Panel> 


               <asp:Panel ID="PnlListDetail" runat="server">
                 <div class="form_title">
                <div class="title_strip"> </div>
                <div class="form_single">
                    <h3> INFORMASI PENARIKAN </h3>
                </div>
            </div>
             
                <div class="form_box">
                    <div class="form_left">
                        <label> No Kontrak</label> 
                        <asp:HyperLink ID="hypAgreementNo" runat="server" />
                    </div>
                    <div class="form_right">
                        <label>Nama Customer</label>
                        <asp:HyperLink ID="hypCustomerName" runat="server" />
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label> Alamat</label>
                        <asp:Label ID="lblAddress" runat="server" />
                    </div>
                </div>

                <div class="form_box">
                    <div class="form_left">
                        <label>Angsuran Ke</label>
                        <asp:Label ID="lblInstallmentNo" runat="server" />
                    </div>
                    <div class="form_right">
                        <label>Jumlah Angsuran</label>
                        <asp:Label ID="lblInstallmentAmount" runat="server" />
                    </div>
                </div>

                <div class="form_box">
                    <div class="form_left">
                        <label>Tanggal Jatuh Tempo</label>
                        <asp:Label ID="lblDuedate" runat="server" />
                    </div>
                    <div class="form_right">
                        <label>Jumlah Hari Telat</label>
                        <asp:Label ID="lblOverDueDays" runat="server" />
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>Jumlah Tunggakan</label>
                        <asp:Label ID="lblODAmount" runat="server" />
                    </div>
                    <div class="form_right">
                        <label>Denda Keterlambatan</label>
                        <asp:Label ID="lblLateCharges" runat="server" />
                    </div>
                </div> 
                <div class="form_box">
                    <div class="form_left">
                        <label>Sisa A/R</label>
                        <asp:Label ID="lblOSBallance" runat="server" />
                    </div>
                    <div class="form_right">
                        <label> Biaya Tarik(Repossess Fee)</label>
                        <asp:Label ID="LblRepossessFee" runat="server" />
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <%--<label>No SKT</label>--%>
                        <label>No SKE</label>
                        <asp:Label ID="lblRALNo" runat="server" />
                    </div>
                    <div class="form_right">
                        <%--<label>Periode SKT</label>--%>
                        <label>Periode SKE</label>
                        <asp:Label ID="lblRALPeriod" runat="server" />
                    </div>
                </div> 
                <div class="form_box">
                    <div class="form_left">
                        <label> Eksekutor</label>
                        <asp:Label ID="lblExecutor" runat="server" />
                    </div>
                    <div class="form_right">
                        <%--<label> SKT Ditangani</label>--%>
                        <label> SKE Ditangani</label>
                        <asp:Label ID="lblOnHand" runat="server" />&nbsp;hari
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label> Nama Asset</label>
                        <asp:Label ID="lblAsset" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>No Polisi</label>
                        <asp:Label ID="lblLicensePlate" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>No Mesin</label>
                        <asp:Label ID="lblEngineNo" runat="server" />
                    </div>
                </div>

                <div class="form_box">
                    <div class="form_left">
                        <label>No Rangka</label>
                        <asp:Label ID="lblChassisNo" runat="server" />
                    </div>
                   <div class="form_right">
                        <label> Warna</label>
                      <asp:Label ID="lblColor" runat="server" CssClass="long_text" />
                    </div>
                </div>

                <div class="form_box">
                    <div class="form_left">
                        <label>Tahun Produksi</label>
                        <asp:Label ID="lblYear" runat="server" />
                    </div>
                    <div class="form_right">
                       
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>BAPK</label>
                        <asp:Label ID="lblBAPK" runat="server" />
                    </div>
                </div>

                <div class="form_box_header">
                    <div class="form_single">
                        <h4>DATA PENARIKAN </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label > Tanggal Tarik</label>
                         <asp:Label  ID="lblRepossesDate" runat="server" />  
                    </div>
                   <div class="form_right">
                        <label> STNK</label>
                         <asp:Label  runat="server" ID="lblSTNK" />
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label  > Lokasi Tarik</label>
                          <asp:Label  ID="lblAssetLocation" runat="server" CssClass="long_text" /> 
                    </div>
                       <div class="form_right">
                           <div id='divPajakStnk' >
                            <label> Masa Berlaku Pajak STNK</label>
                              <asp:Label  ID="lblSTNKEndDate" runat="server" /> 
                        </div>
                    </div>
                  
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label  > Kondisi Asset</label>
                         <asp:Label  ID="lblAssetCondition" runat="server" CssClass="long_text" /> 
                    </div>
                     <div class="form_right">
                        <label > Lokasi Penyimpanan</label>
                         <asp:Label runat="server" ID="lblLokasiPenyimpanan" /> 
                    </div>

                </div>
                <div class="form_box"> 
                    <div class="form_left">
                        <label >  Biaya Tarik</label> 
                        <asp:Label id="lblReposessExpense" runat="server" /> 
                    </div>
                    
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>KM Asset</label>                                             
                        <asp:Label id="lblKMAsset" runat="server" /> 
                    </div> 
                </div>
 
                  <div class="form_box">
                    <div class="form_left">
                        <label>Grade</label>                                             
                         <asp:Label runat="server" ID="lblGrade" />
                    </div>

                    <div class="form_right"> 
                    </div>
                </div>
            </asp:Panel>



          <asp:Panel ID="pnlApproval" runat="server" Width="100%">
                <div class="form_box_title">
                    <div class="form_single">
                        <h4>
                            APPROVE PENARIKAN ASSET
                        </h4>
                    </div>
                </div>

                <div class="form_box">
                    <div class="form_single">
                        <label>Pemeriksa </label>
                          <asp:Label ID="lblPemeriksa" runat="server"/>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>Jabatan Pemeriksa</label>
                        <asp:Label ID="lblJabatanPemeriksa" runat="server"/>
                    </div>
                </div>

                <div class="form_box">
                    <div class="form_single">
                        <label>Tangggal Periksa</label>
                        <asp:Label ID="lblTglPeriksa" runat="server"/>
                    </div>
                </div>
                
                 <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">Approval</label>
                        <asp:DropDownList ID="ddlApprove" runat="server">
                            <asp:ListItem Value="0">Select One</asp:ListItem>
                            <asp:ListItem Value="R">Approve</asp:ListItem>
                            <asp:ListItem Value="J">Reject</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="reqFloatingPeriod" runat="server"   Display="Dynamic" ErrorMessage="Harap pilih Approval" ControlToValidate="ddlApprove" CssClass="validator_general" InitialValue="0" />
                    </div>
                </div>
                 <div class="form_box">
                    <div class="form_single">
                        <label>Catatan</label>
                        <asp:TextBox ID="txtNotes" runat="server" CssClass="multiline_textbox" TextMode="MultiLine" />
                    </div>
                </div>
                 <div class="form_button">
                    <asp:Button ID="btnSave" runat="server" Text="Proses" CssClass="small button blue" /> &nbsp;
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="small button gray" CausesValidation="False" />
                </div>
            </asp:Panel>
    </form>
</body>
</html>
