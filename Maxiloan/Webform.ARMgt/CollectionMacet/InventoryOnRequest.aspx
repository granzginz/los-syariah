﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="InventoryOnRequest.aspx.vb"
    Inherits="Maxiloan.Webform.ARMgt.InventoryOnRequest" %>

<%@ Register TagPrefix="uc1" TagName="UcBranchCollection" Src="../../Webform.UserController/UcBranchCollection.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>InventoryOnRequest</title>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';


        function OpenWinCust(pStrCustID, pStrType) {
            var lStrAddress;
            if (pStrType == 'P')
                lStrAddress = 'ViewSearch.aspx';
            else
                lStrAddress = 'ViewSearchCompany.aspx';

            window.open('http://' + ServerName + '/' + App + '/Application/SmartSearch/' + lStrAddress + '?custid=' + pStrCustID, 'WinLink', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0,scrollbars=1,resizable=1,center=1');
        }

        function OpenWinAgreement(pStrAgreementNo) {
            window.open('http://' + ServerName + '/' + App + '/Application/SmartSearch/ViewAgreementDetail.aspx?AgreementNo=' + pStrAgreementNo, 'WinLink', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0,scrollbars=1,resizable=1,center=1');
        }

        function OpenWinAsset(pStrApplicationID, pStrAgreementNo, asq) {
            window.open('http://' + ServerName + '/' + App + '/Application/SmartSearch/perAsset.aspx?applicationid=' + pStrApplicationID + '&agreementno=' + pStrAgreementNo + '&assetseqno=' + asq, 'WinLink', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=1,resizable=1,center=1');
        }

//        function PlanRALChange() {
//            var myID = 'UcRALDate_txtDate';
//            var objDate = eval('document.forms[0].' + myID);
//            var result

//            if (document.forms[0].rbRAL_0.checked = true) {
//                result = 'Plan';
//            }
//            if (document.forms[0].rbRAL_1.checked = true) {
//                result = 'Undo Plan';
//            }

//            alert(result);
//            if (result == 'Undo Plan') {
//                objDate.disabled = true;
//                document.forms[0].all.UcRALDate_txtDate_imgCalender.disabled = true;
//                document.forms[0].all.UcRALDate_txtDate_imgCalender.disabled = true;
//                document.forms[0].UcRALDate_txtDate.value = '';
//            }
//        }
//
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">           
                <h3>
                    INVENTORY ON REQUEST
                </h3>   
            </div>
        </div>          
        <asp:Panel ID="PnlSearch" runat="server">        
        <div class="form_box">
	        <div class="form_single">
                <label>Collection Group</label>
                <uc1:UcBranchCollection id="oCGID" runat="server"></uc1:UcBranchCollection>	        
            </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Cari Berdasarkan</label>
                <asp:DropDownList ID="cboSearchBy" runat="server">
                    <asp:ListItem Value="Select One">Select One</asp:ListItem>
                    <asp:ListItem Value="Agreement.AgreementNo">No Kontrak</asp:ListItem>
                    <asp:ListItem Value="Customer.Name">Nama Customer</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearchBy" runat="server" ></asp:TextBox>
	        </div>
        </div>
        <div class="form_button">
            <asp:Button ID="Buttonsearch" runat="server"  Text="Search" CssClass ="small button blue"></asp:Button>&nbsp;
            <asp:Button ID="ButtonReset" runat="server"  Text="Reset" CssClass ="small button gray"
                CausesValidation="False"></asp:Button>
	    </div>                       
        </asp:Panel>
        <asp:Panel ID="PnlList" runat="server">
        <div class="form_box_title">
	        <div class="form_single">
                DAFTAR INVENTORY ON REQUEST
	        </div>
        </div> 
        <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="Dtg" runat="server" Width="100%" CssClass="grid_general" BorderStyle="None" BorderWidth="0" AutoGenerateColumns="False"
                        AllowSorting="True">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn SortExpression="Agreement.AgreementNO" HeaderText="NO KONTRAK">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="hlAgreementNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AgreementNo") %>'>
                                    </asp:HyperLink>
                                    <asp:Literal ID="ltlAgreemenNO" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AgreementNo") %>'
                                        Visible="False">
                                    </asp:Literal>
                                    <asp:Literal ID="ltlBranchID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BranchID") %>'
                                        Visible="False">
                                    </asp:Literal>
                                    <asp:Literal ID="ltlApplicationID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ApplicationID") %>'
                                        Visible="False">
                                    </asp:Literal>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Customer.Name" HeaderText="NAMA CUSTOMER">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="hlCustomer" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CustomerName") %>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="AssetDescription" SortExpression="assetMaster.Description"
                                HeaderText="NAMA ASSET">                                
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="RepossesDate" SortExpression="RepossesDate" HeaderText="TGL TARIK">                                
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="InventoryExpectedDate" SortExpression="InventoryExpectedDate"
                                HeaderText="TGL MASUK INVENTORY">                                
                            </asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="PILIH">                                
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkReposses" CausesValidation="False" runat="server" Enabled="True"
                                        Text="Inventory" CommandName="INVENTORY">INVENTORY</asp:LinkButton>
                                    <asp:Literal ID="ltlCustomerID" runat="server" Visible="FAlse" Text='<%# DataBinder.Eval(Container, "DataItem.CustomerID") %>'>
                                    </asp:Literal>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>                        
                    </asp:DataGrid>        
                <div class="button_gridnavigation">
	                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
		                CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
		                CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
		                CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
		                CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
	                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
		                EnableViewState="False"></asp:Button>
	                <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage"
				        MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer"
				        CssClass="validator_general"></asp:RangeValidator>
			        <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
				        ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general">
			        </asp:RequiredFieldValidator>              
                </div>
                <div class="label_gridnavigation">
	                <asp:Label ID="lblPage" runat="server"></asp:Label>of
	                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
	                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                </div>
            </div>
        </div>        
        </div> 
        </asp:Panel>    
        <asp:Panel ID="PnlListDetail"  runat="server">        
        <div class="form_box">	        
                <div class="form_left">
                    <label>No Kontrak</label>
                    <asp:HyperLink ID="hypAgreementNo" runat="server"></asp:HyperLink>
		        </div>
		        <div class="form_right">		
                    <label>Nama Customer</label>	
                    <asp:HyperLink ID="hypCustomerId" runat="server"></asp:HyperLink>
		        </div>	        
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Alamat</label>
                <asp:Label ID="lblAddress" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Nama Asset</label>
                <asp:Label ID="lblAsset" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <label>No Polisi</label>
                    <asp:Label ID="lblLicensePlate" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">	
                    <label>Warna</label>	
                    <asp:Label ID="lblColor" runat="server"></asp:Label>	
		        </div>	        
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <label>No Rangka</label>
                    <asp:Label ID="lblChasisNo" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">	
                    <label>No Mesin</label>		
                    <asp:Label ID="lblEngineNo" runat="server"></asp:Label>
		        </div>	        
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <label>Status Default</label>
                    <asp:Label ID="lblDefaultStatus" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">	
                    <label>Tanggal Berakhir STNK</label>		
                    <asp:Label ID="lblSTNKEndDate" runat="server"></asp:Label>
		        </div>	        
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <label>Tanggal Tarik</label>
                    <asp:Label ID="lblRepossDate" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">		
                    <label>Tanggal Masuk Inventory</label>	
                    <asp:Label ID="lblInvExpDate" runat="server"></asp:Label>
		        </div>	        
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <label>Tanggal Jatuh Tempo</label>
                    <asp:Label ID="lblDueDate" runat="server">Label</asp:Label>
		        </div>
		        <div class="form_right">			
		        </div>	        
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <label>Angsuran Ke</label>
                    <asp:Label ID="lblInstallNo" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">			
                    <label>Jumlah Angsuran</label>
                    <asp:Label ID="lblInstallAmount" runat="server"></asp:Label>
		        </div>	        
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <label>Jumlah Hari Telat</label>
                    <asp:Label ID="lblOverdueDays" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">		
                    <label>Jumlah Tunggakan</label>	
                    <asp:Label ID="lblAmountOverDue" runat="server"></asp:Label>
		        </div>	        
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <label>Lokasi Asset</label>
                    <asp:Label ID="lblAssetLocation" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">	
                    <label>Kondisi Asset</label>	
                    <asp:Label ID="lblAssetCondition" runat="server"></asp:Label>	
		        </div>	        
        </div>
        <div class="form_box_header">
            <div class="form_single">              
                <h4>
                    <strong>DATA INVENTORY</strong>
                </h4>
            </div>
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <label>Sisa Asuransi</label>
                    <asp:Label ID="lblOSIns" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">	
                    <label>Klaim Asuransi</label>		
                    <asp:Label ID="lblOSClaimIns" runat="server"></asp:Label>
		        </div>	        
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <label>Biaya STNK</label>
                    <asp:Label ID="lblOSSTNK" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">
                    <label>Biaya Tarik</label>			
                    <asp:Label ID="lblOSRepossFee" runat="server"></asp:Label>
		        </div>	        
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <label>Sisa Pokok</label>
                    <asp:Label ID="lblOSPrincipal" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">
                    <label>Sisa Bunga</label>			
                    <asp:Label ID="lblOSInterest" runat="server"></asp:Label>
		        </div>	        
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <label>Tunggakan Angsuran</label>
                    <asp:Label ID="lblOSInstallDue" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">			
                    <label>Tunggakan Asuransi</label>
                    <asp:Label ID="lblOSInsDue" runat="server"></asp:Label>
		        </div>	        
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <label>Denda Keterlambatan Angsuran</label>
                    <asp:Label ID="lblOSLCInstall" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">		
                    <label>Denda Keterlambatan Asuransi</label>	
                    <asp:Label ID="lblOSLCIns" runat="server"></asp:Label>
		        </div>	        
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <label>Biaya Tagih Angsuran</label>
                    <asp:Label ID="lblOSInstallCollFee" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">		
                    <label>Biaya Tagih Asuransi</label>	
                    <asp:Label ID="lblOSInsCollFee" runat="server"></asp:Label>
		        </div>	        
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <label>Biaya Tolakan PDC</label>
                    <asp:Label ID="lblOSPDCBounceFee" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">	
                    <label>Nilai Write Off</label>
                    <asp:Label ID="lblOSWOAmount" runat="server"></asp:Label>		
		        </div>	        
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <label>Nilai Stop Hitung Bunga</label>
                    <asp:Label ID="lblOSNAAmount" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">		
                    <label>Jumlah Prepaid</label>
                    <asp:Label ID="lblPrePaidAmount" runat="server"></asp:Label>	
		        </div>	        
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <label>Total A/R Gross</label>
                    <asp:Label ID="lblARGross" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">		
                    <label>Pendapatan Bunga (Income Recognize)</label>	
                    <asp:Label ID="lblAmtIncRecognize" runat="server"></asp:Label>
		        </div>	        
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <label>UCI (UnEarnd Contract Interest)</label>
                    <asp:Label ID="lblUCI" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">		
                    <label>ECI (Earnd Contract Interest)</label>	
                    <asp:Label ID="lblECI" runat="server"></asp:Label>
		        </div>	        
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <label>Nilai Inventory</label>
                    <asp:Label ID="lblInvAmount" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">			
		        </div>	        
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonSave" runat="server"  Text="Save" CssClass ="small button blue"></asp:Button><a href="javascript:history.go(-1);">&nbsp;</a>
            <asp:Button ID="ButtonCancel" runat="server"  Text="Cancel" CssClass ="small button gray"></asp:Button>
	    </div>        
    </asp:Panel>
    </form>
</body>
</html>
