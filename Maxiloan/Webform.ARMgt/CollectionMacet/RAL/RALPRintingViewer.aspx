﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="RALPRintingViewer.aspx.vb"
    Inherits="Maxiloan.Webform.ARMgt.RALPRintingViewer" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="Stylesheet"  href ="../../../Include/General.css" type="text/css" />
    <link rel="Stylesheet"  href ="../../../Include/Buttons.css" type="text/css" />  
</head>
<body>
    <form id="form1" runat="server">
    <div class ="form_button">
        <asp:Button runat="server" ID="imbBack" Text ="Back" CssClass ="small button gray" />
    </div>
    <div>
        <CR:CrystalReportViewer ID="crvRALPrinting" runat="server" AutoDataBind="true" />
    </div>
    </form>
</body>
</html>
