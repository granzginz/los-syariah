﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class RALChangeExecutor
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents ucRALEnddate As ValidDate

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.RALChangeExec
    Private oController As New RALChangeExecController


    Private currentPageChange As Int32 = 1
    Private pageSizeChange As Int16 = 10
    Private currentPageNumberChange As Int16 = 1
    Private totalPagesChange As Double = 1
    Private recordCountChange As Int64 = 1


#End Region

#Region "Property"
    Private Property ApplicationID() As String
        Get
            Return CStr(viewstate("ApplicationID"))
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property
    Private Property oData() As DataTable
        Get
            Return CType(viewstate("oData"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            viewstate("oData") = Value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            If Request.QueryString("strFileLocation") <> "" Then
                Dim strHTTPServer As String
                Dim StrHTTPApp As String
                Dim strNameServer As String
                Dim strFileLocation As String

                strHTTPServer = Request.ServerVariables("PATH_INFO")
                strNameServer = Request.ServerVariables("SERVER_NAME")
                StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                strFileLocation = "http://" & strNameServer & "/" & StrHTTPApp & "/XML/" & Request.QueryString("strFileLocation") & ".pdf"
                '& "history.back(-1) " & vbCrLf _
                'Call file PDF 
                Response.Write("<script language = javascript>" & vbCrLf _
                & "window.open('" & strFileLocation & "','Collection', 'left=0, top=0, width=600, height=480, menubar=0, scrollbars=yes') " & vbCrLf _
                & "</script>")
            End If
            Me.FormID = "CollRALChgExec"
            If CheckForm(Me.Loginid, Me.FormID, "Maxiloan") Then
                InitialDefaultPanel()
                FillCbo()
            End If
        End If
    End Sub
    Private Sub InitialDefaultPanel()
        pnlDtGrid.Visible = False
        pnlsearch.Visible = True
        pnlChange.Visible = False
        lblMessage.Text = ""
    End Sub
    Private Sub FillCbo()
        Dim oDataTable As New DataTable
        With oCustomClass
            .strConnection = GetConnectionSTring
            .strKey = "CG"
            .CGID = Me.GroubDbID
            .CollectorType = ""
        End With
        oCustomClass = oController.RALListCG(oCustomClass)
        oDataTable = oCustomClass.ListRAL
        With cboCG
            .DataSource = oDataTable
            .DataTextField = "CGName"
            .DataValueField = "CGID"
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = ""
            If oDataTable.Rows.Count = 1 Then
                .Items.FindByValue(Me.GroubDbID).Selected = True
            End If
        End With
    End Sub

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            lblMessage.Text = "Data tidak ditemukan ....."
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click
        lblMessage.Text = ""
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub
#End Region
#Region "NavigationSelect"
    Private Sub PagingFooterSelect()
        lblPageSelect.Text = currentPageChange.ToString()
        totalPagesChange = Math.Ceiling(CType((recordCountChange / CType(pageSizeChange, Integer)), Double))
        If totalPages = 0 Then
            lblMessage.Text = "Data tidak ditemukan ....."
            lblTotPageSelect.Text = "1"
            rgvGoSelect.MaximumValue = "1"
        Else
            lblTotPageSelect.Text = CType(totalPagesChange, String)
            rgvGoSelect.MaximumValue = CType(totalPagesChange, String)
        End If
        lblRecordSelect.Text = CType(recordCountChange, String)

        If currentPageChange = 1 Then
            imbPrevPageSelect.Enabled = False
            imbFirstPageSelect.Enabled = False
            If totalPagesChange > 1 Then
                imbNextPageSelect.Enabled = True
                imbLastPageSelect.Enabled = True
            Else
                imbPrevPageSelect.Enabled = False
                imbNextPageSelect.Enabled = False
                imbLastPageSelect.Enabled = False
                imbFirstPageSelect.Enabled = False
            End If
        Else
            imbPrevPageSelect.Enabled = True
            imbFirstPageSelect.Enabled = True
            If currentPageChange = totalPagesChange Then
                imbNextPageSelect.Enabled = False
                imbLastPageSelect.Enabled = False
            Else
                imbLastPageSelect.Enabled = True
                imbNextPageSelect.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLinkSelect_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "FirstSelect" : currentPageChange = 1
            Case "LastSelect" : currentPageChange = Int32.Parse(lblTotPageSelect.Text)
            Case "NextSelect" : currentPageChange = Int32.Parse(lblPageSelect.Text) + 1
            Case "PrevSelect" : currentPageChange = Int32.Parse(lblPageSelect.Text) - 1
        End Select
        'If Me.SortBy Is Nothing Then
        '    Me.SortBy = ""
        'End If
        BindGridChangeExecutor()
    End Sub

    Private Sub imbGoPageSelect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumbSelect.Click
        lblMessage.Text = ""
        If IsNumeric(txtPageSelect.Text) Then
            If CType(lblTotPageSelect.Text, Integer) > 1 And CType(txtPageSelect.Text, Integer) <= CType(lblTotPageSelect.Text, Integer) Then
                currentPageChange = CType(txtPageSelect.Text, Int16)
                'If Me.SortBy Is Nothing Then
                '    Me.SortBy = ""
                'End If
                BindGridChangeExecutor()
            End If
        End If
    End Sub
    Sub BindGridChangeExecutor()
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        Dim intloop As Integer
        Dim hypID As HyperLink

        With oCustomClass
            .strConnection = GetConnectionString
            .CGID = Me.GroubDbID
            .CurrentPage = currentPageChange
            .PageSize = pageSizeChange
        End With
        oCustomClass = oController.RALViewDataCollector(oCustomClass)
        DtUserList = oCustomClass.ListRAL
        DvUserList = DtUserList.DefaultView
        recordCountChange = oCustomClass.TotalRecord
        dtgSelect.DataSource = DvUserList

        Try
            dtgSelect.DataBind()
            With cboSelectExecutor
                .DataSource = DtUserList
                .DataTextField = "CollectorName"
                .DataValueField = "CollectorID"
                .DataBind()
                .Items.Insert(0, "Select One")
                .Items(0).Value = ""
            End With
        Catch
            dtgSelect.CurrentPageIndex = 0
            dtgSelect.DataBind()
        End Try
        PagingFooterSelect()
        pnlChange.Visible = True
        pnlsearch.Visible = False
        pnlDtGrid.Visible = False
    End Sub
#End Region

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        Dim intloop As Integer
        Dim hypID As HyperLink

        With oCustomClass
            .strConnection = GetConnectionString
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oCustomClass = oController.RALChangeExecList(oCustomClass)

        DtUserList = oCustomClass.ListRAL
        DvUserList = DtUserList.DefaultView
        recordCount = oCustomClass.TotalRecord
        DvUserList.Sort = Me.SortBy
        dtgRALChangeExec.DataSource = DvUserList
        With oCustomClass
            .strConnection = GetConnectionString
        End With
        oCustomClass = oController.RALDataChangeExec(oCustomClass)
        Me.oData = oCustomClass.ListRAL
        Try
            dtgRALChangeExec.DataBind()
        Catch
            dtgRALChangeExec.CurrentPageIndex = 0
            dtgRALChangeExec.DataBind()
        End Try



        PagingFooter()
        pnlDtGrid.Visible = True
        pnlsearch.Visible = True
        lblMessage.Text = ""
    End Sub
    Sub BindGridSelectExecutor()
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        Dim intloop As Integer
        Dim hypID As HyperLink

        With oCustomClass
            .strConnection = GetConnectionString
            .CGID = cboCG.SelectedValue.Trim
            .CurrentPage = currentPageChange
            .PageSize = pageSizeChange
        End With
        oCustomClass = oController.RALViewDataCollector(oCustomClass)
        DtUserList = oCustomClass.ListRAL
        DvUserList = DtUserList.DefaultView
        recordCountChange = oCustomClass.TotalRecord
        dtgSelect.DataSource = DvUserList

        Try
            dtgSelect.DataBind()
            With cboSelectExecutor
                .DataSource = DtUserList
                .DataTextField = "CollectorName"
                .DataValueField = "CollectorID"
                .DataBind()
                .Items.Insert(0, "Select One")
                .Items(0).Value = ""
            End With
        Catch
            dtgSelect.CurrentPageIndex = 0
            dtgSelect.DataBind()
        End Try
        PagingFooterSelect()
        pnlChange.Visible = True
        pnlsearch.Visible = False
        pnlDtGrid.Visible = False
    End Sub


    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Buttonsearch.Click
        Me.SearchBy = "CollectionAgreement.CGID='" & cboCG.SelectedItem.Value.Trim & "'"
        If cboSearchBy.SelectedItem.Value.Trim <> "Select One" Then
            Me.SearchBy = Me.SearchBy + " and " + cboSearchBy.SelectedItem.Value.Trim + " like '%" + txtSearchBy.Text.Trim + "%'"
        End If

        Me.SortBy = ""
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub dtgRALChangeExec_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgRALChangeExec.ItemCommand
        Select Case e.CommandName
            Case "Change"
                Dim lblApplicationId As Label
                Dim lblRALNodtg As Label
                Dim oDataTable As New DataTable
                lblApplicationId = CType(dtgRALChangeExec.Items(e.Item.ItemIndex).FindControl("lblApplicationId"), Label)
                lblRALNodtg = CType(dtgRALChangeExec.Items(e.Item.ItemIndex).Cells(4).FindControl("lblRALNodtg"), Label)
                If checkfeature(Me.Loginid, Me.FormID, "Chg", "Maxiloan") Then
                    With oCustomClass
                        .strConnection = GetConnectionString
                        .ApplicationID = lblApplicationId.Text.Trim
                        .RALNo = lblRALNodtg.Text.Trim
                        .BusinessDate = Me.BusinessDate
                        '.RALNo = CType(dtgRALPrinting.Items(e.Item.ItemIndex).Cells(4).Text.Trim, String)
                    End With
                    oCustomClass = oController.RequestDataSelect(oCustomClass)
                    oDataTable = oCustomClass.ListRAL

                    Me.ApplicationID = lblApplicationId.Text
                    If Not IsDBNull(oDataTable.Rows(0)("AgreementNo")) Then
                        hypAgreementNoSlct.Text = CType(oDataTable.Rows(0)("AgreementNo"), String)
                    End If
                    If Not IsDBNull(oDataTable.Rows(0)("CustomerName")) Then
                        hypCustomerNameSlct.Text = CType(oDataTable.Rows(0)("CustomerName"), String)
                    End If
                    If Not IsDBNull(oDataTable.Rows(0)("MailingAddress")) Then
                        lblAddress.Text = CType(oDataTable.Rows(0)("MailingAddress"), String)
                    End If
                    If Not IsDBNull(oDataTable.Rows(0)("Description")) Then
                        lblAsset.Text = CType(oDataTable.Rows(0)("Description"), String)
                    End If
                    If Not IsDBNull(oDataTable.Rows(0)("InstallmentNo")) Then
                        lblInstallmentNo.Text = CType(oDataTable.Rows(0)("InstallmentNo"), String)
                    End If
                    If Not IsDBNull(oDataTable.Rows(0)("InstallmentAmount")) Then
                        lblInstallmentAmount.Text = FormatNumber(oDataTable.Rows(0)("InstallmentAmount"), 2)
                    End If
                    If Not IsDBNull(oDataTable.Rows(0)("DueDate")) Then
                        lblDuedate.Text = Format(oDataTable.Rows(0)("DueDate"), "dd/MM/yyyy")
                    End If
                    If Not IsDBNull(oDataTable.Rows(0)("EndPastDuedays")) Then
                        lblOverDueDays.Text = CType(oDataTable.Rows(0)("EndPastDuedays"), String)
                    End If
                    If Not IsDBNull(oDataTable.Rows(0)("OutstandingLC")) Then
                        lblODAmount.Text = FormatNumber(oDataTable.Rows(0)("OutstandingLC"), 2)
                    End If

                    If Not IsDBNull(oDataTable.Rows(0)("OSBillingCharges")) Then
                        lblOSBillingCharges.Text = FormatNumber(oDataTable.Rows(0)("OSBillingCharges"), 2)
                    End If
                    If Not IsDBNull(oDataTable.Rows(0)("CollectionExpense")) Then
                        lblCollExpense.Text = FormatNumber(oDataTable.Rows(0)("CollectionExpense"), 2)
                    End If
                    If Not IsDBNull(oDataTable.Rows(0)("OSBalance")) Then
                        lblOSBalance.Text = FormatNumber(oDataTable.Rows(0)("OSBalance"), 2)
                    End If

                    If Not IsDBNull(oDataTable.Rows(0)("RALNo")) Then
                        lblRALNo.Text = CType(oDataTable.Rows(0)("RALNo"), String)
                    End If

                    If Not IsDBNull(oDataTable.Rows(0)("Startdate")) Then
                        lblRALPeriod1.Text = Format(oDataTable.Rows(0)("Startdate"), "dd/MM/yyyy")
                    End If
                    If Not IsDBNull(oDataTable.Rows(0)("Enddate")) Then
                        lblRALPeriod2.Text = Format(oDataTable.Rows(0)("Enddate"), "dd/MM/yyyy")
                    End If
                    If Not IsDBNull(oDataTable.Rows(0)("ExecutorID")) Then
                        lblExecutor.Text = CType(oDataTable.Rows(0)("ExecutorID"), String)
                        lblExecutorName.Text = CType(oDataTable.Rows(0)("CollectorName"), String)
                    End If

                    BindGridSelectExecutor()
                    'With oCustomClass
                    '    .strConnection = GetConnectionString
                    '    .CGID = Me.GroubDbID
                    'End With
                    'oCustomClass = oController.RALViewDataCollector(oCustomClass)
                    'oDataTable = oCustomClass.ListRAL
                    'dtgSelect.DataSource = oDataTable
                    'dtgSelect.DataBind()
                    'With cboSelectExecutor
                    '    .DataSource = oDataTable
                    '    .DataTextField = "CollectorName"
                    '    .DataValueField = "CollectorID"
                    '    .DataBind()
                    '    .Items.Insert(0, "Select One")
                    '    .Items(0).Value = ""
                    'End With
                    'pnlSelect.Visible = True
                    'pnlsearch.Visible = False
                    'pnlDtGrid.Visible = False
                End If
            Case "PrintRAL"

                If CheckFeature(Me.Loginid, Me.FormID, "PrRAL", "Maxiloan") Then
                    Dim lblApplicationId As Label
                    Dim hasil As Integer
                    Dim lblRALNodtg, hypExecutorIDdtg As Label
                    lblApplicationId = CType(dtgRALChangeExec.Items(e.Item.ItemIndex).FindControl("lblApplicationId"), Label)
                    hypExecutorIDdtg = CType(dtgRALChangeExec.Items(e.Item.ItemIndex).FindControl("hypExecutorIDdtg"), Label)
                    lblRALNodtg = CType(dtgRALChangeExec.Items(e.Item.ItemIndex).FindControl("lblRALNodtg"), Label)
                    If CheckFeature(Me.Loginid, Me.FormID, "PrRAL", "Maxiloan") Then
                        With oCustomClass
                            .strConnection = GetConnectionString()
                            .ApplicationID = lblApplicationId.Text.Trim
                            .RALNo = lblRALNodtg.Text.Trim
                            .BusinessDate = Me.BusinessDate
                            .LoginId = Me.Loginid
                        End With
                        oCustomClass = oController.RALSaveDataPrintRAL(oCustomClass)
                        hasil = oCustomClass.hasil
                        'Dim cookie As HttpCookie = Request.Cookies("RptRALPrinting")
                        'If Not cookie Is Nothing Then
                        '    cookie.Values("ExecutorID") = hypExecutorIDdtg.Text.Trim
                        '    cookie.Values("RALNo") = lblRALNodtg.Text.Trim
                        '    cookie.Values("ApplicationID") = lblapplicationid.Text.Trim
                        '    cookie.Values("CGID") = cboCG.SelectedValue.Trim
                        '    Response.AppendCookie(cookie)
                        'Else
                        Dim cookieNew As New HttpCookie("RptRALPrinting")
                        cookieNew.Values.Add("ExecutorID", hypExecutorIDdtg.Text.Trim)
                        cookieNew.Values.Add("RALNo", lblRALNodtg.Text.Trim)
                        cookieNew.Values.Add("ApplicationID", lblApplicationId.Text.Trim)
                        cookieNew.Values.Add("CGID", cboCG.SelectedValue.Trim)
                        Response.AppendCookie(cookieNew)
                        'End If
                        Response.Redirect("RALPrintingViewer.aspx?cmd=RAL&back=Executor&File=RALChangeExecutor.aspx")
                    End If
                End If

            Case "PrintBPKP"
                Dim lblApplicationId As Label
                Dim hasil As Integer
                Dim hypExecutorID As Label
                lblApplicationId = CType(dtgRALChangeExec.Items(e.Item.ItemIndex).FindControl("lblApplicationId"), Label)
                hypExecutorID = CType(dtgRALChangeExec.Items(e.Item.ItemIndex).FindControl("hypExecutorIDdtg"), Label)
                If checkfeature(Me.Loginid, Me.FormID, "PrLst", "Maxiloan") Then
                    'Dim cookie As HttpCookie = Request.Cookies("RptRALChangeExec")
                    'If Not cookie Is Nothing Then
                    '    'cookie.Values("ExecutorID") = CType(dtgRALChangeExec.Items(e.Item.ItemIndex).Cells(5).Text.Trim, String)
                    '    cookie.Values.Add("ExecutorID", hypExecutorID.Text.Trim)
                    '    cookie.Values.Add("ApplicationID", lblApplicationId.Text.Trim)
                    '    cookie.Values.Add("CGID", cboCG.SelectedValue.Trim)
                    '    Response.AppendCookie(cookie)
                    'Else
                    Dim cookieNew As New HttpCookie("RptRALPrinting")
                    'cookieNew.Values.Add("ExecutorID", CType(dtgRALChangeExec.Items(e.Item.ItemIndex).Cells(5).Text.Trim, String))
                    cookieNew.Values.Add("ExecutorID", hypExecutorID.Text.Trim)
                    cookieNew.Values.Add("ApplicationID", lblApplicationId.Text.Trim)
                    cookieNew.Values.Add("CGID", cboCG.SelectedValue.Trim)
                    Response.AppendCookie(cookieNew)
                    'End If
                    Response.Redirect("RALPrintingViewer.aspx?cmd=CheckList&back=Executor&File=RALChangeExecutor.aspx")
                End If
        End Select
    End Sub

    Private Sub imbSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
        Dim hasil As Integer
        Dim executor As String
        executor = lblExecutor.Text.Trim

        If cboSelectExecutor.SelectedItem.Value.Trim = "" Then
            lblMessage.Text = "Harap pilih Eksekutor"
            Exit Sub
        End If
        If cboSelectExecutor.SelectedItem.Value.Trim = executor Then
            lblMessage.Text = "Harap pilih Eksekutor Lain"
            Exit Sub
        End If
        With oCustomClass
            .strConnection = GetConnectionString()
            .ApplicationID = Me.ApplicationID
            .ExecutorID = cboSelectExecutor.SelectedItem.Value
            .CollectorIDOld = lblExecutor.Text
            .BusinessDate = Me.BusinessDate
            .NextBD = Me.BusinessDate
            .RALNOld = lblRALNo.Text
        End With
        oCustomClass = oController.RALSaveDataExecutor(oCustomClass)
        hasil = oCustomClass.hasil
        If hasil = 0 Then
            lblMessage.Text = "Gagal"
        Else
            pnlsearch.Visible = True
            pnlDtGrid.Visible = True
            pnlChange.Visible = False
            DoBind(Me.SearchBy, Me.SortBy)
        End If
    End Sub

    Private Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        pnlsearch.Visible = True
        pnlDtGrid.Visible = True
        pnlChange.Visible = False
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Public Function isVisiblePrint(ByVal strKey As String) As Boolean
        If strKey.Trim = "" Then
            Return False
        Else
            Return True
        End If
    End Function

    Public Function isVisible(ByVal strNo As String) As Boolean
        Dim DataRow As DataRow()
        DataRow = Me.oData.Select(" NewRALNo ='" & strNo.Trim & "' ")
        If DataRow.Length > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub imbReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        InitialDefaultPanel()
        cboSearchBy.ClearSelection()
        cboCG.ClearSelection()
        txtSearchBy.Text = ""

    End Sub
    Private Sub dtgRALChangeExec_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgRALChangeExec.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim hyTemp As HyperLink
            Dim lblApplicationId As Label
            lblApplicationId = CType(e.Item.FindControl("lblApplicationId"), Label)
            '*** Agreement No link
            hyTemp = CType(e.Item.FindControl("hypAgreementNodtg"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenAgreementNo('" & "Collection" & "', '" & Server.UrlEncode(lblApplicationId.Text.Trim) & "')"
        End If
    End Sub

End Class