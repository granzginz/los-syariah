﻿Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Controller

Public Class RALPRintingViewer
    Inherits Webform.WebBased

    Private m_coll As New RALPrintingController
    Private oCustomClass As New Parameter.RALPrinting
    Private FROM_FILE As String = ""
    Dim PDFFile As String = ""

#Region "Property"
    Private Property ExecutorID() As String
        Get
            Return CType(viewstate("ExecutorID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ExecutorID") = Value
        End Set
    End Property

    Private Property RALNo() As String
        Get
            Return CType(viewstate("RALNo"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("RALNo") = Value
        End Set
    End Property

    Private Property ApplicationID() As String
        Get
            Return CType(viewstate("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property

    Private Property CGID() As String
        Get
            Return CType(viewstate("CGID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CGID") = Value
        End Set
    End Property

    Private Property ExecutorTypeID() As String
        Get
            Return CType(ViewState("ExecutorTypeID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ExecutorTypeID") = Value
        End Set
    End Property

    Private Property PageBack() As String
        Get
            Return CType(viewstate("PageBack"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("PageBack") = Value
        End Set
    End Property

#End Region



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then Exit Sub

        Me.PageBack = Request.QueryString("back")
        FROM_FILE = Request.QueryString("File")

        If Request.QueryString("cmd").Trim = "RAL" Then
            BindReport()
        Else
            BindReportCheckList()
        End If
    End Sub

    Sub BindReport()
        GetCookies()
        With oCustomClass
            .strConnection = GetConnectionString
            .ApplicationID = Me.ApplicationID
            .RALNo = Me.RALNo
            .ExecutorID = Me.ExecutorID
            .CGID = Me.CGID
        End With
        oCustomClass = m_coll.RALListReport(oCustomClass)
        Dim numCopies As Int16 = 1
        Dim Collate As Boolean = False
        Dim startpage As Int16 = 1
        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions
        Dim strFileLocation As String
        PDFFile = "RptRAL"
        strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
        strFileLocation += Me.Session.SessionID + Me.Loginid + PDFFile + ".pdf"
        DiskOpts.DiskFileName = strFileLocation
        If Me.ExecutorTypeID = "EP" Then
            Dim rptRALPrinting As New RptRALPrinting
            rptRALPrinting.SetDataSource(oCustomClass.ListReport)
            crvRALPrinting.ReportSource = rptRALPrinting
            rptRALPrinting.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
            rptRALPrinting.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat
            crvRALPrinting.Visible = True
            crvRALPrinting.DataBind()
            rptRALPrinting.ExportOptions.DestinationOptions = DiskOpts
            rptRALPrinting.Export()
            rptRALPrinting.Close()
            rptRALPrinting.Dispose()
        Else
            Dim rptRALPrintingEI As New RptRALPrintingEI
            rptRALPrintingEI.SetDataSource(oCustomClass.ListReport)
            crvRALPrinting.ReportSource = rptRALPrintingEI
            rptRALPrintingEI.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
            rptRALPrintingEI.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat
            crvRALPrinting.Visible = True
            crvRALPrinting.DataBind()
            rptRALPrintingEI.ExportOptions.DestinationOptions = DiskOpts
            rptRALPrintingEI.Export()
            rptRALPrintingEI.Close()
            rptRALPrintingEI.Dispose()
        End If
        Select Case Me.PageBack
            Case "Request"
                Response.Redirect("RALOnRequest.aspx?strFileLocation=" & Me.Session.SessionID & Me.Loginid & PDFFile)
            Case "Extend"
                Response.Redirect("RALExtend.aspx?strFileLocation=" & Me.Session.SessionID & Me.Loginid & PDFFile)
            Case "Printing"
                Response.Redirect("RALPrinting.aspx?strFileLocation=" & Me.Session.SessionID & Me.Loginid & PDFFile)
            Case "Executor"
                Response.Redirect("RALChangeExecutor.aspx?strFileLocation=" & Me.Session.SessionID & Me.Loginid & PDFFile)
        End Select
    End Sub

    Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies("RptRALPrinting")
        Me.ApplicationID = cookie.Values("ApplicationID")
        Me.RALNo = cookie.Values("RALNo")
        Me.ExecutorID = cookie.Values("ExecutorID")
        Me.CGID = cookie.Values("CGId")
        Me.ExecutorTypeID = cookie.Values("ExecutorTypeID")
    End Sub

    Private Sub imbBack_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbBack.Click
        Select Case Me.PageBack
            Case "Request"
                Response.Redirect("RALOnRequest.aspx")
            Case "Extend"
                Response.Redirect("RALExtend.aspx")
            Case "Printing"
                Response.Redirect("RALPrinting.aspx")
            Case "Executor"
                Response.Redirect("RALChangeExecutor.aspx")
        End Select
    End Sub

    Sub BindReportCheckList()
        GetCookies()
        With oCustomClass
            .strConnection = GetConnectionString
            .ApplicationID = Me.ApplicationID
            .ExecutorID = Me.ExecutorID
            .CGID = Me.CGID
            .RALNo = Me.RALNo
        End With

        oCustomClass = m_coll.RALListReportCheckList(oCustomClass)

        Dim rptRALPrinting As New RptRALPrintingCheckList
        Dim numCopies As Int16 = 1
        Dim Collate As Boolean = False
        Dim startpage As Int16 = 1
        rptRALPrinting.SetDataSource(oCustomClass.ListReport)

        crvRALPrinting.ReportSource = rptRALPrinting
        crvRALPrinting.Visible = True
        crvRALPrinting.DataBind()

        'Dim strHTTPServer As String
        'Dim StrHTTPApp As String
        'Dim strNameServer As String
        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions

        rptRALPrinting.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
        rptRALPrinting.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat

        Dim strFileLocation As String
        PDFFile = "RptRALCheckList"
        strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
        strFileLocation += Me.Session.SessionID + Me.Loginid + PDFFile + ".pdf"
        DiskOpts.DiskFileName = strFileLocation
        'oPPK.PrintOptions.PaperSize = PaperSize.PaperA4
        rptRALPrinting.ExportOptions.DestinationOptions = DiskOpts
        rptRALPrinting.Export()
        Select Case Me.PageBack
            Case "Request"
                Response.Redirect("RALOnRequest.aspx?strFileLocation=" & Me.Session.SessionID & Me.Loginid & PDFFile)
            Case "Extend"
                Response.Redirect("RALExtend.aspx?strFileLocation=" & Me.Session.SessionID & Me.Loginid & PDFFile)
            Case "Printing"
                Response.Redirect("RALPrinting.aspx?strFileLocation=" & Me.Session.SessionID & Me.Loginid & PDFFile)
            Case "Executor"
                Response.Redirect("RALChangeExecutor.aspx?strFileLocation=" & Me.Session.SessionID & Me.Loginid & PDFFile)
        End Select

        '        Response.Redirect("RALPrinting.aspx?strFileLocation=" & Me.Session.SessionID & Me.Loginid & PDFFile)

        '=====================================
        'strHTTPServer = Request.ServerVariables("PATH_INFO")
        'strNameServer = Request.ServerVariables("SERVER_NAME")
        'StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
        'strFileLocation = "http://" & strNameServer & "/" & StrHTTPApp & "/XML/" + Me.Session.SessionID + Me.Loginid + "rpt_RAL.pdf"
        ''Call file PDF 

        'Response.Write("<script language = javascript>" & vbCrLf _
        '            & "history.back(-1) " & vbCrLf _
        '            & "window.open('" & strFileLocation & "') " & vbCrLf _
        '            & "</script>")
    End Sub
End Class