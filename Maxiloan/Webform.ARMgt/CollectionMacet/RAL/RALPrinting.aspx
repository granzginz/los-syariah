﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="RALPrinting.aspx.vb" Inherits="Maxiloan.Webform.ARMgt.RALPrinting" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>RALPrinting</title>
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../../Include/General.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';
    </script>
    <script language="javascript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;

        function OpenWinHistory(pCGID, pExID, pExName) {
            window.open('../ExecutorHistory.aspx?CGID=' + pCGID + '&ExecutorID=' + pExID + '&ExecutorName=' + pExName + '', 'UserLookup', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenWinCollector(pCollID, pCGID) {
            window.open('../../Webform.Setup/CollectorView.aspx?CGID=' + pCGID + '&CollectorID=' + pCollID + '', 'UserLookup', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenWinCustomer(pID) {
            window.open('../../../Webform.LoanOrg/Credit/ViewPersonalCustomer.aspx?CustomerID=' + pID + '&style=Collection', 'UserLookup', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
        onclick="hideMessage();"></asp:Label>
    <div class="form_title">
        <div class="form_single">
            <h3>
                CETAK SURAT KUASA PENARIKAN
            </h3>
        </div>
    </div>
    <asp:Panel ID="pnlsearch" runat="server">
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    Cabang
                </label>
                <asp:DropDownList ID="cboCG" runat="server">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="rfvCG" runat="server" Display="Dynamic" ControlToValidate="cboCG"
                    CssClass="validator_general" ErrorMessage="*"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cari Berdasarkan</label>
                <asp:DropDownList ID="cboSearchBy" runat="server">
                    <asp:ListItem Value="Select One">Select One</asp:ListItem>
                    <asp:ListItem Value="CollectionAgreement.AgreementNo">No Kontrak</asp:ListItem>
                    <asp:ListItem Value="Customer.Name">Nama Konsumen</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearchBy" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="Buttonsearch" runat="server" Text="Search" CssClass="small button blue">
            </asp:Button>&nbsp;
            <asp:Button ID="ButtonReset" runat="server" Text="Reset" CssClass="small button gray"
                CausesValidation="False"></asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlDtGrid" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    CETAK SURAT KUASA PENARIKAN
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ws">
                    <asp:DataGrid ID="dtgRALPrinting" runat="server" Width="1500px" CellPadding="0" OnSortCommand="SortGrid"
                        CssClass="grid_general" BorderStyle="None" BorderWidth="0" DataKeyField="AgreementNo"
                        AutoGenerateColumns="False" AllowSorting="True">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">
                                <ItemTemplate>
                                    <asp:HyperLink ID="hypAgreementNodtg" runat="server" Text='<%# Container.Dataitem("AgreementNo")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="CustomerName" HeaderText="NAMA KONSUMEN">
                                <ItemTemplate>
                                    <a href="javascript:OpenWinCustomer('<%# Container.DataItem("CustomerID")%>');">
                                        <asp:Label ID="hypCustomerNamedtg" runat="server" Text='<%# Container.Dataitem("CustomerName")%>'>
                                        </asp:Label></a>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="INSTALLMENTNO" SortExpression="InstallmentNo" HeaderText="ANGS KE">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="ENDPASTDUEDAYS" SortExpression="EndPastDueDays" HeaderText="HARI TELAT">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="RALNO" SortExpression="RALNo" HeaderText="NO SKP"></asp:BoundColumn>
                            <asp:BoundColumn DataField="RALMenu" SortExpression="RALMenu" HeaderText="SUMBER SKP">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="EXECUTORID" SortExpression="ExecutorID" HeaderText="ID EKSEKUTOR">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="ExecutorName" SortExpression="ExecutorName" HeaderText="EKSEKUTOR">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="ExecutorType" SortExpression="ExecutorType" HeaderText="TIPE EKSEKUTOR">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="ExecutorTypeID" SortExpression="ExecutorTypeID" HeaderText="TIPE EKSEKUTOR ID"
                                Visible="false"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="PILIH">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgSelect" CommandName="Select" runat="server" Visible='<%# isVisible(Container.dataitem("ExecutorID"))%>'
                                        ImageUrl="../../../images/IconDocument.gif"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="CETAK SKP">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgPrint" CommandName="Print" runat="server" Visible='<%# isVisiblePrint(Container.DataItem("ExecutorID"))%>'
                                        ImageUrl="../../../images/IconPrinter.gif"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="CETAK CHECKLIST">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgPrintCheckList" CommandName="PrintCheckList" runat="server"
                                        Visible='<%# isVisiblePrint(Container.DataItem("ExecutorID"))%>' ImageUrl="../../../images/IconPrinter.gif">
                                    </asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="ApplicationId" HeaderText="ApplicationId" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblApplicationId" runat="server" Text='<%# Container.Dataitem("ApplicationId")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
                <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../../Images/grid_navbutton01.png"
                        CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../../Images/grid_navbutton02.png"
                        CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../../Images/grid_navbutton03.png"
                        CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../../Images/grid_navbutton04.png"
                        CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                    </asp:ImageButton>
                    <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                    <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                        EnableViewState="False"></asp:Button>
                    <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" MinimumValue="1"
                        ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                        ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general">
                    </asp:RequiredFieldValidator>
                </div>
                <div class="label_gridnavigation">
                    <asp:Label ID="lblPage" runat="server"></asp:Label>of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                    <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                </div>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlSelect" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    CETAK SURAT KUASA PENARIKAN
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    No Kontrak</label>
                <asp:HyperLink ID="hypAgreementNoSlct" runat="server"></asp:HyperLink>
            </div>
            <div class="form_right">
                <label>
                    Nama Konsumen</label>
                <asp:HyperLink ID="hypCustomerNameSlct" runat="server"></asp:HyperLink>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Alamat</label>
                <asp:Label ID="lblAddress" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Nama Asset</label>
                <asp:Label ID="lblAsset" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Angsuran Ke</label>
                <asp:Label ID="lblInstallmentNo" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Jumlah Angsuran</label>
                <asp:Label ID="lblInstallmentAmount" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Tanggal Jatuh Tempo</label>
                <asp:Label ID="lblDuedate" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Jumlah Hari Telat</label>
                <asp:Label ID="lblOverDueDays" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Jumlah Tunggakan</label>
                <asp:Label ID="lblODAmount" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Denda Keterlambatan</label>
                <asp:Label ID="lblLateCharges" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Biaya Tagih</label>
                <asp:Label ID="lblOSBillingCharges" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Biaya Collection</label>
                <asp:Label ID="lblCollExpense" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Sisa A/R</label>
                <asp:Label ID="lblOSBallance" runat="server"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    No SKP</label>
                <asp:Label ID="lblRALNo" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Periode SKP</label>
                <asp:Label ID="lblRALPeriod1" runat="server"></asp:Label>&nbsp;S/D&nbsp;
                <asp:Label ID="lblRALPeriod2" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Eksekutor Sebelumnya</label>
                <asp:Label ID="lblPrevExc" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Pemberi Kuasa
                </label>
                &nbsp;<asp:DropDownList ID="cboSelectPemberiKuasa" runat="server">
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    ID Eksekutor
                </label>
                &nbsp;<asp:DropDownList ID="cboSelectExecutor" runat="server">
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Masa Berlaku Surat Kuasa</label>
                <asp:TextBox ID="MasaBerlaku" runat="server"></asp:TextBox> 
                <asp:RequiredFieldValidator runat="server" id = "MasaSKPIVal" ControlToValidate = "MasaBerlaku" InitialValue = "" ErrorMessage = "Isi dengan Angka" CssClass = "validator_general" ></asp:RequiredFieldValidator>                             
                hari</div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonSave" runat="server" Text="Save" CssClass="small button blue">
            </asp:Button>&nbsp;
            <asp:Button ID="ButtonCancel" runat="server" Text="Cancel" CssClass="small button gray"
                CausesValidation="False"></asp:Button>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    HISTORY SKP EKSEKUTOR
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgSelect" runat="server" Width="100%" OnSortCommand="SortGrid"
                        AutoGenerateColumns="False" AllowSorting="False" CssClass="grid_general" BorderStyle="None"
                        BorderWidth="0">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="ID EKSEKUTOR">
                                <ItemTemplate>
                                    <a href="javascript:OpenWinCollector('<%#Container.DataItem("CollectorID")%>','<%#Container.dataItem("CGID")%>');">
                                        <asp:Label ID="lblExecutorID" runat="server" Text='<%# Container.DataItem("CollectoriD")%>'>
                                        </asp:Label></a>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="COLLECTORNAME" HeaderText="NAMA EKSEKUTOR"></asp:BoundColumn>
                            <asp:BoundColumn DataField="AGREEMENTONHAND" HeaderText="SKP DITANGANI"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="HISTORY">
                                <ItemTemplate>
                                    <a href="javascript:OpenWinHistory('<%#Container.DataItem("CGID")%>','<%#Container.DataItem("CollectorID")%>','<%#Container.DataItem("CollectorName")%>');">
                                        <asp:Image ID="imgHistory" runat="server" ImageUrl="../../../Images/IconTnC.gif">
                                        </asp:Image></a>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                    <div class="form_box_hide">
                        <div class="button_gridnavigation">
                            <asp:ImageButton ID="imbFirstPageSelect" runat="server" ImageUrl="../../../Images/grid_navbutton01.png"
                                CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbPrevPageSelect" runat="server" ImageUrl="../../../Images/grid_navbutton02.png"
                                CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbNextPageSelect" runat="server" ImageUrl="../../../Images/grid_navbutton03.png"
                                CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbLastPageSelect" runat="server" ImageUrl="../../../Images/grid_navbutton04.png"
                                CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                            </asp:ImageButton>
                            <asp:TextBox ID="txtPageSelect" runat="server">1</asp:TextBox>
                            <asp:Button ID="btnPageNumbSelect" runat="server" Text="Go" CssClass="small buttongo blue"
                                EnableViewState="False"></asp:Button>
                            <asp:RangeValidator ID="rgvGoSelect" runat="server" ControlToValidate="txtPageSelect"
                                MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer"
                                CssClass="validator_general"></asp:RangeValidator>
                            <asp:RequiredFieldValidator ID="rfvGoSelect" runat="server" ControlToValidate="txtPageSelect"
                                ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general">
                            </asp:RequiredFieldValidator>
                        </div>
                        <div class="label_gridnavigation">
                            <asp:Label ID="lblPageSelect" runat="server"></asp:Label>of
                            <asp:Label ID="lblTotPageSelect" runat="server"></asp:Label>, Total
                            <asp:Label ID="lblrecordSelect" runat="server"></asp:Label>record(s)
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
