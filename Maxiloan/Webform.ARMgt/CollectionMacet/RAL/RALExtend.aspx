﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="RALExtend.aspx.vb" Inherits="Maxiloan.Webform.ARMgt.RALExtend" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>RALExtend</title>
    <script src="../../../Maxiloan.js" type="text/javascript" language="javascript"></script>
    <link rel="Stylesheet" href="../../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../../Include/General.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%#Request.servervariables("SERVER_NAME")%>/';
    </script>
    <script language="javascript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;
        function OpenWinCollector(pCollID, pCGID) {
            window.open('../../Webform.Setup/CollectorView.aspx?CGID=' + pCGID + '&CollectorID=' + pCollID + '', 'UserLookup', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }
        function OpenWinCustomer(pID) {
            window.open('../../../Webform.LoanOrg/Credit/ViewPersonalCustomer.aspx?CustomerID=' + pID + '&style=Collection', 'UserLookup', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>    
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
            <div class="form_title">
                <div class="form_single">
                    <h3>
                        <%--PERPANJANG SURAT KUASA TARIK--%>
                        PERPANJANG SURAT KUASA PENARIKAN
                    </h3>
                </div>
            </div>
            <asp:Panel ID="pnlsearch" runat="server">
                <div class="form_box">
                    <div class="form_single">
                        <label class ="label_req">
                            Collection Group </label>
                        <asp:DropDownList ID="cboCG" runat="server">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="rfvCG" runat="server" ErrorMessage="*" ControlToValidate="cboCG"
                            CssClass="validator_general" Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Cari Berdasarkan</label>
                        <asp:DropDownList ID="cboSearchBy" runat="server">
                            <asp:ListItem Value="Select One">Select One</asp:ListItem>
                            <asp:ListItem Value="CollectionAgreement.AgreementNo">No Kontrak</asp:ListItem>
                            <asp:ListItem Value="Customer.Name">Nama Customer</asp:ListItem>
                            <%--<asp:ListItem Value="RAL.RALNo">No SKT</asp:ListItem>--%>
                            <asp:ListItem Value="RAL.RALNo">No SKP</asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtSearchBy" runat="server" ></asp:TextBox>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Hari Sampai Expired &lt;=</label>
                        <asp:TextBox ID="txtDays" runat="server"  Width="51px"></asp:TextBox>
                        <asp:RangeValidator ID="rgvDays" runat="server" ErrorMessage="*" ControlToValidate="txtDays"
                            CssClass="validator_general" Display="Dynamic" MaximumValue="90" Type="Integer"
                            MinimumValue="0"></asp:RangeValidator>days
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="Buttonsearch" runat="server" Text="Search" CssClass="small button blue">
                    </asp:Button>&nbsp;
                    <asp:Button ID="ButtonReset" runat="server" Text="Reset" CssClass="small button gray"
                        CausesValidation="False"></asp:Button>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlDtGrid" runat="server">
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            <%--PERPANJANGAN SURAT KUASA TARIK--%>
                            PERPANJANGAN SURAT KUASA PENARIKAN
                        </h4>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ws">
                            <asp:DataGrid ID="dtgRALExtend" runat="server" Width="1500px" AllowSorting="True"
                                AutoGenerateColumns="False" DataKeyField="AgreementNo" OnSortCommand="SortGrid"
                                CssClass="grid_general" BorderStyle="None" BorderWidth="0">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hypAgreementNodtg" runat="server" Text='<%# Container.Dataitem("AgreementNo")%>'>
                                            </asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="CustomerName" HeaderText="NAMA CUSTOMER">
                                        <ItemTemplate>
                                            <a href="javascript:OpenWinCustomer('<%# Container.DataItem("CustomerID")%>');">
                                                <asp:Label ID="hypCustomerNamedtg" runat="server" Text='<%# Container.Dataitem("CustomerName")%>'>
                                                </asp:Label></a>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="INSTALLMENTNO" SortExpression="InstallmentNo" HeaderText="ANGS KE">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="ENDPASTDUEDAYS" SortExpression="EndPastDueDays" HeaderText="HARI TELAT">
                                    </asp:BoundColumn>
                                    <%--<asp:TemplateColumn SortExpression="RALNO" HeaderText="NO SKT">--%>
                                    <asp:TemplateColumn SortExpression="RALNO" HeaderText="NO SKP">
                                        <ItemTemplate>
                                            <asp:Image runat="server" ImageUrl="../../../images/red.gif" Visible='<%# isVisible(Container.DataItem("RALNo"))%>'
                                                ID="Image1"></asp:Image>
                                            <asp:Label ID="lblRALNodtg" runat="server" Text='<%# Container.dataitem("RALNo") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <%--<asp:BoundColumn DataField="RALPERIOD" SortExpression="RALPERIOD" HeaderText="PERIODE SKT">--%>
                                    <asp:BoundColumn DataField="RALPERIOD" SortExpression="RALPERIOD" HeaderText="PERIODE SKP">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="Printed" SortExpression="Printed" HeaderText="PRINTED" Visible="false" >
                                    </asp:BoundColumn>
                                    <asp:TemplateColumn SortExpression="EXECUTORID" HeaderText="EKSEKUTOR">
                                        <ItemTemplate>
                                            <a href="javascript:OpenWinCollector('<%# Container.DataItem("ExecutorID")%>','<%# Container.DataItem("CGID")%>')">
                                                <asp:Label ID="hypExecutorIDdtg" runat="server" Text='<%# Container.DataItem("ExecutorID")%>'>
                                                </asp:Label></a>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="CollectorName" SortExpression="CollectorName" HeaderText="NAMA">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="CollectorType" SortExpression="CollectorType" HeaderText="TIPE">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="CollectorTypeID" SortExpression="CollectorTypeID" HeaderText="CollectorTypeID" Visible ="false" >
                                    </asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="PERPANJANG">
                                        <ItemTemplate>
                                            <asp:ImageButton CommandName="Extend" ID="imgSelect" runat="server" ImageUrl="../../../images/IconDocument.gif">
                                            </asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <%--<asp:TemplateColumn HeaderText="CETAK SKT">--%>
                                    <asp:TemplateColumn HeaderText="CETAK SKP">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgPrint" CommandName="Print" runat="server" ImageUrl="../../../images/IconPrinter.gif">
                                            </asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="CETAK CHECKLIST">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgPrintCheckList" CommandName="PrintCheckList" runat="server"
                                                ImageUrl="../../../images/IconPrinter.gif"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="ApplicationId" HeaderText="ApplicationId" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblApplicationId" runat="server" Text='<%# Container.Dataitem("ApplicationId")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                        </div>
                        <div class="button_gridnavigation">
                            <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../../Images/grid_navbutton01.png"
                                CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../../Images/grid_navbutton02.png"
                                CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../../Images/grid_navbutton03.png"
                                CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../../Images/grid_navbutton04.png"
                                CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                            </asp:ImageButton>
                            <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                            <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                                EnableViewState="False"></asp:Button>
                            <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" MinimumValue="1"
                                ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"></asp:RangeValidator>
                            <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                                ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general">
                            </asp:RequiredFieldValidator>
                        </div>
                        <div class="label_gridnavigation">
                            <asp:Label ID="lblPage" runat="server"></asp:Label>of
                            <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                            <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlExtend" runat="server">
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            <%--PERPANJANG SURAT KUASA TARIK--%>
                            PERPANJANG SURAT KUASA PENARIKAN
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            No Kontrak</label>
                        <asp:HyperLink ID="hypAgreementNoSlct" runat="server"></asp:HyperLink>
                    </div>
                    <div class="form_right">
                        <label>
                            Nama Customer</label>
                        <asp:HyperLink ID="hypCustomerNameSlct" runat="server"></asp:HyperLink>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Alamat</label>
                        <asp:Label ID="lblAddress" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Nama Asset</label>
                        <asp:Label ID="lblAsset" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Angsuran Ke</label>
                        <asp:Label ID="lblInstallmentNo" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Jumlah Angsuran</label>
                        <asp:Label ID="lblInstallmentAmount" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Tanggal Jatuh Tempo</label>
                        <asp:Label ID="lblDuedate" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Jumlah Hari Telat</label>
                        <asp:Label ID="lblOverDueDays" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Jumlah Tunggakan</label>
                        <asp:Label ID="lblODAmount" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Denda Keterlambatan</label>
                        <asp:Label ID="lblLateCharges" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Biaya Tagih</label>
                        <asp:Label ID="lblOSBillingCharges" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Biaya Collection</label>
                        <asp:Label ID="lblCollExpense" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Sisa A/R</label>
                        <asp:Label ID="lblOSBallance" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            <%--No SKT--%>
                            No SKP
                        </label>
                        <asp:Label ID="lblRALNo" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            <%--Periode SKT--%>
                            Periode SKP
                        </label>
                        <asp:Label ID="lblRALPeriod" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Eksekutor</label>
                        <asp:Label ID="lblExecutor" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            <%--Jumlah SKT ditangani--%>
                            Jumlah SKP ditangani
                        </label>
                        <asp:Label ID="lblOnHand" runat="server"></asp:Label>&nbsp;days
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            <%--Maksimum Tgl Berakhirnya SKT--%>
                            Maksimum Tgl Berakhirnya SKP
                        </label>
                        <asp:Label ID="lblMaxEndDate" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                       <label class ="label_req">
                            <%--Tgl Berakhirnya SKT Baru--%>
                            Tgl Berakhirnya SKP Baru
                       </label>
                        <asp:TextBox ID="txtRALEndDate" runat="server"></asp:TextBox>
                        <asp:CalendarExtender ID="txtRALEndDate_CalendarExtender" runat="server" Enabled="True"
                            TargetControlID="txtRALEndDate" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                            CssClass="validator_general" ControlToValidate="txtRALEndDate"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_general">
                            Catatan</label>
                        <asp:TextBox ID="txtNotes" runat="server" CssClass="multiline_textbox" TextMode="MultiLine"></asp:TextBox>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonSave" runat="server" Text="Save" CssClass="small button blue">
                    </asp:Button>&nbsp;
                    <asp:Button ID="ButtonCancel" runat="server" Text="Cancel" CssClass="small button gray"
                        CausesValidation="False"></asp:Button>
                </div>
            </asp:Panel>        
    </form>
</body>
</html>
