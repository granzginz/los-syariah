﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class RALPrinting
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private currentPageSelect As Int32 = 1
    Private pageSizeSelect As Int16 = 10
    Private currentPageNumberSelect As Int16 = 1
    Private totalPagesSelect As Double = 1
    Private recordCountSelect As Int64 = 1


    Private oCustomClass As New Parameter.RALPrinting
    Private oController As New RALPrintingController
    Dim LimitHariSKE As Integer = 0
#End Region

    Private Property ApplicationID() As String
        Get
            Return CStr(Viewstate("ApplicationID"))
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.FormID = "RALPrint"
            If CheckForm(Me.Loginid, Me.FormID, "Maxiloan") Then
                InitialDefaultPanel()
                FillCbo()
                Me.SearchBy = ""
            End If
            If Request.QueryString("strFileLocation") <> "" Then
                Dim strHTTPServer As String
                Dim StrHTTPApp As String
                Dim strNameServer As String
                Dim strFileLocation As String

                strHTTPServer = Request.ServerVariables("PATH_INFO")
                strNameServer = Request.ServerVariables("SERVER_NAME")
                StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                strFileLocation = "http://" & strNameServer & "/" & StrHTTPApp & "/XML/" & Request.QueryString("strFileLocation") & ".pdf"

                Response.Write("<script language = javascript>" & vbCrLf _
                & "var x = screen.width;" & vbCrLf _
                & "var y = screen.heigth;" & vbCrLf _
                & "window.open('" & strFileLocation & "','Collection', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes') " & vbCrLf _
                & "</script>")

                Me.SearchBy = "CollectionAgreement.CGID='" & cboCG.SelectedItem.Value.Trim & "' and CollectionAgreement.EndPastDuedays > 0 and RAL.RALStatus='OP' "
                pnlDtGrid.Visible = True
                DoBind(Me.SearchBy, "")
            End If

        End If
    End Sub

    Private Sub InitialDefaultPanel()
        pnlDtGrid.Visible = False
        pnlsearch.Visible = True
        pnlSelect.Visible = False        
    End Sub

    Private Sub FillCbo()
        Dim oDataTable As New DataTable
        With oCustomClass
            .strConnection = GetConnectionSTring
            .strKey = "CG"
            .CGID = Me.GroubDbID
            .CollectorType = ""
        End With
        oCustomClass = oController.RALListCG(oCustomClass)
        oDataTable = oCustomClass.ListRAL
        With cboCG
            .DataSource = oDataTable
            .DataTextField = "CGName"
            .DataValueField = "CGID"
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = ""
            If oDataTable.Rows.Count = 1 Then
                .Items.FindByValue(Me.GroubDbID).Selected = True
            End If
        End With
    End Sub

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView

        With oCustomClass
            .strConnection = GetConnectionString
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oCustomClass = oController.RALPrintingList(oCustomClass)

        DtUserList = oCustomClass.ListRAL
        DvUserList = DtUserList.DefaultView
        recordCount = oCustomClass.TotalRecord
        DvUserList.Sort = Me.SortBy
        dtgRALPrinting.DataSource = DvUserList

        Try
            dtgRALPrinting.DataBind()
        Catch
            dtgRALPrinting.CurrentPageIndex = 0
            dtgRALPrinting.DataBind()
        End Try
        PagingFooter()
        pnlDtGrid.Visible = True
        pnlsearch.Visible = True        
    End Sub

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then            
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click        
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub
#End Region

#Region "NavigationSelect"
    Private Sub PagingFooterSelect()
        lblPageSelect.Text = currentPageSelect.ToString()
        totalPagesSelect = Math.Ceiling(CType((recordCountSelect / CType(pageSizeSelect, Integer)), Double))
        If totalPagesSelect = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPageSelect.Text = "1"
            rgvGoSelect.MaximumValue = "1"
        Else
            lblTotPageSelect.Text = CType(totalPagesSelect, String)
            rgvGoSelect.MaximumValue = CType(totalPagesSelect, String)
        End If
        lblRecordSelect.Text = CType(recordCountSelect, String)

        If currentPageSelect = 1 Then
            imbPrevPageSelect.Enabled = False
            imbFirstPageSelect.Enabled = False
            If totalPagesSelect > 1 Then
                imbNextPageSelect.Enabled = True
                imbLastPageSelect.Enabled = True
            Else
                imbPrevPageSelect.Enabled = False
                imbNextPageSelect.Enabled = False
                imbLastPageSelect.Enabled = False
                imbFirstPageSelect.Enabled = False
            End If
        Else
            imbPrevPageSelect.Enabled = True
            imbFirstPageSelect.Enabled = True
            If currentPageSelect = totalPagesSelect Then
                imbNextPageSelect.Enabled = False
                imbLastPageSelect.Enabled = False
            Else
                imbLastPageSelect.Enabled = True
                imbNextPageSelect.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLinkSelect_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "FirstSelect" : currentPageSelect = 1
            Case "LastSelect" : currentPageSelect = Int32.Parse(lblTotPageSelect.Text)
            Case "NextSelect" : currentPageSelect = Int32.Parse(lblPageSelect.Text) + 1
            Case "PrevSelect" : currentPageSelect = Int32.Parse(lblPageSelect.Text) - 1
        End Select
        'If Me.SortBy Is Nothing Then
        '    Me.SortBy = ""
        'End If
        BindGridExecutorHist()
    End Sub

    Private Sub imbGoPageSelect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumbSelect.Click        
        If IsNumeric(txtPageSelect.Text) Then
            If CType(lblTotPageSelect.Text, Integer) > 1 And CType(txtPageSelect.Text, Integer) <= CType(lblTotPageSelect.Text, Integer) Then
                currentPageSelect = CType(txtPageSelect.Text, Int16)
                'If Me.SortBy Is Nothing Then
                '    Me.SortBy = ""
                'End If
                BindGridExecutorHist()
            End If
        End If
    End Sub
    Sub BindGridSelectExecutor()
        Dim DtUserList As New DataTable                

        With oCustomClass
            .strConnection = GetConnectionString
            .CGID = Me.GroubDbID
            .CurrentPage = currentPageSelect
            .PageSize = pageSizeSelect
            .SPType = "COMBO"
        End With

        oCustomClass = oController.RALViewDataCollector(oCustomClass)
        DtUserList = oCustomClass.ListRAL        
        recordCountSelect = oCustomClass.TotalRecord

        With cboSelectExecutor
            .DataSource = DtUserList
            .DataTextField = "CollectorName"
            .DataValueField = "CollectorID"
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = ""
        End With
    End Sub

    Sub BindGridExecutorHist()
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView

        With oCustomClass
            .strConnection = GetConnectionString()
            .CGID = Me.GroubDbID
            .CurrentPage = currentPageSelect
            .PageSize = pageSizeSelect
            .SPType = "HiSTORY"
        End With

        oCustomClass = oController.RALViewDataCollector(oCustomClass)
        DtUserList = oCustomClass.ListRAL
        DvUserList = DtUserList.DefaultView
        recordCountSelect = oCustomClass.TotalRecord
        dtgSelect.DataSource = DvUserList

        Try
            dtgSelect.DataBind()           
        Catch
            dtgSelect.CurrentPageIndex = 0
            dtgSelect.DataBind()
        End Try

        'PagingFooterSelect()
        pnlSelect.Visible = True
        pnlsearch.Visible = False
        pnlDtGrid.Visible = False
    End Sub
#End Region

    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Buttonsearch.Click
        If CheckFeature(Me.Loginid, Me.FormID, "Srch", "Maxiloan") Then
            Me.SearchBy = "CollectionAgreement.CGID='" & cboCG.SelectedItem.Value.Trim & "' and CollectionAgreement.EndPastDuedays > 0 and RAL.RALStatus='OP' "
            If cboSearchBy.SelectedItem.Value.Trim <> "Select One" Then
                Me.SearchBy = Me.SearchBy & " and " & cboSearchBy.SelectedItem.Value.Trim & " LIKE '%" & txtSearchBy.Text.Trim & "%'"
            End If
            'Me.SearchBy = Me.SearchBy & " and CollectionAgreement.EndPastDuedays > 0 and RAL.RALStatus='OP' "
            Me.SortBy = ""
            DoBind(Me.SearchBy, Me.SortBy)
        End If
    End Sub

    Public Function isVisible(ByVal strKey As String) As Boolean
        If strKey.Trim = "" Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function isVisiblePrint(ByVal strKey As String) As Boolean
        If strKey.Trim = "" Then
            Return False
        Else
            Return True
        End If
    End Function

    Private Sub dtgRALPrinting_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgRALPrinting.ItemCommand
        If e.CommandName = "Select" Then
            Dim lblApplicationId As Label
            Dim oDataTable As New DataTable
            lblApplicationId = CType(dtgRALPrinting.Items(e.Item.ItemIndex).FindControl("lblApplicationId"), Label)
            If checkfeature(Me.Loginid, Me.FormID, "Slect", "Maxiloan") Then
                With oCustomClass
                    .strConnection = GetConnectionString
                    .ApplicationID = lblApplicationId.Text.Trim
                    .RALNo = CType(dtgRALPrinting.Items(e.Item.ItemIndex).Cells(4).Text.Trim, String)
                End With
                oCustomClass = oController.RALViewDataSelect(oCustomClass)
                oDataTable = oCustomClass.ListRAL
                Me.ApplicationID = lblApplicationId.Text.Trim


                If Not IsDBNull(oDataTable.Rows(0)("AgreementNo")) Then
                    hypAgreementNoSlct.Text = CType(oDataTable.Rows(0)("AgreementNo"), String)
                End If
                If Not IsDBNull(oDataTable.Rows(0)("CustomerName")) Then
                    hypCustomerNameSlct.Text = CType(oDataTable.Rows(0)("CustomerName"), String)
                End If
                If Not IsDBNull(oDataTable.Rows(0)("MailingAddress")) Then
                    lblAddress.Text = CType(oDataTable.Rows(0)("MailingAddress"), String)
                End If
                If Not IsDBNull(oDataTable.Rows(0)("Description")) Then
                    lblAsset.Text = CType(oDataTable.Rows(0)("Description"), String)
                End If
                If Not IsDBNull(oDataTable.Rows(0)("InstallmentNo")) Then
                    lblInstallmentNo.Text = CType(oDataTable.Rows(0)("InstallmentNo"), String)
                End If
                If Not IsDBNull(oDataTable.Rows(0)("DueDate")) Then
                    lblDuedate.Text = Format(oDataTable.Rows(0)("DueDate"), "dd/MM/yyyy")
                End If
                If Not IsDBNull(oDataTable.Rows(0)("EndPastDueAmt")) Then
                    lblODAmount.Text = FormatNumber(oDataTable.Rows(0)("EndPastDueAmt"), 2)
                End If
                If Not IsDBNull(oDataTable.Rows(0)("OSBillingCharges")) Then
                    lblOSBillingCharges.Text = FormatNumber(oDataTable.Rows(0)("OSBillingCharges"), 2)
                End If
                If Not IsDBNull(oDataTable.Rows(0)("OS_Gross")) Then
                    lblOSBallance.Text = FormatNumber(oDataTable.Rows(0)("OS_Gross"), 2)
                End If
                If Not IsDBNull(oDataTable.Rows(0)("RALNo")) Then
                    lblRALNo.Text = CType(oDataTable.Rows(0)("RALNo"), String)
                End If
                If Not IsDBNull(oDataTable.Rows(0)("InstallmentAmount")) Then
                    lblInstallmentAmount.Text = FormatNumber(oDataTable.Rows(0)("InstallmentAmount"), 2)
                End If
                If Not IsDBNull(oDataTable.Rows(0)("EndPastDuedays")) Then
                    lblOverDueDays.Text = CType(oDataTable.Rows(0)("EndPastDuedays"), String)
                End If
                If Not IsDBNull(oDataTable.Rows(0)("LateCharges")) Then
                    lblLateCharges.Text = FormatNumber(oDataTable.Rows(0)("LateCharges"), 2)
                End If
                If Not IsDBNull(oDataTable.Rows(0)("StartDate")) Then
                    lblRALPeriod1.Text = Format(oDataTable.Rows(0)("StartDate"), "dd/MM/yyyy")
                End If
                If Not IsDBNull(oDataTable.Rows(0)("endDate")) Then
                    lblRALPeriod2.Text = Format(oDataTable.Rows(0)("endDate"), "dd/MM/yyyy")
                End If
                If Not IsDBNull(oDataTable.Rows(0)("CollectionExpense")) Then
                    lblCollExpense.Text = FormatNumber(oDataTable.Rows(0)("CollectionExpense"), 2)
                End If
                lblPrevExc.Text = CType(IIf(IsDBNull(oDataTable.Rows(0)("PreviousExecutor")), "", oDataTable.Rows(0)("PreviousExecutor")), String)

                BindGridSelectExecutor()
                BindGridSelectPemberiKuasa()
                BindGridExecutorHist()

                'With oCustomClass
                '    .strConnection = GetConnectionString
                '    .CGID = Me.GroubDbID
                'End With
                'oCustomClass = oController.RALViewDataCollector(oCustomClass)
                'oDataTable = oCustomClass.ListRAL
                'dtgSelect.DataSource = oDataTable
                'dtgSelect.DataBind()
                'With cboSelectExecutor
                '    .DataSource = oDataTable
                '    .DataTextField = "CollectorName"
                '    .DataValueField = "CollectorID"
                '    .DataBind()
                '    .Items.Insert(0, "Select One")
                '    .Items(0).Value = ""
                'End With
                'pnlSelect.Visible = True
                'pnlsearch.Visible = False
                'pnlDtGrid.Visible = False
            End If
        ElseIf e.CommandName = "Print" Then
            Dim lblApplicationId As Label
            lblApplicationId = CType(dtgRALPrinting.Items(e.Item.ItemIndex).FindControl("lblApplicationId"), Label)
            If checkfeature(Me.Loginid, Me.FormID, "PrRAL", "Maxiloan") Then
                With oCustomClass
                    .strConnection = GetConnectionSTring
                    .ApplicationID = lblApplicationId.Text.Trim
                    .RALNo = CType(dtgRALPrinting.Items(e.Item.ItemIndex).Cells(4).Text.Trim, String)
                    .BusinessDate = Me.BusinessDate
                    .LoginId = Me.Loginid
                End With
                oCustomClass = oController.RALSaveDataPrintRAL(oCustomClass)

                If oCustomClass.hasil = 0 Then                    
                    ShowMessage(lblMessage, "Gagal", True)
                Else
                    Dim cookie As HttpCookie = Request.Cookies("RptRALPrinting")
                    'If Not cookie Is Nothing Then
                    '    cookie.Values("ExecutorID") = CType(dtgRALPrinting.Items(e.Item.ItemIndex).Cells(5).Text.Trim, String)
                    '    cookie.Values("RALNo") = CType(dtgRALPrinting.Items(e.Item.ItemIndex).Cells(4).Text.Trim, String)
                    '    cookie.Values("ApplicationID") = lblApplicationId.Text.Trim
                    '    cookie.Values("CGID") = cboCG.SelectedItem.Value.Trim
                    '    Response.AppendCookie(cookie)
                    'Else
                    Dim strExecutorType As String = CType(dtgRALPrinting.Items(e.Item.ItemIndex).Cells(9).Text.Trim, String)
                    Dim cookieNew As New HttpCookie("RptRALPrinting")
                    cookieNew.Values.Add("ExecutorID", CType(dtgRALPrinting.Items(e.Item.ItemIndex).Cells(6).Text.Trim, String))
                    cookieNew.Values.Add("RALNo", CType(dtgRALPrinting.Items(e.Item.ItemIndex).Cells(4).Text.Trim, String))
                    cookieNew.Values.Add("ApplicationID", lblApplicationId.Text.Trim)
                    cookieNew.Values.Add("CGID", cboCG.SelectedItem.Value.Trim)
                    cookieNew.Values.Add("ExecutorTypeID", strExecutorType)
                    Response.AppendCookie(cookieNew)
                    'End If
                    Response.Redirect("RALPrintingViewer.aspx?cmd=RAL&back=Printing")
                End If
            End If
        ElseIf e.CommandName = "PrintCheckList" Then
            Dim lblApplicationId As Label            
            lblApplicationId = CType(dtgRALPrinting.Items(e.Item.ItemIndex).FindControl("lblApplicationId"), Label)
            If checkfeature(Me.Loginid, Me.FormID, "PrLst", "Maxiloan") Then
                Dim cookie As HttpCookie = Request.Cookies("RptRALPrinting")
                'If Not cookie Is Nothing Then
                '    cookie.Values("ExecutorID") = CType(dtgRALPrinting.Items(e.Item.ItemIndex).Cells(5).Text.Trim, String)
                '    cookie.Values("ApplicationID") = lblApplicationId.Text.Trim
                '    Response.AppendCookie(cookie)
                'Else
                Dim cookieNew As New HttpCookie("RptRALPrinting")
                cookieNew.Values.Add("ExecutorID", CType(dtgRALPrinting.Items(e.Item.ItemIndex).Cells(6).Text.Trim, String))
                cookieNew.Values.Add("ApplicationID", lblApplicationId.Text.Trim)
                cookieNew.Values.Add("CGId", cboCG.SelectedItem.Value.Trim)
                cookieNew.Values.Add("RALNo", CType(dtgRALPrinting.Items(e.Item.ItemIndex).Cells(4).Text.Trim, String))
                Response.AppendCookie(cookieNew)
                'End If
                Response.Redirect("RALPrintingViewer.aspx?cmd=CheckList&back=Printing")
            End If
        End If
    End Sub

    Private Sub imbCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        pnlsearch.Visible = True
        pnlDtGrid.Visible = True
        pnlSelect.Visible = False
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
        Dim hasil As Integer
        If CInt(MasaBerlaku.Text) > getLimitCetakSKE() Then
            ShowMessage(lblMessage, "Masa Berlaku Surat Kuasa harus <= " & getLimitCetakSKE(), True)
            Exit Sub
        End If

        If cboSelectExecutor.SelectedItem.Value.Trim = "" Then            
            ShowMessage(lblMessage, "Harap pilih Eksekutor", True)
            Exit Sub
        End If

        If cboSelectPemberiKuasa.SelectedItem.Value.Trim = "" Then
            ShowMessage(lblMessage, "Harap pilih Pemberi Kuasa", True)
            Exit Sub
        End If

        With oCustomClass
            .strConnection = GetConnectionString()
            .ApplicationID = Me.ApplicationID
            .RALNo = lblRALNo.Text.Trim
            .ExecutorID = cboSelectExecutor.SelectedItem.Value
            .BusinessDate = Me.BusinessDate
            .MASASKE = MasaBerlaku.Text
            .IdPemberiKuasa = cboSelectPemberiKuasa.SelectedItem.Value
        End With
        oCustomClass = oController.RALSaveDataExecutor(oCustomClass)
        hasil = oCustomClass.hasil
        If hasil = 0 Then            
            ShowMessage(lblMessage, "Gagal", True)
        Else
            pnlsearch.Visible = True
            pnlDtGrid.Visible = True
            pnlSelect.Visible = False
            DoBind(Me.SearchBy, Me.SortBy)
        End If
    End Sub

    Private Sub imbReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        InitialDefaultPanel()
        cboCG.ClearSelection()
        cboSearchBy.ClearSelection()
        txtSearchBy.Text = ""
    End Sub

    Private Sub dtgRALPrinting_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgRALPrinting.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim hyTemp As HyperLink
            Dim lblApplicationId As Label
            lblApplicationId = CType(e.Item.FindControl("lblApplicationId"), Label)
            '*** Agreement No link
            hyTemp = CType(e.Item.FindControl("hypAgreementNodtg"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenAgreementNo('" & "Collection" & "', '" & Server.UrlEncode(lblApplicationId.Text.Trim) & "')"
        End If
    End Sub

    'Wira 20161019
    Private Function getLimitCetakSKE() As Decimal
        If LimitHariSKE = 0 Then
            Dim oClass As New Parameter.GeneralSetting
            Dim oCtr As New GeneralSettingController
            With oClass
                .strConnection = GetConnectionString()
                .GSID = "LimitSKE"
                .ModuleID = "GEN"
            End With
            Try
                Return oCtr.GetGeneralSettingValue(oClass).Trim
            Catch ex As Exception
                Return ""
            End Try
        Else
            Return LimitHariSKE
        End If
    End Function

    Sub BindGridSelectPemberiKuasa()
        Dim DtUserList As New DataTable

        With oCustomClass
            .strConnection = GetConnectionString
            .CGID = Me.GroubDbID
            .CurrentPage = currentPageSelect
            .PageSize = pageSizeSelect
            .SPType = "PEMBERIKUASA"
        End With

        oCustomClass = oController.RALViewDataCollector(oCustomClass)
        DtUserList = oCustomClass.ListRAL
        recordCountSelect = oCustomClass.TotalRecord

        With cboSelectPemberiKuasa
            .DataSource = DtUserList
            .DataTextField = "CollectorName"
            .DataValueField = "CollectorID"
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = ""
        End With
    End Sub
End Class