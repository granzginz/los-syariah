﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class RALOnRequest
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents GridNavigator As ucGridNav
    Protected WithEvents GridNavigatorSelect As ucGridNav
    Private Property ApplicationID() As String
        Get
            Return CStr(viewstate("ApplicationID"))
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property
#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private currentPageSelect As Int32 = 1
    Private pageSizeSelect As Int16 = 10
    Private currentPageNumberSelect As Int16 = 1
    Private totalPagesSelect As Double = 1
    Private recordCountSelect As Int64 = 1
    Private oCustomClass As New Parameter.RALPrinting
    Private oController As New RALOnRequestController
    Dim LimitHariSKE As Integer = 0
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        'If SessionInvalid() Then
        '    Exit Sub
        'End If
        AddHandler GridNavigator.PageChanged, AddressOf PageNavigation
        AddHandler GridNavigatorSelect.PageChanged, AddressOf PageNavigationSelect
        If Not IsPostBack Then
            Me.FormID = "RALPrint"
            If CheckForm(Me.Loginid, Me.FormID, "Maxiloan") Then
                InitialDefaultPanel()
                FillCbo()
                Me.SearchBy = ""
            End If
            If Request.QueryString("strFileLocation") <> "" Then
                Dim strHTTPServer As String
                Dim StrHTTPApp As String
                Dim strNameServer As String
                Dim strFileLocation As String

                strHTTPServer = Request.ServerVariables("PATH_INFO")
                strNameServer = Request.ServerVariables("SERVER_NAME")
                StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                strFileLocation = "http://" & strNameServer & "/" & StrHTTPApp & "/XML/" & Request.QueryString("strFileLocation") & ".pdf"
                Response.Write("<script language = javascript>" & vbCrLf _
                 & "var x = screen.width;" & vbCrLf _
                 & "var y = screen.heigth;" & vbCrLf _
                 & "window.open('" & strFileLocation & "','Collection', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes') " & vbCrLf _
                 & "</script>")
            End If

            'Me.SearchBy = "CollectionAgreement.CGID='" & cboCG.SelectedItem.Value.Trim & "'"
            'pnlDtGrid.Visible = True
            'DoBind(Me.SearchBy.Trim, "")
        End If
    End Sub

    Private Sub InitialDefaultPanel()
        pnlDtGrid.Visible = False
        pnlsearch.Visible = True
        pnlSelect.Visible = False        
    End Sub

    Private Sub FillCbo()
        Dim oDataTable As New DataTable
        With oCustomClass
            .strConnection = GetConnectionSTring
            .strKey = "CG"
            .CGID = Me.GroubDbID
            .CollectorType = ""
        End With
        oCustomClass = oController.RALListCG(oCustomClass)
        oDataTable = oCustomClass.ListRAL
        With cboCG
            .DataSource = oDataTable
            .DataTextField = "CGName"
            .DataValueField = "CGID"
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = ""
            If oDataTable.Rows.Count = 1 Then
                .Items.FindByValue(Me.GroubDbID).Selected = True
            End If
        End With
    End Sub
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String, Optional isFrNav As Boolean = False)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView

        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oCustomClass = oController.RALPrintingList(oCustomClass)

        DtUserList = oCustomClass.ListRAL
        DvUserList = DtUserList.DefaultView
        recordCount = oCustomClass.TotalRecord
        DvUserList.Sort = Me.SortBy
        dtgRALPrinting.DataSource = DvUserList

        Try
            dtgRALPrinting.DataBind()
        Catch
            dtgRALPrinting.CurrentPageIndex = 0
            dtgRALPrinting.DataBind()
        End Try
        '  PagingFooter()
        If (isFrNav = False) Then
            GridNavigator.Initialize(recordCount, pageSize)
        End If
        pnlDtGrid.Visible = True
        pnlsearch.Visible = True
    End Sub

    Protected Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        currentPage = e.CurrentPage
        DoBind(Me.SearchBy, Me.SortBy, True)
        GridNavigator.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
    End Sub 


    Protected Sub PageNavigationSelect(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        currentPage = e.CurrentPage
        DoBind(Me.SearchBy, Me.SortBy, True)
        GridNavigatorSelect.ReInitialize(e.CurrentPage, recordCountSelect, e.TotalPage)
    End Sub 


    Sub BindGridSelectExecutor(Optional isFrNav As Boolean = False)
        Dim DtUserList As New DataTable

        With oCustomClass
            .strConnection = GetConnectionString()
            .CGID = Me.GroubDbID
            .CurrentPage = currentPageSelect
            .PageSize = pageSizeSelect
            .SPType = "COMBO"
        End With

        oCustomClass = oController.RALViewDataCollector(oCustomClass)
        DtUserList = oCustomClass.ListRAL
        recordCountSelect = oCustomClass.TotalRecord

        With cboSelectExecutor
            .DataSource = DtUserList
            .DataTextField = "CollectorName"
            .DataValueField = "CollectorID"
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = ""
        End With

        Dim caseController As New CasesSettingController
        Dim CsSetting As New Parameter.CasesSetting
        CsSetting.strConnection = GetConnectionString()
        CsSetting = caseController.ListCases(CsSetting)
        With cboCases
            .DataSource = CsSetting.ListCasesSetting
            .DataTextField = "Penyebab"
            .DataValueField = "CasesID"
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = ""
        End With


        If (isFrNav = False) Then
            GridNavigatorSelect.Initialize(recordCountSelect, pageSizeSelect)
        End If
    End Sub

    Sub BindGridExecutorHist()
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView

        With oCustomClass
            .strConnection = GetConnectionString()
            .CGID = Me.GroubDbID
            .CurrentPage = currentPageSelect
            .PageSize = pageSizeSelect
            .SPType = "HiSTORY"
        End With

        oCustomClass = oController.RALViewDataCollector(oCustomClass)
        DtUserList = oCustomClass.ListRAL
        DvUserList = DtUserList.DefaultView
        recordCountSelect = oCustomClass.TotalRecord
        dtgSelect.DataSource = DvUserList

        Try
            dtgSelect.DataBind()
        Catch
            dtgSelect.CurrentPageIndex = 0
            dtgSelect.DataBind()
        End Try

        'PagingFooterSelect()
        pnlSelect.Visible = True
        pnlsearch.Visible = False
        pnlDtGrid.Visible = False
    End Sub

    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Buttonsearch.Click
        If CheckFeature(Me.Loginid, Me.FormID, "Srch", "Maxiloan") Then
            Me.SearchBy = "CollectionAgreement.CGID='" & cboCG.SelectedItem.Value.Trim & "'"
            If cboSearchBy.SelectedItem.Value.Trim <> "Select One" And txtSearchBy.Text.Trim <> "" Then
                If Right(txtSearchBy.Text.Trim, 1) <> "%" Then
                    Me.SearchBy = Me.SearchBy & " and " & cboSearchBy.SelectedItem.Value.Trim & " like '%" + txtSearchBy.Text.Trim + "%'"
                Else
                    Me.SearchBy = Me.SearchBy & " and " & cboSearchBy.SelectedItem.Value.Trim & " like '%" + txtSearchBy.Text.Trim + "%'"
                End If
                Me.SearchBy = Me.SearchBy & " and (CollectionAgreement.EndPastDuedays >= 0 or ('" & Me.BusinessDate & "' >= CollectionAgreement.PlanDateToSKT and (CollectionAGreement.RALNo='' or CollectionAGreement.RALNo='-') and CollectionAgreement.EndPastDueDays >= 0)) AND CollectionAgreement.Repossess='N' "
            End If
            Me.SortBy = " PlanDateToSKT "
            DoBind(Me.SearchBy, Me.SortBy)
        End If
    End Sub
    Public Function isVisible(ByVal strKey As String) As Boolean
        If strKey.Trim = "" Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function isVisiblePrint(ByVal strKey As String) As Boolean
        If strKey.Trim = "" Then
            Return False
        Else
            Return True
        End If
    End Function

    Private Sub dtgRALPrinting_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgRALPrinting.ItemCommand
        If e.CommandName = "Select" Then
            Dim lblApplicationId As Label
            Dim oDataTable As New DataTable
            lblApplicationId = CType(dtgRALPrinting.Items(e.Item.ItemIndex).FindControl("lblApplicationId"), Label)
            If CheckFeature(Me.Loginid, Me.FormID, "Slect", "Maxiloan") Then
                With oCustomClass
                    .strConnection = GetConnectionString()
                    .ApplicationID = lblApplicationId.Text.Trim

                End With
                oCustomClass = oController.RequestDataSelect(oCustomClass)
                oDataTable = oCustomClass.ListRAL
                Me.ApplicationID = lblApplicationId.Text
                If Not IsDBNull(oDataTable.Rows(0)("AgreementNo")) Then
                    hypAgreementNoSlct.Text = CType(oDataTable.Rows(0)("AgreementNo"), String)
                End If
                If Not IsDBNull(oDataTable.Rows(0)("CustomerName")) Then
                    hypCustomerNameSlct.Text = CType(oDataTable.Rows(0)("CustomerName"), String)
                End If
                If Not IsDBNull(oDataTable.Rows(0)("MailingAddress")) Then
                    lblAddress.Text = CType(oDataTable.Rows(0)("MailingAddress"), String)
                End If
                If Not IsDBNull(oDataTable.Rows(0)("Description")) Then
                    lblAsset.Text = CType(oDataTable.Rows(0)("Description"), String)
                End If
                If Not IsDBNull(oDataTable.Rows(0)("InstallmentNo")) Then
                    lblInstallmentNo.Text = CType(oDataTable.Rows(0)("InstallmentNo"), String)
                End If
                If Not IsDBNull(oDataTable.Rows(0)("DueDate")) Then
                    lblDuedate.Text = Format(oDataTable.Rows(0)("DueDate"), "dd/MM/yyyy")
                End If
                If Not IsDBNull(oDataTable.Rows(0)("EndPastDueAmt")) Then
                    lblODAmount.Text = FormatNumber(oDataTable.Rows(0)("EndPastDueAmt"), 2)
                End If
                If Not IsDBNull(oDataTable.Rows(0)("OSBillingCharges")) Then
                    lblOSBillingCharges.Text = FormatNumber(oDataTable.Rows(0)("OSBillingCharges"), 2)
                End If
                If Not IsDBNull(oDataTable.Rows(0)("OS_Gross")) Then
                    lblOSBallance.Text = FormatNumber(oDataTable.Rows(0)("OS_Gross"), 2)
                End If
                If Not IsDBNull(oDataTable.Rows(0)("RALNo")) Then
                    lblRALNo.Text = CType(oDataTable.Rows(0)("RALNo"), String)
                End If
                If Not IsDBNull(oDataTable.Rows(0)("InstallmentAmount")) Then
                    lblInstallmentAmount.Text = FormatNumber(oDataTable.Rows(0)("InstallmentAmount"), 2)
                End If
                If Not IsDBNull(oDataTable.Rows(0)("EndPastDuedays")) Then
                    lblOverDueDays.Text = CType(oDataTable.Rows(0)("EndPastDuedays"), String)
                End If
                If Not IsDBNull(oDataTable.Rows(0)("LateCharges")) Then
                    lblLateCharges.Text = FormatNumber(oDataTable.Rows(0)("LateCharges"), 2)
                End If
                'If Not IsDBNull(oDataTable.Rows(0)("StartDate")) Then
                lblRALPeriod1.Text = ""
                'End If
                'If Not IsDBNull(oDataTable.Rows(0)("endDate")) Then
                lblRALPeriod2.Text = ""
                'End If
                If Not IsDBNull(oDataTable.Rows(0)("CollectionExpense")) Then
                    lblCollExpense.Text = FormatNumber(oDataTable.Rows(0)("CollectionExpense"), 2)
                End If

                BindGridSelectExecutor()
                BindGridSelectPemberiKuasa()
                BindGridExecutorHist()
            End If
        ElseIf e.CommandName = "Print" Then
            Dim lblApplicationId As Label
            Dim hasil As Integer
            lblApplicationId = CType(dtgRALPrinting.Items(e.Item.ItemIndex).FindControl("lblApplicationId"), Label)
            If CheckFeature(Me.Loginid, Me.FormID, "PrRAL", "Maxiloan") Then
                With oCustomClass
                    .strConnection = GetConnectionString()
                    .ApplicationID = lblApplicationId.Text.Trim
                    .RALNo = CType(dtgRALPrinting.Items(e.Item.ItemIndex).Cells(13).Text.Trim, String)
                    .BusinessDate = Me.BusinessDate
                    .LoginId = Me.Loginid
                End With
                oCustomClass = oController.RALSaveDataPrintRAL(oCustomClass)
                hasil = oCustomClass.hasil
                If hasil = 0 Then
                    ShowMessage(lblMessage, "Gagal", True)
                Else
                    Dim cookie As HttpCookie = Request.Cookies("RptRALPrinting")
                 
                    Dim cookieNew As New HttpCookie("RptRALPrinting")
                    cookieNew.Values.Add("ExecutorID", CType(dtgRALPrinting.Items(e.Item.ItemIndex).Cells(6).Text.Trim, String))
                    cookieNew.Values.Add("RALNo", CType(dtgRALPrinting.Items(e.Item.ItemIndex).Cells(13).Text.Trim, String))
                    cookieNew.Values.Add("ApplicationID", lblApplicationId.Text.Trim)
                    cookieNew.Values.Add("CGId", cboCG.SelectedValue.Trim)
                    cookieNew.Values.Add("ExecutorTypeID", CType(dtgRALPrinting.Items(e.Item.ItemIndex).Cells(9).Text.Trim, String))
                    Response.AppendCookie(cookieNew)
                    'End If
                    Response.Redirect("RALPrintingViewer.aspx?cmd=RAL&back=Request&File=RALOnRequest.aspx")
                End If
            End If
        ElseIf e.CommandName = "PrintCheckList" Then
            Dim lblApplicationId As Label

            lblApplicationId = CType(dtgRALPrinting.Items(e.Item.ItemIndex).FindControl("lblApplicationId"), Label)
            If CheckFeature(Me.Loginid, Me.FormID, "PrLst", "Maxiloan") Then
                Dim cookie As HttpCookie = Request.Cookies("RptRALPrinting")
                 
                Dim cookieNew As New HttpCookie("RptRALPrinting")
                cookieNew.Values.Add("ExecutorID", CType(dtgRALPrinting.Items(e.Item.ItemIndex).Cells(6).Text.Trim, String))
                cookieNew.Values.Add("RALNo", CType(dtgRALPrinting.Items(e.Item.ItemIndex).Cells(13).Text.Trim, String))
                cookieNew.Values.Add("ApplicationID", lblApplicationId.Text.Trim)
                cookieNew.Values.Add("CGId", cboCG.SelectedValue.Trim)
                Response.AppendCookie(cookieNew)
                'End If
                Response.Redirect("RALPrintingViewer.aspx?cmd=CheckList&back=Request&File=RALOnRequest.aspx")
            End If
        End If
    End Sub

    Private Sub imbCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        pnlsearch.Visible = True
        pnlDtGrid.Visible = True
        pnlSelect.Visible = False
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
        Dim hasil As Integer
        If CInt(MasaBerlaku.Text) > getLimitCetakSKE() Then
            ShowMessage(lblMessage, "Masa Berlaku Surat Kuasa harus <= " & getLimitCetakSKE(), True)
            Exit Sub
        End If

        If cboSelectExecutor.SelectedItem.Value.Trim = "" Then
            ShowMessage(lblMessage, "Harap pilih Eksekutor", True)
            Exit Sub
        End If
        With oCustomClass
            .strConnection = GetConnectionString()
            .ApplicationID = Me.ApplicationID
            .ExecutorID = cboSelectExecutor.SelectedItem.Value
            .BusinessDate = Me.BusinessDate
            .NextBD = Me.BusinessDate
            .MASASKE = MasaBerlaku.Text
            .Cases = cboCases.SelectedValue
            .RalNotes = txtNotes.Text
            .IdPemberiKuasa = cboSelectPemberiKuasa.SelectedItem.Value
        End With
        oCustomClass = oController.RALSaveDataExecutor(oCustomClass)
        hasil = oCustomClass.hasil
        If hasil = 0 Then
            ShowMessage(lblMessage, "Gagal", True)
        Else
            pnlsearch.Visible = True
            pnlDtGrid.Visible = True
            pnlSelect.Visible = False
            DoBind(Me.SearchBy, Me.SortBy)
        End If
    End Sub

    Private Sub imbReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        InitialDefaultPanel()
        cboCG.ClearSelection()
        cboSearchBy.ClearSelection()
        txtSearchBy.Text = ""
    End Sub

    Private Sub dtgRALPrinting_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgRALPrinting.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim hyTemp As HyperLink
            Dim lblApplicationId As Label
            lblApplicationId = CType(e.Item.FindControl("lblApplicationId"), Label)
            '*** Agreement No link
            hyTemp = CType(e.Item.FindControl("hypAgreementNodtg"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenAgreementNo('" & "Collection" & "', '" & Server.UrlEncode(lblApplicationId.Text.Trim) & "')"
        End If
    End Sub

    'Wira 20161019
    Private Function getLimitCetakSKE() As Decimal
        If LimitHariSKE = 0 Then
            Dim oClass As New Parameter.GeneralSetting
            Dim oCtr As New GeneralSettingController
            With oClass
                .strConnection = GetConnectionString()
                .GSID = "LimitSKE"
                .ModuleID = "GEN"
            End With
            Try
                Return oCtr.GetGeneralSettingValue(oClass).Trim
            Catch ex As Exception
                Return ""
            End Try
        Else
            Return LimitHariSKE
        End If
    End Function

    Sub BindGridSelectPemberiKuasa()
        Dim DtUserList As New DataTable

        With oCustomClass
            .strConnection = GetConnectionString
            .CGID = Me.GroubDbID
            .CurrentPage = currentPageSelect
            .PageSize = pageSizeSelect
            .SPType = "PEMBERIKUASA"
        End With

        oCustomClass = oController.RALViewDataCollector(oCustomClass)
        DtUserList = oCustomClass.ListRAL
        recordCountSelect = oCustomClass.TotalRecord

        With cboSelectPemberiKuasa
            .DataSource = DtUserList
            .DataTextField = "CollectorName"
            .DataValueField = "CollectorID"
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = ""
        End With
    End Sub
End Class