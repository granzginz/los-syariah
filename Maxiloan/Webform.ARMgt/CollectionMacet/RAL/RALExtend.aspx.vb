﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class RALExtend
    Inherits Maxiloan.Webform.WebBased    

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.RALExtend
    Private oController As New RALExtendController
#End Region

#Region "Property"
    Private Property ApplicationID() As String
        Get
            Return CStr(viewstate("ApplicationID"))
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property
    Private Property oData() As DataTable
        Get
            Return CType(viewstate("oData"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            viewstate("oData") = Value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            If Request.QueryString("strFileLocation") <> "" Then
                Dim strHTTPServer As String
                Dim StrHTTPApp As String
                Dim strNameServer As String
                Dim strFileLocation As String

                strHTTPServer = Request.ServerVariables("PATH_INFO")
                strNameServer = Request.ServerVariables("SERVER_NAME")
                StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                strFileLocation = "http://" & strNameServer & "/" & StrHTTPApp & "/XML/" & Request.QueryString("strFileLocation") & ".pdf"
                Response.Write("<script language = javascript>" & vbCrLf _
                & "var x = screen.width;" & vbCrLf _
                & "var y = screen.heigth;" & vbCrLf _
                & "window.open('" & strFileLocation & "','Collection', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes') " & vbCrLf _
                & "</script>")
            End If
            Me.FormID = "RALExtend"
            If CheckForm(Me.Loginid, Me.FormID, "Maxiloan") Then
                InitialDefaultPanel()
                FillCbo()
            End If
        End If
    End Sub
    Private Sub InitialDefaultPanel()
        pnlDtGrid.Visible = False
        pnlsearch.Visible = True
        pnlExtend.Visible = False        
    End Sub
    Private Sub FillCbo()
        Dim oDataTable As New DataTable
        With oCustomClass
            .strConnection = GetConnectionSTring
            .strKey = "CG"
            .CGID = Me.GroubDbID
            .CollectorType = ""
        End With
        oCustomClass = oController.RALListCG(oCustomClass)
        oDataTable = oCustomClass.ListRAL
        With cboCG
            .DataSource = oDataTable
            .DataTextField = "CGName"
            .DataValueField = "CGID"
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = ""
            If oDataTable.Rows.Count = 1 Then
                .Items.FindByValue(Me.GroubDbID).Selected = True
            End If
        End With
    End Sub
#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then            
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click        
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub
#End Region

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView

        With oCustomClass
            .strConnection = GetConnectionString
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oCustomClass = oController.RALExtendList(oCustomClass)

        DtUserList = oCustomClass.ListRAL
        DvUserList = DtUserList.DefaultView
        recordCount = oCustomClass.TotalRecord
        DvUserList.Sort = Me.SortBy
        dtgRALExtend.DataSource = DvUserList
        With oCustomClass
            .strConnection = GetConnectionString
        End With
        oCustomClass = oController.RALDataExtend(oCustomClass)
        Me.oData = oCustomClass.ListRAL
        Try
            dtgRALExtend.DataBind()
        Catch
            dtgRALExtend.CurrentPageIndex = 0
            dtgRALExtend.DataBind()
        End Try



        PagingFooter()
        pnlDtGrid.Visible = True
        pnlsearch.Visible = True        
    End Sub
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Buttonsearch.Click
        Me.SearchBy = "CollectionAgreement.CGID='" & cboCG.SelectedItem.Value.Trim & "'"
        If cboSearchBy.SelectedItem.Value.Trim <> "Select One" Then
            Me.SearchBy = Me.SearchBy & " and " & cboSearchBy.SelectedItem.Value.Trim & " like '%" + txtSearchBy.Text.Trim + "%'"
        End If
        If txtDays.Text.Trim <> "" Then
            Me.SearchBy = Me.SearchBy & " and (RAL.EndDate - '" & Me.BusinessDate & "' <= " & txtDays.Text.Trim & ")"
        End If
        Me.SortBy = ""
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
    Private Sub imbReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        InitialDefaultPanel()
        cboSearchBy.ClearSelection()
        cboCG.ClearSelection()
        txtSearchBy.Text = ""
        txtDays.Text = ""
    End Sub
    Private Sub dtgRALExtend_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgRALExtend.ItemCommand
        If e.CommandName = "Extend" Then
            If checkfeature(Me.Loginid, Me.FormID, "Extnd", "Maxiloan") Then
                Dim lblApplicationId As Label
                Dim lblRALNodtg As Label
                Dim oDatatable As New DataTable
                lblApplicationId = CType(dtgRALExtend.Items(e.Item.ItemIndex).Cells(0).FindControl("lblApplicationId"), Label)
                lblRALNodtg = CType(dtgRALExtend.Items(e.Item.ItemIndex).Cells(4).FindControl("lblRALNodtg"), Label)
                With oCustomClass
                    .strConnection = GetConnectionString
                    .ApplicationID = lblApplicationId.Text.Trim
                    .RALNo = lblRALNodtg.Text.Trim
                    .BusinessDate = Me.BusinessDate
                End With
                oCustomClass = oController.RALExtendView(oCustomClass)
                Me.ApplicationID = lblApplicationId.Text.Trim
                oDatatable = oCustomClass.ListRAL
                If oDatatable.Rows.Count > 0 Then
                    If Not IsDBNull(oDatatable.Rows(0)("AgreementNo")) Then
                        hypAgreementNoSlct.Text = CType(oDatatable.Rows(0)("AgreementNo"), String)
                    End If
                    If Not IsDBNull(oDatatable.Rows(0)("CustomerName")) Then
                        hypCustomerNameSlct.Text = CType(oDatatable.Rows(0)("CustomerName"), String)
                    End If
                    If Not IsDBNull(oDatatable.Rows(0)("MailingAddress")) Then
                        lblAddress.Text = CType(oDatatable.Rows(0)("MailingAddress"), String)
                    End If
                    If Not IsDBNull(oDatatable.Rows(0)("Description")) Then
                        lblAsset.Text = CType(oDatatable.Rows(0)("Description"), String)
                    End If
                    If Not IsDBNull(oDatatable.Rows(0)("InstallmentNo")) Then
                        lblInstallmentNo.Text = CType(oDatatable.Rows(0)("InstallmentNo"), String)
                    End If
                    If Not IsDBNull(oDatatable.Rows(0)("DueDate")) Then
                        lblDuedate.Text = Format(oDatatable.Rows(0)("DueDate"), "dd/MM/yyyy")
                    End If
                    If Not IsDBNull(oDatatable.Rows(0)("EndPastDueAmt")) Then
                        lblODAmount.Text = FormatNumber(oDatatable.Rows(0)("EndPastDueAmt"), 2)
                    End If
                    If Not IsDBNull(oDatatable.Rows(0)("OSBillingCharges")) Then
                        lblOSBillingCharges.Text = FormatNumber(oDatatable.Rows(0)("OSBillingCharges"), 2)
                    End If
                    If Not IsDBNull(oDatatable.Rows(0)("OS_Gross")) Then
                        lblOSBallance.Text = FormatNumber(oDatatable.Rows(0)("OS_Gross"), 2)
                    End If
                    If Not IsDBNull(oDatatable.Rows(0)("RALNo")) Then
                        lblRALNo.Text = CType(oDatatable.Rows(0)("RALNo"), String)
                    End If
                    If Not IsDBNull(oDatatable.Rows(0)("InstallmentAmount")) Then
                        lblInstallmentAmount.Text = FormatNumber(oDatatable.Rows(0)("InstallmentAmount"), 2)
                    End If
                    If Not IsDBNull(oDatatable.Rows(0)("EndPastDuedays")) Then
                        lblOverDueDays.Text = CType(oDatatable.Rows(0)("EndPastDuedays"), String)
                    End If
                    If Not IsDBNull(oDatatable.Rows(0)("LateCharges")) Then
                        lblLateCharges.Text = FormatNumber(oDatatable.Rows(0)("LateCharges"), 2)
                    End If
                    If Not IsDBNull(oDatatable.Rows(0)("OnHand")) Then
                        lblOnHand.Text = CType(oDatatable.Rows(0)("OnHand"), String)
                    End If
                    lblRALNo.Text = lblRALNodtg.Text.Trim
                    lblRALPeriod.Text = CType(dtgRALExtend.Items(e.Item.ItemIndex).Cells(5).Text.Trim, String)
                    If Not IsDBNull(oDatatable.Rows(0)("Executorid")) Then
                        lblExecutor.Text = CType(oDatatable.Rows(0)("Executorid"), String)
                    End If
                    If Not IsDBNull(oDatatable.Rows(0)("MaxEnddate")) Then
                        lblMaxEndDate.Text = Format(oDatatable.Rows(0)("MaxEndDate"), "dd/MM/yyyy")
                    End If
                    txtRALEnddate.Text = ""                    
                    txtNotes.Text = ""
                End If
                pnlExtend.Visible = True
                pnlDtGrid.Visible = False
                pnlsearch.Visible = False
            End If
        ElseIf e.CommandName = "Print" Then
            If checkfeature(Me.Loginid, Me.FormID, "PrRAL", "Maxiloan") Then
                Dim lblApplicationId As Label
                Dim hasil As Integer
                Dim lblRALNodtg, hypExecutorIDdtg As Label
                lblApplicationId = CType(dtgRALExtend.Items(e.Item.ItemIndex).FindControl("lblApplicationId"), Label)
                hypExecutorIDdtg = CType(dtgRALExtend.Items(e.Item.ItemIndex).FindControl("hypExecutorIDdtg"), Label)
                lblRALNodtg = CType(dtgRALExtend.Items(e.Item.ItemIndex).FindControl("lblRALNodtg"), Label)
                If checkfeature(Me.Loginid, Me.FormID, "PrRAL", "Maxiloan") Then
                    With oCustomClass
                        .strConnection = GetConnectionString
                        .ApplicationID = lblApplicationId.Text.Trim
                        .RALNo = lblRALNodtg.Text.Trim
                        .BusinessDate = Me.BusinessDate
                        .LoginId = Me.Loginid
                    End With
                    oCustomClass = oController.RALSaveDataPrint(oCustomClass)
                    hasil = oCustomClass.hasil
                    If hasil = 0 Then                        
                        ShowMessage(lblMessage, "Gagal", True)
                    Else
                        Dim cookie As HttpCookie = Request.Cookies("RptRALPrinting")
                        'If Not cookie Is Nothing Then
                        '    cookie.Values("ExecutorID") = hypExecutorIDdtg.Text.Trim
                        '    cookie.Values("RALNo") = lblRALNodtg.Text.Trim
                        '    cookie.Values("ApplicationID") = lblApplicationId.Text.Trim
                        '    cookie.Values("CGID") = cboCG.SelectedValue.Trim
                        '    Response.AppendCookie(cookie)
                        'Else
                        Dim cookieNew As New HttpCookie("RptRALPrinting")
                        cookieNew.Values.Add("ExecutorID", hypExecutorIDdtg.Text.Trim)
                        cookieNew.Values.Add("RALNo", lblRALNodtg.Text.Trim)
                        cookieNew.Values.Add("ApplicationID", lblApplicationId.Text.Trim)
                        cookieNew.Values.Add("CGID", cboCG.SelectedValue.Trim)
                        cookieNew.Values.Add("ExecutorTypeID", CType(dtgRALExtend.Items(e.Item.ItemIndex).Cells(10).Text.Trim, String))
                        Response.AppendCookie(cookieNew)
                        'End If
                        Response.Redirect("RALPrintingViewer.aspx?cmd=RAL&back=Extend&File=RALExtend.aspx")
                    End If
                End If
            End If
        ElseIf e.CommandName = "PrintCheckList" Then
            Dim lblApplicationId As Label            
            Dim hypExecutorIDdtg As Label
            lblApplicationId = CType(dtgRALExtend.Items(e.Item.ItemIndex).FindControl("lblApplicationId"), Label)
            hypExecutorIDdtg = CType(dtgRALExtend.Items(e.Item.ItemIndex).FindControl("hypExecutorIDdtg"), Label)
            If checkfeature(Me.Loginid, Me.FormID, "PrLst", "Maxiloan") Then
                Dim cookie As HttpCookie = Request.Cookies("RptRALPrinting")
                'If Not cookie Is Nothing Then
                '    cookie.Values("ExecutorID") = hypExecutorIDdtg.Text.Trim
                '    cookie.Values("ApplicationID") = lblApplicationId.Text.Trim
                '    Response.AppendCookie(cookie)
                'Else
                Dim cookieNew As New HttpCookie("RptRALPrinting")
                cookieNew.Values.Add("ExecutorID", hypExecutorIDdtg.Text.Trim)
                cookieNew.Values.Add("ApplicationID", lblApplicationId.Text.Trim)
                cookieNew.Values.Add("CGID", cboCG.SelectedValue.Trim)
                Response.AppendCookie(cookieNew)
                'End If
                Response.Redirect("RALPrintingViewer.aspx?cmd=CheckList&back=Extend&File=RALExtend.aspx")
            End If

        End If
    End Sub

    Private Sub imbSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
        'If Me.ConvertDate2(txtRALEndDate.Text) > Me.ConvertDate2(lblMaxEndDate.Text.Trim) Then
        If ConvertDate2(txtRALEndDate.Text) <= ConvertDate2(lblMaxEndDate.Text.Trim) Then
            'lblMessage.Text = "New RAL End Date cannot be greater than " & lblMaxEndDate.Text.Trim
            'ShowMessage(lblMessage, "New RAL End Date cannot be greater than " & lblMaxEndDate.Text.Trim, True)
            ShowMessage(lblMessage, "Tgl Berakhirnya SKE Baru harus >=  " & lblMaxEndDate.Text.Trim, True)
            Exit Sub
        End If
        If ConvertDate2(txtRALEndDate.Text) < Me.BusinessDate Then
            'ShowMessage(lblMessage, "Tgl Berakhirnya SKT Baru harus >=  " & Format(Me.BusinessDate, "dd/MM/yyyy"), True)
            ShowMessage(lblMessage, "Tgl Berakhirnya SKE Baru harus >=  " & Format(Me.BusinessDate, "dd/MM/yyyy"), True)
            Exit Sub
        End If
        With oCustomClass
            .strConnection = GetConnectionString()
            .ApplicationID = Me.ApplicationID
            .RALNo = lblRALNo.Text.Trim
            .BusinessDate = Me.BusinessDate
            .EndDate = ConvertDate2(txtRALEndDate.Text)
            .Notes = txtNotes.Text.Trim
            .OnHand = CInt(lblOnHand.Text.Trim)
        End With

        Try
            oCustomClass = oController.RALExtendSave(oCustomClass)

            If oCustomClass.hasil = 0 Then
                ShowMessage(lblMessage, "Simpan Data Gagal!", True)
            Else
                pnlExtend.Visible = False
                DoBind(Me.SearchBy, Me.SortBy)
            End If

        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

    Private Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        pnlExtend.Visible = False
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Public Function isVisible(ByVal strNo As String) As Boolean
        Dim DataRow As DataRow()
        DataRow = Me.oData.Select(" NewRALNo ='" & strNo.Trim & "' ")
        If DataRow.Length > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub dtgRALExtend_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgRALExtend.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim hyTemp As HyperLink
            Dim lblApplicationId As Label
            lblApplicationId = CType(e.Item.FindControl("lblApplicationId"), Label)
            '*** Agreement No link
            hyTemp = CType(e.Item.FindControl("hypAgreementNodtg"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenAgreementNo('" & "Collection" & "', '" & Server.UrlEncode(lblApplicationId.Text.Trim) & "')"
        End If
    End Sub
End Class