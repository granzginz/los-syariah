﻿#Region "Imports"
Imports System.Data
Imports System.Data.SqlClient
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Parameter.ViewCollection
#End Region

Public Class ViewAppraisal
    Inherits Maxiloan.Webform.WebBased

#Region " Private Const "
    Dim m_ViewAppraisal As New InventoryAppraisalController
#End Region

#Region "Property"
    Private Property ApprovalNo() As String
        Get
            Return CStr(viewstate("ApprovalNo"))
        End Get
        Set(ByVal Value As String)
            viewstate("ApprovalNo") = Value
        End Set
    End Property

    Private Property ApplicationID() As String
        Get
            Return CStr(viewstate("ApplicationID"))
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property
    Private Property AgreementNo() As String
        Get
            Return CStr(viewstate("AgreementNo"))
        End Get
        Set(ByVal Value As String)
            viewstate("AgreementNo") = Value
        End Set
    End Property



#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then            
            Me.ApplicationID = Request.QueryString("ApplicationID")
            Me.ApprovalNo = Request.QueryString("ApprovalNo")
            'Me.ApprovalNo = Me.ApprovalNo + "021200416"


            Dim oViewAppraisal As New Parameter.InventoryAppraisal
            With oViewAppraisal
                .strConnection = GetConnectionString()
                .BusinessDate = Me.BusinessDate
                .ApplicationID = Me.ApplicationID
                .ApprovalNo = Me.ApprovalNo


            End With

            Dim dt As New DataTable
            Dim dv As New DataView



            oViewAppraisal = m_ViewAppraisal.ViewAppraisal(oViewAppraisal)

            dt = oViewAppraisal.Listappraisal

            If dt.Rows.Count > 0 Then
                hypAgreementNo.Text = CStr(dt.Rows(0)("AgreementNo"))
                hypCustomerName.Text = CStr(dt.Rows(0)("CustomerName"))
                lblAsset.Text = CStr(dt.Rows(0)("Description"))
                lblestimationamount.Text = FormatNumber(CStr(dt.Rows(0)("estimationamount")), 0)
                lblestimationdate.Text = CStr(dt.Rows(0)("estimationdate"))
                lblinvamount.Text = FormatNumber(dt.Rows(0)("inventoryamount"), 0)
                lblARInsurance.Text = FormatNumber(dt.Rows(0)("AR_Insurance"), 0)
                lblLateCharges.Text = FormatNumber(dt.Rows(0)("LateCharges"), 0)
                lblbillingcharges.Text = FormatNumber(dt.Rows(0)("BillingCharges"), 0)
                lblrepossessfee.Text = FormatNumber(dt.Rows(0)("RepossessFee"), 0)
            End If
            oViewAppraisal = m_ViewAppraisal.GetAccruedInterest(oViewAppraisal)
            lblaccruedinterest.Text = FormatNumber(oViewAppraisal.accruedInterest, 0)
            lbltotalAR.Text = FormatNumber(oViewAppraisal.accruedInterest + _
                                CDbl(dt.Rows(0)("RepossessFee")) + _
                                CDbl(dt.Rows(0)("BillingCharges")) + _
                                CDbl(dt.Rows(0)("LateCharges")) + _
                                CDbl(dt.Rows(0)("AR_Insurance")), 0)



            With oViewAppraisal
                .strConnection = GetConnectionString()
                .BusinessDate = Me.BusinessDate
                .ApplicationID = Me.ApplicationID
                .approvalno = Me.ApprovalNo


            End With
            oViewAppraisal = m_ViewAppraisal.ViewAppraisalbidder(oViewAppraisal)
            dt = oViewAppraisal.ListRAL
            dv = dt.DefaultView
            dtgbidder.DataSource = dv
            dtgbidder.DataBind()

        End If
    End Sub


End Class