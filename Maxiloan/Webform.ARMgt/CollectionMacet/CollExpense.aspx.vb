﻿#Region "Import"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCacheHelper
#End Region

Public Class CollExpense
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oCGID As UcBranchCollection
    Protected WithEvents oApprovalRequest As ucApprovalRequest
    'Protected WithEvents txtERA As ucNumberFormat
    Protected WithEvents txtTotalExecutorFee As ucNumberFormat



#Region "properties"
    Public Property CustomerID() As String
        Get
            Return CType(viewstate("CustomerID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerID") = Value
        End Set
    End Property
    Public Property BankType() As String
        Get
            Return CType(viewstate("BankType"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("BankType") = Value
        End Set
    End Property

    Public Property BankPurpose() As String
        Get
            Return CType(viewstate("BankPurpose"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("BankPurpose") = Value
        End Set
    End Property
    Public Property cgid() As String
        Get
            Return CType(viewstate("cgid"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("cgid") = Value
        End Set
    End Property
    Public Property AgreementNo() As String
        Get
            Return CType(viewstate("AgreementNo"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AgreementNo") = Value
        End Set
    End Property
    Public Property RALNo() As String
        Get
            Return CType(viewstate("RALNo"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("RALNo") = Value
        End Set
    End Property
    Public Property ApplicationID() As String
        Get
            Return CType(viewstate("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property    
#End Region

#Region " Private Const "
    Dim m_CollExpense As New CollExpenseController
    Private oController As New DataUserControlController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region
#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If

    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        Bindgrid(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        If txtPage.Text = "" Then
            txtPage.Text = "0"
        Else
            If IsNumeric(txtPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                    currentPage = CType(txtPage.Text, Int32)
                    Bindgrid(Me.SearchBy, Me.SortBy)
                End If
            End If
        End If


    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        Bindgrid(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        Page.Header.DataBind()

        If SessionInvalid() Then
            Exit Sub
        End If

        If Not Page.IsPostBack Then            
            Me.FormID = "CollExpense"
            Me.cgid = Me.GroubDbID

            oApprovalRequest.ReasonTypeID = "CLEX"
            'oApprovalRequest.ApprovalScheme = "FPAY"
            oApprovalRequest.ApprovalScheme = "CLEX"            

            If CheckForm(Me.Loginid, Me.FormID, "Maxiloan") Then
                InitialDefaultPanel()
            Else
                Dim strHTTPServer As String
                Dim strHTTPApp As String
                Dim strNameServer As String
                strHTTPServer = Request.ServerVariables("PATH_INFO")
                strNameServer = Request.ServerVariables("SERVER_NAME")
                strHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                Response.Redirect("http://" & strNameServer & "/" & strHTTPApp & "/NotAuthorized.html")
            End If
        End If
    End Sub


    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Buttonsearch.Click
        Me.SearchBy = ""
        Me.SortBy = ""        
        If oCGID.BranchID.Trim <> "" Then
            Me.SearchBy = "CGID='" & oCGID.BranchID & "'"
            Me.cgid = oCGID.BranchID
        End If
        If cboSearchBy.SelectedIndex <> 0 And txtSearchBy.Text <> "" Then
            If Me.SearchBy = "" Then
                Me.SearchBy = cboSearchBy.SelectedItem.Value.Trim & " like '%" + txtSearchBy.Text.Trim + "%'"
            Else
                Me.SearchBy = Me.SearchBy & " and " & cboSearchBy.SelectedItem.Value.Trim & " like '%" + txtSearchBy.Text.Trim + "%'"
            End If
        End If

        Bindgrid(Me.SearchBy, Me.SortBy)
    End Sub
    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        InitialDefaultPanel()
        cboSearchBy.ClearSelection()
        txtSearchBy.Text = ""
    End Sub
    Private Sub InitialDefaultPanel()
        pnlDtGrid.Visible = False
        pnlsearch.Visible = True
        pnlSelect.Visible = False
        pnlSelect2.Visible = False
        PnlSelect3.Visible = False

        'With txtERA
        '    .RequiredFieldValidatorEnable = True
        '    .RangeValidatorEnable = False
        '    .AutoPostBack = True            
        'End With        
    End Sub
    Sub Bindgrid(ByVal cmdWhere As String, ByVal cmSort As String)
        Dim dtvEntity As New DataView
        Dim dtsEntity As New DataTable
        Dim oCollExpense As New Parameter.CollExpense

        With oCollExpense
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oCollExpense = m_CollExpense.CollExpenseList(oCollExpense)

        With oCollExpense
            lblrecord.Text = CType(.TotalRecord, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecord
        End With

        dtsEntity = oCollExpense.listData
        dtsEntity.TableName = "Table"

        dtvEntity = dtsEntity.DefaultView

        dtgCollExpenseList.DataSource = dtvEntity

        Try
            dtgCollExpenseList.DataBind()
        Catch ex As Exception

        End Try

        PagingFooter()
        pnlDtGrid.Visible = True
    End Sub

    Private Sub dtgCollExpenseList_itemcommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgCollExpenseList.ItemCommand
        Me.AgreementNo = e.Item.Cells(0).Text.Trim
        Me.RALNo = e.Item.Cells(4).Text.Trim

        If e.CommandName = "Request" Then
            If checkFeature(Me.Loginid, Me.FormID, "Req", Me.AppId) Then
                If sessioninvalid() Then
                    Dim strHTTPServer As String
                    Dim strHTTPApp As String
                    Dim strNameServer As String
                    strHTTPServer = Request.ServerVariables("PATH_INFO")
                    strNameServer = Request.ServerVariables("SERVER_NAME")
                    strHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                    Response.Redirect("http://" & strNameServer & "/" & strHTTPApp & "/NotAuthorized.html")
                Else
                    oApprovalRequest.ReasonID = ""
                    oApprovalRequest.Notes = ""
                    oApprovalRequest.ToBeApprove = ""
                    'txtERA.Text = ""
                    Dim lblBranchID, lblCustomerID As Label
                    If e.Item.ItemIndex >= 0 Then
                        lblBranchID = CType(e.Item.FindControl("lblBranchID"), Label)
                        lblCustomerID = CType(e.Item.FindControl("lblCustomerID"), Label)
                        Me.BranchID = lblBranchID.Text
                        Me.CustomerID = lblCustomerID.Text
                    End If
                    viewRepoDetail(Me.AgreementNo, Me.RALNo)
                End If
            End If

        End If
    End Sub
    Private Sub viewRepoDetail(ByVal agreementno As String, ByVal ralNo As String)

        pnlDtGrid.Visible = False
        pnlsearch.Visible = False
        pnlSelect.Visible = True
        pnlSelect2.Visible = True
        PnlSelect3.Visible = True

        Dim oAssetRepo As New Parameter.AssetRepo
        Dim oCollExpense As New Parameter.CollExpense
        Dim oCollExpenseReqList As New Parameter.CollExpense
        Dim m_AssetRepo As New AssetRepoController
        Dim dtsEntity As New DataTable
        Dim dtvEntity As New DataView
        Dim aprStatus As String
        Dim dt As New DataTable

        Dim dtBankAccount As New DataTable
        Dim dtBankAccountCache As New DataTable
        'Dim strCacheName As String
        Me.BankType = "B"
        'strCacheName = CACHE_BANKACCOUNT & Me.BranchID & Me.BankType & Me.BankPurpose
        'dtBankAccountCache = oController.GetBankAccount(GetConnectionString, Me.BranchID, _
        '                    Me.BankType, Me.BankPurpose)
        'Me.Cache.Insert(strCacheName, dtBankAccountCache, Nothing, DateTime.Now.AddHours(DEFAULT_TIME_DURATION_CACHE_HOUR), TimeSpan.Zero)
        'dtBankAccount = CType(Me.Cache.Item(strCacheName), DataTable)
        'cboBankAccount.DataTextField = "Name"
        'cboBankAccount.DataValueField = "ID"
        'cboBankAccount.DataSource = dtBankAccount
        'cboBankAccount.DataBind()
        'cboBankAccount.Items.Insert(0, "Select One")
        'cboBankAccount.Items(0).Value = "0"

        With oAssetRepo
            .strConnection = getConnectionString()
            .AgreementNo = agreementno
            .BusinessDate = Me.BusinessDate
            .RALNo = ralNo
        End With

        oAssetRepo = m_AssetRepo.AssetRepoDetail(oAssetRepo)

        dt = oAssetRepo.listData

        Me.ApplicationID = CStr(dt.Rows(0).Item("ApplicationID")).Trim
        Me.BranchID = CStr(dt.Rows(0).Item("BranchID")).Trim
        hypAgreementNoSlct.Text = CStr(dt.Rows(0).Item("AgreementNo"))
        hypAgreementNoSlct.NavigateUrl = "javascript:OpenAgreementNo('" & "Collection" & "', '" & Server.UrlEncode(Me.ApplicationID.Trim) & "')"

        hypCustomerNameSlct.Text = CStr(dt.Rows(0).Item("CustomerName"))
        hypCustomerNameSlct.NavigateUrl = "javascript:OpenCustomer('" & "Collection" & "', '" & Server.UrlEncode(Me.CustomerID.Trim) & "')"
        lblAddress.Text = CStr(dt.Rows(0).Item("MailingAddress"))
        lblDuedate.Text = CStr(dt.Rows(0).Item("Duedate"))
        lblOverDueDays.Text = CStr(dt.Rows(0).Item("EndPastDuedays"))
        lblInstallmentNo.Text = CStr(dt.Rows(0).Item("InstallmentNo"))
        lblInstallmentAmount.Text = FormatNumber(CStr(dt.Rows(0).Item("InstallmentAmount")), 0)
        lblODAmount.Text = FormatNumber(CStr(dt.Rows(0).Item("EndPastDueAmt")), 0)
        lblLateCharges.Text = FormatNumber(CStr(dt.Rows(0).Item("LC")), 0)
        lblOSBillingCharges.Text = FormatNumber(CStr(dt.Rows(0).Item("OSBillingCharges")), 0)
        lblOSBallance.Text = FormatNumber(CStr(dt.Rows(0).Item("OS_Gross")), 0)
        lblCollExpense.Text = FormatNumber(CStr(dt.Rows(0).Item("CollectionExpense")), 0)
        lblRALNo.Text = CStr(dt.Rows(0).Item("RALNo"))
        lblRALPeriod1.Text = CStr(dt.Rows(0).Item("RALPeriod"))

        lblExecutor.Text = CStr(dt.Rows(0).Item("CollectorID"))
        lblAsset.Text = CStr(dt.Rows(0).Item("Description"))

        'txtStandarExecutorFee.Text = FormatNumber(CStr(dt.Rows(0).Item("std")), 2)
        txtTotalExecutorFee.Text = FormatNumber(CStr(dt.Rows(0).Item("std")), 0)

        lblNamaEksekutor.Text = CStr(dt.Rows(0).Item("EksekutorName"))
        lblNamaBank.Text = CStr(dt.Rows(0).Item("BankName"))
        lblNamaCabangBank.Text = CStr(dt.Rows(0).Item("BankBranch"))
        lblNamaRekening.Text = CStr(dt.Rows(0).Item("AccountName"))
        lblNoRekening.Text = CStr(dt.Rows(0).Item("AccountNo"))
        hdfBankAccountID.Value = CStr(dt.Rows(0).Item("BankBranchID"))
        '==============================================================

        With oCollExpense
            .strConnection = GetConnectionString()
            .WhereCond = "CollExpense.ApplicationID = '" & Me.ApplicationID & "'"
        End With

        oCollExpenseReqList = m_CollExpense.CollExpenseReqList(oCollExpense)
        dtsEntity = oCollExpenseReqList.listData
        If dtsEntity.Rows.Count > 0 Then
            pnlSelect2.Visible = True
            aprStatus = CStr(dtsEntity.Rows(0).Item("ApprovalStatus"))
            dtsEntity.TableName = "Table"

            dtvEntity = dtsEntity.DefaultView

            dtgSelect.DataSource = dtvEntity

            Try
                dtgSelect.DataBind()
            Catch ex As Exception

            End Try
        Else
            pnlSelect2.Visible = False
        End If

        If lblExecutor.Text.Trim = "" Then
            ButtonSave.Visible = False
            ShowMessage(lblMessage, "Harap pilih Eksekutor", True)            
        Else
            ButtonSave.Visible = True
            lblMessage.Visible = False
        End If
        If aprStatus = "REQ" Then
            ButtonSave.Visible = False
            ShowMessage(lblMessage, "Permintaan Sebelumnya belum disetujui", True)            
        End If

    End Sub

    Private Sub ButtonSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
        Dim oAssetRepo As New Parameter.AssetRepo
        Dim oCollExpense As New Parameter.CollExpense
        Dim m_AssetRepo As New AssetRepoController
        Dim m_CollExpense As New CollExpenseController        

        With oCollExpense
            .strConnection = getConnectionString()
            .AgreementNo = Me.AgreementNo
            .ApplicationID = Me.ApplicationID
            .BusinessDate = Me.BusinessDate
            .RALNo = Me.RALNo
            .Notes = oApprovalRequest.Notes
            .CollectorID = lblExecutor.Text.Trim
            '.ExpenseAmount = CDbl(txtERA.Text) + CDbl(txtStandarExecutorFee.Text)
            '.ExpenseAmount = CDbl(txtStandarExecutorFee.Text) + CDbl(lblCollExpense.Text)
            .ExpenseAmount = CDbl(txtTotalExecutorFee.Text)
            .ApproveBy = oApprovalRequest.ToBeApprove
            .BranchId = Me.BranchID
            .LoginId = Me.Loginid
            .BankAccountID = hdfBankAccountID.Value

        End With

        Try
            m_CollExpense.CollExpenseSave(oCollExpense)            
            ShowMessage(lblMessage, "Tambah Data Berhasil ", False)
            Bindgrid(Me.SearchBy, Me.SortBy)
            pnlsearch.Visible = True
            pnlSelect.Visible = False
            pnlSelect2.Visible = False
            PnlSelect3.Visible = False
        Catch ex As Exception            
            ShowMessage(lblMessage, ex.Message, True)
        End Try

    End Sub

    Private Sub imbCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        Response.Redirect("CollExpense.aspx")
    End Sub

    Private Sub dtgCollExpenseList_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtgCollExpenseList.SelectedIndexChanged

    End Sub

    'Public Sub txtERA_TextChanged()                
    '    txtTotalExecutorFee.Text = FormatNumber(CInt(txtStandarExecutorFee.Text) + CInt(txtERA.Text), 0)
    '    'pnlSelect.Visible = True
    '    'pnlSelect2.Visible = True
    '    'PnlSelect3.Visible = True
    '    'pnlDtGrid.Visible = False
    '    'pnlsearch.Visible = False
    'End Sub
End Class
