﻿#Region "Imports"
Imports System.Data
Imports System.Data.SqlClient
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Parameter.ViewCollection
#End Region

Public Class ViewAppraisalApproval
    Inherits Maxiloan.Webform.WebBased

#Region " Private Const "
    Dim m_ViewAppraisal As New InventoryAppraisalController
#End Region

#Region "Property"
    Private Property ApprovalNo() As String
        Get
            Return CStr(ViewState("ApprovalNo"))
        End Get
        Set(ByVal Value As String)
            ViewState("ApprovalNo") = Value
        End Set
    End Property

    Private Property ApplicationID() As String
        Get
            Return CStr(ViewState("ApplicationID"))
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
    Private Property AgreementNo() As String
        Get
            Return CStr(ViewState("AgreementNo"))
        End Get
        Set(ByVal Value As String)
            ViewState("AgreementNo") = Value
        End Set
    End Property



#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.ApplicationID = Request.QueryString("ApplicationID")
            Me.ApprovalNo = Request.QueryString("ApprovalNo")

            Dim oViewAppraisal As New Parameter.InventoryAppraisal
            With oViewAppraisal
                .strConnection = GetConnectionString()
                .BusinessDate = Me.BusinessDate
                .ApplicationID = Me.ApplicationID
                .approvalno = Me.ApprovalNo
            End With

            Dim dt As New DataTable
            Dim dv As New DataView

            oViewAppraisal = m_ViewAppraisal.ViewAppraisal(oViewAppraisal)

            dt = oViewAppraisal.Listappraisal

            If dt.Rows.Count > 0 Then
                hypAgreementNoSlct.Text = CStr(dt.Rows(0)("AgreementNo"))
                hypCustomerNameSlct.Text = CStr(dt.Rows(0)("CustomerName"))
                lblAsset.Text = CStr(dt.Rows(0)("Description"))
                lblRepossessDate.Text = Format(dt.Rows(0)("RepossesDate"), "dd/MM/yyyy")
                lblrepossessby.Text = CStr(dt.Rows(0)("CollectorName"))
                lblInventoryDate.Text = Format(dt.Rows(0)("InventoryDate"), "dd/MM/yyyy")
                LblInventoryAmount.Text = FormatNumber(dt.Rows(0)("OutstandingPrincipal"), 0)
                lblEstimationPrice.Text = FormatNumber(CStr(dt.Rows(0)("estimationamount")), 0)
                lblEstimationDate.Text = Format(dt.Rows(0)("estimationdate"), "dd/MM/yyyy")
                lblGrade.Text = CStr(dt.Rows(0)("Grade")) + " - " + CStr(dt.Rows(0)("GradeValue"))
                lblMarginDealer.Text = FormatNumber(CStr(dt.Rows(0)("MarginDealer")), 0)
                lblEstimatedBy.Text = CStr(dt.Rows(0)("EstimationBy"))
                lblNilaiPenyesuaian.Text = FormatNumber(dt.Rows(0)("NilaiPenyesuaian"), 0)
                lblApprove.Text = CStr(dt.Rows(0)("ApprovalBy"))
                lblRepossessFee.Text = FormatNumber(dt.Rows(0)("RepossessFee"), 0)

                lblKoordinasiFee.Text = FormatNumber(dt.Rows(0)("BiayaKoordinasi"), 0)
                lblMobilisasiFee.Text = FormatNumber(dt.Rows(0)("BiayaMobilisasi"), 0)
                lblTotalMinSalesPrice.Text = FormatNumber(dt.Rows(0)("MinimumSalesPrice"), 0)
                lblNotes.Text = CStr(dt.Rows(0)("Notes"))


                lblPenawarTerbaik.Text = dt.Rows(0)("BestBidderName")
                lblCaraPembayaran.Text = dt.Rows(0)("CaraPembayaran")
                lblHargaPenawarTerbaik.Text = FormatNumber(dt.Rows(0)("BestBidderValue"), 0)
                lblRekeningBank.Text = dt.Rows(0)("BankAccount")
                lblPerlunasanAwal.Text = FormatNumber(dt.Rows(0)("PerlunasanAwal"), 0)
                lblPerkiraanRL.Text = FormatNumber(CDbl(dt.Rows(0)("OutstandingPrincipal")) - CDbl(dt.Rows(0)("PerlunasanAwal")), 0)
            End If
            'oViewAppraisal = m_ViewAppraisal.GetAccruedInterest(oViewAppraisal)
            'lblaccruedinterest.Text = FormatNumber(oViewAppraisal.accruedInterest, 0)
            'lbltotalAR.Text = FormatNumber(oViewAppraisal.accruedInterest + _
            '                    CDbl(dt.Rows(0)("RepossessFee")) + _
            '                    CDbl(dt.Rows(0)("BillingCharges")) + _
            '                    CDbl(dt.Rows(0)("LateCharges")) + _
            '                    CDbl(dt.Rows(0)("AR_Insurance")), 0)

            With oViewAppraisal
                .strConnection = GetConnectionString()
                .BusinessDate = Me.BusinessDate
                .ApplicationID = Me.ApplicationID
                .approvalno = Me.ApprovalNo
                .TableName = "spViewAppraisalBidderApproval"
            End With
            oViewAppraisal = m_ViewAppraisal.ViewAppraisalbidder(oViewAppraisal)
            dt = oViewAppraisal.ListRAL
            dv = dt.DefaultView
            dtgbidder.DataSource = dv
            dtgbidder.DataBind()
        End If
    End Sub


End Class