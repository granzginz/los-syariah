﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AssetRepossession.aspx.vb"
    Inherits="Maxiloan.Webform.ARMgt.AssetRepossession" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcBranchCollection" Src="../../Webform.UserController/UcBranchCollection.ascx" %>
<%@ Register TagName="ucNumberFormat" TagPrefix="uc1" Src="../../webform.UserController/ucNumberFormat.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="System.Web.UI.WebControls" Assembly="System.Web" %>
<%@ Register Src="../../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>AssetRepossession</title>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <script src="../../js/jquery-2.1.1.min.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;

        var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%#Request.servervariables("SERVER_NAME")%>/';


        function OpenWinCust(pStrCustID, pStrType) {
            var lStrAddress;
            if (pStrType == 'P')
                lStrAddress = 'ViewSearch.aspx';
            else
                lStrAddress = 'ViewSearchCompany.aspx';

            window.open('http://' + ServerName + '/' + App + '/Application/SmartSearch/' + lStrAddress + '?custid=' + pStrCustID, 'WinLink', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0,scrollbars=1,resizable=1,center=1');
        }

        function OpenWinAgreement(pStrAgreementNo) {
            window.open('http://' + ServerName + '/' + App + '/Application/SmartSearch/ViewAgreementDetail.aspx?AgreementNo=' + pStrAgreementNo, 'WinLink', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0,scrollbars=1,resizable=1,center=1');
        }

        function OpenWinAsset(pStrApplicationID, pStrAgreementNo, asq) {
            window.open('http://' + ServerName + '/' + App + '/Application/SmartSearch/perAsset.aspx?applicationid=' + pStrApplicationID + '&agreementno=' + pStrAgreementNo + '&assetseqno=' + asq, 'WinLink', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=1,resizable=1,center=1');
        }

        function PlanRALChange() {
            var myID = 'UcRALDate_txtDate';
            var objDate = eval('document.forms[0].' + myID);
            var result

            if (document.forms[0].rbRAL_0.checked = true) {
                result = 'Plan';
            }
            if (document.forms[0].rbRAL_1.checked = true) {
                result = 'Undo Plan';
            }

            alert(result);
            if (result == 'Undo Plan') {
                objDate.disabled = true;
                document.forms[0].all.UcRALDate_txtDate_imgCalender.disabled = true;
                document.forms[0].all.UcRALDate_txtDate_imgCalender.disabled = true;
                document.forms[0].UcRALDate_txtDate.value = '';
            }
        }

        function boo(e) {
            if (e == '0') {
                ValidatorEnable(document.getElementById("<%#RequiredFieldValidator6.ClientID%>"), false);
                $('#divPajakStnk').hide();
            }
            else {
                $('#divPajakStnk').show();
                ValidatorEnable(document.getElementById("<%#RequiredFieldValidator6.ClientID%>"), true);
            }
        }
        $(document).ready(function () {
            $("#ddlSTNK").change(function (e) {
                boo(this.value);
            });
            boo($('#ddlSTNK option:selected').val());
        });

    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>    
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>
                        PENARIKAN ASSET
                    </h3>
                </div>
            </div>
            <asp:Panel ID="PnlSearch" runat="server" Width="100%">
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Collection Group
                        </label>
                        &nbsp;<uc1:ucbranchcollection id="oCGID" runat="server"></uc1:ucbranchcollection>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Cari Berdasarkan</label>
                        <asp:DropDownList ID="cboSearchBy" runat="server">
                            <asp:ListItem Value="Select One">Select One</asp:ListItem>
                            <asp:ListItem Value="CollectionAgreement.AgreementNo">No Kontrak</asp:ListItem>
                            <asp:ListItem Value="Customer.Name">Nama Customer</asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtSearchBy" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="Buttonsearch" runat="server" Text="Search" CssClass="small button blue">
                    </asp:Button>&nbsp;
                    <asp:Button ID="ButtonReset" runat="server" Text="Reset" CssClass="small button gray"
                        CausesValidation="False"></asp:Button>
                </div>
            </asp:Panel>
            <asp:Panel ID="PnlList" runat="server" Width="100%">
                <div class="form_box_title">
                    <div class="form_single">
                        <h4>
                            DAFTAR PENARIKAN ASSET
                        </h4>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="Dtg" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False" OnSortCommand="Sorting" CssClass="grid_general" BorderStyle="None" BorderWidth="0">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="PILIH">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkReposses" runat="server" Enabled="True" Text="TARIK" CommandName="REPOSSESS" /> &nbsp;&nbsp;
                                            <asp:LinkButton ID="lnkEditReposses" runat="server" Enabled="True" Text="EDIT" CommandName="EDITREPOSSESS" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn Visible="False" DataField="AgreementNo" />
                                    <asp:BoundColumn Visible="False" DataField="CustomerID" />
                                    <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlAgreement" runat="server" Enabled="True" Text='<%#container.dataitem("AgreementNo")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="CustomerName" HeaderText="NAMA CUSTOMER">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlCustomer" runat="server" Enabled="True" Text='<%# container.dataitem("CustomerName")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="AssetDescription" SortExpression="AssetDescription" HeaderText="NAMA ASSET" />
                                    <asp:BoundColumn DataField="EndPastDueDays" SortExpression="EndPastDueDays" HeaderText="HARI TELAT" />
                                     <asp:BoundColumn DataField="Collectorid" SortExpression="Collectorid" HeaderText="EKSEKUTOR" />
                                     <asp:BoundColumn DataField="CollectorName" SortExpression="CollectorName" HeaderText="NAMA EKSEKUTOR" />
                                    <asp:TemplateColumn Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCollectorId" runat="server" Text='<%# container.dataitem("CollectorId")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn Visible="False" DataField="AgreementNo" />
                                    <asp:BoundColumn Visible="False" DataField="RALNo" />
                                     <asp:BoundColumn Visible="False" DataField="RepossesionStatus" />
                                </Columns>
                            </asp:DataGrid>
                             <uc2:ucGridNav id="GridNavigator" runat="server"/>   
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel ID="PnlListDetail" runat="server">
                <div class="form_box_header">
                    <div class="form_single">
                        <h4>
                            INFORMASI PENARIKAN
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            No Kontrak</label>
                        <asp:HyperLink ID="hypAgreementNo" runat="server"></asp:HyperLink>
                    </div>
                    <div class="form_right">
                        <label>
                            Nama Customer</label>
                        <asp:HyperLink ID="hypCustomerName" runat="server"></asp:HyperLink>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Alamat</label>
                        <asp:Label ID="lblAddress" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Angsuran Ke</label>
                        <asp:Label ID="lblInstallmentNo" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Jumlah Angsuran</label>
                        <asp:Label ID="lblInstallmentAmount" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Tanggal Jatuh Tempo</label>
                        <asp:Label ID="lblDuedate" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Jumlah Hari Telat</label>
                        <asp:Label ID="lblOverDueDays" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Jumlah Tunggakan</label>
                        <asp:Label ID="lblODAmount" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Denda Keterlambatan</label>
                        <asp:Label ID="lblLateCharges" runat="server"></asp:Label>
                    </div>
                </div> 
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Sisa A/R</label>
                        <asp:Label ID="lblOSBallance" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Biaya Tarik(Repossess Fee)</label>
                        <asp:Label ID="LblRepossessFee" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            <%--No SKT--%>
                            No SKE
                        </label>
                        <asp:Label ID="lblRALNo" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            <%--Periode SKT--%>
                            Periode SKE
                        </label>
                        <asp:Label ID="lblRALPeriod" runat="server"></asp:Label>
                    </div>
                </div> 
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Eksekutor</label>
                        <asp:Label ID="lblExecutor" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            <%--SKT Ditangani--%>
                            SKE Ditangani
                        </label>
                        <asp:Label ID="lblOnHand" runat="server"></asp:Label>&nbsp;hari
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Nama Asset</label>
                        <asp:Label ID="lblAsset" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            No Polisi</label>
                        <asp:Label ID="lblLicensePlate" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            No Rangka</label>
                        <asp:Label ID="lblChassisNo" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            No Mesin</label>
                        <asp:Label ID="lblEngineNo" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Tahun Produksi</label>
                        <asp:Label ID="lblYear" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Warna</label>
                        <asp:Label ID="lblColor" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            BAPK</label>
                        <asp:Label ID="lblBAPK" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <h4>
                            DATA PENARIKAN
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label class="label_req"> Tanggal Tarik</label>
                        <asp:TextBox ID="txtRepossesDate" runat="server" />
                        <asp:CalendarExtender ID="txtRepossesDate_CalendarExtender" runat="server" Enabled="True" TargetControlID="txtRepossesDate" Format="dd/MM/yyyy" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*" CssClass="validator_general" ControlToValidate="txtRepossesDate" />
                    </div>
                   <div class="form_right">
                        <label> STNK</label>
                        <asp:DropDownList runat="server" ID="ddlSTNK">
                            <asp:ListItem Value="1" Text="ADA" Selected="True" />
                            <asp:ListItem Value="0" Text="TIDAK ADA" />
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label class="label_req"> Lokasi Tarik</label>
                        <asp:TextBox ID="txtAssetLocation" runat="server" CssClass="long_text" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" CssClass="validator_general" Display="Dynamic" ErrorMessage="Harap isi Lokasi Asset" ControlToValidate="txtAssetLocation" />
                    </div>
                       <div class="form_right">
                           <div id='divPajakStnk' >
                            <label class="label_req"> Masa Berlaku Pajak STNK</label>
                            <asp:TextBox ID="txtSTNKEndDate" runat="server" />
                            <asp:CalendarExtender ID="txtSTNKEndDate_CalendarExtender" runat="server" Enabled="True" TargetControlID="txtSTNKEndDate" Format="dd/MM/yyyy" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="*" CssClass="validator_general" ControlToValidate="txtSTNKEndDate" />
                        </div>
                    </div>
                  
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label class="label_req"> Kondisi Asset</label>
                        <asp:TextBox ID="txtAssetCondition" runat="server" CssClass="long_text" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" CssClass="validator_general" Display="Dynamic" ErrorMessage="Harap isi Kondisi Asset" ControlToValidate="txtAssetCondition" />
                    </div>
                    <div class="form_right">
                        <label> Warna</label>
                        <asp:TextBox ID="txtColor" runat="server" CssClass="long_text" />
                    </div>

                </div>
                <div class="form_box">
                    <div class="form_left" style="display:none;">
                        <label> Nama STNK</label>
                        <asp:TextBox ID="txtSTNKName" runat="server" CssClass="long_text" />
                    </div>
                    <div class="form_left">
                        <label class="label_req">  Biaya Tarik</label> 
                        <uc1:ucnumberformat id="txtReposessExpense" runat="server" /> 
                    </div>
                    <div class="form_right">
                        <label> No Polisi</label>
                        <asp:TextBox ID="txtlisencePlate" runat="server" CssClass="long_text" />
                    </div> 
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>KM Asset</label>                                             
                        <uc1:ucnumberformat id="txtKMAsset" runat="server" /> 
                    </div>

                    <div class="form_right">
                        <label class="label_req"> Lokasi Penyimpanan</label>
                        <asp:DropDownList runat="server" ID="ddlLokasiPenyimpanan" />
                        <asp:DropDownList Style="z-index: 0" ID="oBranch" runat="server" Visible="false" />
                        <asp:RequiredFieldValidator Style="z-index: 0" ID="reqFloatingPeriod" runat="server"   Display="Dynamic" ErrorMessage="Harap pilih Lokasi Asset" ControlToValidate="ddlLokasiPenyimpanan" CssClass="validator_general" InitialValue="0" />
                    </div>
                </div>
 
                  <%--<div class="form_box">
                    <div class="form_left">
                        <label>Grade</label>                                             
                         <asp:DropDownList runat="server" ID="ddlGrade" />
                    </div>

                    <div class="form_right"> 
                    </div>
                </div>--%>

                <div class="form_box_title">
                    <div class="form_single">
                        <h4>
                            ASSET CHECK LIST
                        </h4>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtgCheckList" runat="server" Width="100%" AutoGenerateColumns="False"
                                AllowPaging="false" AllowSorting="True" CssClass="grid_general" BorderStyle="None"
                                BorderWidth="0">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:BoundColumn DataField="id" HeaderText="NO"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="Description" HeaderText="PERTANYAAN"></asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="JAWABAN">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtAnswer" runat="server"  Text ='<%# container.dataitem("Quantity")%>'></asp:TextBox>
                                            <asp:RadioButtonList ID="rbAnswer" runat="server" class="opt_single" RepeatDirection="Horizontal"> 
                                                <asp:ListItem Value="Y" Selected='True' >Yes</asp:ListItem>
                                                <asp:ListItem Value="N" >No</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="KETERANGAN">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtDescription" runat="server" Text ='<%# container.dataitem("Notes")%>'></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn Visible="False" DataField="isYNQuestion"></asp:BoundColumn>
                                    <asp:BoundColumn Visible="False" DataField="CheckListID"></asp:BoundColumn>
                                    <asp:BoundColumn Visible="False" DataField="YNQuestion"></asp:BoundColumn>
                                </Columns>
                            </asp:DataGrid>
                        </div>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req"> Pemeriksa</label>
                        <asp:TextBox ID="txtChecker" runat="server" Width="150px" MaxLength="50" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" CssClass="validator_general" Display="Dynamic" ErrorMessage="Harap isi Nama Pemeriksa" ControlToValidate="txtChecker" />
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label> Jabatan Pemeriksa</label>
                        <asp:TextBox ID="txtCheckerJob" runat="server" Width="200px" MaxLength="50" />
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req"> Tanggal Periksa</label>
                        <asp:TextBox ID="txtCheckDate" runat="server" />
                        <asp:CalendarExtender ID="txtCheckDate_CalendarExtender" runat="server" Enabled="True" TargetControlID="txtCheckDate" Format="dd/MM/yyyy" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="*" CssClass="validator_general" ControlToValidate="txtCheckDate"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_general"> Catatan</label>
                        <asp:TextBox ID="txtNotes" runat="server" CssClass="multiline_textbox" TextMode="MultiLine" />
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonSave" runat="server" Text="Save" CssClass="small button blue" />&nbsp;
                    <asp:Button ID="ButtonCancel" runat="server" Text="Cancel" CssClass="small button gray" CausesValidation="False" />
                </div>
            </asp:Panel>        
    </form>
</body>
</html>
