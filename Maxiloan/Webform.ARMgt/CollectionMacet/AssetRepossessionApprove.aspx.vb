﻿
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController

Public Class AssetRepossessionApprove
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents txtTglSTNK As ucDateCE


    Protected WithEvents oCGID As UcBranchCollection
    Protected WithEvents txtReposessExpense As ucNumberFormat
    Protected WithEvents txtKMAsset As ucNumberFormat
    Protected WithEvents GridNavigator As ucGridNav



    Dim m_AssetRepo As New AssetRepoController

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Public Property CollectorID() As String
        Get
            Return CType(ViewState("CollectorID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CollectorID") = Value
        End Set
    End Property
    Public Property cgid() As String
        Get
            Return CType(viewstate("CGID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CGID") = Value
        End Set
    End Property
    Public Property AssetSeqNo() As Integer
        Get
            Return CType(Viewstate("AssetSeqNo"), Integer)
        End Get
        Set(ByVal Value As Integer)
            Viewstate("AssetSeqNo") = Value
        End Set
    End Property

    Public Property ApplicationID() As String
        Get
            Return CType(viewstate("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property
    Public Property RALNO() As String
        Get
            Return CType(ViewState("RALNO"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("RALNO") = Value
        End Set
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If
        AddHandler GridNavigator.PageChanged, AddressOf PageNavigation
        If Not Me.IsPostBack Then
            Me.cgid = Me.GroubDbID

            Me.FormID = "CollRepoApr"
            PnlSearch.Visible = True
            PnlList.Visible = False
            pnlApproval.Visible = False
            PnlListDetail.Visible = False
            cboSearchBy.ClearSelection()
            txtSearchBy.Text = ""
            txtTglSTNK.Text = ""


            lblPemeriksa.Text = ""
            lblJabatanPemeriksa.Text = ""
            lblTglPeriksa.Text = ""
            ddlApprove.SelectedIndex = 0
            txtNotes.Text = ""



        End If

    End Sub
    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        PnlSearch.Visible = True
        PnlList.Visible = False
        pnlApproval.Visible = False
        cboSearchBy.ClearSelection()
        txtSearchBy.Text = ""
        txtTglSTNK.Text = ""

        lblPemeriksa.Text = ""
        lblJabatanPemeriksa.Text = ""
        lblTglPeriksa.Text = ""
        ddlApprove.SelectedIndex = 0
        txtNotes.Text = ""
    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        Bindgrid()
    End Sub
    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Buttonsearch.Click

        Me.SearchBy = ""
        Me.SortBy = ""

        If oCGID.BranchID.Trim <> "" Then
            Me.SearchBy = "AssetrepossessionOrder.CGID='" & oCGID.BranchID & "'"
        End If

        If cboSearchBy.SelectedIndex <> 0 And txtSearchBy.Text <> "" Then
            If Me.SearchBy = "" Then
                Me.SearchBy = cboSearchBy.SelectedItem.Value.Trim & " like '%" + txtSearchBy.Text.Trim + "%'"
            Else
                Me.SearchBy = Me.SearchBy & " and " & cboSearchBy.SelectedItem.Value.Trim & " like '%" + txtSearchBy.Text.Trim + "%'"
            End If
        End If

        If (txtTglSTNK.Text <> "") Then
            Me.SearchBy = String.Format("{0} and agreementAsset.TaxDate = '{1}' ", SearchBy, ConvertDate2(txtTglSTNK.Text.Trim))
        End If

        Bindgrid()
        PnlList.Visible = True
        PnlListDetail.Visible = False
    End Sub

    Protected Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        currentPage = e.CurrentPage
        Bindgrid(True)
        GridNavigator.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
    End Sub
    Sub Bindgrid(Optional isFrNav As Boolean = False) 
        Dim oAssetRepo As New Parameter.AssetRepo
        Dim dtvEntity As New DataView
        Dim dtsEntity As New DataTable
        With oAssetRepo
            .strConnection = GetConnectionString()
            .WhereCond = SearchBy
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oAssetRepo = m_AssetRepo.AssetRepoApprovalList(oAssetRepo)
        recordCount = oAssetRepo.TotalRecord
        'Dtg.DataSource = oAssetRepo.listData.DefaultView
        dtsEntity = oAssetRepo.listData
        dtsEntity.TableName = "Table"

        dtvEntity = dtsEntity.DefaultView
        dtvEntity.Sort = Me.SortBy

        Dtg.DataSource = dtvEntity
        Try
            Dtg.DataBind()
        Catch ex As Exception

        End Try

        If (isFrNav = False) Then
            GridNavigator.Initialize(recordCount, pageSize)
        End If

    End Sub

    Protected Sub dtgEntity_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles Dtg.ItemCommand
        Select e.CommandName
            Case "REPOSSESS"
                If SessionInvalid() Then
                    Exit Sub
                End If
               


                lblPemeriksa.Text = CType(e.Item.FindControl("lblCheckListPic"), Label).Text
                lblJabatanPemeriksa.Text = CType(e.Item.FindControl("lblJobTitlePIC"), Label).Text
                lblTglPeriksa.Text = CType(CType(e.Item.FindControl("lblCheckerListDate"), Label).Text, DateTime).ToString("dd/MM/yyyy")
                ddlApprove.SelectedIndex = 0
                txtNotes.Text = ""

                CollectorID = e.Item.Cells(5).Text.Trim
                RALNO = e.Item.Cells(10).Text.Trim
                AssetSeqNo = CInt(e.Item.Cells(11).Text.Trim)
                ApplicationID = e.Item.Cells(12).Text.Trim
                BranchID = e.Item.Cells(13).Text.Trim

                viewRepoDetail(e.Item.Cells(9).Text.Trim)

                PnlSearch.Visible = False
                PnlList.Visible = False
                pnlApproval.Visible = True

        End Select
    End Sub

    Private Sub viewRepoDetail(ByVal agreementno As String)
        Dim oAssetRepo As New Parameter.AssetRepo
        Dim dt As New DataTable
        PnlListDetail.Visible = True

        With oAssetRepo
            .strConnection = GetConnectionString()
            .AgreementNo = agreementno
            .BusinessDate = Me.BusinessDate
            .RALNo = ralNo
        End With

        oAssetRepo = m_AssetRepo.AssetRepoDetail(oAssetRepo)
        dt = oAssetRepo.listData



        hypAgreementNo.Text = CStr(dt.Rows(0).Item("AgreementNo")) 
        hypAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "Collection" & "', '" & CStr(dt.Rows(0).Item("ApplicationID")).Trim & "')" 
        hypCustomerName.Text = CStr(dt.Rows(0).Item("CustomerName")) 
        ' hypCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "Collection" & "', '" & Server.UrlEncode(Me.CustomerID.Trim) & "')"

        lblAddress.Text = CStr(dt.Rows(0).Item("MailingAddress"))
        lblDuedate.Text = CStr(dt.Rows(0).Item("Duedate"))
        lblOverDueDays.Text = CStr(dt.Rows(0).Item("EndPastDuedays"))
        lblInstallmentNo.Text = CStr(dt.Rows(0).Item("InstallmentNo"))
        lblInstallmentAmount.Text = FormatNumber(CStr(dt.Rows(0).Item("InstallmentAmount")), 0)
        lblODAmount.Text = FormatNumber(CStr(dt.Rows(0).Item("EndPastDueAmt")), 0)
        lblLateCharges.Text = FormatNumber(CStr(dt.Rows(0).Item("LC")), 0)
        lblOSBallance.Text = FormatNumber(CStr(dt.Rows(0).Item("OS_Gross")), 0)

        lblRALNo.Text = CStr(dt.Rows(0).Item("RALNo"))
        lblRALPeriod.Text = CStr(dt.Rows(0).Item("RALPeriod"))


        lblExecutor.Text = IIf(IsDBNull(dt.Rows(0).Item("CollectorID")), "", dt.Rows(0).Item("CollectorID").ToString).ToString
        lblOnHand.Text = CStr(dt.Rows(0).Item("OnHand"))
        lblAsset.Text = CStr(dt.Rows(0).Item("Description"))
        lblLicensePlate.Text = CStr(dt.Rows(0).Item("LicensePlate"))
        lblChassisNo.Text = CStr(dt.Rows(0).Item("ChassisNo"))
        lblEngineNo.Text = CStr(dt.Rows(0).Item("EngineNo"))
        lblYear.Text = CStr(dt.Rows(0).Item("Year")).Trim
        lblColor.Text = CStr(dt.Rows(0).Item("Color")).Trim
         
        lblAssetLocation.Text = CStr(dt.Rows(0).Item("AssetLocation")).Trim
        lblAssetCondition.Text = CStr(dt.Rows(0).Item("AssetCondition")).Trim
        lblBAPK.Text = CStr(dt.Rows(0).Item("BAPK")).Trim
        lblKMAsset.Text = FormatNumber(IIf(IsDBNull(dt.Rows(0).Item("KMAsset")) = True, "0", dt.Rows(0).Item("KMAsset")), 0)
        lblColor.Text = IIf(IsDBNull(dt.Rows(0).Item("KMAsset")) = True, "", dt.Rows(0).Item("Warna")).ToString 
        ' lbllisencePlate.Text = dt.Rows(0).Item("LicensePlate").ToString 
        lblChassisNo.Text = IIf(IsDBNull(dt.Rows(0).Item("KMAsset")) = True, "", dt.Rows(0).Item("NoRangka")).ToString
        lblEngineNo.Text = IIf(IsDBNull(dt.Rows(0).Item("KMAsset")) = True, "", dt.Rows(0).Item("NoMesin")).ToString 
        LblRepossessFee.Text = FormatNumber(CStr(dt.Rows(0).Item("ReposessFee")).Trim, 0)


        lblRepossesDate.Text = CStr(dt.Rows(0).Item("RepossesDate"))
        lblReposessExpense.Text = FormatNumber(CStr(dt.Rows(0).Item("biayaTarik")).Trim, 0)
        lblGrade.Text = CStr(dt.Rows(0).Item("Grade"))
        lblSTNK.Text = IIf(CStr(dt.Rows(0).Item("AdaSTNK")) = "0", "Tidak Ada", "Ada")
        lblSTNKEndDate.Text = IIf(CStr(dt.Rows(0).Item("AdaSTNK")) = "0", "", CStr(dt.Rows(0).Item("STNKEndDate")))
        lblLokasiPenyimpanan.Text = CStr(dt.Rows(0).Item("LokasiAsset"))
    End Sub


    Private Sub Dtg_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles Dtg.ItemDataBound
        Dim lbTemp As HyperLink
        If e.Item.ItemIndex >= 0 Then
            '*** Add agreement view
            lbTemp = CType(e.Item.FindControl("hlAgreement"), HyperLink)
            lbTemp.NavigateUrl = "javascript:OpenAgreementNo('" & "Collection" & "', '" & e.Item.Cells(7).Text.Trim & "')"

            '*** Add customer  view
            lbTemp = CType(e.Item.FindControl("hlCustomer"), HyperLink)
            lbTemp.NavigateUrl = "javascript:OpenCustomer('" & "Collection" & "', '" & e.Item.Cells(8).Text.Trim & "')"
        End If
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        PnlSearch.Visible = True
        PnlList.Visible = True
        pnlApproval.Visible = False


        lblPemeriksa.Text = ""
        lblJabatanPemeriksa.Text = ""
        lblTglPeriksa.Text = ""
        ddlApprove.SelectedIndex = 0
        txtNotes.Text = ""

    End Sub


    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim oAssetRepo As New Parameter.AssetRepo

        With oAssetRepo
            .strConnection = GetConnectionString()
            .CGID = Me.cgid
            .AssetSeqNo = Me.AssetSeqNo
            .ApplicationID = Me.ApplicationID
            .BranchId = Me.BranchID
            .RALNo = RALNO
            .BussinessDate = Me.BusinessDate
            .LoginId = Me.Loginid
            .Notes = txtNotes.Text
            .CollectorID = CollectorID
        End With
        Try 
            m_AssetRepo.AssetRepoRejectApprove(oAssetRepo, ddlApprove.SelectedValue)

            btnCancel_Click(Nothing, Nothing)
            imgsearch_Click(Nothing, Nothing)

        Catch ex As Exception
            ShowMessage(lblMessage, "Simpan Data Gagal", True)
            Exit Sub
        End Try
    End Sub
End Class