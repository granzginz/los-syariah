﻿#Region "Import"
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Parameter.AssetRepo
#End Region

Public Class InventoryOnRequest
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oCGID As UcBranchCollection

#Region "Private Const"
    Private m_InvOnRequest As New CollInvOnRequestController
    Private m_error As Boolean = False
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region

#Region "private Property"
    Private Property cgid() As String
        Get
            Return CType(viewstate("CGID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CGID") = Value
        End Set
    End Property

    Private Property CustomerID() As String
        Get
            Return CType(viewstate("CustomerID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerID") = Value
        End Set
    End Property

    Private Property ApplicationID() As String
        Get
            Return CType(viewstate("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property
    Private Property AgreementNo() As String
        Get
            Return CType(viewstate("AgreementNo"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AgreementNo") = Value
        End Set
    End Property
#End Region

#Region "Private Sub And Function"
    Private Sub BindInventoryOnRequest()
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim cInvOnRequest As New GeneralPagingController
        Dim oInvOnRequest As New Parameter.GeneralPaging
        With oInvOnRequest
            .strConnection = GetConnectionString
            .WhereCond = Me.SearchBy
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = Me.SortBy
            .SpName = "spInventoryOnRequest "
        End With
        Try
            oInvOnRequest = cInvOnRequest.GetGeneralPaging(oInvOnRequest)
        Catch ex As Exception            
            ShowMessage(lblMessage, ex.Message.Trim, True)
            Exit Sub
        End Try

        With oInvOnRequest
            lblrecord.Text = CType(.TotalRecords, String)
            lblTotPage.Text = Math.Ceiling(.TotalRecords / pageSize).ToString
            lblPage.Text = CType(currentPage, String)
            recordCount = .TotalRecords
        End With
        dtsEntity = oInvOnRequest.ListData
        dtvEntity = dtsEntity.DefaultView
        If dtvEntity.Count <= 0 Then            
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
        End If
        Dtg.DataSource = dtvEntity
        Try
            Dtg.DataBind()
        Catch
            Dtg.CurrentPageIndex = 0
            Dtg.DataBind()
        End Try

        PagingFooter()
    End Sub
    Private Sub ViewInventoryOnRequest()
        Dim oInvOnRequest As New Parameter.CollInvSelling
        Dim dtInvOnRequest As New DataTable
        With oInvOnRequest
            .strConnection = getConnectionString()
            .ApplicationID = Me.ApplicationID
            .BusinessDate = Me.BusinessDate
            .BranchId = Me.BranchID
        End With
        Try
            oInvOnRequest = m_InvOnRequest.CollInvOnRequest(oInvOnRequest)
        Catch ex As Exception            
            ShowMessage(lblMessage, ex.Message.Trim, True)
        End Try

        dtInvOnRequest = oInvOnRequest.listData
        If 0 < dtInvOnRequest.Rows.Count Then
            With dtInvOnRequest
                hypAgreementNo.Text = CStr(.Rows(0).Item("AgreementNo"))
                hypCustomerId.Text = CStr(.Rows(0).Item("Name"))
                lblAddress.Text = CStr(.Rows(0).Item("MailingAddress"))
                lblAsset.Text = CStr(.Rows(0).Item("Description"))
                lblLicensePlate.Text = CStr(.Rows(0).Item("LicensePlate"))
                lblColor.Text = CStr(.Rows(0).Item("Color"))
                lblChasisNo.Text = CStr(.Rows(0).Item("SerialNo1"))
                lblEngineNo.Text = CStr(.Rows(0).Item("SerialNo2"))
                lblDefaultStatus.Text = CStr(.Rows(0).Item("DefaultStatus"))
                lblSTNKEndDate.Text = CStr(.Rows(0).Item("STNKEndDate"))
                lblRepossDate.Text = CStr(.Rows(0).Item("RepossesDate"))
                lblInvExpDate.Text = CStr(.Rows(0).Item("InventoryExpectedDate"))
                lblDueDate.Text = CStr(.Rows(0).Item("NextInstallmentDate"))
                lblInstallNo.Text = CStr(.Rows(0).Item("NextInstallmentNumber"))
                lblInstallAmount.Text = FormatNumber(CStr(.Rows(0).Item("InstallmentAmount")), 0)
                lblOverdueDays.Text = CStr(.Rows(0).Item("EndpastDueDays"))
                lblAmountOverDue.Text = FormatNumber(CStr(.Rows(0).Item("Amount_Overdue_Gross")), 0)
                lblAssetLocation.Text = CStr(.Rows(0).Item("AssetLocation"))
                lblAssetCondition.Text = CStr(.Rows(0).Item("AssetCondition"))
                lblOSIns.Text = FormatNumber(CStr(.Rows(0).Item("OSIns")), 0)
                lblOSClaimIns.Text = FormatNumber(CStr(.Rows(0).Item("OSClaimIns")), 0)
                lblOSSTNK.Text = FormatNumber(CStr(.Rows(0).Item("OSSTNK")), 0)
                lblOSRepossFee.Text = FormatNumber(CStr(.Rows(0).Item("OSRepossesFee")), 0)
                lblOSPrincipal.Text = FormatNumber(CStr(.Rows(0).Item("OutstandingPrincipalUndue")), 0)
                lblOSInterest.Text = FormatNumber(CStr(.Rows(0).Item("OutstandingInterestUndue")), 0)
                lblOSInstallDue.Text = FormatNumber(CStr(.Rows(0).Item("OSInstallDue")), 0)
                lblOSInsDue.Text = FormatNumber(CStr(.Rows(0).Item("OSInsDue")), 0)
                lblOSLCInstall.Text = FormatNumber(CStr(.Rows(0).Item("OSLCInstall")), 0)
                lblOSLCIns.Text = FormatNumber(CStr(.Rows(0).Item("OSLCIns")), 0)
                lblOSInstallCollFee.Text = FormatNumber(CStr(.Rows(0).Item("OSInstallCollFee")), 0)
                lblOSInsCollFee.Text = FormatNumber(CStr(.Rows(0).Item("OSInsCollFee")), 0)
                lblOSPDCBounceFee.Text = FormatNumber(CStr(.Rows(0).Item("OSPDCBounceFee")), 0)
                lblOSWOAmount.Text = FormatNumber(CStr(.Rows(0).Item("OSWOAmount")), 0)
                lblOSNAAmount.Text = FormatNumber(CStr(.Rows(0).Item("OSNAAmount")), 0)
                lblPrePaidAmount.Text = FormatNumber(CStr(.Rows(0).Item("ContractPrepaidAmount")), 0)
                lblARGross.Text = FormatNumber(CStr(.Rows(0).Item("ARGross")), 0)
                lblAmtIncRecognize.Text = FormatNumber(CStr(.Rows(0).Item("AmountIncRecognize")), 0)
                lblUCI.Text = FormatNumber(CStr(.Rows(0).Item("UCI")), 0)
                lblECI.Text = FormatNumber(CStr(.Rows(0).Item("ECI")), 0)
                lblInvAmount.Text = FormatNumber(CStr(.Rows(0).Item("InvAmount")), 0)
            End With
            PnlListDetail.Visible = True
            PnlSearch.Visible = False
            PnlList.Visible = False            
        Else
            Me.ApplicationID = ""
            Me.BranchID = ""
            ShowMessage(lblMessage, " Data Detail tidak ditemukan", True)            
        End If
    End Sub
    Private Sub UpdateInvOnRequest()
        Dim oInvOnRequestProses As New Parameter.CollInvSelling
        With oInvOnRequestProses
            .strConnection = getConnectionString()
            .BusinessDate = Me.BusinessDate
            .ApplicationID = Me.ApplicationID
            .BranchId = Me.BranchID
        End With
        Try
            m_InvOnRequest.InvOnRequestProses(oInvOnRequestProses)
            m_error = False
        Catch ex As Exception
            ShowMessage(lblMessage, "Data ini sudah jadi Inventory", True)
            m_error = True            
        End Try

    End Sub
#End Region

#Region "Page Load "
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        'Put user code to initialize the page here
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not Me.IsPostBack Then

            Me.FormID = "CollOnRequest"
            Me.cgid = Me.GroubDbID
            InitialDefaultPanel()
        End If
    End Sub
#End Region

#Region " InitialDefaultPanel "
    Private Sub InitialDefaultPanel()
        PnlSearch.Visible = True
        PnlList.Visible = False
        PnlListDetail.Visible = False
    End Sub
#End Region

#Region "Event Search & Reset Navigation "
    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        InitialDefaultPanel()
        cboSearchBy.ClearSelection()
        txtSearchBy.Text = ""
        Me.SearchBy = ""
        Me.SortBy = ""        
    End Sub

    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Buttonsearch.Click
        Me.SearchBy = ""
        Me.SortBy = ""        
        If oCGID.BranchID.Trim <> "" Then
            Me.SearchBy = " CollectionAgreement.CGid='" & oCGID.BranchID & "'"
            Me.cgid = oCGID.BranchID
        End If

        If cboSearchBy.SelectedIndex <> 0 And txtSearchBy.Text <> "" Then
            If Me.SearchBy = "" Then
                If txtSearchBy.Text.IndexOf("%") >= 0 Then
                    Me.SearchBy = cboSearchBy.SelectedItem.Value.Trim & " like '%" + txtSearchBy.Text.Trim + "%'"

                Else
                    Me.SearchBy = cboSearchBy.SelectedItem.Value.Trim & " like '%" + txtSearchBy.Text.Trim + "%'"
                End If
            Else
                If txtSearchBy.Text.IndexOf("%") >= 0 Then
                    Me.SearchBy = Me.SearchBy & " and " & cboSearchBy.SelectedItem.Value.Trim & " like '%" + txtSearchBy.Text.Trim + "%'"

                Else
                    Me.SearchBy = Me.SearchBy & " and " & cboSearchBy.SelectedItem.Value.Trim & " like '%" + txtSearchBy.Text.Trim + "%'"

                End If
            End If
        End If
        BindInventoryOnRequest()
        PnlList.Visible = True
    End Sub

#End Region

#Region "DTG panel Search "
    Private Sub Dtg_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles Dtg.SortCommand
        If Me.SortBy.IndexOf("DESC") > 0 Then
            Me.SortBy = e.SortExpression & " ASC"
        Else
            Me.SortBy = e.SortExpression & " DESC"
        End If
        Call BindInventoryOnRequest()
    End Sub

    Private Sub Dtg_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles Dtg.ItemCommand
        Dim oltlApplicationID As Literal
        Dim oltlBranchID As Literal
        Dim oLtlAgreementNo As Literal
        Dim oLtlCustomerID As Literal
        oltlApplicationID = DirectCast(e.Item.FindControl("ltlApplicationID"), Literal)
        oltlBranchID = DirectCast(e.Item.FindControl("ltlBranchID"), Literal)
        oLtlAgreementNo = DirectCast(e.Item.FindControl("ltlAgreemenNO"), Literal)
        oLtlCustomerID = DirectCast(e.Item.FindControl("ltlCustomerID"), Literal)
        Select Case e.CommandName
            Case "INVENTORY"
                'If checkFeature(Me.Loginid, Me.FormID, "INV", Me.AppId) Then
                '    If sessioninvalid() Then
                '        Exit Sub
                '    End If
                'End If

                Me.ApplicationID = oltlApplicationID.Text.Trim
                Me.BranchID = oltlBranchID.Text.Trim
                Call ViewInventoryOnRequest()
                hypAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "Collection" & "', '" & oLtlAgreementNo.Text.Trim & "')"
                hypCustomerId.NavigateUrl = "javascript:OpenCustomer('" & "Collection" & "', '" & oLtlCustomerID.Text.Trim & "')"

        End Select
    End Sub


    Private Sub Dtg_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles Dtg.ItemDataBound
        Dim oLtlAgreementNo As Literal
        Dim oLtlCustomerID As Literal

        Dim lbTemp As HyperLink

        If e.Item.ItemIndex >= 0 Then
            oLtlAgreementNo = DirectCast(e.Item.FindControl("ltlAgreemenNO"), Literal)
            oLtlCustomerID = DirectCast(e.Item.FindControl("ltlCustomerID"), Literal)
            '*** Add agreement view
            lbTemp = CType(e.Item.FindControl("hlAgreementNo"), HyperLink)
            lbTemp.NavigateUrl = "javascript:OpenAgreementNo('" & "Collection" & "', '" & oLtlAgreementNo.Text.Trim & "')"

            '*** Add customer  view
            lbTemp = CType(e.Item.FindControl("hlCustomer"), HyperLink)
            lbTemp.NavigateUrl = "javascript:OpenCustomer('" & "Collection" & "', '" & oLtlCustomerID.Text.Trim & "')"
        End If
    End Sub
#End Region

#Region "Event Detail Data"
    Private Sub imbCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        PnlListDetail.Visible = False
        Me.SearchBy = ""
        Me.SortBy = ""        
        PnlSearch.Visible = True
        PnlList.Visible = True
    End Sub

    Private Sub imbSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
        UpdateInvOnRequest()
        If Not (m_error) Then            
            Call BindInventoryOnRequest()
            PnlSearch.Visible = True
            PnlList.Visible = False
            PnlListDetail.Visible = False
        End If
    End Sub
#End Region

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        BindInventoryOnRequest()
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                BindInventoryOnRequest()
            End If
        End If
    End Sub
#End Region

End Class