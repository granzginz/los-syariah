﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class ChangeAddress
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oAddress As UcCompanyAddress

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.ChangeAddress
    Private oController As New ChangeAddressController

#End Region

#Region "Property"
    Private Property ApplicationID() As String
        Get
            Return CType(viewstate("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property

    Private Property AgreementNo() As String
        Get
            Return CType(viewstate("AgreementNo"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AgreementNo") = Value
        End Set
    End Property

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            oAddress.Style = "collection"
            Me.FormID = "ChangeAddress"
            If CheckForm(Me.Loginid, Me.FormID, "Maxiloan") Then
                InitialDefaultPanel()
                FillCbo()
            End If
            If Request.QueryString("cmd") <> "" Then
                Me.ApplicationID = Request.QueryString("id")
                Me.AgreementNo = Request.QueryString("no")
                If CheckFeature(Me.Loginid, Me.FormID, "Chg", "Maxiloan") Then
                    CopyAddress(Me.ApplicationID, Me.AgreementNo)
                End If
            End If
        End If
    End Sub
    Private Sub InitialDefaultPanel()
        pnlDtGrid.Visible = False
        pnlsearch.Visible = True
        pnlChange.Visible = False        
    End Sub
    Private Sub FillCbo()
        Dim oDataTable As New DataTable
        With oCustomClass
            .strConnection = GetConnectionSTring
            .strKey = "CG"
            .CGID = Me.GroubDbID
            .CollectorType = ""
        End With
        oCustomClass = oController.AddressListCG(oCustomClass)
        oDataTable = oCustomClass.ListAddress
        With cboCG
            .DataSource = oDataTable
            .DataTextField = "CGName"
            .DataValueField = "CGID"
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = ""
            If oDataTable.Rows.Count = 1 Then
                .Items.FindByValue(Me.GroubDbID).Selected = True
            End If
        End With
    End Sub

    Private Sub imgsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Buttonsearch.Click
        Me.SearchBy = "CollectionAgreement.CGID='" & cboCG.SelectedItem.Value.Trim & "'"
        Me.SearchBy = Me.SearchBy & " and Agreement.ContractStatus<>'EXP' "
        If cboSearchBy.SelectedItem.Value.Trim <> "Select One" Then
            Me.SearchBy = Me.SearchBy & " and " & cboSearchBy.SelectedItem.Value.Trim & " like '%" + txtSearchBy.Text.Trim + "%'"
        End If
        Me.SortBy = ""
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then            
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click        
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub
#End Region

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        Dim intloop As Integer
        Dim hypID As HyperLink

        With oCustomClass
            .strConnection = GetConnectionString
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oCustomClass = oController.ChangeAddressList(oCustomClass)

        DtUserList = oCustomClass.ListAddress
        DvUserList = DtUserList.DefaultView
        recordCount = oCustomClass.TotalRecord
        DvUserList.Sort = Me.SortBy
        dtgAddress.DataSource = DvUserList
        With oCustomClass
            .strConnection = GetConnectionString
        End With
        Try
            dtgAddress.DataBind()
        Catch
            dtgAddress.CurrentPageIndex = 0
            dtgAddress.DataBind()
        End Try

        PagingFooter()
        pnlDtGrid.Visible = True
        pnlsearch.Visible = True
        pnlChange.Visible = False        
    End Sub
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub CopyAddress(ByVal strID As String, ByVal strNo As String)
        Dim oDataTable As New DataTable
        pnlDtGrid.Visible = False
        pnlsearch.Visible = False
        pnlChange.Visible = True
        With oCustomClass
            .strConnection = GetConnectionString
            .AgreementNo = strNo
            .ApplicationID = strID
        End With
        oCustomClass = oController.ChangeAddressView(oCustomClass)
        oDataTable = oCustomClass.ListAddress
        If oDataTable.Rows.Count > 0 Then
            If Not IsDBNull(oDataTable.Rows(0)("AgreementNo")) Then
                hypAgreementNo.Text = Trim(CType(oDataTable.Rows(0)("AgreementNo"), String))
                hypAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "Collection" & "', '" & Server.UrlEncode(strID.Trim) & "')"
            End If
            If Not IsDBNull(oDataTable.Rows(0)("Name")) Then
                hypCustomerName.Text = Trim(CType(oDataTable.Rows(0)("Name"), String))
                hypCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "Collection" & "', '" & Server.UrlEncode(CType(oDataTable.Rows(0)("CustomerID"), String)) & "')"
            End If
            If Not IsDBNull(oDataTable.Rows(0)("MailingAddress")) Then
                lblAddress.Text = Trim(CType(oDataTable.Rows(0)("MailingAddress"), String))
            End If
            If Not IsDBNull(oDataTable.Rows(0)("MailingRT")) Then
                lblRT.Text = Trim(CType(oDataTable.Rows(0)("MailingRT"), String))
            End If
            If Not IsDBNull(oDataTable.Rows(0)("MailingRW")) Then
                lblRW.Text = Trim(CType(oDataTable.Rows(0)("MailingRW"), String))
            End If
            If Not IsDBNull(oDataTable.Rows(0)("MailingKelurahan")) Then
                lblKelurahan.Text = Trim(CType(oDataTable.Rows(0)("MailingKelurahan"), String))
            End If
            If Not IsDBNull(oDataTable.Rows(0)("MailingKecamatan")) Then
                lblKecamatan.Text = Trim(CType(oDataTable.Rows(0)("MailingKecamatan"), String))
            End If
            If Not IsDBNull(oDataTable.Rows(0)("mailingCity")) Then
                lblCity.Text = Trim(CType(oDataTable.Rows(0)("mailingCity"), String))
            End If
            If Not IsDBNull(oDataTable.Rows(0)("MailingZipCode")) Then
                lblZipCode.Text = Trim(CType(oDataTable.Rows(0)("MailingZipCode"), String))
            End If
            If Not IsDBNull(oDataTable.Rows(0)("MailingPhone1")) Then
                lblPhone1.Text = Trim(CType(oDataTable.Rows(0)("MailingPhone1"), String))
            End If
            If Not IsDBNull(oDataTable.Rows(0)("MailingAreaPhone1")) Then
                lblAreaPhone1.Text = Trim(CType(oDataTable.Rows(0)("MailingAreaPhone1"), String))
            End If
            If Not IsDBNull(oDataTable.Rows(0)("MailingPhone2")) Then
                lblPhone2.Text = Trim(CType(oDataTable.Rows(0)("MailingPhone2"), String))
            End If
            If Not IsDBNull(oDataTable.Rows(0)("MailingAreaPhone2")) Then
                lblAreaPhone2.Text = Trim(CType(oDataTable.Rows(0)("MailingAreaPhone2"), String))
            End If
            If Not IsDBNull(oDataTable.Rows(0)("MailingFax")) Then
                lblFaxNo.Text = Trim(CType(oDataTable.Rows(0)("MailingFax"), String))
            End If
            If Not IsDBNull(oDataTable.Rows(0)("MailingAreaFax")) Then
                lblAreaFax.Text = Trim(CType(oDataTable.Rows(0)("MailingAreaFax"), String))
            End If
            If Not IsDBNull(oDataTable.Rows(0)("MobilePhone")) Then
                lblMobilePhone.Text = Trim(CType(oDataTable.Rows(0)("MobilePhone"), String))
                txtMobilePhone.Text = lblMobilePhone.Text
            End If
            fillAddress(CType(oDataTable.Rows(0)("CustomerID"), String))
        End If
    End Sub
    Private Sub imbReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        InitialDefaultPanel()
        cboSearchBy.ClearSelection()
        txtSearchBy.Text = ""
    End Sub
    Private Sub imbSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
        Dim hasil As Integer
        With oCustomClass
            .strConnection = GetConnectionString()
            .ApplicationID = Me.ApplicationID.Trim
            .AgreementNo = Me.AgreementNo.Trim
            .MailingAddress = oAddress.Address.Trim
            .MailingKelurahan = oAddress.Kelurahan.Trim
            .MailingKecamatan = oAddress.Kecamatan.Trim
            .MailingCity = oAddress.City.Trim
            .MailingZipCode = oAddress.ZipCode.Trim
            .MailingRT = oAddress.RT.Trim
            .MailingRW = oAddress.RW.Trim
            .MailingAreaPhone1 = oAddress.AreaPhone1.Trim
            .MailingAreaPhone2 = oAddress.AreaPhone2.Trim
            .MailingPhone1 = oAddress.Phone1.Trim
            .MailingPhone2 = oAddress.Phone2.Trim
            .MailingAreaFax = oAddress.AreaFax.Trim
            .MailingFax = oAddress.Fax.Trim
            .Notes = txtNotes.Text.Trim
            .MobilePhone = txtMobilePhone.Text.Trim
            .BusinessDate = Me.BusinessDate
        End With
        oCustomClass = oController.ChangeAddressSave(oCustomClass)
        hasil = oCustomClass.hasil
        If hasil = 0 Then            
            ShowMessage(lblMessage, "Gagal", True)
        Else
            pnlChange.Visible = False
            If Me.SearchBy Is Nothing Then
                Me.SearchBy = "CollectionAGreement.CGID='" & Me.GroubDbID & "'"
            End If
            Me.SortBy = ""
            DoBind(Me.SearchBy, Me.SortBy)
        End If
    End Sub

    Sub fillAddress(ByVal id As String)
        Dim oData As New DataTable
        'Dim oApplication As New Parameter.Application
        Dim oRow As DataRow
        Dim intloop As Integer
        With oCustomClass
            .strConnection = GetConnectionString
            .CustomerID = id.Trim
        End With
        oCustomClass = oController.GetAddress(oCustomClass)
        oData = oCustomClass.ListAddress
        cboCopy.DataSource = oData.DefaultView
        cboCopy.DataTextField = "Combo"
        cboCopy.DataValueField = "Type"
        cboCopy.DataBind()
        cboCopy.Items.Insert(0, "Select One")
        cboCopy.Items(0).Value = ""
        Dim index As Integer
        For intloop = 0 To oData.Rows.Count - 1
            If oData.Rows(intloop).Item("Type").ToString.Trim = "Legal" Then
                index = intloop
            End If
        Next
        'cboCopy.Items.FindByValue("Legal").Selected = True
        'oRow = oData.Rows(index)
        'oAddress.Address = oRow.Item(0).ToString.Trim
        'oAddress.RT = oRow.Item(1).ToString.Trim
        'oAddress.RW = oRow.Item(2).ToString.Trim
        'oAddress.Kelurahan = oRow.Item(3).ToString.Trim
        'oAddress.Kecamatan = oRow.Item(4).ToString.Trim
        'oAddress.City = oRow.Item(5).ToString.Trim
        'oAddress.ZipCode = oRow.Item(6).ToString.Trim
        'oAddress.AreaPhone1 = oRow.Item(7).ToString.Trim
        'oAddress.Phone1 = oRow.Item(8).ToString.Trim
        'oAddress.AreaPhone2 = oRow.Item(9).ToString.Trim
        'oAddress.Phone2 = oRow.Item(10).ToString.Trim
        'oAddress.AreaFax = oRow.Item(11).ToString.Trim
        'oAddress.Fax = oRow.Item(12).ToString.Trim
        'oAddress.BindAddress()

        oData = oCustomClass.ListAddress
        Div1.InnerHtml = GenerateScript(oData)
        'End If
    End Sub
    Private Function GenerateScript(ByVal DtTable As DataTable) As String
        Dim strScript As String
        Dim strScript1 As String
        Dim strType As String
        Dim DataRow As DataRow()
        Dim i As Int32
        Dim j As Int32

        strScript = "<script language=""JavaScript"">" & vbCrLf
        strScript &= "ListData = new Array(" & vbCrLf
        strType = ""
        For j = 0 To cboCopy.Items.Count - 1
            DataRow = DtTable.Select(" Type = '" & cboCopy.Items(j).Value.Trim & "'")
            If DataRow.Length > 0 Then
                For i = 0 To DataRow.Length - 1
                    If strType <> CStr(DataRow(i)("Type")).Trim Then
                        strType = CStr(DataRow(i)("Type")).Trim
                        strScript &= "new Array(" & vbCrLf
                        strScript1 = ""
                    End If
                    If strScript1 = "" Then
                        strScript1 = " new Array('" & DataRow(i)("Address").ToString.Trim & "','" & DataRow(i)("RT").ToString.Trim & "' " & _
                                        ",'" & DataRow(i)("RW").ToString.Trim & "','" & DataRow(i)("Kelurahan").ToString.Trim & "'" & _
                                        ",'" & DataRow(i)("Kecamatan").ToString.Trim & "','" & DataRow(i)("City").ToString.Trim & "'" & _
                                        ",'" & DataRow(i)("ZipCode").ToString.Trim & "','" & DataRow(i)("AreaPhone1").ToString.Trim & "'" & _
                                        ",'" & DataRow(i)("Phone1").ToString.Trim & "','" & DataRow(i)("AreaPhone2").ToString.Trim & "'" & _
                                        ",'" & DataRow(i)("Phone2").ToString.Trim & "','" & DataRow(i)("AreaFax").ToString.Trim & "'" & _
                                        ",'" & DataRow(i)("Fax").ToString.Trim & "') "
                    End If
                Next
                strScript &= strScript1 & ")," & vbCrLf
            Else
                strScript &= " null," & vbCrLf
            End If
        Next
        If Right(strScript.Trim, 5) = "null," Then
            strScript = Left(strScript.Trim, strScript.Trim.Length - 1)
        Else
            strScript = Left(strScript.Trim, strScript.Trim.Length - 2)
        End If

        strScript &= vbCrLf & "));" & vbCrLf
        strScript &= "</script>"
        Return strScript
    End Function

    Private Sub dtgAddress_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgAddress.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim lblTemp As Label
            Dim hyTemp As HyperLink
            Dim lblApplicationId As Label
            lblApplicationId = CType(e.Item.FindControl("lblApplicationId"), Label)
            '*** Customer Link
            'lblTemp = CType(e.Item.FindControl("lblCustomerId"), Label)
            'hyTemp = CType(e.Item.FindControl("hyCustomerName"), HyperLink)
            'hyTemp.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(lblTemp.Text.Trim) & "')"
            '*** Agreement No link
            hyTemp = CType(e.Item.FindControl("hypAgreementNodtg"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenAgreementNo('" & "Collection" & "', '" & Server.UrlEncode(lblApplicationId.Text.Trim) & "')"
            '*** ApplicationId link
            hyTemp = CType(e.Item.FindControl("hypApplicationIDdtg"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenApplicationId('" & "Collection" & "', '" & Server.UrlEncode(hyTemp.Text.Trim) & "')"
        End If
    End Sub
    Private Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        pnlsearch.Visible = True
        pnlDtGrid.Visible = True
        pnlChange.Visible = False
        If Me.SearchBy Is Nothing Then
            Me.SearchBy = "CollectionAGreement.CGID='" & Me.GroubDbID & "'"
        End If
        Me.SortBy = ""
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

End Class