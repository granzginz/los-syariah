﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class ChangeAddressInq
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oSearchBy As UcSearchBy
    Protected WithEvents oBranch As UcBranch

#Region "Property"
    Private Property cmdwhere() As String
        Get
            Return CType(ViewState("cmdwhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("cmdwhere") = Value
        End Set
    End Property

    Private Property FilterBy() As String
        Get
            Return CType(ViewState("FilterBy"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("FilterBy") = Value
        End Set
    End Property

#End Region

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.AgreementList
    Private oController As New AgreementListController
#End Region


    Private Sub InitialDefaultPanel()
        pnlDatagrid.Visible = False
        lblMessage.Visible = False

    End Sub


#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            If Request.QueryString("message") <> "" Then
                ShowMessage(lblMessage, Request.QueryString("message"), True)
            End If
            Me.FormID = "CollInqChgAddress"
            oSearchBy.ListData = "AgreementNo,Agreement No-Name,Customer Name"
            oSearchBy.BindData()
            'sdate.isCalendarPostBack = False
            'sdate.FillRequired = False
            'sdate.ValidationErrMessage = "Please Fill Effective date With dd/MM/yyyy"
            'sdate.dateValue = Me.BusinessDate.ToString("dd/MM/yyyy")

            'sdate.Display = "Dynamic"

            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.SearchBy = ""
                Me.SortBy = ""
            End If
        End If
    End Sub

#End Region

#Region "DoBind"
    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        Dim intloop As Integer
        Dim hypID As HyperLink
        Dim oPaging As New Parameter.GeneralPaging
        Dim m_controller As New GeneralPagingController

        With oPaging
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
            .SpName = "spChangeAddressInqPrintPaging"
        End With

        oPaging = m_controller.GetGeneralPaging(oPaging)
        If Not oPaging Is Nothing Then
            DtUserList = oPaging.ListData
            recordCount = oPaging.TotalRecords
        Else
            recordCount = 0
        End If
        DtgAgree.DataSource = DtUserList.DefaultView
        DtgAgree.CurrentPageIndex = 0
        DtgAgree.DataBind()
        PagingFooter()

        pnlDatagrid.Visible = True
    End Sub

#End Region

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub

#End Region

#Region "databound"
    Private Sub DtgAgree_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgree.ItemDataBound
        Dim lblTemp As Label
        Dim lbbranch As Label
        Dim hyTemp As HyperLink

        If SessionInvalid() Then
            Exit Sub
        End If
        Dim m As Int32
        Dim hypCancel As HyperLink
        Dim inHyView As HyperLink
        Dim HyPrint As HyperLink
        Dim hyApplicationid As HyperLink
        Dim lblInStatus As Label
        Dim inlblSeqNo As New Label
        Dim instyle As String = "AccMnt"
        Dim lblStatus As Label

        If e.Item.ItemIndex >= 0 Then
            lblInStatus = CType(e.Item.FindControl("lblStatus"), Label)
            lbbranch = CType(e.Item.FindControl("lbranchid"), Label)
            lblTemp = CType(e.Item.FindControl("lblCustomerId"), Label)
            hyTemp = CType(e.Item.FindControl("hyApplicationId"), HyperLink)
            'inHyView = CType(e.Item.FindControl("HyView"), HyperLink)
            inlblSeqNo = CType(e.Item.FindControl("lblSeqNO"), Label)
            HyPrint = CType(e.Item.FindControl("HyPrint"), HyperLink)
            lblStatus = CType(e.Item.FindControl("lblStatus"), Label)
            'inHyView.NavigateUrl = "javascript:OpenAgreementTransfer('" & "AccMnt" & "', '" & Server.UrlEncode(lbbranch.Text.Trim) & "', '" & Server.UrlEncode(hyTemp.Text.Trim) & "', '" & Server.UrlEncode(inlblSeqNo.Text.Trim) & "')"

            '*** Customer Link
            'lblTemp = CType(e.Item.FindControl("lblCustomerId"), Label)
            hyTemp = CType(e.Item.FindControl("hyCustomerName"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(lblTemp.Text.Trim) & "')"
            '*** Agreement No link
            hyApplicationid = CType(e.Item.FindControl("hyApplicationId"), HyperLink)
            hyTemp = CType(e.Item.FindControl("hyAgreementNo"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(hyApplicationid.Text.Trim) & "')"
            HyPrint.NavigateUrl = "ViewChangeAddressInq.aspx?ApplicationID=" & hyApplicationid.Text.Trim & "&BranchID=" & lbbranch.Text.Trim
            'If lblInStatus.Text.Trim <> "E" Then
            '    HyPrint.Visible = False
            'End If
            '*** ApplicationId link
            hyTemp = CType(e.Item.FindControl("hyApplicationId"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenApplicationId('" & "AccMnt" & "', '" & Server.UrlEncode(hyTemp.Text.Trim) & "')"

        End If
    End Sub
#End Region

#Region "Reset"

    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        InitialDefaultPanel()
        'Server.Transfer("ATInq.aspx")
    End Sub
#End Region

#Region "Search"
    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Buttonsearch.Click
        If SessionInvalid() Then
            Exit Sub
        End If
        If CheckFeature(Me.Loginid, Me.FormID, "SRCH", Me.AppId) Then
            Dim filterby As String = ""
            Me.SearchBy = ""
            Me.cmdwhere = ""
            Me.SearchBy = Me.SearchBy & "  CA.branchid = '" & oBranch.BranchID & "'"

            If filterby <> "" Then
                filterby = filterby & " , Branch : " & oBranch.BranchName & " "
            Else
                filterby = filterby & "Branch : " & oBranch.BranchName & " "
            End If

            'If cboStatus.SelectedItem.Value <> "ALL" Then
            '    Me.SearchBy = Me.SearchBy & " and  AGT.Status = '" & cboStatus.SelectedItem.Value & "'"
            '    If filterby <> "" Then
            '        filterby = filterby & " , Status : " & cboStatus.SelectedItem.Value & " "
            '    Else
            '        filterby = filterby & "Status : " & cboStatus.SelectedItem.Value & " "
            '    End If

            'Else
            '    Me.SearchBy = Me.SearchBy & " and  AGT.Status in ('A','R','C','E')"
            'End If
            'If sdate.dateValue <> "" Then
            '    Me.SearchBy = Me.SearchBy & " and  AGT.effectivedate = '" & ConvertDate2(sdate.dateValue) & "'"
            '    If filterby <> "" Then
            '        filterby = filterby & " , Effective Date : " & ConvertDate2(sdate.dateValue).ToString("dd/MM/yyyy") & " "
            '    Else
            '        filterby = filterby & "Effective Date : " & ConvertDate2(sdate.dateValue).ToString("dd/MM/yyyy") & " "
            '    End If
            'End If


            'Me.SearchBy = Me.SearchBy & " And IsPrintEndors=" & ddlPrint.SelectedValue
            Me.SearchBy = Me.SearchBy

            If oSearchBy.Text.Trim <> "" Then
                If Right(oSearchBy.Text.Trim, 1) = "%" Then
                    Me.SearchBy = Me.SearchBy & " and " & oSearchBy.ValueID & " like '" & oSearchBy.Text.Trim.Replace("'", "''") & "'"
                    If filterby <> "" Then
                        filterby = filterby & " , " & oSearchBy.ValueID & "  like " & oSearchBy.Text.Trim.Replace("'", "''") & " "
                    Else
                        filterby = filterby & "" & oSearchBy.ValueID & "  like " & oSearchBy.Text.Trim.Replace("'", "''") & " "
                    End If
                Else
                    Me.SearchBy = Me.SearchBy & " and " & oSearchBy.ValueID & " = '" & oSearchBy.Text.Trim.Replace("'", "''") & "'"
                    If filterby <> "" Then
                        filterby = filterby & " , " & oSearchBy.ValueID & "  : " & oSearchBy.Text.Trim.Replace("'", "''") & " "
                    Else
                        filterby = filterby & "" & oSearchBy.ValueID & "  : " & oSearchBy.Text.Trim.Replace("'", "''") & " "
                    End If
                End If

            End If
            Me.FilterBy = filterby
            Me.cmdwhere = Me.SearchBy

            pnlDatagrid.Visible = True
            DtgAgree.Visible = True

            DoBind(Me.SearchBy, Me.SortBy)

        End If
    End Sub
#End Region

#Region "Sort"
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

#Region "Print"
    Private Sub imbPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonPrint.Click
        Dim cookie As HttpCookie = Request.Cookies("AT")
        If Not cookie Is Nothing Then
            cookie.Values("cmdwhere") = Me.cmdwhere
            cookie.Values("FilterBy") = Me.FilterBy
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie("AT")
            cookieNew.Values.Add("cmdwhere", Me.cmdwhere)
            cookieNew.Values.Add("FilterBy", Me.FilterBy)
            Response.AppendCookie(cookieNew)
        End If
        Response.Redirect("ViewChangeAddressInq.aspx")
    End Sub

#End Region

End Class