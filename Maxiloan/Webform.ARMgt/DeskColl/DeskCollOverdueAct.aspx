﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="DeskCollOverdueAct.aspx.vb"
    Inherits="Maxiloan.Webform.ARMgt.DeskCollOverdueAct" EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="CollActResult" Src="../../Webform.UserController/ucCollectionActResult.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>DeskCollOverdueAct</title>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%#Request.servervariables("SERVER_NAME")%>/';
    </script>
    <script language="javascript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;
        function ResultChange() {
            var myID = 'ucPromiseDate_txtDate';
            var myID1 = 'ucPlanDate_txtDate';
            var objDate = eval('document.forms[0].' + myID);
            var objDate1 = eval('document.forms[0].' + myID1);
            var result = document.forms[0].cboResult.value;
            if ((result == 'PTPYPICKUP') || (result == 'PTPYTOBANK') || (result == 'PTPYTOSUPP') || (result == 'PTPYTOFIN')) {
                //                objDate.disabled = false;
                //                objDate1.disabled = true;
                //                objDate1.value = '';
                //                document.forms[0].all.ucPromiseDate_txtDate_imgCalender.disabled = false;
                //                document.forms[0].all.ucPlanDate_txtDate_imgCalender.disabled = true;
                //                document.forms[0].ucPlanDate_txtDate.value = '';
                document.forms[0].txtHour.disabled = true;
                document.forms[0].txtMinute.disabled = true;
                document.forms[0].cboPlanActivity.disabled = true;
            }
            else {
                //                objDate.disabled = true;
                //                objDate1.disabled = false;
                //                objDate.value = '';
                //                document.forms[0].all.ucPromiseDate_txtDate_imgCalender.disabled = true;
                //                document.forms[0].all.ucPlanDate_txtDate_imgCalender.disabled = false;
                //                document.forms[0].ucPromiseDate_txtDate.value = '';
                document.forms[0].txtHour.disabled = false;
                document.forms[0].txtMinute.disabled = false;
                document.forms[0].cboPlanActivity.disabled = false;
            }
        }
        function OpenActivity() {
            window.open('../Inquiry/InqCollActivityResult.aspx?AgreementNo=' + hdnAgreement.value + '&CustomerName=' + hdnCustName.value + '&CustomerID=' + hdnCustID.value + '&Style=Collection&Referrer=Activity', 'UserLookup', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenCollection() {
            window.open('../Inquiry/InqCollActivityHistory.aspx?AgreementNo=' + hdnAgreement.value + '&CustomerName=' + hdnCustName.value + '&CustomerID=' + hdnCustID.value + '&Style=Collection', 'UserLookup', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }

        var hdnDetail;
        var hdndetailvalue;
        var cboBankAccount = null;
        function ParentChange(pCmbOfPayment, pBankAccount, pHdnDetail, pHdnDetailValue, itemArray) {
            hdnDetail = eval('document.forms[0].' + pHdnDetail);
            HdnDetailValue = eval('document.forms[0].' + pHdnDetailValue);
            var i, j;
            for (i = eval('document.forms[0].' + pBankAccount).options.length; i >= 0; i--) {
                eval('document.forms[0].' + pBankAccount).options[i] = null

            };
            if (itemArray == null) {
                j = 0;
            }
            else {
                j = 2;
            };
            eval('document.forms[0].' + pBankAccount).options[0] = new Option('Select One', '0');
            eval('document.forms[0].' + pBankAccount).options[1] = new Option('ALL', 'ALL');
            if (itemArray != null) {
                for (i = 0; i < itemArray.length; i++) {
                    eval('document.forms[0].' + pBankAccount).options[j++] = new Option(itemArray[i][0], itemArray[i][1]);

                };
                eval('document.forms[0].' + pBankAccount).selected = true;
            }
        };

        function cboChildonChange(selIndex, l, j) {
            hdnDetail.value = l;
            HdnDetailValue.value = j;
            eval('document.forms[0].hdnBankAccount').value = selIndex;
        }

        function RestoreInsuranceCoIndex(BankAccountIndex) {
            cboChild = eval('document.forms[0].cboChild');
            if (cboChild != null)
                if (eval('document.forms[0].hdnBankAccount').value != null) {
                    if (BankAccountIndex != null)
                        cboChild.selectedIndex = BankAccountIndex;
                }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>    
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>
                        DESK COLLECTION OVERDUE CALL
                    </h3>
                </div>
            </div>
            <asp:Panel ID="pnlsearch" runat="server">
                <input id="hdnChildValue" type="hidden" name="hdnSP" runat="server" />
                <input id="hdnChildName" type="hidden" name="hdnSP" runat="server" />
                <input id="hdnBankAccount" type="hidden" name="hdSP" runat="server" />
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Collection Group</label>
                        <asp:DropDownList ID="cboParent" runat="server" onchange="<%#CollectionGroupIDChange()%>">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="rfvcboCollectionGroup" runat="server" InitialValue="0"
                            CssClass="validator_general" Display="Dynamic" ErrorMessage="Harap pilih Collection Group"
                            ControlToValidate="cboParent"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Collector</label>
                        <asp:DropDownList ID="cboChild" runat="server" onchange="cboChildonChange(this.selectedIndex,this.options[this.selectedIndex].value,this.options[this.selectedIndex].text);">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="rfvCollector" runat="server" InitialValue="0" Display="Dynamic"
                            CssClass="validator_general" ErrorMessage="Harap pilih Collector" ControlToValidate="cboChild"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="Btnsearch" runat="server" CausesValidation="False" Text="Search"
                        CssClass="small button blue" />
                    <asp:Button ID="ButtonReset" runat="server" CausesValidation="False" Text="Reset"
                        CssClass="small button gray" />
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlList" runat="server">
                <div class="form_box_title">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtgDeskCollList" runat="server" Width="100%" OnSortCommand="SortGrid"
                                CssClass="grid_general" BorderStyle="None" BorderWidth="0" DataKeyField="AgreementNo"
                                AutoGenerateColumns="False" AllowSorting="True">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hypAgreementNodtg" runat="server" Text='<%# Container.Dataitem("AgreementNo")%>'>
                                            </asp:HyperLink>
                                            <asp:Label ID="hypApplicationID" Visible="False" runat="server" Text='<%# Container.Dataitem("ApplicationID")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="CustomerName" HeaderText="NAMA CUSTOMER">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCustomerID" Visible="False" runat="server" Text='<%# Container.Dataitem("CustomerID")%>'>
                                            </asp:Label>
                                            <asp:HyperLink ID="hypCustomerNamedtg" runat="server" Text='<%# Container.Dataitem("CustomerName")%>'>
                                            </asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>                                    
                                    <asp:TemplateColumn SortExpression="LegalPhone1" HeaderText="NO TELP KTP">
                                        <ItemTemplate>
                                            <asp:Label ID="lblLegalPhone" runat="server" Text='<%#Container.DataItem("LegalPhone1")%>'
                                                ForeColor='<%# getColor(Container.DataItem("LegalPhoneYN"))%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="ResidencePhone1" HeaderText="NO TELP RUMAH">
                                        <ItemTemplate>
                                            <asp:Label ID="lblResidencePhone" runat="server" Text='<%#Container.DataItem("ResidencePhone1")%>'
                                                ForeColor='<%# getColor(Container.DataItem("HomePhoneYN"))%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="CompanyPhone1" HeaderText="NO TELP KANTOR">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCompanyPhone" runat="server" Text='<%#Container.DataItem("CompanyPhone1")%>'
                                                ForeColor='<%# getColor(Container.DataItem("CompanyPhoneYN"))%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="isSMSSent" HeaderText="KIRIM SMS" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSMSSent" runat="server" Text='<%#Container.DataItem("isSMSSent")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="PDCEmptyStatus" HeaderText="PDC" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPDCRFQ" runat="server" Text='<%#Container.DataItem("PDCEmptyStatus")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="EndPastDueDays" SortExpression="EndPastDueDays" HeaderText="DAYS">                                
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="NextInstallmentDate" SortExpression="NextInstallmentDate"
                                        HeaderText="TGL JATUH TEMPO" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="NewTaskYN" SortExpression="NewTaskYN" HeaderText="NEW TASK" Visible="false">
                                    </asp:BoundColumn>
                                </Columns>
                            </asp:DataGrid>
                            <div class="button_gridnavigation">
                                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                                    CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                                    CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                                    CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                                    CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                                    EnableViewState="False"></asp:Button>
                                <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" MinimumValue="1"
                                    ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"></asp:RangeValidator>
                                <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                                    ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general">
                                </asp:RequiredFieldValidator>
                            </div>
                            <div class="label_gridnavigation">
                                <asp:Label ID="lblPage" runat="server"></asp:Label>of
                                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonStartWorking" runat="server" Text="Start Working" CssClass="small button blue">
                    </asp:Button>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlWorking" runat="server">
                <div class="form_box_title">
                    <div class="form_single">
                        <h4>
                            DESK COLLECTION OVERDUE CALL RESULT
                        </h4>
                    </div>
                </div>
              
                    <uc1:CollActResult id="ucCollActResult" runat="server" ></uc1:CollActResult>

       <%--         <div class="form_box">
                    <div class="form_left">
                        <label>No Kontrak</label>
                        <asp:HyperLink ID="hypAgreementNo" runat="server"></asp:HyperLink>
                    </div>
                    <div class="form_right">
                        <label>Nama Customer</label>
                        <asp:HyperLink ID="hypCustomerName" runat="server"></asp:HyperLink>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>Alamat</label>
                        <asp:Label ID="lblAddress" runat="server"></asp:Label>
                    </div>
                </div>


                <div class="form_box">
                    <div class="form_left">
                        <label>Asset</label>
                        <asp:Label ID="lblAsset" runat="server"></asp:Label>
                    </div>
                     <div class="form_right">
                        <label>No Polisi</label>
                        <asp:Label ID="lblLicenseNo" runat="server"></asp:Label>
                     </div> 
                </div>

                 
                <div class="form_box">
                    <div class="form_left">
                        <label>Nama CMO</label>
                        <asp:Label ID="lblCMONo" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                      <label>Collector</label>
                        <asp:Literal ID="ltlCollector" runat="server"></asp:Literal> 
                   
                    </div>
                </div>



                <div class="form_box">
                     <div class="form_left">
                        <label>No Telepon Alamat KTP</label>
                        <asp:Label ID="lblLegalPhone1" runat="server"></asp:Label>
                        <asp:Label ID="lblLegalPhone2" runat="server"></asp:Label>
                    </div>

                    <div class="form_right"> 
                        <label> No Telepon tempat tinggal</label>
                        <asp:Label ID="lblResidencePhone1" runat="server"></asp:Label>
                        <asp:Label ID="lblResidencePhone2" runat="server"></asp:Label> 
                    </div>
                </div>


               
                <div class="form_box">
                      <div class="form_left">
                        <label>No Telepon Kantor</label>
                        <asp:Label ID="lblCompanyPhone1" runat="server"></asp:Label>
                        <asp:Label ID="lblCompanyPhone2" runat="server"></asp:Label>
                    </div>
                     <div class="form_right">
                        <label> No HandPhone</label>
                        <asp:Label ID="lblMobilePhone" runat="server"></asp:Label>
                     </div>
                </div>
                 
                <div class="form_box">
                    <div class="form_left">
                        <label>No Telepon Alamat Tagih</label>
                        <asp:Label ID="lblMailingPhone1" runat="server"></asp:Label>
                        <asp:Label ID="lblMailingPhone2" runat="server"></asp:Label>
                    </div>
                     <div class="form_right">
                          <label>No Telepon Suami/Istri</label>
                           <asp:Label ID="lblPhonePasangan" runat="server"></asp:Label>
                     </div> 
                </div>

                 <div class="form_box">
                    <div class="form_single">
                        <label>bagian ocx</label> 
                    </div>
                </div>


                <div class="form_box">
                    <div class="form_left">
                        <label> Cara Pembayaran</label>
                        <asp:Literal ID="ltlWayOfPayment" runat="server"></asp:Literal>
                    </div>
                   
                     <div class="form_right">
                        <label>Jumlah Hari Tunggak</label>
                        <asp:Label ID="lblOverDuedays" runat="server"></asp:Label>&nbsp;Hari
                    </div>

                </div>

--%>
<%--

                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Status Kirim SMS</label>
                        <asp:Label ID="lblisSMSSent" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Permintaan PDC</label>
                        <asp:Label ID="lblPDCRequest" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <asp:Label ID="lblPreviousOverDue" runat="server" Visible="False"></asp:Label>
                    </div>
                  
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Tanggal Jatuh Tempo</label>
                        <asp:Label ID="lblDuedate" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Angsuran Ke</label>
                        <asp:Label ID="lblInstallmentNo" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                      
                    </div>
                    <div class="form_right">
                        <label>
                            Supervisor</label>
                        <asp:Literal ID="ltlSupervisor" runat="server"></asp:Literal>
                    </div>
                </div>--%>
                <div class="form_box_header">
                    <div class="form_single">
                        <h4>
                            DATA FINANCIAL
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Jumlah Angsuran</label>
                        <asp:Label ID="lblInstallmentAmount" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Sisa Saldo (O/S Balance)</label>
                        <asp:Label ID="lblOSBalance" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Jumlah Tunggakan</label>
                        <asp:Label ID="lblOverDueAmt" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Denda keterlambatan Angsuran</label>
                        <asp:Label ID="lblOSInstallment" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Denda keterlambatan Asuransi</label>
                        <asp:Label ID="lblOSInsurance" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Biaya Tagih</label>
                        <asp:Label ID="lblOSBillingCharge" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Biaya Tolakan PDC</label>
                        <asp:Label ID="lblOSPDC" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Status Bayar</label>
                        <asp:Label ID="lblStatusBayar" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Aktivitas Baru</label>
                        <asp:Label ID="lblAktivitasBaru" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <h4>
                            HASIL KEGIATAN
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Kegiatan</label>
                        <label>
                            Telepon</label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Hasil</label>
                        <asp:DropDownList ID="cboResult" runat="server" onchange="ResultChange();">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Harap pilih Hasil"
                            CssClass="validator_general" ControlToValidate="cboResult"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Tanggal Janji Bayar</label>
                        <asp:TextBox ID="txtPromiseDate" runat="server"></asp:TextBox>
                        <asp:CalendarExtender ID="txtPromiseDate_CalendarExtender" runat="server" Enabled="True"
                            TargetControlID="txtPromiseDate" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                        <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                            CssClass="validator_general" ControlToValidate="txtPromiseDate"></asp:RequiredFieldValidator>--%>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Rencana Kegiatan</label>
                        <asp:DropDownList ID="cboPlanActivity" runat="server">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Tanggal Rencana</label>
                        <asp:TextBox ID="txtPlanDate" runat="server"></asp:TextBox>
                        <asp:CalendarExtender ID="txtPlanDate_CalendarExtender" runat="server" Enabled="True"
                            TargetControlID="txtPlanDate" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                        <%--                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*"
                            CssClass="validator_general" ControlToValidate="txtPlanDate"></asp:RequiredFieldValidator>--%>
                        Jam
                        <asp:TextBox ID="txtHour" runat="server" Width="30px" Visible="True" Enabled="True"></asp:TextBox>
                        <asp:RangeValidator ID="rgvHour" runat="server" Display="Dynamic" ErrorMessage="*"
                            CssClass="validator_general" ControlToValidate="txtHour" Visible="False" Enabled="False"
                            Type="Integer" MinimumValue="0" MaximumValue="24"></asp:RangeValidator>
                        <asp:TextBox ID="txtMinute" runat="server" Width="36px" Visible="True" Enabled="True"
                            MaxLength="2"></asp:TextBox>Minutes
                        <asp:RangeValidator ID="rgvMinute" runat="server" Display="Dynamic" ErrorMessage="*"
                            CssClass="validator_general" ControlToValidate="txtMinute" Visible="False" Enabled="False"
                            Type="Integer" MinimumValue="0" MaximumValue="60"></asp:RangeValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Perlu Perhantian Supervisor</label>
                        <asp:CheckBox ID="chkRequest" runat="server"></asp:CheckBox>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_general">
                            Catatan</label>
                        <asp:TextBox ID="txtNote" runat="server" CssClass="multiline_textbox" TextMode="MultiLine"></asp:TextBox>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Telepon yg Berhasil dihubungi</label>
                        <asp:DropDownList ID="cboSuccess" runat="server">
                            <asp:ListItem Value="">Select One</asp:ListItem>
                            <asp:ListItem Value="Legal Phone">No Telepon Sesuai KTP</asp:ListItem>
                            <asp:ListItem Value="Residence Phone">No Telepon Tempat Tinggal</asp:ListItem>
                            <asp:ListItem Value="Company Phone">No Telepon Perusahaan</asp:ListItem>
                            <asp:ListItem Value="Mobile Phone">No HandPhone</asp:ListItem>
                            <asp:ListItem Value="Mailing Address Phone">No Telepon Alamat Tagih</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <%--<div class="form_box">
                    <div class="form_left">
                        <a href="javascript:OpenActivity()">
                            <asp:Label ID="lblActivityHistory" runat="server" Visible="True">HISTORY KEGIATAN</asp:Label></a>
                    </div>
                    <div class="form_left">
                        <a href="javascript:OpenCollection()">
                            <asp:Label ID="lblCollectionHistory" runat="server" Visible="True">HISTORY COLLECTION</asp:Label></a>
                    </div>
                </div>--%>
                <div class="form_button">
                    <asp:Button ID="ButtonSave" runat="server" Text="Save" CssClass="small button blue"
                        CausesValidation="True"></asp:Button>
                    <asp:Button ID="ButtonPrevious" runat="server" CausesValidation="False" Text="Previous"
                        CssClass="small button gray"></asp:Button>
                    <asp:Button ID="ButtonNext" runat="server" Text="Next" CssClass="small button green"
                        CausesValidation="false"></asp:Button>
                    <asp:Button ID="ButtonBackList" runat="server" Text="Back List" CssClass="small button gray"
                        CausesValidation="False"></asp:Button>
                </div>
            </asp:Panel>
            <div id="divInnerHTML" runat="server">
            </div>
            <input id="hdnAgreement" type="hidden" name="hdnAgreement" runat="server" />
            <input id="hdnCustName" type="hidden" name="hdnCustName" runat="server" />
            <input id="hdnCustID" type="hidden" name="hdnCustID" runat="server" />        
    </form>
</body>
</html>
