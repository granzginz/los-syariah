﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="DownloadSMS.aspx.vb" Inherits="Maxiloan.Webform.ARMgt.DownloadSMS" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcBranchCollection" Src="../../Webform.UserController/UcBranchCollection.ascx" %>
<%@ Register TagPrefix="uc1" Src="../../webform.UserController/ucDateCE.ascx" TagName="ucDateCE"  %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Download SMS Reminder</title>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%#Request.servervariables("SERVER_NAME")%>/';			
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>    
        <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
        <div class="form_title">    
            <div class="title_strip">
            </div>
            <div class="form_single">           
                <h3>
                    DOWNLOAD SMS REMINDER
                </h3>
            </div>
        </div>  
        <asp:Panel ID="PnlSearch" runat="server" >        
        <%--<div class="form_box">
	        <div class="form_single">
                <label  class ="label_req" >Cabang</label>
                <uc1:UcBranchCollection id="oCabang" runat="server"></uc1:UcBranchCollection>
            </div>
        </div>--%>
        <div class="form_box">
	        <div class="form_single">
                <label>SMS Type</label>
                <asp:DropDownList ID="cboSMSType" runat="server">
                    <asp:ListItem Value="ALL" Selected="True">ALL</asp:ListItem>
                    <asp:ListItem Value="R1">Reminder-1</asp:ListItem>
                    <asp:ListItem Value="R2">Reminder-2</asp:ListItem>
                    <asp:ListItem Value="R3">Reminder-3</asp:ListItem>
                </asp:DropDownList>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label class ="label_req">Tanggal</label>                
                <uc1:ucdatece runat="server" id="txtTanggal"></uc1:ucdatece>
	        </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonSearch" runat="server"  Text="Search" CssClass ="small button blue"
                Enabled="True"></asp:Button>&nbsp;
            <asp:Button ID="ButtonReset" runat="server"  Text="Reset" CssClass ="small button gray"
                Enabled="True" CausesValidation="False"></asp:Button>
	    </div>        
        </asp:Panel>    
        <asp:Panel ID="pnlList" runat="server">
        <div class="form_box_title">
            <div class="form_single">              
                <h4>
                    DAFTAR SMS REMINDER
                </h4>
            </div>
        </div>
        <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgSMS" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        CssClass="grid_general" BorderStyle="None" BorderWidth="0"
                        OnSortCommand="Sorting" Width="100%">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />                        
                        <Columns>
                            <asp:BoundColumn DataField="MessageCreatedDate" SortExpression="MessageCreatedDate" HeaderText="TGL">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="SMSType" SortExpression="SMSType" HeaderText="TIPE">
                            </asp:BoundColumn>
                            <asp:TemplateColumn SortExpression="AGREEMENTNO" Visible="True" HeaderText="NO KONTRAK">                                                                                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="lblAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="CustomerName" HeaderText="NAMA CUSTOMER">
                                <ItemTemplate>
                                    <asp:HyperLink ID="lblCustomerName" runat="server" Text='<%#Container.DataItem("CustomerName")%>'>
                                    </asp:HyperLink>
                                    <asp:Label ID="lblCustomerID" runat="server" Visible="False" Text='<%#Container.DataItem("CustomerID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="MobilePhoneNumber" SortExpression="MobilePhoneNumber" HeaderText="MOBILE NO">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="NextInstallmentDueNo" SortExpression="NextInstallmentDueNo" HeaderText="ANGS">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="NextInstallmentDueDate" SortExpression="NextInstallmentDueDate" HeaderText="TGL JT">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="SMSMessage" SortExpression="SMSMessage" HeaderText="ISI PESAN">
                            </asp:BoundColumn>
                        </Columns>
                </asp:DataGrid>    
                <div class="button_gridnavigation">
	                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
		                CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
		                CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
		                CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
		                CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
	                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
		                EnableViewState="False"></asp:Button>
	                <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage"
				        MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer"
				        CssClass="validator_general"></asp:RangeValidator>
			        <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
				        ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general">
			        </asp:RequiredFieldValidator>              
                </div>
                <div class="label_gridnavigation">
	                <asp:Label ID="lblPage" runat="server"></asp:Label>of
	                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
	                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                </div>    
            </div>   
        </div>        
        </div> 
        <div class="form_button">
            <asp:Button ID="ButtonDownload" runat="server"  Text="Download" CssClass ="small button blue"
                Enabled="True"></asp:Button>&nbsp;
	    </div>        
    </asp:Panel>          
    </form>
</body>
</html>
