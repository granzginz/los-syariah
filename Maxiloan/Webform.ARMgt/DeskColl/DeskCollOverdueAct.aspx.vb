﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Framework.SQLEngine
#End Region

Public Class DeskCollOverdueAct
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents ucCollActResult As ucCollectionActResult

#Region "Property"
    Private Property ValidDueDate() As String
        Get
            Return CType(ViewState("ValidDueDate"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ValidDueDate") = Value
        End Set
    End Property
    Private Property ValidDueDateDDMMYY() As String
        Get
            Return CType(ViewState("ValidDueDateDDMMYY"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ValidDueDateDDMMYY") = Value
        End Set
    End Property

    Private Property ApplicationID() As String

        Get
            Return CType(ViewState("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property

    Private Property DeskCollID() As String
        Get
            Return CType(ViewState("DeskCollID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("DeskCollID") = Value
        End Set
    End Property

    Private Property dtgItem() As Integer
        Get
            Return CType(ViewState("dtgItem"), Integer)
        End Get
        Set(ByVal Value As Integer)
            ViewState("dtgItem") = Value
        End Set
    End Property

    Private Property NextDueDate() As String
        Get
            Return CType(ViewState("NextDueDate"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("NextDueDate") = Value
        End Set
    End Property

    Private Property untildatedeskcoll() As String
        Get
            Return CType(ViewState("untildatedeskcoll"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("untildatedeskcoll") = Value
        End Set
    End Property
    Private Property CustomerID() As String
        Get
            Return CType(ViewState("CustomerID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property

    Private Property CustomerName() As String
        Get
            Return CType(ViewState("CustomerName"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerName") = Value
        End Set
    End Property
    Private Property AgreementNo() As String
        Get
            Return CType(ViewState("AgreementNo"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("AgreementNo") = Value
        End Set
    End Property
    Public Property strAll() As String
        Get
            Return CType(ViewState("strAll"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("strAll") = Value
        End Set
    End Property
    Public Property strKey() As String
        Get
            Return CType(ViewState("strKey"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("strKey") = Value
        End Set
    End Property
#End Region

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.DeskCollReminderActivity
    Private oController As New ReminderCallActivityController
    Private oClassAddress As New Parameter.Address
    Private oClassPersonal As New Parameter.Personal
    Private oController1 As New CLActivityController
    'Dim chrClientID As String
#End Region

#Region "LinkTo"
    Function LinkToAgreement(ByVal strAgreementNo As String) As String
        Return "javascript:OpenWinAgreement('" & strAgreementNo & "')"
    End Function
    Function LinkToCustomer(ByVal strCustomerID As String, ByVal strStyle As String) As String
        Return "javascript:OpenWinCustomer('" & strCustomerID & "','" & strStyle & "')"
    End Function

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            InitialDefaultPanel()
            Me.FormID = "CollDCOverDueAct"
            If CheckForm(Me.Loginid, Me.FormID, "Maxiloan") Then
                Me.strKey = "DC"
                'chrClientID = Trim(cboChild.ClientID)
                'Me.SearchBy = " DeskCollTask.PlanDate = '" & Me.BusinessDate & "' and DeskCollTask.TaskType ='CI' "
                'Me.SortBy = ""
                'DoBind(Me.SearchBy, Me.SortBy)

                '-------------gantinya usercontrol------------------
                Dim dtParent As New DataTable
                Dim Coll As New Parameter.RptCollAct
                Dim ControllerCG As New RptCollActController
                'Dim CollList As New Parameter.Collector
                'RequiredFieldValidator1.Enabled = False
                If Me.strAll = "1" Then
                    With Coll
                        .strConnection = GetConnectionString()
                        .CGID = Me.GroubDbID
                        .strKey = "CG_ALL"
                        .CollectorType = ""
                    End With
                Else
                    With Coll
                        .strConnection = GetConnectionString()
                        .CGID = Me.GroubDbID
                        .strKey = "CG"
                        .CollectorType = ""
                    End With
                End If
                Coll = ControllerCG.ViewDataCollector(Coll)
                dtParent = Coll.ListCollector

                cboParent.DataTextField = "CGName"
                cboParent.DataValueField = "CGID"
                cboParent.DataSource = dtParent
                cboParent.DataBind()
                cboParent.Items.Insert(0, "Select One")
                cboParent.Items(0).Value = "0"
                'cboParent.SelectedIndex = 0
                'If dtParent.Rows.Count = 1 Then
                '    CollectionGroupIDChange()
                '    cboParent.Items.FindByValue(Me.GroubDbID).Selected = True
                'End If
                'BindChild()
                getchildcombo()
            End If
        End If
    End Sub

#Region "Tambahan"
    Protected Sub getchildcombo()
        Dim dtChild As New DataTable
        Dim Coll As New Parameter.CLActivity
        Dim CollList As New Parameter.CLActivity
        With Coll
            .strConnection = GetConnectionString()
            .CGID = Me.GroubDbID
            .LoginId = Me.Loginid
            .strKey = Me.strKey
        End With
        CollList = oController1.CLActivityListCollector(Coll)
        dtChild = CollList.ListCLActivity
        Response.Write(GenerateScript(dtChild))
    End Sub

    Public Function CollectionGroupIDChange() As String
        Return "ParentChange('" & Trim(cboParent.ClientID) & "','" & Trim(cboChild.ClientID) & "','" & Trim(hdnChildValue.ClientID) & "','" & Trim(hdnChildName.ClientID) & "',((this.selectedIndex == -1) ? null : ListData[this.selectedIndex]));"
    End Function

    Private Function GenerateScript(ByVal DtTable As DataTable) As String
        Dim strScript As String
        Dim strScript1 As String
        Dim DataRow As DataRow()
        Dim strType As String
        Dim i As Int32
        Dim j As Int32
        strScript = "<script language=""JavaScript"">" & vbCrLf
        strScript &= "ListData = new Array(" & vbCrLf
        strType = ""
        For j = 0 To cboParent.Items.Count - 1
            DataRow = DtTable.Select(" CGID = '" & cboParent.Items(j).Value & "'")
            If DataRow.Length > 0 Then
                For i = 0 To DataRow.Length - 1
                    If strType <> CStr(DataRow(i)("CGID")).Trim Then
                        strType = CStr(DataRow(i)("CGID")).Trim
                        strScript &= "new Array(" & vbCrLf
                        strScript1 = ""
                    End If


                    If strScript1 = "" Then
                        strScript1 = " new Array('" & CStr(DataRow(i)("Name")).Trim & "','" & CStr(IIf(IsDBNull(DataRow(i)("CollectorID")), "null", DataRow(i)("CollectorID"))).Trim & "') "
                    Else
                        strScript1 &= "," & vbCrLf & " new Array('" & CStr(DataRow(i)("Name")).Trim & "','" & CStr(IIf(IsDBNull(DataRow(i)("CollectorID")), "null", DataRow(i)("CollectorID"))).Trim & "') "
                    End If
                Next
                strScript &= strScript1 & ")," & vbCrLf
            Else
                strScript &= " null," & vbCrLf
            End If
        Next

        If Right(strScript.Trim, 5) = "null," Then
            strScript = Left(strScript.Trim, strScript.Trim.Length - 1)
        Else
            strScript = Left(strScript.Trim, strScript.Trim.Length - 2)
        End If

        If Right(strScript.Trim, 4) = "null" Then
            strScript &= vbCrLf & ");" & vbCrLf
        Else
            strScript &= vbCrLf & "));" & vbCrLf
        End If
        'strScript &= vbCrLf & "));" & vbCrLf

        strScript &= "</script>"
        Return strScript
    End Function
#End Region

    Private Sub InitialDefaultPanel()
        pnlWorking.Visible = False
        pnlList.Visible = False
        pnlsearch.Visible = True
    End Sub

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        Dim intloop As Integer
        Dim hypID As HyperLink

        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            '.Collector = Me.DeskCollID
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        Try
            oCustomClass = oController.DeskCollActivityList(oCustomClass)

            DtUserList = oCustomClass.ListDeskColl
            DvUserList = DtUserList.DefaultView
            recordCount = oCustomClass.TotalRecord
            DvUserList.Sort = Me.SortBy
            dtgDeskCollList.DataSource = DvUserList


            If recordCount > 0 Then
                dtgDeskCollList.DataBind()
                pnlList.Visible = True
                PagingFooter()
                pnlsearch.Visible = False
            Else
                ShowMessage(lblMessage, "Data tidak ditemukan", True)
                cboParent.SelectedIndex = 0
            End If
        Catch ex As Exception
            dtgDeskCollList.CurrentPageIndex = 0
            dtgDeskCollList.DataBind()
        End Try

        '------tambahan----------
        'getchildcombo()
        'Dim strScript As String
        'strScript = "<script language=""JavaScript"">" & vbCrLf
        'strScript &= "ParentChange('" & Trim(cboParent.ClientID) & "','" & Trim(cboChild.ClientID) & "','" & Trim(hdnChildValue.ClientID) & "','" & Trim(hdnChildName.ClientID) & "',((eval('document.forms[0]." & cboParent.ClientID & "').selectedIndex  == -1) ? null : ListData[eval('document.forms[0]." & cboParent.ClientID & "').selectedIndex]));"
        'strScript &= "RestoreInsuranceCoIndex(" & Request("hdnBankAccount") & ");"
        'strScript &= "</script>"
        'divInnerHTML.InnerHtml = strScript
        '------------------------
    End Sub

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub
#End Region

    Private Sub imgStartWorking_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonStartWorking.Click
        Context.Trace.Write("StartWorking")
        Context.Trace.Write("Me.dtgItem = " & Me.dtgItem)
        divInnerHTML.InnerHtml = ""

        If CheckFeature(Me.Loginid, Me.FormID, "START", Me.AppId) Then
            If dtgDeskCollList.Items.Count > 0 Then

                Dim oDataTable As New DataTable
                Dim lblAgreementNo As HyperLink
                Dim lblApplicationid As Label
                Dim lblCustomerID As Label
                Dim lblCustomerName As HyperLink

                lblAgreementNo = CType(dtgDeskCollList.Items(0).FindControl("hypAgreementNodtg"), HyperLink)
                lblApplicationid = CType(dtgDeskCollList.Items(0).FindControl("hypApplicationID"), Label)
                lblCustomerID = CType(dtgDeskCollList.Items(0).FindControl("lblCustomerID"), Label)
                lblCustomerName = CType(dtgDeskCollList.Items(0).FindControl("hypCustomerNamedtg"), HyperLink)
                Me.AgreementNo = lblAgreementNo.Text
                Me.CustomerID = lblCustomerID.Text
                Me.CustomerName = lblCustomerName.Text
                Me.ApplicationID = lblApplicationid.Text

                With oCustomClass
                    .strConnection = GetConnectionString()
                    .ApplicationID = lblApplicationid.Text.Trim
                End With
                oCustomClass = oController.DeskCollActivityView(oCustomClass)
                oDataTable = oCustomClass.ListDeskColl
                Me.dtgItem = 0
                cboSuccess.ClearSelection()
                txtNote.Text = ""
                txtPromiseDate.Text = ""
                txtPlanDate.Text = ""
                txtHour.Text = ""
                txtMinute.Text = ""
                FillData(oDataTable)
                pnlList.Visible = False
                pnlWorking.Visible = True
                pnlsearch.Visible = False
            End If
        End If
    End Sub

    Private Sub FillData(ByVal oDataTable As DataTable)
        Dim oRow As DataRow
        Dim oResult As New DataTable
        Dim oAction As New DataTable
        If oDataTable.Rows.Count > 0 Then
            clearVariable()
            txtNote.Text = ""
            txtPromiseDate.Text = ""
            txtPlanDate.Text = ""
            txtHour.Text = Hour(Now).ToString
            chkRequest.Checked = False
            txtMinute.Text = Minute(Now).ToString
            oRow = oDataTable.Rows(0)



            ucCollActResult.hypAgreementNo.Text = CType(oRow("AgreementNo"), String)
            ucCollActResult.hypCustomerName.Text = CType(oRow("Name"), String)
            Dim customerid As String = CType(oRow("CustomerID"), String)

            ucCollActResult.hypAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "Collection" & "', '" & Me.ApplicationID & "')"
            ucCollActResult.hypCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "Collection" & "', '" & Me.CustomerID & "')"

            hdnAgreement.Value = CType(oRow("AgreementNo"), String).Trim
            hdnCustName.Value = CType(oRow("Name"), String).Trim
            hdnCustID.Value = CType(oRow("CustomerID"), String).Trim

            If Not IsDBNull(oRow("Address")) Then
                ucCollActResult.lblAddress.Text = CType(oRow("Address"), String)
            End If
            If Not IsDBNull(oRow("Description")) Then
                ucCollActResult.lblAsset.Text = CType(oRow("Description"), String)
            End If
            If Not IsDBNull(oRow("LicensePlate")) Then
                ucCollActResult.lblLicenseNo.Text = CType(oRow("LicensePlate"), String)
            End If
            If Not IsDBNull(oRow("EmployeeName")) Then
                ucCollActResult.lblCMONo.Text = CType(oRow("EmployeeName"), String)
            End If
            'If Not IsDBNull(oRow("aospv")) Then
            '    lblCMOSPVNo.Text = CType(oRow("aospv"), String)
            'End If
            If CType(oRow("LegalPhoneYN"), String) = "Y" Then
                ucCollActResult.lblLegalPhone1.ForeColor = System.Drawing.Color.Blue
                ucCollActResult.lblLegalPhone2.ForeColor = System.Drawing.Color.Blue
            End If
            If Not IsDBNull(oRow("LegalPhone1")) Then
                ucCollActResult.lblLegalPhone1.Text = CType(oRow("LegalPhone1"), String)
            End If
            If Not IsDBNull(oRow("LegalPhone2")) Then
                ucCollActResult.lblLegalPhone2.Text = CType(oRow("LegalPhone2"), String)
            End If
            If CType(oRow("HomePhoneYN"), String) = "Y" Then
                ucCollActResult.lblResidencePhone1.ForeColor = System.Drawing.Color.Blue
                ucCollActResult.lblResidencePhone2.ForeColor = System.Drawing.Color.Blue
            End If
            If Not IsDBNull(oRow("ResidencePhone1")) Then
                ucCollActResult.lblResidencePhone1.Text = CType(oRow("ResidencePhone1"), String)
            End If
            If Not IsDBNull(oRow("ResidencePhone2")) Then
                ucCollActResult.lblResidencePhone2.Text = CType(oRow("ResidencePhone2"), String)
            End If
            If CType(oRow("CompanyPhoneYN"), String) = "Y" Then
                ucCollActResult.lblCompanyPhone1.ForeColor = System.Drawing.Color.Blue
                ucCollActResult.lblCompanyPhone2.ForeColor = System.Drawing.Color.Blue
            End If
            If Not IsDBNull(oRow("CompanyPhone1")) Then
                ucCollActResult.lblCompanyPhone1.Text = CType(oRow("CompanyPhone1"), String)
            End If
            If Not IsDBNull(oRow("CompanyPhone2")) Then
                ucCollActResult.lblCompanyPhone2.Text = CType(oRow("CompanyPhone2"), String)
            End If
            If Not IsDBNull(oRow("MobilePhone")) Then
                ucCollActResult.lblMobilePhone.Text = CType(oRow("MobilePhone"), String)
            End If
            If CType(oRow("MailingPhoneYN"), String) = "Y" Then
                ucCollActResult.lblMailingPhone1.ForeColor = System.Drawing.Color.Blue
                ucCollActResult.lblMailingPhone2.ForeColor = System.Drawing.Color.Blue
            End If
            If Not IsDBNull(oRow("MailingPhone1")) Then
                ucCollActResult.lblMailingPhone1.Text = CType(oRow("MailingPhone1"), String)
            End If
            If Not IsDBNull(oRow("MailingPhone2")) Then
                ucCollActResult.lblMailingPhone2.Text = CType(oRow("MailingPhone2"), String)
            End If

            If Not IsDBNull(oRow("SIPhone1")) Then
                ucCollActResult.lblSuamiIstriPhone1.Text = CType(oRow("SIPhone1"), String)
            End If
            If Not IsDBNull(oRow("SIPhone2")) Then
                ucCollActResult.lblSuamiIstriPhone2.Text = CType(oRow("SIPhone2"), String)
            End If

            'If Not IsDBNull(oRow("isSMSSent")) Then
            '    lblisSMSSent.Text = CType(oRow("isSMSSent"), String)
            'End If
            'If Not IsDBNull(oRow("LastPastDueDays")) Then
            '    lblPreviousOverDue.Text = CType(oRow("LastPastDueDays"), String)
            'End If
            'If Not IsDBNull(oRow("NextInstallmentDate")) Then
            '    lblDuedate.Text = Format(CDate(oRow("NextInstallmentDate")), "dd/MM/yyyy")
            'End If
            'If Not IsDBNull(oRow("StatusPDC")) Then
            '    lblPDCRequest.Text = CType(oRow("StatusPDC"), String)
            'End If
            If Not IsDBNull(oRow("EndPastDueDays")) Then
                ucCollActResult.lblOverDuedays.Text = CType(oRow("EndPastDueDays"), String)
            End If
            'If Not IsDBNull(oRow("NextInstallmentNumber")) Then
            '    lblInstallmentNo.Text = CType(oRow("NextInstallmentNumber"), String)
            'End If
            If Not IsDBNull(oRow("InstallmentAmount")) Then
                lblInstallmentAmount.Text = FormatNumber(oRow("InstallmentAmount"), 2)
            End If

            If Not IsDBNull(oRow("EndPastDueAmt")) Then
                Dim EpDAmt As Double = CDbl(oRow("EndPastDueAmt"))
                lblOverDueAmt.Text = FormatNumber(oRow("EndPastDueAmt"), 2)
                If EpDAmt <= 0 Then
                    ShowMessage(lblMessage, "Kontrak ini tidak Nunggak ", True)
                End If
            End If
            If Not IsDBNull(oRow("OSLCInstallment")) Then
                lblOSInstallment.Text = FormatNumber(oRow("OSLCInstallment"), 2)
            End If
            If Not IsDBNull(oRow("OSBillingCharges")) Then
                lblOSBillingCharge.Text = FormatNumber(oRow("OSBillingCharges"), 2)
            End If
            If Not IsDBNull(oRow("OSBalance")) Then
                lblOSBalance.Text = FormatNumber(oRow("OSBalance"), 2)
            End If
            If Not IsDBNull(oRow("OSLCInsurance")) Then
                lblOSInsurance.Text = FormatNumber(oRow("OSLCInsurance"), 2)
            End If
            If Not IsDBNull(oRow("OSPDCBounceFee")) Then
                lblOSPDC.Text = FormatNumber(oRow("OSPDCBounceFee"), 2)
            End If
            If Not IsDBNull(oRow("NewTask")) Then
                lblAktivitasBaru.Text = oRow("NewTask").ToString
            End If
            If Not IsDBNull(oRow("Bayar")) Then
                lblStatusBayar.Text = oRow("Bayar").ToString
            End If
            If Not IsDBNull(oRow("ValidDueDate")) Then
                Me.ValidDueDate = CType(oRow("ValidDueDate"), String)
            Else
                Me.ValidDueDate = ""
            End If

            If Not IsDBNull(oRow("ValidDueDateDDMMYYYY")) Then
                Me.ValidDueDateDDMMYY = CType(oRow("ValidDueDateDDMMYYYY"), String)
            Else
                Me.ValidDueDateDDMMYY = ""
            End If


            'Format(CDate(oRow("ValidDueDate")), "dd/MM/yyyy")
            Me.ApplicationID = CType(oRow("ApplicationID"), String)
            Me.DeskCollID = CType(oRow("DeskCollID"), String)

            If Not IsDBNull(oRow("NextInstallmentDate")) Then
                Me.NextDueDate = Format(CDate(oRow("NextInstallmentDate")), "dd/MM/yyyy")
            Else
                Me.NextDueDate = ""
            End If

            If Not IsDBNull(oRow("untildatedeskcoll")) Then
                Me.untildatedeskcoll = Format(CDate(oRow("untildatedeskcoll")), "dd/MM/yyyy")
            Else
                Me.untildatedeskcoll = ""
            End If

            ucCollActResult.ltlWayOfPayment.Text = CStr(IIf(IsDBNull(oRow("WayOfPayment")), "", oRow("WayOfPayment")))
            ucCollActResult.lblCollector.Text = CStr(IIf(IsDBNull(oRow("Collector")), "", oRow("Collector")))
            'ltlSupervisor.Text = CStr(IIf(IsDBNull(oRow("Supervisor")), "", oRow("Supervisor")))
            ucCollActResult.DoBindInstallmentSchedule(Me.ApplicationID, Me.BusinessDate)
        Else
            clearVariable()
            'hypAgreementNo.Text = ""
            'hypCustomerName.Text = ""
            'lblAddress.Text = ""
            'lblAddress.Text = ""
            'lblAsset.Text = ""
            'lblLicenseNo.Text = ""
            'lblCMONo.Text = ""
            'lblLegalPhone1.Text = ""
            'lblLegalPhone2.Text = ""
            'lblResidencePhone1.Text = ""
            'lblResidencePhone2.Text = ""
            'lblCompanyPhone1.Text = ""
            'lblCompanyPhone2.Text = ""
            'lblMobilePhone.Text = ""
            'lblMailingPhone1.Text = ""
            'lblMailingPhone2.Text = ""

            'lblOverDuedays.Text = ""
            'ltlWayOfPayment.Text = ""
            'ltlCollector.Text = ""

            'lblisSMSSent.Text = ""
            'lblPreviousOverDue.Text = ""
            'lblDuedate.Text = ""
            'lblPDCRequest.Text = ""



            'lblInstallmentNo.Text = ""
            'lblInstallmentAmount.Text = ""
            'lblOverDueAmt.Text = ""
            'lblOSInstallment.Text = ""
            'lblOSBillingCharge.Text = ""
            'lblOSBalance.Text = ""
            'lblOSInsurance.Text = ""
            'lblOSPDC.Text = ""

            'ltlSupervisor.Text = ""
        End If
        With oCustomClass
            .strConnection = GetConnectionString()
            .strKey = "Result"
        End With
        oCustomClass = oController.ListResultAction(oCustomClass)
        oResult = oCustomClass.ListDeskColl
        With cboResult
            .DataSource = oResult
            .DataTextField = "Description"
            .DataValueField = "ResultID"
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = ""
        End With
        With oCustomClass
            .strConnection = GetConnectionString()
            .strKey = ""
        End With
        oCustomClass = oController.ListResultAction(oCustomClass)
        oAction = oCustomClass.ListDeskColl
        With cboPlanActivity
            .DataSource = oAction
            .DataTextField = "ActionDescription"
            .DataValueField = "ActionID"
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = "0"
        End With
    End Sub

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
        Dim strResult As String = cboResult.SelectedItem.Value
        Dim hasil As Integer



        If Left(strResult, 4) = "PTPY" Then
            If txtPromiseDate.Text = "" Then
                Context.Trace.Write("Kondisi 1")
                ShowMessage(lblMessage, "Harap isi tanggal janji bayar", True)
                Exit Sub
            Else
                'Remark dulu untuk keperluan migrasi
                'If ConvertDate2(txtPromiseDate.Text) > ConvertDate2(Me.ValidDueDateDDMMYY) Then
                '    Context.Trace.Write("Kondisi 3")
                '    ShowMessage(lblMessage, "Tanggal Janji bayar tidak boleh > dari " & Me.ValidDueDate, True)
                '    Exit Sub
                'End If
                If ConvertDate2(txtPromiseDate.Text) < Me.BusinessDate Then
                    Context.Trace.Write("Kondisi 4")
                    ShowMessage(lblMessage, "Tanggal Janji Bayar tidak boleh < Tanggal hari ini", True)
                    Exit Sub
                End If
            End If
        End If


        If Left(strResult, 4) <> "PTPY" Then
            If txtPlanDate.Text <> "" Then
                'If ConvertDate2(txtPlanDate.Text) < ConvertDate2(Me.ValidDueDateDDMMYY) Then
                If ConvertDate2(txtPlanDate.Text) < ConvertDate2(Me.NextDueDate) Then
                    Context.Trace.Write("Kondisi 4")
                    ShowMessage(lblMessage, "Tanggal rencana tidak boleh < Tanggal jatuh tempo", True)
                    Exit Sub
                End If
                If cboPlanActivity.SelectedItem.Value = "" Then
                    Context.Trace.Write("Kondisi 5")
                    ShowMessage(lblMessage, "Harap isi tanggal rencana dan Rencana Kegiatan", True)
                    Exit Sub
                End If
                If ConvertDate2(txtPlanDate.Text) < Me.BusinessDate Then
                    Context.Trace.Write("Kondisi 6")
                    ShowMessage(lblMessage, "Tanggal rencana tidak boleh < tanggal hari ini", True)
                    Exit Sub
                End If
                If DateDiff(DateInterval.Day, ConvertDate2(Me.untildatedeskcoll), ConvertDate2(txtPlanDate.Text)) > 0 Then
                    Context.Trace.Write("Kondisi 9")
                    ShowMessage(lblMessage, "Tanggal Rencana (" & txtPlanDate.Text & ") tidak boleh > dari Task Expired Date (" & Me.untildatedeskcoll & ")", True)
                    Exit Sub
                End If

                If txtHour.Text.Trim = "" Or txtMinute.Text.Trim = "" Then
                    ShowMessage(lblMessage, "Harap isi dengan Jam dan menit", True)
                    Exit Sub
                End If
            Else
                If cboPlanActivity.SelectedItem.Value <> "0" Then
                    Context.Trace.Write("Kondisi 7")
                    ShowMessage(lblMessage, "Harap isi tanggal rencana dan Rencana Aktivitas", True)
                    Exit Sub
                End If
            End If
        End If


        If txtPromiseDate.Text <> "" And Left(strResult, 4) <> "PTPY" Then
            Context.Trace.Write("Kondisi 2")
            ShowMessage(lblMessage, "Tanggal janji bayar salah, Hasil tidak boleh ada Tanggal Janji Bayar", True)
            Exit Sub
        End If

        If (Left(strResult, 4) = "PTPY" Or cboResult.SelectedItem.Value.Trim = "LM") And cboSuccess.SelectedItem.Value = "" Then
            Context.Trace.Write("Kondisi 8")
            ShowMessage(lblMessage, "Harap pilih No Telepon dihubungi", True)
            Exit Sub
        End If

        'ShowMessage(lblMessage, ConvertDate(txtPlanDate.Text) & " " & txtHour.Text.Trim & ":" & txtMinute.Text.Trim, True)


        With oCustomClass
            .AgreementNo = ucCollActResult.hypAgreementNo.Text.Trim
            .strConnection = GetConnectionString()
            .ApplicationID = Me.ApplicationID.Trim
            .ResultID = cboResult.SelectedItem.Value
            If txtPlanDate.Text <> "" Then
                Context.Trace.Write("Kondisi 10")
                ' .PlanDate = ConvertDate2(txtPlanDate.Text) & " " & txtHour.Text.Trim & ":" & txtMinute.Text.Trim
                .PlanDate = ConvertDate2(txtPlanDate.Text).ToString("yyyyMMdd")
            Else
                Context.Trace.Write("Kondisi 11")
                .PlanDate = Nothing
            End If
            If txtPromiseDate.Text <> "" Then
                .PTPYDate = ConvertDate2(txtPromiseDate.Text).ToString("yyyyMMdd")
            Else
                .PTPYDate = Nothing
            End If
            If cboPlanActivity.SelectedItem.Value = "0" Then
                .PlanActivityID = Nothing
            Else
                .PlanActivityID = cboPlanActivity.SelectedItem.Value
            End If

            .ActionID = "Call"
            .isRequestAssign = chkRequest.Checked
            .Contact = cboSuccess.SelectedItem.Value
            .DeskCollID = Me.DeskCollID
            .BusinessDate = Me.BusinessDate
            .Notes = txtNote.Text.Trim
            .strKey = "CI"
        End With




        oCustomClass = oController.SaveDeskCollActivity(oCustomClass)
        hasil = oCustomClass.hasil



        If hasil = 0 Then
            ShowMessage(lblMessage, "Gagal", True)
        Else
            If Me.dtgItem < dtgDeskCollList.Items.Count - 1 Then
                Me.dtgItem = Me.dtgItem + 1
                Dim oDataTable As New DataTable
                Dim lblAgreementNo As HyperLink
                Dim lblApplicationid As Label
                Dim lblCustomerID As Label
                Dim lblCustomerName As HyperLink

                lblAgreementNo = CType(dtgDeskCollList.Items(Me.dtgItem).FindControl("hypAgreementNodtg"), HyperLink)
                lblApplicationid = CType(dtgDeskCollList.Items(Me.dtgItem).FindControl("hypApplicationID"), Label)
                lblCustomerID = CType(dtgDeskCollList.Items(Me.dtgItem).FindControl("lblCustomerID"), Label)
                lblCustomerName = CType(dtgDeskCollList.Items(Me.dtgItem).FindControl("hypCustomerNamedtg"), HyperLink)
                Me.AgreementNo = lblAgreementNo.Text
                Me.CustomerID = lblCustomerID.Text
                Me.CustomerName = lblCustomerName.Text
                Me.ApplicationID = lblApplicationid.Text
                With oCustomClass
                    .strConnection = GetConnectionString()
                    .ApplicationID = lblApplicationid.Text.Trim
                End With
                oCustomClass = oController.DeskCollActivityView(oCustomClass)
                oDataTable = oCustomClass.ListDeskColl
                FillData(oDataTable)
            Else
                pnlList.Visible = True
                pnlWorking.Visible = False
                pnlsearch.Visible = True
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub


    Private Sub btnBackList_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonBackList.Click
        'InitialDefaultPanel()
        pnlList.Visible = True
        pnlWorking.Visible = False
        pnlsearch.Visible = True
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Function getColor(ByVal LegalPhoneYN As String) As System.Drawing.Color
        If LegalPhoneYN = "Y" Then
            Return System.Drawing.Color.Blue
        Else
            Return System.Drawing.Color.Black
        End If
    End Function

    Private Sub Btnsearch_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles Btnsearch.Click
        Me.SearchBy = ""
        Me.SearchBy = " DeskCollTask.PlanDate = '" & Me.BusinessDate & "' AND DeskCollTask.TaskType ='CI'  AND Agreement.NextInstallmentDate BETWEEN DATEADD(d,-1 * (PO.DeskcollOD),'" & Me.BusinessDate & "') AND DATEADD(d,-1,'" & Me.BusinessDate & "')"
        'Me.SearchBy = " DeskCollTask.PlanDate = '" & Me.BusinessDate & "' AND DeskCollTask.TaskType ='CI'"
        If cboParent.SelectedItem.Value.Trim <> "" Then
            Me.SearchBy = Me.SearchBy & " and CollectionAgreement.CGID='" & cboParent.SelectedItem.Value.Trim & "'"
        End If
        'Me.SearchBy = Me.SearchBy & " and CollectionAgreement.DeskCollID='" & oCLActivity.CollectorID.Trim & "'"
        If hdnChildValue.Value.Trim <> "0" And hdnChildValue.Value.Trim <> "ALL" And hdnChildValue.Value.Trim <> "" Then
            Me.SearchBy = Me.SearchBy & " and CollectionAgreement.DeskCollID='" & hdnChildValue.Value.Trim & "'"
            'oCLActivity.CollectorID = oCLActivity.CollectorID.Trim
        End If
        'Me.DeskCollID = hdnChildValue.Value.Trim
        Me.SortBy = ""
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        Response.Redirect("DeskCollOverdueAct.aspx")
        'InitialDefaultPanel()
    End Sub

    Private Sub btnNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonNext.Click
        Context.Trace.Write("Next")
        Context.Trace.Write("Me.dtgItem = " & Me.dtgItem)
        Context.Trace.Write("dtgDeskCollList.Items.Count - 1 = " & dtgDeskCollList.Items.Count - 1)
        If Me.dtgItem < dtgDeskCollList.Items.Count - 1 Then
            Context.Trace.Write("Memenuhi Syarat")
            cboResult.SelectedIndex = 1
            Me.dtgItem = Me.dtgItem + 1

            Dim oDataTable As New DataTable
            Dim lblAgreementNo As HyperLink
            Dim lblApplicationid As Label
            Dim lblCustomerID As Label
            Dim lblCustomerName As HyperLink

            lblAgreementNo = CType(dtgDeskCollList.Items(Me.dtgItem).FindControl("hypAgreementNodtg"), HyperLink)
            lblApplicationid = CType(dtgDeskCollList.Items(Me.dtgItem).FindControl("hypApplicationID"), Label)
            lblCustomerID = CType(dtgDeskCollList.Items(Me.dtgItem).FindControl("lblCustomerID"), Label)
            lblCustomerName = CType(dtgDeskCollList.Items(Me.dtgItem).FindControl("hypCustomerNamedtg"), HyperLink)

            Me.AgreementNo = lblAgreementNo.Text
            Me.CustomerID = lblCustomerID.Text
            Me.CustomerName = lblCustomerName.Text
            Me.ApplicationID = lblApplicationid.Text

            With oCustomClass
                .strConnection = GetConnectionString()
                .ApplicationID = lblApplicationid.Text.Trim
            End With
            oCustomClass = oController.DeskCollActivityView(oCustomClass)
            oDataTable = oCustomClass.ListDeskColl
            FillData(oDataTable)
        Else
            Context.Trace.Write("Tidak Memenuhi Syarat")
            pnlList.Visible = True
            pnlWorking.Visible = False
            pnlsearch.Visible = True
            DoBind(Me.SearchBy, Me.SortBy)
        End If
    End Sub

    Private Sub dtgDeskCollList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgDeskCollList.ItemDataBound

        Dim lblAgreementNo As HyperLink
        Dim lblApplicationid As Label
        Dim lblCustomerID As Label
        Dim lblCustomerName As HyperLink

        If e.Item.ItemIndex >= 0 Then

            lblAgreementNo = CType(e.Item.FindControl("hypAgreementNodtg"), HyperLink)
            lblApplicationid = CType(e.Item.FindControl("hypApplicationID"), Label)
            lblCustomerID = CType(e.Item.FindControl("lblCustomerID"), Label)
            lblCustomerName = CType(e.Item.FindControl("hypCustomerNamedtg"), HyperLink)
            lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "Collection" & "', '" & lblApplicationid.Text.Trim & "')"
            lblCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "Collection" & "', '" & lblCustomerID.Text & "')"
        End If
    End Sub

    Public Sub clearVariable()
        'lblLegalPhone1.Text = ""
        'lblLegalPhone2.Text = ""
        'lblResidencePhone1.Text = ""
        'lblResidencePhone2.Text = ""
        'lblCompanyPhone1.Text = ""
        'lblCompanyPhone2.Text = ""
        'lblMobilePhone.Text = ""
        'lblMailingPhone1.Text = ""
        'lblMailingPhone2.Text = ""
        'lblisSMSSent.Text = ""
        'lblPreviousOverDue.Text = ""
        'lblDuedate.Text = ""
        'lblPDCRequest.Text = ""
        'lblInstallmentNo.Text = ""
        ucCollActResult.ClearVariable()

        lblInstallmentAmount.Text = ""
        lblOverDueAmt.Text = ""
        lblOSInstallment.Text = ""
        lblOSBillingCharge.Text = ""
        lblOSBalance.Text = ""
        lblOSInsurance.Text = ""
        lblOSPDC.Text = ""

    End Sub


    Private Sub Previous_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonPrevious.Click
        Context.Trace.Write("Previous")
        Context.Trace.Write("Me.dtgItem = " & Me.dtgItem)
        Context.Trace.Write("dtgDeskCollList.Items.Count - 1 = " & dtgDeskCollList.Items.Count - 1)
        If Me.dtgItem < dtgDeskCollList.Items.Count - 1 And Me.dtgItem > 0 Then
            Context.Trace.Write("Memenuhi Syarat")
            cboResult.SelectedIndex = 1
            Me.dtgItem = Me.dtgItem - 1

            Dim oDataTable As New DataTable
            Dim lblAgreementNo As HyperLink
            Dim lblApplicationid As Label
            Dim lblCustomerID As Label
            Dim lblCustomerName As HyperLink

            lblAgreementNo = CType(dtgDeskCollList.Items(Me.dtgItem).FindControl("hypAgreementNodtg"), HyperLink)
            lblApplicationid = CType(dtgDeskCollList.Items(Me.dtgItem).FindControl("hypApplicationID"), Label)
            lblCustomerID = CType(dtgDeskCollList.Items(Me.dtgItem).FindControl("lblCustomerID"), Label)
            lblCustomerName = CType(dtgDeskCollList.Items(Me.dtgItem).FindControl("hypCustomerNamedtg"), HyperLink)

            Me.AgreementNo = lblAgreementNo.Text
            Me.CustomerID = lblCustomerID.Text
            Me.CustomerName = lblCustomerName.Text
            Me.ApplicationID = lblApplicationid.Text

            With oCustomClass
                .strConnection = GetConnectionString()
                .ApplicationID = lblApplicationid.Text.Trim
            End With
            oCustomClass = oController.DeskCollActivityView(oCustomClass)
            oDataTable = oCustomClass.ListDeskColl
            FillData(oDataTable)
        Else
            Context.Trace.Write("Tidak Memenuhi Syarat")
            pnlList.Visible = True
            pnlWorking.Visible = False
            pnlsearch.Visible = True
            DoBind(Me.SearchBy, Me.SortBy)
        End If

    End Sub

End Class