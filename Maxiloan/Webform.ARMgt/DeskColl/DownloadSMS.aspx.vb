﻿#Region "Imports"
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Webform.UserController
#End Region

Public Class DownloadSMS
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents txtTanggal As ucDateCE
    'Protected WithEvents oCabang As UcBranchCollection

#Region " Private Const "
    Private oCustomClass As New Parameter.DeskCollReminderActivity
    Private oController As New ReminderCallActivityController

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        'Put user code to initialize the page here
        If SessionInvalid() Then
            Exit Sub
        End If

        If Me.IsHoBranch = False Then
            NotAuthorized()
            Exit Sub
        End If

        If Not Page.IsPostBack Then
            Me.FormID = "DWNSMSREM"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                txtTanggal.Text = Format(Me.BusinessDate, "dd/MM/yyyy")
                PnlSearch.Visible = True
                pnlList.Visible = False
            End If
        End If
    End Sub

#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString
        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select

        PnlSearch.Visible = True
        BindGrid(Me.SearchBy, Me.SortBy)
        pnlList.Visible = True
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click

        If txtPage.Text = "" Then
            txtPage.Text = "0"
        Else
            If IsNumeric(txtPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                    currentPage = CType(txtPage.Text, Int32)
                    BindGrid(Me.SearchBy, Me.SortBy)

                End If
            End If
        End If

        pnlList.Visible = True

    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If

        BindGrid(Me.SearchBy, Me.SortBy)
        pnlList.Visible = True
    End Sub

#End Region



#Region " BindGrid"
    Sub BindGrid(ByVal cmdWhere As String, ByVal cmSort As String)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim oCustomClass As New Parameter.DeskCollReminderActivity

        With oCustomClass
            .strConnection = GetConnectionString()
            .BranchId = Me.BranchID
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oCustomClass = oController.DownloadSMSReminderPaging(oCustomClass)

        With oCustomClass
            lblrecord.Text = CType(.TotalRecord, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecord
        End With

        dtsEntity = oCustomClass.listdata
        dtvEntity = dtsEntity.DefaultView
        dtvEntity.Sort = Me.SortBy
        dtgSMS.DataSource = dtvEntity
        Try
            dtgSMS.DataBind()
        Catch
            dtgSMS.CurrentPageIndex = 0
            dtgSMS.DataBind()
        End Try

        PagingFooter()
    End Sub
#End Region

    Private Sub BtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReset.Click

        'With oCabang
        '    .BranchID = ""
        '    .DataBind()
        'End With
        txtTanggal.Text = ""
        cboSMSType.SelectedIndex = 0

        Me.SortBy = ""
        Me.SearchBy = "all"

        BindGrid("ALL", "")
        pnlList.Visible = True
    End Sub

    Private Sub ImbSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSearch.Click
        Me.SearchBy = ""
        Me.SortBy = ""
        Dim datefrom As String

        If CheckFeature(Me.Loginid, Me.FormID, "Srch", Me.AppId) Then
            If SessionInvalid() Then
                Exit Sub
            End If
        End If

        If txtTanggal.Text.Trim <> "" Then
            datefrom = CStr(ConvertDate2(txtTanggal.Text.Trim))
            Me.SearchBy = " MessageCreatedDate='" + datefrom + "'"
        End If
        ' If (oCabang.BranchID <> "0" And oCabang.BranchID <> "ALL") Then Me.SearchBy = Me.SearchBy + " and SMSMessageOut.BranchId='" + oCabang.BranchID.Trim + "'"
        If cboSMSType.SelectedIndex <> 0 Then Me.SearchBy = Me.SearchBy + " and SMSType='" + cboSMSType.SelectedItem.Value + "'"

        BindGrid(Me.SearchBy, Me.SortBy)
        pnlList.Visible = True
    End Sub
    Public Sub NotAuthorized()
        Dim strHTTPServer As String
        Dim StrHTTPApp As String
        Dim strNameServer As String
        strHTTPServer = Request.ServerVariables("PATH_INFO")
        strNameServer = Request.ServerVariables("SERVER_NAME")
        StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
        Response.Redirect("http://" & strNameServer & "/" & StrHTTPApp & "/error_notauthorized.aspx")
    End Sub

    Private Sub dtgSMS_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgSMS.ItemDataBound
        Dim lblAgreementNo As HyperLink
        Dim lblCustomerID As Label
        Dim lblCustomerName As HyperLink

        If e.Item.ItemIndex >= 0 Then
            lblAgreementNo = CType(e.Item.FindControl("lblAgreementNo"), HyperLink)
            lblCustomerID = CType(e.Item.FindControl("lblCustomerID"), Label)
            lblCustomerName = CType(e.Item.FindControl("lblCustomerName"), HyperLink)
            lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & lblAgreementNo.Text.Trim & "')"

            lblCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & lblCustomerID.Text & "')"
        End If
    End Sub

    Private Sub ButtonDownload_Click(sender As Object, e As System.EventArgs) Handles ButtonDownload.Click
        Dim oParam As New Parameter.DeskCollReminderActivity
        Dim dt As DataTable
        Dim adaData As Boolean = False

        With oParam
            .strConnection = GetConnectionString()
            .WhereCond = Me.SearchBy
        End With

        oParam = oController.DownloadSMSReminderList(oParam)
        dt = oParam.listdata

        If dt IsNot Nothing Then
            If dt.Rows.Count > 0 Then
                'Build the CSV file data as a Comma separated string.
                Dim csv As String = String.Empty
                adaData = True

                For Each column As DataColumn In dt.Columns
                    'Add the Header row for CSV file.
                    csv += column.ColumnName + ","c
                Next

                'Add new line.
                csv += vbCr & vbLf

                For Each row As DataRow In dt.Rows
                    For Each column As DataColumn In dt.Columns
                        'Add the Data rows.
                        csv += row(column.ColumnName).ToString().Replace(",", ";") + ","c
                    Next

                    'Add new line.
                    csv += vbCr & vbLf
                Next

                'Download the CSV file.
                Response.Clear()
                Response.Buffer = True
                Response.AddHeader("content-disposition", "attachment;filename=SMSReminder.csv")
                Response.Charset = ""
                Response.ContentType = "application/text"
                Response.Output.Write(csv)
                Response.Flush()
                Response.End()
            End If
        End If
        If adaData = True Then
            ShowMessage(lblMessage, "Data tidak ditemukan", True)
        End If
        
    End Sub


End Class