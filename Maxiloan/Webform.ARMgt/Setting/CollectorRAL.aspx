﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CollectorRAL.aspx.vb"
    Inherits="Maxiloan.Webform.ARMgt.CollectorRAL" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>CollectorRAL</title>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;

        function fConfirm() {
            if (window.confirm("Apakah yakin mau hapus data ini ? "))
                return true;
            else
                return false;
        }
        function fback() {
            history.go(-1);
            return false;
        }

        function OpenViewCollGroup(pCGID) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.ARMGT/Setting/CollectionGroupView.aspx?CGID=' + pCGID, 'InsuranceCo', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenViewAgreement(pAgreementNo) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.LoanOrg/Credit/ViewStatementofAccount.aspx?AgreementNo=' + pAgreementNo + '&Style=Collection', 'AgreementView', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenViewCustomer(pCustomerID) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.LoanOrg/Credit/ViewPersonalCustomer.aspx?CustomerID=' + pCustomerID + '&Style=Collection', 'CustomerView', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }
        function fClose() {
            window.close();
            return false;
        }	
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>    
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">           
                <h3>
                    EKSEKUTOR
                </h3>
            </div>
        </div>  
        <asp:Panel ID="pnlList" runat="server">
        <div class="form_box">
	        <div class="form_single">
                <label>Collection Group</label>
                <asp:Label ID="lblCGName" runat="server" Width="352px"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Nama Executor</label>
                <asp:Label ID="lblExecutorName" runat="server" Width="352px"></asp:Label>
	        </div>
        </div>
       <div class="form_box_title">
            <div class="form_single">              
                <h4>
                    HISTORY SURAT KUASA TARIK
                </h4>
            </div>
        </div>    
        <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgRAL" runat="server" Width="100%" OnSortCommand="Sorting"
                        CssClass="grid_general" BorderStyle="None" BorderWidth="0"
                        AutoGenerateColumns="False" AllowSorting="True">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:BoundColumn DataField="APPLICATIONID" SortExpression="APPLICATIONID" HeaderText="NO APLIKASI"></asp:BoundColumn>
                            <asp:BoundColumn DataField="CUSTOMERNAME" SortExpression="CUSTOMERNAME" HeaderText="NAMA CUSTOMER">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="INSTALLMENTNO" SortExpression="INSTALLMENTNO" HeaderText="ANGS KE"></asp:BoundColumn>
                            <%--<asp:BoundColumn DataField="RALPERIOD" SortExpression="RALPERIOD" HeaderText="PERIODE SKT">--%>
                            <asp:BoundColumn DataField="RALPERIOD" SortExpression="RALPERIOD" HeaderText="PERIODE SKE">
                            </asp:BoundColumn>
                            <%--<asp:BoundColumn DataField="RALSTATUS" SortExpression="RALSTATUS" HeaderText="STATUS SKT"></asp:BoundColumn>--%>
                            <asp:BoundColumn DataField="RALSTATUS" SortExpression="RALSTATUS" HeaderText="STATUS SKE"></asp:BoundColumn>
                            <asp:BoundColumn DataField="CANCELREASON" SortExpression="CANCELREASON" HeaderText="ALASAN BATAL">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="REPOSSESS" SortExpression="REPOSSESS" HeaderText="TARIK"></asp:BoundColumn>
                        </Columns>
                </asp:DataGrid>
            </div>    
        </div>    
        </div>   
        <div class="form_button">
            <asp:Button ID="Buttonclose" OnClientClick="fClose();"  runat="server"  Text="Close" CssClass ="small button gray">
            </asp:Button>&nbsp;
            <asp:Button ID="ButtonBack" runat="server"  Text="Back" CssClass ="small button gray">
            </asp:Button>
	    </div>       
    </asp:Panel>
    </form>
</body>
</html>