﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PemberiKuasaSKE.aspx.vb" Inherits="Maxiloan.Webform.ARMgt.PemberiKuasaSKE" %>

<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../../Webform.UserController/UcSearchBy.ascx" %>
<%@ Register TagPrefix="uc2" TagName="ucCompanyAddress" Src="../../Webform.UserController/ucCompanyAddress.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PemberiKuasaSKE</title>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <script src="../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
        onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlDtGrid" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    DAFTAR PEMBERI KUASA SKE
                </h3>
            </div>
        </div>


        <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgPemberiKuasaSKE" runat="server" Width="100%" CssClass="grid_general"
                        BorderStyle="None" BorderWidth="0" DataKeyField="EmployeeId" AutoGenerateColumns="False"
                        AllowSorting="true">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="EDIT">
                                <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbEdit" runat="server" ImageUrl="../../Images/IconEdit.gif"
                                        CommandName="EDIT"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DELETE">
                                <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbDelete" runat="server" ImageUrl="../../Images/IconDelete.gif"
                                        CommandName="DELETE"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="EmployeeID" HeaderText="ID KARYAWAN">
                                <ItemTemplate>
                                    <asp:Label ID="lblEmployeeID" Text='<%#Container.dataItem("EmployeeID")%>' runat="server">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="EmployeeName" HeaderText="NAMA KARYAWAN">
                                <ItemTemplate>
                                    <asp:Label ID="lblEmployeeName" Text='<%#Container.dataItem("EmployeeName")%>' runat="server">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="EmployeePosition" HeaderText="JABATAN">
                                <ItemTemplate>
                                    <asp:Label ID="lblEmployeePosition" Text='<%#Container.dataItem("EmployeePosition")%>' runat="server">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                    <div class="button_gridnavigation">
                        <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                            CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                            CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                            CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                            CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                        <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                            EnableViewState="False"></asp:Button>
                        <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" MinimumValue="1"
                            ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                            ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general">
                        </asp:RequiredFieldValidator>
                    </div>
                    <div class="label_gridnavigation">
                        <asp:Label ID="lblPage" runat="server"></asp:Label>of
                        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                        <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                    </div>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonAddNew" runat="server" Text="Add" CssClass="small button blue">
            </asp:Button>
<%--            <asp:Button ID="ButtonPrint" runat="server" Text="Print" CssClass="small button blue">
            </asp:Button>--%>
        </div>
        <asp:Panel ID="pnlList" runat="server">
            <div class="form_box_title">
                <div class="form_single">
                    <h4>
                        DAFTAR LOKASI
                    </h4>
                </div>
            </div>
            <div class="form_box_uc">
                <uc1:ucsearchby id="oSearchBy" runat="server"></uc1:ucsearchby>
            </div>
            <div class="form_button">
                <asp:Button ID="Buttonsearch" runat="server" Text="Search" CssClass="small button blue">
                </asp:Button>&nbsp;
                <asp:Button ID="ButtonReset" runat="server" Text="Reset" CssClass="small button gray"
                    CausesValidation="False"></asp:Button>
            </div>
        </asp:Panel>
    </asp:Panel>
    <asp:Panel ID="pnlAdd" runat="server">
        <div class="form_box_title">
            <div class="form_single">
                <h4>
                    <asp:Label ID="lblTitle" runat="server"></asp:Label>
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    Employee ID</label>
                <asp:TextBox ID="txtEmployeeId" runat="server" MaxLength="20"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvId" runat="server" CssClass="validator_general"
                    ControlToValidate="txtEmployeeId" ErrorMessage="Harap isi ID" Display="Dynamic"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    Employee Name</label>
                <asp:TextBox ID="txtEmployeeName" runat="server" Width="475px" MaxLeght="450"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvDescription" runat="server" CssClass="validator_general"
                    ControlToValidate="txtEmployeeName" ErrorMessage="Harap isi Keterangan" Display="Dynamic"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    Posisi</label>
                <asp:DropDownList ID="cboPosition" runat="server" onchange="cbopositionchange(this);">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="reqPosition" runat="server" ControlToValidate="cboPosition"
                    ErrorMessage="Harap pilih Posisi" Display="Dynamic" InitialValue="0" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonSaveAdd" runat="server" Text="Save" CssClass="small button blue"
                CausesValidation="True"></asp:Button>&nbsp;
            <asp:Button ID="ButtonCancelAdd" runat="server" Text="Cancel" CssClass="small button gray"
                CausesValidation="False"></asp:Button><a href="javascript:history.back();"></a>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
