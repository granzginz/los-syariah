﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Webform.UserController
#End Region

Public Class CollectionGroupView
    Inherits Maxiloan.Webform.WebBased

    Dim m_CollGroup As New CollGroupController


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If sessioninvalid() Then
            Exit Sub
        End If
        If Not Page.IsPostBack Then
            lblCGID.Text = Request.QueryString("CGID")

            viewbyID(lblCGID.Text)
            PnlView.Visible = True
            ButtonClose.Attributes.Add("onclick", "windowClose()")
        End If
    End Sub

    Private Sub viewbyID(ByVal CGID As String)

        Dim CollGroup As New Parameter.CollGroup
        Dim dtInsCo As New DataTable

        With CollGroup
            .strConnection = getConnectionString
            .CGID = CGID
        End With

        Try

            CollGroup = m_CollGroup.GetCollGroupByID(CollGroup)
            dtInsCo = CollGroup.ListData

            'Isi semua field Edit Action

            lblCGName.Text = CStr(dtInsCo.Rows(0).Item("CGName"))
            lblCGHead.Text = CStr(dtInsCo.Rows(0).Item("CGHeadName"))

            lblAddress.Text = CStr(dtInsCo.Rows(0).Item("Address"))
            lblRT.Text = CStr(dtInsCo.Rows(0).Item("RT"))
            lblRW.Text = CStr(dtInsCo.Rows(0).Item("RW"))
            lblKelurahan.Text = CStr(dtInsCo.Rows(0).Item("Kelurahan"))
            lblKecamatan.Text = CStr(dtInsCo.Rows(0).Item("Kecamatan"))
            lblCity.Text = CStr(dtInsCo.Rows(0).Item("City"))
            lblZipCode.Text = CStr(dtInsCo.Rows(0).Item("ZipCode"))
            lblPhone1.Text = CStr(dtInsCo.Rows(0).Item("AreaPhone1")) + "-" + CStr(dtInsCo.Rows(0).Item("PhoneNo1"))
            lblPhone2.Text = CStr(dtInsCo.Rows(0).Item("AreaPhone2")) + "-" + CStr(dtInsCo.Rows(0).Item("PhoneNo2"))
            lblFax.Text = CStr(dtInsCo.Rows(0).Item("AreaFax")) + "-" + CStr(dtInsCo.Rows(0).Item("FaxNo"))

            lblCPName.Text = CStr(dtInsCo.Rows(0).Item("ContactPerson"))
            lblCPTitle.Text = CStr(dtInsCo.Rows(0).Item("ContactPersonTitle"))
            lblMobilePhone.Text = CStr(dtInsCo.Rows(0).Item("ContactPersonMobilePhone"))
            lblCPEmail.Text = CStr(dtInsCo.Rows(0).Item("ContactPersonEmail"))


            lblCGName.Visible = True
            lblCGHead.Visible = True

            lblAddress.Visible = True
            lblRT.Visible = True
            lblRW.Visible = True
            lblKelurahan.Visible = True
            lblKecamatan.Visible = True
            lblCity.Visible = True
            lblZipCode.Visible = True
            lblPhone1.Visible = True
            lblPhone2.Visible = True
            lblFax.Visible = True

            lblCPName.Visible = True
            lblCPTitle.Visible = True
            lblMobilePhone.Visible = True
            lblCPEmail.Visible = True

        Catch ex As Exception

        End Try

    End Sub

End Class