﻿#Region "imports"
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class CollDesk
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents UCEmployee As UcFindEmployee
    Protected WithEvents UCBranchCollection1 As UcBranchCollection
    Protected WithEvents UcCG As UcBranchCollection

    Dim dtCollector As New DataTable


#Region " Private Const "
    Dim m_CollZipCode As New CollZipCodeController
    Dim m_Coll As New CollectorController

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

#End Region

#Region " Property "

    Private Property DefaultCollector() As String
        Get
            Return CStr(ViewState("DefaultCollector"))
        End Get
        Set(ByVal Value As String)
            ViewState("DefaultCollector") = Value
        End Set
    End Property

    Private Property BindMenu() As String
        Get
            Return CStr(ViewState("BindMenu"))
        End Get
        Set(ByVal Value As String)
            ViewState("BindMenu") = Value
        End Set
    End Property

    Private Property CGID() As String
        Get
            Return CStr(ViewState("CGID"))
        End Get
        Set(ByVal Value As String)
            ViewState("CGID") = Value
        End Set
    End Property

    Private Property City() As String
        Get
            Return CStr(ViewState("City"))
        End Get
        Set(ByVal Value As String)
            ViewState("City") = Value
        End Set
    End Property

    Private Property CGName() As String
        Get
            Return CStr(ViewState("CGName"))
        End Get
        Set(ByVal Value As String)
            ViewState("CGName") = Value
        End Set
    End Property

#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False        
        If SessionInvalid() Then
            Exit Sub
        End If
        lblDefaultCollector.Visible = False        
        If Not Page.IsPostBack Then
            Me.CGID = Me.GroubDbID
            Me.FormID = "DESKCOLL"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                InitialDefaultPanel()
                Me.SearchBy = " CollectorType='CL' and CGID='" & sesBranchId.Replace("'", "") & "'"
                Me.SortBy = " CollectorID ASC"
                getCollectorCombo()
            End If
        End If
    End Sub
#Region " PanelAllFalse "
    Private Sub PanelAllFalse()        
        PnlSearchDetail.Visible = False
        pnlList.Visible = False
    End Sub
#End Region
#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            lblTotPage.Text = "1"
            'rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            'rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select

        PnlSearchDetail.Visible = True
        BindGrid()
        pnlList.Visible = True
    End Sub

    Protected Sub btnPageNumb_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPageNumb.Click

        If txtPage.Text = "" Then
            txtPage.Text = "0"
        Else
            If IsNumeric(txtPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then                    
                    currentPage = CType(txtPage.Text, Int32)
                    BindGrid()
                End If
            End If
        End If

        pnlList.Visible = True

    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If

        If Me.CGID = "" Then Me.CGID = Me.GroubDbID

        BindGrid()        
        pnlList.Visible = True
        PnlSearchDetail.Visible = True
    End Sub

#End Region
#Region " InitialDefaultPanel "
    Private Sub InitialDefaultPanel()
        PnlSearch.Visible = True
        PnlSearchDetail.Visible = False
        pnlList.Visible = False
    End Sub
#End Region
#Region " BindGrid"
    Sub BindGrid()
        Dim cPaging As New GeneralPagingController
        Dim oPaging As New Parameter.GeneralPaging
        Dim dtEntity As System.Data.DataTable = Nothing

        With oPaging
            .strConnection = GetConnectionString()
            .WhereCond = SearchBy
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
            .SpName = "spCollectorDeskCollPaging"
        End With

        oPaging = cPaging.GetGeneralPaging(oPaging)

        If Not oPaging Is Nothing Then
            dtEntity = oPaging.ListData
            recordCount = oPaging.TotalRecords
        Else
            recordCount = 0
        End If

        dtgColl.DataSource = dtEntity.DefaultView
        dtgColl.CurrentPageIndex = 0
        dtgColl.DataBind()
        PagingFooter()
    End Sub
#End Region
    Private Sub BtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSearch.Click

        If CheckFeature(Me.Loginid, Me.FormID, "Srch", Me.AppId) Then
            If SessionInvalid() Then
                Exit Sub
            End If
        End If

        With UcCG
            Me.CGID = .BranchID
            Me.CGName = .BranchName
        End With

        If UcCG.BranchID <> "0" Then
            PanelAllFalse()
            BindGrid()

            pnlList.Visible = True

            getCollectorCombo()                       

            PnlSearchDetail.Visible = True            
        End If

    End Sub
    Private Sub ButtonReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        InitialDefaultPanel()
        Me.SearchBy = " CollectorType='CL' and CGID='" & sesBranchId.Replace("'", "") & "'"
        Me.SortBy = " CollectorID ASC"        
    End Sub
    Private Sub ButtonCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        InitialDefaultPanel()
        Me.SearchBy = " CollectorType='CL' and CGID='" & sesBranchId.Replace("'", "") & "'"
        Me.SortBy = " CollectorID ASC"
    End Sub
    Private Sub dtgColl_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgColl.ItemDataBound
        Dim cbo As New DropDownList
        If e.Item.ItemIndex >= 0 Then
            cbo = CType(e.Item.FindControl("cboDeskColl"), DropDownList)

            Dim oCollList As New Parameter.CollZipCode            
            Dim oColl As New Parameter.Collector
            Dim dt As New DataTable

            With oCollList
                .strConnection = GetConnectionString()
                .CGID = Me.CGID
                .CollectorType = "D"
            End With

            dt = m_CollZipCode.GetCollectorCombo(oCollList)

            With oColl
                .strConnection = GetConnectionString()
                .CGID = e.Item.Cells(3).Text.Trim
                .CollectorID = e.Item.Cells(2).Text.Trim
            End With

            oColl = m_Coll.GetCollectorDeskCollByID(oColl)

            cbo.DataSource = dt
            cbo.DataTextField = "collectorName"
            cbo.DataValueField = "collectorID"
            cbo.DataBind()

            cbo.Items.Insert(0, "Select One")
            cbo.Items(0).Value = ""


            If oColl.ListData.Rows.Count > 0 Then
                cbo.SelectedIndex = cbo.Items.IndexOf(cbo.Items.FindByValue(oColl.ListData.Rows(0).Item("DeskCollID").ToString.Trim))
            End If


        End If

    End Sub
    Private Sub getCollectorCombo()
        Dim oColl As New Parameter.CollZipCode
        Dim strconn As String = GetConnectionString()
        Dim dt As New DataTable

        With oColl
            .strConnection = GetConnectionString()
            .CGID = Me.CGID
            .CollectorType = "D"
        End With

        dt = m_CollZipCode.GetCollectorCombo(oColl)
        cboDefaultCollector.DataSource = dt
        cboDefaultCollector.DataTextField = "CollectorName"
        cboDefaultCollector.DataValueField = "CollectorID"
        cboDefaultCollector.DataBind()

        cboDefaultCollector.Items.Insert(0, "Select One")
        cboDefaultCollector.Items(0).Value = "0"

        dtCollector = dt
    End Sub
    Private Sub imbSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
        SavePage()
    End Sub
    Private Sub SavePage()
        Dim oColl As New Parameter.Collector

        Dim strCGID As String
        Dim strCollectorID As String
        Dim cboColl As New DropDownList


        For index = 0 To dtgColl.Items.Count - 1
            strCGID = CType(dtgColl.Items(index).Cells(3).Text.Trim, String)
            strCollectorID = CType(dtgColl.Items(index).Cells(2).Text.Trim, String)
            cboColl = CType(dtgColl.Items(index).Cells(1).FindControl("cboDeskColl"), DropDownList)

            With oColl
                .CGID = strCGID.Trim
                .CollectorID = strCollectorID.Trim
                .DeskCollID = cboColl.SelectedValue.Trim
                .strConnection = GetConnectionString()
            End With
            Try
                m_Coll.CollectorDeskCollSave(oColl)
                ShowMessage(lblMessage, "Update data Berhasil ", False)
            Catch ex As Exception
                ShowMessage(lblMessage, ex.Message, True)
            End Try

        Next
        BindGrid()
        pnlList.Visible = True        
        PnlSearchDetail.Visible = True
    End Sub    
    Private Sub imbApply_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonApply.Click
        If cboDefaultCollector.SelectedIndex <= 0 Then
            lblDefaultCollector.Visible = True
            Exit Sub
        End If

        If cboDefaultCollector.SelectedItem.Value <> "0" Then

            Dim cboDeskColl As New DropDownList            


            If CBool(cboCriteria.SelectedValue.Trim) Then
                For index = 0 To dtgColl.Items.Count - 1
                    cboDeskColl = CType(dtgColl.Items(index).Cells(3).FindControl("cboDeskColl"), DropDownList)

                    If cboDeskColl.SelectedValue = "" Then
                        cboDeskColl.SelectedIndex = cboDeskColl.Items.IndexOf(cboDeskColl.Items.FindByValue(cboDefaultCollector.SelectedItem.Value))
                    End If
                Next
            Else
                For index = 0 To dtgColl.Items.Count - 1
                    cboDeskColl = CType(dtgColl.Items(index).Cells(3).FindControl("cboDeskColl"), DropDownList)
                    cboDeskColl.SelectedIndex = cboDeskColl.Items.IndexOf(cboDeskColl.Items.FindByValue(cboDefaultCollector.SelectedItem.Value))
                Next
            End If

            Me.DefaultCollector = cboDefaultCollector.SelectedItem.Value

            pnlList.Visible = True
            PnlSearchDetail.Visible = True            

        End If
    End Sub    
End Class