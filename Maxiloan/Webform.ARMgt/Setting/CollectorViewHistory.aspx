﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CollectorViewHistory.aspx.vb"
    Inherits="Maxiloan.Webform.ARMgt.CollectorViewHistory" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>CollectorView</title>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;

        function windowClose() {
            window.close();
        }
        function fback() {
            history.go(-1);
            return false;
        }
        function click() {
            if (event.button == 2) {
                alert('Anda Tidak Berhak');
            }
        }
        document.onmousedown = click

        function OpenViewRAL(pCGID, pCollectorID) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.ARMgt/Setting/CollectorRAL.aspx?CGID=' + pCGID + '&CollectorID=' + pCollectorID, 'RALView', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Panel ID="PnlView" runat="server" Visible="False">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    VIEW - MOU HISTORY</h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    ID EKSEKUTOR</label>
                <asp:Label ID="lblCollectorID" runat="server" Width="184px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    NAMA KOLEKTOR</label>
                <asp:Label ID="lblNamaCollector" runat="server" Width="456px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    COLLECTION GROUP</label>
                <asp:Label ID="lblCollectionGroup" runat="server" Width="448px"></asp:Label>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid runat="server" ID="dtgMOU" CssClass="grid_general" Width="100%" BorderStyle="None"
                        BorderWidth="0" AutoGenerateColumns="false" OnSortCommand="Sorting" AllowSorting="true">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="NO">
                                <ItemStyle CssClass="command_col"></ItemStyle>
                                 <ItemTemplate>
                                        <asp:Label ID="lblNo" runat="server"></asp:Label>
                                    </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="NoMOU" SortExpression="NoMOU" HeaderText="NO MOU"></asp:BoundColumn>
                            <asp:BoundColumn DataField="TglMOU" SortExpression="TglMOU" HeaderText="TANGGAL MOU"  DataFormatString="{0:dd/MM/yyyy}" >
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="TglExpiredMOU" SortExpression="TglExpiredMOU" HeaderText="TANGGAL EXPIRED MOU"  DataFormatString="{0:dd/MM/yyyy}">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="Keterangan" SortExpression="Keterangan" HeaderText="KETERANGAN">
                            </asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <div class="button_gridnavigation">
                        <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                            CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                            CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                            CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                            CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                        <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                            EnableViewState="False"></asp:Button>
                        <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" MinimumValue="1"
                            ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                            ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general">
                        </asp:RequiredFieldValidator>
                    </div>
                    <div class="label_gridnavigation">
                        <asp:Label ID="lblPage" runat="server"></asp:Label>of
                        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                        <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlClose" runat="server">
        <div class="form_button">
            <asp:Button ID="ButtonClose" runat="server" Text="Close" CssClass="small button gray"
                CausesValidation="true"></asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
