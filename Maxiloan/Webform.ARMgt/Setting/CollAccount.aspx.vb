﻿#Region "imports"
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class CollAccount
    Inherits Maxiloan.Webform.WebBased

#Region " Const "
    Dim m_Coll As New CollectorController
    Protected WithEvents UcCG As UcBranchCollection
    Protected WithEvents UcMaxAccCollList As UcMaxAccCollList
    Protected WithEvents txtMaksimalKontrak As ucNumberFormat
#End Region
#Region " Property "    
    Private Property CGID() As String
        Get
            Return CStr(ViewState("CGID"))
        End Get
        Set(ByVal Value As String)
            ViewState("CGID") = Value
        End Set
    End Property        
    Private Property CGName() As String
        Get
            Return CStr(ViewState("CGName"))
        End Get
        Set(ByVal Value As String)
            ViewState("CGName") = Value
        End Set
    End Property
    Public Property ZipCode() As String
        Get
            Return CStr(ViewState("ZipCode"))
        End Get
        Set(ByVal Value As String)
            ViewState("ZipCode") = Value
        End Set
    End Property

#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False        
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not Me.IsPostBack Then            
            Me.FormID = "MAXACCCOLL"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                DefaultPanelFalse()
                PnlSearch.Visible = True
                With txtMaksimalKontrak                    
                    .RangeValidatorEnable = False
                    .RequiredFieldValidatorEnable = False
                    '.RangeValidatorMaximumValue = GetValue.ToString
                    '.RangeValidatorMinimumValue = "0"
                End With
                
            End If
        End If
    End Sub
    Sub DefaultPanelFalse()
        pnlAdd.Visible = False
        pnlList.Visible = False
        PnlSearch.Visible = False
    End Sub

    Private Function GetValue() As Integer        
        Dim generalSet As New Parameter.GeneralSetting
        Dim conGeneral As New GeneralSettingController
        Dim value As Integer = 0
        Dim status As Boolean = True

        generalSet.strConnection = GetConnectionString()
        generalSet.GSID = "MAXACCCOLL"

        generalSet = conGeneral.GetGeneralSettingByID(generalSet)

        If generalSet.ListData.Rows.Count > 0 Then
            value = CInt(generalSet.ListData.Rows(0).Item("GSValue").ToString.Trim)
        End If

        Return value
    End Function

    Private Sub ButtonSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSearch.Click
        bindData
    End Sub

    Sub bindData()
        DefaultPanelFalse()
        PnlSearch.Visible = True
        pnlList.Visible = True        
        UcMaxAccCollList.WhereCond = " CGID = '" & UcCG.BranchID & "'"
        UcMaxAccCollList.Sort = "CollectorID ASC"
        UcMaxAccCollList.DoBind()
    End Sub
    Private Sub UcMaxAccCollList_Click(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles UcMaxAccCollList.EventEdit
        DefaultPanelFalse()
        pnlAdd.Visible = True
        lblCollectorID.Text = e.Item.Cells(1).Text
        lblNamaCollector.Text = e.Item.Cells(2).Text
        txtMaksimalKontrak.Text = e.Item.Cells(3).Text
        CGID = e.Item.Cells(4).Text
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim oColl As New Parameter.Collector

        Try
            With oColl
                .CGID = CGID.Trim
                .CollectorID = lblCollectorID.Text.Trim
                .MaksimalKontrak = CInt(txtMaksimalKontrak.Text)
                .strConnection = GetConnectionString()
            End With

            m_Coll.CollectorEditMaxAcc(oColl)
            ShowMessage(lblMessage, "Update data Berhasil ", False)
            bindData()
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
        
    End Sub
    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        bindData()
    End Sub
End Class