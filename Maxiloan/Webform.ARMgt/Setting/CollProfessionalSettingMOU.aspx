﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CollProfessionalSettingMOU.aspx.vb"
    Inherits="Maxiloan.Webform.ARMgt.CollProfessionalSettingMOU" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>CollProfessionalSettingMOU</title>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
</head>
<body>
    <script language="javascript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;

        function fConfirm() {
            if (window.confirm("Apakah yakin mau hapus data ini ? "))
                return true;
            else
                return false;
        }
        function fback() {
            history.go(-1);
            return false;
        }

        function OpenViewCollectionGroup(pCGID) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.ARMgt/Setting/CollectionGroupView.aspx?CGID=' + pCGID, 'CollectionGroupView', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenViewHistory(pBranchID, pCollID, pNoMOU) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.ARMgt/Setting/CollectorViewHistory.aspx?CollectorID=' + pCollID + '&BranchID=' + pBranchID + '&NoMOU=' + pNoMOU, 'CollectorViewHistory', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }


    </script>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlAddEdit" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    MOU PROFESIONAL KOLEKTOR
                </h3>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <h4>
                    <asp:Label ID="lblTitle" runat="server"></asp:Label>
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    NO MOU
                </label>
                <asp:TextBox ID="txtNoMOU" runat="server"  MaxLength="50">
                </asp:TextBox>
                <%--<asp:Label ID="lblbintang2" runat="server" Width="11px" ForeColor="Red"></asp:Label>--%>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" CssClass="validator_general"
                    ControlToValidate="txtNoMOU" ErrorMessage="Harap isi NO MOU ">
                </asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Tanggal MOU</label>
                <asp:TextBox ID="txtTglMOU" runat="server" CssClass="small_text">
                </asp:TextBox>
                <asp:CalendarExtender ID="StartDate_CalendarExtender" runat="server" Enabled="True"
                    TargetControlID="txtTglMOU" Format="dd/MM/yyyy">
                </asp:CalendarExtender>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Tanggal MOU Expired</label>
                <asp:TextBox ID="txtTglMOUExpired" runat="server" CssClass="small_text">
                </asp:TextBox>
                <asp:CalendarExtender ID="StartDate_CalendarExtender2" runat="server" Enabled="True"
                    TargetControlID="txtTglMOUExpired" Format="dd/MM/yyyy">
                </asp:CalendarExtender>
            </div>
        </div>
        <asp:Panel ID="pnlExtend" runat="server">
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Tanggal MOU Expired Baru</label>
                    <asp:TextBox ID="txtTglMOUExpiredNew" runat="server" CssClass="small_text">
                    </asp:TextBox>
                    <asp:CalendarExtender ID="StartDate_CalendarExtender3" runat="server" Enabled="True"
                        TargetControlID="txtTglMOUExpiredNew" Format="dd/MM/yyyy">
                    </asp:CalendarExtender>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label class="label_general">
                        Keterangan</label>
                    <asp:TextBox ID="txtKet" runat="server" TextMode="MultiLine" CssClass="multiline_textbox">
                    </asp:TextBox>
                </div>
            </div>
        </asp:Panel>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlSaveCancel">
        <div class="form_button">
            <asp:Button ID="ButtonSave" runat="server" Text="Save" CssClass="small button blue">
            </asp:Button>&nbsp;
            <asp:Button ID="ButtonCancel" runat="server" Text="Cancel" CssClass="small button gray"
                CausesValidation="False"></asp:Button>&nbsp; <a href="javascript:history.back();">
            </a>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    PROFESIONAL EKSEKUTOR
                </h3>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid runat="server" ID="dtgMOU" CssClass="grid_general" Width="100%" BorderStyle="None"
                        BorderWidth="0" AutoGenerateColumns="false" OnSortCommand="Sorting" AllowSorting="true">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="EDIT">
                                <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImbEdit" runat="server" ImageUrl="../../Images/iconedit.gif"
                                        CommandName="Edit"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DELETE">
                                <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImbDelete" runat="server" ImageUrl="../../Images/icondelete.gif"
                                        CommandName="Delete" CausesValidation="False"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="EXTEND">                                
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkEXTEND" runat="server" CommandName="EXTEND" CausesValidation="False">EXTEND</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="HISTORY">                                
                                <ItemTemplate>                                    
                                    <asp:HyperLink runat="server" Enabled="True" Text="HISTORY" ID="lnkHISTORY" NavigateUrl='<%# LinkTo(container.dataItem("BranchID"),container.dataItem("CollectorID"),container.dataItem("NoMOU")) %>'>                                    
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="NoMOU" SortExpression="NoMOU" HeaderText="NO MOU"></asp:BoundColumn>
                            <asp:BoundColumn DataField="TglMOU" SortExpression="TglMOU" HeaderText="TANGGAL MOU"
                                DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="TglExpiredMOU" SortExpression="TglExpiredMOU" HeaderText="TANGGAL EXPIRED MOU"
                                DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <div class="button_gridnavigation">
                        <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                            CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                            CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                            CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                            CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                        <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                            EnableViewState="False"></asp:Button>
                        <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" MinimumValue="1"
                            ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general">
                        </asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                            ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general">
                        </asp:RequiredFieldValidator>
                    </div>
                    <div class="label_gridnavigation">
                        <asp:Label ID="lblPage" runat="server"></asp:Label>of
                        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                        <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlAddCancel">
        <div class="form_button">
            <asp:Button ID="imbButtonAdd" runat="server" CausesValidation="False" Text="Add"
                CssClass="small button blue"></asp:Button>
            <asp:Button ID="imbButtonCancel" runat="server" CausesValidation="false" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
