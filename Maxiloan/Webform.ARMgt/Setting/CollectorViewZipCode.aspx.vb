﻿#Region "imports"
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class CollectorViewZipCode
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents UCCompanyAdress1 As UcCompanyAddress
    Protected WithEvents UCContactPerson1 As UcContactPerson
    Protected WithEvents UCbranch1 As UcBranch

#Region " Private Const "
    Dim m_Collector As New CollectorController

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

#End Region

#Region " Property "

    Private Property CallerPage() As String
        Get
            Return CStr(viewstate("CallerPage"))
        End Get
        Set(ByVal Value As String)
            viewstate("CallerPage") = Value
        End Set
    End Property

    Private Property CGID() As String
        Get
            Return CStr(viewstate("CGID"))
        End Get
        Set(ByVal Value As String)
            viewstate("CGID") = Value
        End Set
    End Property

    Private Property CollectorID() As String
        Get
            Return CStr(viewstate("CollectorID"))
        End Get
        Set(ByVal Value As String)
            viewstate("CollectorID") = Value
        End Set
    End Property


    Private Property CollectorName() As String
        Get
            Return CStr(viewstate("CollectorName"))
        End Get
        Set(ByVal Value As String)
            viewstate("CollectorName") = Value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not Me.IsPostBack Then
            InitialDefaultPanel()
            Me.FormID = "VWZIP"

            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If Request.QueryString("CGID") <> "" Then Me.CGID = Request.QueryString("CGID")
                If Request.QueryString("CollectorID") <> "" Then Me.CollectorID = Request.QueryString("CollectorID")
                If Request.QueryString("CollectorName") <> "" Then Me.CollectorName = Request.QueryString("CollectorName")
                If Request.QueryString("CallerPage") <> "" Then Me.CallerPage = Request.QueryString("CallerPage")
                Me.SearchBy = "all"
                Me.SortBy = ""
                BindGridZipCode(Me.CGID, Me.CollectorID, Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub

#Region " PanelAllFalse "
    Private Sub PanelAllFalse()        
        pnlList.Visible = False
    End Sub
#End Region

#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridZipCode(Me.CGID, Me.CollectorID, Me.SearchBy, Me.SortBy)
    End Sub

    Protected Sub btnPageNumb_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPageNumb.Click

        If txtPage.Text = "" Then
            txtPage.Text = "0"
        Else
            If IsNumeric(txtPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                    currentPage = CType(txtPage.Text, Int32)
                    BindGridZipCode(Me.CGID, Me.CollectorID, Me.SearchBy, Me.SortBy)
                End If
            End If
        End If


    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        Me.SearchBy = ""
        BindGridZipCode(Me.CGID, Me.CollectorID, Me.SearchBy, Me.SortBy)

    End Sub

#End Region

#Region " InitialDefaultPanel "
    Private Sub InitialDefaultPanel()
        pnlList.Visible = True
    End Sub
#End Region

#Region " BindGrid"
    Sub BindGridZipCode(ByVal CGID As String, ByVal CollectorID As String, ByVal cmdWhere As String, ByVal cmSort As String)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim oCollector As New Parameter.Collector
        Dim oCollectorList As New Parameter.Collector

        lblCollectorName.Text = Me.CollectorName

        With oCollector
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
            .CGID = Me.CGID
            .CollectorID = Me.CollectorID
        End With


        oCollectorList = m_Collector.GetCollectorZipCode(oCollector)

        With oCollectorList
            lblrecord.Text = CType(.TotalRecord, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecord
        End With

        dtsEntity = oCollectorList.ListData
        dtvEntity = dtsEntity.DefaultView
        dtvEntity.Sort = Me.SortBy
        dtgZipCode.DataSource = dtvEntity
        Try
            dtgZipCode.DataBind()
        Catch
            dtgZipCode.CurrentPageIndex = 0
            dtgZipCode.DataBind()
        End Try

        PagingFooter()
    End Sub
#End Region

    Private Sub imbBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonBack.Click
        Response.Redirect(Me.CallerPage & "?CGID=" & Me.CGID)
    End Sub

End Class