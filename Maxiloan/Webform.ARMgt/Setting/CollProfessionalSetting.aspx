﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CollProfessionalSetting.aspx.vb"
    Inherits="Maxiloan.Webform.ARMgt.CollProfessionalSetting" %>

<%@ Register TagPrefix="uc1" TagName="UcBranchCollection" Src="../../Webform.UserController/UcBranchCollection.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucCompanyAddress" Src="../../Webform.UserController/ucCompanyAddress.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcBankAccount" Src="../../Webform.UserController/UcBankAccount.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>CollProfessionalSetting</title>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <script src="../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;

        function fConfirm() {
            if (window.confirm("Apakah yakin mau hapus data ini ? "))
                return true;
            else
                return false;
        }
        function fback() {
            history.go(-1);
            return false;
        }

        function OpenViewCollectionGroup(pCGID) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.ARMgt/Setting/CollectionGroupView.aspx?CGID=' + pCGID, 'CollectionGroupView', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenViewCollector(pCollID, pCGID) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.ARMgt/Setting/CollectorView.aspx?CollectorID=' + pCollID + '&CGID=' + pCGID, 'CollectorView', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }
        function showimagepreview(input, imgClient) {
            if (input.files && input.files[0]) {
                var filerdr = new FileReader();
                filerdr.onload = function (e) {
                    $('#' + imgClient).attr('src', e.target.result);
                }
                filerdr.readAsDataURL(input.files[0]);
            }
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
        onclick="hideMessage();"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                DAFTAR EKSEKUTOR
            </h3>
        </div>
    </div>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgCollector" runat="server" Width="100%" OnSortCommand="Sorting"
                        CssClass="grid_general" BorderStyle="None" BorderWidth="0" AutoGenerateColumns="False"
                        AllowSorting="True">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="EDIT">
                                <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImbEdit" runat="server" ImageUrl="../../Images/iconedit.gif"
                                        CommandName="Edit"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DELETE">
                                <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImbDelete" runat="server" ImageUrl="../../Images/icondelete.gif"
                                        CommandName="Delete" CausesValidation="False"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="CGID" HeaderText="COLLECTION GROUP">
                                <ItemTemplate>
                                    <asp:HyperLink runat="server" Enabled="True" ID="hpCGID" Text='<%# container.dataitem("CGName") %>'
                                        NavigateUrl='<%# LinkTo(container.dataItem("CGID")) %>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn Visible="False" DataField="CGID" SortExpression="CGID" HeaderText="ID COLL GROUP">
                            </asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="CollectorID" HeaderText="ID EKSEKUTOR">
                            </asp:BoundColumn>
                            <asp:TemplateColumn SortExpression="CollectorID" HeaderText="ID EKSEKUTOR">
                                <ItemTemplate>
                                    <asp:HyperLink runat="server" Enabled="True" ID="hpCollector" Text='<%# container.dataitem("CollectorID") %>'
                                        NavigateUrl='<%# LinkToCollector(container.dataItem("CollectorID"),container.dataItem("CGID")) %>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="CollectorName" SortExpression="CollectorName" HeaderText="NAMA EKSEKUTOR">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="PhoneNo" SortExpression="PhoneNo" HeaderText="NO TELEPON">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="MOBILEPHONE" SortExpression="MOBILEPHONE" HeaderText="NO HANDPHONE">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="Active" SortExpression="Active" HeaderText="AKTIF"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="MOU">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkBankAcc" runat="server" CommandName="MOU" CausesValidation="False">MOU</asp:LinkButton>
                                    <asp:Label runat="server" Enabled="True" Visible="false" ID="BankID" Text='<%# DataBinder.eval(Container,"DataItem.BankID") %>' />
                                    <asp:Label runat="server" Enabled="True" Visible="false" ID="BankBranchID" Text='<%# DataBinder.eval(Container,"DataItem.BankBranchID") %>' />
                                    <asp:Label runat="server" Enabled="True" Visible="false" ID="AccountName" Text='<%# dataBinder.eval(Container,"DataItem.AccountName")%>' />
                                    <asp:Label runat="server" Enabled="True" Visible="false" ID="AccountNo" Text='<%# dataBinder.eval(Container,"DataItem.AccountNo") %>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                    <div class="button_gridnavigation">
                        <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                            CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                            CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                            CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                            CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                        <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                            EnableViewState="False"></asp:Button>
                        <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" MinimumValue="1"
                            ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                            ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general">
                        </asp:RequiredFieldValidator>
                    </div>
                    <div class="label_gridnavigation">
                        <asp:Label ID="lblPage" runat="server"></asp:Label>of
                        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                        <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                    </div>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonAdd" runat="server" Text="Add" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="ButtonPrint" runat="server" Text="Print" CssClass="small button blue">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlSearch" runat="server">
        <div class="form_box_title">
            <div class="form_single">
                <h4>
                    EKSEKUTOR
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Collection Group</label>
                <uc1:ucbranchcollection id="UcBranchCollection1" runat="server"></uc1:ucbranchcollection>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cari Berdasarkan</label>
                <asp:DropDownList ID="cboSearchBy" runat="server" Width="144px">
                    <asp:ListItem Value="CollectorID">ID Eksekutor</asp:ListItem>
                    <asp:ListItem Value="CollectorName">Nama Eksekutor</asp:ListItem>
                </asp:DropDownList>
                &nbsp;
                <asp:TextBox ID="TxtSearchByValue" runat="server" Width="168px"></asp:TextBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonSearch" runat="server" Text="Search" CssClass="small button blue">
            </asp:Button>&nbsp;
            <asp:Button ID="ButtonReset" runat="server" Text="Reset" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlAddEdit" runat="server">
        <div class="form_box_title">
            <div class="form_single">
                <h4>
                    EKSEKUTOR -&nbsp;
                    <asp:Label ID="lblMenuAddEdit" runat="server"></asp:Label>
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Collection Group</label>
                <asp:Label ID="lblCollGroup" runat="server" Width="376px" ForeColor="#404040"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    ID Eksekutor
                </label>
                <asp:Label ID="lblExecutorID" runat="server" Width="89px" ForeColor="Black"></asp:Label>
                <asp:TextBox ID="txtExecutorID" runat="server" Width="120px" MaxLength="12"></asp:TextBox>&nbsp;
                <asp:Label ID="lblbintang1" runat="server" Width="11px" ForeColor="Red"></asp:Label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" CssClass="validator_general"
                    ControlToValidate="txtExecutorID" ErrorMessage="Harap isi ID Eksekutor"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Nama Eksekutor
                </label>
                <asp:TextBox ID="txtExecutorName" runat="server" MaxLength="50"></asp:TextBox>
                <asp:Label ID="lblExecutorName" runat="server" Width="88px" ForeColor="Black"></asp:Label>
                <asp:Label ID="lblbintang2" runat="server" Width="11px" ForeColor="Red"></asp:Label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" CssClass="validator_general"
                    ControlToValidate="txtExecutorName" ErrorMessage="Harap isi Nama Eksekutor"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Jenis Collector</label>
                <label>
                    Eksekutor</label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Supervisor</label>
                <asp:DropDownList ID="cboSupervisor" runat="server">
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_general">
                    Status</label>
                    <asp:RadioButtonList ID="rbnCollectorStatus" runat="server" class="opt_single"
                            RepeatDirection="Horizontal">
                            <asp:ListItem Value="N" Selected="True">Normal</asp:ListItem>
                            <asp:ListItem Value="W">Warning</asp:ListItem>
                            <asp:ListItem Value="B">Bad</asp:ListItem>
                    </asp:RadioButtonList>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <h4>
                    ALAMAT
                </h4>
            </div>
        </div>
        <div class="form_box_uc">
            <uc1:uccompanyaddress id="oUcCompanyAdress" runat="server"></uc1:uccompanyaddress>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    No HandPhone</label>
                <asp:TextBox ID="txtMobilePhone" runat="server" onkeypress="return numbersonly2(event)"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    e-Mail</label>
                <asp:TextBox ID="txtEmail" runat="server" Width="288px"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_general">
                    Catatan</label>
                <asp:TextBox ID="txtNotes" runat="server" CssClass="multiline_textbox" TextMode="MultiLine"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Active</label>
                <asp:CheckBox ID="cbActive" runat="server" BorderStyle="None"></asp:CheckBox>
            </div>
        </div>
        <div class="form_box_uc">
            <uc1:ucbankaccount id="UcBankAccount" runat="server"></uc1:ucbankaccount>
        </div>
        <div class="form_box">
            <%--edit npwp, ktp, telepon by ario--%>
            <div class="form_single">
                <label class="label_req">
                    No NPWP
                </label>
                <asp:TextBox runat="server" ID="txtNoNPWP" Width="200px" MaxLength="35" onkeypress="return numbersonly2(event)"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidatornpwp" runat="server"
                    ControlToValidate="txtNoNPWP" ErrorMessage="Harap isi dengan Angka" ValidationExpression="\d*"
                    CssClass="validator_general">
                </asp:RegularExpressionValidator>
                 <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtNoNPWP"
                ErrorMessage="Harap isi NPWP" Display="Dynamic" CssClass ="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    No KTP
                </label>
                <asp:TextBox runat="server" ID="txtNoKTP" Width="200px" MaxLength="50" onkeypress="return numbersonly2(event)"></asp:TextBox>
                 <asp:RegularExpressionValidator ID="Regularexpressionvalidator1" runat="server" Display="Dynamic"
                ValidationExpression="[0-9]{0,4}" ErrorMessage="Harap isi dengan Angka"
                ControlToValidate="txtNoKTP" CssClass="validator_general"></asp:RegularExpressionValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtNoKTP"
                ErrorMessage="Harap isi Nomor KTP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
             <div class="form_left">
                 <label>
                    Lampiran KTP
                 </label>
                 <asp:FileUpload ID="uplLampiranKTP" runat="server" onchange="showimagepreview(this,'imgLampiranKTP')"  />
             </div>
             <div class="form_right">
                 <asp:Button ID="btnupload" runat="server" Text="Upload" CssClass="small button blue"  />
                        </div>
                    </div>
         <div class="form_box">
             <div class="form_left">
                   <div>
                       <asp:Image ID="imgLampiranKTP" runat="server" />
                   </div>
             </div>
         </div>
        <div class="form_button">
            <asp:Button ID="ButtonSave" runat="server" Text="Save" CssClass="small button blue">
            </asp:Button>&nbsp;
            <asp:Button ID="ButtonCancel" runat="server" Text="Cancel" CssClass="small button gray"
                CausesValidation="False"></asp:Button>&nbsp; <a href="javascript:history.back();">
            </a>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
