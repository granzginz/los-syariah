﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CasesSetting.aspx.vb"
    Inherits="Maxiloan.Webform.ARMgt.CasesSetting" %>

<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../../Webform.UserController/UcSearchBy.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>CasesSetting</title>
    <script type="text/javascript" src="../../Maxiloan.js"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        function fConfirm() {
            if (window.confirm("Apakah yakin mau hapus data ini ? "))
                return true;
            else
                return false;
        }
        function fback() {
            history.go(-1);
            return false;
        }	
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
        onclick="hideMessage();"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                DAFTAR KASUS
            </h3>
        </div>
    </div>
    <asp:Panel ID="pnlDatagrid" runat="server">
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgCasesSetting" runat="server" Width="100%" OnSortCommand="SortGrid"
                        CssClass="grid_general" BorderStyle="None" BorderWidth="0" DataKeyField="CasesID"
                        AutoGenerateColumns="False" AllowSorting="True">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="EDIT">
                                <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbEdit" runat="server" ImageUrl="../../Images/IconEdit.gif"
                                        CausesValidation="False" CommandName="EDIT"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DELETE">
                                <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbDelete" runat="server" ImageUrl="../../Images/IconDelete.gif"
                                        CausesValidation="False" CommandName="DELETE"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="CasesID" HeaderText="ID">
                                <ItemTemplate>
                                    <asp:Label ID="lblCasesID" runat="server" Text='<%#Container.DataItem("CasesID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="KLAS">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="klas" Text='<%#container.dataitem("klas") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="KONSUMEN">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="KonsumenDesc" Text='<%#container.dataitem("KonsumenDesc") %>'></asp:Label>
                                    <asp:Label Visible="false" runat="server" ID="konsumen" Text='<%#container.dataitem("Konsumen") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="UNIT">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="UnitDesc" Text='<%#container.dataitem("UnitDesc") %>'></asp:Label>
                                     <asp:Label Visible="false" runat="server" ID="unit" Text='<%#container.dataitem("Unit") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="PENYEBAB">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="penyebab" Text='<%#container.dataitem("Penyebab") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                           
                        </Columns>
                    </asp:DataGrid>
                    <div class="button_gridnavigation">
                        <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                            CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                            CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                            CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                            CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                        <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                            EnableViewState="False"></asp:Button>
                        <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" MinimumValue="1"
                            ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                            ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general">
                        </asp:RequiredFieldValidator>
                    </div>
                    <div class="label_gridnavigation">
                        <asp:Label ID="lblPage" runat="server"></asp:Label>of
                        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                        <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                    </div>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonAddNew" runat="server" Text="Add" CssClass="small button blue">
            </asp:Button>&nbsp;
            <asp:Button ID="ButtonPrint" runat="server" Text="Print" CssClass="small button blue">
            </asp:Button>
        </div>
        <asp:Panel ID="pnlList" runat="server">
            <div class="form_box_title">
                <div class="form_single">
                    <h4>
                        CARI KASUS
                    </h4>
                </div>
            </div>
            <div class="form_box_uc">
                <uc1:ucsearchby id="oSearchBy" runat="server"></uc1:ucsearchby>
            </div>
            <div class="form_button">
                <asp:Button ID="Buttonsearch" runat="server" Text="Search" CssClass="small button blue">
                </asp:Button>&nbsp;
                <asp:Button ID="ButtonReset" runat="server" Text="Reset" CssClass="small button gray"
                    CausesValidation="False"></asp:Button>
            </div>
        </asp:Panel>
    </asp:Panel>
    <asp:Panel ID="pnlAdd" runat="server">
        <div class="form_box_title">
            <div class="form_single">
                <h4>
                    <asp:Label ID="lblTitle" runat="server"></asp:Label>
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <!-- <label class="label_general">-->
                <label class="label_req">
                    Konsumen</label>
                <asp:DropDownList ID="cboKonsumen" runat="server" AutoPostBack="true" OnSelectedIndexChanged="unit">
                    <asp:ListItem Selected="True" Value="-"></asp:ListItem>
                    <asp:ListItem Text="ADA" Value="1"></asp:ListItem>
                    <asp:ListItem Text="TIDAK ADA" Value="0"></asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" ErrorMessage="Konsumen Belum DiPilih"
                    ControlToValidate="cboKonsumen" InitialValue="-" CssClass="validator_general" Display="Dynamic"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    Unit</label>
                <asp:DropDownList ID="cboUnit" runat="server" AutoPostBack="true" OnSelectedIndexChanged="unit">
                    <asp:ListItem Selected="True" Value="-"></asp:ListItem>
                    <asp:ListItem Text="ADA" Value="1"></asp:ListItem>
                    <asp:ListItem Text="TIDAK ADA" Value="0"></asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator3" runat="server" ErrorMessage="Unit Belum DiPilih"
                    ControlToValidate="cboUnit" InitialValue="-" CssClass="validator_general" Display="Dynamic"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    Penyebab</label>
    
                <asp:DropDownList ID="cboPenyebab" runat="server" AutoPostBack="true" OnSelectedIndexChanged="penyebab">
                    <asp:ListItem Selected="True" Value="-"></asp:ListItem>
                    <asp:ListItem Text="KARAKTER" Value='Karakter'></asp:ListItem>
                    <asp:ListItem Text="APARAT" Value='Aparat'></asp:ListItem>
                    <asp:ListItem Text="BARANG BUKTI" Value='Barang Bukti'></asp:ListItem>
                    <asp:ListItem Text="ASURANSI" Value='Asuransi'></asp:ListItem>
                    <asp:ListItem Text="PIHAK KETIGA" Value='Pihak Ketiga'></asp:ListItem>
                    <asp:ListItem Text="FIKTIF" Value='Fiktif'></asp:ListItem>
                    <asp:ListItem Text="CASHFLOW" Value='CashFlow'></asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" ErrorMessage="Unit Belum DiPilih"
                    ControlToValidate="cboPenyebab" InitialValue="-" CssClass="validator_general" Display="Dynamic"></asp:RequiredFieldValidator>
            </div>
        </div>
           <div class="form_box">
            <div class="form_single">
                <label>
                    Klasifikasi</label>
                <asp:Label ID="lblKlasifikasi" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Kode Kasus</label>
                <asp:Label ID="lblkodekasus" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonSaveAdd" runat="server" Text="Save" CssClass="small button blue"
                CausesValidation="True"></asp:Button>&nbsp;
            <asp:Button ID="ButtonCancelAdd" runat="server" Text="Cancel" CssClass="small button gray"
                CausesValidation="False"></asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
