﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CollectorSetting.aspx.vb"
    Inherits="Maxiloan.Webform.ARMgt.CollectorSetting" %>

<%@ Register TagPrefix="uc1" TagName="UcBranchCollection" Src="../../Webform.UserController/UcBranchCollection.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Collector Setting</title>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <script language="javascript" type="text/javascript">
		var x = screen.width;
        var y = screen.height - 100;

        function fConfirm() {
            if (window.confirm("Apakah yakin mau hapus data ini ? "))
                return true;
            else
                return false;
        }
        function fback() {
            history.go(-1);
            return false;
        }

        function OpenViewCollectionGroup(pCGID) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.ARMgt/Setting/CollectionGroupView.aspx?CGID=' + pCGID, 'CollectionGroupView', 'left=0, top=0, width='+ x + ', height = ' + y +', menubar=0, scrollbars=yes');
        }

        function OpenViewCollector(pCollID, pCGID) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.ARMgt/Setting/CollectorView.aspx?CollectorID=' + pCollID + '&CGID=' + pCGID, 'CollectorView', 'left=0, top=0, width='+ x + ', height = ' + y +', menubar=0, scrollbars=yes');
        }


        function CBOCollTypeonChange() {
            document.forms[0].cboSPVId.disabled = false;

            if (document.forms[0].cboCollectorType.selectedIndex == 0) {
                document.forms[0].cboSPVId.disabled = true;
            }

            if (document.forms[0].cboCollectorType.value == 'COL') {
                document.forms[0].cboSPVId.disabled = true;
            }

            if (document.forms[0].cboCollectorType.value == 'COM') {
                document.forms[0].cboSPVId.disabled = true;
            }

            if (document.forms[0].cboCollectorType.value == 'ADM') {
                document.forms[0].cboSPVId.disabled = true;
            }

            if (document.forms[0].cboCollectorType.value == 'CH') {
                document.forms[0].cboSPVId.disabled = true;
            }

            if (document.forms[0].cboCollectorType.value == 'DS') {
                document.forms[0].cboSPVId.disabled = true;
            }
        }
-->
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">           
                <h3>    
                    DAFTAR COLLECTOR
                </h3>
            </div>
        </div>  
        <asp:Panel ID="pnlList" runat="server">
        <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgCollector" runat="server"
                        AllowSorting="True" AutoGenerateColumns="False" CssClass="grid_general" BorderStyle="None" BorderWidth="0"
                        OnSortCommand="Sorting" Width="100%">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="EDIT">
                                <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImbEdit" runat="server" ImageUrl="../../Images/iconedit.gif"
                                        CommandName="Edit"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DELETE">
                                <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImbDelete" runat="server" ImageUrl="../../Images/icondelete.gif"
                                        CommandName="Delete" CausesValidation="False"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="VIEW KODE POS">                                
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbZipCode" runat="server" ImageUrl="../../images/icondetail.gif"
                                        CommandName="ZipCode" Visible='<%# iif(Cstr(container.dataitem("CollectorType")).trim ="CL",true,false) %>'>
                                    </asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="CGID" HeaderText="COLLECTION GROUP"  >
                                <ItemTemplate>
                                    <asp:HyperLink runat="server" Enabled="True" ID="hpCGID" Text='<%# container.dataitem("CGName") %>'
                                        NavigateUrl='<%# LinkTo(container.dataItem("CGID")) %>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn Visible="False" DataField="CGID" SortExpression="CGID" HeaderText="COLL GROUP ID">
                            </asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="CollectorID" HeaderText="ID COLLECTOR">
                            </asp:BoundColumn>
                            <asp:TemplateColumn SortExpression="CollectorID" HeaderText="ID COLLECTOR"  >
                                <ItemTemplate>
                                    <asp:HyperLink runat="server" Enabled="True" ID="hpCollector" Text='<%# container.dataitem("CollectorID") %>'
                                        NavigateUrl='<%# LinkToCollector(container.dataItem("CollectorID"),container.dataItem("CGID")) %>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="Name" SortExpression="Name" HeaderText="NAMA COLLECTOR"
                                 ></asp:BoundColumn>
                            <asp:BoundColumn DataField="CollectorTypeDesc" SortExpression="CollectorTypeDesc"
                                HeaderText="JENIS COLLECTOR"  ></asp:BoundColumn>
                            <asp:BoundColumn DataField="Active" SortExpression="Active" HeaderText="AKTIF"></asp:BoundColumn>
                            <asp:BoundColumn DataField="CollectorType" SortExpression="CollectorType" HeaderText="COLLECTOR TYPE"></asp:BoundColumn>
                        </Columns>
                </asp:DataGrid>       
                <div class="button_gridnavigation">
	                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
		                CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
		                CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
		                CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
		                CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
	                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
		                EnableViewState="False"></asp:Button>
	                <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage"
				        MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer"
				        CssClass="validator_general"></asp:RangeValidator>
			        <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
				        ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general">
			        </asp:RequiredFieldValidator>              
                </div>
                <div class="label_gridnavigation">
	                <asp:Label ID="lblPage" runat="server"></asp:Label>of
	                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
	                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                </div>
            </div>  
        </div>  
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonAdd" runat="server"  Text="Add" CssClass ="small button blue">
            </asp:Button>
            <asp:Button ID="ButtonPrint" runat="server"  Text="Print" CssClass ="small button blue">
            </asp:Button>
	    </div>        
        </asp:Panel>
        <asp:Panel ID="pnlSearch" runat="server">
        <div class="form_box_title">
            <div class="form_single">              
                <h4>
                    COLLECTOR
                </h4>
            </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Collection Group</label>
                <uc1:UcBranchCollection id="UcBranchCollection1" runat="server"></uc1:UcBranchCollection>	        
            </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Cari Berdasarkan</label>
                <asp:DropDownList ID="cboSearchBy" runat="server" Width="144px" >
                    <asp:ListItem Value="CollectorID">ID Collector</asp:ListItem>
                    <asp:ListItem Value="CollectorName">Nama Collector</asp:ListItem>
                    <asp:ListItem Value="CollectorType">Jenis Collector</asp:ListItem>
                </asp:DropDownList>
                &nbsp;
                <asp:TextBox ID="TxtSearchByValue" runat="server" Width="168px" ></asp:TextBox>
	        </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonSearch" runat="server"  Text="Search" CssClass ="small button blue">
            </asp:Button>&nbsp;
            <asp:Button ID="ButtonReset" runat="server"  Text="Reset" CssClass ="small button gray">
            </asp:Button>
	    </div>       
        </asp:Panel>
        <asp:Panel ID="pnlAddEdit" runat="server">
        <div class="form_box_title">
            <div class="form_single">              
                <h4>
                    COLLECTOR -&nbsp;
                    <asp:Label ID="lblMenuAddEdit" runat="server"></asp:Label>
                </h4>
            </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Collection Group</label>
                <asp:Label ID="lblCollectionGroup" runat="server" Width="352px" ForeColor="Black"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>ID Collector</label>
                <asp:DropDownList ID="cboCollectorID" runat="server">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator" runat="server" Display="Dynamic"
                                    InitialValue="0" ControlToValidate="cboCollectorID" ErrorMessage="*"
                                    CssClass="validator_general"></asp:RequiredFieldValidator>
                <asp:Label ID="lblCollectorID" runat="server" Width="384px" ForeColor="Black"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Jenis Collector</label>
                <asp:DropDownList ID="cboCollectorType" runat="server" onchange="<%#CBOCollTypeonChange()%>">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic"
                                    InitialValue="0" ControlToValidate="cboCollectorType" ErrorMessage="*"
                                    CssClass="validator_general"></asp:RequiredFieldValidator>
	        </div>
        </div>
        <div class="form_box">                
	            <div class="form_single">                    
                    <label>Supervisor</label>
                    <asp:DropDownList ID="cboColTypeSPV" AutoPostBack="true" OnSelectedIndexChanged="cboColTypeSPV_IndexChanged" runat="server">
                    </asp:DropDownList>&nbsp;&nbsp;
                    <asp:DropDownList ID="cboSPVId" runat="server">
                    </asp:DropDownList>		                    
		        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Aktif</label>
                <asp:CheckBox ID="cbActive" runat="server"></asp:CheckBox>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Password Mobile</label>
                <asp:TextBox ID="txtPasswordMobile" runat="server" Width="180px" MaxLength='20' ></asp:TextBox>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Imei Mobile</label>
                <asp:TextBox ID="txtImeiMobile" runat="server" Width="300px" MaxLength='100' ></asp:TextBox>
	        </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonSave" runat="server"  Text="Save" CssClass ="small button blue">
            </asp:Button>&nbsp;
            <asp:Button ID="ButtonCancel" runat="server"  Text="Cancel" CssClass ="small button gray"
            CausesValidation="False"></asp:Button>&nbsp; <a href="javascript:history.back();">
            </a>
	    </div>
    </asp:Panel>
    </form>
</body>
</html>
