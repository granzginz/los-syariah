﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class CollAction
    Inherits Maxiloan.Webform.WebBased

#Region " Private Const "
    Dim m_ActionResult As New ActionResultController

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

#End Region

#Region " Property "
    Private Property ActionID() As String
        Get
            Return CStr(ViewState("ActionID"))
        End Get
        Set(ByVal Value As String)
            ViewState("ActionID") = Value
        End Set
    End Property

    Private Property ActionName() As String
        Get
            Return CStr(ViewState("ActionName"))
        End Get
        Set(ByVal Value As String)
            ViewState("ActionName") = Value
        End Set
    End Property

    Public Property ActionAddEdit() As String
        Get
            Return CStr(ViewState("ActionAddEdit"))
        End Get
        Set(ByVal Value As String)
            ViewState("ActionAddEdit") = Value
        End Set
    End Property

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        'Put user code to initialize the page here
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not Me.IsPostBack Then
            InitialDefaultPanel()
            Me.FormID = "CollAction"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.SearchBy = ""
                Me.SortBy = ""
                If Request("cond") <> "" Then
                    Me.SearchBy = Request("cond")
                Else
                    Me.SearchBy = "ALL"
                End If
                Me.SortBy = ""
                BindGridAction(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub

#Region " PanelAllFalse "
    Private Sub PanelAllFalse()
        pnlAddEdit.Visible = False
        pnlList.Visible = False
    End Sub
#End Region

#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridAction(Me.SearchBy, Me.SortBy)
    End Sub

    Protected Sub btnPageNumb_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPageNumb.Click

        If txtPage.Text = "" Then
            txtPage.Text = "0"
        Else
            If IsNumeric(txtPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                    currentPage = CType(txtPage.Text, Int32)
                    BindGridAction(Me.SearchBy, Me.SortBy)
                End If
            End If
        End If


    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        BindGridAction(Me.SearchBy, Me.SortBy)
    End Sub

#End Region

#Region " InitialDefaultPanel "
    Private Sub InitialDefaultPanel()
        pnlList.Visible = True
        pnlAddEdit.Visible = False
    End Sub
#End Region

#Region " BindGrid"
    Sub BindGridAction(ByVal cmdWhere As String, ByVal cmSort As String)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim oAction As New Parameter.ActionResult

        With oAction
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With


        oAction = m_ActionResult.GetActionList(oAction)

        With oAction
            lblrecord.Text = CType(.TotalRecord, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecord
        End With

        dtsEntity = oAction.ListData
        dtvEntity = dtsEntity.DefaultView
        dtvEntity.Sort = Me.SortBy
        dtgAction.DataSource = dtvEntity
        Try
            dtgAction.DataBind()
        Catch
            dtgAction.CurrentPageIndex = 0
            dtgAction.DataBind()
        End Try

        PagingFooter()
    End Sub
#End Region

    ' Private Sub BtnAdd_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnAdd.Click
    'If CheckFeature(Me.Loginid, Me.FormID, "Add", Me.AppId) Then
    '    If SessionInvalid() Then
    '        Exit Sub
    '    End If
    'End If

    'PanelAllFalse()

    'Me.ActionAddEdit = "ADD"
    'txtActionID.Text = ""
    'lblActionID.Visible = False
    'txtActionID.Text = ""
    'txtActionID.Visible = True
    'txtActionName.Text = ""
    'pnlAddEdit.Visible = True
    'lblMenuAddEdit.Text = Me.ActionAddEdit

    'End Sub



    Private Sub viewbyID(ByVal oAction As Parameter.ActionResult)

        Dim ds As DataTable


        txtActionID.Visible = False
        lblActionID.Text = oAction.actionid
        txtActionName.Text = oAction.actionname
        lblActionID.Visible = True
        lblMenuAddEdit.Text = Me.ActionAddEdit


        oAction = m_ActionResult.GetActionByID(oAction)

        ds = oAction.ListData


    End Sub

    Private Sub Add()
        Dim oAction As New Parameter.ActionResult
        With oAction
            .strConnection = GetConnectionString()
            .actionid = txtActionID.Text.Trim
            .actionname = txtActionName.Text
        End With

        Try
            m_ActionResult.ActionAdd(oAction)
            ShowMessage(lblMessage, "Tambah data Berhasil ", False)
            'MsgBox(MessageHelper.MESSAGE_INSERT_SUCCESS)
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try

    End Sub

    Private Sub edit(ByVal actionid As String)

        Dim oAction As New Parameter.ActionResult
        With oAction
            .strConnection = GetConnectionString()
            .actionid = actionid
            .actionname = txtActionName.Text
        End With

        Try
            m_ActionResult.ActionEdit(oAction)
            ShowMessage(lblMessage, "Update Data Berhasil ", False)
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

    Private Sub dtgAction_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgAction.ItemCommand
        Me.ActionID = e.Item.Cells(3).Text
        Me.ActionName = e.Item.Cells(4).Text

        Select Case e.CommandName
            Case "Edit"
                If CheckFeature(Me.Loginid, Me.FormID, "Edit", Me.AppId) Then
                    If SessionInvalid() Then
                        Exit Sub
                    End If
                End If

                Me.ActionID = e.Item.Cells(3).Text

                Dim oAction As New Parameter.ActionResult
                With oAction
                    .strConnection = GetConnectionString()
                    .actionid = Me.ActionID
                    .actionname = Me.ActionName
                    Me.ActionAddEdit = "EDIT"
                End With

                PanelAllFalse()
                viewbyID(oAction)
                pnlAddEdit.Visible = True

            Case "Delete"
                If CheckFeature(Me.Loginid, Me.FormID, "Del", Me.AppId) Then
                    If SessionInvalid() Then
                        Exit Sub
                    End If
                End If

                Me.ActionID = e.Item.Cells(3).Text

                Dim oAction As New Parameter.ActionResult
                With oAction
                    .strConnection = GetConnectionString()
                    .actionid = Me.ActionID
                End With

                Try
                    m_ActionResult.ActionDelete(oAction)
                    ShowMessage(lblMessage, "Hapus Data Berhasil ", False)
                Catch ex As Exception
                    ShowMessage(lblMessage, ex.Message, True)

                Finally
                    pnlAddEdit.Visible = False
                    BindGridAction("ALL", "")
                    pnlList.Visible = True

                End Try

            Case "ViewResult"
                Response.Redirect("CollResult.aspx?ActionID=" & Me.ActionID & "&ActionName=" & Me.ActionName)
        End Select
    End Sub

    ''Private Sub BtnReset_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ButtonReset.Click
    ''    cboSearchBy.SelectedIndex = 0
    ''    TxtSearchByValue.Text = ""
    ''    BindGridAction("ALL", "")
    ''End Sub

    Private Sub dtgAction_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgAction.ItemDataBound
        Dim imbDelete As ImageButton

        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("ImbDelete"), ImageButton)
            imbDelete.Attributes.Add("onclick", "return fConfirm()")
        End If

    End Sub



    Private Sub imbSave_Click() Handles ButtonSave.Click
        Select Case Me.ActionAddEdit
            Case "ADD"
                PanelAllFalse()
                Add()
                BindGridAction(Me.SearchBy, Me.SortBy)
                pnlList.Visible = True

            Case "EDIT"
                PanelAllFalse()
                edit(Me.ActionID)
                BindGridAction(Me.SearchBy, Me.SortBy)
                pnlList.Visible = True


        End Select
    End Sub

    Private Sub ImageButton1_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ButtonPrint.Click
        Response.Redirect("report/ActionResultRpt.aspx")
    End Sub



    Protected Sub BtnAdd_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BtnAdd.Click
        If CheckFeature(Me.Loginid, Me.FormID, "Add", Me.AppId) Then
            If SessionInvalid() Then
                Exit Sub
            End If
        End If

        PanelAllFalse()

        Me.ActionAddEdit = "ADD"
        txtActionID.Text = ""
        lblActionID.Visible = False
        txtActionID.Text = ""
        txtActionID.Visible = True
        txtActionName.Text = ""
        pnlAddEdit.Visible = True
        lblMenuAddEdit.Text = Me.ActionAddEdit
    End Sub


    Protected Sub ButtonCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonCancel.Click
        PanelAllFalse()
        InitialDefaultPanel()
    End Sub


    Protected Sub ButtonSearch_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonSearch.Click
        Dim StrSearchBy As String = cboSearchBy.SelectedItem.Value
        Dim StrSearchByValue As String = TxtSearchByValue.Text.Trim
        If StrSearchByValue = "" Then
            Me.SearchBy = ""
        Else
            Me.SearchBy = StrSearchBy + " like '%" + StrSearchByValue + "%'"
        End If
        PanelAllFalse()
        BindGridAction(Me.SearchBy, Me.SortBy)
        pnlList.Visible = True
    End Sub

    Protected Sub ButtonReset_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonReset.Click
        cboSearchBy.SelectedIndex = 0
        TxtSearchByValue.Text = ""
        BindGridAction("ALL", "")
    End Sub
End Class