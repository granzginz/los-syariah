﻿#Region "imports"
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class CollectorZipCodeSetting
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents UCEmployee As UcFindEmployee
    Protected WithEvents UCBranchCollection1 As UcBranchCollection
    Protected WithEvents UcCG As UcBranchCollection

    Dim dtCollector As New DataTable


#Region " Private Const "
    Dim m_CollZipCode As New CollZipCodeController

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

#End Region

#Region " Property "

    Private Property DefaultCollector() As String
        Get
            Return CStr(viewstate("DefaultCollector"))
        End Get
        Set(ByVal Value As String)
            viewstate("DefaultCollector") = Value
        End Set
    End Property

    Private Property BindMenu() As String
        Get
            Return CStr(viewstate("BindMenu"))
        End Get
        Set(ByVal Value As String)
            viewstate("BindMenu") = Value
        End Set
    End Property

    Private Property CGID() As String
        Get
            Return CStr(viewstate("CGID"))
        End Get
        Set(ByVal Value As String)
            viewstate("CGID") = Value
        End Set
    End Property

    Private Property City() As String
        Get
            Return CStr(viewstate("City"))
        End Get
        Set(ByVal Value As String)
            viewstate("City") = Value
        End Set
    End Property


    Private Property CGName() As String
        Get
            Return CStr(viewstate("CGName"))
        End Get
        Set(ByVal Value As String)
            viewstate("CGName") = Value
        End Set
    End Property

    Private Property ZipCode() As String
        Get
            Return CStr(ViewState("ZipCode"))
        End Get
        Set(ByVal Value As String)
            viewstate("ZipCode") = Value
        End Set
    End Property
#End Region


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        'Put user code to initialize the page here
        If SessionInvalid() Then
            Exit Sub
        End If
        lblDefaultCollector.Visible = False
        'createxmdlist()
        If Not Page.IsPostBack Then
            Me.CGID = Me.GroubDbID
            Me.FormID = "CollectorZipCode"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                InitialDefaultPanel()
                Me.SearchBy = "all"
                Me.SortBy = ""
                If Request("cond") <> "" Then
                    Me.SearchBy = Request("cond")
                Else
                    Me.SearchBy = "ALL"
                End If
                'getCGCombo()
                getCollectorCombo()
            End If
        End If
    End Sub

#Region " PanelAllFalse "
    Private Sub PanelAllFalse()        
        PnlSearch.Visible = False
        PnlSearchDetail.Visible = False
        pnlList.Visible = False
    End Sub
#End Region

#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            lblTotPage.Text = "1"
            'rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            'rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select

        'SavePage()
        Me.BindMenu = ""
        PnlSearch.Visible = False
        PnlSearchDetail.Visible = True
        BindGridCollZipCode(Me.SearchBy, Me.SortBy)
        pnlList.Visible = True
    End Sub

    Protected Sub btnPageNumb_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPageNumb.Click

        If txtPage.Text = "" Then
            txtPage.Text = "0"
        Else
            If IsNumeric(txtPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                    'SavePage()
                    Me.BindMenu = ""
                    currentPage = CType(txtPage.Text, Int32)
                    BindGridCollZipCode(Me.SearchBy, Me.SortBy)
                End If
            End If
        End If

        pnlList.Visible = True

    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If

        If Me.CGID = "" Then Me.CGID = Me.GroubDbID

        BindGridCollZipCode(Me.SearchBy, Me.SortBy)
        PnlSearch.Visible = False
        pnlList.Visible = True
        PnlSearchDetail.Visible = True
    End Sub

#End Region

#Region " InitialDefaultPanel "
    Private Sub InitialDefaultPanel()
        PnlSearch.Visible = True
        PnlSearchDetail.Visible = False
        pnlList.Visible = False
    End Sub
#End Region


#Region " BindGrid"
    Sub BindGridCollZipCode(ByVal cmdWhere As String, ByVal cmSort As String)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim oCollZipCode As New Parameter.CollZipCode

        With oCollZipCode
            .strConnection = GetConnectionString()
            .CGID = Me.CGID
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oCollZipCode = m_CollZipCode.GetCollectorZipCodeList(oCollZipCode)

        With oCollZipCode
            lblrecord.Text = CType(.TotalRecord, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecord
        End With

        dtsEntity = oCollZipCode.ListData
        dtvEntity = dtsEntity.DefaultView
        dtvEntity.Sort = Me.SortBy
        dtgCollZipCode.DataSource = dtvEntity
        Try
            dtgCollZipCode.DataBind()
        Catch
            dtgCollZipCode.CurrentPageIndex = 0
            dtgCollZipCode.DataBind()
        End Try
        txtSearchByValue.Text = ""
        PagingFooter()

    End Sub
#End Region

    Private Sub BtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSearch.Click

        If CheckFeature(Me.Loginid, Me.FormID, "Srch", Me.AppId) Then
            If SessionInvalid() Then
                Exit Sub
            End If
        End If

        With UcCG
            Me.CGID = .BranchID
            Me.CGName = .BranchName
        End With

        Me.SearchBy = ""
        Me.SortBy = ""        

        If UcCG.BranchID <> "0" Then
            PanelAllFalse()
            BindGridCollZipCode(Me.SearchBy, Me.SortBy)

            pnlList.Visible = True

            getCollectorCombo()
            lblCGID.Text = Me.CGID
            lblCGName.Text = Me.BranchName
            cbUnZipCode.Checked = False

            PnlSearchDetail.Visible = True
            PnlSearch.Visible = False
        End If

    End Sub

    Private Sub ButtonReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        cboSearchBy.SelectedIndex = 0
        txtSearchByValue.Text = ""

        Me.SortBy = ""
        Me.SearchBy = "all"

        BindGridCollZipCode("ALL", "")
        pnlList.Visible = True
    End Sub

    Private Sub dtgCollZipCode_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgCollZipCode.ItemDataBound
        Dim cbo As New DropDownList
        If e.Item.ItemIndex >= 0 Then
            cbo = CType(e.Item.FindControl("cboCollector"), DropDownList)

            Dim oCollZipCodeList As New Parameter.CollZipCode
            Dim oCollZipCode As New Parameter.CollZipCode
            Dim strconn As String = getConnectionString()
            Dim dt As New DataTable

            With oCollZipCode
                .strConnection = getConnectionString()
                .CGID = Me.CGID
                .CollectorType = "CL"
            End With

            dt = m_CollZipCode.GetCollectorCombo(oCollZipCode)
            cbo.DataSource = dt

            cbo.DataTextField = "collectorName"
            cbo.DataValueField = "collectorID"
            cbo.DataBind()

            cbo.Items.Insert(0, "Select One")
            cbo.Items(0).Value = ""

            If Me.BindMenu = "All" Then
                cbo.SelectedIndex = cbo.Items.IndexOf(cbo.Items.FindByValue(Me.DefaultCollector))
            ElseIf Me.BindMenu = "UnZipCode" Then
                If cbo.SelectedItem.Value = "" Then cbo.SelectedIndex = cbo.Items.IndexOf(cbo.Items.FindByValue(Me.DefaultCollector))
            Else
                cbo.SelectedIndex = cbo.Items.IndexOf(cbo.Items.FindByValue(e.Item.Cells(4).Text.Trim))
            End If

        End If

    End Sub

    Private Sub ButtonCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        Response.Redirect("CollectorZipCodeSetting.aspx")
    End Sub

    Private Sub imbPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonPrint.Click
        If SessionInvalid() Then
            Exit Sub
        End If

        Dim cookie As HttpCookie = Request.Cookies(COOKIES_COLLECTOR_SETTING)
        If Not cookie Is Nothing Then
            cookie.Values("CGId") = Me.CGID
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie(COOKIES_COLLECTOR_SETTING)
            cookieNew.Values.Add("CGId", Me.CGID)
            Response.AppendCookie(cookieNew)
        End If
        Response.Redirect("report/CollectorZipCodeRpt.aspx")
    End Sub

    'Private Sub getCGCombo()
    '    Dim oCollZipCodeList As New Parameter.CollZipCode
    '    Dim strconn As String = getConnectionString()
    '    Dim dt As New DataTable

    '    dt = m_CollZipCode.GetCollectionGroupCombo(strconn)
    '    cboCG.DataSource = dt
    '    cboCG.DataTextField = "cgname"
    '    cboCG.DataValueField = "cgid"
    '    cboCG.DataBind()

    '    cboCG.Items.Insert(0, "Select One")
    '    cboCG.Items(0).Value = "0"
    'End Sub

    Private Sub getCollectorCombo()
        Dim oCollZipCode As New Parameter.CollZipCode
        Dim strconn As String = getConnectionString()
        Dim dt As New DataTable

        With oCollZipCode
            .strConnection = getConnectionString()
            .CGID = Me.CGID
            .CollectorType = "CL"
        End With

        dt = m_CollZipCode.GetCollectorCombo(oCollZipCode)
        cboDefaultCollector.DataSource = dt
        cboDefaultCollector.DataTextField = "CollectorName"
        cboDefaultCollector.DataValueField = "CollectorID"
        cboDefaultCollector.DataBind()

        cboDefaultCollector.Items.Insert(0, "Select One")
        cboDefaultCollector.Items(0).Value = "0"

        dtCollector = dt
    End Sub

    Private Sub imbSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
        SavePage()
    End Sub

    Private Sub SavePage()
        Dim oCollZipCode As New Parameter.CollZipCode

        Dim LoopZipCode As Int16
        Dim strZipCode As String
        Dim strKelurahan As String
        Dim strKecamatan As String
        Dim cboColl As New DropDownList

        For LoopZipCode = 0 To CType(dtgCollZipCode.Items.Count - 1, Int16)
            strZipCode = CType(dtgCollZipCode.Items(LoopZipCode).Cells(0).Text.Trim, String)
            strKelurahan = CType(dtgCollZipCode.Items(LoopZipCode).Cells(1).Text.Trim, String)
            strKecamatan = CType(dtgCollZipCode.Items(LoopZipCode).Cells(2).Text.Trim, String)
            cboColl = CType(dtgCollZipCode.Items(LoopZipCode).Cells(3).FindControl("cboCollector"), DropDownList)

            With oCollZipCode
                .City = Me.City
                .CGID = Me.CGID
                .CollectorID = cboColl.SelectedItem.Value
                .ZipCode = strZipCode.Trim
                .Kelurahan = strKelurahan.Trim
                .Kecamatan = strKecamatan.Trim
                .strConnection = GetConnectionString
            End With
            Try
                m_CollZipCode.CollectorZipCodeEdit(oCollZipCode)                
                ShowMessage(lblMessage, "Update data Berhasil ", False)
            Catch ex As Exception                
                ShowMessage(lblMessage, ex.Message, True)
            End Try
        Next
        BindGridCollZipCode(Me.SearchBy, Me.SortBy)
        pnlList.Visible = True
        PnlSearch.Visible = False
        PnlSearchDetail.Visible = True
    End Sub

    Private Sub BtnSearchDetail_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSearchDetail.Click
        Me.BindMenu = ""

        If cboSearchBy.SelectedItem.Value <> "0" And txtSearchByValue.Text.Trim <> "" Then
            Me.SearchBy = cboSearchBy.SelectedItem.Value & " like '%" & txtSearchByValue.Text.Trim & "%'"

            If cbUnZipCode.Checked = True Then
                Me.SearchBy = Me.SearchBy + " and (CollectorID='' or CollectorID is null)"
            End If
        Else

            If cbUnZipCode.Checked = True Then
                Me.SearchBy = " (CollectorID='' or CollectorID is null)"
                Me.DefaultCollector = ""
            Else
                Me.SearchBy = ""
            End If
        End If

        Me.SortBy = ""
        BindGridCollZipCode(Me.SearchBy, Me.SortBy)
        pnlList.Visible = True
        PnlSearch.Visible = False
        PnlSearchDetail.Visible = True
    End Sub

    Private Sub imbApply_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonApply.Click
        If cboDefaultCollector.SelectedIndex <= 0 Then
            lblDefaultCollector.Visible = True
            Exit Sub
        End If

        If cboDefaultCollector.SelectedItem.Value <> "0" Then

            Dim cboCollector As New DropDownList
            Dim LoopZipCode As Int16

            If cboCriteria.SelectedItem.Value.Trim = "All" Then
                Me.BindMenu = "All"
                For LoopZipCode = 0 To CType(dtgCollZipCode.Items.Count - 1, Int16)
                    cboCollector = CType(dtgCollZipCode.Items(LoopZipCode).Cells(3).FindControl("cboCollector"), DropDownList)
                    cboCollector.SelectedIndex = cboCollector.Items.IndexOf(cboCollector.Items.FindByValue(cboDefaultCollector.SelectedItem.Value))
                Next

            Else
                Me.BindMenu = "UnZipCode"
                For LoopZipCode = 0 To CType(dtgCollZipCode.Items.Count - 1, Int16)
                    cboCollector = CType(dtgCollZipCode.Items(LoopZipCode).Cells(3).FindControl("cboCollector"), DropDownList)
                    If cboCollector.SelectedItem.Value = "" Then
                        cboCollector.SelectedIndex = cboCollector.Items.IndexOf(cboCollector.Items.FindByValue(cboDefaultCollector.SelectedItem.Value))
                        'cboCollector.SelectedIndex = cboCG.Items.IndexOf(cboCG.Items.FindByValue(cboDefaultCollector.SelectedItem.Value))
                    End If
                Next
            End If

            Me.DefaultCollector = cboDefaultCollector.SelectedItem.Value

            pnlList.Visible = True
            PnlSearchDetail.Visible = True
            PnlSearch.Visible = False

        End If
    End Sub

    Private Sub BtnResetDetail_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonResetDetail.Click
        Me.BindMenu = ""
        Me.SearchBy = ""
        Me.SortBy = ""
        BindGridCollZipCode(Me.SearchBy, Me.SortBy)
        pnlList.Visible = True
        PnlSearch.Visible = False
        PnlSearchDetail.Visible = True
    End Sub

End Class