﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="LokasiAsset.aspx.vb" Inherits="Maxiloan.Webform.ARMgt.LokasiAsset" %>

<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../../Webform.UserController/UcSearchBy.ascx" %>
<%@ Register TagPrefix="uc2" TagName="ucCompanyAddress" Src="../../Webform.UserController/ucCompanyAddress.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>LokasiAsset</title>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <script src="../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
        onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlDtGrid" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    DAFTAR LOKASI ASSET
                </h3>
            </div>
        </div>


        <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgLokasiAsset" runat="server" Width="100%" CssClass="grid_general"
                        BorderStyle="None" BorderWidth="0" DataKeyField="IdLokasiAsset" AutoGenerateColumns="False"
                        AllowSorting="true">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="EDIT">
                                <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbEdit" runat="server" ImageUrl="../../Images/IconEdit.gif"
                                        CommandName="EDIT"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DELETE">
                                <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbDelete" runat="server" ImageUrl="../../Images/IconDelete.gif"
                                        CommandName="DELETE"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="IdLokasiAsset" HeaderText="ID LOKASI ASSET">
                                <ItemTemplate>
                                    <asp:Label ID="lblId" Text='<%#Container.dataItem("IdLokasiAsset")%>' runat="server">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Description" HeaderText="KETERANGAN">
                                <ItemTemplate>
                                    <asp:Label ID="lblDescription" Text='<%#Container.dataItem("IdLokasiDesc")%>' runat="server">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="idAlamat" HeaderText="ALAMAT">
                                <ItemTemplate>
                                    <asp:Label ID="lblAlamat" Text='<%#Container.dataItem("IdAlamat")%>' runat="server">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="idRT" HeaderText="RT" Visible ="false" >
                                <ItemTemplate>
                                    <asp:Label ID="lblRT" Text='<%#Container.dataItem("IdRT")%>' runat="server">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="idRW" HeaderText="RW" Visible ="false" >
                                <ItemTemplate>
                                    <asp:Label ID="lblRW" Text='<%#Container.dataItem("IdRW")%>' runat="server">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="idKelurahan" HeaderText="KELURAHAN">
                                <ItemTemplate>
                                    <asp:Label ID="lblKelurahan" Text='<%#Container.dataItem("IdKelurahan")%>' runat="server">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="idKecamatan" HeaderText="KECAMATAN">
                                <ItemTemplate>
                                    <asp:Label ID="lblKecamatan" Text='<%#Container.dataItem("IdKecamatan")%>' runat="server">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="idKota" HeaderText="KOTA">
                                <ItemTemplate>
                                    <asp:Label ID="lblKota" Text='<%#Container.dataItem("IdKota")%>' runat="server">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="idKodePos" HeaderText="KODE POS" Visible ="false" >
                                <ItemTemplate>
                                    <asp:Label ID="lblKodePos" Text='<%#Container.dataItem("IdKodePos")%>' runat="server">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="idKontak" HeaderText="KONTAK" Visible ="false" >
                                <ItemTemplate>
                                    <asp:Label ID="lblKontak" Text='<%#Container.dataItem("IdKontak")%>' runat="server">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="idJabatan" HeaderText="JABATAN" Visible ="false" >
                                <ItemTemplate>
                                    <asp:Label ID="lblJabatan" Text='<%#Container.dataItem("IdJabatan")%>' runat="server">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="idHandphone" HeaderText="HANDPHONE" Visible ="false" >
                                <ItemTemplate>
                                    <asp:Label ID="lblHandphone" Text='<%#Container.dataItem("IdHandphone")%>' runat="server">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="idEmail" HeaderText="EMAIL" Visible ="false" >
                                <ItemTemplate>
                                    <asp:Label ID="lblEmail" Text='<%#Container.dataItem("IdEmail")%>' runat="server">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="idStatus" HeaderText="STATUS" Visible ="false" >
                                <ItemTemplate>
                                    <asp:Label ID="lblStatus" Text='<%#Container.dataItem("IdStatus")%>' runat="server">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>

                        </Columns>
                    </asp:DataGrid>
                    <div class="button_gridnavigation">
                        <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                            CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                            CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                            CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                            CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                        <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                            EnableViewState="False"></asp:Button>
                        <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" MinimumValue="1"
                            ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                            ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general">
                        </asp:RequiredFieldValidator>
                    </div>
                    <div class="label_gridnavigation">
                        <asp:Label ID="lblPage" runat="server"></asp:Label>of
                        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                        <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                    </div>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonAddNew" runat="server" Text="Add" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="ButtonPrint" runat="server" Text="Print" CssClass="small button blue">
            </asp:Button>
        </div>
        <asp:Panel ID="pnlList" runat="server">
            <div class="form_box_title">
                <div class="form_single">
                    <h4>
                        DAFTAR LOKASI
                    </h4>
                </div>
            </div>
            <div class="form_box_uc">
                <uc1:ucsearchby id="oSearchBy" runat="server"></uc1:ucsearchby>
            </div>
            <div class="form_button">
                <asp:Button ID="Buttonsearch" runat="server" Text="Search" CssClass="small button blue">
                </asp:Button>&nbsp;
                <asp:Button ID="ButtonReset" runat="server" Text="Reset" CssClass="small button gray"
                    CausesValidation="False"></asp:Button>
            </div>
        </asp:Panel>
    </asp:Panel>
    <asp:Panel ID="pnlAdd" runat="server">
        <div class="form_box_title">
            <div class="form_single">
                <h4>
                    <asp:Label ID="lblTitle" runat="server"></asp:Label>
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    ID</label>
                <asp:TextBox ID="txtId" runat="server" MaxLength="10"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvId" runat="server" CssClass="validator_general"
                    ControlToValidate="txtId" ErrorMessage="Harap isi ID" Display="Dynamic"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    Keterangan</label>
                <asp:TextBox ID="txtDescription" runat="server" Width="475px" MaxLeght="450"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvDescription" runat="server" CssClass="validator_general"
                    ControlToValidate="txtDescription" ErrorMessage="Harap isi Keterangan" Display="Dynamic"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box_uc">
                <uc2:uccompanyaddress id="oUcCompanyAdress" runat="server"></uc2:uccompanyaddress>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Kontak</label>
                <asp:TextBox ID="txtKontak" runat="server" Width="200px" MaxLeght="20"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" CssClass="validator_general"
                    ControlToValidate="txtKontak" ErrorMessage="Harap isi Kontak" Display="Dynamic"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Jabatan</label>
                <asp:TextBox ID="txtJabatan" runat="server" Width="200px" MaxLeght="35"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" CssClass="validator_general"
                    ControlToValidate="txtJabatan" ErrorMessage="Harap isi Jabatan" Display="Dynamic"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Handphone</label>
                <asp:TextBox ID="txtHandphone" runat="server" Width="200px" MaxLeght="20" onkeypress="return numbersonly2(event)"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" CssClass="validator_general"
                    ControlToValidate="txtHandphone" ErrorMessage="Harap isi Handphone" Display="Dynamic"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Email</label>
                <asp:TextBox ID="txtEmail" runat="server" Width="200px" MaxLeght="30"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" CssClass="validator_general"
                    ControlToValidate="txtEmail" ErrorMessage="Harap isi Email" Display="Dynamic"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Status</label>
                    <asp:CheckBox ID="cbActive" runat="server" BorderStyle="None"></asp:CheckBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonSaveAdd" runat="server" Text="Save" CssClass="small button blue"
                CausesValidation="True"></asp:Button>&nbsp;
            <asp:Button ID="ButtonCancelAdd" runat="server" Text="Cancel" CssClass="small button gray"
                CausesValidation="False"></asp:Button><a href="javascript:history.back();"></a>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
