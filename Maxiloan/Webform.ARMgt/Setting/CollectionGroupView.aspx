﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CollectionGroupView.aspx.vb"
    Inherits="Maxiloan.Webform.ARMgt.CollectionGroupView" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>CollectionGroupView</title>
    <script language="javascript" type="text/javascript">
        function windowClose() {
            window.close();
        }
        function fback() {
            history.go(-1);
            return false;
        }	
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">           
                <h3>
                    VIEW - COLLECTION GROUP
                </h3>
            </div>
        </div>  
        <asp:Panel ID="PnlView" runat="server" Visible="False">
        <div class="form_box">
	        <div class="form_single">
                <label>ID Collection Group</label>
                <asp:Label ID="lblCGID" runat="server" Width="184px"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Nama Collection Group</label>
                <asp:Label ID="lblCGName" runat="server" Width="184px"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Kepala Collection Group</label>
                <asp:Label ID="lblCGHead" runat="server" Width="184px"></asp:Label>
	        </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">              
                <h4>
                    ALAMAT
                </h4>
            </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Alamat</label>
                <asp:Label ID="lblAddress" runat="server" Width="256px"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>RT/RW</label>
                <asp:Label ID="lblRT" runat="server" Width="32px"></asp:Label>
                <asp:Label ID="Label3" runat="server" Width="8px">/</asp:Label>
                <asp:Label ID="lblRW" runat="server" Width="39px"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Kelurahan</label>
                <asp:Label ID="lblKelurahan" runat="server" Width="256px"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Kecamatan</label>
                <asp:Label ID="lblKecamatan" runat="server" Width="256px"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Kota</label>
                <asp:Label ID="lblCity" runat="server" Width="256px"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Kode Pos</label>
                <asp:Label ID="lblZipCode" runat="server" Width="80px"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>No Telepon-1</label>
                <asp:Label ID="lblPhone1" runat="server" Width="144px"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>No Telepon-2</label>
                <asp:Label ID="lblPhone2" runat="server" Width="128px"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>No Fax</label>
                <asp:Label ID="lblFax" runat="server" Width="175px"></asp:Label>
	        </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">              
                <h4>
                    KONTAK
                </h4>
            </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Kontak</label>
                <asp:Label ID="lblCPName" runat="server" Visible="False" Width="256px"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Jabatan</label>
                <asp:Label ID="lblCPTitle" runat="server" Width="256px"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>No HandPhone</label>
                <asp:Label ID="lblMobilePhone" runat="server" Width="256px"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>e-mail</label>
                <asp:Label ID="lblCPEmail" runat="server" Visible="False" Width="256px"></asp:Label>
	        </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonClose" runat="server" CausesValidation="False"  Text="Close" CssClass ="small button gray">
            </asp:Button>&nbsp; <a href="javascript:history.back();"></a>
	    </div>        
    </asp:Panel>
    </form>
</body>
</html>
