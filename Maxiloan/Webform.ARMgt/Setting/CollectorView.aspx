﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CollectorView.aspx.vb"
    Inherits="Maxiloan.Webform.ARMgt.CollectorView" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>CollectorView</title>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;

        function windowClose() {
            window.close();
        }
        function fback() {
            history.go(-1);
            return false;
        }
        function click() {
            if (event.button == 2) {
                alert('Anda Tidak Berhak');
            }
        }
        document.onmousedown = click

        function OpenViewRAL(pCGID, pCollectorID) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.ARMgt/Setting/CollectorRAL.aspx?CGID=' + pCGID + '&CollectorID=' + pCollectorID, 'RALView', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:Panel ID="PnlView"  runat="server" Visible="False" >
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">           
                <h3>    
                    VIEW - COLLECTOR
                </h3>
            </div>
        </div>  
        <div class="form_box">
	        <div class="form_single">
                <label>Collection Group</label>
                <asp:Label ID="lblCGName" runat="server" Width="184px"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>ID Collector</label>
                <asp:Label ID="lblCollectorID" runat="server" Width="184px"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>ID Karyawan</label>
                <asp:Label ID="lblEmployeeID" runat="server" Width="456px"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Nama Karyawan</label>
                <asp:Label ID="lblEmployeeName" runat="server" Width="448px"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Jenis Collector</label>
                <asp:Label ID="lblCollectorType" runat="server" Width="448px"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Supervisor</label>
                <asp:Label ID="lblSupervisor" runat="server" Width="184px"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Aktif</label>
                <asp:Label ID="lblActive" runat="server" Width="184px"></asp:Label>
	        </div>
        </div>        
        </asp:Panel>
        <asp:Panel ID="PnlView2" runat="server" Visible="False">
        <div class="form_box">
	        <div class="form_single">
                <label>Alamat</label>
                <asp:Label ID="lblAddress" runat="server" Width="256px"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>No Telepon</label>
                <asp:Label ID="lblPhoneNo" runat="server" Width="376px"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>No HandPhone</label>
                <asp:Label ID="lblMobilePhone" runat="server" Width="376px"></asp:Label>
	        </div>
        </div>   
        <div class="form_box">
	        <div class="form_single">
                <label>Catatan</label>
                <asp:Label ID="lblNotes" runat="server" Width="504px"></asp:Label>
	        </div>
        </div>  
        <div class="form_box">
	        <div class="form_single">
                <asp:HyperLink ID="hpRAL" runat="server" Enabled="True" Text="RAL History"></asp:HyperLink>
	        </div>
        </div>         
        </asp:Panel>
        <asp:Panel ID="pnlClose" runat="server" >
        <div class="form_button">
            <asp:Button ID="ButtonClose" runat="server"  Text="Close" CssClass ="small button gray"
            CausesValidation="true"></asp:Button>
	    </div>
        </asp:Panel>
    </form>
</body>
</html>
