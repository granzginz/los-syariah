﻿#Region "imports"
Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
#End Region

Public Class CollectorSetting
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents UcBranchCollection1 As UcBranchCollection
    Protected WithEvents UCEmployee As UcFindEmployee
    Protected WithEvents UcCombo2Level As UCCombo

#Region " Private Const "
    Dim m_Collector As New CollectorController

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

#End Region

#Region " Property "
    Private Property colltype1() As String
        Get
            Return CStr(viewstate("colltype1"))
        End Get
        Set(ByVal Value As String)
            viewstate("colltype1") = Value
        End Set
    End Property


    Private Property CGID() As String
        Get
            Return CStr(viewstate("CGID"))
        End Get
        Set(ByVal Value As String)
            viewstate("CGID") = Value
        End Set
    End Property

    Private Property CollectorID() As String
        Get
            Return CStr(viewstate("CollectorID"))
        End Get
        Set(ByVal Value As String)
            viewstate("CollectorID") = Value
        End Set
    End Property

    Private Property CollectorName() As String
        Get
            Return CStr(viewstate("CollectorName"))
        End Get
        Set(ByVal Value As String)
            viewstate("CollectorName") = Value
        End Set
    End Property


    Private Property CGName() As String
        Get
            Return CStr(viewstate("CGName"))
        End Get
        Set(ByVal Value As String)
            viewstate("CGName") = Value
        End Set
    End Property

    Private Property CGHead() As String
        Get
            Return CStr(viewstate("CGHead"))
        End Get
        Set(ByVal Value As String)
            viewstate("CGHead") = Value
        End Set
    End Property

    Public Property CheckAll() As String
        Get
            Return CStr(ViewState("CheckAll"))
        End Get
        Set(ByVal Value As String)
            viewstate("CheckAll") = Value
        End Set
    End Property

    Private Property EmployeeID() As String
        Get
            Return CStr(viewstate("EmployeeID"))
        End Get
        Set(ByVal Value As String)
            viewstate("EmployeeID") = Value
        End Set
    End Property

    Private Property ActionAddEdit() As String
        Get
            Return CStr(viewstate("ActionAddEdit"))
        End Get
        Set(ByVal Value As String)
            viewstate("ActionAddEdit") = Value
        End Set
    End Property
    Private Property SPVId() As String
        Get
            Return CStr(viewstate("SPVId"))
        End Get
        Set(ByVal Value As String)
            viewstate("SPVId") = Value
        End Set
    End Property

    Private Property CollectorType() As String
        Get
            Return CStr(ViewState("CollectorType"))
        End Get
        Set(ByVal Value As String)
            ViewState("CollectorType") = Value
        End Set
    End Property

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        'Put user code to initialize the page here
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not Me.IsPostBack Then
            Me.CGID = Me.GroubDbID
            Me.FormID = "Collector"
            GetCollectorCombo()
            getComboCollectorType()
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                InitialDefaultPanel()
                'If Request.QueryString("cgid") <> "" Then
                If Me.CGID <> "" Then
                    Me.SearchBy = "CGID=" & Me.CGID.Trim
                    Me.SortBy = ""
                    defaultCboSPVID()
                    BindGridCollector(Me.SearchBy, Me.SortBy)
                    pnlList.Visible = True
                Else
                    Me.SearchBy = "all"
                    Me.SortBy = ""
                    If Request("cond") <> "" Then
                        Me.SearchBy = Request("cond")
                    Else
                        Me.SearchBy = "ALL"
                    End If
                    Me.SortBy = ""
                    defaultCboSPVID()
                    BindGridCollector(Me.SearchBy, Me.SortBy)
                End If
                
            End If
        End If
    End Sub

#Region " PanelAllFalse "
    Private Sub PanelAllFalse()        
        pnlSearch.Visible = False
        pnlAddEdit.Visible = False
        pnlList.Visible = False
    End Sub
#End Region

#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridCollector(Me.SearchBy, Me.SortBy)
    End Sub

    Protected Sub btnPageNumb_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPageNumb.Click

        If txtPage.Text = "" Then
            txtPage.Text = "0"
        Else
            If IsNumeric(txtPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                    currentPage = CType(txtPage.Text, Int32)
                    BindGridCollector(Me.SearchBy, Me.SortBy)
                End If
            End If
        End If


    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        BindGridCollector(Me.SearchBy, Me.SortBy)
    End Sub

#End Region

#Region " InitialDefaultPanel "
    Private Sub InitialDefaultPanel()
        pnlSearch.Visible = True
        pnlList.Visible = False
        pnlAddEdit.Visible = False
    End Sub
#End Region

#Region " BindGrid"
    Sub BindGridCollector(ByVal cmdWhere As String, ByVal cmSort As String)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim oCollector As New Parameter.Collector

        Me.CGID = Me.GroubDbID

        With oCollector
            .strConnection = GetConnectionString()
            .CGID = Me.CGID
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With


        oCollector = m_Collector.GetCollectorList(oCollector)

        With oCollector
            lblrecord.Text = CType(.TotalRecord, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecord
        End With

        dtsEntity = oCollector.ListData
        dtvEntity = dtsEntity.DefaultView
        dtvEntity.Sort = Me.SortBy
        dtgCollector.DataSource = dtvEntity
        Try
            dtgCollector.DataBind()
        Catch
            dtgCollector.CurrentPageIndex = 0
            dtgCollector.DataBind()
        End Try

        PagingFooter()
    End Sub
#End Region

    Private Sub BtnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonAdd.Click
        If CheckFeature(Me.Loginid, Me.FormID, "Add", Me.AppId) Then
            If SessionInvalid() Then
                Exit Sub
            End If
        End If

        PanelAllFalse()
        ClearAddForm()
        cboSPVId.Items.Insert(0, "Select One")
        cboSPVId.Items(0).Value = "0"
        cboSPVId.SelectedIndex = 0

        Me.ActionAddEdit = "ADD"
        pnlAddEdit.Visible = True
        lblMenuAddEdit.Text = Me.ActionAddEdit

    End Sub

    Private Sub BtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSearch.Click
        Dim StrSearchBy As String = cboSearchBy.SelectedItem.Value
        Dim StrSearchByValue As String = TxtSearchByValue.Text.Trim
        If StrSearchByValue = "" Then
            Me.SearchBy = ""
        Else
            If StrSearchByValue.IndexOf("%") >= 0 Then
                Me.SearchBy = StrSearchBy + " like '%" + StrSearchByValue + "%' and CGID ='" + UcBranchCollection1.BranchID + "'"
            Else
                Me.SearchBy = StrSearchBy + " like '%" + StrSearchByValue + "%' and CGID ='" + UcBranchCollection1.BranchID + "'"
            End If
        End If
        PanelAllFalse()
        BindGridCollector(Me.SearchBy, Me.SortBy)
        pnlSearch.Visible = True
        pnlList.Visible = True

        Me.BranchName = UcBranchCollection1.BranchName
        Me.CGID = UcBranchCollection1.BranchID
    End Sub

    Private Sub viewbyID(ByVal oCollector As Parameter.Collector)

        Dim ds As DataTable
        Context.Trace.Write("viewbyID")

        lblMenuAddEdit.Text = Me.ActionAddEdit


        oCollector = m_Collector.GetCollectorByID(oCollector)
        ds = oCollector.ListData

        'CboCollectionGroup.SelectedIndex = CboCollectionGroup.Items.IndexOf(CboCollectionGroup.Items.FindByValue(CStr(ds.Rows(0).Item("CGID"))))
        'cboCollectorID.SelectedIndex = cboCollectorID.Items.IndexOf(cboCollectorID.Items.FindByValue(CStr(ds.Rows(0).Item("CollectorID"))))

        lblCollectionGroup.Text = Me.BranchName
        lblCollectorID.Text = CStr(ds.Rows(0).Item("CollectorID")).Trim

        cboCollectorID.Visible = False
        lblCollectionGroup.Visible = True
        lblCollectorID.Visible = True

        'With UCEmployee
        '    .EmployeeName = CStr(IIf(IsDBNull(ds.Rows(0).Item("EmployeeName")), "-", ds.Rows(0).Item("EmployeeName"))).Trim
        '    .Text = "Employee Name "
        '    .Style = "Collection"
        '    .BindData()
        'End With

        'Me.EmployeeID = CStr(IIf(IsDBNull(ds.Rows(0).Item("EmployeeID")), "-", ds.Rows(0).Item("EmployeeID"))).Trim

        colltype1 = CStr(ds.Rows(0).Item("CollectorType")).Trim
        cboCollectorType.SelectedIndex = cboCollectorType.Items.IndexOf(cboCollectorType.Items.FindByText(CStr(ds.Rows(0).Item("CollectorTypeDesc")).Trim))

        Select Case CStr(ds.Rows(0).Item("CollectorType")).Trim

            'Case "ADM"
            '    Context.Trace.Write("CollectorType = ADM")
            '    cboSPVId.SelectedIndex = 0
            '    cboSPVId.Enabled = False
            'Case "COL"
            '    Context.Trace.Write("CollectorType = COL")
            '    cboSPVId.SelectedIndex = 0
            '    cboSPVId.Enabled = False
            'Case "COM"
            '    Context.Trace.Write("CollectorType = COM")
            '    cboSPVId.SelectedIndex = 0
            '    cboSPVId.Enabled = False
            'Case "DS"
            '    Context.Trace.Write("CollectorType = DS")
            '    cboSPVId.SelectedIndex = 0
            '    cboSPVId.Enabled = False
            'Case "CH"
            '    Context.Trace.Write("CollectorType = CH")
            '    cboSPVId.SelectedIndex = 0
            '    cboSPVId.Enabled = False
            'Case "COLADM"
            '    Context.Trace.Write("CollectorType = COLADM")
            '    cboSPVId.SelectedIndex = 0
            '    cboSPVId.Enabled = False
            'Case Else
            '    Context.Trace.Write("CollectorType = Else")
            '    cboSPVId.SelectedIndex = cboSPVId.Items.IndexOf(cboSPVId.Items.FindByText(CStr(ds.Rows(0).Item("Supervisor")).Trim))
            '    cboSPVId.Enabled = True
        End Select

        cboSPVId.SelectedIndex = cboSPVId.Items.IndexOf(cboSPVId.Items.FindByText(CStr(ds.Rows(0).Item("Supervisor")).Trim))
        cboSPVId.Enabled = True


        'With UcCombo2Level
        '    .cgid = Me.CGID
        '    .CollectorTypeID = CStr(IIf(IsDBNull(ds.Rows(0).Item("CollectorType")), "-", ds.Rows(0).Item("CollectorType")))


        '    .SupervisorID = CStr(IIf(IsDBNull(ds.Rows(0).Item("Supervisor")), "-", ds.Rows(0).Item("Supervisor")))
        '    .InsertComboChild()

        'End With

        Dim checked As Boolean
        If CStr(ds.Rows(0).Item("Active")) = "Y" Then checked = True Else checked = False

        cbActive.Checked = checked
        txtPasswordMobile.Text = CStr(ds.Rows(0).Item("PasswordMobile")).Trim
        txtImeiMobile.Text = CStr(ds.Rows(0).Item("ImeiMobile")).Trim
    End Sub

    Private Sub ClearAddForm()
        'With UCEmployee
        '    .Style = "Collection"
        '    .Text = "Employee Name "
        '    .EmployeeID = ""
        '    .EmployeeName = ""
        '    .BindData()
        'End With

        cboCollectorID.Visible = True
        cboCollectorID.SelectedIndex = 0
        cboCollectorType.SelectedIndex = 0        

        lblCollectionGroup.Text = Me.BranchName
        lblCollectionGroup.Visible = True
        lblCollectorID.Visible = False
        txtPasswordMobile.Visible = False
        txtImeiMobile.Visible = False

        cbActive.Checked = True
        cbActive.DataBind()
    End Sub

    Private Sub Add()
        Dim oCollector As New Parameter.Collector
        Dim oClassAddress As New Parameter.Address
        Dim oClassPersonal As New Parameter.Personal

        If CStr(Session("BranchIDCollection")) <> "" Then
            Me.BranchID = CStr(Session("BranchIDCollection"))
        End If

        With oCollector
            .strConnection = GetConnectionString
            .CGID = Me.CGID.Trim
            .BranchId = Me.BranchID

            .CollectorID = cboCollectorID.SelectedItem.Value.Trim.Trim
            '.EmployeeID = UCEmployee.EmployeeID.Trim
            '.EmployeeName = UCEmployee.EmployeeName.Trim

            .CollectorType = cboCollectorType.SelectedItem.Value
            .Supervisor = Me.SPVId

            .Active = CStr(IIf(cbActive.Checked = True, "Y", "N"))
            Context.Trace.Write("Add - CollectorID = " & .CollectorID)
            Context.Trace.Write("Add - Collector Type = " & .CollectorType)
            Context.Trace.Write("Add - Supervisor = " & .Supervisor)
            .PasswordMobile = txtPasswordMobile.Text.Trim
            .ImeiMobile = txtImeiMobile.Text.Trim
        End With

        Try
            m_Collector.CollectorAdd(oCollector)
            ShowMessage(lblMessage, "Tambah Data Berhasil ", False)

        Catch ex As Exception
            'lblMessage.Text = ex.Message
            'lblMessage.Text = MessageHelper.MESSAGE_INSERT_FAILED            
            ShowMessage(lblMessage, "Collector ini sudah ada dengan  ID Collector = " & oCollector.CollectorID, True)

        End Try

    End Sub

    Private Sub edit(ByVal CGID As String)
        Context.Trace.Write("editbyvalCGID")

        Dim oCollector As New Parameter.Collector
        Dim oClassAddress As New Parameter.Address
        Dim oClassPersonal As New Parameter.Personal

        If CStr(Session("BranchIDCollection")) <> "" Then
            Me.BranchID = CStr(Session("BranchIDCollection"))
        End If

        With oCollector
            .strConnection = GetConnectionString
            .CGID = Me.CGID.Trim
            .BranchId = Me.BranchID
            .CollectorID = lblCollectorID.Text.Trim
            '.EmployeeID = Me.EmployeeID
            '.EmployeeID = UCEmployee.EmployeeID.Trim
            '.EmployeeName = UCEmployee.EmployeeName.Trim
            'If Trim(.EmployeeID) = "" Then
            '    .EmployeeID = Me.EmployeeID
            'End If

            .CollectorType = cboCollectorType.SelectedItem.Value
            .Supervisor = Me.SPVId

            '.CollectorType = UcCombo2Level.CollectorTypeID.Trim
            '.Supervisor = UcCombo2Level.SupervisorID.Trim

            Context.Trace.Write("edit - CollectorID = " & .CollectorID)
            Context.Trace.Write("edit - Collector Type = " & .CollectorType)
            Context.Trace.Write("edit - Supervisor = " & .Supervisor)

            .Active = CStr(IIf(cbActive.Checked = True, "Y", "N"))
            .OldCollectorType = Me.CollectorType.Trim
            .PasswordMobile = txtPasswordMobile.Text.Trim
            .ImeiMobile = txtImeiMobile.Text.Trim
        End With


        Try
            m_Collector.CollectorEdit(oCollector)
            ShowMessage(lblMessage, "Simpan Data Berhasil ", False)
        Catch ex As Exception
            'lblMessage.Text = ex.Message & ex.StackTrace
            'lblMessage.Text = MessageHelper.MESSAGE_INSERT_FAILED            
            ShowMessage(lblMessage, "Collector ini sudah ada dengan ID Collector = " & oCollector.CollectorID, True)
        End Try
    End Sub

    Private Sub dtgCollector_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgCollector.ItemCommand
        Me.CGID = e.Item.Cells(4).Text.Trim
        Me.CollectorID = e.Item.Cells(5).Text.Trim
        Me.CollectorName = e.Item.Cells(7).Text.Trim
        Me.CollectorType = e.Item.Cells(10).Text.Trim

        Select Case e.CommandName
            Case "Edit"
                Context.Trace.Write("e.CommandName = Edit")
                If checkFeature(Me.Loginid, Me.FormID, "Edit", Me.AppId) Then
                    If sessioninvalid() Then
                        Exit Sub
                    End If
                End If


                Dim oCollector As New Parameter.Collector
                With oCollector
                    .strConnection = getConnectionString
                    .CGID = Me.CGID
                    .CollectorID = Me.CollectorID
                    .CollectorType = Me.CollectorType.Trim
                    Me.ActionAddEdit = "EDIT"
                End With

                PanelAllFalse()
                viewbyID(oCollector)
                pnlAddEdit.Visible = True

            Case "Delete"
                If checkFeature(Me.Loginid, Me.FormID, "Del", Me.AppId) Then
                    If sessioninvalid() Then
                        Exit Sub
                    End If
                End If

                If CStr(Session("BranchIDCollection")) <> "" Then
                    Me.BranchID = CStr(Session("BranchIDCollection"))
                End If

                Dim oCollector As New Parameter.Collector
                With oCollector
                    .strConnection = getConnectionString
                    .CGID = Me.CGID
                    .BranchId = Me.BranchID
                    .CollectorID = Me.CollectorID
                    .CollectorType = Me.CollectorType.Trim
                End With

                Try
                    m_Collector.CollectorDelete(oCollector)                    
                    ShowMessage(lblMessage, "Hapus Data Berhasil ", False)
                Catch ex As Exception                    
                    ShowMessage(lblMessage, ex.Message, True)

                Finally
                    pnlAddEdit.Visible = False
                    BindGridCollector("ALL", "")
                    pnlList.Visible = True

                End Try

            Case "ZipCode"

                Response.Redirect("CollectorViewZipCode.aspx?cgid=" & Me.CGID & "&CollectorID=" & Me.CollectorID & "&CollectorName=" & Me.CollectorName & "&CallerPage=CollectorSetting.aspx")

        End Select
    End Sub

    Private Sub BtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        cboSearchBy.SelectedIndex = 0
        TxtSearchByValue.Text = ""        

        BindGridCollector("ALL", "")
        pnlList.Visible = True
    End Sub

    Function LinkTo(ByVal strCGID As String) As String
        Return "javascript:OpenViewCollectionGroup('" & strCGID & "')"
    End Function

    Function LinkToCollector(ByVal strCollectorID As String, ByVal cgid As String) As String
        Return "javascript:OpenViewCollector('" & strCollectorID & "','" & cgid & "')"
    End Function

    Private Sub dtgCollector_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgCollector.ItemDataBound

        Dim imbDelete As ImageButton

        If e.Item.ItemIndex >= 0 Then

            imbDelete = CType(e.Item.FindControl("ImbDelete"), ImageButton)
            imbDelete.Attributes.Add("onclick", "return fConfirm()")
        End If

    End Sub

    Private Sub imgCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        InitialDefaultPanel()
        BindGridCollector(Me.SearchBy, Me.SortBy)
        pnlList.Visible = True
    End Sub

    Private Sub imbSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSave.Click

        '-------------------------------------------------------------------
        'Periksa apakah kombinasi CollectorType dan Supervisor sudah valid ?
        '-------------------------------------------------------------------
        Dim Colltype As String
        Dim SPVType As String
        Dim valid As Boolean

        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(GetConnectionString)
        Dim objReader As SqlDataReader
        Dim sum As Integer

        Colltype = cboCollectorType.SelectedItem.Value.Trim

        If cboSPVId.SelectedItem.Value.Trim = "0" Or cboSPVId.SelectedItem.Text.Trim = "Select One" Then
            Me.SPVId = ""
            Context.Trace.Write("SPV 1")
        Else
            Me.SPVId = cboSPVId.SelectedItem.Text.Trim
            Context.Trace.Write("SPV 2")
        End If
        Context.Trace.Write("imbSave_Click - CoolType =" & Colltype)
        '----------------------------------------------------------------------------------------------
        'Dapetin Collection type Id dari Supervisor yasng dipilih buat validasi save
        '----------------------------------------------------------------------------------------------
        Dim objCommand1 As New SqlCommand

        Dim objReader1 As SqlDataReader
        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objCommand1.CommandType = CommandType.StoredProcedure
            objCommand1.CommandText = "spGetCollIdSPV"
            objCommand1.Connection = objConnection
            objCommand1.Parameters.Add("@SPVName", SqlDbType.Char, 10).Value = Me.SPVId.Trim
            objReader1 = objCommand1.ExecuteReader()
            If objReader1.Read Then
                SPVType = objReader1.Item("CollType").ToString.Trim
            End If
            objReader1.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
            objReader1.Close()
        End Try
        '----------------------------------------------------------------------------------------------
        '----------------------------------------------------------------------------------------------
        valid = True
        '-- rmcs   If Colltype = "D" And SPVType <> "DS" Then valid = False
        '-- rmcs   If Colltype = "CL" And SPVType <> "COL" Then valid = False
        If Colltype = "EI" And SPVType <> "COM" Then valid = False

        If valid = False Then
            Select Case Colltype
                '-- rmcs  Case "D"
                '-- rmcs    lblMessage.Text = "Please select Supervisor with type DeskColl Supervisor"
                '-- rmcs Case "CL"
                '-- rmcs lblMessage.Text = "Please select Supervisor with type Collection Officer Lancar"
                Case "EI"                    
                    ShowMessage(lblMessage, "Harap pilih Supervisor dengan Jenis Collection Officer Macet", True)
            End Select
            lblMessage.Visible = True
            Exit Sub
        End If


        '------------------------------------------------------------------------

        Context.Trace.Write("Me.ActionAddEdit = " & Me.ActionAddEdit)
        Select Case Me.ActionAddEdit

            Case "ADD"
                Context.Trace.Write("Me.ActionAddEdit -> ADD")
                PanelAllFalse()
                Add()
                Me.SearchBy = "all"
                Me.SortBy = ""
                BindGridCollector(Me.SearchBy, Me.SortBy)
                pnlSearch.Visible = True
                pnlList.Visible = True
            Case "EDIT"
                Context.Trace.Write("Me.ActionAddEdit -> EDIT")
                PanelAllFalse()
                Me.SearchBy = "all"
                Me.SortBy = ""
                If cboCollectorType.SelectedItem.Value.Trim <> colltype1 Or cbActive.Checked.ToString.Trim = "False" Then

                    Try
                        If objConnection.State = ConnectionState.Closed Then objConnection.Open()
                        objCommand.CommandType = CommandType.StoredProcedure
                        objCommand.CommandText = "spCheckCollType"
                        objCommand.Connection = objConnection
                        objCommand.Parameters.Add("@Colltype1", SqlDbType.Char, 10).Value = colltype1
                        objCommand.Parameters.Add("@CGId", SqlDbType.Char, 10).Value = Me.CGID
                        objReader = objCommand.ExecuteReader()
                        If objReader.Read Then
                            sum = CType(objReader.Item("counter"), Integer)
                        End If
                        objReader.Close()
                    Catch ex As Exception
                        Response.Write(ex.Message)
                        objReader.Close()
                    End Try
                    If sum = 1 Then                        
                        ShowMessage(lblMessage, "Tidak bisa Edit, karena posisi sebelumnya belum diisi", True)
                        BindGridCollector(Me.SearchBy, Me.SortBy)
                        pnlSearch.Visible = True
                        pnlList.Visible = True
                        Exit Sub


                    End If



                End If
                edit(Me.CGID)
                BindGridCollector(Me.SearchBy, Me.SortBy)
                pnlSearch.Visible = True
                pnlList.Visible = True


        End Select
    End Sub

    Private Sub imbPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonPrint.Click
        Response.Redirect("report/CollectorRpt.aspx")
    End Sub

    Private Sub GetCollectorCombo()
        Context.Trace.Write("GetCollectorCombo")
        Dim oCollectorList As New Parameter.Collector
        Dim oCollector As New Parameter.Collector
        Dim strconn As String = getConnectionString()
        Dim dt As New DataTable

        With oCollector
            .strConnection = getConnectionString()
        End With

        oCollectorList = m_Collector.GetCollectorCombo(oCollector)
        dt = oCollectorList.ListData
        cboCollectorID.DataSource = dt
        cboCollectorID.DataValueField = "LoginID"
        cboCollectorID.DataTextField = "FullName"
        cboCollectorID.DataBind()

        cboCollectorID.Items.Insert(0, "Select One")
        cboCollectorID.Items.Item(0).Value = "0"

        cboCollectorID.SelectedIndex = 0

    End Sub

    Private Sub getComboCollectorType()
        Dim Coll As New Parameter.Collector
        Dim CollList As New Parameter.Collector
        Dim CollSPV As New Parameter.Collector
        Dim CollListSPV As New Parameter.Collector
        Dim dt As New DataTable
        Dim dtSPV As New DataTable

        Context.Trace.Write("getComboCollectorType")
        With Coll
            .strConnection = getconnectionstring()
            .CGID = Me.GroubDbID
            .WhereCond = ""
        End With

        CollList = m_Collector.GetCollectorTypeCombo(Coll)
        dt = CollList.ListData


        cboCollectorType.DataTextField = "Description"
        cboCollectorType.DataValueField = "CollectorType"
        cboCollectorType.DataSource = dt
        cboCollectorType.DataBind()
        cboCollectorType.Items.Insert(0, "Select One")
        cboCollectorType.Items(0).Value = "0"
        cboCollectorType.SelectedIndex = 0

        Context.Trace.Write("getComboCollectorType")
        With CollSPV
            .strConnection = GetConnectionString()
            .CGID = Me.GroubDbID
            .WhereCond = " id in ('CH','DS','COL','COM') "
        End With

        CollListSPV = m_Collector.GetCollectorTypeCombo(CollSPV)
        dtSPV = CollListSPV.ListData

        cboColTypeSPV.DataTextField = "Description"
        cboColTypeSPV.DataValueField = "CollectorType"
        cboColTypeSPV.DataSource = dtSPV
        cboColTypeSPV.DataBind()
        cboColTypeSPV.Items.Insert(0, "Select One")
        cboColTypeSPV.Items(0).Value = "0"
        cboColTypeSPV.SelectedIndex = 0
    End Sub


    Protected Function CBOCollTypeonChange() As String
        Return "CBOCollTypeonChange();"
    End Function

    Sub cboColTypeSPV_IndexChanged()
        Dim Coll As New Parameter.Collector
        Dim CollList As New Parameter.Collector
        Dim dt As New DataTable

        If cboColTypeSPV.SelectedValue.Trim = "0" Then
            cboSPVId.Items.Insert(0, "Select One")
            cboSPVId.Items(0).Value = "0"
            cboSPVId.SelectedIndex = 0
        Else
            With Coll
                .strConnection = GetConnectionString()
                .CGID = Me.GroubDbID
                .WhereCond = " CollectorType ='" & cboColTypeSPV.SelectedValue.Trim & "'"
            End With
            Context.Trace.Write("Jalankan Method GetSupervisorCombo")

            CollList = m_Collector.GetSupervisorCombo(Coll)
            dt = CollList.ListData

            cboSPVId.DataTextField = "name"
            cboSPVId.DataValueField = "id"
            cboSPVId.DataSource = dt
            cboSPVId.DataBind()
            cboSPVId.Items.Insert(0, "Select One")
            cboSPVId.Items(0).Value = "0"
            cboSPVId.SelectedIndex = 0
        End If
       
    End Sub

    Sub defaultCboSPVID()
        cboSPVId.Items.Insert(0, "Select One")
        cboSPVId.Items(0).Value = "0"
        cboSPVId.SelectedIndex = 0
    End Sub
End Class