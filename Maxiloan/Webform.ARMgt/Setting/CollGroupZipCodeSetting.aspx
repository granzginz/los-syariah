﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CollGroupZipCodeSetting.aspx.vb"
    Inherits="Maxiloan.Webform.ARMgt.CollGroupZipCodeSetting" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>CollGroupZipCodeSetting</title>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;

        function fConfirm() {
            if (window.confirm("Apakah yakin mau hapus data ini ? "))
                return true;
            else
                return false;
        }
        function fback() {
            history.go(-1);
            return false;
        }

        function OpenViewCollectionGroup(pCGID) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.ARMgt/Setting/CollectionGroupView.aspx?CGID=' + pCGID, 'CollectionGroupView', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenViewCollector(pCollID, pCGID) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.ARMgt/Setting/CollectorView.aspx?CollectorID=' + pCollID + '&CGID=' + pCGID, 'CollectorView', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">           
                <h3>
                    ALOKASI KODE POS COLLECTION GROUP
                </h3>
            </div>
        </div>  
        <asp:Panel ID="PnlSearch" runat="server">
        <div class="form_box">
	        <div class="form_single">
                <label>Kota</label>
                <asp:DropDownList ID="cboCity" runat="server" Width="144px" >
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic" CssClass="validator_general"
                InitialValue="0" ControlToValidate="cboCity" ErrorMessage="Harap pilih Kota"></asp:RequiredFieldValidator>
	        </div>
        </div>
        <div class="form_button">   
            <asp:Button ID="ButtonSearch" runat="server"  Text="Search" CssClass ="small button blue">
            </asp:Button>&nbsp;
            <asp:Button ID="ButtonReset" runat="server"  Text="Reset" CssClass ="small button gray"
                CausesValidation="False"></asp:Button>
	    </div>               
        </asp:Panel>
        <asp:Panel ID="PnlSearchDetail" runat="server">
        <div class="form_box_title">
            <div class="form_single">              
                <h4>
                    ALOKASI KODE POS COLLECTION GROUP
                </h4>
            </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Kota</label>
                <asp:Label ID="lblCity" runat="server" Width="435px"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Cari Berdasarkan</label>
                <asp:DropDownList ID="cboSearchBy" runat="server" Width="109px" >
                    <asp:ListItem Value="0">Select One</asp:ListItem>
                    <asp:ListItem Value="Kelurahan">Kelurahan</asp:ListItem>
                    <asp:ListItem Value="Kecamatan">Kecamatan</asp:ListItem>
                    <asp:ListItem Value="ZipCode">Kode Pos</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearchByValue" runat="server" ></asp:TextBox>
	        </div>
        </div> 
        <div class="form_box">
	        <div class="form_single">
                <label>Kode Pos Belum Alokasi</label>
                <asp:CheckBox ID="cbUnZipCode" runat="server"></asp:CheckBox>
	        </div>  
        </div> 
        <div class="form_button">
            <asp:Button ID="ButtonSearchDetail" runat="server"  Text="Search" CssClass ="small button blue"
                CausesValidation="False"></asp:Button>&nbsp;
            <asp:Button ID="ButtonResetDetail" runat="server"  Text="Reset" CssClass ="small button gray"
                CausesValidation="False"></asp:Button>
	    </div>
        <div class="form_box_title">
            <div class="form_single">              
                <h4>
                    ALOKASI KODE POS COLLECTION GROUP DEFAULT
                </h4>
            </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Default Collection Group</label>
                <asp:DropDownList ID="cboDefaultCG" runat="server" >
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="Dynamic" CssClass="validator_general"
                InitialValue="0" ControlToValidate="cboDefaultCG" ErrorMessage="Harap pilih Default Collection Group"></asp:RequiredFieldValidator>
	        </div>
        </div>    
        <div class="form_box">
	        <div class="form_single">
                <label>Default Kriteria</label>
                <asp:DropDownList ID="cboCriteria" runat="server" >
                    <asp:ListItem Value="All">Semua Kode Pos</asp:ListItem>
                    <asp:ListItem Value="UnZipCode">Kode Pos Belum Alokasi</asp:ListItem>
                </asp:DropDownList>
                of this page
	        </div>
        </div> 
        <div class="form_button">
            <asp:Button ID="ButtonApply" runat="server"  Text="Apply" CssClass ="small button blue">
            </asp:Button>
	    </div>                   
        </asp:Panel>
        <asp:Panel ID="pnlList"  runat="server">
        <div class="form_box_title">
            <div class="form_single">              
                <h4>
                    DAFTAR ALOKASI KODE POS COLLECTION GROUP
                </h4>
            </div>
        </div>
        <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgCollZipCode" runat="server" HorizontalAlign="Center" Width="100%"
                        OnSortCommand="Sorting" CssClass="grid_general" BorderStyle="None" BorderWidth="0" AutoGenerateColumns="False" AllowSorting="True">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:BoundColumn DataField="ZIPCODE" SortExpression="ZIPCODE" HeaderText="KODE POS">                                
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="KELURAHAN" SortExpression="KELURAHAN" HeaderText="KELURAHAN">                                
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="KECAMATAN" SortExpression="KECAMATAN" HeaderText="KECAMATAN">                                
                            </asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="COLLECTION GROUP">
                                <ItemTemplate>
                                    <asp:DropDownList ID="cboCG" runat="server">
                                    </asp:DropDownList>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn Visible="False" DataField="CGID"></asp:BoundColumn>
                            <asp:TemplateColumn Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblZipCode" runat="server" Visible="False">
												<%# container.dataitem("ZipCode")%>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblKelurahan" runat="server" Visible="False">
												<%# container.dataitem("Kelurahan")%>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblKecamatan" runat="server" Visible="False">
												<%# container.dataitem("Kecamatan")%>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                </asp:DataGrid>        
                <div class="button_gridnavigation">
	                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
		                CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
		                CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
		                CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
		                CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
	                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
		                EnableViewState="False"></asp:Button>
	                <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage"
				        MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer"
				        CssClass="validator_general"></asp:RangeValidator>
			        <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
				        ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general">
			        </asp:RequiredFieldValidator>              
                </div>
                <div class="label_gridnavigation">
	                <asp:Label ID="lblPage" runat="server"></asp:Label>of
	                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
	                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                </div>
            </div>  
        </div>   
        </div> 
        <div class="form_button">
            <asp:Button ID="Buttonsave" runat="server"  Text="Save" CssClass ="small button blue"
                CausesValidation="False"></asp:Button>&nbsp;            
            <asp:Button ID="ButtonCancel" runat="server"  Text="Cancel" CssClass ="small button gray"
                CausesValidation="False"></asp:Button>&nbsp;            
            <asp:Button ID="ButtonPrint" runat="server"  Text="Print" CssClass ="small button blue"
                CausesValidation="False"></asp:Button>
	    </div>        
    </asp:Panel>
    </form>
</body>
</html>
