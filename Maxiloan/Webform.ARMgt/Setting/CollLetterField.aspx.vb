﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.General.CommonCookiesHelper
Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class CollLetterField
    Inherits Maxiloan.Webform.WebBased
    Private oCollLetter As New Parameter.CollLetter
    Private m_controller As New CollLetterController

    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not Me.IsPostBack Then
            txtPage.Text = "1"
            Me.SearchBy = ""
            Me.SortBy = ""
            BindGridEntity()
            ButtonBack.Attributes.Add("onClick", "javascript:Close()")
        End If

    End Sub
    Sub BindGridEntity()
        With oCollLetter
            .WhereCond = Me.SearchBy
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = Me.SortBy
            .strConnection = GetConnectionString
        End With
        oCollLetter = m_controller.CollLetterFieldPaging(oCollLetter)
        recordCount = oCollLetter.TotalRecord
        dtgFieldName.DataSource = oCollLetter.ListData
        dtgFieldName.CurrentPageIndex = 0
        dtgFieldName.DataBind()
        PagingFooter()
    End Sub

#Region "Navigation "
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridEntity()
    End Sub
#End Region
#Region "Paging Footer"

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then            
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            'rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            'rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblRecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
#End Region
#Region "Go Page"
    Protected Sub btnPageNumb_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPageNumb.Click
        If IsNumeric(txtpage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtpage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtpage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                BindGridEntity()
            End If
        End If
    End Sub

#End Region
#Region "Sorting"

    Public Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        BindGridEntity()
    End Sub

#End Region

    Private Sub imbBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonBack.Click

    End Sub

End Class