﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class AssetCheckList
    Inherits Maxiloan.Webform.WebBased

#Region "Property"
    Private Property Mode() As String
        Get
            Return CType(ViewState("Mode"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Mode") = Value
        End Set
    End Property
#End Region

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.AssetCheckList
    Private oController As New AssetCheckListController
    Private oClassAddress As New Parameter.Address
    Private oClassPersonal As New Parameter.Personal
    Protected WithEvents oSearchBy As UcSearchBy
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            InitialDefaultPanel()
            Me.FormID = "CollCheckList"
            oSearchBy.ListData = "ASSETTYPEID,Asset Type-CHECKLISTID,Check List Id-DESCRIPTION,Check List Description"
            oSearchBy.BindData()
            If CheckForm(Me.Loginid, Me.FormID, "Maxiloan") Then
                Me.SearchBy = ""
                Me.SortBy = ""
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub
    Private Sub InitialDefaultPanel()
        pnlAdd.Visible = False
        pnlList.Visible = True
        'pnlDtGrid.Visible = False
        'pnlEditDetail.Visible = False
        'pnlView.Visible = False        
    End Sub
    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        Dim intloop As Integer
        Dim hypID As HyperLink

        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oCustomClass = oController.AssetCheckList(oCustomClass)

        DtUserList = oCustomClass.ListAssetCheckList
        DvUserList = DtUserList.DefaultView
        recordCount = oCustomClass.TotalRecord
        DvUserList.Sort = Me.SortBy
        dtgAssetCheckList.DataSource = DvUserList

        Try
            dtgAssetCheckList.DataBind()
        Catch
            dtgAssetCheckList.CurrentPageIndex = 0
            dtgAssetCheckList.DataBind()
        End Try
        PagingFooter()
        pnlList.Visible = True
        pnlDtGrid.Visible = True
        'pnlEditDetail.Visible = False        
    End Sub
    Private Sub dtgAssetCheckList_ItemCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgAssetCheckList.ItemCommand
        Select Case e.CommandName
            Case "EDIT"
                If CheckFeature(Me.Loginid, Me.FormID, "EDIT", "Maxiloan") Then
                    Me.Mode = "EDIT"
                    Dim lbAssetTypeID, lbCheckListID, lbDescription, lbIsYesNoQuestion, lblIsActive As Label
                    lbAssetTypeID = CType(dtgAssetCheckList.Items(e.Item.ItemIndex).FindControl("lblAssetTypeIDdtg"), Label)
                    lbCheckListID = CType(dtgAssetCheckList.Items(e.Item.ItemIndex).FindControl("lblCheckListIDdtg"), Label)
                    lbDescription = CType(dtgAssetCheckList.Items(e.Item.ItemIndex).FindControl("lblDescription"), Label)
                    lbIsYesNoQuestion = CType(dtgAssetCheckList.Items(e.Item.ItemIndex).FindControl("lblIsYNQuestion"), Label)
                    lblIsActive = CType(dtgAssetCheckList.Items(e.Item.ItemIndex).FindControl("lblIsActive"), Label)

                    pnlList.Visible = False
                    pnlDtGrid.Visible = False
                    pnlAdd.Visible = True
                    cboAssetType.Visible = False
                    txtCheckListID.Visible = False
                    lblAssetTypeID.Text = lbAssetTypeID.Text
                    lblCheckListID.Text = lbCheckListID.Text
                    txtDescription.Text = lbDescription.Text
                    If lbIsYesNoQuestion.Text = "YesNo" Then
                        rdoQuestioType.Items.FindByValue("1").Selected = True
                        rdoQuestioType.Items.FindByValue("0").Selected = False
                    Else
                        rdoQuestioType.Items.FindByValue("1").Selected = False
                        rdoQuestioType.Items.FindByValue("0").Selected = True
                    End If
                    If lblIsActive.Text = "Yes" Then
                        chkIsActive.Checked = True
                    Else
                        chkIsActive.Checked = False
                    End If
                    lblTitle.Text = "ASSET CHECK LIST  - EDIT"
                    'btnCancelAdd.Attributes.Add("onclick", "return fback()")
                End If
            Case "DELETE"
                If CheckFeature(Me.Loginid, Me.FormID, "DEL", "Maxiloan") Then
                    Dim lbAssetTypeID, lbCheckListID As Label
                    lbAssetTypeID = CType(dtgAssetCheckList.Items(e.Item.ItemIndex).FindControl("lblAssetTypeIDdtg"), Label)
                    lbCheckListID = CType(dtgAssetCheckList.Items(e.Item.ItemIndex).FindControl("lblCheckListIDdtg"), Label)
                    With oCustomClass
                        .strConnection = GetConnectionString()
                        .AssetTypeID = lbAssetTypeID.Text
                        .CheckListID = lbCheckListID.Text
                    End With
                    Dim strError As String
                    strError = oController.AssetCheckListDelete(oCustomClass)
                    If strError = "" Then
                        ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, False)
                    Else
                        ShowMessage(lblMessage, strError, True)
                    End If
                    DoBind(Me.SearchBy, Me.SortBy)
                End If
        End Select
    End Sub
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub
#End Region
    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Buttonsearch.Click
        If oSearchBy.Text.Trim <> "" Then
            Me.SearchBy = oSearchBy.ValueID & " like '%" & oSearchBy.Text.Trim.Replace("'", "''") & "%'"
        Else
            Me.SearchBy = ""
        End If
        pnlDtGrid.Visible = True
        pnlList.Visible = True
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
    Private Sub DtgCasesSetting_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgAssetCheckList.ItemDataBound
        Dim m As Int32
        Dim imbDelete As ImageButton
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imbDelete"), ImageButton)
            imbDelete.Attributes.Add("onclick", "return fConfirm()")
        End If
    End Sub
    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        Me.SearchBy = ""
        oSearchBy.Text = ""
        oSearchBy.BindData()
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
    Private Sub btnSaveAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSaveAdd.Click
        pnlList.Visible = True
        pnlDtGrid.Visible = True
        pnlAdd.Visible = False
        Dim strError As String
        'If Page.IsValid Then
        Select Case Mode
            Case "ADD"
                With oCustomClass
                    .strConnection = GetConnectionString()
                    .AssetTypeID = cboAssetType.SelectedItem.Value
                    .CheckListID = txtCheckListID.Text
                    .Description = txtDescription.Text
                    If rdoQuestioType.SelectedItem.Value = "1" Then
                        .isYNQuestion = "Y"
                        .isQTYQuestion = "N"
                    Else
                        .isYNQuestion = "N"
                        .isQTYQuestion = "Y"
                    End If
                    If chkIsActive.Checked = True Then
                        .IsActive = "1"
                    Else
                        .IsActive = "0"
                    End If
                    .LoginId = Me.Loginid
                End With
                strError = oController.AssetCheckListAdd(oCustomClass)
                If strError <> "" Then
                    ShowMessage(lblMessage, strError, True)
                Else
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_SUCCESS, False)
                    DoBind(Me.SearchBy, Me.SortBy)
                    If Not (Me.Cache.Item("DESCRIPTION")) Is Nothing Then
                        Me.Cache.Remove("DESCRIPTION")
                    End If
                End If
            Case "EDIT"
                With oCustomClass
                    .strConnection = GetConnectionString()
                    .AssetTypeID = lblAssetTypeID.Text
                    .CheckListID = lblCheckListID.Text
                    .Description = txtDescription.Text
                    If rdoQuestioType.SelectedItem.Value = "1" Then
                        .isYNQuestion = "Y"
                        .isQTYQuestion = "N"
                    Else
                        .isYNQuestion = "N"
                        .isQTYQuestion = "Y"
                    End If
                    If chkIsActive.Checked = True Then
                        .IsActive = "1"
                    Else
                        .IsActive = "0"
                    End If
                    .LoginId = Me.Loginid
                End With
                strError = oController.AssetCheckListEdit(oCustomClass)
                If strError <> "" Then
                    ShowMessage(lblMessage, strError, True)
                Else
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
                    DoBind(Me.SearchBy, Me.SortBy)
                    If Not (Me.Cache.Item("DESCRIPTION")) Is Nothing Then
                        Me.Cache.Remove("DESCRIPTION")
                    End If
                End If
        End Select
        'End If
        'DoBind(Me.SearchBy, Me.SortBy)
    End Sub
    Private Sub ImgAddNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonAddNew.Click
        If CheckFeature(Me.Loginid, Me.FormID, "ADD", "Maxiloan") Then
            Dim oDataTable As New DataTable
            pnlAdd.Visible = True
            pnlList.Visible = False
            pnlDtGrid.Visible = False
            Me.Mode = "ADD"
            lblTitle.Text = "ASSET CHECK LIST - ADD"
            lblAssetTypeID.Text = ""
            lblCheckListID.Text = ""
            cboAssetType.Visible = True
            With oCustomClass
                .strConnection = GetConnectionString()
            End With
            oCustomClass = oController.AssetCheckListLookUp(oCustomClass)
            oDataTable = oCustomClass.ListAssetCheckList
            With cboAssetType
                .DataSource = oDataTable
                .DataTextField = "Description"
                .DataValueField = "AssetTypeID"
                .DataBind()
                .Items.Insert(0, "Select One")
                .Items(0).Value = ""
            End With
            txtCheckListID.Text = ""
            txtCheckListID.Visible = True
            txtDescription.Text = ""
            chkIsActive.Checked = True
            'btnCancelAdd.Attributes.Add("onclick", "return fback()")
        End If
    End Sub

    Private Sub imbPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonPrint.Click
        If SessionInvalid() Then
            Exit Sub
        End If
        If CheckFeature(Me.Loginid, Me.FormID, "PRINT", Me.AppId) Then
            Dim cookie As HttpCookie = Request.Cookies(Me.FormID)
            If Not cookie Is Nothing Then
                cookie.Values("Where") = Me.SearchBy
                cookie.Values("SortBy") = Me.SortBy
                Response.AppendCookie(cookie)
            Else
                Dim cookieNew As New HttpCookie("AssetCheck")
                cookieNew.Values.Add("Where", Me.SearchBy)
                cookieNew.Values.Add("SortBy", Me.SortBy)
                Response.AppendCookie(cookieNew)
            End If
            Response.Redirect("../setting/Report/AssetCheckListRpt.aspx")
        End If
    End Sub

    Private Sub btnCancelAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonCancelAdd.Click
        'pnlAdd.Visible = False
        'pnlList.Visible = False
        lblMessage.Visible = False
        InitialDefaultPanel()
        pnlDtGrid.Visible = True
    End Sub


End Class