﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class PemberiKuasaSKE
    Inherits Maxiloan.Webform.WebBased
#Region "Property"
    Private Property Id() As String
        Get
            Return CType(ViewState("Id"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Id") = Value
        End Set
    End Property
#End Region

#Region "Constanta"
    Dim m_ActionResult As New LokasiAssetController
    Dim m_Employee As New EmployeeController

    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.Collector
    Private oController As New CollectorController
    Protected WithEvents oSearchBy As UcSearchBy
    Protected WithEvents oUcCompanyAdress As UcCompanyAddress
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not Me.IsPostBack Then
            InitialDefaultPanel()
            Me.FormID = "PKSKE"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.SearchBy = ""
                Me.SortBy = ""
                oSearchBy.ListData = "EmployeeID,ID Karyawan-EmployeeName,Nama Karyawan"
                oSearchBy.BindData()
                If Request("cond") <> "" Then
                    Me.SearchBy = Request("cond")
                Else
                    Me.SearchBy = ""
                End If
                Me.SortBy = ""
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub

    Private Sub InitialDefaultPanel()
        pnlAdd.Visible = False
        pnlList.Visible = True
    End Sub

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView

        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oCustomClass = oController.GetPemberiKuasaSKEList(oCustomClass)

        With oCustomClass
            lblrecord.Text = CType(.TotalRecord, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecord
        End With

        DtUserList = oCustomClass.listPemberiKuasaSKE
        DvUserList = DtUserList.DefaultView
        recordCount = oCustomClass.TotalRecord
        DvUserList.Sort = Me.SortBy
        dtgPemberiKuasaSKE.DataSource = DvUserList

        Try
            dtgPemberiKuasaSKE.DataBind()
        Catch
            dtgPemberiKuasaSKE.CurrentPageIndex = 0
            dtgPemberiKuasaSKE.DataBind()
        End Try

        PagingFooter()
    End Sub
    Private Sub dtgPemberiKuasaSKE_ItemCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPemberiKuasaSKE.ItemCommand
        Select Case e.CommandName
            Case "EDIT"
                'If CheckFeature(Me.Loginid, Me.FormID, "EDIT", "Maxiloan") Then
                Me.Id = "EDIT"
                Dim lblEmployeeID, lblEmployeeName, lblEmployeePosition As Label
                FillCombo()
                lblEmployeeID = CType(dtgPemberiKuasaSKE.Items(e.Item.ItemIndex).FindControl("lblEmployeeID"), Label)
                lblEmployeeName = CType(dtgPemberiKuasaSKE.Items(e.Item.ItemIndex).FindControl("lblEmployeeName"), Label)
                lblEmployeePosition = CType(dtgPemberiKuasaSKE.Items(e.Item.ItemIndex).FindControl("lblEmployeePosition"), Label)
                cboPosition.SelectedIndex = cboPosition.Items.IndexOf(cboPosition.Items.FindByText(CStr(lblEmployeePosition.Text)))

                pnlList.Visible = False
                pnlDtGrid.Visible = False
                pnlAdd.Visible = True
                txtEmployeeId.Text = lblEmployeeID.Text
                txtEmployeeId.Enabled = False
                txtEmployeeName.Text = lblEmployeeName.Text
                lblTitle.Text = "PEMBERI KUASA SKE - EDIT"
                'End If
            Case "DELETE"
                'If CheckFeature(Me.Loginid, Me.FormID, "DEL", "Maxiloan") Then
                Dim lblEmployeeID As Label
                lblEmployeeID = CType(dtgPemberiKuasaSKE.Items(e.Item.ItemIndex).FindControl("lblEmployeeID"), Label)
                With oCustomClass
                    .strConnection = GetConnectionString()
                    .EmployeeID = lblEmployeeID.Text
                End With
                Dim strError As String
                strError = oController.PemberiKuasaSKEDelete(oCustomClass)
                If strError = "" Then
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, False)
                Else
                    ShowMessage(lblMessage, strError, True)
                End If
                DoBind(Me.SearchBy, Me.SortBy)
                'End If
        End Select
    End Sub
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "EmployeeID") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + "EmployeeID"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub
#End Region
    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Buttonsearch.Click
        If oSearchBy.Text.Trim <> "" Then
            Me.SearchBy = oSearchBy.ValueID & " like '%" & oSearchBy.Text.Trim.Replace("'", "''") & "%'"
        Else
            Me.SearchBy = ""
        End If
        pnlDtGrid.Visible = True
        pnlList.Visible = True
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
    Private Sub DtgCasesSetting_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPemberiKuasaSKE.ItemDataBound
        Dim m As Int32
        Dim imbDelete As ImageButton
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imbDelete"), ImageButton)
            imbDelete.Attributes.Add("onclick", "return fConfirm()")
        End If
    End Sub
    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        Me.SearchBy = ""
        oSearchBy.Text = ""
        oSearchBy.BindData()
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
    Private Sub btnSaveAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSaveAdd.Click
        pnlList.Visible = True
        pnlDtGrid.Visible = True
        pnlAdd.Visible = False

        Dim strError As String
        Dim oCollector As New Parameter.Collector

        Select Case Id
            Case "ADD"
                With oCollector
                    .strConnection = GetConnectionString()
                    .EmployeeID = txtEmployeeId.Text
                    .EmployeeName = txtEmployeeName.Text
                    .Position = cboPosition.SelectedItem.Value.Trim
                End With

                strError = oController.PemberiKuasaSKEAdd(oCollector)
                If strError <> "" Then
                    ShowMessage(lblMessage, strError, True)
                Else
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_SUCCESS, False)
                    DoBind(Me.SearchBy, Me.SortBy)
                    If Not (Me.Cache.Item("ID")) Is Nothing Then
                        Me.Cache.Remove("ID")
                    End If
                End If
            Case "EDIT"
                With oCollector
                    .strConnection = GetConnectionString()
                    .EmployeeID = txtEmployeeId.Text
                    .EmployeeName = txtEmployeeName.Text
                    .Position = cboPosition.SelectedItem.Value.Trim
                End With

                strError = oController.PemberiKuasaSKEEdit(oCollector)
                If strError <> "" Then
                    ShowMessage(lblMessage, strError, True)
                Else
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
                    DoBind(Me.SearchBy, Me.SortBy)
                    If Not (Me.Cache.Item("ID")) Is Nothing Then
                        Me.Cache.Remove("ID")
                    End If
                End If
        End Select
    End Sub
    Private Sub ImgAddNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonAddNew.Click
        'If CheckFeature(Me.Loginid, Me.FormID, "ADD", "Maxiloan") Then
        '    Dim oDataTable As New DataTable
        pnlAdd.Visible = True
        pnlList.Visible = False
        pnlDtGrid.Visible = False
        Me.Id = "ADD"
        lblTitle.Text = "PEMBERI KUASA SKE - ADD"
        txtEmployeeId.Enabled = True
        txtEmployeeId.Text = ""
        txtEmployeeName.Text = ""
        'cboPosition.Text = ""
        FillCombo()
    End Sub

    Private Sub btnCancelAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonCancelAdd.Click
        lblMessage.Visible = False
        InitialDefaultPanel()
        pnlDtGrid.Visible = True
    End Sub

#Region "FillCombo"
    Private Sub FillCombo()
        Dim strCombo As String
        Dim strConn As String
        Dim dtCombo As New DataTable

        strConn = getConnectionString()

        '--------------------------------
        ' Combo Position
        '--------------------------------
        strCombo = "Position"

        dtCombo = m_Employee.getCombo(strConn, strCombo, Me.BranchID)  
        cboPosition.DataTextField = "Name"
        cboPosition.DataValueField = "ID"
        cboPosition.DataSource = dtCombo
        cboPosition.DataBind()

        cboPosition.Items.Insert(0, "Select One")
        cboPosition.Items(0).Value = "0" 
    End Sub
#End Region
End Class