﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CollAction.aspx.vb" Inherits="Maxiloan.Webform.ARMgt.CollAction" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>CollAction</title>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <script type="text/javascript" language="javascript">
        function fConfirm() {
            if (window.confirm("Apakah yakin mau hapus data ini ? "))
                return true;
            else
                return false;
        }
        function fback() {
            history.go(-1);
            return false;
        }	
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">           
                <h3>
                    DAFTAR TINDAKAN
                </h3>
            </div>
        </div>  
        <asp:Panel ID="pnlList" runat="server">       
        <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgAction" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        CssClass="grid_general" BorderStyle="None" BorderWidth="0"
                        OnSortCommand="Sorting" Width="100%">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="EDIT">
                                <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImbEdit" runat="server" ImageUrl="../../Images/iconedit.gif"
                                        CommandName="Edit"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DELETE">
                                <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImbDelete" runat="server" ImageUrl="../../Images/icondelete.gif"
                                        CommandName="Delete" CausesValidation="False"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="VIEW HASIL">                                
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbViewResult" runat="server" ImageUrl="../../Images/icondetail.gif"
                                        CommandName="ViewResult"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="ACTIONID" HeaderText="ID">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="ACTIONNAME" HeaderText="NAMA TINDAKAN">
                            </asp:BoundColumn>
                        </Columns>
                </asp:DataGrid>        
                <div class="button_gridnavigation">
	                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
		                CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
		                CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
		                CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
		                CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
	                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
		                EnableViewState="False"></asp:Button>
	                <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage"
				        MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer"
				        CssClass="validator_general"></asp:RangeValidator>
			        <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
				        ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general">
			        </asp:RequiredFieldValidator>              
                </div>
                <div class="label_gridnavigation">
	                <asp:Label ID="lblPage" runat="server"></asp:Label>of
	                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
	                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                </div>
            </div>   
        </div>        
        </div> 
        <div class="form_button">
            <asp:Button ID="BtnAdd" runat="server"  Text="Add" CssClass ="small button blue">
            </asp:Button>
            <asp:Button ID="ButtonPrint" runat="server"  Text="Print" CssClass ="small button blue">
            </asp:Button>
	    </div>
        <div class="form_box_title">
            <div class="form_single">              
                <h4>
                    TINDAKAN
                </h4>
            </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Cari Berdasarkan</label>
                <asp:DropDownList ID="cboSearchBy" runat="server" Width="144px" >
                    <asp:ListItem Value="ActionID" Selected="True">ID Tindakan</asp:ListItem>
                    <asp:ListItem Value="ActionDescription">Nama Tindakan</asp:ListItem>
                </asp:DropDownList>
                &nbsp;
                <asp:TextBox ID="TxtSearchByValue" runat="server" Width="168px" ></asp:TextBox>
	        </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonSearch" runat="server"  Text="Search" CssClass ="small button blue">
            </asp:Button>&nbsp;
            <asp:Button ID="ButtonReset" runat="server"  Text="Reset" CssClass ="small button gray">
            </asp:Button>
	    </div>       
        </asp:Panel>
        <asp:Panel ID="pnlAddEdit" runat="server">
        <div class="form_box_title">
            <div class="form_single">              
                <h4>                    
                    <asp:Label ID="lblMenuAddEdit" runat="server"></asp:Label>
                </h4>
            </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
               <label class ="label_req">
			   ID Tindakan </label>
                <asp:TextBox ID="txtActionID" runat="server" Width="100px"  MaxLength="10"></asp:TextBox>
                <asp:Label ID="lblActionID" runat="server"></asp:Label>                                        
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"  CssClass="validator_general"
                ErrorMessage="Harap isi ID Tindakan" ControlToValidate="txtActionID"></asp:RequiredFieldValidator>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label class ="label_req">
				Nama Tindakan </label>
                <asp:TextBox ID="txtActionName" runat="server" Width="291px"  MaxLength="100"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfv1" runat="server" CssClass="validator_general" ErrorMessage="Harap isi Nama tindakan"
                ControlToValidate="txtActionName"></asp:RequiredFieldValidator>
	        </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonSave" runat="server"  Text="Save" CssClass ="small button blue">
            </asp:Button>&nbsp;
            <asp:Button ID="ButtonCancel" runat="server"  Text="Cancel" CssClass ="small button gray"
                CausesValidation="False"></asp:Button>&nbsp; <a href="javascript:history.back();">
                </a>
	    </div>     
    </asp:Panel>
    </form>
</body>
</html>
