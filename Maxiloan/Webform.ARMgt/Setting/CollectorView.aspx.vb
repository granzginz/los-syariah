﻿#Region "imports"
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class CollectorView
    Inherits Maxiloan.Webform.WebBased

#Region " Property "
    Private Property CGID() As String
        Get
            Return CStr(viewstate("CGID"))
        End Get
        Set(ByVal Value As String)
            viewstate("CGID") = Value
        End Set
    End Property

    Private Property CollectorID() As String
        Get
            Return CStr(viewstate("CollectorID"))
        End Get
        Set(ByVal Value As String)
            viewstate("CollectorID") = Value
        End Set
    End Property

    Private Property CollectorType() As String
        Get
            Return CStr(ViewState("CollectorType"))
        End Get
        Set(ByVal Value As String)
            viewstate("CollectorType") = Value
        End Set
    End Property

#End Region
    Dim m_Collector As New CollectorController


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If sessioninvalid() Then
            Exit Sub
        End If

        If Not Page.IsPostBack Then
            If Request.QueryString("CollectorID").Trim <> "" Then Me.CollectorID = Request.QueryString("CollectorID")
            If Request.QueryString("CGID").Trim <> "" Then Me.CGID = Request.QueryString("CGID")

            Me.FormID = "CollectorView"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                viewbyID(Me.CollectorID, Me.CGID)
                PnlView.Visible = True
                pnlClose.Visible = True
                ButtonClose.Attributes.Add("onclick", "windowClose()")
                Dim hp As HyperLink
                hp = CType(Me.FindControl("hpRAL"), HyperLink)
                'hp.NavigateUrl = "javascript:OpenViewRAL('" & Me.CGID.Trim & "','" & Me.CollectorID.Trim & 
                hp.NavigateUrl = "CollectorRAL.aspx?CGID=" & Me.CGID.Trim & "&CollectorID=" & Me.CollectorID.Trim & "&CGName=" & lblCGName.Text.Trim & "&CollectorName=" & lblEmployeeName.Text.Trim & ""
            End If
        End If
    End Sub


    Private Sub viewbyID(ByVal CollectorID As String, ByVal CGID As String)

        Dim Collector As New Parameter.Collector
        Dim CollectorList As New Parameter.Collector
        Dim dtInsCo As New DataTable

        With Collector
            .strConnection = getConnectionString
            .CollectorID = Me.CollectorID
        End With

        Try

            CollectorList = m_Collector.GetCollectorDetail(Collector)
            dtInsCo = CollectorList.ListData

            'Isi semua field Edit Action

            lblCGName.Text = CStr(dtInsCo.Rows(0).Item("CGID"))

            lblEmployeeName.Text = CStr(dtInsCo.Rows(0).Item("CollectorName"))
            lblCollectorID.Text = CStr(dtInsCo.Rows(0).Item("CollectorID"))
            lblSupervisor.Text = CStr(dtInsCo.Rows(0).Item("Supervisor"))
            lblCollectorType.Text = CStr(dtInsCo.Rows(0).Item("CollectorType"))
            lblActive.Text = CStr(dtInsCo.Rows(0).Item("Active"))

            lblAddress.Text = CStr(IIf(IsDBNull(dtInsCo.Rows(0).Item("Address")), "-", dtInsCo.Rows(0).Item("Address")))
            lblPhoneNo.Text = CStr(IIf(IsDBNull(dtInsCo.Rows(0).Item("Phone")), "-", dtInsCo.Rows(0).Item("Phone")))
            lblMobilePhone.Text = CStr(IIf(IsDBNull(dtInsCo.Rows(0).Item("ContactPersonMobilePhone")), "-", dtInsCo.Rows(0).Item("ContactPersonMobilePhone")))
            lblNotes.Text = CStr(IIf(IsDBNull(dtInsCo.Rows(0).Item("Notes")), "-", dtInsCo.Rows(0).Item("Notes")))

            If (lblCollectorType.Text.Trim = "EP") Or (lblCollectorType.Text.Trim = "EI") Then
                PnlView2.Visible = True
                pnlClose.Visible = True
            Else
                PnlView2.Visible = False
                pnlClose.Visible = True
            End If

        Catch ex As Exception

        End Try

    End Sub

    Function LinkToRAL() As String
        Return "javascript:OpenViewRAL('" & Me.CGID & "','" & Me.CollectorID & "')"
    End Function



End Class