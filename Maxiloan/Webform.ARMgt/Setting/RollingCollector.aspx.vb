﻿#Region "imports"
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
Imports System.Data.SqlClient
#End Region

Public Class RollingCollector
    Inherits Maxiloan.Webform.WebBased    

    Dim dtCollector As New DataTable
    Protected WithEvents UcCG As UcBranchCollection

#Region " Private Const "
    Dim m_CollZipCode As New CollZipCodeController
    Dim m_Coll As New CollectorController

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

#End Region
#Region " Property "
    Private Property CGID() As String
        Get
            Return CStr(ViewState("CGID"))
        End Get
        Set(ByVal Value As String)
            ViewState("CGID") = Value
        End Set
    End Property
    Private Property CGName() As String
        Get
            Return CStr(ViewState("CGName"))
        End Get
        Set(ByVal Value As String)
            ViewState("CGName") = Value
        End Set
    End Property
    Private Property ZipCode() As String
        Get
            Return CStr(ViewState("ZipCode"))
        End Get
        Set(ByVal Value As String)
            ViewState("ZipCode") = Value
        End Set
    End Property
    Private Property TempDataTable() As DataTable
        Get
            Return CType(ViewState("TempDataTable"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            ViewState("TempDataTable") = Value
        End Set
    End Property
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False        
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not Page.IsPostBack Then
            Me.CGID = Me.GroubDbID
            Me.FormID = "ROLLCOLL"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                PnlSearch.Visible = True
                pnlList.Visible = False
            End If
        End If
    End Sub
#Region " BindGrid"
    Sub BindGrid(ByVal cmdWhere As String, ByVal cmSort As String)        
    End Sub
#End Region
    Private Sub BtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSearch.Click

        If CheckFeature(Me.Loginid, Me.FormID, "Srch", Me.AppId) Then
            If SessionInvalid() Then
                Exit Sub
            End If
        End If

        If UcCG.BranchID <> "0" Then
            BindGrid(Me.SearchBy, Me.SortBy)
            pnlList.Visible = True
            PnlSearch.Visible = True


            If dtgColl.Items.Count = 0 Then
                AddRecord()
            End If


        End If

    End Sub
    Private Sub ButtonReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        PnlSearch.Visible = True
        pnlList.Visible = False
    End Sub
    Private Sub dtgColl_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgColl.ItemCommand
        If e.CommandName = "Delete" Then
            DeleteBaris(e.Item.ItemIndex)
        End If
    End Sub
    Private Sub imbSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
        Dim oColl As New Parameter.Collector
        Dim Err As String = ""

        Try
            Err = PopulateData()


            If Err.Trim <> "" Then
                ShowMessage(lblMessage, Err, True)
                Exit Sub
            End If


            If TempDataTable.Rows.Count > 0 Then

                For index = 0 To TempDataTable.Rows.Count - 1
                    oColl = New Parameter.Collector
                    oColl.CollectorIDA = TempDataTable.Rows(index).Item("CollectorA").ToString
                    oColl.CollectorIDB = TempDataTable.Rows(index).Item("CollectorB").ToString
                    oColl.strConnection = GetConnectionString()

                    'm_Coll.RollingCollectorSave(oColl)
                    '20160527 wira rubah,karena sering kena error time out
                    Dim ds As New DataSet
                    Dim adapter As New SqlDataAdapter
                    Dim objCommand As New SqlCommand
                    Dim objcon As New SqlConnection(GetConnectionString)

                    objCommand.CommandType = CommandType.StoredProcedure
                    objCommand.Connection = objcon
                    objCommand.CommandText = "spRollingCollectorSave"
                    objCommand.Parameters.Add("@CollectorIDA", SqlDbType.Char, 20).Value = TempDataTable.Rows(index).Item("CollectorA").ToString
                    objCommand.Parameters.Add("@CollectorIDB", SqlDbType.Char, 20).Value = TempDataTable.Rows(index).Item("CollectorB").ToString
                    objCommand.CommandTimeout = 300

                    adapter.SelectCommand = objCommand
                    adapter.Fill(ds)
                Next

            Else
                ShowMessage(lblMessage, "Data harus diisi", True)
                Exit Sub
            End If

            ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub
    Private Sub ButtonAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonAdd.Click
        AddRecord()
    End Sub
    Private Sub AddRecord()
        Dim objectDataTable As New DataTable
        Dim intLoopGrid As Integer
        Dim plus As Integer = 0
        Dim oRow As DataRow
        Dim cboCollectorA As New DropDownList
        Dim cboCollectorB As New DropDownList

        With objectDataTable
            .Columns.Add(New DataColumn("CollectorA", GetType(String)))
            .Columns.Add(New DataColumn("CollectorB", GetType(String)))
        End With

        For intLoopGrid = 0 To dtgColl.Items.Count - 1
            cboCollectorA = CType(dtgColl.Items(intLoopGrid).FindControl("cboCollectorA"), DropDownList)
            cboCollectorB = CType(dtgColl.Items(intLoopGrid).FindControl("cboCollectorB"), DropDownList)

            '----- Add row -------'
            oRow = objectDataTable.NewRow()
            oRow("CollectorA") = CType(cboCollectorA.SelectedValue, String)
            oRow("CollectorB") = CType(cboCollectorB.SelectedValue, String)
            objectDataTable.Rows.Add(oRow)

        Next

        oRow = objectDataTable.NewRow()
        oRow("CollectorA") = ""
        oRow("CollectorB") = ""
        objectDataTable.Rows.Add(oRow)


        dtgColl.DataSource = objectDataTable
        dtgColl.DataBind()
        BindCollector()

        For intLoopGrid = 0 To dtgColl.Items.Count - 1
            cboCollectorA = CType(dtgColl.Items(intLoopGrid).FindControl("cboCollectorA"), DropDownList)
            cboCollectorB = CType(dtgColl.Items(intLoopGrid).FindControl("cboCollectorB"), DropDownList)
            If objectDataTable.Rows(intLoopGrid).Item("CollectorA").ToString <> "" Then
                cboCollectorA.SelectedIndex = cboCollectorA.Items.IndexOf(cboCollectorA.Items.FindByValue(objectDataTable.Rows(intLoopGrid).Item("CollectorA").ToString.Trim))
            End If
            If objectDataTable.Rows(intLoopGrid).Item("CollectorB").ToString <> "" Then
                cboCollectorB.SelectedIndex = cboCollectorB.Items.IndexOf(cboCollectorB.Items.FindByValue(objectDataTable.Rows(intLoopGrid).Item("CollectorB").ToString.Trim))
            End If
        Next
    End Sub
    Sub BindCollector()
        Dim cboCollectorA As New DropDownList
        Dim cboCollectorB As New DropDownList
        Dim oCollZipCodeList As New Parameter.CollZipCode
        Dim oCollZipCode As New Parameter.CollZipCode
        Dim strconn As String = GetConnectionString()
        Dim dt As New DataTable

        For index = 0 To dtgColl.Items.Count - 1
            dt = New DataTable
            cboCollectorA = CType(dtgColl.Items(index).FindControl("cboCollectorA"), DropDownList)
            cboCollectorB = CType(dtgColl.Items(index).FindControl("cboCollectorB"), DropDownList)

            With oCollZipCode
                .strConnection = GetConnectionString()
                .CGID = Me.CGID
                .CollectorType = "CL"
            End With

            dt = m_CollZipCode.GetCollectorCombo(oCollZipCode)

            cboCollectorA.DataSource = dt
            cboCollectorA.DataTextField = "collectorName"
            cboCollectorA.DataValueField = "collectorID"
            cboCollectorA.DataBind()
            cboCollectorA.Items.Insert(0, "Select One")
            cboCollectorA.Items(0).Value = ""

            cboCollectorB.DataSource = dt
            cboCollectorB.DataTextField = "collectorName"
            cboCollectorB.DataValueField = "collectorID"
            cboCollectorB.DataBind()
            cboCollectorB.Items.Insert(0, "Select One")
            cboCollectorB.Items(0).Value = ""
        Next

    End Sub
    Sub DeleteBaris(ByVal Index As Integer)
        Dim objectDataTable As New DataTable
        Dim intLoopGrid As Integer
        Dim plus As Integer = 0
        Dim oRow As DataRow
        Dim cboCollectorA As New DropDownList
        Dim cboCollectorB As New DropDownList

        With objectDataTable
            .Columns.Add(New DataColumn("CollectorA", GetType(String)))
            .Columns.Add(New DataColumn("CollectorB", GetType(String)))
        End With

        For intLoopGrid = 0 To dtgColl.Items.Count - 1
            cboCollectorA = CType(dtgColl.Items(intLoopGrid).FindControl("cboCollectorA"), DropDownList)
            cboCollectorB = CType(dtgColl.Items(intLoopGrid).FindControl("cboCollectorB"), DropDownList)

            '----- Add row -------'
            If intLoopGrid <> Index Then
                oRow = objectDataTable.NewRow()
                oRow("CollectorA") = CType(cboCollectorA.SelectedValue, String)
                oRow("CollectorB") = CType(cboCollectorA.SelectedValue, String)
                objectDataTable.Rows.Add(oRow)
            End If
        Next
        dtgColl.DataSource = objectDataTable
        dtgColl.DataBind()
        BindCollector()

        For intLoopGrid = 0 To dtgColl.Items.Count - 1
            cboCollectorA = CType(dtgColl.Items(intLoopGrid).FindControl("cboCollectorA"), DropDownList)
            cboCollectorB = CType(dtgColl.Items(intLoopGrid).FindControl("cboCollectorB"), DropDownList)
            If objectDataTable.Rows(intLoopGrid).Item("CollectorA").ToString <> "" Then
                cboCollectorA.SelectedIndex = cboCollectorA.Items.IndexOf(cboCollectorA.Items.FindByValue(objectDataTable.Rows(intLoopGrid).Item("CollectorA").ToString.Trim))
            End If
            If objectDataTable.Rows(intLoopGrid).Item("CollectorB").ToString <> "" Then
                cboCollectorB.SelectedIndex = cboCollectorB.Items.IndexOf(cboCollectorB.Items.FindByValue(objectDataTable.Rows(intLoopGrid).Item("CollectorB").ToString.Trim))
            End If
        Next
    End Sub
    Function PopulateData() As String
        Dim Err As String = ""
        Dim cboCollectorA As New DropDownList
        Dim cboCollectorB As New DropDownList
        TempDataTable = New DataTable
        With TempDataTable
            .Columns.Add(New DataColumn("CollectorA", GetType(String)))
            .Columns.Add(New DataColumn("CollectorB", GetType(String)))
        End With

        For index = 0 To dtgColl.Items.Count - 1
            cboCollectorA = CType(dtgColl.Items(index).FindControl("cboCollectorA"), DropDownList)
            cboCollectorB = CType(dtgColl.Items(index).FindControl("cboCollectorB"), DropDownList)


            If cboCollectorA.SelectedValue.Trim <> "" And cboCollectorB.SelectedValue.Trim <> "" Then


                If cboCollectorA.SelectedValue.Trim = cboCollectorB.SelectedValue.Trim Then
                    Err = "Pilihan yang dirolling dan merolling tidak boleh sama."
                End If


                Dim oRow As DataRow
                oRow = TempDataTable.NewRow()
                oRow("CollectorA") = cboCollectorA.SelectedValue.Trim
                oRow("CollectorB") = cboCollectorB.SelectedValue.Trim
                TempDataTable.Rows.Add(oRow)
            End If

        Next

        Return Err
    End Function
End Class