﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class CasesSetting
    Inherits Maxiloan.Webform.WebBased

#Region "Property"
    Private Property PageNumber() As String
        Get
            Return (CType(Viewstate("page"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("page") = Value
        End Set
    End Property

    Private Property Mode() As String
        Get
            Return (CType(viewstate("Mode"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("Mode") = Value
        End Set
    End Property
    Private Property hypID() As String
        Get
            Return (CType(ViewState("hypID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("hypID") = Value
        End Set
    End Property
    Private Property hypklas() As String
        Get
            Return (CType(ViewState("hypklas"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("hypklas") = Value
        End Set
    End Property
    Private Property hypkonsumen() As String
        Get
            Return (CType(ViewState("hypkonsumen"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("hypkonsumen") = Value
        End Set
    End Property
    Private Property hypunit() As String
        Get
            Return (CType(ViewState("hypunit"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("hypunit") = Value
        End Set
    End Property
    Private Property hyppenyebab() As String
        Get
            Return (CType(ViewState("hyppenyebab"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("hyppenyebab") = Value
        End Set
    End Property



#End Region

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.CasesSetting
    Private oController As New CasesSettingController
   
    Protected WithEvents oSearchBy As UcSearchBy
#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            InitialDefaultPanel()
            Me.FormID = "CollCases"
            oSearchBy.ListData = "CASESID,Cases ID"
            oSearchBy.BindData()

            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.SearchBy = ""
                Me.SortBy = ""
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub

    Private Sub InitialDefaultPanel()
        pnlAdd.Visible = False
        pnlList.Visible = True
        pnlDatagrid.Visible = True
        lblMessage.Text = ""
    End Sub

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        Dim intloop As Integer
        Dim hypID As HyperLink

        With oCustomClass
            .strConnection = GetConnectionString
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oCustomClass = oController.CasesSettingList(oCustomClass)

        DtUserList = oCustomClass.ListCasesSetting
        DvUserList = DtUserList.DefaultView
        recordCount = oCustomClass.TotalRecord
        DvUserList.Sort = Me.SortBy
        DtgCasesSetting.DataSource = DvUserList

        Try
            DtgCasesSetting.DataBind()
        Catch
            DtgCasesSetting.CurrentPageIndex = 0
            DtgCasesSetting.DataBind()
        End Try
        PagingFooter()
        pnlDatagrid.Visible = True
    End Sub
#End Region

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            lblMessage.Text = "Data tidak ditemukan ....."
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        lblMessage.Text = ""
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Protected Sub btnPageNumb_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPageNumb.Click
        lblMessage.Text = ""
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub
#End Region

    Sub bindedit()

        lblkodekasus.Text = Me.hypID
        cboKonsumen.SelectedValue = Me.hypkonsumen
        cboUnit.SelectedValue = Me.hypunit
        lblKlasifikasi.Text = Me.hypklas
        cboPenyebab.SelectedValue = Me.hyppenyebab

    End Sub

#Region "DataGrid Command"
    Private Sub DtgCasesSetting_ItemCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgCasesSetting.ItemCommand
        Select Case e.CommandName
            Case "EDIT"
                If CheckFeature(Me.Loginid, Me.FormID, "ADD", "Maxiloan") Then
                    lblMessage.Text = ""
                    Me.Mode = "EDIT"
                    Dim hypID, hypklas, hypkonsumen, hypunit, hyppenyebab As New Label
                    hypID = CType(DtgCasesSetting.Items(e.Item.ItemIndex).FindControl("lblCasesID"), Label)
                    hypklas = CType(DtgCasesSetting.Items(e.Item.ItemIndex).FindControl("klas"), Label)
                    hypkonsumen = CType(DtgCasesSetting.Items(e.Item.ItemIndex).FindControl("Konsumen"), Label)
                    hypunit = CType(DtgCasesSetting.Items(e.Item.ItemIndex).FindControl("Unit"), Label)
                    hyppenyebab = CType(DtgCasesSetting.Items(e.Item.ItemIndex).FindControl("penyebab"), Label)

                    If hypkonsumen.Text = True Then
                        hypkonsumen.Text = "1"
                    Else
                        hypkonsumen.Text = "0"
                    End If

                    If hypunit.Text = True Then
                        hypunit.Text = "1"
                    Else
                        hypunit.Text = "0"
                    End If

                    Me.hypID = hypID.Text
                    Me.hypklas = hypklas.Text
                    Me.hypkonsumen = hypkonsumen.Text
                    Me.hypunit = hypunit.Text
                    Me.hyppenyebab = hyppenyebab.Text



                    pnlList.Visible = False
                    pnlDatagrid.Visible = False
                    pnlAdd.Visible = True
                    bindedit()
                    'txtCasesID.Text = hypID.Text
                    'txtDescription.Text = hypDescription.Text

                    'txtCasesID.Enabled = False

                    lblTitle.Text = "EDIT - KASUS"
                    'btnCancelAdd.Attributes.Add("onclick", "return fback()")
                End If
            Case "DELETE"
                    If CheckFeature(Me.Loginid, Me.FormID, "DEL", "Maxiloan") Then
                        lblMessage.Text = ""
                        Dim hypID As Label
                        hypID = CType(DtgCasesSetting.Items(e.Item.ItemIndex).FindControl("lblCasesID"), Label)
                        oCustomClass.strConnection = GetConnectionString()
                        oCustomClass.CasesID = hypID.Text
                        Dim strError As String
                        strError = oController.CasesSettingDelete(oCustomClass)
                        If strError = "" Then
                            lblMessage.Text = MessageHelper.MESSAGE_DELETE_SUCCESS
                        Else
                            lblMessage.Text = strError
                        End If
                        DoBind(Me.SearchBy, Me.SortBy)
                    End If
        End Select
    End Sub

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        lblMessage.Text = ""
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

    Private Sub ImgAddNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonAddNew.Click
        If CheckFeature(Me.Loginid, Me.FormID, "ADD", "Maxiloan") Then
            pnlAdd.Visible = True
            pnlList.Visible = False
            pnlDatagrid.Visible = False
            Me.Mode = "ADD"
            lblTitle.Text = "TAMBAH - KASUS"
            lblMessage.Text = ""
            cboKonsumen.SelectedIndex = 0
            cboUnit.SelectedIndex = 0
            cboPenyebab.SelectedIndex = 0
            Requiredfieldvalidator3.Enabled = True
            lblKlasifikasi.Text = ""
            lblkodekasus.Text = ""
            'btnCancelAdd.Attributes.Add("onclick", "return fback()")
        End If
    End Sub

    Private Sub btnSaveAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSaveAdd.Click
        pnlList.Visible = True
        pnlDatagrid.Visible = False
        pnlAdd.Visible = False
        lblMessage.Text = ""
        Dim strError As String
        If Page.IsValid Then
            With oCustomClass
                .strConnection = GetConnectionString()
                .CasesID = lblkodekasus.Text.Trim
                .konsumen = CBool(cboKonsumen.SelectedValue.Trim)
                .unit = cboUnit.SelectedValue.Trim
                .klas = lblKlasifikasi.Text
                .penyebab = cboPenyebab.SelectedValue.Trim
                .LoginId = Me.Loginid
            End With
            Select Case Mode
                Case "ADD"
                    lblMessage.Text = ""
                    strError = oController.CasesSettingAdd(oCustomClass)
                    If strError <> "" Then
                        lblMessage.Visible = True
                        ShowMessage(lblMessage, MessageHelper.MESSAGES_FIELD_ALREADY_EXISTS, False)
                        DoBind(Me.SearchBy, Me.SortBy)
                    Else
                        lblMessage.Text = MessageHelper.MESSAGE_INSERT_SUCCESS
                        DoBind(Me.SearchBy, Me.SortBy)
                    End If

                Case "EDIT"
                    With oCustomClass
                        .CasesID = Me.hypID.Trim
                        .CasesIDnew = lblkodekasus.Text.Trim
                    End With
                    lblMessage.Text = ""
                    strError = oController.CasesSettingEdit(oCustomClass)
                    If strError <> "" Then
                        lblMessage.Visible = True
                        ShowMessage(lblMessage, MessageHelper.MESSAGES_FIELD_ALREADY_EXISTS, False)
                        DoBind(Me.SearchBy, Me.SortBy)
                    Else
                        lblMessage.Text = MessageHelper.MESSAGE_UPDATE_SUCCESS
                        DoBind(Me.SearchBy, Me.SortBy)
                        If Not (Me.Cache.Item("DESCRIPTION")) Is Nothing Then
                            Me.Cache.Remove("DESCRIPTION")
                        End If
                    End If

                Case "DELETE"
                    lblMessage.Text = ""
                    strError = oController.CasesSettingEdit(oCustomClass)
                    If strError <> "" Then
                        lblMessage.Text = strError
                    Else
                        lblMessage.Text = MessageHelper.MESSAGE_DELETE_SUCCESS
                        DoBind(Me.SearchBy, Me.SortBy)
                        If Not (Me.Cache.Item("DESCRIPTION")) Is Nothing Then
                            Me.Cache.Remove("DESCRIPTION")
                        End If
                    End If
            End Select
        End If
    End Sub

    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Buttonsearch.Click
        lblMessage.Text = ""
        If oSearchBy.Text.Trim <> "" Then
            Me.SearchBy = oSearchBy.ValueID & " like '%" & oSearchBy.Text.Trim.Replace("'", "''") & "%'"
        Else
            Me.SearchBy = ""
        End If
        pnlDatagrid.Visible = True
        pnlList.Visible = True
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub DtgCasesSetting_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgCasesSetting.ItemDataBound
        Dim m As Int32
        Dim imbDelete As ImageButton
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imbDelete"), ImageButton)
            imbDelete.Attributes.Add("onclick", "return fConfirm()")
        End If
    End Sub

    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        lblMessage.Text = ""
        Me.SearchBy = ""
        oSearchBy.Text = ""
        oSearchBy.BindData()
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonPrint.Click
        If SessionInvalid() Then
            Exit Sub
        End If
        If CheckFeature(Me.Loginid, Me.FormID, "PRINT", Me.AppId) Then
            Response.Redirect("Report/CasesSettingRpt.aspx")
        End If
    End Sub

    Private Sub btnCancelAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonCancelAdd.Click
        Response.Redirect("CasesSetting.aspx")
    End Sub

   
    Sub unit()
        If cboKonsumen.SelectedValue.Trim = "1" And cboUnit.SelectedValue.Trim = "1" Then
            lblKlasifikasi.Text = "A"
        ElseIf cboKonsumen.SelectedValue.Trim = "1" And cboUnit.SelectedValue.Trim = "0" Then
            lblKlasifikasi.Text = "B"
        ElseIf cboKonsumen.SelectedValue.Trim = "0" And cboUnit.SelectedValue.Trim = "1" Then
            lblKlasifikasi.Text = "C"
        ElseIf cboKonsumen.SelectedValue.Trim = "0" And cboUnit.SelectedValue.Trim = "0" Then
            lblKlasifikasi.Text = "D"
        Else
            lblKlasifikasi.Text = ""
        End If
    End Sub
   
    Sub penyebab()
        If lblKlasifikasi.Text = "A" And cboPenyebab.SelectedValue.Trim = "Karakter" Then
            lblkodekasus.Text = "A1"
        ElseIf lblKlasifikasi.Text = "A" And cboPenyebab.SelectedValue.Trim = "Aparat" Then
            lblkodekasus.Text = "A2"
        ElseIf lblKlasifikasi.Text = "A" And cboPenyebab.SelectedValue.Trim = "Barang Bukti" Then
            lblkodekasus.Text = "A3"
        ElseIf lblKlasifikasi.Text = "A" And cboPenyebab.SelectedValue.Trim = "Asuransi" Then
            lblkodekasus.Text = "A4"
        ElseIf lblKlasifikasi.Text = "A" And cboPenyebab.SelectedValue.Trim = "Pihak Ketiga" Then
            lblkodekasus.Text = "A5"
        ElseIf lblKlasifikasi.Text = "A" And cboPenyebab.SelectedValue.Trim = "Fiktif" Then
            lblkodekasus.Text = "A6"
        ElseIf lblKlasifikasi.Text = "A" And cboPenyebab.SelectedValue.Trim = "CashFlow" Then
            lblkodekasus.Text = "A7"

        ElseIf lblKlasifikasi.Text = "B" And cboPenyebab.SelectedValue.Trim = "Karakter" Then
            lblkodekasus.Text = "B1"
        ElseIf lblKlasifikasi.Text = "B" And cboPenyebab.SelectedValue.Trim = "Aparat" Then
            lblkodekasus.Text = "B2"
        ElseIf lblKlasifikasi.Text = "B" And cboPenyebab.SelectedValue.Trim = "Barang Bukti" Then
            lblkodekasus.Text = "B3"
        ElseIf lblKlasifikasi.Text = "B" And cboPenyebab.SelectedValue.Trim = "Asuransi" Then
            lblkodekasus.Text = "B4"
        ElseIf lblKlasifikasi.Text = "B" And cboPenyebab.SelectedValue.Trim = "Pihak Ketiga" Then
            lblkodekasus.Text = "B5"
        ElseIf lblKlasifikasi.Text = "B" And cboPenyebab.SelectedValue.Trim = "Fiktif" Then
            lblkodekasus.Text = "B6"
        ElseIf lblKlasifikasi.Text = "B" And cboPenyebab.SelectedValue.Trim = "CashFlow" Then
            lblkodekasus.Text = "B7"


        ElseIf lblKlasifikasi.Text = "C" And cboPenyebab.SelectedValue.Trim = "Karakter" Then
            lblkodekasus.Text = "C1"
        ElseIf lblKlasifikasi.Text = "C" And cboPenyebab.SelectedValue.Trim = "Aparat" Then
            lblkodekasus.Text = "C2"
        ElseIf lblKlasifikasi.Text = "C" And cboPenyebab.SelectedValue.Trim = "Barang Bukti" Then
            lblkodekasus.Text = "C3"
        ElseIf lblKlasifikasi.Text = "C" And cboPenyebab.SelectedValue.Trim = "Asuransi" Then
            lblkodekasus.Text = "C4"
        ElseIf lblKlasifikasi.Text = "C" And cboPenyebab.SelectedValue.Trim = "Pihak Ketiga" Then
            lblkodekasus.Text = "C5"
        ElseIf lblKlasifikasi.Text = "C" And cboPenyebab.SelectedValue.Trim = "Fiktif" Then
            lblkodekasus.Text = "C6"
        ElseIf lblKlasifikasi.Text = "C" And cboPenyebab.SelectedValue.Trim = "CashFlow" Then
            lblkodekasus.Text = "C7"


        ElseIf lblKlasifikasi.Text = "D" And cboPenyebab.SelectedValue.Trim = "Karakter" Then
            lblkodekasus.Text = "D1"
        ElseIf lblKlasifikasi.Text = "D" And cboPenyebab.SelectedValue.Trim = "Aparat" Then
            lblkodekasus.Text = "D2"
        ElseIf lblKlasifikasi.Text = "D" And cboPenyebab.SelectedValue.Trim = "Barang Bukti" Then
            lblkodekasus.Text = "D3"
        ElseIf lblKlasifikasi.Text = "D" And cboPenyebab.SelectedValue.Trim = "Asuransi" Then
            lblkodekasus.Text = "D4"
        ElseIf lblKlasifikasi.Text = "D" And cboPenyebab.SelectedValue.Trim = "Pihak Ketiga" Then
            lblkodekasus.Text = "D5"
        ElseIf lblKlasifikasi.Text = "D" And cboPenyebab.SelectedValue.Trim = "Fiktif" Then
            lblkodekasus.Text = "D6"
        ElseIf lblKlasifikasi.Text = "D" And cboPenyebab.SelectedValue.Trim = "CashFlow" Then
            lblkodekasus.Text = "D7"
        Else
            lblkodekasus.Text = ""
        End If

    End Sub

    
End Class