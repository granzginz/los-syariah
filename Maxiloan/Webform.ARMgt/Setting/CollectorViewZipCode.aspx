﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CollectorViewZipCode.aspx.vb"
    Inherits="Maxiloan.Webform.ARMgt.CollectorViewZipCode" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>CollectorViewZipCode</title>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;

        function fConfirm() {
            if (window.confirm("Apakah yakin mau hapus data ini ? "))
                return true;
            else
                return false;
        }
        function fback() {
            history.go(-1);
            return false;
        }

        function OpenViewCollGroup(pCGID) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.ARMgt/Setting/CollectionGroupView.aspx?CGID=' + pCGID, 'InsuranceCo', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">           
                <h3>
                    VIEW KODE POS
                </h3>
            </div>
        </div>  
        <asp:Panel ID="pnlList" runat="server">
        <div class="form_box">
	        <div class="form_single">
                <label>Collector</label>
                <asp:Label ID="lblCollectorName" runat="server" Width="352px"></asp:Label>
	        </div>
        </div>
        <div class="form_box_title">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgZipCode" runat="server" Width="100%" CssClass="grid_general" BorderStyle="None" BorderWidth="0"
                    OnSortCommand="Sorting" AllowSorting="True" AutoGenerateColumns="False">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <Columns>
                    <asp:BoundColumn DataField="ZIPCODE" SortExpression="ZIPCODE" HeaderText="KODE POS"
                         ></asp:BoundColumn>
                    <asp:BoundColumn DataField="KELURAHAN" SortExpression="KELURAHAN" HeaderText="KELURAHAN"
                         ></asp:BoundColumn>
                    <asp:BoundColumn DataField="KECAMATAN" SortExpression="KECAMATAN" HeaderText="KECAMATAN"
                         ></asp:BoundColumn>
                    <asp:BoundColumn DataField="CITY" SortExpression="CITY" HeaderText="KOTA"  >
                    </asp:BoundColumn>
                </Columns>
                </asp:DataGrid>        
                <div class="button_gridnavigation">
	                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
		                CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
		                CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
		                CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
		                CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
	                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
		                EnableViewState="False"></asp:Button>
	                <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage"
				        MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer"
				        CssClass="validator_general"></asp:RangeValidator>
			        <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
				        ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general">
			        </asp:RequiredFieldValidator>              
                </div>
                <div class="label_gridnavigation">
	                <asp:Label ID="lblPage" runat="server"></asp:Label>of
	                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
	                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                </div>
            </div>     
        </div>     
        </div>     
        <div class="form_button">
            <asp:Button ID="ButtonBack" runat="server"  Text="Back" CssClass ="small button gray">
            </asp:Button>
	    </div>        
    </asp:Panel>
    </form>
</body>
</html>
