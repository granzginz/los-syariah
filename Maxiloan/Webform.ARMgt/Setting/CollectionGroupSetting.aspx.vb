﻿#Region "Imports"
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class CollectionGroupSetting
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents UCCompanyAdress1 As UcCompanyAddress
    Protected WithEvents UCContactPerson1 As UcContactPerson
    Protected WithEvents UcFindEmployee1 As ucFindEmpployee2
    Protected WithEvents UCbranch1 As UcBranch

#Region " Private Const "
    Dim m_CollGroup As New CollGroupController

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

#End Region

#Region " Property "
    Private Property CGID() As String
        Get
            Return CStr(viewstate("CGID"))
        End Get
        Set(ByVal Value As String)
            viewstate("CGID") = Value
        End Set
    End Property

    Private Property CGName() As String
        Get
            Return CStr(viewstate("CGName"))
        End Get
        Set(ByVal Value As String)
            viewstate("CGName") = Value
        End Set
    End Property

    Private Property CGHead() As String
        Get
            Return CStr(viewstate("CGHead"))
        End Get
        Set(ByVal Value As String)
            viewstate("CGHead") = Value
        End Set
    End Property

    Private Property CollGroupAddEdit() As String
        Get
            Return CStr(ViewState("CollGroupAddEdit"))
        End Get
        Set(ByVal Value As String)
            viewstate("CollGroupAddEdit") = Value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not Me.IsPostBack Then
            InitialDefaultPanel()
            Me.FormID = "CollGroup"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.SearchBy = "all"
                Me.SortBy = ""
                If Request("cond") <> "" Then
                    Me.SearchBy = Request("cond")
                Else
                    Me.SearchBy = "ALL"
                End If
                Me.SortBy = ""
                BindGridCollGroup(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub

#Region " PanelAllFalse "
    Private Sub PanelAllFalse()        
        pnlSearch.Visible = False
        pnlAddEdit.Visible = False
        pnlList.Visible = False
    End Sub
#End Region

#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridCollGroup(Me.SearchBy, Me.SortBy)
    End Sub

    Protected Sub btnPageNumb_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPageNumb.Click

        If txtPage.Text = "" Then
            txtPage.Text = "0"
        Else
            If IsNumeric(txtPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                    currentPage = CType(txtPage.Text, Int32)
                    BindGridCollGroup(Me.SearchBy, Me.SortBy)
                End If
            End If
        End If


    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        BindGridCollGroup(Me.SearchBy, Me.SortBy)
    End Sub

#End Region

#Region " InitialDefaultPanel "
    Private Sub InitialDefaultPanel()
        pnlSearch.Visible = True
        pnlList.Visible = True
        pnlAddEdit.Visible = False
    End Sub
#End Region


#Region " BindGrid"
    Sub BindGridCollGroup(ByVal cmdWhere As String, ByVal cmSort As String)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim oCollGroup As New Parameter.CollGroup

        cmdWhere = IIf(cmdWhere = "ALL" Or cmdWhere = "", " CollectionGroup.BranchID = '" & Me.sesBranchId.Replace("'", "") & "'", " CollectionGroup.BranchID = '" & Me.sesBranchId.Replace("'", "") & "' and " & cmdWhere)
        With oCollGroup
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With


        oCollGroup = m_CollGroup.GetCollGroupList(oCollGroup)

        With oCollGroup
            lblrecord.Text = CType(.TotalRecord, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecord
        End With

        dtsEntity = oCollGroup.ListData
        dtvEntity = dtsEntity.DefaultView
        dtvEntity.Sort = Me.SortBy
        dtgCollGroup.DataSource = dtvEntity
        Try
            dtgCollGroup.DataBind()
        Catch
            dtgCollGroup.CurrentPageIndex = 0
            dtgCollGroup.DataBind()
        End Try

        PagingFooter()

    End Sub
#End Region

   

    Private Sub viewbyID(ByVal oCollGroup As Parameter.CollGroup)

        Dim ds As DataTable

        lblBintang.Visible = False
        txtCGID.Visible = False
        lblCGID.Text = oCollGroup.CGID
        txtCGName.Text = oCollGroup.CGName
        lblCGID.Visible = True
        lblMenuAddEdit.Text = Me.CollGroupAddEdit


        oCollGroup = m_CollGroup.GetCollGroupByID(oCollGroup)

        ds = oCollGroup.ListData

        getBranchCombo()

        cboBranch.SelectedIndex = cboBranch.Items.IndexOf(cboBranch.Items.FindByValue(CStr(ds.Rows(0).Item("branchId"))))


        With UcFindEmployee1
            .EmployeeName = CStr(ds.Rows(0).Item("CGHeadName")).Trim
            .Text = "Collection Group Head"
            '.bindData()
        End With


        With UcCompanyAdress1
            .Address = CStr(ds.Rows(0).Item("address")).Trim
            .RT = CStr(ds.Rows(0).Item("rt")).Trim
            .RW = CStr(ds.Rows(0).Item("rt")).Trim
            .Kecamatan = CStr(ds.Rows(0).Item("kecamatan")).Trim
            .Kelurahan = CStr(ds.Rows(0).Item("kelurahan")).Trim
            .City = CStr(ds.Rows(0).Item("city")).Trim
            .ZipCode = CStr(ds.Rows(0).Item("zipcode")).Trim
            .AreaPhone1 = CStr(ds.Rows(0).Item("areaphone1")).Trim
            .Phone1 = CStr(ds.Rows(0).Item("phoneNo1")).Trim
            .AreaPhone2 = CStr(ds.Rows(0).Item("areaphone2")).Trim
            .Phone2 = CStr(ds.Rows(0).Item("phoneNo2")).Trim
            .AreaFax = CStr(ds.Rows(0).Item("areafax")).Trim
            .Fax = CStr(ds.Rows(0).Item("faxno")).Trim
            .Style = "Collection"
            .showMandatoryAll()
            .BindAddress()
        End With

        With UcContactPerson1
            .ContactPerson = CStr(ds.Rows(0).Item("ContactPerson")).Trim
            .ContactPersonTitle = CStr(ds.Rows(0).Item("ContactPersonTitle")).Trim
            .MobilePhone = CStr(ds.Rows(0).Item("ContactPersonMobilePhone")).Trim
            .Email = CStr(ds.Rows(0).Item("ContactPersonEmail")).Trim
            .EnabledContactPerson()
            .BindContacPerson()
        End With



    End Sub

    Private Sub ClearAddForm()

        txtCGID.Text = ""
        txtCGName.Text = ""

        getBranchCombo()


        With UcFindEmployee1
            ' .bindData()
            .BranchID = Me.BranchID
            .EmployeeID = ""
            .EmployeeName = ""
            .Text = "Collection Group Head"
        End With


        With UcCompanyAdress1
            .Address = ""
            .RT = ""
            .RW = ""
            .Kecamatan = ""
            .Kelurahan = ""
            .City = ""
            .ZipCode = ""
            .AreaPhone1 = ""
            .Phone1 = ""
            .AreaPhone2 = ""
            .Phone2 = ""
            .AreaFax = ""
            .Fax = ""
            .Style = "Collection"
            .showMandatoryAll()
            .BindAddress()

        End With

        With UcContactPerson1
            .ContactPerson = ""
            .ContactPersonTitle = ""
            .MobilePhone = ""
            .Email = ""
            .EnabledContactPerson()
            .BindContacPerson()
        End With


    End Sub

    Private Sub Add()
        Dim oCollGroup As New Parameter.CollGroup
        Dim oClassAddress As New Parameter.Address
        Dim oClassPersonal As New Parameter.Personal

        With oCollGroup
            .strConnection = GetConnectionString
            .CGID = txtCGID.Text.Trim
            .CGName = txtCGName.Text
            .CGHead = UcFindEmployee1.EmployeeID.Trim
            .BranchId = UcFindEmployee1.BranchID.Trim
        End With

        With oClassAddress
            .Address = UcCompanyAdress1.Address.Trim
            .RT = UcCompanyAdress1.RT.Trim
            .RW = UcCompanyAdress1.RW.Trim
            .Kecamatan = UcCompanyAdress1.Kecamatan.Trim
            .Kelurahan = UcCompanyAdress1.Kelurahan.Trim
            .City = UcCompanyAdress1.City.Trim
            .ZipCode = UcCompanyAdress1.ZipCode.Trim
            .AreaPhone1 = UcCompanyAdress1.AreaPhone1.Trim
            .Phone1 = UcCompanyAdress1.Phone1.Trim
            .AreaPhone2 = UcCompanyAdress1.AreaPhone2.Trim
            .Phone2 = UcCompanyAdress1.Phone2.Trim
            .AreaFax = UcCompanyAdress1.AreaFax.Trim
            .Fax = UcCompanyAdress1.Fax.Trim
        End With

        With oClassPersonal
            .PersonName = UcContactPerson1.ContactPerson.Trim
            .PersonTitle = UcContactPerson1.ContactPersonTitle.Trim
            .MobilePhone = UcContactPerson1.MobilePhone.Trim
            .Email = UcContactPerson1.Email.Trim
        End With

        Try
            m_CollGroup.CollGroupAdd(oCollGroup, oClassAddress, oClassPersonal)            
            ShowMessage(lblMessage, "Tambah data Berhasil ", False)

        Catch ex As Exception            
            ShowMessage(lblMessage, ex.Message, True)
        End Try

    End Sub

    Private Sub edit(ByVal CGID As String)

        Dim oCollGroup As New Parameter.CollGroup
        Dim oClassAddress As New Parameter.Address
        Dim oClassPersonal As New Parameter.Personal

        With oCollGroup
            .strConnection = GetConnectionString
            .CGID = Me.CGID
            .CGName = txtCGName.Text

            If UcFindEmployee1.EmployeeID <> "" Then
                .CGHead = UcFindEmployee1.EmployeeID
            Else
                .CGHead = Me.CGHead
            End If
            '.BranchId = Me.BranchID
            .BranchId = Me.sesBranchId.Replace("'", "")
        End With

        With oClassAddress
            .Address = UcCompanyAdress1.Address
            .RT = UcCompanyAdress1.RT
            .RW = UcCompanyAdress1.RW
            .Kecamatan = UcCompanyAdress1.Kecamatan
            .Kelurahan = UcCompanyAdress1.Kelurahan
            .City = UcCompanyAdress1.City
            .ZipCode = UcCompanyAdress1.ZipCode
            .AreaPhone1 = UcCompanyAdress1.AreaPhone1
            .Phone1 = UcCompanyAdress1.Phone1
            .AreaPhone2 = UcCompanyAdress1.AreaPhone2
            .Phone2 = UcCompanyAdress1.Phone2
            .AreaFax = UcCompanyAdress1.AreaFax
            .Fax = UcCompanyAdress1.Fax
        End With

        With oClassPersonal
            .PersonName = UcContactPerson1.ContactPerson
            .PersonTitle = UcContactPerson1.ContactPersonTitle
            .MobilePhone = UcContactPerson1.MobilePhone
            .Email = UcContactPerson1.Email
        End With

        Try
            m_CollGroup.CollGroupEdit(oCollGroup, oClassAddress, oClassPersonal)            
            ShowMessage(lblMessage, "Simpan Data Berhasil ", False)
        Catch ex As Exception            
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

    Private Sub dtgCollGroup_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgCollGroup.ItemCommand
        Me.CGID = e.Item.Cells(2).Text
        Me.CGName = e.Item.Cells(4).Text
        Me.BranchID = e.Item.Cells(5).Text
        Me.CGHead = e.Item.Cells(9).Text

        Select Case e.CommandName
            Case "Edit"
                If checkFeature(Me.Loginid, Me.FormID, "Edit", Me.AppId) Then
                    If sessioninvalid() Then
                        Exit Sub
                    End If
                End If

                Me.CGID = e.Item.Cells(3).Text

                Dim oCollGroup As New Parameter.CollGroup
                With oCollGroup
                    .strConnection = getConnectionString
                    .CGID = Me.CGID
                    .CGName = Me.CGName
                    .BranchId = Me.BranchID
                    Me.CollGroupAddEdit = "EDIT"
                End With

                PanelAllFalse()
                viewbyID(oCollGroup)
                pnlAddEdit.Visible = True

            Case "Delete"
                If checkFeature(Me.Loginid, Me.FormID, "Del", Me.AppId) Then
                    If sessioninvalid() Then
                        Exit Sub
                    End If
                End If

                Me.CGID = e.Item.Cells(3).Text

                Dim oCollGroup As New Parameter.CollGroup
                With oCollGroup
                    .strConnection = getConnectionString
                    .CGID = Me.CGID
                    .BranchId = Me.BranchID
                End With

                Try
                    m_CollGroup.CollGroupDelete(oCollGroup)                    
                    ShowMessage(lblMessage, "Hapus Data Berhasil ", False)
                Catch ex As Exception                    
                    ShowMessage(lblMessage, ex.Message, True)

                Finally
                    pnlAddEdit.Visible = False
                    BindGridCollGroup("ALL", "")
                    pnlList.Visible = True

                End Try


        End Select
    End Sub

    Public Function LinkTo(ByVal strAssetTypeID As String) As String
        Return "javascript:OpenViewCollGroup('" & strAssetTypeID & "')"
    End Function

    Private Sub dtgCollGroup_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgCollGroup.ItemDataBound

        Dim imbDelete As ImageButton

        If e.Item.ItemIndex >= 0 Then

            imbDelete = CType(e.Item.FindControl("ImbDelete"), ImageButton)
            imbDelete.Attributes.Add("onclick", "return fConfirm()")
        End If

    End Sub

    Private Sub ImageButton1_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ButtonPrint.Click
        Response.Redirect("report/CollGroupRpt.aspx")
    End Sub

    Private Sub getBranchCombo()
        Dim oCollGroup As New Parameter.CollGroup
        Dim oCollGroupList As New Parameter.CollGroup
        Dim dt As New DataTable

        With oCollGroup
            .strConnection = getConnectionString()
        End With

        oCollGroupList = m_CollGroup.GetBranchCombo(oCollGroup)

        dt = oCollGroupList.ListData
        cboBranch.DataSource = dt
        cboBranch.DataValueField = "ID"
        cboBranch.DataTextField = "Name"
        cboBranch.DataBind()

    End Sub


    Protected Sub ButtonAdd_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonAdd.Click
        If CheckFeature(Me.Loginid, Me.FormID, "Add", Me.AppId) Then
            If SessionInvalid() Then
                Exit Sub
            End If
        End If

        PanelAllFalse()
        ClearAddForm()

        Me.CollGroupAddEdit = "ADD"
        txtCGID.Text = ""
        lblCGID.Visible = False
        txtCGID.Text = ""
        txtCGID.Visible = True
        txtCGName.Text = ""
        pnlAddEdit.Visible = True
        lblMenuAddEdit.Text = Me.CollGroupAddEdit

        UcFindEmployee1.BranchID = ""

    End Sub

    Protected Sub ButtonSearch_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonSearch.Click
        Dim StrSearchBy As String = cboSearchBy.SelectedItem.Value
        Dim StrSearchByValue As String = TxtSearchByValue.Text.Trim
        If StrSearchByValue = "" Then
            Me.SearchBy = ""
        Else
            Me.SearchBy = StrSearchBy + " like '%" + StrSearchByValue + "%'"
        End If
        PanelAllFalse()
        BindGridCollGroup(Me.SearchBy, Me.SortBy)
        pnlSearch.Visible = True
        pnlList.Visible = True
    End Sub

    Protected Sub ButtonCopyAddress_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonCopyAddress.Click
        Dim collgroup As New Parameter.CollGroup
        With collgroup
            .strConnection = GetConnectionString()
            .BranchId = cboBranch.SelectedItem.Value

        End With

        Dim strconn As String = GetConnectionString()
        Dim dtbranchaddress As New DataTable

        Try
            dtbranchaddress = m_CollGroup.GetBranchAddress(collgroup)
        Catch ex As Exception

        End Try


        UCCompanyAdress1.Address = CStr(dtbranchaddress.Rows(0).Item("branchAddress")).Trim
        UCCompanyAdress1.Kelurahan = CStr(dtbranchaddress.Rows(0).Item("BranchKelurahan")).Trim
        UCCompanyAdress1.Kecamatan = CStr(dtbranchaddress.Rows(0).Item("BranchKecamatan")).Trim
        UCCompanyAdress1.City = CStr(dtbranchaddress.Rows(0).Item("BranchCity")).Trim
        UCCompanyAdress1.ZipCode = CStr(dtbranchaddress.Rows(0).Item("BranchZipCode")).Trim

        UCCompanyAdress1.BindAddress()
        pnlList.Visible = False
        pnlSearch.Visible = False
        pnlAddEdit.Visible = True
    End Sub

    Protected Sub ButtonSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonSave.Click
        Select Case Me.CollGroupAddEdit
            Case "ADD"
                PanelAllFalse()
                Add()
                Me.SearchBy = "ALL"
                Me.SortBy = ""
                BindGridCollGroup(Me.SearchBy, Me.SortBy)
                pnlSearch.Visible = True
                pnlList.Visible = True

            Case "EDIT"
                PanelAllFalse()
                Me.SearchBy = "ALL"
                Me.SortBy = ""
                edit(Me.CGID)
                BindGridCollGroup(Me.SearchBy, Me.SortBy)
                pnlSearch.Visible = True
                pnlList.Visible = True


        End Select
    End Sub

    Protected Sub ButtonCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonCancel.Click
        PanelAllFalse()
        InitialDefaultPanel()
    End Sub

    
    Protected Sub ButtonReset_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonReset.Click
        cboSearchBy.SelectedIndex = 0
        TxtSearchByValue.Text = ""
        BindGridCollGroup("ALL", "")
    End Sub
End Class