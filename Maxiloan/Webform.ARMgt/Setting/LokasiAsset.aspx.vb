﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class LokasiAsset
    Inherits Maxiloan.Webform.WebBased
#Region "Property"
    Private Property Id() As String
        Get
            Return CType(ViewState("Id"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Id") = Value
        End Set
    End Property
#End Region

#Region "Constanta"
    Dim m_ActionResult As New LokasiAssetController

    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.LokasiAsset
    Private oController As New LokasiAssetController
    Protected WithEvents oSearchBy As UcSearchBy
    Protected WithEvents oUcCompanyAdress As UcCompanyAddress
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not Me.IsPostBack Then
            InitialDefaultPanel()
            Me.FormID = "LOKASIASSET"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.SearchBy = ""
                Me.SortBy = ""
                oSearchBy.ListData = "ID,Kode Lokasi-DESCRIPTION,Keterangan"
                oSearchBy.BindData()
                If Request("cond") <> "" Then
                    Me.SearchBy = Request("cond")
                Else
                    Me.SearchBy = ""
                End If
                Me.SortBy = ""
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub

    Private Sub InitialDefaultPanel()
        pnlAdd.Visible = False
        pnlList.Visible = True
    End Sub

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView

        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oCustomClass = oController.LokasiAsset(oCustomClass)

        With oCustomClass
            lblrecord.Text = CType(.TotalRecord, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecord
        End With

        DtUserList = oCustomClass.listLokasiAsset
        DvUserList = DtUserList.DefaultView
        recordCount = oCustomClass.TotalRecord
        DvUserList.Sort = Me.SortBy
        dtgLokasiAsset.DataSource = DvUserList

        Try
            dtgLokasiAsset.DataBind()
        Catch
            dtgLokasiAsset.CurrentPageIndex = 0
            dtgLokasiAsset.DataBind()
        End Try

        PagingFooter()
    End Sub
    Private Sub dtgLokasiAsset_ItemCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgLokasiAsset.ItemCommand
        Select Case e.CommandName
            Case "EDIT"
                If CheckFeature(Me.Loginid, Me.FormID, "EDIT", "Maxiloan") Then
                    Me.Id = "EDIT"
                    Dim lblId, lbDescription, lblAlamat, lblRT, lblRW, lblKelurahan, lblKecamatan, lblKota, lblKodePos, lblKontak As Label
                    Dim lblJabatan, lblHandphone, lblEmail, lblStatus As Label

                    lblId = CType(dtgLokasiAsset.Items(e.Item.ItemIndex).FindControl("lblId"), Label)
                    lbDescription = CType(dtgLokasiAsset.Items(e.Item.ItemIndex).FindControl("lblDescription"), Label)
                    lblAlamat = CType(dtgLokasiAsset.Items(e.Item.ItemIndex).FindControl("lblAlamat"), Label)
                    lblRT = CType(dtgLokasiAsset.Items(e.Item.ItemIndex).FindControl("lblRT"), Label)
                    lblRW = CType(dtgLokasiAsset.Items(e.Item.ItemIndex).FindControl("lblRW"), Label)
                    lblKelurahan = CType(dtgLokasiAsset.Items(e.Item.ItemIndex).FindControl("lblKelurahan"), Label)
                    lblKecamatan = CType(dtgLokasiAsset.Items(e.Item.ItemIndex).FindControl("lblKecamatan"), Label)
                    lblKota = CType(dtgLokasiAsset.Items(e.Item.ItemIndex).FindControl("lblKota"), Label)
                    lblKodePos = CType(dtgLokasiAsset.Items(e.Item.ItemIndex).FindControl("lblKodePos"), Label)
                    lblKontak = CType(dtgLokasiAsset.Items(e.Item.ItemIndex).FindControl("lblKontak"), Label)
                    lblJabatan = CType(dtgLokasiAsset.Items(e.Item.ItemIndex).FindControl("lblJabatan"), Label)
                    lblHandphone = CType(dtgLokasiAsset.Items(e.Item.ItemIndex).FindControl("lblHandphone"), Label)
                    lblEmail = CType(dtgLokasiAsset.Items(e.Item.ItemIndex).FindControl("lblEmail"), Label)
                    lblStatus = CType(dtgLokasiAsset.Items(e.Item.ItemIndex).FindControl("lblStatus"), Label)

                    pnlList.Visible = False
                    pnlDtGrid.Visible = False
                    pnlAdd.Visible = True
                    txtId.Text = lblId.Text
                    txtDescription.Text = lbDescription.Text
                    lblTitle.Text = "LOKASI ASSET - EDIT"

                    With oUcCompanyAdress
                        .Address = lblAlamat.Text
                        .RT = lblRT.Text
                        .RW = lblRW.Text
                        .Kelurahan = lblKelurahan.Text
                        .Kecamatan = lblKecamatan.Text
                        .City = lblKota.Text
                        .ZipCode = lblKodePos.Text
                        .Style = "Collection"
                        .BindAddress()
                        .ValidatorFalse()
                        .BPKBView()
                    End With
                    txtKontak.Text = lblKontak.Text
                    txtJabatan.Text = lblJabatan.Text
                    txtHandphone.Text = lblHandphone.Text
                    txtEmail.Text = lblEmail.Text

                    Dim checked As Boolean
                    If lblStatus.Text = "Y" Then checked = True Else checked = False
                    cbActive.Checked = checked

                End If
            Case "DELETE"
                If CheckFeature(Me.Loginid, Me.FormID, "DEL", "Maxiloan") Then
                    Dim lblId As Label
                    lblId = CType(dtgLokasiAsset.Items(e.Item.ItemIndex).FindControl("lblId"), Label)
                    With oCustomClass
                        .strConnection = GetConnectionString()
                        .Id = lblId.Text
                    End With
                    Dim strError As String
                    strError = oController.LokasiAssetDelete(oCustomClass)
                    If strError = "" Then
                        ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, False)
                    Else
                        ShowMessage(lblMessage, strError, True)
                    End If
                    DoBind(Me.SearchBy, Me.SortBy)
                End If
        End Select
    End Sub
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "ID") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + "ID"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub
#End Region
    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Buttonsearch.Click
        If oSearchBy.Text.Trim <> "" Then
            Me.SearchBy = oSearchBy.ValueID & " like '%" & oSearchBy.Text.Trim.Replace("'", "''") & "%'"
        Else
            Me.SearchBy = ""
        End If
        pnlDtGrid.Visible = True
        pnlList.Visible = True
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
    Private Sub DtgCasesSetting_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgLokasiAsset.ItemDataBound
        Dim m As Int32
        Dim imbDelete As ImageButton
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imbDelete"), ImageButton)
            imbDelete.Attributes.Add("onclick", "return fConfirm()")
        End If
    End Sub
    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        Me.SearchBy = ""
        oSearchBy.Text = ""
        oSearchBy.BindData()
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
    Private Sub btnSaveAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSaveAdd.Click
        pnlList.Visible = True
        pnlDtGrid.Visible = True
        pnlAdd.Visible = False

        Dim strError As String
        Dim oClassAddress As New Parameter.Address

        Select Case Id
            Case "ADD"
                With oCustomClass
                    .strConnection = GetConnectionString()
                    .Id = txtId.Text
                    .Description = txtDescription.Text
                    .LoginId = Me.Loginid
                    .Kontak = txtKontak.Text
                    .Jabatan = txtJabatan.Text
                    .Handphone = txtHandphone.Text
                    .Email = txtEmail.Text
                    .Status = CStr(IIf(cbActive.Checked = True, "Y", "N"))
                End With

                With oClassAddress
                    .Address = oUcCompanyAdress.Address
                    .RT = oUcCompanyAdress.RT
                    .RW = oUcCompanyAdress.RW
                    .Kecamatan = oUcCompanyAdress.Kecamatan
                    .Kelurahan = oUcCompanyAdress.Kelurahan
                    .City = oUcCompanyAdress.City
                    .ZipCode = oUcCompanyAdress.ZipCode
                End With

                strError = oController.LokasiAssetAdd(oCustomClass, oClassAddress)
                If strError <> "" Then
                    ShowMessage(lblMessage, strError, True)
                Else
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_SUCCESS, False)
                    DoBind(Me.SearchBy, Me.SortBy)
                    If Not (Me.Cache.Item("ID")) Is Nothing Then
                        Me.Cache.Remove("ID")
                    End If
                End If
            Case "EDIT"
                With oCustomClass
                    .strConnection = GetConnectionString()
                    .Id = txtId.Text
                    .Description = txtDescription.Text
                    .LoginId = Me.Loginid
                    .Kontak = txtKontak.Text
                    .Jabatan = txtJabatan.Text
                    .Handphone = txtHandphone.Text
                    .Email = txtEmail.Text
                    .Status = CStr(IIf(cbActive.Checked = True, "Y", "N"))
                End With

                With oClassAddress
                    .Address = oUcCompanyAdress.Address
                    .RT = oUcCompanyAdress.RT
                    .RW = oUcCompanyAdress.RW
                    .Kecamatan = oUcCompanyAdress.Kecamatan
                    .Kelurahan = oUcCompanyAdress.Kelurahan
                    .City = oUcCompanyAdress.City
                    .ZipCode = oUcCompanyAdress.ZipCode
                End With

                strError = oController.LokasiAssetEdit(oCustomClass, oClassAddress)
                If strError <> "" Then
                    ShowMessage(lblMessage, strError, True)
                Else
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
                    DoBind(Me.SearchBy, Me.SortBy)
                    If Not (Me.Cache.Item("ID")) Is Nothing Then
                        Me.Cache.Remove("ID")
                    End If
                End If
        End Select
    End Sub
    Private Sub ImgAddNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonAddNew.Click
        'If CheckFeature(Me.Loginid, Me.FormID, "ADD", "Maxiloan") Then
        '    Dim oDataTable As New DataTable
        pnlAdd.Visible = True
        pnlList.Visible = False
        pnlDtGrid.Visible = False
        Me.Id = "ADD"
        lblTitle.Text = "LOKASI ASSET - ADD"
        txtId.Text = ""
        txtDescription.Text = ""
        txtKontak.Text = ""
        txtJabatan.Text = ""
        txtHandphone.Text = ""
        txtEmail.Text = ""
        cbActive.Text = ""
        With oUcCompanyAdress
            .Address = ""
            .RT = ""
            .RW = ""
            .Kecamatan = ""
            .Kelurahan = ""
            .City = ""
            .ZipCode = ""
            .Style = "Collection"
            .BindAddress()
            .ValidatorFalse()
        End With
    End Sub
    Private Sub imbPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonPrint.Click
        If SessionInvalid() Then
            Exit Sub
        End If
        If CheckFeature(Me.Loginid, Me.FormID, "PRINT", Me.AppId) Then
            Dim cookie As HttpCookie = Request.Cookies(Me.FormID)
            If Not cookie Is Nothing Then
                cookie.Values("Where") = Me.SearchBy
                cookie.Values("SortBy") = Me.SortBy
                Response.AppendCookie(cookie)
            Else
                Dim cookieNew As New HttpCookie("LokasiAsset")
                cookieNew.Values.Add("Where", Me.SearchBy)
                cookieNew.Values.Add("SortBy", Me.SortBy)
                Response.AppendCookie(cookieNew)
            End If
            Response.Redirect("../setting/Report/LokasiAssetRpt.aspx")
        End If
    End Sub
    Private Sub btnCancelAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonCancelAdd.Click
        lblMessage.Visible = False
        InitialDefaultPanel()
        pnlDtGrid.Visible = True
    End Sub
End Class