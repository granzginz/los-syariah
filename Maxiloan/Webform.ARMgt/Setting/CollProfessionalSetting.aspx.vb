﻿#Region "imports"
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
Imports System.Drawing
Imports System.IO
Imports System.Collections.Generic
#End Region

Public Class CollProfessionalSetting
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents UCEmployee As UcFindEmployee
    Protected WithEvents oUcCompanyAdress As UcCompanyAddress
    Protected WithEvents UcBranchCollection1 As UcBranchCollection
    Protected WithEvents UcBankAccount As UcBankAccount

#Region " Private Const "
    Dim m_Collector As New CollectorProfController

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Dim strStyle As String = "Marketing"
#End Region

#Region " Property "
    Private Property CGID() As String
        Get
            Return CStr(ViewState("CGID"))
        End Get
        Set(ByVal Value As String)
            ViewState("CGID") = Value
        End Set
    End Property

    Private Property CollectorID() As String
        Get
            Return CStr(ViewState("CollectorID"))
        End Get
        Set(ByVal Value As String)
            ViewState("CollectorID") = Value
        End Set
    End Property

    Private Property CollectorName() As String
        Get
            Return CStr(ViewState("CollectorName"))
        End Get
        Set(ByVal Value As String)
            ViewState("CollectorName") = Value
        End Set
    End Property


    Private Property CGName() As String
        Get
            Return CStr(ViewState("CGName"))
        End Get
        Set(ByVal Value As String)
            ViewState("CGName") = Value
        End Set
    End Property

    Private Property CGHead() As String
        Get
            Return CStr(ViewState("CGHead"))
        End Get
        Set(ByVal Value As String)
            ViewState("CGHead") = Value
        End Set
    End Property

    Public Property ActionAddEdit() As String
        Get
            Return CStr(ViewState("ActionAddEdit"))
        End Get
        Set(ByVal Value As String)
            ViewState("ActionAddEdit") = Value
        End Set
    End Property
    Public Property BankID() As String
        Get
            Return CStr(ViewState("BankID"))
        End Get
        Set(ByVal Value As String)
            ViewState("BankID") = Value
        End Set
    End Property
    Public Property BankbRANCHID() As String
        Get
            Return CStr(ViewState("BankbRANCHID"))
        End Get
        Set(ByVal Value As String)
            ViewState("BankbRANCHID") = Value
        End Set
    End Property
    Public Property BankName() As String
        Get
            Return CStr(ViewState("BankName"))
        End Get
        Set(ByVal Value As String)
            ViewState("BankName") = Value
        End Set
    End Property
    Public Property AccountName() As String
        Get
            Return CStr(ViewState("AccountName"))
        End Get
        Set(ByVal Value As String)
            ViewState("AccountName") = Value
        End Set
    End Property
    Public Property AccountNo() As String
        Get
            Return CStr(ViewState("AccountNo"))
        End Get
        Set(ByVal Value As String)
            ViewState("AccountNo") = Value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        'Put user code to initialize the page here
        If SessionInvalid() Then
            Exit Sub
        End If
        'createxmdlist()
        If Not Me.IsPostBack Then
            InitialDefaultPanel()
            Me.CGID = Me.GroubDbID
            Me.FormID = "CollProfessional"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.SearchBy = "all"
                Me.SortBy = ""
                If Request("cond") <> "" Then
                    Me.SearchBy = Request("cond")
                Else
                    Me.SearchBy = "ALL"
                End If
                Me.SortBy = ""
                
                BindGridCollector(Me.SearchBy, Me.SortBy)
                getSupervisorCombo()
            End If
        End If
    End Sub

#Region " PanelAllFalse "
    Private Sub PanelAllFalse()
        pnlSearch.Visible = False
        pnlAddEdit.Visible = False
        pnlList.Visible = False
    End Sub
#End Region

#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridCollector(Me.SearchBy, Me.SortBy)
        pnlList.Visible = True
    End Sub

    Protected Sub btnPageNumb_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPageNumb.Click

        If txtPage.Text = "" Then
            txtPage.Text = "0"
        Else
            If IsNumeric(txtPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                    currentPage = CType(txtPage.Text, Int32)
                    BindGridCollector(Me.SearchBy, Me.SortBy)
                End If
            End If
        End If

        pnlList.Visible = True

    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If

        If Me.CGID = "" Then Me.CGID = Me.GroubDbID

        BindGridCollector(Me.SearchBy, Me.SortBy)
        pnlList.Visible = True
    End Sub

#End Region

#Region " InitialDefaultPanel "
    Private Sub InitialDefaultPanel()
        pnlSearch.Visible = True
        pnlList.Visible = True
        pnlAddEdit.Visible = False
    End Sub
#End Region

    'Sub createxmdlist()
    '    Dim dtEntity As New DataTable
    '    Dim dsList As New DataSet
    '    Dim oAction As Parameter.ActionResult
    '    Dim strconn As String = getconnectionstring

    '    oAction = m_Collector.GetCollectorReport(strconn)
    '    dtEntity = oAction.ListData

    '    dsList.Tables.Add(dtEntity.Clone)
    '    dsList.DataSetName = "Action"
    '    dsList.WriteXmlSchema("c:\reportxmd\action.xmd")
    'End Sub

#Region " BindGrid"
    Sub BindGridCollector(ByVal cmdWhere As String, ByVal cmSort As String)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim oCollector As New Parameter.Collector

        With oCollector
            .strConnection = GetConnectionString()
            .CGID = Me.CGID
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With


        oCollector = m_Collector.GetCollectorProfList(oCollector)

        With oCollector
            lblrecord.Text = CType(.TotalRecord, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecord
        End With

        dtsEntity = oCollector.ListData
        dtvEntity = dtsEntity.DefaultView
        dtvEntity.Sort = Me.SortBy
        dtgCollector.DataSource = dtvEntity
        Try
            dtgCollector.DataBind()
        Catch
            dtgCollector.CurrentPageIndex = 0
            dtgCollector.DataBind()
        End Try

        PagingFooter()
    End Sub
#End Region

    Sub ShowBankAccount()
        With UcBankAccount
            .BindBankAccount()
            .ValidatorTrue()
        End With
    End Sub
    Private Sub BtnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonAdd.Click
        If CheckFeature(Me.Loginid, Me.FormID, "Add", Me.AppId) Then
            If SessionInvalid() Then
                Exit Sub
            End If
        End If

        PanelAllFalse()
        ClearAddForm()
        ShowBankAccount()
        With UcBankAccount
            .clearBankBranch()
            .AccountName = ""
            .AccountNo = ""            
        End With
        
        Me.ActionAddEdit = "ADD"
        pnlAddEdit.Visible = True
        lblMenuAddEdit.Text = Me.ActionAddEdit
    End Sub

    Private Sub BtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSearch.Click
        Dim StrSearchBy As String = cboSearchBy.SelectedItem.Value
        Dim StrSearchByValue As String = TxtSearchByValue.Text.Trim
        If StrSearchByValue = "" Then
            Me.SearchBy = ""
        Else
            Me.SearchBy = " CGID ='" + UcBranchCollection1.BranchID + "' and " + StrSearchBy + " like '%" + StrSearchByValue + "%' "
        End If

        Me.CGID = UcBranchCollection1.BranchID
        Me.CGName = UcBranchCollection1.BranchName

        PanelAllFalse()
        BindGridCollector(Me.SearchBy, Me.SortBy)
        pnlSearch.Visible = True
        pnlList.Visible = True

    End Sub

    Private Sub viewbyID(ByVal oCollector As Parameter.Collector)

        Dim ds As New DataTable

        lblMenuAddEdit.Text = Me.ActionAddEdit
        lblCollGroup.Text = Me.CGName

        oCollector = m_Collector.GetCollectorProfByID(oCollector)
        ds = oCollector.ListData

        lblCollGroup.Text = Me.CGName
        lblExecutorID.Text = CStr(ds.Rows(0).Item("CollectorID")).Trim
        txtExecutorName.Text = CStr(ds.Rows(0).Item("CollectorName")).Trim

        lblCollGroup.Visible = True
        lblExecutorID.Visible = True
        txtExecutorID.Visible = False
        txtExecutorName.Visible = True

        txtEmail.Text = CStr(ds.Rows(0).Item("Email")).Trim
        txtMobilePhone.Text = CStr(ds.Rows(0).Item("MobilePhone")).Trim
        txtNotes.Text = CStr(ds.Rows(0).Item("Notes")).Trim
        txtNoKTP.Text = CStr(ds.Rows(0).Item("IDNumber")).Trim

        With oUcCompanyAdress
            .Address = CStr(ds.Rows(0).Item("Address")).Trim
            .RT = CStr(IIf(IsDBNull(ds.Rows(0).Item("RT")), "-", CStr(ds.Rows(0).Item("RT")).Trim))
            .RW = CStr(IIf(IsDBNull(ds.Rows(0).Item("RW")), "-", CStr(ds.Rows(0).Item("RW")).Trim))
            .Kelurahan = CStr(IIf(IsDBNull(ds.Rows(0).Item("Kelurahan")), "-", CStr(ds.Rows(0).Item("Kelurahan")).Trim))
            .Kecamatan = CStr(IIf(IsDBNull(ds.Rows(0).Item("Kecamatan")), "-", CStr(ds.Rows(0).Item("Kecamatan")).Trim))
            .City = CStr(ds.Rows(0).Item("City")).Trim
            .ZipCode = CStr(ds.Rows(0).Item("ZipCode")).Trim
            .AreaPhone1 = CStr(ds.Rows(0).Item("AreaPhone1")).Trim
            .Phone1 = CStr(ds.Rows(0).Item("PhoneNo1")).Trim
            .AreaPhone2 = CStr(ds.Rows(0).Item("AreaPhone2")).Trim
            .Phone2 = CStr(ds.Rows(0).Item("PhoneNo2")).Trim
            .AreaFax = CStr(ds.Rows(0).Item("AreaFax")).Trim
            .Fax = CStr(ds.Rows(0).Item("Fax")).Trim
            .Style = "Collection"
            .BindAddress()
            .ValidatorFalse()
        End With

        cboSupervisor.SelectedIndex = cboSupervisor.Items.IndexOf(cboSupervisor.Items.FindByValue(CStr(ds.Rows(0).Item("Supervisor")).Trim))

        Dim checked As Boolean
        If CStr(ds.Rows(0).Item("Active")) = "Y" Then checked = True Else checked = False
        cbActive.Checked = checked

        lblbintang1.Visible = False
        lblbintang2.Visible = False

        With UcBankAccount
            .BankBranchId = CStr(ds.Rows(0).Item("BankBranchId")).Trim
            .BindBankAccount()
            .ValidatorTrue()            
            .BankID = CStr(ds.Rows(0).Item("BankID")).Trim
            .AccountName = CStr(ds.Rows(0).Item("AccountName")).Trim
            .AccountNo = CStr(ds.Rows(0).Item("AccountNo")).Trim
        End With

        rbnCollectorStatus.SelectedIndex = rbnCollectorStatus.Items.IndexOf(rbnCollectorStatus.Items.FindByValue(CStr(ds.Rows(0).Item("CollectorStatus")).ToString))
        txtNoNPWP.Text = CStr(ds.Rows(0).Item("CollectorNPWP")).Trim
        cekimage()
    End Sub

    Private Sub ClearAddForm()

        lblCollGroup.Visible = True
        lblCollGroup.Text = Me.CGName
        lblCollGroup.Visible = True
        lblExecutorID.Visible = False
        lblExecutorName.Visible = False
        txtExecutorID.Text = ""
        txtExecutorName.Text = ""
        txtExecutorID.Visible = True
        txtExecutorName.Visible = True
        lblbintang1.Visible = True
        lblbintang2.Visible = True

        With oUcCompanyAdress
            .Address = ""
            .RT = ""
            .RW = ""
            .Kecamatan = ""
            .Kelurahan = ""
            .City = ""
            .ZipCode = ""
            .AreaPhone1 = ""
            .Phone1 = ""
            .AreaPhone2 = ""
            .Phone2 = ""
            .AreaFax = ""
            .Fax = ""
            .Style = "Collection"
            .BindAddress()
            .ValidatorFalse()
        End With

        txtEmail.Text = ""
        txtNotes.Text = ""
        txtMobilePhone.Text = ""
        getSupervisorCombo()
        cbActive.Checked = True
        cbActive.DataBind()
        txtNoKTP.Text = ""
    End Sub

    Private Sub Add()
        Dim oCollector As New Parameter.Collector
        Dim oClassAddress As New Parameter.Address
        Dim oClassPersonal As New Parameter.Personal

        With oCollector
            .strConnection = GetConnectionString()
            .CGID = Me.CGID.Trim
            .CollectorID = txtExecutorID.Text.Trim
            .CollectorName = txtExecutorName.Text.Trim
            .CollectorType = "EP"
            .Supervisor = cboSupervisor.SelectedItem.Value.Trim
            .Active = CStr(IIf(cbActive.Checked = True, "Y", "N"))
            .Notes = txtNotes.Text.Trim
            .IDNumber = txtNoKTP.Text.Trim
            .CollectorNPWP = txtNoNPWP.Text.Trim
            .CollectorStatus = rbnCollectorStatus.SelectedIndex

            .collBankID = UcBankAccount.BankID.Trim
            .collBankBranchID = IIf(UcBankAccount.BankBranchId.Trim = "", "0", UcBankAccount.BankBranchId.Trim).ToString
            .collBankBranch = UcBankAccount.BankBranch.Trim
            .collAcountName = UcBankAccount.AccountName.Trim
            .collAccountNo = UcBankAccount.AccountNo.Trim
        End With

        With oClassAddress
            .Address = oUcCompanyAdress.Address
            .RT = oUcCompanyAdress.RT
            .RW = oUcCompanyAdress.RW
            .Kecamatan = oUcCompanyAdress.Kecamatan
            .Kelurahan = oUcCompanyAdress.Kelurahan
            .City = oUcCompanyAdress.City
            .ZipCode = oUcCompanyAdress.ZipCode
            .AreaPhone1 = oUcCompanyAdress.AreaPhone1
            .Phone1 = oUcCompanyAdress.Phone1
            .AreaPhone2 = oUcCompanyAdress.AreaPhone2
            .Phone2 = oUcCompanyAdress.Phone2
            .AreaFax = oUcCompanyAdress.AreaFax
            .Fax = oUcCompanyAdress.Fax
        End With

        With oClassPersonal
            .MobilePhone = txtMobilePhone.Text.Trim
            .Email = txtEmail.Text.Trim
        End With

        Try
            m_Collector.CollectorProfAdd(oCollector, oClassAddress, oClassPersonal)
            ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_SUCCESS, False)
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try

    End Sub

    Private Sub edit(ByVal CGID As String)

        Dim oCollector As New Parameter.Collector
        Dim oClassAddress As New Parameter.Address
        Dim oClassPersonal As New Parameter.Personal

        With oCollector
            .strConnection = GetConnectionString()
            .CGID = Me.CGID.Trim
            .CollectorID = Me.CollectorID
            .CollectorName = txtExecutorName.Text.Trim
            .CollectorType = "EP"
            .Supervisor = cboSupervisor.SelectedItem.Value
            .Active = CStr(IIf(cbActive.Checked = True, "Y", "N"))
            .Notes = txtNotes.Text.Trim
            .BranchId = BranchID
            .IDNumber = txtNoKTP.Text.Trim
            .CollectorNPWP = txtNoNPWP.Text.Trim
            .CollectorStatus = rbnCollectorStatus.SelectedValue

            .collBankID = UcBankAccount.BankID
            .collBankBranchID = IIf(UcBankAccount.BankBranchId.Trim = "", "0", UcBankAccount.BankBranchId.Trim).ToString
            .collBankBranch = UcBankAccount.BankBranch
            .collAcountName = UcBankAccount.AccountName
            .collAccountNo = UcBankAccount.AccountNo
        End With

        With oClassAddress
            .Address = oUcCompanyAdress.Address
            .RT = oUcCompanyAdress.RT
            .RW = oUcCompanyAdress.RW
            .Kecamatan = oUcCompanyAdress.Kecamatan
            .Kelurahan = oUcCompanyAdress.Kelurahan
            .City = oUcCompanyAdress.City
            .ZipCode = oUcCompanyAdress.ZipCode
            .AreaPhone1 = oUcCompanyAdress.AreaPhone1
            .Phone1 = oUcCompanyAdress.Phone1
            .AreaPhone2 = oUcCompanyAdress.AreaPhone2
            .Phone2 = oUcCompanyAdress.Phone2
            .AreaFax = oUcCompanyAdress.AreaFax
            .Fax = oUcCompanyAdress.Fax
        End With

        With oClassPersonal
            .MobilePhone = txtMobilePhone.Text.Trim
            .Email = txtEmail.Text.Trim
        End With

        Try
            m_Collector.CollectorProfEdit(oCollector, oClassAddress, oClassPersonal)
            ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

    Private Sub dtgCollector_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgCollector.ItemCommand
        Try
            Dim lblBankID As New Label
            Dim lblBankBranchID As New Label
            Dim lblAccountNo As New Label
            Dim lblAccountName As New Label

            Me.CollectorID = e.Item.Cells(4).Text.Trim
            lblBankID.Text = CType(e.Item.FindControl("BankID"), Label).Text.Trim
            lblBankBranchID.Text = CType(e.Item.FindControl("BankBranchID"), Label).Text.Trim
            lblAccountName.Text = CType(e.Item.FindControl("AccountName"), Label).Text.Trim
            lblAccountNo.Text = CType(e.Item.FindControl("AccountNo"), Label).Text.Trim

            Select Case e.CommandName
                Case "Edit"
                    If CheckFeature(Me.Loginid, Me.FormID, "Edit", Me.AppId) Then
                        If SessionInvalid() Then
                            Exit Sub
                        End If
                    End If


                    Dim oCollector As New Parameter.Collector
                    With oCollector
                        .strConnection = GetConnectionString()
                        .CGID = Me.CGID
                        .CollectorID = Me.CollectorID
                        Me.ActionAddEdit = "EDIT"

                        .collBankID = lblBankID.Text
                        .collBankBranchID = lblBankBranchID.Text
                        .collAcountName = lblAccountName.Text
                        .collAccountNo = lblAccountNo.Text
                    End With

                    PanelAllFalse()
                    viewbyID(oCollector)
                    pnlAddEdit.Visible = True

                Case "Delete"
                    If CheckFeature(Me.Loginid, Me.FormID, "Del", Me.AppId) Then
                        If SessionInvalid() Then
                            Exit Sub
                        End If
                    End If

                    Dim oCollector As New Parameter.Collector
                    With oCollector
                        .strConnection = GetConnectionString()
                        .CGID = Me.CGID
                        .CollectorID = Me.CollectorID
                    End With

                    Try
                        m_Collector.CollectorProfDelete(oCollector)
                        ShowMessage(lblMessage, "Hapus Data Berhasil ", True)
                    Catch ex As Exception
                        ShowMessage(lblMessage, ex.Message, True)

                    Finally
                        pnlAddEdit.Visible = False
                        BindGridCollector("ALL", "")
                        pnlList.Visible = True
                    End Try

                Case "MOU"
                    Dim target As String = "CollProfessionalSetting" + e.CommandName
                    BranchID = Me.CGID
                    CollectorID = Me.CollectorID
                    Dim XX As String
                    XX = "" & target & ".aspx?CollectorID=" & CollectorID & "&BranchID=" & BranchID & ""
                    Response.Redirect(XX)
            End Select
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
       
    End Sub

    Private Sub BtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        cboSearchBy.SelectedIndex = 0
        TxtSearchByValue.Text = ""

        Me.SortBy = ""
        Me.SearchBy = "all"

        BindGridCollector("ALL", "")
        pnlList.Visible = True
    End Sub

    Function LinkTo(ByVal strCGID As String) As String
        Return "javascript:OpenViewCollectionGroup('" & strCGID & "')"
    End Function

    Function LinkToCollector(ByVal strCollectorID As String, ByVal cgid As String) As String
        Return "javascript:OpenViewCollector('" & strCollectorID & "','" & cgid & "')"
    End Function

    Private Sub dtgCollector_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgCollector.ItemDataBound

        Dim imbDelete As ImageButton

        If e.Item.ItemIndex >= 0 Then

            imbDelete = CType(e.Item.FindControl("ImbDelete"), ImageButton)
            imbDelete.Attributes.Add("onclick", "return fConfirm()")
            BranchID = BranchID
        End If

    End Sub

    Private Sub imgCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        PanelAllFalse()
        InitialDefaultPanel()
    End Sub

    Private Sub imbSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
        Select Case Me.ActionAddEdit
            Case "ADD"
                PanelAllFalse()
                Add()
                Me.SearchBy = "all"
                Me.SortBy = ""
                BindGridCollector(Me.SearchBy, Me.SortBy)
                pnlSearch.Visible = True
                pnlList.Visible = True

            Case "EDIT"
                PanelAllFalse()
                Me.SearchBy = "all"
                Me.SortBy = ""
                edit(Me.CGID)
                BindGridCollector(Me.SearchBy, Me.SortBy)
                pnlSearch.Visible = True
                pnlList.Visible = True

        End Select
    End Sub

    Private Sub imbPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonPrint.Click
        Response.Redirect("report/CollectorProfRpt.aspx")
    End Sub

    Private Sub getSupervisorCombo()
        Dim oCollectorList As New Parameter.Collector
        Dim oCollector As New Parameter.Collector
        Dim strconn As String = GetConnectionString()
        Dim dt As New DataTable

        With oCollector
            .strConnection = GetConnectionString()
            .CGID = Me.GroubDbID
            .CollectorType = "COM"
        End With

        oCollectorList = m_Collector.GetSupervisorCombo(oCollector)
        dt = oCollectorList.ListData
        cboSupervisor.DataSource = dt
        cboSupervisor.DataValueField = "SupervisorID"
        cboSupervisor.DataTextField = "SupervisorName"
        cboSupervisor.DataBind()

    End Sub

#Region "Upload image eksekutor"
    Private Sub cekimage()
        If File.Exists(pathFile("Executor", lblExecutorID.Text) + "LampiranKTP.jpg") Then
            imgLampiranKTP.ImageUrl = ResolveClientUrl(getDownloadPath(sesBranchId.Replace("'", "") & "/Executor/" + lblExecutorID.Text + "/LampiranKTP.jpg"))
        Else
            imgLampiranKTP.ImageUrl = ResolveClientUrl("../../xml/NoFoto.png")
        End If

        imgLampiranKTP.Height = 200
        imgLampiranKTP.Width = 300
 
    End Sub
    Private Function getpathFolder() As String
        Dim oClass As New Parameter.GeneralSetting
        Dim oCtr As New GeneralSettingController
        With oClass
            .strConnection = GetConnectionString()
            .GSID = "UploadFolder"
            .ModuleID = "GEN"
        End With
        Try
            Return oCtr.GetGeneralSettingValue(oClass).Trim
        Catch ex As Exception
            Return "0"
        End Try
    End Function
    Private Function getDownloadPath(filePath As String) As String
        Return Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd("/"c) + "/Webform.Utility/GetImage.aspx?file=" & Server.UrlEncode(getpathFolder() & filePath)
    End Function
    Private Function pathFile(ByVal Group As String, ByVal ExecutorID As String) As String
        Dim strDirectory As String = ""
        strDirectory = getpathFolder() & sesBranchId.Replace("'", "") & "\" & Group & "\" & ExecutorID & "\"
        If Not System.IO.Directory.Exists(strDirectory) Then
            System.IO.Directory.CreateDirectory(strDirectory)
        End If
        Return strDirectory
    End Function

    Private Sub btnupload_Click(sender As Object, e As System.EventArgs) Handles btnupload.Click
        Dim FileName As String = ""
        Dim strExtension As String = ""

        If lblExecutorID.Text = "" Then
            txtExecutorID.Text = lblExecutorID.Text
        End If

        If uplLampiranKTP.HasFile Then
            If uplLampiranKTP.PostedFile.ContentType = "image/jpeg" Then
                FileName = "LampiranKTP"
                strExtension = Path.GetExtension(uplLampiranKTP.PostedFile.FileName)
                uplLampiranKTP.PostedFile.SaveAs(pathFile("Executor", lblExecutorID.Text) + FileName + strExtension)
            End If
        End If
        cekimage()
    End Sub
#End Region
End Class