﻿#Region "Imports"
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class CollectorRAL
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents UCCompanyAdress1 As UcCompanyAddress
    Protected WithEvents UCContactPerson1 As UcContactPerson
    Protected WithEvents UCbranch1 As UcBranch

#Region " Private Const "
    Dim m_Collector As New CollectorController

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

#End Region

#Region " Property "
    Private Property CGID() As String
        Get
            Return CStr(viewstate("CGID"))
        End Get
        Set(ByVal Value As String)
            viewstate("CGID") = Value
        End Set
    End Property

    Private Property CGName() As String
        Get
            Return CStr(viewstate("CGName"))
        End Get
        Set(ByVal Value As String)
            viewstate("CGName") = Value
        End Set
    End Property

    Private Property CollectorID() As String
        Get
            Return CStr(viewstate("CollectorID"))
        End Get
        Set(ByVal Value As String)
            viewstate("CollectorID") = Value
        End Set
    End Property


    Private Property CollectorName() As String
        Get
            Return CStr(viewstate("CollectorName"))
        End Get
        Set(ByVal Value As String)
            viewstate("CollectorName") = Value
        End Set
    End Property


#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        'Put user code to initialize the page here
        If SessionInvalid() Then
            Exit Sub
        End If
        'createxmdlist()
        If Not Me.IsPostBack Then
            Me.FormID = "VWZIP"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                InitialDefaultPanel()

                If Request.QueryString("CGID") <> "" Then Me.CGID = Request.QueryString("CGID")
                If Request.QueryString("CollectorID") <> "" Then Me.CollectorID = Request.QueryString("CollectorID")
                If Request.QueryString("CGName") <> "" Then Me.CGName = Request.QueryString("CGName")
                If Request.QueryString("CollectorName") <> "" Then Me.CollectorName = Request.QueryString("CollectorName")
                Me.SearchBy = "all"
                Me.SortBy = ""
                lblCGName.Text = Me.CGName.Trim
                lblExecutorName.Text = Me.CollectorName.Trim
                BindGridZipCode(Me.CGID, Me.CollectorID, Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub

#Region " PanelAllFalse "
    Private Sub PanelAllFalse()
        lblMessage.Visible = False
        pnlList.Visible = False
    End Sub
#End Region


#Region " Navigation "

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        BindGridZipCode(Me.CGID, Me.CollectorID, Me.SearchBy, Me.SortBy)
    End Sub

#End Region

#Region " InitialDefaultPanel "
    Private Sub InitialDefaultPanel()
        pnlList.Visible = True
    End Sub
#End Region

#Region " BindGrid"
    Sub BindGridZipCode(ByVal CGID As String, ByVal CollectorID As String, ByVal cmdWhere As String, ByVal cmSort As String)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim oCollector As New Parameter.Collector
        Dim oCollectorList As New Parameter.Collector


        With oCollector
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
            .CGID = Me.CGID
            .CollectorID = Me.CollectorID
        End With


        oCollectorList = m_Collector.GetCollectorRAL(oCollector)

        dtsEntity = oCollectorList.ListData
        dtvEntity = dtsEntity.DefaultView
        dtvEntity.Sort = Me.SortBy
        dtgRAL.DataSource = dtvEntity
        Try
            dtgRAL.DataBind()
        Catch
            dtgRAL.CurrentPageIndex = 0
            dtgRAL.DataBind()
        End Try

    End Sub
#End Region

    Private Sub imbBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonBack.Click
        Response.Redirect("CollectorView.aspx?CGID=" & Me.CGID & "&CollectorID=" & Me.CollectorID & "")
    End Sub

End Class