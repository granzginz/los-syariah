﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class CollectorZipCodeDetail
    Inherits Maxiloan.Webform.WebBased    

#Region "Constanta"
    Private m_controller As New CollectorController
    Dim oCollector As New Parameter.Collector
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Dim oRow As DataRow
#End Region
#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property
    Private Property Kecamatan() As String
        Get
            Return CType(ViewState("Kecamatan"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Kecamatan") = Value
        End Set
    End Property
    Private Property Kota() As String
        Get
            Return CType(ViewState("Kota"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Kota") = Value
        End Set
    End Property
    Private Property TempDataTable() As DataTable
        Get
            Return CType(ViewState("TempDataTable"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            ViewState("TempDataTable") = Value
        End Set
    End Property
    Private Property CollectorID() As String
        Get
            Return CType(ViewState("CollectorID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CollectorID") = Value
        End Set
    End Property
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not Me.IsPostBack Then
            Me.Kecamatan = Request.QueryString("kecamatan")
            Me.Kota = Request.QueryString("kota")
            Me.Sort = "CollectorID ASC"
            Me.CmdWhere = ""
            getComboCollectorType()
            BindByKecamatan()
            BindGrid(Me.CmdWhere)
        End If
    End Sub
    Sub BindGrid(ByVal cmdWhere As String)
        Dim dtEntity As DataTable = Nothing
        Dim oCustomClass As New Parameter.Collector


        Dim DataList As New List(Of Parameter.Collector)
        Dim custom As New Parameter.Collector
        If Not TempDataTable Is Nothing Then
            For index = 0 To TempDataTable.Rows.Count - 1
                custom = New Parameter.Collector

                custom.CollectorID = TempDataTable.Rows(index).Item("CollectorID").ToString.Trim

                DataList.Add(custom)
            Next
        End If

        If cmdWhere Is Nothing Then
            cmdWhere = ""
        End If


        oCustomClass.PageSize = pageSize
        oCustomClass.WhereCond = cmdWhere
        oCustomClass.SortBy = Me.Sort
        oCustomClass.CurrentPage = currentPage
        oCustomClass.strConnection = GetConnectionString()
        oCustomClass = m_controller.GetCollectorZPDetail(oCustomClass)

        If Not oCustomClass Is Nothing Then
            dtEntity = oCustomClass.ListData
            recordCount = oCustomClass.TotalRecord
        Else
            recordCount = 0
        End If

        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()
        PagingFooter()

         For intLoopGrid = 0 To dtgPaging.Items.Count - 1
            Dim Chek As CheckBox
            Chek = CType(dtgPaging.Items(intLoopGrid).FindControl("Chek"), CheckBox)
            Dim CollectorID As String = dtgPaging.DataKeys.Item(intLoopGrid).ToString


            Me.CollectorID = CollectorID

            Dim query As New Parameter.Collector
            If DataList.Count > 0 Then
                query = DataList.Find(AddressOf PredicateFunction)
            Else
                query = Nothing
            End If

            If Not query Is Nothing Then
                Chek.Checked = True
            End If
        Next
    End Sub
    Public Function PredicateFunction(ByVal custom As Parameter.Collector) As Boolean
        Return custom.CollectorID.Trim = Me.CollectorID.Trim
    End Function
    Sub BindData()
        Dim DataList As New List(Of Parameter.Collector)
        Dim Collector As New Parameter.Collector        

        If TempDataTable Is Nothing Then
            TempDataTable = New DataTable
            With TempDataTable
                .Columns.Add(New DataColumn("CollectorID", GetType(String)))
            End With
        Else
            For index = 0 To TempDataTable.Rows.Count - 1
                Collector = New Parameter.Collector

                Collector.CollectorID = TempDataTable.Rows(index).Item("CollectorID").ToString.Trim
                DataList.Add(Collector)
            Next
        End If


        Dim oRow As DataRow

        For intLoopGrid = 0 To dtgPaging.Items.Count - 1
            Dim Chek As CheckBox
            Chek = CType(dtgPaging.Items(intLoopGrid).FindControl("Chek"), CheckBox)
            Dim CollectorID As String = dtgPaging.DataKeys.Item(intLoopGrid).ToString


            Me.CollectorID = CollectorID

            Dim query As New Parameter.Collector
            If DataList.Count > 0 Then
                query = DataList.Find(AddressOf PredicateFunction)
            Else
                query = Nothing
            End If


            If Chek.Checked And query Is Nothing Then
                oRow = TempDataTable.NewRow()
                oRow("CollectorID") = Me.CollectorID
                TempDataTable.Rows.Add(oRow)
            End If
        Next
    End Sub
    Sub BindClear()
        TempDataTable = New DataTable
        With TempDataTable
            .Columns.Add(New DataColumn("CollectorID", GetType(String)))
        End With
    End Sub
    Sub BindByKecamatan()
        Dim oCustomClass As New Parameter.Collector
        Dim oRow As DataRow
        
        oCustomClass.Kecamatan = Me.Kecamatan
        oCustomClass.Kota = Me.Kota
        oCustomClass.Status = "1"
        oCustomClass.strConnection = GetConnectionString()
        oCustomClass = m_controller.GetCollectorZPByKecamatan(oCustomClass)

        If Not oCustomClass Is Nothing Then

            If TempDataTable Is Nothing Then
                TempDataTable = New DataTable
                With TempDataTable
                    .Columns.Add(New DataColumn("CollectorID", GetType(String)))
                End With

                For index = 0 To oCustomClass.ListData.Rows.Count - 1
                    oRow = TempDataTable.NewRow()
                    oRow("CollectorID") = oCustomClass.ListData.Rows(index).Item("CollectorID").ToString.Trim
                    TempDataTable.Rows.Add(oRow)
                Next
            Else

                For index = 0 To oCustomClass.ListData.Rows.Count - 1
                    oRow = TempDataTable.NewRow()
                    oRow("CollectorID") = oCustomClass.ListData.Rows(index).Item("CollectorID").ToString.Trim
                    TempDataTable.Rows.Add(oRow)
                Next

            End If
        End If
    End Sub
    Sub BindKodePos(ByVal ListArray As List(Of String))
        Dim oCustomClass As New Parameter.Collector        

        oCustomClass.Kecamatan = Me.Kecamatan
        oCustomClass.Kota = Me.Kota
        oCustomClass.Status = "2"
        oCustomClass.strConnection = GetConnectionString()
        oCustomClass = m_controller.GetCollectorZPByKecamatan(oCustomClass)

        If Not oCustomClass Is Nothing Then

            For index = 0 To oCustomClass.ListData.Rows.Count - 1

                If Not ListArray.Contains(oCustomClass.ListData.Rows(index).Item("ZipCode").ToString.Trim) Then
                    ListArray.Add(oCustomClass.ListData.Rows(index).Item("ZipCode").ToString.Trim)
                End If

            Next
        End If
    End Sub
    Function CreateDataTable() As DataTable
        Dim data As New DataTable
        With data
            .Columns.Add(New DataColumn("CollectorID", GetType(String)))
            .Columns.Add(New DataColumn("ZipCode", GetType(String)))            
        End With

        Return data
    End Function
#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindData()
        BindGrid(Me.CmdWhere)
    End Sub
    Private Sub imgbtnPageNumb_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int32)
                BindData()
                BindGrid(Me.CmdWhere)
            End If
        End If
    End Sub
#End Region
#Region "Sorting"
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindData()
        BindGrid(Me.CmdWhere)
    End Sub
#End Region
#Region "Button"
    Private Sub ButtonCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        BindClear()        
        Response.Redirect("CollectorZipCode.aspx?page=detail&err=cancel")
    End Sub
    Private Sub ButtonSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
        Dim oCustomClass As New Parameter.Collector
        Dim ListKodePos As New List(Of String)
        Dim data As New DataTable
        Dim oRow As DataRow
        BindClear()

        If TempDataTable Is Nothing Then
            BindData()
        Else
            If TempDataTable.Rows.Count = 0 Then
                BindData()
            End If
        End If

        BindKodePos(ListKodePos)
        data = CreateDataTable()

        For index = 0 To TempDataTable.Rows.Count - 1

            For indexKodePos = 0 To ListKodePos.Count - 1
                oRow = data.NewRow()
                oRow("CollectorID") = TempDataTable.Rows(index).Item("CollectorID").ToString.Trim
                oRow("ZipCode") = ListKodePos(indexKodePos).Trim
                data.Rows.Add(oRow)

            Next

        Next


        Try
            oCustomClass.strConnection = GetConnectionString()
            oCustomClass.BranchId = sesBranchId.Replace("'", "")
            oCustomClass.BusinessDate = Me.BusinessDate
            oCustomClass.LoginId = Me.Loginid
            oCustomClass.Kota = Me.Kota
            oCustomClass.Kecamatan = Me.Kecamatan

            oCustomClass = m_controller.CollectorZPSave(oCustomClass, data)                
            Response.Redirect("CollectorZipCode.aspx?page=detail&err=ok")
        Catch ex As Exception            
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub
#End Region
#Region "ItemDataBound"
    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        
        If e.Item.ItemIndex >= 0 Then            
            e.Item.Cells(3).Text = cboCollectorType.Items.FindByValue(e.Item.Cells(3).Text.Trim).Text
        End If
    End Sub
#End Region
    Private Sub getComboCollectorType()
        Dim dtEntity As New DataTable
        Dim m_cust As New CustomerController
        Dim oCustomer As New Parameter.Customer
        oCustomer.strConnection = GetConnectionString()
        oCustomer.Table = "tblCollectorType"
        oCustomer = m_cust.CustomerPersonalAdd(oCustomer)
        If Not oCustomer Is Nothing Then
            dtEntity = oCustomer.listdata
        End If
        cboCollectorType.DataTextField = "Description"
        cboCollectorType.DataValueField = "ID"
        cboCollectorType.DataSource = dtEntity
        cboCollectorType.DataBind()
    End Sub
End Class