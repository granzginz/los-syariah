﻿#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
Imports Maxiloan.General.CommonCacheHelper
#End Region

Public Class LetterTemplate
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oSearchBy As UcSearchBy

#Region "Constanta"
    'Private m_controller As New PremiumToCustomerController
    Private oCollLetter As New Parameter.CollLetter
    Private m_controller As New CollLetterController
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private Enum ActionType
        Add
        Edit
    End Enum
#End Region
#Region "Properties"
    Private Property SaveAction() As ActionType
        Get
            Return DirectCast(viewstate("SaveAction"), ActionType)
        End Get
        Set(ByVal Value As ActionType)
            viewstate("SaveAction") = Value

        End Set
    End Property
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        'Put user code to initialize the page here
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            InitialDefaultPanel()
            viewLetter.NavigateUrl = "javascript:OpenCollLetterFieldView('" & "Collection" & "')"

            Me.FormID = "CollLetterTemplate"
            oSearchBy.ListData = "LetterId,Letter ID-LetterName,Letter Name"
            oSearchBy.BindData()
            If CheckForm(Me.Loginid, Me.FormID, "Maxiloan") Then
                Me.SearchBy = ""
                Me.SortBy = ""
            End If
        End If
    End Sub
#Region "InitialDefaultPanel"
    Private Sub InitialDefaultPanel()
        pnlTop.Visible = True
        pnlGrid.Visible = True
        pnlAddEdit.Visible = False        
    End Sub
#End Region
#Region "Navigation "
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridEntity()
    End Sub
#End Region
#Region "Paging Footer"

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then            
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            'rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            'rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
#End Region
#Region "Go Page"
    Protected Sub btnPageNumb_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                BindGridEntity()
            End If
        End If
    End Sub

#End Region
#Region "Sorting"

    Public Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        BindGridEntity()
    End Sub

#End Region
#Region "BindGridEntity"
    Sub BindGridEntity()
        With oCollLetter
            .WhereCond = Me.SearchBy
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = Me.SortBy
            .strConnection = GetConnectionString
        End With
        oCollLetter = m_controller.CollLetterPaging(oCollLetter)
        recordCount = oCollLetter.TotalRecord
        dtgLetterTemplate.DataSource = oCollLetter.ListData
        dtgLetterTemplate.CurrentPageIndex = 0
        dtgLetterTemplate.DataBind()

        pnlGrid.Visible = True
        pnlAddEdit.Visible = False
        PagingFooter()
    End Sub

#End Region

    Private Sub dtgLetterTemplate_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgLetterTemplate.ItemCommand
        Dim oLetterId As Literal
        Dim oLettername As Literal
        Dim oTemplate As Literal

        oLetterId = CType(e.Item.FindControl("ltlLetterId"), Literal)
        oLettername = CType(e.Item.FindControl("ltlLetterName"), Literal)
        oTemplate = CType(e.Item.FindControl("ltlTemplate"), Literal)


        If e.CommandName = "Edit" Then
            If CheckFeature(Me.Loginid, "CollLetterTemplate", "Edit", "Maxiloan") Then
                If sessioninvalid() Then
                    Exit Sub
                End If
            End If
            pnlAddEdit.Visible = True
            pnlGrid.Visible = False
            pnlTop.Visible = False

            txtLetterId.Visible = False
            ltlLetterIDView.Visible = True
            ltlLetterIDView.Text = oLetterId.Text
            txtLetterName.Text = oLettername.Text
            txtTemplate.Text = oTemplate.Text
            lblTitle.Text = "EDIT"
            SaveAction = ActionType.Edit

        Else
            If e.CommandName = "Delete" Then                

                With oCollLetter
                    .strConnection = GetConnectionString
                    '.BucketId = ltlbucketId.Text
                    .LetterId = oLetterId.Text
                End With
                Try
                    m_controller.CollLetterDelete(oCollLetter)                    
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, False)
                    BindGridEntity()

                Catch ex As Exception                
                    ShowMessage(lblMessage, ex.Message, True)
                End Try


            End If
        End If
    End Sub

    Private Sub dtgLetterTemplate_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgLetterTemplate.ItemDataBound
        Dim oDelete As ImageButton
        With e.Item
            If .ItemIndex >= 0 Then
                oDelete = DirectCast(.FindControl("imgbdelete"), ImageButton)
                oDelete.Attributes.Add("onClick", "javascript:return fConfirm()")
            End If
        End With
    End Sub

    Private Sub btnSaveAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonSaveAdd.Click
        If SaveAction = ActionType.Edit Then
            With oCollLetter
                .strConnection = GetConnectionString()
                .LetterId = ltlLetterIDView.Text
                .LetterName = txtLetterName.Text
                .Template = txtTemplate.Text

                Try
                    m_controller.CollLetterUpdate(oCollLetter)                    
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
                    BindGridEntity()

                Catch ex As Exception                    
                    ShowMessage(lblMessage, ex.Message, True)
                End Try


            End With

        Else
            If SaveAction = ActionType.Add Then
                With oCollLetter
                    .strConnection = GetConnectionString()
                    .LetterId = txtLetterId.Text
                    .LetterName = txtLetterName.Text
                    .Template = txtTemplate.Text

                End With
                Try
                    m_controller.CollLetterSave(oCollLetter)                    
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_SUCCESS, False)
                    BindGridEntity()
                Catch ex As Exception                    
                    ShowMessage(lblMessage, ex.Message, False)
                End Try
            End If


        End If
    End Sub

    Private Sub btnCancelAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonCancelAdd.Click
        pnlAddEdit.Visible = False
        pnlGrid.Visible = True
        pnlTop.Visible = True
        lblTitle.Text = ""
        txtLetterId.Text = ""
        txtLetterName.Text = ""
        txtTemplate.Text = ""        
    End Sub

    Private Sub ImgAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonAddNew.Click
        'hyReceiptNotes.NavigateUrl = "javascript:OpenReceiptNotesView('" & "Collection" & "','" & Server.UrlEncode(Me.ApplicationID.Trim) & "','" & Server.UrlEncode(Me.BranchID1.Trim) & "','" & Server.UrlEncode(Me.CustomerID.Trim) & "','" & Server.UrlEncode(Me.CustomerName.Trim) & "','" & Server.UrlEncode(Me.AgreementNo.Trim) & "')"

        pnlAddEdit.Visible = True
        pnlGrid.Visible = False
        pnlTop.Visible = False


        SaveAction = ActionType.Add
        lblTitle.Text = "TAMBAH"

        txtLetterId.Enabled = True
        ltlLetterIDView.Visible = False
        txtLetterId.Text = ""
        txtLetterName.Text = ""
        txtTemplate.Text = ""        
    End Sub

    Private Sub imbPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonPrint.Click
        If SessionInvalid() Then
            Exit Sub
        End If


        If CheckFeature(Me.Loginid, Me.FormID, "PRINT", Me.AppId) Then            
            Dim cookie As HttpCookie = Request.Cookies(COOKIES_COLL_LETTER_ON_REQ)
            If Not cookie Is Nothing Then
                cookie.Values("Where") = Me.SearchBy
                cookie.Values("Sort") = Me.SortBy
                Response.AppendCookie(cookie)
            Else
                Dim cookieNew As New HttpCookie(COOKIES_COLL_LETTER_ON_REQ)
                cookieNew.Values.Add("Where", Me.SearchBy)
                cookieNew.Values.Add("Sort", Me.SortBy)
                Response.AppendCookie(cookieNew)
            End If
            Response.Redirect("Report/CollLetterViewer.aspx")
        End If


    End Sub

    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Buttonsearch.Click        
        Me.SearchBy = ""
        Me.SortBy = ""

        If oSearchBy.Text.Trim <> "" Then
            Me.SearchBy = " where " & oSearchBy.ValueID & " = '" & oSearchBy.Text & "'"
        End If
        pnlGrid.Visible = True
        pnlTop.Visible = True
        BindGridEntity()
    End Sub

    Private Sub imbReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonReset.Click        
        Me.SearchBy = ""
        oSearchBy.Text = ""
        oSearchBy.BindData()
        BindGridEntity()

    End Sub

End Class