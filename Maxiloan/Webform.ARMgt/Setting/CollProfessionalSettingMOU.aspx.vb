﻿#Region "imports"

Imports Maxiloan.Parameter
'Imports Maxiloan.BusinessProcess
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
Imports AjaxControlToolkit
#End Region



Public Class CollProfessionalSettingMOU
    Inherits Maxiloan.Webform.WebBased
    'Protected WithEvents ucAddress As UcCompanyAddress


#Region "Constanta"

    'Dim m_Controller As New CollectorMOUController
    Dim m_Controller As New CollectorProfController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

#End Region

    Private Property CollectorID() As String
        Get
            Return CStr(ViewState("CollectorID"))
        End Get
        Set(ByVal Value As String)
            ViewState("CollectorID") = Value
        End Set
    End Property

    Private Property NoMOU() As Integer
        Get
            Return CStr(ViewState("NoMOU"))
        End Get
        Set(ByVal Value As Integer)
            ViewState("NoMOU") = Value
        End Set
    End Property


    Private Property TglMOU() As Date
        Get
            Return CStr(ViewState("TglMOU"))
        End Get
        Set(ByVal Value As Date)
            ViewState("TglMOU") = Value
        End Set
    End Property

    Private Property TglExpiredMOU() As Date
        Get
            Return CStr(ViewState("TglExpiredMOU"))
        End Get
        Set(ByVal Value As Date)
            ViewState("TglExpiredMOU") = Value
        End Set
    End Property

    Public Property ActionAddEdit() As String
        Get
            Return CStr(ViewState("ActionAddEdit"))
        End Get
        Set(ByVal Value As String)
            ViewState("ActionAddEdit") = Value
        End Set
    End Property

    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property WhereCond2() As String
        Get
            Return CType(ViewState("vwsWhereCond2"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsWhereCond2") = Value
        End Set
    End Property

#Region "GetCookies"
    Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies("Supplier")
        Me.CmdWhere = cookie.Values("where")
        Me.WhereCond2 = cookie.Values("where2")
        Me.CollectorID = cookie.Values("CollectorID")
        Me.BranchID = cookie.Values("BranchID")
    End Sub
#End Region
#Region "clean"
    Sub clean()
        txtNoMOU.Text = Nothing
        txtTglMOU.Text = Nothing
        txtTglMOUExpired.Text = Nothing
        txtTglMOUExpiredNew.Text = Nothing
        txtKet.Text = Nothing
    End Sub
#End Region


#Region " PanelAllFalse "
    Private Sub PanelAllFalse()
        'pnlSearch.Visible = False
        pnlAddEdit.Visible = False
        pnlList.Visible = False
    End Sub
#End Region

#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridCollectorMOU(Me.SearchBy, Me.SortBy)
        pnlList.Visible = True
    End Sub

    Protected Sub btnPageNumb_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPageNumb.Click

        If txtPage.Text = "" Then
            txtPage.Text = "0"
        Else
            If IsNumeric(txtPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                    currentPage = CType(txtPage.Text, Int32)
                    BindGridCollectorMOU(Me.SearchBy, Me.SortBy)
                End If
            End If
        End If

        pnlList.Visible = True

    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If

        BindGridCollectorMOU(Me.SearchBy, Me.SortBy)
        pnlList.Visible = True
    End Sub

#End Region

    Function LinkTo(ByVal pBranchID As String, ByVal pCollID As String, ByVal pNoMOU As String) As String
        Return "javascript:OpenViewHistory('" & pBranchID & "','" & pCollID & "','" & pNoMOU & "')"

    End Function

#Region " BindGrid"
    Sub BindGridCollectorMOU(ByVal cmdWhere As String, ByVal cmSort As String)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim oCollector As New Parameter.Collector

        With oCollector
            .strConnection = GetConnectionString()
            .BranchId = Me.BranchID.Trim
            .CollectorID = Me.CollectorID.Trim
            '.CGID = Me.CGID
            .WhereCond = cmdWhere
            .WhereCond2 = WhereCond2
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With


        oCollector = m_Controller.GetCollectorMOUList(oCollector)

        With oCollector
            lblrecord.Text = CType(.TotalRecord, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecord
        End With

        dtsEntity = oCollector.ListData
        dtvEntity = dtsEntity.DefaultView
        dtvEntity.Sort = Me.SortBy
        dtgMOU.DataSource = dtvEntity
        Try
            dtgMOU.DataBind()
        Catch
            dtgMOU.CurrentPageIndex = 0
            dtgMOU.DataBind()
        End Try

        PagingFooter()
    End Sub

    Sub InitialPanel()
        pnlList.Visible = True
        pnlAddEdit.Visible = False
        pnlSaveCancel.Visible = False
        txtPage.Text = "1"
    End Sub

#End Region
    Sub InitialDefaultPanel()
        pnlList.Visible = True
        pnlAddEdit.Visible = False
        pnlAddCancel.Visible = True
        pnlSaveCancel.Visible = False
    End Sub

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        'Put user code to initialize the page here
        If SessionInvalid() Then
            Exit Sub
        End If
        'createxmdlist()
        If Not Me.IsPostBack Then
            InitialDefaultPanel()

            Me.FormID = "CollMOU"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.SearchBy = "all"
                Me.SortBy = ""
                If Request("back") = "1" Then
                    GetCookies()
                Else
                    If Request("cond") <> "" Then
                        Me.SearchBy = Request("cond")
                    Else
                        Me.SearchBy = "ALL"
                    End If
                    Me.SortBy = ""
                    ' Me.BranchID = Request("BranchID")
                    Me.BranchID = Request.QueryString("BranchID").Trim
                    Me.CollectorID = Request.QueryString("CollectorID").Trim
                    'Me.CollectorID = Request("CollectorID")
                    Me.WhereCond2 = "CollectorID='" & Me.CollectorID & "'"
                    'Me.WhereCond2 = "BranchID = '" & Me.BranchID & "'&CollectorID='" & Me.CollectorID & "'"
                    BindGridCollectorMOU(Me.SearchBy, Me.SortBy)
                End If
            End If
        End If
    End Sub

    Protected Sub ButtonSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonSave.Click
        Dim oCollector As New Parameter.Collector
        Dim ErrMessage As String = ""

        If txtTglMOU.Text.Trim <> "" And txtTglMOUExpired.Text.Trim <> "" Then
            If (ConvertDate2(txtTglMOU.Text) > ConvertDate2(txtTglMOUExpired.Text)) Then
                ShowMessage(lblMessage, "Tanggal MOU Expired harus lebih besar dari Tanggal MOU ", True)
                pnlList.Visible = False
                pnlAddEdit.Visible = True
                Exit Sub
            End If
        Else
            ShowMessage(lblMessage, "Please fill in Period!", True)
            pnlList.Visible = False
            pnlAddEdit.Visible = True
            Exit Sub
        End If

        If Me.ActionAddEdit <> "EXTEND" Then
            If txtTglMOUExpired.Text.Trim <> "" Then
                If ConvertDate2(txtTglMOUExpired.Text) <= Me.BusinessDate And Me.ActionAddEdit <> "EXTEND" Then
                    ShowMessage(lblMessage, "Tanggal MOU Expired tidak boleh lebih kecil atau sama dengan business date!", True)
                    Exit Sub
                End If
            Else
                ShowMessage(lblMessage, "Please fill in Period!", True)
                pnlList.Visible = False
                pnlAddEdit.Visible = True
                Exit Sub
            End If
        End If

        If Me.ActionAddEdit = "EXTEND" Then
            If txtTglMOUExpiredNew.Text.Trim <> "" Then
                If ConvertDate2(txtTglMOUExpiredNew.Text) <= Me.BusinessDate And Me.ActionAddEdit = "EXTEND" Then
                    ShowMessage(lblMessage, "Tanggal MOU Expired Baru tidak boleh lebih kecil atau sama dengan business date!", True)
                    Exit Sub
                End If
            Else
                ShowMessage(lblMessage, "Please fill in Period!", True)
                pnlList.Visible = False
                pnlAddEdit.Visible = True
                Exit Sub
            End If
        End If



        'If txtTglMOU.Text.Trim <> "" Or txtTglMOUExpired.Text.Trim <> "" Or txtTglMOUExpiredNew.Text <> "" Then
        '    If (ConvertDate2(txtTglMOU.Text) > ConvertDate2(txtTglMOUExpired.Text)) Then
        '        ShowMessage(lblMessage, "Tanggal MOU Expired harus lebih besar dari Tanggal MOU ", True)
        '        pnlList.Visible = False
        '        pnlAddEdit.Visible = True
        '        Exit Sub
        '    ElseIf ConvertDate2(txtTglMOUExpired.Text) <= Me.BusinessDate And Me.ActionAddEdit <> "EXTEND" Then
        '        ShowMessage(lblMessage, "Tanggal MOU Expired tidak boleh lebih kecil atau sama dengan business date!", True)
        '        Exit Sub
        '    ElseIf ConvertDate2(txtTglMOUExpiredNew.Text) <= Me.BusinessDate And Me.ActionAddEdit = "EXTEND" Then
        '        ShowMessage(lblMessage, "Tanggal MOU Expired Baru tidak boleh lebih kecil atau sama dengan business date!", True)
        '        Exit Sub
        '    End If

        'Else
        '    ShowMessage(lblMessage, "Please fill in Period From!", True)
        '    pnlList.Visible = False
        '    pnlAddEdit.Visible = True
        '    Exit Sub
        'End If

        oCollector.ActionAddEdit = Me.ActionAddEdit
        oCollector.strConnection = GetConnectionString()
        oCollector.BranchId = Me.BranchID
        oCollector.CollectorID = Me.CollectorID.Trim
        oCollector.NoMOU = txtNoMOU.Text
        oCollector.TglMOU = ConvertDate2(txtTglMOU.Text.Trim)
        oCollector.TglExpiredMOU = ConvertDate2(txtTglMOUExpired.Text.Trim)
        'oCollector.TglMOU.ToString("dd/MM/yyyy") = txtTglMOU.Text.Trim
        'oCollector.TglExpiredMOU.ToString("dd/MM/yyyy") = txtTglMOUExpired.Text.Trim
        'Date.ParseExact(tglMulaiVendor.Text, "dd/MM/yyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo)


        If Me.ActionAddEdit = "Add" Then
            oCollector.BranchId = Me.BranchID.Trim
            oCollector.CollectorID = Me.CollectorID.Trim
            m_Controller.CollectorMOUAdd(oCollector)
            ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
            lblMessage.Visible = True
            InitialDefaultPanel()
            BindGridCollectorMOU(Me.SearchBy, Me.SortBy)

        ElseIf Me.ActionAddEdit = "EDIT" Then
            oCollector.BranchId = Me.BranchID.Trim
            oCollector.CollectorID = Me.CollectorID.Trim
            oCollector.NoMOU = oCollector.NoMOU
            'oCollector.TglMOU = Me.TglMOU.ToString("dd/MM/yyyy")

            oCollector.TglMOU = ConvertDate2(txtTglMOU.Text)
            oCollector.TglExpiredMOU = ConvertDate2(txtTglMOUExpired.Text)

            m_Controller.CollectorMOUEdit(oCollector)
            ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
            lblMessage.Visible = True
            InitialDefaultPanel()
            BindGridCollectorMOU(Me.SearchBy, Me.SortBy)

        ElseIf Me.ActionAddEdit = "EXTEND" Then
            oCollector.BranchId = Me.BranchID.Trim
            oCollector.CollectorID = Me.CollectorID.Trim
            oCollector.NoMOU = oCollector.NoMOU
            oCollector.TglMOU = ConvertDate2(txtTglMOU.Text)
            oCollector.TglExpiredMOU = ConvertDate2(txtTglMOUExpired.Text)
            oCollector.TglExpiredMOUNew = ConvertDate2(txtTglMOUExpiredNew.Text)
            oCollector.ket = txtKet.Text.Trim

            m_Controller.CollectorMOUExtendUpdate(oCollector)
            ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
            lblMessage.Visible = True
            InitialDefaultPanel()
            BindGridCollectorMOU(Me.SearchBy, Me.SortBy)
        End If

    End Sub

    Private Sub dtgMOU_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgMOU.ItemCommand

        Select Case e.CommandName
            Case "Edit"
                Me.ActionAddEdit = "EDIT"
                If CheckFeature(Me.Loginid, Me.FormID, "Edit", Me.AppId) Then
                    If SessionInvalid() Then
                        Exit Sub
                    End If
                End If

                Dim oCollector As New Parameter.Collector

                'txtTglMOU.Text = oCollector.TglMOU.ToString("dd/MM/yyyy")
                'txtTglMOUExpired.Text = oCollector.TglExpiredMOU.ToString("dd/MM/yyyy")

                With oCollector
                    .strConnection = GetConnectionString()
                    .BranchId = Me.BranchID.Trim
                    .CollectorID = Me.CollectorID.Trim
                    .NoMOU = e.Item.Cells(4).Text.Trim
                    .TglMOU = ConvertDate2(e.Item.Cells(5).Text.Trim)
                    .TglExpiredMOU = ConvertDate2(e.Item.Cells(6).Text.Trim)
                    Me.ActionAddEdit = "EDIT"
                End With

                'PanelAllFalse()
                'viewbyID(oCollector)
                txtNoMOU.Enabled = False
                txtTglMOU.Enabled = True
                txtTglMOUExpired.Enabled = True
                pnlAddEdit.Visible = True
                pnlSaveCancel.Visible = True
                pnlList.Visible = False
                pnlAddCancel.Visible = False
                pnlExtend.Visible = False

                lblTitle.Text = Me.ActionAddEdit
                txtNoMOU.Text = oCollector.NoMOU.Trim
                txtTglMOU.Text = oCollector.TglMOU.ToString("dd/MM/yyyy")
                txtTglMOUExpired.Text = oCollector.TglExpiredMOU.ToString("dd/MM/yyyy")


            Case "Delete"
                Me.ActionAddEdit = "Delete"
                If CheckFeature(Me.Loginid, Me.FormID, "Del", Me.AppId) Then
                    If SessionInvalid() Then
                        Exit Sub
                    End If
                End If

                Dim oCollector As New Parameter.Collector
                With oCollector
                    .strConnection = GetConnectionString()
                    .BranchId = Me.BranchID
                    .NoMOU = e.Item.Cells(4).Text.Trim
                    .CollectorID = Me.CollectorID.Trim
                End With

                Try
                    m_Controller.CollectorMOUDelete(oCollector)
                    ' ShowMessage(lblMessage, "Hapus Data Berhasil ", True)
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, False)
                Catch ex As Exception
                    ShowMessage(lblMessage, ex.Message, True)

                Finally
                    pnlAddEdit.Visible = False
                    lblMessage.Visible = True
                    InitialDefaultPanel()
                    BindGridCollectorMOU(Me.SearchBy, Me.SortBy)
                    pnlList.Visible = True
                End Try

            Case "EXTEND"
                Me.ActionAddEdit = "EXTEND"
                If CheckFeature(Me.Loginid, Me.FormID, "Edit", Me.AppId) Then
                    If SessionInvalid() Then
                        Exit Sub
                    End If
                End If

                Dim oCollector As New Parameter.Collector
                'txtTglMOU.Text = oCollector.TglMOU.ToString("dd/MM/yyyy")
                'txtTglMOUExpired.Text = oCollector.TglExpiredMOU.ToString("dd/MM/yyyy")

                With oCollector
                    .strConnection = GetConnectionString()
                    .BranchId = Me.BranchID.Trim
                    .CollectorID = Me.CollectorID.Trim
                    .NoMOU = e.Item.Cells(4).Text.Trim
                    .TglMOU = ConvertDate2(e.Item.Cells(5).Text.Trim)
                    .TglExpiredMOU = ConvertDate2(e.Item.Cells(6).Text.Trim)
                    Me.ActionAddEdit = "EXTEND"
                End With

                'PanelAllFalse()
                'viewbyID(oCollector)
                txtNoMOU.Enabled = False
                txtTglMOU.Enabled = False
                txtTglMOUExpired.Enabled = False
                txtTglMOUExpiredNew.Visible = True
                pnlAddEdit.Visible = True
                pnlSaveCancel.Visible = True
                pnlList.Visible = False
                pnlAddCancel.Visible = False
                pnlExtend.Visible = True
                txtTglMOUExpiredNew.Text = Nothing
                txtKet.Text = Nothing

                lblTitle.Text = Me.ActionAddEdit
                'txtNoMOU.Text = e.Item.Cells(3).Text.Trim
                txtNoMOU.Text = oCollector.NoMOU.Trim
                txtTglMOU.Text = oCollector.TglMOU.ToString("dd/MM/yyyy")
                txtTglMOUExpired.Text = oCollector.TglExpiredMOU.ToString("dd/MM/yyyy")
                txtKet.Text = txtKet.Text.Trim
        End Select
    End Sub

    Protected Sub imbButtonAdd_Click(ByVal sender As Object, ByVal e As EventArgs) Handles imbButtonAdd.Click
        pnlAddEdit.Visible = True
        Me.ActionAddEdit = "Add"
        lblTitle.Text = "ADD"
        pnlAddEdit.Visible = True
        pnlAddCancel.Visible = False
        pnlSaveCancel.Visible = True
        pnlList.Visible = False
        pnlExtend.Visible = False

        txtNoMOU.Enabled = True
        txtTglMOU.Enabled = True
        txtTglMOUExpired.Enabled = True
        clean()

    End Sub



    Protected Sub imbButtonCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles imbButtonCancel.Click
        Response.Redirect("CollProfessionalSetting.aspx")
    End Sub

    Protected Sub ButtonCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonCancel.Click
        InitialDefaultPanel()
    End Sub

    Private Sub dtgMOU_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgMOU.ItemDataBound
        Dim imbDelete As ImageButton
        Dim lnkEXTEND As LinkButton

        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imbDelete"), ImageButton)
            imbDelete.Attributes.Add("Onclick", "return DeleteConfirm()")

            'untuk hidden linkbutton EXTEND 
            lnkEXTEND = CType(e.Item.FindControl("lnkEXTEND"), LinkButton)
            If (ConvertDate2(e.Item.Cells(6).Text.Trim) <= Me.BusinessDate) Then
                lnkEXTEND.Visible = True
            Else
                lnkEXTEND.Visible = False
            End If
        End If

        'lnkEXTEND = lnkEXTEND
    End Sub
End Class