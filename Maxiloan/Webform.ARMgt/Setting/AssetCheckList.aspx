﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AssetCheckList.aspx.vb"
    Inherits="Maxiloan.Webform.ARMgt.AssetCheckList" %>

<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../../Webform.UserController/UcSearchBy.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>AssetCheckList</title>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <!--
		<script language="javascript">
			function fback() {
					history.back(-1);
					return false;
			}	
		</script>		
		-->
</head>
<body>
    <form id="form1" runat="server">
        <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
      <asp:Panel ID="pnlDtGrid" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">           
                <h3>
                    DAFTAR ASSET CHECK LIST
                </h3>
            </div>
        </div>  
        
        <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgAssetCheckList" runat="server" Width="100%"
                        CssClass="grid_general" BorderStyle="None" BorderWidth="0"
                        DataKeyField="AssetTypeID" AutoGenerateColumns="False"
                        AllowSorting="True">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="EDIT">
                                <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbEdit" runat="server" ImageUrl="../../Images/IconEdit.gif"
                                        CausesValidation="False" CommandName="EDIT"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DELETE">
                                <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbDelete" runat="server" ImageUrl="../../Images/IconDelete.gif"
                                        CausesValidation="False" CommandName="DELETE"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AssetTypeID" HeaderText="JENIS ASSET">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblAssetTypeIDdtg" Text='<%#Container.dataItem("AssetTypeID")%>' runat="server">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="CheckListID" HeaderText="ID CHECK LIST">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblCheckListIDdtg" runat="server" Text='<%#Container.DataItem("CheckListID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Description" HeaderText="KETERANGAN">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblDescription" runat="server" Text='<%#Container.DataItem("Description")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="IsYNQuestion" HeaderText="YESNO / QTY">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblIsYNQuestion" runat="server" Text='<%#Container.DataItem("IsYNQuestion")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="IsActive" HeaderText="AKTIF">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblIsActive" runat="server" Text='<%#Container.DataItem("IsActive")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>                        
                </asp:DataGrid>       
                <div class="button_gridnavigation">
	                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
		                CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
		                CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
		                CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
		                CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
	                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
		                EnableViewState="False"></asp:Button>
	                <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage"
				        MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer"
				        CssClass="validator_general"></asp:RangeValidator>
			        <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
				        ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general">
			        </asp:RequiredFieldValidator>              
                </div>
                <div class="label_gridnavigation">
	                <asp:Label ID="lblPage" runat="server"></asp:Label>of
	                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
	                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                </div>
            </div>        
        </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonAddNew" runat="server"  Text="Add" CssClass ="small button blue">
            </asp:Button>
             <asp:Button ID="ButtonPrint" runat="server"  Text="Print" CssClass ="small button blue">
             </asp:Button>
	    </div>        
        <asp:Panel ID="pnlList" runat="server">
        <div class="form_box_title">
            <div class="form_single">              
                <h4>
                    DAFTAR CHECK LIST
                </h4>
            </div>
        </div>
        <div class="form_box_uc">
                <uc1:ucsearchby id="oSearchBy" runat="server"></uc1:ucsearchby>	        
        </div>           
        <div class="form_button">
            <asp:Button ID="Buttonsearch" runat="server"  Text="Search" CssClass ="small button blue"></asp:Button>&nbsp;
            <asp:Button ID="ButtonReset" runat="server"  Text="Reset" CssClass ="small button gray"
            CausesValidation="False"></asp:Button>
	    </div>
        </asp:Panel>
        </asp:Panel>
        <asp:Panel ID="pnlAdd" runat="server">
        <div class="form_box_title">
            <div class="form_single">              
                <h4>
                    <asp:Label ID="lblTitle" runat="server"></asp:Label>
                </h4>   
            </div>
        </div>
       <div class="form_box">
	        <div class="form_single">
                <label class ="label_req">
				Jenis Asset</label>
                <asp:DropDownList ID="cboAssetType" runat="server">
                </asp:DropDownList>
                <asp:Label ID="lblAssetTypeID" runat="server"></asp:Label>
                <asp:RequiredFieldValidator ID="rfvAssetType" runat="server"    CssClass="validator_general"
                    ControlToValidate="cboAssetType" ErrorMessage="Harap pilih Jenis Asset" Display="Dynamic"></asp:RequiredFieldValidator>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
               <label class ="label_req">
				ID Check List</label>
                <asp:TextBox ID="txtCheckListID" runat="server"  MaxLength="10"></asp:TextBox>
                <asp:Label ID="lblCheckListID" runat="server"></asp:Label>
                <asp:RequiredFieldValidator ID="rfvCheckListID" runat="server"    CssClass="validator_general"
                ControlToValidate="txtCheckListID" ErrorMessage="Harap pilih ID Check List" Display="Dynamic"></asp:RequiredFieldValidator>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
               <label class ="label_req">
			   Keterangan</label>
                <asp:TextBox ID="txtDescription" runat="server" Width="475px" 
                    MaxLength="100"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvDescription" runat="server"    CssClass="validator_general"
                    ControlToValidate="txtDescription" ErrorMessage="Harap isi Keterangan" Display="Dynamic"></asp:RequiredFieldValidator>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
               <!-- <label class="label_general">-->
				<label class ="label_req label_general">
				Jenis Pertanyaan</label>
                <asp:RadioButtonList ID="rdoQuestioType" runat="server" class="opt_single"  RepeatDirection="Horizontal">
                    <asp:ListItem Value="1">Pertanyaan Y/N</asp:ListItem>
                    <asp:ListItem Value="0">Pertanyaan QTY</asp:ListItem>
                </asp:RadioButtonList>
                <asp:RequiredFieldValidator ID="REQUIREDFIELDVALIDATOR1" runat="server"  CssClass="validator_general"
                      ControlToValidate="rdoQuestioType" ErrorMessage="Harap Pilih Jenis Pertanyaan"
                    Display="Dynamic"></asp:RequiredFieldValidator>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Aktif</label>
                <asp:CheckBox ID="chkIsActive" runat="server"></asp:CheckBox>
	        </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonSaveAdd" runat="server"  Text="Save" CssClass ="small button blue"
                CausesValidation="True"></asp:Button>&nbsp;
            <asp:Button ID="ButtonCancelAdd" runat="server"  Text="Cancel" CssClass ="small button gray"
                CausesValidation="False"></asp:Button><a href="javascript:history.back();"></a>
	    </div>             
    </asp:Panel>
    </form>
</body>
</html>
