﻿#Region "imports"
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class GantiCollector
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents UCEmployee As UcFindEmployee
    Protected WithEvents UCBranchCollection1 As UcBranchCollection
    Protected WithEvents UcCG As UcBranchCollection

    Dim dtCollector As New DataTable


#Region " Private Const "
    Dim m_CollZipCode As New CollZipCodeController
    Dim m_Coll As New CollectorController

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

#End Region
#Region " Property "

    Private Property DefaultCollector() As String
        Get
            Return CStr(ViewState("DefaultCollector"))
        End Get
        Set(ByVal Value As String)
            ViewState("DefaultCollector") = Value
        End Set
    End Property
    Private Property CGID() As String
        Get
            Return CStr(ViewState("CGID"))
        End Get
        Set(ByVal Value As String)
            ViewState("CGID") = Value
        End Set
    End Property
    Private Property City() As String
        Get
            Return CStr(ViewState("City"))
        End Get
        Set(ByVal Value As String)
            ViewState("City") = Value
        End Set
    End Property
    Private Property CGName() As String
        Get
            Return CStr(ViewState("CGName"))
        End Get
        Set(ByVal Value As String)
            ViewState("CGName") = Value
        End Set
    End Property
    Private Property ZipCode() As String
        Get
            Return CStr(ViewState("ZipCode"))
        End Get
        Set(ByVal Value As String)
            ViewState("ZipCode") = Value
        End Set
    End Property
    Private Property TempDataTableA() As DataTable
        Get
            Return CType(ViewState("TempDataTableA"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            ViewState("TempDataTableA") = Value
        End Set
    End Property
    Private Property TempDataTableB() As DataTable
        Get
            Return CType(ViewState("TempDataTableB"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            ViewState("TempDataTableB") = Value
        End Set
    End Property
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not Page.IsPostBack Then
            Me.CGID = Me.GroubDbID
            Me.FormID = "GANTICOLL"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                PnlSearch.Visible = True
                PnlA.Visible = False
                pnlGridA.Visible = False
                pnlB.Visible = False                
                getCollectorCombo()
            End If
        End If
    End Sub
#Region " BindGrid"
    Sub BindGridA(ByVal SearchBy As String, ByVal SortBy As String)
        Dim cPaging As New GeneralPagingController
        Dim oPaging As New Parameter.GeneralPaging
        Dim dtEntity As System.Data.DataTable = Nothing

        With oPaging
            .strConnection = GetConnectionString()
            .WhereCond = SearchBy
            .CurrentPage = 1
            .PageSize = 1000
            .SortBy = SortBy
            .SpName = "spRollingWilayahPaging"
        End With

        oPaging = cPaging.GetGeneralPaging(oPaging)

        dtEntity = oPaging.ListData
        dtgA.DataSource = dtEntity.DefaultView
        dtgA.CurrentPageIndex = 0
        dtgA.DataBind()
    End Sub    
#End Region
    Private Sub BtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSearch.Click

        If CheckFeature(Me.Loginid, Me.FormID, "Srch", Me.AppId) Then
            If SessionInvalid() Then
                Exit Sub
            End If
        End If

        With UcCG
            Me.CGID = .BranchID
            Me.CGName = .BranchName
        End With

        If UcCG.BranchID <> "0" Then
            lblCGID.Text = Me.CGID
            lblCGName.Text = Me.BranchName
        End If

        PnlSearch.Visible = False
        PnlA.Visible = True
        pnlGridA.Visible = False
        pnlB.Visible = False        
    End Sub
    Private Sub BtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSearch.Click

    End Sub
    Private Sub ButtonSearchDetailA_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSearchDetailA.Click
        Dim SearchBy As String = "  Collector.CGID = '" + lblCGID.Text.Trim + "'"
        Dim SortBy As String = " Collector.CollectorID ASC"

        PnlSearch.Visible = False
        PnlA.Visible = True
        pnlGridA.Visible = True
        pnlB.Visible = True        


        If txtSearchByValueA.Text.Trim <> "" Then
            Dim tmptxtSearchBy As String = txtSearchByValueA.Text.Replace("%", "")
            SearchBy += " and " + cboSearchByA.SelectedItem.Value + " like '%" + tmptxtSearchBy + "%'"
        End If


        If cboCollA.SelectedValue.Trim <> "" Then
            SearchBy += " and CollectorAllocation.CollectorID = '" + cboCollA.SelectedValue.Trim + "'"
        End If


        BindGridA(SearchBy, SortBy)
    End Sub    
    Private Sub ButtonResetDetailA_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonResetDetailA.Click
        PnlSearch.Visible = False
        PnlA.Visible = True
        pnlGridA.Visible = False
        pnlB.Visible = False        
        cboCollA.SelectedValue = ""
    End Sub    
    Private Sub getCollectorCombo()
        Dim oColl As New Parameter.CollZipCode
        Dim strconn As String = GetConnectionString()
        Dim dt As New DataTable

        With oColl
            .strConnection = GetConnectionString()
            .CGID = Me.CGID
            .CollectorType = "CL"
        End With

        dt = m_CollZipCode.GetCollectorCombo(oColl)
        cboCollA.DataSource = dt
        cboCollA.DataTextField = "CollectorName"
        cboCollA.DataValueField = "CollectorID"
        cboCollA.DataBind()

        cboCollA.Items.Insert(0, "Select One")
        cboCollA.Items(0).Value = ""

        cboCollB.DataSource = dt
        cboCollB.DataTextField = "CollectorName"
        cboCollB.DataValueField = "CollectorID"
        cboCollB.DataBind()

        cboCollB.Items.Insert(0, "Select One")
        cboCollB.Items(0).Value = ""
    End Sub
    Private Sub imbSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
        Dim oColl As New Parameter.Collector

        Try
            PopulateData()

            If cboCollA.SelectedValue.Trim = cboCollB.SelectedValue.Trim Then
                ShowMessage(lblMessage, "Pilihan yang dirolling dan merolling tidak boleh sama.", True)
                PnlSearch.Visible = False
                PnlA.Visible = True
                pnlGridA.Visible = True
                pnlB.Visible = True                
                cboCollB.SelectedValue = ""
                Exit Sub
            End If

            If TempDataTableA.Rows.Count > 0 Then
                oColl = New Parameter.Collector
                oColl.strConnection = GetConnectionString()
                oColl.CollectorA = TempDataTableA
                oColl.CollectorIDB = cboCollB.SelectedValue.Trim
                m_Coll.GantiCollectorSave(oColl)
            Else
                ShowMessage(lblMessage, "Data harus dipilih", True)
                Exit Sub
            End If

            ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub
    Sub PopulateData()

        TempDataTableA = New DataTable
        With TempDataTableA
            .Columns.Add(New DataColumn("Kelurahan", GetType(String)))
            .Columns.Add(New DataColumn("ZipCode", GetType(String)))
            .Columns.Add(New DataColumn("CGID", GetType(String)))
            .Columns.Add(New DataColumn("CollectorID", GetType(String)))
        End With

        For index = 0 To dtgA.Items.Count - 1
            Dim chek As New CheckBox
            chek = CType(dtgA.Items(index).FindControl("chk"), CheckBox)

            If chek.Checked Then
                Dim oRow As DataRow
                oRow = TempDataTableA.NewRow()
                oRow("Kelurahan") = dtgA.Items(index).Cells(1).Text.Trim
                oRow("ZipCode") = dtgA.Items(index).Cells(2).Text.Trim
                oRow("CGID") = dtgA.Items(index).Cells(3).Text.Trim
                oRow("CollectorID") = dtgA.Items(index).Cells(4).Text.Trim
                TempDataTableA.Rows.Add(oRow)
            End If

        Next

    End Sub
    Public Sub Changed_chkAll_dtgA(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim chkSender As CheckBox = CType(sender, CheckBox)

        For index = 0 To dtgA.Items.Count - 1
            Dim chek As New CheckBox
            chek = CType(dtgA.Items(index).FindControl("chk"), CheckBox)
            chek.Checked = chkSender.Checked
        Next

    End Sub    
End Class