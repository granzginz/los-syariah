﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CollectionGroupSetting.aspx.vb"
    Inherits="Maxiloan.Webform.ARMgt.CollectionGroupSetting" %>

<%@ Register TagPrefix="uc1" TagName="UcContactPerson" Src="../../Webform.UserController/UcContactPerson.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucCompanyAddress" Src="../../Webform.UserController/ucCompanyAddress.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcFindEmpployee2" Src="../../Webform.UserController/UcFindEmpployee2.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Collection Group Setting</title>
    <script src="../../Maxiloan.js" type="text/javascript" language="javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;

        function fConfirm() {
            if (window.confirm("Apakah yakin mau hapus data ini ? "))
                return true;
            else
                return false;
        }
        function fback() {
            history.go(-1);
            return false;
        }

        //        function OpenViewCollGroup(pCGID) {
        //            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        //            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
        //            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.ARMgt/Setting/CollectionGroupView.aspx?CGID=' + pCGID, 'InsuranceCo', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        //        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
        onclick="hideMessage();"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                DAFTAR COLLECTION GROUP
            </h3>
        </div>
    </div>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgCollGroup" runat="server" Width="100%" OnSortCommand="Sorting"
                        AutoGenerateColumns="False" AllowSorting="True" CssClass="grid_general" BorderStyle="None"
                        BorderWidth="0">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="EDIT">
                                <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImbEdit" runat="server" ImageUrl="../../Images/iconedit.gif"
                                        CommandName="Edit"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DELETE">
                                <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImbDelete" runat="server" ImageUrl="../../Images/icondelete.gif"
                                        CommandName="Delete" CausesValidation="False"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="CGID" HeaderText="ID">
                                <ItemTemplate>
                                    <asp:HyperLink runat="server" Enabled="True" ID="hpCGID" Text='<%# container.dataitem("CGID") %>'
                                        NavigateUrl='<%# LinkTo(container.dataItem("CGID")) %>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn Visible="False" DataField="CGID" SortExpression="CGID" HeaderText="COLL GROUP ID">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="CGName" SortExpression="CGName" HeaderText="NAMA COLLECTION GROUP">
                            </asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="BranchID"></asp:BoundColumn>
                            <asp:BoundColumn DataField="FullAddress" SortExpression="Address" HeaderText="ALAMAT">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="FullPhone1" SortExpression="PhoneNo1" HeaderText="NO TELP">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="CGHeadName" SortExpression="CGHead" HeaderText="KEPALA COLL GROUP">
                            </asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="cghead"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <div class="button_gridnavigation">
                        <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                            CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                            CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                            CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                            CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                        <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                            EnableViewState="False"></asp:Button>
                        <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" MinimumValue="1"
                            ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                            ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general">
                        </asp:RequiredFieldValidator>
                    </div>
                    <div class="label_gridnavigation">
                        <asp:Label ID="lblPage" runat="server"></asp:Label>of
                        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                        <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                    </div>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonAdd" runat="server" Text="Add" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="ButtonPrint" runat="server" Text="Print" CssClass="small button blue">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlSearch" runat="server">
        <div class="form_box_title">
            <div class="form_single">
                <h4>
                    COLLECTION GROUP
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cari Berdasarkan</label>
                <asp:DropDownList ID="cboSearchBy" runat="server" Width="144px">
                    <asp:ListItem Value="CGID" Selected="True">ID CG</asp:ListItem>
                    <asp:ListItem Value="CGName">Nama CG</asp:ListItem>
                </asp:DropDownList>
                &nbsp;
                <asp:TextBox ID="TxtSearchByValue" runat="server" Width="168px"></asp:TextBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonSearch" runat="server" Text="Search" CssClass="small button blue">
            </asp:Button>&nbsp;
            <asp:Button ID="ButtonReset" runat="server" Text="Reset" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlAddEdit" runat="server">
        <div class="form_box_title">
            <div class="form_single">
                <h4>
                    <asp:Label ID="lblMenuAddEdit" runat="server"></asp:Label>
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    ID Collection Group
                </label>
                <asp:TextBox ID="txtCGID" runat="server" Width="50px" MaxLength="3"></asp:TextBox>
                <asp:Label ID="lblBintang" runat="server" Width="8px" Height="16px" ForeColor="Red"></asp:Label>
                <asp:Label ID="lblCGID" runat="server" ForeColor="Black"></asp:Label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" CssClass="validator_general"
                    ErrorMessage="Harap isi ID Collection Group" ControlToValidate="txtCGID"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    Nama Collection Group
                </label>
                <asp:TextBox ID="txtCGName" runat="server" Width="291px" MaxLength="20"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfv1" runat="server" Width="184px" ErrorMessage="Harap isi Nama Collection Group"
                    CssClass="validator_general" ControlToValidate="txtCGName"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box_uc">
            <uc1:ucfindempployee2 id="UcFindEmployee1" runat="server"></uc1:ucfindempployee2>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <h4>
                    ALAMAT
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Copy Alamat Dari Cabang</label>
                <asp:DropDownList ID="cboBranch" runat="server">
                </asp:DropDownList>
                <%--</div>
            <div class="form_right">--%>
                <asp:Button ID="ButtonCopyAddress" runat="server" Text="Copy" CssClass="small buttongo blue"
                    CausesValidation="False"></asp:Button>
            </div>
        </div>
        <div class="form_box_uc">
            <uc1:uccompanyaddress id="UcCompanyAdress1" runat="server"></uc1:uccompanyaddress>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <h4>
                    KONTAK
                </h4>
            </div>
        </div>
        <div class="form_box_uc">
            <uc1:uccontactperson id="UcContactPerson1" runat="server" />
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonSave" runat="server" Text="Save" CssClass="small button blue">
            </asp:Button>&nbsp;
            <asp:Button ID="ButtonCancel" runat="server" Text="Cancel" CssClass="small button gray"
                CausesValidation="False"></asp:Button>&nbsp; <a href="javascript:history.back();">
            </a>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
