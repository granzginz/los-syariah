﻿#Region "imports"
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class CollResult
    Inherits Maxiloan.Webform.WebBased

#Region " Private Const "
    Dim m_ActionResult As New ActionResultController

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

#End Region

#Region " Property "
    Private Property ActionID() As String
        Get
            Return CStr(viewstate("ActionID"))
        End Get
        Set(ByVal Value As String)
            viewstate("ActionID") = Value
        End Set
    End Property

    Private Property ActionName() As String
        Get
            Return CStr(viewstate("ActionName"))
        End Get
        Set(ByVal Value As String)
            viewstate("ActionName") = Value
        End Set
    End Property

    Private Property ResultID() As String
        Get
            Return CStr(viewstate("ResultID"))
        End Get
        Set(ByVal Value As String)
            viewstate("ResultID") = Value
        End Set
    End Property

    Private Property ResultName() As String
        Get
            Return CStr(viewstate("ResultName"))
        End Get
        Set(ByVal Value As String)
            viewstate("ResultName") = Value
        End Set
    End Property

    Private Property ResultCategory() As String
        Get
            Return CStr(viewstate("ResultCategory"))
        End Get
        Set(ByVal Value As String)
            viewstate("ResultCategory") = Value
        End Set
    End Property

    Public Property ActionAddEdit() As String
        Get
            Return CStr(ViewState("ActionAddEdit"))
        End Get
        Set(ByVal Value As String)
            viewstate("ActionAddEdit") = Value
        End Set
    End Property

#End Region

    Private Sub getResultCategory()
        Dim oResultCat As New Parameter.ActionResult
        Dim strconn As String = getConnectionString()
        Dim dt As New DataTable

        oResultCat = m_ActionResult.GetResultCategory(strconn)
        dt = oResultCat.ListData
        cboResultCategory.DataSource = dt
        cboResultCategory.DataValueField = "ID"
        cboResultCategory.DataTextField = "description"
        cboResultCategory.DataBind()

    End Sub

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        'Put user code to initialize the page here
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not Me.IsPostBack Then
            Me.FormID = "CollResult"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                InitialDefaultPanel()
                Me.ActionID = Request.QueryString("ActionID").Trim
                Me.ActionName = Request.QueryString("ActionName").Trim
                lblActionName.Text = Me.ActionName.Trim
                Me.SearchBy = ""
                Me.SortBy = ""
                If Request("cond") <> "" Then
                    Me.SearchBy = Request("cond")
                Else
                    Me.SearchBy = "ALL"
                End If
                BindGridResult(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub

#Region " PanelAllFalse "
    Private Sub PanelAllFalse()        
        pnlAddEdit.Visible = False
        pnlList.Visible = False
    End Sub
#End Region

#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
            Else
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
            Else
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridResult(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click

        If txtPage.Text = "" Then
            txtPage.Text = "0"
        Else
            If IsNumeric(txtPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                    currentPage = CType(txtPage.Text, Int32)
                    BindGridResult(Me.SearchBy, Me.SortBy)
                End If
            End If
        End If


    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        BindGridResult(Me.SearchBy, Me.SortBy)
    End Sub

#End Region

#Region " InitialDefaultPanel "
    Private Sub InitialDefaultPanel()
        pnlList.Visible = True
        pnlAddEdit.Visible = False
    End Sub
#End Region

    Sub createxmdlist()
        Dim dtEntity As New DataTable
        Dim dsList As New DataSet
        Dim oResult As Parameter.ActionResult

        oResult = m_ActionResult.GetActionList(oResult)
        dtEntity = oResult.ListData

        dsList.Tables.Add(dtEntity.Clone)
        dsList.DataSetName = "Action"
        dsList.WriteXmlSchema("c:\reportxmd\Result.xmd")
    End Sub

#Region " BindGrid"
    Sub BindGridResult(ByVal cmdWhere As String, ByVal cmSort As String)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim oResult As New Parameter.ActionResult

        With oResult
            .strConnection = GetConnectionString()
            .actionid = Me.ActionID
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With


        oResult = m_ActionResult.GetResultList(oResult)

        With oResult
            lblrecord.Text = CType(.TotalRecord, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecord
        End With

        dtsEntity = oResult.ListData
        dtvEntity = dtsEntity.DefaultView
        dtvEntity.Sort = Me.SortBy
        dtgResult.DataSource = dtvEntity
        Try
            dtgResult.DataBind()
        Catch
            dtgResult.CurrentPageIndex = 0
            dtgResult.DataBind()
        End Try

        PagingFooter()
    End Sub
#End Region

    Private Sub BtnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonAdd.Click
        If CheckFeature(Me.Loginid, Me.FormID, "Add", Me.AppId) Then
            If SessionInvalid() Then
                Exit Sub
            End If
        End If

        PanelAllFalse()

        Me.ActionAddEdit = "TAMBAH"
        lblActionIDAddEdit.Text = Me.ActionID
        txtResultID.Text = ""
        lblResultID.Visible = False
        txtResultID.Text = ""
        txtResultID.Visible = True
        txtResultName.Text = ""
        lblMenuAddEdit.Text = "TAMBAH"
        getResultCategory()        
        pnlAddEdit.Visible = True

    End Sub

    'Sub printlist()
    '    Dim rptInsCo As New ReportDocument
    '    Dim numCopies As Int16 = 1
    '    Dim Collate As Boolean = False
    '    Dim startpage As Int16 = 1

    '    Dim dtEntity As New DataTable
    '    Dim dsList As New DataSet
    '    Dim oInsCo As Parameter.InsCoSelectionList

    '    oInsCo = m_InsCo.GetInsCompanyListDataSet(oInsCo)
    '    dsList = oInsCo.ListDataSet

    '    Dim objReport As New rptInsCo
    '    objReport.SetDataSource(dsList)

    '    'CrystalReportViewer1.ReportSource = objReport
    '    'CrystalReportViewer1.Visible = True
    '    'CrystalReportViewer1.DataBind()
    'End Sub

    Private Sub viewbyID(ByVal Result As Parameter.ActionResult)

        Dim ds As DataTable

        lblMenuAddEdit.Text = "EDIT"
        lblActionIDAddEdit.Text = Me.ActionID
        txtResultID.Visible = False
        lblResultID.Text = Result.resultid
        txtResultName.Text = Result.resultname

        lblResultID.Visible = True

        getResultCategory()

        cboResultCategory.SelectedIndex = cboResultCategory.Items.IndexOf(cboResultCategory.Items.FindByValue(Me.ResultCategory))


        Result = m_ActionResult.GetResultbyID(Result)

        ds = Result.ListData


    End Sub

    Private Sub Add()
        Dim oResult As New Parameter.ActionResult
        With oResult
            .strConnection = GetConnectionString
            .actionid = Me.ActionID
            .resultcategory = cboResultCategory.SelectedItem.Value
            .resultid = txtResultID.Text.Trim
            .resultname = txtResultName.Text
        End With

        Try
            m_ActionResult.ResultAdd(oResult)            
            ShowMessage(lblMessage, "Tambah data berhasil", False)
            'MsgBox(MessageHelper.MESSAGE_INSERT_SUCCESS)
        Catch ex As Exception            
            ShowMessage(lblMessage, ex.Message, True)
        End Try

    End Sub

    Private Sub edit(ByVal Resultid As String)

        Dim oResult As New Parameter.ActionResult
        With oResult
            .strConnection = GetConnectionString
            .actionid = Me.ActionID
            .resultid = Resultid
            .resultname = txtResultName.Text
            .resultcategory = cboResultCategory.SelectedItem.Value
        End With

        Try
            m_ActionResult.ResultEdit(oResult)            
            ShowMessage(lblMessage, "Simpan data berhasil ", False)
            'MsgBox(MessageHelper.MESSAGE_INSERT_SUCCESS)
        Catch ex As Exception            
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

    Private Sub dtgResult_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgResult.ItemCommand
        Me.ResultID = e.Item.Cells(2).Text
        Me.ResultName = e.Item.Cells(3).Text
        Me.ResultCategory = e.Item.Cells(5).Text

        Select Case e.CommandName
            Case "Edit"
                If checkFeature(Me.Loginid, Me.FormID, "Edit", Me.AppId) Then
                    If sessioninvalid() Then
                        Exit Sub
                    End If
                End If

                Me.ResultID = e.Item.Cells(2).Text

                Dim oResult As New Parameter.ActionResult
                With oResult
                    .strConnection = getConnectionString
                    .resultid = Me.ResultID
                    .resultname = Me.ResultName
                    .resultcategory = Me.ResultCategory
                    Me.ActionAddEdit = "EDIT"
                End With

                PanelAllFalse()
                pnlAddEdit.Visible = True
                viewbyID(oResult)

            Case "Delete"
                If checkFeature(Me.Loginid, Me.FormID, "Del", Me.AppId) Then
                    If sessioninvalid() Then
                        Exit Sub
                    End If
                End If

                Me.ResultID = e.Item.Cells(2).Text
                Me.ResultName = e.Item.Cells(3).Text

                Dim oResult As New Parameter.ActionResult
                With oResult
                    .strConnection = getConnectionString
                    .actionid = Me.ActionID
                    .resultname = Me.ResultName
                    .resultid = Me.ResultID
                End With

                Try
                    m_ActionResult.ResultDelete(oResult)                    
                    ShowMessage(lblMessage, "Hapus data berhasil ", False)
                Catch ex As Exception                    
                    ShowMessage(lblMessage, ex.Message, True)

                Finally
                    BindGridResult("ALL", "")                    
                    pnlAddEdit.Visible = False
                    pnlList.Visible = True

                End Try

        End Select
    End Sub

    Private Sub BtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        PanelAllFalse()
        BindGridResult("ALL", "")
    End Sub

    Private Sub dtgResult_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgResult.ItemDataBound
        Dim imbDelete As ImageButton
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("ImbDelete"), ImageButton)
            imbDelete.Attributes.Add("onclick", "return fConfirm()")
        End If
    End Sub

    Private Sub imgCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        PanelAllFalse()
        InitialDefaultPanel()
    End Sub


    Private Sub imbSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
        Select Case Me.ActionAddEdit
            Case "TAMBAH"
                PanelAllFalse()
                Add()

                BindGridResult(Me.SearchBy, Me.SortBy)
                pnlList.Visible = True

            Case "EDIT"
                PanelAllFalse()
                edit(Me.ResultID)
                BindGridResult(Me.SearchBy, Me.SortBy)
                pnlList.Visible = True


        End Select
    End Sub

    Private Sub ImageButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonBack.Click
        Response.Redirect("CollAction.aspx")
    End Sub

End Class