﻿#Region "imports"
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class CollGroupZipCodeSetting
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents UCEmployee As UcFindEmployee
    Protected WithEvents UCBranchCollection1 As UcBranchCollection

    Dim dtCity As New DataTable

#Region " Private Const "
    Dim m_CollZipCode As New CollZipCodeController

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

#End Region

#Region " Property "

    Private Property DefaultCG() As String
        Get
            Return CStr(viewstate("DefaultCG"))
        End Get
        Set(ByVal Value As String)
            viewstate("DefaultCG") = Value
        End Set
    End Property

    Private Property BindMenu() As String
        Get
            Return CStr(viewstate("BindMenu"))
        End Get
        Set(ByVal Value As String)
            viewstate("BindMenu") = Value
        End Set
    End Property

    Private Property CGID() As String
        Get
            Return CStr(viewstate("CGID"))
        End Get
        Set(ByVal Value As String)
            viewstate("CGID") = Value
        End Set
    End Property

    Private Property City() As String
        Get
            Return CStr(viewstate("City"))
        End Get
        Set(ByVal Value As String)
            viewstate("City") = Value
        End Set
    End Property

    Private Property CityName() As String
        Get
            Return CStr(viewstate("CityName"))
        End Get
        Set(ByVal Value As String)
            viewstate("CityName") = Value
        End Set
    End Property

    Private Property CGName() As String
        Get
            Return CStr(viewstate("CGName"))
        End Get
        Set(ByVal Value As String)
            viewstate("CGName") = Value
        End Set
    End Property

    Public Property ZipCode() As String
        Get
            Return CStr(ViewState("ZipCode"))
        End Get
        Set(ByVal Value As String)
            viewstate("ZipCode") = Value
        End Set
    End Property

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        'Put user code to initialize the page here
        'createxmdlist()
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not Me.IsPostBack Then
            Me.CGID = Me.GroubDbID
            Me.FormID = "CollGroupZipCode"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                InitialDefaultPanel()
                Me.SearchBy = "all"
                Me.SortBy = ""
                If Request("cond") <> "" Then
                    Me.SearchBy = Request("cond")
                Else
                    Me.SearchBy = "ALL"
                End If
                Me.SortBy = ""
                getCityCombo()
            End If
        End If
    End Sub

#Region " PanelAllFalse "
    Private Sub PanelAllFalse()        
        PnlSearch.Visible = False
        PnlSearchDetail.Visible = False
        pnlList.Visible = False
    End Sub
#End Region

#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
            'Trim(lblTotPage.Text)
            '
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select

        'SavePage()
        Me.BindMenu = ""
        PnlSearch.Visible = False
        PnlSearchDetail.Visible = True
        BindGridCollZipCode(Me.SearchBy, Me.SortBy)

        pnlList.Visible = True
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click

        If txtPage.Text = "" Then
            txtPage.Text = "0"
        Else
            If IsNumeric(txtPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And (CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer)) Then
                    'SavePage()
                    Me.BindMenu = ""
                    currentPage = CType(txtPage.Text, Int32)
                    BindGridCollZipCode(Me.SearchBy, Me.SortBy)
                End If
            End If
        End If

        pnlList.Visible = True

    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If

        If Me.CGID = "" Then Me.CGID = Me.GroubDbID

        BindGridCollZipCode(Me.SearchBy, Me.SortBy)
        pnlList.Visible = True
    End Sub

#End Region

#Region " InitialDefaultPanel "
    Private Sub InitialDefaultPanel()
        PnlSearch.Visible = True
        PnlSearchDetail.Visible = False
        pnlList.Visible = False
    End Sub
#End Region

    'Sub createxmdlist()
    '    Dim dtEntity As New DataTable
    '    Dim dsList As New DataSet
    '    Dim oAction As Entities.ActionResult
    '    Dim strconn As String = getconnectionstring

    '    oAction = m_CollZipCode.GetCollZipCodeReport(strconn)
    '    dtEntity = oAction.ListData

    '    dsList.Tables.Add(dtEntity.Clone)
    '    dsList.DataSetName = "Action"
    '    dsList.WriteXmlSchema("c:\reportxmd\action.xmd")
    'End Sub

#Region " BindGrid"
    Sub BindGridCollZipCode(ByVal cmdWhere As String, ByVal cmSort As String)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim oCollZipCode As New Parameter.CollZipCode

        With oCollZipCode
            .strConnection = GetConnectionString()
            .City = Me.City
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oCollZipCode = m_CollZipCode.GetCollZipCodeList(oCollZipCode)

        With oCollZipCode
            lblrecord.Text = CType(.TotalRecord, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecord
        End With

        dtsEntity = oCollZipCode.ListData
        dtvEntity = dtsEntity.DefaultView
        dtvEntity.Sort = Me.SortBy
        dtgCollZipCode.DataSource = dtvEntity
        Try
            dtgCollZipCode.DataBind()
        Catch
            dtgCollZipCode.CurrentPageIndex = 0
            dtgCollZipCode.DataBind()
        End Try

        PagingFooter()
    End Sub
#End Region

    Private Sub BtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSearch.Click

        Me.City = cboCity.SelectedItem.Value.Trim
        Me.CityName = cboCity.SelectedItem.Text.Trim
        Me.SearchBy = ""
        Me.SortBy = ""
        Me.BindMenu = ""        

        If cboCity.SelectedItem.Value <> "0" Then
            PanelAllFalse()
            BindGridCollZipCode(Me.SearchBy, Me.SortBy)

            pnlList.Visible = True

            getCGCombo()
            lblCity.Text = Me.City
            cbUnZipCode.Checked = False
            txtSearchByValue.Text = ""
            txtPage.Text = "1"

            PnlSearchDetail.Visible = True
            PnlSearch.Visible = False
        End If

    End Sub

    Private Sub BtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        cboSearchBy.SelectedIndex = 0
        txtSearchByValue.Text = ""
        cbUnZipCode.Checked = False        

        Me.SortBy = ""
        Me.SearchBy = "all"

        BindGridCollZipCode("ALL", "")
        pnlList.Visible = True
    End Sub

    Private Sub dtgCollZipCode_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgCollZipCode.ItemDataBound
        Dim cbo As New DropDownList
        If e.Item.ItemIndex >= 0 Then
            cbo = CType(e.Item.FindControl("cboCG"), DropDownList)

            Dim oCollZipCodeList As New Parameter.CollZipCode
            Dim strconn As String = getConnectionString()
            Dim dt As New DataTable

            dt = m_CollZipCode.GetCollectionGroupCombo(strconn)
            cbo.DataSource = dt

            cbo.DataTextField = "cgname"
            cbo.DataValueField = "cgid"
            cbo.DataBind()

            cbo.Items.Insert(0, "Select One")
            cbo.Items(0).Value = ""

            If Me.BindMenu = "All" Then
                cbo.SelectedIndex = cbo.Items.IndexOf(cbo.Items.FindByValue(Me.DefaultCG))
            ElseIf Me.BindMenu = "UnZipCode" Then
                If cbo.SelectedItem.Value = "" Then cbo.SelectedIndex = cbo.Items.IndexOf(cbo.Items.FindByValue(Me.DefaultCG))
            Else
                cbo.SelectedIndex = cbo.Items.IndexOf(cbo.Items.FindByValue(e.Item.Cells(4).Text.Trim))
            End If

        End If

    End Sub

    Private Sub imgCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        PanelAllFalse()
        InitialDefaultPanel()
    End Sub

    Private Sub imbPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonPrint.Click
        If SessionInvalid() Then
            Exit Sub
        End If
        Dim cookie As HttpCookie = Request.Cookies(COOKIES_COLLECTOR_SETTING)
        If Not cookie Is Nothing Then
            cookie.Values("City") = cboCity.SelectedItem.Text.Trim
            cookie.Values("CityId") = cboCity.SelectedItem.Value.Trim
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie(COOKIES_COLLECTOR_SETTING)
            cookieNew.Values.Add("City", cboCity.SelectedItem.Text.Trim)
            cookieNew.Values.Add("CityId", cboCity.SelectedItem.Value.Trim)
            Response.AppendCookie(cookieNew)
        End If
        Response.Redirect("report/CollGroupZipCodeRpt.aspx")
    End Sub

    Private Sub getCityCombo()
        Dim oCollZipCodeList As New Parameter.CollZipCode
        Dim strconn As String = getConnectionString()

        dtCity = m_CollZipCode.GetCityCombo(strconn)
        cboCity.DataSource = dtCity
        cboCity.DataTextField = "city"
        cboCity.DataValueField = "city"

        cboCity.DataBind()

        cboCity.Items.Insert(0, "Select One")
        cboCity.Items(0).Value = "0"

    End Sub

    Private Sub getCGCombo()
        Dim oCollZipCodeList As New Parameter.CollZipCode
        Dim strconn As String = getConnectionString()
        Dim dt As New DataTable

        dt = m_CollZipCode.GetCollectionGroupCombo(strconn)
        cboDefaultCG.DataSource = dt
        cboDefaultCG.DataTextField = "cgname"
        cboDefaultCG.DataValueField = "cgid"
        cboDefaultCG.DataBind()

        cboDefaultCG.Items.Insert(0, "Select One")
        cboDefaultCG.Items(0).Value = "0"
    End Sub

    'Private Sub fillCGComboGrid(ByVal cbo As DropDownList)

    '    Dim oCollZipCodeList As New Parameter.CollZipCode
    '    Dim strconn As String = getConnectionString()
    '    Dim dt As New DataTable

    '    dt = m_CollZipCode.GetCollectionGroupCombo(strconn)
    '    cbo.DataSource = dt
    '    cboDefaultCG.DataTextField = "cgid"
    '    cboDefaultCG.DataValueField = "cgname"
    '    cbo.DataBind()
    'End Sub

    Private Sub imbSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Buttonsave.Click
        SavePage()
    End Sub

    Private Sub SavePage()
        Dim oCollZipCode As New Parameter.CollZipCode

        Dim LoopZipCode As Int16
        Dim strZipCode As String
        Dim strKelurahan As String
        Dim strKecamatan As String
        Dim cboCG As New DropDownList

        For LoopZipCode = 0 To CType(dtgCollZipCode.Items.Count - 1, Int16)
            strZipCode = CType(dtgCollZipCode.Items(LoopZipCode).Cells(0).Text.Trim, String)
            strKelurahan = CType(dtgCollZipCode.Items(LoopZipCode).Cells(1).Text.Trim, String)
            strKecamatan = CType(dtgCollZipCode.Items(LoopZipCode).Cells(2).Text.Trim, String)
            cboCG = CType(dtgCollZipCode.Items(LoopZipCode).Cells(3).FindControl("cboCG"), DropDownList)

            With oCollZipCode
                .City = Me.City
                .CGID = cboCG.SelectedItem.Value
                .ZipCode = strZipCode.Trim
                .Kelurahan = strKelurahan.Trim
                .Kecamatan = strKecamatan.Trim
                .strConnection = GetConnectionString
            End With
            Try
                m_CollZipCode.CollZipCodeEdit(oCollZipCode)                
                ShowMessage(lblMessage, "Update Data berhasil ", True)
            Catch ex As Exception                
                ShowMessage(lblMessage, ex.Message, True)
            End Try


        Next
        BindGridCollZipCode(Me.SearchBy, Me.SortBy)
        pnlList.Visible = True
        PnlSearch.Visible = False
        PnlSearchDetail.Visible = True
    End Sub

    Private Sub BtnSearchDetail_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSearchDetail.Click
        Me.BindMenu = ""

        If cboSearchBy.SelectedItem.Value <> "0" Or txtSearchByValue.Text.Trim <> "" Then
            Me.SearchBy = " and " & cboSearchBy.SelectedItem.Value & " like '%" & txtSearchByValue.Text.Trim & "%'"

            If cbUnZipCode.Checked = True Then
                Me.SearchBy = Me.SearchBy + " and (cgid='' or cgid is null)"
            End If
        Else

            If cbUnZipCode.Checked = True Then
                Me.SearchBy = " AND (cgid='' or cgid is null)"
                Me.DefaultCG = ""
            Else
                Me.SearchBy = ""
            End If
        End If

        Me.SortBy = ""
        BindGridCollZipCode(Me.SearchBy, Me.SortBy)
        pnlList.Visible = True
        PnlSearch.Visible = False
        PnlSearchDetail.Visible = True
    End Sub

    Private Sub imbApply_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonApply.Click
        If cboDefaultCG.SelectedItem.Value <> "0" Then

            Dim cboCG As New DropDownList
            Dim LoopZipCode As Int16

            If cboCriteria.SelectedItem.Value.Trim = "All" Then
                Me.BindMenu = "All"
                For LoopZipCode = 0 To CType(dtgCollZipCode.Items.Count - 1, Int16)
                    cboCG = CType(dtgCollZipCode.Items(LoopZipCode).Cells(3).FindControl("cboCG"), DropDownList)
                    cboCG.SelectedIndex = cboCG.Items.IndexOf(cboCG.Items.FindByValue(cboDefaultCG.SelectedItem.Value))
                Next

            Else
                Me.BindMenu = "UnZipCode"
                For LoopZipCode = 0 To CType(dtgCollZipCode.Items.Count - 1, Int16)
                    cboCG = CType(dtgCollZipCode.Items(LoopZipCode).Cells(3).FindControl("cboCG"), DropDownList)
                    If cboCG.SelectedItem.Value = "" Then
                        cboCG.SelectedIndex = cboCG.Items.IndexOf(cboCG.Items.FindByValue(cboDefaultCG.SelectedItem.Value))
                    End If
                Next
            End If


            Me.DefaultCG = cboDefaultCG.SelectedItem.Value

            pnlList.Visible = True
            PnlSearchDetail.Visible = True
            PnlSearch.Visible = False

        End If
    End Sub

    Private Sub BtnResetDetail_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonResetDetail.Click
        Me.SearchBy = ""
        Me.SortBy = ""
        Me.BindMenu = ""

        cbUnZipCode.Checked = False
        cboSearchBy.SelectedIndex = 0
        txtSearchByValue.Text = ""

        BindGridCollZipCode(Me.SearchBy, Me.SortBy)
        pnlList.Visible = True
        PnlSearch.Visible = False
        PnlSearchDetail.Visible = True
    End Sub

    Private Sub ImageButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonPrint.Click
        InitialDefaultPanel()
    End Sub


End Class