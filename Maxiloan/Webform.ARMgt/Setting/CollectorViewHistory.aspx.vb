﻿#Region "imports"
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region



Public Class CollectorViewHistory
    Inherits Maxiloan.Webform.WebBased


    Dim m_Controller As New CollectorProfController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1


#Region " Property "
    Private Property CGID() As String
        Get
            Return CStr(ViewState("CGID"))
        End Get
        Set(ByVal Value As String)
            ViewState("CGID") = Value
        End Set
    End Property

    Private Property CollectorID() As String
        Get
            Return CStr(ViewState("CollectorID"))
        End Get
        Set(ByVal Value As String)
            ViewState("CollectorID") = Value
        End Set
    End Property

    Private Property NoMOU() As String
        Get
            Return CStr(ViewState("NoMOU"))
        End Get
        Set(ByVal Value As String)
            ViewState("NoMOU") = Value
        End Set
    End Property

#End Region


#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        Me.SortBy = ""
        'viewbyID(Me.CollectorID, Me.BranchID, Me.NoMOU)
        BindGridCollectorMOU(Me.SearchBy, Me.SortBy)
        pnlList.Visible = True
    End Sub

    Protected Sub btnPageNumb_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPageNumb.Click

        If txtPage.Text = "" Then
            txtPage.Text = "0"
        Else
            If IsNumeric(txtPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                    currentPage = CType(txtPage.Text, Int32)
                    viewbyID(Me.CollectorID, Me.BranchID, Me.NoMOU)
                End If
            End If
        End If

        pnlList.Visible = True

    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If

        viewbyID(Me.CollectorID, Me.BranchID, Me.NoMOU)
        pnlList.Visible = True
    End Sub
#End Region

    Private Sub FillNumberInDgrItemBG()
        For Baris = 0 To dtgMOU.Items.Count - 1
            Dim lblNo As Label
            lblNo = CType(dtgMOU.Items(Baris).FindControl("lblNo"), Label)
            lblNo.Text = CStr(Baris + 1)
        Next
    End Sub

    Sub BindGridCollectorMOU(ByVal cmdWhere As String, ByVal cmSort As String)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim oCollector As New Parameter.Collector

        With oCollector
            .strConnection = GetConnectionString()
            .BranchId = Me.BranchID
            .CollectorID = Me.CollectorID
            .NoMOU = Me.NoMOU
            '.CGID = Me.CGID
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            If SortBy = Nothing Then
                .SortBy = ""
            End If
        End With


        oCollector = m_Controller.CollectorMOUExtendHistory(oCollector)

        With oCollector
            lblrecord.Text = CType(.TotalRecord, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecord
        End With

        dtsEntity = oCollector.ListData
        dtvEntity = dtsEntity.DefaultView
        dtvEntity.Sort = Me.SortBy
        dtgMOU.DataSource = dtvEntity
        Try
            dtgMOU.DataBind()
        Catch
            dtgMOU.CurrentPageIndex = 0
            dtgMOU.DataBind()
        End Try
        FillNumberInDgrItemBG()
        PagingFooter()
    End Sub

    Private Sub viewbyID(ByVal CollectorID As String, ByVal BranchID As String, ByVal NoMOU As String)
        Try
            Dim oCollector As New Parameter.Collector
            'Dim dtInsCo As New DataTable
            Me.SortBy = ""
            oCollector.CollectorID = Me.CollectorID
            oCollector.BranchId = Me.BranchID
            oCollector.NoMOU = Me.NoMOU
            If SortBy = Nothing Then
                oCollector.SortBy = ""
            End If
            oCollector.CurrentPage = currentPage
            oCollector.PageSize = pageSize
            oCollector.strConnection = GetConnectionString()


            Dim dtl As New DataTable
            Dim lblCollectorID2 As String
            Dim lblNamaCollector2 As String
            Dim lblCollectionGroup2 As String
            oCollector = m_Controller.CollectorMOUExtendHistory(oCollector)

            dtl = oCollector.ListData
            If dtl.Rows.Count > 0 Then

                lblCollectorID2 = CStr(dtl.Rows(0).Item("CollectorID"))
                lblNamaCollector2 = CStr(dtl.Rows(0).Item("CollectorName"))
                lblCollectionGroup2 = CStr(dtl.Rows(0).Item("CGName"))

                lblCollectorID.Text = lblCollectorID2.Trim
                lblNamaCollector.Text = lblNamaCollector2.Trim
                lblCollectionGroup.Text = lblCollectionGroup2.Trim
           
            End If
        Catch ex As Exception

        End Try

    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not Page.IsPostBack Then
            'If Request.QueryString("CollectorID").Trim <> "" Then Me.CollectorID = Request.QueryString("pCollID")
            'If Request.QueryString("BranchID").Trim <> "" Then Me.BranchID = Request.QueryString("pBranchID")
            'If Request.QueryString("NoMOU").Trim <> "" Then Me.NoMOU = Request.QueryString("pNoMOU")

            Me.CollectorID = Request.QueryString("CollectorID").Trim
            Me.BranchID = Request.QueryString("BranchID").Trim
            Me.NoMOU = Request.QueryString("NoMOU").Trim



            Me.FormID = "CollectorViewHistory"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.SearchBy = "all"
                Me.SortBy = ""
                viewbyID(Me.CollectorID, Me.BranchID, Me.NoMOU)
                PnlView.Visible = True
                pnlClose.Visible = True
                ButtonClose.Attributes.Add("onclick", "windowClose()")
                BindGridCollectorMOU(Me.SearchBy, Me.SortBy)

            End If
        End If

    End Sub

End Class