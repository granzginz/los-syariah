﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="GantiCollector.aspx.vb"
    Inherits="Maxiloan.Webform.ARMgt.GantiCollector" %>

<%@ Register TagPrefix="uc1" TagName="UcBranchCollection" Src="../../Webform.UserController/UcBranchCollection.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>GantiCollector</title>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;

        function fConfirm() {
            if (window.confirm("Apakah yakin mau hapus data ini ? "))
                return true;
            else
                return false;
        }
        function fback() {
            history.go(-1);
            return false;
        }
        
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
        onclick="hideMessage();"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                GANTI COLLECTOR
            </h3>
        </div>
    </div>
    <asp:Panel ID="PnlSearch" runat="server">
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <uc1:ucbranchcollection id="UcCG" runat="server"></uc1:ucbranchcollection>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonSearch" runat="server" Text="Search" CssClass="small button blue">
            </asp:Button>&nbsp;
            <asp:Button ID="ButtonReset" runat="server" Text="Reset" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="PnlA" runat="server">
        <div class="form_box_title">
            <div class="form_single">
                <h4>
                    WILAYAH YANG MAU DIGANTI
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    ID Cabang</label>
                <asp:Label ID="lblCGID" runat="server" Width="184px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Nama Cabang</label>
                <asp:Label ID="lblCGName" runat="server" Width="184px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Collector</label>
                <asp:DropDownList ID="cboCollA" runat="server">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="Dynamic"
                    InitialValue="" ControlToValidate="cboCollA" ErrorMessage="*" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cari Berdasarkan</label>
                <asp:DropDownList ID="cboSearchByA" runat="server" Width="109px">
                    <asp:ListItem Value="" Selected="True">Select One</asp:ListItem>
                    <asp:ListItem Value="CollectorAllocation.Kelurahan">Kelurahan</asp:ListItem>
                    <asp:ListItem Value="CollectorAllocation.ZipCode">KodePos</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearchByValueA" runat="server" Width="88px"></asp:TextBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonSearchDetailA" runat="server" Text="Search" CssClass="small button blue">
            </asp:Button>&nbsp;
            <asp:Button ID="ButtonResetDetailA" runat="server" Text="Reset" CssClass="small button gray"
                CausesValidation="False"></asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlGridA">
        <div class="form_box_title">
            <div class="form_single">
                <h4>
                    DAFTAR DATA
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgA" runat="server" Width="100%" CssClass="grid_general" BorderStyle="None"
                        BorderWidth="0" AllowSorting="True" AutoGenerateColumns="False">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn>
                                <HeaderTemplate>
                                    <asp:CheckBox ID="chkAll" AutoPostBack="true" OnCheckedChanged="Changed_chkAll_dtgA"
                                        runat="server"></asp:CheckBox>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chk" runat="server"></asp:CheckBox>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="Kelurahan" SortExpression="KELURAHAN" HeaderText="KELURAHAN">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="ZipCode" SortExpression="ZIPCODE" HeaderText="KODE POS">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="CGID" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="CollectorID" Visible="false"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlB" runat="server">
        <div class="form_box_title">
            <div class="form_single">
                <h4>
                    COLLECTOR YANG MENGGANTI
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Collector</label>
                <asp:DropDownList ID="cboCollB" runat="server">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" Display="Dynamic"
                    InitialValue="" ControlToValidate="cboCollB" ErrorMessage="*" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonSave" runat="server" Text="Save" CssClass="small button blue"></asp:Button>&nbsp;
        </div> 
    </asp:Panel>
    </form>
</body>
</html>
