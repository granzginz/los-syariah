﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewKewajibanNasabah.aspx.vb"
    Inherits="Maxiloan.Webform.ARMgt.ViewKewajibanNasabah" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ViewKewajibanNasabah</title>
<%--    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';			
    </script>--%>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link href="../../include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
</head>
<body>
    <input id="hdnAgreement" type="hidden" name="hdnAgreement" runat="server" />
    <input id="hdnCustName" type="hidden" name="hdnCustName" runat="server" />
    <input id="hdnCustID" type="hidden" name="hdnCustID" runat="server" />
    <input id="hdnBranchID" type="hidden" name="hdnBranchID" runat="server" />
    <input id="hdnApplicationID" type="hidden" name="hdnApplicationID" runat="server" />
    <input id="hdnAlamat" type="hidden" name="hdnALamat" runat="server" />
    <script language="javascript" type="text/javascript">
        function fClose() {
            window.close();
            return false;
        }	
    </script>

    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">           
                <h3>
                    VIEW KEWAJIBAN NASABAH
                </h3>
            </div>
        </div>  
        <asp:Panel ID="PnlListDetail" runat="server">
        <div class="form_box">	        
                <div class="form_left">
                    <label>No Kontrak</label>
                    <asp:HyperLink ID="lblAgreementNo" runat="server"></asp:HyperLink>
		        </div>
		        <div class="form_right">	
                    <label>Alamat</label>	
                    <asp:HyperLink ID="lblAlamat" runat="server"></asp:HyperLink>	
		        </div>	        
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <label>Nomor Nasabah</label>
                    <asp:Label ID="lblCustomerID" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">			
                    <label>Nama Nasabah</label>
                    <asp:Label ID="lblCustomerName" runat="server"></asp:Label>
		        </div>	        
        </div>
         <div class="form_box">	        
                <div class="form_left">
                    <label>Tanggal Pelunasan</label>
                        <asp:TextBox ID="txtTglPelunasan" runat="server" CssClass="small_text" onkeypress="return numbersonly2(event)"></asp:TextBox>
                        <asp:CalendarExtender ID="txtTglPelunasan_CalendarExtender" runat="server" 
                        Enabled="True" TargetControlID="txtTglPelunasan" Format ="dd/MM/yyyy"></asp:CalendarExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*" CssClass="validator_general"
                        ControlToValidate="txtTglPelunasan"></asp:RequiredFieldValidator>                 
		        </div>
		        <div class="form_right">	
                    <asp:Button ID="btnHitung" runat="server" Text="Hitung" CssClass="small button blue"
                        CausesValidation="True"></asp:Button>
		        </div>	        
        </div>
<div class="form_box_header">
        <div class="form_single">
            <asp:DataGrid ID="dtg" runat="server" AutoGenerateColumns="False" BorderWidth="0px"
                CellPadding="3" EnableViewState="False" CellSpacing="1" CssClass="grid_general"
                Width="100%">
                <ItemStyle CssClass="item_grid" />
                <HeaderStyle CssClass="th" />
                <Columns>
                    <asp:TemplateColumn HeaderText="NO" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Center">                       
                        <ItemTemplate>
                            <asp:Label ID="lblNo" runat="server"  Text='<%# formatnumber(container.dataitem("InsSeqNo"),0) %>'> </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:BoundColumn Visible="False" DataField="InsSeqNo"></asp:BoundColumn>
<%--                    <asp:BoundColumn DataField="DUEDATE" HeaderText="TGL JT" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">  
                    </asp:BoundColumn>--%>
                    <asp:TemplateColumn HeaderText="TGL JT" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Center">                       
                        <ItemTemplate>
                            <asp:Label ID="lblDueDate" runat="server"  Text='<%# container.dataitem("DueDate") %>'> </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                     <asp:BoundColumn DataField="PaidDate" HeaderText="TGL BAYAR" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">                        
                    </asp:BoundColumn>
                     <asp:BoundColumn Visible="False" DataField="GracePeriod" HeaderText="Grace Period" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Center">                        
                    </asp:BoundColumn>
                     <asp:BoundColumn DataField="OD" HeaderText="OD" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Center">                        
                    </asp:BoundColumn>
<%--                     <asp:BoundColumn DataField="Denda" HeaderText="DENDA" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right">                        
                    </asp:BoundColumn>--%>
                    <asp:TemplateColumn HeaderText="DENDA" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Center">                       
                        <ItemTemplate>
                            <asp:Label ID="lblDenda" runat="server"  Text='<%# container.dataitem("Denda") %>'> </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="JML BAYAR" HeaderStyle-HorizontalAlign="Right" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Right">                        
                        <ItemTemplate>
                            <asp:Label ID="lblPaidAmount" runat="server" Text='<%# formatnumber(container.dataitem("PaidAmount"),0) %>'>
                            </asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>                            
                            <asp:Label ID="lblTotPaidAmount" runat="server"></asp:Label>
                        </FooterTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="BYR POKOK" HeaderStyle-HorizontalAlign="Right" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Right">                        
                        <ItemTemplate>
                            <asp:Label ID="lblPaidPrincipal" runat="server" Text='<%# formatnumber(container.dataitem("PaidPrincipal"),0) %>'>
                            </asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>                            
                            <asp:Label ID="lblTotPaidPrincipal" runat="server"></asp:Label>
                        </FooterTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="BYR BUNGA" HeaderStyle-HorizontalAlign="Right" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Right">                        
                        <ItemTemplate>
                            <asp:Label ID="lblPaidInterest" runat="server" Text='<%# formatnumber(container.dataitem("PaidInterest"),0) %>'>
                            </asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>                            
                            <asp:Label ID="lblTotPaidInterest" runat="server"></asp:Label>
                        </FooterTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="BYR DENDA" HeaderStyle-HorizontalAlign="Right" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Right">                        
                        <ItemTemplate>
                            <asp:Label ID="lblPaidLateCharges" runat="server" Text='<%# formatnumber(container.dataitem("PaidLateCharges"),0) %>'>
                            </asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>                            
                            <asp:Label ID="lblTotPaidLateCharges" runat="server"></asp:Label>
                        </FooterTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="ANGSURAN" HeaderStyle-HorizontalAlign="Right" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Right">                        
                        <ItemTemplate>
                            <asp:Label ID="lblInstallmentAmount" runat="server" Text='<%# formatnumber(container.dataitem("InstallmentAmount"),0) %>'>
                            </asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>                            
                            <asp:Label ID="lblTotInstallmentAmount" runat="server"></asp:Label>
                        </FooterTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="POKOK" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right">                        
                        <ItemTemplate>
                            <asp:Label ID="lblPrincipalAmount" runat="server" Text='<%# formatnumber(container.dataitem("PrincipalAmount"),0) %>'>
                            </asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="lblTotPrincipalAmount" runat="server"></asp:Label>
                        </FooterTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="BUNGA" HeaderStyle-HorizontalAlign="Right" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Right">                        
                        <ItemTemplate>
                            <asp:Label ID="lblINTERESTAMOUNT" runat="server" Text='<%# formatnumber(container.dataitem("INTERESTAMOUNT"),0) %>'>
                            </asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="lblTotINTERESTAMOUNT" runat="server"></asp:Label>
                        </FooterTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="O/S POKOK" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right">                        
                        <ItemTemplate>
                            <asp:Label ID="lblDtgOSPrincipal" runat="server" Text='<%# formatnumber(container.dataitem("OutstandingPrincipal"),0) %>'>
                            </asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="lblTotOSPrincipal" runat="server"></asp:Label>
                        </FooterTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="O/S BUNGA" HeaderStyle-HorizontalAlign="Right" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Right">                        
                        <ItemTemplate>
                            <asp:Label ID="lblDtgOSInterest" runat="server" Text='<%# formatnumber(container.dataitem("OutstandingInterest"),0) %>'>
                            </asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="lblTotOSInterest" runat="server"></asp:Label>
                        </FooterTemplate>
                    </asp:TemplateColumn>
                </Columns>
            </asp:DataGrid>
        </div>
    </div>
        <div class="form_box">
	        <div class="form_single">
                    <asp:Label ID="lblKeterangan" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_button">            
        <asp:Button ID="ButtonClose" OnClientClick="fClose();" runat="server" CausesValidation="false" Text="Close" CssClass ="small button gray">
        </asp:Button>     
        <asp:Button ID="ButtonPrint" runat="server" Text="Print" CssClass="small button blue" CausesValidation="True">
        </asp:Button>
    </div>   
    </asp:Panel>
    </form>
</body>
</html>
