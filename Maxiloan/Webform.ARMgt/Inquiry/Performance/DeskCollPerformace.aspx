﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="DeskCollPerformace.aspx.vb"
    Inherits="Maxiloan.Webform.ARMgt.DeskCollPerformace" %>

<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../../../Webform.UserController/UcSearchBy.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcBranchCollection" Src="../../../Webform.UserController/UcBranchCollection.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>DeskCollPerformace</title>
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../../Include/General.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';

        function fConfirm() {
            if (window.confirm("Apakah yakin mau hapus data ini ? "))
                return true;
            else
                return false;
        }
        function fback() {
            history.go(-1);
            return false;
        }	
    </script>
</head>
<body>
    <form id="form1" runat="server">        
        <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">           
                <h3>
                    DESKCOLL PERFORMANCE
                </h3>
            </div>
        </div>  
        <asp:Panel ID="pnlSearch" runat="server">
        <div class="form_box">
	        <div class="form_single">
                <label class ="label_req">Collection Group </label>
                <uc1:UcBranchCollection id="oCGID" runat="server"></uc1:UcBranchCollection>	        
            </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Bulan</label>
                <asp:DropDownList ID="cboMonth" runat="server">
                    <asp:ListItem Value="1">1</asp:ListItem>
                    <asp:ListItem Value="2">2</asp:ListItem>
                    <asp:ListItem Value="3">3</asp:ListItem>
                    <asp:ListItem Value="4">4</asp:ListItem>
                    <asp:ListItem Value="5">5</asp:ListItem>
                    <asp:ListItem Value="6">6</asp:ListItem>
                    <asp:ListItem Value="7">7</asp:ListItem>
                    <asp:ListItem Value="8">8</asp:ListItem>
                    <asp:ListItem Value="9">9</asp:ListItem>
                    <asp:ListItem Value="10">10</asp:ListItem>
                    <asp:ListItem Value="11">11</asp:ListItem>
                    <asp:ListItem Value="12">12</asp:ListItem>
                    <asp:ListItem></asp:ListItem>
                </asp:DropDownList>
                Tahun
                <asp:TextBox ID="txtYear" runat="server"  Width="80px" MaxLength="4"
                    Columns="4"></asp:TextBox>
                <asp:RequiredFieldValidator ID="reqYear" runat="server" Display="Static" ErrorMessage="Harap isi Tahun" CssClass="validator_general"
                    ControlToValidate="txtYear"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="regYear" runat="server" Display="Static" ControlToValidate="txtYear" CssClass="validator_general"
                    ErrorMessage="Harap isi dengan Angka" Enabled="True" ValidationExpression="^\d{1,}$|^\d{1,}[.{1}]\d{1,}$"
                    Visible="True"></asp:RegularExpressionValidator>
	        </div>
        </div>        
        <div class="form_box">
	        <div class="form_single">
                <label>Periode</label>
                <asp:DropDownList ID="cboPeriod" runat="server">
                </asp:DropDownList>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Status Awal</label>
                <asp:DropDownList ID="cboBeginStatus" runat="server">
                </asp:DropDownList>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Status Akhir</label>
                <asp:DropDownList ID="cboEndStatus" runat="server">
                </asp:DropDownList>
	        </div>
        </div>
        <div class="form_box_uc">
                <uc1:ucsearchby id="oSearchBy" runat="server"></uc1:ucsearchby>	        
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonSearch" runat="server"  Text="Search" CssClass ="small button blue"
                CausesValidation="True"></asp:Button>&nbsp;
            <asp:Button ID="ButtonReset" runat="server"  Text="Reset" CssClass ="small button gray"
                CausesValidation="False"></asp:Button>
	    </div>      
        </asp:Panel>
        <asp:Panel ID="pnlgrid" runat="server">
        <div class="form_box_title">
            <div class="form_single">              
                <h4>
                    DAFTAR BUCKET MOVEMENT
                </h4>
            </div>
        </div>
        <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgPaging" runat="server" Width="100%" AllowSorting="True"
                        AutoGenerateColumns="False" DataKeyField="Period" OnSortCommand="Sorting" CssClass="grid_general" BorderStyle="None" BorderWidth="0">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="BULAN" SortExpression="BmMonth">                                
                                <ItemTemplate>
                                    <asp:Literal ID="ltlMonth" runat="server" Visible="true" Text='<%#Container.DataItem("BMMonth")%>'>
                                    </asp:Literal>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="PERIODE" SortExpression="Period">                                
                                <ItemTemplate>
                                    <asp:Literal ID="ltlPeriod" runat="server" Visible="true" Text='<%#Container.DataItem("Period")%>'>
                                    </asp:Literal>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="NO KONTRAK" SortExpression="AgreementNo">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo").trim %>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="NAMA CUSTOMER" SortExpression="Name">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="HypCustomerName" runat="server" Text='<%#Container.DataItem("CustomerName")%>'>
                                    </asp:HyperLink>
                                    <asp:Literal ID="ltlCustomerId" runat="server" Visible="false" Text='<%#Container.DataItem("CustomerId")%>'>
                                    </asp:Literal>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="MULAI TELAT" SortExpression="BeginPastDueDays">                                
                                <ItemTemplate>
                                    <asp:Literal ID="Literal1" runat="server" Visible="true" Text='<%#Container.DataItem("BeginOD")%>'>
                                    </asp:Literal>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="STATUS AWAL" SortExpression="BeginStatus">                                
                                <ItemTemplate>
                                    <asp:Literal ID="ltlBeginStatus" runat="server" Visible="true" Text='<%#Container.DataItem("BeginStatus")%>'>
                                    </asp:Literal>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="BEGIN HOLDER" SortExpression="BeginHolder">                                
                                <ItemTemplate>
                                    <asp:Literal ID="ltlBeginHolder" runat="server" Visible="true" Text='<%#Container.DataItem("BeginHolder")%>'>
                                    </asp:Literal>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="AKHIR TELAT" SortExpression="EndPastDueDays">                                
                                <ItemTemplate>
                                    <asp:Literal ID="ltlEndOD" runat="server" Visible="true" Text='<%#Container.DataItem("EndOD")%>'>
                                    </asp:Literal>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="STATUS AKHIR" SortExpression="EndStatus">                                
                                <ItemTemplate>
                                    <asp:Literal ID="ltlEndStatus" runat="server" Visible="true" Text='<%#Container.DataItem("EndStatus")%>'>
                                    </asp:Literal>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="END HOLDER" SortExpression="EndHolder">                                
                                <ItemTemplate>
                                    <asp:Literal ID="ltlEndHolder" runat="server" Visible="true" Text='<%#Container.DataItem("EndHolder")%>'>
                                    </asp:Literal>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                </asp:DataGrid>       
                <div class="button_gridnavigation">
	                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../../Images/grid_navbutton01.png"
		                CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../../Images/grid_navbutton02.png"
		                CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../../Images/grid_navbutton03.png"
		                CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../../Images/grid_navbutton04.png"
		                CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
	                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
		                EnableViewState="False"></asp:Button>
	                <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage"
				        MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer"
				        CssClass="validator_general"></asp:RangeValidator>
			        <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
				        ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general">
			        </asp:RequiredFieldValidator>              
                </div>
                <div class="label_gridnavigation">
	                <asp:Label ID="lblPage" runat="server"></asp:Label>of
	                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
	                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                </div>
            </div>
        </div>    
        </div>    
    </asp:Panel>
    </form>
</body>
</html>
