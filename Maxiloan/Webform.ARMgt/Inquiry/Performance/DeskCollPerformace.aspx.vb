﻿#Region "Imports"
Imports System.Text
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class DeskCollPerformace
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oSearchBY As UcSearchBy
    Protected WithEvents oCGID As UcBranchCollection

#Region "Constanta"
    'Private m_controller As New PremiumToCustomerController
    Private oDeskCollPerformance As New Parameter.DeskCollPerformance
    Private m_controller As New DeskCollPerformance
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        'Put user code to initialize the page here
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not Me.IsPostBack Then
            txtPage.Text = "1"
            'cboBeginStatus.SelectedIndex = cboBeginStatus.Items.IndexOf(cboBeginStatus.Items.FindByValue(EndStatusValue.Text))
            cboMonth.SelectedIndex = cboMonth.Items.IndexOf(cboMonth.Items.FindByValue(CStr(Month(Date.Now))))
            txtYear.Text = CStr(Year(Date.Now))

            oSearchBY.ListData = "AGREEMENTNO,Agreement No-Name,Customer Name"
            oSearchBY.BindData()

            'With cboCG
            '    .DataSource = m_controller.GetComboCG(GetConnectionString)
            '    .DataValueField = "ID"
            '    .DataTextField = "Name"
            '    .DataBind()
            '    .Items.Insert(0, "ALL")
            '    .Items(0).Value = "ALL"
            'End With
            With cboPeriod
                .DataSource = m_controller.GetPeriod(GetConnectionString)
                .DataValueField = "Period"
                .DataTextField = "Period"
                .DataBind()
                .Items.Insert(0, "ALL")
                .Items(0).Value = "ALL"
            End With
            With cboBeginStatus
                .DataSource = m_controller.GetBeginStatus(GetConnectionString)
                .DataValueField = "ID"
                .DataTextField = "Name"
                .DataBind()
                .Items.Insert(0, "ALL")
                .Items(0).Value = "ALL"
            End With
            With cboEndStatus
                .DataSource = m_controller.GetEndStatus(GetConnectionString)
                .DataValueField = "ID"
                .DataTextField = "Name"
                .DataBind()
                .Items.Insert(0, "ALL")
                .Items(0).Value = "ALL"
            End With


            If CheckForm(Me.Loginid, "CollInqBm", "Maxiloan") Then
                Me.SearchBy = ""
                Me.SortBy = ""
                InitialDefaultPanel()

            End If
        End If
    End Sub
#Region "InitialDefaultPanel"
    Private Sub InitialDefaultPanel()

        pnlSearch.Visible = True        
    End Sub

#End Region
#Region "Navigation "
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region
#Region "Paging Footer"

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then            
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            'rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            'rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
#End Region
#Region "Go Page"
    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
                'BindGridEntity()
            End If
        End If
    End Sub

#End Region
#Region "Sorting"

    Public Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

#End Region
#Region "Reseh..eh Reset !"
    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        Me.SearchBy = " Agreement.BranchID = '" & Me.sesBranchId.Trim.Replace("'", "") & "' "
        Me.SortBy = ""
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region


    Private Sub dtgPaging_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        'Dim hypAgreement As HyperLink
        'Dim ltlCustomerid As Literal
        'Dim hypCustName As HyperLink

        'hypAgreement = CType(e.Item.FindControl("hyAgreementNo"), HyperLink)
        'ltlCustomerid = CType(e.Item.FindControl("ltlCustomerId"), Literal)
        'hypCustName = CType(e.Item.FindControl("hypCustomerName"), HyperLink)

        'hypCustName.NavigateUrl = "javascript:OpenCustomer('" & "Collection" & "', '" & Server.UrlEncode(ltlCustomerid.Text.Trim) & "')"
        'hypAgreement.NavigateUrl = "javascript:OpenAgreementNo('" & "Collection" & "', '" & Server.UrlEncode(hypAgreement.Text.Trim) & "')"

    End Sub

    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim hypAgreement As HyperLink
            Dim ltlCustomerid As Literal
            Dim hypCustName As HyperLink

            hypAgreement = CType(e.Item.FindControl("hyAgreementNo"), HyperLink)
            ltlCustomerid = CType(e.Item.FindControl("ltlCustomerId"), Literal)
            hypCustName = CType(e.Item.FindControl("hypCustomerName"), HyperLink)

            hypCustName.NavigateUrl = "javascript:OpenCustomer('" & "Collection" & "', '" & Server.UrlEncode(ltlCustomerid.Text.Trim) & "')"
            hypAgreement.NavigateUrl = "javascript:OpenAgreementNo('" & "Collection" & "', '" & Server.UrlEncode(hypAgreement.Text.Trim) & "')"

        End If
    End Sub

    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSearch.Click

        If SessionInvalid() Then
            Exit Sub
        End If
        'If checkfeature(Me.Loginid, Me.FormID, "SRCH", Me.AppId) Then
        Dim strSearch As New StringBuilder
        Dim strFilter As New StringBuilder


        If strSearch.ToString <> "" Then
            strSearch.Append(" and CGID='" & oCGID.BranchID & "'")
        Else
            strSearch.Append(" CGID='" & oCGID.BranchID & "'")
        End If

        If cboMonth.SelectedItem.Value <> "ALL" And txtYear.Text <> "" Then
            If strSearch.ToString <> "" Then
                strSearch.Append(" and BMMonth='" & cboMonth.SelectedItem.Value & "' and bmYear='" & txtYear.Text & "'")

            Else
                strSearch.Append("  BMMonth='" & cboMonth.SelectedItem.Value & "' and bmYear='" & txtYear.Text & "'")
            End If
        End If
        If cboPeriod.SelectedItem.Value <> "ALL" Then
            If strSearch.ToString <> "" Then
                strSearch.Append(" and Period ='" & cboPeriod.SelectedItem.Value & "'")
            Else
                strSearch.Append(" and Period ='" & cboPeriod.SelectedItem.Value & "'")
            End If
        End If

        If cboBeginStatus.SelectedItem.Value <> "ALL" Then
            If strSearch.ToString <> "" Then
                strSearch.Append(" and BeginStatus='" & cboBeginStatus.SelectedItem.Value & "'")
            Else
                strSearch.Append(" BeginStatus='" & cboBeginStatus.SelectedItem.Value & "'")
            End If
        End If

        If cboEndStatus.SelectedItem.Value <> "ALL" Then
            If strSearch.ToString <> "" Then
                strSearch.Append(" and EndStatus='" & cboEndStatus.SelectedItem.Value & "'")
            Else
                strSearch.Append("EndStatus='" & cboEndStatus.SelectedItem.Value & "'")
            End If
        End If

        If oSearchBY.Text <> "" Then
            strSearch.Append(" and ")
            If Right(oSearchBY.Text, 1) = "%" Then
                strSearch.Append(oSearchBY.ValueID)
                strSearch.Append(" like '")
                strSearch.Append(oSearchBY.Text)
                strSearch.Append("'")
            Else
                strSearch.Append(oSearchBY.ValueID)
                strSearch.Append(" = '")
                strSearch.Append(oSearchBY.Text)
                strSearch.Append("'")
            End If
        End If

        Me.SearchBy = strSearch.ToString

        pnlgrid.Visible = True
        dtgPaging.Visible = True

        DoBind(Me.SearchBy, Me.SortBy)


    End Sub

#Region "DoBind"

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        Dim intloop As Integer
        Dim hypID As HyperLink


        With oDeskCollPerformance
            .strConnection = GetConnectionString
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oDeskCollPerformance = m_controller.DeskCollPerformancePaging(oDeskCollPerformance)
        If Not oDeskCollPerformance Is Nothing Then
            DtUserList = oDeskCollPerformance.ListData
            recordCount = oDeskCollPerformance.TotalRecord
        Else
            recordCount = 0
        End If
        dtgPaging.DataSource = DtUserList.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()
        PagingFooter()

        pnlgrid.Visible = True        
    End Sub

#End Region

End Class