﻿#Region "Imports"
Imports System.Data.SqlClient
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Parameter.AssetRepo
#End Region

Public Class ViewAssetStatusDetail
    Inherits Maxiloan.Webform.WebBased

#Region " Private Const "
    Dim m_InqAssetRepo As New InqAssetRepoController
#End Region

#Region "Property"
    Private Property CustomerID() As String
        Get
            Return CStr(viewstate("CustomerID"))
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerID") = Value
        End Set
    End Property

    Private Property CGID() As String
        Get
            Return CStr(viewstate("CGID"))
        End Get
        Set(ByVal Value As String)
            viewstate("CGID") = Value
        End Set
    End Property

    Private Property CustomerName() As String
        Get
            Return CStr(viewstate("CustomerName"))
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerName") = Value
        End Set
    End Property

    Private Property AgreementNo() As String
        Get
            Return CStr(viewstate("AgreementNo"))
        End Get
        Set(ByVal Value As String)
            viewstate("AgreementNo") = Value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.CGID = Request.QueryString("CGID")
            Me.AgreementNo = Request.QueryString("AgreementNo")
            Me.CustomerID = Request.QueryString("CustomerID")
            Me.CustomerName = Request.QueryString("CustomerName")
            Dim oInqAssetrepo As New Parameter.AssetRepo
            With oInqAssetrepo
                .strConnection = GetConnectionString()
                .CGID = Me.CGID
                .AgreementNo = Me.AgreementNo
            End With

            Dim dt As New DataTable

            oInqAssetrepo = m_InqAssetRepo.AssetRepoDetail(oInqAssetrepo)
            dt = oInqAssetrepo.listData

            hypAgreementNo.Text = Me.AgreementNo
            hypCustomerName.Text = Me.CustomerName
            If dt.Rows.Count > 0 Then
                lblAsset.Text = CStr(dt.Rows(0).Item("AssetDesc"))
                lblEngineNo.Text = CStr(dt.Rows(0).Item("EngineNo"))
                lblChassisNo.Text = CStr(dt.Rows(0).Item("ChassisNo"))
                lblLicenseNo.Text = CStr(dt.Rows(0).Item("LicensePlate"))
                lblYear.Text = CStr(dt.Rows(0).Item("Year"))
                lblRepoDate.Text = CStr(dt.Rows(0).Item("RepossessDate"))
                lblRepossessBy.Text = CStr(dt.Rows(0).Item("RepossessBy"))
                lblAssetStatus.Text = CStr(dt.Rows(0).Item("AssetStatus"))
                lblInvExpDate.Text = CStr(dt.Rows(0).Item("InventoryExpDate"))
                lblInventoryDate.Text = CStr(dt.Rows(0).Item("InventoryDate"))
                lblInvAmount.Text = FormatNumber(CStr(dt.Rows(0).Item("InventoryAmount")), 0)
                lblSellingPrice.Text = FormatNumber(CStr(dt.Rows(0).Item("SellingAmount")), 0)
                lblSellingDate.Text = CStr(dt.Rows(0).Item("SellingDate"))
                lblBuyer.Text = CStr(dt.Rows(0).Item("Buyer"))
                lblSellingNotes.Text = CStr(dt.Rows(0).Item("SellingNotes"))
                lblRALDate.Text = CStr(dt.Rows(0).Item("RALDate"))
                lblRALNo.Text = CStr(dt.Rows(0).Item("RALNo"))
            End If
        End If
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles ButtonClose.Click
        If Request.QueryString("Referrer") <> "" Then
            Select Case Request.QueryString("Referrer").Trim
                Case "InqAssetRepo"
                    Response.Redirect("InqAssetRepo.aspx")
                Case "InqAssetInventory"
                    Response.Redirect("InqAssetInventory.aspx")
            End Select
        End If

    End Sub

End Class