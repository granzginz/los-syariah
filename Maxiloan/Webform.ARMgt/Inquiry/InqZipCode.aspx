﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="InqZipCode.aspx.vb" Inherits="Maxiloan.Webform.ARMgt.InqZipCode" %>

<%@ Register TagPrefix="cc1" Namespace="System.Web.UI.WebControls" Assembly="System.Web" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>InqZipCode</title>
    <script language="javascript" type="text/javascript">
        function fback() {
            history.go(-1);
            return false;
        }
			
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">    
        <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">           
                <h3>
                    INQUIRY KODE POS
                </h3>
            </div>
        </div>  
        <!--this Panel to create a search City form-->
        <asp:Panel ID="PnlSearch" runat="server">     
        <div class="form_box">
	        <div class="form_single">
                <label class ="label_req">
				Kota </label>
                <asp:DropDownList ID="cboCity" runat="server" Width="144px" >
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Harap pilih Kota" CssClass="validator_general"
                    ControlToValidate="cboCity" InitialValue="0" Display="Dynamic"></asp:RequiredFieldValidator>
	        </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonSearch" runat="server"  Text="Search" CssClass ="small button blue">
            </asp:Button>&nbsp;
            <asp:Button ID="ButtonReset" runat="server"  Text="Reset" CssClass ="small button gray"
                CausesValidation="False"></asp:Button>
	    </div>
        </asp:Panel>
        <!--this panel to view search detail, keluarahan kecamatan zipccode, unallocated zipcode-->
        <asp:Panel ID="PnlSearchDetail" runat="server">
        <div class="form_box_title">
	        <div class="form_single">
                <label>Kota</label>
                <asp:Label ID="lblCity" runat="server" Width="435px"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Cari Berdasarkan</label>
                <asp:DropDownList ID="cboSearchBy" runat="server" Width="109px" >
                    <asp:ListItem Value="0">Select One</asp:ListItem>
                    <asp:ListItem Value="Kelurahan">Kelurahan</asp:ListItem>
                    <asp:ListItem Value="Kecamatan">Kecamatan</asp:ListItem>
                    <asp:ListItem Value="ZipCode">Kode Pos</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearchByValue" runat="server" ></asp:TextBox>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Kode Pos Belum Dialokasi</label>
                <asp:CheckBox ID="cbUnZipCode" runat="server"></asp:CheckBox>
	        </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonSearchDetail" runat="server"  Text="Search" CssClass ="small button blue"
                CausesValidation="False"></asp:Button>&nbsp;
            <asp:Button ID="ButtonResetDetail" runat="server"  Text="Reset" CssClass ="small button gray"
                CausesValidation="False"></asp:Button>
	    </div>        
        </asp:Panel>
        <asp:Panel ID="pnlList" runat="server">
        <div class="form_box_title">
            <div class="form_single">              
                <h4>
                    DAFTAR KODE POS
                </h4>
            </div>
        </div>        
        <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgInqZipCode" runat="server" Width="100%"
                        CssClass="grid_general" BorderStyle="None" BorderWidth="0" OnSortCommand="Sorting" AllowSorting="True" AutoGenerateColumns="False">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:BoundColumn DataField="ZIPCODE" SortExpression="ZIPCODE" HeaderText="KODE POS">                                
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="KELURAHAN" SortExpression="KELURAHAN" HeaderText="KELURAHAN">                                
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="KECAMATAN" SortExpression="KECAMATAN" HeaderText="KECAMATAN">                                
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="CGNAME" SortExpression="CGNAME" HeaderText="COLLECTION GROUP">                                
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="CNAME" SortExpression="CNAME" HeaderText="NAMA COLLECTOR">                                
                            </asp:BoundColumn>
                        </Columns>
                </asp:DataGrid>
                <div class="button_gridnavigation">
	                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
		                CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
		                CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
		                CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
		                CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
	                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
		                EnableViewState="False"></asp:Button>
	                <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage"
				        MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer"
				        CssClass="validator_general"></asp:RangeValidator>
			        <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
				        ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general">
			        </asp:RequiredFieldValidator>              
                </div>
                <div class="label_gridnavigation">
	                <asp:Label ID="lblPage" runat="server"></asp:Label>of
	                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
	                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                </div>
            </div>   
        </div>     
        </div>    
        <div class="form_button">
            <asp:Button ID="ButtonBack" runat="server"  Text="Back" CssClass ="small button gray"
            CausesValidation="False"></asp:Button>
	    </div>               
    </asp:Panel>
    </form>
</body>
</html>
