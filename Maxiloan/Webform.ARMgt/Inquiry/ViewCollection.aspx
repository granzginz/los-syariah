﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewCollection.aspx.vb"
    Inherits="Maxiloan.Webform.ARMgt.ViewCollection" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ViewCollection</title>
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';			
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;

        function OpenRalHist(pStyle, pApplicationID, pBranchID, pCustomerID, pCustomerName, pAgreementNO) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.ARMgt/Inquiry/RALHistory.aspx?Style=' + pStyle + '&ApplicationID=' + pApplicationID + '&BranchID=' + pBranchID + '&CustomerID=' + pCustomerID + '&CustomerName=' + pCustomerName + '&AgreementNO=' + pAgreementNO, 'RALHISTORY', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0,scrollbars=1');
        }
        function OpenReceiptNotesView(pStyle, pApplicationID, pBranchID, pCustomerID, pCustomerName, pAgreementNO) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.ARMgt/Inquiry/ReceiptNotesView.aspx?Style=' + pStyle + '&ApplicationID=' + pApplicationID + '&BranchID=' + pBranchID + '&CustomerID=' + pCustomerID + '&CustomerName=' + pCustomerName + '&AgreementNO=' + pAgreementNO, 'RECEIPTNOTES', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0,scrollbars=1');
        }
        function OpenActivity(pStyle, pApplicationID, pBranchID, pCustomerID, pCustomerName, pAgreementNO) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.ARMgt/Inquiry/InqCollActivityResult.aspx?Style=' + pStyle + '&ApplicationID=' + pApplicationID + '&BranchID=' + pBranchID + '&CustomerID=' + pCustomerID + '&CustomerName=' + pCustomerName + '&AgreementNO=' + pAgreementNO, 'OPENACTIVITY', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0,scrollbars=1');
        }
        function OpenCollection(pStyle, pApplicationID, pBranchID, pCustomerID, pCustomerName, pAgreementNO) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.ARMgt/Inquiry/InqCollActivityHistory.aspx?Style=' + pStyle + '&ApplicationID=' + pApplicationID + '&BranchID=' + pBranchID + '&CustomerID=' + pCustomerID + '&CustomerName=' + pCustomerName + '&AgreementNO=' + pAgreementNO, 'RECEIPTNOTES', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0,scrollbars=1');
        }
        function OpenInsurance(pStyle, pApplicationID, pBranchID) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.Insurance/ViewInsuranceDetail.aspx?Style=' + pStyle + '&ApplicationID=' + pApplicationID + '&BranchID=' + pBranchID, 'OPENINSURANCE', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0,scrollbars=1');
        }
        function fClose() {
            window.close();
            return false;
        }	
    </script>
</head>
<body>
    <input id="hdnAgreement" type="hidden" name="hdnAgreement" runat="server" />
    <input id="hdnCustName" type="hidden" name="hdnCustName" runat="server" />
    <input id="hdnCustID" type="hidden" name="hdnCustID" runat="server" />
    <input id="hdnBranchID" type="hidden" name="hdnBranchID" runat="server" />
    <input id="hdnApplicationID" type="hidden" name="hdnApplicationID" runat="server" />
    <form id="form1" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">           
                <h3>
                    VIEW - COLLECTION
                </h3>
            </div>
        </div>  
        <asp:Panel ID="PnlListDetail" runat="server">
        <div class="form_box">	        
                <div class="form_left">
                    <label>No Kontrak</label>
                    <asp:HyperLink ID="lblAgreementNo" runat="server"></asp:HyperLink>
		        </div>
		        <div class="form_right">	
                    <label>Nama Customer</label>	
                    <asp:HyperLink ID="lblCustomerName" runat="server"></asp:HyperLink>	
		        </div>	        
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <label>Alamat</label>
                    <asp:Label ID="lblAddress" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">			
                    <label>Kode Pos</label>
                    <asp:Label ID="lblZipCode" runat="server"></asp:Label>
		        </div>	        
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Nama Asset</label>
                <asp:Label ID="lblAsset" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Collection Group</label>
                <asp:Label ID="lblCollectionGroup" runat="server"></asp:Label>
	        </div>
        </div>
         <div class="form_box">	        
                <div class="form_left">
                    <label>Collector</label>
                    <asp:Label ID="lblCollector" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">	
                    <label>Supervisor</label>		
                    <asp:Label ID="lblSupervisor" runat="server"></asp:Label>
		        </div>	        
        </div>
         <div class="form_box">	        
                <div class="form_left">
                    <label>Status Contract</label>
                    <asp:Label ID="lblContractStatus" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">		
                    <label>Status Asset</label>	
                    <asp:Label ID="lblAssetStatus" runat="server"></asp:Label>
		        </div>	        
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <label>Status Collection</label>
                    <asp:Label ID="lblCollStatus" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">	
                    <label>Ex Tarikan</label>	
                    <asp:Label ID="lblExRepo" runat="server"></asp:Label>	
		        </div>	        
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <label>Fixed Assign</label>
                    <asp:Label ID="lblFixedAssign" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">	
                    <%--<label>tanggal Rencana SKT</label>		--%>
                    <label>tanggal Rencana SKE</label>		
                    <asp:Label ID="lblPlanDateToRAL" runat="server"></asp:Label>
		        </div>	        
        </div>        
        <div class="form_box">	        
                <div class="form_left">
                    <label>Tanggal Rencana</label>
                    <asp:Label ID="lblPlanDate" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">		
                    <label>Rencana Kegiatan</label>	
                    <asp:Label ID="lblPlanActivity" runat="server"></asp:Label>
		        </div>	        
        </div>
        <div class="form_box_header">
            <div class="form_single">              
                <h4>
                    TUNGGAKAN &amp; DATA FINANCIAL
                </h4>
            </div>
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <label>Jumlah Hari Telat</label>
                    <asp:Label ID="lblDaysoverdue" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">		
                    <label>Jumlah Tunggakan</label>	
                    <asp:Label ID="lblOverdueAmount" runat="server"></asp:Label>
		        </div>	        
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <label>Angsuran Ke</label>
                    <asp:Label ID="lblInstallmentNo" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">			
                    <label>Tanggal Angsuran</label>
                    <asp:Label ID="lblInstallmentDate" runat="server"></asp:Label>
		        </div>	        
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <label>Jumlah Angsuran</label>
                    <asp:Label ID="lblInstallmentAmount" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">			
		        </div>	        
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <label>Sisa A/R</label>
                    <asp:Label ID="lblOSBalance" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">	
                    <label>Asuransi</label>		
                    <asp:Label ID="lblOSInsurance" runat="server"></asp:Label>
		        </div>	        
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <label>Biaya Tagih</label>
                    <asp:Label ID="lblOSCollectionFee" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">		
                    <label>Denda Keterlambatan Angsuran</label>	
                    <asp:Label ID="lblOSLateCharges" runat="server"></asp:Label>
		        </div>	        
        </div>
        <div class="form_box_header">
            <div class="form_single">              
                <h4>
                    <%--SKT (SURAT KUASA TARIK)--%>
                    SKE (SURAT KUASA EKSEKUSI)
                </h4>
            </div>
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <%--<label>No SKT</label>--%>
                    <label>No SKE</label>
                    <asp:Label ID="lblRalNo" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">	
                    <%--<label>Tanggal SKT</label>		--%>
                    <label>Tanggal SKE</label>		
                    <asp:Label ID="lblRalDate" runat="server"></asp:Label>
		        </div>	        
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Eksekutor</label>
                <asp:Label ID="lblExecutor" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <asp:HyperLink ID="hyRalHist" runat="server">RAL Hist</asp:HyperLink>&nbsp;|&nbsp;
                <asp:HyperLink ID="hyCollActivity" runat="server">Collection Activity</asp:HyperLink>&nbsp;|&nbsp;
                <asp:HyperLink ID="hyCollEvent" runat="server">Collection Event</asp:HyperLink>&nbsp;|&nbsp;
                <asp:HyperLink ID="hyInsurance" runat="server">Insurance</asp:HyperLink>&nbsp;|&nbsp;
                <asp:HyperLink ID="hyReceiptNotes" runat="server">ReceiptNotes</asp:HyperLink>
	        </div>
        </div>
        <div class="form_button">            
        <asp:Button ID="ButtonClose" OnClientClick="fClose();" runat="server" CausesValidation="false" Text="Close" CssClass ="small button gray">
        </asp:Button>     
    </div>   
    </asp:Panel>
    </form>
</body>
</html>
