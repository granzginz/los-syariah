﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="InqAssetInventory.aspx.vb"
    Inherits="Maxiloan.Webform.ARMgt.InqAssetInventory" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcBranchCollection" Src="../../Webform.UserController/UcBranchCollection.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>InqAssetInventory</title>
    <script src="../../Maxiloan.js" type="text/javascript" ></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <script language="javascript" type="text/javascript" >
        var x = screen.width;
        var y = screen.height - 100;

        function fConfirm() {
            if (window.confirm("Apakah yakin mau hapus data ini ? "))
                return true;
            else
                return false;
        }
        function fback() {
            history.go(-1);
            return false;
        }

        function OpenViewCollector(pCollID, pCGID) {
            var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            window.open('http://<%#Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.ARMgt/Setting/CollectorView.aspx?CollectorID=' + pCollID + '&CGID=' + pCGID, 'CollectorView', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenWindowActivity(pAgreementNo, pCustomerID, pCustomerName) {
            var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            window.open('http://<%#Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.ARMgt/Inquiry/InqCollActivityResult.aspx?AgreementNo=' + pAgreementNo + '&CustomerID=' + pCustomerID + '&CustomerName=' + pCustomerName + '&Style=Collection&referrer=CollActivity', 'CollectionActivity', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenWindowHistory(pAgreementNo, pCustomerID, pCustomerName) {
            var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            window.open('http://<%#Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.ARMgt/Inquiry/InqCollActivityHistory.aspx?AgreementNo=' + pAgreementNo + '&CustomerID=' + pCustomerID + '&CustomerName=' + pCustomerName, 'CollectionActivity', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenViewAgreement(pAgreementNo) {
            var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            window.open('http://<%#Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.LoanOrg/Credit/ViewStatementofAccount.aspx?AgreementNo=' + pAgreementNo + '&Style=Collection', 'AgreementView', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenViewCustomer(pCustomerID) {
            var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            window.open('http://<%#Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.LoanOrg/Credit/ViewPersonalCustomer.aspx?CustomerID=' + pCustomerID + '&Style=Collection', 'CustomerView', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>    
        <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">           
                <h3>
                    INVENTORY ASSET
                </h3>
            </div>
        </div>  
        <asp:Panel ID="PnlSearch" runat="server">        
        <div class="form_box">
	        <div class="form_single">
                <label>Collection Group</label>
                <uc1:UcBranchCollection id="oCG" runat="server"></uc1:UcBranchCollection>	        
            </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Cari Berdasarkan</label>
                <asp:DropDownList ID="cboSearchBy" runat="server">
                    <asp:ListItem Value="0" Selected="True">Select One</asp:ListItem>
                    <asp:ListItem Value="AgreementNo">No Kontrak</asp:ListItem>
                    <asp:ListItem Value="CustomerName">Nama Customer</asp:ListItem>
                </asp:DropDownList>
                &nbsp;
                <asp:TextBox ID="txtSearchByValue" runat="server" ></asp:TextBox>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label class ="label_req">Tanggal dicari </label>
                <asp:DropDownList ID="Searchdate" runat="server">
                    <asp:ListItem Value="0" Selected="True">Select One</asp:ListItem>
                    <asp:ListItem Value="RepossesDate">Tanggal Tarik</asp:ListItem>
                    <asp:ListItem Value="InventoryDate">Tanggal Inventory</asp:ListItem>
                </asp:DropDownList>

                
                <asp:TextBox ID="txtValidDateFrom" runat="server"></asp:TextBox>                   
                <asp:CalendarExtender ID="txtValidDateFrom_CalendarExtender" runat="server" 
                    Enabled="True" TargetControlID="txtValidDateFrom" Format ="dd/MM/yyyy"></asp:CalendarExtender>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*" CssClass="validator_general"
                ControlToValidate="txtValidDateFrom"></asp:RequiredFieldValidator>&nbsp;S/D&nbsp;
                
                <asp:TextBox ID="txtValidDateTo" runat="server"></asp:TextBox>                   
                <asp:CalendarExtender ID="txtValidDateTo_CalendarExtender" runat="server" 
                    Enabled="True" TargetControlID="txtValidDateTo" Format ="dd/MM/yyyy"></asp:CalendarExtender>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*" CssClass="validator_general"
                ControlToValidate="txtValidDateTo"></asp:RequiredFieldValidator>
	        </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonSearch" runat="server"  Text="Search" CssClass ="small button blue"
                Enabled="True"></asp:Button>&nbsp;
            <asp:Button ID="ButtonReset" runat="server"  Text="Reset" CssClass ="small button gray"
                Enabled="True" CausesValidation="False"></asp:Button>
	    </div>
        </asp:Panel>    
        <asp:Panel ID="pnlList"  runat="server">
        <div class="form_box_title">
            <div class="form_single">              
                <h4>
                    DAFTAR INVENTORY ASSET
                </h4>
            </div>
        </div>
        <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtg" runat="server" Width="100%" OnSortCommand="Sorting" CssClass="grid_general" BorderStyle="None" BorderWidth="0" AutoGenerateColumns="False"
                        AllowSorting="True">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn SortExpression="AGREEMENTNO" HeaderText="NO KONTRAK">                                
                                <ItemTemplate>
                                    <asp:LinkButton runat="server" Enabled="True" ID="hpAgreementNo">
												<%# container.dataitem("AgreementNo") %>
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="CUSTOMERNAME" HeaderText="NAMA CUSTOMER">                                
                                <ItemTemplate>
                                    <asp:LinkButton runat="server" Enabled="True" ID="hpCustomerName">
												<%# container.dataitem("CustomerName") %>
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn Visible="False" DataField="AgreementNo"></asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="CustomerID"></asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="CustomerName"></asp:BoundColumn>
                            <asp:BoundColumn DataField="AssetDesc" SortExpression="AssetDesc" HeaderText="NAMA ASSET">                                
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="REPOSSESSDATE" SortExpression="REPOSSESSDATE" HeaderText="TGL TARIK">                                
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="INVENTORYDATE" SortExpression="INVENTORYDATE" HeaderText="TGL INVENTORY">                                
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="INVAMOUNT" SortExpression="INVAMOUNT" HeaderText="NILAI INVENTORY">                                
                            </asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="DETAIL">                                
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbDetail" runat="server" ImageUrl="../../images/icondetail.gif"
                                        CommandName="Detail"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                </asp:DataGrid>        
                <div class="button_gridnavigation">
	                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
		                CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
		                CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
		                CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
		                CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
	                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
		                EnableViewState="False"></asp:Button>
	                <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage"
				        MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer"
				        CssClass="validator_general"></asp:RangeValidator>
			        <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
				        ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general">
			        </asp:RequiredFieldValidator>              
                </div>
                <div class="label_gridnavigation">
	                <asp:Label ID="lblPage" runat="server"></asp:Label>of
	                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
	                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                </div>
            </div> 
        </div>        
        </div>  
    </asp:Panel>           
    </form>
</body>
</html>
