﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="RALInquiry.aspx.vb" Inherits="Maxiloan.Webform.ARMgt.RALInquiry" Culture = "en-GB" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>RALInquiry</title>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />

    <script language="javascript" type="text/javascript">
        var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%#Request.servervariables("SERVER_NAME")%>/';
    </script>
    <script language="javascript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;

        function OpenWinCollector(pCollID, pCGID) {
            window.open('../Setting/CollectorView.aspx?CGID=' + pCGID + '&CollectorID=' + pCollID + '', 'UserLookup', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>    
        <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">           
                <h3>
                    <%--INQUIRY SKT (SURAT KUASA TARIK)--%>
                    INQUIRY SKE (SURAT KUASA EKSEKUSI)
                </h3>
            </div>
        </div>  
        <asp:Panel ID="pnlsearch" runat="server">
        <div class="form_box">
	        <div class="form_single">
                <label>Collection Group</label>
                <asp:DropDownList ID="cboCG" runat="server" />
	        </div>
        </div>
         <div class="form_box">
	        <div class="form_single">
                <label>Eksekutor Type</label>
                <asp:DropDownList ID="cboExecutorType" runat="server" autopostback="true"> 
                    <asp:ListItem Value="">Select One</asp:ListItem>
                    <asp:ListItem Value="EP">Eksternal</asp:ListItem>
                    <asp:ListItem Value="EI">Internal</asp:ListItem> 
                </asp:DropDownList>
	        </div>
        </div> 
        <div class="form_box">
	        <div class="form_single">
                <label>Eksekutor</label>
                <asp:DropDownList ID="cboExecutor" runat="server" />
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Cari Berdasarkan</label>
                <asp:DropDownList ID="cboSearchBy" runat="server">
                    <asp:ListItem Value="">Select One</asp:ListItem>
                    <asp:ListItem Value="Agreement.AgreementNo">No Kontrak</asp:ListItem>
                    <asp:ListItem Value="Customer.Name">Nama Customer</asp:ListItem>
                    <%--<asp:ListItem Value="RAL.RALNo">No SKT</asp:ListItem>--%>
                    <asp:ListItem Value="RAL.RALNo">No SKE</asp:ListItem>
                </asp:DropDownList>
                &nbsp;
                <asp:TextBox ID="txtSearchBy" runat="server" />
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <%--<label class ="label_req"> Tanggal Mulai SKT</label>                --%>
                <label class ="label_req"> Tanggal Mulai SKE</label>
                <asp:TextBox ID="txtRALDate1" runat="server" />
                <asp:CalendarExtender ID="txtRALDate1_CalendarExtender" runat="server"  Enabled="True" TargetControlID="txtRALDate1" Format ="dd/MM/yyyy" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*" CssClass="validator_general" ControlToValidate="txtRALDate1" />
                &nbsp;S/D&nbsp;                
                <asp:TextBox ID="txtRALDate2" runat="server"></asp:TextBox>                   
                <asp:CalendarExtender ID="txtRALDate2_CalendarExtender" runat="server"  Enabled="True" TargetControlID="txtRALDate2" Format ="dd/MM/yyyy" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*" CssClass="validator_general" ControlToValidate="txtRALDate2" />
                <asp:CompareValidator ID="cmpVal1" ControlToCompare="txtRALDate1"  ControlToValidate="txtRALDate2" Type="Date" Operator="GreaterThanEqual"    ErrorMessage="*Invalid Data" runat="server" />
	        </div>
        </div>        
        <div class="form_box">
	        <div class="form_single">
                <%--<label>Status SKT</label>--%>
                <label>Status SKE</label>
                <asp:DropDownList ID="cboRALStatus" runat="server">
                    <asp:ListItem Value="">Select One</asp:ListItem>
                    <asp:ListItem Value="OP">Open</asp:ListItem>
                    <asp:ListItem Value="CL">Close</asp:ListItem>
                    <asp:ListItem Value="RL">Release</asp:ListItem>
                </asp:DropDownList>
	        </div>
        </div>
        <div class="form_button">
            <asp:Button ID="Buttonsearch" runat="server"  Text="Search" CssClass ="small button blue" />&nbsp;
            <asp:Button ID="ButtonReset" runat="server"  Text="Reset" CssClass ="small button gray" CausesValidation="False" />
	    </div>       
        </asp:Panel>    
        <asp:Panel ID="pnlDtGrid" runat="server">
        <div class="form_box_title">
            <div class="form_single">              
                <%--<h4>DAFTAR SKT (SURAT KUASA TARIK)</h4>--%>
                <h4>DAFTAR SKE (SURAT KUASA EKSEKUSI)</h4>
            </div>
        </div>
        <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgRALInquiry" runat="server" Width="100%" OnSortCommand="SortGrid"
                        CssClass="grid_general" BorderStyle="None" BorderWidth="0"
                        DataKeyField="AgreementNo" AutoGenerateColumns="False" AllowSorting="True">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="hypAgreementNodtg" runat="server" Text='<%# Container.Dataitem("AgreementNo")%>' />
                                    <asp:Label ID="lblApplicationId" Visible="False" runat="server" Text='<%# Container.Dataitem("ApplicationId")%>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="CustomerName" HeaderText="NAMA CUSTOMER">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="hypCustomerNamedtg" runat="server" Text='<%# Container.Dataitem("CustomerName")%>' />
                                    <asp:Label ID="lblCustomerId" Visible="False" runat="server" Text='<%#Container.DataItem("CustomerID")%>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <%--<asp:BoundColumn DataField="RALNO" SortExpression="RALNO" HeaderText="NO SKT" />--%>
                            <asp:BoundColumn DataField="RALNO" SortExpression="RALNO" HeaderText="NO SKE" />
                            <%--<asp:BoundColumn DataField="RALPERIOD" SortExpression="RALPERIOD" HeaderText="PERIODE SKT" />--%>
                            <asp:BoundColumn DataField="RALPERIOD" SortExpression="RALPERIOD" HeaderText="PERIODE SKE" />
                            <asp:TemplateColumn SortExpression="EXECUTORID" HeaderText="EKSEKUTOR">                                
                                <ItemTemplate>
                                    <a href="javascript:OpenWinCollector('<%#Container.DataItem("ExecutorID")%>','<%#Container.dataItem("CGID")%>');">
                                        <asp:Label ID="lblExecutorID" runat="server" Text='<%# Container.DataItem("ExecutorID")%>' /></a>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="ExecutorType" SortExpression="ExecutorType" HeaderText="TIPE EKSEKUTOR" />
                            <%--<asp:BoundColumn DataField="RALSTATUS" SortExpression="RALSTATUS" HeaderText="STATUS SKT" />--%>
                            <asp:BoundColumn DataField="RALSTATUS" SortExpression="RALSTATUS" HeaderText="STATUS SKE" />
                            <asp:BoundColumn DataField="REPOSSESS" SortExpression="REPOSSESS" HeaderText="TARIK" />
                            <asp:BoundColumn DataField="ContractStatus" SortExpression="ContractStatus" HeaderText="STATUS KONTRAK" />
                            <asp:TemplateColumn HeaderText="HISTORY PERPANJANGAN">                                
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbHistory" runat="server" CommandName="History" ImageUrl="../../images/icondocument.gif" />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>                        
                </asp:DataGrid>   
                  <uc2:ucGridNav id="GridNavigator" runat="server"/>     
            </div>  
        </div>        
        </div> 
    </asp:Panel>          
    </form>
</body>
</html>
