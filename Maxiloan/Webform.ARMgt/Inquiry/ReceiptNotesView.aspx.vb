﻿#Region "Imports"
Imports System.Text
Imports System.Threading
Imports System.Data.SqlClient
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.General
Imports Maxiloan.Webform.UserController
#End Region

Public Class ReceiptNotesView
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"

    Private m_controller As New ReceiptNotesViewController
    Private oReceiptNotes As New Parameter.ReceiptNotesView

    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region
#Region "properties"
    Public Property AgreementNo() As String
        Get
            Return CType(viewstate("AgreementNo"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AgreementNo") = Value
        End Set
    End Property
    Public Property CustomerName() As String
        Get
            Return CType(viewstate("CustomerName"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerName") = Value
        End Set
    End Property
    Public Property CustomerID() As String
        Get
            Return CType(viewstate("CustomerID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerID") = Value
        End Set
    End Property
    Private Property AppliactionID() As String
        Get
            Return CType(viewstate("AppliactionID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AppliactionID") = Value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here

        If Not Me.IsPostBack Then
            txtpage.Text = "1"
            Me.FormID = "CollViewRN"

            Me.FormID = "CollRALHistory"
            Me.AppliactionID = Request.QueryString("ApplicationID")
            Me.BranchID = Request.QueryString("BranchID")
            Me.CustomerID = Request.QueryString("CustomerID")
            Me.CustomerName = Request.QueryString("CustomerName")
            Me.AgreementNo = Request.QueryString("AgreementNo")
            lblAgreementNo.Text = Me.AgreementNo
            lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "Collection" & "', '" & Me.AppliactionID.Trim & "')"

            If CheckForm(Me.Loginid, Me.FormID, "Maxiloan") Then
                Me.SearchBy = "ApplicationId= " & "'" & Me.AppliactionID & "'" & " and BranchID= " & "'" & Me.BranchID & "'"

                Me.SortBy = ""
                BindGridEntity()
            Else
                Dim strHTTPServer As String
                Dim strHTTPApp As String
                Dim strNameServer As String
                strHTTPServer = Request.ServerVariables("PATH_INFO")
                strNameServer = Request.ServerVariables("SERVER_NAME")
                strHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                Response.Redirect("http://" & strNameServer & "/" & strHTTPApp & "/NotAuthorized.html")
            End If
        End If

    End Sub

#Region "Navigation "
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridEntity()
    End Sub
#End Region
#Region "Paging Footer"

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            lblTotPage.Text = "1"
            'rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()

        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
#End Region
#Region "Go Page"
    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                BindGridEntity()
            End If
        End If
    End Sub

#End Region
#Region "Sorting"

    Public Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        BindGridEntity()
    End Sub

#End Region
    Sub BindGridEntity()
        With oReceiptNotes
            .WhereCond = Me.SearchBy
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = Me.SortBy
            .strConnection = GetConnectionString
        End With
        oReceiptNotes = m_controller.ReceiptNotesViewPaging(oReceiptNotes)
        recordCount = oReceiptNotes.TotalRecord
        dtgReceiptNotes.DataSource = oReceiptNotes.ListData
        dtgReceiptNotes.CurrentPageIndex = 0
        dtgReceiptNotes.DataBind()
        dtgReceiptNotes.Visible = True
        PagingFooter()
    End Sub

End Class