﻿#Region "Imports"
Imports System.Text
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class inqBASTK
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oSearchBy As UcSearchBy

#Region "Property"
    Private Property cmdwhere() As String
        Get
            Return CType(viewstate("cmdwhere"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("cmdwhere") = Value
        End Set
    End Property

    Private Property FilterBy() As String
        Get
            Return CType(viewstate("FilterBy"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("FilterBy") = Value
        End Set
    End Property

#End Region

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.AgreementList
    Private oController As New CollZipCodeController
    Private Dcontroller As New DataUserControlController
#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            PopupReport()

            Me.FormID = "INQAGREELIST"
            oSearchBy.ListData = "Agreementno, Agreement No-Name, Name-LicensePlate, License Plate-InstallmentAmount, Installment Amount-SerialNo1, Chassis No-SerialNo2,EngineNo"
            oSearchBy.BindData()

            Dim dtbranch As New DataTable
            '  dtCG = oController.GetCollectionGroupCombo(GetConnectionString)
            With cboCG
                .DataSource = oController.GetCollectionGroupCombo(GetConnectionString)
                .DataValueField = "cgid"
                .DataTextField = "cgname"
                .DataBind()
                .Items.Insert(0, "ALL")
                .Items(0).Value = "ALL"
            End With


            With cboBranch

                .DataSource = Dcontroller.GetBranchAll(GetConnectionString)
                .DataValueField = "ID"
                .DataTextField = "Name"
                .DataBind()
                .Items.Insert(0, "ALL")
                .Items(0).Value = "ALL"
            End With

            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.SearchBy = ""
                Me.SortBy = ""
            End If
        End If
    End Sub

#End Region

#Region "DoBind"
    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        Dim intloop As Integer
        Dim hypID As HyperLink
        Dim oPaging As New Parameter.GeneralPaging
        Dim m_controller As New GeneralPagingController

        With oPaging
            .strConnection = GetConnectionString
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
            .SpName = "spCollAgreementListPaging"
        End With

        oPaging = m_controller.GetGeneralPaging(oPaging)
        If Not oPaging Is Nothing Then
            DtUserList = oPaging.ListData
            recordCount = oPaging.TotalRecords
        Else
            recordCount = 0
        End If
        DtgAgree.DataSource = DtUserList.DefaultView
        DtgAgree.CurrentPageIndex = 0
        DtgAgree.DataBind()
        PagingFooter()

        pnlDatagrid.Visible = True        
    End Sub

#End Region

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then            
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub

#End Region

#Region "databound"
    Private Sub DtgAgree_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgree.ItemDataBound
        Dim lblTemp As Label
        Dim lbbranch As Label
        Dim hyagreementno As HyperLink
        Dim lblcustomerid As Label
        Dim hyTemp As HyperLink

        If sessioninvalid() Then
            Exit Sub
        End If
        Dim m As Int32
        Dim hypCancel As HyperLink
        Dim inHyView As HyperLink
        Dim hyApplicationId As HyperLink
        Dim hycustomername As HyperLink
        Dim lblInStatus As Label
        Dim inlblSeqNo As New Label
        Dim instyle As String = "AccMnt"

        If e.Item.ItemIndex >= 0 Then
            hyagreementno = CType(e.Item.FindControl("hyAgreementNo"), HyperLink)
            hycustomername = CType(e.Item.FindControl("hyCustomerName"), HyperLink)
            lblcustomerid = CType(e.Item.FindControl("lblCustomerId"), Label)
            lblInStatus = CType(e.Item.FindControl("lblStatus"), Label)
            lbbranch = CType(e.Item.FindControl("lbranchid"), Label)
            lblTemp = CType(e.Item.FindControl("lblCustomerId"), Label)
            hyTemp = CType(e.Item.FindControl("hyApplicationId"), HyperLink)
            hyApplicationId = CType(e.Item.FindControl("hyApplicationId"), HyperLink)
            inHyView = CType(e.Item.FindControl("HyView"), HyperLink)
            inlblSeqNo = CType(e.Item.FindControl("lblSeqNO"), Label)

            '*** Customer Link
            'lblTemp = CType(e.Item.FindControl("lblCustomerId"), Label)
            hyTemp = CType(e.Item.FindControl("hyCustomerName"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(lblTemp.Text.Trim) & "')"
            '*** Agreement No link
            hyTemp = CType(e.Item.FindControl("hyAgreementNo"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(hyApplicationId.Text.Trim) & "')"
            '*** ApplicationId link
            hyTemp = CType(e.Item.FindControl("hyApplicationId"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenApplicationId('" & "AccMnt" & "', '" & Server.UrlEncode(hyTemp.Text.Trim) & "')"
            '*** ViewCollectinId link
            hyTemp = CType(e.Item.FindControl("hyviewcollection"), HyperLink)
            hyTemp.NavigateUrl = "../../Webform.LoanOrg/Credit/Print/BASTKViewer.aspx?ApplicationID=" & hyApplicationId.Text.Trim & "&BranchID=" & lbbranch.Text.Trim
        End If
    End Sub
#End Region

#Region "Reset"

    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        Server.Transfer("InqAgreementList.aspx")
    End Sub
#End Region

#Region "Search"
    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Buttonsearch.Click
        If SessionInvalid() Then
            Exit Sub
        End If
        If CheckFeature(Me.Loginid, Me.FormID, "SRCH", Me.AppId) Then
            Dim strSearch As New StringBuilder
            Dim strFilter As New StringBuilder
            If cboBranch.SelectedItem.Value <> "ALL" Then
                strSearch.Append("  agr.branchid = '" & cboBranch.SelectedItem.Value & "'")
            End If
            If cboCG.SelectedItem.Value <> "ALL" Then
                If strSearch.ToString <> "" Then
                    strSearch.Append(" and  coll.CGID = '" & cboCG.SelectedItem.Value & "' ")
                Else
                    strSearch.Append("   coll.CGID = '" & cboCG.SelectedItem.Value & "' ")
                End If
            End If
            If oSearchBy.Text.Trim <> "" Then
                If strSearch.ToString <> "" Then
                    If Right(oSearchBy.Text.Trim, 1) = "%" Then
                        strSearch.Append(" and " & oSearchBy.ValueID & " like '" & oSearchBy.Text.Trim.Replace("'", "''") & "' ")
                    Else
                        strSearch.Append(" and " & oSearchBy.ValueID & " = '" & oSearchBy.Text.Trim.Replace("'", "''") & "' ")
                    End If
                Else
                    If Right(oSearchBy.Text.Trim, 1) = "%" Then
                        strSearch.Append(oSearchBy.ValueID & " like '" & oSearchBy.Text.Trim.Replace("'", "''") & "' ")
                    Else
                        strSearch.Append(oSearchBy.ValueID & " = '" & oSearchBy.Text.Trim.Replace("'", "''") & "' ")
                    End If
                End If
            End If

            Me.SearchBy = strSearch.ToString

            pnlDatagrid.Visible = True
            DtgAgree.Visible = True

            DoBind(Me.SearchBy, Me.SortBy)

        End If
    End Sub
#End Region

#Region "Sort"
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

    Sub PopupReport()
        If Request.QueryString("strFileLocation") <> "" Then
            Dim strHTTPServer As String
            Dim StrHTTPApp As String
            Dim strNameServer As String
            Dim strFileLocation As String

            strHTTPServer = Request.ServerVariables("PATH_INFO")
            strNameServer = Request.ServerVariables("SERVER_NAME")
            StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
            strFileLocation = "http://" & strNameServer & "/" & StrHTTPApp & "/XML/" & Request.QueryString("strFileLocation") & ".pdf"
            Response.Write("<script language = javascript>" & vbCrLf _
            & "window.open('" & strFileLocation & "','accacq', 'left=0, top=0, menubar=0') " & vbCrLf _
            & "</script>")

        End If
    End Sub

End Class