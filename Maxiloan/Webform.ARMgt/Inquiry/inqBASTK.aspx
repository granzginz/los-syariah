﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="inqBASTK.aspx.vb" Inherits="Maxiloan.Webform.ARMgt.inqBASTK" %>

<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../../Webform.UserController/UcSearchBy.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>inqBASTK</title>
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">           
                <h3>
                    CARI KONTRAK
                </h3>
            </div>
        </div>        
        <div class="form_box">
	        <div class="form_single">
                <label>Collection Group</label>
                <asp:DropDownList ID="cboCG" runat="server">
                </asp:DropDownList>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Cabang</label>
                <asp:DropDownList ID="cboBranch" runat="server">
                </asp:DropDownList>
	        </div>
        </div>
        <div class="form_box_uc">
                <uc1:ucsearchby id="oSearchBy" runat="server">
                </uc1:ucsearchby>	        
        </div>        
        <div class="form_button">
            <asp:Button ID="Buttonsearch" runat="server"  Text="Search" CssClass ="small button blue">
            </asp:Button>&nbsp;
            <asp:Button ID="ButtonReset" runat="server"  Text="Reset" CssClass ="small button gray"
                CausesValidation="False"></asp:Button>
	    </div>        
        <asp:Panel ID="pnlDatagrid" runat="server" Visible="false">
        <div class="form_box_title">
            <div class="form_single">              
                <h4>
                    DAFTAR KONTRAK BASTK (BERITA ACARA SERAH TERIMA ASSET YANG DIBIAYAI)
                </h4>
            </div>
        </div>
        <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="DtgAgree" runat="server" Width="100%" AllowSorting="True" OnSortCommand="SortGrid"
                        AutoGenerateColumns="False" DataKeyField="Applicationid"
                        CssClass="grid_general" BorderStyle="None" BorderWidth="0" Visible="False">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn SortExpression="DETAIL" HeaderText="PILIH">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyviewcollection" runat="server" Text="BASTK"></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" SortExpression="Branchid" HeaderText="BRANCH ID">                                
                                <ItemTemplate>
                                    <asp:Label ID="lbranchid" runat="server" Text='<%#Container.DataItem("branchid")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" SortExpression="ApplicationID" HeaderText="APPLICATION ID">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyApplicationId" runat="server" Text='<%#Container.DataItem("ApplicationID")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Name" HeaderText="NAMA CUSTOMER">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyCustomerName" runat="server" Text='<%#Container.DataItem("Name")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Address" HeaderText="ALAMAT">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblAddress" runat="server" Text='<%#Container.DataItem("Address")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AssetDesc" HeaderText="NAMA ASSET">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblAsset" runat="server" Text='<%#Container.DataItem("AssetDesc")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="LicensePlate" HeaderText="NO POLISI">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblLicPlate" runat="server" Text='<%#Container.DataItem("LicensePlate")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="InstallmentAmount" HeaderText="JUMLAH ANGSURAN">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblInstallmentAmount" runat="server" Text='<%#formatnumber(Container.DataItem("InstallmentAmount"),2)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="contractstatus" HeaderText="STATUS KONTRAK">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblcontractstatus" runat="server" Text='<%#Container.DataItem("contractstatus")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="CGName" HeaderText="CG">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblcg" runat="server" Text='<%#Container.DataItem("CGName")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" HeaderText="CUSTOMER ID">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblCustomerId" runat="server" Text='<%#Container.DataItem("CustomerId")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" HeaderText="INS.DETAIL">                                
                                <ItemTemplate>
                                    <a href='ViewInsuranceDetail.aspx?ApplicationID=<%# DataBinder.eval(Container,"DataItem.ApplicationID") %>&pagesource=InquiryAgreement&BranchId=<%# DataBinder.eval(Container,"DataItem.BranchID") %>'>
                                        <asp:Image ID="imgRate" ImageUrl="../../images/iconinsurance.gif" runat="server">
                                        </asp:Image>
                                    </a>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>                        
                </asp:DataGrid>        
                <div class="button_gridnavigation">
	                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
		                CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
		                CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
		                CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
		                CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
	                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
		                EnableViewState="False"></asp:Button>
	                <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage"
				        MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer"
				        CssClass="validator_general"></asp:RangeValidator>
			        <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
				        ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general">
			        </asp:RequiredFieldValidator>              
                </div>
                <div class="label_gridnavigation">
	                <asp:Label ID="lblPage" runat="server"></asp:Label>of
	                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
	                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                </div>
            </div> 
        </div>     
        </div> 
    </asp:Panel>
    </form>
</body>
</html>
