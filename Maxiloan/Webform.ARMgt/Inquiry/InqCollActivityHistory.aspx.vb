﻿#Region "Imports"
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Webform.UserController
#End Region

Public Class InqCollActivityHistory
    Inherits Maxiloan.Webform.WebBased    

#Region " Private Const "
    Dim m_CollAct As New CollActivityController

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1    
    Private recordCount As Int64 = 1

#End Region

#Region " Property "
    Private Property ApplicationID() As String
        Get
            Return CStr(viewstate("CGApplicationIDID"))
        End Get
        Set(ByVal Value As String)
            viewstate("CGID") = Value
        End Set
    End Property
    Private Property CGID() As String
        Get
            Return CStr(viewstate("CGID"))
        End Get
        Set(ByVal Value As String)
            viewstate("CGID") = Value
        End Set
    End Property


    Private Property CGName() As String
        Get
            Return CStr(viewstate("CGName"))
        End Get
        Set(ByVal Value As String)
            viewstate("CGName") = Value
        End Set
    End Property

    Private Property CustomerID() As String
        Get
            Return CStr(viewstate("CustomerID"))
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerID") = Value
        End Set
    End Property

    Private Property CustomerName() As String
        Get
            Return CStr(viewstate("CustomerName"))
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerName") = Value
        End Set
    End Property

    Private Property AgreementNo() As String
        Get
            Return CStr(viewstate("AgreementNo"))
        End Get
        Set(ByVal Value As String)
            viewstate("AgreementNo") = Value
        End Set
    End Property


    Private Property CollectorID() As String
        Get
            Return CStr(viewstate("CollectorID"))
        End Get
        Set(ByVal Value As String)
            viewstate("CollectorID") = Value
        End Set
    End Property

    Private Property ActivityDate() As Date
        Get
            Return CType(viewstate("ActivityDate"), Date)
        End Get
        Set(ByVal Value As Date)
            viewstate("ActivityDate") = Value
        End Set
    End Property

#End Region

#Region "PAGE LOAD"
    Private Sub PanelAllFalse()        
        PnlList.Visible = False

    End Sub

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        'Put user code to initialize the page here
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not Page.IsPostBack Then
            Me.CGID = Me.GroubDbID
            Me.FormID = "INQCOLLDCR"

            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                InitialDefaultPanel()
                If Request.QueryString("ApplicationID") <> "" Then Me.ApplicationID = Request.QueryString("ApplicationID")
                If Request.QueryString("BranchID") <> "" Then Me.BranchID = Request.QueryString("BranchID")
                If Request.QueryString("AgreementNo") <> "" Then Me.AgreementNo = Request.QueryString("AgreementNo")
                If Request.QueryString("CustomerID") <> "" Then Me.CustomerID = Request.QueryString("CustomerID")
                If Request.QueryString("CustomerName") <> "" Then Me.CustomerName = Request.QueryString("CustomerName")

                hpAgreementNo.Text = Me.AgreementNo
                hpCustomerName.Text = Me.CustomerName
                hpCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "AccAcq" & "', '" & Me.CustomerID & "')"
                'hpAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "Collection" & "', '" & Me.ApplicationID.Trim & "')"

                Me.SearchBy = "all"
                Me.SortBy = ""

                BindGrid(Me.SearchBy, Me.SortBy)
                PnlList.Visible = True
                'Dim imb As New ImageButton
                'imb = CType(Me.FindControl("imbClose"), ImageButton)
                'imb.Attributes.Add("onClick", "return windowClose()")
            End If
        End If
    End Sub
#End Region

#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select

        BindGrid(Me.SearchBy, Me.SortBy)
        PnlList.Visible = True
    End Sub

    Protected Sub btnPageNumb_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPageNumb.Click

        If txtPage.Text = "" Then
            txtPage.Text = "0"
        Else
            If IsNumeric(txtPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                    currentPage = CType(txtPage.Text, Int32)
                    BindGrid(Me.SearchBy, Me.SortBy)

                End If
            End If
        End If

        PnlList.Visible = True

    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If

        If Me.CGID = "" Then Me.CGID = Me.GroubDbID

        BindGrid(Me.SearchBy, Me.SortBy)
        PnlList.Visible = True
    End Sub

#End Region

#Region " InitialDefaultPanel "
    Private Sub InitialDefaultPanel()        
        PnlList.Visible = True
    End Sub
#End Region

#Region " BindGrid"
    Sub BindGrid(ByVal cmdWhere As String, ByVal cmSort As String)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim oCollAct As New Parameter.CollActivity
        Dim oCollActList As New Parameter.CollActivity

        With oCollAct
            .strConnection = GetConnectionString()
            .AgreementNo = Me.AgreementNo
            .CustomerID = Me.CustomerID
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oCollActList = m_CollAct.GetCollActivityHistory(oCollAct)

        With oCollActList
            lblrecord.Text = CType(.TotalRecord, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecord
        End With

        dtsEntity = oCollActList.ListData
        dtvEntity = dtsEntity.DefaultView
        dtvEntity.Sort = Me.SortBy
        dtg.DataSource = dtvEntity
        Try
            dtg.DataBind()
        Catch
            dtg.CurrentPageIndex = 0
            dtg.DataBind()
        End Try

        PagingFooter()
    End Sub
#End Region

    Private Sub dtg_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtg.ItemDataBound
        'Dim imbDtl As ImageButton

        'If e.Item.ItemIndex >= 0 Then

        '    imbDtl = CType(e.Item.FindControl("ImbDetail"), ImageButton)
        '    imbDtl.Attributes.Add("onclick", "return OpenWindowDetail('" + Me.AgreementNo + "','" + Me.CustomerID + "','" + Me.CustomerName + "')")

        'End If

    End Sub

    Private Sub dtg_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtg.ItemCommand
        Me.CollectorID = e.Item.Cells(1).Text.Trim
        Me.ActivityDate = CType(e.Item.Cells(0).Text.Trim, Date)

        Select Case e.CommandName
            Case "DETAIL"
                Response.Redirect("InqCollActivityDetail.aspx?agreementno=" + Me.AgreementNo + "&CustomerID=" + Me.CustomerID + "&CustomerName=" + Me.CustomerName + "&CollectorID=" + Me.CollectorID + "&ActivityDate=" + CStr(Me.ActivityDate) + "")

        End Select
    End Sub
End Class