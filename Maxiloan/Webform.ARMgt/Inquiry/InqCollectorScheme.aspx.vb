﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class InqCollectorScheme
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oBranchCollection As UcBranchCollection

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.InqCollectorScheme
    Private oController As New InqCollectorSchemeController
    Private oClassAddress As New Parameter.Address
    Private oClassPersonal As New Parameter.Personal
    Protected WithEvents oSearchBy As UcSearchBy
#End Region
    Private Property Criteria() As String
        Get
            Return CType(viewstate("Criteria"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Criteria") = Value
        End Set
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not IsPostBack Then
            Me.FormID = "InqCollScheme"
            If CheckForm(Me.Loginid, Me.FormID, "Maxiloan") Then
                Me.SearchBy = ""
                Me.SortBy = ""
                pnlDtGrid.Visible = False
                If CheckFeature(Me.Loginid, Me.FormID, "VwZip", "Maxiloan") Then
                    hpLookup.NavigateUrl = "javascript:OpenWinZipCode('" & txtZipCode.ClientID & "','" & lblKelurahan.ClientID & "','" & lblKecamatan.ClientID & "','" & lblCity.ClientID & "','collection')"
                End If
            End If
        End If
    End Sub
    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Buttonsearch.Click
        If oBranchCollection.BranchID <> "" Then
            Me.SearchBy = "CGID='" & oBranchCollection.BranchID & "'"
        Else
            Me.SearchBy = ""
        End If
        If txtZipCode.Text <> "" Then
            Me.SearchBy = "Kelurahan.CGID='" & oBranchCollection.BranchID & "' and Kelurahan.ZipCode='" & txtZipCode.Text & "'"
            Me.Criteria = "ZipCode"
        Else
            Me.Criteria = ""
        End If
        pnlDtGrid.Visible = True
        DoBind(Me.SearchBy, Me.SortBy, Me.Criteria)
    End Sub

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String, ByVal SearchBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        Dim intloop As Integer
        Dim hypID As HyperLink

        With oCustomClass
            .strConnection = GetConnectionString
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
            .Criteria = SearchBy
        End With

        oCustomClass = oController.InqCollectorSchemeList(oCustomClass)

        DtUserList = oCustomClass.CollectorSchemeList
        DvUserList = DtUserList.DefaultView
        recordCount = oCustomClass.TotalRecord
        DvUserList.Sort = Me.SortBy
        dtgCollectorScheme.DataSource = DvUserList

        Try
            dtgCollectorScheme.DataBind()
        Catch
            dtgCollectorScheme.CurrentPageIndex = 0
            dtgCollectorScheme.DataBind()
        End Try
        PagingFooter()
        pnlDtGrid.Visible = True        
    End Sub

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy, Me.Criteria)
    End Sub

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then            
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SearchBy, Me.SortBy, Me.Criteria)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click        
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy, Me.Criteria)
            End If
        End If
    End Sub
#End Region

    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        Me.SearchBy = ""
        Me.SortBy = ""
        pnlDtGrid.Visible = False
        txtZipCode.Text = ""
        oBranchCollection.BranchID = ""
    End Sub


End Class