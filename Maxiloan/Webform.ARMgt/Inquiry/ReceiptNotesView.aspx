﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ReceiptNotesView.aspx.vb"
    Inherits="Maxiloan.Webform.ARMgt.ReceiptNotesView" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ReceiptNotesView</title>
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';			
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;

        function OpenViewCustomer(pCustomerID) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.LoanOrg/Credit/ViewPersonalCustomer.aspx?CustomerID=' + pCustomerID + '&Style=<%=request("style") %>', 'ViewCustomer', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }
        function fClose() {
            window.close();
            return false;
        }	
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">           
                <h3>
                    VIEW - KWITANSI TAGIH
                </h3>
            </div>
        </div>          
        <div class="form_box">
	        <div class="form_single">
                <label>No Kontrak</label>
                <asp:HyperLink ID="lblAgreementNo" runat="server" Text="<%me.agreementNo%>"> </asp:HyperLink>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Nama Customer</label>
                <a href="../../Webform.LoanOrg/Credit/ViewPersonalCustomer.aspx?Style=Collection&amp;CustomerId=<%=me.Customername%>">
                    <%=me.customername%>
                </a>
	        </div>
        </div>
        <div class="form_box_title">
            <div class="form_single">              
                <h4>
                    DAFTAR KWITANSI TAGIH
                </h4>
            </div>
        </div>
        <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgReceiptNotes" runat="server" Visible="False" DataKeyField="ReceiptNo"
                    AutoGenerateColumns="False" OnSortCommand="Sorting" AllowSorting="True" Width="100%"
                    CssClass="grid_general" BorderStyle="None" BorderWidth="0">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <Columns>
                        <asp:TemplateColumn SortExpression="ReceiptNo" HeaderText="NO KWITANSI">                            
                            <ItemTemplate>
                                <asp:Literal ID="ltlReceiptNo" runat="server" Text='<%#Container.DataItem("ReceiptNo")%>'>
                                </asp:Literal>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="ReceiptDate" HeaderText="TGL KWITANSI">                            
                            <ItemTemplate>
                                <asp:Literal ID="ltlReceiptDate" runat="server" Text='<%#Container.DataItem("ReceiptDate")%>'>
                                </asp:Literal>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="CGID" HeaderText="COLL GROUP">                            
                            <ItemTemplate>
                                <asp:Literal ID="ltlCGId" runat="server" Text='<%#Container.DataItem("CGId")%>'>
                                </asp:Literal>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="CollectorId" HeaderText="ID COLLECTOR">                            
                            <ItemTemplate>
                                <asp:Literal ID="ltlCollectorId" runat="server" Text='<%#Container.DataItem("CollectorId")%>'>
                                </asp:Literal>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="InstallmentNo" HeaderText="ANGS KE">                            
                            <ItemTemplate>
                                <asp:Literal ID="ltlInstallmentNo" runat="server" Text='<%#Container.DataItem("InstallmentNo")%>'>
                                </asp:Literal>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="DueDate" HeaderText="TGL JT">                            
                            <ItemTemplate>
                                <asp:Literal ID="ltlDueDate" runat="server" Text='<%#Container.DataItem("DueDate")%>'>
                                </asp:Literal>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="DaysOverDue" HeaderText="HARI TELAT">                            
                            <ItemTemplate>
                                <asp:Literal ID="ltlDaysOverDue" runat="server" Text='<%#Container.DataItem("DaysOverDue")%>'>
                                </asp:Literal>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="RNStatus" HeaderText="STATUS">                            
                            <ItemTemplate>
                                <asp:Literal ID="ltlRnStatus" runat="server" Text='<%#Container.DataItem("RNStatus")%>'>
                                </asp:Literal>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>                    
                </asp:DataGrid>        
                <div class="button_gridnavigation">
	                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
		                CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
		                CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
		                CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
		                CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
	                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
		                EnableViewState="False"></asp:Button>
	                <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage"
				        MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer"
				        CssClass="validator_general"></asp:RangeValidator>
			        <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
				        ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general">
			        </asp:RequiredFieldValidator>              
                </div>
                <div class="label_gridnavigation">
	                <asp:Label ID="lblPage" runat="server"></asp:Label>of
	                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
	                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                </div>
            </div> 
        </div>     
        </div>     
        <div class="form_button">            
            <asp:Button ID="imbClose" OnClientClick="fClose();" runat="server" CausesValidation="false" Text="Close" CssClass ="small button gray">
            </asp:Button>     
        </div>           
    </form>
</body>
</html>
