﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="InqCollectorScheme.aspx.vb"
    Inherits="Maxiloan.Webform.ARMgt.InqCollectorScheme" %>

<%@ Register TagPrefix="uc1" TagName="UcBranchCollection" Src="../../Webform.UserController/UcBranchCollection.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>InqCollectorScheme</title>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;

        function OpenWinCollection(inID, inCGID) {
            window.open('../setting/CollectorView.aspx?CollectorID=' + inID + '&CGID=' + inCGID + '', 'CollectorView', config = 'left=0, top=0, width='+ x + ', height = ' + y +',toolbar=no,menubar=no,scrollbars=yes,resizeable=no');
        }

        function OpenWinZipCode(pZipCode, pKelurahan, pKecamatan, pCity, pStyle) {
            //var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            //var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
            //window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/General/LookUpZipCode.aspx?ZipCode=' + pZipCode + '&style=' + pStyle, 'UserLookup', 'left=50, top=10, width=600, height=480, menubar=0, scrollbars=yes');
            window.open('../../General/LookUpZipCode.aspx?ZipCode=' + pZipCode + '&Kelurahan=' + pKelurahan + '&Kecamatan=' + pKecamatan + '&City=' + pCity + '&style=' + pStyle, 'UserLookup', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }
		
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:Label ID="lblKelurahan" runat="server" Visible="False"></asp:Label>
        <asp:Label ID="lblKecamatan" runat="server" Visible="False"></asp:Label>
        <asp:Label ID="lblCity" runat="server" Visible="False"></asp:Label>
        <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">           
                <h3>
                    SKEMA COLLECTOR
                </h3>
            </div>
        </div>          
        <div class="form_box">
	        <div class="form_single">
                <label>Collection Group</label>
                <uc1:UcBranchCollection id="oBranchCollection" runat="server">
                </uc1:UcBranchCollection>	        
            </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Kode Pos</label>
                <asp:TextBox ID="txtZipCode" runat="server" ></asp:TextBox>
                <asp:HyperLink ID="hpLookup" runat="server" ImageUrl="../../images/IconDetail.gif"></asp:HyperLink>
	        </div>
        </div>       
        <div class="form_button">
            <asp:Button ID="Buttonsearch" runat="server"  Text="Search" CssClass ="small button blue">
            </asp:Button>&nbsp;
            <asp:Button ID="ButtonReset" runat="server"  Text="Reset" CssClass ="small button gray"
            CausesValidation="False"></asp:Button>
	    </div>        
        <asp:Panel ID="pnlDtGrid" runat="server">
        <div class="form_box_title">
            <div class="form_single">              
                <h4>
                    DAFTAR SKEMA COLLECTOR
                </h4>
            </div>
        </div>       
        <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgCollectorScheme" runat="server" Width="100%" OnSortCommand="SortGrid"
                        CssClass="grid_general" BorderStyle="None" BorderWidth="0"
                        DataKeyField="CollectorID" AutoGenerateColumns="False" AllowSorting="True">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="KODE POS" HeaderStyle-HorizontalAlign="Center">                                
                                <ItemTemplate>
                                    <a href="../setting/CollectorViewZipCode.aspx?CollectorID=<%# Container.dataItem("CollectorID")%>&CollectorName=<%# Container.dataItem("EmployeeName")%>&CGID=<%# Container.dataItem("CGID")%>&CallerPage=../Inquiry/InqCollectorScheme.aspx">
                                        <asp:Image ID="imgZipCode" runat="server" ImageUrl="../../images/IconZIPCode.gif">
                                        </asp:Image></a>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="ID COLLECTOR" HeaderStyle-HorizontalAlign="Center"
                                SortExpression="CollectorID">                                
                                <ItemTemplate>
                                    <a href="javascript:OpenWinCollection('<%# Container.dataItem("CollectorID")%>','<%# Container.dataItem("CGID")%>');">
                                        <asp:Label ID="lblCollectorID" runat="server" Text='<%# COntainer.dataItem("CollectorID")%>'></asp:Label></a>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="NAMA COLLECTOR" HeaderStyle-HorizontalAlign="Center"
                                SortExpression="EmployeeName">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblNameCollector" runat="server" Text='<%# Container.dataItem("EmployeeName")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>                        
                </asp:DataGrid>        
                <div class="button_gridnavigation">
	                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
		                CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
		                CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
		                CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
		                CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
	                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
		                EnableViewState="False"></asp:Button>
	                <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage"
				        MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer"
				        CssClass="validator_general"></asp:RangeValidator>
			        <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
				        ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general">
			        </asp:RequiredFieldValidator>              
                </div>
                <div class="label_gridnavigation">
	                <asp:Label ID="lblPage" runat="server"></asp:Label>of
	                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
	                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                </div>
            </div>  
        </div>      
        </div>  
    </asp:Panel>
    </form>
</body>
</html>
