﻿#Region "Imports"
Imports System.Data
Imports System.Data.SqlClient
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Parameter.ViewCollection
#End Region

Public Class ViewCollection
    Inherits Maxiloan.Webform.WebBased

#Region " Private Const "
    Dim m_ViewCollection As New ViewCollectionController
#End Region

#Region "Property"
    Private Property BranchID1() As String
        Get
            Return CStr(viewstate("BranchID1"))
        End Get
        Set(ByVal Value As String)
            viewstate("BranchID1") = Value
        End Set
    End Property

    Private Property ApplicationID() As String
        Get
            Return CStr(viewstate("ApplicationID"))
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property
    Private Property AgreementNo() As String
        Get
            Return CStr(viewstate("AgreementNo"))
        End Get
        Set(ByVal Value As String)
            viewstate("AgreementNo") = Value
        End Set
    End Property

    Private Property CustomerName() As String
        Get
            Return CStr(viewstate("CustomerName"))
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerName") = Value
        End Set
    End Property
    Private Property CustomerID() As String
        Get
            Return CStr(viewstate("CustomerID"))
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerID") = Value
        End Set
    End Property


#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            'lblApplicationid = CType(e.Item.FindControl("hypApplicationID"), Label)

            'Dim lblRalHist As HyperLink
            'Dim lblCollActivity As HyperLink
            'Dim lblCollEvent As HyperLink
            'Dim lblInsurance As HyperLink
            'Dim lblApplicationid As Label
            'Dim lblCustomerID As Label
            'If e.Item.ItemIndex >= 0 Then

            '    lblAgreementNo = CType(e.Item.FindControl("hypAgreementNodtg"), HyperLink)
            '    lblApplicationid = CType(e.Item.FindControl("hypApplicationID"), Label)
            '    lblCustomerID = CType(e.Item.FindControl("lblCustomerID"), Label)
            '    lblCustomerName = CType(e.Item.FindControl("hypCustomerNamedtg"), HyperLink)
            '  hy.NavigateUrl = "javascript:OpenAgreementNo('" & "Collection" & "', '" & lblApplicationid.Text.Trim & "')"
            '    lblCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "Collection" & "', '" & lblCustomerID.Text & "')"
            'End If
            ' hyTemp = CType(e.Item.FindControl("hyviewcollection"), HyperLink)
            ' hyTemp.NavigateUrl = "javascript:OpenViewCollection('" & "AccMnt" & "','" & Server.UrlEncode(hyApplicationId.Text.Trim) & "','" & Server.UrlEncode(lbbranch.Text.Trim) & "')"
            Me.BranchID1 = Request.QueryString("BranchID")
            Me.ApplicationID = Request.QueryString("ApplicationID")
            Me.AgreementNo = Request.QueryString("AgreementNo")
            Me.CustomerID = Request.QueryString("CustomerID")
            Me.CustomerName = Request.QueryString("CustomerName")
            hdnAgreement.Value = CType(Me.AgreementNo, String)
            hdnCustID.Value = CType(Me.CustomerID, String)
            hdnApplicationID.Value = CType(Me.ApplicationID, String)
            hdnBranchID.Value = CType(Me.BranchID1, String)
            hdnCustName.Value = CType(Me.CustomerName, String)
            lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "Collection" & "', '" & Me.ApplicationID.Trim & "')"
            lblCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "Collection" & "', '" & Me.CustomerID.Trim & "')"
            'hypReceiptNotes.NavigateUrl = "javascript:OpenReceiptNotesView('" & "Collection" & "','" & Server.UrlEncode(hdnApplicationID.Value.Trim) & "','" & Server.UrlEncode(hdnBranchID.Value.Trim) & "')"
            'lnkAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccAcq" & "', '" & Server.UrlEncode(lblApplicationId.Text) & "')"
            'hyInsurance.NavigateUrl = "javascript:OpenInsurance('" & "AccMnt" & "','" & Server.UrlEncode(Me.ApplicationID.Trim) & "','" & Server.UrlEncode(Me.BranchID1.Trim) & "')"
            hyRalHist.NavigateUrl = "javascript:OpenRalHist('" & "AccMnt" & "','" & Server.UrlEncode(Me.ApplicationID.Trim) & "','" & Server.UrlEncode(Me.BranchID1.Trim) & "','" & Server.UrlEncode(Me.CustomerID.Trim) & "','" & Server.UrlEncode(Me.CustomerName.Trim) & "','" & Server.UrlEncode(Me.AgreementNo.Trim) & "')"
            hyReceiptNotes.NavigateUrl = "javascript:OpenReceiptNotesView('" & "Collection" & "','" & Server.UrlEncode(Me.ApplicationID.Trim) & "','" & Server.UrlEncode(Me.BranchID1.Trim) & "','" & Server.UrlEncode(Me.CustomerID.Trim) & "','" & Server.UrlEncode(Me.CustomerName.Trim) & "','" & Server.UrlEncode(Me.AgreementNo.Trim) & "')"
            hyCollActivity.NavigateUrl = "javascript:OpenActivity('" & "Collection" & "','" & Server.UrlEncode(Me.ApplicationID.Trim) & "','" & Server.UrlEncode(Me.BranchID1.Trim) & "','" & Server.UrlEncode(Me.CustomerID.Trim) & "','" & Server.UrlEncode(Me.CustomerName.Trim) & "','" & Server.UrlEncode(Me.AgreementNo.Trim) & "')"
            hyCollEvent.NavigateUrl = "javascript:OpenCollection('" & "Collection" & "','" & Server.UrlEncode(Me.ApplicationID.Trim) & "','" & Server.UrlEncode(Me.BranchID1.Trim) & "','" & Server.UrlEncode(Me.CustomerID.Trim) & "','" & Server.UrlEncode(Me.CustomerName.Trim) & "','" & Server.UrlEncode(Me.AgreementNo.Trim) & "')"
            hyInsurance.NavigateUrl = "javascript:OpenInsurance('" & "Collection" & "','" & Server.UrlEncode(Me.ApplicationID.Trim) & "','" & Server.UrlEncode(Me.BranchID1.Trim) & "')"


            Dim oViewCollection As New Parameter.ViewCollection
            With oViewCollection
                .strConnection = GetConnectionString()
                .BranchID1 = Me.BranchID1
                .ApplicationID = Me.ApplicationID
                .AgreementNo = Me.AgreementNo
                .CustomerName = Me.CustomerName

            End With

            Dim dt As New DataTable

            oViewCollection = m_ViewCollection.ViewCollection(oViewCollection)
            dt = oViewCollection.listData

            If dt.Rows.Count > 0 Then
                lblAgreementNo.Text = CStr(dt.Rows(0).Item("AgreementNo"))
                lblCustomerName.Text = CStr(dt.Rows(0).Item("Name"))
                lblAddress.Text = CStr(dt.Rows(0).Item("Address"))
                lblZipCode.Text = CStr(dt.Rows(0).Item("MailingZipCode"))
                lblAsset.Text = CStr(dt.Rows(0).Item("Description"))
                lblCollectionGroup.Text = CStr(dt.Rows(0).Item("cgName"))
                lblCollector.Text = CStr(dt.Rows(0).Item("Collector"))
                lblSupervisor.Text = CStr(dt.Rows(0).Item("Supervisor"))
                lblContractStatus.Text = CStr(dt.Rows(0).Item("ContractStatus"))
                lblAssetStatus.Text = CStr(dt.Rows(0).Item("AssetStatus"))

                If CType(CStr(dt.Rows(0).Item("CollStatus")), Integer) = 1 Then
                    lblCollStatus.Text = "Not RAL"
                ElseIf CType(CStr(dt.Rows(0).Item("CollStatus")), Integer) = 2 Then
                    lblCollStatus.Text = "RAL"
                ElseIf CType(CStr(dt.Rows(0).Item("CollStatus")), Integer) = 3 Then
                    lblCollStatus.Text = "Repossess"
                End If

                If CType(CStr(dt.Rows(0).Item("ExRepo")), String) = "True" Then
                    lblExRepo.Text = "Yes"
                ElseIf CType(CStr(dt.Rows(0).Item("ExRepo")), String) = "False" Then
                    lblExRepo.Text = "No"
                End If

                lblFixedAssign.Text = CStr(dt.Rows(0).Item("FixedAssign"))
                lblPlanDate.Text = CStr(dt.Rows(0).Item("PlanDate"))
                lblPlanDateToRAL.Text = CStr(dt.Rows(0).Item("PlanDateToSKT"))
                lblPlanActivity.Text = CStr(dt.Rows(0).Item("PlanActivity"))
                lblDaysoverdue.Text = CStr(dt.Rows(0).Item("EndPastDueDays"))
                lblOverdueAmount.Text = FormatNumber(CStr(dt.Rows(0).Item("overdue_amount")), 0)
                lblInstallmentNo.Text = CStr(dt.Rows(0).Item("NextInstallmentNumber"))
                lblInstallmentDate.Text = CStr(dt.Rows(0).Item("NextInstallmentDate"))
                lblInstallmentAmount.Text = FormatNumber(CStr(dt.Rows(0).Item("InstallmentAmount")), 0)
                lblOSBalance.Text = FormatNumber(CStr(dt.Rows(0).Item("os_balance")), 0)
                lblOSInsurance.Text = FormatNumber(CStr(dt.Rows(0).Item("os_insurance")), 0)
                lblOSCollectionFee.Text = FormatNumber(CStr(dt.Rows(0).Item("os_collectionfee")), 0)
                lblOSLateCharges.Text = FormatNumber(CStr(dt.Rows(0).Item("os_latecharges")), 0)
                lblRalNo.Text = CStr(dt.Rows(0).Item("RALNo"))
                lblRalDate.Text = CStr(dt.Rows(0).Item("RALDate"))
                lblExecutor.Text = CStr(dt.Rows(0).Item("executor"))

            End If
        End If
    End Sub


End Class