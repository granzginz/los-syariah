﻿#Region "Imports"
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class InqEndPastDueDays
    Inherits Maxiloan.Webform.WebBased
    Dim dtCity As New DataTable

    Protected WithEvents oBranch As ucBranchAll
#Region "Private Const"
    Dim m_InqEndPastDueDyas As New InquiryEndPastDueDaysController

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region
#Region "Property"
    Private Property BindMenu() As String
        Get
            Return CStr(ViewState("BindMenu"))
        End Get
        Set(ByVal Value As String)
            ViewState("BindMenu") = Value
        End Set
    End Property

    Private Property CGID() As String
        Get
            Return CStr(ViewState("CGID"))
        End Get
        Set(ByVal Value As String)
            ViewState("CGID") = Value
        End Set
    End Property
    Private Property City() As String
        Get
            Return CStr(ViewState("City"))
        End Get
        Set(ByVal Value As String)
            ViewState("City") = Value
        End Set
    End Property
    Private Property CityName() As String
        Get
            Return CStr(ViewState("CityName"))
        End Get
        Set(ByVal Value As String)
            ViewState("CityName") = Value
        End Set
    End Property

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim oDataTable As New DataTable
        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not Me.IsPostBack Then
            Me.CGID = Me.GroubDbID
            Me.FormID = "InqEndPastDueDays"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                InitialDefaultPanel()
                Me.SearchBy = "all"
                Me.SortBy = ""
                If Request("cond") <> "" Then
                    Me.SearchBy = Request("cond")
                Else
                    Me.SearchBy = "ALL"
                End If
                Me.SortBy = ""
                getCityCombo()
            End If
        End If
    End Sub

#Region " PanelAllFalse "
    Private Sub PanelAllFalse()
        PnlSearch.Visible = False
        PnlSearchDetail.Visible = False
        pnlList.Visible = False
    End Sub
#End Region


#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)

        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select

        Me.BindMenu = ""
        PnlSearch.Visible = False
        PnlSearchDetail.Visible = True
        BindGridInqEndPastDueDays(Me.SearchBy, Me.SortBy)

        pnlList.Visible = True
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click

        If txtPage.Text = "" Then
            txtPage.Text = "0"
        Else
            If IsNumeric(txtPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And (CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer)) Then
                    Me.BindMenu = ""
                    currentPage = CType(txtPage.Text, Int32)
                    BindGridInqEndPastDueDays(Me.SearchBy, Me.SortBy)
                End If
            End If
        End If

        pnlList.Visible = True

    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If

        If Me.CGID = "" Then Me.CGID = Me.GroubDbID

        BindGridInqEndPastDueDays(Me.SearchBy, Me.SortBy)
        pnlList.Visible = True
        PnlSearchDetail.Visible = True
    End Sub

#End Region

#Region " BindGrid"
    Sub BindGridInqEndPastDueDays(ByVal cmdWhere As String, ByVal cmSort As String)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim oInqEndPastDueDays As New Parameter.InqEndPastDueDays

        With oInqEndPastDueDays
            .strConnection = GetConnectionString()
            .City = Me.City
            .CGID = Me.CGID
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
            .BranchId = Me.BranchID
            .BranchName = Me.BranchName
        End With

        oInqEndPastDueDays = m_InqEndPastDueDyas.InqEndPastDueDays(oInqEndPastDueDays)

        With oInqEndPastDueDays
            lblrecord.Text = CType(.TotalRecord, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecord
        End With

        dtsEntity = oInqEndPastDueDays.ListData
        dtvEntity = dtsEntity.DefaultView
        dtvEntity.Sort = Me.SortBy
        dtgInqEndPastDueDays.DataSource = dtvEntity
        Try
            dtgInqEndPastDueDays.DataBind()
        Catch
            dtgInqEndPastDueDays.CurrentPageIndex = 0
            dtgInqEndPastDueDays.DataBind()
        End Try

        PagingFooter()
    End Sub
#End Region


#Region " InitialDefaultPanel "
    Private Sub InitialDefaultPanel()
        PnlSearch.Visible = True
        PnlSearchDetail.Visible = False
        pnlList.Visible = False
    End Sub
#End Region
    Private Sub getCityCombo()
        GenerateCombo(drdBranch, "spCboBranch", "Description", "ID")
    End Sub
    'Private Sub GenerateCombo(ByVal cbogenerate As System.Web.UI.WebControls.DropDownList, ByVal spName As String, ByVal TextField As String, ByVal ValueField As String)
    '    Dim PGeneral As New Parameter.GeneralPaging
    '    Dim CGeneral As New GeneralPagingController
    '    Dim dtTemp As New DataTable
    '    With PGeneral
    '        .SpName = spName
    '        .strConnection = GetConnectionString()
    '        .CurrentPage = 1
    '        .PageSize = 100
    '        .WhereCond = ""
    '        .SortBy = ""
    '    End With

    '    dtTemp = CGeneral.GetGeneralPaging(PGeneral).ListData

    '    With cbogenerate
    '        .DataSource = dtTemp
    '        .DataTextField = TextField
    '        .DataValueField = ValueField
    '        .DataBind()
    '        .Items.Insert(0, "Select One")
    '        .Items(0).Value = ""
    '        .SelectedIndex = 0
    '    End With
    'End Sub

    Private Sub GenerateCombo(ByVal cbogenerate As System.Web.UI.WebControls.DropDownList, ByVal spName As String, ByVal TextField As String, ByVal ValueField As String)
        'Dim PGeneral As New Parameter.GeneralPaging
        'Dim CGeneral As New GeneralPagingController
        'Dim dtTemp As New DataTable
        'With PGeneral
        '    .SpName = spName
        '    .strConnection = GetConnectionString()
        '    .CurrentPage = 1
        '    .PageSize = 100
        '    .WhereCond = ""
        '    .SortBy = ""
        'End With

        'dtTemp = CGeneral.GetGeneralPaging(PGeneral).ListData

        With cbogenerate
            If IsHoBranch Then
                .DataSource = New DataUserControlController().GetBranchAll(GetConnectionString())
                .DataValueField = "ID"
                .DataTextField = "Name"
                .DataBind()
                .Items.Insert(0, "Select One")
                .Items(0).Value = "0"
            Else
                .DataSource = New DataUserControlController().GetBranchName(GetConnectionString(), sesBranchId)
                .DataValueField = "ID"
                .DataTextField = "Name"
                .DataBind()
                .Items.Insert(0, "Select One")
                .Items(0).Value = "0"


            End If
            .SelectedIndex = .Items.IndexOf(.Items.FindByValue(sesBranchId.Replace("'", "")))
        End With
    End Sub
    'Private Sub BtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSearch.Click

    '    Me.BranchID = oBranch.BranchID.Trim
    '    Me.BranchName = oBranch.BranchName.Trim
    '    Me.SearchBy = ""
    '    Me.SortBy = ""
    '    Me.BindMenu = ""

    '    If oBranch.BranchID <> "0" Then
    '        PanelAllFalse()
    '        BindGridInqEndPastDueDays(Me.SearchBy, Me.SortBy)

    '        pnlList.Visible = True
    '        lblBranch.Text = Me.BranchName
    '        txtSearchByValue.Text = ""
    '        txtPage.Text = "1"

    '        PnlSearchDetail.Visible = True
    '        PnlSearch.Visible = False
    '    End If

    'End Sub

    Private Sub BtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSearch.Click

        Me.BranchID = drdBranch.SelectedItem.Value
        Me.BranchName = drdBranch.SelectedItem.Text
        Me.SearchBy = ""
        Me.SortBy = ""
        Me.BindMenu = ""

        Me.SearchBy = Me.SearchBy & " and collectionAgreement.branchid = '" & drdBranch.SelectedItem.Value & "'"

        If drdBranch.SelectedItem.Value <> "0" Then
            PanelAllFalse()
            BindGridInqEndPastDueDays(Me.SearchBy, Me.SortBy)

            pnlList.Visible = True
            lblBranch.Text = Me.BranchName
            txtSearchByValue.Text = ""
            txtPage.Text = "1"

            PnlSearchDetail.Visible = True
            PnlSearch.Visible = False
        End If

    End Sub

    Private Sub BtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        cboSearchBy.SelectedIndex = 0
        txtSearchByValue.Text = ""

        Me.SortBy = ""
        Me.SearchBy = "all"

        BindGridInqEndPastDueDays("ALL", "")
        pnlList.Visible = True
    End Sub

    Private Sub imgCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        PanelAllFalse()
        InitialDefaultPanel()
    End Sub

    Private Sub BtnSearchDetail_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSearchDetail.Click
        Me.BindMenu = ""
        Me.SearchBy = ""

        If cboSearchBy.SelectedItem.Value <> "0" Then
            If txtSearchByValue.Text.Trim <> "" Then
                If Right(txtSearchByValue.Text.Trim, Len(txtSearchByValue.Text.Trim) - 1) = "%" Then
                    Me.SearchBy = cboSearchBy.SelectedItem.Value & "like '" & txtSearchByValue.Text.Trim & "' "
                Else
                    Me.SearchBy = cboSearchBy.SelectedItem.Value & "='" & txtSearchByValue.Text.Trim & "' "
                End If
            End If
        End If

        Me.SortBy = ""
        BindGridInqEndPastDueDays(Me.SearchBy, Me.SortBy)
        pnlList.Visible = True
        PnlSearch.Visible = False
        PnlSearchDetail.Visible = True
    End Sub

    'Private Sub BtnResetDetail_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonResetDetail.Click
    '    Me.SearchBy = ""
    '    Me.SortBy = ""
    '    Me.BindMenu = ""

    '    cboSearchBy.SelectedIndex = 0
    '    txtSearchByValue.Text = ""

    '    BindGridInqEndPastDueDays(Me.SearchBy, Me.SortBy)
    '    pnlList.Visible = True
    '    PnlSearch.Visible = False
    '    PnlSearchDetail.Visible = True
    'End Sub

    Private Sub BtnResetDetail_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonResetDetail.Click
        Me.SearchBy = ""
        Me.SortBy = ""
        Me.BindMenu = ""

        Me.SearchBy = Me.SearchBy & " and collectionAgreement.branchid = '" & drdBranch.SelectedItem.Value & "'"


        BindGridInqEndPastDueDays(Me.SearchBy, Me.SortBy)
        pnlList.Visible = True
        PnlSearch.Visible = False
        PnlSearchDetail.Visible = True
    End Sub

    Private Sub ImageButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonBack.Click
        InitialDefaultPanel()
    End Sub

    Private Sub dtgInqEndPastDueDays_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgInqEndPastDueDays.ItemDataBound
        'Dim hypReceive As HyperLink
        Dim lblApplicationid As HyperLink
        Dim lblAgreementNo As HyperLink

        If e.Item.ItemIndex >= 0 Then
            lblApplicationid = CType(e.Item.FindControl("lblApplicationid"), HyperLink)

            lblAgreementNo = CType(e.Item.FindControl("lblAgreementNo"), HyperLink)
            If lblAgreementNo.Text.Trim.Length > 0 Then
                lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(lblApplicationid.Text.Trim) & "')"
            End If

            If lblApplicationid.Text.Trim.Length > 0 Then
                lblApplicationid.NavigateUrl = "javascript:OpenApplicationId('" & "AccMnt" & "', '" & Server.UrlEncode(lblApplicationid.Text.Trim) & "')"
            End If

        End If
    End Sub

End Class