﻿#Region "Imports"
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Webform.UserController
#End Region

Public Class InqDCR
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oCG As UcBranchCollection


#Region " Private Const "
    Dim m_DCR As New InqDCRController

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Protected WithEvents PnlSearch As System.Web.UI.WebControls.Panel
    Protected WithEvents dtgCollZipCode As System.Web.UI.WebControls.DataGrid
    Protected WithEvents cboTaskType As System.Web.UI.WebControls.DropDownList
    Protected WithEvents RequiredFieldValidator1 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents BtnSearch As System.Web.UI.WebControls.ImageButton
    Protected WithEvents Label1 As System.Web.UI.WebControls.Label
    Protected WithEvents imgbtnPageNumb As System.Web.UI.WebControls.ImageButton
    Protected WithEvents rfvGo As System.Web.UI.WebControls.RequiredFieldValidator
    Private recordCount As Int64 = 1

#End Region

#Region " Property "


    Private Property AgreementNo() As String
        Get
            Return CStr(viewstate("AgreementNo"))
        End Get
        Set(ByVal Value As String)
            viewstate("AgreementNo") = Value
        End Set
    End Property

    Private Property CustomerID() As String
        Get
            Return CStr(viewstate("CustomerID"))
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerID") = Value
        End Set
    End Property

    Private Property DefaultCollector() As String
        Get
            Return CStr(viewstate("DefaultCollector"))
        End Get
        Set(ByVal Value As String)
            viewstate("DefaultCollector") = Value
        End Set
    End Property

    Private Property BindMenu() As String
        Get
            Return CStr(viewstate("BindMenu"))
        End Get
        Set(ByVal Value As String)
            viewstate("BindMenu") = Value
        End Set
    End Property

    Private Property CGID() As String
        Get
            Return CStr(viewstate("CGID"))
        End Get
        Set(ByVal Value As String)
            viewstate("CGID") = Value
        End Set
    End Property

    Private Property City() As String
        Get
            Return CStr(viewstate("City"))
        End Get
        Set(ByVal Value As String)
            viewstate("City") = Value
        End Set
    End Property


    Private Property CGName() As String
        Get
            Return CStr(viewstate("CGName"))
        End Get
        Set(ByVal Value As String)
            viewstate("CGName") = Value
        End Set
    End Property

    Private Property ZipCode() As String
        Get
            Return CStr(ViewState("ZipCode"))
        End Get
        Set(ByVal Value As String)
            viewstate("ZipCode") = Value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        'Put user code to initialize the page here
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not Page.IsPostBack Then
            Me.CGID = Me.GroubDbID
            Me.FormID = "INQCOLLDCR"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                InitialDefaultPanel()
                getTaskTypeCombo()
                txtValidDateFrom.Text = Format(Me.BusinessDate, "dd/MM/yyyy")
                txtValidDateTo.Text = Format(Me.BusinessDate, "dd/MM/yyyy")
                PnlSearch.Visible = True
                pnlList.Visible = False
            End If
            'getCGCombo()
        End If
    End Sub

    Private Sub PanelAllFalse()        
        PnlSearch.Visible = False
        pnlList.Visible = False
    End Sub

#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString
        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select

        PnlSearch.Visible = True
        BindGrid(Me.SearchBy, Me.SortBy)
        pnlList.Visible = True
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click

        If txtPage.Text = "" Then
            txtPage.Text = "0"
        Else
            If IsNumeric(txtPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                    currentPage = CType(txtPage.Text, Int32)
                    BindGrid(Me.SearchBy, Me.SortBy)

                End If
            End If
        End If

        pnlList.Visible = True

    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If

        If Me.CGID = "" Then Me.CGID = Me.GroubDbID

        BindGrid(Me.SearchBy, Me.SortBy)
        pnlList.Visible = True
    End Sub

#End Region

#Region " InitialDefaultPanel "
    Private Sub InitialDefaultPanel()
        PnlSearch.Visible = True
        pnlList.Visible = False
    End Sub
#End Region

#Region " BindGrid"
    Sub BindGrid(ByVal cmdWhere As String, ByVal cmSort As String)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim oDCR As New Parameter.InqDCR

        With oDCR
            .strConnection = GetConnectionString()
            .BranchId = Me.BranchID
            .CGID = Me.CGID
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oDCR = m_DCR.InqDCRList(oDCR)

        With oDCR
            lblrecord.Text = CType(.TotalRecord, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecord
        End With

        dtsEntity = oDCR.listdata
        dtvEntity = dtsEntity.DefaultView
        dtvEntity.Sort = Me.SortBy
        dtgDCR.DataSource = dtvEntity
        Try
            dtgDCR.DataBind()
        Catch
            dtgDCR.CurrentPageIndex = 0
            dtgDCR.DataBind()
        End Try

        PagingFooter()
    End Sub
#End Region

    Private Sub BtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        cboSearchBy.SelectedIndex = 0
        txtSearchByValue.Text = ""

        With oCG
            .BranchID = ""
            .DataBind()
        End With

        cboTaskType.SelectedIndex = 0        

        Me.SortBy = ""
        Me.SearchBy = "all"

        BindGrid("ALL", "")
        pnlList.Visible = True
    End Sub

    Private Sub getCGCombo()

        'Dim strconn As String = getConnectionString()
        'Dim dt As New DataTable

        'dt = m_DCR.GetComboCG(strconn)

        'cboCG.DataSource = dt
        'cboCG.DataTextField = "Name"
        'cboCG.DataValueField = "ID"
        'cboCG.DataBind()

        'cboCG.Items.Insert(0, "Select One")
        'cboCG.Items(0).Value = "0"

    End Sub

    Private Sub getTaskTypeCombo()

        Dim strconn As String = getConnectionString()
        Dim dt As New DataTable

        dt = m_DCR.GetComboTaskType(strconn)

        cboTaskType.DataSource = dt
        cboTaskType.DataTextField = "Name"
        cboTaskType.DataValueField = "ID"
        cboTaskType.DataBind()

        cboTaskType.Items.Insert(0, "Select One")
        cboTaskType.Items(0).Value = "0"

    End Sub

    Private Sub ImbSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSearch.Click
        Me.SearchBy = ""
        Me.SortBy = ""

        If (oCG.BranchID <> "0") Then Me.SearchBy = " CGID='" + oCG.BranchID.Trim + "'"

        If cboTaskType.SelectedIndex <> 0 Then Me.SearchBy = Me.SearchBy + " and TaskType='" + cboTaskType.SelectedItem.Value + "'"
        If txtValidDateFrom.Text.Trim <> "" And txtValidDateTo.Text.Trim <> "" Then
            If (cboSearchBy.SelectedIndex <> 0) And (txtSearchByValue.Text.Trim <> "") Then
                Me.SearchBy = Me.SearchBy + " and " + cboSearchBy.SelectedItem.Value.Trim + " like '%" + txtSearchByValue.Text.Trim + "%'"
            End If

            Dim datefrom As String
            Dim dateto As String

            'Convert ke yyyy-mm-dd (sesuai di database)
            datefrom = CStr(ConvertDate2(txtValidDateFrom.Text.Trim))
            dateto = CStr(ConvertDate2(txtValidDateTo.Text.Trim))

            Me.SearchBy = Me.SearchBy + " and (DCRDate between '" + datefrom + "' and '" + dateto + "')"
        End If

        If CheckFeature(Me.Loginid, Me.FormID, "Srch", Me.AppId) Then
            If SessionInvalid() Then
                Exit Sub
            End If
        End If

        BindGrid(Me.SearchBy, Me.SortBy)
        pnlList.Visible = True
    End Sub

    Private Sub InitializeComponent()

    End Sub

    Private Sub dtgDCR_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgDCR.ItemDataBound
        Dim lblAgreementNo As HyperLink
        Dim lblApplicationid As HyperLink
        Dim lblCustomerID As Label
        Dim lblCustomerName As HyperLink

        If e.Item.ItemIndex >= 0 Then
            'Me.AgreementNo = e.Item.Cells(1).Text.Trim
            'Me.CustomerID = e.Item.Cells(8).Text
            lblAgreementNo = CType(e.Item.FindControl("lblAgreementNo"), HyperLink)
            lblApplicationid = CType(e.Item.FindControl("lblApplicationid"), HyperLink)
            lblCustomerID = CType(e.Item.FindControl("lblCustomerID"), Label)
            lblCustomerName = CType(e.Item.FindControl("lblCustomerName"), HyperLink)
            lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & lblApplicationid.Text.Trim & "')"

            lblCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & lblCustomerID.Text & "')"
        End If
    End Sub
End Class