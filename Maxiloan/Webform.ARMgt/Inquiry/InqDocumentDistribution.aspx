﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="InqDocumentDistribution.aspx.vb"
    Inherits="Maxiloan.Webform.ARMgt.InqDocumentDistribution" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ucCollectorRpt" Src="../../Webform.UserController/ucCollectorRpt.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>InqDocumentDistribution</title>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%#Request.servervariables("SERVER_NAME")%>/';
    </script>
    <script language="javascript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;

        function OpenWinCustomer(pID) {
            window.open('../../Webform.LoanOrg/Credit/ViewPersonalCustomer.aspx?CustomerID=' + pID + '&style=Collection', 'UserLookup', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>    
        <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">           
                <h3>
                    INQUIRY DISTRIBUSI DOKUMEN
                </h3>
            </div>
        </div>  
        <asp:Panel ID="pnlsearch" runat="server">
        <div class="form_box_uc">
                <uc1:ucCollectorRpt id="oCollector" runat="server"></uc1:ucCollectorRpt>	        
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Cari Berdasarkan</label>
                <asp:DropDownList ID="cboSearchBy" runat="server">
                    <asp:ListItem Value="Select One">Select One</asp:ListItem>
                    <asp:ListItem Value="CollectionAgreement.AgreementNo">No Kontrak</asp:ListItem>
                    <asp:ListItem Value="Customer.Name">Nama Customer</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearchBy" runat="server" ></asp:TextBox>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label class ="label_req">
				Tanggal Kirim</label>                
                <asp:TextBox ID="txtSentDate1" runat="server"></asp:TextBox>                   
                <asp:CalendarExtender ID="txtSentDate1_CalendarExtender" runat="server" 
                    Enabled="True" TargetControlID="txtSentDate1" Format ="dd/MM/yyyy"></asp:CalendarExtender>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*" CssClass="validator_general"
                ControlToValidate="txtSentDate1"></asp:RequiredFieldValidator>
                &nbsp;S/D&nbsp;                
                <asp:TextBox ID="txtSentDate2" runat="server"></asp:TextBox>                   
                <asp:CalendarExtender ID="txtSentDate2_CalendarExtender" runat="server" 
                    Enabled="True" TargetControlID="txtSentDate2" Format ="dd/MM/yyyy"></asp:CalendarExtender>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*" CssClass="validator_general"
                ControlToValidate="txtSentDate2"></asp:RequiredFieldValidator>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Status Polis</label>
                <asp:DropDownList ID="cboReceiveStatus" runat="server">
                    <asp:ListItem Value="All">All</asp:ListItem>
                    <asp:ListItem Value="InsuranceAsset.PolicyNumber <> '-' and InsuranceAsset.PolicyNumber <> '0 ">Terima</asp:ListItem>
                    <asp:ListItem Value="InsuranceAsset.PolicyNumber = '-">Belum Terima</asp:ListItem>
                </asp:DropDownList>
	        </div>
        </div>
        <div class="form_button">
            <asp:Button ID="Buttonsearch" runat="server" CausesValidation="False"  Text="Search" CssClass ="small button blue"></asp:Button>&nbsp;
            <asp:Button ID="ButtonReset" runat="server"  Text="Reset" CssClass ="small button gray"
                CausesValidation="False"></asp:Button>
	    </div>        
        </asp:Panel>    
        <asp:Panel ID="pnlDtGrid" runat="server">
        <div class="form_box_title">
            <div class="form_single">              
                <h4>
                    DAFTAR DISTRIBUSI DOKUMEN
                </h4>
            </div>
        </div>
        <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgdocumentdistribution" runat="server" Width="100%"
                        AllowSorting="True" AutoGenerateColumns="False" DataKeyField="AgreementNo" 
                        CssClass="grid_general" BorderStyle="None" BorderWidth="0" OnSortCommand="SortGrid"                        >
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="hypAgreementNodtg" runat="server" Text='<%# Container.Dataitem("AgreementNo")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="CustomerName" HeaderText="NAMA CUSTOMER">                                
                                <ItemTemplate>
                                    <a href="javascript:OpenWinCustomer('<%# Container.DataItem("CustomerID")%>');">
                                        <asp:Label ID="hypCustomerNamedtg" runat="server" Text='<%# Container.Dataitem("CustomerName")%>'>
                                        </asp:Label></a>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="WayOfPayment" HeaderText="CARA PEMBAYARAN">                                
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="AgreementSentDateReceiveBy" HeaderText="TGL KIRIM DOKUMEN">                                
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="SlipSentDateReceiveBy" HeaderText="TGL KIRIM SLIP">                                
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="PolicySentDateReceiveBy" HeaderText="TGL KIRIM POLIS">                                
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="PolicyReceiveStatus" HeaderText="STATUS POLIS">                                
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="CollectorID" HeaderText="ID COLLECTOR"></asp:BoundColumn>
                            <asp:TemplateColumn Visible="False" SortExpression="ApplicationId" HeaderText="ApplicationId">
                                <ItemTemplate>
                                    <asp:Label ID="lblApplicationId" runat="server" Text='<%# Container.Dataitem("ApplicationId")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" HeaderText="BranchId">
                                <ItemTemplate>
                                    <asp:Label ID="lblBranchId" runat="server" Text='<%# Container.Dataitem("BranchID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>                        
                </asp:DataGrid>      
                <div class="button_gridnavigation">
	                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
		                CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
		                CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
		                CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
		                CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
	                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
		                EnableViewState="False"></asp:Button>
	                <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage"
				        MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer"
				        CssClass="validator_general"></asp:RangeValidator>
			        <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
				        ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general">
			        </asp:RequiredFieldValidator>              
                </div>
                <div class="label_gridnavigation">
	                <asp:Label ID="lblPage" runat="server"></asp:Label>of
	                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
	                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                </div>
            </div>  
        </div>  
        </div>
    </asp:Panel>          
    </form>
</body>
</html>
