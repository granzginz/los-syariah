﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ExtendHistory.aspx.vb"
    Inherits="Maxiloan.Webform.ARMgt.ExtendHistory" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ExtendHistory</title>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';
    </script>
</head>
<body>
    <form id="form1" runat="server">    
        <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">           
                <h3>
                    <%--HISTORY PERPANJANGAN SKT (SURAT KUASA TARIK)--%>
                    HISTORY PERPANJANGAN SKE (SURAT KUASA EKSEKUSI)
                </h3>
            </div>
        </div>  
        <div class="form_box">
	        <div class="form_single">
                <label>No Kontrak</label>
                <asp:HyperLink ID="hypAgreementNo" runat="server"></asp:HyperLink>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Nama Customer</label>
                <asp:HyperLink ID="hypCustomerName" runat="server"></asp:HyperLink>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Nama Asset</label>
                <asp:Label ID="lblAsset" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box_title">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgExtendHistory" runat="server" Width="100%" OnSortCommand="SortGrid"                    
                    AutoGenerateColumns="False" AllowSorting="True" CssClass="grid_general" BorderStyle="None" BorderWidth="0">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <Columns>
                        <asp:BoundColumn DataField="INSTALLMENTNO" SortExpression="INSTALLMENTNO" HeaderText="ANGS KE">                            
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="EXTENDDATE" SortExpression="EXTENDDATE" HeaderText="TGL PERPANJANGAN">                            
                        </asp:BoundColumn>
                        <%--<asp:BoundColumn DataField="PREVRALNO" SortExpression="PREVRALNO" HeaderText="NO SKT SEBELUMNYA">                            --%>
                        <asp:BoundColumn DataField="PREVRALNO" SortExpression="PREVRALNO" HeaderText="NO SKE SEBELUMNYA">
                        </asp:BoundColumn>
                        <%--<asp:BoundColumn DataField="PREVPERIOD" SortExpression="PREVPERIOD" HeaderText="PERIODE SKT SEBELUMNYA">                           --%>
                        <asp:BoundColumn DataField="PREVPERIOD" SortExpression="PREVPERIOD" HeaderText="PERIODE SKE SEBELUMNYA">                           
                        </asp:BoundColumn>
                        <%--<asp:BoundColumn DataField="NEWRALNO" SortExpression="NEWRALNO" HeaderText="NO SKT BARU">                            --%>
                        <asp:BoundColumn DataField="NEWRALNO" SortExpression="NEWRALNO" HeaderText="NO SKE BARU">
                        </asp:BoundColumn>
                        <%--<asp:BoundColumn DataField="NEWPERIOD" SortExpression="NEWPERIOD" HeaderText="PERIODE SKT BARU">                            --%>
                        <asp:BoundColumn DataField="NEWPERIOD" SortExpression="NEWPERIOD" HeaderText="PERIODE SKE BARU">
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="NOTES" SortExpression="NOTES" HeaderText="CATATAN">                            
                        </asp:BoundColumn>
                    </Columns>                    
                </asp:DataGrid>        
                <div class="button_gridnavigation">
	                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
		                CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
		                CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
		                CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
		                CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
	                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
		                EnableViewState="False"></asp:Button>
	                <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage"
				        MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer"
				        CssClass="validator_general"></asp:RangeValidator>
			        <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
				        ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general">
			        </asp:RequiredFieldValidator>              
                </div>
                <div class="label_gridnavigation">
	                <asp:Label ID="lblPage" runat="server"></asp:Label>of
	                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
	                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                </div>
            </div>  
        </div>        
        </div> 
        <div class="form_button">
            <asp:Button ID="ButtonBack" runat="server"  Text="Back" CssClass ="small button gray">
            </asp:Button>
	    </div>
        
    </form>
</body>
</html>
