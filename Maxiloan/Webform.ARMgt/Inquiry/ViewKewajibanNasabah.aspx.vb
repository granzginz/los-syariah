﻿#Region "Imports"
Imports System.Data
Imports System.Data.SqlClient
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Parameter.ViewCollection
#End Region

Public Class ViewKewajibanNasabah
    Inherits Maxiloan.Webform.WebBased

#Region " Private Const "
    Dim m_ViewCollection As New ViewCollectionController
    Private oCustomClass As New Parameter.SPPrint
    Private oController As New SPPrintController
#End Region

#Region "Property"
    Private Property BranchID1() As String
        Get
            Return CStr(ViewState("BranchID1"))
        End Get
        Set(ByVal Value As String)
            ViewState("BranchID1") = Value
        End Set
    End Property

    Private Property ApplicationID() As String
        Get
            Return CStr(ViewState("ApplicationID"))
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
    Private Property AgreementNo() As String
        Get
            Return CStr(ViewState("AgreementNo"))
        End Get
        Set(ByVal Value As String)
            ViewState("AgreementNo") = Value
        End Set
    End Property

    Private Property CustomerName() As String
        Get
            Return CStr(ViewState("CustomerName"))
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerName") = Value
        End Set
    End Property
    Private Property CustomerID() As String
        Get
            Return CStr(ViewState("CustomerID"))
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property

    Private Property Alamat() As String
        Get
            Return CStr(ViewState("Alamat"))
        End Get
        Set(ByVal Value As String)
            ViewState("Alamat") = Value
        End Set
    End Property

    Private Property TotalKewajibanNasabah() As Decimal
        Get
            Return CType(ViewState("TotalKewajibanNasabah"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            ViewState("TotalKewajibanNasabah") = Value
        End Set
    End Property

#End Region
    Dim tempPrincipalAmount As Double = 0
    Dim tempINTERESTAMOUNT As Double = 0
    Dim tempDenda As Double = 0
    Dim tempByrDenda As Double = 0
    Dim tempPaidAmount As Double = 0
    Dim tempTotal As Double = 0

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.BranchID1 = Request.QueryString("BranchID")
            Me.ApplicationID = Request.QueryString("ApplicationID")
            Me.AgreementNo = Request.QueryString("AgreementNo")
            Me.CustomerID = Request.QueryString("CustomerID")
            Me.CustomerName = Request.QueryString("CustomerName")
            Me.Alamat = Request.QueryString("Address")
            hdnAgreement.Value = CType(Me.AgreementNo, String)
            hdnCustID.Value = CType(Me.CustomerID, String)
            hdnApplicationID.Value = CType(Me.ApplicationID, String)
            hdnBranchID.Value = CType(Me.BranchID1, String)
            hdnCustName.Value = CType(Me.CustomerName, String)
            hdnAlamat.Value = CType(Me.Alamat, String)
            lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Me.ApplicationID.Trim & "')"

            lblAgreementNo.Text = Me.AgreementNo
            lblCustomerID.Text = Me.CustomerID
            lblCustomerName.Text = Me.CustomerName
            lblAlamat.Text = Me.Alamat
        End If
    End Sub

    Sub Bindgrid()
        Dim dtGrid As New DataTable
        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(getConnectionString)

        'Dim adapter As New SqlDataAdapter
        Dim objReader As SqlDataReader

        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()

            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.CommandText = "spViewAmortizationKewajibanNasabah"
            objCommand.Connection = objConnection
            objCommand.Parameters.Add("@ApplicationID", SqlDbType.VarChar, 20).Value = Me.ApplicationID.Trim
            objCommand.Parameters.Add("@BusinessDate", SqlDbType.DateTime).Value = Me.BusinessDate
            objCommand.Parameters.Add("@DueDate", SqlDbType.DateTime).Value = Date.ParseExact(txtTglPelunasan.Text.ToString, "dd/MM/yyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo)
            objReader = objCommand.ExecuteReader

            dtg.DataSource = objReader
            dtg.DataBind()
            objReader.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        Finally
            objCommand.Dispose()
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try
    End Sub

    Protected Sub btnHitung_Click(sender As Object, e As EventArgs) Handles btnHitung.Click
        Bindgrid()
        lblKeterangan.Text = "Total Kewajiban per Tanggal " & txtTglPelunasan.Text & " adalah Sebesar Rp. " & FormatNumber(tempTotal, 2) & " "
    End Sub

    Private Sub dtg_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtg.ItemDataBound
        Dim lb As New Label
        Dim lb1 As New Label
        Dim DueDate As Date
        Dim DueDateLbl As Label

        If e.Item.ItemIndex >= 0 Then
            DueDateLbl = CType(e.Item.FindControl("lblDueDate"), Label)
            DueDate = Date.ParseExact(DueDateLbl.Text.ToString, "dd/MM/yyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo)
            If DueDate <= Date.ParseExact(txtTglPelunasan.Text.ToString, "dd/MM/yyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo) Then
                'isi total 
                lb = CType(e.Item.FindControl("lblPrincipalAmount"), Label)
                tempPrincipalAmount = tempPrincipalAmount + CDbl(lb.Text)

                lb = CType(e.Item.FindControl("lblINTERESTAMOUNT"), Label)
                tempINTERESTAMOUNT = tempINTERESTAMOUNT + CDbl(lb.Text)

                lb = CType(e.Item.FindControl("lblDenda"), Label)
                tempDenda = tempDenda + CDbl(Replace(lb.Text, "-", 0))

                lb = CType(e.Item.FindControl("lblPaidAmount"), Label)
                tempPaidAmount = tempPaidAmount + CDbl(Replace(lb.Text, "-", 0))

                lb = CType(e.Item.FindControl("lblPaidLateCharges"), Label)
                tempByrDenda = tempByrDenda + CDbl(Replace(lb.Text, "-", 0))

                tempTotal = (tempPrincipalAmount + tempINTERESTAMOUNT + tempDenda) - tempPaidAmount - tempByrDenda
                Me.TotalKewajibanNasabah = tempTotal
            End If
        End If
    End Sub

    Private Sub imbPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonPrint.Click      

            Dim cookie As HttpCookie = Request.Cookies("RptViewKewajibanNasabahPrint")
        If Not cookie Is Nothing Then
            cookie.Values("AgreementNo") = Me.AgreementNo
            cookie.Values("CustomerID") = Me.CustomerID
            cookie.Values("ApplicationID") = Me.ApplicationID.Trim
            cookie.Values("BranchID1") = Me.BranchID1
            cookie.Values("CustomerName") = Me.CustomerName
            cookie.Values("Alamat") = Me.Alamat
            cookie.Values("DueDate") = Date.ParseExact(txtTglPelunasan.Text.ToString, "dd/MM/yyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo)
            cookie.Values("TotalKewajibanNasabah") = Me.TotalKewajibanNasabah
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie("RptViewKewajibanNasabahPrint")
            cookieNew.Values.Add("AgreementNo", Me.AgreementNo)
            cookieNew.Values.Add("CustomerID", Me.CustomerID)
            cookieNew.Values.Add("ApplicationID", Me.ApplicationID.Trim)
            cookieNew.Values.Add("BranchID1", Me.BranchID1)
            cookieNew.Values.Add("CustomerName", Me.CustomerName)
            cookieNew.Values.Add("Alamat", Me.Alamat)
            cookieNew.Values.Add("DueDate", Date.ParseExact(txtTglPelunasan.Text.ToString, "dd/MM/yyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo))
            cookieNew.Values.Add("TotalKewajibanNasabah", Me.TotalKewajibanNasabah)
            Response.AppendCookie(cookieNew)
        End If
        Response.Redirect("ViewKewajibanNasabahViewer.aspx")
    End Sub
End Class