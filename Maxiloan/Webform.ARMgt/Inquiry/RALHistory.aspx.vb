﻿#Region "Import"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class RALHistory
    Inherits Maxiloan.Webform.WebBased

#Region " Private Const "
    Dim m_RALHistory As New RALHistoryController


#End Region
#Region "properties"
    Public Property AgreementNo() As String
        Get
            Return CType(viewstate("AgreementNo"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AgreementNo") = Value
        End Set
    End Property
    Public Property CustomerName() As String
        Get
            Return CType(viewstate("CustomerName"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerName") = Value
        End Set
    End Property
    Public Property CustomerID() As String
        Get
            Return CType(viewstate("CustomerID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerID") = Value
        End Set
    End Property
    Private Property AppliactionID() As String
        Get
            Return CType(viewstate("AppliactionID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AppliactionID") = Value
        End Set
    End Property
#End Region


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If sessioninvalid() Then
            Exit Sub
        End If
        If Not Page.IsPostBack Then
            Me.FormID = "CollRALHistory"
            Me.AppliactionID = Request.QueryString("ApplicationID")
            Me.BranchID = Request.QueryString("BranchID")
            Me.CustomerID = Request.QueryString("CustomerID")
            Me.CustomerName = Request.QueryString("CustomerName")
            Me.AgreementNo = Request.QueryString("AgreementNo")
            lblAgreementNo.Text = Me.AgreementNo
            lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "Collection" & "', '" & Me.AppliactionID.Trim & "')"
        End If
        If CheckForm(Me.Loginid, Me.FormID, Me.AppID) Then
            bindData()
        Else
            Dim strHTTPServer As String
            Dim strHTTPApp As String
            Dim strNameServer As String
            strHTTPServer = Request.ServerVariables("PATH_INFO")
            strNameServer = Request.ServerVariables("SERVER_NAME")
            strHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
            Response.Redirect("http://" & strNameServer & "/" & strHTTPApp & "/NotAuthorized.html")
        End If
    End Sub
    Sub bindData()
        Dim dtvEntity As New DataView
        Dim dtsEntity As New DataTable
        Dim oRALHistory As New Parameter.RALHistory

        With oRALHistory
            .strConnection = GetConnectionString()
            .BranchId = Me.BranchID
            .AppliactionID = Me.AppliactionID
            .AgreementNo = Me.AgreementNo
        End With

        oRALHistory = m_RALHistory.RALHistoryList(oRALHistory)

        dtsEntity = oRALHistory.ListRAL
        dtsEntity.TableName = "Table"

        dtvEntity = dtsEntity.DefaultView
        dtvEntity.Sort = Me.SortBy

        dtgRALHistory.DataSource = dtvEntity

        If Not dtvEntity Is Nothing Then
            dtgRALHistory.DataBind()
        End If
    End Sub


End Class