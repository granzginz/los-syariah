﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="InqCollActivityDetail.aspx.vb"
    Inherits="Maxiloan.Webform.ARMgt.InqCollActivityDetail" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>InqCollActivityDetail</title>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;

        function fConfirm() {
            if (window.confirm("Apakah yakin mau hapus data ini ? "))
                return true;
            else
                return false;
        }
        function fback() {
            history.go(-1);
            return false;
        }
        function OpenViewCollectionGroup(pCGID) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.ARMgt/Setting/CollectionGroupView.aspx?CGID=' + pCGID, 'CollectionGroupView', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenViewCollector(pCollID, pCGID) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.ARMgt/Setting/CollectorView.aspx?CollectorID=' + pCollID + '&CGID=' + pCGID, 'CollectorView', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenViewAgreement(pAgreementNo) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.LoanOrg/Credit/ViewStatementofAccount.aspx?AgreementNo=' + pAgreementNo + '&Style=Collection', 'AgreementView', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenViewCustomer(pCustomerID) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.LoanOrg/Credit/ViewPersonalCustomer.aspx?CustomerID=' + pCustomerID + '&Style=Collection', 'CustomerView', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">           
                <h3>
                    KEGIATAN COLLECTION
                </h3>
            </div>
        </div> 
        <asp:Panel ID="PnlList" runat="server">        
        <div class="form_box">
	        <div class="form_single">
                <label>No Kontrak.</label>
                <asp:HyperLink ID="hpAgreementNo" runat="server"></asp:HyperLink>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Nama Customer</label>
                <asp:HyperLink ID="hpCustomerName" runat="server"></asp:HyperLink>
	        </div>  
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Collector</label>
                <asp:Label ID="lblCollectorID" runat="server" Width="368px"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Tanggal Kegiatan</label>
                <asp:Label ID="lblActivityDate" runat="server" Width="368px"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Kegiatan</label>
                <asp:Label ID="lblActivity" runat="server" Width="368px"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Hasil Kegiatan</label>
                <asp:Label ID="lblActivityResult" runat="server" Width="368px"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Tanggal Janji Bayar</label>
                <asp:Label ID="lblPTPYDate" runat="server" Width="368px"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">                                
                <label class="label_general">Catatan</label>
                <asp:TextBox ID="txtNotes" runat="server" CssClass="multiline_textbox" TextMode="MultiLine"></asp:TextBox>
	        </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonBack" runat="server"  Text="Back" CssClass ="small button gray"
            CausesValidation="False" Enabled="True"></asp:Button>
	    </div>       
    </asp:Panel>
    </form>
</body>
</html>
