﻿Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports Maxiloan.General.CommonCookiesHelper
Imports System.Data.SqlClient
Imports System.IO
Imports Maxiloan.Webform.UserController

Public Class InqCollActivityResultViewer
    Inherits Maxiloan.Webform.WebBased
#Region "Constanta"
    Private m_coll As New SPPrintController
    Private m_CollAct As New CollActivityController
    Private oCustomClass As New Parameter.SPPrint
#End Region

#Region "Property"
    Private Property BranchID1() As String
        Get
            Return CStr(ViewState("BranchID1"))
        End Get
        Set(ByVal Value As String)
            ViewState("BranchID1") = Value
        End Set
    End Property

    Private Property ApplicationID() As String
        Get
            Return CStr(ViewState("ApplicationID"))
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
    Private Property AgreementNo() As String
        Get
            Return CStr(ViewState("AgreementNo"))
        End Get
        Set(ByVal Value As String)
            ViewState("AgreementNo") = Value
        End Set
    End Property

    Private Property CustomerName() As String
        Get
            Return CStr(ViewState("CustomerName"))
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerName") = Value
        End Set
    End Property
    Private Property CustomerID() As String
        Get
            Return CStr(ViewState("CustomerID"))
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property

    Private Property Style As String
        Get
            Return CStr(ViewState("Style"))
        End Get
        Set(ByVal Value As String)
            ViewState("Style") = Value
        End Set
    End Property
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("cmdwhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("cmdwhere") = Value
        End Set
    End Property
#End Region
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        GetCookies()
        BindReport()
    End Sub
    Sub BindReport()
        Dim oDataSet As New DataSet
        Dim oCollAct As New Parameter.CollActivity
        Dim oCollActList As New Parameter.CollActivity
        Dim objreport As ReportDocument
        Dim dtsEntity As New DataTable
        Dim ds As New DataSet

        objreport = New RptInqCollActivityResult
        Me.CmdWhere = " WHERE Agreement.AgreementNo ='" & Me.AgreementNo.Trim & "'" & " and  customer.customerid = '" & Me.CustomerID & "'" & " "
        'Me.CmdWhere = Me.CmdWhere & " and  customer.customerid = '" & Me.CustomerID & "'" & " "

        With oCollAct
            .strConnection = GetConnectionString()
            .CGID = Me.BranchID
            .WhereCond = Me.CmdWhere
        End With

        'ds = GetData()
        dtsEntity = m_CollAct.GetCollActivityResultListPrint(oCollAct)
        'oDataSet = oCollActList.ListReport
        oDataSet.Tables.Add(dtsEntity.Copy)

        objreport.SetDataSource(oDataSet)

        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions

        objreport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
        objreport.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat

        Dim strFileLocation As String

        strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
        strFileLocation += Me.Session.SessionID & Me.Loginid & "InqCollActivityHistoryPrint.pdf"
        DiskOpts.DiskFileName = strFileLocation
        objreport.ExportOptions.DestinationOptions = DiskOpts
        objreport.Export()
        objreport.Close()
        objreport.Dispose()
        'Response.Redirect("InqCollActivityResult.aspx?strFileLocation=" & Me.Session.SessionID & Me.Loginid & Me.Style & Me.AgreementNo & Me.ApplicationID & Me.CustomerID & Me.BranchID & "InqCollActivityHistoryPrint")        
        Response.Redirect("InqCollActivityResult.aspx?AgreementNo=" + Me.AgreementNo + "&ApplicationID=" + Me.ApplicationID + "&CustomerID=" + Me.CustomerID + "&CustomerName=" + Me.CustomerName + "&BranchID=" & Me.BranchID & "&Style=" & Me.Style & "&strFileLocation=" & Me.Session.SessionID & Me.Loginid & "InqCollActivityHistoryPrint")
    End Sub

    Public Function GetData() As DataSet
        Dim ds As New DataSet
        Dim adapter As New SqlDataAdapter
        Dim objCommand As New SqlCommand
        Dim objcon As New SqlConnection(GetConnectionString)

        Try
            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.Connection = objcon
            objCommand.CommandText = "spViewCollActivityPrint"
            objCommand.Parameters.Add("@AgreementNo", SqlDbType.VarChar, 20).Value = Me.AgreementNo.Trim
            objCommand.Parameters.Add("@CustomerID", SqlDbType.VarChar, 20).Value = Me.CustomerID
            objCommand.CommandTimeout = 60

            adapter.SelectCommand = objCommand
            adapter.Fill(ds)
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.GetData")
        End Try

        Return ds
    End Function
    Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies("InqCollActivityResultPrint")
        Me.AgreementNo = cookie.Values("AgreementNo")
        Me.CustomerID = cookie.Values("CustomerID")
        Me.Style = cookie.Values("Style")
        Me.ApplicationID = cookie.Values("ApplicationID")
        Me.CustomerName = cookie.Values("CustomerName")
        Me.BranchID = cookie.Values("BranchID")
    End Sub
End Class