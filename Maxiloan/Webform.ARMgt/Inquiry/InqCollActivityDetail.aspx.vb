﻿#Region "Imports"
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Webform.UserController
#End Region

Public Class InqCollActivityDetail
    Inherits Maxiloan.Webform.WebBased



#Region " Private Const "
    Dim m_CollAct As New CollActivityController

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1    
    Private recordCount As Int64 = 1

#End Region

#Region " Property "

    Private Property CGID() As String
        Get
            Return CStr(viewstate("CGID"))
        End Get
        Set(ByVal Value As String)
            viewstate("CGID") = Value
        End Set
    End Property


    Private Property CGName() As String
        Get
            Return CStr(viewstate("CGName"))
        End Get
        Set(ByVal Value As String)
            viewstate("CGName") = Value
        End Set
    End Property

    Private Property CustomerID() As String
        Get
            Return CStr(viewstate("CustomerID"))
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerID") = Value
        End Set
    End Property

    Private Property CustomerName() As String
        Get
            Return CStr(viewstate("CustomerName"))
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerName") = Value
        End Set
    End Property

    Private Property AgreementNo() As String
        Get
            Return CStr(viewstate("AgreementNo"))
        End Get
        Set(ByVal Value As String)
            viewstate("AgreementNo") = Value
        End Set
    End Property

    Private Property ApplicationID() As String
        Get
            Return CStr(viewstate("ApplicationID"))
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property

    Private Property CollectorID() As String
        Get
            Return CStr(viewstate("CollectorID"))
        End Get
        Set(ByVal Value As String)
            viewstate("CollectorID") = Value
        End Set
    End Property

    Private Property ActivityDate() As Date
        Get
            Return CType(viewstate("ActivityDate"), Date)
        End Get
        Set(ByVal Value As Date)
            viewstate("ActivityDate") = Value
        End Set
    End Property

    Private Property CollSeqNo() As Int16
        Get
            Return CType(viewstate("CollSeqNo"), Int16)
        End Get
        Set(ByVal Value As Int16)
            viewstate("CollSeqNo") = Value
        End Set
    End Property

#End Region

#Region "PAGE LOAD"
    Private Sub PanelAllFalse()        
        PnlList.Visible = False
    End Sub

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        'Put user code to initialize the page here
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not Page.IsPostBack Then
            InitialDefaultPanel()
            Me.CGID = Me.GroubDbID
            Me.FormID = "INQCOLLDCR"

            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then

                Me.AgreementNo = Request.QueryString("AgreementNo")
                Me.ApplicationID = Request.QueryString("ApplicationID")
                Me.CustomerID = Request.QueryString("CustomerID")
                Me.CustomerName = Request.QueryString("CustomerName")
                Me.CollectorID = Request.QueryString("CollectorID")
                'Me.ActivityDate = ConvertDate2(ConvertDate(Request.QueryString("ActivityDate")))
                'Me.ActivityDate = ConvertDate2(Request.QueryString("ActivityDate"))
                Me.ActivityDate = CType(Request.QueryString("ActivityDate"), Date)
                Dim CollSeqNo As String = Request.QueryString("CollSeqNo")
                Context.Trace.Write("CollSeqNo = " & CollSeqNo)
                Me.CollSeqNo = CType(Request.QueryString("CollSeqNo"), Int16)
                Me.BranchID = Request.QueryString("branchID")
                Dim strActivityDate As String
                strActivityDate = ConvertDate(Request.QueryString("ActivityDate"))
                Context.Trace.Write("Me.AgreementNo = " & Me.AgreementNo)
                Context.Trace.Write("Me.ApplicationID = " & Me.ApplicationID)
                Context.Trace.Write("Me.CustomerID = " & Me.CustomerID)
                Context.Trace.Write("Me.CustomerName=" & Me.CustomerName)
                Context.Trace.Write("Me.CollectorID = " & Me.CollectorID)
                Context.Trace.Write("Me.ActivityDate = " & Me.ActivityDate)
                Context.Trace.Write("strActivityDate = " & strActivityDate)
                Context.Trace.Write("Me.CollSeqNo = " & Me.CollSeqNo)
                Context.Trace.Write("Request.QueryString(CollSeqNo) = " & Request.QueryString("CollSeqNo"))

                Dim collAct As New Parameter.CollActivity
                With collAct
                    .strConnection = GetConnectionString()
                    '.ApplicationID = Me.ApplicationID
                    .AgreementNo = Me.AgreementNo
                    .CustomerID = Me.CustomerID
                    .CollectorID = Me.CollectorID
                    .ActivityDate = Me.ActivityDate
                    .CollSeqNo = Me.CollSeqNo
                End With
                collAct = m_CollAct.GetCollActivityDetail(collAct)
                Dim dt As New DataTable
                dt = collAct.ListData
                hpAgreementNo.Text = Me.AgreementNo
                hpCustomerName.Text = Me.CustomerName

                If dt.Rows.Count > 0 Then
                    lblCollectorID.Text = CStr(dt.Rows(0).Item("CollectorID"))
                    lblActivityDate.Text = CStr(dt.Rows(0).Item("ActivityDate"))
                    lblCollectorID.Text = CStr(dt.Rows(0).Item("CollectorID"))
                    lblActivity.Text = CStr(dt.Rows(0).Item("Activity"))
                    lblPTPYDate.Text = CStr(IIf(IsDBNull(dt.Rows(0).Item("PTPYDate")), "-", dt.Rows(0).Item("PTPYDate")))
                    lblActivityResult.Text = CStr(dt.Rows(0).Item("Result"))
                    txtNotes.Text = CStr(dt.Rows(0).Item("Notes"))
                End If
                hpAgreementNo.Attributes.Add("onclick", "return OpenViewAgreement('" + Me.AgreementNo + "')")
                hpCustomerName.Attributes.Add("onclick", "return OpenViewCustomer('" + Me.CustomerID + "')")
                Dim imb As New ImageButton
                imb = CType(Me.FindControl("BtnBack"), ImageButton)
                'imb.Attributes.Add("OnClick", "return fback()")
            End If
        End If
    End Sub
#End Region

#Region " InitialDefaultPanel "
    Private Sub InitialDefaultPanel()
        lblMessage.Visible = False
        PnlList.Visible = True
    End Sub
#End Region

    Private Sub BtnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonBack.Click
        Response.Redirect("InqCollActivityResult.aspx?Style=Collection&ApplicationID=" & Me.ApplicationID.Trim & "&branchID=" & Me.BranchID.Trim & "&CustomerID=" & Me.CustomerID.Trim & "&CustomerName=" & Me.CustomerName.Trim & "&AgreementNO=" & Me.AgreementNo.Trim & "")

    End Sub

End Class