﻿#Region "Imports"
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Webform.UserController
#End Region

Public Class InqCollActivityResult
    Inherits Maxiloan.Webform.WebBased



#Region " Private Const "
    Dim m_CollAct As New CollActivityController

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1    
    Private recordCount As Int64 = 1

#End Region

#Region " Property "

    Private Property CGID() As String
        Get
            Return CStr(viewstate("CGID"))
        End Get
        Set(ByVal Value As String)
            viewstate("CGID") = Value
        End Set
    End Property


    Private Property CGName() As String
        Get
            Return CStr(viewstate("CGName"))
        End Get
        Set(ByVal Value As String)
            viewstate("CGName") = Value
        End Set
    End Property

    Private Property CustomerID() As String
        Get
            Return CStr(viewstate("CustomerID"))
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerID") = Value
        End Set
    End Property

    Private Property CustomerName() As String
        Get
            Return CStr(viewstate("CustomerName"))
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerName") = Value
        End Set
    End Property

    Private Property ApplicationID() As String
        Get
            Return CStr(viewstate("ApplicationID"))
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property

    Private Property AgreementNo() As String
        Get
            Return CStr(viewstate("AgreementNo"))
        End Get
        Set(ByVal Value As String)
            viewstate("AgreementNo") = Value
        End Set
    End Property

    Private Property CollectorID() As String
        Get
            Return CStr(viewstate("CollectorID"))
        End Get
        Set(ByVal Value As String)
            viewstate("CollectorID") = Value
        End Set
    End Property

    Private Property ActivityDate() As Date
        Get
            Return CType(viewstate("ActivityDate"), Date)
        End Get
        Set(ByVal Value As Date)
            viewstate("ActivityDate") = Value
        End Set
    End Property

    Property Style() As String
        Get
            Return viewstate("Style").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("Style") = Value
        End Set
    End Property

    Private Property CollSeqNo() As Int16
        Get
            Return CType(viewstate("CollSeqNo"), Int16)
        End Get
        Set(ByVal Value As Int16)
            viewstate("CollSeqNo") = Value
        End Set
    End Property

#End Region

#Region "PAGE LOAD"
    Private Sub PanelAllFalse()        
        PnlList.Visible = False
    End Sub

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        'Put user code to initialize the page here
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not Page.IsPostBack Then
            Me.CGID = Me.GroubDbID
            Me.FormID = "INQCOLLACTRESULT"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If Request.QueryString("strFileLocation") <> "" Then
                    Dim strHTTPServer As String
                    Dim StrHTTPApp As String
                    Dim strNameServer As String
                    Dim strFileLocation As String

                    strHTTPServer = Request.ServerVariables("PATH_INFO")
                    strNameServer = Request.ServerVariables("SERVER_NAME")
                    StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                    strFileLocation = "http://" & strNameServer & "/" & StrHTTPApp & "/XML/" & Request.QueryString("strFileLocation") & ".pdf"
                    'Call file PDF 
                    Response.Write("<script language = javascript>" & vbCrLf _
                    & "var x = screen.width; var y = screen.height;  " & vbCrLf _
                    & " window.open('" & strFileLocation & "','accacq', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes'); " & vbCrLf _
                    & "</script>")
                End If
                InitialDefaultPanel()
                Me.Style = Request("Style").ToString
                Me.ApplicationID = Request.QueryString("ApplicationID")
                Me.BranchID = Request.QueryString("BranchID")
                Me.CustomerID = Request.QueryString("CustomerID")
                Me.CustomerName = Request.QueryString("CustomerName")
                Me.AgreementNo = Request.QueryString("AgreementNo")


                Context.Trace.Write("Me.ApplicationID  = " & Me.ApplicationID)
                Context.Trace.Write("Me.AgreementNo = " & Me.AgreementNo)
                '===============================================
                'Cek apakah referrer dari layar View Agreement ?
                '===============================================
                'If Request.QueryString("Referrer").Trim = "ViewAgreement" Then
                '    imbBack.Visible = True
                'End If

                hpAgreementNo.Text = Request.QueryString("AgreementNo")
                hpCustomerName.Text = Me.CustomerName


                Me.SearchBy = "all"
                Me.SortBy = ""
                BindGrid(Me.SearchBy, Me.SortBy)
                PnlList.Visible = True

                'hpAgreementNo.Attributes.Add("onclick", "return OpenViewAgreement('" + Me.AgreementNo + "')")
                hpCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "AccAcq" & "', '" & Me.CustomerID & "')"
                hpAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "Collection" & "', '" & Me.AgreementNo.Trim & "')"
                'hpCustomerName.Attributes.Add("onclick", "return OpenViewCustomer('" + Me.CustomerID + "')")

                'Dim imb As New ImageButton
                'imb = CType(Me.FindControl("imbClose"), ImageButton)
                'imb.Attributes.Add("onClick", "return windowClose()")
            End If
        End If
    End Sub
#End Region

#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select

        BindGrid(Me.SearchBy, Me.SortBy)
        PnlList.Visible = True
    End Sub

    Protected Sub btnPageNumb_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPageNumb.Click

        If txtPage.Text = "" Then
            txtPage.Text = "0"
        Else
            If IsNumeric(txtPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                    currentPage = CType(txtPage.Text, Int32)
                    BindGrid(Me.SearchBy, Me.SortBy)

                End If
            End If
        End If

        PnlList.Visible = True

    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If

        If Me.CGID = "" Then Me.CGID = Me.GroubDbID

        BindGrid(Me.SearchBy, Me.SortBy)
        PnlList.Visible = True
    End Sub

#End Region

#Region " InitialDefaultPanel "
    Private Sub InitialDefaultPanel()        
        PnlList.Visible = True
    End Sub
#End Region

#Region " BindGrid"
    Sub BindGrid(ByVal cmdWhere As String, ByVal cmSort As String)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim oCollAct As New Parameter.CollActivity
        Dim oCollActList As New Parameter.CollActivity

        With oCollAct
            .strConnection = GetConnectionString()
            '.ApplicationID = Me.ApplicationID
            .AgreementNo = Me.AgreementNo
            .CustomerID = Me.CustomerID
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oCollActList = m_CollAct.GetCollActivityResultList(oCollAct)

        With oCollActList
            lblrecord.Text = CType(.TotalRecord, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecord
        End With

        dtsEntity = oCollActList.ListData
        dtvEntity = dtsEntity.DefaultView
        dtvEntity.Sort = Me.SortBy
        dtg.DataSource = dtvEntity
        Try
            dtg.DataBind()
        Catch
            dtg.CurrentPageIndex = 0
            dtg.DataBind()
        End Try

        PagingFooter()
    End Sub
#End Region


    Private Sub dtg_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtg.ItemDataBound
        'Dim imbDtl As ImageButton

        'If e.Item.ItemIndex >= 0 Then

        '    imbDtl = CType(e.Item.FindControl("ImbDetail"), ImageButton)
        '    imbDtl.Attributes.Add("onclick", "return OpenWindowDetail('" + Me.AgreementNo + "','" + Me.CustomerID + "','" + Me.CustomerName + "')")

        'End If

    End Sub

    Private Sub dtg_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtg.ItemCommand
        Me.CollectorID = e.Item.Cells(1).Text.Trim
        Me.ActivityDate = ConvertDate2(e.Item.Cells(0).Text.Trim)
        Me.CollSeqNo = CType(e.Item.Cells(6).Text.Trim, Int16)

        'context.Trace.Write(" e.Item.Cells(3).Text.Trim = " & e.Item.Cells(3).Text.Trim)
        'context.Trace.Write(" e.Item.Cells(4).Text.Trim = " & e.Item.Cells(4).Text.Trim)
        'context.Trace.Write(" e.Item.Cells(5).Text.Trim = " & e.Item.Cells(5).Text.Trim)
        'context.Trace.Write(" Me.CollSeqNo = " & Me.CollSeqNo)

        Dim CollSeqNoString As String = CType(Me.CollSeqNo, String)
        'Context.Trace.Write("CollSeqNoString = " & CollSeqNoString)
        'context.Trace.Write(" Me.ActivityDate = " & Me.ActivityDate)

        Select Case e.CommandName
            Case "DETAIL"
                'Response.Redirect("InqCollActivityDetail.aspx?ApplicationID=" + Me.ApplicationID + "&CustomerID=" + Me.CustomerID + "&CustomerName=" + Me.CustomerName + "&CollectorID=" + Me.CollectorID + "&ActivityDate=" + CStr(Me.ActivityDate) + "")
                Dim stringpanjang As String = "InqCollActivityDetail.aspx?AgreementNo=" + Me.AgreementNo + "&ApplicationID=" + Me.ApplicationID + "&CustomerID=" + Me.CustomerID + "&CustomerName=" + Me.CustomerName + "&CollectorID=" + Me.CollectorID + "&CollSeqNo=" + CollSeqNoString + "&ActivityDate=" + CType(Me.ActivityDate, String)
                context.Trace.Write("stringpanjang = " & stringpanjang)
                Response.Redirect("InqCollActivityDetail.aspx?AgreementNo=" + Me.AgreementNo + "&ApplicationID=" + Me.ApplicationID + "&CustomerID=" + Me.CustomerID + "&CustomerName=" + Me.CustomerName + "&CollectorID=" + Me.CollectorID + "&CollSeqNo=" + CollSeqNoString + "&ActivityDate=" + CType(Me.ActivityDate, String) + "&BranchID=" & Me.BranchID & "")
        End Select
    End Sub

    'Private Sub Click_btnBack(ByVal sender As Object, ByVal e As EventArgs) Handles btnBack.Click
    '    Response.Redirect("../../Webform.LoanOrg/Credit/viewStatementOfAccount.aspx?AgreementNo=" & Me.AgreementNo & "&Style=" & Me.Style & "&ApplicationID=" & Me.ApplicationID)
    'End Sub
    Private Sub BtnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnPrint.Click
        If SessionInvalid() Then
            Exit Sub
        End If

        Dim cookie As HttpCookie = Request.Cookies("InqCollActivityResultPrint")

        If Not cookie Is Nothing Then
            cookie.Values("CustomerID") = Me.CustomerID
            cookie.Values("AgreementNo") = Me.AgreementNo
            cookie.Values("Style") = Me.Style
            cookie.Values("ApplicationID") = Me.ApplicationID
            cookie.Values("CustomerName") = Me.CustomerName
            cookie.Values("BranchID") = Me.BranchID
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie("InqCollActivityResultPrint")

            cookieNew.Values.Add("CustomerID", Me.CustomerID)
            cookieNew.Values.Add("AgreementNo", Me.AgreementNo)
            cookieNew.Values.Add("Style", Me.Style)
            cookieNew.Values.Add("ApplicationID", Me.ApplicationID)
            cookieNew.Values.Add("CustomerName", Me.CustomerName)
            cookieNew.Values.Add("BranchID", Me.BranchID)
            Response.AppendCookie(cookieNew)
        End If

        Response.Redirect("InqCollActivityResultViewer.aspx")
    End Sub
End Class