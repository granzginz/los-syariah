﻿#Region "Imports"
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Webform.UserController
#End Region

Public Class InqCollActivity
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents UcCollectorCG As ucCollectorRpt    

#Region " Private Const "
    Dim m_CollAct As New CollActivityController

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1

    Private recordCount As Int64 = 1

#End Region
#Region " Property "

    Private Property DefaultCollector() As String
        Get
            Return CStr(viewstate("DefaultCollector"))
        End Get
        Set(ByVal Value As String)
            viewstate("DefaultCollector") = Value
        End Set
    End Property

    Private Property BindMenu() As String
        Get
            Return CStr(viewstate("BindMenu"))
        End Get
        Set(ByVal Value As String)
            viewstate("BindMenu") = Value
        End Set
    End Property

    Private Property CollectorID() As String
        Get
            Return CStr(viewstate("CollectorID"))
        End Get
        Set(ByVal Value As String)
            viewstate("CollectorID") = Value
        End Set
    End Property

    Private Property CGID() As String
        Get
            Return CStr(viewstate("CGID"))
        End Get
        Set(ByVal Value As String)
            viewstate("CGID") = Value
        End Set
    End Property

    Private Property CGName() As String
        Get
            Return CStr(viewstate("CGName"))
        End Get
        Set(ByVal Value As String)
            viewstate("CGName") = Value
        End Set
    End Property

    Private Property AgreementNo() As String
        Get
            Return CStr(ViewState("AgreementNo"))
        End Get
        Set(ByVal Value As String)
            viewstate("AgreementNo") = Value
        End Set
    End Property

    Private Property CustomerID() As String
        Get
            Return CStr(ViewState("CustomerID"))
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerID") = Value
        End Set
    End Property

    Private Property CustomerName() As String
        Get
            Return CStr(ViewState("CustomerName"))
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerName") = Value
        End Set
    End Property


#End Region

#Region "PAGE LOAD"
    Private Sub PanelAllFalse()        
        PnlSearch.Visible = False
        pnlList.Visible = False
    End Sub

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        'Put user code to initialize the page here
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Context.Trace.Write("Bukan PostBack Lewat Sini !!")
            InitialDefaultPanel()
            Context.Trace.Write("UcCollectorCG.CollectorType = " & UcCollectorCG.CollectorType)
            LblCollector.Text = UcCollectorCG.CollectorName
            UcCollectorCG.CollectorType = ""
            Me.CGID = Me.GroubDbID
            Me.FormID = "CollActivity"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.SearchBy = "all"
                Me.SortBy = ""
                PnlSearch.Visible = True
                pnlList.Visible = False

                txtValidDateFrom.Text = Format(Me.BusinessDate, "dd/MM/yyyy")
                txtValidDateTo.Text = Format(Me.BusinessDate, "dd/MM/yyyy")
            End If
        End If
    End Sub
#End Region

#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString
        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select

        PnlSearch.Visible = True
        BindGrid(Me.SearchBy, Me.SortBy)
        pnlList.Visible = True
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click

        If txtPage.Text = "" Then
            txtPage.Text = "0"
        Else
            If IsNumeric(txtPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                    currentPage = CType(txtPage.Text, Int32)
                    BindGrid(Me.SearchBy, Me.SortBy)

                End If
            End If
        End If

        pnlList.Visible = True

    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If

        If Me.CGID = "" Then Me.CGID = Me.GroubDbID

        BindGrid(Me.SearchBy, Me.SortBy)
        pnlList.Visible = True
    End Sub

#End Region

#Region " InitialDefaultPanel "
    Private Sub InitialDefaultPanel()
        PnlSearch.Visible = True
        pnlList.Visible = False
    End Sub
#End Region

#Region " BindGrid"
    Sub BindGrid(ByVal cmdWhere As String, ByVal cmSort As String)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim oCollAct As New Parameter.CollActivity
        Dim oCollActList As New Parameter.CollActivity


        With oCollAct
            .strConnection = GetConnectionString()
            .BranchId = Me.BranchID
            .CGID = Me.CGID
            .CollectorID = Me.CollectorID
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oCollActList = m_CollAct.GetCollActivityList(oCollAct)

        With oCollActList
            lblrecord.Text = CType(.TotalRecord, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecord
        End With

        dtsEntity = oCollActList.ListData
        dtvEntity = dtsEntity.DefaultView
        dtvEntity.Sort = Me.SortBy
        dtg.DataSource = dtvEntity
        Try
            dtg.DataBind()
        Catch
            dtg.CurrentPageIndex = 0
            dtg.DataBind()
        End Try

        PagingFooter()
    End Sub
#End Region

    Private Sub BtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReset.Click

        'Response.Redirect("InqCollActivity.aspx")
        txtSearchByValue.Text = ""
        InitialDefaultPanel()
        UcCollectorCG.CollectorType = ""
        UcCollectorCG.BindChild()
        BindGrid("ALL", Me.SortBy)

    End Sub


    Private Sub ImbSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSearch.Click
        'Me.SearchBy = ""
        'Me.SortBy = ""
        LblCollector.Text = UcCollectorCG.CollectorID

        Context.Trace.Write("Search ->UcCollectorCG.CollectorType = " & UcCollectorCG.CollectorType)

        With UcCollectorCG
            Me.CGID = .CGIDParent
            Me.CollectorID = .CollectorID
        End With

        Context.Trace.Write("Search ->Me.CGID" & Me.CGID)
        Context.Trace.Write("Search ->Me.CollectorID" & Me.CollectorID)

        If Me.CGID <> "0" Then
            Me.SearchBy = " CGID='" + Me.CGID + "'"
        End If
        If Me.CGID = "0" Then Me.SearchBy = ""

        If Me.CollectorID <> "" And Me.CollectorID <> "0" Then
            Me.SearchBy = Me.SearchBy + " and CollectorID='" + Me.CollectorID + "'"
        Else
            Me.SearchBy = ""
        End If

        If (cboSearchBy.SelectedIndex <> 0) And (txtSearchByValue.Text.Trim <> "") Then
            If Me.SearchBy = "" Then
                If Right(txtSearchByValue.Text.Trim, 1) = "%" Then
                    Me.SearchBy = cboSearchBy.SelectedItem.Value.Trim + " like '" + txtSearchByValue.Text.Trim + "%'"
                Else
                    Me.SearchBy = cboSearchBy.SelectedItem.Value.Trim + "='" + txtSearchByValue.Text.Trim + "'"
                End If
            Else
                If Right(txtSearchByValue.Text.Trim, 1) = "%" Then
                    Me.SearchBy = Me.SearchBy + " and " + cboSearchBy.SelectedItem.Value.Trim + " like '" + txtSearchByValue.Text.Trim + "%'"
                Else
                    Me.SearchBy = Me.SearchBy + " and " + cboSearchBy.SelectedItem.Value.Trim + "='" + txtSearchByValue.Text.Trim + "'"
                End If
            End If
        End If

        If txtValidDateFrom.Text.Trim <> "" And txtValidDateTo.Text.Trim <> "" Then

            Dim datefrom As String
            Dim dateto As String

            'Convert ke yyyy-mm-dd (sesuai di database)
            datefrom = CStr(ConvertDate2(txtValidDateFrom.Text.Trim))
            dateto = CStr(ConvertDate2(txtValidDateTo.Text.Trim))

            If Me.SearchBy = "" Then
                Me.SearchBy = Me.SearchBy + " (ActivityDate between '" + datefrom + "' and '" + dateto + "')"
            Else
                Me.SearchBy = Me.SearchBy + " and (ActivityDate between '" + datefrom + "' and '" + dateto + "')"
            End If
        End If

        'If checkFeature(Me.Loginid, Me.FormID, "Srch", Me.AppId) Then
        '    If sessioninvalid() Then
        '        Exit Sub
        '    End If
        'End If

        'If Me.SortBy = "" Then Me.SortBy = " Agreement.AgreementNo "

        BindGrid(Me.SearchBy, Me.SortBy)
        pnlList.Visible = True


    End Sub

    Private Sub dtg_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtg.ItemCommand
        Me.AgreementNo = e.Item.Cells(2).Text.Trim
        Me.CustomerID = e.Item.Cells(3).Text.Trim
        Me.CustomerName = e.Item.Cells(4).Text.Trim

        'Select Case e.CommandName
        '    Case "Activity"
        '        Response.Redirect("InqCollActivityResult.asp?agreementNo=" & Me.AgreementNo & "&CustomerID=" & Me.CustomerID)
        '    Case "History"
        '        Response.Redirect("InqCollActivityHistory.asp?agreementNo=" & Me.AgreementNo & "&CustomerID=" & Me.CustomerID)

        'End Select
    End Sub

    Private Sub dtg_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtg.ItemDataBound
        Dim imbAct As ImageButton
        Dim imbHist As ImageButton
        Dim hpAgreement As LinkButton
        Dim hpCustomer As LinkButton

        If e.Item.ItemIndex >= 0 Then
            Me.AgreementNo = e.Item.Cells(2).Text.Trim
            Me.CustomerID = e.Item.Cells(3).Text.Trim
            Me.CustomerName = e.Item.Cells(4).Text.Trim

            imbAct = CType(e.Item.FindControl("ImbActivity"), ImageButton)
            imbAct.Attributes.Add("onclick", "return OpenWindowActivity('" + Me.AgreementNo + "','" + Me.CustomerID + "','" + Me.CustomerName + "')")

            imbHist = CType(e.Item.FindControl("ImbHistory"), ImageButton)
            imbHist.Attributes.Add("onclick", "return OpenWindowHistory('" + Me.AgreementNo + "','" + Me.CustomerID + "','" + Me.CustomerName + "')")

            hpAgreement = CType(e.Item.FindControl("hpAgreementNo"), LinkButton)
            hpAgreement.Attributes.Add("onclick", "OpenViewAgreement('" + Me.AgreementNo + "')")

            hpCustomer = CType(e.Item.FindControl("hpCustomerName"), LinkButton)
            hpCustomer.Attributes.Add("onclick", "OpenViewCustomer('" + Me.CustomerID + "')")
        End If

    End Sub

    Private Sub InitializeComponent()

    End Sub


End Class