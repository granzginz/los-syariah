﻿Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports Maxiloan.General.CommonCookiesHelper
Imports System.Data.SqlClient
Imports System.IO
Imports Maxiloan.Webform.UserController

Public Class ViewKewajibanNasabahViewer
    Inherits Maxiloan.Webform.WebBased
#Region "Constanta"
    Private m_coll As New SPPrintController
    Private oCustomClass As New Parameter.SPPrint
#End Region

#Region "Property"
    Private Property BranchID1() As String
        Get
            Return CStr(ViewState("BranchID1"))
        End Get
        Set(ByVal Value As String)
            ViewState("BranchID1") = Value
        End Set
    End Property

    Private Property ApplicationID() As String
        Get
            Return CStr(ViewState("ApplicationID"))
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
    Private Property AgreementNo() As String
        Get
            Return CStr(ViewState("AgreementNo"))
        End Get
        Set(ByVal Value As String)
            ViewState("AgreementNo") = Value
        End Set
    End Property

    Private Property CustomerName() As String
        Get
            Return CStr(ViewState("CustomerName"))
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerName") = Value
        End Set
    End Property
    Private Property CustomerID() As String
        Get
            Return CStr(ViewState("CustomerID"))
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property

    Private Property Alamat() As String
        Get
            Return CStr(ViewState("Alamat"))
        End Get
        Set(ByVal Value As String)
            ViewState("Alamat") = Value
        End Set
    End Property

    Private Property DueDate() As Date
        Get
            Return CType(ViewState("DueDate"), String)
        End Get
        Set(ByVal Value As Date)
            ViewState("DueDate") = Value
        End Set
    End Property

    Private Property TotalKewajibanNasabah() As Decimal
        Get
            Return CType(ViewState("TotalKewajibanNasabah"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            ViewState("TotalKewajibanNasabah") = Value
        End Set
    End Property
#End Region
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        GetCookies()
        BindReport()
    End Sub
    Sub BindReport()
        Dim ds As New DataSet
        Dim objreport As ReportDocument

        objreport = New RptViewKewajibanNasabah

        ds = GetData()
        objreport.SetDataSource(ds)

        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions

        objreport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
        objreport.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat

        Dim strFileLocation As String

        strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
        strFileLocation += Me.Session.SessionID & Me.Loginid & "PrintViewKewajibanNasabah.pdf"
        DiskOpts.DiskFileName = strFileLocation
        objreport.ExportOptions.DestinationOptions = DiskOpts
        objreport.Export()
        objreport.Close()
        objreport.Dispose()
        Response.Redirect("InqKewajibanNasabah.aspx?strFileLocation=" & Me.Session.SessionID & Me.Loginid & "PrintViewKewajibanNasabah")
    End Sub

    Public Function GetData() As DataSet
        Dim ds As New DataSet
        Dim adapter As New SqlDataAdapter
        Dim objCommand As New SqlCommand
        Dim objcon As New SqlConnection(GetConnectionString)

        Try
            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.Connection = objcon
            objCommand.CommandText = "spViewAmortizationKewajibanNasabahReport"
            objCommand.Parameters.Add("@ApplicationID", SqlDbType.VarChar, 20).Value = Me.ApplicationID.Trim
            objCommand.Parameters.Add("@BusinessDate", SqlDbType.DateTime).Value = Me.BusinessDate
            objCommand.Parameters.Add("@DueDate", SqlDbType.DateTime).Value = Me.DueDate
            objCommand.Parameters.Add("@TotalKewajibanNasabah", SqlDbType.Decimal).Value = Me.TotalKewajibanNasabah
            objCommand.CommandTimeout = 60

            adapter.SelectCommand = objCommand
            adapter.Fill(ds)
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.GetData")
        End Try

        Return ds
    End Function
    Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies("RptViewKewajibanNasabahPrint")
        Me.AgreementNo = cookie.Values("AgreementNo")
        Me.CustomerID = cookie.Values("CustomerID")
        Me.ApplicationID = cookie.Values("ApplicationID")
        Me.BranchID1 = cookie.Values("BranchID1")
        Me.CustomerName = cookie.Values("CustomerName")
        Me.Alamat = cookie.Values("Alamat")
        Me.DueDate = cookie.Values("DueDate")
        Me.TotalKewajibanNasabah = cookie.Values("TotalKewajibanNasabah")
    End Sub
End Class