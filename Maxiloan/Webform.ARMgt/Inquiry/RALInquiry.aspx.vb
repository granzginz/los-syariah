﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class RALInquiry
    Inherits Maxiloan.Webform.WebBased

    Protected WithEvents GridNavigator As ucGridNav
#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.RALInquiry
    Private oController As New RALInquiryController

#End Region

#Region "Property"
    Private Property ApplicationID() As String
        Get
            Return CType(viewstate("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If
        AddHandler GridNavigator.PageChanged, AddressOf PageNavigation
        If Not IsPostBack Then
            Me.FormID = "CollInqRAL"
            If CheckForm(Me.Loginid, Me.FormID, "Maxiloan") Then
                InitialDefaultPanel()
                FillCbo()
                ' FillCboExecutor()
                With cboExecutor  
                    .Items.Insert(0, "Select One")
                    .Items(0).Value = ""
                End With
            End If
        End If
    End Sub
    Protected Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        currentPage = e.CurrentPage
        DoBind(True)
        GridNavigator.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
    End Sub
    Private Sub InitialDefaultPanel()
        pnlDtGrid.Visible = False
        pnlsearch.Visible = True        
    End Sub
    Private Sub FillCbo()
        Dim oDataTable As New DataTable
        With oCustomClass
            .strConnection = GetConnectionSTring
            .strKey = "CG"
            .CGID = Me.GroubDbID
            .CollectorType = ""
        End With
        oCustomClass = oController.RALInqListCG(oCustomClass)
        oDataTable = oCustomClass.ListInquiry
        With cboCG
            .DataSource = oDataTable
            .DataTextField = "CGName"
            .DataValueField = "CGID"
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = ""
            If oDataTable.Rows.Count = 1 Then
                .Items.FindByValue(Me.GroubDbID).Selected = True
            End If
        End With
    End Sub

    Private Sub cboExecutorType_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cboExecutorType.SelectedIndexChanged
        Dim type = cboExecutorType.SelectedItem.Value 
        If (type = "") Then
            With cboExecutor
                .Items.Clear()
                .Items.Insert(0, "Select One")
                .Items(0).Value = ""
            End With
            Return
        End If
        FillCboExecutor(type)
    End Sub

    Private Sub FillCboExecutor(type As String)

        Dim oDataTable As New DataTable
        With oCustomClass
            .strConnection = GetConnectionString()
            .CGID = Me.GroubDbID
            .CollectorType = type
        End With
        oCustomClass = oController.RALInqListExecutorCombo(oCustomClass)
        oDataTable = oCustomClass.ListInquiry
        With cboExecutor
            .DataSource = oDataTable
            .DataTextField = "CollectorName"
            .DataValueField = "CollectorID"
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = ""
        End With
    End Sub
    Sub DoBind(Optional isFrNav As Boolean = False)
          
        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereCond = SearchBy
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oCustomClass = oController.RALInqList(oCustomClass)

        Dim DtUserList = oCustomClass.ListInquiry
        Dim DvUserList = DtUserList.DefaultView
        recordCount = oCustomClass.TotalRecord
        DvUserList.Sort = Me.SortBy
        dtgRALInquiry.DataSource = DvUserList
        With oCustomClass
            .strConnection = GetConnectionString()
        End With
        Try
            dtgRALInquiry.DataBind()
        Catch
            dtgRALInquiry.CurrentPageIndex = 0
            dtgRALInquiry.DataBind()
        End Try
        If (isFrNav = False) Then
            GridNavigator.Initialize(recordCount, pageSize)
        End If
        pnlDtGrid.Visible = True
        pnlsearch.Visible = True
    End Sub
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        SortBy = String.Format("{0} {1}", e.SortExpression, IIf(InStr(Me.SortBy, "DESC") > 0, "", "DESC"))
        DoBind()
    End Sub
    Private Sub imgsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Buttonsearch.Click

        SearchBy = String.Format("CollectionAgreement.CGID='{0}'", cboCG.SelectedItem.Value.Trim)

        If (cboExecutorType.SelectedItem.Value.Trim <> "") Then 
            SearchBy = String.Format("{0} and Collector.collectortype {1} 'EP'", SearchBy, IIf(cboExecutorType.SelectedItem.Value.Trim = "EP", "=", "<>"))
            SearchBy = String.Format("{0} {1}", SearchBy, IIf(cboExecutor.SelectedItem.Value.Trim <> "", String.Format("and RAL.ExecutorID='{0}'", cboExecutor.SelectedItem.Value.Trim), ""))
        End If

         
        SearchBy = String.Format("{0} {1}", SearchBy, IIf(cboSearchBy.SelectedItem.Value.Trim <> "", String.Format("and {0} like '%{1}%'", cboSearchBy.SelectedItem.Value.Trim, txtSearchBy.Text.Trim), ""))

        If txtRALDate1.Text <> "" And txtRALDate2.Text <> "" Then
            SearchBy = String.Format("{0}  and RAL.StartDate between '{1}' and '{2}'", SearchBy, ConvertDate(txtRALDate1.Text), ConvertDate(txtRALDate2.Text))
        End If

        SearchBy = String.Format("{0} {1}", SearchBy, IIf(cboRALStatus.SelectedItem.Value.Trim <> "", String.Format("and RAL.RALStatus='{0}'", cboRALStatus.SelectedItem.Value.Trim), ""))

        Me.SortBy = ""

        DoBind()
    End Sub
 

    Private Sub imbReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        cboExecutor.ClearSelection()
        cboSearchBy.ClearSelection()
        cboRALStatus.ClearSelection()
        txtSearchBy.Text = ""
        txtRALDate1.Text = ""
        txtRALDate2.Text = ""
        pnlDtGrid.Visible = False
    End Sub
    Private Sub dtgRALInquiry_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgRALInquiry.ItemCommand
        If e.CommandName = "History" Then
            If checkfeature(Me.Loginid, Me.FormID, "Hist", "Maxiloan") Then 
                Dim hypAgreementNo = CType(dtgRALInquiry.Items(e.Item.ItemIndex).Cells(0).FindControl("hypAgreementNodtg"), HyperLink)
                Dim hypCustomerName = CType(dtgRALInquiry.Items(e.Item.ItemIndex).Cells(1).FindControl("hypCustomerNamedtg"), HyperLink)
                Dim strAgreementNo = hypAgreementNo.Text.Trim
                Dim strCustomerName = hypCustomerName.Text.Trim
                Dim strRALNo = CType(dtgRALInquiry.Items(e.Item.ItemIndex).Cells(2).Text.Trim, String) 

                Dim lblTemp = CType(e.Item.FindControl("lblCustomerId"), Label) 
                Response.Redirect("ExtendHistory.aspx?RALNo=" & strRALNo & "&CustomerId=" & lblTemp.Text.Trim & "&CustomerName=" & strCustomerName & "&AgreementNo=" & strAgreementNo & "&ApplicationId=" & Me.ApplicationID)
            End If
        End If

    End Sub

    Private Sub dtgRALInquiry_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgRALInquiry.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim lblTemp As Label
            Dim hyTemp As HyperLink
            Dim lblApplicationId As Label
            lblApplicationId = CType(e.Item.FindControl("lblApplicationId"), Label)
            Me.ApplicationID = lblApplicationId.Text.Trim
            '*** Customer Link
            lblTemp = CType(e.Item.FindControl("lblCustomerId"), Label)
            hyTemp = CType(e.Item.FindControl("hypCustomerNamedtg"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenCustomer('" & "Collection" & "', '" & Server.UrlEncode(lblTemp.Text.Trim) & "')"
            '*** Agreement No link
            hyTemp = CType(e.Item.FindControl("hypAgreementNodtg"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenAgreementNo('" & "Collection" & "', '" & Server.UrlEncode(lblApplicationId.Text.Trim) & "')"
        End If
    End Sub

End Class