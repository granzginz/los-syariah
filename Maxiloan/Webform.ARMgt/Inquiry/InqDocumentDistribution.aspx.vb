﻿#Region "Imports"
Imports System.Text
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonVariableHelper
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class InqDocumentDistribution
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oCollector As ucCollectorRpt    

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.DocDistribution
    Private oController As New DocDistributionController
    Private CounterColumn As Integer = 0

#End Region
#Region "Property"
    Private Property ApplicationID() As String
        Get
            Return CStr(viewstate("ApplicationID"))
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property
    Private Property oData() As DataTable
        Get
            Return CType(viewstate("oData"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            viewstate("oData") = Value
        End Set
    End Property


#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.FormID = "CollInqDocDist"
            If CheckForm(Me.Loginid, Me.FormID, "Maxiloan") Then
                InitialDefaultPanel()
                oCollector.CollectorType = "CL"
            End If
        End If
    End Sub
    Private Sub InitialDefaultPanel()
        pnlDtGrid.Visible = False
        pnlsearch.Visible = True
    End Sub
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then            
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If



        DoBind(Me.SearchBy, Me.SortBy)

    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click        
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If

                DoBind(Me.SearchBy, Me.SortBy)

            End If
        End If
    End Sub
#End Region
    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        Dim intloop As Integer
        Dim hypID As HyperLink

        With oCustomClass
            .strConnection = GetConnectionString
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oCustomClass = oController.InqDocumentDistributionPaging(oCustomClass)

        DtUserList = oCustomClass.ListDistribution
        DvUserList = DtUserList.DefaultView
        recordCount = oCustomClass.TotalRecord
        DvUserList.Sort = Me.SortBy
        dtgdocumentdistribution.DataSource = DvUserList

        Try
            dtgdocumentdistribution.DataBind()
        Catch
            dtgdocumentdistribution.CurrentPageIndex = 0
            dtgdocumentdistribution.DataBind()
        End Try

        PagingFooter()
        pnlDtGrid.Visible = True
        pnlsearch.Visible = True        
    End Sub
    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Buttonsearch.Click

        Me.SearchBy = "CollectionAgreement.CGID='" & oCollector.CGIDParent.Trim & "'"
        If cboSearchBy.SelectedItem.Value.Trim <> "Select One" Then
            Me.SearchBy = Me.SearchBy & " and " & cboSearchBy.SelectedItem.Value.Trim & " ='" & txtSearchBy.Text.Trim & "'"
        End If
        If cboReceiveStatus.SelectedItem.Value.Trim <> "All" Then
            Me.SearchBy = Me.SearchBy & " and " & cboReceiveStatus.SelectedItem.Value.Trim & "'"
        End If
        If oCollector.CollectorID.Trim <> "" And oCollector.CollectorID.Trim <> "0" Then
            Me.SearchBy = Me.SearchBy & " and documentdistribution.collectorid='" & oCollector.CollectorID.Trim & "'"

        End If
        If txtSentDate1.Text.Trim <> "" And txtSentDate2.Text.Trim = "" Then            
            ShowMessage(lblMessage, "Harap isi Tanggal Kirim", True)
            Exit Sub
        ElseIf txtSentDate1.Text.Trim = "" And txtSentDate2.Text.Trim <> "" Then            
            ShowMessage(lblMessage, "Harap isi Tanggal Kirim", True)
            Exit Sub
        End If

        If txtSentDate1.Text.Trim <> "" And txtSentDate2.Text.Trim <> "" Then

            Dim datefrom As String
            Dim dateto As String

            'Convert ke yyyy-mm-dd (sesuai di database)
            datefrom = CStr(ConvertDate2(txtSentDate1.Text.Trim))
            dateto = CStr(ConvertDate2(txtSentDate2.Text.Trim))
            Me.SearchBy = Me.SearchBy + " and ((documentdistribution.AgreementSentDate between '" + datefrom + "' and '" + dateto + "')or (documentdistribution.PolicySentDate between '" + datefrom + "' and '" + dateto + "') or (documentdistribution.slipSentDate between '" + datefrom + "' and '" + dateto + "')) "

        End If

        Me.SortBy = ""
        DoBind(Me.SearchBy, Me.SortBy)


        With cboReceiveStatus
            .DataValueField = "ID"
            .DataTextField = "Name"

        End With
        oCollector.CollectorID = ""

    End Sub

    Private Sub imbReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        InitialDefaultPanel()
        cboSearchBy.ClearSelection()
        txtSearchBy.Text = ""
    End Sub

    Private Sub dtgdocumentdistribution_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgdocumentdistribution.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim hyTemp As HyperLink
            Dim lblApplicationId As Label


            lblApplicationId = CType(e.Item.FindControl("lblApplicationId"), Label)            
            hyTemp = CType(e.Item.FindControl("hypAgreementNodtg"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenAgreementNo('" & "Collection" & "', '" & Server.UrlEncode(lblApplicationId.Text.Trim) & "')"
        End If

    End Sub


End Class