﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewAssetStatusDetail.aspx.vb"
    Inherits="Maxiloan.Webform.ARMgt.ViewAssetStatusDetail" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ViewAssetStatusDetail</title>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;

        function Close() {
            window.close();
        }
        function fback() {
            history.go(-1);
            return false;
        }

        function OpenViewAgreement(pAgreementNo) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.LoanOrg/Credit/ViewStatementofAccount.aspx?AgreementNo=' + pAgreementNo + '&Style=Collection', 'AgreementView', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenViewCustomer(pCustomerID) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.LoanOrg/Credit/ViewPersonalCustomer.aspx?CustomerID=' + pCustomerID + '&Style=Collection', 'CustomerView', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">           
                <h3>
                    STATUS ASSET
                </h3>
            </div>
        </div>  
        <asp:Panel ID="PnlListDetail" runat="server">
        <div class="form_box">	        
                <div class="form_left">
                    <label>No Kontrak</label>
                    <asp:HyperLink ID="hypAgreementNo" runat="server"></asp:HyperLink>
		        </div>
		        <div class="form_right">		
                    <label>Nama Customer</label>	
                    <asp:HyperLink ID="hypCustomerName" runat="server"></asp:HyperLink>
		        </div>	        
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Nama Asset</label>
                <asp:Label ID="lblAsset" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <label>No Mesin</label>
                    <asp:Label ID="lblEngineNo" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">	    
                    <label>No Rangka</label>		
                    <asp:Label ID="lblChassisNo" runat="server"></asp:Label>
		        </div>	        
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <label>No Polisi</label>
                    <asp:Label ID="lblLicenseNo" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">			
                    <label>Tahun Produksi</label>
                    <asp:Label ID="lblYear" runat="server"></asp:Label>
		        </div>	        
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <label>Warna</label>
                    <asp:Label ID="lblColor" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">			
		        </div>	        
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <%--<label>No SKT</label>--%>
                    <label>No SKE</label>
                    <asp:Label ID="lblRALNo" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">	
                    <%--<label>Tanggal SKT</label>      --%>
                    <label>Tanggal SKE</label>      
                    <asp:Label ID="lblRALDate" runat="server"></asp:Label>              		
		        </div>	        
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <label>Tanggal Tarik</label>
                    <asp:Label ID="lblRepoDate" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">	
                    <label>Ditarik Oleh</label>	
                    <asp:Label ID="lblRepossessBy" runat="server"></asp:Label>	
		        </div>	        
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Status Asset</label>
                <asp:Label ID="lblAssetStatus" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <label>Tanggal Rencana Inventory</label>
                    <asp:Label ID="lblInvExpDate" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">	
                    <label>Tanggal Inventory</label>	
                    <asp:Label ID="lblInventoryDate" runat="server"></asp:Label>	
		        </div>	        
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <label>Nilai Inventory</label>
                    <asp:Label ID="lblInvAmount" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">			
		        </div>	        
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <label>Harga Jual</label>
                    <asp:Label ID="lblSellingPrice" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">	
                    <label>Tanggal Jual</label>		
                    <asp:Label ID="lblSellingDate" runat="server"></asp:Label>
		        </div>	        
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Pembeli</label>
                <asp:Label ID="lblBuyer" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Catatan Penjualan</label>
                <asp:Label ID="lblSellingNotes" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonClose" runat="server" CausesValidation="False"  Text="Close" CssClass ="small button gray" />
	    </div>       
    </asp:Panel>
    </form>
</body>
</html>
