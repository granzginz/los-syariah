﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="RALHistory.aspx.vb" Inherits="Maxiloan.Webform.ARMgt.RALHistory" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>RALHistory</title>
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';			
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;

        function OpenViewCustomer(pCustomerID) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.LoanOrg/Credit/ViewPersonalCustomer.aspx?CustomerID=' + pCustomerID + '&Style=<%=request("style") %>', 'ViewCustomer', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }
        function fClose() {
            window.close();
            return false;
        }	
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">           
                <h3>
                    <%--HISTORY SKT--%>
                    HISTORY SKE
                </h3>
            </div>
        </div>  
        <div class="form_box">
	        <div class="form_single">
                <label>No Kontrak</label>
                <asp:HyperLink ID="lblAgreementNo" runat="server"></asp:HyperLink>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Nama Customer</label>
                <a href="../../Webform.LoanOrg/Credit/ViewPersonalCustomer.aspx?Style=Collection&amp;CustomerId=<%=me.CustomerID%>">
                    <%=me.customername%>
                </a>
	        </div>
        </div>
        <div class="form_box_header">
        <div class="form_single">
        <div class="grid_wrapper_ns"> 
            <asp:DataGrid ID="dtgRALHistory" runat="server" Width="100%" CssClass="grid_general" BorderStyle="None" BorderWidth="0">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
            </asp:DataGrid>
        </div>
        </div>
        </div>
        <div class="form_button">
            <asp:Button runat="server" id="ButtonClose" OnClientClick="fClose();"  Text="Close" CssClass ="small button gray"/>
	    </div>         
    </form>
</body>
</html>
