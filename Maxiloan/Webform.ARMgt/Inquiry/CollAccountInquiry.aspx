﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CollAccountInquiry.aspx.vb" Inherits="Maxiloan.Webform.ARMgt.CollAccountInquiry" %>

<%@ Register TagPrefix="uc1" TagName="UcBranchCollection" Src="../../Webform.UserController/UcBranchCollection.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcMaxAccCollList" Src="../../Webform.UserController/UcMaxAccCollList.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>CollAccountInquiry</title>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;

        function fConfirm() {
            if (window.confirm("Apakah yakin mau hapus data ini ? "))
                return true;
            else
                return false;
        }
        function fback() {
            history.go(-1);
            return false;
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                INQUIRY COLLECTOR ACCOUNT
            </h3>
        </div>
    </div>
    <asp:Panel ID="PnlSearch" runat="server">
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <uc1:ucbranchcollection id="UcCG" runat="server"></uc1:ucbranchcollection>
            </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Collector</label>
                <asp:DropDownList ID="cboDefaultCollector" runat="server" >
                </asp:DropDownList>                
	        </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonSearch" runat="server" Text="Search" CssClass="small button blue">
            </asp:Button>
        </div>
    </asp:Panel>   
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DAFTAR DATA
                </h4>
            </div>
        </div>
        <uc1:UcMaxAccCollList id="UcMaxAccCollList" runat="server"></uc1:UcMaxAccCollList>
    </asp:Panel>
    </form>
</body>
</html>
