﻿#Region "imports"
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class CollAccountInquiry
    Inherits Maxiloan.Webform.WebBased

#Region " Const "
    Dim m_Coll As New CollectorController
    Protected WithEvents UcCG As UcBranchCollection
    Protected WithEvents UcMaxAccCollList As UcMaxAccCollList
    Dim m_CollZipCode As New CollZipCodeController

#End Region
#Region " Property "
    Private Property CGID() As String
        Get
            Return CStr(ViewState("CGID"))
        End Get
        Set(ByVal Value As String)
            ViewState("CGID") = Value
        End Set
    End Property
    Private Property CGName() As String
        Get
            Return CStr(ViewState("CGName"))
        End Get
        Set(ByVal Value As String)
            ViewState("CGName") = Value
        End Set
    End Property
    Public Property ZipCode() As String
        Get
            Return CStr(ViewState("ZipCode"))
        End Get
        Set(ByVal Value As String)
            ViewState("ZipCode") = Value
        End Set
    End Property

#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not Me.IsPostBack Then
            Me.FormID = "ACCCOLLING"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                DefaultPanelFalse()
                UcCG.LoadData()
                getCollectorCombo()
                PnlSearch.Visible = True                
            End If
        End If
    End Sub
    Sub DefaultPanelFalse()        
        pnlList.Visible = False
        PnlSearch.Visible = False
    End Sub

    Private Sub getCollectorCombo()
        Dim oCollZipCode As New Parameter.CollZipCode
        Dim strconn As String = getConnectionString()
        Dim dt As New DataTable

        With oCollZipCode
            .strConnection = getConnectionString()
            .CGID = UcCG.BranchID
            .CollectorType = "CL"
        End With

        dt = m_CollZipCode.GetCollectorCombo(oCollZipCode)
        cboDefaultCollector.DataSource = dt
        cboDefaultCollector.DataTextField = "CollectorName"
        cboDefaultCollector.DataValueField = "CollectorID"
        cboDefaultCollector.DataBind()

        cboDefaultCollector.Items.Insert(0, "Select One")
        cboDefaultCollector.Items(0).Value = "0"
    End Sub

    Private Sub ButtonSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSearch.Click
        bindData()
    End Sub

    Sub bindData()
        Dim where As String = " CGID = '" & UcCG.BranchID & "'"
        DefaultPanelFalse()
        PnlSearch.Visible = True
        pnlList.Visible = True


        If cboDefaultCollector.SelectedValue <> "0" Then
            where += " and CollectorID = '" & cboDefaultCollector.SelectedValue & "'"
        End If


        UcMaxAccCollList.WhereCond = where
        UcMaxAccCollList.Sort = "CollectorID ASC"
        UcMaxAccCollList.Edit = False
        UcMaxAccCollList.DoBind()
    End Sub
End Class