﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class AdminActivity
    
    '''<summary>
    '''form1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents form1 As Global.System.Web.UI.HtmlControls.HtmlForm
    
    '''<summary>
    '''ScriptManager1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ScriptManager1 As Global.System.Web.UI.ScriptManager
    
    '''<summary>
    '''hdnAgreement control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnAgreement As Global.System.Web.UI.HtmlControls.HtmlInputHidden
    
    '''<summary>
    '''hdnCustName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnCustName As Global.System.Web.UI.HtmlControls.HtmlInputHidden
    
    '''<summary>
    '''hdnCustID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnCustID As Global.System.Web.UI.HtmlControls.HtmlInputHidden
    
    '''<summary>
    '''lblMessage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblMessage As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''pnlsearch control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlsearch As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''cboSearchBy control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboSearchBy As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''txtSearchBy control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtSearchBy As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''rfvSearchBy control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rfvSearchBy As Global.System.Web.UI.WebControls.RequiredFieldValidator
    
    '''<summary>
    '''Buttonsearch control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Buttonsearch As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''ButtonReset control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ButtonReset As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''pnlDtGrid control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlDtGrid As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''dtgCLActivity control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents dtgCLActivity As Global.System.Web.UI.WebControls.DataGrid
    
    '''<summary>
    '''imbFirstPage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imbFirstPage As Global.System.Web.UI.WebControls.ImageButton
    
    '''<summary>
    '''imbPrevPage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imbPrevPage As Global.System.Web.UI.WebControls.ImageButton
    
    '''<summary>
    '''imbNextPage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imbNextPage As Global.System.Web.UI.WebControls.ImageButton
    
    '''<summary>
    '''imbLastPage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imbLastPage As Global.System.Web.UI.WebControls.ImageButton
    
    '''<summary>
    '''txtPage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtPage As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''btnPageNumb control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnPageNumb As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''rgvGo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rgvGo As Global.System.Web.UI.WebControls.RangeValidator
    
    '''<summary>
    '''rfvGo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rfvGo As Global.System.Web.UI.WebControls.RequiredFieldValidator
    
    '''<summary>
    '''lblPage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPage As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblTotPage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblTotPage As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblrecord control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblrecord As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''pnlResult control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlResult As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''hypAgreementNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hypAgreementNo As Global.System.Web.UI.WebControls.HyperLink
    
    '''<summary>
    '''hypCustomerName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hypCustomerName As Global.System.Web.UI.WebControls.HyperLink
    
    '''<summary>
    '''lblAddress control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblAddress As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblAsset control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblAsset As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblLicenseNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblLicenseNo As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblCMOName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblCMOName As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblCMOSPVNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblCMOSPVNo As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblCollector control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblCollector As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblLegalPhone1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblLegalPhone1 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblLegalPhone2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblLegalPhone2 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblResidencePhone1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblResidencePhone1 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblResidencePhone2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblResidencePhone2 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblCompanyPhone1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblCompanyPhone1 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblCompanyPhone2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblCompanyPhone2 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblMobilePhone control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblMobilePhone As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblMailingPhone1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblMailingPhone1 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblMailingPhone2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblMailingPhone2 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblisSMS control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblisSMS As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblPDCRequest control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPDCRequest As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblPreviousOverDue control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPreviousOverDue As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblOverDueDays control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblOverDueDays As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblDueDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblDueDate As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblInstallmentNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblInstallmentNo As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblRALNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblRALNo As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblRALDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblRALDate As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblInstallmentAmt control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblInstallmentAmt As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblOSBalance control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblOSBalance As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblOverDueAmt control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblOverDueAmt As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblOSInstallmentAmt control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblOSInstallmentAmt As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblOSInsurance control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblOSInsurance As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblOSBillingCharge control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblOSBillingCharge As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblPDCBounceFee control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPDCBounceFee As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''txtPromiseDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtPromiseDate As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtPromiseDate_CalendarExtender control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtPromiseDate_CalendarExtender As Global.AjaxControlToolkit.CalendarExtender
    
    '''<summary>
    '''cboPlanActivity control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboPlanActivity As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''txtPlanDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtPlanDate As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtPlanDate_CalendarExtender control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtPlanDate_CalendarExtender As Global.AjaxControlToolkit.CalendarExtender
    
    '''<summary>
    '''txtHour control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtHour As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''rgvHour control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rgvHour As Global.System.Web.UI.WebControls.RangeValidator
    
    '''<summary>
    '''txtMinute control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtMinute As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''rgvMinute control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rgvMinute As Global.System.Web.UI.WebControls.RangeValidator
    
    '''<summary>
    '''chkRequestSpv control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkRequestSpv As Global.System.Web.UI.WebControls.CheckBox
    
    '''<summary>
    '''txtNotes control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtNotes As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''cboSuccess control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboSuccess As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''lblActivityHistory control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblActivityHistory As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblCollectionHistory control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblCollectionHistory As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''ButtonSave control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ButtonSave As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''ButtonCancel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ButtonCancel As Global.System.Web.UI.WebControls.Button
End Class
