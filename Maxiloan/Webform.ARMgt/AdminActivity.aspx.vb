﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class AdminActivity
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oAction As ucAction
    Protected WithEvents oActivity As ucCollectorActivity

#Region "Property"
    Private Property ValidDueDate() As String
        Get
            Return CType(viewstate("ValidDueDate"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ValidDueDate") = Value
        End Set
    End Property

    Private Property ApplicationID() As String
        Get
            Return CType(viewstate("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property


    Private Property CGID() As String
        Get
            Return CType(viewstate("CGID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CGID") = Value
        End Set
    End Property

    Private Property NextDueDate() As String
        Get
            Return CType(viewstate("NextDueDate"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("NextDueDate") = Value
        End Set
    End Property

    Private Property CustomerID() As String
        Get
            Return CType(viewstate("CustomerID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerID") = Value
        End Set
    End Property

    Private Property CustomerName() As String
        Get
            Return CType(viewstate("CustomerName"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerName") = Value
        End Set
    End Property
    Private Property AgreementNo() As String
        Get
            Return CType(viewstate("AgreementNo"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AgreementNo") = Value
        End Set
    End Property
#End Region
#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.AdminActivity
    Private oController As New AdminActivityController

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.FormID = "CollAdmAct"
            If CheckForm(Me.Loginid, Me.FormID, "Maxiloan") Then
                Context.Trace.Write("If Not IsPostBack Then")
                pnlDtGrid.Visible = False
                pnlResult.Visible = False
                pnlsearch.Visible = True
                oActivity.strKey = "DC"
                oActivity.CollectorID = ""
            End If
        End If
    End Sub

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        Dim intloop As Integer
        Dim hypID As HyperLink

        With oCustomClass
            .strConnection = GetConnectionString
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oCustomClass = oController.AdminActivityList(oCustomClass)

        DtUserList = oCustomClass.ListCLActivity
        DvUserList = DtUserList.DefaultView
        recordCount = oCustomClass.TotalRecord
        DvUserList.Sort = Me.SortBy
        dtgCLActivity.DataSource = DvUserList

        Try
            dtgCLActivity.DataBind()
        Catch
            dtgCLActivity.CurrentPageIndex = 0
            dtgCLActivity.DataBind()
        End Try
        PagingFooter()
        pnlDtGrid.Visible = True
        pnlResult.Visible = False
        pnlsearch.Visible = True        
    End Sub
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then            
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Protected Sub btnPageNumb_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPageNumb.Click        
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub
#End Region

    Function getColor(ByVal LegalPhoneYN As String) As System.Drawing.Color
        If LegalPhoneYN = "Y" Then
            Return System.Drawing.Color.Blue
        Else
            Return System.Drawing.Color.Black
        End If
    End Function
    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        'pnlDtGrid.Visible = False
        'pnlsearch.Visible = True
        'txtSearchBy.Text = ""
        'cboSearchBy.ClearSelection()
        Response.Redirect("AdminActivity.aspx")
    End Sub

    Private Sub imgsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Buttonsearch.Click
        Me.SearchBy = "CollectionAgreement.CGID='" & Me.GroubDbID.Trim & "'"
        Context.Trace.Write(" Me.SearchBy-1 = " & Me.SearchBy)

        If oActivity.CollectorID <> "" Then
            Me.SearchBy = Me.SearchBy & " and CollectionAgreement.CollectorID='" & oActivity.CollectorID.Trim & "'"
        End If
        Context.Trace.Write("oActivity.CollectorID = " & oActivity.CollectorID)
        Context.Trace.Write(" Me.SearchBy-2 = " & Me.SearchBy)
        oActivity.CollectorID = ""
        Context.Trace.Write("Harusnya oActivity.CollectorID Sudah Kosong = " & oActivity.CollectorID)
        If cboSearchBy.SelectedItem.Value.Trim <> "" And txtSearchBy.Text.Trim <> "" Then
            Me.SearchBy = Me.SearchBy & " and " & cboSearchBy.SelectedItem.Value.Trim & " like '%" + txtSearchBy.Text.Trim + "%'"
        End If
        Context.Trace.Write(" cboSearchBy.SelectedItem.Value.Trim = " & cboSearchBy.SelectedItem.Value.Trim)
        Context.Trace.Write(" Me.SearchBy-3 = " & Me.SearchBy)
        pnlDtGrid.Visible = True
        pnlResult.Visible = False
        Me.SortBy = ""
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub dtgCLActivity_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgCLActivity.ItemDataBound

        Dim lblAgreementNo As HyperLink
        Dim lblApplicationid As Label
        Dim lblCustomerID As Label
        Dim lblCustomerName As HyperLink

        If e.Item.ItemIndex >= 0 Then

            lblAgreementNo = CType(e.Item.FindControl("hypAgreementNodtg"), HyperLink)
            lblApplicationid = CType(e.Item.FindControl("hypApplicationID"), Label)
            lblCustomerID = CType(e.Item.FindControl("lblCustomerID"), Label)
            lblCustomerName = CType(e.Item.FindControl("hypCustomerNamedtg"), HyperLink)
            lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "Collection" & "', '" & lblApplicationid.Text.Trim & "')"
            lblCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "Collection" & "', '" & lblCustomerID.Text & "')"
        End If
    End Sub


    Private Sub dtgCLActivity_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgCLActivity.ItemCommand
        If e.CommandName = "result" Then
            If checkfeature(Me.Loginid, Me.FormID, "Rsult", "Maxiloan") Then

                Dim oDataTable As New DataTable
                Dim lblAgreementNo As HyperLink
                Dim lblApplicationid As Label
                Dim lblCustomerID As Label
                Dim lblCustomerName As HyperLink

                lblAgreementNo = CType(dtgCLActivity.Items(e.Item.ItemIndex).FindControl("hypAgreementNodtg"), HyperLink)
                lblApplicationid = CType(dtgCLActivity.Items(e.Item.ItemIndex).FindControl("hypApplicationID"), Label)
                lblCustomerID = CType(dtgCLActivity.Items(e.Item.ItemIndex).FindControl("lblCustomerID"), Label)
                lblCustomerName = CType(dtgCLActivity.Items(e.Item.ItemIndex).FindControl("hypCustomerNamedtg"), HyperLink)
                Me.AgreementNo = lblAgreementNo.Text
                Me.CustomerID = lblCustomerID.Text
                Me.CustomerName = lblCustomerName.Text
                Me.ApplicationID = lblApplicationid.Text
                With oCustomClass
                    .strConnection = GetConnectionString
                    .ApplicationID = lblApplicationid.Text.Trim
                End With
                oCustomClass = oController.AdminActivityDataAgreement(oCustomClass)
                oDataTable = oCustomClass.ListCLActivity
                pnlDtGrid.Visible = False
                pnlResult.Visible = True
                pnlsearch.Visible = False
                FillData(oDataTable)
            End If
        End If
    End Sub

    Private Sub FillData(ByVal oDataTable As DataTable)
        Dim oRow As DataRow
        Dim oResult As New DataTable
        Dim dtAction As New DataTable
        If oDataTable.Rows.Count > 0 Then
            cboPlanActivity.ClearSelection()
            txtPlanDate.Text = ""
            txtPromiseDate.Text = ""
            txtHour.Text = ""
            txtMinute.Text = ""
            chkRequestSpv.Checked = False
            txtNotes.Text = ""
            cboSuccess.ClearSelection()
            oRow = oDataTable.Rows(0)
            hypAgreementNo.Text = CType(oRow("AgreementNo"), String)
            hypCustomerName.Text = CType(oRow("Name"), String)
            hdnAgreement.Value = CType(oRow("AgreementNo"), String)
            hdnCustName.Value = CType(oRow("Name"), String)
            hdnCustID.Value = CType(oRow("CustomerID"), String)



            hypAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "Collection" & "', '" & Me.ApplicationID & "')"
            hypCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "Collection" & "', '" & Me.CustomerID & "')"
            If Not IsDBNull(oRow("Address")) Then
                lblAddress.Text = CType(oRow("Address"), String)
            End If
            If Not IsDBNull(oRow("Description")) Then
                lblAsset.Text = CType(oRow("Description"), String)
            End If
            If Not IsDBNull(oRow("LicensePlate")) Then
                lblLicenseNo.Text = CType(oRow("LicensePlate"), String)
            End If
            If Not IsDBNull(oRow("EmployeeName")) Then
                lblCMOName.Text = CType(oRow("EmployeeName"), String)
            End If
            If Not IsDBNull(oRow("aospv")) Then
                lblCMOSPVNo.Text = CType(oRow("aospv"), String)
            End If
            If CType(oRow("LegalPhoneYN"), String) = "Y" Then
                lblLegalPhone1.ForeColor = System.Drawing.Color.Blue
                lblLegalPhone2.ForeColor = System.Drawing.Color.Blue
            End If
            If Not IsDBNull(oRow("LegalPhone1")) Then
                lblLegalPhone1.Text = CType(oRow("LegalPhone1"), String)
            End If
            If Not IsDBNull(oRow("LegalPhone2")) Then
                lblLegalPhone2.Text = CType(oRow("LegalPhone2"), String)
            End If
            If CType(oRow("HomePhoneYN"), String) = "Y" Then
                lblResidencePhone1.ForeColor = System.Drawing.Color.Blue
                lblResidencePhone2.ForeColor = System.Drawing.Color.Blue
            End If
            If Not IsDBNull(oRow("ResidencePhone1")) Then
                lblResidencePhone1.Text = CType(oRow("ResidencePhone1"), String)
            End If
            If Not IsDBNull(oRow("ResidencePhone2")) Then
                lblResidencePhone2.Text = CType(oRow("ResidencePhone2"), String)
            End If
            If CType(oRow("CompanyPhoneYN"), String) = "Y" Then
                lblCompanyPhone1.ForeColor = System.Drawing.Color.Blue
                lblCompanyPhone2.ForeColor = System.Drawing.Color.Blue
            End If
            If Not IsDBNull(oRow("CompanyPhone1")) Then
                lblCompanyPhone1.Text = CType(oRow("CompanyPhone1"), String)
            End If
            If Not IsDBNull(oRow("CompanyPhone2")) Then
                lblCompanyPhone2.Text = CType(oRow("CompanyPhone2"), String)
            End If
            If Not IsDBNull(oRow("MobilePhone")) Then
                lblMobilePhone.Text = CType(oRow("MobilePhone"), String)
            End If
            If CType(oRow("MailingPhoneYN"), String) = "Y" Then
                lblMailingPhone1.ForeColor = System.Drawing.Color.Blue
                lblMailingPhone2.ForeColor = System.Drawing.Color.Blue
            End If
            If Not IsDBNull(oRow("MailingPhone1")) Then
                lblMailingPhone1.Text = CType(oRow("MailingPhone1"), String)
            End If
            If Not IsDBNull(oRow("MailingPhone2")) Then
                lblMailingPhone2.Text = CType(oRow("MailingPhone2"), String)
            End If
            If Not IsDBNull(oRow("isSMSSent")) Then
                lblisSMS.Text = CType(oRow("isSMSSent"), String)
            End If
            If Not IsDBNull(oRow("LastPastDueDays")) Then
                lblPreviousOverDue.Text = CType(oRow("LastPastDueDays"), String)
            End If
            If Not (oRow("NextInstallmentDate")) Is Nothing Then
                lblDueDate.Text = Format(CDate(oRow("NextInstallmentDate")), "dd/MM/yyyy")
            End If
            If Not IsDBNull(oRow("StatusPDC")) Then
                lblPDCRequest.Text = CType(oRow("StatusPDC"), String)
            End If
            If Not IsDBNull(oRow("EndPastDueDays")) Then
                lblOverDueDays.Text = CType(oRow("EndPastDueDays"), String)
            End If
            If Not IsDBNull(oRow("NextInstallmentNumber")) Then
                lblInstallmentNo.Text = CType(oRow("NextInstallmentNumber"), String)
            End If
            If Not IsDBNull(oRow("CollectorID")) Then
                lblCollector.Text = CType(oRow("CollectorID"), String)
            End If
            If Not IsDBNull(oRow("InstallmentAmount")) Then
                lblInstallmentAmt.Text = FormatNumber(oRow("InstallmentAmount"), 2)
            End If
            If Not IsDBNull(oRow("EndPastDueAmt")) Then
                Dim EPDAmt As Double = CDbl(oRow("EndPastDueAmt"))
                lblOverDueAmt.Text = FormatNumber(EPDAmt, 2)
                If EPDAmt <= 0 Then                    
                    ShowMessage(lblMessage, "Kontrak ini tidak jatuh Tempo", True)
                End If
            End If
            If Not IsDBNull(oRow("OSLCInstallment")) Then

                lblOSInstallmentAmt.Text = FormatNumber(oRow("OSLCInstallment"), 2)
            End If
            If Not IsDBNull(oRow("OSBillingCharges")) Then
                lblOSBillingCharge.Text = FormatNumber(oRow("OSBillingCharges"), 2)
            End If
            If Not IsDBNull(oRow("OSBalance")) Then
                lblOSBalance.Text = FormatNumber(oRow("OSBalance"), 2)
            End If
            If Not IsDBNull(oRow("OSLCInsurance")) Then
                lblOSInsurance.Text = FormatNumber(oRow("OSLCInsurance"), 2)
            End If
            If Not IsDBNull(oRow("OSPDCBounceFee")) Then
                lblPDCBounceFee.Text = FormatNumber(oRow("OSPDCBounceFee"), 2)
            End If
            If Not IsDBNull(oRow("RALNO")) Then
                lblRALNo.Text = CType(oRow("RALNo"), String)
            End If
            If IsDate(oRow("RALDate")) Then
                lblRALDate.Text = Format(CDate(oRow("RALDate")), "dd/MM/yyyy")
            End If
            If IsDate(oRow("ValidDueDate")) Then
                Me.ValidDueDate = Format(CDate(oRow("ValidDueDate")), "dd/MM/yyyy")
            End If
            If Not IsDBNull(oRow("ApplicationID")) Then
                Me.ApplicationID = CType(oRow("ApplicationID"), String)
            End If
            If IsDate(oRow("NextInstallmentDueDate")) Then
                Me.NextDueDate = Format(CDate(oRow("NextInstallmentDueDate")), "dd/MM/yyyy")
            End If

        Else
            cboPlanActivity.ClearSelection()
            txtPlanDate.Text = ""
            txtPromiseDate.Text = ""
            txtHour.Text = ""
            txtMinute.Text = ""
            chkRequestSpv.Checked = False
            txtNotes.Text = ""
            cboSuccess.ClearSelection()
            hypAgreementNo.Text = ""
            hypCustomerName.Text = ""
            hdnAgreement.Value = ""
            hdnCustName.Value = ""
            hdnCustID.Value = ""
            lblAddress.Text = ""
            lblAsset.Text = ""
            lblLicenseNo.Text = ""
            lblCMOName.Text = ""
            lblLegalPhone1.Text = ""
            lblLegalPhone2.Text = ""
            lblResidencePhone1.Text = ""
            lblResidencePhone2.Text = ""
            lblCompanyPhone1.Text = ""
            lblCompanyPhone2.Text = ""
            lblMobilePhone.Text = ""
            lblMailingPhone1.Text = ""
            lblMailingPhone2.Text = ""
            lblisSMS.Text = ""
            lblPreviousOverDue.Text = ""
            lblDueDate.Text = ""
            lblPDCRequest.Text = ""
            lblOverDueDays.Text = ""
            lblInstallmentNo.Text = ""
            lblCollector.Text = ""
            lblInstallmentAmt.Text = ""
            lblOverDueAmt.Text = ""
            lblOSInstallmentAmt.Text = ""
            lblOSBillingCharge.Text = ""
            lblOSBalance.Text = ""
            lblOSInsurance.Text = ""
            lblPDCBounceFee.Text = ""
            lblRALNo.Text = ""
            lblRALDate.Text = ""
            Me.ValidDueDate = ""
            Me.ApplicationID = ""
            Me.NextDueDate = ""
        End If
        With oCustomClass
            .strConnection = GetConnectionString
            .strKey = ""
        End With
        oCustomClass = oController.AdminActivityAction(oCustomClass)
        dtAction = oCustomClass.ListCLActivity
        With cboPlanActivity
            .DataSource = dtAction
            .DataTextField = "ActionDescription"
            .DataValueField = "ActionID"
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = ""
        End With
    End Sub

    Private Sub imbSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
        Dim strResult As String = oAction.ResultID.Trim    
        Dim oDate As New Maxiloan.Webform.WebBased
        Context.Trace.Write("Kondisi 1")
        If Left(oAction.ResultID.Trim, 4) = "PTPY" Then
            Context.Trace.Write("Kondisi 2")
            If txtPlanDate.Text <> "" Then                
                ShowMessage(lblMessage, "Tidak bisa buat Rencana Kegiatan dan Tanggal Kegiatan, karena yang dipilih adalah Janji Bayar", True)
                Context.Trace.Write("Kondisi 3")
                Exit Sub
            End If
            If cboPlanActivity.SelectedItem.Value.Trim <> "" Then                
                ShowMessage(lblMessage, "Tidak bisa buat Rencana Kegiatan dan Tanggal Kegiatan, karena yang dipilih adalah Janji Bayar", True)
                Context.Trace.Write("Kondisi 4")
                Exit Sub
            End If
            'If cboSuccess.SelectedItem.Value.Trim = "" Then
            '    lblMessage.Text = "Please Select Contact Phone"
            '    Exit Sub
            'End If
            If txtPromiseDate.Text = "" Then                
                ShowMessage(lblMessage, "Harap isi Tanggal janji bayar", True)
                Context.Trace.Write("Kondisi 5")
                Exit Sub
            End If
        Else
            If txtPromiseDate.Text <> "" Then                
                ShowMessage(lblMessage, "Tidak bisa isi Tanggal Janji bayar, Karena Yang dipilih Bukan janji Bayar", True)
                Context.Trace.Write("Kondisi 6")
                Exit Sub
            End If
        End If
        If cboPlanActivity.SelectedItem.Value.Trim <> "" Then
            If txtPlanDate.Text = "" Then            
                ShowMessage(lblMessage, "Harap isi Tanggal Rencana Kegiatan", True)
                Context.Trace.Write("Kondisi 7")
                Exit Sub
            End If
        Else
            If txtPlanDate.Text <> "" Then                
                ShowMessage(lblMessage, "Harap pilih Rencana Kegiatan", True)
                Context.Trace.Write("Kondisi 8")
                Exit Sub
            End If
        End If
        If txtPromiseDate.Text <> "" Then
            'If CLActivity.ConvertDate(txtPromiseDate.Text) > CLActivity.ConvertDate(Me.ValidDueDate) Then
            '    lblMessage.Text = "Promise To pay date can't be greater then " & Me.ValidDueDate
            '    Context.Trace.Write("Kondisi 9")
            '    Exit Sub
            'End If
        End If
        'If txtPlanDate.Text <> "" And CLActivity.ConvertDate(txtPlanDate.Text) > CLActivity.ConvertDate(Me.NextDueDate) Then
        '    lblMessage.Text = "Plan Date can't be greater than Due date"
        '    Context.Trace.Write("Kondisi 10")
        '    Exit Sub
        'End If
        If (oAction.ActionID = "CALL") And (Left(strResult, 4) = "PTPY" Or strResult = "LM" Or strResult = "PAID" Or strResult = "REP" Or strResult = "REQ" Or strResult = "INS") And cboSuccess.SelectedItem.Value = "" Then            
            ShowMessage(lblMessage, "Harap pilih No Telepon yang berhasil di hubungi", True)
            Context.Trace.Write("Kondisi 11")
            Exit Sub
        End If
        'If txtPlanDate.Text <> "" Then
        '    If txtHour.Text.Trim = "" Or txtMinute.Text.Trim = "" Then
        '        lblMessage.Text = "Please fill hour and minute Plan Date"
        '        Context.Trace.Write("Kondisi 12")
        '        Exit Sub
        '    End If
        'End If
        With oCustomClass
            .strConnection = GetConnectionString()
            .AgreementNo = hypAgreementNo.Text.Trim
            .ApplicationID = Me.ApplicationID.Trim
            .Contact = cboSuccess.SelectedItem.Value.Trim
            .isRequest = chkRequestSpv.Checked
            If txtPlanDate.Text <> "" Then
                Context.Trace.Write("Kondisi 10")
                '.PlanDate = oDate.ConvertDate(txtPlanDate.Text) & " " & txtHour.Text.Trim & ":" & txtMinute.Text.Trim
                .PlanDate = oDate.ConvertDate(txtPlanDate.Text)
            Else
                Context.Trace.Write("Kondisi 11")
                .PlanDate = Nothing
            End If
            If txtPromiseDate.Text <> "" Then
                .PTPYDate = oDate.ConvertDate(txtPromiseDate.Text)
            Else
                .PTPYDate = Nothing
            End If
            .ResultID = oAction.ResultID.Trim   'cboResult.SelectedItem.Value.Trim
            .ActivityID = oAction.ActionID.Trim
            .PlanActivityID = cboPlanActivity.SelectedItem.Value.Trim
            .Notes = txtNotes.Text.Trim
            .BusinessDate = Me.BusinessDate
            .CGID = Me.GroubDbID
            .LoginId = Me.Loginid
        End With

        Try
            oCustomClass = oController.AdminActivitySave(oCustomClass)
            If oCustomClass.Hasil = 0 Then
                ShowMessage(lblMessage, "Failed!", True)                
            Else
                ShowMessage(lblMessage, "Data saved!", False)
                pnlsearch.Visible = True
                pnlResult.Visible = False
            End If
        Catch ex As Exception
            'Dim err As New MaxiloanExceptions
            'err.WriteLog("AdminActivity.aspx", "mbSave_Click", err.Source, err.TargetSite.Name, err.Message, err.StackTrace)
            ShowMessage(lblMessage, ex.Message, True)
        End Try


        'If oCustomClass.Hasil = 0 Then            
        '    ShowMessage(lblMessage, "Gagal", True)
        '    Context.Trace.Write("Kondisi 13")
        'End If
        'If lblMessage.Text <> "" Then
        '    Context.Trace.Write("Kondisi 14")
        '    Exit Sub
        'Else
        '    Context.Trace.Write("Kondisi 15")
        '    DoBind(Me.SearchBy, Me.SortBy)
        'End If
    End Sub

    Private Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        Response.Redirect("AdminActivity.aspx")
    End Sub

End Class