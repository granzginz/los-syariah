﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AdminActivity.aspx.vb"
    Inherits="Maxiloan.Webform.ARMgt.AdminActivity" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ucAction" Src="../Webform.UserController/ucAction.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="System.Web.UI.WebControls" Assembly="System.Web" %>
<%@ Register TagPrefix="uc1" TagName="ucCollectorActivity" Src="../Webform.UserController/ucCollectorActivity.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Admin Activity</title>
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%#Request.servervariables("SERVER_NAME")%>/';			
    </script>
    <script language="javascript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;

        function OpenActivity() {
            window.open('Inquiry/InqCollActivityResult.aspx?AgreementNo=' + hdnAgreement.value + '&CustomerName=' + hdnCustName.value + '&CustomerID=' + hdnCustID.value + '&Style=Collection&Referrer=Activity', 'UserLookup', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenCollection() {
            window.open('Inquiry/InqCollActivityHistory.aspx?AgreementNo=' + hdnAgreement.value + '&CustomerName=' + hdnCustName.value + '&CustomerID=' + hdnCustID.value + '', 'UserLookup', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>    
            <input id="hdnAgreement" type="hidden" name="hdnAgreement" runat="server" />
            <input id="hdnCustName" type="hidden" name="hdnCustName" runat="server" />
            <input id="hdnCustID" type="hidden" name="hdnCustID" runat="server" />
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>
                        KEGIATAN ADMIN COLLECTION
                    </h3>
                </div>
            </div>
            <asp:Panel ID="pnlsearch" runat="server">
                <div class="form_box_uc">
                    <uc1:uccollectoractivity id="oActivity" runat="server"></uc1:uccollectoractivity>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Cari Berdasarkan</label>
                        <asp:DropDownList ID="cboSearchBy" runat="server">
                            <asp:ListItem Value="">Select One</asp:ListItem>
                            <asp:ListItem Value="Agreement.AgreementNo">No Kontrak</asp:ListItem>
                            <asp:ListItem Value="Customer.Name">Nama Customer</asp:ListItem>
                            <asp:ListItem Value="AgreementAsset.LicensePlate">No Polisi</asp:ListItem>
                            <asp:ListItem Value="AgreementAsset.SerialNo1">No Rangka</asp:ListItem>
                            <asp:ListItem Value="AgreementAsset.SerialNo2">No Mesin</asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtSearchBy" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvSearchBy" runat="server" ControlToValidate="cboSearchBy"
                            CssClass="validator_general" Display="Dynamic" ErrorMessage="*"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="Buttonsearch" runat="server" Text="Search" CssClass="small button blue">
                    </asp:Button>&nbsp;
                    <asp:Button ID="ButtonReset" runat="server" Text="Reset" CssClass="small button gray"
                        CausesValidation="False"></asp:Button>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlDtGrid" runat="server">
                <div class="form_box_title">
                    <div class="form_single">
                        <h4>
                            DAFTAR KEGIATAN ADMIN COLLECTION
                        </h4>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtgCLActivity" runat="server" Width="100%" OnSortCommand="SortGrid"
                                CssClass="grid_general" BorderStyle="None" BorderWidth="0" DataKeyField="AgreementNo"
                                AutoGenerateColumns="False" AllowSorting="True">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hypAgreementNodtg" runat="server" Text='<%# Container.Dataitem("AgreementNo")%>'>
                                            </asp:HyperLink>
                                            <asp:Label ID="hypApplicationID" Visible="False" runat="server" Text='<%# Container.Dataitem("ApplicationID")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="CustomerName" HeaderText="NAMA CUSTOMER">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCustomerID" Visible="False" runat="server" Text='<%# Container.Dataitem("CustomerID")%>'>
                                            </asp:Label>
                                            <asp:HyperLink ID="hypCustomerNamedtg" runat="server" Text='<%# Container.Dataitem("CustomerName")%>'>
                                            </asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="Days" SortExpression="Days" HeaderText="DAYS">                                
                                    </asp:BoundColumn>
                                    <asp:TemplateColumn SortExpression="LegalPhone1" HeaderText="NOTELP SESUAI KTP">
                                        <ItemTemplate>
                                            <asp:Label ID="lblLegalPhone" runat="server" Text='<%#Container.DataItem("LegalPhone1")%>'
                                                ForeColor='<%# getColor(Container.DataItem("LegalPhoneYN"))%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="ResidencePhone1" HeaderText="NOTELP RUMAH">
                                        <ItemTemplate>
                                            <asp:Label ID="lblResidencePhone" runat="server" Text='<%#Container.DataItem("ResidencePhone1")%>'
                                                ForeColor='<%# getColor(Container.DataItem("HomePhoneYN"))%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="CompanyPhone1" HeaderText="NOTELP KANTOR">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCompanyPhone" runat="server" Text='<%#Container.DataItem("CompanyPhone1")%>'
                                                ForeColor='<%# getColor(Container.DataItem("CompanyPhoneYN"))%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="MobilePhone" SortExpression="MobilePhone" HeaderText="NO HANDPHONE">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="PDCEmptyStatus" SortExpression="PDCEmptyStatus" HeaderText="PDC REQUEST">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="NextInstallmentDuedate" SortExpression="NextInstallmentDuedate"
                                        HeaderText="TGL JT" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="HASIL">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbResult" runat="server" ImageUrl="../images/IconReceived.gif"
                                                CommandName="result"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="ApplicationId" HeaderText="ApplicationId" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblApplicationId" runat="server" Text='<%# Container.Dataitem("ApplicationId")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                            <div class="button_gridnavigation">
                                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../Images/grid_navbutton01.png"
                                    CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../Images/grid_navbutton02.png"
                                    CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../Images/grid_navbutton03.png"
                                    CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../Images/grid_navbutton04.png"
                                    CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                                    EnableViewState="False"></asp:Button>
                                <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" MinimumValue="1"
                                    ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"></asp:RangeValidator>
                                <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                                    ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general">
                                </asp:RequiredFieldValidator>
                            </div>
                            <div class="label_gridnavigation">
                                <asp:Label ID="lblPage" runat="server"></asp:Label>of
                                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlResult" runat="server">
                <div class="form_box_title">
                    <div class="form_single">
                        <h4>
                            HASIL KEGIATAN COLLECTION
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            No Kontrak</label>
                        <asp:HyperLink ID="hypAgreementNo" runat="server"></asp:HyperLink>
                    </div>
                    <div class="form_right">
                        <label>
                            Nama Customer</label>
                        <asp:HyperLink ID="hypCustomerName" runat="server"></asp:HyperLink>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Alamat</label>
                        <asp:Label ID="lblAddress" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Nama Asset</label>
                        <asp:Label ID="lblAsset" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            No Polisi</label>
                        <asp:Label ID="lblLicenseNo" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Nama CMO</label>
                        <asp:Label ID="lblCMOName" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Supervisor CMO</label>
                        <asp:Label ID="lblCMOSPVNo" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Collector</label>
                        <asp:Label ID="lblCollector" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            No Telepon Alamat KTP</label>
                        <asp:Label ID="lblLegalPhone1" runat="server"></asp:Label>
                        <asp:Label ID="lblLegalPhone2" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            No Telepon Rumah</label>
                        <asp:Label ID="lblResidencePhone1" runat="server"></asp:Label>
                        <asp:Label ID="lblResidencePhone2" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            No Telepon Perusahaan</label>
                        <asp:Label ID="lblCompanyPhone1" runat="server"></asp:Label>
                        <asp:Label ID="lblCompanyPhone2" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            No HandPhone</label>
                        <asp:Label ID="lblMobilePhone" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            No Telepon Alamat Tagih</label>
                        <asp:Label ID="lblMailingPhone1" runat="server"></asp:Label>
                        <asp:Label ID="lblMailingPhone2" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Status Kirim SMS</label>
                        <asp:Label ID="lblisSMS" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            PDC Request</label>
                        <asp:Label ID="lblPDCRequest" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Tunggakan Sebelumnya</label>
                        <asp:Label ID="lblPreviousOverDue" runat="server"></asp:Label>&nbsp;days
                    </div>
                    <div class="form_right">
                        <label>
                            Jumlah Hari Telat</label>
                        <asp:Label ID="lblOverDueDays" runat="server"></asp:Label>&nbsp;days
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Tanggal Jatuh Tempo</label>
                        <asp:Label ID="lblDueDate" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Angsuran Ke</label>
                        <asp:Label ID="lblInstallmentNo" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            <%--No SKT--%>
                            No SKE
                        </label>
                        <asp:Label ID="lblRALNo" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            <%--Tanggal SKT--%>
                            Tanggal SKE
                        </label>
                        <asp:Label ID="lblRALDate" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <h4>
                            DATA FINANCIAL
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Jumlah Angsuran</label>
                        <asp:Label ID="lblInstallmentAmt" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Sisa A/R</label>
                        <asp:Label ID="lblOSBalance" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Jumlah Tunggakan</label>
                        <asp:Label ID="lblOverDueAmt" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Denda Keterlambatan Angsuran</label>
                        <asp:Label ID="lblOSInstallmentAmt" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Denda Keterlambatan Asuransi</label>
                        <asp:Label ID="lblOSInsurance" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Biaya Tagih</label>
                        <asp:Label ID="lblOSBillingCharge" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Fee Tolakan PDC</label>
                        <asp:Label ID="lblPDCBounceFee" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <h4>
                            KEGIATAN &amp; HASIL
                        </h4>
                    </div>
                </div>
                <div class="form_box_uc">
                    <uc1:ucaction id="oAction" runat="server"></uc1:ucaction>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Tanggal Janji Bayar</label>
                        <asp:TextBox ID="txtPromiseDate" runat="server"></asp:TextBox>
                        <asp:CalendarExtender ID="txtPromiseDate_CalendarExtender" runat="server" Enabled="True"
                            TargetControlID="txtPromiseDate" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                        <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*" CssClass="validator_general"
                ControlToValidate="txtPromiseDate"></asp:RequiredFieldValidator>--%>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Rencana Kegiatan</label>
                        <asp:DropDownList ID="cboPlanActivity" runat="server">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Tanggal Rencana</label>
                        <asp:TextBox ID="txtPlanDate" runat="server"></asp:TextBox>
                        <asp:CalendarExtender ID="txtPlanDate_CalendarExtender" runat="server" Enabled="True"
                            TargetControlID="txtPlanDate" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                        <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*" CssClass="validator_general"
                ControlToValidate="txtPlanDate"></asp:RequiredFieldValidator>--%>                        
                        <asp:TextBox ID="txtHour" runat="server" Width="36px" Visible="False" MaxLength="2"></asp:TextBox>&nbsp;
                        <asp:RangeValidator ID="rgvHour" runat="server" ControlToValidate="txtHour" Display="Dynamic"
                            CssClass="validator_general" ErrorMessage="*" Enabled="False" MaximumValue="12"
                            MinimumValue="0" Type="Integer"></asp:RangeValidator>&nbsp; :&nbsp;&nbsp;&nbsp;
                        <asp:TextBox ID="txtMinute" runat="server" Width="36px" Visible="False" MaxLength="2"></asp:TextBox>&nbsp;
                        <asp:RangeValidator ID="rgvMinute" runat="server" ControlToValidate="txtMinute" Display="Dynamic"
                            CssClass="validator_general" ErrorMessage="*" Enabled="False" MaximumValue="60"
                            MinimumValue="0" Type="Integer"></asp:RangeValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Perlu Perhatian Supervisor</label>
                        <asp:CheckBox ID="chkRequestSpv" runat="server"></asp:CheckBox>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_general">
                            Catatan</label>
                        <asp:TextBox ID="txtNotes" runat="server" CssClass="multiline_textbox" TextMode="MultiLine"></asp:TextBox>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Telepon yg Berhasil Dihubungi</label>
                        <asp:DropDownList ID="cboSuccess" runat="server">
                            <asp:ListItem>Select One</asp:ListItem>
                            <asp:ListItem Value="LegalPhone">Telepon Alamat KTP</asp:ListItem>
                            <asp:ListItem Value="ResidencePhone">Telepon Rumah</asp:ListItem>
                            <asp:ListItem Value="CompanyPhone">Telepon Kantor</asp:ListItem>
                            <asp:ListItem Value="MailingPhone">Telepon Alamat Tagih</asp:ListItem>
                            <asp:ListItem Value="MobilePhone">HandPhone</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <b><a href="javascript:OpenActivity()">
                            <asp:Label ID="lblActivityHistory" runat="server" Visible="True">HISTORY KEGIATAN</asp:Label></a></b>
                        <b><a href="javascript:OpenCollection()">
                            <asp:Label ID="lblCollectionHistory" runat="server" Visible="True">HISTORY COLLECTION</asp:Label></a></b>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonSave" runat="server" Text="Save" CssClass="small button blue">
                    </asp:Button>&nbsp;
                    <asp:Button ID="ButtonCancel" runat="server" Text="Cancel" CssClass="small button gray">
                    </asp:Button>
                </div>
            </asp:Panel>        
    </form>
</body>
</html>
