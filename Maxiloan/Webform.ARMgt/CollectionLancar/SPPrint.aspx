﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SPPrint.aspx.vb" Inherits="Maxiloan.Webform.ARMgt.SPPrint" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ucCollectorRpt" Src="../../Webform.UserController/ucCollectorRpt.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>SPPrint</title>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%#Request.servervariables("SERVER_NAME")%>/';
    </script>
    <script language="javascript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;
        function OpenWinCustomer(pID) {
            window.open('../../Webform.LoanOrg/Credit/ViewPersonalCustomer.aspx?CustomerID=' + pID + '&style=Collection', 'UserLookup', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }
    </script>
</head>
<body>    
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>    
        <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">           
                <h3>
                    CETAK SURAT PERINGATAN
                </h3>
            </div>
        </div>  
        <asp:Panel ID="pnlsearch" runat="server">     
        <div class="form_box">
                <%--<uc1:ucCollectorRpt id="UcCollector" runat="server"></uc1:ucCollectorRpt>--%>
            <div class="form_single">
            <label class="label_req">Collection Group</label> 
            <asp:DropDownList ID="cboCG" runat="server" Width ="10%" CssClass="medium_text"/>   
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*" CssClass="validator_general"
                ControlToValidate="cboCG"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
                <%--<uc1:ucCollectorRpt id="UcCollector" runat="server"></uc1:ucCollectorRpt>--%>
            <div class="form_single">
                <label class="label_req">Collector</label> 
            <asp:DropDownList ID="cboCN" runat="server" Width ="10%" CssClass="medium_text"/> 
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*" CssClass="validator_general"
                ControlToValidate="cboCN"></asp:RequiredFieldValidator>  
            </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Tanggal Surat Peringatan</label>                
                <asp:TextBox ID="txtSPDate" runat="server"></asp:TextBox>                   
                <asp:CalendarExtender ID="txtSPDate_CalendarExtender" runat="server" 
                    Enabled="True" TargetControlID="txtSPDate" Format ="dd/MM/yyyy"></asp:CalendarExtender>
                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*" CssClass="validator_general"
                ControlToValidate="txtSPDate"></asp:RequiredFieldValidator>--%>
	        </div>
        </div>  
        <div class="form_box">
	        <div class="form_single">
                <label>Cari berdasarkan</label>
                <asp:DropDownList ID="cboSearchBy" runat="server">
                    <asp:ListItem Value="Select One">Select One</asp:ListItem>
                    <asp:ListItem Value="Agreement.AgreementNo">No Kontrak</asp:ListItem>
                    <asp:ListItem Value="Customer.Name">Nama Customer</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearchBy" runat="server" ></asp:TextBox>
	        </div>
        </div> 
        <div class="form_box">
	        <div class="form_single">
                <label>Jenis SP</label>
                <asp:DropDownList ID="cboSPType" runat="server">
                    <asp:ListItem Value="SP">SP1</asp:ListItem>
                    <asp:ListItem Value="SP1">SP2</asp:ListItem>
                    <asp:ListItem Value="SP2">SP3</asp:ListItem>
                </asp:DropDownList>
	        </div>
               <%-- <div class="form_right">
                    <label>
                        Status Cetak
                    </label>
                    <asp:DropDownList ID="ddlIsPrint" runat="server" >
                        <asp:ListItem Value="0" Selected="True">NO</asp:ListItem>
                        <asp:ListItem Value="1">YES</asp:ListItem>
                    </asp:DropDownList>
                </div>--%>
        </div> 
        <div class="form_box">
	        <div class="form_single">
                <label>Produk</label>
                <asp:DropDownList ID="cboProduk" runat="server">
                    <asp:ListItem Value="FACT">Factoring</asp:ListItem>
                    <asp:ListItem Value="MDKJ">Modal Kerja</asp:ListItem>
                    <asp:ListItem Value="AUTO" Selected="True">AUTO</asp:ListItem>
                </asp:DropDownList>
	        </div>
        </div>
        <div class="form_button">
            <asp:Button ID="Buttonsearch" runat="server"  Text="Search" CssClass ="small button blue">
            </asp:Button>&nbsp;
            <asp:Button ID="ButtonReset" runat="server"  Text="Reset" CssClass ="small button gray"
                CausesValidation="False"></asp:Button>
	    </div>                 
        </asp:Panel>    
        <asp:Panel ID="pnlDtGrid" runat="server">

            <div class="form_box_title" style="float:left">
             &nbsp&nbsp&nbsp<asp:Label ID="KepalaCabang" runat="server"> Kepala Cabang   </asp:Label><asp:DropDownList ID="cboauthor1" runat="server" Width ="7%" CssClass="medium_text"/> 
             &nbsp&nbsp&nbsp<asp:Label ID="Rubrik" runat="server"> Rubrik   </asp:Label><asp:DropDownList ID="cborubrik" runat="server" Width ="7%" CssClass="medium_text"/>
             </div>
            

        <div class="form_box_title">
            <div class="form_single">              
                <h4>
                    DAFTAR KONTRAK CETAK SURAT PERINGATAN
                </h4>
            </div>
        </div>
        <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgSPPrinting" runat="server" Width="100%" OnSortCommand="SortGrid"
                        BorderStyle="None" BorderWidth="0"
                        DataKeyField="AgreementNo" AutoGenerateColumns="False" AllowSorting="True" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="CETAK">
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkprint" runat="server"></asp:CheckBox>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="SPTYPE" SortExpression="SPTYPE" HeaderText="JENIS SP">                                
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="SPNO" SortExpression="SPNO" HeaderText="NO SP">                                
                            </asp:BoundColumn>
                            <asp:TemplateColumn SortExpression="AGREEMENTNO" HeaderText="NO KONTRAK">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="hypAgreementNo" runat="server" Text='<%# Container.dataitem("AgreementNo") %>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="CUSTOMERNAME" HeaderText="NAMA CUSTOMER">                                
                                <ItemTemplate>
                                    <a href="javascript:OpenWinCustomer();">
                                        <asp:Label ID="hypCustomerName" runat="server" Text='<%# Container.dataitem("Customername") %>'>
                                        </asp:Label></a>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="INSTALLMENTNO" SortExpression="INSTALLMENTNO" HeaderText="ANGSURAN KE">                                
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="DueDate" SortExpression="DueDate" HeaderText="TGL JATUH TEMPO"
                                DataFormatString="{0:dd/MM/yyyy}">                                
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="AMOUNTOVERDUE" SortExpression="AMOUNTOVERDUE" HeaderText="JUMLAH TUNGGAKAN" HeaderStyle-CssClass="item_grid_center"
                                DataFormatString="{0:###,###,###.00}">
                                <ItemStyle CssClass="item_grid_right"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="LATECHARGES" SortExpression="LATECHARGES" HeaderText="DENDA" HeaderStyle-CssClass="item_grid_center"
                                DataFormatString="{0:###,###,###.00}">
                                <ItemStyle CssClass="item_grid_right"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="CollectorID" SortExpression="CollectorID" HeaderText="COLLECTOR" HeaderStyle-CssClass="item_grid_center">   
                                <ItemStyle CssClass="item_grid_center"></ItemStyle>                             
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="Days" SortExpression="Days" HeaderText="DAYS">                                
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="isPrint" SortExpression="StatusCetak" HeaderText="Status Cetak" HeaderStyle-CssClass="item_grid_center"> 
                                <ItemStyle CssClass="item_grid_center"></ItemStyle>                               
                            </asp:BoundColumn>
                            <asp:TemplateColumn Visible="False" SortExpression="ApplicationId" HeaderText="ApplicationId">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblApplicationId" runat="server" Text='<%# Container.DataItem("ApplicationId")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>                        
                    </asp:DataGrid>       
                <div class="button_gridnavigation">
	                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
		                CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
		                CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
		                CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
		                CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
	                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
		                EnableViewState="False"></asp:Button>
	                <asp:RangeValidator   CssClass="validator_general"   ID="rgvGo" runat="server" ControlToValidate="txtPage"
                        MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer"></asp:RangeValidator>
                    <asp:RequiredFieldValidator    CssClass="validator_general"   ID="rfvGo" runat="server" ControlToValidate="txtPage"
                        ErrorMessage="No Halaman Salah" Display="Dynamic"></asp:RequiredFieldValidator>    
                </div>
                <div class="label_gridnavigation">
	                <asp:Label ID="lblPage" runat="server"></asp:Label>of
	                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
	                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                </div>
            </div>     
        </div>             
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonPrint" runat="server"  Text="Print" CssClass ="small button blue">
            </asp:Button>
	    </div>                
    </asp:Panel>    
    </form>
</body>
</html>
