﻿Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports Maxiloan.General.CommonCookiesHelper


Public Class SSPrintViewer
    Inherits Maxiloan.Webform.WebBased
#Region "Constanta"
    Private m_coll As New SPPrintController
    Private oCustomClass As New Parameter.SPPrint
#End Region

#Region "Property"
    Private Property cmdwhere() As String
        Get
            Return CType(viewstate("cmdwhere"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("cmdwhere") = Value
        End Set
    End Property
#End Region
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        BindReport()
    End Sub
    Sub BindReport()
        GetCookies()
        Dim dtvEntity As New DataView
        Dim dtsEntity As New DataTable
        Dim dsSPPrint As New DataSet
        Dim oSPPrintReport As New Parameter.SPPrint
        Dim intr As Integer = 0
        Dim oReport As RptSrtSelesaiPrinting = New RptSrtSelesaiPrinting


        Dim strconn As String = GetConnectionString()
        With oCustomClass
            .strConnection = GetConnectionString
            .WhereCond = Me.cmdwhere
            .BusinessDate = Me.BusinessDate
        End With

        oSPPrintReport = m_coll.ListReportSSPrinting(oCustomClass)

        oReport.SetDataSource(oSPPrintReport.ListReport)
        'oReport.SetParameterValue(0, Me.BusinessDate)
        'oReport.SetParameterValue(1, Me.sesCompanyName)
        crvSPPrint.ReportSource = oReport
        crvSPPrint.DataBind()

        With oReport.ExportOptions
            .ExportDestinationType = ExportDestinationType.DiskFile
            .ExportFormatType = ExportFormatType.PortableDocFormat
        End With

        Dim doctoprint As New System.Drawing.Printing.PrintDocument
        Dim i As Integer
        'doctoprint.PrinterSettings.PrinterName = "Maxiloan Printer Server"

        For i = 0 To doctoprint.PrinterSettings.PaperSizes.Count - 1
            Dim rawKind As Integer
            If doctoprint.PrinterSettings.PaperSizes(i).PaperName = "rpt_SSPrint" Then
                rawKind = CInt(doctoprint.PrinterSettings.PaperSizes(i).GetType().GetField("kind", Reflection.BindingFlags.Instance Or Reflection.BindingFlags.NonPublic).GetValue(doctoprint.PrinterSettings.PaperSizes(i)))
                oReport.PrintOptions.PaperSize = CType(rawKind, CrystalDecisions.Shared.PaperSize)
                Exit For
            End If
        Next

        Dim strFileLocation As String

        strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
        strFileLocation += Me.Session.SessionID + Me.Loginid + "SSPrint.pdf"

        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = _
        New CrystalDecisions.Shared.DiskFileDestinationOptions

        DiskOpts.DiskFileName = strFileLocation

        With oReport
            .ExportOptions.DestinationOptions = DiskOpts
            .Export()
            .Close()
            .Dispose()
        End With

        Response.Redirect("SSPrint.aspx?file=" & Me.Session.SessionID + Me.Loginid + "SSPrint.pdf")
    End Sub
    Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies("RptSSPrint")
        Me.cmdwhere = cookie.Values("cmdwhere")
    End Sub
End Class