﻿#Region "Imports"
Imports System.Text
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
#End Region

Public Class SPPrint
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents UcCollector As ucCollectorRpt

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.SPPrint
    Private oController As New SPPrintController

#End Region

#Region "Property"
    Private Property AgreementChosen() As String
        Get
            Return CType(ViewState("AgreementChosen"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("AgreementChosen") = Value
        End Set
    End Property
    Private Property IsPrint() As Boolean
        Get
            Return CType(ViewState("IsPrint"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            ViewState("IsPrint") = Value
        End Set
    End Property
    Private Property ApplicationID() As String
        Get
            Return CType(ViewState("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
    Private Property SPType() As String
        Get
            Return CType(ViewState("SPType"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("SPType") = Value
        End Set
    End Property
    Private Property CollectorID() As String
        Get
            Return CType(ViewState("CollectorID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CollectorID") = Value
        End Set
    End Property
    Private Property CGIDParent() As String
        Get
            Return CType(ViewState("CGIDParent"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CGIDParent") = Value
        End Set
    End Property
    Private Property author1() As String
        Get
            Return CType(ViewState("author1"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("author1") = Value
        End Set
    End Property
    Private Property Rubrik1() As String
        Get
            Return CType(ViewState("Rubrik1"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Rubrik1") = Value
        End Set
    End Property
    Private Property Produk() As String
        Get
            Return CType(ViewState("Produk"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Produk") = Value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If



        If Not IsPostBack Then
            If Request.QueryString("file") <> "" Then
                Dim strFileLocation As String
                strFileLocation = "../../XML/" & Request.QueryString("file")

                Response.Write("<script language = javascript>" & vbCrLf _
                                & "var x = screen.width; " & vbCrLf _
                                & "var y = screen.height; " & vbCrLf _
               & "window.open('" & strFileLocation & "','accacq', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes, resizable=1') " & vbCrLf _
               & "</script>")

            End If



            Me.FormID = "COLLSPPRINT"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                pnlDtGrid.Visible = False
                pnlsearch.Visible = True
                txtSPDate.Text = Format(Me.BusinessDate, "dd/MM/yyyy")
                FillCbo()
                FillCbo1()
                'UcCollector.CollectorType = "CL"
                If Me.sesBranchId = "'014'" Then
                    KepalaCabang.Visible = True
                    cboauthor1.Visible = True
                Else
                    KepalaCabang.Visible = False
                    cboauthor1.Visible = False
                End If
            End If
        End If


    End Sub

    Private Sub FillCbo()
        Dim oDataTable As New DataTable
        With oCustomClass
            .strConnection = GetConnectionString()
            .CGID = Me.GroubDbID
            .strKey = "CG"
            .CollectorType = ""
        End With
        oCustomClass = oController.ListCGSPPrint(oCustomClass)
        oDataTable = oCustomClass.ListSPPrint
        With cboCG
            .DataSource = oDataTable
            .DataTextField = "CGName"
            .DataValueField = "CGID"
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = ""
            If oDataTable.Rows.Count = 1 Then
                .Items.FindByValue(Me.GroubDbID).Selected = True
            End If
        End With
    End Sub
    Private Sub FillCbo1()
        Dim oDataTable As New DataTable
        With oCustomClass
            .strConnection = GetConnectionString()
            .CGID = Me.GroubDbID
            .CollectorType = "CN"
            .strKey = ""
        End With
        oCustomClass = oController.ListCGSPPrint(oCustomClass)
        oDataTable = oCustomClass.ListSPPrint
        With cboCN
            .DataSource = oDataTable
            .DataTextField = "Name"
            .DataValueField = "CollectorID"
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = ""
            'If oDataTable.Rows.Count = 1 Then
            '    .Items.FindByValue(Me.GroubDbID).Selected = True
            'End If
        End With
    End Sub
    Private Sub FillRubrik(cborubrik As DropDownList)
        Dim oDataTable As New DataTable
        With oCustomClass
            .strConnection = GetConnectionString()
            .CGID = Me.GroubDbID
        End With
        oCustomClass = oController.ListRubrikPrint(oCustomClass)
        oDataTable = oCustomClass.ListRubrikPrint
        With cborubrik
            .DataSource = oDataTable
            .DataTextField = "Nama_Unit"
            .DataValueField = "Rubrik_Unit"
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = ""
            'If oDataTable.Rows.Count = 1 Then
            '    .Items.FindByValue(Me.GroubDbID).Selected = True
            'End If
        End With
    End Sub
    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Buttonsearch.Click
        Dim strSearch As New StringBuilder
        FillCbo(cboauthor1, 1)
        FillRubrik(cborubrik)
        'strSearch.Append(" AGreementSP.IsPrint = " & CType(ddlIsPrint.SelectedItem.Value, Integer))

        strSearch.Append(" AGreementSP.CGID='" & CType(cboCG.SelectedItem.Value, String) & "' and AGreementSP.CollectorID='" & CType(cboCN.SelectedItem.Value, String) & "'")
        'If UcCollector.CollectorID.Trim <> "" And UcCollector.CollectorID.Trim <> "0" Then
        '    strSearch.Append(" and AGreementSP.CGID='" & UcCollector.CGIDParent.Trim & "' and AGreementSP.CollectorID='" & UcCollector.CollectorID.Trim & "'")
        'Else
        '    strSearch.Append(" and AGreementSP.CGID='" & UcCollector.CGIDParent.Trim & "'")
        'End If
        'Permintaan Mas Danang, karena hasil yang di cetak kelebihan 1 hari
        If txtSPDate.Text.Trim <> "" Then
            'strSearch.Append(" and SPDate=cast( '" & Format((DateAdd(DateInterval.Day, 1, ConvertDate2(txtSPDate.Text.Trim))), "yyyyMMdd") & "' as date)")
            strSearch.Append(" and SPDate >= cast( '" & Format(ConvertDate2(txtSPDate.Text.Trim), "yyyyMMdd") & "' as date)")
        End If
        If cboSearchBy.SelectedItem.Value.Trim <> "Select One" Then
            If Right(txtSearchBy.Text.Trim, 1) = "%" Then
                strSearch.Append(" and " & cboSearchBy.SelectedItem.Value.Trim & " like '%" & txtSearchBy.Text.Trim & "%'")
            Else
                strSearch.Append(" and " & cboSearchBy.SelectedItem.Value.Trim & " like '%" & txtSearchBy.Text.Trim & "%'")
            End If
        End If
        If cboSPType.SelectedItem.Value.Trim <> "" Then
            strSearch.Append(" and SpType='" & cboSPType.SelectedItem.Value.Trim & "'")
        End If

        'If cboProduk.SelectedItem.Value.Trim = "FACT" Then
        '    strSearch.Append(" and productid=35")
        'ElseIf cboProduk.SelectedItem.Value.Trim = "MDKJ" Then
        '    strSearch.Append(" and productid=36")
        'Else
        '    strSearch.Append(" and productid not in (35,36)")
        'End If
        Me.Produk = cboProduk.SelectedItem.Value
        Me.SearchBy = strSearch.ToString
        Me.SortBy = ""
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
    Protected Sub FillCbo(ByVal cboName As DropDownList, ByVal EmployeePosition As String)
        Dim customClass As New Parameter.APDisbApp
        With customClass
            .strConnection = GetConnectionString()
            .BranchId = Replace(Me.sesBranchId, "'", "")
            .EmployeePosition = EmployeePosition
            .CurrentPage = 1
            .PageSize = 100
            .SortBy = ""
            .SpName = "spCboGetEmployeeCollector"
        End With
        customClass = GetEmployee(customClass)
        Dim dt As DataTable
        dt = customClass.listDataReport
        With cboName
            .DataTextField = "EmployeeName"
            .DataValueField = "EmployeeID"
            .DataSource = dt
            .DataBind()
            .Items.Insert(0, "Select One")
            '.Items(0).Value = vbNullChar
            .SelectedIndex = 0
        End With
    End Sub
    Public Function GetEmployee(ByVal CustomClass As Parameter.APDisbApp) As Parameter.APDisbApp
        Dim params() As SqlParameter = New SqlParameter(1) {}
        params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 30)
        params(0).Value = CustomClass.BranchId
        params(1) = New SqlParameter("@EmployeePosition", SqlDbType.VarChar, 30)
        params(1).Value = CustomClass.EmployeePosition


        Try
            CustomClass.listDataReport = SqlHelper.ExecuteDataset(CustomClass.strConnection, CommandType.StoredProcedure, CustomClass.SpName, params).Tables(0)
            Return CustomClass
        Catch exp As Exception
            Throw New Exception(exp.Message)
        End Try
    End Function
    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        pnlDtGrid.Visible = False
        pnlsearch.Visible = True
        'cboCG.ClearSelection()
        txtSPDate.Text = Format(Me.BusinessDate, "dd/MM/yyyy")
    End Sub

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        Dim intloop As Integer
        Dim hypID As HyperLink

        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
            .produk = Me.Produk
        End With

        oCustomClass = oController.ListSPPrinting(oCustomClass)

        DtUserList = oCustomClass.ListSPPrint
        DvUserList = DtUserList.DefaultView
        recordCount = oCustomClass.TotalRecord
        DvUserList.Sort = Me.SortBy
        dtgSPPrinting.DataSource = DvUserList

        Try
            dtgSPPrinting.DataBind()
        Catch
            dtgSPPrinting.CurrentPageIndex = 0
            dtgSPPrinting.DataBind()
        End Try
        PagingFooter()
        pnlDtGrid.Visible = True
        'pnlResult.Visible = False
        pnlsearch.Visible = True
    End Sub
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Protected Sub btnPageNumb_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub
#End Region

    Private Sub imbPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonPrint.Click
        If CheckFeature(Me.Loginid, Me.FormID, "Print", Me.AppId) Then


            IsiAgreementChoosen()
            If AgreementChosen <> "" Then
                Me.SearchBy = Me.SearchBy + " and agreementsp.applicationID in (" & AgreementChosen & ")"
            End If

            Dim hasil As Integer

            With oCustomClass
                .strConnection = GetConnectionString()
                .WhereCond = Me.SearchBy
                .BusinessDate = Me.BusinessDate
                .LoginId = Me.Loginid
            End With

            'Me.IsPrint = ddlIsPrint.SelectedItem.Value
            Me.ApplicationID = AgreementChosen()
            Me.SPType = cboSPType.SelectedItem.Value.Trim()
            Me.CollectorID = cboCN.SelectedItem.Value.Trim()
            Me.CGIDParent = cboCG.SelectedItem.Value.Trim()
            Me.author1 = cboauthor1.SelectedItem.Value.Trim()
            Me.Rubrik1 = cborubrik.SelectedItem.Value.Trim()

            oCustomClass = oController.SPPrintProcess(oCustomClass)
            hasil = oCustomClass.hasil
            If hasil = 1 Then
                Dim cookie As HttpCookie = Request.Cookies("RptSPPrint")
                Dim cmdwhere As String
                'Dim IsPrint As Boolean
                'Dim ApplicationID As String
                'Dim SPType As String
                'Dim CollectorID As String
                'Dim CGIDParent As String
                cmdwhere = Me.SearchBy
                If Not cookie Is Nothing Then
                    cookie.Values("cmdwhere") = cmdwhere
                    cookie.Values("IsPrint") = IsPrint
                    cookie.Values("ApplicationID") = AgreementChosen
                    cookie.Values("SPType") = SPType
                    cookie.Values("CollectorID") = CollectorID
                    cookie.Values("CGIDParent") = CGIDParent
                    cookie.Values("author1") = author1
                    cookie.Values("Rubrik1") = Rubrik1
                    Response.AppendCookie(cookie)
                Else
                    Dim cookieNew As New HttpCookie("RptSPPrint")
                    cookieNew.Values.Add("cmdwhere", cmdwhere)
                    cookieNew.Values.Add("IsPrint", IsPrint)
                    cookieNew.Values.Add("ApplicationID", AgreementChosen)
                    cookieNew.Values.Add("SPType", SPType)
                    cookieNew.Values.Add("CollectorID", CollectorID)
                    cookieNew.Values.Add("CGIDParent", CGIDParent)
                    cookieNew.Values.Add("author1", author1)
                    cookieNew.Values.Add("Rubrik1", Rubrik1)
                    Response.AppendCookie(cookieNew)
                End If
                Response.Redirect("SPPrintViewer.aspx")
            Else
                ShowMessage(lblMessage, "Gagal", True)
            End If

        End If
    End Sub

    Private Sub dtgSPPrinting_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgSPPrinting.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim hyTemp As HyperLink

            Dim lblApplicationId As Label

            lblApplicationId = CType(e.Item.FindControl("lblApplicationId"), Label)
            '*** Agreement No link
            hyTemp = CType(e.Item.FindControl("hypAgreementNo"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenAgreementNo('" & "Collection" & "', '" & Server.UrlEncode(lblApplicationId.Text.Trim) & "')"

        End If
    End Sub

    Sub IsiAgreementChoosen()
        Dim strAgreement As New StringBuilder
        Dim i, j As Integer
        Dim jmlAgreement As Integer
        Dim chkSelect As CheckBox
        Dim lblApplicationID As Label
        Dim isSelect As Boolean
        isSelect = True
        For i = 0 To dtgSPPrinting.Items.Count - 1
            chkSelect = CType(dtgSPPrinting.Items(i).FindControl("chkprint"), CheckBox)
            lblApplicationID = CType(dtgSPPrinting.Items(i).FindControl("lblapplicationid"), Label)
            If chkSelect.Checked = True Then
                strAgreement.Append(CStr(IIf(strAgreement.ToString = "", "", ",")) & "'" & lblApplicationID.Text.Trim & "'")
                isSelect = False
            End If
        Next

        AgreementChosen = strAgreement.ToString()

    End Sub
End Class