﻿Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCacheHelper

Public Class CollectorAllocation
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oBranch As UcBranch

#Region " Const"
    Dim dtCollector As New DataTable
    Dim dt As New DataTable
    Dim m_CollZipCode As New CollZipCodeController
    Dim ControllerCG As New RptCollActController
    Private oController1 As New CLActivityController
    Private oController2 As New CollectorController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

#End Region

#Region " Properties"
    Private Property DefaultCGID() As String
        Get
            Return CStr(viewstate("DefaultCGID"))
        End Get
        Set(ByVal Value As String)
            viewstate("DefaultCGID") = Value
        End Set
    End Property

    Private Property DefaultCollector() As String
        Get
            Return CStr(viewstate("DefaultCollector"))
        End Get
        Set(ByVal Value As String)
            viewstate("DefaultCollector") = Value
        End Set
    End Property

    Private Property DefaultDeskColl() As String
        Get
            Return CStr(viewstate("DefaultDeskColl"))
        End Get
        Set(ByVal Value As String)
            viewstate("DefaultDeskColl") = Value
        End Set
    End Property

    Private Property BindMenu() As String
        Get
            Return CStr(viewstate("BindMenu"))
        End Get
        Set(ByVal Value As String)
            viewstate("BindMenu") = Value
        End Set
    End Property
    Private Property CGID() As String
        Get
            Return CStr(viewstate("CGID"))
        End Get
        Set(ByVal Value As String)
            viewstate("CGID") = Value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If
        lblDefaultCollector.Visible = False
        lblDefaultDC.Visible = False
        If Not Page.IsPostBack Then
            Me.CGID = Me.GroubDbID
            Me.FormID = "COLLALLOC"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                InitialDefaultPanel()
                Me.SearchBy = ""
                Me.SortBy = ""
                getCollectorGroupCombo()
                getCollectorCombo()
                getchildcombo()
            End If
        End If
    End Sub

    Private Sub InitialDefaultPanel()
        PnlSearchDetail.Visible = True
        pnlList.Visible = False
    End Sub

    Private Sub getCollectorCombo()
        Dim oCollZipCode As New Parameter.CollZipCode
        Dim strconn As String = getConnectionString()
        Dim dt As New DataTable
        With oCollZipCode
            .strConnection = getConnectionString()
            .CGID = Me.CGID
            .CollectorType = "CL"
        End With
        dt = m_CollZipCode.GetCollectorCombo(oCollZipCode)
        cboDefaultCollector.DataSource = dt
        cboDefaultCollector.DataTextField = "CollectorName"
        cboDefaultCollector.DataValueField = "CollectorID"
        cboDefaultCollector.DataBind()
        cboDefaultCollector.Items.Insert(0, "Select One")
        cboDefaultCollector.Items(0).Value = "0"
        dtCollector = dt
    End Sub

    Private Sub getCollectorGroupCombo()
        Dim dtParent As New DataTable
        Dim Coll As New Parameter.RptCollAct
        Dim ControllerCG As New RptCollActController
        With Coll
            .strConnection = getconnectionstring()
            .CGID = Me.GroubDbID
            .strKey = "CG"
            .CollectorType = ""
        End With
        Coll = ControllerCG.ViewDataCollector(Coll)
        dtParent = Coll.ListCollector
        cboParent.DataTextField = "CGName"
        cboParent.DataValueField = "CGID"
        cboParent.DataSource = dtParent
        cboParent.DataBind()
        cboParent.Items.Insert(0, "Select One")
        cboParent.Items(0).Value = "0"
    End Sub
    Sub getchildcombo()
        Dim Coll As New Parameter.CLActivity
        Dim CollList As New Parameter.CLActivity
        Dim intr As Integer = 0
        With Coll
            .strConnection = GetConnectionString()
            .CGID = Me.GroubDbID
            .LoginId = Me.Loginid
            .strKey = "DCALL"
        End With
        CollList = oController1.CLActivityListCollector(Coll)
        cboChild.DataSource = CollList.ListCLActivity
        cboChild.DataTextField = "Name"
        cboChild.DataValueField = "CollectorID"
        cboChild.DataBind()
        cboChild.Items.Insert(0, "Select One")
        cboChild.Items(0).Value = "0"
    End Sub   

    Private Sub BtnSearchDetail_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSearchDetail.Click
        If CheckFeature(Me.Loginid, Me.FormID, "Srch", Me.AppId) Then
            If SessionInvalid() Then
                Exit Sub
            End If
        End If

        Me.BindMenu = ""
        Dim strSearch As String

        If cboSearchBy.SelectedItem.Text.ToString = "Collector" Then
            strSearch = " and d.CollectorID = '" & cboCollectorFind.SelectedValue.Trim & "'"
        Else
            strSearch = cboSearchBy.SelectedItem.Text.ToString()
        End If

        Me.SearchBy = strSearch
        Me.SortBy = ""        

        BindGrid(Me.SearchBy, Me.SortBy)

        pnlList.Visible = True
        PnlSearchDetail.Visible = False
    End Sub

    Sub BindGrid(ByVal cmdWhere As String, ByVal cmSort As String)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim oPar As New Parameter.Collector
        With oPar
            .strConnection = GetConnectionString()
            .CGID = Me.CGID
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
            .BranchId = oBranch.BranchID
        End With
        oPar = oController2.GetCollectionAllocationPaging(oPar)
        With oPar
            lblrecord.Text = CType(.TotalRecord, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecord
        End With
        dtsEntity = oPar.ListData
        dtvEntity = dtsEntity.DefaultView
        dtvEntity.Sort = Me.SortBy
        dtgCollZipCode.DataSource = dtvEntity
        Try
            dtgCollZipCode.DataBind()
        Catch
            dtgCollZipCode.CurrentPageIndex = 0
            dtgCollZipCode.DataBind()
        End Try
        PagingFooter()
    End Sub

#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            lblTotPage.Text = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        Me.BindMenu = ""
        BindGrid(Me.SearchBy, Me.SortBy)
        pnlList.Visible = True
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click

        If txtPage.Text = "" Then
            txtPage.Text = "0"
        Else
            If IsNumeric(txtPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                    Me.BindMenu = ""
                    currentPage = CType(txtPage.Text, Int32)
                    BindGrid(Me.SearchBy, Me.SortBy)
                End If
            End If
        End If
    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If

        If Me.CGID = "" Then Me.CGID = Me.GroubDbID

        BindGrid(Me.SearchBy, Me.SortBy)
        pnlList.Visible = True
        PnlSearchDetail.Visible = True
    End Sub

#End Region

    Private Sub ImageButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonButton1.Click
        PnlSearchDetail.Visible = True
        pnlList.Visible = False
    End Sub

    Private Sub dtgCollZipCode_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgCollZipCode.ItemDataBound
        Dim cbocgid As New DropDownList
        Dim cbo As New DropDownList
        Dim cboDC As New DropDownList

        If e.Item.ItemIndex >= 0 Then
            cbocgid = CType(e.Item.FindControl("cboCGID"), DropDownList)
            cbo = CType(e.Item.FindControl("cboCollector"), DropDownList)
            cboDC = CType(e.Item.FindControl("cboDeskCollector"), DropDownList)

            PopulateCboCGIDGrid(cbocgid)
            PopulateCboCollGrid(cbo)
            PopulateCboDeskCollGrid(cboDC)

            If Me.BindMenu = "All" Then
                cbocgid.SelectedIndex = cbocgid.Items.IndexOf(cbocgid.Items.FindByValue(Me.DefaultCGID))
                cbo.SelectedIndex = cbo.Items.IndexOf(cbo.Items.FindByValue(Me.DefaultCollector))
                cboDC.SelectedIndex = cboDC.Items.IndexOf(cboDC.Items.FindByValue(Me.DefaultDeskColl))
            ElseIf Me.BindMenu = "Unallocated" Then
                If e.Item.Cells(1).Text = "" Then cbocgid.SelectedIndex = cbocgid.Items.IndexOf(cbocgid.Items.FindByValue(Me.DefaultCGID))
                If e.Item.Cells(2).Text = "" Then cbo.SelectedIndex = cbo.Items.IndexOf(cbo.Items.FindByValue(Me.DefaultCollector))
                If e.Item.Cells(3).Text = "" Then cboDC.SelectedIndex = cboDC.Items.IndexOf(cboDC.Items.FindByValue(Me.DefaultDeskColl))
            Else
                cbocgid.SelectedIndex = cbocgid.Items.IndexOf(cbocgid.Items.FindByValue(e.Item.Cells(1).Text))
                'cbo.SelectedIndex = cbo.Items.IndexOf(cbo.Items.FindByValue(e.Item.Cells(2).Text))
                cbo.SelectedIndex = cbo.Items.IndexOf(cbo.Items.FindByValue(LTrim(RTrim(e.Item.Cells(2).Text))))
                'cboDC.SelectedIndex = cboDC.Items.IndexOf(cboDC.Items.FindByValue(e.Item.Cells(3).Text))
                cboDC.SelectedIndex = cboDC.Items.IndexOf(cboDC.Items.FindByValue(LTrim(RTrim(e.Item.Cells(3).Text))))
            End If
        End If
    End Sub

    Private Sub imbSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
        Dim oClass As New Parameter.Collector
        Dim x As Int16
        For x = 0 To CType(dtgCollZipCode.Items.Count - 1, Int16)
            With oClass
                .strConnection = GetConnectionString()
                .AppID = dtgCollZipCode.Items(x).Cells(0).Text
                .CGID = CType(dtgCollZipCode.Items(x).Cells(7).FindControl("cboCGID"), DropDownList).SelectedValue.Trim
                .CollectorID = CType(dtgCollZipCode.Items(x).Cells(8).FindControl("cboCollector"), DropDownList).SelectedValue.Trim
                .DeskCollID = CType(dtgCollZipCode.Items(x).Cells(9).FindControl("cboDeskCollector"), DropDownList).SelectedValue.Trim
                .ZipCode = dtgCollZipCode.Items(x).Cells(10).Text
            End With
            Try
                oController2.CollectionAllocationSave(oClass)
            Catch ex As Exception                
                ShowMessage(lblMessage, "Gagal simpan Kontrak No  " & dtgCollZipCode.Items(x).Cells(5).Text, True)
                Exit For
            End Try
        Next
        BindGrid(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbApply_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonApply.Click
        If Not ValidateCombosDefault() Then Exit Sub

        If cboDefaultCollector.SelectedItem.Value <> "0" Then
            Dim cboCGID As New DropDownList
            Dim cboCollector As New DropDownList
            Dim cboDC As New DropDownList
            Dim LoopZipCode As Int16

            If cboCriteria.SelectedItem.Value.Trim = "All" Then
                Me.BindMenu = "All"
                For LoopZipCode = 0 To CType(dtgCollZipCode.Items.Count - 1, Int16)
                    cboCGID = CType(dtgCollZipCode.Items(LoopZipCode).Cells(7).FindControl("cboCGID"), DropDownList)
                    cboCollector = CType(dtgCollZipCode.Items(LoopZipCode).Cells(8).FindControl("cboCollector"), DropDownList)
                    cboDC = CType(dtgCollZipCode.Items(LoopZipCode).Cells(9).FindControl("cboDeskCollector"), DropDownList)

                    cboCGID.SelectedIndex = cboCGID.Items.IndexOf(cboCGID.Items.FindByValue(cboParent.SelectedItem.Value))
                    cboCollector.SelectedIndex = cboCollector.Items.IndexOf(cboCollector.Items.FindByValue(cboDefaultCollector.SelectedItem.Value))
                    cboDC.SelectedIndex = cboDC.Items.IndexOf(cboDC.Items.FindByValue(cboChild.SelectedItem.Value))
                Next
            Else
                Me.BindMenu = "Unallocated"
                For LoopZipCode = 0 To CType(dtgCollZipCode.Items.Count - 1, Int16)
                    cboCGID = CType(dtgCollZipCode.Items(LoopZipCode).Cells(7).FindControl("cboCGID"), DropDownList)
                    cboCollector = CType(dtgCollZipCode.Items(LoopZipCode).Cells(8).FindControl("cboCollector"), DropDownList)
                    cboDC = CType(dtgCollZipCode.Items(LoopZipCode).Cells(9).FindControl("cboDeskCollector"), DropDownList)

                    If dtgCollZipCode.Items(LoopZipCode).Cells(1).Text.Trim = "0" Then _
                        cboCGID.SelectedIndex = cboCGID.Items.IndexOf(cboCGID.Items.FindByValue(cboParent.SelectedItem.Value))

                    If dtgCollZipCode.Items(LoopZipCode).Cells(2).Text.Trim = "0" Then _
                        cboCollector.SelectedIndex = cboCollector.Items.IndexOf(cboCollector.Items.FindByValue(cboDefaultCollector.SelectedItem.Value))

                    If dtgCollZipCode.Items(LoopZipCode).Cells(3).Text.Trim = "0" Then _
                        cboDC.SelectedIndex = cboDC.Items.IndexOf(cboDC.Items.FindByValue(cboChild.SelectedItem.Value))
                Next
            End If

            Me.DefaultCGID = cboParent.SelectedItem.Value
            Me.DefaultCollector = cboDefaultCollector.SelectedItem.Value
            Me.DefaultDeskColl = cboChild.SelectedItem.Value

            pnlList.Visible = True
        End If
    End Sub

    Private Function ValidateCombosDefault() As Boolean
        Dim bResult As Boolean = True
        If cboParent.SelectedIndex <= 0 Then
            lblDefaultCollectionGroup.Visible = True
            bResult = False
        Else
            lblDefaultCollectionGroup.Visible = False
        End If
        If cboDefaultCollector.SelectedIndex <= 0 Then
            lblDefaultCollector.Visible = True
            bResult = False
        Else
            lblDefaultCollector.Visible = False
        End If
        If cboChild.SelectedIndex <= 0 Then
            lblDefaultDC.Visible = True
            bResult = False
        Else
            lblDefaultDC.Visible = False
        End If
        Return bResult
    End Function

    Private Sub PopulateCboCGIDGrid(ByVal cbo As DropDownList)
        Dim Coll As New Parameter.RptCollAct

        With Coll
            .strConnection = getconnectionstring()
            .CGID = Me.GroubDbID
            .strKey = "CG"
            .CollectorType = ""
        End With

        dt = CType(Me.Cache.Item(CACHE_CGID & Me.sesBranchId), DataTable)
        If dt Is Nothing Then
            Dim dtcache As New DataTable
            dtcache = ControllerCG.ViewDataCollector(Coll).ListCollector
            Me.Cache.Insert(CACHE_CGID & Me.sesBranchId, dtcache, Nothing, DateTime.Now.AddHours(DEFAULT_TIME_DURATION_CACHE_HOUR), TimeSpan.Zero)
            dt = CType(Me.Cache.Item(CACHE_CGID & Me.sesBranchId), DataTable)
        End If

        cbo.DataValueField = "CGID"
        cbo.DataTextField = "CGName"
        cbo.DataSource = dt
        cbo.DataBind()
        cbo.Items.Insert(0, "Select One")
        cbo.Items(0).Value = "0"
        dt = Nothing
    End Sub

    Private Sub PopulateCboCollGrid(ByVal cbo As DropDownList)
        Dim oCollZipCode As New Parameter.CollZipCode
        With oCollZipCode
            .strConnection = getConnectionString()
            .CGID = Me.CGID
            .CollectorType = "CL"
        End With

        dt = CType(Me.Cache.Item(CACHE_COLLECTOR & Me.sesBranchId), DataTable)
        If dt Is Nothing Then
            Dim dtcache As New DataTable
            dtcache = m_CollZipCode.GetCollectorCombo(oCollZipCode)
            Me.Cache.Insert(CACHE_COLLECTOR & Me.sesBranchId, dtcache, Nothing, DateTime.Now.AddHours(DEFAULT_TIME_DURATION_CACHE_HOUR), TimeSpan.Zero)
            dt = CType(Me.Cache.Item(CACHE_COLLECTOR & Me.sesBranchId), DataTable)
        End If

        cbo.DataValueField = "collectorID"
        cbo.DataTextField = "collectorName"
        cbo.DataSource = dt
        cbo.DataBind()
        cbo.Items.Insert(0, "Select One")
        cbo.Items(0).Value = "0"
        dt = Nothing
    End Sub

    Private Sub PopulateCboDeskCollGrid(ByVal cbo As DropDownList)
        Dim CollDC As New Parameter.CLActivity
        With CollDC
            .strConnection = getconnectionstring()
            .CGID = Me.GroubDbID
            .LoginId = Me.Loginid
            .strKey = "DCALL"
        End With

        dt = CType(Me.Cache.Item(CACHE_DESKCOLL & Me.sesBranchId), DataTable)
        If dt Is Nothing Then
            Dim dtcache As New DataTable
            dtcache = oController1.CLActivityListCollector(CollDC).ListCLActivity
            Me.Cache.Insert(CACHE_DESKCOLL & Me.sesBranchId, dtcache, Nothing, DateTime.Now.AddHours(DEFAULT_TIME_DURATION_CACHE_HOUR), TimeSpan.Zero)
            dt = CType(Me.Cache.Item(CACHE_DESKCOLL & Me.sesBranchId), DataTable)
        End If

        cbo.DataValueField = "collectorID"
        cbo.DataTextField = "Name"
        cbo.DataSource = dt
        cbo.DataBind()
        cbo.Items.Insert(0, "Select One")
        cbo.Items(0).Value = "0"
        dt = Nothing
    End Sub

    Private Sub cboSearchBy_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboSearchBy.SelectedIndexChanged
        If cboSearchBy.SelectedItem.Text = "Collector" Then
            BindCollectorFind()
            cboCollectorFind.Visible = True
        Else
            cboCollectorFind.Visible = False
        End If
    End Sub

    Private Sub BindCollectorFind()
        Dim oCollZipCode As New Parameter.CollZipCode
        Dim dtx As New DataTable

        With oCollZipCode
            .strConnection = getConnectionString()
            .CGID = Me.CGID
            .CollectorType = "CL"
        End With

        dtx = m_CollZipCode.GetCollectorCombo(oCollZipCode)

        cboCollectorFind.DataValueField = "collectorID"
        cboCollectorFind.DataTextField = "collectorName"
        cboCollectorFind.DataSource = dtx
        cboCollectorFind.DataBind()
        dtx = Nothing
    End Sub

End Class