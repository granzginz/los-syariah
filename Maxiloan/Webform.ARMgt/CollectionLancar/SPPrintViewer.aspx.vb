﻿Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports Maxiloan.General.CommonCookiesHelper

Public Class SPPrintViewer
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private m_coll As New SPPrintController
    Private oCustomClass As New Parameter.SPPrint
#End Region

#Region "Property"
    Private Property cmdwhere() As String
        Get
            Return CType(viewstate("cmdwhere"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("cmdwhere") = Value
        End Set
    End Property
    Private Property AgreementChosen() As String
        Get
            Return CType(ViewState("AgreementChosen"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("AgreementChosen") = Value
        End Set
    End Property
    Private Property IsPrint() As Boolean
        Get
            Return CType(ViewState("IsPrint"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            ViewState("IsPrint") = Value
        End Set
    End Property
    Private Property ApplicationID() As String
        Get
            Return CType(ViewState("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
    Private Property SPType() As String
        Get
            Return CType(ViewState("SPType"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("SPType") = Value
        End Set
    End Property
    Private Property CollectorID() As String
        Get
            Return CType(ViewState("CollectorID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CollectorID") = Value
        End Set
    End Property
    Private Property CGIDParent() As String
        Get
            Return CType(ViewState("CGIDParent"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CGIDParent") = Value
        End Set
    End Property
    Private Property author1() As String
        Get
            Return CType(ViewState("author1"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("author1") = Value
        End Set
    End Property
    Private Property Rubrik1() As String
        Get
            Return CType(ViewState("Rubrik1"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Rubrik1") = Value
        End Set
    End Property

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If
        BindReport()
    End Sub
    Sub BindReport()
        GetCookies()
        Dim dtvEntity As New DataView
        Dim dtsEntity As New DataTable
        Dim dsSPPrint As New DataSet
        Dim oSPPrintReport As New Parameter.SPPrint
        Dim intr As Integer = 0
        Dim oReport As RptSPPrinting = New RptSPPrinting


        Dim strconn As String = GetConnectionString()
        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereCond = " AgreementSP.SpType='" & SPType & "'" + " and AGreementSP.CGID='" & CGIDParent & "' and AGreementSP.CollectorID='" & CollectorID & "'" + " and agreementsp.applicationID in (" & AgreementChosen & ")"
            .ApplicationID = Me.AgreementChosen
            .author1 = Me.author1
            .Rubrik1 = Me.Rubrik1
            .BusinessDate = Me.BusinessDate
        End With

        oSPPrintReport = m_coll.ListReportSPPrinting(oCustomClass)

        oReport.SetDataSource(oSPPrintReport.ListReport)
        oReport.SetParameterValue(0, Me.BusinessDate)
        oReport.SetParameterValue(1, Me.sesCompanyName)
        crvSPPrint.ReportSource = oReport
        crvSPPrint.DataBind()

        With oReport.ExportOptions
            .ExportDestinationType = ExportDestinationType.DiskFile
            .ExportFormatType = ExportFormatType.PortableDocFormat
        End With

        Dim doctoprint As New System.Drawing.Printing.PrintDocument
        Dim i As Integer
        'doctoprint.PrinterSettings.PrinterName = "Maxiloan Printer Server"

        For i = 0 To doctoprint.PrinterSettings.PaperSizes.Count - 1
            Dim rawKind As Integer
            If doctoprint.PrinterSettings.PaperSizes(i).PaperName = "rpt_SPPrint" Then
                rawKind = CInt(doctoprint.PrinterSettings.PaperSizes(i).GetType().GetField("kind", Reflection.BindingFlags.Instance Or Reflection.BindingFlags.NonPublic).GetValue(doctoprint.PrinterSettings.PaperSizes(i)))
                oReport.PrintOptions.PaperSize = CType(rawKind, CrystalDecisions.Shared.PaperSize)
                Exit For
            End If
        Next

        Dim strFileLocation As String

        strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
        strFileLocation += Me.Session.SessionID + Me.Loginid + "SPPrint.pdf"

        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = _
        New CrystalDecisions.Shared.DiskFileDestinationOptions

        DiskOpts.DiskFileName = strFileLocation

        With oReport
            .ExportOptions.DestinationOptions = DiskOpts
            .Export()
            .Close()
            .Dispose()
        End With

        Response.Redirect("SPPrint.aspx?file=" & Me.Session.SessionID + Me.Loginid + "SPPrint.pdf")
    End Sub
    Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies("RptSPPrint")
        Me.cmdwhere = cookie.Values("cmdwhere")
        'Me.IsPrint = cookie.Values("IsPrint")
        Me.AgreementChosen = cookie.Values("ApplicationID")
        Me.SPType = cookie.Values("SPType")
        Me.CollectorID = cookie.Values("CollectorID")
        Me.CGIDParent = cookie.Values("CGIDParent")
        Me.author1 = cookie.Values("author1")
        Me.Rubrik1 = cookie.Values("Rubrik1")
    End Sub


End Class