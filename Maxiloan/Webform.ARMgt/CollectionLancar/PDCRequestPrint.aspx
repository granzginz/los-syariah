﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PDCRequestPrint.aspx.vb"
    Inherits="Maxiloan.Webform.ARMgt.PDCRequestPrint" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PDCRequestPrint</title>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%#Request.servervariables("SERVER_NAME")%>/';
    </script>
    <script language="javascript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;
        function OpenWinCustomer(pID) {
            window.open('../../Webform.LoanOrg/Credit/ViewPersonalCustomer.aspx?CustomerID=' + pID + '&style=Collection', 'UserLookup', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>    
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>    
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">           
            <h3>
                PERMINTAAN PDC
            </h3>
        </div>
    </div>  
    <asp:Panel ID="pnlsearch" runat="server">        
        <div class="form_box">	        
                <div class="form_left">
                    <label class ="label_req">Collection Group</label>
                    <asp:DropDownList ID="cboCG" runat="server">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="rfvCG" runat="server" Display="Dynamic" ControlToValidate="cboCG" CssClass="validator_general"
                    ErrorMessage="*"></asp:RequiredFieldValidator>
		        </div>
		        <div class="form_right">
                    <label>Cari Berdasarkan</label>	
                    <asp:DropDownList ID="cboSearchBy" runat="server">
                        <asp:ListItem Value="IsPrintReminderLetter">Reminder Letter</asp:ListItem>
                        <asp:ListItem Value="IsPrintRequestLetter">Request Letter</asp:ListItem>
                    </asp:DropDownList>		
		        </div>	        
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <label class ="label_req">Tanggal Jatuh Tempo</label>                     
                     <asp:TextBox ID="txtTaskDate1" Width="60px" runat="server"></asp:TextBox>                   
                    <asp:CalendarExtender ID="txtTaskDate1_CalendarExtender" runat="server" 
                        Enabled="True" TargetControlID="txtTaskDate1" Format ="dd/MM/yyyy"></asp:CalendarExtender>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*" CssClass="validator_general"
                    ControlToValidate="txtTaskDate1"></asp:RequiredFieldValidator>                     
                     To                    
                    <asp:TextBox ID="txtTaskDate2" Width="60px" runat="server"></asp:TextBox>                   
                    <asp:CalendarExtender ID="txtTaskDate2_CalendarExtender" runat="server" 
                        Enabled="True" TargetControlID="txtTaskDate2" Format ="dd/MM/yyyy"></asp:CalendarExtender>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*" CssClass="validator_general"
                    ControlToValidate="txtTaskDate2"></asp:RequiredFieldValidator>
		        </div>
		        <div class="form_right">	
                    <label>Status Cetak</label>		
                    <asp:DropDownList ID="cboisPrinted" runat="server">
                        <asp:ListItem Value="all" Selected="True">ALL</asp:ListItem>
                        <asp:ListItem Value="1">Yes</asp:ListItem>
                        <asp:ListItem Value="0">No</asp:ListItem>
                    </asp:DropDownList>
		        </div>	        
        </div>        
        <div class="form_button">
            <asp:Button ID="Buttonsearch" runat="server"  Text="Search" CssClass ="small button blue">
            </asp:Button>&nbsp;
            <asp:Button ID="ButtonReset" runat="server"  Text="Reset" CssClass ="small button gray"
                CausesValidation="False"></asp:Button>
	    </div>        
    </asp:Panel>    
    <asp:Panel ID="pnlDtGrid" runat="server">
        <div class="form_box_title">
            <div class="form_single">              
                <h4>
                    DAFTAR PERMINTAAN PDC
                </h4>
            </div>
        </div>
        <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgPDCRequest" runat="server" Width="100%" OnSortCommand="SortGrid"
                        BorderStyle="None" BorderWidth="0"
                        DataKeyField="AgreementNo" AutoGenerateColumns="False" AllowSorting="True" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn Visible="False">                                
                                <HeaderTemplate>
                                    <asp:CheckBox ID="chkAll" runat="server" AutoPostBack="True" OnCheckedChanged="checkAll">
                                    </asp:CheckBox>
                                </HeaderTemplate>                               
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkPDC" runat="server"></asp:CheckBox>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AGREEMENTNO" HeaderText="NO KONTRAK">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="CUSTOMERNAME" HeaderText="NAMA CUSTOMER">                                
                                <ItemTemplate>
                                    <a href="javascript:OpenWinCustomer('<%# Container.DataItem("CustomerID")%>');">
                                        <asp:Label ID="lblCustomerName" runat="server" Text='<%#Container.Dataitem("CustomerName")%>'>
                                        </asp:Label></a>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="NEXTINSTALLMENTNO" SortExpression="NEXTINSTALLMENTNO"
                                HeaderText="NO ANGS BERIKUT">                                
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="DUEDATE" SortExpression="DUEDATE" HeaderText="TGL JATUH TEMPO"
                                DataFormatString="{0:dd/MM/yyyy}">                                
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="COLLECTORID" SortExpression="COLLECTORID" HeaderText="COLLECTOR">                                
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="ISPRINTREMINDERLETTER" SortExpression="ISPRINTREMINDERLETTER"
                                HeaderText="CETAK PDC REMINDER LETTER">                                
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="ISPRINTREQUESTLETTER" SortExpression="ISPRINTREQUESTLETTER"
                                HeaderText="CETAK PDC REQUEST LETTER">                                
                            </asp:BoundColumn>
                            <asp:TemplateColumn SortExpression="ApplicationId" HeaderText="ApplicationId" Visible="False">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblApplicationId" runat="server" Text='<%# Container.Dataitem("ApplicationId")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>                        
                    </asp:DataGrid>       
                <div class="button_gridnavigation">
	                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
		                CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
		                CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
		                CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
		                CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
	                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
		                EnableViewState="False"></asp:Button>
	                <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage"
		                MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer"
		                CssClass="validator_general"></asp:RangeValidator>
	                <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
		                ErrorMessage="No Halaman Salah" MinimumValue="1" type="integer" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>            
                </div>
                <div class="label_gridnavigation">
	                <asp:Label ID="lblPage" runat="server"></asp:Label>of
	                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
	                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                </div>
            </div>
        </div>                  
        </div>    
        <div class="form_button">
            <asp:Button ID="ButtonPrintSurat" runat="server"  Text="Print" CssClass ="small button blue">
            </asp:Button>&nbsp;        
            <asp:Button ID="ButtonPrint" runat="server"  Text="Preview" CssClass ="small button blue">
            </asp:Button>
	    </div>        
    </asp:Panel>    
    </form>
</body>
</html>