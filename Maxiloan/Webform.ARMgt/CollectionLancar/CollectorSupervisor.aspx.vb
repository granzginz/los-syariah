﻿#Region "Imports"
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Controller
Imports Maxiloan.Parameter.SPVActivity
Imports Maxiloan.Webform.UserController
#End Region

Public Class CollectorSupervisor
    Inherits Maxiloan.Webform.WebBased

#Region "properties"
    Public Property cgid() As String
        Get
            Return CType(viewstate("CGID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CGID") = Value
        End Set
    End Property

    Public Property WhereAgreementNo() As String
        Get
            Return CType(viewstate("WhereAgreementNo"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("WhereAgreementNo") = Value
        End Set
    End Property

    Public Property SupervisorID() As String
        Get
            Return CType(viewstate("SupervisorID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("SupervisorID") = Value
        End Set
    End Property

    Public Property CollectorID() As String
        Get
            Return CType(viewstate("CollectorID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CollectorID") = Value
        End Set
    End Property

    Public Property CollectorName() As String
        Get
            Return CType(viewstate("CollectorName"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CollectorName") = Value
        End Set
    End Property

    Public Property CollectorType() As String
        Get
            Return CType(viewstate("CollectorType"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CollectorType") = Value
        End Set
    End Property

    Public Property strKey() As String
        Get
            Return CType(viewstate("strKey"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("strKey") = Value
        End Set
    End Property

    Public Property isCheckAll() As String
        Get
            Return CType(viewstate("isCheckAll"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("isCheckAll") = Value
        End Set
    End Property


#End Region

#Region " Private Const "
    Dim m_SPVAct As New SPVActivityController

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

#End Region

    Dim dsColl As DataSet    
    Protected WithEvents UcCollectorSearch As UcCollector
    Protected WithEvents UcCollectorFixed As UcCollector   
    Dim gBytPageSize As Integer


#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
            Else
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
            Else
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        Bindgrid(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click

        If txtPage.Text = "" Then
            txtPage.Text = "0"
        Else
            If IsNumeric(txtPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                    currentPage = CType(txtPage.Text, Int32)
                    Bindgrid(Me.SearchBy, Me.SortBy)
                End If
            End If
        End If


    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        Bindgrid(Me.SearchBy, Me.SortBy)
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not Me.IsPostBack Then
            Me.FormID = "SupervisorActivity"
            Me.cgid = Me.GroubDbID
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                InitialDefaultPanel()
                If Request("cond") <> "" Then
                    Me.SearchBy = Request("cond")
                Else
                    Me.SearchBy = "ALL"
                End If
                Me.SortBy = ""

                Me.isCheckAll = "N"
                Bindgrid(Me.SearchBy, Me.SortBy)
                '========================================
                ' Check Login ID
                '========================================
                Dim login As String
                login = Me.Loginid
                FillComboBox()
            End If
        End If
    End Sub

#Region " InitialDefaultPanel "
    Private Sub InitialDefaultPanel()
        PnlSearch.Visible = True
        PnlList.Visible = False
        pnlFixed.Visible = False
        pnlCases.Visible = False
        PnlListChoosen.Visible = False
    End Sub
#End Region


    Sub Connection()
        Dim strconnect As String
        strconnect = GetConnectionString()
    End Sub

    Private Sub FillComboBox()
        '===============================
        'Combo Collector di Search Panel
        '===============================
        With UcCollectorSearch
            .cgid = Me.cgid
            .SupervisorID = ""
            .WhereCollectorType = "collectortype='D' or collectortype='CL'"
            .BindCollectorSPV()
        End With

        '============================
        'Combo Cases di Search Panel
        '============================
        Dim dt As New DataTable
        Dim oSPVAct As New Parameter.SPVActivity

        With oSPVAct
            .strConnection = GetConnectionString()
        End With

        oSPVAct = m_SPVAct.GetCasesCombo(oSPVAct)
        dt = oSPVAct.listdata

        cboCasesSearch.DataSource = dt
        cboCasesSearch.DataTextField = "Name"
        cboCasesSearch.DataValueField = "ID"
        cboCasesSearch.DataBind()

        cboCasesSearch.Items.Insert(0, "Select One")
        cboCasesSearch.Items(0).Value = "0"


        '============================
        'Combo Cases di Cases Panel
        '============================
        cboCases.DataSource = dt
        cboCases.DataTextField = "Name"
        cboCases.DataValueField = "ID"
        cboCases.DataBind()

        cboCases.Items.Insert(0, "Select One")
        cboCases.Items(0).Value = "0"

    End Sub

    Sub Bindgrid(ByVal cmdWhere As String, ByVal cmSort As String)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim oSPVAct As New Parameter.SPVActivity

        With oSPVAct
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With


        oSPVAct = m_SPVAct.SPVActivityList(oSPVAct)

        With oSPVAct
            lblrecord.Text = CType(.TotalRecord, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecord
        End With

        dtsEntity = oSPVAct.listdata
        dtvEntity = dtsEntity.DefaultView
        dtvEntity.Sort = Me.SortBy
        Dtg.DataSource = dtvEntity
        Try
            Dtg.DataBind()
        Catch ex As Exception

        End Try

        PagingFooter()
    End Sub

    Sub PageCommand(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        ''If dtg.PageCount - 1 <> dtg.CurrentPageIndex Then
        ''IsiViewstate()
        ''End If
        ''Dim LstrArg As String = CType(sender.commandArgument(), String)
        'Bindgrid(LstrArg)
        'If dtg.PageCount - 1 = dtg.CurrentPageIndex Then
        '    Isicheck()
        'End If
    End Sub

    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Buttonsearch.Click
        Bindgrid(Me.SearchBy, Me.SortBy)
        PnlList.Visible = True
    End Sub

    Private Sub imbDoAction_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonDoAction.Click
        PnlList.Visible = False
        PnlSearch.Visible = False
        PnlListChoosen.Visible = True

        IsiViewState()

        Select Case Trim(rbTask.SelectedItem.Value)
            Case "Fixed"
                pnlFixed.Visible = True
            Case "Cases"
                pnlCases.Visible = True
            Case "RAL"
                pnlRAL.Visible = True
            Case "RedDot"

        End Select
    End Sub

    Public Sub checkAll()
        Dim i As Integer
        Dim cb As CheckBox

        If Me.isCheckAll = "N" Then
            Me.isCheckAll = "Y"
        Else
            Me.isCheckAll = "N"
        End If

        For i = 0 To Dtg.PageSize - 1
            cb = CType(Dtg.Items(i).FindControl("chkSelect"), CheckBox)
            If Me.isCheckAll = "Y" Then
                cb.Checked = True
            Else
                cb.Checked = False
            End If
        Next

    End Sub

    Private Sub IsiViewState()
        Dim i As Integer
        Dim cb As CheckBox
        Dim lnkAgreement As HyperLink
        Dim strAgreement As String
        Dim koma As String

        strAgreement = ""

        For i = 0 To Dtg.PageSize - 1
            cb = CType(Dtg.Items(i).FindControl("chkSelect"), CheckBox)
            lnkAgreement = CType(Dtg.Items(i).FindControl("lnkAgreement"), HyperLink)


            If cb.Checked = True Then
                If strAgreement = "" Then koma = "" Else koma = ","
                strAgreement = strAgreement & koma & lnkAgreement.Text
                ViewState("hal" & CStr(Dtg.CurrentPageIndex).Trim) = strAgreement
                ShowMessage(lblMessage, "hal" & CStr(Dtg.CurrentPageIndex).Trim & "=" & strAgreement, True)
            End If
        Next

    End Sub

    'Sub Isicheck()
    '    'Mengisi check box di halaman yang sedang aktif 
    '    'Check box yg diisi hanya di halaman ini saja.
    '    Dim i, j, n As Integer
    '    Dim lBytIndex, counter As Byte
    '    Dim chkPrn As New CheckBox
    '    Dim totalrecord, totalhalaman As Integer
    '    Dim lnkAgreement As New HyperLink
    '    Dim strAgreement As String

    '    totalrecord = CInt(lblrecord.Text)
    '    totalhalaman = CInt(Dtg.PageCount)
    '    If Dtg.CurrentPageIndex <= (totalrecord / Dtg.PageSize) - 1 Then
    '        Dim splithalaman As String
    '        splithalaman = Split(viewstate("hal" & CStr(Dtg.CurrentPageIndex).Trim), ",")
    '        For i = 0 To Dtg.PageSize - 1
    '            chkPrn = CType(Dtg.Items(i).FindControl("chkSelect"), CheckBox)
    '            lnkAgreement = CType(Dtg.Items(i).FindControl("lnkAgreementNo"), HyperLink)
    '            For n = 0 To UBound(splithalaman)
    '                If lnkAgreement.Text.Trim = Trim(splithalaman(n)) Then
    '                    chkPrn.Checked = True
    '                End If
    '            Next
    '        Next i
    '    Else
    '        Dim splithalaman As String
    '        splithalaman = Split(viewstate("hal" & CStr(Dtg.CurrentPageIndex).Trim), ",")
    '        For i = 0 To (totalrecord Mod Dtg.PageSize) - 1
    '            chkPrn = CType(Dtg.Items(i).FindControl("chkSelect"), CheckBox)
    '            lnkAgreement = CType(Dtg.Items(i).FindControl("lnkAgreementNo"), HyperLink)
    '            For n = 0 To UBound(splithalaman)
    '                If lnkAgreement.Text.Trim = RTrim(splithalaman(n)) Then
    '                    chkPrn.Checked = True
    '                End If
    '            Next
    '        Next
    '    End If

    'End Sub

    Private Sub btnFSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonFSave.Click
        Dim FixedStatus As String = rbFixed.SelectedItem.Value.Trim()
        If FixedStatus = "Fixed" Then FixedStatus = "1" Else FixedStatus = "0"

        Dim oSpvAct As Parameter.SPVActivity
        With oSpvAct
            .WhereAgreementNo = Me.WhereAgreementNo
            .FixedStatus = FixedStatus
            .CollectorID = UcCollectorFixed.CollectorID
        End With

        Try
            m_SPVAct.SPVActFixed(oSpvAct)
        Catch ex As Exception

        End Try


    End Sub
End Class