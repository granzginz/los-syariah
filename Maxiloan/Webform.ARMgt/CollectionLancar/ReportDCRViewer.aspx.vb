﻿Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports Maxiloan.General.CommonCookiesHelper

Public Class ReportDCRViewer
    Inherits Maxiloan.Webform.WebBased

#Region "Property"
    Private Property CGID() As String
        Get
            Return CType(viewstate("CGID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CGID") = Value
        End Set
    End Property

    Private Property CollectorID() As String
        Get
            Return CType(viewstate("CollectorID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CollectorID") = Value
        End Set
    End Property

    Private Property DateFrom() As String
        Get
            Return CType(viewstate("DateFrom"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("DateFrom") = Value
        End Set
    End Property

    Private Property DateTo() As String
        Get
            Return CType(viewstate("DateTo"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("DateTo") = Value
        End Set
    End Property

    Private Property TaskType() As String
        Get
            Return CType(viewstate("TaskType"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("TaskType") = Value
        End Set
    End Property

    Private Property cmdwhere() As String
        Get
            Return CType(viewstate("cmdwhere"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("cmdwhere") = Value
        End Set
    End Property

#End Region


    Private m_DCR As New InqDCRController

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If
        BindReport()
    End Sub

    Sub BindReport()
        GetCookies()
        Dim dtvEntity As New DataView
        Dim dtsEntity As New DataTable
        Dim ds As New DataSet
        Dim oDCRReport As New Parameter.InqDCR
        Dim oReport As ReportDCRPrint = New ReportDCRPrint
        Dim strconn As String = GetConnectionString()

        With oDCRReport
            .strConnection = GetConnectionString
            .CGID = Me.CGID
            .WhereCond = Me.cmdwhere
        End With

        dtsEntity = m_DCR.ListReport(oDCRReport)        

        'If dtsEntity.Rows.Count > 0 Then

        ds.Tables.Add(dtsEntity.Copy)
        oReport.SetDataSource(ds)
        oReport.SetParameterValue(0, Me.CGID)
        oReport.SetParameterValue(1, Me.UserID)
        oReport.SetParameterValue(2, Me.DateFrom)
        oReport.SetParameterValue(3, Me.DateTo)
        oReport.SetParameterValue(4, Me.sesCompanyName)
        CRV.ReportSource = oReport
        CRV.DataBind()

        With oReport.ExportOptions
            .ExportDestinationType = ExportDestinationType.DiskFile
            .ExportFormatType = ExportFormatType.PortableDocFormat
        End With

        Dim doctoprint As New System.Drawing.Printing.PrintDocument
        Dim i As Integer
        'doctoprint.PrinterSettings.PrinterName = "Maxiloan Printer Server"

        For i = 0 To doctoprint.PrinterSettings.PaperSizes.Count - 1
            Dim rawKind As Integer
            If doctoprint.PrinterSettings.PaperSizes(i).PaperName = "rpt_DCR" Then
                rawKind = CInt(doctoprint.PrinterSettings.PaperSizes(i).GetType().GetField("kind", Reflection.BindingFlags.Instance Or Reflection.BindingFlags.NonPublic).GetValue(doctoprint.PrinterSettings.PaperSizes(i)))
                oReport.PrintOptions.PaperSize = CType(rawKind, CrystalDecisions.Shared.PaperSize)
                Exit For
            End If
        Next

        Dim strFileLocation As String

        strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
        strFileLocation += Me.Session.SessionID + Me.Loginid + "DCR.pdf"

        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = _
        New CrystalDecisions.Shared.DiskFileDestinationOptions

        DiskOpts.DiskFileName = strFileLocation
        With oReport
            .ExportOptions.DestinationOptions = DiskOpts
            .Export()
            .Close()
            .Dispose()
        End With

        Response.Redirect("ReportDCR.aspx?rptFile=" & Me.Session.SessionID + Me.Loginid + "DCR.pdf")
        'Else
        'Response.Redirect("ReportDCR.aspx")
        'End If

    End Sub

    Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies("RptCollDCR")
        Me.CGID = cookie.Values("CGID")
        Me.Loginid = cookie.Values("LoginID")
        Me.CollectorID = cookie.Values("CollectorID")
        Me.DateFrom = cookie.Values("DateFrom")
        Me.DateTo = cookie.Values("DateTo")
        Me.cmdwhere = cookie.Values("cmdwhere")
    End Sub
End Class