﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CollectorAllocation.aspx.vb"
    Inherits="Maxiloan.Webform.ARMgt.CollectorAllocation" %>

<%@ Register TagPrefix="uc1" TagName="UcBranch" Src="../../Webform.UserController/UcBranch.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>CollectorAllocation</title>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        function fConfirm() {
            if (window.confirm("Apakah yakin mau hapus data ini ? "))
                return true;
            else
                return false;
        }
        function fback() {
            history.go(-1);
            return false;
        }				
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                CARI KONTRAK
            </h3>
        </div>
    </div>
    <asp:Panel ID="PnlSearchDetail" runat="server">
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <uc1:ucbranch id="oBranch" runat="server"></uc1:ucbranch>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Kategori Collector</label>
                <asp:DropDownList ID="cboSearchBy" runat="server" AutoPostBack="True">
                    <asp:ListItem Value="0" Selected="True">Select All</asp:ListItem>
                    <asp:ListItem Value="Collector">Collector</asp:ListItem>
                    <asp:ListItem Value="Collector Non Active">Collector Non Active</asp:ListItem>
                    <asp:ListItem Value="Tanpa Collector">Tanpa Collector</asp:ListItem>
                </asp:DropDownList>
                <asp:DropDownList ID="cboCollectorFind" runat="server" Visible="False">
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonSearchDetail" runat="server" CausesValidation="False" Text="Search"
                CssClass="small button blue"></asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_box_title">
            <div class="form_single">
                <h4>
                    ALOKASI DENGAN DATA DEFAULT
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Default Collection Group</label>
                <asp:DropDownList ID="cboParent" runat="server">
                </asp:DropDownList>
                <asp:Label ID="lblDefaultCollectionGroup" runat="server" Visible="False" CssClass="validator_general">Please Select Default Collection Group</asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Default Collector</label>
                <asp:DropDownList ID="cboDefaultCollector" runat="server">
                </asp:DropDownList>
                <asp:Label ID="lblDefaultCollector" runat="server" Visible="False" CssClass="validator_general">Harap pilih Default Collector</asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Default Desk Collector</label>
                <asp:DropDownList ID="cboChild" runat="server">
                </asp:DropDownList>
                <asp:Label ID="lblDefaultDC" runat="server" Visible="False" CssClass="validator_general">Harap pilih Default Desk Collector</asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Default Kriteria</label>
                <asp:DropDownList ID="cboCriteria" runat="server">
                    <asp:ListItem Value="All">Semua</asp:ListItem>
                    <asp:ListItem Value="Unallocated">Customer Belum Alokasi</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonApply" runat="server" Text="Apply" CssClass="small button blue">
            </asp:Button>
        </div>
        <div class="form_box_title">
            <div class="form_single">
                <h4>
                    ALOKASI COLLECTOR UNTUK KONTRAK
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgCollZipCode" runat="server" Width="100%" OnSortCommand="Sorting"
                        AllowSorting="True" AutoGenerateColumns="False" CssClass="grid_general" BorderStyle="None"
                        BorderWidth="0">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:BoundColumn Visible="False" DataField="applicationid"></asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="CGID"></asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="CollectorID"></asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="DeskCollID"></asp:BoundColumn>
                            <asp:BoundColumn DataField="CustomerName" SortExpression="f.name" HeaderText="NAMA CUSTOMER">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="AgreementNo" SortExpression="a.AgreementNo" HeaderText="NO KONTRAK">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="Address" HeaderText="ALAMAT"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="COLLECTOR GROUP">
                                <ItemTemplate>
                                    <asp:DropDownList ID="cboCGID" runat="server">
                                    </asp:DropDownList>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="COLLECTOR">
                                <ItemTemplate>
                                    <asp:DropDownList ID="cboCollector" runat="server">
                                    </asp:DropDownList>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DESK COLLECTOR">
                                <ItemTemplate>
                                    <asp:DropDownList ID="cboDeskCollector" runat="server">
                                    </asp:DropDownList>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn Visible="False" DataField="zipcode"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <div class="button_gridnavigation">
                        <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                            CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                            CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                            CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                            CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                        <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                            EnableViewState="False"></asp:Button>
                        <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" MinimumValue="1"
                            ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"
                            Display="Dynamic"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                            ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general">
                        </asp:RequiredFieldValidator>
                    </div>
                    <div class="label_gridnavigation">
                        <asp:Label ID="lblPage" runat="server"></asp:Label>of
                        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                        <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                    </div>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonSave" runat="server" CausesValidation="False" Text="Save" CssClass="small button blue">
            </asp:Button>&nbsp;
            <asp:Button ID="ButtonButton1" runat="server" CausesValidation="False" Text="Back"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
