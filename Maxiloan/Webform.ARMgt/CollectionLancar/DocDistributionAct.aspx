﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="DocDistributionAct.aspx.vb"
    Inherits="Maxiloan.Webform.ARMgt.DocDistributionAct" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ucCollectorRpt" Src="../../Webform.UserController/ucCollectorRpt.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcCollector" Src="../../Webform.UserController/UcCollector.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>DocDistributionAct</title>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%#Request.servervariables("SERVER_NAME")%>/';
    </script>
    <script language="javascript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;
        function OpenWinCustomer(pID) {
            window.open('../../Webform.LoanOrg/Credit/ViewPersonalCustomer.aspx?CustomerID=' + pID + '&style=Collection', 'UserLookup', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>    
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label> 
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">           
            <h3>
                KEGIATAN DISTRIBUSI DOKUMEN
            </h3>
        </div>
    </div>  
    <asp:Panel ID="pnlsearch" runat="server">       
        <div class="form_box_uc">
                <uc1:ucCollectorRpt id="oCollector" runat="server"></uc1:ucCollectorRpt>	        
        </div>        
        <div class="form_box">
	        <div class="form_single">
                <label>Cari Berdasarkan</label>
                <asp:DropDownList ID="cboSearchBy" runat="server">
                    <asp:ListItem Value="Select One">Select One</asp:ListItem>
                    <asp:ListItem Value="CollectionAgreement.AgreementNo">No Kontrak</asp:ListItem>
                    <asp:ListItem Value="Customer.Name">Nama Konsumen</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearchBy" runat="server" ></asp:TextBox>
	        </div>
        </div>  
        <div class="form_box">
	        <div class="form_single">
                <label>Status Polis</label>
                <asp:DropDownList ID="cboReceiveStatus" runat="server">
                    <asp:ListItem Value="All">All</asp:ListItem>
                    <asp:ListItem Value="InsuranceAsset.PolicyNumber <> '-' and InsuranceAsset.PolicyNumber <> '0 ">Terima</asp:ListItem>
                    <asp:ListItem Value="InsuranceAsset.PolicyNumber = '-">Belum Terima</asp:ListItem>
                </asp:DropDownList>
	        </div>
        </div>        
        <div class="form_button">
            <asp:Button ID="Buttonsearch" runat="server"  Text="Search" CssClass ="small button blue"></asp:Button>&nbsp;
            <asp:Button ID="ButtonReset" runat="server"  Text="Reset" CssClass ="small button gray"  CausesValidation="False"></asp:Button>
	    </div>                     
    </asp:Panel>    
    <asp:Panel ID="pnlDtGrid" runat="server">
        <div class="form_box_title">
            <div class="form_single">              
                <h4>
                    DAFTAR KEGIATAN DISTRIBUSI DOKUMEN
                </h4>
            </div>
        </div>
        <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgdocumentdistribution" runat="server" Width="100%"
                        AllowSorting="True" AutoGenerateColumns="False" DataKeyField="AgreementNo" BorderStyle="None" BorderWidth="0" OnSortCommand="SortGrid"
                        CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn Visible="False" HeaderText="Check All">                                
                                <HeaderTemplate>
                                    <asp:CheckBox ID="ChkAll" runat="server" OnCheckedChanged="ChkStatus_CheckedChanged"
                                        AutoPostBack="True"></asp:CheckBox>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkPrint" runat="server"></asp:CheckBox>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="PILIH">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgSelect" runat="server" CommandName="result" ImageUrl="../../images/Iconresult.gif">
                                    </asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="hypAgreementNodtg" runat="server" Text='<%# Container.Dataitem("AgreementNo")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="CustomerName" HeaderText="NAMA KONSUMEN">                                
                                <ItemTemplate>
                                    <a href="javascript:OpenWinCustomer('<%# Container.DataItem("CustomerID")%>');">
                                        <asp:Label ID="hypCustomerNamedtg" runat="server" Text='<%# Container.Dataitem("CustomerName")%>'>
                                        </asp:Label></a>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="WayOfPayment" HeaderText="CARA BAYAR">                    
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="AgreementSent" HeaderText="KONTRAK DIKIRIM">                                
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="SlipSent" HeaderText="PDC DIKIRIM">                                
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="PolicySent" HeaderText="POLIS DIKIRIM"></asp:BoundColumn>
                            <asp:BoundColumn DataField="PolicyReceiveStatus" HeaderText="STATUS POLIS">
                            </asp:BoundColumn>
                            <asp:BoundColumn SortExpression="CollectorID" DataField="CollectorID" HeaderText="COLLECTOR" Visible="false">
                            </asp:BoundColumn>
                            <asp:BoundColumn SortExpression="CollectorName" DataField="CollectorName" HeaderText="COLLECTOR" >
                            </asp:BoundColumn>
                            <asp:TemplateColumn Visible="False" SortExpression="ApplicationId" HeaderText="ApplicationId">                                
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblApplicationId" runat="server" Text='<%# Container.Dataitem("ApplicationId")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" HeaderText="BranchId">
                                <ItemTemplate>
                                    <asp:Label ID="lblBranchId" runat="server" Text='<%# Container.Dataitem("BranchID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>                       
                    </asp:DataGrid>        
                <div class="button_gridnavigation">
	                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
		                CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
		                CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
		                CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
		                CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
	                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
		                EnableViewState="False"></asp:Button>
	                <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage"
				        MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"></asp:RangeValidator>
	                <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage" CssClass="validator_general"
		                ErrorMessage="No Halaman Salah" Display="Dynamic"></asp:RequiredFieldValidator>
                </div>
                <div class="label_gridnavigation">
	                <asp:Label ID="lblPage" runat="server"></asp:Label>of
	                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
	                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                </div>
            </div> 
        </div>
        </div>                            
    </asp:Panel> 
    <asp:Panel ID="pnlDistributionActivity" runat="server">
        <div class="form_box_title">
            <div class="form_single">              
                <h4>
                    HASIL KEGIATAN DISTRIBUSI DOKUMEN
                </h4>
            </div>
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <label>No Kontrak</label>
                    <asp:HyperLink ID="hypAgreementNoSlct" runat="server"></asp:HyperLink>
		        </div>
		        <div class="form_right">
                    <label>Nama Customer</label>	
                    <asp:HyperLink ID="hypCustomerNameSlct" runat="server"></asp:HyperLink>		
		        </div>	        
        </div>   
        <div class="form_box">	        
                <div class="form_left">
                    <label>Tanggal Aktivasi Pembiayaan</label>
                    <asp:Label ID="lblgolivedate" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">	
                    <label>Cara Pembayaran</label>	
                    <asp:Label ID="lblwayofpayment" runat="server"></asp:Label>	
		        </div>	        
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <label>Di Asuransi Oleh</label>
                    <asp:Label ID="lblinsuredby" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">	
                    <label>Di Bayar Oleh</label>		
                    <asp:Label ID="lblpaidby" runat="server"></asp:Label>
		        </div>	        
        </div>
        <div class="form_box">	        
                <div class="form_left">
                    <label>Polis diterima</label>
                    <asp:Label ID="lblpolicyreceive" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">
                    <label>Collector</label>	
                    <asp:Label ID="lblcollector" runat="server"></asp:Label>		
		        </div>	        
        </div> 
        <div class="form_box">	        
                <div class="form_left">
                    <label class ="label_req">Tanggal Kirim Kontrak</label>                    
                    <asp:TextBox ID="txtAgreementDate" runat="server"></asp:TextBox>                   
                    <asp:CalendarExtender ID="txtAgreementDate_CalendarExtender" runat="server"  Enabled="True" TargetControlID="txtAgreementDate" Format ="dd/MM/yyyy" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*" CssClass="validator_general" ControlToValidate="txtAgreementDate" />
		        </div>
		        <div class="form_right">	
                    <label class ="label_req">Kontrak diterima Oleh</label>         
                    <asp:TextBox ID="txtAgreementBy" runat="server" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Silahkan Isi Penerima Kontrak" CssClass="validator_general" ControlToValidate="txtAgreementBy" />
		        </div>	        
        </div>
        <div class="form_box">	        
                <div class="form_left">
                   <label class ="label_req">Tanggal Kirim PDC</label>                    
                    <asp:TextBox ID="txtSlipDate" runat="server"></asp:TextBox>                   
                    <asp:CalendarExtender ID="txtSlipDate_CalendarExtender" runat="server"  Enabled="True" TargetControlID="txtSlipDate" Format ="dd/MM/yyyy" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*" CssClass="validator_general" ControlToValidate="txtSlipDate" />
		        </div>
		        <div class="form_right">	
                    <label class ="label_req">Slip Setoran Diterima Oleh</label>	
                    <asp:TextBox ID="txtSlipSetoranBy" runat="server" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Silahkan Isi Penerima" CssClass="validator_general" ControlToValidate="txtSlipSetoranBy" />
		        </div>	        
        </div>
        <div class="form_box">	        
                <div class="form_left">
                   <label>Tanggal Kirim Polis</label>                    
                    <asp:TextBox ID="txtInsuranceDate" runat="server" />
                    <asp:CalendarExtender ID="txtInsuranceDate_CalendarExtender" runat="server"  Enabled="True" TargetControlID="txtInsuranceDate" Format ="dd/MM/yyyy" />
                   <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*" CssClass="validator_general"
                    ControlToValidate="txtInsuranceDate"></asp:RequiredFieldValidator>--%>
		        </div>
		        <div class="form_right">		
                    <label>Polis diterima oleh</label>	
                    <asp:TextBox ID="txtInsuranceBy" runat="server" ></asp:TextBox>
		        </div>	     
                   
        </div> 
        <div class="form_box">	        
                <div class="form_left">
                   <label class ="">Tanggal Welcome Letter</label>                    
                    <asp:TextBox ID="txtWelcomeLetter" runat="server"></asp:TextBox>   
                     <asp:CalendarExtender ID="txtWelcomeLetter_CalendarExtender" runat="server"  Enabled="True" TargetControlID="txtWelcomeLetter" Format ="dd/MM/yyyy" />                
		        </div>
		        <div class="form_right">		
                    <label>Welcome Letter diterima oleh</label>	
                    <asp:TextBox ID="txtWelcomeLetterreceiveBy" runat="server" ></asp:TextBox>
		        </div>	        
        </div> 
        <div class="form_box">	        
                <div class="form_left">
                   <label class ="">Tanggal Kartu Angsuran</label>                    
                    <asp:TextBox ID="txtKartuAngsuran" runat="server"></asp:TextBox>    
                      <asp:CalendarExtender ID="txtKartuAngsuran_CalendarExtender" runat="server"  Enabled="True" TargetControlID="txtKartuAngsuran" Format ="dd/MM/yyyy" />               
		        </div>
		        <div class="form_right">		
                    <label>Kartu Angsuran diterima oleh</label>	
                    <asp:TextBox ID="txtKartuAngsuranReceiveBy" runat="server" ></asp:TextBox>
		        </div>	        
        </div> 
        <div class="form_box">	        
                <div class="form_left">
                   <label class ="">Kurir</label>                    
                    <asp:TextBox ID="txtKurir" runat="server"></asp:TextBox>                   
		        </div>
        </div> 
        <div class="form_button">
            <asp:Button ID="ButtonSave" runat="server"  Text="Save" CssClass ="small button blue" />&nbsp;
            <asp:Button ID="ButtonCancel" runat="server"  Text="Cancel" CssClass ="small button gray" CausesValidation="False" />
	    </div>        
    </asp:Panel>     
    </form>
</body>
</html>
