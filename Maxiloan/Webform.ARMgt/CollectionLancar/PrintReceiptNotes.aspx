﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PrintReceiptNotes.aspx.vb"
    Inherits="Maxiloan.Webform.ARMgt.PrintReceiptNotes" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ucCollectorRpt" Src="../../Webform.UserController/ucCollectorRpt2.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PrintReceiptNotes</title>
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%#Request.servervariables("SERVER_NAME")%>/';			
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>    
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>       
    <div class="form_title">    
        <div class="title_strip">
        </div>
        <div class="form_single">           
            <h3>
                CETAK KWITANSI TAGIH
            </h3>
        </div>
    </div>   
    <div class="form_box_uc">
            <uc1:uccollectorrpt id="UcCollector" runat="server">
            </uc1:uccollectorrpt>	    
    </div>
    <div class="form_box">
	    <div class="form_single">
            <label class ="label_req">Tanggal Kwitansi</label>            
            <asp:TextBox ID="txtReceiptDate" runat="server"></asp:TextBox>                   
            <asp:CalendarExtender ID="txtReceiptDate_CalendarExtender" runat="server" 
                Enabled="True" TargetControlID="txtReceiptDate" Format ="dd/MM/yyyy"></asp:CalendarExtender>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*" CssClass="validator_general"
            ControlToValidate="txtReceiptDate"></asp:RequiredFieldValidator>
	    </div>
    </div>
    <div class="form_button">
        <asp:Button ID="ButtonSearch" runat="server" Enabled="True"  Text="Search" CssClass ="small button blue">
        </asp:Button>&nbsp;
        <asp:Button ID="ButtonReset" runat="server" Enabled="True"  Text="Reset" CssClass ="small button gray"
            CausesValidation="False"></asp:Button>&nbsp;
	</div>                
    <asp:Panel ID="PnlList" runat="server">
    <div class="form_box_title">
        <div class="form_single">              
            <h4>
                    CETAK KWITANSI TAGIH
            </h4>
        </div>
    </div>
    <div class="form_box_header">
    <div class="form_single">
        <div class="grid_wrapper_ns">
        <asp:DataGrid ID="dtg" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        BorderStyle="None" BorderWidth="0" CssClass="grid_general"
                        OnSortCommand="Sorting" Width="100%">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn>                                
                                <HeaderTemplate>
                                    <asp:CheckBox ID="chkAll" OnCheckedChanged="checkAll" runat="server" AutoPostBack="True">
                                    </asp:CheckBox>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkSelect" runat="server"></asp:CheckBox>
                                    <asp:Label ID="lblCounter" runat="server" Visible="false"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="ReceiptNo" SortExpression="ReceiptNo" HeaderText="NO KWITANSI">
                            </asp:BoundColumn>
                            <asp:TemplateColumn SortExpression="AGREEMENTNO" HeaderText="NO KONTRAK">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkAgreementNo" runat="server" Text='<%#Container.DataItem("AGREEMENTNO")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="CUSTOMERNAME" HeaderText="NAMA CUSTOMER">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkCust" runat="server" Text='<%# Container.DataItem("CustomerName")%>'>
                                    </asp:HyperLink>                                    
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" SortExpression="CustomerID" HeaderText="CustomerID">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblCustomerID" runat="server" Text='<%# Container.DataItem("CustomerID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="DaysOverdue" SortExpression="DaysOverdue" HeaderText="HARI TELAT">                                
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="InstallmentNo" SortExpression="InstallmentNo" HeaderText="ANGSURAN KE">                                
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="LateCharges" SortExpression="LateCharges" HeaderText="DENDA KETERLAMBATAN"
                                DataFormatString="{0:N2}">                                
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="CollectorId" SortExpression="CollectorId" HeaderText="COLLECTOR">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="IsPrint" SortExpression="IsPrint" HeaderText="CETAK">                                
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="PrintCounter" SortExpression="PrintCounter" HeaderText="CETAK KE">                                
                            </asp:BoundColumn>
                            <asp:TemplateColumn Visible="False" SortExpression="ApplicationId" HeaderText="ApplicationId">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblReceiptNo" runat="server" Text='<%# Container.Dataitem("ReceiptNo")%>'>
                                    </asp:Label>
                                    <asp:Label ID="lblApplicationId" runat="server" Text='<%# Container.Dataitem("ApplicationId")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
        </asp:DataGrid>    
        <div class="button_gridnavigation">
	                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
		                CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
		                CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
		                CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
		                CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
	                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
		                EnableViewState="False"></asp:Button>
	                <asp:RangeValidator   CssClass="validator_general"   ID="rgvGo" runat="server" ControlToValidate="txtPage"
                        MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer"></asp:RangeValidator>
                    <asp:RequiredFieldValidator    CssClass="validator_general"   ID="rfvGo" runat="server" ControlToValidate="txtPage"
                        ErrorMessage="No Halaman Salah" Display="Dynamic"></asp:RequiredFieldValidator>   
                </div>
        <div class="label_gridnavigation">
	                <asp:Label ID="lblPage" runat="server"></asp:Label>of
	                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
	                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                </div>
        </div>
    </div>                
    </div> 
    <div class="form_button">
        <asp:Button ID="ButtonPrint" runat="server" CausesValidation="False" Text="Print" CssClass ="small button blue">
        </asp:Button>&nbsp;
        <asp:Button ID="ButtonViewRpt" runat="server" CausesValidation="False"  Text="Print Preview" CssClass ="small button blue">
        </asp:Button>&nbsp;
	</div>                         
    </asp:Panel>       
    </form>
</body>
</html>
