﻿#Region "Imports"
Imports System.Text
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class LetterOnRequest
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oSearchBy As UcSearchBy
    Protected WithEvents oCGID As UcBranchCollection


#Region "Constanta"
    'Private m_controller As New PremiumToCustomerController
    Private oCollLetterOnReq As New Parameter.CollLetterOnReq
    Private m_controller As New CollLetterOnReqController
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private Enum ActionType
        Add
        Edit
    End Enum
#End Region
#Region "Properties"
    Private Property SaveAction() As ActionType
        Get
            Return DirectCast(viewstate("SaveAction"), ActionType)
        End Get
        Set(ByVal Value As ActionType)
            viewstate("SaveAction") = Value

        End Set
    End Property
    Private Property ApplicationId() As String
        Get
            Return DirectCast(viewstate("ApplicationId"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationId") = Value
        End Set
    End Property
    Private Property AgreementNo() As String
        Get
            Return DirectCast(viewstate("AgreementNo"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AgreementNo") = Value
        End Set
    End Property
    Private Property CustomerId() As String
        Get
            Return DirectCast(viewstate("CustomerId"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerId") = Value
        End Set
    End Property
    Private Property LetterId() As String
        Get
            Return DirectCast(viewstate("LetterId"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("LetterId") = Value
        End Set
    End Property

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        'Put user code to initialize the page here
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            InitialDefaultPanel()
            Me.FormID = "CollLetterOnReq"
            oSearchBy.ListData = "AgreementNo,Agreement No-Name,Customer Name"
            oSearchBy.BindData()
            With oCollLetterOnReq
                .strConnection = GetConnectionString()
            End With
            oCollLetterOnReq.ListCombo = m_controller.LetterNameOnReqCombo(oCollLetterOnReq)
            With cboLetterName
                .DataTextField = "LetterName"
                .DataValueField = "LetterId"
                .DataSource = oCollLetterOnReq.ListCombo
                .DataBind()
            End With
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.SearchBy = ""
                Me.SortBy = ""
            End If


        End If

    End Sub
#Region "InitialDefaultPanel"
    Private Sub InitialDefaultPanel()
        pnlgrid.Visible = False
        pnlLetter.Visible = False
        pnlViewTemplate.Visible = False
        pnlTop.Visible = True        
    End Sub
#End Region
#Region "Navigation "
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridEntity()
    End Sub
#End Region
#Region "Paging Footer"

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then            
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            'rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            'rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
#End Region
#Region "Go Page"
    Protected Sub btnPageNumb_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                BindGridEntity()
            End If
        End If
    End Sub
#End Region
#Region "Sorting"

    Public Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        BindGridEntity()
    End Sub

#End Region
#Region "BindGridEntity"
    Sub BindGridEntity()
        With oCollLetterOnReq
            .WhereCond = Me.SearchBy
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = Me.SortBy
            .strConnection = GetConnectionString
        End With
        oCollLetterOnReq = m_controller.CollLetterOnReqPaging(oCollLetterOnReq)
        recordCount = oCollLetterOnReq.TotalRecord
        dtgLetterOnRequest.DataSource = oCollLetterOnReq.listdata
        dtgLetterOnRequest.CurrentPageIndex = 0
        dtgLetterOnRequest.DataBind()

        pnlgrid.Visible = True
        pnlLetter.Visible = False
        pnlViewTemplate.Visible = False

        PagingFooter()
    End Sub

#End Region


    Private Sub dtgLetterOnRequest_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgLetterOnRequest.ItemCommand
        Dim oAgreementNo As HyperLink
        Dim oCustomerId As Literal
        Dim oApplicationId As Literal

        If e.CommandName = "Letter" Then
            oAgreementNo = CType(e.Item.FindControl("hypAgreementNoGrid"), HyperLink)
            oCustomerId = CType(e.Item.FindControl("ltlCustomerId"), Literal)
            oApplicationId = CType(e.Item.FindControl("ltlApplicationId"), Literal)

            Me.ApplicationId = oApplicationId.Text
            Me.AgreementNo = oAgreementNo.Text
            Me.CustomerId = oCustomerId.Text



            If sessioninvalid() Then
                Exit Sub
            End If

            pnlgrid.Visible = False
            pnlLetter.Visible = True
            pnlViewTemplate.Visible = False
            pnlTop.Visible = False
            With oCollLetterOnReq
                .strConnection = getconnectionstring
                .AgreementNo = oAgreementNo.Text.Trim
                .Applicationid = oApplicationId.Text.Trim
            End With
            m_controller.CollLetterOnReqView(oCollLetterOnReq)
            With oCollLetterOnReq
                hypAgreementNo.Text = .AgreementNo
                hypCustomerName.Text = .CustomerName
                ltlAsset.Text = .Asset
                ltlInstallmentNo.Text = .InstallmentNo
                ltlInstallmentAmount.Text = FormatNumber(CDbl(.InstallMentAmount), 2)
                ltlInstallmentDate.Text = .InstallmentDate
                ltlOSBalance.Text = FormatNumber(CDbl(.OsBalance), 2)

            End With
            'hypAgreementNoGrid.NavigateUrl = "javascript:OpenAgreementNo('" & "Collection" & "', '" & Server.UrlEncode(Me.ApplicationId) & "')"
            'hypCustomerNameGrid.NavigateUrl = "javascript:OpenCustomer('" & "Collection" & "', '" & Server.UrlEncode(Me.CustomerId) & "')"

            hypAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "Collection" & "', '" & Server.UrlEncode(Me.ApplicationId.Trim) & "')"
            hypCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "Collection" & "', '" & Server.UrlEncode(Me.CustomerId.Trim) & "')"

        End If



    End Sub

    Private Sub imgsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Buttonsearch.Click
        Dim strSearch As New StringBuilder        
        Me.SearchBy = ""
        Me.SortBy = ""


        If oSearchBy.Text.Trim <> "" Then
            strSearch.Append(" and ")
            If oSearchBy.ValueID = "AgreementNo" Then
                strSearch.Append("CollAgr.")
            End If
            If Right(oSearchBy.Text, 1) = "%" Then
                strSearch.Append(oSearchBy.ValueID)
                strSearch.Append(" Like '")
                strSearch.Append(oSearchBy.Text)
                strSearch.Append("' and cgid=")
                strSearch.Append(oCGID.BranchID)
            Else
                strSearch.Append(oSearchBy.ValueID)
                strSearch.Append(" = '")
                strSearch.Append(oSearchBy.Text)
                strSearch.Append("' and cgid=")
                strSearch.Append(oCGID.BranchID)
            End If
        Else
            strSearch.Append(" and cgid = ")
            strSearch.Append("'" & oCGID.BranchID & "'")
        End If

        Me.SearchBy = strSearch.ToString
        pnlgrid.Visible = True

        BindGridEntity()
    End Sub

    Private Sub imbReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        InitialDefaultPanel()
    End Sub

    Private Sub btnNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonNext.Click
        Dim dataTag As New DataTable
        Dim dataField As DataTable


        pnlgrid.Visible = False
        pnlLetter.Visible = False
        pnlTop.Visible = False
        pnlViewTemplate.Visible = True
        With oCollLetterOnReq
            .strConnection = GetConnectionString()
            .LetterId = cboLetterName.SelectedValue
            Me.LetterId = cboLetterName.SelectedValue
        End With
        m_controller.LetterNameOnReqComboView(oCollLetterOnReq)
        dataTag = m_controller.CollLetterTemplateField(oCollLetterOnReq)

        With oCollLetterOnReq
            .strConnection = GetConnectionString()
            .Applicationid = Me.ApplicationId
            .AgreementNo = Me.AgreementNo

        End With
        dataField = m_controller.CollLetterTemplateFieldName(oCollLetterOnReq)



        With oCollLetterOnReq
            ltlLetterName.Text = .LetterName
            txtTemplate.Text = .LetterTemplate

            For i As Integer = 0 To dataTag.Rows.Count - 1
                txtTemplate.Text = Replace(txtTemplate.Text, CType(dataTag.Rows(i).Item("TagName"), String), CType(dataField.Rows(0).Item(i), String))

            Next

        End With

    End Sub

    Private Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonBack.Click
        pnlLetter.Visible = True
        pnlViewTemplate.Visible = False
        pnlgrid.Visible = False
        pnlTop.Visible = False

    End Sub

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        pnlgrid.Visible = True
        pnlLetter.Visible = False
        pnlViewTemplate.Visible = False
        pnlTop.Visible = True

    End Sub

    Private Sub btnCancelTemplate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonCancelTemplate.Click
        pnlViewTemplate.Visible = False
        pnlLetter.Visible = True
        pnlgrid.Visible = False
        pnlTop.Visible = False
    End Sub

    Private Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonPrint.Click

        If SessionInvalid() Then
            Exit Sub
        End If
        With oCollLetterOnReq
            .strConnection = GetConnectionString()
            .LetterId = Me.LetterId
            .TextToPrint = txtTemplate.Text
        End With
        m_controller.CollLetterOnReqSaving(oCollLetterOnReq)





        If CheckFeature(Me.Loginid, Me.FormID, "PRINT", Me.AppId) Then            
            Dim cookie As HttpCookie = Request.Cookies(COOKIES_COLL_LETTER_ON_REQ)
            If Not cookie Is Nothing Then
                cookie.Values("letterId") = Me.LetterId
                'cookie.Values("ApplicationId") = Me.ApplicationId
                'cookie.Values("AgreementNo") = Me.AgreementNo
                Response.AppendCookie(cookie)
            Else
                Dim cookieNew As New HttpCookie(COOKIES_COLL_LETTER_ON_REQ)
                cookieNew.Values.Add("letterId", Me.LetterId)
                'cookieNew.Values.Add("ApplicationId", Me.ApplicationId)
                'cookieNew.Values.Add("AgreementNo", Me.AgreementNo)
                Response.AppendCookie(cookieNew)
            End If
            Response.Redirect("LetterOnRequestViewer.aspx")
        End If

    End Sub

    Private Sub dtgLetterOnRequest_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgLetterOnRequest.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim oCustomerId As Literal
            Dim oApplicationId As Literal
            Dim oHypagreement As HyperLink
            Dim oHypCustomername As HyperLink
            Dim oCustomerName As HyperLink


            oCustomerId = CType(e.Item.FindControl("ltlCustomerId"), Literal)
            oApplicationId = CType(e.Item.FindControl("ltlApplicationId"), Literal)


            Me.ApplicationId = oApplicationId.Text
            Me.CustomerId = oCustomerId.Text

            oHypagreement = CType(e.Item.FindControl("hypAgreementNoGrid"), HyperLink)
            oHypagreement.NavigateUrl = "javascript:OpenAgreementNo('" & "Collection" & "', '" & Server.UrlEncode(Me.ApplicationId.Trim) & "')"

            oHypCustomername = CType(e.Item.FindControl("hypCustomerNameGrid"), HyperLink)
            oHypCustomername.NavigateUrl = "javascript:OpenCustomer('" & "Collection" & "', '" & Server.UrlEncode(Me.CustomerId.Trim) & "')"

        End If

    End Sub
End Class