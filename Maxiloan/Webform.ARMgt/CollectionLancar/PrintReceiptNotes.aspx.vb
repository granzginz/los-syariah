﻿#Region "Imports"
Imports System.Text
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonVariableHelper
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class PrintReceiptNotes
    Inherits Maxiloan.Webform.WebBased

#Region " Private Const "
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1    
    Protected WithEvents UcCollector As UCCollectorRpt2
    Protected WithEvents ImbViewRpt As System.Web.UI.WebControls.ImageButton
    Dim m_CollAct As New CollActivityController
#End Region

#Region " Property "

    Private Property CollectorID() As String
        Get
            Return CStr(viewstate("CollectorID"))
        End Get
        Set(ByVal Value As String)
            viewstate("CollectorID") = Value
        End Set
    End Property

    Private Property CGID() As String
        Get
            Return CStr(viewstate("CGID"))
        End Get
        Set(ByVal Value As String)
            viewstate("CGID") = Value
        End Set
    End Property

    Private Property CGName() As String
        Get
            Return CStr(viewstate("CGName"))
        End Get
        Set(ByVal Value As String)
            viewstate("CGName") = Value
        End Set
    End Property

    Private Property AgreementNo() As String
        Get
            Return CStr(ViewState("AgreementNo"))
        End Get
        Set(ByVal Value As String)
            viewstate("AgreementNo") = Value
        End Set
    End Property

    Private Property CustomerID() As String
        Get
            Return CStr(ViewState("CustomerID"))
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerID") = Value
        End Set
    End Property

    Private Property CustomerName() As String
        Get
            Return CStr(ViewState("CustomerName"))
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerName") = Value
        End Set
    End Property
    Private Property Rdate() As String
        Get
            Return CStr(viewstate("Rdate"))
        End Get
        Set(ByVal Value As String)
            viewstate("Rdate") = Value
        End Set
    End Property
    Private Property FilterBy() As String
        Get
            Return CStr(viewstate("FilterBy"))
        End Get
        Set(ByVal Value As String)
            viewstate("FilterBy") = Value
        End Set
    End Property

    Private Property SelectedReceiptNo() As String
        Get
            Return CType(viewstate("SelectedReceiptNo"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("SelectedReceiptNo") = Value
        End Set
    End Property
#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            If Request.QueryString("strFileLocation") <> "" Then
                Dim strHTTPServer As String
                Dim StrHTTPApp As String
                Dim strNameServer As String
                Dim strFileLocation As String

                '    strHTTPServer = Request.ServerVariables("PATH_INFO")
                '    strNameServer = Request.ServerVariables("SERVER_NAME")
                '    StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                '    strFileLocation = "http://" & strNameServer & "/" & StrHTTPApp & "/XML/" & Request.QueryString("strFileLocation") & ".pdf"
                '    '& "history.back(-1) " & vbCrLf _
                '    'Call file PDF 
                '    Response.Write("<script language = javascript>" & vbCrLf _
                '    & "window.open('" & strFileLocation & "','Collection', 'left=0, top=0, width=600, height=480, menubar=0, scrollbars=yes') " & vbCrLf _
                '    & "</script>")


                strHTTPServer = Request.ServerVariables("PATH_INFO")
                strNameServer = Request.ServerVariables("SERVER_NAME")
                StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                strFileLocation = "http://" & strNameServer & "/" & StrHTTPApp & "/XML/" & Me.Session.SessionID & Me.Loginid & "PrintKuitansiTagih.pdf"
                'Call file PDF 

                Response.Write("<script language = javascript>" & vbCrLf _
                            & "window.open('" & strFileLocation & "','Collection', 'left=0, top=0, width=600, height=480, menubar=0, scrollbars=yes') " & vbCrLf _
                            & "</script>")
            End If

            PnlList.Visible = False
            txtReceiptDate.Text = Format(Me.BusinessDate, "dd/MM/yyyy")
            Context.Trace.Write(" txtReceiptDate.Text = " & txtReceiptDate.Text)
            UcCollector.CollectorType = "CL"
        End If
    End Sub
#End Region

#Region "Search"
    Private Sub ImbSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSearch.Click
        Dim filterby As String
        PnlList.Visible = True
        Me.SearchBy = ""
        filterby = ""
        Dim dateoReceiptDate As String = CStr(ConvertDate2(txtReceiptDate.Text.Trim))
        Me.Rdate = " Receipt Date : " & ConvertDate2(txtReceiptDate.Text.Trim).ToString("dd/MM/yyyy")
        Me.SearchBy = "ReceiptNotes.CGID='" & UcCollector.CGIDParent.Trim & "'"
        If filterby <> "" Then
            filterby = filterby & " , ReceiptNotes.Collection Group : " & UcCollector.CGIDParentName.Trim & " "
        Else
            filterby = filterby & "ReceiptNotes.Collection Group : " & UcCollector.CGIDParentName.Trim & " "

        End If
        If UcCollector.CollectorID.Trim <> "" And UcCollector.CollectorID.Trim <> "0" Then
            Me.SearchBy = Me.SearchBy & " and ReceiptNotes.CollectorID='" & UcCollector.CollectorID.Trim & "'"
            If filterby <> "" Then
                filterby = filterby & " , ReceiptNotes.Collector : " & UcCollector.CollectorID.Trim & " "
            Else
                filterby = filterby & "ReceiptNotes.Collector : " & UcCollector.CollectorID.Trim & " "
            End If
        End If
        If txtReceiptDate.Text <> "" Then
            Me.SearchBy = Me.SearchBy & " and ReceiptNotes.ReceiptDate = '" & dateoReceiptDate & "'"
        End If
        Context.Trace.Write(" Me.SearchBy = " & Me.SearchBy)
        If Me.SortBy = "" Then Me.SortBy = " ReceiptNo "
        Me.FilterBy = filterby
        BindGrid(Me.SearchBy, Me.SortBy)
        If UcCollector.CollectorID.Trim <> "" And UcCollector.CollectorID.Trim <> "0" Then
            'ucCollector.ClearItems()
            UcCollector.CollectorID = ""
        End If

    End Sub
#End Region

#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
            'Trim(lblTotPage.Text)
            '
        End If
        lblrecord.Text = recordCount.ToString


        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select

        'SavePage()

        BindGrid(Me.SearchBy, Me.SortBy)

    End Sub

    Protected Sub btnPageNumb_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPageNumb.Click

        If txtPage.Text = "" Then
            txtPage.Text = "0"
        Else
            If IsNumeric(txtPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And (CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer)) Then
                    'SavePage()
                    currentPage = CType(txtPage.Text, Int32)
                    BindGrid(Me.SearchBy, Me.SortBy)
                End If
            End If
        End If


    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If

        BindGrid(Me.SearchBy, Me.SortBy)

    End Sub

#End Region

#Region "BindGrid"
    Sub BindGrid(ByVal cmdWhere As String, ByVal cmSort As String)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim oCollAct As New Parameter.GeneralPaging
        Dim oGeneralPaging As New GeneralPagingController

        Context.Trace.Write("cmdwhere = " & cmdWhere)
        Context.Trace.Write("cmSort = " & cmSort)
        With oCollAct
            .strConnection = GetConnectionString()
            .CurrentPage = currentPage
            .PageSize = pageSize
            .WhereCond = cmdWhere
            .SortBy = cmSort
            .SpName = "spPagingPrintReceiptNotes"
            Context.Trace.Write("CurrentPage = " & .CurrentPage)
            Context.Trace.Write("PageSize = " & .PageSize)
            'Context.Trace.Write("WhereCond = " & .WhereCond)
            'Context.Trace.Write("SortBy = " & .SortBy)
            'Context.Trace.Write("SpName = " & .SpName)
        End With

        oCollAct = oGeneralPaging.GetGeneralPaging(oCollAct)

        With oCollAct
            lblrecord.Text = CType(.TotalRecords, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecords
        End With

        dtsEntity = oCollAct.ListData
        dtvEntity = dtsEntity.DefaultView
        dtvEntity.Sort = Me.SortBy
        dtg.DataSource = dtvEntity
        Try
            dtg.DataBind()
        Catch
            dtg.CurrentPageIndex = 0
            dtg.DataBind()
        End Try
        If UcCollector.CollectorID.Trim <> "" And UcCollector.CollectorID.Trim <> "0" Then
            'ucCollector.ClearItems()
        End If
        PagingFooter()
    End Sub
#End Region

#Region "Reset"

    Private Sub BtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReset.Click

    End Sub

#End Region

#Region "Print"
    Private Sub ImbPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonPrint.Click
        If Not checkReceiptSelection() Then            
            ShowMessage(lblMessage, "Harap pilih satu atau beberapa Kwitansi untuk dicetak", True)
            Exit Sub
        End If

        SendCookies()
        Response.Redirect("PrintKuitansiTagihViewer.aspx")
    End Sub
#End Region

#Region "SendCookies"
    Sub SendCookies()
        Context.Trace.Write("Send Cookies !")

        Dim cookie As HttpCookie = Request.Cookies(General.CommonCookiesHelper.COOKIES_PRINT_RECEIPT_NOTES)
        Dim strWhere As New StringBuilder

        strWhere.Append(Me.SearchBy + " and ReceiptNotes.ReceiptNo IN (" & Me.SelectedReceiptNo & ")")

        If Not cookie Is Nothing Then
            cookie.Values("SearchBy") = strWhere.ToString
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie(General.CommonCookiesHelper.COOKIES_PRINT_RECEIPT_NOTES)
            cookieNew.Values("SearchBy") = strWhere.ToString
            Response.AppendCookie(cookieNew)
        End If
        Context.Trace.Write("strWhere = " & strWhere.ToString)

    End Sub

#End Region

#Region "dtg_ItemDataBound"
    Private Sub dtg_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtg.ItemDataBound
        Dim lnkAgreementNo As HyperLink
        Dim lnkCust As HyperLink
        Dim lblCustomerID As Label
        Dim lblApplicationId As Label
        If e.Item.ItemIndex >= 0 Then
            lnkAgreementNo = CType(e.Item.FindControl("lnkAgreementNo"), HyperLink)
            lnkCust = CType(e.Item.FindControl("lnkCust"), HyperLink)
            lblCustomerID = CType(e.Item.FindControl("lblCustomerID"), Label)
            lblApplicationId = CType(e.Item.FindControl("lblApplicationId"), Label)
            lnkAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccAcq" & "', '" & Server.UrlEncode(lblApplicationId.Text.Trim) & "')"
            lnkCust.NavigateUrl = "javascript:OpenCustomer('" & "AccAcq" & "', '" & Server.UrlEncode(lblCustomerID.Text) & "')"

        End If
    End Sub
#End Region

#Region "ImbViewRpt_Click"
    Private Sub ImbViewRpt_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonViewRpt.Click
        SendCookiesList()
        Response.Redirect("PrintKuitansiTagihListNew.aspx")
    End Sub
#End Region

#Region "SendCookiesList"
    Sub SendCookiesList()
        Context.Trace.Write("Send Cookies !")

        Dim cookie As HttpCookie = Request.Cookies("ReceiptNotesList")
        Dim strWhere As New StringBuilder
        strWhere.Append(Me.SearchBy)
        If Not cookie Is Nothing Then
            cookie.Values("SearchBy") = strWhere.ToString
            cookie.Values("RDate") = Me.Rdate
            cookie.Values("FilterBy") = Me.FilterBy
            cookie.Values("ReceiptDate") = ConvertDate2(txtReceiptDate.Text).ToString("MM-dd-yyyy")
            cookie.Values("CGName") = UcCollector.CGIDParentName
            cookie.Values("CollectorName") = UcCollector.CollectorIDChildName
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie("ReceiptNotesList")
            cookieNew.Values("SearchBy") = strWhere.ToString
            cookieNew.Values("RDate") = Me.Rdate
            cookieNew.Values("FilterBy") = Me.FilterBy
            cookieNew.Values("ReceiptDate") = ConvertDate2(txtReceiptDate.Text).ToString("MM-dd-yyyy")
            cookieNew.Values("CGName") = UcCollector.CGIDParentName
            cookieNew.Values("CollectorName") = UcCollector.CollectorIDChildName
            Response.AppendCookie(cookieNew)
        End If
        Context.Trace.Write("strWhere = " & strWhere.ToString)

    End Sub
#End Region

    Public Sub checkAll(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim loopitem As Int16
        Dim Chk As CheckBox        

        For loopitem = 0 To CType(dtg.Items.Count - 1, Int16)
            Chk = New CheckBox
            Chk = CType(dtg.Items(loopitem).FindControl("chkSelect"), CheckBox)

            If Chk.Enabled Then
                If CType(sender, CheckBox).Checked = True Then
                    Chk.Checked = True
                Else
                    Chk.Checked = False
                End If
            Else
                Chk.Checked = False
            End If
        Next
    End Sub

    Public Function checkReceiptSelection() As Boolean
        Dim strSelection As New StringBuilder
        Dim i, j As Integer
        Dim jmlAgreement As Integer
        Dim chkSelect As CheckBox
        Dim lblSelection As Label
        Dim isSelect As Boolean
        isSelect = False
        For i = 0 To dtg.Items.Count - 1
            chkSelect = CType(dtg.Items(i).FindControl("chkSelect"), CheckBox)
            lblSelection = CType(dtg.Items(i).FindControl("lblReceiptNo"), Label)
            If chkSelect.Checked = True Then
                strSelection.Append(CStr(IIf(strSelection.ToString = "", "", ",")) & "'" & lblSelection.Text.Trim & "'")
                isSelect = True
            End If
        Next
        Me.SelectedReceiptNo = strSelection.ToString
        Return isSelect
    End Function
End Class