﻿#Region "Imports"
Option Strict On
Imports System.Text
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Parameter.SPVActivity
#End Region

Public Class SupervisorActivity
    Inherits Maxiloan.Webform.WebBased

#Region "properties"
    Private Property FixedUnfixed() As String
        Get
            Return CType(Viewstate("FixedUnfixed"), String)
        End Get
        Set(ByVal Value As String)
            Viewstate("FixedUnfixed") = Value
        End Set
    End Property

    Private Property AllPage() As String
        Get
            Return CType(Viewstate("AllPage"), String)
        End Get
        Set(ByVal Value As String)
            Viewstate("AllPage") = Value
        End Set
    End Property

    Private Property WhereAgreementNo() As String
        Get
            Return CType(viewstate("WhereAgreementNo"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("WhereAgreementNo") = Value
        End Set
    End Property

    Private Property isCheckAll() As Boolean
        Get
            Return CType(viewstate("isCheckAll"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            viewstate("isCheckAll") = Value
        End Set
    End Property
#End Region

#Region " Private Const "
    Private oController As New SPVActivityController
    Private oCustomClass As New Parameter.SPVActivity
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region
    
    Protected WithEvents UcCGID As UcBranchCollection
    Protected WithEvents UcCollectorSearch As UcCollector
    Protected WithEvents UcCollectorFixed As UcCollector    

#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString
        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If

    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        Bindgrid(Me.SearchBy, Me.SortBy)
    End Sub

    Protected Sub btnPageNumb_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPageNumb.Click
        If txtPage.Text = "" Then
            txtPage.Text = "0"
        Else
            If IsNumeric(txtPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                    currentPage = CType(txtPage.Text, Int32)
                    Bindgrid(Me.SearchBy, Me.SortBy)
                End If
            End If
        End If
    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Dim a As String
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        Bindgrid(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not Me.IsPostBack Then
            Me.FormID = "CollSPVAct"
            '            Me.GroubDbID = Me.GroubDbID

            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If SessionInvalid() Then
                    Exit Sub
                End If

                InitialDefaultPanel()
                'If Request("cond") <> "" Then
                '    Me.SearchBy = Request("cond")
                'Else
                '    Me.SearchBy = "ALL"
                'End If
                Me.SearchBy = ""
                Me.SortBy = ""
                'Bindgrid(Me.SearchBy, Me.SortBy)

                '========================================
                ' Check Login ID
                '========================================

                With oCustomClass
                    .strConnection = GetConnectionString()
                    .CollectorID = Me.Loginid
                    .CGID = Me.GroubDbID
                End With
                FillComboBox(oController.CheckCollectorType(oCustomClass))
            End If
        End If
    End Sub

#Region " InitialDefaultPanel "
    Private Sub InitialDefaultPanel()
        PnlSearch.Visible = True
        PnlList.Visible = False
        pnlFixed.Visible = False
        pnlCases.Visible = False
        PnlListChoosen.Visible = False
        PnlPlan.Visible = False
    End Sub
#End Region

    Private Sub FillComboBox(ByVal sCollectorType As String)
        '===============================
        'Combo Collector di Search Panel
        '===============================
        Dim strWhereCollectorType As String

        'Select Case sCollectorType.Trim
        '    Case "CH"
        '        strWhereCollectorType = "collectortype='D' or collectortype='CL'"
        '    Case "DS"
        '        strWhereCollectorType = "collectortype='D'"
        '    Case "COL"
        '        strWhereCollectorType = "collectortype='CL'"
        '    Case Else
        '        strWhereCollectorType = "collectortype='D' or collectortype='CL'"
        'End Select

        strWhereCollectorType = "collectortype='CL' order by Employeename ASC"

        With UcCollectorSearch
            .cgid = Me.GroubDbID
            .SupervisorID = ""
            .WhereCollectorType = strWhereCollectorType
            .BindCollectorSPV()
        End With

        With UcCollectorFixed
            .cgid = Me.GroubDbID
            .SupervisorID = ""
            .WhereCollectorType = strWhereCollectorType
            .BindCollectorSPV()
        End With

        '============================
        'Combo Cases di Search Panel
        '============================

        With oCustomClass
            .strConnection = GetConnectionString()
        End With

        oCustomClass = oController.GetCasesCombo(oCustomClass)

        cboCasesSearch.DataSource = oCustomClass.listdata
        'cboCasesSearch.DataTextField = "ID"
        cboCasesSearch.DataTextField = "Name"
        cboCasesSearch.DataValueField = "ID"
        cboCasesSearch.DataBind()

        cboCasesSearch.Items.Insert(0, "Select All")
        cboCasesSearch.Items(0).Value = "0"

        '============================
        'Combo Cases di Cases Panel
        '============================
        cboCases.DataSource = oCustomClass.listdata
        'cboCases.DataTextField = "ID"
        cboCases.DataTextField = "Name"
        cboCases.DataValueField = "ID"
        cboCases.DataBind()

        cboCases.Items.Insert(0, "Select One")
        cboCases.Items(0).Value = "0"

    End Sub

    Sub Bindgrid(ByVal cmdWhere As String, ByVal cmSort As String)
        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With
        oCustomClass = oController.SPVActivityList(oCustomClass)
        With oCustomClass
            lblrecord.Text = CType(.TotalRecord, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecord
        End With

        Dtg.DataSource = oCustomClass.listdata.DefaultView
        Try
            Dtg.DataBind()
            PagingFooter()
        Catch ex As Exception
            Dim err As New MaxiloanExceptions
            err.WriteLog("", "", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Sub BindgridChoosen(ByVal strAgreementSelected As String)
        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereAgreementNo = Me.WhereAgreementNo
            .CurrentPage = currentPage
            .PageSize = pageSize
        End With
        oCustomClass = oController.SPVActivityChoosenList(oCustomClass)
        dtgchoosen.DataSource = oCustomClass.listdata.DefaultView
        Try
            dtgchoosen.DataBind()
            dtgchoosen.Visible = True
            PnlListChoosen.Visible = True
        Catch ex As Exception
            lblMessage.Text = ex.Message
        End Try

    End Sub

    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Buttonsearch.Click
        Dim strSearch As New StringBuilder
        If CheckFeature(Me.Loginid, Me.FormID, "Srch", Me.AppId) Then
            If SessionInvalid() Then
                Exit Sub
            End If
        End If

        lblMessage.Text = ""
        rbTask.SelectedIndex = 0

        If UcCGID.BranchID <> "" Then
            strSearch.Append("CGID='" & Me.GroubDbID & "'")
        End If
        If txtAgreementNo.Text.Trim <> "" Then
            strSearch.Append(" and AgreementNo like '%" + txtAgreementNo.Text.Trim + "%'")
        End If
        If (UcCollectorSearch.CollectorID) <> "" And (UcCollectorSearch.CollectorID <> "0") Then
            strSearch.Append(" and CollectorID='" & UcCollectorSearch.CollectorID & "'")
        End If

        'If chkCollectorReq.Checked = True Then
        '    strSearch.Append(" and isRequestAssign=1")
        'Else
        '    strSearch.Append(" and isRequestAssign=0")
        'End If
        If cboCollectorReq.SelectedValue = "1" Then
            strSearch.Append(" and isRequestAssign=1")        
        End If

        If txtCustomerName.Text.Trim <> "" Then
            strSearch.Append(" and CustomerName like'%" + txtCustomerName.Text.Trim + "%'")
        End If

        If cboCasesSearch.SelectedItem.Value <> "0" Then
            strSearch.Append(" and CasesID='" & cboCasesSearch.SelectedItem.Value & "'")
        End If
        If txtOverdueDays.Text.Trim <> "" Then
            strSearch.Append(" and endpastduedays>=" & txtOverdueDays.Text.Trim)
        End If
        Me.SearchBy = strSearch.ToString
        Bindgrid(Me.SearchBy, Me.SortBy)
        PnlList.Visible = True
    End Sub

    Private Sub imbDoAction_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonDoAction.Click
        If CheckFeature(Me.Loginid, Me.FormID, "Act", Me.AppId) Then
            If SessionInvalid() Then
                Exit Sub
            End If
        End If
        Dim isRALdate As Boolean
        lblMessage.Text = ""
        '=======================================================
        ' Kalo sudah pernah paging, ngga usah lagi IsiViewstate
        '=======================================================
        If IsiGridAgreementChoosen() Then
            lblMessage.Text = "Harap pilih Kontrak"
            lblMessage.Visible = True
            PnlListChoosen.Visible = False
            PnlSearch.Visible = True
            PnlList.Visible = True
        Else
            PnlList.Visible = False
            PnlSearch.Visible = False
            PnlListChoosen.Visible = True
            Select Case Trim(rbTask.SelectedItem.Value)
                Case "Fixed"
                    If CheckContradictive("Fixed") = False Then
                        PnlSearch.Visible = False
                        PnlList.Visible = False
                        pnlFixed.Visible = True
                        lblMenuTask.Text = "AGREEMENT LISTING TO FIXED / UNFIXED ASSIGN"
                        If Me.FixedUnfixed.Trim = "Y" Then
                            rbFixed.Enabled = True
                            rbFixed.SelectedIndex = 0
                        Else
                            rbFixed.SelectedIndex = 0
                            rbFixed.Enabled = False
                            UcCollectorFixed.CollectorID = "0"
                            UcCollectorFixed.DataBind()
                        End If
                    End If
                    rbFixed.Attributes.Add("onclick", "rbFixedChange();")

                Case "Cases"
                    pnlCases.Visible = True
                    lblMenuTask.Text = "AGREEMENT LISTING TO CASES ADDED"
                Case "RAL"
                    If CheckContradictive("RAL") = False Then
                        PnlSearch.Visible = False
                        PnlList.Visible = False
                        pnlRAL.Visible = True
                        lblMenuTask.Text = "AGREEMENT LISTING TO PLAN / UNDO PLAN TO RAL"
                        If (dtgchoosen.Items(0).Cells(8).Text.Trim <> "") And (dtgchoosen.Items(0).Cells(8).Text.Trim <> "&nbsp;") Then
                            isRALdate = True
                        Else
                            isRALdate = False
                        End If

                        If isRALdate = True Then
                            lblPlanRAL.Text = "Undo Plan"                            
                        Else
                            lblPlanRAL.Text = "Plan"
                            txtRALDate.Text = ""                            
                        End If
                    End If
                Case "RedDot"
                    Dim oSpvAct As New Parameter.SPVActivity
                    With oSpvAct
                        .strConnection = GetConnectionString()
                        .WhereAgreementNo = Me.WhereAgreementNo
                    End With

                    Try
                        oController.SPVActRedDot(oSpvAct)
                        lblMessage.Text = "Simpan data Berhasil"
                        lblMessage.Visible = True
                    Catch ex As Exception
                        lblMessage.Text = ex.Message
                        lblMessage.Visible = True
                    End Try
                    Bindgrid(Me.SearchBy, Me.SortBy)
                    PnlList.Visible = True
                    PnlSearch.Visible = True
                    PnlListChoosen.Visible = False
                Case "Plan"
                    Dim ds As DataSet
                    Dim oSpvAct As New Parameter.SPVActivity
                    With oSpvAct
                        .strConnection = GetConnectionString()
                    End With
                    PnlPlan.Visible = True
                    ds = oController.SPVListResultAction(oSpvAct)
                    If ds Is Nothing Then
                        lblMessage.Text = "Baca Data Error"
                        lblMessage.Visible = True
                        ButtonlanSave.Visible = False
                        Exit Sub
                    End If
                    If ddlPlan.DataSource Is Nothing Then
                        ddlPlan.DataSource = ds.Tables(0)
                        ddlPlan.DataTextField = "ActionDescription"
                        ddlPlan.DataValueField = "ActionID"
                        ddlPlan.DataBind()
                    End If
            End Select
        End If
    End Sub

    Public Function CheckContradictive(ByVal action As String) As Boolean
        Dim i, intloop As Integer
        Dim Contradictive As Boolean
        Contradictive = False
        Dim checkfirst As String
        Dim check As String
        Dim isRALdate As Boolean
        Dim LastCheck As String
        Dim NextRALDate As Boolean
        intloop = 0
        For i = 0 To Dtg.Items.Count - 1
            If CType(Dtg.Items(i).FindControl("chkSelect"), CheckBox).Checked Then
                Select Case (action)
                    Case "Fixed"
                        check = Dtg.Items(i).Cells(10).Text.Trim
                        If (check.Trim <> "") And (check <> "&nbsp;") Then
                            If i <> 0 Then
                                LastCheck = Dtg.Items(i - 1).Cells(10).Text.Trim
                            Else
                                check = Dtg.Items(i).Cells(10).Text.Trim
                            End If
                        End If
                        If intloop <> 0 Then
                            If (check.Trim <> LastCheck.Trim) Then
                                Contradictive = True
                                Exit For
                            Else
                                Contradictive = False
                            End If
                        End If

                        Me.FixedUnfixed = check

                    Case "RAL"
                        check = Dtg.Items(i).Cells(9).Text.Trim
                        If (check.Trim <> "") And (check <> "&nbsp;") Then
                            'If i <> 0 Then
                            '    NextRALDate = True
                            'Else
                            isRALdate = True
                            'End If
                        Else
                            'If i <> 0 Then
                            '    NextRALDate = False
                            'Else
                            isRALdate = False
                            'End If
                        End If
                        If intloop <> 0 Then
                            If isRALdate = NextRALDate Then
                                Contradictive = False
                            Else
                                Contradictive = True
                                Exit For
                            End If
                        End If
                        NextRALDate = isRALdate
                End Select
                intloop += 1
            End If

        Next
        If Contradictive = True Then
            pnlFixed.Visible = False
            pnlCases.Visible = False
            pnlRAL.Visible = False
            PnlSearch.Visible = True
            PnlList.Visible = True
            PnlListChoosen.Visible = False
            lblMessage.Text = "Kontrak yg dipilih memiliki status kontradiktif"
            lblMessage.Visible = True
            Return True
        Else
            Return False
        End If
    End Function

    Public Function IsiGridAgreementChoosen() As Boolean
        Dim strAgreement As New StringBuilder
        Dim i, j As Integer
        Dim jmlAgreement As Integer
        Dim chkSelect As CheckBox
        Dim lblApplicationID As Label
        Dim isSelect As Boolean
        isSelect = True
        For i = 0 To Dtg.Items.Count - 1
            chkSelect = CType(Dtg.Items(i).FindControl("chkSelect"), CheckBox)
            lblApplicationID = CType(Dtg.Items(i).FindControl("lblApplicationID"), Label)
            If chkSelect.Checked = True Then
                strAgreement.Append(CStr(IIf(strAgreement.ToString = "", "", ",")) & "'" & lblApplicationID.Text.Trim & "'")
                isSelect = False
            End If
        Next
        Me.WhereAgreementNo = strAgreement.ToString
        BindgridChoosen(Me.WhereAgreementNo)
    End Function

#Region "Reset Process"
    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        Bindgrid(Me.SearchBy, Me.SortBy)
        PnlList.Visible = True
        lblMessage.Text = ""
        rbTask.SelectedIndex = 0
        '==========================
        'Bersihkan Search Criteria
        '==========================
        txtAgreementNo.Text = ""
        UcCollectorSearch.CollectorID = ""
        'chkCollectorReq.Checked = False
        txtCustomerName.Text = ""
        cboCasesSearch.SelectedIndex = 0
        txtOverdueDays.Text = ""
    End Sub
#End Region

#Region "Cancel Process"
    Private Sub btnFCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonFCancel.Click

        Bindgrid(Me.SearchBy, Me.SortBy)
        PnlList.Visible = True
        PnlSearch.Visible = True
        pnlFixed.Visible = False
        PnlListChoosen.Visible = False
        lblMessage.Text = ""

        '==========================
        'Bersihkan Search Criteria
        '==========================
        txtAgreementNo.Text = ""
        UcCollectorSearch.CollectorID = ""
        'chkCollectorReq.Checked = False
        txtCustomerName.Text = ""
        cboCasesSearch.SelectedIndex = 0
        txtOverdueDays.Text = ""
    End Sub

    Private Sub btnRCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonRCancel.Click

        Bindgrid(Me.SearchBy, Me.SortBy)
        PnlList.Visible = True
        PnlSearch.Visible = True
        pnlRAL.Visible = False
        PnlListChoosen.Visible = False
        lblMessage.Text = ""

        '==========================
        'Bersihkan Search Criteria
        '==========================
        txtAgreementNo.Text = ""
        UcCollectorSearch.CollectorID = ""
        'chkCollectorReq.Checked = False
        txtCustomerName.Text = ""
        cboCasesSearch.SelectedIndex = 0
        txtOverdueDays.Text = ""
    End Sub

    Private Sub btnCCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click

        Bindgrid(Me.SearchBy, Me.SortBy)
        PnlList.Visible = True
        PnlSearch.Visible = True
        pnlCases.Visible = False
        lblMessage.Text = ""
        PnlListChoosen.Visible = False

        '==========================
        'Bersihkan Search Criteria
        '==========================
        txtAgreementNo.Text = ""
        UcCollectorSearch.CollectorID = ""
        'chkCollectorReq.Checked = False
        txtCustomerName.Text = ""
        cboCasesSearch.SelectedIndex = 0
        txtOverdueDays.Text = ""
    End Sub

    Private Sub ImbPlanCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonlanCancel.Click
        InitialDefaultPanel()
        PnlList.Visible = True
    End Sub
#End Region

#Region "Save Process"
    Private Sub btnFSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonFSave.Click
        Dim FixedStatus As String = rbFixed.SelectedItem.Value.Trim()
        If FixedStatus = "Fixed" Then
            FixedStatus = "1"
            '=====================================================
            'Validasi jika pilih Fixed, maka harus pilih Collector
            '=====================================================
            If UcCollectorFixed.CollectorID = "0" Or UcCollectorFixed.CollectorID = "" Then
                lblMessage.Text = "Harap pilih collector"
                lblMessage.Visible = True
                Exit Sub
            End If
        Else
            FixedStatus = "0"
        End If

        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereAgreementNo = Me.WhereAgreementNo
            .Action = rbFixed.SelectedItem.Value.Trim
            .FixedStatus = FixedStatus
            .CollectorID = UcCollectorFixed.CollectorID
        End With

        lblMessage.Text = ""
        Try
            oController.SPVActFixed(oCustomClass)
            lblMessage.Text = "Simpan data Berhasil"
            lblMessage.Visible = True
            'Me.SearchBy = ""
            Me.SortBy = ""
            Bindgrid(Me.SearchBy, Me.SortBy)
            PnlList.Visible = True
            PnlSearch.Visible = True
            pnlFixed.Visible = False
            pnlRAL.Visible = False
            pnlCases.Visible = False
            PnlListChoosen.Visible = False

            '==========================
            'Bersihkan Search Criteria
            '==========================
            txtAgreementNo.Text = ""
            UcCollectorSearch.CollectorID = ""
            'chkCollectorReq.Checked = False
            txtCustomerName.Text = ""
            cboCasesSearch.SelectedIndex = 0
            txtOverdueDays.Text = ""

        Catch ex As Exception
            lblMessage.Text = ex.Message
            lblMessage.Visible = True
        End Try
    End Sub

    Private Sub btnCSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCSave.Click

        Dim oSpvAct As New Parameter.SPVActivity
        With oSpvAct
            .strConnection = GetConnectionString()
            .WhereAgreementNo = Me.WhereAgreementNo
            .casesID = cboCases.SelectedItem.Value
        End With


        lblMessage.Text = ""
        Try
            oController.SPVActCases(oSpvAct)
            lblMessage.Text = "Simpan data Berhasil"
            lblMessage.Visible = True
        Catch ex As Exception
            lblMessage.Text = ex.Message
            lblMessage.Visible = True
        End Try


        Me.SortBy = ""
        Bindgrid(Me.SearchBy, Me.SortBy)
        PnlList.Visible = True
        PnlSearch.Visible = True
        pnlCases.Visible = False
        pnlRAL.Visible = False
        pnlCases.Visible = False
        PnlListChoosen.Visible = False

        '==========================
        'Bersihkan Search Criteria
        '==========================
        txtAgreementNo.Text = ""
        UcCollectorSearch.CollectorID = ""
        'chkCollectorReq.Checked = False
        txtCustomerName.Text = ""
        cboCasesSearch.SelectedIndex = 0
        txtOverdueDays.Text = ""
    End Sub

    Private Sub btnRSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonRSave.Click
        Dim oSpvAct As New Parameter.SPVActivity
        With oSpvAct
            .strConnection = GetConnectionString()
            .WhereAgreementNo = Me.WhereAgreementNo
            .Action = lblPlanRAL.Text.Trim
            If .Action = "Undo Plan" Then
                .PlanDateRAL = CDate("1/1/1900")
            Else
                '-------------------
                'Validasi Plan Date
                '-------------------
                If txtRALDate.Text = "" Then
                    'lblMessage.Text = "Harap Input Tanggal Rencana untuk SKT"
                    lblMessage.Text = "Harap Input Tanggal Rencana untuk SKE"
                    lblMessage.Visible = True
                    Exit Sub
                End If
                If ConvertDate2(txtRALDate.Text) < Me.BusinessDate Then
                    lblMessage.Text = "Tanggal Rencana harus >=  Tanggal hari ini"
                    lblMessage.Visible = True
                    Exit Sub
                End If
                .PlanDateRAL = ConvertDate2(txtRALDate.Text)
            End If
        End With
        lblMessage.Text = ""
        Try
            oController.SPVActRAL(oSpvAct)
            lblMessage.Text = "Simpan data Berhasil"
            lblMessage.Visible = True
        Catch ex As Exception
            lblMessage.Text = ex.Message
            lblMessage.Visible = True
        End Try


        Me.SortBy = ""
        Bindgrid(Me.SearchBy, Me.SortBy)
        PnlList.Visible = True
        PnlSearch.Visible = True
        pnlRAL.Visible = False
        pnlRAL.Visible = False
        pnlCases.Visible = False
        PnlListChoosen.Visible = False

        '==========================
        'Bersihkan Search Criteria
        '==========================
        txtAgreementNo.Text = ""
        UcCollectorSearch.CollectorID = ""
        'chkCollectorReq.Checked = False
        txtCustomerName.Text = ""
        cboCasesSearch.SelectedIndex = 0
        txtOverdueDays.Text = ""

    End Sub

    Private Sub ImbPlanSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonlanSave.Click
        If txtPlanDate.Text.Trim = "" Then
            lblMessage.Text = "Harap isi Tanggal Rencana"
            lblMessage.Visible = True
            Exit Sub
        End If
        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereAgreementNo = Me.WhereAgreementNo
            .Action = ddlPlan.SelectedValue
            .PlanDateRAL = ConvertDate2(txtPlanDate.Text)
        End With
        If Not oController.SPVActivityPlanSave(oCustomClass) Then
            lblMessage.Text = "Simpan data Gagal"
            lblMessage.Visible = True
            Exit Sub
        End If
        InitialDefaultPanel()
        Bindgrid(Me.SearchBy, Me.SortBy)
        PnlList.Visible = True
    End Sub
#End Region

    Protected Function rbFixedChange() As String
        Return "rbFixedChange();"
    End Function

#Region "CheckAllCheckBox"

    Public Sub checkAll(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim loopitem As Int16
        Dim Chk As CheckBox
        lblMessage.Text = ""

        For loopitem = 0 To CType(Dtg.Items.Count - 1, Int16)
            Chk = New CheckBox
            Chk = CType(Dtg.Items(loopitem).FindControl("chkSelect"), CheckBox)

            If Chk.Enabled Then
                If CType(sender, CheckBox).Checked = True Then
                    Chk.Checked = True
                Else
                    Chk.Checked = False
                End If
            Else
                Chk.Checked = False
            End If
        Next
    End Sub
#End Region

#Region "Item DataBound"
    Private Sub Dtg_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles Dtg.ItemDataBound
        Dim lblAgreementNo As HyperLink
        Dim lblApplicationid As Label
        Dim lblCustomerID As Label
        Dim lblCustomerName As HyperLink
        Dim lbTemp As LinkButton
        If e.Item.ItemIndex >= 0 Then
            'Me.AgreementNo = e.Item.Cells(11).Text.Trim
            'Me.CustomerID = e.Item.Cells(12).Text.Trim
            'Me.AgreementNo = e.Item.Cells(1).Text.Trim
            'Me.CustomerID = e.Item.Cells(8).Text
            lblAgreementNo = CType(e.Item.FindControl("lnkAgreement"), HyperLink)
            lblApplicationid = CType(e.Item.FindControl("lblApplicationid"), Label)
            lblCustomerID = CType(e.Item.FindControl("lblCustomerID"), Label)
            lblCustomerName = CType(e.Item.FindControl("lblCustomerName"), HyperLink)
            lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & lblApplicationid.Text.Trim & "')"
            lblCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & lblCustomerID.Text & "')"
        End If
    End Sub
#End Region
End Class