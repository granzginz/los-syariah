﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class PDCRequestPrint
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.PDCRequestPrint
    Private oController As New PDCRequestPrintController

#End Region

    Private Property SelectAll() As Integer
        Get
            Return CInt(viewstate("SelectAll"))
        End Get
        Set(ByVal Value As Integer)
            viewstate("SelectAll") = Value
        End Set
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not IsPostBack Then
            If Request.QueryString("strFileLocation") <> "" Then
                Dim strHTTPServer As String
                Dim StrHTTPApp As String
                Dim strNameServer As String
                Dim strFileLocation As String

                strHTTPServer = Request.ServerVariables("PATH_INFO")
                strNameServer = Request.ServerVariables("SERVER_NAME")
                StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                strFileLocation = "http://" & strNameServer & "/" & StrHTTPApp & "/XML/" & Request.QueryString("strFileLocation") & ".pdf"
                '& "history.back(-1) " & vbCrLf _
                'Call file PDF 
                Response.Write("<script language = javascript>" & vbCrLf _
                & "window.open('" & strFileLocation & "','Collection', 'left=0, top=0, width=600, height=480, menubar=0, scrollbars=yes') " & vbCrLf _
                & "</script>")

            End If
            Me.FormID = "PDCRequest"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppID) Then
                InitialDefaultPanel()
                FillCbo()
            End If
        End If
    End Sub
    Private Sub InitialDefaultPanel()
        pnlDtGrid.Visible = False
        pnlsearch.Visible = True
    End Sub

    Private Sub FillCbo()
        Dim oDataTable As New DataTable
        With oCustomClass
            .strConnection = GetConnectionSTring
            .strKey = "CG"
            .CGID = Me.GroubDbID
            .CollectorType = ""
        End With
        oCustomClass = oController.PDCListCG(oCustomClass)
        oDataTable = oCustomClass.ListPDC
        With cboCG
            .DataSource = oDataTable
            .DataTextField = "CGName"
            .DataValueField = "CGID"
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = ""
            If oDataTable.Rows.Count = 1 Then
                .Items.FindByValue(Me.GroubDbID).Selected = True
            End If
        End With
    End Sub
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            lblMessage.Text = "Data tidak ditemukan ....."
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click
        lblMessage.Text = ""
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub
#End Region
    Sub CollectWhereCond()
        Dim strletter, strLetterDate, strvar As String

        '  Me.SearchBy = "PDCRequest.CGID='" & cboCG.SelectedItem.Value.Trim & "' and PDCRequest.PDCEmptyStatus='Y' "
        Me.SearchBy = "PDCRequest.CGID='" & cboCG.SelectedItem.Value.Trim & "' "
        If cboSearchBy.SelectedItem.Value.Trim <> "" Then
            If cboSearchBy.SelectedItem.Value.Trim = "IsPrintReminderLetter" Then
                Me.SearchBy = Me.SearchBy & " and PDCRequest.ReminderLetterNo<>'-'"
                If cboisPrinted.SelectedItem.Value.Trim <> "all" Then
                    Me.SearchBy = Me.SearchBy & " and PDCRequest.IsPrintReminderLetter = " & cboisPrinted.SelectedItem.Value.Trim & ""
                End If
                strletter = "ReminderLetterDate"
            ElseIf cboSearchBy.SelectedItem.Value.Trim = "IsPrintRequestLetter" Then
                Me.SearchBy = Me.SearchBy & " and PDCRequest.RequestLetterNo <>'-'"
                If cboisPrinted.SelectedItem.Value.Trim <> "all" Then
                    Me.SearchBy = Me.SearchBy & " and PDCRequest.IsPrintRequestLetter = " & cboisPrinted.SelectedItem.Value.Trim & ""
                End If
                strletter = "RequestLetterDate"
            End If
        End If
        If txtTaskDate1.Text.Trim <> "" Then
            strvar = "NextDueDate"
            'strLetterDate = " and" & strletter & "=convert(datetime,'" & txtTaskDate1.Text.Trim & "',103)"
            strLetterDate = " and" & strvar & "=convert(datetime,'" & txtTaskDate1.Text.Trim & "',103)"
            If txtTaskDate2.Text.Trim <> "" Then
                ' strLetterDate = " and " & strletter & " between convert(datetime,'" & txtTaskDate1.Text.Trim & "',103) and convert(datetime,'" & txtTaskDate2.Text.Trim & "',103)"
                strLetterDate = " and " & strvar & " between convert(datetime,'" & txtTaskDate1.Text.Trim & "',103) and convert(datetime,'" & txtTaskDate2.Text.Trim & "',103)"
            End If
        Else
            strLetterDate = ""
        End If


        Me.SearchBy += strLetterDate
        Me.SortBy = ""

    End Sub

    Private Sub imgsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Buttonsearch.Click
        CollectWhereCond()
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        InitialDefaultPanel()
        cboSearchBy.ClearSelection()
        lblMessage.Text = ""
    End Sub

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        Dim intloop As Integer
        Dim hypID As HyperLink

        With oCustomClass
            .strConnection = GetConnectionString
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oCustomClass = oController.PDCRequestList(oCustomClass)

        DtUserList = oCustomClass.ListPDC
        DvUserList = DtUserList.DefaultView
        recordCount = oCustomClass.TotalRecord
        DvUserList.Sort = Me.SortBy
        dtgPDCRequest.DataSource = DvUserList

        Try
            dtgPDCRequest.DataBind()
        Catch
            dtgPDCRequest.CurrentPageIndex = 0
            dtgPDCRequest.DataBind()
        End Try
        PagingFooter()
        pnlDtGrid.Visible = True
        pnlsearch.Visible = True
        lblMessage.Text = ""
    End Sub

    Private Sub imbPrintSurat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonPrintSurat.Click
        'If checkfeature(Me.Loginid, Me.FormID, "Surat", Me.AppID) Then
        Dim hasil As Integer
        CollectWhereCond()
        With oCustomClass
            .strConnection = GetConnectionString()
            .BusinessDate = Me.BusinessDate
            .Letter = cboSearchBy.SelectedItem.Value.Trim
            .WhereCond = Me.SearchBy
        End With
        oCustomClass = oController.PDCRequestSave(oCustomClass)
        hasil = oCustomClass.hasil
        If hasil = 0 Then
            lblMessage.Text = "Cetak Gagal"
            Exit Sub
        Else
            Dim cookie As HttpCookie = Request.Cookies("RptPDCRequest")
            If Not cookie Is Nothing Then
                cookie.Values("Letter") = cboSearchBy.SelectedItem.Value.Trim
                cookie.Values("wherecond") = Me.SearchBy
                Response.AppendCookie(cookie)
            Else
                Dim cookieNew As New HttpCookie("RptPDCRequest")
                cookieNew.Values.Add("Letter", cboSearchBy.SelectedItem.Value.Trim)
                cookieNew.Values.Add("wherecond", Me.SearchBy)
                Response.AppendCookie(cookieNew)
            End If

            Response.Redirect("PDCRequestsurat.aspx")
        End If

        'End If
    End Sub
    Private Sub imbPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonPrint.Click
        If CheckFeature(Me.Loginid, Me.FormID, "Print", Me.AppID) Then
            'Dim strAgreement As HyperLink
            'Dim chkPDC As CheckBox
            'Dim oDataTable As New DataTable
            'Dim oRow As DataRow
            'Dim intloop, hasil As Integer
            'With oCustomClass
            '    .strConnection = GetConnectionString
            '    .BusinessDate = Me.BusinessDate
            '    .Letter = cboSearchBy.SelectedItem.Value.Trim
            'End With
            'oCustomClass = oController.PDCRequestSave(oCustomClass)
            'hasil = oCustomClass.hasil
            'If hasil = 0 Then
            '    lblMessage.Text = "Print Data failed"
            '    Exit Sub
            'Else
            CollectWhereCond()
            Dim cookie As HttpCookie = Request.Cookies("RptPDCRequest")
            If Not cookie Is Nothing Then
                cookie.Values("Letter") = cboSearchBy.SelectedItem.Value.Trim
                cookie.Values("wherecond") = Me.SearchBy
                Response.AppendCookie(cookie)
            Else
                Dim cookieNew As New HttpCookie("RptPDCRequest")
                cookieNew.Values.Add("Letter", cboSearchBy.SelectedItem.Value.Trim)
                cookieNew.Values.Add("wherecond", Me.SearchBy)
                Response.AppendCookie(cookieNew)
            End If
            Response.Redirect("PDCRequestViewer.aspx")
            'End If
        End If
    End Sub

    Public Sub checkAll(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim intloopgrid As Integer
        Dim ChkSelect As CheckBox
        If Me.SelectAll = 0 Then
            For intloopgrid = 0 To dtgPDCRequest.Items.Count - 1
                ChkSelect = CType(dtgPDCRequest.Items(intloopgrid).FindControl("chkPDC"), CheckBox)
                ChkSelect.Checked = True
            Next
            Me.SelectAll = 1
        Else
            For intloopgrid = 0 To dtgPDCRequest.Items.Count - 1
                ChkSelect = CType(dtgPDCRequest.Items(intloopgrid).FindControl("chkPDC"), CheckBox)
                ChkSelect.Checked = False
            Next
            Me.SelectAll = 0
        End If
    End Sub

    Private Sub dtgPDCRequest_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPDCRequest.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim hyTemp As HyperLink
            Dim lblApplicationId As Label
            '*** Agreement No link
            hyTemp = CType(e.Item.FindControl("hyAgreementNo"), HyperLink)
            lblApplicationId = CType(e.Item.FindControl("lblApplicationId"), Label)
            hyTemp.NavigateUrl = "javascript:OpenAgreementNo('" & "Collection" & "', '" & Server.UrlEncode(lblApplicationId.Text.Trim) & "')"
        End If
    End Sub

End Class