﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CollectorSupervisor.aspx.vb"
    Inherits="Maxiloan.Webform.ARMgt.CollectorSupervisor" %>

<%@ Register TagPrefix="uc1" TagName="ucCollectorRpt" Src="../../UserController/ucCollectorRpt.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcCollector" Src="../../UserController/UcCollector.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>CollectorSupervisor</title>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;

        var ServerName = '<%=Request.ServerVariables("SERVER_NAME")%>';
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);

        function fback() {
            history.go(-1);
            return false;

        }
        function OpenWinCust(pStrCustID, pStrType) {
            var lStrAddress;
            if (pStrType == 'P')
                lStrAddress = 'ViewSearch.aspx';
            else
                lStrAddress = 'ViewSearchCompany.aspx';

            window.open('http://' + ServerName + '/' + App + '/Application/SmartSearch/' + lStrAddress + '?custid=' + pStrCustID, 'WinLink', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0,scrollbars=1,resizable=1,center=1');
        }

        function OpenWinAgreement(pStrAgreementNo) {
            window.open('http://' + ServerName + '/' + App + '/Application/SmartSearch/ViewAgreementDetail.aspx?AgreementNo=' + pStrAgreementNo, 'WinLink', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0,scrollbars=1,resizable=1,center=1');
        }

        function OpenWinAsset(pStrApplicationID, pStrAgreementNo, asq) {
            window.open('http://' + ServerName + '/' + App + '/Application/SmartSearch/perAsset.aspx?applicationid=' + pStrApplicationID + '&agreementno=' + pStrAgreementNo + '&assetseqno=' + asq, 'WinLink', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=1,resizable=1,center=1');
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>    
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">           
            <h3>
                AKTIVITAS SUPERVISOR
            </h3>
        </div>
    </div>  
    <asp:Panel ID="PnlSearch" runat="server" >
    <div class="form_box">	    
		    <div class="form_left">
                <label>No Kontrak</label>
                <asp:TextBox ID="txtAgreementNo" runat="server" ></asp:TextBox>
		    </div>
		    <div class="form_right">	
                <label>Nama Customer</label>	
                <asp:TextBox ID="txtCustomerName" runat="server" ></asp:TextBox>	
		    </div>	    
    </div>
    <div class="form_box">
		    <div class="form_left">
                <label>Collector</label>
                <uc1:UcCollector id="UcCollectorSearch" runat="server"></uc1:UcCollector>
		    </div>
		    <div class="form_right">
                <label>Kasus</label>		
                <asp:DropDownList ID="cboCasesSearch" runat="server" DataTextField="Description"
                    DataValueField="CasesId">
                </asp:DropDownList>	
		    </div>	    
    </div>
    <div class="form_box">	    
		    <div class="form_left">
                <label>Permintaan Collector</label>
                <asp:CheckBox ID="chkCollectorReq" runat="server"></asp:CheckBox>
		    </div>
		    <div class="form_right">		
                <label>Jumlah Hari Telat &gt;=</label>	
                <asp:TextBox ID="txtOverdueDays" runat="server" Width="50px"  MaxLength="3"></asp:TextBox>
		    </div>	    
    </div>       
    <div class="form_button">
        <asp:Button ID="Buttonsearch" runat="server"  Text="Search" CssClass ="small button blue"></asp:Button>&nbsp;
        <asp:Button ID="ButtonReset" runat="server"  Text="Reset" CssClass ="small button gray" CausesValidation="False"></asp:Button>
	</div>               
    </asp:Panel>
    <asp:Panel ID="PnlList" runat="server">
    <div class="form_box_title">
        <div class="form_single">              
            <h4>
                DAFTAR AKTIVITAS SUPERVISOR
            </h4>
        </div>
    </div>       
    <div class="form_box_header">
    <div class="form_single">
        <div class="grid_wrapper_ns">
        <asp:DataGrid ID="Dtg" runat="server" BorderStyle="None" BorderWidth="0" OnSortCommand="Sorting" AllowSorting="True"
             AutoGenerateColumns="False" CssClass="grid_general">
            <HeaderStyle CssClass="th" />
            <ItemStyle CssClass="item_grid" />
            <Columns>
                <asp:TemplateColumn HeaderText="NO">                   
                    <HeaderTemplate>
                        <asp:CheckBox ID="chkAll" runat="server" AutoPostBack="true"></asp:CheckBox>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:CheckBox ID="chkSelect" runat="server"></asp:CheckBox>
                        <asp:Label runat="server" ID="lblCounter" Visible="false"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">                    
                    <ItemTemplate>
                        <asp:Image ID="imgBulat" runat="server" ImageUrl="../../Images/BulatMerah.gif" Visible='<%# iif(container.dataitem("IsRequestAssign")="Y",true,false)%>'>
                        </asp:Image>
                        <asp:HyperLink ID="lnkAgreementNo" Enabled="True" runat="server" Text='<%# container.dataitem("AgreementNo")%>'>
                        </asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:BoundColumn DataField="CustomerName" SortExpression="CustomerName" HeaderText="NAMA CUSTOMER">                    
                </asp:BoundColumn>
                <asp:BoundColumn DataField="CustomerAddress" SortExpression="CustomerAddress" HeaderText="ALAMAT">
                </asp:BoundColumn>
                <asp:BoundColumn DataField="CollectorName" SortExpression="CollectorName" HeaderText="COLLECTOR">                    
                </asp:BoundColumn>
                <asp:BoundColumn DataField="OverDueDays" SortExpression="OverDueDays" HeaderText="HARI TELAT">                    
                </asp:BoundColumn>
                <asp:BoundColumn DataField="Amount_Overdue_Gross" SortExpression="Amount_Overdue_Gross"
                    HeaderText="JUMLAH TUNGGAKAN" DataFormatString="{0:N2}">                    
                </asp:BoundColumn>
                <asp:BoundColumn DataField="OS_Gross" SortExpression="OS_Gross" HeaderText="SISA A/R"
                    DataFormatString="{0:N2}">                    
                <ItemStyle CssClass="item_grid_right"></ItemStyle>
                </asp:BoundColumn>
                <asp:TemplateColumn Visible="False" HeaderText="AgreementNo">
                    <ItemTemplate>
                        <asp:Label ID="lblCollectorId" runat="server" Text='<%# container.dataitem("CollectorId")%>'>
                        </asp:Label>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:BoundColumn DataField="RALDate" SortExpression="RALDate" HeaderText="TGL SKE">
                </asp:BoundColumn>
                <asp:BoundColumn DataField="FIXEDASSIGN" SortExpression="FIXEDASSIGN" HeaderText="FIXED ASSIGN">
                </asp:BoundColumn>
            </Columns>            
        </asp:DataGrid>
        <div class="button_gridnavigation">
	                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
		                CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
		                CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
		                CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
		                CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
	                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
		                EnableViewState="False"></asp:Button>
	                <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage"
		                MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer"
		                CssClass="validator_general"></asp:RangeValidator>
	                <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
		                ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>
        <div class="label_gridnavigation">
	        <asp:Label ID="lblPage" runat="server"></asp:Label>of
	        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
	        <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
        </div>
        </div>        
    </div>                             
    </div>
    </asp:Panel>    
    <asp:Panel ID="pnlFixed" runat="server" Visible="False">
    <div class="form_box_title">
        <div class="form_single">              
            <h4>
                <strong>FIXED / UNFIXED ASSIGN</strong>
            </h4>
        </div>
    </div>
    <div class="form_box">
	    <div class="form_single">
            <label class="label_general">Pilihan</label>
            <asp:RadioButtonList ID="rbFixed" runat="server" class="opt_single"  RepeatDirection="Horizontal" AutoPostBack="True">
                <asp:ListItem Value="Fixed" Selected="True">Fixed</asp:ListItem>
                <asp:ListItem Value="Unfixed">UnFixed</asp:ListItem>
            </asp:RadioButtonList>
	    </div>
    </div>
    <div class="form_box">
	    <div class="form_single">
            <label>Alokasi Kepada</label>
            <uc1:UcCollector id="UcCollectorFixed" runat="server"></uc1:UcCollector>	    
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="ButtonFSave" runat="server"  Text="Save" CssClass ="small button blue"
            CausesValidation="False"></asp:Button>&nbsp;
        <asp:Button ID="ButtonFCancel" runat="server"  Text="Cancel" CssClass ="small button gray"
            CausesValidation="False"></asp:Button>
	</div>    
    </asp:Panel>    
    <asp:Panel ID="pnlRAL" runat="server" Visible="False">
    <div class="form_box_title">
        <div class="form_single">              
            <h4>
                <%--<b>PLAN / UNDO PLAN MENJADI SKT</b>--%>
                <b>PLAN / UNDO PLAN MENJADI SKE</b>
            </h4>
        </div>
    </div>
    <div class="form_box">
	    <div class="form_single">
            <label class="label_general">Pilihan</label>
            <asp:RadioButtonList ID="rbRAL" runat="server" class="opt_single"  RepeatDirection="Horizontal"
                AutoPostBack="True">
                <asp:ListItem Value="Plan" Selected="True">Plan</asp:ListItem>
                <asp:ListItem Value="UndoPlan">Undo Plan</asp:ListItem>
            </asp:RadioButtonList>
	    </div>
    </div>
    <div class="form_box">
	    <div class="form_single">
            <%--<label>Tanggal SKT</label>--%>
            <label>Tanggal SKE</label>
            <asp:Label ID="validAUntilDate" runat="server" ForeColor="Red"></asp:Label>
	    </div>
    </div>    
    <div class="form_button">
        <asp:Button ID="ButtonASave" runat="server"  Text="Save" CssClass ="small button blue"
            CausesValidation="False" Visible="False"></asp:Button>&nbsp;
        <asp:Button ID="ButtonACancel" runat="server"  Text="Cancel" CssClass ="small button gray">
        </asp:Button>
	</div>       
    </asp:Panel>    
    <asp:Panel ID="pnlCases" runat="server" Visible="False">
    <div class="form_box_title">
        <div class="form_single">              
            <h4>
                <strong>KASUS DITAMBAHKAN</strong>
            </h4>
        </div>
    </div>
    <div class="form_box">
	    <div class="form_single">
            <label>Kasus</label>
            <asp:DropDownList ID="cboCases" runat="server" DataTextField="Description" DataValueField="CasesId">
            </asp:DropDownList>
	    </div>
    </div>
    <div class="form_button">
        <asp:Button ID="ButtonButton2" runat="server"  Text="Save" CssClass ="small button blue"
            CausesValidation="False"></asp:Button>&nbsp;
        <asp:Button ID="ButtonButton1" runat="server"  Text="Cancel" CssClass ="small button gray"
            CausesValidation="False"></asp:Button>
	</div>       
    </asp:Panel>    
    <asp:Panel ID="PnlListChoosen" runat="server">
    <div class="form_box_title">
	    <div class="form_single">
            <asp:Label ID="lblMenuTask" runat="server"></asp:Label>
	    </div>
    </div>
    <div class="form_box_header">
    <div class="form_single">
        <div class="grid_wrapper_ns">
            <asp:DataGrid ID="dtgchoosen" runat="server" AutoGenerateColumns="False"
            Visible="False" BorderStyle="None" BorderWidth="0" DataKeyField="AgreementNo" CssClass="grid_general">
            <HeaderStyle CssClass="th" />
            <ItemStyle CssClass="item_grid" />
            <Columns>
                <asp:BoundColumn DataField="id" HeaderText="NO"></asp:BoundColumn>
                <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">
                    <ItemTemplate>
                        <asp:Image ID="Image1" runat="server" ImageUrl="../../Images/BulatMerah.gif" Visible='<%# iif(container.dataitem("IsRequestAssign")="Y",true,false)%>'>
                        </asp:Image>
                        <asp:HyperLink ID="lnkFAgreementNo" runat="server" Text='<%# container.dataitem("AgreementNo")%>'>
                        </asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:BoundColumn DataField="CustomerName" SortExpression="CustomerName" HeaderText="NAMA CUSTOMER">
                </asp:BoundColumn>
                <asp:BoundColumn DataField="CollectorName" SortExpression="CollectorName" HeaderText="COLLECTOR">
                </asp:BoundColumn>
                <asp:BoundColumn DataField="EndPastDueDays" SortExpression="EndPastDueDays" HeaderText="HARI TELAT">
                </asp:BoundColumn>
                <asp:BoundColumn DataField="Cases" SortExpression="Cases" HeaderText="KASUS"></asp:BoundColumn>
                <asp:BoundColumn DataField="FixedAssign" SortExpression="FixedAssign" HeaderText="FIXED ASSIGN">
                </asp:BoundColumn>
                <asp:TemplateColumn Visible="False" HeaderText="AgreementNo">
                    <ItemTemplate>
                        <asp:Label ID="lblFCollectorId" runat="server" Text='<%# container.dataitem("CollectorId")%>'>
                        </asp:Label>
                    </ItemTemplate>
                </asp:TemplateColumn>
            </Columns>            
            </asp:DataGrid>            
    </div>          
    </div>    
    </div>    
    <div class="form_box">
	    <div class="form_single">            
            <asp:RadioButtonList ID="rbTask" runat="server"  class="opt_single"  RepeatDirection="Horizontal">
                <asp:ListItem Value="FU">Fixed / Unfixed</asp:ListItem>
                <asp:ListItem Value="CA">Tambahkan Kasus</asp:ListItem>
                <%--<asp:ListItem Value="PRAL">Plan / Undo Plan SKT</asp:ListItem>--%>
                <asp:ListItem Value="PRAL">Plan / Undo Plan SKE</asp:ListItem>
                <%--<asp:ListItem Value="D">Release Titik Merah</asp:ListItem>--%>
            </asp:RadioButtonList>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="rbTask" CssClass="validator_general"
                ErrorMessage="Harap pilih Pilihan" Display="Dynamic"></asp:RequiredFieldValidator>
	    </div>
    </div>           
    <div class="form_button">
        <asp:Button ID="ButtonDoAction" runat="server"  Text="Action" CssClass ="small button blue" EnableViewState="False"></asp:Button>
	</div>                 
    </asp:Panel>
    </form>
</body>
</html>
