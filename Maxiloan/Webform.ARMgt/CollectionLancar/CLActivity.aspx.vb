﻿#Region "Imports"
Imports System.Data.SqlClient
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class CLActivity
    Inherits Maxiloan.Webform.WebBased

    Protected WithEvents oAction As ucAction

    Protected WithEvents ucCollActResult As ucCollectionActResult
#Region "Property"
    Private Property ValidDueDate() As String
        Get
            Return CType(viewstate("ValidDueDate"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ValidDueDate") = Value
        End Set
    End Property

    Private Property ApplicationID() As String
        Get
            Return CType(viewstate("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property

    Private Property SearchBy2() As String
        Get
            Return CType(ViewState("SearchBy2CL"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("SearchBy2CL") = Value
        End Set
    End Property

    Private Property CGID() As String
        Get
            Return CType(viewstate("CGID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CGID") = Value
        End Set
    End Property

    Private Property NextDueDate() As String
        Get
            Return CType(viewstate("NextDueDate"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("NextDueDate") = Value
        End Set
    End Property

    Private Property AgreementNo() As String
        Get
            Return CType(viewstate("AgreementNo"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AgreementNo") = Value
        End Set
    End Property
    Private Property CustomerID() As String
        Get
            Return CType(viewstate("CustomerID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerID") = Value
        End Set
    End Property

    Private Property CustomerName() As String
        Get
            Return CType(viewstate("CustomerName"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerName") = Value
        End Set
    End Property
    Public Property strAll() As String
        Get
            Return CType(viewstate("strAll"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("strAll") = Value
        End Set
    End Property
    Public Property strKey() As String
        Get
            Return CType(viewstate("strKey"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("strKey") = Value
        End Set
    End Property

    Private Property MaxPromiseDateBank() As String
        Get
            Return CType(ViewState("MaxPromiseDateBank"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("MaxPromiseDateBank") = Value
        End Set
    End Property

    Private Property MaxPromiseDateCompany() As String
        Get
            Return CType(ViewState("MaxPromiseDateCompany"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("MaxPromiseDateCompany") = Value
        End Set
    End Property

#End Region

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.CLActivity
    Private oController As New CLActivityController
    Private oController1 As New CLActivityController
    Dim chrClientID As String

    Private m_CollZipCode As New CollZipCodeController
#End Region

#Region "LinkTo"
    Function LinkToAgreement(ByVal strAgreementNo As String) As String
        Return "javascript:OpenWinAgreement('" & strAgreementNo & "')"
    End Function
    Function LinkToCustomer(ByVal strCustomerID As String, ByVal strStyle As String) As String
        Return "javascript:OpenWinCustomer('" & strCustomerID & "','" & strStyle & "')"
    End Function

#End Region


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.FormID = "CollCLAct"
            If CheckForm(Me.Loginid, Me.FormID, "Maxiloan") Then

                pnlDtGrid.Visible = False
                pnlResult.Visible = False
                pnlsearch.Visible = True

                txtPlanDate.Text = Format(Me.BusinessDate, "dd/MM/yyyy")
                txtInputDate.Text = Format(Me.BusinessDate, "dd/MM/yyyy")

                Me.strKey = "CL"
                chrClientID = Trim(cboChild.ClientID)
                'FillCbo()
                '-------------gantinya usercontrol------------------
                Dim dtParent As New DataTable
                Dim Coll As New Parameter.RptCollAct
                Dim ControllerCG As New RptCollActController
                'Dim CollList As New Parameter.Collector
                'RequiredFieldValidator1.Enabled = False
                If Me.strAll = "1" Then
                    With Coll
                        .strConnection = GetConnectionString()
                        .CGID = Me.GroubDbID
                        .strKey = "CG_ALL"
                        .CollectorType = ""
                    End With
                Else
                    With Coll
                        .strConnection = GetConnectionString()
                        .CGID = Me.GroubDbID
                        .strKey = "CG"
                        .CollectorType = ""
                    End With
                End If
                Coll = ControllerCG.ViewDataCollector(Coll)
                dtParent = Coll.ListCollector

                cboParent.DataTextField = "CGName"
                cboParent.DataValueField = "CGID"
                cboParent.DataSource = dtParent
                cboParent.DataBind()
                cboParent.Items.Insert(0, "Select One")
                cboParent.Items(0).Value = "0"
                'getchildcombo()

                BindCollector()                
            End If
        End If
    End Sub

    Private Sub BindCollector()
        Dim oCollZipCode As New Parameter.CollZipCode
        Dim strconn As String = getConnectionString()
        Dim dt As New DataTable

        With oCollZipCode
            .strConnection = getConnectionString()
            .CGID = Me.GroubDbID
            .CollectorType = "CL"
        End With

        dt = m_CollZipCode.GetCollectorCombo(oCollZipCode)

        cboChild.DataSource = dt
        cboChild.DataTextField = "CollectorName"
        cboChild.DataValueField = "CollectorID"
        cboChild.DataBind()
        cboChild.Items.Insert(0, "Select One")
        cboChild.Items(0).Value = "0"
    End Sub

#Region "Tambahan"
    Protected Sub getchildcombo()
        Dim dtChild As New DataTable
        Dim Coll As New Parameter.CLActivity
        Dim CollList As New Parameter.CLActivity
        With Coll
            .strConnection = GetConnectionString()
            .CGID = Me.GroubDbID
            .LoginId = Me.Loginid
            .strKey = Me.strKey
        End With
        CollList = oController1.CLActivityListCollector(Coll)
        dtChild = CollList.ListCLActivity
        Response.Write(GenerateScript(dtChild))
    End Sub
    Protected Function CollectionGroupIDChange() As String
        Return "ParentChange('" & Trim(cboParent.ClientID) & "','" & Trim(cboChild.ClientID) & "','" & Trim(hdnChildValue.ClientID) & "','" & Trim(hdnChildName.ClientID) & "',((this.selectedIndex == -1) ? null : ListData[this.selectedIndex]));"
    End Function

    Private Function GenerateScript(ByVal DtTable As DataTable) As String
        Dim strScript As String
        Dim strScript1 As String
        Dim DataRow As DataRow()
        Dim strType As String
        Dim i As Int32
        Dim j As Int32
        strScript = "<script language=""JavaScript"">" & vbCrLf
        strScript &= "ListData = new Array(" & vbCrLf
        strType = ""
        For j = 0 To cboParent.Items.Count - 1
            DataRow = DtTable.Select(" CGID = '" & cboParent.Items(j).Value & "'")
            If DataRow.Length > 0 Then
                For i = 0 To DataRow.Length - 1
                    If strType <> CStr(DataRow(i)("CGID")).Trim Then
                        strType = CStr(DataRow(i)("CGID")).Trim
                        strScript &= "new Array(" & vbCrLf
                        strScript1 = ""
                    End If


                    If strScript1 = "" Then
                        strScript1 = " new Array('" & CStr(DataRow(i)("Name")).Trim & "','" & CStr(IIf(IsDBNull(DataRow(i)("CollectorID")), "null", DataRow(i)("CollectorID"))).Trim & "') "
                    Else
                        strScript1 &= "," & vbCrLf & " new Array('" & CStr(DataRow(i)("Name")).Trim & "','" & CStr(IIf(IsDBNull(DataRow(i)("CollectorID")), "null", DataRow(i)("CollectorID"))).Trim & "') "
                    End If
                Next
                strScript &= strScript1 & ")," & vbCrLf
            Else
                strScript &= " null," & vbCrLf
            End If
        Next

        If Right(strScript.Trim, 5) = "null," Then
            strScript = Left(strScript.Trim, strScript.Trim.Length - 1)
        Else
            strScript = Left(strScript.Trim, strScript.Trim.Length - 2)
        End If

        If Right(strScript.Trim, 4) = "null" Then
            strScript &= vbCrLf & ");" & vbCrLf
        Else
            strScript &= vbCrLf & "));" & vbCrLf
        End If
        'strScript &= vbCrLf & "));" & vbCrLf

        strScript &= "</script>"
        Return strScript
    End Function
#End Region
 
    Private Sub Buttonsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Buttonsearch.Click

        'Me.SearchBy = "COllectionAgreement.CGID='" & cboParent.SelectedItem.Value.Trim & "' "
        'Me.SearchBy = Me.SearchBy & " and COllectionAgreement.COllectorID='" & cboChild.SelectedValue.Trim & "'"
        'harusnya pake dcr saja, jika COllectionAgreement terjadi deadlock
        Me.SearchBy = "DCR.CGID='" & cboParent.SelectedItem.Value.Trim & "' "
        Me.SearchBy = Me.SearchBy & " and DCR.COllectorID='" & cboChild.SelectedValue.Trim & "'"

        Me.SearchBy2 = "DCR.CGID='" & cboParent.SelectedItem.Value.Trim & "' " & " and DCR.COllectorID='" & cboChild.SelectedValue.Trim & "'"

        If cboSearchBy.SelectedItem.Value <> "Select One" Then
            If txtSearchBy.Text.Trim <> "" Then
                Me.SearchBy = Me.SearchBy + " and " + cboSearchBy.SelectedItem.Value + " like'%" + txtSearchBy.Text.Trim + "%'"
            End If
        End If
        pnlDtGrid.Visible = True
        pnlResult.Visible = False
        Me.SortBy = ""
        DoBind(Me.SearchBy, Me.SortBy, Me.SearchBy2)
    End Sub
    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String, ByVal where2 As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView


        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
            .WhereCond2 = where2
        End With

        oCustomClass = oController.CLActivityList(oCustomClass)

        DtUserList = oCustomClass.ListCLActivity
        DvUserList = DtUserList.DefaultView
        recordCount = oCustomClass.TotalRecord
        DvUserList.Sort = Me.SortBy
        dtgCLActivity.DataSource = DvUserList

        Try
            dtgCLActivity.DataBind()
        Catch
            dtgCLActivity.CurrentPageIndex = 0
            dtgCLActivity.DataBind()
        End Try



        PagingFooter()
        pnlDtGrid.Visible = True
        pnlResult.Visible = False
        pnlsearch.Visible = True

    End Sub
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy, Me.SearchBy2)
    End Sub

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then            
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SearchBy, Me.SortBy, Me.SearchBy2)
    End Sub

    Protected Sub btnPageNumb_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy, Me.SearchBy2)
            End If
        End If
    End Sub
#End Region
    Private Sub dtgCLActivity_ItemCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgCLActivity.ItemCommand
        If e.CommandName = "result" Then
            If CheckFeature(Me.Loginid, Me.FormID, "Rsult", Me.AppId) Then

                Dim oDataTable As New DataTable
                Dim lblAgreementNo As HyperLink
                Dim lblApplicationid As Label
                Dim lblCustomerID As Label
                Dim lblCustomerName As HyperLink

                lblAgreementNo = CType(dtgCLActivity.Items(e.Item.ItemIndex).FindControl("hypAgreementNodtg"), HyperLink)
                lblApplicationid = CType(dtgCLActivity.Items(e.Item.ItemIndex).FindControl("hypApplicationID"), Label)
                lblCustomerID = CType(dtgCLActivity.Items(e.Item.ItemIndex).FindControl("lblCustomerID"), Label)
                lblCustomerName = CType(dtgCLActivity.Items(e.Item.ItemIndex).FindControl("hypCustomerNamedtg"), HyperLink)
                Me.AgreementNo = lblAgreementNo.Text
                Me.CustomerID = lblCustomerID.Text
                Me.CustomerName = lblCustomerName.Text
                Me.ApplicationID = lblApplicationid.Text


                Me.AgreementNo = lblAgreementNo.Text.Trim
                With oCustomClass
                    .strConnection = GetConnectionString()
                    .ApplicationID = lblApplicationid.Text.Trim
                End With
                oCustomClass = oController.CLActivityDataAgreement(oCustomClass)
                oDataTable = oCustomClass.ListCLActivity
                pnlDtGrid.Visible = False
                pnlResult.Visible = True
                pnlsearch.Visible = False
                FillData(oDataTable)
            End If
        End If
    End Sub
    Private Sub FillData(ByVal oDataTable As DataTable)
        Dim oRow As DataRow
        Dim oResult As New DataTable
        Dim dtAction As New DataTable
        If oDataTable.Rows.Count > 0 Then
            cboPlanActivity.ClearSelection()
            'txtPlanDate.Text = ""            
            txtPromiseDate.Text = ""
            'txtHour.Text = CStr(Hour(Now()))
            'txtMinute.Text = CStr(Minute(Now))

            chkRequestSpv.Checked = False
            txtNotes.Text = ""
            cboSuccess.ClearSelection()
            oRow = oDataTable.Rows(0)

            'Ambil dari db supaya pas save ngga null, Wira 20160727
            If Not IsDBNull(oRow("PlanDate")) Then
                'ShowMessage(lblMessage, "Harap Isi Rencana DCR dulu di menu Rencana DCR", True)
                txtPlanDate.Text = CType(oRow("PlanDate"), String)
                txtHour.Text = CType(oRow("PlanDateHour"), String)
                txtMinute.Text = CType(oRow("PlanDateMinute"), String)
            End If

            ucCollActResult.hypAgreementNo.Text = CType(oRow("AgreementNo"), String)
            ucCollActResult.hypCustomerName.Text = CType(oRow("Name"), String)
            hdnAgreement.Value = CType(oRow("AgreementNo"), String)
            hdnCustName.Value = CType(oRow("Name"), String)
            hdnCustID.Value = CType(oRow("CustomerID"), String)

            ucCollActResult.hypAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "Collection" & "', '" & Me.ApplicationID & "')"
            ucCollActResult.hypCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "Collection" & "', '" & Me.CustomerID & "')"


            If Not IsDBNull(oRow("Address")) Then
                ucCollActResult.lblAddress.Text = CType(oRow("Address"), String)
            End If
            If Not IsDBNull(oRow("Description")) Then
                ucCollActResult.lblAsset.Text = CType(oRow("Description"), String)
            End If
            If Not IsDBNull(oRow("LicensePlate")) Then
                ucCollActResult.lblLicenseNo.Text = CType(oRow("LicensePlate"), String)
            End If
            If Not IsDBNull(oRow("EmployeeName")) Then
                ucCollActResult.lblCMONo.Text = CType(oRow("EmployeeName"), String)
            End If

            If CType(oRow("LegalPhoneYN"), String) = "Y" Then
                ucCollActResult.lblLegalPhone1.ForeColor = System.Drawing.Color.Blue
                ucCollActResult.lblLegalPhone2.ForeColor = System.Drawing.Color.Blue
            End If
            If Not IsDBNull(oRow("LegalPhone1")) Then
                ucCollActResult.lblLegalPhone1.Text = CType(oRow("LegalPhone1"), String)
            End If
            If Not IsDBNull(oRow("LegalPhone2")) Then
                ucCollActResult.lblLegalPhone2.Text = CType(oRow("LegalPhone2"), String)
            End If
            If CType(oRow("HomePhoneYN"), String) = "Y" Then
                ucCollActResult.lblResidencePhone1.ForeColor = System.Drawing.Color.Blue
                ucCollActResult.lblResidencePhone2.ForeColor = System.Drawing.Color.Blue
            End If
            If Not IsDBNull(oRow("ResidencePhone1")) Then
                ucCollActResult.lblResidencePhone1.Text = CType(oRow("ResidencePhone1"), String)
            End If
            If Not IsDBNull(oRow("ResidencePhone2")) Then
                ucCollActResult.lblResidencePhone2.Text = CType(oRow("ResidencePhone2"), String)
            End If
            If CType(oRow("CompanyPhoneYN"), String) = "Y" Then
                ucCollActResult.lblCompanyPhone1.ForeColor = System.Drawing.Color.Blue
                ucCollActResult.lblCompanyPhone2.ForeColor = System.Drawing.Color.Blue
            End If
            If Not IsDBNull(oRow("CompanyPhone1")) Then
                ucCollActResult.lblCompanyPhone1.Text = CType(oRow("CompanyPhone1"), String)
            End If
            If Not IsDBNull(oRow("CompanyPhone2")) Then
                ucCollActResult.lblCompanyPhone2.Text = CType(oRow("CompanyPhone2"), String)
            End If
            If Not IsDBNull(oRow("MobilePhone")) Then
                ucCollActResult.lblMobilePhone.Text = CType(oRow("MobilePhone"), String)
            End If
            If CType(oRow("MailingPhoneYN"), String) = "Y" Then
                ucCollActResult.lblMailingPhone1.ForeColor = System.Drawing.Color.Blue
                ucCollActResult.lblMailingPhone2.ForeColor = System.Drawing.Color.Blue
            End If
            If Not IsDBNull(oRow("MailingPhone1")) Then
                ucCollActResult.lblMailingPhone1.Text = CType(oRow("MailingPhone1"), String)
            End If
            If Not IsDBNull(oRow("MailingPhone2")) Then
                ucCollActResult.lblMailingPhone2.Text = CType(oRow("MailingPhone2"), String)
            End If

            If Not IsDBNull(oRow("SIPhone1")) Then
                ucCollActResult.lblSuamiIstriPhone1.Text = CType(oRow("SIPhone1"), String)
            End If
            If Not IsDBNull(oRow("SIPhone2")) Then
                ucCollActResult.lblSuamiIstriPhone2.Text = CType(oRow("SIPhone2"), String)
            End If

            If Not IsDBNull(oRow("EndPastDueDays")) Then
                ucCollActResult.lblOverDuedays.Text = CType(oRow("EndPastDueDays"), String)
            End If
            If Not IsDBNull(oRow("CollectorID")) Then
                hdnCollector.Value = CType(oRow("CollectorID"), String)
            End If


            If Not IsDBNull(oRow("InstallmentAmount")) Then
                lblInstallmentAmt.Text = FormatNumber(oRow("InstallmentAmount"), 2)
            End If
            If Not IsDBNull(oRow("EndPastDueAmt")) Then
                lblOverDueAmt.Text = FormatNumber(oRow("EndPastDueAmt"), 2)
            End If
            If Not IsDBNull(oRow("OSLCInstallment")) Then
                lblOSInstallmentAmt.Text = FormatNumber(oRow("OSLCInstallment"), 2)
            End If
            If Not IsDBNull(oRow("OSBillingCharges")) Then
                lblOSBillingCharge.Text = FormatNumber(oRow("OSBillingCharges"), 2)
            End If
            If Not IsDBNull(oRow("OSBalance")) Then
                lblOSBalance.Text = FormatNumber(oRow("OSBalance"), 2)
            End If
            If Not IsDBNull(oRow("OSLCInsurance")) Then
                lblOSInsurance.Text = FormatNumber(oRow("OSLCInsurance"), 2)
            End If
            If Not IsDBNull(oRow("OSPDCBounceFee")) Then
                lblPDCBounceFee.Text = FormatNumber(oRow("OSPDCBounceFee"), 2)
            End If

            LblPreviousPlanActivity.Text = CType(oRow("PlanActivity"), String)

            Me.ValidDueDate = CType(oRow("ValidDueDate"), String)
            Me.ApplicationID = CType(oRow("ApplicationID"), String)
            Me.NextDueDate = Format(CDate(oRow("NextInstallmentDueDate")), "dd/MM/yyyy")

            Me.MaxPromiseDateBank = Format(CDate(oRow("MaxPromiseDateBank")), "dd/MM/yyyy")
            Me.MaxPromiseDateCompany = Format(CDate(oRow("MaxPromiseDateCompany")), "dd/MM/yyyy")

            ucCollActResult.ltlWayOfPayment.Text = CStr(IIf(IsDBNull(oRow("WayOfPayment")), "", oRow("WayOfPayment")))
            ucCollActResult.lblCollector.Text = CStr(IIf(IsDBNull(oRow("Collector")), "", oRow("Collector")))
            ucCollActResult.DoBindInstallmentSchedule(Me.ApplicationID, Me.BusinessDate)
        Else
            cboPlanActivity.ClearSelection()
            ucCollActResult.ClearVariable()

            txtPlanDate.Text = ""
            txtPromiseDate.Text = ""
            txtHour.Text = CStr(Hour(Now()))
            txtMinute.Text = CStr(Minute(Now))
            chkRequestSpv.Checked = False
            txtNotes.Text = ""
            cboSuccess.ClearSelection()

            hdnAgreement.Value = ""
            hdnCustName.Value = ""
            hdnCustID.Value = ""

            lblInstallmentAmt.Text = ""
            lblOverDueAmt.Text = ""
            lblOSInstallmentAmt.Text = ""
            lblOSBillingCharge.Text = ""
            lblOSBalance.Text = ""
            lblOSInsurance.Text = ""
            lblPDCBounceFee.Text = ""
            Me.ValidDueDate = ""
            Me.ApplicationID = ""
            Me.NextDueDate = ""
            Me.MaxPromiseDateBank = ""
            Me.MaxPromiseDateCompany = ""
        End If
        With oCustomClass
            .strConnection = GetConnectionString()
            .strKey = "Result"
        End With
        oCustomClass = oController.CLActivityActionResult(oCustomClass)
        oResult = oCustomClass.ListCLActivity

        With oCustomClass
            .strConnection = GetConnectionString()
            .strKey = ""
        End With
        oCustomClass = oController.CLActivityActionResult(oCustomClass)
        dtAction = oCustomClass.ListCLActivity
        With cboPlanActivity
            .DataSource = dtAction
            .DataTextField = "ActionDescription"
            .DataValueField = "ActionID"
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = "0"
        End With

    End Sub

    Function getColor(ByVal LegalPhoneYN As String) As System.Drawing.Color
        If LegalPhoneYN = "Y" Then
            Return System.Drawing.Color.Blue
        Else
            Return System.Drawing.Color.Black
        End If
    End Function

    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        Response.Redirect("CLActivity.aspx")
       
    End Sub

    Private Sub dtgCLActivity_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgCLActivity.ItemDataBound
        Dim lblAgreementNo As HyperLink
        Dim lblApplicationid As Label
        Dim lblCustomerID As Label
        Dim lblCustomerName As HyperLink

        If e.Item.ItemIndex >= 0 Then

            lblAgreementNo = CType(e.Item.FindControl("hypAgreementNodtg"), HyperLink)
            lblApplicationid = CType(e.Item.FindControl("hypApplicationID"), Label)
            lblCustomerID = CType(e.Item.FindControl("lblCustomerID"), Label)
            lblCustomerName = CType(e.Item.FindControl("hypCustomerNamedtg"), HyperLink)
            lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "Collection" & "', '" & lblApplicationid.Text.Trim & "')"
            lblCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "Collection" & "', '" & lblCustomerID.Text & "')"
        End If
    End Sub

    Private Sub ImbCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        Response.Redirect("CLActivity.aspx")
    End Sub

    Private Sub BtnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
        Dim strResult As String = oAction.ResultID.Trim
        Me.AgreementNo = ucCollActResult.hypAgreementNo.Text.Trim


        If Left(oAction.ResultID.Trim, 4) = "PTPY" Then           
            If txtPlanDate.Text <> "" And cboPlanActivity.SelectedItem.Value.Trim <> "" Then                
                ShowMessage(lblMessage, "Tidak boleh isi Rencana Aktivitas dan Tanggal Rencana Jika pilih Janji Bayar", True)                
                Exit Sub
            End If
            If txtPromiseDate.Text = "" Then                    
                ShowMessage(lblMessage, "Harap isi Tanggal Janji Bayar", True)              
                Exit Sub
            End If
            If txtPromiseDate.Text <> "" And ConvertDate2(txtPromiseDate.Text) < Me.BusinessDate Then
                ShowMessage(lblMessage, "Tanggal Janji Bayar harus >=  " & Me.BusinessDate.ToString("dd/MM/yyyy"), True)
                Exit Sub
            End If
            'ga boleh lebih dari tanggal offering
            If oAction.ResultID.Trim = "PTPYTOBANK" And txtPromiseDate.Text <> "" And
                ((ConvertDate2(txtPromiseDate.Text) < ConvertDate2(Me.MaxPromiseDateBank) And ConvertDate2(txtPromiseDate.Text) < Me.BusinessDate) Or
                 (ConvertDate2(txtPromiseDate.Text) > ConvertDate2(Me.MaxPromiseDateBank) And ConvertDate2(txtPromiseDate.Text) > Me.BusinessDate)) Then
                ShowMessage(lblMessage, "Tanggal Janji Bayar harus >=  " & Me.BusinessDate.ToString("dd/MM/yyyy") & " dan Tanggal Janji Bayar harus <= " & Me.MaxPromiseDateBank, True)
                Exit Sub
            End If
            If oAction.ResultID.Trim = "PTPYTOFIN" And txtPromiseDate.Text <> "" And
                ((ConvertDate2(txtPromiseDate.Text) < ConvertDate2(Me.MaxPromiseDateBank) And ConvertDate2(txtPromiseDate.Text) < Me.BusinessDate) Or
                 (ConvertDate2(txtPromiseDate.Text) > ConvertDate2(Me.MaxPromiseDateBank) And ConvertDate2(txtPromiseDate.Text) > Me.BusinessDate)) Then
                ShowMessage(lblMessage, "Tanggal Janji Bayar harus >=  " & Me.BusinessDate.ToString("dd/MM/yyyy") & " dan Tanggal Janji Bayar harus <= " & Me.MaxPromiseDateCompany, True)
                Exit Sub
            End If
        Else
            If txtPromiseDate.Text <> "" Then
                ShowMessage(lblMessage, "Tidak bisa isi Tanggal Janji bayar jika Hasil yg dipilih Bukan Janji Bayar", True)
                Exit Sub
            End If
        End If




        If Left(oAction.ResultID.Trim, 4) <> "PTPY" And cboPlanActivity.SelectedItem.Value.Trim <> "0" Then
            If txtPlanDate.Text = "" Then
                ShowMessage(lblMessage, "Harap isi tanggal rencana Aktivitas", True)
                Exit Sub
            End If
        End If




        If Left(oAction.ResultID.Trim, 4) <> "PTPY" And txtPromiseDate.Text <> "" Then
            If ConvertDate2(txtPromiseDate.Text) > Me.ValidDueDate Then
                ShowMessage(lblMessage, "Tanggal Janji bayar tidak boleh > Tanggal Jatuh Tempo", True)
                Exit Sub
            End If
            If txtPromiseDate.Text <> "" And ConvertDate2(txtPromiseDate.Text) < Me.BusinessDate Then
                ShowMessage(lblMessage, "Tanggal Janji bayar harus  >=  " & Me.BusinessDate.ToString("dd/MM/yyyy"), True)
                Exit Sub
            End If
        End If



        'If txtPlanDate.Text <> "" And CLActivity.ConvertDate(txtPlanDate.Text) > CLActivity.ConvertDate(Me.NextDueDate) Then
        '    lblMessage.Text = "Plan Date can't be greater than Due date"
        '    Context.Trace.Write("Pesan = " & lblMessage.Text)
        '    Context.Trace.Write("txtPlanDate.Text  = " & txtPlanDate.Text)
        '    Context.Trace.Write("CLActivity.ConvertDate(txtPlanDate.Text)  = " & CLActivity.ConvertDate(txtPlanDate.Text))
        '    Context.Trace.Write("CLActivity.ConvertDate(Me.NextDueDate)  = " & CLActivity.ConvertDate(Me.NextDueDate))
        '    Context.Trace.Write("syarat 8")
        '    Exit Sub
        'End If

        If txtPlanDate.Text <> "" Then
            If txtHour.Text.Trim = "" Or txtMinute.Text.Trim = "" Then
                ShowMessage(lblMessage, "Harap isi Jam dan menit pada", True)
                Exit Sub
            End If
            If (txtHour.Text.Trim <> "" Or txtMinute.Text.Trim <> "") And ConvertDate2(txtPlanDate.Text) < Me.BusinessDate Then
                ShowMessage(lblMessage, "Tanggal Rencana Bayar harus >=  " & Me.BusinessDate.ToString("dd/MM/yyyy"), True)
                Exit Sub
            End If
        End If



        If oAction.ActionID.Trim <> "Visit" And cboSuccess.SelectedItem.Value = "" And (Left(strResult, 4) = "PTPY" Or strResult = "LM" Or strResult = "PAID" Or strResult = "REP" Or strResult = "REQ" Or strResult = "INS") Then
            ShowMessage(lblMessage, "Harap pilih Telepon yang berhasil dihubungi", True)
            Exit Sub
        End If

        If DateDiff(DateInterval.Day, ConvertDate2(txtInputDate.Text), Me.BusinessDate) > 1 Then
            ShowMessage(lblMessage, "Maksimum Tanggal Input H-1 saja !!!", True)
            Exit Sub
        End If

        With oCustomClass
            .strConnection = GetConnectionString()
            .AgreementNo = ucCollActResult.hypAgreementNo.Text.Trim
            .ApplicationID = Me.ApplicationID.Trim
            .Contact = cboSuccess.SelectedItem.Value.Trim
            .isRequest = chkRequestSpv.Checked
            If txtPlanDate.Text <> "" Then
                Context.Trace.Write("Kondisi 10")
                .PlanDate = ConvertDate2(txtPlanDate.Text).ToString("yyyyMMdd") & " " & txtHour.Text.Trim & ":" & txtMinute.Text.Trim
            Else
                .PlanDate = Nothing
            End If
            If txtPromiseDate.Text <> "" Then
                .PTPYDate = ConvertDate2(txtPromiseDate.Text).ToString("yyyyMMdd")
            Else
                .PTPYDate = Nothing
            End If
            .ResultID = oAction.ResultID.Trim
            .ActivityID = oAction.ActionID.Trim
            If cboPlanActivity.SelectedItem.Value = "0" Then
                .PlanActivityID = Nothing
            Else
                .PlanActivityID = cboPlanActivity.SelectedItem.Value.Trim
            End If

            .Notes = txtNotes.Text.Trim
            '.BusinessDate = Me.BusinessDate
            'Modify by Wira, bisa input backdate requirement AAF
            .BusinessDate = ConvertDate2(txtInputDate.Text)
            .CGID = Me.GroubDbID
            .CollectorID = hdnCollector.Value.Trim
        End With
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "cboChildonChange", "cboChildonChange(this.options[this.selectedIndex].value);", True)
        Try
            oCustomClass = oController.CLActivitySave(oCustomClass)
            If oCustomClass.Hasil = 0 Then
                ShowMessage(lblMessage, "Gagal", True)
                Exit Sub
            Else
                lblMessage.Text = "Data Berhasil disimpan"
                ShowMessage(lblMessage, "Data Berhasil disimpan", False)
                DoBind(Me.SearchBy, Me.SortBy, Me.SearchBy2)
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

End Class