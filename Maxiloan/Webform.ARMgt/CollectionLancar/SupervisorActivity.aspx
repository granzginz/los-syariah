﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SupervisorActivity.aspx.vb"
    Inherits="Maxiloan.Webform.ARMgt.SupervisorActivity" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="cc1" Namespace="System.Web.UI.WebControls" Assembly="System.Web" %>
<%@ Register TagPrefix="uc1" TagName="UcBranchCollection" Src="../../Webform.UserController/UcBranchCollection.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcCollector" Src="../../Webform.UserController/UcCollector.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucCollectorRpt" Src="../../Webform.UserController/ucCollectorRpt.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>SupervisorActivity</title>
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%#Request.servervariables("SERVER_NAME")%>/';			
    </script>
    <script language="javascript" type="text/javascript">
        function fback() {
            history.go(-1);
            return false;

        }

//        function PlanRALChange() {
//            var myID = 'UcRALDate_txtDate';
//            var objDate = eval('document.forms[0].' + myID);
//            var result

//            if (document.forms[0].rbRAL_0.checked = true) {
//                result = 'Plan';
//            }
//            if (document.forms[0].rbRAL_1.checked = true) {
//                result = 'Undo Plan';
//            }

//            alert(result);
//            if (result == 'Undo Plan') {
//                objDate.disabled = true;
//                document.forms[0].all.UcRALDate_txtDate_imgCalender.disabled = true;
//                document.forms[0].all.UcRALDate_txtDate_imgCalender.disabled = true;
//                document.forms[0].UcRALDate_txtDate.value = '';
//            }
//        }

        function rbFixedChange() {
            if (document.forms[0].rbFixed_1.checked) {
                document.forms[0].UcCollectorFixed_cboCollector.disabled = true;
            }
            else {
                document.forms[0].UcCollectorFixed_cboCollector.disabled = false;
            }
        }	
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>    
        <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">           
                <h3>
                    KEGIATAN SUPERVISOR
                </h3>
            </div>
        </div>  
        <asp:Panel ID="PnlSearch" runat="server" Width="100%">
        <div class="form_box">
	        <div class="form_single">
                <label>Collection Group</label>
                <uc1:UcBranchCollection id="UcCGID" runat="server"></uc1:UcBranchCollection>	        
            </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Nama Customer</label>
                <asp:TextBox ID="txtCustomerName" runat="server" ></asp:TextBox>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>No Kontrak</label>
                <asp:TextBox ID="txtAgreementNo" runat="server" ></asp:TextBox>
	        </div>
        </div>   
        <div class="form_box">
	        <div class="form_single">
                <label>Kasus</label>
                <asp:DropDownList ID="cboCasesSearch" runat="server" DataTextField="Description"
                    DataValueField="CasesId">
                </asp:DropDownList>
	        </div>
        </div>  
        <div class="form_box">
	        <div class="form_single">
                <label>Collector</label>
                <uc1:UcCollector id="UcCollectorSearch" runat="server"></uc1:UcCollector>	        
            </div>  
        </div>  
        <div class="form_box">
	        <div class="form_single">
                <label>Jumlah Hari telat &gt;=</label>
                <asp:TextBox ID="txtOverdueDays" runat="server" Width="50px"  MaxLength="3"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" CssClass="validator_general"
                    ControlToValidate="txtOverdueDays" ErrorMessage="Harap diisi dengan Angka"
                    Display="Dynamic" ValidationExpression="[0-9]{0,12}"></asp:RegularExpressionValidator>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Kategori Customer</label>
                <%--<asp:CheckBox ID="chkCollectorReq" runat="server"></asp:CheckBox>--%>
                <asp:DropDownList runat="server" ID="cboCollectorReq">
                    <asp:ListItem Value="" Selected="True">Selected ALL</asp:ListItem>
                    <asp:ListItem Value="1">Permintaan Khusus Collector</asp:ListItem>
                </asp:DropDownList>
	        </div>
        </div>               
        <div class="form_button">
            <asp:Button ID="Buttonsearch" runat="server"  Text="Search" CssClass ="small button blue"></asp:Button>&nbsp;
            <asp:Button ID="ButtonReset" runat="server"  Text="Reset" CssClass ="small button gray" CausesValidation="False"></asp:Button>
	    </div>                
        </asp:Panel>
        <asp:Panel ID="PnlList" runat="server" Width="100%">
        <div class="form_box_title">
            <div class="form_single">           
                <h4>
                    DAFTAR KONTRAK</h4>
            </div>
        </div>                 
        <asp:Panel ID="PnlPaging" runat="server" Visible="True">
            <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="Dtg" runat="server" Width="100%"
                        AllowSorting="True" AutoGenerateColumns="False" BorderStyle="None" BorderWidth="0" OnSortCommand="Sorting" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="NO">                                
                                <HeaderTemplate>
                                    <asp:CheckBox ID="chkAll" OnCheckedChanged="checkAll" runat="server" AutoPostBack="True">
                                    </asp:CheckBox>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkSelect" runat="server"></asp:CheckBox>
                                    <asp:Label ID="lblCounter" runat="server" Visible="false"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">                                
                                <ItemTemplate>
                                    <asp:Image ID="imgBulat" runat="server" Visible='<%# iif(container.dataitem("IsRequestAssign")="Y",true,false)%>'
                                        ImageUrl="../../Images/BulatMerah.gif" ToolTip='<%# container.dataitem("Notes")%>'>
                                    </asp:Image>
                                    <asp:HyperLink ID="lnkAgreement" runat="server" Text='<%# container.dataitem("AgreementNo")%>'
                                        Enabled="True">
                                    </asp:HyperLink>
                                    <asp:Label ID="lblApplicationID" Visible="False" runat="server" Text='<%# container.dataitem("ApplicationID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="CustomerName" HeaderText="NAMA CUSTOMER">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="lblCustomerName" runat="server" Text='<%# container.dataitem("CustomerName")%>'
                                        Enabled="True">
                                    </asp:HyperLink>
                                    <asp:Label ID="lblCustomerID" runat="server" Visible="False" Text='<%# container.dataitem("CustomerID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="CustomerAddress" SortExpression="CustomerAddress" HeaderText="ALAMAT">                                
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="CollectorName" SortExpression="CollectorName" HeaderText="COLLECTOR">                                
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="OverDueDays" SortExpression="OverDueDays" HeaderText="HARI TELAT">                                
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="Amount_Overdue_Gross" SortExpression="Amount_Overdue_Gross"
                                HeaderText="JUMLAH TUNGGAKAN" DataFormatString="{0:N2}">        
                            <ItemStyle CssClass="item_grid_right"></ItemStyle>                        
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="OS_Gross" SortExpression="OS_Gross" HeaderText="SISA A/R"
                                DataFormatString="{0:N2}">
                            <ItemStyle CssClass="item_grid_right"></ItemStyle>                                
                            </asp:BoundColumn>
                            <asp:TemplateColumn Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblCollectorId" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="RALDate" SortExpression="RALDate" HeaderText="TGL SKE">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="FIXEDASSIGN" SortExpression="FIXEDASSIGN" HeaderText="FIXED ASSIGN">
                            </asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="AgreementNo" SortExpression="AgreementNo">
                            </asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="CustomerID" SortExpression="CustomerID">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="PLANDATE" HeaderText="TGL RENCANA"></asp:BoundColumn>
                        </Columns>            
                    </asp:DataGrid>        
                    <div class="button_gridnavigation">
	                    <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
		                    CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
	                    </asp:ImageButton>  
	                    <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
		                    CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
	                    </asp:ImageButton>
	                    <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
		                    CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
	                    </asp:ImageButton>
	                    <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
		                    CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
	                    </asp:ImageButton>
	                    <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
	                    <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
		                    EnableViewState="False"></asp:Button>
	                    <asp:RangeValidator   CssClass="validator_general"   ID="rgvGo" runat="server" ControlToValidate="txtPage"
                        MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer"></asp:RangeValidator>
                        <asp:RequiredFieldValidator    CssClass="validator_general"   ID="rfvGo" runat="server" ControlToValidate="txtPage"
                            ErrorMessage="No Halaman Salah" Display="Dynamic"></asp:RequiredFieldValidator>   
                    </div>
                    <div class="label_gridnavigation">
	                    <asp:Label ID="lblPage" runat="server"></asp:Label>of
	                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
	                    <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                    </div>        
                </div>
            </div>
            </div>
            <div class="form_box">
	            <div class="form_single">
                    <asp:RadioButtonList ID="rbTask" runat="server"  class="opt_single"  RepeatDirection="Horizontal">
                        <asp:ListItem Value="Fixed">Fixed / Unfixed</asp:ListItem>
                        <asp:ListItem Value="Cases">Tambah Kasus</asp:ListItem>
                        <%--<asp:ListItem Value="RAL">Plan / Undo Plan SKT</asp:ListItem>--%>
                        <asp:ListItem Value="RAL">Plan / Undo Plan SKE</asp:ListItem>
                        <%--<asp:ListItem Value="RedDot">Release Titik merah</asp:ListItem>--%>
                        <asp:ListItem Value="Plan">Plan</asp:ListItem>
                    </asp:RadioButtonList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="rbTask" CssClass="validator_general"
                        ErrorMessage="Harap Pilih Jenis Tindakan" Display="Dynamic"></asp:RequiredFieldValidator>
	            </div>
            </div>     
            <div class="form_button">
                <asp:Button ID="ButtonDoAction" runat="server"  Text="Action" CssClass ="small button blue"
                EnableViewState="False"></asp:Button>
	        </div>                         
        </asp:Panel>
        </asp:Panel>   
        <asp:Panel ID="PnlListChoosen" runat="server" Width="100%" >
        <div class="form_box_title">
            <div class="form_single">              
                <h4>
                    <asp:Label ID="lblMenuTask" runat="server"></asp:Label>
                </h4>
            </div>
        </div>
        <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgchoosen" runat="server" Width="100%"
                    AllowSorting="True" AutoGenerateColumns="False" BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <Columns>
                        <asp:BoundColumn DataField="id" HeaderText="NO"></asp:BoundColumn>
                        <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">
                            <ItemTemplate>
                                <asp:Image ID="Image1" runat="server" ImageUrl="../../Images/BulatMerah.gif" Visible='<%# iif(container.dataitem("IsRequestAssign")="Y",true,false)%>'>
                                </asp:Image>
                                <asp:HyperLink ID="lnkFAgreementNo" runat="server" Text='<%# container.dataitem("AgreementNo")%>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="CustomerName" SortExpression="CustomerName" HeaderText="NAMA CUSTOMER">
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="CollectorName" SortExpression="CollectorName" HeaderText="COLLECTOR">
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="DaysOverdue" SortExpression="DaysOverdue" HeaderText="HARI TELAT">
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="CasesID" SortExpression="CasesID" HeaderText="KASUS">
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="FixedAssign" SortExpression="FixedAssign" HeaderText="FIXED ASSIGN">
                        </asp:BoundColumn>
                        <asp:TemplateColumn Visible="False" HeaderText="AgreementNo">
                            <ItemTemplate>
                                <asp:Label ID="lblFCollectorId" runat="server" Text='<%# container.dataitem("CollectorId")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="RALDate" SortExpression="RALDate" HeaderText="PLAN SKE">
                        </asp:BoundColumn>
                    </Columns>            
                </asp:DataGrid>
            </div> 
        </div>       
        </div>   
        </asp:Panel>
        <asp:Panel ID="pnlFixed" Width="100%"
        Visible="False" runat="server">
        <div class="form_box_title">
            <div class="form_single">              
                <h4>
                    <strong>FIXED / UNFIXED</strong>
                </h4>
            </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label class="label_general">Pilihan</label>
                <asp:RadioButtonList ID="rbFixed" onclick="<%#rbFixedChange()%>" class="opt_single"  runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Value="Fixed">Fixed</asp:ListItem>
                    <asp:ListItem Value="Unfixed">Unfixed</asp:ListItem>
                </asp:RadioButtonList>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Alokasi Kepada</label>
                <uc1:UcCollector id="UcCollectorFixed" runat="server"></uc1:UcCollector>	        
            </div>  
        </div>  
        <div class="form_button">
            <asp:Button ID="ButtonFSave" runat="server"  Text="Save" CssClass ="small button blue"
                CausesValidation="False"></asp:Button>&nbsp;
            <asp:Button ID="ButtonFCancel" runat="server"  Text="Cancel" CssClass ="small button gray"
                CausesValidation="False"></asp:Button>
	    </div>                          
        </asp:Panel>
        <asp:Panel ID="pnlCases" Width="100%"
        Visible="False" runat="server">
        <div class="form_box_title">
            <div class="form_single">              
                <h4>
                    <strong>TAMBAH KASUS</strong>
                </h4>   
            </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Kasus</label>
                <asp:DropDownList ID="cboCases" runat="server" DataTextField="Description" DataValueField="CasesId">
                </asp:DropDownList>
	        </div>
        </div>         
        <div class="form_button">
            <asp:Button ID="ButtonCSave" runat="server"  Text="Save" CssClass ="small button blue"
                CausesValidation="False" Visible="True"></asp:Button>&nbsp;
            <asp:Button ID="btnCancel" runat="server"  Text="Cancel" CssClass ="small button gray"
                CausesValidation="False"></asp:Button>
	    </div>                   
        </asp:Panel>
        <asp:Panel ID="pnlRAL" Width="100%"
        Visible="False" runat="server">
        <div class="form_box_title">
            <div class="form_single">              
                <h4>
                    <%--<strong><strong><b>PLAN / UNDO PLAN SKT</b></strong></strong>--%>
                    <strong><strong><b>PLAN / UNDO PLAN SKE</b></strong></strong>
                </h4>
            </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Pilihan</label>
                <asp:Label ID="lblPlanRAL" runat="server"></asp:Label>
	        </div>
        </div> 
        <div class="form_box">
	        <div class="form_single">
                <%--<label class ="label_req">Tanggal SKT</label>                --%>
                <label class ="label_req">Tanggal SKE</label>
                <asp:TextBox ID="txtRALDate" runat="server"></asp:TextBox>                   
                <asp:CalendarExtender ID="txtRALDate_CalendarExtender" runat="server" 
                    Enabled="True" TargetControlID="txtRALDate" Format ="dd/MM/yyyy"></asp:CalendarExtender>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*" CssClass="validator_general"
                ControlToValidate="txtRALDate"></asp:RequiredFieldValidator>
	        </div>
        </div> 
        <div class="form_button">
            <asp:Button ID="ButtonRSave" runat="server"  Text="Save" CssClass ="small button blue"
                CausesValidation="False" Visible="True"></asp:Button>&nbsp;
            <asp:Button ID="ButtonRCancel" runat="server"  Text="Cancel" CssClass ="small button gray" />            
	    </div>                 
        </asp:Panel>
        <asp:Panel ID="PnlPlan"  Width="100%"
        Visible="False" runat="server">
        <div class="form_box_title">
            <div class="form_single">              
                <h4>
                    <strong><strong><b>RENCANA KEGIATAN</b></strong></strong>
                </h4>
            </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Rencana Kegiatan</label>
                <asp:DropDownList ID="ddlPlan" runat="server" DataTextField="Description" DataValueField="CasesId">
                </asp:DropDownList>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
               <label class ="label_req">Tanggal Rencana</label>                
                <asp:TextBox ID="txtPlanDate" runat="server"></asp:TextBox>                   
                <asp:CalendarExtender ID="txtPlanDate_CalendarExtender" runat="server" 
                    Enabled="True" TargetControlID="txtPlanDate" Format ="dd/MM/yyyy"></asp:CalendarExtender>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*" CssClass="validator_general"
                ControlToValidate="txtPlanDate"></asp:RequiredFieldValidator>
	        </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonlanSave" runat="server"  Text="Save" CssClass ="small button blue"
                CausesValidation="False" Visible="True"></asp:Button>&nbsp;
            <asp:Button ID="ButtonlanCancel" runat="server"  Text="Cancel" CssClass ="small button gray" />            
	    </div>    
    </asp:Panel>       
    </form>
</body>
</html>
