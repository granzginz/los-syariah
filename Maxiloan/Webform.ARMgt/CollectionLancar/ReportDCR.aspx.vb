﻿
#Region "Imports"
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class ReportDCR
    Inherits Maxiloan.Webform.WebBased

    Protected WithEvents UcCollector As ucCollectorRpt

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        Me.FormID = "RPTCOLLDCR"
        If Not IsPostBack Then
            If Request("rptFile") <> "" Then
                Dim strFileLocation As String = "../../XML/" & Request.QueryString("rptFile")
                Response.Write("<script language = javascript>" & vbCrLf _
                                & "var x = screen.width; " & vbCrLf _
                                & "var y = screen.height; " & vbCrLf _
               & "window.open('" & strFileLocation & "','accacq', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes, resizable=1') " & vbCrLf _
               & "</script>")
            End If

            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                'FilldataCollector()
                txtDateFrom.Text = Format(Me.BusinessDate, "dd/MM/yyyy")
                UcCollector.CollectorType = "CL"
            End If

        End If
    End Sub


    Protected Sub btnprint_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnprint.Click
        If CheckFeature(Me.Loginid, Me.FormID, "View", Me.AppId) Then
            Dim cookie As HttpCookie = Request.Cookies(Me.FormID)
            Dim cmdwhere As String

            cmdwhere = "DCR.CGID='" & UcCollector.CGIDParent.Trim & "'"

            If UcCollector.CollectorID.Trim <> "" And UcCollector.CollectorID.Trim <> "0" Then
                cmdwhere = cmdwhere & " and DCR.CollectorID='" & UcCollector.CollectorID.Trim & "'"
            End If
            If txtDateFrom.Text <> "" Then
                cmdwhere = cmdwhere & " and convert(char(10),DCR.PlanDate,103) = '" & txtDateFrom.Text & "'"
            End If
            If Not cookie Is Nothing Then
                cookie.Values("CGID") = UcCollector.CGIDParent.Trim
                cookie.Values("LoginID") = Me.Loginid
                If UcCollector.CollectorID.Trim <> "" Then
                    cookie.Values("CollectorID") = UcCollector.CollectorID.Trim
                Else
                    cookie.Values("CollectorID") = "All"
                End If
                cookie.Values("DateFrom") = txtDateFrom.Text
                cookie.Values("DateTo") = txtDateFrom.Text
                cookie.Values("cmdwhere") = cmdwhere
                Response.AppendCookie(cookie)
            Else
                Dim cookieNew As New HttpCookie("RptCollDCR")
                cookieNew.Values.Add("CGID", UcCollector.CGIDParent.Trim)
                cookieNew.Values.Add("LoginID", Me.Loginid)
                If UcCollector.CollectorID.Trim <> "" Then
                    cookieNew.Values.Add("CollectorID", UcCollector.CollectorID.Trim)
                Else
                    cookieNew.Values.Add("CollectorID", "All")
                End If
                cookieNew.Values.Add("DateFrom", txtDateFrom.Text)
                cookieNew.Values.Add("DateTo", txtDateFrom.Text)
                cookieNew.Values.Add("cmdwhere", cmdwhere)
                Response.AppendCookie(cookieNew)
            End If
            Response.Redirect("ReportDCRViewer.aspx")
        End If
    End Sub
End Class
