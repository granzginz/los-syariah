﻿#Region "Imports"
Imports System.Text
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonVariableHelper
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class DocDistributionAct
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oCollector As ucCollectorRpt    

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.DocDistribution
    Private oController As New DocDistributionController
    Private CounterColumn As Integer = 0

    Private currentPageChange As Int32 = 1
    Private pageSizeChange As Int16 = 10
    Private currentPageNumberChange As Int16 = 1
    Private totalPagesChange As Double = 1
    Private recordCountChange As Int64 = 1


#End Region

#Region "Property"
    Private Property ApplicationID() As String
        Get
            Return CStr(viewstate("ApplicationID"))
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property
    Private Property oData() As DataTable
        Get
            Return CType(viewstate("oData"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            viewstate("oData") = Value
        End Set
    End Property
    Private Property Flag() As String
        Get
            Return CStr(viewstate("Flag"))
        End Get
        Set(ByVal Value As String)
            viewstate("Flag") = Value
        End Set
    End Property
    Private Property Flag1() As String
        Get
            Return CStr(viewstate("Flag1"))
        End Get
        Set(ByVal Value As String)
            viewstate("Flag1") = Value
        End Set
    End Property
    Private Property Flag2() As String
        Get
            Return CStr(viewstate("Flag2"))
        End Get
        Set(ByVal Value As String)
            viewstate("Flag2") = Value
        End Set
    End Property

#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.FormID = "CollDocDistribution"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                InitialDefaultPanel()

                oCollector.CollectorType = "CL"

            End If
        End If
    End Sub
    Private Sub InitialDefaultPanel()
        pnlDtGrid.Visible = False
        pnlsearch.Visible = True        
        pnlDistributionActivity.Visible = False        
    End Sub
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#Region "Navigation"
    Private Sub PagingFooter()        
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then            
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        FillViewStateHalaman()


        DoBind(Me.SearchBy, Me.SortBy)
        CheckIfEverChecked()
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click        
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                FillViewStateHalaman()
                DoBind(Me.SearchBy, Me.SortBy)
                CheckIfEverChecked()

            End If
        End If
    End Sub
#End Region
    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        Dim intloop As Integer
        Dim hypID As HyperLink

        With oCustomClass
            .strConnection = GetConnectionString
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oCustomClass = oController.DocumentDistributionPaging(oCustomClass)

        DtUserList = oCustomClass.ListDistribution
        DvUserList = DtUserList.DefaultView
        recordCount = oCustomClass.TotalRecord
        DvUserList.Sort = Me.SortBy
        dtgdocumentdistribution.DataSource = DvUserList

        Try
            dtgdocumentdistribution.DataBind()
        Catch
            dtgdocumentdistribution.CurrentPageIndex = 0
            dtgdocumentdistribution.DataBind()
        End Try

        PagingFooter()
        pnlDtGrid.Visible = True
        pnlsearch.Visible = True        
    End Sub
    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Buttonsearch.Click
        pnlDistributionActivity.Visible = False
        Me.SearchBy = "CollectionAgreement.CGID='" & oCollector.CGIDParent.Trim & "'"
        If cboSearchBy.SelectedItem.Value.Trim <> "Select One" Then
            Me.SearchBy = Me.SearchBy & " and " & cboSearchBy.SelectedItem.Value.Trim & _
                " like '%" + txtSearchBy.Text.Trim + "%'"
        End If
        If cboReceiveStatus.SelectedItem.Value.Trim <> "All" Then
            Me.SearchBy = Me.SearchBy & " and " & cboReceiveStatus.SelectedItem.Value.Trim & "'"
        End If
        If oCollector.CollectorID.Trim <> "" And oCollector.CollectorID.Trim <> "0" Then
            Me.SearchBy = Me.SearchBy & " and documentdistribution.collectorid='" & oCollector.CollectorID.Trim & "'"

        End If

        Me.SortBy = ""
        DoBind(Me.SearchBy, Me.SortBy)


        With cboReceiveStatus
            .DataValueField = "ID"
            .DataTextField = "Name"

        End With
        oCollector.CollectorID = ""

    End Sub
    Private Sub dtgdocumentdistribution_ItemCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgdocumentdistribution.ItemCommand
        Select Case e.CommandName
            Case "result"                
                txtAgreementBy.Text = ""
                txtSlipSetoranBy.Text = ""
                txtInsuranceBy.Text = ""
                txtSlipDate.Text = ""
                txtInsuranceDate.Text = ""
                txtAgreementDate.Text = ""

                txtWelcomeLetter.Text = ""
                txtWelcomeLetterreceiveBy.Text = ""
                txtKartuAngsuran.Text = ""
                txtKartuAngsuranReceiveBy.Text = ""
                txtKurir.Text = ""
                Dim lblbranchid As Label
                lblbranchid = CType(dtgdocumentdistribution.Items(e.Item.ItemIndex).FindControl("lblBranchId"), Label)
                Dim lblApplicationId As Label
                Dim collector As String
                Dim oDataTable As New DataTable
                Dim hypAgreementNo As HyperLink
                Dim lblcustomerName As Label
                lblApplicationId = CType(dtgdocumentdistribution.Items(e.Item.ItemIndex).FindControl("lblApplicationId"), Label)
                If CheckFeature(Me.Loginid, Me.FormID, "Rsult", Me.AppId) Then


                    hypAgreementNo = CType(dtgdocumentdistribution.Items(e.Item.ItemIndex).FindControl("hypAgreementNodtg"), HyperLink)
                    hypAgreementNoSlct.Text = hypAgreementNo.Text
                    lblcustomerName = CType(dtgdocumentdistribution.Items(e.Item.ItemIndex).FindControl("hypCustomerNamedtg"), Label)
                    hypCustomerNameSlct.Text = lblcustomerName.Text
                    lblwayofpayment.Text = e.Item.Cells(4).Text
                    lblcollector.Text = e.Item.Cells(9).Text

                    With oCustomClass
                        .strConnection = GetConnectionString()
                        .ApplicationID = lblApplicationId.Text.Trim
                        .BranchId = lblbranchid.Text.Trim
                        .BusinessDate = Me.BusinessDate
                    End With
                    oCustomClass = oController.RequestDataDocDistribution(oCustomClass)
                    oDataTable = oCustomClass.ListView
                    Me.BranchID = lblbranchid.Text.Trim
                    Me.ApplicationID = lblApplicationId.Text


                    If Not IsDBNull(oDataTable.Rows(0)("GoLiveDate")) Then
                        lblgolivedate.Text = Format(oDataTable.Rows(0)("GoLiveDate"), "dd/MM/yyyy")
                    End If

                    If Not IsDBNull(oDataTable.Rows(0)("InsuredBy")) Then
                        lblinsuredby.Text = CType(oDataTable.Rows(0)("InsuredBy"), String)
                    End If

                    If Not IsDBNull(oDataTable.Rows(0)("Insurancepaidby")) Then
                        lblpaidby.Text = CType(oDataTable.Rows(0)("Insurancepaidby"), String)
                    End If
                    If Not IsDBNull(oDataTable.Rows(0)("PolicyReceiveDate")) Then
                        lblpolicyreceive.Text = Format(oDataTable.Rows(0)("PolicyReceiveDate"), "dd/MM/yyyy")
                    End If
                    If Not IsDBNull(oDataTable.Rows(0)("Agreementsentdate")) Then
                        txtAgreementDate.Text = Format(oDataTable.Rows(0)("Agreementsentdate"), "dd/MM/yyyy")
                        Me.Flag = "YES"
                    End If
                    If Not IsDBNull(oDataTable.Rows(0)("SlipSentDate")) Then
                        txtSlipDate.Text = Format(oDataTable.Rows(0)("SlipSentDate"), "dd/MM/yyyy")
                        Me.Flag1 = "YES"
                    End If
                    If Not IsDBNull(oDataTable.Rows(0)("PolicySentDate")) Then
                        txtInsuranceDate.Text = Format(oDataTable.Rows(0)("PolicySentDate"), "dd/MM/yyyy")
                        Me.Flag2 = "YES"
                    End If
                    If Not IsDBNull(oDataTable.Rows(0)("AgreementReceiveBy")) Then
                        txtAgreementBy.Text = CType(oDataTable.Rows(0)("AgreementReceiveBy"), String).Trim
                    End If

                    If Not IsDBNull(oDataTable.Rows(0)("SlipReceiveBy")) Then
                        txtSlipSetoranBy.Text = CType(oDataTable.Rows(0)("SlipReceiveBy"), String).Trim
                    End If

                    If Not IsDBNull(oDataTable.Rows(0)("PolicyReceiveBy")) Then
                        txtInsuranceBy.Text = CType(oDataTable.Rows(0)("PolicyReceiveBy"), String).Trim
                    End If

                    If Not IsDBNull(oDataTable.Rows(0)("WelcomeLetter")) Then
                        txtWelcomeLetter.Text = CType(oDataTable.Rows(0)("WelcomeLetter"), String)
                    End If

                    If Not IsDBNull(oDataTable.Rows(0)("WelcomeLetterReceiveBy")) Then
                        txtWelcomeLetterreceiveBy.Text = CType(oDataTable.Rows(0)("WelcomeLetterReceiveBy"), String)
                    End If

                    If Not IsDBNull(oDataTable.Rows(0)("CardInstallment")) Then
                        txtKartuAngsuran.Text = CType(oDataTable.Rows(0)("CardInstallment"), String)
                    End If

                    If Not IsDBNull(oDataTable.Rows(0)("CardInstallmentReceiveBy")) Then
                        txtKartuAngsuranReceiveBy.Text = CType(oDataTable.Rows(0)("CardInstallmentReceiveBy"), String)
                    End If

                    If Not IsDBNull(oDataTable.Rows(0)("Courier")) Then
                        txtKurir.Text = CType(oDataTable.Rows(0)("Courier"), String)
                    End If


                    If lblwayofpayment.Text <> "SB" Then
                        txtSlipDate.Enabled = False

                        txtSlipSetoranBy.Enabled = False
                    Else
                    End If

                    txtSlipDate.Enabled = True
                    txtSlipSetoranBy.Enabled = True

                    'If lblinsuredby.Text = "CU" Or (lblinsuredby.Text = "CO" And lblpaidby.Text = "AC") Then
                    '    txtInsuranceDate.Enabled = False
                    '    txtInsuranceBy.Enabled = False
                    'Else
                    '    txtInsuranceDate.Enabled = True
                    '    txtInsuranceBy.Enabled = True
                    'End If

                    pnlsearch.Visible = False
                    pnlDtGrid.Visible = False

                    pnlDistributionActivity.Visible = True

                End If
        End Select
    End Sub
    Private Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        pnlsearch.Visible = True
        pnlDtGrid.Visible = True

        pnlDistributionActivity.Visible = False

        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
    Public Function isVisiblePrint(ByVal strKey As String) As Boolean
        If strKey.Trim = "" Then
            Return False
        Else
            Return True
        End If
    End Function
#Region "SendCookies"



    Sub SendCookies()
        FillViewStateHalaman()
        Dim AggrementChecked As String = JoinAllViewstateHalaman().Trim

        Context.Trace.Write("Send Cookies !")

        Dim cookie As HttpCookie = Request.Cookies(General.CommonCookiesHelper.COOKIES_INVENTORY_APPRAISAL)
        Dim strWhere As New StringBuilder
        strWhere.Append(Me.SearchBy)
        If Not cookie Is Nothing Then
            cookie.Values("SearchBy") = strWhere.ToString
            cookie.Values("WhereAggNo") = AggrementChecked

            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie(General.CommonCookiesHelper.COOKIES_INVENTORY_APPRAISAL)
            cookieNew.Values("SearchBy") = strWhere.ToString
            cookie.Values("WhereAggNo") = AggrementChecked
            Response.AppendCookie(cookieNew)
        End If
        Context.Trace.Write("strWhere = " & strWhere.ToString)

    End Sub

#End Region
    Private Sub imbReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        InitialDefaultPanel()
        cboSearchBy.ClearSelection()
        txtSearchBy.Text = ""

    End Sub
    Private Sub dtgdocumentdistribution_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgdocumentdistribution.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim hyTemp As HyperLink
            Dim lblApplicationId As Label


            lblApplicationId = CType(e.Item.FindControl("lblApplicationId"), Label)
            '*** Agreement No link
            hyTemp = CType(e.Item.FindControl("hypAgreementNodtg"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenAgreementNo('" & "Collection" & "', '" & Server.UrlEncode(lblApplicationId.Text.Trim) & "')"

        End If

    End Sub
    Public Sub ChkStatus_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim loopitem As Int16
        Dim ChkStatusAgreementNo As CheckBox
        For loopitem = 0 To CType(dtgdocumentdistribution.Items.Count - 1, Int16)
            Context.Trace.Write("loopitem = " & loopitem)
            ChkStatusAgreementNo = New CheckBox
            ChkStatusAgreementNo = CType(dtgdocumentdistribution.Items(loopitem).FindControl("ChkPrint"), CheckBox)

            If ChkStatusAgreementNo.Enabled Then
                If CType(sender, CheckBox).Checked = True Then
                    Context.Trace.Write("CType(sender, CheckBox).Checked  = True ")
                    ChkStatusAgreementNo.Checked = True
                Else
                    Context.Trace.Write("CType(sender, CheckBox).Checked  = False ")
                    ChkStatusAgreementNo.Checked = False
                End If
            Else
                Context.Trace.Write("ChkStatusAgreementNo.Checked = False")
                ChkStatusAgreementNo.Checked = False
            End If
        Next

    End Sub

#Region "FillViewStateHalaman"

    Private Sub FillViewStateHalaman()
        'Isi ViewState per halaman grid
        Context.Trace.Write("FillViewStateHalaman")
        Dim irecord As Int16
        Dim cb As CheckBox
        Dim hyAgreementGrid As Label
        Dim strAgreement As String
        Dim koma As String
        Dim recordDataGrid As Int16


        recordDataGrid = CType(dtgdocumentdistribution.Items.Count, Int16)
        ' recordDataGrid = 10
        strAgreement = ""
        viewstate("hal" & CStr(CInt(lblPage.Text)).Trim) = ""
        Context.Trace.Write("recordDataGrid :" + CType(recordDataGrid, String))
        Try
            For irecord = 0 To CType(recordDataGrid - 1, Int16)
                Context.Trace.Write("Var Irecord :" + CType(irecord, String))
                Context.Trace.Write("Var PageSize :" + CType(pageSize, String))
                cb = CType(dtgdocumentdistribution.Items(irecord).FindControl("chkPrint"), CheckBox)
                ' hyAgreementGrid = CType(dtgPaging.Items(irecord).FindControl("hyAgreementGrid"), Label)
                hyAgreementGrid = CType(dtgdocumentdistribution.Items(irecord).Cells(0).FindControl("lblApplicationId"), Label)

                If cb.Checked = True Then
                    If strAgreement = "" Then koma = "" Else koma = ","
                    strAgreement = strAgreement & koma & hyAgreementGrid.Text.Trim
                    viewstate("hal" & CStr(CInt(lblPage.Text)).Trim) = strAgreement.Trim
                End If
            Next

            Context.Trace.Write("strAgreement = " & strAgreement)

        Catch ex As Exception
            Response.Write(ex.Message)
            Dim oErr As New MaxiloanExceptions
            oErr.WriteLog("InventoryAppraisal.aspx", "Sub FillViewStateHalaman", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
        End Try

    End Sub


#End Region
#Region "JoinAllViewstateHalaman"

    Private Function JoinAllViewstateHalaman() As String
        Dim totalpages As Integer = CType(lblTotPage.Text, Integer)
        Dim strAgreement As String
        Dim koma As String
        Dim ihal As Integer
        Dim hal As String
        Context.Trace.Write("JoinAllViewstateHalaman")

        strAgreement = ""
        For ihal = 1 To totalpages
            hal = CStr("hal" & CStr(ihal))
            If strAgreement = "" Then koma = "" Else koma = ","
            strAgreement = strAgreement & koma & CType(viewstate(hal), String)
            Viewstate("AllPage") = strAgreement
        Next
        Context.Trace.Write("strAgreement.Trim = " & strAgreement.Trim)
        Return strAgreement.Trim

    End Function

#End Region
#Region "Check apakah agrement ini pernah di centang atau tidak"

    Public Sub CheckIfEverChecked()
        Context.Trace.Write("CheckIfEverChecked()")
        Dim agreementchecked(pageSize) As String
        Dim chkAgreement As CheckBox
        Dim loopingchecked As Int16
        Dim hyAgreementGrid As HyperLink
        Dim recordDataGrid As Int16
        recordDataGrid = CType(dtgdocumentdistribution.Items.Count, Int16)


        If CType(viewstate("hal" & CStr(CInt(lblPage.Text)).Trim), String) <> "" Then
            agreementchecked = Split(CType(viewstate("hal" & CStr(CInt(lblPage.Text)).Trim), String), ",")
            For loopingchecked = 0 To CType(recordDataGrid - 1, Int16)
                Context.Trace.Write("Var loopingchecked :" + CType(loopingchecked, String))
                chkAgreement = CType(dtgdocumentdistribution.Items(loopingchecked).FindControl("chkPrint"), CheckBox)
                hyAgreementGrid = CType(dtgdocumentdistribution.Items(loopingchecked).FindControl("hypAgreementNodtg"), HyperLink)

                For periksa As Int16 = 0 To CType(UBound(agreementchecked), Int16)
                    If chkAgreement.Checked = False Then
                        If hyAgreementGrid.Text.Trim = agreementchecked(periksa).Trim Then
                            chkAgreement.Checked = True
                        End If
                    End If

                Next
            Next


        End If

    End Sub

#End Region
#Region "Clear ViewState"

    Private Sub ClearViewState()
        Dim totalpages As Integer = dtgdocumentdistribution.PageCount
        Dim strAgreement As String
        Dim koma As String
        Dim i As Integer
        Dim hal As String

        strAgreement = ""
        For i = 0 To totalpages
            hal = CStr("hal" & CStr(i))
            viewstate(hal) = ""
        Next
        viewstate("AllPage") = ""
    End Sub

#End Region


    Private Sub ImbSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
         
        If ConvertDate2(txtAgreementDate.Text) > Me.BusinessDate Then
            ShowMessage(lblMessage, "Tanggal kirim kontrak harus <= tgl hari ini", True)
            Exit Sub
        End If

        If ConvertDate2(txtSlipDate.Text) > Me.BusinessDate Then
            ShowMessage(lblMessage, "Tanggal PDC harus <= tanggal hari ini", True)
            Exit Sub
        End If
          




        With oCustomClass

            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationID
            .CGID = Me.GroubDbID
            .strConnection = GetConnectionString()
            .BusinessDate = Me.BusinessDate
            .LoginId = Me.Loginid

            .AgreementSentDate = ConvertDate2(txtAgreementDate.Text)
            .AgreementReceive = txtAgreementBy.Text.Trim
             
            .SlipSetoranDate = ConvertDate2(txtSlipDate.Text)
            .SlipSetoranReceive = txtSlipSetoranBy.Text.Trim

            .Insurancedate = ConvertDate2(txtInsuranceDate.Text) 
            .InsuranceReceive = txtInsuranceBy.Text.Trim

            Dim txtWelc = IIf(txtWelcomeLetter.Text.Trim = "", "01/01/1900", txtWelcomeLetter.Text.Trim)
            .WelcomeLetter = ConvertDate2(txtWelc)
            .WelcomeLetterReceiveBy = txtWelcomeLetterreceiveBy.Text.Trim

            Dim txtCardInst = IIf(txtKartuAngsuran.Text.Trim = "", "01/01/1900", txtKartuAngsuran.Text.Trim)
            .CardInstallment = ConvertDate2(txtCardInst)
            .CardInstallmentReceiveBy = txtKartuAngsuranReceiveBy.Text.Trim
            .Courier = txtKurir.Text.Trim
        End With
        Try

       
            oCustomClass = oController.DocumentDistributionSave(oCustomClass)
            If (oCustomClass.approveby.Trim <> "") Then
                ShowMessage(lblMessage, oCustomClass.approveby, True)
                Exit Sub 
            End If

        pnlsearch.Visible = True
        pnlDtGrid.Visible = True
        pnlDistributionActivity.Visible = False
        txtAgreementBy.Text = ""
        txtSlipSetoranBy.Text = ""
        txtInsuranceBy.Text = ""
        txtSlipDate.Text = ""
        txtInsuranceDate.Text = ""
        txtAgreementDate.Text = ""

        txtWelcomeLetter.Text = ""
        txtWelcomeLetterreceiveBy.Text = ""
        txtKartuAngsuran.Text = ""
        txtKartuAngsuranReceiveBy.Text = ""
        txtKurir.Text = ""

        Me.Flag = "NO"
        Me.Flag1 = "NO"
            Me.Flag2 = "NO"

        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

End Class