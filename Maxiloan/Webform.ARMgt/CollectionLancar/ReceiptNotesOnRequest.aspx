﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ReceiptNotesOnRequest.aspx.vb"
    Inherits="Maxiloan.Webform.ARMgt.ReceiptNotesOnRequest" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ReceiptNotesOnRequest</title>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />  
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%#Request.servervariables("SERVER_NAME")%>/';
    </script>
    <script language="javascript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;
        function OpenWinCustomer(pID) {
            window.open('../../Webform.LoanOrg/Credit/ViewPersonalCustomer.aspx?CustomerID=' + pID + '&style=Collection', 'UserLookup', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>    
        <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">           
                <h3>
                    CETAK KWITANSI ON REQUEST
                </h3>
            </div>
        </div>  
        <asp:Panel ID="pnlsearch" runat="server">    
        <div class="form_box">
	        <div class="form_single">
                <label class ="label_req">Collection Group</label>
                <asp:DropDownList ID="cboCG" runat="server">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="rfvCG" runat="server" Display="Dynamic" ControlToValidate="cboCG" CssClass="validator_general"
                    ErrorMessage="*"></asp:RequiredFieldValidator>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Cari Berdasarkan</label>
                <asp:DropDownList ID="cboSearchBy" runat="server">
                    <asp:ListItem Value="Select One">Select One</asp:ListItem>
                    <asp:ListItem Value="CollectionAgreement.AgreementNo">No Kontrak</asp:ListItem>
                    <asp:ListItem Value="Customer.Name">Nama Customer</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearchBy" runat="server" ></asp:TextBox>
	        </div>
        </div>
        <div class="form_button">
            <asp:Button ID="Buttonsearch" runat="server"  Text="Search" CssClass ="small button blue">
            </asp:Button>&nbsp;
            <asp:Button ID="ButtonReset" runat="server"  Text="Reset" CssClass ="small button gray"
                CausesValidation="False"></asp:Button>
	    </div>                  
        </asp:Panel>
        <asp:Panel ID="pnlDtGrid" runat="server">
        <div class="form_box_title">
            <div class="form_single">           
                <h4>
                    DAFTAR KONTRAK CETAK KWITANSI ON REQUEST
                </h4>
            </div>
        </div>  
        <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
            <asp:DataGrid ID="dtgReceiptNotes" runat="server" Width="100%" BorderStyle="None" BorderWidth="0" OnSortCommand="SortGrid"                        
                        DataKeyField="AgreementNo" AutoGenerateColumns="False" AllowSorting="True" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="hypAgreementNodtg" runat="server" Text='<%# Container.Dataitem("AgreementNo")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="CustomerName" HeaderText="NAMA CUSTOMER">                                
                                <ItemTemplate>
                                    <a href="javascript:OpenWinCustomer('<%# Container.Dataitem("CustomerID")%>');">
                                        <asp:Label ID="hypCustomerNamedtg" runat="server" Text='<%# Container.Dataitem("CustomerName")%>'>
                                        </asp:Label></a>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="NEXTINSTALLMENTNUMBER" SortExpression="NextInstallmentNumber"
                                HeaderText="ANGSURAN KE">                                
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="NEXTINSTALLMENTDATE" SortExpression="NextInstallmentDate"
                                HeaderText="TGL ANGSURAN BERIKUT" DataFormatString="{0:dd/MM/yyyy}">                                
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="ENDPASTDUEDAYS" SortExpression="EndPastDueDays" HeaderText="HARI TELAT">                                                                
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="Collector" SortExpression="COLLECTOR" HeaderText="COLLECTOR">                                
                            </asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="CETAK">                                
                                <ItemTemplate>
                                    <asp:ImageButton CommandName="Print" ID="imgSelect" runat="server" ImageUrl="../../images/IconDocument.gif">
                                    </asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" SortExpression="ApplicationId" HeaderText="ApplicationId">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblApplicationId" runat="server" Text='<%# Container.Dataitem("ApplicationId")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" HeaderText="BranchId">
                                <ItemTemplate>
                                    <asp:Label ID="lblBranchId" runat="server" Text='<%# Container.Dataitem("BranchID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>                        
                    </asp:DataGrid>        
            <div class="button_gridnavigation">
	                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
		                CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
		                CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
		                CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
		                CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
	                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
		                EnableViewState="False"></asp:Button>
	                <asp:RangeValidator   CssClass="validator_general"   ID="rgvGo" runat="server" ControlToValidate="txtPage"
                        MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer"></asp:RangeValidator>
                    <asp:RequiredFieldValidator    CssClass="validator_general"   ID="rfvGo" runat="server" ControlToValidate="txtPage"
                        ErrorMessage="No Halaman Salah" Display="Dynamic"></asp:RequiredFieldValidator>    
                </div>
            <div class="label_gridnavigation">
	                <asp:Label ID="lblPage" runat="server"></asp:Label>of
	                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
	                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
            </div>
            </div> 
        </div>                           
        </div> 
        </asp:Panel>
        <asp:Panel ID="pnlChange" runat="server">        
        <div class="form_box">	        
		        <div class="form_left">
                    <label>No Kontrak</label>
                    <asp:HyperLink ID="hypAgreementNoSlct" runat="server"></asp:HyperLink>
		        </div>
		        <div class="form_right">	
                    <label>Nama Customer</label>	
                    <asp:HyperLink ID="hypCustomerNameSlct" runat="server"></asp:HyperLink>	
		        </div>	        
        </div>
        <div class="form_box">	        
		        <div class="form_left">
                    <label>Angsuran Ke</label>
                    <asp:Label ID="lblInstallmentNo" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">	
                    <label>Tanggal Angsuran</label>		
                    <asp:Label ID="lblInstallmentDate" runat="server"></asp:Label>
		        </div>	        
        </div>
        <div class="form_box">	        
		        <div class="form_left">
                    <label>Jumlah Hari Telat</label>
                    <asp:Label ID="lblOverDueDays" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">			
                    <label>Collector</label>
                    <asp:Label ID="lblCollector" runat="server"></asp:Label>
		        </div>	        
        </div>
        <div class="form_box_title">
            <div class="form_single">           
                <h4>
                    JADWAL ANGSURAN
                </h4>
            </div>
        </div>  
        <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
            <asp:DataGrid ID="dtgSelect" runat="server" Width="100%" OnSortCommand="SortGrid"
                        BorderStyle="None" BorderWidth="0"
                        AutoGenerateColumns="False" AllowSorting="True" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:BoundColumn DataField="InsSeqNo" HeaderText="ANGS KE"></asp:BoundColumn>
                            <asp:BoundColumn DataField="InstallmentAmount" HeaderText="JUMLAH ANGSURAN" DataFormatString="{0:###,###,###.00}">
                            <ItemStyle CssClass="item_grid_right"></ItemStyle>                                
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="DueDate" HeaderText="TGL JT" DataFormatString="{0:dd/MM/yyyy}">                                
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="PaidAmount" HeaderText="JUMLAH DIBAYAR" DataFormatString="{0:###,###,###.00}">
                                <ItemStyle CssClass="item_grid_right"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="PaidDate" HeaderText="TGL BAYAR" DataFormatString="{0:dd/MM/yyyy}">                                
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="ReceiptNo" HeaderText="NO KWITANSI"></asp:BoundColumn>
                        </Columns>                        
            </asp:DataGrid>       
            <div class="button_gridnavigation">
	                <asp:ImageButton ID="imbFirstPageSelect" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
		                CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbPrevPageSelect" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
		                CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbNextPageSelect" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
		                CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbLastPageSelect" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
		                CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:TextBox ID="txtPageSelect" runat="server">1</asp:TextBox>
	                <asp:Button ID="btnPageNumbSelect" runat="server" Text="Go" CssClass="small buttongo blue"
		                EnableViewState="False"></asp:Button>
	                <asp:RangeValidator   CssClass="validator_general"   ID="rgvGoSelect" runat="server" ControlToValidate="txtPage"
                        MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer"></asp:RangeValidator>
                    <asp:RequiredFieldValidator    CssClass="validator_general"   ID="rfvGoSelect" runat="server" ControlToValidate="txtPage"
                        ErrorMessage="No Halaman Salah" Display="Dynamic"></asp:RequiredFieldValidator>  
                </div>
            <div class="label_gridnavigation">
	            <asp:Label ID="lblPageSelect" runat="server"></asp:Label>of
	            <asp:Label ID="lblTotPageSelect" runat="server"></asp:Label>, Total
	            <asp:Label ID="lblrecordSelect" runat="server"></asp:Label>record(s)
            </div>
            </div>   
        </div> 
        </div> 
        <div class="form_box">
	        <div class="form_single">
                <label>Cetak Kwitansi Angsuran Ke</label>
                <asp:DropDownList ID="cboSelectInstallmentNo" runat="server">
                </asp:DropDownList>
	        </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonSave" runat="server"  Text="Save" CssClass ="small button blue">
            </asp:Button>&nbsp;
            <asp:Button ID="ButtonCancel" runat="server"  Text="Cancel" CssClass ="small button gray"
                CausesValidation="False"></asp:Button>
	    </div>       
    </asp:Panel>         
    </form>
</body>
</html>
