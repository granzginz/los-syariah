﻿#Region "Imports"
Imports System.Text
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class SSPrint
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.SPPrint
    Private oController As New SPPrintController

#End Region
    Private Property AgreementChosen() As String
        Get
            Return CType(ViewState("AgreementChosen"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("AgreementChosen") = Value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not IsPostBack Then
            If Request.QueryString("file") <> "" Then
                Dim strFileLocation As String
                strFileLocation = "../../XML/" & Request.QueryString("file")

                Response.Write("<script language = javascript>" & vbCrLf _
                                & "var x = screen.width; " & vbCrLf _
                                & "var y = screen.height; " & vbCrLf _
               & "window.open('" & strFileLocation & "','accacq', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes, resizable=1') " & vbCrLf _
               & "</script>")
            End If
            Me.FormID = "COLLSSPRINT"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                pnlDtGrid.Visible = False
                pnlsearch.Visible = True
                txtSPDate.Text = Format(Me.BusinessDate, "dd/MM/yyyy")
                'FillCbo()
            End If
        End If
    End Sub

    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Buttonsearch.Click
        Dim strSearch As New StringBuilder
        If txtSPDate.Text.Trim <> "" Then
            strSearch.Append("SPDate='" & SPPrint.ConvertDate(txtSPDate.Text) & "'")
        End If
        If cboSearchBy.SelectedItem.Value.Trim <> "Select One" Then
            If Right(txtSearchBy.Text.Trim, 1) = "%" Then
                strSearch.Append(" and " & cboSearchBy.SelectedItem.Value.Trim & " like '" & txtSearchBy.Text.Trim & "'")
            Else
                strSearch.Append(" and " & cboSearchBy.SelectedItem.Value.Trim & "='" & txtSearchBy.Text.Trim & "'")
            End If
        End If
        If cboSPType.SelectedItem.Value.Trim <> "" Then
            strSearch.Append(" and SpType='" & cboSPType.SelectedItem.Value.Trim & "'")
        End If
        Me.SearchBy = strSearch.ToString
        Me.SortBy = ""
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
    Private Sub imbPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonPrint.Click
        If CheckFeature(Me.Loginid, Me.FormID, "Print", Me.AppID) Then


            IsiAgreementChoosen()
            If AgreementChosen <> "" Then
                Me.SearchBy = Me.SearchBy + " and RepoSuratSelesai.ApplicationID in (" & AgreementChosen & ")"
            End If

            Dim hasil As Integer

            With oCustomClass
                .strConnection = GetConnectionString()
                .WhereCond = Me.SearchBy
                .BusinessDate = Me.BusinessDate
                .LoginId = Me.Loginid
            End With
            oCustomClass = oController.SSPrintProcess(oCustomClass)
            hasil = oCustomClass.hasil
            If hasil = 1 Then
                Dim cookie As HttpCookie = Request.Cookies("RptSSPrint")
                Dim cmdwhere As String
                cmdwhere = Me.SearchBy
                If Not cookie Is Nothing Then
                    cookie.Values("cmdwhere") = cmdwhere
                    Response.AppendCookie(cookie)
                Else
                    Dim cookieNew As New HttpCookie("RptSSPrint")
                    cookieNew.Values.Add("cmdwhere", cmdwhere)
                    Response.AppendCookie(cookieNew)
                End If
                Response.Redirect("SSPrintViewer.aspx")
            Else
                ShowMessage(lblMessage, "Gagal", True)
            End If

        End If
    End Sub
    Sub IsiAgreementChoosen()
        Dim strAgreement As New StringBuilder
        Dim i, j As Integer
        Dim jmlAgreement As Integer
        Dim chkSelect As CheckBox
        Dim lblApplicationID As Label
        Dim isSelect As Boolean
        isSelect = True
        For i = 0 To dtgSSPrinting.Items.Count - 1
            chkSelect = CType(dtgSSPrinting.Items(i).FindControl("chkprint"), CheckBox)
            lblApplicationID = CType(dtgSSPrinting.Items(i).FindControl("lblapplicationid"), Label)
            If chkSelect.Checked = True Then
                strAgreement.Append(CStr(IIf(strAgreement.ToString = "", "", ",")) & "'" & lblApplicationID.Text.Trim & "'")
                isSelect = False
            End If
        Next

        AgreementChosen = strAgreement.ToString()

    End Sub

    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        pnlDtGrid.Visible = False
        pnlsearch.Visible = True
        'cboCG.ClearSelection()
        txtSPDate.Text = Format(Me.BusinessDate, "dd/MM/yyyy")
    End Sub
    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        Dim intloop As Integer
        Dim hypID As HyperLink

        With oCustomClass
            .strConnection = GetConnectionString
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oCustomClass = oController.ListSSPrinting(oCustomClass)

        DtUserList = oCustomClass.ListSPPrint
        DvUserList = DtUserList.DefaultView
        recordCount = oCustomClass.TotalRecord
        DvUserList.Sort = Me.SortBy
        dtgSSPrinting.DataSource = DvUserList

        Try
            dtgSSPrinting.DataBind()
        Catch
            dtgSSPrinting.CurrentPageIndex = 0
            dtgSSPrinting.DataBind()
        End Try
        PagingFooter()
        pnlDtGrid.Visible = True
        'pnlResult.Visible = False
        pnlsearch.Visible = True
    End Sub
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        'DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblRecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        'DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Protected Sub btnPageNumb_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPageNumb.Click
        If IsNumeric(txtpage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtpage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtpage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                'DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub
#End Region

    Private Sub dtgSSPrinting_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgSSPrinting.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim hyTemp As HyperLink

            Dim lblApplicationId As Label

            lblApplicationId = CType(e.Item.FindControl("lblApplicationId"), Label)
            '*** Agreement No link
            hyTemp = CType(e.Item.FindControl("hypAgreementNo"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenAgreementNo('" & "Collection" & "', '" & Server.UrlEncode(lblApplicationId.Text.Trim) & "')"

        End If
    End Sub
End Class