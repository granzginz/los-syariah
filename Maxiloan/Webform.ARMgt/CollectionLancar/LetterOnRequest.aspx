﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="LetterOnRequest.aspx.vb"
    Inherits="Maxiloan.Webform.ARMgt.LetterOnRequest" %>

<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../../Webform.UserController/UcSearchBy.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcBranchCollection" Src="../../Webform.UserController/UcBranchCollection.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>LetterOnRequest</title>
    <script language="javascript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';

        function fConfirm() {
            if (window.confirm("Apakah yakin mau hapus data ini ? "))
                return true;
            else
                return false;
        }
        function fback() {
            history.go(-1);
            return false;
        }

        function OpenCollLetterFieldView(pStyle) {
            window.open(ServerName + App + '/Webform.ARMgt/Setting/CollLetterField.aspx?Style=' + pStyle, 'RECEIPTNOTES', 'left=0, top=0, width='+ x + ', height = ' + y +', menubar=0,scrollbars=1');
        }

        function OpenAgreementNo(Style, ApplicationId) {
            window.open(ServerName + App + '/Webform.LoanOrg/Credit/ViewStatementOfAccount.aspx?Style=' + Style + '&ApplicationId=' + ApplicationId, 'Agreement', 'left=0, top=0, width='+ x + ', height = ' + y +', menubar=1,scrollbars=1')
        }

        function OpenCustomer(Style, CustomerId) {
            window.open(ServerName + App + '/Webform.LoanOrg/Credit/ViewPersonalCustomer.aspx?Style=' + Style + '&CustomerId=' + CustomerId, 'Personal', 'left=0, top=0, width='+ x + ', height = ' + y +', menubar=0,scrollbars=1')
        }
        function Close() {
            window.close();
        }
    </script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">           
            <h3>
                CETAK SURAT BERDASARKAN PERMINTAAN
            </h3>
        </div>
    </div>    
    <asp:Panel ID="pnlTop" runat="Server">
        <div class="form_box">
	        <div class="form_single">
                <label class ="label_req" >Collection Group</label>
                <uc1:UcBranchCollection id="oCGID" runat="server"></uc1:UcBranchCollection>	        
            </div>
        </div>
        <div class="form_box_uc">
                <uc1:UcSearchBy id="oSearchBy" runat="server"></uc1:UcSearchBy>	        
        </div>
        <div class="form_button">
            <asp:Button ID="Buttonsearch" runat="server"  Text="Search" CssClass ="small button blue">
            </asp:Button>&nbsp;
            <asp:Button ID="ButtonReset" runat="server"  Text="Reset" CssClass ="small button gray"
                CausesValidation="False"></asp:Button>
	    </div>                
    </asp:Panel>
    <asp:Panel ID="pnlgrid" runat="Server">
        <div class="form_box_title">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgLetterOnRequest" runat="server" Width="100%"
                        AllowSorting="True" AutoGenerateColumns="False" DataKeyField="AgreementNo" BorderStyle="None" BorderWidth="0" OnSortCommand="Sorting"
                        CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn SortExpression="CollAgr.AgreementNo" HeaderText="NO KONTRAK">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="hypAgreementNoGrid" runat="server" Visible="true" Text='<%#Container.DataItem("AgreementNo")%>'>
                                    </asp:HyperLink>
                                    <asp:Literal ID="ltlApplicationId" runat="server" Visible="false" Text='<%#Container.DataItem("ApplicationId")%>'>
                                    </asp:Literal>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Name" HeaderText="NAMA CUSTOMER">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="hypCustomerNameGrid" runat="server" Visible="true" Text='<%#Container.DataItem("CustomerName")%>'>
                                    </asp:HyperLink>
                                    <asp:Literal ID="ltlCustomerId" runat="server" Visible="false" Text='<%#Container.DataItem("CustomerId")%>'>
                                    </asp:Literal>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="MailingAddress" HeaderText="ALAMAT">                                
                                <ItemTemplate>
                                    <asp:Literal ID="Literal1" runat="server" Visible="true" Text='<%#Container.DataItem("MailingAddress")%>'>
                                    </asp:Literal>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="SURAT">                                
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbEdit" runat="server" ImageUrl="../../Images/IconLetter.gif"
                                        CausesValidation="False" CommandName="Letter"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>                     
                    </asp:DataGrid>        
                <div class="button_gridnavigation">
	                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
		                CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
		                CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
		                CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
		                CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
	                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
		                EnableViewState="False"></asp:Button>
	                <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage"
		                MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer"
		                CssClass="validator_general"></asp:RangeValidator>
	                <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
		                ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>
                <div class="label_gridnavigation">
	                <asp:Label ID="lblPage" runat="server"></asp:Label>of
	                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
	                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                </div>
            </div>                     
        </div>  
        </div>  
    </asp:Panel>
    <asp:Panel ID="pnlLetter" runat="server">
        <div class="form_box_title">
	        <div class="form_single">
                <label>No Kontrak</label>
                <asp:HyperLink ID="hypAgreementNo" runat="server">AgreementNo</asp:HyperLink>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">   
                <label>Nama Customer</label>
                <asp:HyperLink ID="hypCustomerName" runat="server">CustomerName</asp:HyperLink>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Nama Asset</label>
                <asp:Literal ID="ltlAsset" runat="server" Visible="True"></asp:Literal>
	        </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">              
                <h4>
                    Angsuran Ke
                </h4>
            </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <asp:Literal ID="ltlInstallmentNo" runat="server" Visible="True"></asp:Literal>
            </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Jumlah Angsuran</label>
                <asp:Literal ID="ltlInstallmentAmount" runat="server" Visible="True"></asp:Literal>
	        </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">              
                <h4>
                    Tanggal Angsuran
                </h4>
            </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <asp:Literal ID="ltlInstallmentDate" runat="server" Visible="True"></asp:Literal>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Sisa A/R</label>
                <asp:Literal ID="ltlOSBalance" runat="server" Visible="True"></asp:Literal>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Nama Surat</label>
                <asp:DropDownList ID="cboLetterName" runat="server">
                </asp:DropDownList>
	        </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonNext" runat="server"  Text="Next" CssClass ="small button blue"
                CausesValidation="True"></asp:Button>&nbsp;
            <asp:Button ID="ButtonCancel" runat="server"  Text="Cancel" CssClass ="small button gray"
                CausesValidation="False"></asp:Button>
	    </div>               
    </asp:Panel>
    <asp:Panel ID="pnlViewTemplate" runat="server">   
        <div class="form_box_title">
	        <div class="form_single">
                <label>Nama Surat</label>
                <asp:Literal ID="ltlLetterName" runat="server" Visible="True"></asp:Literal>
	        </div>
        </div> 
        <div class="form_box">
	        <div class="form_single">
                <label>Template</label>
                <asp:TextBox ID="txtTemplate" runat="server"  Height="286px" Width="632px"
                TextMode="MultiLine"></asp:TextBox>
	        </div>
        </div> 
        <div class="form_button">
            <asp:Button ID="ButtonPrint" runat="server"  Text="Print" CssClass ="small button blue"
                CausesValidation="True"></asp:Button>&nbsp;
            <asp:Button ID="ButtonBack" runat="server"  Text="Back" CssClass ="small button gray"
                CausesValidation="True"></asp:Button>&nbsp;
            <asp:Button ID="ButtonCancelTemplate" runat="server"  Text="Cancel" CssClass ="small button gray"
                CausesValidation="False"></asp:Button>
	    </div>               
    </asp:Panel>
    </form>
</body>
</html>
