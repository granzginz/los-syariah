﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ReportDCR.aspx.vb" Inherits="Maxiloan.Webform.ARMgt.ReportDCR" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ucCollectorRpt" Src="../../Webform.UserController/ucCollectorRpt.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>ReportDCR</title>
    <script src="../../Maxiloan.js" type="text/javascript">
			function fConfirm() {
				if (window.confirm("Are you sure want to delete this record?")) 
					return true;
				else
					return false;
			}
			function fback() {
					history.go(-1);
					return false;
			}	
					
			function OpenViewCollector(pCollID,pCGID){
			var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
			var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
			window.open('http://<%#Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.ARMgt/Setting/CollectorView.aspx?CollectorID=' + pCollID + '&CGID=' + pCGID, 'CollectorView', 'left=15, top=10, width=900, height=350, menubar=0, scrollbars=yes');
			}
			
			function OpenWindowActivity(pAgreementNo,pCustomerID,pCustomerName){
			var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
			var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
			window.open('http://<%#Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.ARMgt/Inquiry/InqCollActivityResult.aspx?AgreementNo=' + pAgreementNo + '&CustomerID=' + pCustomerID + '&CustomerName=' + pCustomerName + '&Style=Collection&referrer=CollActivity', 'CollectionActivity', 'left=0, top=0, width=1000, height=650, menubar=0, scrollbars=yes');
			}
			
			function OpenWindowHistory(pAgreementNo,pCustomerID,pCustomerName){
			var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
			var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
			window.open('http://<%#Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.ARMgt/Inquiry/InqCollActivityHistory.aspx?AgreementNo=' + pAgreementNo + '&CustomerID=' + pCustomerID + '&CustomerName=' + pCustomerName, 'CollectionActivity', 'left=0, top=0, width=1000, height=650, menubar=0, scrollbars=yes');
			}
			
			function OpenViewAgreement(pAgreementNo){
			var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
			var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
			window.open('http://<%#Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.LoanOrg/Credit/ViewStatementofAccount.aspx?AgreementNo=' + pAgreementNo + '&Style=Collection', 'AgreementView', 'left=5, top=5, width=985, height=600, menubar=0, scrollbars=yes');
			}
			
			function OpenViewCustomer(pCustomerID){
			var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
			var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
			window.open('http://<%#Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.LoanOrg/Credit/ViewPersonalCustomer.aspx?CustomerID=' + pCustomerID + '&Style=Collection', 'CustomerView', 'left=5, top=5, width=985, height=600, menubar=0, scrollbars=yes');
			}
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
</head>
<body>
    <form id="Form1" method="post" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="PnlSearch" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    CETAK DAILY COLLECTOR REPORT
                </h3>
            </div>
        </div>
        <div class="form_box_uc">
            <uc1:uccollectorrpt id="UcCollector" runat="server"></uc1:uccollectorrpt>
        </div>
        <div class="form_box">
            <div class="form_single">                
                        <label>
                            Tanggal Rencana</label>
                        <asp:TextBox ID="txtDateFrom" runat="server"></asp:TextBox>
                        <asp:CalendarExtender ID="txtDateFrom_CalendarExtender" runat="server"
                            Enabled="True" TargetControlID="txtDateFrom" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator" runat="server" ErrorMessage="*"
                            CssClass="validator_general" ControlToValidate="txtDateFrom"></asp:RequiredFieldValidator>                    
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnprint" runat="server" Text="Print" CssClass="small button blue">
            </asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
