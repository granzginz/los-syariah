﻿#Region "Imports"
Imports System.Text
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonVariableHelper
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class ReceiptNotesOnRequest
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.ReceiptNotesOnRequest
    Private oController As New ReceiptNotesOnRequestController


    Private currentPageChange As Int32 = 1
    Private pageSizeChange As Int16 = 10
    Private currentPageNumberChange As Int16 = 1
    Private totalPagesChange As Double = 1
    Private recordCountChange As Int64 = 1


#End Region

#Region "Property"
    Private Property ApplicationID() As String
        Get
            Return CStr(viewstate("ApplicationID"))
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property
    Private Property oData() As DataTable
        Get
            Return CType(viewstate("oData"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            viewstate("oData") = Value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            If Request.QueryString("strFileLocation") <> "" Then
                Dim strHTTPServer As String
                Dim StrHTTPApp As String
                Dim strNameServer As String
                Dim strFileLocation As String

                strHTTPServer = Request.ServerVariables("PATH_INFO")
                strNameServer = Request.ServerVariables("SERVER_NAME")
                StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                strFileLocation = "http://" & strNameServer & "/" & StrHTTPApp & "/XML/" & Request.QueryString("strFileLocation") & ".pdf"
                '& "history.back(-1) " & vbCrLf _
                'Call file PDF 
                Response.Write("<script language = javascript>" & vbCrLf _
                & "window.open('" & strFileLocation & "','Collection', 'left=0, top=0, width=600, height=480, menubar=0, scrollbars=yes') " & vbCrLf _
                & "</script>")
            End If
            Me.FormID = "CollReceiptNotes"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppID) Then
                InitialDefaultPanel()
                FillCbo()
            End If
        End If
    End Sub
    Private Sub InitialDefaultPanel()
        pnlDtGrid.Visible = False
        pnlsearch.Visible = True
        pnlChange.Visible = False
    End Sub
    Private Sub FillCbo()
        Dim oDataTable As New DataTable
        With oCustomClass
            .strConnection = GetConnectionSTring
            .strKey = "CG"
            .CGID = Me.GroubDbID
            .CollectorType = ""
        End With
        oCustomClass = oController.ReceiptNotesOnRequestListCG(oCustomClass)
        oDataTable = oCustomClass.ListRAL
        With cboCG
            .DataSource = oDataTable
            .DataTextField = "CGName"
            .DataValueField = "CGID"
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = ""
            If oDataTable.Rows.Count = 1 Then
                .Items.FindByValue(Me.GroubDbID).Selected = True
            End If
        End With
    End Sub

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub
#End Region
#Region "NavigationSelect"
    Private Sub PagingFooterSelect()
        lblPageSelect.Text = currentPageChange.ToString()
        totalPagesChange = Math.Ceiling(CType((recordCountChange / CType(pageSizeChange, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPageSelect.Text = "1"
            rgvGoSelect.MaximumValue = "1"
        Else
            lblTotPageSelect.Text = CType(totalPagesChange, String)
            rgvGoSelect.MaximumValue = CType(totalPagesChange, String)
        End If
        lblRecordSelect.Text = CType(recordCountChange, String)

        If currentPageChange = 1 Then
            imbPrevPageSelect.Enabled = False
            imbFirstPageSelect.Enabled = False
            If totalPagesChange > 1 Then
                imbNextPageSelect.Enabled = True
                imbLastPageSelect.Enabled = True
            Else
                imbPrevPageSelect.Enabled = False
                imbNextPageSelect.Enabled = False
                imbLastPageSelect.Enabled = False
                imbFirstPageSelect.Enabled = False
            End If
        Else
            imbPrevPageSelect.Enabled = True
            imbFirstPageSelect.Enabled = True
            If currentPageChange = totalPagesChange Then
                imbNextPageSelect.Enabled = False
                imbLastPageSelect.Enabled = False
            Else
                imbLastPageSelect.Enabled = True
                imbNextPageSelect.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLinkSelect_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "FirstSelect" : currentPageChange = 1
            Case "LastSelect" : currentPageChange = Int32.Parse(lblTotPageSelect.Text)
            Case "NextSelect" : currentPageChange = Int32.Parse(lblPageSelect.Text) + 1
            Case "PrevSelect" : currentPageChange = Int32.Parse(lblPageSelect.Text) - 1
        End Select
        'If Me.SortBy Is Nothing Then
        '    Me.SortBy = ""
        'End If
        BindGridInstallment()
    End Sub

    Private Sub imbGoPageSelect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumbSelect.Click
        If IsNumeric(txtPageSelect.Text) Then
            If CType(lblTotPageSelect.Text, Integer) > 1 And CType(txtPageSelect.Text, Integer) <= CType(lblTotPageSelect.Text, Integer) Then
                currentPageChange = CType(txtPageSelect.Text, Int16)
                'If Me.SortBy Is Nothing Then
                '    Me.SortBy = ""
                'End If
                BindGridInstallment()
            End If
        End If
    End Sub
    Sub BindGridInstallment()
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        Dim intloop As Integer
        Dim hypID As HyperLink

        With oCustomClass
            .strConnection = GetConnectionString
            .CGID = Me.GroubDbID
            .CurrentPage = currentPageChange
            .PageSize = pageSizeChange
            .ApplicationID = Me.ApplicationID
            .NextInstallment = CType(lblInstallmentNo.Text, Integer)
        End With
        oCustomClass = oController.ViewDataInstallment(oCustomClass)
        DtUserList = oCustomClass.ListRAL
        DvUserList = DtUserList.DefaultView
        recordCountChange = oCustomClass.TotalRecord
        dtgSelect.DataSource = DvUserList

        Try
            dtgSelect.DataBind()
            With cboSelectInstallmentNo
                .DataSource = DtUserList
                .DataTextField = "InsSeqNo"
                .DataValueField = "InsSeqNo"
                .DataBind()
                .Items.Insert(0, "Select One")
                .Items(0).Value = ""
            End With
        Catch
            dtgSelect.CurrentPageIndex = 0
            dtgSelect.DataBind()
        End Try
        PagingFooterSelect()
        pnlChange.Visible = True
        pnlsearch.Visible = False
        pnlDtGrid.Visible = False
    End Sub
#End Region

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        Dim intloop As Integer
        Dim hypID As HyperLink

        With oCustomClass
            .strConnection = GetConnectionString
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oCustomClass = oController.ReceiptNotesOnRequestList(oCustomClass)

        DtUserList = oCustomClass.ListRAL
        DvUserList = DtUserList.DefaultView
        recordCount = oCustomClass.TotalRecord
        DvUserList.Sort = Me.SortBy
        dtgReceiptNotes.DataSource = DvUserList
        With oCustomClass
            .strConnection = GetConnectionString
        End With
        oCustomClass = oController.DataReceiptNotesOnRequest(oCustomClass)
        Me.oData = oCustomClass.ListRAL
        Try
            dtgReceiptNotes.DataBind()
        Catch
            dtgReceiptNotes.CurrentPageIndex = 0
            dtgReceiptNotes.DataBind()
        End Try



        PagingFooter()
        pnlDtGrid.Visible = True
        pnlsearch.Visible = True
    End Sub

    Sub BindGridSelectExecutor()
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        Dim intloop As Integer
        Dim hypID As HyperLink

        With oCustomClass
            .strConnection = GetConnectionString
            .CGID = cboCG.SelectedValue.Trim
            .CurrentPage = currentPageChange
            .PageSize = pageSizeChange
            .NextInstallment = CType(lblInstallmentNo.Text, Integer)
        End With
        oCustomClass = oController.ViewDataInstallment(oCustomClass)
        DtUserList = oCustomClass.ListRAL
        DvUserList = DtUserList.DefaultView
        recordCountChange = oCustomClass.TotalRecord
        dtgSelect.DataSource = DvUserList

        Try
            dtgSelect.DataBind()
            With cboSelectInstallmentNo
                .DataSource = DtUserList
                .DataTextField = "InsSeqNo"
                .DataValueField = "InsSeqNo"
                .DataBind()
                .Items.Insert(0, "Select One")
                .Items(0).Value = ""
            End With
        Catch
            dtgSelect.CurrentPageIndex = 0
            dtgSelect.DataBind()
        End Try
        PagingFooterSelect()
        pnlChange.Visible = True
        pnlsearch.Visible = False
        pnlDtGrid.Visible = False
    End Sub


    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Buttonsearch.Click
        Me.SearchBy = "CollectionAgreement.CGID='" & cboCG.SelectedItem.Value.Trim & "'"
        If cboSearchBy.SelectedItem.Value.Trim <> "Select One" Then
            Me.SearchBy = Me.SearchBy + " and " + cboSearchBy.SelectedItem.Value.Trim + " like'%" + txtSearchBy.Text.Trim + "%'"
        End If

        Me.SortBy = ""
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub dtgReceiptNotes_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgReceiptNotes.ItemCommand
        Select Case e.CommandName
            Case "Print"
                Dim lblbranchid As Label
                lblbranchid = CType(dtgReceiptNotes.Items(e.Item.ItemIndex).FindControl("lblBranchId"), Label)
                Dim lblApplicationId As Label
                Dim collector As String
                Dim oDataTable As New DataTable
                lblApplicationId = CType(dtgReceiptNotes.Items(e.Item.ItemIndex).FindControl("lblApplicationId"), Label)
                If checkfeature(Me.Loginid, Me.FormID, "Print", Me.AppID) Then
                    With oCustomClass
                        .strConnection = GetConnectionString
                        .ApplicationID = lblApplicationId.Text.Trim
                        .BusinessDate = Me.BusinessDate
                        .BranchId = lblbranchid.Text.Trim


                    End With
                    oCustomClass = oController.RequestDataSelect(oCustomClass)
                    oDataTable = oCustomClass.ListRAL
                    Me.BranchID = lblbranchid.Text.Trim
                    Me.ApplicationID = lblApplicationId.Text
                    If Not IsDBNull(oDataTable.Rows(0)("AgreementNo")) Then
                        hypAgreementNoSlct.Text = CType(oDataTable.Rows(0)("AgreementNo"), String)
                    End If
                    If Not IsDBNull(oDataTable.Rows(0)("CustomerName")) Then
                        hypCustomerNameSlct.Text = CType(oDataTable.Rows(0)("CustomerName"), String)
                    End If
                    If Not IsDBNull(oDataTable.Rows(0)("InstallmentNo")) Then
                        lblInstallmentNo.Text = CType(oDataTable.Rows(0)("InstallmentNo"), String)
                    End If

                    If Not IsDBNull(oDataTable.Rows(0)("EndPastDuedays")) Then
                        lblOverDueDays.Text = CType(oDataTable.Rows(0)("EndPastDuedays"), String)
                    End If
                    If Not IsDBNull(oDataTable.Rows(0)("InstallmentDate")) Then
                        lblInstallmentDate.Text = Format(oDataTable.Rows(0)("InstallmentDate"), "dd/MM/yyyy")
                    End If

                    If Not IsDBNull(e.Item.Cells(5).Text) Then
                        lblCollector.Text = CType(e.Item.Cells(5).Text, String)
                    End If

                    BindGridSelectExecutor()

                End If
        End Select
    End Sub

    Private Sub imbPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
        If cboSelectInstallmentNo.SelectedItem.Value.Trim = "" Then            
            ShowMessage(lblMessage, "Harap Pilih No Angsuran", True)
            Exit Sub
        End If
        Dim hasil As Integer
        With oCustomClass
            .strConnection = GetConnectionString()
            .ApplicationID = Me.ApplicationID
            .NextInstallment = CType(cboSelectInstallmentNo.SelectedItem.Value, Integer)
            .BusinessDate = Me.BusinessDate
            .NextBD = Me.BusinessDate
            .CGID = cboCG.SelectedValue.Trim
            .daysoverdue = CType(lblOverDueDays.Text.Trim, Integer)
            .ExecutorID = lblCollector.Text.Trim
            .BranchId = Me.BranchID



        End With

        oCustomClass = oController.GenerateKuitansiTagihOnRequest(oCustomClass)
        hasil = oCustomClass.hasil
        If hasil = 0 Then            
            ShowMessage(lblMessage, "Gagal", True)
        ElseIf hasil = 2 Then            
            ShowMessage(lblMessage, "Kwitansi untuk Angsuran  " + cboSelectInstallmentNo.SelectedItem.Value + " Sudah dibuat pada " + CType(oCustomClass.receiptdates, String), True)
        ElseIf hasil = 1 Then
            pnlsearch.Visible = True
            pnlDtGrid.Visible = True
            pnlChange.Visible = False
            DoBind(Me.SearchBy, Me.SortBy)


            Me.SearchBy = "ReceiptNotes.CGID='" & cboCG.SelectedItem.Value.Trim & "'"
            Me.SearchBy = Me.SearchBy & " and ReceiptNotes.InstallmentNo='" & CType(cboSelectInstallmentNo.SelectedItem.Value, Integer) & "' and ReceiptNotes.ApplicationId='" & Me.ApplicationID.Trim & "'"

            SendCookies()
            Response.Redirect("PrintKuitansiTagihOnRequestViewer.aspx")
        End If
    End Sub

    Private Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        pnlsearch.Visible = True
        pnlDtGrid.Visible = True
        pnlChange.Visible = False
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
    Public Function isVisiblePrint(ByVal strKey As String) As Boolean
        If strKey.Trim = "" Then
            Return False
        Else
            Return True
        End If
    End Function
#Region "SendCookies"
    Sub SendCookies()
        Context.Trace.Write("Send Cookies !")

        Dim cookie As HttpCookie = Request.Cookies(General.CommonCookiesHelper.COOKIES_PRINT_RECEIPT_NOTES)
        Dim strWhere As New StringBuilder
        strWhere.Append(Me.SearchBy)
        If Not cookie Is Nothing Then
            cookie.Values("SearchBy") = strWhere.ToString
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie(General.CommonCookiesHelper.COOKIES_PRINT_RECEIPT_NOTES)
            cookieNew.Values("SearchBy") = strWhere.ToString
            Response.AppendCookie(cookieNew)
        End If
        Context.Trace.Write("strWhere = " & strWhere.ToString)

    End Sub

#End Region
    Public Function isVisible(ByVal strNo As String) As Boolean
        Dim DataRow As DataRow()
        DataRow = Me.oData.Select(" NewRALNo ='" & strNo.Trim & "' ")
        If DataRow.Length > 0 Then
            Return True
        Else
            Return False
        End If
    End Function
    Private Sub imbReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        InitialDefaultPanel()
        cboSearchBy.ClearSelection()
        cboCG.ClearSelection()
        txtSearchBy.Text = ""

    End Sub
    Private Sub dtgReceiptNotes_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgReceiptNotes.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim hyTemp As HyperLink
            Dim lblApplicationId As Label
            lblApplicationId = CType(e.Item.FindControl("lblApplicationId"), Label)
            '*** Agreement No link
            hyTemp = CType(e.Item.FindControl("hypAgreementNodtg"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenAgreementNo('" & "Collection" & "', '" & Server.UrlEncode(lblApplicationId.Text.Trim) & "')"
        End If
    End Sub

End Class