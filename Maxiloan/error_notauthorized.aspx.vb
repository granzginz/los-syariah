﻿Public Class error_notauthorized
    Inherits WebBased

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ShowMessage(lblMessage, "Anda tidak diperbolehkan untuk melakukan proses ini!", True)
    End Sub

End Class