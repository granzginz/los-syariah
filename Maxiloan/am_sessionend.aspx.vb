﻿Public Class am_sessionend
    Inherits Maxiloan.Webform.WebBased

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ShowMessage(lblMessage, "Sesi login telah berakhir, silahkan logout kemudian login kembali.", True)
    End Sub

End Class