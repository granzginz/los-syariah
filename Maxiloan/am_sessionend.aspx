﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="am_sessionend.aspx.vb"
    Inherits="Maxiloan.Webform.am_sessionend" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="Maxiloan.js" type ="text/javascript" ></script>
    <link rel="Stylesheet" href="Include/General.css" type="text/css" />
       <!-- Global stylesheets -->
    <link href="lib/limitless/assets/css/fonts.css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css" />
    <link href="lib/limitless/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css" />
    <link href="lib/limitless/assets/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="lib/limitless/assets/css/core.css" rel="stylesheet" type="text/css" />
    <link href="lib/limitless/assets/css/components.css" rel="stylesheet" type="text/css" />
    <link href="lib/limitless/assets/css/colors.css" rel="stylesheet" type="text/css" />
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <%--<script type="text/javascript" src="lib/limitless/assets/js/plugins/loaders/pace.min.js"></script>--%>
    <script type="text/javascript" src="lib/limitless/assets/js/core/libraries/jquery.min.js"></script>
    <script type="text/javascript" src="lib/limitless/assets/js/core/libraries/bootstrap.min.js"></script>
    <script type="text/javascript" src="lib/limitless/assets/js/plugins/loaders/blockui.min.js"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="lib/limitless/assets/js/plugins/forms/styling/uniform.min.js"></script>

    <script type="text/javascript" src="lib/limitless/assets/js/plugins/notifications/pnotify.min.js"></script>
    <script type="text/javascript" src="lib/limitless/assets/js/pages/components_notifications_pnotify.js"></script>

    <!-- /theme JS files -->
</head>
<body>
    <form id="form1" runat="server">
    <div>        
        <asp:Label runat="server" ID="lblMessage"></asp:Label>
    </div>
    </form>
</body>
</html>
