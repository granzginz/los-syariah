﻿
<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ApprovalScheme.aspx.vb"
    Inherits="Maxiloan.Webform.Workflow.ApprovalScheme" %>
    <%@ Register tagname="ucNumberFormat" tagprefix="uc1"     src="../webform.UserController/ucNumberFormat.ascx" %>



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ApprovalScheme</title>
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <input id="hdnApplication" type="hidden" name="hdnApplication" runat="server" />
    <input id="hdnIsLimit" type="hidden" name="hdnIsLimit" runat="server" />
    <asp:Label ID="lblMessage" runat="server" ForeColor="Red" EnableViewState="False"></asp:Label>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    SKEMA APPROVAL</h3>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgScheme" runat="server" OnSortCommand="sortgrid" AutoGenerateColumns="False"
                        DataKeyField="ApprovalSchemeID" AllowSorting="True" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <FooterStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="ID SKEMA APPROVAL" SortExpression="ApprovalSchemeID">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="20%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:LinkButton ID="dtgApprovalSchemeID" runat="server" CausesValidation="False"
                                        Text='<%#container.dataitem("ApprovalSchemeID")%>' CommandName="ShowView">
                                    </asp:LinkButton>
                                    <asp:Label ID="dtgApprovalType" runat="server" Visible="False" Text='<%#container.dataitem("ApprovalTypeID")%>'>
                                    </asp:Label>
                                    <asp:Label ID="dtgIsLimitNeeded" runat="server" Visible="False" Text='<%#container.dataitem("IsLimitNeeded")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="NAMA SKEMA APPROVAL" SortExpression="ApprovalSchemeName">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="60%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="dtgApprovalName" runat="server" Text='<%#container.dataitem("ApprovalSchemeName")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="MEMBER">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="5%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="imbMember" runat="server" causesvalidation="False" Text="Member"
                                        commandname="EDIT"></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                        <PagerStyle Visible="False"></PagerStyle>
                    </asp:DataGrid>
                    <div class="button_gridnavigation">
                        <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="First" ImageUrl="../Images/grid_navbutton01.png"></asp:ImageButton>
                        <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="Prev" ImageUrl="../Images/grid_navbutton02.png"></asp:ImageButton>
                        <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="Next" ImageUrl="../Images/grid_navbutton03.png"></asp:ImageButton>
                        <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="Last" ImageUrl="../Images/grid_navbutton04.png"></asp:ImageButton>
                        Page&nbsp;
                        <asp:TextBox ID="txtPage" runat="server"  Width="34px">1</asp:TextBox>
                        <asp:Button ID="btnGoPage" runat="server" EnableViewState="False" Text="Go" CssClass="small buttongo blue">
                        </asp:Button>
                        <asp:RangeValidator ID="rgvGo" runat="server"  Display="Dynamic"
                         Type="Integer" MaximumValue="999999999" ErrorMessage="No Halaman Salah"
                            MinimumValue="1" ControlToValidate="txtpage" CssClass="validator_general"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="rfvGo" runat="server" 
                        CssClass="validator_general" ErrorMessage="No Halaman Salah" ControlToValidate="txtpage"
                            Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>
                    <div class="label_gridnavigation">
                     <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                    <asp:Label ID="lblrecord" runat="server"></asp:Label>&nbsp;record(s)
                    </div>
                </div>
            </div>
        </div>       
    </asp:Panel>
    <asp:Panel ID="pnlView" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    View Master Type Scheme</h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Approval Type</label>
                <asp:Label ID="lblApprType" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Approval ID
                </label>
                <asp:Label ID="lblApprID" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Approval Name
                </label>
                <asp:Label ID="lblApprNm" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Is Staging
                </label>
                <asp:Label ID="lblIsStaging" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Is Limited Needed
                </label>
                <asp:Label ID="lblIsLimited" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Transaction Label
                </label>
                <asp:Label ID="lblLabelTrans" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Transaction Address
                </label>
                <asp:Label ID="lblAddrTrans" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Optional Link Label 1
                </label>
                <asp:Label ID="lblLabelOptional1" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Optional&nbsp;Address 1
                </label>
                <asp:Label ID="lblAddrOptional1" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Optional SQL Command 1
                </label>
                <asp:Label ID="lblSQLOptional1" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Optional Link Label 2
                </label>
                <asp:Label ID="lblLabelOptional2" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Optional&nbsp;Address 2
                </label>
                <asp:Label ID="lblAddrOptional2" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Optional SQL Command 2
                </label>
                <asp:Label ID="lblSQLOptional2" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Note Label
                </label>
                <asp:Label ID="lblLabelNote" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Note SQL Command
                </label>
                <asp:Label ID="lblSQLNote" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Limit Label
                </label>
                <asp:Label ID="lblLabelLimit" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Link Label 1
                </label>
                <asp:Label ID="lblLabelLink1" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Address Label 1
                </label>
                <asp:Label ID="lblAddrLink1" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Link Label 2
                </label>
                <asp:Label ID="lblLabelLink2" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Address Label 2
                </label>
                <asp:Label ID="lblAddrLink2" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Link Label 3
                </label>
                <asp:Label ID="lblLabelLink3" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Address Label 3
                </label>
                <asp:Label ID="lblAddrLink3" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Link Label 4
                </label>
                <asp:Label ID="lblLabelLink4" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Address Label 4
                </label>
                <asp:Label ID="lblAddrLink4" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Link Label 5
                </label>
                <asp:Label ID="lblLabelLink5" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Address Label 5
                </label>
                <asp:Label ID="lblAddrLink5" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnCancelView" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button  gray"></asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
