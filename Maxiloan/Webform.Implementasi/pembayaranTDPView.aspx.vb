﻿Imports Maxiloan.General
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController

Public Class pembayaranTDPView
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oSearBy As UcSearchBy
    Protected WithEvents GridNavigator As ucGridNav
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private m_controller As New DataUserControlController
    Protected WithEvents oBranch As ucBranchAll
    Protected WithEvents oSearchBy As UcSearchBy


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.FormID = "BYRTDPView"
        If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
            lblMessage.Visible = False
            If SessionInvalid() Then
                Exit Sub
            End If
            AddHandler GridNavigator.PageChanged, AddressOf PageNavigation

            If Not IsPostBack Then
                If Request.QueryString("message") <> "" And Request.QueryString("msg") = "success" Then
                    ShowMessage(lblMessage, Request.QueryString("message"), False)
                End If

                oSearBy.ListData = "Name, Customer Name-ApplicationID,Application ID-Agreementno, Agreement No"
                oSearBy.BindData()

                If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                    Me.SearchBy = ""
                    Me.SortBy = ""
                End If
            End If
        End If
    End Sub

    Protected Function BranchIDChange() As String
        Return "ParentChange('" & Trim(oBranch.BranchID) & "'),((this.selectedIndex == -1) ? null : ListData[this.selectedIndex]));"
    End Function

    Protected Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        currentPage = e.CurrentPage
        DoBind(True)
        GridNavigator.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
    End Sub

    Sub DoBind(Optional isFrNav As Boolean = False)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView

        Dim oPaging As New Parameter.GeneralPaging
        Dim m_controller As New GeneralPagingController

        With oPaging
            .strConnection = GetConnectionString()
            .WhereCond = Me.SearchBy
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
            .SpName = "spAgreementTDPViewList"
        End With

        oPaging = m_controller.GetGeneralPaging(oPaging)
        If Not oPaging Is Nothing Then
            DtUserList = oPaging.ListData
            recordCount = oPaging.TotalRecords
        Else
            recordCount = 0
        End If
        DtgAsset.DataSource = DtUserList.DefaultView
        DtgAsset.CurrentPageIndex = 0
        DtgAsset.DataBind()
        If (isFrNav = False) Then
            GridNavigator.Initialize(recordCount, pageSize)
        End If
        pnlList.Visible = True
        pnlDatagrid.Visible = True
    End Sub
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        Me.SortBy = String.Format("{0} {1}", e.SortExpression, IIf(InStr(Me.SortBy, "DESC") > 0, "", "DESC"))
        DoBind()
    End Sub
    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Me.SearchBy = " branchid = '" & oBranch.BranchID.Trim & "'"
        Response.Redirect("pembayaranTDPView.aspx")
    End Sub

    Private Sub btnsearch_Click(sender As Object, e As EventArgs) Handles btnsearch.Click
        If SessionInvalid() Then
            Exit Sub
        End If
        Dim bNoAND As Boolean
        bNoAND = True
        Me.SearchBy = ""
        Dim strSearch As New StringBuilder
        If (oBranch.BranchID.Trim <> "0" And oBranch.BranchID.Trim <> "ALL") Then
            Me.SearchBy = " and a.branchid = " & "'" & oBranch.BranchID.Trim & "'"
            bNoAND = False
        End If
        If oSearBy.Text.Trim <> "" Then
            strSearch.Append(" and " & oSearBy.ValueID & " like  '%" & oSearBy.Text.Trim.Replace("'", "''") & "%'")
        End If

        If oSearBy.Text.Trim <> "" Then
            Me.SearchBy = Me.SearchBy & " and " & oSearBy.ValueID & " like '%" & oSearBy.Text.Trim.Replace("'", "''").Replace("%", "") & "%'"
        End If
        pnlDatagrid.Visible = True
        DtgAsset.Visible = True
        pnlList.Visible = True
        DoBind()
    End Sub

    Private Sub DtgAsset_ItemDataBound(sender As Object, e As DataGridItemEventArgs) Handles DtgAsset.ItemDataBound
        If SessionInvalid() Then
            Exit Sub
        End If
        Dim Hyterima As HyperLink
        Dim hyTemp As HyperLink
        Dim HyappId As HyperLink
        Dim lblTemp As New Label

        If e.Item.ItemIndex >= 0 Then
            HyappId = CType(e.Item.FindControl("hyApplicationId"), HyperLink)

            Hyterima = CType(e.Item.FindControl("Hyterima"), HyperLink)
            Hyterima.NavigateUrl = "TerimaPembayaranTDP.aspx?Applicationid=" & HyappId.Text.Trim

            lblTemp = CType(e.Item.FindControl("lblCustomerId"), Label)
            hyTemp = CType(e.Item.FindControl("hyCustomerName"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenCustomer('" & "AssetDocument" & "', '" & Server.UrlEncode(lblTemp.Text.Trim) & "')"

            hyTemp = CType(e.Item.FindControl("hyAgreementNo"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenAgreementNo('" & "AssetDocument" & "', '" & Server.UrlEncode(HyappId.Text.Trim) & "')"

            hyTemp = CType(e.Item.FindControl("hyApplicationId"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenApplicationId('" & "AssetDocument" & "', '" & Server.UrlEncode(hyTemp.Text.Trim) & "')"
        End If
    End Sub
End Class