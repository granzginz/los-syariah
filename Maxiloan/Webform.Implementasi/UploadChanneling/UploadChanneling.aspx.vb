﻿#Region "Imports"
Imports Maxiloan.Controller
Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.Webform.UserController
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data.OleDb
Imports System.Data.Odbc
Imports System.Text
Imports System.Data.SqlClient
Imports System.IO
Imports System.Web.UI.WebControls.FileUpload
Imports System.Text.RegularExpressions
Imports System.Diagnostics
Imports System.Drawing
Imports System.Runtime.InteropServices
Imports DocumentFormat.OpenXml.Packaging
Imports DocumentFormat.OpenXml.Spreadsheet
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Parameter

#End Region

Public Class UploadChanneling
    Inherits Maxiloan.Webform.WebBased

    Private myCmd As New SqlCommand
    Private myAdapter As New SqlDataAdapter
    Private myDS As New DataSet
    Private myReader As DataTableReader
    Private FileName As String
    Private strFileTemp As String
    Private strFile As String
    Private _NoKontrak As String
    Private FileDirectory As String
    Private TotalAgreement As Double
    Private TotalAmountReceive As Decimal
    Private FileType As String
    Private _TglTransaksi As String
    Private _NoRekening As String
    Private _BranchID As String
    Private _AgreementNo As String
    Private objCon As SqlConnection
    Private TextFile As String
    Private ocustomclass As Parameter.UploadChanneling
    Protected Const PARAM_HASIL As String = "@TransactionNo"

    Public Property FilaName() As String
        Get
            Return CType(ViewState("FilaName"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("FilaName") = Value
        End Set
    End Property
    Public Property TransactionNo() As String
        Get
            Return CType(ViewState("TransactionNo"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("TransactionNo") = Value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblLoading.Visible = False
        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not Me.IsPostBack Then

            Me.FormID = "UploadChanneling"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) = False Then
                Exit Sub
            End If
        End If
    End Sub
    Private Sub btnUpload_Click(sender As Object, e As System.EventArgs) Handles btnUpload.Click
        lblLoading.Visible = True
        btnUpload.Enabled = False
        Try
            If FileVA.HasFile Then
                strFileTemp = FileVA.FileName
                FileName = Context.Request.MapPath("~\\xml\\") + strFileTemp
                File.Delete(FileName)
                FileVA.SaveAs(FileName)
                Dim larr As Array = CType(strFileTemp.ToString.Split(CChar(".")), Array)
                TextFile = strFileTemp
                FileDirectory = Context.Request.MapPath("~\\xml\\").ToString.Trim
                FileType = larr.GetValue(1).ToString.Trim()

                Me.FileName = FileName
                DoProses()

            Else
                lblLoading.Visible = False
                ShowMessage(lblMessage, "File belum dipilih!", True)
                Exit Sub
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try

        btnUpload.Enabled = True
        lblLoading.Visible = False
    End Sub
    Function ValidasiFile() As Boolean
        Dim result As Boolean = True
        objCon = New SqlConnection(GetConnectionString)
        Try
            myDS = New DataSet
            myCmd.Parameters.Clear()
            myCmd.CommandText = "spGetUploadChannelingValidFile"
            myCmd.CommandType = CommandType.StoredProcedure
            myCmd.Parameters.Add(New SqlParameter("@FileName", SqlDbType.VarChar, 200) With {.Value = strFileTemp.Trim})
            myCmd.Connection = objCon
            myAdapter.SelectCommand = myCmd
            myAdapter.Fill(myDS)

            If myDS.Tables(0).Rows.Count > 0 Then
                result = False
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message)
            ShowMessage(lblMessage, ex.Message, True)
        End Try
        Return result
    End Function
    'Private Function Text_To_DataTable(ByVal path As String, ByVal delimitter As Char, ByVal header As Boolean) As DataTable
    '    Dim source As String = String.Empty
    '    Dim dt As DataTable = New DataTable

    '    If IO.File.Exists(path) Then
    '        source = IO.File.ReadAllText(path)
    '    Else
    '        Throw New IO.FileNotFoundException("Could not find the file at " & path, path)
    '    End If

    '    Dim rows() As String = source.Split({Environment.NewLine}, StringSplitOptions.None)

    '    For i As Integer = 0 To rows(0).Split(delimitter).Length - 1
    '        Dim column As String = rows(0).Split(delimitter)(i)
    '        dt.Columns.Add(If(header, column, "column" & i + 1))
    '    Next

    '    For i As Integer = If(header, 1, 0) To rows.Length - 1
    '        Dim dr As DataRow = dt.NewRow

    '        For x As Integer = 0 To rows(i).Split(delimitter).Length - 1
    '            If x <= dt.Columns.Count - 1 Then
    '                dr(x) = rows(i).Split(delimitter)(x)
    '            Else
    '                Throw New Exception("The number of columns on row " & i + If(header, 0, 1) & " is greater than the amount of columns in the " & If(header, "header.", "first row."))
    '            End If
    '        Next

    '        dt.Rows.Add(dr)
    '    Next

    '    Return dt
    'End Function

    'Private Function Text_To_DataTable_2(ByVal path As String, ByVal delimitter As Char, ByVal header As Boolean) As DataTable
    '    Dim source As String = String.Empty
    '    Dim dt As DataTable = New DataTable

    '    If IO.File.Exists(path) Then
    '        source = IO.File.ReadAllText(path)
    '    Else
    '        Throw New IO.FileNotFoundException("Could not find the file at " & path, path)
    '    End If

    '    Dim rows() As String = source.Split({Environment.NewLine}, StringSplitOptions.None)

    '    For i As Integer = 0 To rows(0).Split(delimitter).Length - 1
    '        Dim column As String = rows(0).Split(delimitter)(i)
    '        dt.Columns.Add(If(header, column, "column" & i + 1))
    '    Next

    '    For i As Integer = 0 To rows.Length - 1
    '        Dim dr As DataRow = dt.NewRow

    '        For x As Integer = 0 To rows(i).Split(delimitter).Length - 1
    '            If x <= dt.Columns.Count - 1 Then
    '                Dim str As String = rows(i).Split(delimitter)(x).Trim

    '                Try
    '                    Dim number As Decimal
    '                    number = Convert.ToDecimal(str.Substring(3, 20))
    '                    dr(x) = str
    '                    dt.Rows.Add(dr)
    '                Catch ex As Exception
    '                    Continue For
    '                End Try
    '            Else
    '                Throw New Exception("The number of columns on row " & i + If(header, 0, 1) & " is greater than the amount of columns in the " & If(header, "header.", "first row."))
    '            End If
    '        Next
    '    Next

    '    Return dt
    'End Function

    Private Function createDataUpload() As DataTable
        Dim dt As New DataTable

        'dt.Columns.Add("ValutaDate")
        'dt.Columns.Add("FileDirectory")
        'dt.Columns.Add("TotalAgreement")
        'dt.Columns.Add("TotalAmountReceive")
        'dt.Columns.Add("sequenceno")

        'dt.Columns.Add("WaktuTransaksi")
        'dt.Columns.Add("AgreementNo")
        'dt.Columns.Add("CustomerName")
        'dt.Columns.Add("AmountReceive")
        'dt.Columns.Add("JournalNo")
        'dt.Columns.Add("StatusLog")
        'dt.Columns.Add("Keterangan")

        dt.Columns.Add("JENIS_NASABAH")
        dt.Columns.Add("NAMA_PANGGILAN")
        dt.Columns.Add("NAMA_DEPAN")
        dt.Columns.Add("NAMA_BELAKANG")
        dt.Columns.Add("NAMA_PERUSAHAAN")
        dt.Columns.Add("ALAMAT_JALAN")
        dt.Columns.Add("ALAMAT_RT_PERUM")
        dt.Columns.Add("ALAMAT_KELURAHAN")
        dt.Columns.Add("ALAMAT_KECAMATAN")
        dt.Columns.Add("KODE_POS")
        dt.Columns.Add("NO_TELEPON")
        dt.Columns.Add("KODE_LOKASI")
        dt.Columns.Add("KODE_GROUP")
        dt.Columns.Add("JENIS_IDENTITAS")
        dt.Columns.Add("NO_IDENTITAS")
        dt.Columns.Add("LOKASI_PENERBITAN_ID")
        dt.Columns.Add("TANGGAL_EXPIRE_ID")
        dt.Columns.Add("NPWP")
        dt.Columns.Add("NAMA_GADIS")
        dt.Columns.Add("KODE_AGAMA")
        dt.Columns.Add("TANGGAL_LAHIR")
        dt.Columns.Add("TEMPAT_LAHIR")
        dt.Columns.Add("STATUS_PERKAWINAN")
        dt.Columns.Add("JENIS_KELAMIN")
        dt.Columns.Add("NO_PK")
        dt.Columns.Add("MAXIMUM_KREDIT")
        dt.Columns.Add("JANGKA_WAKTU")
        dt.Columns.Add("PERUNTUKAN")
        dt.Columns.Add("NO_REKENING")
        dt.Columns.Add("JENIS_JAMINAN")
        dt.Columns.Add("DESKRIPSI_PINJAMAN")
        dt.Columns.Add("NILAI_TAKSASI")
        dt.Columns.Add("NILAI_APPRAISAL")
        dt.Columns.Add("TANGGAL_APPRAISAL")
        dt.Columns.Add("JENIS_HUKUM")
        dt.Columns.Add("NO_REF_FEO")
        dt.Columns.Add("NOMINAL_PENGIKATAN")
        dt.Columns.Add("RATE_BUNGA")
        dt.Columns.Add("NO_CIF")
        dt.Columns.Add("ANNIVERSARY_DATE")
        dt.Columns.Add("PEMBERI_KERJA")
        dt.Columns.Add("KODE_PEKERJAAN")
        dt.Columns.Add("REK_TUJUAN_PENCAIRAN")
        dt.Columns.Add("NAMA_PEMILIK_AGUNAN")
        dt.Columns.Add("BUKTI_KEPEMILIKAN_AGUNAN")
        dt.Columns.Add("ALAMAT_1_AGUNAN")
        dt.Columns.Add("ALAMAT_2_AGUNAN")
        dt.Columns.Add("INCREMENT_RATE")
        dt.Columns.Add("INCREMENT_SIGN")
        dt.Columns.Add("TUNGGAKAN_POKOK")
        dt.Columns.Add("TUNGGAKAN_BUNGA")
        dt.Columns.Add("LAST_ARR_DATE")
        dt.Columns.Add("FIXED_RATE_DATE")
        dt.Columns.Add("NO_APLIKASI")
        dt.Columns.Add("NOREK_FEE")
        dt.Columns.Add("NAMA_ALIAS")
        dt.Columns.Add("ALMT_PEMBERI_KERJA_1")
        dt.Columns.Add("ALMT_PEMBERI_KERJA_2")
        dt.Columns.Add("DESKRIPSI_PEKERJAAN")
        dt.Columns.Add("PENGHASILAN")
        dt.Columns.Add("FREK_PENGHASILAN")
        dt.Columns.Add("ALAMAT_JALAN_TERKINI")
        dt.Columns.Add("ALAMAT_RT_PERUM_TERKINI")
        dt.Columns.Add("ALAMAT_KELURAHAN_TERKINI")
        dt.Columns.Add("ALAMAT_KECAMATAN_TERKINI")
        dt.Columns.Add("KODE_POS_TERKINI")
        dt.Columns.Add("NO_TLPN_RMH_TERKINI")
        dt.Columns.Add("NO_TLPN_HP_TERKINI")
        dt.Columns.Add("TANGGAL_BERLAKU")
        dt.Columns.Add("TANGGAL_JATUH_TEMPO")
        dt.Columns.Add("LOKASI_BI_TERKINI")
        dt.Columns.Add("KODE_PENDIDIKAN")
        dt.Columns.Add("NAMA_TENGAH")
        dt.Columns.Add("TANGGAL_MULAI_GRACE_PERIOD")
        dt.Columns.Add("JENIS_GRACE_PERIOD")
        dt.Columns.Add("JANGKA_WAKTU_GRACE_PERIOD")
        dt.Columns.Add("IND_SURAT_MENYURAT")
        dt.Columns.Add("IND_PEMBERITAHUAN")
        dt.Columns.Add("KATEGORI_PORTFOLIO")
        dt.Columns.Add("KATEGORI_DEBITUR")
        dt.Columns.Add("JNS_FASILITAS_KREDIT")
        dt.Columns.Add("SALES_CODE")
        dt.Columns.Add("OVERRIDE_MESSAGE")
        dt.Columns.Add("BranchID")
        dt.Columns.Add("ProductID")
        dt.Columns.Add("ProductOfferingID")
        dt.Columns.Add("FacilityNo")
        dt.Columns.Add("BatchNo")
        dt.Columns.Add("AssetCode")
        dt.Columns.Add("SupplierID")
        dt.Columns.Add("ProvisiDealer")
        dt.Columns.Add("ProvisiBNIMF")
        dt.Columns.Add("AdminFeeDealer")
        dt.Columns.Add("AdminFeeBNIMF")
        dt.Columns.Add("UploadDate")
        dt.Columns.Add("strFileTemp")

        Return dt
    End Function
    'modify Nofi 2018-10-24
    Private Sub DoProses()
        Dim SequenceNo As Integer = 0
        Dim list As New List(Of Parameter.UploadChanneling)
        If ValidasiFile() Then
            list = UploadFile()
        Else
            ShowMessage(lblMessage, "File pernah di upload!", True)
            Exit Sub
        End If
        Try
            Dim dt As DataTable = createDataUpload()
            TotalAgreement = list.Count
            If list.Count > 0 Then
                For index = 0 To list.Count - 1
                    Dim Upload As New Parameter.UploadChanneling
                    Upload = list(index)
                    Upload.UploadDate = BusinessDate
                    Upload.strFileTemp = strFileTemp.Trim
                    'dt.Rows.Add(payment.UploadDate, payment.TglTransaksi, strFileTemp, FileDirectory, TotalAgreement, TotalAmountReceive, SequenceNo, payment.WaktuTransaksi, payment.NoKontrak,
                    '            payment.NamaNasabah, payment.NilaiTransaksi, payment.NoJournal, payment.Post, payment.Keterangan)
                    dt.Rows.Add(Upload.JENIS_NASABAH, Upload.NAMA_PANGGILAN, Upload.NAMA_DEPAN, Upload.NAMA_BELAKANG, Upload.NAMA_PERUSAHAAN, Upload.ALAMAT_JALAN,
                                Upload.ALAMAT_RT_PERUM, Upload.ALAMAT_KELURAHAN, Upload.ALAMAT_KECAMATAN, Upload.KODE_POS, Upload.NO_TELEPON, Upload.KODE_LOKASI, Upload.KODE_GROUP, Upload.JENIS_IDENTITAS,
                                Upload.NO_IDENTITAS, Upload.LOKASI_PENERBITAN_ID, Upload.TANGGAL_EXPIRE_ID, Upload.NPWP, Upload.NAMA_GADIS, Upload.KODE_AGAMA, Upload.TANGGAL_LAHIR, Upload.TEMPAT_LAHIR,
                                Upload.STATUS_PERKAWINAN, Upload.JENIS_KELAMIN, Upload.NO_PK, Upload.MAXIMUM_KREDIT, Upload.JANGKA_WAKTU, Upload.PERUNTUKAN, Upload.NO_REKENING, Upload.JENIS_JAMINAN, Upload.DESKRIPSI_PINJAMAN,
                                Upload.NILAI_TAKSASI, Upload.NILAI_APPRAISAL, Upload.TANGGAL_APPRAISAL, Upload.JENIS_HUKUM, Upload.NO_REF_FEO, Upload.NOMINAL_PENGIKATAN, Upload.RATE_BUNGA, Upload.NO_CIF, Upload.ANNIVERSARY_DATE,
                                Upload.PEMBERI_KERJA, Upload.KODE_PEKERJAAN, Upload.REK_TUJUAN_PENCAIRAN, Upload.NAMA_PEMILIK_AGUNAN, Upload.BUKTI_KEPEMILIKAN_AGUNAN, Upload.ALAMAT_1_AGUNAN, Upload.ALAMAT_2_AGUNAN, Upload.INCREMENT_RATE,
                                Upload.INCREMENT_SIGN, Upload.TUNGGAKAN_POKOK, Upload.TUNGGAKAN_BUNGA, Upload.LAST_ARR_DATE, Upload.FIXED_RATE_DATE, Upload.NO_APLIKASI, Upload.NOREK_FEE, Upload.NAMA_ALIAS, Upload.ALMT_PEMBERI_KERJA_1,
                                Upload.ALMT_PEMBERI_KERJA_2, Upload.DESKRIPSI_PEKERJAAN, Upload.PENGHASILAN, Upload.FREK_PENGHASILAN, Upload.ALAMAT_JALAN_TERKINI, Upload.ALAMAT_RT_PERUM_TERKINI, Upload.ALAMAT_KELURAHAN_TERKINI,
                                Upload.ALAMAT_KECAMATAN_TERKINI, Upload.KODE_POS_TERKINI, Upload.NO_TLPN_RMH_TERKINI, Upload.NO_TLPN_HP_TERKINI, Upload.TANGGAL_BERLAKU, Upload.TANGGAL_JATUH_TEMPO, Upload.LOKASI_BI_TERKINI,
                                Upload.KODE_PENDIDIKAN, Upload.NAMA_TENGAH, Upload.TANGGAL_MULAI_GRACE_PERIOD, Upload.JENIS_GRACE_PERIOD, Upload.JANGKA_WAKTU_GRACE_PERIOD, Upload.IND_SURAT_MENYURAT, Upload.IND_PEMBERITAHUAN,
                                Upload.KATEGORI_PORTFOLIO, Upload.KATEGORI_DEBITUR, Upload.JNS_FASILITAS_KREDIT, Upload.SALES_CODE, Upload.OVERRIDE_MESSAGE, Upload.BranchID, Upload.ProductID, Upload.ProductOfferingID, Upload.FacilityNo,
                                Upload.BatchNo, Upload.AssetCode, Upload.SupplierID, Upload.ProvisiDealer, Upload.ProvisiBNIMF, Upload.AdminFeeDealer, Upload.AdminFeeBNIMF, Upload.UploadDate,
                                Upload.strFileTemp)
                Next
            End If

            UploadAngsuranByBatchSave(dt)
            ShowMessage(lblMessage, "Upload Channeling Berhasil", False)

        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub


    Sub UploadAngsuranByBatchSave(ByRef udtTable As DataTable)
        'Try
        '    objCon = New SqlConnection(GetConnectionString)
        '    myCmd.Parameters.Clear()
        '    myCmd.CommandText = "SPUploadChannelingSave"
        '    myCmd.CommandType = CommandType.StoredProcedure
        '    myCmd.Parameters.Add(New SqlParameter("@udtTable", SqlDbType.Structured) With {.Value = udtTable})
        '    myCmd.Parameters.Add(New SqlParameter("@Cabang", SqlDbType.Char, 3) With {.Value = sesBranchId.Replace("'", "")})

        '    objCon.Open()
        '    myCmd.Connection = objCon
        '    myCmd.CommandTimeout = 0
        '    myCmd.ExecuteNonQuery()
        '    objCon.Close()

        Dim params() As SqlParameter = New SqlParameter(2) {}

        'With ocustomclass
        '    .strConnection = GetConnectionString()
        'End With

        params(0) = New SqlParameter("@udtTable", SqlDbType.Structured)
        params(0).Value = udtTable
        params(1) = New SqlParameter("@Cabang", SqlDbType.Char, 3)
        params(1).Value = Replace(Me.sesBranchId, "'", "")
        params(2) = New SqlParameter(PARAM_HASIL, SqlDbType.VarChar, 25)
        params(2).Direction = ParameterDirection.Output
        Try
            SqlHelper.ExecuteScalar(GetConnectionString, CommandType.StoredProcedure, "SPUploadChannelingSave", params).ToString()
            Me.TransactionNo = params(2).Value
            Dim oCustomClass As New Parameter.CreditScoring With {
                  .strConnection = GetConnectionString().Trim,
                  .ApplicationID = Me.TransactionNo
              }
            Dim oEntitiesApproval As New Parameter.Approval
            With oEntitiesApproval
                .BranchId = Me.sesBranchId.Replace("'", "")
                .SchemeID = "CNL"
                .RequestDate = Me.BusinessDate
                .ApprovalNote = ""
                .ApprovalValue = TotalAmountReceive
                .UserRequest = Me.Loginid
                .UserApproval = ""
                .AprovalType = Approval.ETransactionType.AmortisasiBiaya_Approval
                '.AprovalType = SCHEME_ID
                .Argumentasi = ""
                .TransactionNo = Me.TransactionNo
            End With

            Dim oController As New CreditScoringController
            oCustomClass.Approval = oEntitiesApproval
            Dim resultapp = oController.GetApprovalCreditScoring(oCustomClass, "R")

        Catch ex As Exception
            Throw New Exception(ex.Message)
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub


    Function UploadFile() As List(Of Parameter.UploadChanneling)
        Dim list As New List(Of Parameter.UploadChanneling)
        Dim data As New DataTable
        Dim NoRekening As String = ""
        Dim TglTransaksi As Date = Nothing

        Try
            'data = CSVToDataTable(Me.FileName)--> kalau pakai csv
            data = FileToDataTable()

            For index = 0 To data.Rows.Count - 1
                If (index >= 0) Then
                    Dim Upload As New Parameter.UploadChanneling

                    'Payment.TglTransaksi = ConvertDate2(IIf(data.Rows(index).Item(1).ToString().Trim = "", "01/01/1900", data.Rows(index).Item(1).ToString().Trim))
                    'Payment.WaktuTransaksi = Now.Hour.ToString("00") & ":" & Now.Minute.ToString("00") & ":" & Now.Second.ToString("00")
                    'Payment.NoKontrak = data.Rows(index).Item(2).ToString()
                    'Payment.NamaNasabah = data.Rows(index).Item(3).ToString()
                    'Payment.NilaiTransaksi = data.Rows(index).Item(4).ToString()
                    'Payment.NoJournal = data.Rows(index).Item(5).ToString()
                    'Payment.Keterangan = data.Rows(index).Item(6).ToString()
                    'Payment.Post = "N"

                    Upload.JENIS_NASABAH = data.Rows(index).Item(0).ToString()
                    Upload.NAMA_PANGGILAN = IIf(data.Rows(index).Item(1).ToString().Trim = "", "", data.Rows(index).Item(1).ToString().Trim)
                    Upload.NAMA_DEPAN = data.Rows(index).Item(2).ToString()
                    Upload.NAMA_BELAKANG = data.Rows(index).Item(3).ToString()
                    Upload.NAMA_PERUSAHAAN = data.Rows(index).Item(4).ToString()
                    Upload.ALAMAT_JALAN = data.Rows(index).Item(5).ToString()
                    Upload.ALAMAT_RT_PERUM = data.Rows(index).Item(6).ToString()
                    Upload.ALAMAT_KELURAHAN = data.Rows(index).Item(7).ToString()
                    Upload.ALAMAT_KECAMATAN = data.Rows(index).Item(8).ToString()
                    Upload.KODE_POS = data.Rows(index).Item(9).ToString()
                    Upload.NO_TELEPON = data.Rows(index).Item(10).ToString()
                    Upload.KODE_LOKASI = data.Rows(index).Item(11).ToString()
                    Upload.KODE_GROUP = data.Rows(index).Item(12).ToString()
                    Upload.JENIS_IDENTITAS = data.Rows(index).Item(13).ToString()
                    Upload.NO_IDENTITAS = data.Rows(index).Item(14).ToString()
                    Upload.LOKASI_PENERBITAN_ID = data.Rows(index).Item(15).ToString()
                    Upload.TANGGAL_EXPIRE_ID = ConvertDate2(IIf(data.Rows(index).Item(16).ToString().Trim = "", "01/01/1900", data.Rows(index).Item(16).ToString().Trim))
                    Upload.NPWP = data.Rows(index).Item(17).ToString()
                    Upload.NAMA_GADIS = data.Rows(index).Item(18).ToString()
                    Upload.KODE_AGAMA = data.Rows(index).Item(19).ToString()
                    Upload.TANGGAL_LAHIR = ConvertDate2(IIf(data.Rows(index).Item(20).ToString().Trim = "", "01/01/1900", data.Rows(index).Item(20).ToString().Trim))
                    Upload.TEMPAT_LAHIR = data.Rows(index).Item(21).ToString()
                    Upload.STATUS_PERKAWINAN = data.Rows(index).Item(22).ToString()
                    Upload.JENIS_KELAMIN = data.Rows(index).Item(23).ToString()
                    Upload.NO_PK = data.Rows(index).Item(24).ToString()
                    Upload.MAXIMUM_KREDIT = CDbl(IIf(data.Rows(index).Item(25).ToString() = "", 0, data.Rows(index).Item(25).ToString()))
                    Upload.JANGKA_WAKTU = data.Rows(index).Item(26).ToString()
                    Upload.PERUNTUKAN = data.Rows(index).Item(27).ToString()
                    Upload.NO_REKENING = data.Rows(index).Item(28).ToString()
                    Upload.JENIS_JAMINAN = data.Rows(index).Item(29).ToString()
                    Upload.DESKRIPSI_PINJAMAN = data.Rows(index).Item(30).ToString()
                    Upload.NILAI_TAKSASI = CDbl(IIf(data.Rows(index).Item(31).ToString() = "", 0, data.Rows(index).Item(31).ToString()))
                    Upload.NILAI_APPRAISAL = CDbl(IIf(data.Rows(index).Item(32).ToString() = "", 0, data.Rows(index).Item(32).ToString()))
                    Upload.TANGGAL_APPRAISAL = ConvertDate2(IIf(data.Rows(index).Item(33).ToString().Trim = "", "01/01/1900", data.Rows(index).Item(33).ToString().Trim))
                    Upload.JENIS_HUKUM = data.Rows(index).Item(34).ToString()
                    Upload.NO_REF_FEO = data.Rows(index).Item(35).ToString()
                    Upload.NOMINAL_PENGIKATAN = CDbl(IIf(data.Rows(index).Item(36).ToString() = "", 0, data.Rows(index).Item(36).ToString()))
                    Upload.RATE_BUNGA = CDbl(data.Rows(index).Item(37).ToString())
                    Upload.NO_CIF = data.Rows(index).Item(38).ToString()
                    Upload.ANNIVERSARY_DATE = ConvertDate2(IIf(data.Rows(index).Item(39).ToString().Trim = "", "01/01/1900", data.Rows(index).Item(39).ToString().Trim))
                    Upload.PEMBERI_KERJA = data.Rows(index).Item(40).ToString()
                    Upload.KODE_PEKERJAAN = data.Rows(index).Item(41).ToString()
                    Upload.REK_TUJUAN_PENCAIRAN = data.Rows(index).Item(42).ToString()
                    Upload.NAMA_PEMILIK_AGUNAN = data.Rows(index).Item(43).ToString()
                    Upload.BUKTI_KEPEMILIKAN_AGUNAN = data.Rows(index).Item(44).ToString()
                    Upload.ALAMAT_1_AGUNAN = data.Rows(index).Item(45).ToString()
                    Upload.ALAMAT_2_AGUNAN = data.Rows(index).Item(46).ToString()
                    Upload.INCREMENT_RATE = CDbl(data.Rows(index).Item(47).ToString())
                    Upload.INCREMENT_SIGN = data.Rows(index).Item(48).ToString()
                    Upload.TUNGGAKAN_POKOK = CDbl(data.Rows(index).Item(49).ToString())
                    Upload.TUNGGAKAN_BUNGA = CDbl(data.Rows(index).Item(50).ToString())
                    Upload.LAST_ARR_DATE = ConvertDate2(IIf(data.Rows(index).Item(51).ToString().Trim = "", "01/01/1900", data.Rows(index).Item(51).ToString().Trim))
                    Upload.FIXED_RATE_DATE = ConvertDate2(IIf(data.Rows(index).Item(52).ToString().Trim = "", "01/01/1900", data.Rows(index).Item(52).ToString().Trim))
                    Upload.NO_APLIKASI = data.Rows(index).Item(53).ToString()
                    Upload.NOREK_FEE = CDbl(data.Rows(index).Item(54).ToString())
                    Upload.NAMA_ALIAS = data.Rows(index).Item(55).ToString()
                    Upload.ALMT_PEMBERI_KERJA_1 = data.Rows(index).Item(56).ToString()
                    Upload.ALMT_PEMBERI_KERJA_2 = data.Rows(index).Item(57).ToString()
                    Upload.DESKRIPSI_PEKERJAAN = data.Rows(index).Item(58).ToString()
                    Upload.PENGHASILAN = CDbl(data.Rows(index).Item(59).ToString())
                    Upload.FREK_PENGHASILAN = data.Rows(index).Item(60).ToString()
                    Upload.ALAMAT_JALAN_TERKINI = data.Rows(index).Item(61).ToString()
                    Upload.ALAMAT_RT_PERUM_TERKINI = data.Rows(index).Item(62).ToString()
                    Upload.ALAMAT_KELURAHAN_TERKINI = data.Rows(index).Item(63).ToString()
                    Upload.ALAMAT_KECAMATAN_TERKINI = data.Rows(index).Item(64).ToString()
                    Upload.KODE_POS_TERKINI = data.Rows(index).Item(65).ToString()
                    Upload.NO_TLPN_RMH_TERKINI = data.Rows(index).Item(66).ToString()
                    Upload.NO_TLPN_HP_TERKINI = data.Rows(index).Item(67).ToString()
                    Upload.TANGGAL_BERLAKU = ConvertDate2(IIf(data.Rows(index).Item(68).ToString().Trim = "", "01/01/1900", data.Rows(index).Item(68).ToString().Trim))
                    Upload.TANGGAL_JATUH_TEMPO = ConvertDate2(IIf(data.Rows(index).Item(69).ToString().Trim = "", "01/01/1900", data.Rows(index).Item(69).ToString().Trim))
                    Upload.LOKASI_BI_TERKINI = data.Rows(index).Item(70).ToString()
                    Upload.KODE_PENDIDIKAN = data.Rows(index).Item(71).ToString()
                    Upload.NAMA_TENGAH = data.Rows(index).Item(72).ToString()
                    Upload.TANGGAL_MULAI_GRACE_PERIOD = ConvertDate2(IIf(data.Rows(index).Item(73).ToString().Trim = "", "01/01/1900", data.Rows(index).Item(73).ToString().Trim))
                    Upload.JENIS_GRACE_PERIOD = data.Rows(index).Item(74).ToString()
                    Upload.JANGKA_WAKTU_GRACE_PERIOD = data.Rows(index).Item(75).ToString()
                    Upload.IND_SURAT_MENYURAT = data.Rows(index).Item(76).ToString()
                    Upload.IND_PEMBERITAHUAN = data.Rows(index).Item(77).ToString()
                    Upload.KATEGORI_PORTFOLIO = data.Rows(index).Item(78).ToString()
                    Upload.KATEGORI_DEBITUR = data.Rows(index).Item(79).ToString()
                    Upload.JNS_FASILITAS_KREDIT = data.Rows(index).Item(80).ToString()
                    Upload.SALES_CODE = data.Rows(index).Item(81).ToString()
                    Upload.OVERRIDE_MESSAGE = data.Rows(index).Item(82).ToString()
                    Upload.BranchID = data.Rows(index).Item(83).ToString()
                    Upload.ProductID = data.Rows(index).Item(84).ToString()
                    Upload.ProductOfferingID = data.Rows(index).Item(85).ToString()
                    Upload.FacilityNo = data.Rows(index).Item(86).ToString()
                    Upload.BatchNo = data.Rows(index).Item(87).ToString()
                    Upload.AssetCode = data.Rows(index).Item(88).ToString()
                    Upload.SupplierID = data.Rows(index).Item(89).ToString()
                    Upload.ProvisiDealer = CDbl(IIf(data.Rows(index).Item(90).ToString() = "", 0, data.Rows(index).Item(90).ToString()))
                    Upload.ProvisiBNIMF = CDbl(IIf(data.Rows(index).Item(91).ToString() = "", 0, data.Rows(index).Item(91).ToString()))
                    Upload.AdminFeeDealer = CDbl(IIf(data.Rows(index).Item(92).ToString() = "", 0, data.Rows(index).Item(92).ToString()))
                    Upload.AdminFeeBNIMF = CDbl(IIf(data.Rows(index).Item(93).ToString() = "", 0, data.Rows(index).Item(93).ToString()))


                    Me._TglTransaksi = BusinessDate
                    Me._NoKontrak = Upload.NO_PK

                    Dim query As New Parameter.UploadChanneling
                    If list.Count > 0 Then
                        query = list.Find(AddressOf PredicateFunction)
                    Else
                        query = Nothing
                    End If

                    If query Is Nothing Then
                        TotalAmountReceive = Upload.MAXIMUM_KREDIT
                        list.Add(Upload)
                    End If
                End If

            Next
        Catch ex As Exception
            Throw New ApplicationException(ex.Message)
        End Try

        Return list
    End Function
    Public Function PredicateFunction(ByVal custom As Parameter.UploadChanneling) As Boolean
        Return custom.TglTransaksi = Me._TglTransaksi And custom.NoKontrak = Me._NoKontrak
    End Function

    'Sub SaveUploadAngsuran(ByVal payment As Parameter.UploadAngsuran)
    '    Try
    '        objCon = New SqlConnection(GetConnectionString)
    '        myCmd.Parameters.Clear()
    '        myCmd.CommandText = "spUploadChannelingHeaderSave"
    '        myCmd.CommandType = CommandType.StoredProcedure
    '        myCmd.Parameters.Add(New SqlParameter("@UploadDate", SqlDbType.DateTime) With {.Value = BusinessDate})
    '        myCmd.Parameters.Add(New SqlParameter("@FileName", SqlDbType.VarChar, 200) With {.Value = strFileTemp})
    '        myCmd.Parameters.Add(New SqlParameter("@TotalAgreement", SqlDbType.BigInt) With {.Value = TotalAgreement})
    '        myCmd.Parameters.Add(New SqlParameter("@TotalAmount", SqlDbType.Decimal) With {.Value = TotalAmountReceive})

    '        objCon.Open()
    '        myCmd.Connection = objCon
    '        myCmd.ExecuteNonQuery()
    '        objCon.Close()
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message)
    '        ShowMessage(lblMessage, ex.Message, True)
    '    End Try
    'End Sub
    'Sub SaveLog(ByRef SequenceNo As Integer)
    '    Try

    '        objCon = New SqlConnection(GetConnectionString)
    '        myCmd.Parameters.Clear()
    '        myCmd.CommandText = "spUploadChannelingHeaderSave"
    '        myCmd.CommandType = CommandType.StoredProcedure
    '        myCmd.Parameters.Add(New SqlParameter("@UploadDate", SqlDbType.DateTime) With {.Value = BusinessDate})
    '        myCmd.Parameters.Add(New SqlParameter("@FileName", SqlDbType.VarChar, 200) With {.Value = strFileTemp})
    '        myCmd.Parameters.Add(New SqlParameter("@TotalAgreement", SqlDbType.BigInt) With {.Value = TotalAgreement})
    '        myCmd.Parameters.Add(New SqlParameter("@TotalAmount", SqlDbType.Decimal) With {.Value = TotalAmountReceive})
    '        objCon.Open()
    '        myCmd.Connection = objCon
    '        myCmd.CommandTimeout = 0
    '        myCmd.ExecuteNonQuery()
    '        objCon.Close()
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message)
    '        ShowMessage(lblMessage, ex.Message, True)
    '    End Try
    'End Sub
    'Sub UpdateLog(ByRef SequenceNo As Integer)
    '    Try
    '        objCon = New SqlConnection(GetConnectionString)
    '        myCmd.Parameters.Clear()
    '        myCmd.CommandText = "spUploadPaymentBybatchLogUpdate"
    '        myCmd.CommandType = CommandType.StoredProcedure
    '        myCmd.Parameters.Add(New SqlParameter("@UploadDate", SqlDbType.DateTime) With {.Value = BusinessDate})
    '        myCmd.Parameters.Add(New SqlParameter("@SequenceNo", SqlDbType.SmallInt) With {.Value = SequenceNo})

    '        objCon.Open()
    '        myCmd.Connection = objCon
    '        myCmd.CommandTimeout = 0
    '        myCmd.ExecuteNonQuery()
    '        objCon.Close()
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message)
    '        ShowMessage(lblMessage, ex.Message, True)
    '    End Try
    'End Sub
    Private Sub btnReset_Click(sender As Object, e As System.EventArgs) Handles btnReset.Click
        Response.Redirect("UploadChanneling.aspx")
    End Sub
    Private Function GetValue(doc As SpreadsheetDocument, cell As Cell) As String
        'Dim value As String = cell.CellValue.InnerText
        Dim value As String = ""

        value = cell.InnerText 'IIf((cell.CellValue) Is Nothing, "", cell.CellValue.InnerText)

        If cell.DataType IsNot Nothing AndAlso cell.DataType.Value = CellValues.SharedString Then
            Return doc.WorkbookPart.SharedStringTablePart.SharedStringTable.ChildElements.GetItem(Integer.Parse(value)).InnerText
        End If
        Return value
    End Function
    Private Function FileToDataTable() As DataTable
        'Create a new DataTable.
        Dim dt As New DataTable()

        Using doc As SpreadsheetDocument = SpreadsheetDocument.Open(FileName, False)
            'Read the first Sheet from Excel file.
            Dim sheet As Sheet = doc.WorkbookPart.Workbook.Sheets.GetFirstChild(Of Sheet)()

            'Get the Worksheet instance.
            Dim worksheet As Worksheet = TryCast(doc.WorkbookPart.GetPartById(sheet.Id.Value), WorksheetPart).Worksheet

            'Fetch all the rows present in the Worksheet.
            Dim rows As IEnumerable(Of Row) = worksheet.GetFirstChild(Of SheetData)().Descendants(Of Row)()

            'Loop through the Worksheet rows.
            For Each row As Row In rows
                'Use the first row to add columns to DataTable.
                If row.RowIndex.Value = 1 Then
                    Dim clm As Integer = 0
                    For Each cell As Cell In row.Descendants(Of Cell)()
                        If GetValue(doc, cell).Trim <> "" Then
                            dt.Columns.Add(GetValue(doc, cell))
                        Else
                            dt.Columns.Add(GetValue(doc, cell).Trim & clm.ToString)
                        End If
                        clm += 1
                    Next
                Else
                    'Add rows to DataTable.
                    dt.Rows.Add()
                    Dim i As Integer = 0
                    For Each cell As Cell In row.Descendants(Of Cell)()
                        'dt.Rows(dt.Rows.Count - 1)(i) = GetValue(doc, cell)

                        If GetValue(doc, cell).Trim <> "" Then
                            dt.Rows(dt.Rows.Count - 1)(i) = GetValue(doc, cell)
                        Else
                            dt.Rows(dt.Rows.Count - 1)(i) = GetValue(doc, cell).Trim & ""
                        End If
                        i += 1
                    Next
                End If
            Next
        End Using
        Return dt
    End Function

    'Private Function CSVToDataTable(ByVal path As String) As DataTable

    '    Dim lines = IO.File.ReadAllLines(path)
    '    Dim tbl As New DataTable
    '    tbl.Columns.Add(New DataColumn("ColumnA", GetType(String)))

    '    For Each line In lines
    '        If (line.Length > 0) Then
    '            Dim oRow As DataRow
    '            oRow = tbl.NewRow
    '            tbl.Rows.Add(line)
    '        End If
    '    Next

    '    Return tbl
    'End Function

End Class