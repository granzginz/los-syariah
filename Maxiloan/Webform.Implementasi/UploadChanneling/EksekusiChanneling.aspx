﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="EksekusiChanneling.aspx.vb"
    Inherits="Maxiloan.Webform.Implementasi.EksekusiChanneling" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../../Webform.UserController/UcSearchBy.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>EksekusiChanneling</title>
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%#Request.servervariables("SERVER_NAME")%>/';	
        
        function DeleteConfirm() {
            if (confirm("Apakah yakin mau membatalkan data ini ?")) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>
                        EKSEKUSI APLIKASI CHANNELING
                    </h3>
                </div>
            </div>
            <asp:Panel ID="pnlList" runat="server">
                <div class="form_box_uc">
                    <uc1:ucsearchby id="oSearchBy" runat="server"></uc1:ucsearchby>
                </div> 
                <div class="form_button">
                    <asp:Button ID="Buttonsearch" runat="server" Text="Search" CssClass="small button blue">
                    </asp:Button>&nbsp;
                    <asp:Button ID="ButtonReset" runat="server" Text="Reset" CssClass="small button gray"
                        CausesValidation="False"></asp:Button>
                </div>
                <asp:Panel ID="pnlDtGrid" runat="server">
                    <div class="form_title">
                        <div class="form_single">
                            <h4>
                                DAFTAR EKSEKUSI APLIKASI CHANNELING
                            </h4>
                        </div>
                    </div>
                    <div class="form_box_header">
                        <div class="form_single">
                            <div class="grid_wrapper_ns">
                                <asp:DataGrid ID="dtgChannelingExecutionList" runat="server" Width="100%" OnSortCommand="SortGrid"
                                    CssClass="grid_general" BorderStyle="None" BorderWidth="0" AutoGenerateColumns="False"
                                    AllowSorting="True">
                                    <HeaderStyle CssClass="th" />
                                    <ItemStyle CssClass="item_grid" />
                                    <Columns>
                                        <asp:TemplateColumn HeaderText="SETUJU">
                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center" Width="6%"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imbExecute" CommandName="Eksekusi" runat="server" Text="EXECUTE" ImageUrl="../../Images/iconedit.gif"></asp:ImageButton>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="BATAL">
                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center" Width="6%"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imbCancel" CommandName="DEL" runat="server" Text="CANCEL" ImageUrl="../../Images/icondelete.gif"></asp:ImageButton>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn SortExpression="TransactionNo" HeaderText="NO REQUEST">
                                            <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:label ID="lblTransactionNo" runat="server" Text='<%#Container.DataItem("TransactionNo")%>'>
                                                </asp:label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn SortExpression="FileName" HeaderText="NAMA FILE">
                                            <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:label ID="lblFileName" runat="server" Text='<%#Container.DataItem("FileName")%>'>
                                                </asp:label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn> 
                                        <asp:TemplateColumn SortExpression="UploadDate" HeaderText="TGL UPLOAD">
                                            <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblUploadDate" runat="server" Text='<%#Container.DataItem("UploadDate")%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn SortExpression="TotalAgreement" HeaderText="TOTAL AGREEMENT">
                                            <HeaderStyle Font-Underline="True" HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Left" Width="15%"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblTotalAgreement" runat="server" Text='<%#Container.DataItem("TotalAgreement")%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn SortExpression="TotalAmount" HeaderText="TOTAL AMOUNT ">
                                            <HeaderStyle HorizontalAlign="Right" CssClass="item_grid_right" Height="30px"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Right" CssClass="item_grid_right" Width="16%"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblTotalAmount"   runat="server" Text='<%#FormatNumber(Container.DataItem("TotalAmount"), 0)%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn> 
                                    </Columns>
                                </asp:DataGrid>
                                <div class="button_gridnavigation">
                                    <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                                        CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                                    </asp:ImageButton>
                                    <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                                        CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                                    </asp:ImageButton>
                                    <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                                        CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                                    </asp:ImageButton>
                                    <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                                        CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                                    </asp:ImageButton>
                                    <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                                    <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                                        EnableViewState="False"></asp:Button>
                                    <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" MinimumValue="1"
                                        ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"></asp:RangeValidator>
                                    <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                                        ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general">
                                    </asp:RequiredFieldValidator>
                                </div>
                                <div class="label_gridnavigation">
                                    <asp:Label ID="lblPage" runat="server"></asp:Label>of
                                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                                    <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
