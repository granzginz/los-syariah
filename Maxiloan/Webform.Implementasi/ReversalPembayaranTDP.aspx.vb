﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region


Public Class ReversalPembayaranTDP
    Inherits Maxiloan.Webform.AccMntWebBased

#Region "Constants"
    Private STR_FORM_ID As String = "ReverTransLainnya"
#End Region
#Region "InquiryVars"
    Private oCustomClass As New Parameter.GeneralPaging
    Private oController As New Controller.GeneralPagingController
    Private m_oCustomClass As New Parameter.PaymentHistory
    Private m_oController As New PaymentHistoryController
#End Region
#Region "MemberVars"
    Private m_controller As New DataUserControlController
    Private oControllerChild As New DataUserControlController
    Dim oEntitesChild As New Parameter.InsCoAllocationDetailList
    Dim m_dtBankAccount As New DataTable
#End Region
#Region "NavigationVars"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private totalAmount As Double
    Private totalDebitAmount As Double
    Private totalCreditAmount As Double
#End Region
#Region "Properties"
    Private Property ParamReport() As String
        Get
            Return (CType(ViewState("ParamReport"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("ParamReport") = Value
        End Set
    End Property
    Private Property LastBranchComboIndex() As String
        Get
            Return (CType(ViewState("LastBranchComboIndex"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("LastBranchComboIndex") = Value
        End Set
    End Property
#End Region
    Private Sub DisplayError(ByVal strErrMsg As String)
        With lblMessage
            .Text = strErrMsg
            .Visible = True
        End With
    End Sub

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load, Me.Load
        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not IsPostBack Then
            Me.FormID = STR_FORM_ID
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then

                txtPostingDate.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
                txtPostingDate2.Text = Me.BusinessDate.ToString("dd/MM/yyyy")

                Me.SearchBy = ""
                Me.SortBy = ""
                BuildMasterDetailCombo()
            End If
        End If
    End Sub

    Private Sub BuildMasterDetailCombo()
        FillComboWithBranches(cboParent, m_controller)
        RetrieveBankAccount()
    End Sub
    Private Sub RetrieveBankAccount()
        With oEntitesChild
            .strConnection = GetConnectionString()
            .BranchId = Me.sesBranchId
        End With
        m_dtBankAccount = oControllerChild.ViewBankAccount(GetConnectionString)
        Response.Write(GenerateScript(m_dtBankAccount))
    End Sub
    Private Sub FillComboWithBranches(ByVal cbo As WebControls.DropDownList, ByVal duccController As DataUserControlController)
        Try
            Dim dtbranch As New DataTable
            dtbranch = duccController.GetBranchName(GetConnectionString, Me.sesBranchId)
            With cbo
                If Me.IsHoBranch Then
                    .DataSource = duccController.GetBranchAll(GetConnectionString)
                    .DataValueField = "ID"
                    .DataTextField = "Name"
                    .DataBind()
                    .Items.Insert(0, "Select One")
                    .Items(0).Value = "0"
                    .Items.Insert(1, "ALL")
                    .Items(1).Value = "ALL"
                Else
                    .DataSource = duccController.GetBranchName(GetConnectionString, Me.sesBranchId)
                    .DataValueField = "ID"
                    .DataTextField = "Name"
                    .DataBind()
                    .Items.Insert(0, "Select One")
                    .Items(0).Value = "0"
                    Dim strBranch() As String
                    strBranch = Split(Me.sesBranchId, ",")
                    If UBound(strBranch) > 0 Then
                        .Items.Insert(1, "ALL")
                        .Items(1).Value = "ALL"
                    End If
                End If
            End With
        Catch ex As Exception
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Function GenerateScript(ByVal DtTable As DataTable) As String
        Dim strScript As String
        Dim strScript1 As String
        Dim DataRow As DataRow()
        Dim strType As String
        Dim i As Int32
        Dim j As Int32
        strScript = "<script language=""JavaScript"">" & vbCrLf
        strScript &= " var ListData " & vbCrLf
        strScript &= "ListData = new Array(" & vbCrLf
        strType = ""
        For j = 0 To cboParent.Items.Count - 1
            DataRow = DtTable.Select(" BranchID = '" & cboParent.Items(j).Value & "'")
            If DataRow.Length > 0 Then
                For i = 0 To DataRow.Length - 1
                    If strType <> CStr(DataRow(i)("BranchID")).Trim Then
                        strType = CStr(DataRow(i)("BranchID")).Trim
                        strScript &= "new Array(" & vbCrLf
                        strScript1 = ""
                    End If

                    If strScript1 = "" Then
                        strScript1 = " new Array('" & CStr(DataRow(i)("BankAccountName")).Trim & "','" & CStr(IIf(IsDBNull(DataRow(i)("BankAccountId")), "null", DataRow(i)("BankAccountId"))).Trim & "') "
                    Else
                        strScript1 &= "," & vbCrLf & " new Array('" & CStr(DataRow(i)("BankAccountName")).Trim & "','" & CStr(IIf(IsDBNull(DataRow(i)("BankAccountId")), "null", DataRow(i)("BankAccountId"))).Trim & "') "
                    End If
                Next
                strScript &= strScript1 & ")," & vbCrLf
            Else
                strScript &= " null," & vbCrLf
            End If
        Next

        If Right(strScript.Trim, 5) = "null," Then
            strScript = Left(strScript.Trim, strScript.Trim.Length - 1)
        Else
            strScript = Left(strScript.Trim, strScript.Trim.Length - 2)
        End If

        If Right(strScript.Trim, 4) = "null" Then
            strScript &= vbCrLf & ");" & vbCrLf
        Else
            strScript &= vbCrLf & "));" & vbCrLf
        End If

        strScript &= "</script>"
        Return strScript
    End Function

    'Protected Function BodyOnLoad() As String
    '    Return "ParentChange('" & Trim(cboParent.ClientID) & "','" & Trim(cboChild.ClientID) & "','" & Trim(hdnChildValue.ClientID) & "','" & Trim(hdnChildName.ClientID) & "', " & CStr(cboParent.SelectedIndex) & "));"
    'End Function

    'Protected Function RestoreBankAccount() As String
    '    Return "RestoreBankAccountIndex('" & cboChild.ClientID & "')"
    'End Function

    'Protected Function BranchIDChange() As String
    '    Return "ParentChange('" & Trim(cboParent.ClientID) & "','" & Trim(cboChild.ClientID) & "','" & Trim(hdnChildValue.ClientID) & "','" & Trim(hdnChildName.ClientID) & "',((this.selectedIndex == -1) ? null : ListData[this.selectedIndex]));"
    'End Function

    Private Sub Btnsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btnsearch.Click
        Dim par As String
        Dim bNoAND As Boolean
        par = ""

        Dim strFilterBy As String = ""
        txtPage.Text = "1"

        Me.SearchBy = ""
        bNoAND = True
        If (cboParent.SelectedItem.Value <> "0" And cboParent.SelectedItem.Value <> "ALL") Then
            Me.SearchBy = "  a.branchid = " & "'" & cboParent.SelectedValue.Trim & "'"
            bNoAND = False
        End If

        If hdnChildValue.Value.Trim.Length > 0 Then
            Me.SearchBy = Me.SearchBy & CType(IIf(bNoAND, "", " AND "), String) & " CBT.BankAccountID = '" & hdnChildValue.Value.Trim & "' "
            bNoAND = False
            If strFilterBy.Trim.Length > 0 Then
                strFilterBy = strFilterBy & ", "
            End If
            strFilterBy = strFilterBy & ", Bank Account = " & hdnChildName.Value.Trim
        End If

        'If cboTransaction.SelectedItem.Value <> "0" Then
        '    Me.SearchBy = Me.SearchBy & " and cbt.ProcessID = " & "'" & cboTransaction.SelectedValue.Trim & "'"
        '    bNoAND = False
        'Else
        '    Me.SearchBy = Me.SearchBy & " and cbt.ProcessID in ('OTHRCVNA', 'OTHDSBNA') "
        '    bNoAND = False
        'End If

        If cboSearchBy.SelectedItem.Value <> "0" Then
            If txtSearchBy.Text.Trim.Length > 0 Then
                Dim strOperator As String

                If Me.SearchBy.IndexOf("%") <> -1 Then
                    strOperator = " = "
                Else
                    strOperator = " LIKE "
                End If
                Me.SearchBy = Me.SearchBy & CType(IIf(bNoAND, "", " AND "), String) & " (" & cboSearchBy.SelectedItem.Value & strOperator & " '" & txtSearchBy.Text.Trim & "')"
                bNoAND = False
                If strFilterBy.Trim.Length > 0 Then
                    strFilterBy = strFilterBy & ", "
                End If
                strFilterBy = strFilterBy & cboSearchBy.SelectedItem.Text & " = " & txtSearchBy.Text.Trim
            End If
        End If

        If txtPostingDate.Text.Trim.Length > 0 And txtPostingDate2.Text.Trim.Length > 0 Then
            Me.SearchBy = Me.SearchBy & CType(IIf(bNoAND, "", " AND "), String) & " cast(CBT.PostingDate as date) between '" & ConvertDate2(txtPostingDate.Text) & "' and '" & ConvertDate2(txtPostingDate2.Text) & "'"

            If strFilterBy.Trim.Length > 0 Then
                strFilterBy = strFilterBy & ", "
            End If
            strFilterBy = strFilterBy & ", Posting Date = " & ConvertDate2(txtPostingDate.Text)
        End If

        With oCustomClass
            .WhereCond = Me.SearchBy
            .SortBy = Me.SortBy
            Me.ParamReport = strFilterBy
        End With

        pnlDatagrid.Visible = True
        DtgAgree.Visible = True
        pnlList.Visible = True
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As DataTable
        Dim DvUserList As DataView

        Try
            lblMessage.Text = ""
            With oCustomClass
                .strConnection = GetConnectionString()
                .WhereCond = cmdWhere
                .CurrentPage = currentPage
                .PageSize = pageSize
                .SortBy = SortBy
                .SpName = "spReversalPembayaranTDPPaging"
            End With

            oCustomClass = oController.GetGeneralPaging(oCustomClass)

            If oCustomClass Is Nothing Then
                Throw New Exception("No record found. Search conditions: " & Me.SearchBy)
            End If

            DtUserList = oCustomClass.ListData
            DvUserList = DtUserList.DefaultView
            recordCount = oCustomClass.TotalRecords
            DtgAgree.DataSource = DvUserList

            Try
                DtgAgree.DataBind()
            Catch
                DtgAgree.CurrentPageIndex = 0
                DtgAgree.DataBind()
            End Try
            PagingFooter()
            pnlList.Visible = True
            pnlDatagrid.Visible = True

            RetrieveBankAccount()

            Dim strScript As String
            strScript = "<script language=""JavaScript"">" & vbCrLf
            strScript &= "ParentChange('" & Trim(cboParent.ClientID) & "','" & Trim(hdnChildValue.ClientID) & "','" & Trim(hdnChildName.ClientID) & "',((eval('document.forms[0]." & cboParent.ClientID & "').selectedIndex  == -1) ? null : ListData[eval('document.forms[0]." & cboParent.ClientID & "').selectedIndex]));"
            strScript &= "RestoreBankAccountIndex(" & Request("hdnBankAccount") & ");"
            strScript &= "</script>"
            mydiv.InnerHtml = strScript

        Catch ex As Exception
            DisplayError(ex.Message + " " + ex.Source + " " + ex.StackTrace)
        End Try
    End Sub

    Private Sub DtgAgree_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles DtgAgree.SortCommand
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression + " ASC"
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub DtgAgree_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgree.ItemDataBound
        Dim hypReceive As HyperLink
        Dim lblTemp As Label
        Dim hytemp As HyperLink
        Dim lbltr_nomor As Label
        Dim lnkReverse As LinkButton

        If e.Item.ItemIndex >= 0 Then
            lblTemp = CType(e.Item.FindControl("lblBranchId"), Label)
            lbltr_nomor = CType(e.Item.FindControl("lbltr_nomor"), Label)
            hypReceive = CType(e.Item.FindControl("HypReceive"), HyperLink)
            lnkReverse = CType(e.Item.FindControl("lnkReverse"), LinkButton)
            lnkReverse.Attributes.Add("onclick", "return reverseconfirm();")

            If Not lblTemp Is Nothing Then
                hytemp = CType(e.Item.FindControl("hyVoucherNo"), HyperLink)
                hytemp.NavigateUrl = "javascript:OpenWinMain('" & lblTemp.Text.Trim & "','" & hytemp.Text.Trim & "', '" & lbltr_nomor.Text.Trim & "')"
            End If
        End If
    End Sub

    Private Sub DtgAgree_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgAgree.ItemCommand
        Select Case e.CommandName
            Case "reverse"
                If CheckFeature(Me.Loginid, Me.FormID, "REV", Me.AppId) Then
                    lblMessage.Visible = False

                    Dim DtUserList As New DataTable
                    Dim DvUserList As New DataView
                    Dim VoucherNo As New HyperLink

                    VoucherNo = CType(e.Item.FindControl("hyVoucherNo"), HyperLink)

                    With m_oCustomClass
                        .ReferenceNo = VoucherNo.Text
                        .strConnection = GetConnectionString()
                    End With

                    Try
                        m_oCustomClass = m_oController.ReversalPembayaranTDPSave(m_oCustomClass)
                        If m_oCustomClass.Err = "" Then
                            Me.SearchBy = ""
                            Me.SortBy = ""
                            BuildMasterDetailCombo()
                            'DoBind(Me.SearchBy, Me.SortBy)
                            ShowMessage(lblMessage, "Reversal Berhasil", False)
                        Else
                            ShowMessage(lblMessage, "Error On spReversalPembayaranTDPSave " + m_oCustomClass.Err, True)
                        End If
                    Catch exp As Exception
                        ShowMessage(lblMessage, exp.Message, True)
                    End Try

                End If
        End Select
    End Sub


    Private Sub BtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Me.SearchBy = ""
        cboSearchBy.SelectedIndex = 0
        txtSearchBy.Text = ""
        'cboChild.SelectedIndex = 0
        txtPostingDate.Text = ""
        BuildMasterDetailCombo()
        pnlDatagrid.Visible = False
    End Sub

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbGoPage.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub
#End Region


End Class