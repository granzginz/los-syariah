﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ReversalPembayaranTDP.aspx.vb" Inherits="Maxiloan.Webform.Implementasi.ReversalPembayaranTDP" EnableEventValidation="false"  %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ucBranchHO" Src="../Webform.UserController/ucBranchHO.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcBranch" Src="../Webform.UserController/UcBranch.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcBranchAll" Src="../Webform.UserController/UcBranchAll.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../Webform.UserController/UcSearchBy.ascx" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Reversal Pembayaran TDP</title>
    <link href="../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <script type="text/javascript">
        var hdnDetail;
        var HdnDetailValue;
        var cboBankAccount = null;
        function ParentChange(pCmbOfPayment, pBankAccount, pHdnDetail, pHdnDetailValue, itemArray) {
            hdnDetail = eval('document.forms[0].' + pHdnDetail);
            HdnDetailValue = eval('document.forms[0].' + pHdnDetailValue); 
            var i, j;
            for (i = eval('document.forms[0].' + pBankAccount).options.length; i >= 0; i--) {
                eval('document.forms[0].' + pBankAccount).options[i] = null
            };
            if (itemArray == null) {
                j = 0;
            }
            else {
                j = 1;
            };
            eval('document.forms[0].' + pBankAccount).options[0] = new Option('Select One', '0');
            if (itemArray != null) {
                for (i = 0; i < itemArray.length; i++) {
                    eval('document.forms[0].' + pBankAccount).options[j++] = new Option(itemArray[i][0], itemArray[i][1]);
                };
                eval('document.forms[0].' + pBankAccount).selected = true;
            }
        };
          function cboChildonChange(selIndex, l, j) {
            hdnDetail.value = l; 
            HdnDetailValue.value = j; 
            eval('document.forms[0].hdnBankAccount').value = selIndex;
          }
          function RestoreBankAccountIndex(BankAccountIndex) {
              cboBankAccount = eval('document.forms[0].cboChild');
              if (cboBankAccount != null)
                  if (eval('document.forms[0].hdnBankAccount').value != null) {
                      if (BankAccountIndex != null)
                          cboBankAccount.selectedIndex = BankAccountIndex;
                  }
          }
          function reverseconfirm() {
              if (window.confirm("Apakah yakin mau hapus transaksi ini ? "))
                  return true;
              else
                  return false;
          }
    </script>
</head>
<body>
<script language="javascript" type="text/javascript"> 
    function OpenWinMain(BranchId, VoucherNo, tr_nomor) {
        var x = screen.width; var y = screen.height - 100;
        window.open(ServerName + App + '/Webform.LoanMnt/View/CashBankVoucher.aspx?BranchId=' + BranchId + '&VoucherNo=' + VoucherNo + '&tr_nomor=' + tr_nomor + '&style=AccMnt', null, 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0,scrollbars=1')
    }
</script>
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server" ID="ScriptManager1"> </asp:ScriptManager>
         <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
     <input id="hdnChildValue" type="hidden" runat="server" name="hdnChildValue" />
    <input id="hdnChildName" type="hidden" runat="server" name="hdnChildName" />
    <input id="hdnBankAccount" style="z-index: 101; position: absolute; top: 16px; left: 328px" runat="server" type="hidden" name="hdnBankAccount" />
        <asp:Panel ID="pnlList" runat="server">
       <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    Reversal Pembayaran TDP
                </h3>
            </div>
        </div> 
         <div class="form_box">
            <div class="form_left">
                <label class="label_req">
                    Cabang
                </label>
                <asp:DropDownList ID="cboParent" runat="server" >
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="rfvcbobranchid" runat="server" InitialValue="0" ErrorMessage="Harap pilih Cabang"
                    ControlToValidate="cboParent" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
            <%--<div class="form_right"> 
                <label>
                    Transaksi
                </label>
                <asp:DropDownList ID="cboTransaction" runat="server">
                    <asp:ListItem Value="0">All</asp:ListItem>
                    <asp:ListItem Value="OTHRCVNA">Penerimaan Lainnya</asp:ListItem>
                    <asp:ListItem Value="OTHDSBNA">Pembayaran Lainnya</asp:ListItem> 
                </asp:DropDownList>
            </div>--%>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Tgl Posting
                </label>
                <asp:TextBox runat="server" ID="txtPostingDate"></asp:TextBox>
                <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtPostingDate"
                    Format="dd/MM/yyyy">
                </asp:CalendarExtender>
                &nbsp;S/D&nbsp;
                <asp:TextBox runat="server" ID="txtPostingDate2"></asp:TextBox>
                <asp:CalendarExtender runat="server" ID="CalendarExtender2" TargetControlID="txtPostingDate2"
                    Format="dd/MM/yyyy">
                </asp:CalendarExtender>
            </div>
        </div>
        <div class="form_box">
            <%--<div class="form_left">
                <label>
                    Rekening
                </label>
                <asp:DropDownList ID="cboChild" runat="server" onchange="cboChildonChange(this.selectedIndex, this.options[this.selectedIndex].value,this.options[this.selectedIndex].text);">
                </asp:DropDownList>
            </div>--%>
            <div class="form_right">
                <label>
                    Cari Berdasarkan
                </label>
                <asp:DropDownList ID="cboSearchBy" runat="server">
                    <asp:ListItem Value="0">Select One</asp:ListItem>
                    <asp:ListItem Value="VoucherNo">No Voucher</asp:ListItem>
                    <asp:ListItem Value="Description">Keterangan</asp:ListItem>
                    <asp:ListItem Value="EmployeeName">Kasir</asp:ListItem>
                    <asp:ListItem Value="ReferenceNo">No Bukti Kas</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearchBy" runat="server" Width="200px" ></asp:TextBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="Btnsearch" runat="server" Text="Find" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
            </asp:Button>
        </div> 
    </asp:Panel> 
    
     <asp:Panel ID="pnlDatagrid" runat="server" Visible="false">
         <div class="form_title">
            <div class="form_single">
                <h4>
                    DAFTAR TRANSAKSI
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgAgree" runat="server" Visible="False" AllowPaging="True" AllowSorting="True"
                        AutoGenerateColumns="False" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="PILIH">
                                <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkReverse" runat="server" Text="REVERSE" CommandName="reverse"
                                        Visible='<%#IIf(Container.DataItem("IsReverse") = "1", False, True)%>'>
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn> 
                            <asp:TemplateColumn SortExpression="VoucherNo" HeaderText="NO VOUCHER">
                                <ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyVoucherNo" runat="server" Text='<%#Container.DataItem("VoucherNo")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="ApplicationID" SortExpression="ApplicationID" HeaderText="NO APLIKASI"></asp:BoundColumn>
                            <asp:BoundColumn DataField="AgreementNo" SortExpression="AgreementNo" HeaderText="NO KONTRAK"></asp:BoundColumn>
                            <asp:BoundColumn DataField="JournalNo" SortExpression="JournalNo" HeaderText="NO JURNAL"></asp:BoundColumn>
                            <asp:BoundColumn DataField="ValueDate" SortExpression="ValueDate" HeaderText="TGL VALUTA"
                                DataFormatString="{0:dd/MM/yyyy}">
                                <HeaderStyle HorizontalAlign="Center" CssClass="tdjudul"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="7%"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="PostingDate" SortExpression="PostingDate" HeaderText="TGL POSTING"
                                DataFormatString="{0:dd/MM/yyyy}">
                                <HeaderStyle HorizontalAlign="Center" CssClass="tdjudul"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="7%"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:TemplateColumn SortExpression="Description" HeaderText="KETERANGAN">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width = "30%">  </ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblDescription" runat="server" Text='<%# Container.DataItem("Description") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>  
                            <asp:TemplateColumn SortExpression="DebitAmount" HeaderText="JUMLAH DEBET">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" Width="10%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblDebitAmount" runat="server" Text='<%#formatnumber(Container.DataItem("DebitAmount"),0)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="CreditAmount" HeaderText="JUMLAH KREDIT">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" Width="10%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblCreditAmount" runat="server" Text='<%#formatnumber(Container.DataItem("CreditAmount"),0)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="BE.EmployeeName" HeaderText="KASIR">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="20%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="lblAdvanceStatus" runat="server" Text='<%#Container.DataItem("CashierName")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" HeaderText="BRANCH ID">
                                <ItemTemplate>
                                    <asp:Label ID="lblBranchId" runat="server" Text='<%#Container.DataItem("BranchId")%>'>Label</asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" HeaderText="Tr Nomor">
                                <ItemTemplate>
                                    <asp:Label ID="lbltr_nomor" runat="server" Text='<%#Container.DataItem("tr_nomor")%>'>Label</asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="SourceRK" SortExpression="SourceRK" HeaderText="Received From"></asp:BoundColumn>
                            <asp:BoundColumn DataField="SeqNo" SortExpression="SeqNo" HeaderText="SeqNo" Visible="false"></asp:BoundColumn>
                        </Columns>
                        <PagerStyle Visible="False" HorizontalAlign="Left"  
                            Mode="NumericPages"></PagerStyle>
                    </asp:DataGrid>
                    <div class="button_gridnavigation">
                        <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="First" ImageUrl="../Images/grid_navbutton01.png"></asp:ImageButton>
                        <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="Prev" ImageUrl="../Images/grid_navbutton02.png"></asp:ImageButton>
                        <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="Next" ImageUrl="../Images/grid_navbutton03.png"></asp:ImageButton>
                        <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="Last" ImageUrl="../Images/grid_navbutton04.png"></asp:ImageButton>
                        Page&nbsp;
                        <asp:TextBox ID="txtPage" runat="server" Width="34px" >1</asp:TextBox>
                        <asp:Button ID="imbGoPage" runat="server" CssClass="buttongo small blue" Text="Go"
                            EnableViewState="False"></asp:Button>
                        <asp:RangeValidator ID="rgvGo" runat="server" Type="Integer" MaximumValue="999999999"
                            ErrorMessage="No Halaman Salah" MinimumValue="1" ControlToValidate="txtPage"
                            Display="Dynamic" CssClass="validator_general"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="rfvGo" runat="server" Display="Dynamic" ErrorMessage="No Halaman Salah"
                            ControlToValidate="txtPage" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                    <div class="label_gridnavigation">
                        <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                        <asp:Label ID="lblrecord" runat="server"></asp:Label>&nbsp;record(s)
                    </div>
                </div>
            </div>
        </div> 
    </asp:Panel>
    <div id="mydiv" runat="server"> </div>
         
    </form>
</body>
</html>
