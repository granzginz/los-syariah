﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="RestructFactAndMUInq.aspx.vb" Inherits="Maxiloan.Webform.Implementasi.RestructFactAndMUInq" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>RestructFactAndMUInq</title>
    <script type="text/javascript" language="javascript">

        var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%#Request.servervariables("SERVER_NAME")%>/';

        function Valid() {
            var cboSearch = window.document.Form1.cboSearch.options[window.document.Form1.cboSearch.selectedIndex].value;
            if (cboSearch == 1) {
                document.Form1.txtSearch.maxlength = 3;
                alert(document.Form1.txtSearch.maxlength);
            }
        }		
    </script>
    <link rel="Stylesheet" type="text/css" href="../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../Include/Buttons.css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="Form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false" />
            <input id="hdnChildValue" type="hidden" name="hdnSP" runat="server" />
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>
                        RESCHEDULING INQUIRY
                    </h3>
                </div>
            </div>
            <asp:Panel ID="pnlSearch" runat="server">
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Branch</label>
                        <asp:DropDownList ID="cboParent" runat="server">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="rfvcbobranchid" runat="server" ControlToValidate="cboParent"
                            ErrorMessage="Please Select Branch" Display="Dynamic" InitialValue="0" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Effective Date</label>
                        <asp:TextBox ID="txtEffDate" runat="server" CssClass="small_text"></asp:TextBox>
                        <asp:CalendarExtender ID="txtEffDate_CalendarExtender" runat="server" Enabled="True"
                            TargetControlID="txtEffDate" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator59" runat="server" ErrorMessage="*"
                            CssClass="validator_general" ControlToValidate="txtEffDate"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Search By</label>
                        <asp:DropDownList ID="cboSearch" runat="server">
                            <asp:ListItem Value="0">Select One</asp:ListItem>
                            <asp:ListItem Value="1">Request No</asp:ListItem>
                            <asp:ListItem Value="2">No Kontrak</asp:ListItem>
                            <asp:ListItem Value="3">Nama Konsumen</asp:ListItem>
                        </asp:DropDownList>
                        &nbsp;
                        <asp:TextBox ID="txtSearch" runat="server" Width="123px" ></asp:TextBox>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Status</label>
                        <asp:DropDownList ID="CboStatus" runat="server">
                            <asp:ListItem Value="0">All</asp:ListItem>
                            <asp:ListItem Value="R">Request</asp:ListItem>
                            <asp:ListItem Value="A">Approve</asp:ListItem>
                            <asp:ListItem Value="E">Execute</asp:ListItem>
                            <asp:ListItem Value="C">Cancel</asp:ListItem>
                            <asp:ListItem Value="R">Reject</asp:ListItem> 
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="buttonSearch" runat="server" Text="Search" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="buttonReset" runat="server" Text="Reset" CssClass="small button gray"
                        CausesValidation="False"></asp:Button>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlDatagrid" runat="server" Visible="false">
                <div class="form_box_header">
                    <div class="form_single">
                        <h4>
                            DAFTAR PENGAJUAN RESCHEDULING
                        </h4>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtgPaging" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                                BorderStyle="None" BorderWidth="0" DataKeyField="AgreementNo" CssClass="grid_general">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="RESCHD">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="lnkview" runat="server">LIHAT</asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="RequestNo" HeaderText="REQUEST NO"> 
                                        <ItemTemplate>
                                            <asp:Label ID="labelRequestNo" runat="server" Text='<%#Container.DataItem("RequestNo")%>'>
                                            </asp:Label>
                                        </ItemTemplate> 
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="HyAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>'>
                                            </asp:HyperLink>
                                            <asp:Label ID="LblBranchID" runat="server" Visible="False" Text='<%#Container.DataItem("BranchID")%>'>
                                            </asp:Label>
                                            <asp:Label ID="LblApplicationId" runat="server" Visible="False" Text='<%#Container.DataItem("ApplicationId")%>'>
                                            </asp:Label>
                                            <asp:Label ID="lblSeqNo" runat="server" Visible="False" Text='<%#Container.DataItem("SeqNo")%>'>
                                            </asp:Label>
                                            <asp:Label ID="lblRequestNo" runat="server" Visible="False" Text='<%#Container.DataItem("RequestNo")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="InvoiceNo" HeaderText="INVOICE NO"> 
                                        <ItemTemplate>
                                            <asp:Label ID="lblInvoiceNo" runat="server" Text='<%#Container.DataItem("InvoiceNo")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                </asp:TemplateColumn>
                                    <asp:TemplateColumn  Visible="False" SortExpression="InvoiceSeqNo" HeaderText="INVOICE SeqNo"> 
                                        <ItemTemplate>
                                            <asp:Label ID="lblInvoiceSeqNo" runat="server" Text='<%#Container.DataItem("InvoiceSeqNo")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="Name" HeaderText="NAMA KONSUMEN">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="HyCustomerName" runat="server" Text='<%#Container.DataItem("Name")%>'>
                                            </asp:HyperLink>
                                            <asp:Label ID="lblCustID" runat="Server" Text='<%#Container.DataItem("CustomerID")%>'
                                                Visible="False">
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="OutstandingPrincipalNew" HeaderText="JUMLAH POKOK">
                                        <ItemTemplate>
                                            <%--<asp:Label ID="lblAmount" runat="server" Text='<%#formatnumber(Container.DataItem("AdministrationFee"),2)%>'>--%>
                                            <asp:Label ID="lblAmount" runat="server" Text='<%#FormatNumber(Container.DataItem("OutstandingPrincipalNew"), 2)%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="EfectiveDate" SortExpression="Rescheduling.Effectivedate"
                                        HeaderText="TGL PENGAJUAN" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
                                    <asp:TemplateColumn SortExpression="Status" HeaderText="STATUS">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStatus" runat="server" Text='<%#Container.DataItem("Status")%>'>
                                            </asp:Label>
                                            <asp:Label ID="lblEffectiveDate" runat="server" Text='<%#FORMAT(Container.DataItem("EfectiveDate"),"dd/MM/yyyy")%>'
                                                Visible="false">
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                            <div class="button_gridnavigation">
                                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                                    CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                                    CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                                    CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                                    CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                                    EnableViewState="False"></asp:Button>
                                <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" MinimumValue="1"
                                    ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"></asp:RangeValidator>
                                <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                                    ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                            </div>
                            <div class="label_gridnavigation">
                                <asp:Label ID="lblPage" runat="server"></asp:Label>of
                                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
