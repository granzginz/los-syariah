﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="RestructFactAndMUStep2.aspx.vb" Inherits="Maxiloan.Webform.Implementasi.RestructFactAndMUStep2" %>


<%@ Register TagPrefix="uc1" TagName="UcPaymentInfo" Src="../../Webform.UserController/UcPaymentInfo.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="aspajax" %>
<%@ Register TagPrefix="uc1" TagName="UcFullPrepayInfo" Src="../../Webform.UserController/UcFullPrepayInfo.ascx" %>
<%@ Register Src="../../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat"
    TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>RestructFactAndMUStep2</title>
    <script type="text/javascript" language="javascript">
        var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%#Request.servervariables("SERVER_NAME")%>/';
        function InstScheme() { 
            document.forms[0].rdoSTTYpe(0).disabled = true;
            document.forms[0].rdoSTTYpe(1).disabled = true;
            document.forms[0].rdoSTTYpe(2).disabled = true;

            if (document.forms[0].cboInstallment.value == 'ST') {
                document.forms[0].rdoSTTYpe(0).disabled = false;
                document.forms[0].rdoSTTYpe(1).disabled = false;
                document.forms[0].rdoSTTYpe(2).disabled = false;
            }
        }
        function rdoDisabled() {
            document.forms[0].rdoSTTYpe(0).checked = true;
            document.forms[0].rdoSTTYpe(0).disabled = true;
            document.forms[0].rdoSTTYpe(1).disabled = true;
            document.forms[0].rdoSTTYpe(2).disabled = true;
        }
    </script>
    <link rel="Stylesheet" type="text/css" href="../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../Include/Buttons.css" />
    <script type="text/javascript" src="../../Maxiloan.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h4>
                        PENGAJUAN RESCHEDULING FACT DAN MODAL USAHA
                    </h4>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>
                        No Kontrak
                    </label>
                    <asp:HyperLink ID="lblAgreementNo" runat="server"></asp:HyperLink>
                </div>
                <div class="form_right">
                    <label>
                        Nama Customer
                    </label>
                    <asp:HyperLink ID="lblCustName" runat="server"></asp:HyperLink>
                </div>
            </div>
            <div class="form_title">
                <div class="form_single">
                    <h4>
                        DATA SAAT INI
                    </h4>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>
                        Jenis Margin
                    </label>
                    <asp:Label ID="lblInterestType" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                    <label>
                        Produk
                    </label>
                    <asp:Label ID="lblProduct" runat="server"></asp:Label>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>
                        Skema Angsuran
                    </label>
                    <asp:Label ID="lblInstallScheme" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                    <label>
                        Package
                    </label>
                    <asp:Label ID="lblPackage" runat="server"></asp:Label>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>
                        Jenis Finance
                    </label>
                    <asp:Label ID="lblFinanceType" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                    <label>
                        Penjamin
                    </label>
                    <asp:Label ID="lblGuarantor" runat="server"></asp:Label>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>
                        Suku Margin Effective (%)
                    </label>
                    <asp:Label ID="lblEffRate" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                    <label>
                        Lama Angsuran
                    </label>
                    <asp:Label ID="lblInstallmentNo" runat="server"></asp:Label>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>
                        Pola Pembayaran
                    </label>
                    <asp:Label ID="lblPaymentFreq" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                    <label>
                        No Rescheduling
                    </label>
                    <asp:Label ID="lblReschedNo" runat="server"></asp:Label>
                </div>
            </div>
            <div class="form_title">
                <div class="form_single">
                    <h4>
                        A/R PER TANGGAL
                        <asp:Label ID="lbljudul" runat="server"></asp:Label>
                    </h4>
                </div>
            </div>
            <uc1:ucpaymentinfo id="oPaymentInfo" runat="server">
                </uc1:ucpaymentinfo>
            <div class="form_box_title_nobg">
                <div class="form_left">
                    <label>
                        Tanggal Efektif
                    </label>
                    <asp:TextBox ID="txtTglEfektif" runat="server" CssClass="small_text" />
                    <aspajax:CalendarExtender ID="calExTglEfektif" runat="server" TargetControlID="txtTglEfektif"
                        Format="dd/MM/yyyy" />
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label class="label_req"> 
                        Tenor Baru
                    </label>
                    <uc1:ucnumberformat id="txtTenor" runat="server" />
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label class="label_req">
                        Skema Angsuran
                    </label>
                    <asp:DropDownList ID="cboInstallment" Enabled="False" runat="server" onchange="InstScheme();">
                        <asp:ListItem Value="0">Select One</asp:ListItem>
                        <asp:ListItem Value="RF">Regular Fixed Installment</asp:ListItem>
                        <asp:ListItem Value="IR">Irregular Installment</asp:ListItem>
                        <asp:ListItem Value="ST" Selected="True">Step Up Step Down</asp:ListItem>
                        <asp:ListItem Value="EP">Even Principle</asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="rfvcbobranchid" runat="server" ControlToValidate="cboInstallment"
                        ErrorMessage="Harap pilih Skema Angsuran" Display="Dynamic" InitialValue="0"
                        CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>
                <div class="form_right">
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label class="label_general">
                        Jenis Step Up Step Down
                    </label>
                    <asp:RadioButtonList ID="rdoSTTYpe" Enabled="True" runat="server" RepeatDirection="Horizontal"
                        CssClass="opt_single">
                        <asp:ListItem Value="NM" Selected="True">Basic Step Up/Step Down</asp:ListItem>
                        <asp:ListItem Value="RL">Normal</asp:ListItem>
                        <asp:ListItem Value="LS">Leasing Step Up/Step Down</asp:ListItem>
                    </asp:RadioButtonList>
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="imbNext" runat="server" CausesValidation="True" Text="Next" CssClass="small button green">
                </asp:Button>&nbsp;
                <asp:Button ID="imbCancel" runat="server" CausesValidation="False" Text="Cancel"
                    CssClass="small button gray"></asp:Button>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
