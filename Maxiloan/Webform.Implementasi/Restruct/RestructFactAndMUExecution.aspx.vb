﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.General
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper

#End Region


Public Class RestructFactAndMUExecution
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents osearchBy As UcSearchBy

#Region "Property "
    Private Property ApplicationID() As String
        Get
            Return CType(ViewState("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
    Private Property CustomerID() As String
        Get
            Return CType(ViewState("CustomerID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property
    Private Property SeqNo() As Integer
        Get
            Return CType(ViewState("SeqNo"), Integer)
        End Get
        Set(ByVal Value As Integer)
            ViewState("SeqNo") = Value
        End Set
    End Property
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("CmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CmdWhere") = Value
        End Set
    End Property
    Private Property FilterBy() As String
        Get
            Return CType(ViewState("FilterBy"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("FilterBy") = Value
        End Set
    End Property
    Private Property BindMenu() As String
        Get
            Return CStr(ViewState("BindMenu"))
        End Get
        Set(ByVal Value As String)
            ViewState("BindMenu") = Value
        End Set
    End Property
#End Region

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.AgreementList
    Private oController As New AgreementListController
#End Region

#Region "Page Load"
    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.FormID = "RestructFactAndMUExec"
            osearchBy.ListData = "Agreementno, No Kontrak-Name, Nama Konsumen"
            osearchBy.BindData()

            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.SearchBy = ""
                Me.SortBy = ""
            End If
        End If
    End Sub
#End Region

#Region "DoBind"
    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        'Dim intloop As Integer
        'Dim hypID As HyperLink
        Dim oPaging As New Parameter.GeneralPaging
        Dim m_controller As New GeneralPagingController

        With oPaging
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
            .SpName = "spRestructFactAndMUExecutionPaging"
        End With

        oPaging = m_controller.GetGeneralPaging(oPaging)
        If Not oPaging Is Nothing Then
            DtUserList = oPaging.ListData
            recordCount = oPaging.TotalRecords
        Else
            recordCount = 0
        End If
        DtgAgree.DataSource = DtUserList.DefaultView
        DtgAgree.CurrentPageIndex = 0
        DtgAgree.DataBind()
        PagingFooter()
        pnlList.Visible = True
        pnlDatagrid.Visible = True
        lblMessage.Visible = False
    End Sub

#End Region

#Region "databound"
    Private Sub DtgAgree_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgree.ItemDataBound
        Dim lblTemp As Label
        Dim lbbranch As Label
        Dim hyTemp As HyperLink
        Dim lblCustomerId, lblEffectiveDate, lblRequestNo As Label

        If SessionInvalid() Then
            Exit Sub
        End If

        Dim hypCancel As HyperLink
        Dim HyExecute As HyperLink
        Dim hyApplicationid, lblSeqNO As HyperLink
        Dim inlblSeqNo As HyperLink
        Dim flagdel As String = "2"
        Dim inflagdel As String = "1"
        Dim lblInvoiceNo As Label
        Dim lblInvoiceSeqNo As Label
        If e.Item.ItemIndex >= 0 Then
            lbbranch = CType(e.Item.FindControl("lbranchid"), Label)
            lblTemp = CType(e.Item.FindControl("lblCustomerId"), Label)
            hyTemp = CType(e.Item.FindControl("hyApplicationId"), HyperLink)
            hyApplicationid = CType(e.Item.FindControl("hyApplicationId"), HyperLink)
            hypCancel = CType(e.Item.FindControl("HYCAN"), HyperLink)
            HyExecute = CType(e.Item.FindControl("HYEXEC"), HyperLink)
            inlblSeqNo = CType(e.Item.FindControl("lblAmountToBepaid"), HyperLink)
            lblSeqNO = CType(e.Item.FindControl("lblSeqNO"), HyperLink)
            lblCustomerId = CType(e.Item.FindControl("lblCustomerId"), Label)
            lblEffectiveDate = CType(e.Item.FindControl("lblEffectiveDate"), Label)
            lblRequestNo = CType(e.Item.FindControl("lblRequestNo"), Label)
            lblInvoiceNo = CType(e.Item.FindControl("lblInvoiceNo"), Label)
            lblInvoiceSeqNo = CType(e.Item.FindControl("lblInvoiceSeqNo"), Label)
            'hypCancel.NavigateUrl = "DExecProc.aspx?Applicationid=" & hyTemp.Text.Trim & "&branchid=" & lbbranch.Text.Trim & "&SeqNo=" & inlblSeqNo.Text.Trim & "&FlagDel=" & inflagdel.Trim
            'HyExecute.NavigateUrl = "DExecProc.aspx?Applicationid=" & hyTemp.Text.Trim & "&branchid=" & lbbranch.Text.Trim & "&SeqNo=" & inlblSeqNo.Text.Trim & "&FlagDel=" & flagdel.Trim
            '*** Customer Link
            'lblTemp = CType(e.Item.FindControl("lblCustomerId"), Label)
            hyTemp = CType(e.Item.FindControl("hyCustomerName"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(lblTemp.Text.Trim) & "')"
            '*** Agreement No link
            hyTemp = CType(e.Item.FindControl("hyAgreementNo"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(hyApplicationid.Text.Trim) & "')"
            '*** ApplicationId link
            hyTemp = CType(e.Item.FindControl("hyApplicationId"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenApplicationId('" & "AccMnt" & "', '" & Server.UrlEncode(hyTemp.Text.Trim) & "')"

            With oCustomClass
                Me.BranchID = lbbranch.Text.Trim
                Me.ApplicationID = hyApplicationid.Text.Trim
                Me.SeqNo = CInt(lblSeqNO.Text)
                Me.CustomerID = lblCustomerId.Text
            End With

            hypCancel.NavigateUrl = "RestructFactAndMUExec.aspx?no=" & lblRequestNo.Text & "&effectivedate=" & lblEffectiveDate.Text.Trim & "&mode=0&seqno=" & CStr(Me.SeqNo) & "&applicationid=" & Server.UrlEncode(Me.ApplicationID) & "&branchid=" & Me.BranchID & "&customerid=" & Server.UrlEncode(CStr(Me.CustomerID)) & "&InvoiceNo=" & lblInvoiceNo.Text.Trim & "&InvoiceSeqNo=" & lblInvoiceSeqNo.Text.Trim
            HyExecute.NavigateUrl = "RestructFactAndMUExec.aspx?no=" & lblRequestNo.Text & "&effectivedate=" & lblEffectiveDate.Text.Trim & "&mode=1&seqno=" & CStr(Me.SeqNo) & "&applicationid=" & Server.UrlEncode(Me.ApplicationID) & "&branchid=" & Me.BranchID & "&customerid=" & Server.UrlEncode(CStr(Me.CustomerID)) & "&InvoiceNo=" & lblInvoiceNo.Text.Trim & "&InvoiceSeqNo=" & lblInvoiceSeqNo.Text.Trim
            inlblSeqNo.NavigateUrl = "RestructFactAndMUExec.aspx?no=" & lblRequestNo.Text & "&effectivedate=" & lblEffectiveDate.Text.Trim & "&mode=2&seqno=" & CStr(Me.SeqNo) & "&applicationid=" & Server.UrlEncode(Me.ApplicationID) & "&branchid=" & Me.BranchID & "&customerid=" & Server.UrlEncode(CStr(Me.CustomerID))

        End If
    End Sub
#End Region

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            ShowMessage(lblMessage, "Data tidak ditemukan  .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imgbtnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub

#End Region

#Region "Reset"
    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbReset.Click
        Server.Transfer("RestructFactAndMUExecution.aspx")
    End Sub
#End Region
#Region "Search"
    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imgsearch.Click
        If SessionInvalid() Then
            Exit Sub
        End If
        If CheckFeature(Me.Loginid, Me.FormID, "SRCH", Me.AppId) Then

            Me.SearchBy = ""
            Me.SearchBy = Me.SearchBy & "  branchid = '" & Me.sesBranchId.Replace("'", "") & "'"
            If osearchBy.Text.Trim <> "" Then
                Me.SearchBy = Me.SearchBy & " and " & osearchBy.ValueID & " like '%" & osearchBy.Text.Trim.Replace("'", "''") & "%'"
            End If
            If txtTglEfektif.Text <> "" Then
                Me.SearchBy = Me.SearchBy & " and Resch.EffectiveDate <= '" & ConvertDate2(txtTglEfektif.Text) & "'"
            End If
            pnlDatagrid.Visible = True
            DtgAgree.Visible = True
            pnlList.Visible = True
            DoBind(Me.SearchBy, Me.SortBy)

        End If
    End Sub
#End Region
#Region "Sort"
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

End Class