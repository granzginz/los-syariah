﻿#Region "Import"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region


Public Class RestructFactAndMUFinal
    Inherits Maxiloan.Webform.AccMntWebBased
    Protected WithEvents oPaymentInfo As UcFullPrepayInfo
    'Protected WithEvents txtEffRate As ucNumberFormat
    'Public WithEvents txtEffRate As TextBox

#Region "Property"
    Property Prepaid() As Double
        Get
            Return CDbl(ViewState("Prepaid"))
        End Get
        Set(ByVal Value As Double)
            ViewState("Prepaid") = Value
        End Set
    End Property
    Private Property No() As Integer
        Get
            Return (CType(ViewState("No"), Integer))
        End Get
        Set(ByVal Value As Integer)
            ViewState("No") = Value
        End Set
    End Property

    Private Property NewTenor() As Integer
        Get
            Return (CType(ViewState("NewTenor"), Integer))
        End Get
        Set(ByVal Value As Integer)
            ViewState("NewTenor") = Value
        End Set
    End Property
    Property MyDataSet() As DataSet
        Get
            Return (CType(ViewState("MyDataSet"), DataSet))
        End Get
        Set(ByVal Value As DataSet)
            ViewState("MyDataSet") = Value
        End Set
    End Property
    Property StepUpDownType() As String
        Get
            Return ViewState("StepUpDownType").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("StepUpDownType") = Value
        End Set
    End Property
    Private Property MinSeqNo() As Integer
        Get
            Return (CType(ViewState("MinSeqNo"), Integer))
        End Get
        Set(ByVal Value As Integer)
            ViewState("MinSeqNo") = Value
        End Set
    End Property
    Private Property newNo() As Integer
        Get
            Return (CType(ViewState("newNo"), Integer))
        End Get
        Set(ByVal Value As Integer)
            ViewState("newNo") = Value
        End Set
    End Property
    Private Property NewNum() As Integer
        Get
            Return (CType(ViewState("NewNum"), Integer))
        End Get
        Set(ByVal Value As Integer)
            ViewState("NewNum") = Value
        End Set
    End Property
    Private Property SeqNo() As Integer
        Get
            Return (CType(ViewState("SeqNo"), Integer))
        End Get
        Set(ByVal Value As Integer)
            ViewState("SeqNo") = Value
        End Set
    End Property
    Private Property GuarantorID() As String
        Get
            Return (CType(ViewState("GuarantorID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("GuarantorID") = Value
        End Set
    End Property
    Private Property InterestType() As String
        Get
            Return (CType(ViewState("InterestType"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("InterestType") = Value
        End Set
    End Property
    Private Property ReasonID() As String
        Get
            Return (CType(ViewState("ReasonID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("ReasonID") = Value
        End Set
    End Property
    Private Property ReasonTypeID() As String
        Get
            Return (CType(ViewState("ReasonTypeID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("ReasonTypeID") = Value
        End Set
    End Property
    Private Property payFreq() As Integer
        Get
            Return (CType(ViewState("payFreq"), Integer))
        End Get
        Set(ByVal Value As Integer)
            ViewState("payFreq") = Value
        End Set
    End Property
    Private Property PaymentFrequency() As Integer
        Get
            Return (CType(ViewState("PaymentFrequency"), Integer))
        End Get
        Set(ByVal Value As Integer)
            ViewState("PaymentFrequency") = Value
        End Set
    End Property
    Private Property DaysDiff() As Integer
        Get
            Return (CType(ViewState("DaysDiff"), Integer))
        End Get
        Set(ByVal Value As Integer)
            ViewState("DaysDiff") = Value
        End Set
    End Property
    Private Property PartialPay() As Double
        Get
            Return (CType(ViewState("PartialPay"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("PartialPay") = Value
        End Set
    End Property
    Private Property AdminFee() As Double
        Get
            Return (CType(ViewState("AdminFee"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("AdminFee") = Value
        End Set
    End Property

    Private Property FlatRate() As Double
        Get
            Return (CType(ViewState("FlatRate"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("FlatRate") = Value
        End Set
    End Property
    Private Property sdate() As String
        Get
            Return (CType(ViewState("sdate"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("sdate") = Value
        End Set
    End Property

    Private Property Tenor() As Integer
        Get
            Return (CType(ViewState("Tenor"), Integer))
        End Get
        Set(ByVal Value As Integer)
            ViewState("Tenor") = Value
        End Set
    End Property

    Private Property InstallmentScheme() As String
        Get
            Return (CType(ViewState("InstallmentScheme"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("InstallmentScheme") = Value
        End Set
    End Property
    Private Property Product() As String
        Get
            Return (CType(ViewState("Product"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("Product") = Value
        End Set
    End Property
    Private Property strCustomerid() As String
        Get
            Return (CType(ViewState("strCustomerid"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("strCustomerid") = Value
        End Set
    End Property
    Private Property dtgData() As DataTable
        Get
            Return (CType(ViewState("dtgData"), DataTable))
        End Get
        Set(ByVal Value As DataTable)
            ViewState("dtgData") = Value
        End Set
    End Property
    Private Property CrossDefault() As DataTable
        Get
            Return (CType(ViewState("CrossDefault"), DataTable))
        End Get
        Set(ByVal Value As DataTable)
            ViewState("CrossDefault") = Value
        End Set
    End Property
    Private Property Term() As DataTable
        Get
            Return (CType(ViewState("Term"), DataTable))
        End Get
        Set(ByVal Value As DataTable)
            ViewState("Term") = Value
        End Set
    End Property
    Private Property Condition() As DataTable
        Get
            Return (CType(ViewState("Condition"), DataTable))
        End Get
        Set(ByVal Value As DataTable)
            ViewState("Condition") = Value
        End Set
    End Property

    Private Property InvoiceNo() As String
        Get
            Return CType(ViewState("InvoiceNo"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("InvoiceNo") = Value
        End Set
    End Property
    Private Property InvoiceSeqNo() As Int64
        Get
            Return CType(ViewState("InvoiceSeqNo"), Int64)
        End Get
        Set(ByVal Value As Int64)
            ViewState("InvoiceSeqNo") = Value
        End Set
    End Property
#End Region

#Region "Constanta"
    Dim myDataTable As DataTable
    Dim myDataColumn As DataColumn
    Dim myDataRow As DataRow
    Dim PokokHutang() As Double
    Dim Bunga() As Double
    Dim PokokBunga() As Double
    Dim HitungUlangSisa As Boolean = False
    Dim HitungUlangKurang As Boolean = False
    Dim SisaPrincipal As Double
    Dim KurangPrincipal As Double
    Dim dblInterestTotal As Double
    Dim i As Integer

    Private oRescheduling As New Parameter.Rescheduling
    Private oCustomClass As New Parameter.DChange
    Dim m_controller As New FinancialDataController
    Dim m_controllerResc As New ReschedulingController
    Dim Entities As New Parameter.FinancialData
#End Region

    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        Me.FormID = "RestructFactAndMU"
        If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
            If Not IsPostBack Then
                Me.ApplicationID = Request.QueryString("ApplicationId")
                Me.BranchID = Request.QueryString("branchID")
                Me.sdate = Request.QueryString("sdate")
                Me.SeqNo = CInt(Request.QueryString("seqno"))
                Me.No = CInt(Me.SeqNo)
                Me.InvoiceNo = Request.QueryString("InvoiceNo")
                Me.InvoiceSeqNo = Request.QueryString("InvoiceSeqNo")
                Dim strRes As String = Request.QueryString("strRes")

                DoBind()
                oRescheduling = Session(strRes)

                With oRescheduling
                    lblPartialPrepayment.Text = FormatNumber(.PartialPay, 2)
                    lblAdmFee.Text = FormatNumber(.AdminFee, 2)
                    lblInstallLC.Text = FormatNumber(.LcInstallment, 2)
                    lblInstallCF.Text = FormatNumber(.InstallmentCollFee, 2)
                    lblInsuranceLC.Text = FormatNumber(.LcInsurance, 2)
                    lblInsuranceCF.Text = FormatNumber(.InsuranceCollFee, 2)
                    lblPDCBounceFee.Text = FormatNumber(.PDCBounceFee, 2)
                    lblSTNKRenewal.Text = FormatNumber(.STNKRenewalFee, 2)
                    InsuranceCF.Text = FormatNumber(.InsuranceClaimExpense, 2)
                    lblRepossesionFee.Text = FormatNumber(.RepossessionFee, 2)
                    lblTotAdvAmt.Text = FormatNumber(.TotAdvAmt, 2)
                    chkAdminFee.Checked = .chkPotong
                    lblTotalAmountToBePaid.Text = FormatNumber(.TotAdvAmt, 2)
                    lblPrepaidAmount.Text = FormatNumber(Me.Prepaid, 2)
                    lblBalanceAmount.Text = FormatNumber(Me.Prepaid - .TotAdvAmt, 2)
                    lblTotDiscAmt.Text = FormatNumber(.TotDiscamt, 2)
                    lblEffectiveRate.Text = FormatNumber(.Rate, 0)
                    lblInstallmentAmount.Text = FormatNumber(.InstallmentAmount, 2)
                    lblReason.Text = .ReasonDescription
                    lblToBe.Text = .RequestTo
                    lblUcNotes.Text = .Notes
                    lblTenor.Text = CStr(.Tenor)
                    lblInstallmentNum.Text = .PODateTo
                    lblFlatRate.Text = .flat & "% per " & lblTenor.Text & " month "
                    lblNPA.Text = FormatNumber(.NewPrincipleAmount, 2)
                    lblPaymentFrequency.Text = .PaymentFreq
                    lblNumberOfStep.Text = .lblNumOfStep
                    lblStep.Text = CStr(.NoStep)
                    lblCummulative.Text = .lblCumm
                    lbCumm.Text = CStr(.Cummulative)

                    lblDiscOutstandingPrincipal.Text = FormatNumber(.OutstandingPrincipal, 2)
                    lblDiscOutstandingInterest.Text = FormatNumber(.OutStandingInterest, 2)
                    lblDiscOutstandingLC.Text = FormatNumber(.LcInstallment, 2)
                    lblDiscAccruedInterest.Text = FormatNumber(.AccruedInterest, 2)
                    lblDiscInstallmentDue.Text = FormatNumber(.InstallmentDue, 2)
                    lblTotAdvAmount.Text = FormatNumber(.NewPrincipleAmount, 2)

                    lblPerjanjianNo.Text = .PerjanjianNo
                    lblPPh.Text = FormatNumber(.PPh, 2)

                    Me.PartialPay = .PartialPay
                    Me.AdminFee = .AdminFee

                    Me.StepUpDownType = .txtSearch
                    Me.InstallmentScheme = .InstallmentScheme
                    If Me.InterestType = "FX" Then
                        If Me.InstallmentScheme = "ST" Then
                            If Me.StepUpDownType = "NM" Then
                                lblStep.Visible = True
                                lblNumberOfStep.Visible = True
                                lbCumm.Visible = False
                                lblCummulative.Visible = False
                            ElseIf Me.StepUpDownType = "RL" Then
                                lblStep.Visible = False
                                lblNumberOfStep.Visible = False
                                lbCumm.Visible = True
                                lblCummulative.Visible = True
                            ElseIf Me.StepUpDownType = "LS" Then
                                lblStep.Visible = True
                                lblNumberOfStep.Visible = True
                                lbCumm.Visible = True
                                lblCummulative.Visible = True
                            End If
                        Else
                            lblStep.Visible = False
                            lbCumm.Visible = False
                            lblNumberOfStep.Visible = False
                            lblCummulative.Visible = False
                        End If
                    End If

                    If Me.InterestType = "FL" Then
                        lblStep.Visible = False
                        lbCumm.Visible = False
                        lblNumberOfStep.Visible = False
                        lblCummulative.Visible = False
                    End If


                    Me.ReasonID = .ReasonID
                    Me.ReasonTypeID = .ReasonTypeID

                    Me.dtgData = .ListData
                    Me.CrossDefault = .ListCrossDefault
                    Me.Term = .ListTerm
                    Me.Condition = .ListCondition
                    Me.FlatRate = CDbl(.flat)
                    Me.payFreq = CInt(.PayFreq)
                    Me.NewTenor = .NewTenor

                    If Me.dtgData.Rows.Count = 0 Then
                        pnlViewST.Visible = False
                        dtgViewInstallment.Visible = False
                    Else
                        dtgViewInstallment.DataSource = Me.dtgData
                        dtgViewInstallment.DataBind()
                    End If
                    dtgCrossDefault.DataSource = Me.CrossDefault
                    dtgCrossDefault.DataBind()
                    dtgTC.DataSource = Me.Term
                    dtgTC.DataBind()
                    dtgTC2.DataSource = Me.Condition
                    dtgTC2.DataBind()
                    If Me.InstallmentScheme = "IR" Then Me.MyDataSet = .MydataSet
                End With

                'InitiateUCnumberFormat(txtEffRate, True, True)
                'txtEffRate.RangeValidatorMinimumValue = "0"
                'txtEffRate.RangeValidatorMaximumValue = "99"
            End If
        End If
    End Sub

    'Private Sub InitiateUCnumberFormat(ByVal uc As ucNumberFormat, ByVal rfvEffRate As Boolean, ByVal rvEffRate As Boolean)
    '    With uc
    '        .RequiredFieldValidatorEnable = rfvEffRate
    '        .RangeValidatorEnable = rvEffRate
    '        .TextCssClass = "numberAlign medium_text"
    '    End With
    'End Sub

#Region "Dobind"
    Sub DoBind()
        Dim totalPrepayment As Double
        lbljudul.Text = Me.sdate
        With oPaymentInfo
            '.IsTitle = True
            .ApplicationID = Me.ApplicationID
            .ValueDate = ConvertDate2(Me.sdate)
            .PrepaymentType = "DI"
            .BranchID = Me.BranchID.Trim
            '.IsPenaltyTerminationShow = False
            .IsPenaltyTerminationShow = True
            .InvoiceNo = Me.InvoiceNo
            .InvoiceSeqNo = Me.InvoiceSeqNo
            '.PaymentInfo()
            .PaymentInfoFactAndMDKJ()

            Me.AgreementNo = .AgreementNo
            Me.strCustomerid = .CustomerID
            Me.CustomerType = .CustomerType
            lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(Me.ApplicationID.Trim) & "')"
            lblAgreementNo.Text = .AgreementNo
            lblCustName.Text = .CustomerName
            lblCustName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(Me.strCustomerid.Trim) & "')"
            totalPrepayment = .TotalPrepaymentAmount - .MaximumPenaltyRate
            lblStopAccruedAmount.Text = FormatNumber(totalPrepayment, 2)
            lblOutstandingPrincipal.Text = FormatNumber(.MaximumOutStandingPrincipal, 2)
            lblOutstandingInterest.Text = FormatNumber(.MaximumOutStandingInterest, 2)
            lblOutstandingLC.Text = FormatNumber(.MaximumLCInstallFee, 2)
            lblAccruedInterest.Text = FormatNumber(.AccruedInterest, 2)
            lblInstallmentDue.Text = FormatNumber(.InstallmentDueAmount, 2)
            'lblTotAdvAmount.Text = FormatNumber(.MaximumOutStandingPrincipal + .MaximumOutStandingInterest + .MaximumLCInstallFee + .AccruedInterest + .InstallmentDueAmount - Me.PartialPay + Me.AdminFee, 2)

            'Me.AccruedAmount = totalPrepayment
            Me.Prepaid = .ContractPrepaidAmount

        End With
        GetList()

    End Sub
#End Region

#Region " Sub & Function "
    Public Function GetFlatRate(ByVal dblInterestTotal As Double, ByVal dblNTF As Double, ByVal intPayFreq As Integer, ByVal intNumInstallment As Integer) As Double
        Return (dblInterestTotal * 1200) / (dblNTF * CDbl(lblTenor.Text))
    End Function
    Public Sub BuatTable()
        myDataTable = New DataTable("AmortTable")

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Int32")
        myDataColumn.ColumnName = "No"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.String")
        myDataColumn.ColumnName = "DueDate"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "Installment"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "Principal"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "Interest"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "PrincBalance"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "PrincInterest"
        myDataTable.Columns.Add(myDataColumn)

        'myDataColumn = New DataColumn
        'myDataColumn.DataType = System.Type.GetType("System.Double")
        'myDataColumn.ColumnName = "InterestTotal"
        'myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "InstallmentSisa"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Int32")
        myDataColumn.ColumnName = "StepFrom"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Int32")
        myDataColumn.ColumnName = "StepTo"
        myDataTable.Columns.Add(myDataColumn)
    End Sub
    Public Function MakeAmortTable2(ByVal intNumInstallment As Integer, ByVal dblInstAmt As Double, ByVal dblNTF As Double, ByVal dblRunRate As Double, ByVal intPayFreq As Integer) As DataSet
        'Regular Installment - Arrear - No Grace Period
        ReDim PokokHutang(intNumInstallment + 1)
        ReDim Bunga(intNumInstallment + 1)
        ReDim PokokBunga(intNumInstallment + 1)
        Dim TotalPrincipal As Double
        Dim tempdate As Date
        Dim j As Integer = 0
        Bunga(0) = 0
        PokokHutang(0) = dblNTF
        'Me.newNo = (Me.No - 1)
        'Me.MinSeqNo = Me.newNo - 1
        'fix
        Me.MinSeqNo = -1
        For i = 1 To intNumInstallment + 1
            myDataRow = myDataTable.NewRow()
            If i = 1 Then 'Installment I
                myDataRow("No") = 0 'Me.SeqNo
                myDataRow("Installment") = 0
                myDataRow("Principal") = 0
                myDataRow("Interest") = 0
            Else
                myDataRow("No") = Me.MinSeqNo + i
                myDataRow("Installment") = dblInstAmt
                myDataRow("Interest") = Math.Round((PokokHutang(i - 1) * dblRunRate) / (m_controller.GetTerm(intPayFreq) * 100), 0)
                myDataRow("Principal") = Math.Round(dblInstAmt - CDbl(myDataRow("Interest")), 0)
            End If
            If i = 1 Then
                myDataRow("PrincBalance") = PokokHutang(0)
            ElseIf i = intNumInstallment + 1 Then
                myDataRow("PrincBalance") = 0
            Else
                myDataRow("PrincBalance") = Math.Round(PokokHutang(i - 1) - CDbl(myDataRow("Principal")), 0)
            End If
            myDataRow("PrincInterest") = 0
            PokokHutang(i) = CDbl(myDataRow("PrincBalance"))
            Bunga(i) = CDbl(myDataRow("Interest"))
            TotalPrincipal = Math.Round(TotalPrincipal + CDbl(myDataRow("Principal")), 0)

            'error due date tidak ke generate
            'If DateDiff(DateInterval.Day, Me.BusinessDate, ConvertDate2(Me.sdate)) >= 0 Then

            'ini dibuka jika effective date boleh lebih kecil dari business date
            If DateDiff(DateInterval.Day, ConvertDate2(Me.sdate), Me.BusinessDate) >= 0 Then

                j = j + 1
                If j = 1 Then
                    tempdate = ConvertDate2(Me.sdate)
                Else
                    tempdate = DateAdd(DateInterval.Month, CDbl(Me.payFreq), tempdate)
                End If
                myDataRow("DueDate") = tempdate.ToString("yyyyMMdd")
            End If

            myDataTable.Rows.Add(myDataRow)
        Next i

        If TotalPrincipal > dblNTF Then
            SisaPrincipal = Math.Round(TotalPrincipal - dblNTF, 0)
        Else
            KurangPrincipal = Math.Round(dblNTF - TotalPrincipal, 0)
        End If

        If SisaPrincipal > 0 Then
            Bunga(intNumInstallment + 1) = Math.Round(Bunga(intNumInstallment + 1) + SisaPrincipal, 0)
        End If
        If KurangPrincipal > 0 Then
            Bunga(intNumInstallment + 1) = Math.Round(Bunga(intNumInstallment + 1) - KurangPrincipal, 0)
        End If

        PokokBunga(intNumInstallment + 1) = 0

        For i = intNumInstallment To 0 Step -1
            PokokBunga(i) = Math.Round(Bunga(i + 1) + PokokBunga(i + 1), 0)
            dblInterestTotal = Math.Round(dblInterestTotal + Bunga(i + 1), 0)
        Next

        Dim myDataSet As DataSet
        myDataSet = New DataSet

        myDataSet.Tables.Add(myDataTable)

        Dim custRow As DataRow

        i = 1
        For Each custRow In myDataSet.Tables("AmortTable").Rows
            If i <= intNumInstallment + 1 Then
                custRow("PrincInterest") = PokokBunga(i)
                'custRow("InterestTotal") = dblInterestTotal
                i = i + 1
            End If
        Next

        For Each custRow In myDataSet.Tables("AmortTable").Rows
            If CInt(custRow("No")) = intNumInstallment Then
                If SisaPrincipal > 0 Then
                    custRow("Principal") = Math.Round(CDbl(custRow("Principal")) - SisaPrincipal, 0)
                    custRow("Interest") = Math.Round(CDbl(custRow("Interest")) + SisaPrincipal, 0)
                    HitungUlangSisa = True
                End If
                If KurangPrincipal > 0 Then
                    custRow("Principal") = Math.Round(CDbl(custRow("Principal")) + KurangPrincipal, 0)
                    custRow("Interest") = Math.Round(CDbl(custRow("Interest")) - KurangPrincipal, 0)
                    HitungUlangKurang = True
                End If
            End If
        Next
        Return myDataSet
    End Function
#End Region

#Region " Data Bound"
    Private Sub dtgTC2_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgTC2.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            CType(e.Item.FindControl("lblTCNo2"), Label).Text = CStr(e.Item.ItemIndex + 1)
        End If
    End Sub
    Private Sub dtgTC_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgTC.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            CType(e.Item.FindControl("lblTCNo"), Label).Text = CStr(e.Item.ItemIndex + 1)
        End If
    End Sub
    Private Sub dtgCrossDefault_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgCrossDefault.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            CType(e.Item.FindControl("lblCDNo"), Label).Text = CStr(e.Item.ItemIndex + 1)
        End If
    End Sub

#End Region

#Region "GetList"
    Sub GetList()
        Dim DController As New DChangeController
        oCustomClass.ApplicationID = Me.ApplicationID
        oCustomClass.BranchId = Me.sesBranchId.Replace("'", "")
        oCustomClass.strConnection = GetConnectionString()
        oCustomClass.InvoiceNo = Me.InvoiceNo
        oCustomClass.InvoiceSeqNo = Me.InvoiceSeqNo
        'oCustomClass = DController.GetList(oCustomClass)
        oCustomClass = DController.GetListFactAndMU(oCustomClass)
        With oCustomClass
            lblInterestType.Text = .InterestTypeDesc.Trim
            Me.InterestType = .InterestType
            lblProduct.Text = .ProductDesc
            lblPackage.Text = .ProductOfferingDesc
            lblInstallScheme.Text = .InstallmentSchemeDesc
            lblFinanceType.Text = .FinanceTypeDesc
            lblGuarantor.Text = .GuarantorName
            Me.GuarantorID = .GuarantorID
            lblEffRate.Text = CStr(.EffectiveRate)
            lblPaymentFreq.Text = .PaymentFrequency
            lblPaymentFrequency.Text = .PaymentFrequency
            Select Case .PaymentFrequency
                Case "1"
                    lblPaymentFreq.Text = "Monthly"
                    lblPaymentFrequency.Text = "Monthly"
                Case "2"
                    lblPaymentFreq.Text = "Bimonthly"
                    lblPaymentFrequency.Text = "Bimonthly"
                Case "3"
                    lblPaymentFreq.Text = "Quarterly"
                    lblPaymentFrequency.Text = "Quarterly"
                Case "6"
                    lblPaymentFreq.Text = "Semi Annualy"
                    lblPaymentFrequency.Text = "Semi Annualy"
            End Select
            lblInstallmentNo.Text = FormatNumber(.NextInstallmentNumber, 0) & " of " & FormatNumber(.NumOfInstallment, 0)
            lblReschedNo.Text = FormatNumber(.ReschedulingNo, 0)

        End With
    End Sub
#End Region

    Private Sub imbSave_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbSave.Click
        Dim FlatRate As Double
        Dim dtEntity As New DataTable
        Dim oFinancialData As New Parameter.FinancialData
        Dim oRescheduling As New Parameter.Rescheduling
        Dim oData1 As New DataTable
        Dim oData2 As New DataTable
        Dim oData3 As New DataTable
        Dim oData4 As New DataTable

        If CInt(txtEffRate.Text) <= 0 Then
            ShowMessage(lblMessage, "Bunga Effective harus >= 0 ", True)
            Exit Sub
        End If

        BuatTable()
        'Me.MinSeqNo = Me.NewTenor - 1
        Me.NewNum = Me.NewTenor

        If Me.InstallmentScheme = "RF" Then
            oFinancialData.MydataSet = MakeAmortTable2(CInt(Me.NewTenor), CDbl(lblInstallmentAmount.Text), CDbl(lblNPA.Text), CDbl(lblEffectiveRate.Text), CInt(Me.payFreq))
        End If

        If Me.InstallmentScheme = "RF" Then  'untuk regular  
            If HitungUlangSisa Then
                dblInterestTotal = Math.Round(dblInterestTotal + SisaPrincipal, 2)
                oFinancialData.InterestTotal = dblInterestTotal
            End If
            If HitungUlangKurang Then
                dblInterestTotal = Math.Round(dblInterestTotal - KurangPrincipal, 2)
                oFinancialData.InterestTotal = dblInterestTotal
            End If
        End If

        If Me.InstallmentScheme = "RF" Then  'untuk regular    
            FlatRate = Math.Round(GetFlatRate(dblInterestTotal, CDbl(lblNPA.Text), CInt(Me.payFreq), CInt(lblTenor.Text)), 5)
            oFinancialData.FlatRate = FlatRate
            oFinancialData.installmentamount = CDbl(lblInstallmentAmount.Text)
            oFinancialData.NumOfInstallment = CInt(lblInstallmentNum.Text)
            oFinancialData.FloatingPeriod = "0"
        End If

        Try
            With oFinancialData
                .strConnection = GetConnectionString()
                .BranchId = Me.BranchID
                .ApplicationID = Me.ApplicationID
                '.EffectiveRate = CDbl(lblEffectiveRate.Text)
                .EffectiveRate = CDbl(txtEffRate.Text)
                .PaymentFrequency = CStr(Me.payFreq)
                .Tenor = CInt(lblTenor.Text)
                .BusinessDate = Me.BusinessDate
                .EffectiveDate = ConvertDate2(Me.sdate)
                .CustomerID = Me.strCustomerid
                .FlatRate = Me.FlatRate
                .InstallmentScheme = Me.InstallmentScheme
                '.NumOfInstallment = CInt(lblInstallmentNum.Text)
                .NumOfInstallment = CInt(lblTenor.Text)
                .OutstandingTenor = Me.NewTenor
                .Tenor = CInt(lblTenor.Text)

                .installmentamount = CDbl(lblInstallmentAmount.Text)
                .PartialPrepaymentAmount = CDbl(lblPartialPrepayment.Text)
                .AdministrationFee = CDbl(lblAdmFee.Text)
                .chkPotong = chkAdminFee.Checked
                .TotalDiscount = CDbl(lblTotDiscAmt.Text)
                .LCInstallmentAmountDisc = CDbl(lblDiscOutstandingLC.Text)
                .AccruedInterestDisc = CDbl(lblDiscAccruedInterest.Text)
                .InstallmentDueDisc = CDbl(lblDiscInstallmentDue.Text)
                .OutstandingInterestDisc = CDbl(lblDiscOutstandingInterest.Text)
                .OutstandingPrincipalDisc = CDbl(lblDiscOutstandingPrincipal.Text)
                .TotalAmountToBePaid = CDbl(lblTotAdvAmt.Text)

                .PerjanjianNo = lblPerjanjianNo.Text
                .PPh = CDbl(lblPPh.Text)
                .InvoiceNo = Me.InvoiceNo
                .InvoiceSeqNo = Me.InvoiceSeqNo

                .ContractPrepaidAmount = CDbl(lblPrepaidAmount.Text)
                .OutstandingPrincipalNew = CDbl(lblNPA.Text)
                .OutstandingInterestNew = 0
                .OutstandingPrincipalOld = oPaymentInfo.MaximumInstallment
                .OutstandingInterestOld = 0
                .InstallmentDue = oPaymentInfo.InstallmentDueAmount
                .InsuranceDue = oPaymentInfo.MaximumInsurance
                .LcInstallment = oPaymentInfo.MaximumLCInstallFee
                .LcInsurance = oPaymentInfo.MaximumLCInsuranceFee
                .InsuranceCollFee = oPaymentInfo.MaximumInsuranceCollFee
                .InstallmentCollFee = oPaymentInfo.MaximumInstallCollFee
                .PDCBounceFee = oPaymentInfo.MaximumPDCBounceFee
                .InsuranceClaimExpense = oPaymentInfo.MaximumInsuranceClaimFee
                .CollectionExpense = oPaymentInfo.MaximumReposessionFee
                .AccruedInterest = oPaymentInfo.AccruedInterest
                .STNKRenewalFee = oPaymentInfo.MaximumSTNKRenewalFee

                '.LCInstallmentAmountDisc = CDbl(lblInstallLC.Text) 
                .LCInsuranceAmountDisc = CDbl(lblInsuranceLC.Text)
                .InstallCollectionFeeDisc = CDbl(lblInstallCF.Text)
                .InsurCollectionFeeDisc = CDbl(lblInsuranceCF.Text)
                .PDCBounceFeeDisc = CDbl(lblPDCBounceFee.Text)
                .STNKFeeDisc = CDbl(lblSTNKRenewal.Text)
                .InsuranceClaimExpenseDisc = CDbl(InsuranceCF.Text)
                .RepossesFeeDisc = CDbl(lblRepossesionFee.Text)
                '.TotalDiscount = CDbl(lblTotDiscAmt.Text)
                '.TotalAmountToBePaid = CDbl(lblNPA.Text)
                .TotalOSAR = CDbl(lblStopAccruedAmount.Text)

                .ReasonTypeID = Me.ReasonTypeID
                .TR_Nomor = "-"
                .RequestTo = lblToBe.Text
                .ReasonID = Me.ReasonID
                .Notes = lblUcNotes.Text
                .Status = "R"
                .LoginId = Me.Loginid
                .GuarantorID = Me.GuarantorID
                .InterestType = Me.InterestType
                .txtSearch = ""
                oData1 = Me.Term
                oData2 = Me.Condition
                oData3 = Me.CrossDefault
                oData4 = Me.dtgData
                .NTF = CDbl(lblNPA.Text)
                .SupplierRate = CDbl(lblEffectiveRate.Text)
                .FirstInstallment = "AR"
                .NumofActiveAgreement = Me.NewTenor
                .GracePeriod = 0
                .GracePeriodType = "0"
                .DiffRate = 0
                .StepUpStepDownType = Me.StepUpDownType
                .InstallmentScheme = Me.InstallmentScheme
                If Me.InstallmentScheme = "IR" Then .MydataSet = Me.MyDataSet
                .Flag = "RFAM"
            End With

            'If Me.InstallmentScheme = "RF" Then
            '    oFinancialData = m_controllerResc.SaveReschedulingData(oFinancialData, oData1, oData2, oData3)
            'ElseIf Me.InstallmentScheme = "IR" Then
            '    oFinancialData = m_controllerResc.SaveReschedulingIRR(oFinancialData, oData1, oData2, oData3)
            'ElseIf Me.InstallmentScheme = "ST" Then
            'oFinancialData = m_controllerResc.SaveReschedulingStepUpStepDown(oFinancialData, oData1, oData2, oData3, oData4)
            'Else
            '    oFinancialData = m_controllerResc.SaveReschedulingEP(oFinancialData, oData1, oData2, oData3)
            'End If
            If Me.InstallmentScheme = "ST" Then
                oFinancialData = m_controllerResc.SaveReschedulingStepUpStepDownFactAndMDKJ(oFinancialData, oData1, oData2, oData3, oData4)
            End If
            Response.Redirect("RestructFactAndMU.aspx?Message=")
        Catch ex As Exception
            Response.Redirect("RestructFactAndMU.aspx?Message=" + ex.Message + "")
        End Try
    End Sub
    Private Sub imbCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles imbCancel.Click
        Server.Transfer("RestructFactAndMU.aspx")
    End Sub

End Class