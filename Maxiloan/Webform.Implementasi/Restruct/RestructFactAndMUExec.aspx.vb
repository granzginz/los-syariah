﻿#Region "Import"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.General
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
Imports System.Data.SqlClient
#End Region

Public Class RestructFactAndMUExec
    Inherits Maxiloan.Webform.AccMntWebBased
    Protected WithEvents oPaymentInfo As UcFullPrepayInfo

#Region "Property"
    Private Property AccruedAmount() As Double
        Get
            Return (CType(ViewState("AccruedAmount"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("AccruedAmount") = Value
        End Set
    End Property
    Property Prepaid() As Double
        Get
            Return CDbl(ViewState("Prepaid"))
        End Get
        Set(ByVal Value As Double)
            ViewState("Prepaid") = Value
        End Set
    End Property
    Property RequestNo() As String
        Get
            Return ViewState("RequestNo").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("RequestNo") = Value
        End Set
    End Property
    Property EffectiveDate() As String
        Get
            Return ViewState("EffectiveDate").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("EffectiveDate") = Value
        End Set
    End Property
    Private Enum ProcessMode
        Cancel = 0
        Execute = 1
        View = 2
    End Enum
    Private Property Mode() As ProcessMode
        Get
            Return (CType(ViewState("Mode"), ProcessMode))
        End Get
        Set(ByVal Value As ProcessMode)
            ViewState("Mode") = Value
        End Set
    End Property
    Private Property No() As Integer
        Get
            Return (CType(ViewState("No"), Integer))
        End Get
        Set(ByVal Value As Integer)
            ViewState("No") = Value
        End Set
    End Property

    Private Property NewTenor() As Integer
        Get
            Return (CType(ViewState("NewTenor"), Integer))
        End Get
        Set(ByVal Value As Integer)
            ViewState("NewTenor") = Value
        End Set
    End Property
    Property MyDataSet() As DataSet
        Get
            Return (CType(ViewState("MyDataSet"), DataSet))
        End Get
        Set(ByVal Value As DataSet)
            ViewState("MyDataSet") = Value
        End Set
    End Property
    Property StepUpDownType() As String
        Get
            Return ViewState("StepUpDownType").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("StepUpDownType") = Value
        End Set
    End Property
    Private Property MinSeqNo() As Integer
        Get
            Return (CType(ViewState("MinSeqNo"), Integer))
        End Get
        Set(ByVal Value As Integer)
            ViewState("MinSeqNo") = Value
        End Set
    End Property
    Private Property newNo() As Integer
        Get
            Return (CType(ViewState("newNo"), Integer))
        End Get
        Set(ByVal Value As Integer)
            ViewState("newNo") = Value
        End Set
    End Property
    Private Property NewNum() As Integer
        Get
            Return (CType(ViewState("NewNum"), Integer))
        End Get
        Set(ByVal Value As Integer)
            ViewState("NewNum") = Value
        End Set
    End Property
    Private Property SeqNo() As Integer
        Get
            Return (CType(ViewState("SeqNo"), Integer))
        End Get
        Set(ByVal Value As Integer)
            ViewState("SeqNo") = Value
        End Set
    End Property
    Private Property GuarantorID() As String
        Get
            Return (CType(ViewState("GuarantorID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("GuarantorID") = Value
        End Set
    End Property
    Private Property InterestType() As String
        Get
            Return (CType(ViewState("InterestType"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("InterestType") = Value
        End Set
    End Property
    Private Property ReasonID() As String
        Get
            Return (CType(ViewState("ReasonID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("ReasonID") = Value
        End Set
    End Property
    Private Property ReasonTypeID() As String
        Get
            Return (CType(ViewState("ReasonTypeID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("ReasonTypeID") = Value
        End Set
    End Property
    Private Property payFreq() As Integer
        Get
            Return (CType(ViewState("payFreq"), Integer))
        End Get
        Set(ByVal Value As Integer)
            ViewState("payFreq") = Value
        End Set
    End Property
    Private Property PaymentFrequency() As Integer
        Get
            Return (CType(ViewState("PaymentFrequency"), Integer))
        End Get
        Set(ByVal Value As Integer)
            ViewState("PaymentFrequency") = Value
        End Set
    End Property
    Private Property DaysDiff() As Integer
        Get
            Return (CType(ViewState("DaysDiff"), Integer))
        End Get
        Set(ByVal Value As Integer)
            ViewState("DaysDiff") = Value
        End Set
    End Property
    Private Property PartialPay() As Double
        Get
            Return (CType(ViewState("PartialPay"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("PartialPay") = Value
        End Set
    End Property
    Private Property AdminFee() As Double
        Get
            Return (CType(ViewState("AdminFee"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("AdminFee") = Value
        End Set
    End Property

    Private Property FlatRate() As Double
        Get
            Return (CType(ViewState("FlatRate"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("FlatRate") = Value
        End Set
    End Property
    Private Property sdate() As String
        Get
            Return (CType(ViewState("sdate"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("sdate") = Value
        End Set
    End Property

    Private Property Tenor() As Integer
        Get
            Return (CType(ViewState("Tenor"), Integer))
        End Get
        Set(ByVal Value As Integer)
            ViewState("Tenor") = Value
        End Set
    End Property

    Private Property InstallmentScheme() As String
        Get
            Return (CType(ViewState("InstallmentScheme"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("InstallmentScheme") = Value
        End Set
    End Property
    Private Property Product() As String
        Get
            Return (CType(ViewState("Product"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("Product") = Value
        End Set
    End Property
    Private Property strCustomerid() As String
        Get
            Return (CType(ViewState("strCustomerid"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("strCustomerid") = Value
        End Set
    End Property
    Private Property dtgData() As DataTable
        Get
            Return (CType(ViewState("dtgData"), DataTable))
        End Get
        Set(ByVal Value As DataTable)
            ViewState("dtgData") = Value
        End Set
    End Property
    Private Property CrossDefault() As DataTable
        Get
            Return (CType(ViewState("CrossDefault"), DataTable))
        End Get
        Set(ByVal Value As DataTable)
            ViewState("CrossDefault") = Value
        End Set
    End Property
    Private Property Term() As DataTable
        Get
            Return (CType(ViewState("Term"), DataTable))
        End Get
        Set(ByVal Value As DataTable)
            ViewState("Term") = Value
        End Set
    End Property
    Private Property Condition() As DataTable
        Get
            Return (CType(ViewState("Condition"), DataTable))
        End Get
        Set(ByVal Value As DataTable)
            ViewState("Condition") = Value
        End Set
    End Property
    Private Property MaxSeqNo() As Integer
        Get
            Return (CType(ViewState("MaxSeqNo"), Integer))
        End Get
        Set(ByVal Value As Integer)
            ViewState("MaxSeqNo") = Value
        End Set
    End Property
    Private Property DueDate() As Date
        Get
            Return (CType(ViewState("DueDate"), Date))
        End Get
        Set(ByVal Value As Date)
            ViewState("DueDate") = Value
        End Set
    End Property
    Private Property NewNumInst() As Integer
        Get
            Return (CType(ViewState("NewNumInst"), Integer))
        End Get
        Set(ByVal Value As Integer)
            ViewState("NewNumInst") = Value
        End Set
    End Property

    Private Property InvoiceNo() As String
        Get
            Return (CType(ViewState("InvoiceNo"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("InvoiceNo") = Value
        End Set
    End Property

    Private Property InvoiceSeqNo() As Int64
        Get
            Return (CType(ViewState("InvoiceSeqNo"), Int64))
        End Get
        Set(ByVal Value As Int64)
            ViewState("InvoiceSeqNo") = Value
        End Set
    End Property

    Private Property StatusView() As String
        Get
            Return (CType(ViewState("StatusView"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("StatusView") = Value
        End Set
    End Property

    Dim tempInstallmentAmount As Double = 0
    Dim tempPaidAmount As Double = 0
    Dim tempWaivedAmount As Double = 0
    Dim tempLC As Double = 0
    Dim tempPrincipalAmount As Double = 0
    Dim tempInterestAmount As Double = 0
    Dim tempOSPrincipal As Double = 0
    Dim tempOSInterest As Double = 0

    Dim tempPaidPrincipal As Double = 0
    Dim tempPaidInterest As Double = 0
    Dim tempPaidLateCharges As Double = 0

    Dim TotDenda As Label
    Dim TotInstallmentAmount As Label
    Dim TotPaidAmount As Label
    Dim TotWaivedAmount As Label
    Dim TotLC As Label
    Dim TotPrincipalAmount As Label
    Dim TotInterestAmount As Label
    Dim TotOSPrincipal As Label
    Dim TotOSInterest As Label

    Dim totPaidPrincipal As Label
    Dim totPaidInterest As Label
    Dim totPaidLateCharges As Label

#End Region

#Region "Constanta"
    Dim myDataTable As DataTable
    Dim myDataColumn As DataColumn
    Dim myDataRow As DataRow
    Dim PokokHutang() As Double
    Dim Bunga() As Double
    Dim PokokBunga() As Double
    Dim HitungUlangSisa As Boolean = False
    Dim HitungUlangKurang As Boolean = False
    Dim SisaPrincipal As Double
    Dim KurangPrincipal As Double
    Dim dblInterestTotal As Double
    Dim i As Integer

    Private oRescheduling As New Parameter.Rescheduling
    Private oCustomClass As New Parameter.DChange
    Dim m_controller As New FinancialDataController
    Dim m_controllerResc As New ReschedulingController
    Dim Entities As New Parameter.FinancialData
    Dim m_controllerResch As New ReschedulingController
#End Region

    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim TotAdvAmount As Double
        Dim TotDisc As Double
        Dim totalPrepayment As Double
        If SessionInvalid() Then
            Exit Sub
        End If
        Me.FormID = "RestructFactAndMUExec"
        If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
            If Not IsPostBack Then
                'Me.sdate = Request.QueryString("sdate") 
                Me.RequestNo = Request.QueryString("no")
                Me.sdate = Request.QueryString("effectivedate")
                'Me.ApplicationID = Request.QueryString("Applicationid")
                Me.BranchID = Request.QueryString("BranchId")
                'Me.SeqNo = CType(Request.QueryString("seqno"), Integer)
                'Me.CustomerID = Request.QueryString("customerid")
                'Me.EffectiveDate = Request.QueryString("effectivedate")
                Me.InvoiceNo = Request.QueryString("InvoiceNo")
                Me.InvoiceSeqNo = Request.QueryString("InvoiceSeqNo")
                Select Case Request.QueryString("mode")
                    Case "0"
                        Me.Mode = ProcessMode.Cancel
                    Case "1"
                        Me.Mode = ProcessMode.Execute
                    Case "2"
                        Me.Mode = ProcessMode.View
                End Select
                InitializeForm()
                Dim strRes As String = Request.QueryString("strRes")

                'DoBind()


                If Me.sdate = "" Then
                    imbSave.Visible = False
                    imbCancel.Visible = False
                    With Entities
                        .strConnection = GetConnectionString()
                        .RequestWONo = Me.RequestNo
                        .BranchId = Me.sesBranchId.Replace("'", "")
                    End With
                    Me.StatusView = "APR"

                    Entities = m_controllerResch.ViewApprovalRestrcutFactAndMU(Entities)
                    With Entities
                        Me.ApplicationID = .ApplicationID
                        Me.AgreementNo = .Agreementno
                        Me.strCustomerid = .CustomerID
                        Me.CustomerType = .CustomerType

                        lblAdmFee.Text = FormatNumber(.AdministrationFee, 2)
                        lblEffectiveRate.Text = FormatNumber(.EffectiveRate, 4)
                        Select Case .PaymentFrequency
                            Case "1"
                                lblPaymentFrequency.Text = "Monthly"
                            Case "2"
                                lblPaymentFrequency.Text = "Bimonthly"
                            Case "3"
                                lblPaymentFrequency.Text = "Quarterly"
                            Case "6"
                                lblPaymentFrequency.Text = "Semi Annualy"
                        End Select
                        lblInstallmentNum.Text = CStr(.NumOfInstallment)
                        lblTenor.Text = CStr(.Tenor)
                        lblFlatRate.Text = FormatNumber(.FlatRate, 2) & "% per " & lblTenor.Text & " month "
                        lblInstallmentAmount.Text = FormatNumber(.installmentamount, 2)
                        lblPartialPrepayment.Text = FormatNumber(.PartialPrepaymentAmount, 2)
                        lblInstallLC.Text = FormatNumber(.LcInstallment, 2)
                        lblInstallCF.Text = FormatNumber(.InstallmentCollFee, 2)
                        lblInsuranceLC.Text = FormatNumber(.LcInsurance, 2)
                        lblInsuranceCF.Text = FormatNumber(.InsuranceCollFee, 2)
                        lblPDCBounceFee.Text = FormatNumber(.PDCBounceFee, 2)
                        lblSTNKRenewal.Text = FormatNumber(.STNKRenewalFee, 2)
                        lblRepossesionFee.Text = FormatNumber(.RepossessionFee, 2)
                        InsuranceCF.Text = FormatNumber(.InsuranceClaimExpense, 2)
                        lblReason.Text = .ReasonID
                        lblToBe.Text = .RequestTo
                        lblUcNotes.Text = .Notes
                        dtgTC.DataSource = .Data1
                        dtgTC.DataBind()
                        dtgTC2.DataSource = .data2
                        dtgTC2.DataBind()
                        dtgCrossDefault.DataSource = .data3
                        dtgCrossDefault.DataBind()
                        Select Case .chkPotong
                            Case True
                                TotAdvAmount = .PartialPrepaymentAmount
                            Case False
                                TotAdvAmount = .PartialPrepaymentAmount + .AdministrationFee
                        End Select
                        'TotAdvAmount = .PartialPrepaymentAmount + .AdministrationFee
                        TotDisc = .LcInstallment + .InstallmentCollFee + .LcInsurance +
                                             .InsuranceCollFee + .PDCBounceFee + .STNKRenewalFee +
                                             .RepossessionFee + .InsuranceClaimExpense
                        lblTotAdvAmt.Text = FormatNumber(TotAdvAmount, 2)
                        lblTotalAmountToBePaid.Text = FormatNumber(TotAdvAmount, 2)
                        lblPrepaidAmount.Text = FormatNumber(Me.Prepaid, 2)
                        lblBalanceAmount.Text = FormatNumber(Me.Prepaid - TotAdvAmount, 2)
                        'lblTotDiscAmt.Text = FormatNumber(TotDisc, 2)
                        lblTotDiscAmt.Text = FormatNumber(.TotalDiscount, 2)
                        lblNPA.Text = FormatNumber(.OutstandingPrincipalNew, 2)
                        lblDiscOutstandingPrincipal.Text = FormatNumber(.OutstandingPrincipalDisc, 2)
                        lblDiscOutstandingInterest.Text = FormatNumber(.OutstandingInterestDisc, 2)
                        lblDiscOutstandingLC.Text = FormatNumber(.LCInstallmentAmountDisc, 2)
                        lblDiscAccruedInterest.Text = FormatNumber(.AccruedInterestDisc, 2)
                        lblDiscInstallmentDue.Text = FormatNumber(.InstallmentDueDisc, 2)
                        lblTotAdvAmount.Text = FormatNumber(.OutstandingPrincipalNew, 2)
                        chkAdminFee.Checked = .chkPotong
                        lblPerjanjianNo.Text = .PerjanjianNo
                        lblPPh.Text = FormatNumber(.PPh, 2)
                        Me.sdate = .EffectiveDate.ToString("dd/MM/yyyy")
                        Me.InvoiceNo = FormatNumber(.InvoiceNo, 2)
                        Me.InvoiceSeqNo = FormatNumber(.InvoiceSeqNo, 2)
                        ViewInstallment(Me.ApplicationID, Me.RequestNo)
                    End With
                Else
                    Me.ApplicationID = Request.QueryString("Applicationid")
                    Me.BranchID = Request.QueryString("branchid")
                    Me.SeqNo = CType(Request.QueryString("seqno"), Integer)
                    Me.CustomerID = Request.QueryString("customerid")
                    Me.EffectiveDate = Request.QueryString("effectivedate")
                    Me.InvoiceNo = Request.QueryString("InvoiceNo")
                    Me.InvoiceSeqNo = Request.QueryString("InvoiceSeqNo")

                    Me.StatusView = "INQ"
                    With Entities
                        .strConnection = GetConnectionString()
                        .BranchId = Me.BranchID
                        .ApplicationID = Me.ApplicationID
                        .RequestWONo = Me.RequestNo
                    End With

                    Entities = m_controllerResch.ViewRestrcutFactAndMU(Entities)
                    With Entities

                        Me.AgreementNo = .Agreementno
                        Me.strCustomerid = .CustomerID
                        Me.CustomerType = .CustomerType

                        lblAdmFee.Text = FormatNumber(.AdministrationFee, 2)
                        lblEffectiveRate.Text = FormatNumber(.EffectiveRate, 4)
                        Select Case .PaymentFrequency
                            Case "1"
                                lblPaymentFrequency.Text = "Monthly"
                            Case "2"
                                lblPaymentFrequency.Text = "Bimonthly"
                            Case "3"
                                lblPaymentFrequency.Text = "Quarterly"
                            Case "6"
                                lblPaymentFrequency.Text = "Semi Annualy"
                        End Select
                        lblInstallmentNum.Text = CStr(.NumOfInstallment)
                        lblTenor.Text = CStr(.Tenor)
                        lblFlatRate.Text = FormatNumber(.FlatRate, 2) & "% per " & lblTenor.Text & " month "
                        lblInstallmentAmount.Text = FormatNumber(.installmentamount, 2)
                        lblPartialPrepayment.Text = FormatNumber(.PartialPrepaymentAmount, 2)
                        lblInstallLC.Text = FormatNumber(.LcInstallment, 2)
                        lblInstallCF.Text = FormatNumber(.InstallmentCollFee, 2)
                        lblInsuranceLC.Text = FormatNumber(.LcInsurance, 2)
                        lblInsuranceCF.Text = FormatNumber(.InsuranceCollFee, 2)
                        lblPDCBounceFee.Text = FormatNumber(.PDCBounceFee, 2)
                        lblSTNKRenewal.Text = FormatNumber(.STNKRenewalFee, 2)
                        lblRepossesionFee.Text = FormatNumber(.RepossessionFee, 2)
                        InsuranceCF.Text = FormatNumber(.InsuranceClaimExpense, 2)
                        lblReason.Text = .ReasonID
                        lblToBe.Text = .RequestTo
                        lblUcNotes.Text = .Notes
                        dtgTC.DataSource = .Data1
                        dtgTC.DataBind()
                        dtgTC2.DataSource = .data2
                        dtgTC2.DataBind()
                        dtgCrossDefault.DataSource = .data3
                        dtgCrossDefault.DataBind()
                        Select Case .chkPotong
                            Case True
                                TotAdvAmount = .PartialPrepaymentAmount
                            Case False
                                TotAdvAmount = .PartialPrepaymentAmount + .AdministrationFee
                        End Select
                        'TotAdvAmount = .PartialPrepaymentAmount + .AdministrationFee
                        TotDisc = .LcInstallment + .InstallmentCollFee + .LcInsurance +
                                             .InsuranceCollFee + .PDCBounceFee + .STNKRenewalFee +
                                             .RepossessionFee + .InsuranceClaimExpense
                        lblTotAdvAmt.Text = FormatNumber(TotAdvAmount, 2)
                        lblTotalAmountToBePaid.Text = FormatNumber(TotAdvAmount, 2)
                        lblPrepaidAmount.Text = FormatNumber(Me.Prepaid, 2)
                        lblBalanceAmount.Text = FormatNumber(Me.Prepaid - TotAdvAmount, 2)
                        'lblTotDiscAmt.Text = FormatNumber(TotDisc, 2)
                        lblTotDiscAmt.Text = FormatNumber(.TotalDiscount, 2)
                        lblNPA.Text = FormatNumber(.OutstandingPrincipalNew, 2)
                        lblDiscOutstandingPrincipal.Text = FormatNumber(.OutstandingPrincipalDisc, 2)
                        lblDiscOutstandingInterest.Text = FormatNumber(.OutstandingInterestDisc, 2)
                        lblDiscOutstandingLC.Text = FormatNumber(.LCInstallmentAmountDisc, 2)
                        lblDiscAccruedInterest.Text = FormatNumber(.AccruedInterestDisc, 2)
                        lblDiscInstallmentDue.Text = FormatNumber(.InstallmentDueDisc, 2)
                        lblTotAdvAmount.Text = FormatNumber(.OutstandingPrincipalNew, 2)
                        chkAdminFee.Checked = .chkPotong
                        lblPerjanjianNo.Text = .PerjanjianNo
                        lblPPh.Text = FormatNumber(.PPh, 2)

                        'lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(Me.ApplicationID.Trim) & "')"
                        'lblAgreementNo.Text = .Agreementno
                        'lblCustName.Text = .CustomerName
                        ''lblCustName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(Me.strCustomerid.Trim) & "')"
                        'lblCustName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(Me.CustomerID.Trim) & "')"
                        ''totalPrepayment = .TotalPrepaymentAmount - .MaximumPenaltyRate
                        'totalPrepayment = .OutstandingPrincipal + .InstallmentDue + .LcInstallment + .InstallmentCollFee +
                        '                    .InsuranceDue + .InsuranceCollFee + .LcInsurance +
                        '                    .PDCBounceFee + .STNKRenewalFee + .InsuranceClaimExpense +
                        '                    .RepossessionFee + .AccruedInterest + .TerminationPenalty - .FaktorPengurang

                        'lblStopAccruedAmount.Text = FormatNumber(totalPrepayment, 2)
                        'lblOutstandingPrincipal.Text = FormatNumber(.OutstandingPrincipal, 2)
                        'lblOutstandingInterest.Text = FormatNumber(.OutStandingInterest, 2)
                        'lblOutstandingLC.Text = FormatNumber(.LcInstallment, 2)
                        'lblAccruedInterest.Text = FormatNumber(.AccruedInterest, 2)
                        'lblInstallmentDue.Text = FormatNumber(.InstallmentDue, 2)
                        'Me.Prepaid = .ContractPrepaidAmount

                        ViewInstallment(Me.ApplicationID, Me.RequestNo)
                    End With
                End If


                DoBind()
            End If
        End If
    End Sub

    Private Sub InitializeForm()
        If Me.Mode = ProcessMode.Cancel Then
            imbExecute.Visible = False
            imbCancel.Visible = True
            imbSave.Visible = True
            imbBack.Visible = False
        ElseIf Me.Mode = ProcessMode.Execute Then
            imbExecute.Visible = True
            imbCancel.Visible = True
            imbSave.Visible = False
            imbBack.Visible = False
        ElseIf Me.Mode = ProcessMode.View Then
            imbExecute.Visible = False
            imbCancel.Visible = False
            imbSave.Visible = False
            imbBack.Visible = True
        End If
    End Sub

    Private Sub imbBack_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbBack.Click
        Response.Redirect("RestructFactAndMUInq.aspx")
    End Sub


    Private Sub dtgViewInstallment_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgViewInstallment.ItemDataBound
        Dim lb As New Label

        If e.Item.ItemIndex >= 0 Then

            lb = CType(e.Item.FindControl("lblInstallment"), Label)
            tempInstallmentAmount = tempInstallmentAmount + CDbl(lb.Text)

            lb = CType(e.Item.FindControl("lblPrincipalAmount"), Label)
            tempPrincipalAmount = tempPrincipalAmount + CDbl(lb.Text)

            lb = CType(e.Item.FindControl("lblInterestAmount"), Label)
            tempInterestAmount = tempInterestAmount + CDbl(lb.Text)

            lb = CType(e.Item.FindControl("lblOsPrincipal"), Label)
            tempOSPrincipal = tempOSPrincipal + CDbl(lb.Text)

            lb = CType(e.Item.FindControl("LblOSInterest"), Label)
            tempOSInterest = tempOSInterest + CDbl(lb.Text)

        End If

        If e.Item.ItemType = ListItemType.Footer Then

            TotInstallmentAmount = CType(e.Item.FindControl("lblTotInstallmentAmount"), Label)
            TotInstallmentAmount.Text = FormatNumber(tempInstallmentAmount, 2)

            TotPrincipalAmount = CType(e.Item.FindControl("lblTotPrincipalAmount"), Label)
            TotPrincipalAmount.Text = FormatNumber(tempPrincipalAmount, 2)

            TotInterestAmount = CType(e.Item.FindControl("lblTotInterestAmount"), Label)
            TotInterestAmount.Text = FormatNumber(tempInterestAmount, 2)

            TotOSPrincipal = CType(e.Item.FindControl("lblTotOSPrincipal"), Label)
            TotOSPrincipal.Text = FormatNumber(tempOSPrincipal, 2)

            TotOSInterest = CType(e.Item.FindControl("lblTotOSInterest"), Label)
            TotOSInterest.Text = FormatNumber(tempOSInterest, 2)

        End If
    End Sub


    Private Sub ViewInstallment(ByVal ApplicationID As String, ByVal RequestNo As String)
        Dim dtGrid As New DataTable
        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(GetConnectionString)
        Dim objReader As SqlDataReader

        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.CommandText = "spViewAmortisasiReschFactAndMU"
            objCommand.Connection = objConnection
            objCommand.Parameters.Add("@ApplicationID", SqlDbType.VarChar, 20).Value = ApplicationID.Trim
            objCommand.Parameters.Add("@RequestNo", SqlDbType.VarChar, 20).Value = RequestNo.Trim
            objReader = objCommand.ExecuteReader

            dtgViewInstallment.DataSource = objReader
            dtgViewInstallment.DataBind()
            objReader.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        Finally
            objCommand.Dispose()
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try
    End Sub



#Region "Dobind"
    Sub DoBind()
        Dim totalPrepayment As Double
        lbljudul.Text = Me.sdate
        With oPaymentInfo
            .ApplicationID = Me.ApplicationID
            .ValueDate = ConvertDate2(Me.sdate)
            .PrepaymentType = "DI"
            '.BranchID = Me.BranchID.Trim
            .BranchID = Me.sesBranchId.Replace("'", "")
            '.IsPenaltyTerminationShow = False
            .IsPenaltyTerminationShow = True
            .InvoiceNo = Me.InvoiceNo
            .InvoiceSeqNo = Me.InvoiceSeqNo
            .PaymentInfoFactAndMDKJ()
            '.PaymentInfo()

            Me.AgreementNo = .AgreementNo
            Me.strCustomerid = .CustomerID
            Me.CustomerType = .CustomerType
            lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(Me.ApplicationID.Trim) & "')"
            lblAgreementNo.Text = .AgreementNo
            lblCustName.Text = .CustomerName
            lblCustName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(Me.strCustomerid.Trim) & "')"
            totalPrepayment = .TotalPrepaymentAmount - .MaximumPenaltyRate
            lblStopAccruedAmount.Text = FormatNumber(totalPrepayment, 2)
            lblOutstandingPrincipal.Text = FormatNumber(.MaximumOutStandingPrincipal, 2)
            lblOutstandingInterest.Text = FormatNumber(.MaximumOutStandingInterest, 2)
            lblOutstandingLC.Text = FormatNumber(.MaximumLCInstallFee, 2)
            lblAccruedInterest.Text = FormatNumber(.AccruedInterest, 2)
            lblInstallmentDue.Text = FormatNumber(.InstallmentDueAmount, 2)
            Me.Prepaid = .ContractPrepaidAmount

        End With
        GetList()

    End Sub
#End Region
#Region "GetList"
    Sub GetList()
        Dim DController As New DChangeController
        oCustomClass.ApplicationID = Me.ApplicationID
        oCustomClass.BranchId = Me.sesBranchId.Replace("'", "")
        oCustomClass.strConnection = GetConnectionString()
        oCustomClass.InvoiceNo = Me.InvoiceNo
        oCustomClass.InvoiceSeqNo = Me.InvoiceSeqNo
        'oCustomClass = DController.GetList(oCustomClass)
        oCustomClass = DController.GetListFactAndMU(oCustomClass)
        With oCustomClass
            lblInterestType.Text = .InterestTypeDesc.Trim
            Me.InterestType = .InterestType
            lblProduct.Text = .ProductDesc
            lblPackage.Text = .ProductOfferingDesc
            lblInstallScheme.Text = .InstallmentSchemeDesc
            lblFinanceType.Text = .FinanceTypeDesc
            lblGuarantor.Text = .GuarantorName
            Me.GuarantorID = .GuarantorID
            lblEffRate.Text = CStr(.EffectiveRate)
            lblPaymentFreq.Text = .PaymentFrequency
            lblPaymentFrequency.Text = .PaymentFrequency
            Select Case .PaymentFrequency
                Case "1"
                    lblPaymentFreq.Text = "Monthly"
                    lblPaymentFrequency.Text = "Monthly"
                Case "2"
                    lblPaymentFreq.Text = "Bimonthly"
                    lblPaymentFrequency.Text = "Bimonthly"
                Case "3"
                    lblPaymentFreq.Text = "Quarterly"
                    lblPaymentFrequency.Text = "Quarterly"
                Case "6"
                    lblPaymentFreq.Text = "Semi Annualy"
                    lblPaymentFrequency.Text = "Semi Annualy"
            End Select
            lblInstallmentNo.Text = FormatNumber(.NextInstallmentNumber, 0) & " of " & FormatNumber(.NumOfInstallment, 0)
            lblReschedNo.Text = FormatNumber(.ReschedulingNo, 0)
        End With
    End Sub
#End Region

    Private Sub imbExecute_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbExecute.Click
        Dim tempdate As Date
        Dim Entities As New Parameter.FinancialData
        Dim m_ControllerResc As New ReschedulingController
        If CheckFeature(Me.Loginid, Me.FormID, "EXEC", Me.AppId) Then
            Entities.strConnection = GetConnectionString()
            Entities.ApplicationID = Me.ApplicationID
            Entities.BusinessDate = ConvertDate2(Me.EffectiveDate)
            Entities.BranchId = Me.BranchID
            Entities.InvoiceNo = Me.InvoiceNo
            Entities.InvoiceSeqNo = Me.InvoiceSeqNo
            'Entities = m_ControllerResc.GetMinDueDate(Entities)
            Entities = m_ControllerResc.GetMinDueDateFactAndMDKJ(Entities)
            With Entities
                Me.SeqNo = .SeqNo
                Me.DueDate = .DueDate
                Me.Tenor = .Tenor
                Me.MaxSeqNo = .MaxSeqNo
                Me.NewNumInst = .NewNumInst
            End With
            tempdate = DateAdd(DateInterval.Month, 1, Me.DueDate)
            'If Me.BusinessDate <= Me.DueDate Then
            If Me.BusinessDate <= Me.DueDate.AddMonths(3) Then
                If CDbl(lblPrepaidAmount.Text) >= CDbl(lblTotalAmountToBePaid.Text) Then
                    With Entities
                        .strConnection = GetConnectionString()
                        .BranchId = Me.BranchID
                        .ApplicationID = Me.ApplicationID
                        .SeqNo = Me.SeqNo
                        .BusinessDate = Me.BusinessDate
                        .EffectiveDate = ConvertDate2(Me.EffectiveDate)
                        .InvoiceNo = Me.InvoiceNo
                        .InvoiceSeqNo = Me.InvoiceSeqNo
                    End With
                    Try
                        m_controllerResch.RestructFactAndMUExecute(Entities)
                        Server.Transfer("RestructFactAndMUExecution.aspx")
                    Catch exp As Exception
                        ShowMessage(lblMessage, exp.Message, True)
                    End Try
                Else
                    ShowMessage(lblMessage, " Jumlah Prepaid harus >= Total yang harus dibayar", True)
                End If
            Else
                'ShowMessage(lblMessage, " Eksekusi Reschedulling Fact dan Mdkj harus <= '" & Me.NextInstallmentDate.ToString("dd/MM/yyyy"), True)
                ShowMessage(lblMessage, " Eksekusi Reschedulling Fact dan Mdkj harus <= '" & Me.DueDate.AddMonths(1).ToString("dd/MM/yyyy"), True)
            End If
        End If
    End Sub

    Private Sub imbSave_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbSave.Click
        'Untuk Proces Cancel
        If CheckFeature(Me.Loginid, Me.FormID, "SAVE", Me.AppId) Then
            Dim m_ControllerResc As New ReschedulingController
            Entities.strConnection = GetConnectionString()
            Entities.ApplicationID = Me.ApplicationID
            Entities.BusinessDate = ConvertDate2(Me.EffectiveDate)
            Entities.BranchId = Me.BranchID
            Entities.InvoiceNo = Me.InvoiceNo
            Entities.InvoiceSeqNo = Me.InvoiceSeqNo
            'Entities = m_ControllerResc.GetMinDueDate(Entities)
            Entities = m_ControllerResc.GetMinDueDateFactAndMDKJ(Entities)
            With Entities
                Me.SeqNo = .SeqNo
                Me.DueDate = .DueDate
                Me.Tenor = .Tenor
                Me.MaxSeqNo = .MaxSeqNo
                Me.NewNumInst = .NewNumInst
            End With

            With Entities
                .strConnection = GetConnectionString()
                .BranchId = Me.BranchID
                .ApplicationID = Me.ApplicationID
                .SeqNo = Me.SeqNo
                .BusinessDate = Me.BusinessDate
                .EffectiveDate = ConvertDate2(Me.EffectiveDate)
                .InvoiceNo = Me.InvoiceNo
                .InvoiceSeqNo = Me.InvoiceSeqNo
            End With
            Try
                'm_controllerResch.ReschedulingCancel(Entities)
                m_controllerResch.ReschedulingCancelFactAndMDKJ(Entities)
                Server.Transfer("RestructFactAndMUExecution.aspx")
            Catch exp As Exception
                ShowMessage(lblMessage, exp.Message, True)
            End Try
        End If
    End Sub

    Private Sub imbCancel_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbCancel.Click
        Response.Redirect("RestructFactAndMUExecution.aspx")
    End Sub

End Class