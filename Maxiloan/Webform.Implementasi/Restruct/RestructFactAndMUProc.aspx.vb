﻿#Region "Import"
Imports System.Threading
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient
Imports System.IO
Imports Maxiloan.Framework.SQLEngine
Imports DocumentFormat.OpenXml.Packaging
Imports DocumentFormat.OpenXml.Spreadsheet
Imports Maxiloan.Webform.LoanMnt
#End Region

Public Class RestructFactAndMUProc
    Inherits Maxiloan.Webform.AccMntWebBased
    Protected WithEvents oPaymentInfo As UcFullPrepayInfo
    Protected WithEvents oApprovalRequest As ucApprovalRequest
    Protected WithEvents txtPartialPay, txtInstallLC, txtAdminFee, txtInstallCollFee, txtInsuranceLC, txtInsurCollFee,
                        txtPDCBounceFee, txtSTNKFee, txtInsuranceClaim, txtReposessFee, txtStep, txtEffRate, txtCummulative,
                        txtInstallmentAmt, txtNoStep, txtEntryInstallment, txtpphaccrued, txtpphdenda,
                        txtOutstandingPrincipal, txtOutstandingInterest, txtOutstandingLC, txtAccruedInterest, txtInstallmentDue, txtPPh As ucNumberFormat

#Region "Property"
    Property Prepaid() As Double
        Get
            Return CDbl(ViewState("Prepaid"))
        End Get
        Set(ByVal Value As Double)
            ViewState("Prepaid") = Value
        End Set
    End Property
    Private Property NewTenor() As Integer
        Get
            Return (CType(ViewState("NewTenor"), Integer))
        End Get
        Set(ByVal Value As Integer)
            ViewState("NewTenor") = Value
        End Set
    End Property
    Private Property MinSeqNo() As Integer
        Get
            Return (CType(ViewState("MinSeqNo"), Integer))
        End Get
        Set(ByVal Value As Integer)
            ViewState("MinSeqNo") = Value
        End Set
    End Property
    Private Property newNo() As Integer
        Get
            Return (CType(ViewState("newNo"), Integer))
        End Get
        Set(ByVal Value As Integer)
            ViewState("newNo") = Value
        End Set
    End Property
    Private Property NewNum() As Integer
        Get
            Return (CType(ViewState("NewNum"), Integer))
        End Get
        Set(ByVal Value As Integer)
            ViewState("NewNum") = Value
        End Set
    End Property
    Property Type() As String
        Get
            Return ViewState("Type").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("Type") = Value
        End Set
    End Property
    Private Property SeqNo() As Integer
        Get
            Return (CType(ViewState("SeqNo"), Integer))
        End Get
        Set(ByVal Value As Integer)
            ViewState("SeqNo") = Value
        End Set
    End Property
    Private Property MaxSeqNo() As Integer
        Get
            Return (CType(ViewState("MaxSeqNo"), Integer))
        End Get
        Set(ByVal Value As Integer)
            ViewState("MaxSeqNo") = Value
        End Set
    End Property
    Private Property NewNumInst() As Integer
        Get
            Return (CType(ViewState("NewNumInst"), Integer))
        End Get
        Set(ByVal Value As Integer)
            ViewState("NewNumInst") = Value
        End Set
    End Property
    Property Status() As Boolean
        Get
            Return CType(ViewState("Status"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            ViewState("Status") = Value
        End Set
    End Property
    Property FRate() As Double
        Get
            Return CDbl(ViewState("FRate"))
        End Get
        Set(ByVal Value As Double)
            ViewState("FRate") = Value
        End Set
    End Property
    Property DiffRate() As Double
        Get
            Return CDbl(ViewState("DiffRate"))
        End Get
        Set(ByVal Value As Double)
            ViewState("DiffRate") = Value
        End Set
    End Property
    Property PrincipleAmount() As Double
        Get
            Return CDbl(ViewState("PrincipleAmount"))
        End Get
        Set(ByVal Value As Double)
            ViewState("PrincipleAmount") = Value
        End Set
    End Property
    Property NTFGrossYield() As Double
        Get
            Return CDbl(ViewState("NTFGrossYield"))
        End Get
        Set(ByVal Value As Double)
            ViewState("NTFGrossYield") = Value
        End Set
    End Property
    Property RejectMinimumIncome() As Double
        Get
            Return CDbl(ViewState("RejectMinimumIncome"))
        End Get
        Set(ByVal Value As Double)
            ViewState("RejectMinimumIncome") = Value
        End Set
    End Property
    Property ReschedulingFeeBehaviour() As String
        Get
            Return ViewState("ReschedulingFeeBehaviour").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ReschedulingFeeBehaviour") = Value
        End Set
    End Property
    Property StepUpDownType() As String
        Get
            Return ViewState("StepUpDownType").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("StepUpDownType") = Value
        End Set
    End Property
    Property Supplier() As String
        Get
            Return ViewState("Supplier").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("Supplier") = Value
        End Set
    End Property

    Property CustName() As String
        Get
            Return ViewState("CustName").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("CustName") = Value
        End Set
    End Property
    Property InterestType() As String
        Get
            Return ViewState("InterestType").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("InterestType") = Value
        End Set
    End Property
    Private Property AccruedAmount() As Double
        Get
            Return (CType(ViewState("AccruedAmount"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("AccruedAmount") = Value
        End Set
    End Property
    Private Property NewPrinciple() As Double
        Get
            Return (CType(ViewState("NewPrinciple"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("NewPrinciple") = Value
        End Set
    End Property
    Private Property ReschedulingFee() As Double
        Get
            Return (CType(ViewState("ReschedulingFee"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("ReschedulingFee") = Value
        End Set
    End Property
    Private Property PaymentFrequency() As Integer
        Get
            Return (CType(ViewState("PaymentFrequency"), Integer))
        End Get
        Set(ByVal Value As Integer)
            ViewState("PaymentFrequency") = Value
        End Set
    End Property
    Private Property DaysDiff() As Integer
        Get
            Return (CType(ViewState("DaysDiff"), Integer))
        End Get
        Set(ByVal Value As Integer)
            ViewState("DaysDiff") = Value
        End Set
    End Property
    Private Property PartialPay() As Double
        Get
            Return (CType(ViewState("PartialPay"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("PartialPay") = Value
        End Set
    End Property
    Private Property AdminFee() As Double
        Get
            Return (CType(ViewState("AdminFee"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("AdminFee") = Value
        End Set
    End Property
    Private Property strCustomerid() As String
        Get
            Return (CType(ViewState("strCustomerid"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("strCustomerid") = Value
        End Set
    End Property

    Private Property sdate() As String
        Get
            Return (CType(ViewState("sdate"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("sdate") = Value
        End Set
    End Property

    Private Property Tenor() As Integer
        Get
            Return (CType(ViewState("Tenor"), Integer))
        End Get
        Set(ByVal Value As Integer)
            ViewState("Tenor") = Value
        End Set
    End Property

    Private Property InstallmentScheme() As String
        Get
            Return (CType(ViewState("InstallmentScheme"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("InstallmentScheme") = Value
        End Set
    End Property
    Private Property Product() As String
        Get
            Return (CType(ViewState("Product"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("Product") = Value
        End Set
    End Property
    Private Property payFreq() As Integer
        Get
            Return (CType(ViewState("payFreq"), Integer))
        End Get
        Set(ByVal Value As Integer)
            ViewState("payFreq") = Value
        End Set
    End Property
    Private Property InsSeqNo() As Integer
        Get
            Return (CType(ViewState("InsSeqNo"), Integer))
        End Get
        Set(ByVal Value As Integer)
            ViewState("InsSeqNo") = Value
        End Set
    End Property
    Private Property InvoiceNo() As String
        Get
            Return (CType(ViewState("InvoiceNo"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("InvoiceNo") = Value
        End Set
    End Property

    Private Property InvoiceSeqNo() As Int64
        Get
            Return (CType(ViewState("InvoiceSeqNo"), Int64))
        End Get
        Set(ByVal Value As Int64)
            ViewState("InvoiceSeqNo") = Value
        End Set
    End Property
#End Region
#Region "Constanta"
    Private oCustomClass As New Parameter.DChange
    Private oController As New AgreementTransferController
    Private oControllerInfo As New UCPaymentInfoController
    Private oCustomClassInfo As New Parameter.AccMntBase
    Private oRescheduling As New Parameter.Rescheduling
    Dim oApplication As New Parameter.Application
    Private m_controller As New ApplicationController
    Private oControllerResc As New DChangeController
    Dim m_controllerFinancial As New FinancialDataController
    Dim m_controllerResch As New ReschedulingController
    Dim Entities As New Parameter.FinancialData
    Dim j As Integer = 0
    Dim RunRate As Double
    Dim FlateRate As Double
    Private Const style As String = "ACCMNT"
    Dim objrow As DataRow
    Dim intLoop As Integer
    Dim myDataTable As DataTable
    Dim myDataColumn As DataColumn
    Dim myDataRow As DataRow
    Dim myDataSet As New DataSet
    Dim dblInterestTotal As Double
    Dim i As Integer
    Dim PokokHutang() As Double
    Dim Hutang() As Double
    Dim PokokBunga() As Double
    Dim Bunga() As Double
    Dim HitungUlangSisa As Boolean = False
    Dim HitungUlangKurang As Boolean = False
    Dim SisaPrincipal As Double
    Dim KurangPrincipal As Double
    Private myCmd As New SqlCommand
    Private myAdapter As New SqlDataAdapter
    Private myDS As New DataSet
    Private myReader As DataTableReader
    Private FileName As String
    Private strFileTemp As String
    Private strFile As String
    Private _NoKontrak As String
    Private FileDirectory As String
    Private TotalAgreement As Double
    Private TotalAmountReceive As Decimal
    Private FileType As String
    Private _TglTransaksi As String
    Private _NoRekening As String
    Private _BranchID As String
    Private _AgreementNo As String
    Private objCon As SqlConnection
    Private TextFile As String
#End Region

#Region "Page Load"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        Me.FormID = "RestructFactAndMU"
        If Not Me.IsPostBack Then
            BindControllerNumber()
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If Not IsPostBack Then
                    Me.ApplicationID = Request.QueryString("ApplicationId")
                    Me.BranchID = Request.QueryString("BranchID")
                    Me.InvoiceNo = Request.QueryString("InvoiceNo")
                    Me.InvoiceSeqNo = Request.QueryString("InvoiceSeqNo")
                    oApprovalRequest.ReasonTypeID = "RESCH"
                    oApprovalRequest.ApprovalScheme = "RESC"
                    DoBind()
                    Me.MinSeqNo = CInt(Me.SeqNo - 1)
                    Me.NewNum = Me.NewTenor '(CInt(lblInstallmentNum.Text) - CInt(Me.MinSeqNo))
                    Call BindCD()
                    lblTenor.Text = CStr(Me.Tenor)
                    hdnTenor.Value = CStr(Me.Tenor)
                    txtAdminFee.Text = Me.ReschedulingFee.ToString
                    Call Total()
                    Call RangeValidatorBind()
                    pnlEntryInstallment.Visible = False
                    pnlViewST.Visible = False
                    txtStep.Enabled = False
                    txtCummulative.Enabled = False

                    If Me.InstallmentScheme = "RF" Then
                        imbUpload.Visible = False
                        FileVA.Visible = False
                        imbReset.Visible = False
                    ElseIf Me.InstallmentScheme = "IR" Then
                        txtInstallmentAmt.Enabled = False
                        imbUpload.Visible = False
                        FileVA.Visible = False
                        imbReset.Visible = False
                    ElseIf Me.InstallmentScheme = "ST" Then
                        txtInstallmentAmt.Enabled = False
                        panelstruktur.Visible = False
                        imbCalRate.Visible = False
                        imbUpload.Visible = True
                        FileVA.Visible = True
                        imbReset.Visible = True
                        If Me.StepUpDownType = "NM" Then
                            txtStep.Enabled = True
                            txtCummulative.Enabled = False
                        ElseIf Me.StepUpDownType = "RL" Then
                            txtStep.Enabled = False
                            txtCummulative.Enabled = True
                        Else
                            txtStep.Enabled = True
                            txtCummulative.Enabled = True
                        End If
                    Else
                        txtInstallmentAmt.Enabled = False
                    End If
                    If Me.InterestType = "FX" Then
                        If Me.InstallmentScheme = "ST" Then
                            If Me.StepUpDownType = "NM" Then
                                txtStep.Visible = True
                                lblNumberOfStep.Visible = True
                                txtCummulative.Visible = False
                                lblCummulative.Visible = False
                            ElseIf Me.StepUpDownType = "RL" Then
                                txtStep.Visible = False
                                lblNumberOfStep.Visible = False
                                txtCummulative.Visible = True
                                lblCummulative.Visible = True
                            ElseIf Me.StepUpDownType = "LS" Then
                                txtStep.Visible = True
                                lblNumberOfStep.Visible = True
                                txtCummulative.Visible = True
                                lblCummulative.Visible = True
                            End If
                        Else
                            txtStep.Visible = False
                            txtCummulative.Visible = False
                            lblNumberOfStep.Visible = False
                            lblCummulative.Visible = False
                        End If
                    End If
                    If Me.InterestType = "FL" Then
                        txtStep.Visible = False
                        txtCummulative.Visible = False
                        lblNumberOfStep.Visible = False
                        lblCummulative.Visible = False
                    End If
                    If Me.StepUpDownType = "NM" Or Me.StepUpDownType = "LS" Then
                        imbCalRate.Enabled = False
                    Else
                        imbCalRate.Enabled = True
                    End If
                End If
            End If
        End If
    End Sub
#End Region
#Region "Dobind"
    Sub DoBind()
        Dim totalPrepayment As Double
        GetCookies()
        lbljudul.Text = Me.sdate
        With oPaymentInfo
            '.IsTitle = True
            .ApplicationID = Me.ApplicationID
            .ValueDate = ConvertDate2(Me.sdate)
            .PrepaymentType = "DI"
            .BranchID = Me.BranchID.Trim
            '.IsPenaltyTerminationShow = False
            .IsPenaltyTerminationShow = True
            .InvoiceNo = Me.InvoiceNo
            .InvoiceSeqNo = Me.InvoiceSeqNo
            .PaymentInfoFactAndMDKJ()
            '.PaymentInfo()

            Me.AgreementNo = .AgreementNo
            Me.strCustomerid = .CustomerID
            Me.CustomerType = .CustomerType
            lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(Me.ApplicationID.Trim) & "')"
            lblAgreementNo.Text = .AgreementNo
            lblCustName.Text = .CustomerName
            lblCustName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(Me.strCustomerid.Trim) & "')"
            'totalPrepayment = .TotalPrePaymentAmountResch
            'totalPrepayment = .MaximumOutStandingPrincipal + .MaximumOutStandingInterest + .MaximumLCInstallFee + .AccruedInterest + .InstallmentDueAmount
            totalPrepayment = .MaximumOutStandingPrincipal + .MaximumLCInstallFee + .AccruedInterest + .InstallmentDueAmount
            lblStopAccruedAmount.Text = FormatNumber(.TotalPrePaymentAmountResch, 2)
            lblOutstandingPrincipal.Text = FormatNumber(.MaximumOutStandingPrincipal, 2)
            lblOutstandingInterest.Text = FormatNumber(.MaximumOutStandingInterest, 2)
            lblOutstandingLC.Text = FormatNumber(.MaximumLCInstallFee, 2)
            lblAccruedInterest.Text = FormatNumber(.AccruedInterest, 2)
            lblInstallmentDue.Text = FormatNumber(.InstallmentDueAmount, 2)
            'lblTotAdvAmount.Text = FormatNumber(.MaximumOutStandingPrincipal + .MaximumOutStandingInterest + .MaximumLCInstallFee + .AccruedInterest + .InstallmentDueAmount - CDbl(IIf(txtPartialPay.Text = "", "0", txtPartialPay.Text)) + CDbl(IIf(txtAdminFee.Text = "", "0", txtAdminFee.Text)), 2)
            lblTotAdvAmount.Text = FormatNumber(.MaximumOutStandingPrincipal + .MaximumLCInstallFee + .AccruedInterest + .InstallmentDueAmount - CDbl(IIf(txtPartialPay.Text = "", "0", txtPartialPay.Text)) + CDbl(IIf(txtAdminFee.Text = "", "0", txtAdminFee.Text)), 2)
            Me.AccruedAmount = totalPrepayment
            Me.Prepaid = .ContractPrepaidAmount

        End With
        GetList()
        BindTC()
    End Sub
#End Region
#Region "Bind Controller Number"
    Sub BindControllerNumber()
        With txtPartialPay
            .RequiredFieldValidatorEnable = True
            .RangeValidatorEnable = False
            .TextCssClass = "numberAlign regular_text"
        End With
        With txtInstallLC
            .RequiredFieldValidatorEnable = True
            .RangeValidatorEnable = False
            .TextCssClass = "numberAlign regular_text"
        End With
        With txtAdminFee
            .RequiredFieldValidatorEnable = True
            .RangeValidatorEnable = False
            .TextCssClass = "numberAlign regular_text"
        End With
        With txtInstallCollFee
            .RequiredFieldValidatorEnable = True
            .RangeValidatorEnable = False
            .TextCssClass = "numberAlign regular_text"
        End With
        With txtInsuranceLC
            .RequiredFieldValidatorEnable = True
            .RangeValidatorEnable = False
            .TextCssClass = "numberAlign regular_text"
        End With
        With txtInsurCollFee
            .RequiredFieldValidatorEnable = True
            .RangeValidatorEnable = False
            .TextCssClass = "numberAlign regular_text"
        End With
        With txtPDCBounceFee
            .RequiredFieldValidatorEnable = True
            .RangeValidatorEnable = False
            .TextCssClass = "numberAlign regular_text"
        End With
        With txtSTNKFee
            .RequiredFieldValidatorEnable = True
            .RangeValidatorEnable = False
            .TextCssClass = "numberAlign regular_text"
        End With
        With txtInsuranceClaim
            .RequiredFieldValidatorEnable = True
            .RangeValidatorEnable = False
            .TextCssClass = "numberAlign regular_text"
        End With
        With txtReposessFee
            .RequiredFieldValidatorEnable = True
            .RangeValidatorEnable = False
            .TextCssClass = "numberAlign regular_text"
        End With
        With txtCummulative
            .RequiredFieldValidatorEnable = True
            .RangeValidatorEnable = False
            .TextCssClass = "numberAlign regular_text"
        End With
        With txtInstallmentAmt
            .RequiredFieldValidatorEnable = True
            .RangeValidatorEnable = False
            .TextCssClass = "numberAlign regular_text"
        End With
        With txtStep
            .RequiredFieldValidatorEnable = True
            .RangeValidatorEnable = True
            .TextCssClass = "numberAlign regular_text"
            .RangeValidatorMinimumValue = "2"
            .RangeValidatorMaximumValue = "999"
        End With
        With txtCummulative
            .RequiredFieldValidatorEnable = True
            .RangeValidatorEnable = True
            .TextCssClass = "numberAlign regular_text"
            .RangeValidatorMinimumValue = "1"
            .RangeValidatorMaximumValue = "999"
        End With
    End Sub
#End Region
#Region "Get Cookies"
    Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies("Rescheduling")
        Me.Tenor = CInt(cookie.Values("Tenor"))
        Me.InstallmentScheme = cookie.Values("InstallmentScheme")
        Me.sdate = cookie.Values("sdate")
        Me.MaxSeqNo = CInt(cookie.Values("MaxSeqNo"))
        Me.NewNumInst = CInt(cookie.Values("NewNumInst"))
        Me.SeqNo = CInt(cookie.Values("SeqNo"))
        Me.StepUpDownType = cookie.Values("Type")
        Me.NewTenor = CInt(cookie.Values("NewTenor"))
    End Sub
#End Region
#Region " Sub & Function "
    Public Sub BuatTableView()
        ' Create new DataColumn, set DataType, ColumnName and add to DataTable.
        myDataTable = New DataTable("AmortTable")

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Int32")
        myDataColumn.ColumnName = "No"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.String")
        myDataColumn.ColumnName = "DueDate"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "Installment"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "Principal"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "Interest"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "PrincBalance"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "PrincInterest"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "InstallmentSisa"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Int32")
        myDataColumn.ColumnName = "StepFrom"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Int32")
        myDataColumn.ColumnName = "StepTo"
        myDataTable.Columns.Add(myDataColumn)
    End Sub

    Public Function MakeAmortTableView(ByVal intNumInstallment As Integer, ByVal dblInstAmt As Double, ByVal dblNTF As Double, ByVal dblRunRate As Double, ByVal intPayFreq As Integer) As DataSet
        Dim m_controller As New FinancialDataController
        Dim oFinancialData As New Parameter.FinancialData
        With oFinancialData
            .PaymentFrequency = CStr(Me.payFreq)
        End With

        ReDim PokokHutang(intNumInstallment + 1)
        ReDim Bunga(intNumInstallment + 1)
        ReDim PokokBunga(intNumInstallment + 1)
        Dim TotalPrincipal As Double
        Dim tempdate As Date
        Dim j As Integer = 0
        Bunga(0) = 0
        PokokHutang(0) = dblNTF

        Me.MinSeqNo = -1
        For i = 1 To intNumInstallment + 1
            myDataRow = myDataTable.NewRow()
            If i = 1 Then
                myDataRow("No") = 0
                myDataRow("Installment") = 0
                myDataRow("Principal") = 0
                myDataRow("Interest") = 0
            Else
                myDataRow("No") = Me.MinSeqNo + i
                myDataRow("Installment") = dblInstAmt
                myDataRow("Interest") = Math.Round((PokokHutang(i - 1) * dblRunRate) / (m_controller.GetTerm(intPayFreq) * 100), 0)
                myDataRow("Principal") = Math.Round(dblInstAmt - CDbl(myDataRow("Interest")), 0)
            End If
            If i = 1 Then
                myDataRow("PrincBalance") = PokokHutang(0)
            ElseIf i = intNumInstallment + 1 Then
                myDataRow("PrincBalance") = 0
            Else
                myDataRow("PrincBalance") = Math.Round(PokokHutang(i - 1) - CDbl(myDataRow("Principal")), 0)
            End If
            myDataRow("PrincInterest") = 0
            PokokHutang(i) = CDbl(myDataRow("PrincBalance"))
            Bunga(i) = CDbl(myDataRow("Interest"))
            TotalPrincipal = Math.Round(TotalPrincipal + CDbl(myDataRow("Principal")), 0)

            If DateDiff(DateInterval.Day, ConvertDate2(Me.sdate), Me.BusinessDate) >= 0 Then

                j = j + 1
                If j = 1 Then
                    tempdate = ConvertDate2(Me.sdate)
                Else
                    tempdate = DateAdd(DateInterval.Month, CInt(cbopaymentFreq.SelectedValue), tempdate)
                End If
                myDataRow("DueDate") = tempdate.ToString("dd/MM/yyyy")
            End If

            myDataTable.Rows.Add(myDataRow)
        Next i

        If TotalPrincipal > dblNTF Then
            SisaPrincipal = Math.Round(TotalPrincipal - dblNTF, 0)
        Else
            KurangPrincipal = Math.Round(dblNTF - TotalPrincipal, 0)
        End If

        If SisaPrincipal > 0 Then
            Bunga(intNumInstallment + 1) = Math.Round(Bunga(intNumInstallment + 1) + SisaPrincipal, 0)
        End If
        If KurangPrincipal > 0 Then
            Bunga(intNumInstallment + 1) = Math.Round(Bunga(intNumInstallment + 1) - KurangPrincipal, 0)
        End If

        PokokBunga(intNumInstallment + 1) = 0

        For i = intNumInstallment To 0 Step -1
            PokokBunga(i) = Math.Round(Bunga(i + 1) + PokokBunga(i + 1), 0)
            dblInterestTotal = Math.Round(dblInterestTotal + Bunga(i + 1), 0)
        Next

        Dim myDataSet As DataSet
        myDataSet = New DataSet

        myDataSet.Tables.Add(myDataTable)

        Dim custRow As DataRow

        i = 1
        For Each custRow In myDataSet.Tables("AmortTable").Rows
            If i <= intNumInstallment + 1 Then
                custRow("PrincInterest") = PokokBunga(i)
                i = i + 1
            End If
        Next

        For Each custRow In myDataSet.Tables("AmortTable").Rows
            If CInt(custRow("No")) = intNumInstallment Then
                If SisaPrincipal > 0 Then
                    custRow("Principal") = Math.Round(CDbl(custRow("Principal")) - SisaPrincipal, 0)
                    custRow("Interest") = Math.Round(CDbl(custRow("Interest")) + SisaPrincipal, 0)
                    HitungUlangSisa = True
                End If
                If KurangPrincipal > 0 Then
                    custRow("Principal") = Math.Round(CDbl(custRow("Principal")) + KurangPrincipal, 0)
                    custRow("Interest") = Math.Round(CDbl(custRow("Interest")) - KurangPrincipal, 0)
                    HitungUlangKurang = True
                End If
            End If
        Next
        Return myDataSet
    End Function

    Private Sub RangeValidatorBind()
        With oPaymentInfo
            txtInstallLC.RangeValidatorMaximumValue = CStr(.MaximumLCInstallFee)
            txtInstallCollFee.RangeValidatorMaximumValue = CStr(.MaximumInstallCollFee)
            txtInsuranceLC.RangeValidatorMaximumValue = CStr(.MaximumLCInsuranceFee)
            txtInsuranceClaim.RangeValidatorMaximumValue = CStr(.MaximumInsuranceCollFee)
            txtPDCBounceFee.RangeValidatorMaximumValue = CStr(.MaximumPDCBounceFee)
            txtSTNKFee.RangeValidatorMaximumValue = CStr(.MaximumSTNKRenewalFee)
            txtInsuranceClaim.RangeValidatorMaximumValue = CStr(.MaximumInsuranceClaimFee)
            txtReposessFee.RangeValidatorMaximumValue = CStr(.MaximumReposessionFee)
        End With
    End Sub
    Private Sub Total()
        Dim inTotDisc As Double
        Dim TotalAmount As Double
        Dim intTotAdvAmt As Double
        inTotDisc = 0
        'lblTotAdvAmt.Text = FormatNumber(CDbl(IIf(txtPartialPay.Text = "", "0", txtPartialPay.Text)) + CDbl(IIf(txtAdminFee.Text = "", "0", txtAdminFee.Text)), 2)
        'inTotDisc = CDbl(IIf(txtInstallLC.Text = "", "0", txtInstallLC.Text)) + CDbl(IIf(txtInstallCollFee.Text = "", "0", txtInstallCollFee.Text)) + CDbl(IIf(txtInsuranceLC.Text = "", "0", txtInsuranceLC.Text))
        'inTotDisc = inTotDisc + CDbl(IIf(txtInsurCollFee.Text = "", "0", txtInsurCollFee.Text)) + CDbl(IIf(txtPDCBounceFee.Text = "", "0", txtPDCBounceFee.Text)) + CDbl(IIf(txtSTNKFee.Text = "", "0", txtSTNKFee.Text))
        'inTotDisc = inTotDisc + CDbl(IIf(txtInsuranceClaim.Text = "", "0", txtInsuranceClaim.Text)) + CDbl(IIf(txtReposessFee.Text = "", "0", txtReposessFee.Text))
        inTotDisc = CDbl(IIf(txtOutstandingPrincipal.Text = "", "0", txtOutstandingPrincipal.Text)) + CDbl(IIf(txtOutstandingInterest.Text = "", "0", txtOutstandingInterest.Text))
        inTotDisc = inTotDisc + CDbl(IIf(txtOutstandingLC.Text = "", "0", txtOutstandingLC.Text)) + CDbl(IIf(txtAccruedInterest.Text = "", "0", txtAccruedInterest.Text)) + CDbl(IIf(txtInstallmentDue.Text = "", "0", txtInstallmentDue.Text)) + CDbl(IIf(txtPPh.Text = "", "0", txtPPh.Text))
        lblTotDiscAmt.Text = FormatNumber(inTotDisc, 2)
        'Me.NewPrinciple = Me.AccruedAmount - CDbl(IIf(txtPartialPay.Text = "", "0", txtPartialPay.Text)) - inTotDisc
        'lblTotAdvAmount.Text = FormatNumber(Me.AccruedAmount - intTotAdvAmt - inTotDisc, 2)
        If chkAdminFee.Checked = True Then
            lblTotAdvAmt.Text = FormatNumber(CDbl(IIf(txtPartialPay.Text = "", "0", txtPartialPay.Text)), 2)
            intTotAdvAmt = CDbl(IIf(txtAdminFee.Text = "", "0", txtAdminFee.Text))
            lblTotAdvAmount.Text = FormatNumber(Me.AccruedAmount + intTotAdvAmt - lblTotAdvAmt.Text - inTotDisc, 2)
            Me.NewPrinciple = Me.AccruedAmount - CDbl(IIf(txtPartialPay.Text = "", "0", txtPartialPay.Text)) - inTotDisc + CDbl(IIf(txtAdminFee.Text = "", "0", txtAdminFee.Text))
        Else
            lblTotAdvAmt.Text = FormatNumber(CDbl(IIf(txtPartialPay.Text = "", "0", txtPartialPay.Text)) + CDbl(IIf(txtAdminFee.Text = "", "0", txtAdminFee.Text)), 2)
            intTotAdvAmt = CDbl(IIf(txtPartialPay.Text = "", "0", txtPartialPay.Text))
            lblTotAdvAmount.Text = FormatNumber(Me.AccruedAmount - intTotAdvAmt - inTotDisc, 2)
            Me.NewPrinciple = Me.AccruedAmount - CDbl(IIf(txtPartialPay.Text = "", "0", txtPartialPay.Text)) - inTotDisc
        End If
        lblNPA.Text = FormatNumber(Me.NewPrinciple, 2)
        'lblTotalAmountToBePaid.Text = FormatNumber(CDbl(IIf(txtPartialPay.Text = "", "0", txtPartialPay.Text)) + CDbl(IIf(txtAdminFee.Text = "", "0", txtAdminFee.Text)), 2)
        lblTotalAmountToBePaid.Text = FormatNumber(lblTotAdvAmt.Text, 2)
        TotalAmount = CDbl(IIf(lblTotalAmountToBePaid.Text = "", "0", lblTotalAmountToBePaid.Text))
        lblPrepaidAmount.Text = FormatNumber(Me.Prepaid, 2)
        lblBalanceAmount.Text = FormatNumber(Me.Prepaid - TotalAmount, 2)

    End Sub
    Sub GetList()
        Dim DController As New DChangeController
        oCustomClass.ApplicationID = Me.ApplicationID
        oCustomClass.BranchId = Me.sesBranchId.Replace("'", "")
        oCustomClass.strConnection = GetConnectionString()
        oCustomClass.InvoiceNo = Me.InvoiceNo
        oCustomClass.InvoiceSeqNo = Me.InvoiceSeqNo
        'oCustomClass = DController.GetList(oCustomClass)
        oCustomClass = DController.GetListFactAndMU(oCustomClass)
        With oCustomClass
            lblInterestType.Text = .InterestTypeDesc.Trim
            lblProduct.Text = .ProductDesc
            lblPackage.Text = .ProductOfferingDesc
            lblInstallScheme.Text = .InstallmentSchemeDesc
            lblFinanceType.Text = .FinanceTypeDesc
            lblGuarantor.Text = .GuarantorName
            lblEffRate.Text = CStr(.EffectiveRate)
            Me.ReschedulingFee = .ReschedulingFee
            Me.ReschedulingFeeBehaviour = .ReschedulingFeeBehaviour
            Me.Product = .ProductId
            Select Case .PaymentFrequency
                Case "1"
                    lblPaymentFreq.Text = "Monthly"
                    hdnPaymentFreq.Value = "1"
                    lblInstallmentNum.Text = CStr(Me.Tenor / 1)
                    cbopaymentFreq.SelectedItem.Value = "1"
                Case "2"
                    lblPaymentFreq.Text = "Bimonthly"
                    hdnPaymentFreq.Value = "2"
                    lblInstallmentNum.Text = CStr(Me.Tenor / 2)
                    cbopaymentFreq.SelectedItem.Value = "2"
                Case "3"
                    lblPaymentFreq.Text = "Quarterly"
                    hdnPaymentFreq.Value = "3"
                    lblInstallmentNum.Text = CStr(Me.Tenor / 3)
                    cbopaymentFreq.SelectedItem.Value = "3"
                Case "6"
                    lblPaymentFreq.Text = "Semi Annualy"
                    hdnPaymentFreq.Value = "6"
                    lblInstallmentNum.Text = CStr(Me.Tenor / 6)
                    cbopaymentFreq.SelectedItem.Value = "6"
            End Select
            lblInstallmentNo.Text = FormatNumber(.NextInstallmentNumber, 0) & " of " & FormatNumber(.NumOfInstallment, 0)
            lblReschedNo.Text = FormatNumber(.ReschedulingNo, 0)
            Me.InterestType = .InterestType
        End With
        oCustomClass.CustomerID = Me.strCustomerid
        oCustomClass.strConnection = GetConnectionString()
        oCustomClass = oController.GetCust(oCustomClass)
        Me.CustomerType = oCustomClass.CustomerType
    End Sub
#End Region
#Region "BindCD"
    Sub BindCD()
        Dim oApplication As New Parameter.Application
        Dim oData As New DataTable
        Dim txtAgreement As TextBox
        Dim lblAgreement As Label
        Dim Name As Label
        Dim AgreementDate As Label
        Dim DefaultStatus As Label
        Dim ContractStatus As Label
        Dim intloop As Integer
        oApplication.AppID = Me.ApplicationID
        oApplication.strConnection = GetConnectionString()
        oApplication = m_controller.GetCD(oApplication)
        If Not oApplication Is Nothing Then
            oData = oApplication.ListData
        End If
        dtgCrossDefault.DataSource = oData
        dtgCrossDefault.DataBind()

        For intloop = 0 To oData.Rows.Count - 1
            txtAgreement = CType(dtgCrossDefault.Items(intloop).FindControl("txtCDAgreement"), TextBox)
            lblAgreement = CType(dtgCrossDefault.Items(intloop).FindControl("lblCDAgreementNo"), Label)
            AgreementDate = CType(dtgCrossDefault.Items(intloop).FindControl("lblCDAgreementDate"), Label)
            txtAgreement.Text = oData.Rows(intloop).Item(0).ToString.Trim
            lblAgreement.Text = oData.Rows(intloop).Item(0).ToString.Trim
            If Not IsDBNull(oData.Rows(intloop).Item(2)) Then
                AgreementDate.Text = FormatDateTime(oData.Rows(intloop).Item(2), "dd/MM/yyyy")
            End If
            txtAgreement.Visible = False
            lblAgreement.Visible = True
        Next
    End Sub
#End Region
#Region "BindTC"
    Sub BindTC()
        '=============Bind Grid TC================
        Dim oData As New DataTable
        Dim validCheck As New Label
        Dim Mandatory As String
        oApplication.strConnection = GetConnectionString()
        oApplication.BranchId = Me.sesBranchId
        oApplication.ProductID = Me.Product
        oApplication.Type = Me.CustomerType
        oApplication = m_controller.GetTC(oApplication)
        If Not oApplication Is Nothing Then
            oData = oApplication.ListData
        End If
        dtgTC.DataSource = oData.DefaultView
        dtgTC.DataKeyField = "TCName"
        dtgTC.DataBind()

        oApplication.strConnection = GetConnectionString()
        oApplication.BranchId = Me.sesBranchId
        oApplication.ProductID = Me.Product
        oApplication.Type = Me.CustomerType
        oApplication = m_controller.GetTC2(oApplication)
        If Not oApplication Is Nothing Then
            oData = oApplication.ListData
        End If
        dtgTC2.DataSource = oData.DefaultView
        dtgTC2.DataKeyField = "TCName"
        dtgTC2.DataBind()

        If dtgTC.Items.Count > 0 Then
            For j = 0 To dtgTC.Items.Count - 1
                validCheck = CType(dtgTC.Items(j).FindControl("lblVTCChecked"), Label)
                Mandatory = dtgTC.Items(j).Cells(3).Text.Trim
                If Mandatory = "v" Then
                    validCheck.Visible = True
                Else
                    validCheck.Visible = False
                End If
            Next
        End If

        If dtgTC2.Items.Count > 0 Then
            For j = 0 To dtgTC2.Items.Count - 1
                validCheck = CType(dtgTC2.Items(j).FindControl("lblVTC2Checked"), Label)
                Mandatory = dtgTC2.Items(j).Cells(4).Text.Trim
                If Mandatory = "v" Then
                    validCheck.Visible = True
                Else
                    validCheck.Visible = False

                End If

            Next
        End If

    End Sub
#End Region

#Region "Calculate"
    Private Sub imbCalculate_Click(ByVal sender As Object, ByVal e As EventArgs) Handles imbCalculate.Click
        If Me.ReschedulingFeeBehaviour <> "L" Then
            With oPaymentInfo
                If CDbl(txtInstallLC.Text) <= CDbl(.MaximumLCInstallFee) And CDbl(txtInstallCollFee.Text) <= CDbl(.MaximumInstallCollFee) And CDbl(txtInsuranceLC.Text) <= CDbl(.MaximumLCInsuranceFee) And
                    CDbl(txtInsurCollFee.Text) <= CDbl(.MaximumInsuranceCollFee) And CDbl(txtPDCBounceFee.Text) <= CDbl(.MaximumPDCBounceFee) And CDbl(txtSTNKFee.Text) <= CDbl(.MaximumSTNKRenewalFee) And
                    CDbl(txtInsuranceClaim.Text) <= CDbl(.MaximumInsuranceClaimFee) And CDbl(txtReposessFee.Text) <= CDbl(.MaximumReposessionFee) And
                    txtInstallLC.Text <> "" And txtInstallCollFee.Text <> "" And txtInsuranceLC.Text <> "" And
                    txtInsurCollFee.Text <> "" And txtPDCBounceFee.Text <> "" And txtSTNKFee.Text <> "" And
                    txtInsuranceClaim.Text <> "" And txtReposessFee.Text <> "" And txtPartialPay.Text <> "" And txtAdminFee.Text <> "" And
                    txtInstallmentDue.Text <> "" And txtAccruedInterest.Text <> "" And txtOutstandingLC.Text <> "" And txtOutstandingInterest.Text <> "" And
                    txtOutstandingPrincipal.Text <> "" And txtPPh.Text <> "" Then
                    Call Total()
                Else
                    ShowMessage(lblMessage, "Harap periksa Data entry Discount", True)
                End If
            End With
        ElseIf Me.ReschedulingFeeBehaviour = "N" Then
            If txtAdminFee.Text >= FormatNumber(Me.ReschedulingFee, 2) Then
                With oPaymentInfo
                    If CDbl(txtInstallLC.Text) <= CDbl(.MaximumLCInstallFee) And CDbl(txtInstallCollFee.Text) <= CDbl(.MaximumInstallCollFee) And CDbl(txtInsuranceLC.Text) <= CDbl(.MaximumLCInsuranceFee) And
                        CDbl(txtInsurCollFee.Text) <= CDbl(.MaximumInsuranceCollFee) And CDbl(txtPDCBounceFee.Text) <= CDbl(.MaximumPDCBounceFee) And CDbl(txtSTNKFee.Text) <= CDbl(.MaximumSTNKRenewalFee) And
                        CDbl(txtInsuranceClaim.Text) <= CDbl(.MaximumInsuranceClaimFee) And CDbl(txtReposessFee.Text) <= CDbl(.MaximumReposessionFee) And
                        txtInstallLC.Text <> "" And txtInstallCollFee.Text <> "" And txtInsuranceLC.Text <> "" And
                        txtInsurCollFee.Text <> "" And txtPDCBounceFee.Text <> "" And txtSTNKFee.Text <> "" And
                        txtInsuranceClaim.Text <> "" And txtReposessFee.Text <> "" And txtPartialPay.Text <> "" And txtAdminFee.Text <> "" Then
                        Call Total()
                    Else
                        ShowMessage(lblMessage, "Harap periksa Data entry Discount", True)
                    End If
                End With
            Else
                ShowMessage(lblMessage, "Minimum Biaya Administrasi " & Me.ReschedulingFee & "- 100.000.000", True)
            End If
        ElseIf Me.ReschedulingFeeBehaviour = "X" Then
            If txtAdminFee.Text <= FormatNumber(Me.ReschedulingFee, 2) Then
                With oPaymentInfo
                    If CDbl(txtInstallLC.Text) <= CDbl(.MaximumLCInstallFee) And CDbl(txtInstallCollFee.Text) <= CDbl(.MaximumInstallCollFee) And CDbl(txtInsuranceLC.Text) <= CDbl(.MaximumLCInsuranceFee) And
                        CDbl(txtInsurCollFee.Text) <= CDbl(.MaximumInsuranceCollFee) And CDbl(txtPDCBounceFee.Text) <= CDbl(.MaximumPDCBounceFee) And CDbl(txtSTNKFee.Text) <= CDbl(.MaximumSTNKRenewalFee) And
                        CDbl(txtInsuranceClaim.Text) <= CDbl(.MaximumInsuranceClaimFee) And CDbl(txtReposessFee.Text) <= CDbl(.MaximumReposessionFee) And
                        txtInstallLC.Text <> "" And txtInstallCollFee.Text <> "" And txtInsuranceLC.Text <> "" And
                        txtInsurCollFee.Text <> "" And txtPDCBounceFee.Text <> "" And txtSTNKFee.Text <> "" And
                        txtInsuranceClaim.Text <> "" And txtReposessFee.Text <> "" And txtPartialPay.Text <> "" And txtAdminFee.Text <> "" Then
                        Call Total()
                    Else
                        ShowMessage(lblMessage, "Harap periksa Data entry Discount", True)
                    End If
                End With
            Else
                ShowMessage(lblMessage, "Minimum Biaya Administrasi  0-" & Me.ReschedulingFee, True)
            End If
        End If
    End Sub
#End Region

#Region "ImbCalculate"
    Private Function IsValidValidation(ByVal text As String, ByVal CompareText As Label, ByVal Message As String) As Boolean
        If text.Trim = "" Then
            ShowMessage(lblMessage, Message & " Harap diisi", True)
            Return False
        ElseIf CDbl(text) = 0 Then
            ShowMessage(lblMessage, Message & " Harus  > 0 !", True)
            Return False
        ElseIf CDbl(text) > CDbl(CompareText.Text) Then
            ShowMessage(lblMessage, Message & " Harus  < lama Angsuran", True)
            Return False
        End If
        Return True
    End Function
    Function InstAmt(ByVal Rate As Double) As Double
        Dim ReturnVal As Double
        RunRate = (Rate / (m_controllerFinancial.GetTerm(CInt(cbopaymentFreq.SelectedValue)) * 100))
        ReturnVal = Math.Round(m_controllerFinancial.GetInstAmtArr(RunRate, CInt(cbopaymentFreq.SelectedValue),
        Me.NewTenor, Me.NewPrinciple), 0)
        Return ReturnVal
    End Function
    Function CountRate(ByVal NTF As Double) As Double
        Dim ReturnVal As Double
        Dim intJmlInst As Integer
        If Me.InstallmentScheme = "ST" And Me.StepUpDownType = "RL" Then
            intJmlInst = CInt(txtCummulative.Text)
        Else
            intJmlInst = Me.NewTenor
        End If
        ReturnVal = m_controllerFinancial.GetEffectiveRateArr(intJmlInst, CDbl(txtInstallmentAmt.Text), NTF, CInt(cbopaymentFreq.SelectedValue))
        Return ReturnVal
    End Function
    Private Sub FlatRate()
        Dim FlateRate As Double
        FlateRate = 100 * ((CDbl(txtInstallmentAmt.Text) * Me.NewTenor) - Me.NewPrinciple) / Me.NewPrinciple
        lblFlatRate.Text = FormatNumber(FlateRate, 2) & "% per " & lblTenor.Text & " month "
        lblFlatRate.Visible = True
        Me.FRate = FlateRate
    End Sub
#End Region

#Region "Upload"
    Private Sub imbUpload_Click(ByVal sender As Object, ByVal e As EventArgs) Handles imbUpload.Click
        lblLoading.Visible = True
        imbUpload.Enabled = False
        Try
            If FileVA.HasFile Then
                strFileTemp = FileVA.FileName
                FileName = Context.Request.MapPath("~\\xml\\") + strFileTemp
                File.Delete(FileName)
                FileVA.SaveAs(FileName)
                Dim larr As Array = CType(strFileTemp.ToString.Split(CChar(".")), Array)
                TextFile = strFileTemp
                FileDirectory = Context.Request.MapPath("~\\xml\\").ToString.Trim
                FileType = larr.GetValue(1).ToString.Trim()

                Me.FileName = FileName
                DoProses()
                ViewInstallmentUpload()
            Else
                lblLoading.Visible = False
                ShowMessage(lblMessage, "File belum dipilih!", True)
                Exit Sub
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try

        imbUpload.Enabled = True
        lblLoading.Visible = False
    End Sub
    Private Sub DoProses()
        Dim list As New List(Of Parameter.AccMntBase)
        If ValidasiFile() Then
            list = UploadFile()
        Else
            ShowMessage(lblMessage, "File pernah di upload!", True)
            Exit Sub
        End If
        Try
            Dim dt As DataTable = createDataUpload()
            Dim schedule As New Parameter.AccMntBase
            TotalAgreement = list.Count
            If list.Count > 0 Then
                For index = 0 To list.Count - 1

                    schedule = list(index)
                    schedule.UploadDate = BusinessDate
                    dt.Rows.Add(schedule.UploadDate, strFileTemp, FileDirectory,
                                schedule.BranchId, schedule.ApplicationID, schedule.InsSeqNo, schedule.DueDate, schedule.PrincipalAmount, schedule.InterestAmount,
                                schedule.OutstandingPrincipal, schedule.OutStandingInterest)
                Next
            End If

            UploadReScheduleBatchSave(dt)
            ShowMessage(lblMessage, "Upload Data Reschedule Berhasil", False)

        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub
    Sub UploadReScheduleBatchSave(ByRef dt As DataTable)
        Try
            objCon = New SqlConnection(GetConnectionString)
            myCmd.Parameters.Clear()
            myCmd.CommandText = "SPUploadReScheduleBatchSave"
            myCmd.CommandType = CommandType.StoredProcedure
            myCmd.Parameters.Add(New SqlParameter("@udtTable", SqlDbType.Structured) With {.Value = dt})
            'myCmd.Parameters.Add(New SqlParameter("@BranchID", SqlDbType.Char, 3) With {.Value = schedule.BranchId})
            'myCmd.Parameters.Add(New SqlParameter("@ApplicationID", SqlDbType.Char, 20) With {.Value = schedule.ApplicationID})
            'myCmd.Parameters.Add(New SqlParameter("@InsSeqNo", SqlDbType.SmallInt) With {.Value = schedule.InsSeqNo})
            'myCmd.Parameters.Add(New SqlParameter("@DueDate", SqlDbType.Date) With {.Value = schedule.DueDate})
            'myCmd.Parameters.Add(New SqlParameter("@PrincipalAmount", SqlDbType.Decimal) With {.Value = schedule.PrincipalAmount})
            'myCmd.Parameters.Add(New SqlParameter("@InterestAmount", SqlDbType.Decimal) With {.Value = schedule.InterestAmount})
            'myCmd.Parameters.Add(New SqlParameter("@OutstandingPrincipal", SqlDbType.Decimal) With {.Value = schedule.OutstandingPrincipal})
            'myCmd.Parameters.Add(New SqlParameter("@OutStandingInterest", SqlDbType.Decimal) With {.Value = schedule.OutStandingInterest})

            objCon.Open()
            myCmd.Connection = objCon
            myCmd.CommandTimeout = 0
            myCmd.ExecuteNonQuery()
            objCon.Close()
        Catch ex As Exception
            Throw New Exception(ex.Message)
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub
    Function ValidasiFile() As Boolean
        Dim result As Boolean = True
        objCon = New SqlConnection(GetConnectionString)
        Try
            myDS = New DataSet
            myCmd.Parameters.Clear()
            myCmd.CommandText = "spGetUploadReScheduleBybatchValidFile"
            myCmd.CommandType = CommandType.StoredProcedure
            myCmd.Parameters.Add(New SqlParameter("@FileName", SqlDbType.VarChar, 200) With {.Value = strFileTemp.Trim})
            myCmd.Connection = objCon
            myAdapter.SelectCommand = myCmd
            myAdapter.Fill(myDS)

            If myDS.Tables(0).Rows.Count > 0 Then
                result = False
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message)
            ShowMessage(lblMessage, ex.Message, True)
        End Try
        Return result
    End Function
    Function UploadFile() As List(Of Parameter.AccMntBase)
        Dim list As New List(Of Parameter.AccMntBase)
        Dim data As New DataTable

        Try
            'data = CSVToDataTable(Me.FileName)--> kalau pakai csv
            data = FileToDataTable()

            For index = 0 To data.Rows.Count - 1
                If (index >= 0) Then
                    Dim schedule As New Parameter.AccMntBase

                    schedule.BranchId = data.Rows(index).Item(0).ToString().Trim
                    schedule.ApplicationID = data.Rows(index).Item(1).ToString().Trim
                    schedule.InsSeqNo = data.Rows(index).Item(2).ToString().Trim
                    schedule.DueDate = ConvertDate2(IIf(data.Rows(index).Item(3).ToString().Trim = "", "01/01/1900", data.Rows(index).Item(3).ToString().Trim))
                    schedule.PrincipalAmount = Convert.ToDecimal(data.Rows(index).Item(4).Trim) 'data.Rows(index).Item(4).ToString()
                    schedule.InterestAmount = Convert.ToDecimal(data.Rows(index).Item(5).Trim) 'data.Rows(index).Item(5).ToString()
                    schedule.OutstandingPrincipal = Convert.ToDecimal(data.Rows(index).Item(6).Trim) 'data.Rows(index).Item(6).ToString()
                    schedule.OutStandingInterest = Convert.ToDecimal(data.Rows(index).Item(7).Trim) 'data.Rows(index).Item(7).ToString()

                    Me.ApplicationID = schedule.ApplicationID
                    Me.InsSeqNo = schedule.InsSeqNo

                    Dim query As New Parameter.AccMntBase
                    If list.Count > 0 Then
                        query = list.Find(AddressOf PredicateFunction)
                    Else
                        query = Nothing
                    End If

                    If query Is Nothing Then
                        list.Add(schedule)
                    End If
                End If

            Next
        Catch ex As Exception
            Throw New ApplicationException(ex.Message)
        End Try

        Return list
    End Function
    Public Function PredicateFunction(ByVal schedule As Parameter.AccMntBase) As Boolean
        Return schedule.ApplicationID = Me.ApplicationID And schedule.InsSeqNo = Me.InsSeqNo
    End Function
    Private Function FileToDataTable() As DataTable
        Dim dt As New DataTable()
        Using doc As SpreadsheetDocument = SpreadsheetDocument.Open(FileName, False)
            Dim sheet As Sheet = doc.WorkbookPart.Workbook.Sheets.GetFirstChild(Of Sheet)()

            Dim worksheet As Worksheet = TryCast(doc.WorkbookPart.GetPartById(sheet.Id.Value), WorksheetPart).Worksheet

            Dim rows As IEnumerable(Of Row) = worksheet.GetFirstChild(Of SheetData)().Descendants(Of Row)()

            For Each row As Row In rows
                If row.RowIndex.Value = 1 Then
                    Dim clm As Integer = 0
                    For Each cell As Cell In row.Descendants(Of Cell)()
                        If GetValue(doc, cell).Trim <> "" Then
                            dt.Columns.Add(GetValue(doc, cell))
                        Else
                            dt.Columns.Add(GetValue(doc, cell).Trim & clm.ToString)
                        End If
                        clm += 1
                    Next
                Else
                    dt.Rows.Add()
                    Dim i As Integer = 0
                    For Each cell As Cell In row.Descendants(Of Cell)()
                        dt.Rows(dt.Rows.Count - 1)(i) = GetValue(doc, cell)
                        i += 1
                    Next
                End If
            Next
        End Using
        Return dt
    End Function
    Private Function GetValue(doc As SpreadsheetDocument, cell As Cell) As String
        Dim value As String = cell.CellValue.InnerText
        If cell.DataType IsNot Nothing AndAlso cell.DataType.Value = CellValues.SharedString Then
            Return doc.WorkbookPart.SharedStringTablePart.SharedStringTable.ChildElements.GetItem(Integer.Parse(value)).InnerText
        End If
        Return value
    End Function
    Private Function createDataUpload() As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("UploadDate")
        dt.Columns.Add("FileName")
        dt.Columns.Add("FileDirectory")
        dt.Columns.Add("BranchId")
        dt.Columns.Add("ApplicationID")
        dt.Columns.Add("InsSeqNo")
        dt.Columns.Add("DueDate")
        dt.Columns.Add("PrincipalAmount")
        dt.Columns.Add("InterestAmount")
        dt.Columns.Add("OutstandingPrincipal")
        dt.Columns.Add("OutstandingInterest")
        Return dt
    End Function
#End Region

    Private Sub imbCalRate_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbCalRate.Click
        Dim Eff As Double
        Dim Supp As Double
        If lblTotAdvAmt.Text <> "" And lblTotDiscAmt.Text <> "" And txtInstallmentAmt.Text <> "" And txtEffRate.Text <> "" Then
            Try
                Call Total()
                If Me.InstallmentScheme = "IR" Then
                    Entities.NTF = Me.NewPrinciple
                    Entities.installmentamount = CDbl(txtInstallmentAmt.Text)
                    Entities.FirstInstallment = "AR"
                    Entities.MydataSet = GetEntryInstallment()
                    Entities.PaymentFrequency = cbopaymentFreq.SelectedValue
                    Eff = m_controllerFinancial.GetNewEffRateIRR(Entities)
                    txtEffRate.Text = Eff.ToString.Trim
                ElseIf Me.InstallmentScheme = "ST" Then
                    If Me.StepUpDownType = "NM" Then
                        If IsValidValidation(txtStep.Text, lblInstallmentNum, "Number Of Step ") = False Then
                            Exit Sub
                        End If
                        Entities.NTF = Me.NewPrinciple
                        Entities.installmentamount = CDbl(txtInstallmentAmt.Text)
                        Entities.FirstInstallment = "AR"
                        Entities.InstallmentScheme = Me.InstallmentScheme
                        Entities.MydataSet = GetEntryInstallmentStepUpDown()
                        Entities.PaymentFrequency = cbopaymentFreq.SelectedValue
                        Eff = m_controllerFinancial.GetNewEffRateIRR(Entities)
                    ElseIf Me.StepUpDownType = "RL" Then
                        If IsValidValidation(txtCummulative.Text, lblInstallmentNum, "Cummulative ") = False Then
                            Exit Sub
                        End If
                        Entities.NTF = Me.NewPrinciple
                        Entities.installmentamount = CDbl(txtInstallmentAmt.Text)
                        Entities.FirstInstallment = "AR"
                        Entities.PaymentFrequency = cbopaymentFreq.SelectedValue
                        Entities.CummulativeStart = CInt(txtCummulative.Text)
                        Eff = m_controllerFinancial.GetNewEffRateRegLeasing(Entities)
                    Else
                        If IsValidValidation(txtStep.Text, lblInstallmentNum, "Number Of Step ") = False Then
                            Exit Sub
                        End If
                        If IsValidValidation(txtCummulative.Text, lblInstallmentNum, "Cummulative ") = False Then
                            Exit Sub
                        End If
                        Entities.NTF = Me.NewPrinciple
                        Entities.installmentamount = CDbl(txtInstallmentAmt.Text)
                        Entities.FirstInstallment = "AR"
                        Entities.InstallmentScheme = Me.InstallmentScheme
                        Entities.MydataSet = GetEntryInstallmentStepUpDown()
                        Entities.PaymentFrequency = cbopaymentFreq.SelectedValue
                        Entities.CummulativeStart = CInt(txtCummulative.Text)
                        Eff = m_controllerFinancial.GetNewEffRateLeasing(Entities)
                    End If
                    txtEffRate.Text = Eff.ToString.Trim
                Else
                    Eff = CountRate(Me.NewPrinciple)
                    Context.Trace.Write("Eff = " & Eff)
                    txtEffRate.Text = Eff.ToString.Trim

                End If

            Catch ex As Exception

                ShowMessage(lblMessage, "Jumlah Angsuran Salah", True)
            End Try
        End If
    End Sub


    Private Sub imbCalAmor_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbCalAmor.Click
        Dim oEntities As New Parameter.FinancialData
        Dim Inst As Double
        If lblTotAdvAmt.Text <> "" And lblTotDiscAmt.Text <> "" And txtInstallmentAmt.Text <> "" And txtEffRate.Text <> "" Then
            If Me.StepUpDownType = "RL" Or Me.StepUpDownType = "LS" Then
                If CInt(txtCummulative.Text) > CInt(lblInstallmentNum.Text) Then

                    ShowMessage(lblMessage, "Kumulatif  < lama Angsuran", True)
                    Exit Sub
                End If

            End If

            If Me.InstallmentScheme = "ST" Then
                Call ViewInstallmentUpload()
            Else
                If CInt(txtEffRate.Text) <= 0 Then

                    ShowMessage(lblMessage, "Bunga Effective harus > 0", True)
                    Exit Sub
                End If
            End If
            imbCalRate.Enabled = True
            Call Total()
            If Me.InstallmentScheme = "RF" Then
                Try

                    Inst = InstAmt(CDbl(txtEffRate.Text))
                    txtInstallmentAmt.Text = Math.Round(Inst, 0).ToString.Trim
                    FlatRate()

                    Call ViewInstallmentRF()
                Catch ex As Exception

                    ShowMessage(lblMessage, "Bunga Effective Salah", True)
                End Try
            ElseIf Me.InstallmentScheme = "IR" Then
                BuatTabelInstallment()
                txtInstallmentAmt.Enabled = True
            Else
                Try
                    With oEntities
                        .NTF = Me.NewPrinciple
                        .NumOfInstallment = Me.NewNum 'CInt(lblInstallmentNum.Text)
                        .GracePeriod = 0
                    End With
                    oEntities = m_controllerFinancial.GetPrincipleAmount(oEntities)
                    Me.PrincipleAmount = oEntities.Principal
                    txtInstallmentAmt.Text = Me.PrincipleAmount.ToString
                Catch ex As Exception

                    ShowMessage(lblMessage, "Jumlah Pokok Salah", True)
                End Try
            End If
        End If
    End Sub
    Public Sub ViewInstallmentRF()
        Dim oFinancialData As New Parameter.FinancialData
        BuatTableView()
        oFinancialData.MydataSet = MakeAmortTableView(CInt(Me.NewTenor), CDbl(txtInstallmentAmt.Text), CDbl(lblNPA.Text), CDbl(txtEffRate.Text), CInt(cbopaymentFreq.SelectedValue))
        dtgViewInstallment2.DataSource = oFinancialData.MydataSet
        pnlViewST.Visible = True
        oFinancialData.MydataSet.Tables(0).Rows(0).Delete()
        dtgViewInstallment2.DataBind()
        dtgViewInstallment2.CurrentPageIndex = 0
    End Sub

    Public Sub ViewInstallmentUpload()
        Dim parameter As New Parameter.AccMntBase
        With parameter
            .ApplicationID = Me.ApplicationID
            .strConnection = GetConnectionString()
        End With
        GetAmortization(parameter)
        dtgViewInstallment.DataSource = parameter.ListData
        pnlViewST.Visible = True
        dtgViewInstallment.DataBind()
        dtgViewInstallment.CurrentPageIndex = 0
    End Sub
    Function GetAmortization(ByVal CustomClass1 As Parameter.AccMntBase) As Parameter.AccMntBase
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            params(0).Value = Me.ApplicationID
            CustomClass1.ListData = SqlHelper.ExecuteDataset(CustomClass1.strConnection, CommandType.StoredProcedure, "spGetAmortList", params).Tables(0)
            Return CustomClass1
        Catch exp As Exception
        End Try
    End Function

#Region "Entry Installment Irregular"
    Private Function GetEntryInstallment() As DataSet
        Dim inc As Integer
        Dim jmlRow As Integer
        Dim InstallmentAmount As Double
        Dim objDs As New DataSet
        Dim oFinancialData As New Parameter.FinancialData

        jmlRow = dtgEntryInstallment.Items.Count
        myDataTable = New DataTable("Installment")
        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Int32")
        myDataColumn.ColumnName = "No"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "NoOfStep"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "Amount"
        myDataTable.Columns.Add(myDataColumn)

        For inc = 0 To jmlRow - 1
            'Dim txtEntryInstallment As New TextBox
            'txtEntryInstallment = CType(dtgEntryInstallment.Items(inc).FindControl("txtEntryInstallment"), TextBox)
            'InstallmentAmount = CDbl(txtEntryInstallment.Text)
            Dim txtEntryInstallment As New ucNumberFormat
            txtEntryInstallment = CType(dtgEntryInstallment.Items(inc).FindControl("txtEntryInstallment"), ucNumberFormat)
            InstallmentAmount = CDbl(txtEntryInstallment.Text)
            myDataRow = myDataTable.NewRow()
            myDataRow("No") = inc
            'myDataRow("NoOfStep") = CDbl(CType(dtgEntryInstallment.Items(inc).FindControl("txtNoStep"), TextBox).Text)
            myDataRow("NoOfStep") = CDbl(CType(dtgEntryInstallment.Items(inc).FindControl("txtNoStep"), ucNumberFormat).Text)
            myDataRow("Amount") = InstallmentAmount
            myDataTable.Rows.Add(myDataRow)
        Next
        objDs.Tables.Add(myDataTable)
        Return objDs
    End Function
    Private Function GetEntryInstallmentStepUpDown() As DataSet
        Dim inc As Integer
        Dim inc2 As Integer
        Dim inc3 As Integer
        Dim intSisa As Integer
        Dim jmlRow As Integer
        Dim jmlRow2 As Integer
        Dim InstallmentAmount As Double
        Dim intPayFreq As Integer
        Dim intNumOfInst As Integer
        Dim objDs As New DataSet
        Dim oFinancialData As New Parameter.FinancialData
        myDataTable = New DataTable
        jmlRow = dtgEntryInstallment.Items.Count
        intNumOfInst = Me.NewNum 'CInt(lblInstallmentNum.Text)

        myDataTable = New DataTable("Installment")
        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Int32")
        myDataColumn.ColumnName = "No"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "Amount"
        myDataTable.Columns.Add(myDataColumn)
        inc3 = 0
        For inc = 0 To jmlRow - 1
            'Dim txtEntryInstallment As New TextBox
            'jmlRow2 = CInt(CType(dtgEntryInstallment.Items(inc).FindControl("txtNoStep"), TextBox).Text)
            'txtEntryInstallment = CType(dtgEntryInstallment.Items(inc).FindControl("txtEntryInstallment"), TextBox)
            Dim txtEntryInstallment As New ucNumberFormat
            jmlRow2 = CInt(CType(dtgEntryInstallment.Items(inc).FindControl("txtNoStep"), ucNumberFormat).Text)
            txtEntryInstallment = CType(dtgEntryInstallment.Items(inc).FindControl("txtEntryInstallment"), ucNumberFormat)
            InstallmentAmount = CDbl(txtEntryInstallment.Text)
            For inc2 = 1 To jmlRow2
                myDataRow = myDataTable.NewRow()
                myDataRow("No") = inc3
                myDataRow("Amount") = InstallmentAmount
                myDataTable.Rows.Add(myDataRow)
                inc3 = inc3 + 1
            Next
        Next
        intSisa = (Me.NewNum - inc3)  '(CInt(lblInstallmentNum.Text) - inc3)
        InstallmentAmount = CDbl(txtInstallmentAmt.Text)
        If inc3 <= Me.NewNum Then  'CInt(lblInstallmentNum.Text) Then
            For inc = 1 To intSisa
                myDataRow = myDataTable.NewRow()
                myDataRow("No") = inc3
                myDataRow("Amount") = InstallmentAmount
                myDataTable.Rows.Add(myDataRow)
                inc3 = inc3 + 1
            Next
        End If
        objDs.Tables.Add(myDataTable)
        Return objDs
    End Function

    Private Function GetInstallmentAmountStepUpDown() As Double
        Dim inc As Integer
        Dim inc2 As Integer
        Dim inc3 As Integer
        Dim intSisa As Integer
        Dim jmlRow As Integer
        Dim jmlRow2 As Integer
        Dim InstallmentAmount As Double
        Dim dblRunRate As Double
        Dim dblPokokHutangST As Double
        Dim intPayFreq As Integer
        Dim intNumOfInst As Integer
        Dim oFinancialData As New Parameter.FinancialData
        myDataTable = New DataTable
        jmlRow = dtgEntryInstallment.Items.Count
        intNumOfInst = Me.NewNum  'CInt(lblInstallmentNum.Text)
        Dim PokokHutang(intNumOfInst) As Double
        Dim Interest(intNumOfInst) As Double
        Dim Principal(intNumOfInst) As Double
        dblRunRate = CDbl(txtEffRate.Text)
        intPayFreq = CInt(cbopaymentFreq.SelectedValue)
        PokokHutang(0) = Me.NewPrinciple
        Principal(0) = 0
        Interest(0) = 0
        inc3 = 0
        For inc = 0 To jmlRow - 1
            'Dim txtEntryInstallment As New TextBox
            'jmlRow2 = CInt(CType(dtgEntryInstallment.Items(inc).FindControl("txtNoStep"), TextBox).Text)
            'txtEntryInstallment = CType(dtgEntryInstallment.Items(inc).FindControl("txtEntryInstallment"), TextBox)
            Dim txtEntryInstallment As New ucNumberFormat
            jmlRow2 = CInt(CType(dtgEntryInstallment.Items(inc).FindControl("txtNoStep"), ucNumberFormat).Text)
            txtEntryInstallment = CType(dtgEntryInstallment.Items(inc).FindControl("txtEntryInstallment"), ucNumberFormat)
            InstallmentAmount = CDbl(txtEntryInstallment.Text)
            For inc2 = 1 To jmlRow2
                inc3 = inc3 + 1
                Interest(inc3) = Math.Round((PokokHutang(inc3 - 1) * dblRunRate) / (m_controllerFinancial.GetTerm(intPayFreq) * 100), 2)

                Principal(inc3) = InstallmentAmount - Interest(inc3)
                PokokHutang(inc3) = PokokHutang(inc3 - 1) - Principal(inc3)
            Next
        Next
        intSisa = (Me.NewNum - inc3) '(CInt(lblInstallmentNum.Text) - inc3)
        RunRate = (CDbl(txtEffRate.Text) / (m_controllerFinancial.GetTerm(CInt(cbopaymentFreq.SelectedValue)) * 100))
        dblPokokHutangST = PokokHutang(inc3) * -1 '((PokokHutang(0) - TotalInstallment) + TotalInterest) * -1

        InstallmentAmount = Pmt(RunRate, intSisa, dblPokokHutangST, 0, DueDate.EndOfPeriod)

        Return InstallmentAmount
    End Function
    Private Function GetInstallmentAmountStepUpDownLeasing() As Double
        Dim inc As Integer
        Dim inc2 As Integer
        Dim inc3 As Integer
        Dim intSisa As Integer
        Dim jmlRow As Integer
        Dim jmlRow2 As Integer
        Dim InstallmentAmount As Double
        Dim dblRunRate As Double
        Dim dblPokokHutangST As Double
        Dim intPayFreq As Integer
        Dim intNumOfInst As Integer
        Dim oFinancialData As New Parameter.FinancialData
        myDataTable = New DataTable
        jmlRow = dtgEntryInstallment.Items.Count
        intNumOfInst = Me.NewNum 'CInt(lblInstallmentNum.Text)
        Dim PokokHutang(intNumOfInst) As Double
        Dim Interest(intNumOfInst) As Double
        Dim Principal(intNumOfInst) As Double
        dblRunRate = CDbl(txtEffRate.Text)
        intPayFreq = CInt(cbopaymentFreq.SelectedValue)
        PokokHutang(0) = Me.NewPrinciple
        Principal(0) = 0
        Interest(0) = 0
        inc3 = 0
        For inc = 0 To jmlRow - 1
            'Dim txtEntryInstallment As New TextBox
            'jmlRow2 = CInt(CType(dtgEntryInstallment.Items(inc).FindControl("txtNoStep"), TextBox).Text)
            'txtEntryInstallment = CType(dtgEntryInstallment.Items(inc).FindControl("txtEntryInstallment"), TextBox)
            Dim txtEntryInstallment As New ucNumberFormat
            jmlRow2 = CInt(CType(dtgEntryInstallment.Items(inc).FindControl("txtNoStep"), ucNumberFormat).Text)
            txtEntryInstallment = CType(dtgEntryInstallment.Items(inc).FindControl("txtEntryInstallment"), ucNumberFormat)
            InstallmentAmount = CDbl(txtEntryInstallment.Text)
            For inc2 = 1 To jmlRow2
                inc3 = inc3 + 1
                Interest(inc3) = Math.Round((PokokHutang(inc3 - 1) * dblRunRate) / (m_controllerFinancial.GetTerm(intPayFreq) * 100), 2)
                Principal(inc3) = InstallmentAmount - Interest(inc3)
                PokokHutang(inc3) = PokokHutang(inc3 - 1) - Principal(inc3)
            Next
        Next
        intSisa = ((CInt(txtCummulative.Text) - Me.SeqNo) + 1 - inc3) '(CInt(txtCummulative.Text) - inc3)
        RunRate = (CDbl(txtEffRate.Text) / (m_controllerFinancial.GetTerm(CInt(cbopaymentFreq.SelectedValue)) * 100))
        dblPokokHutangST = PokokHutang(inc3) * -1
        InstallmentAmount = Pmt(RunRate, intSisa, dblPokokHutangST, 0, DueDate.EndOfPeriod)

        Return InstallmentAmount
    End Function
    Private Function GetInstallmentAmountStepUpDownLeasingTBL() As DataSet
        Dim inc As Integer
        Dim inc2 As Integer
        Dim inc3 As Integer
        Dim intSisa As Integer
        Dim jmlRow As Integer
        Dim jmlRow2 As Integer
        Dim InstallmentAmount As Double
        Dim dblRunRate As Double
        Dim dblPokokHutangST As Double
        Dim intPayFreq As Integer
        Dim intNumOfInst As Integer
        Dim oFinancialData As New Parameter.FinancialData
        myDataTable = New DataTable
        jmlRow = dtgEntryInstallment.Items.Count
        intNumOfInst = Me.NewNum 'CInt(lblInstallmentNum.Text)
        Dim PokokHutang(intNumOfInst) As Double
        Dim Interest(intNumOfInst) As Double
        Dim Principal(intNumOfInst) As Double
        dblRunRate = CDbl(txtEffRate.Text)
        intPayFreq = CInt(cbopaymentFreq.SelectedValue)
        PokokHutang(0) = Me.NewPrinciple
        Principal(0) = 0
        Interest(0) = 0
        inc3 = 0
        myDataTable = New DataTable("Installment")
        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Int32")
        myDataColumn.ColumnName = "No"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "Amount"
        myDataTable.Columns.Add(myDataColumn)

        For inc = 0 To jmlRow - 1
            'Dim txtEntryInstallment As New TextBox			
            'jmlRow2 = CInt(CType(dtgEntryInstallment.Items(inc).FindControl("txtNoStep"), TextBox).Text)
            'txtEntryInstallment = CType(dtgEntryInstallment.Items(inc).FindControl("txtEntryInstallment"), TextBox)
            Dim txtEntryInstallment As New ucNumberFormat
            jmlRow2 = CInt(CType(dtgEntryInstallment.Items(inc).FindControl("txtNoStep"), ucNumberFormat).Text)
            txtEntryInstallment = CType(dtgEntryInstallment.Items(inc).FindControl("txtEntryInstallment"), ucNumberFormat)
            InstallmentAmount = CDbl(txtEntryInstallment.Text)
            For inc2 = 1 To jmlRow2
                inc3 = inc3 + 1
                Interest(inc3) = Math.Round((PokokHutang(inc3 - 1) * dblRunRate) / (m_controllerFinancial.GetTerm(intPayFreq) * 100), 0)
                Principal(inc3) = InstallmentAmount - Interest(inc3)
                PokokHutang(inc3) = PokokHutang(inc3 - 1) - Principal(inc3)
                myDataRow = myDataTable.NewRow()
                myDataRow("No") = inc3
                myDataRow("Amount") = InstallmentAmount
                myDataTable.Rows.Add(myDataRow)
            Next
        Next
        intSisa = ((CInt(txtCummulative.Text) - Me.SeqNo) + 1 - inc3) '(CInt(txtCummulative.Text) - inc3)
        RunRate = (CDbl(txtEffRate.Text) / (m_controllerFinancial.GetTerm(CInt(cbopaymentFreq.SelectedValue)) * 100))
        dblPokokHutangST = PokokHutang(inc3) * -1
        InstallmentAmount = Pmt(RunRate, intSisa, dblPokokHutangST, 0, DueDate.EndOfPeriod)

        For inc = 1 To intSisa - 1
            inc3 = inc3 + 1
            Interest(inc3) = Math.Round((PokokHutang(inc3 - 1) * dblRunRate) / (m_controllerFinancial.GetTerm(intPayFreq) * 100), 0)
            Principal(inc3) = InstallmentAmount - Interest(inc3)
            PokokHutang(inc3) = PokokHutang(inc3 - 1) - Principal(inc3)

            myDataRow = myDataTable.NewRow()
            myDataRow("No") = inc3
            myDataRow("Amount") = InstallmentAmount
            myDataTable.Rows.Add(myDataRow)
        Next
        intSisa = (Me.NewNum - inc3) '(CInt(lblInstallmentNum.Text) - inc3)
        dblPokokHutangST = PokokHutang(inc3) * -1
        InstallmentAmount = Pmt(RunRate, intSisa, dblPokokHutangST, 0, DueDate.EndOfPeriod)
        For inc = 1 To intSisa
            inc3 = inc3 + 1
            Interest(inc3) = Math.Round((PokokHutang(inc3 - 1) * dblRunRate) / (m_controllerFinancial.GetTerm(intPayFreq) * 100), 0)
            Principal(inc3) = InstallmentAmount - Interest(inc3)
            PokokHutang(inc3) = PokokHutang(inc3 - 1) - Principal(inc3)

            myDataRow = myDataTable.NewRow()
            myDataRow("No") = inc3
            myDataRow("Amount") = InstallmentAmount
            myDataTable.Rows.Add(myDataRow)
        Next
        myDataSet.Tables.Add(myDataTable)
        Return myDataSet
    End Function

    Private Sub ImgEntryInstallment_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles ImgEntryInstallment.Click
        Dim oFinancialData As New Parameter.FinancialData
        Dim oDs As New DataSet
        Dim oView As New DataTable

        If Me.InstallmentScheme = "ST" Then
            If IsValidEntry(dtgEntryInstallment, Me.NewPrinciple, CInt(lblInstallmentNum.Text), CInt(txtCummulative.Text), Me.StepUpDownType) = False Then

                ShowMessage(lblMessage, "Data Entry Angsuran Salah", True)
                Exit Sub
            End If
            If Me.StepUpDownType = "NM" Then
                txtInstallmentAmt.Text = Math.Round(GetInstallmentAmountStepUpDown(), 0).ToString.Trim
            Else
                txtInstallmentAmt.Text = Math.Round(GetInstallmentAmountStepUpDownLeasing(), 0).ToString.Trim
            End If
            txtInstallmentAmt.Enabled = True
            Call ViewInstallment()
        Else
            'Call ViewInstallmentIRR()
            With oFinancialData
                .BranchId = Replace(Me.sesBranchId, "'", "")
                .AppID = Me.ApplicationID
                .NTF = Me.NewPrinciple
                .EffectiveRate = CDbl(txtEffRate.Text)
                .SupplierRate = CDbl(txtEffRate.Text)
                .PaymentFrequency = cbopaymentFreq.SelectedValue
                .FirstInstallment = "AR"
                .Tenor = Me.NewNum
                .GracePeriod = 0
                .GracePeriodType = "0"
                .BusDate = Me.BusinessDate
                .DiffRate = Me.DiffRate
                .strConnection = GetConnectionString()
                .MydataSet = GetEntryInstallment()
                .IsSave = 0
                .Flag = "Resch"
            End With
            oFinancialData = m_controllerFinancial.SaveAmortisasiIRR(oFinancialData)
            'oFinancialData = m_controllerResch.ViewReschedulingIRR(oFinancialData)
            'pnlViewST.Visible = True
            'Try
            '    dtgViewInstallment.DataSource = oFinancialData.Data1.DefaultView
            'Catch ex As Exception
            '    ex.Message.ToString()
            'End Try
            'dtgViewInstallment.DataBind()
            txtInstallmentAmt.Text = Math.Round(oFinancialData.installmentamount, 0).ToString.Trim
            'pnlViewST.Visible = False
            pnlViewST.Visible = True
        End If

    End Sub

    Private Function IsValidEntry(ByVal Dg As DataGrid, ByVal NTF As Double, ByVal NumOfIns As Integer, ByVal Cummulative As Integer, ByVal StepUpDownType As String) As Boolean
        Dim inc As Integer
        Dim intStep As Integer
        Dim intTotStep As Integer
        Dim dblAmount As Double
        Dim dblTotAmount As Double
        intTotStep = 0
        dblTotAmount = 0
        For inc = 0 To Dg.Items.Count - 1
            'Dim txtStep As New TextBox
            'Dim txtAmount As New TextBox
            Dim txtStep As New ucNumberFormat
            Dim txtAmount As New ucNumberFormat
            txtStep = CType(Dg.Items(inc).FindControl("txtNoStep"), ucNumberFormat)
            txtAmount = CType(Dg.Items(inc).FindControl("txtEntryInstallment"), ucNumberFormat)
            If StepUpDownType = "NM" Then
                dblAmount = CDbl(txtAmount.Text)
                dblTotAmount = dblTotAmount + dblAmount
                intStep = CInt(txtStep.Text)
                intTotStep = intTotStep + intStep
            ElseIf StepUpDownType = "RL" Then
                intStep = CInt(txtStep.Text)
                intTotStep = intTotStep + intStep
            Else
                intStep = CInt(txtStep.Text)
                dblAmount = CDbl(txtAmount.Text) * intStep
                intTotStep = intTotStep + intStep
                dblTotAmount = dblTotAmount + dblAmount
            End If
        Next
        If StepUpDownType = "NM" Then
            If intTotStep >= NumOfIns Then
                Return False
            End If
            If dblTotAmount >= NTF Then
                Return False
            End If
        ElseIf StepUpDownType = "RL" Then
            If intTotStep >= NumOfIns Then
                Return False
            End If
            If Cummulative < intTotStep Then
                Return False
            End If
        Else
            If Cummulative < intTotStep Then
                Return False
            End If
            If intTotStep >= NumOfIns Then
                Return False
            End If
            If dblTotAmount >= FormatNumber(NTF, 0) Then
                Return False
            End If
        End If
        Return True
    End Function
#End Region

#Region "View Installment Step Up Down"
    Private Sub ViewInstallment()
        Dim oFinancialData As New Parameter.FinancialData
        If Me.InstallmentScheme = "ST" Then
            If Me.StepUpDownType = "NM" Then
                oFinancialData.MydataSet = GetEntryInstallmentStepUpDown()
            ElseIf Me.StepUpDownType = "RL" Then
                Entities.NTF = Me.NewPrinciple
                Entities.EffectiveRate = CDbl(txtEffRate.Text)
                Entities.PaymentFrequency = cbopaymentFreq.SelectedValue
                Entities.FirstInstallment = "AR"
                Entities.CummulativeStart = (CInt(txtCummulative.Text) - Me.SeqNo) + 1 'Me.MaxSeqNo - CInt(txtCummulative.Text) '+ 1
                Entities.NumOfInstallment = Me.NewNum ' CInt(lblInstallmentNum.Text) '
                Entities.installmentamount = CDbl(txtInstallmentAmt.Text)
                Entities = m_controllerFinancial.GetStepUpDownRegLeasingTBL(Entities)
                oFinancialData.MydataSet = Entities.MydataSet
            Else
                oFinancialData.MydataSet = GetInstallmentAmountStepUpDownLeasingTBL()
            End If
        End If
        pnlViewST.Visible = True

        oFinancialData.BranchId = Replace(Me.sesBranchId, "'", "")
        oFinancialData.AppID = Me.ApplicationID
        oFinancialData.NTF = Me.NewPrinciple
        oFinancialData.EffectiveRate = CDbl(txtEffRate.Text)
        oFinancialData.SupplierRate = CDbl(txtEffRate.Text)
        oFinancialData.PaymentFrequency = cbopaymentFreq.SelectedValue
        oFinancialData.FirstInstallment = "AR"
        oFinancialData.Tenor = Me.NewNum
        oFinancialData.GracePeriod = 0
        oFinancialData.GracePeriodType = "0"
        oFinancialData.BusDate = Me.BusinessDate
        oFinancialData.DiffRate = Me.DiffRate
        oFinancialData.Flag = "Resch"
        oFinancialData.strConnection = GetConnectionString()
        oFinancialData = m_controllerFinancial.ListAmortisasiStepUpDown(oFinancialData)
        dtgViewInstallment.DataSource = oFinancialData.ListData
        dtgViewInstallment.DataBind()
    End Sub
#End Region

    Private Sub imbReset_Click(ByVal sender As Object, ByVal e As EventArgs) Handles imbReset.Click
        deletereschedule(Me.ApplicationID.Trim)
        pnlViewST.Visible = False
    End Sub
    Private Sub deletereschedule(ByVal ApplicationID As String)
        Dim objcommand As New SqlCommand
        Dim objconnection As New SqlConnection(GetConnectionString)
        Dim objTransaction As SqlTransaction = Nothing
        Try
            If objconnection.State = ConnectionState.Closed Then objconnection.Open()
            objTransaction = objconnection.BeginTransaction

            objcommand.CommandType = CommandType.StoredProcedure
            objcommand.Connection = objconnection
            objcommand.Transaction = objTransaction
            objcommand.CommandText = "spResetUploadReschedule"
            objcommand.Parameters.Add("@ApplicationID", SqlDbType.VarChar, 50).Value = ApplicationID
            objcommand.ExecuteNonQuery()
            objTransaction.Commit()
            ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, False)
        Catch exp As Exception
            objTransaction.Rollback()
            ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_FAILED, True)
        Finally
            If objconnection.State = ConnectionState.Open Then objconnection.Close()
            objconnection.Dispose()
            objcommand.Dispose()
        End Try
    End Sub


    Private Sub imbPrint_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbPrint.Click
        If SessionInvalid() Then
            Exit Sub
        End If
        If CheckFeature(Me.Loginid, Me.FormID, "PRINT", Me.AppId) Then
            If txtInstallmentAmt.Text <> "" And txtEffRate.Text <> "" Then
                Dim objreport As rptReschedulingPrintTrial = New rptReschedulingPrintTrial

                With Entities
                    .strConnection = GetConnectionString()
                    .BranchId = Me.BranchID
                    .ApplicationID = Me.ApplicationID
                    .BusinessDate = ConvertDate2(Me.sdate) 'Me.BusinessDate
                    .EffectiveDate = ConvertDate2(Me.sdate)
                    .LcInstallment = CDbl(txtInstallLC.Text)
                    .InstallmentCollFee = CDbl(txtInstallCollFee.Text)
                    .PDCBounceFee = CDbl(txtPDCBounceFee.Text)
                    .RepossessionFee = CDbl(txtReposessFee.Text)
                    .LcInsurance = CDbl(txtInsuranceLC.Text)
                    .InsuranceCollFee = CDbl(txtInsurCollFee.Text)
                    .STNKRenewalFee = CDbl(txtSTNKFee.Text)
                    .InsuranceClaimExpense = CDbl(txtInsuranceClaim.Text)
                    .PartialPrepaymentAmount = CDbl(txtPartialPay.Text)
                    .AdministrationFee = CDbl(txtAdminFee.Text)
                    .NewPrincipalAmount = Me.NewPrinciple
                    .EffectiveRate = CDbl(txtEffRate.Text)
                    .NumOfInstallment = CInt(lblInstallmentNum.Text)
                    .installmentamount = CDbl(txtInstallmentAmt.Text)
                    .PaymentFrequency = cbopaymentFreq.SelectedItem.Text
                    .FlatRate = Me.FRate         'CDbl(lblFlatRate.Text)
                    .Tenor = CInt(lblTenor.Text)
                End With

                Dim oData As New DataSet
                oData = m_controllerResch.ReschedulingPrintTrial(Entities)

                objreport.SetDataSource(oData)

                ''========================================================
                Dim discrete As ParameterDiscreteValue
                Dim ParamField As ParameterFieldDefinition
                Dim CurrentValue As CrystalDecisions.Shared.ParameterValues

                ParamField = objreport.DataDefinition.ParameterFields("CompanyName")
                discrete = New ParameterDiscreteValue
                discrete.Value = Me.sesCompanyName
                CurrentValue = New CrystalDecisions.Shared.ParameterValues
                CurrentValue = ParamField.DefaultValues
                CurrentValue.Add(discrete)
                ParamField.ApplyCurrentValues(CurrentValue)


                ParamField = objreport.DataDefinition.ParameterFields("BranchName")
                discrete = New ParameterDiscreteValue
                discrete.Value = Me.BranchName
                CurrentValue = New CrystalDecisions.Shared.ParameterValues
                CurrentValue = ParamField.DefaultValues
                CurrentValue.Add(discrete)
                ParamField.ApplyCurrentValues(CurrentValue)

                ParamField = objreport.DataDefinition.ParameterFields("LoginID")
                discrete = New ParameterDiscreteValue
                discrete.Value = Me.Loginid
                CurrentValue = New CrystalDecisions.Shared.ParameterValues
                CurrentValue = ParamField.DefaultValues
                CurrentValue.Add(discrete)
                ParamField.ApplyCurrentValues(CurrentValue)

                ParamField = objreport.DataDefinition.ParameterFields("EffectiveDate")
                discrete = New ParameterDiscreteValue
                discrete.Value = ConvertDate2(Me.sdate).ToString("MM/dd/yyyy")
                CurrentValue = New CrystalDecisions.Shared.ParameterValues
                CurrentValue = ParamField.DefaultValues
                CurrentValue.Add(discrete)
                ParamField.ApplyCurrentValues(CurrentValue)

                ParamField = objreport.DataDefinition.ParameterFields("BusinessDate")
                discrete = New ParameterDiscreteValue
                discrete.Value = Me.BusinessDate
                CurrentValue = New CrystalDecisions.Shared.ParameterValues
                CurrentValue = ParamField.DefaultValues
                CurrentValue.Add(discrete)
                ParamField.ApplyCurrentValues(CurrentValue)


                Dim strHTTPServer As String
                Dim StrHTTPApp As String
                Dim strNameServer As String
                Dim strFileLocation As String
                Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions

                objreport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
                objreport.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat

                strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
                strFileLocation += Me.Session.SessionID & Me.Loginid & "PrintTrialCalculation.pdf"
                DiskOpts.DiskFileName = strFileLocation
                objreport.ExportOptions.DestinationOptions = DiskOpts
                objreport.Export()

                strHTTPServer = Request.ServerVariables("PATH_INFO")
                strNameServer = Request.ServerVariables("SERVER_NAME")
                StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                strFileLocation = "http://" & strNameServer & "/" & StrHTTPApp & "/XML/" & Me.Session.SessionID & Me.Loginid & "PrintTrialCalculation.pdf"

                Response.Write("<script language = javascript>" & vbCrLf _
                & "var x = screen.width; " & vbCrLf _
                & "var y = screen.height; " & vbCrLf _
                & "window.open('" & strFileLocation & "','PrintTrialCalculation', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes, resizable=1') " & vbCrLf _
                & "</script>")
            End If
        End If
    End Sub

#Region "Make Amortization Table"
    Public Sub BuatTable()
        ' Create new DataColumn, set DataType, ColumnName and add to DataTable.
        myDataTable = New DataTable("AmortTable")

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Int32")
        myDataColumn.ColumnName = "No"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "Installment"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "Principal"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "Interest"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "PrincBalance"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "PrincInterest"
        myDataTable.Columns.Add(myDataColumn)

        'myDataColumn = New DataColumn
        'myDataColumn.DataType = System.Type.GetType("System.Double")
        'myDataColumn.ColumnName = "InterestTotal"
        'myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "InstallmentSisa"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Int32")
        myDataColumn.ColumnName = "StepFrom"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Int32")
        myDataColumn.ColumnName = "StepTo"
        myDataTable.Columns.Add(myDataColumn)
    End Sub
    Private Sub BuatTabelInstallment()
        Dim inc As Integer
        Dim intRowMax As Integer
        pnlEntryInstallment.Visible = True
        intRowMax = Me.NewNum - 1
        If Me.InstallmentScheme = "ST" Then
            If CInt(txtStep.Text) > CInt(lblInstallmentNum.Text) Then

                ShowMessage(lblMessage, "Number of Step harus  < Lama Angsuran (no)", True)
                pnlEntryInstallment.Visible = False

                Exit Sub
            End If

            pnlEntryInstallment.Visible = True
            intRowMax = CInt(txtStep.Text) - 1
        End If
        myDataTable = New DataTable("Installment")

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Int32")
        myDataColumn.ColumnName = "No"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "NoOfStep"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "Amount"
        myDataTable.Columns.Add(myDataColumn)


        For inc = 1 To intRowMax
            myDataRow = myDataTable.NewRow()
            myDataRow("No") = inc
            myDataRow("NoOfStep") = 0
            myDataRow("Amount") = 0
            myDataTable.Rows.Add(myDataRow)
        Next inc
        If Me.InstallmentScheme = "ST" Then
            dtgEntryInstallment.Columns(1).Visible = True
        Else
            dtgEntryInstallment.Columns(1).Visible = False
        End If
        dtgEntryInstallment.DataSource = myDataTable.DefaultView
        dtgEntryInstallment.DataBind()

    End Sub
    Public Function MakeAmortTable1(ByVal intNumInstallment As Integer, ByVal dblInstAmt As Double, ByVal dblNTF As Double, ByVal dblRunRate As Double, ByVal intPayFreq As Integer) As DataSet
        'Regular Installment - Advance - No Grace Period
        ReDim PokokHutang(intNumInstallment + 1)
        ReDim PokokBunga(intNumInstallment + 1)
        ReDim Bunga(intNumInstallment + 1)
        Dim TotalPrincipal As Double
        PokokHutang(0) = dblNTF
        Bunga(0) = 0
        BuatTable()
        For i = 1 To intNumInstallment + 1
            myDataRow = myDataTable.NewRow()
            If i = 1 Then 'Advance dan installment I
                myDataRow("No") = 0
                myDataRow("Installment") = 0
                myDataRow("Principal") = 0
                myDataRow("Interest") = 0
            ElseIf i = 2 Then
                myDataRow("No") = i - 1
                myDataRow("Installment") = dblInstAmt
                myDataRow("Interest") = 0
                myDataRow("Principal") = dblInstAmt
            Else
                myDataRow("No") = i - 1
                myDataRow("Installment") = dblInstAmt
                myDataRow("Interest") = Math.Round((PokokHutang(i - 1) * dblRunRate) / (m_controllerFinancial.GetTerm(intPayFreq) * 100), 2)
                myDataRow("Principal") = Math.Round(dblInstAmt - CDbl(myDataRow("Interest")), 2)
            End If
            If i = 1 Then
                myDataRow("PrincBalance") = PokokHutang(0)
            Else
                If i = intNumInstallment + 1 Then
                    myDataRow("PrincBalance") = 0
                Else
                    myDataRow("PrincBalance") = Math.Round(PokokHutang(i - 1) - CDbl(myDataRow("Principal")), 2)
                End If
            End If
            myDataRow("PrincInterest") = 0
            PokokHutang(i) = CDbl(myDataRow("PrincBalance"))
            Bunga(i) = CDbl(myDataRow("Interest"))
            TotalPrincipal = Math.Round(TotalPrincipal + CDbl(myDataRow("Principal")), 2)
            myDataTable.Rows.Add(myDataRow)
        Next i

        If TotalPrincipal > dblNTF Then
            SisaPrincipal = Math.Round(TotalPrincipal - dblNTF, 2)
        Else
            KurangPrincipal = Math.Round(dblNTF - TotalPrincipal, 2)
        End If

        If SisaPrincipal > 0 Then
            Bunga(intNumInstallment + 1) = Math.Round(Bunga(intNumInstallment + 1) + SisaPrincipal, 2)
        End If
        If KurangPrincipal > 0 Then
            Bunga(intNumInstallment + 1) = Math.Round(Bunga(intNumInstallment + 1) - KurangPrincipal, 2)
        End If

        PokokBunga(intNumInstallment + 1) = 0

        For i = intNumInstallment To 0 Step -1
            PokokBunga(i) = Math.Round(Bunga(i + 1) + PokokBunga(i + 1), 2)
            dblInterestTotal = Math.Round(dblInterestTotal + Bunga(i + 1), 2)
        Next
        Dim myDataSet As DataSet
        myDataSet = New DataSet
        myDataSet.Tables.Add(myDataTable)
        Dim custRow As DataRow
        i = 1
        For Each custRow In myDataSet.Tables("AmortTable").Rows
            If i <= intNumInstallment + 1 Then
                custRow("PrincInterest") = PokokBunga(i)
                'custRow("InterestTotal") = dblInterestTotal
                i = i + 1
            End If
        Next
        For Each custRow In myDataSet.Tables("AmortTable").Rows
            If CInt(custRow("No")) = intNumInstallment Then
                If SisaPrincipal > 0 Then
                    custRow("Principal") = Math.Round(CDbl(custRow("Principal")) - SisaPrincipal, 2)
                    custRow("Interest") = Math.Round(CDbl(custRow("Interest")) + SisaPrincipal, 2)
                    HitungUlangSisa = True
                End If
                If KurangPrincipal > 0 Then
                    custRow("Principal") = Math.Round(CDbl(custRow("Principal")) + KurangPrincipal, 2)
                    custRow("Interest") = Math.Round(CDbl(custRow("Interest")) - KurangPrincipal, 2)
                    HitungUlangKurang = True
                End If
            End If
        Next
        Return myDataSet
    End Function
    Public Function MakeAmortTable2(ByVal intNumInstallment As Integer, ByVal dblInstAmt As Double, ByVal dblNTF As Double, ByVal dblRunRate As Double, ByVal intPayFreq As Integer) As DataSet
        'Regular Installment - Arrear - No Grace Period
        ReDim PokokHutang(intNumInstallment + 1)
        ReDim Bunga(intNumInstallment + 1)
        ReDim PokokBunga(intNumInstallment + 1)
        Dim TotalPrincipal As Double
        Bunga(0) = 0
        PokokHutang(0) = dblNTF
        BuatTable()
        For i = 1 To intNumInstallment + 1
            myDataRow = myDataTable.NewRow()
            If i = 1 Then 'Installment I
                myDataRow("No") = 0
                myDataRow("Installment") = 0
                myDataRow("Principal") = 0
                myDataRow("Interest") = 0
            Else
                myDataRow("No") = i - 1
                myDataRow("Installment") = dblInstAmt
                myDataRow("Interest") = Math.Round((PokokHutang(i - 1) * dblRunRate) / (m_controllerFinancial.GetTerm(intPayFreq) * 100), 2)
                myDataRow("Principal") = Math.Round(dblInstAmt - CDbl(myDataRow("Interest")), 2)
            End If
            If i = 1 Then
                myDataRow("PrincBalance") = PokokHutang(0)
            ElseIf i = intNumInstallment + 1 Then
                myDataRow("PrincBalance") = 0
            Else
                myDataRow("PrincBalance") = Math.Round(PokokHutang(i - 1) - CDbl(myDataRow("Principal")), 2)
            End If
            myDataRow("PrincInterest") = 0
            PokokHutang(i) = CDbl(myDataRow("PrincBalance"))
            Bunga(i) = CDbl(myDataRow("Interest"))
            TotalPrincipal = Math.Round(TotalPrincipal + CDbl(myDataRow("Principal")), 2)
            myDataTable.Rows.Add(myDataRow)
        Next i

        If TotalPrincipal > dblNTF Then
            SisaPrincipal = Math.Round(TotalPrincipal - dblNTF, 2)
        Else
            KurangPrincipal = Math.Round(dblNTF - TotalPrincipal, 2)
        End If

        If SisaPrincipal > 0 Then
            Bunga(intNumInstallment + 1) = Math.Round(Bunga(intNumInstallment + 1) + SisaPrincipal, 2)
        End If
        If KurangPrincipal > 0 Then
            Bunga(intNumInstallment + 1) = Math.Round(Bunga(intNumInstallment + 1) - KurangPrincipal, 2)
        End If

        PokokBunga(intNumInstallment + 1) = 0

        For i = intNumInstallment To 0 Step -1
            PokokBunga(i) = Math.Round(Bunga(i + 1) + PokokBunga(i + 1), 2)
            dblInterestTotal = Math.Round(dblInterestTotal + Bunga(i + 1), 2)
        Next

        Dim myDataSet As DataSet
        myDataSet = New DataSet

        myDataSet.Tables.Add(myDataTable)

        Dim custRow As DataRow

        i = 1
        For Each custRow In myDataSet.Tables("AmortTable").Rows
            If i <= intNumInstallment + 1 Then
                custRow("PrincInterest") = PokokBunga(i)
                'custRow("InterestTotal") = dblInterestTotal
                i = i + 1
            End If
        Next

        For Each custRow In myDataSet.Tables("AmortTable").Rows
            If CInt(custRow("No")) = intNumInstallment Then
                If SisaPrincipal > 0 Then
                    custRow("Principal") = Math.Round(CDbl(custRow("Principal")) - SisaPrincipal, 2)
                    custRow("Interest") = Math.Round(CDbl(custRow("Interest")) + SisaPrincipal, 2)
                    HitungUlangSisa = True
                End If
                If KurangPrincipal > 0 Then
                    custRow("Principal") = Math.Round(CDbl(custRow("Principal")) + KurangPrincipal, 2)
                    custRow("Interest") = Math.Round(CDbl(custRow("Interest")) - KurangPrincipal, 2)
                    HitungUlangKurang = True
                End If
            End If
        Next
        Return myDataSet
    End Function
    Public Function MakeAmortTable3(ByVal intNumInstallment As Integer, ByVal dblInstAmt As Double, ByVal dblNTF As Double, ByVal dblRunRate As Double, ByVal intPayFreq As Integer, ByVal intGrace As Integer) As DataSet
        'Regular Installment - Arrear - Grace Period (Roll Over)
        ReDim PokokHutang(intNumInstallment + 1)
        ReDim Bunga(intNumInstallment + 1)
        ReDim PokokBunga(intNumInstallment + 1)
        Dim intGracePeriod As Integer = intGrace + 1
        Dim TotalPrincipal As Double
        Dim SisaPrincipal As Double
        Dim KurangPrincipal As Double
        Dim HitungUlangSisa As Boolean = False
        Dim HitungUlangKurang As Boolean = False

        Bunga(0) = 0
        PokokHutang(0) = dblNTF

        BuatTable()

        For i = 1 To intNumInstallment + 1
            myDataRow = myDataTable.NewRow()
            If i = 1 Then 'Installment I
                myDataRow("No") = 0
                myDataRow("Installment") = 0
                myDataRow("Principal") = 0
                myDataRow("Interest") = 0
            ElseIf i <= intGracePeriod Then
                myDataRow("No") = i - 1
                myDataRow("Installment") = 0
                myDataRow("Interest") = Math.Round((PokokHutang(i - 1) * dblRunRate) / (m_controllerFinancial.GetTerm(intPayFreq) * 100), 2)
                myDataRow("Principal") = Math.Round(CDbl(myDataRow("Installment")) - CDbl(myDataRow("Interest")), 2)
            Else
                myDataRow("No") = i - 1
                myDataRow("Installment") = dblInstAmt
                myDataRow("Interest") = Math.Round((PokokHutang(i - 1) * dblRunRate) / (m_controllerFinancial.GetTerm(intPayFreq) * 100), 2)
                myDataRow("Principal") = Math.Round(dblInstAmt - CDbl(myDataRow("Interest")), 2)
            End If
            If i = 1 Then
                myDataRow("PrincBalance") = PokokHutang(0)
            Else
                If i = intNumInstallment + 1 Then
                    myDataRow("PrincBalance") = 0
                Else
                    myDataRow("PrincBalance") = Math.Round(PokokHutang(i - 1) - CDbl(myDataRow("Principal")), 2)
                End If
            End If
            myDataRow("PrincInterest") = 0
            PokokHutang(i) = CDbl(myDataRow("PrincBalance"))
            Bunga(i) = CDbl(myDataRow("Interest"))
            TotalPrincipal = Math.Round(TotalPrincipal + CDbl(myDataRow("Principal")), 2)
            myDataTable.Rows.Add(myDataRow)
        Next i

        If TotalPrincipal > dblNTF Then
            SisaPrincipal = Math.Round(TotalPrincipal - dblNTF, 2)
        Else
            KurangPrincipal = Math.Round(dblNTF - TotalPrincipal, 2)
        End If

        If SisaPrincipal > 0 Then
            Bunga(intNumInstallment + 1) = Math.Round(Bunga(intNumInstallment + 1) + SisaPrincipal, 2)
        End If
        If KurangPrincipal > 0 Then
            Bunga(intNumInstallment + 1) = Math.Round(Bunga(intNumInstallment + 1) - KurangPrincipal, 2)
        End If

        PokokBunga(intNumInstallment + 1) = 0

        For i = intNumInstallment To 0 Step -1
            PokokBunga(i) = Math.Round(Bunga(i + 1) + PokokBunga(i + 1), 2)
            dblInterestTotal = Math.Round(dblInterestTotal + Bunga(i + 1), 2)
        Next
        Dim myDataSet As DataSet
        myDataSet = New DataSet
        myDataSet.Tables.Add(myDataTable)
        Dim custRow As DataRow
        i = 1
        For Each custRow In myDataSet.Tables("AmortTable").Rows
            If i <= intNumInstallment + 1 Then
                custRow("PrincInterest") = PokokBunga(i)
                'custRow("InterestTotal") = dblInterestTotal
                i = i + 1
            End If
        Next
        For Each custRow In myDataSet.Tables("AmortTable").Rows
            If CInt(custRow("No")) = intNumInstallment Then
                If SisaPrincipal > 0 Then
                    custRow("Principal") = Math.Round(CDbl(custRow("Principal")) - SisaPrincipal, 2)
                    custRow("Interest") = Math.Round(CDbl(custRow("Interest")) + SisaPrincipal, 2)
                    HitungUlangSisa = True
                End If
                If KurangPrincipal > 0 Then
                    custRow("Principal") = Math.Round(CDbl(custRow("Principal")) + KurangPrincipal, 2)
                    custRow("Interest") = Math.Round(CDbl(custRow("Interest")) - KurangPrincipal, 2)
                    HitungUlangKurang = True
                End If
            End If
        Next
        Return myDataSet
    End Function
    Public Function MakeAmortTable6(ByVal intNumInstallment As Integer, ByVal dblInstAmt As Double, ByVal dblNTF As Double, ByVal dblRunRate As Double, ByVal intPayFreq As Integer, ByVal intGrace As Integer) As DataSet
        'Regular Installment - Arrear - Grace Period (Interest Only)
        ReDim PokokHutang(intNumInstallment + 1)
        ReDim Bunga(intNumInstallment + 1)
        ReDim PokokBunga(intNumInstallment + 1)
        Dim intGracePeriod As Integer = intGrace + 1
        Dim TotalPrincipal As Double
        Dim SisaPrincipal As Double
        Dim KurangPrincipal As Double
        Dim HitungUlangSisa As Boolean = False
        Dim HitungUlangKurang As Boolean = False

        Bunga(0) = 0
        PokokHutang(0) = dblNTF

        BuatTable()

        For i = 1 To intNumInstallment + 1
            myDataRow = myDataTable.NewRow()
            If i = 1 Then 'Installment I
                myDataRow("No") = 0
                myDataRow("Installment") = 0
                myDataRow("Principal") = 0
                myDataRow("Interest") = 0
            ElseIf i <= intGracePeriod Then
                myDataRow("No") = i - 1
                myDataRow("Installment") = Math.Round(dblNTF * dblRunRate / m_controllerFinancial.GetTerm(intPayFreq) / 100, 2)
                myDataRow("Interest") = myDataRow("Installment")
                myDataRow("Principal") = 0
            ElseIf i = intGracePeriod + 1 Then
                myDataRow("No") = i - 1
                myDataRow("Interest") = Math.Round(dblNTF * dblRunRate / m_controllerFinancial.GetTerm(intPayFreq) / 100, 2)
                myDataRow("Installment") = dblInstAmt
                myDataRow("Principal") = Math.Round(dblInstAmt - CDbl(myDataRow("Interest")), 2)
            Else
                myDataRow("No") = i - 1
                myDataRow("Installment") = dblInstAmt
                myDataRow("Interest") = Math.Round((PokokHutang(i - 1) * dblRunRate) / (m_controllerFinancial.GetTerm(intPayFreq) * 100), 2)
                myDataRow("Principal") = Math.Round(dblInstAmt - CDbl(myDataRow("Interest")), 2)
            End If
            If i = 1 Then
                myDataRow("PrincBalance") = PokokHutang(0)
            Else
                If i = intNumInstallment + 1 Then
                    myDataRow("PrincBalance") = 0
                Else
                    myDataRow("PrincBalance") = Math.Round(PokokHutang(i - 1) - CDbl(myDataRow("Principal")), 2)
                End If
            End If
            myDataRow("PrincInterest") = 0
            PokokHutang(i) = CDbl(myDataRow("PrincBalance"))
            Bunga(i) = CDbl(myDataRow("Interest"))
            TotalPrincipal = Math.Round(TotalPrincipal + CDbl(myDataRow("Principal")), 2)
            myDataTable.Rows.Add(myDataRow)
        Next i

        If TotalPrincipal > dblNTF Then
            SisaPrincipal = Math.Round(TotalPrincipal - dblNTF, 2)
        Else
            KurangPrincipal = Math.Round(dblNTF - TotalPrincipal, 2)
        End If

        If SisaPrincipal > 0 Then
            Bunga(intNumInstallment + 1) = Math.Round(Bunga(intNumInstallment + 1) + SisaPrincipal, 2)
        End If
        If KurangPrincipal > 0 Then
            Bunga(intNumInstallment + 1) = Math.Round(Bunga(intNumInstallment + 1) - KurangPrincipal, 2)
        End If

        PokokBunga(intNumInstallment + 1) = 0

        For i = intNumInstallment To 0 Step -1
            PokokBunga(i) = Math.Round(Bunga(i + 1) + PokokBunga(i + 1), 2)
            dblInterestTotal = Math.Round(dblInterestTotal + Bunga(i + 1), 2)
        Next

        Dim myDataSet As DataSet
        myDataSet = New DataSet

        myDataSet.Tables.Add(myDataTable)
        Dim custRow As DataRow
        i = 1
        For Each custRow In myDataSet.Tables("AmortTable").Rows
            If i <= intNumInstallment + 1 Then
                custRow("PrincInterest") = PokokBunga(i)
                'custRow("InterestTotal") = dblInterestTotal
                i = i + 1
            End If
        Next
        For Each custRow In myDataSet.Tables("AmortTable").Rows
            If CInt(custRow("No")) = intNumInstallment Then
                If SisaPrincipal > 0 Then
                    custRow("Principal") = Math.Round(CDbl(custRow("Principal")) - SisaPrincipal, 2)
                    custRow("Interest") = Math.Round(CDbl(custRow("Interest")) + SisaPrincipal, 2)
                    HitungUlangSisa = True
                End If
                If KurangPrincipal > 0 Then
                    custRow("Principal") = Math.Round(CDbl(custRow("Principal")) + KurangPrincipal, 2)
                    custRow("Interest") = Math.Round(CDbl(custRow("Interest")) - KurangPrincipal, 2)
                    HitungUlangKurang = True
                End If
            End If
        Next
        Return myDataSet
    End Function
#End Region

    Private Sub imbNext_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbNext.Click
        Dim dtView As New DataTable
        Dim dtCrossDefault As New DataTable
        Dim dtTC As New DataTable
        Dim dtTC2 As New DataTable
        Dim drView, drCrossDefault, drTc, drTC2 As DataRow
        Dim j, i, k, l, m As Integer
        Dim oFinancialData As New Parameter.FinancialData

        If Me.InstallmentScheme = "RF" Then
            If CDbl(txtInstallmentAmt.Text) <> InstAmt(CDbl(txtEffRate.Text)) Then
                Context.Trace.Write("Cdbl(txtInstallmentAmt.Text) = " & CDbl(txtInstallmentAmt.Text))
                Context.Trace.Write("(CDbl(txtEffRate.Text)) " & CDbl(txtEffRate.Text))

                ShowMessage(lblMessage, "Jumlah Angsuran dan bunga Efektif Tidak syncronized. Harap klik Tombol Calculate", True)
                Exit Sub
            End If
        End If
        With dtView
            .Columns.Add("InsSeqNo", System.Type.GetType("System.String"))
            .Columns.Add("InstallmentAmount", System.Type.GetType("System.String"))
            .Columns.Add("PrincipalAmount", System.Type.GetType("System.String"))
            .Columns.Add("InterestAmount", System.Type.GetType("System.String"))
            .Columns.Add("OutstandingPrincipal", System.Type.GetType("System.String"))
            .Columns.Add("OutstandingInterest", System.Type.GetType("System.String"))
            .Columns.Add("Seq", System.Type.GetType("System.String"))
            .Columns.Add("DueDate", System.Type.GetType("System.String"))
        End With

        For i = 0 To dtgViewInstallment.Items.Count - 1
            drView = dtView.NewRow
            If Not dtgViewInstallment Is Nothing Then
                drView("InsSeqNo") = dtgViewInstallment.Items(i).Cells(0).Text.Trim
                drView("InstallmentAmount") = CType(dtgViewInstallment.Items(i).FindControl("lblIA"), Label).Text
                drView("PrincipalAmount") = CType(dtgViewInstallment.Items(i).FindControl("lblPA"), Label).Text
                drView("InterestAmount") = CType(dtgViewInstallment.Items(i).FindControl("lblInterest"), Label).Text
                drView("OutstandingPrincipal") = CType(dtgViewInstallment.Items(i).FindControl("lblOP"), Label).Text
                drView("OutstandingInterest") = CType(dtgViewInstallment.Items(i).FindControl("lblOI"), Label).Text
                drView("Seq") = CType(dtgViewInstallment.Items(i).FindControl("lblSeq"), Label).Text
                drView("DueDate") = dtgViewInstallment.Items(i).Cells(6).Text.Trim
                dtView.Rows.Add(drView)
            End If
        Next

        With dtCrossDefault
            .Columns.Add("No", System.Type.GetType("System.String"))
            .Columns.Add("Agreement", System.Type.GetType("System.String"))
            .Columns.Add("Name", System.Type.GetType("System.String"))
            .Columns.Add("AgreementDate", System.Type.GetType("System.String"))
            .Columns.Add("DefaultStatus", System.Type.GetType("System.String"))
            .Columns.Add("ContractStatus", System.Type.GetType("System.String"))
            .Columns.Add("CrossDefaultApplicationId", System.Type.GetType("System.String"))
        End With

        For i = 0 To dtgCrossDefault.Items.Count - 1
            drCrossDefault = dtCrossDefault.NewRow
            If Not dtgCrossDefault Is Nothing Then
                drCrossDefault("No") = CType(dtgCrossDefault.Items(i).FindControl("lblCDNo"), Label).Text
                drCrossDefault("Agreement") = CType(dtgCrossDefault.Items(i).FindControl("lblCDAgreementNo"), Label).Text
                drCrossDefault("Name") = CType(dtgCrossDefault.Items(i).FindControl("lblCDName"), Label).Text
                drCrossDefault("AgreementDate") = CType(dtgCrossDefault.Items(i).FindControl("lblCDAgreementDate"), Label).Text
                drCrossDefault("DefaultStatus") = CType(dtgCrossDefault.Items(i).FindControl("lblCDDefaultStatus"), Label).Text
                drCrossDefault("ContractStatus") = CType(dtgCrossDefault.Items(i).FindControl("lblCDContractStatus"), Label).Text
                drCrossDefault("CrossDefaultApplicationId") = CType(dtgCrossDefault.Items(i).FindControl("lblCDApplicationID"), Label).Text
                dtCrossDefault.Rows.Add(drCrossDefault)
            End If
        Next

        With dtTC
            .Columns.Add("No", System.Type.GetType("System.String"))
            .Columns.Add("TCName", System.Type.GetType("System.String"))
            .Columns.Add("Checked", System.Type.GetType("System.String"))
            .Columns.Add("IsMandatory", System.Type.GetType("System.String"))
            .Columns.Add("Notes", System.Type.GetType("System.String"))
            .Columns.Add("MasterTCID", System.Type.GetType("System.String"))
        End With

        For l = 0 To dtgTC.Items.Count - 1
            drTc = dtTC.NewRow
            If Not dtgTC Is Nothing Then
                drTc("No") = CType(dtgTC.Items(l).FindControl("lblTCNo"), Label).Text
                drTc("TCName") = dtgTC.Items(l).Cells(1).Text.Trim
                drTc("Checked") = CType(dtgTC.Items(l).FindControl("chkTCChecked"), CheckBox).Checked
                drTc("IsMandatory") = dtgTC.Items(l).Cells(3).Text.Trim
                drTc("Notes") = CType(dtgTC.Items(l).FindControl("txtTCNotes"), TextBox).Text
                drTc("MasterTCID") = dtgTC.Items(l).Cells(5).Text.Trim
                dtTC.Rows.Add(drTc)
            End If
        Next

        With dtTC2
            .Columns.Add("No", System.Type.GetType("System.String"))
            .Columns.Add("TCName", System.Type.GetType("System.String"))
            .Columns.Add("CheckList", System.Type.GetType("System.String"))
            .Columns.Add("Checked", System.Type.GetType("System.String"))
            .Columns.Add("IsMandatory", System.Type.GetType("System.String"))
            .Columns.Add("Notes", System.Type.GetType("System.String"))
            .Columns.Add("MasterTCID", System.Type.GetType("System.String"))
            .Columns.Add("MSTCCLSequenceNo", System.Type.GetType("System.String"))
        End With

        For m = 0 To dtgTC2.Items.Count - 1
            drTC2 = dtTC2.NewRow
            If Not dtgTC2 Is Nothing Then
                drTC2("No") = CType(dtgTC2.Items(m).FindControl("lblTCNo2"), Label).Text
                drTC2("TCName") = dtgTC2.Items(m).Cells(1).Text.Trim
                drTC2("CheckList") = dtgTC2.Items(m).Cells(2).Text.Trim
                drTC2("Checked") = CType(dtgTC2.Items(m).FindControl("chkTCCheck2"), CheckBox).Checked
                drTC2("IsMandatory") = dtgTC2.Items(m).Cells(4).Text.Trim
                drTC2("Notes") = CType(dtgTC2.Items(m).FindControl("txtTCNotes2"), TextBox).Text
                drTC2("MasterTCID") = dtgTC2.Items(m).Cells(6).Text.Trim
                drTC2("MSTCCLSequenceNo") = dtgTC2.Items(m).Cells(7).Text.Trim
                dtTC2.Rows.Add(drTC2)
            End If
        Next

        With oRescheduling
            Me.PartialPay = CDbl(txtPartialPay.Text)
            Me.AdminFee = CDbl(txtAdminFee.Text)
            'Me.InstallLC = CDbl(txtInstallLC.Text)

            Me.InstallCollFee = CDbl(txtInstallCollFee.Text)
            Me.InsuranceLC = CDbl(txtInsuranceLC.Text)
            Me.InsuranceCollFee = CDbl(txtInsurCollFee.Text)
            Me.PDCBounceFee = CDbl(txtPDCBounceFee.Text)
            Me.STNKRenewalFee = CDbl(txtSTNKFee.Text)
            Me.InsuranceClaim = CDbl(txtInsuranceClaim.Text)
            Me.ReposessionFee = CDbl(txtReposessFee.Text)

            Me.InstallLC = CDbl(txtOutstandingLC.Text)
            .OutstandingPrincipal = CDbl(txtOutstandingPrincipal.Text)
            .OutStandingInterest = CDbl(txtOutstandingInterest.Text)
            .InstallmentDue = CDbl(txtInstallmentDue.Text)
            .AccruedInterest = CDbl(txtAccruedInterest.Text)
            .chkPotong = CDbl(chkAdminFee.Checked)
            .Tenor = CInt(lblTenor.Text)
            .PODateTo = lblInstallmentNum.Text
            .InstallmentScheme = Me.InstallmentScheme
            .PartialPay = Me.PartialPay
            .AdminFee = Me.AdminFee
            .LcInstallment = Me.InstallLC
            .InstallmentCollFee = Me.InstallCollFee
            .LcInsurance = Me.InsuranceLC
            .InsuranceCollFee = Me.InsuranceCollFee
            .PDCBounceFee = Me.PDCBounceFee
            .STNKRenewalFee = Me.STNKRenewalFee
            .InsuranceClaimExpense = Me.InsuranceClaim
            .RepossessionFee = Me.ReposessionFee

            .TotAdvAmt = CDbl(lblTotAdvAmt.Text)
            .TotDiscamt = CDbl(lblTotDiscAmt.Text)

            .Rate = CDbl(txtEffRate.Text)
            .installmentamount = CDbl(txtInstallmentAmt.Text)
            .ListData = dtView
            .ListCrossDefault = dtCrossDefault
            .ListTerm = dtTC
            .ListCondition = dtTC2
            .ReasonID = oApprovalRequest.ReasonID
            .ReasonDescription = oApprovalRequest.ReasonName
            .RequestTo = oApprovalRequest.ToBeApprove
            .Notes = oApprovalRequest.Notes
            .ReasonTypeID = oApprovalRequest.ReasonTypeID
            .NewPrincipleAmount = Me.NewPrinciple
            .PaymentFreq = cbopaymentFreq.SelectedItem.Text
            .PayFreq = cbopaymentFreq.SelectedItem.Value
            .flat = FormatNumber(Me.FRate, 2)
            .txtSearch = Me.StepUpDownType
            .lblNumOfStep = lblNumberOfStep.Text
            .NoStep = CInt(IIf(txtStep.Text = "", "0", txtStep.Text))
            .lblCumm = lblCummulative.Text
            .Cummulative = CInt(IIf(txtCummulative.Text = "", "0", txtCummulative.Text))
            .NewTenor = Me.NewTenor
            .PPh = CDbl(txtPPh.Text)
            .PerjanjianNo = txtPerjanjianNo.Text

            If Me.InstallmentScheme = "IR" Then .MydataSet = GetEntryInstallment()
        End With

        Dim strRes As String = HttpContext.Current.Session.SessionID
        Session(strRes) = oRescheduling
        Response.Redirect("RestructFactAndMUFinal.aspx?Applicationid=" & Me.ApplicationID & "&branchID=" & Me.BranchID.Trim & "&sdate=" & Me.sdate & "&seqno=" & CStr(Me.SeqNo) & "&strRes=" & CStr(strRes) & "&invoiceno=" & Me.InvoiceNo.Trim & "&invoiceseqno=" & Me.InvoiceSeqNo)
    End Sub

End Class