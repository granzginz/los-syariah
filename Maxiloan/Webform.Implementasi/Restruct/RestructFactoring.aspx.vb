﻿#Region "Imports"
Imports System.Data.SqlClient
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.General
Imports Maxiloan.Webform.UserController
#End Region

Public Class RestructFactoring
    Inherits Maxiloan.Webform.WebBased
    'Protected WithEvents txtDrawDownDate As ucDateCE
    Protected WithEvents txtInvoiceDate As ucDateCE
    Protected WithEvents txtInvoiceDueDateNew As ucDateCE
    Protected WithEvents txtInvoiceDueDate As ucDateCE
#Region "Constanta"
    Private m_controller As New FactoringInvoiceController
    Dim objrow As DataRow
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Dim intLoop As Integer
    Dim Status As Boolean = True
#End Region

#Region "Property"
    Private Property ApplicationID() As String
        Get
            Return CType(ViewState("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property

    Private Property AgreementNo() As String
        Get
            Return CType(ViewState("AgreementNo"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("AgreementNo") = Value
        End Set
    End Property

    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property

    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("CmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CmdWhere") = Value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not Me.IsPostBack Then
			Me.FormID = "ResFact"
			InitialDefaultPanel()
			txtInvoiceDueDateNew.Text = Me.BusinessDate.ToString("dd/MM/yyyy")

			If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.SearchBy = ""
                Me.SortBy = ""
                txtGoPage.Text = "1"
                Me.Sort = "ApplicationID ASC"
                Me.CmdWhere = ""

                BindGridEntity(Me.CmdWhere)
            End If
        End If
    End Sub
    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        Dim lnkAgreementNo As HyperLink
        Dim lnkApplicationID As HyperLink
        Dim lnkCustName As HyperLink
        Dim lblCustomerID As Label
        Dim lblBranchID As Label

        If e.Item.ItemIndex >= 0 Then
            lnkAgreementNo = CType(e.Item.FindControl("lnkAgreementNo"), HyperLink)
            lnkApplicationID = CType(e.Item.FindControl("lnkApplicationID"), HyperLink)
            lnkCustName = CType(e.Item.FindControl("lnkCustName"), HyperLink)
            lblCustomerID = CType(e.Item.FindControl("lblCustomerID"), Label)
            lblBranchID = CType(e.Item.FindControl("lblBranchID"), Label)

            If lnkAgreementNo.Text.Trim.Length > 0 Then
                lnkAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccAcq" & "', '" & Server.UrlEncode(lnkAgreementNo.Text.Trim) & "')"
            End If
            lnkApplicationID = CType(e.Item.FindControl("lnkApplicationID"), HyperLink)
            If lnkApplicationID.Text.Trim.Length > 0 Then
                lnkApplicationID.NavigateUrl = "javascript:OpenApplicationId('" & "AccAcq" & "', '" & Server.UrlEncode(lnkApplicationID.Text.Trim) & "','" & Server.UrlEncode(lblBranchID.Text.Trim) & "')"
            End If
            If lnkApplicationID.Text.Trim.Length > 0 Then
                lnkCustName.NavigateUrl = "javascript:OpenCustomer('" & "AccAcq" & "', '" & Server.UrlEncode(lblCustomerID.Text.Trim) & "')"
            End If
			'lnkCustName.Attributes.Add("OnClick", "return OpenCust('" & lblCustomerID.Text & "','accacq');")


		End If

    End Sub

#Region "dtgPaging_ItemCommand"
    Private Sub dtgPaging_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        If e.CommandName = "Restruct" Then
            If CheckFeature(Me.Loginid, Me.FormID, "REST", Me.AppId) Then
                'Dim lblPanelInvoiceNo As Label
                Dim lnkApplicationID As New HyperLink
                'Dim lblPanelAgreementNo As New HyperLink
                Dim lblBranchID As New Label
                'Dim lblDrawDownDate As New Label
                'Dim lblInvoiceDate As New Label
                'Dim lblEffectiveRate As New Label
                'Dim lblFixInterest As New Label
                'Dim lblInvoiceDueDate As New Label
                Dim lnkAgreementNo As HyperLink
                Dim lblInvoiceNo As New Label

                pnlList.Visible = False
                pnlRestruct.Visible = True

                textEffectiveRateNew.Text = 0
                txtInvoiceDueDateNew.Text = Me.BusinessDate.ToString("dd/MM/yyyy")


                lnkApplicationID = CType(e.Item.FindControl("lnkApplicationID"), HyperLink)
                lnkAgreementNo = CType(e.Item.FindControl("lnkAgreementNo"), HyperLink)
                lblBranchID = CType(e.Item.FindControl("lblBranchID"), Label)
                lblInvoiceNo = CType(e.Item.FindControl("lblInvoiceNo"), Label)
                'lblDrawDownDate = CType(e.Item.FindControl("lblDrawDownDate"), Label)
                'lblInvoiceDate = CType(e.Item.FindControl("lblInvoiceDate"), Label)
                'lblEffectiveRate = CType(e.Item.FindControl("lblEffectiveRate"), Label)
                'lblFixInterest = CType(e.Item.FindControl("lblFixInterest"), Label)
                'lblInvoiceDueDate = CType(e.Item.FindControl("lblInvoiceDueDate"), Label)

                Dim oRestructFact As New Parameter.FactoringInvoice
                oRestructFact.BranchId = lblBranchID.Text.ToString.Trim
                oRestructFact.ApplicationID = lnkApplicationID.Text.ToString.Trim
                oRestructFact.AgreementNo = lnkAgreementNo.Text.ToString.Trim
                oRestructFact.InvoiceNo = lblInvoiceNo.Text.ToString.Trim
                oRestructFact.strConnection = GetConnectionString()

                oRestructFact = m_controller.GetRestructFactoringData(oRestructFact)

                lblPanelAgreementNo.Text = oRestructFact.ApplicationID.ToString
                lblDrawDownDate.Text = oRestructFact.DrawDownDate.ToString("dd/MM/yyyy")
                lblPanelInvoiceNo.Text = oRestructFact.InvoiceNo.ToString
                lblPanelEffRate.Text = oRestructFact.EffectiveRate.ToString("####0")
                lblAccruedInterest.Text = oRestructFact.AccruedInterest.ToString("####0")
                lblInvoiceDate.Text = oRestructFact.InvoiceDate.ToString("dd/MM/yyyy")
                lblPanelFixInterest.Text = oRestructFact.FixInterest.ToString("####0")
                lblInvoiceDueDate.Text = oRestructFact.InvoiceDueDate.ToString("dd/MM/yyyy")

                'lblPanelAgreementNo.Text = lnkAgreementNo.ToString.Trim
                'LblPanelInvoiceNo.Text = lnkAgreementNo.ToString.Trim
                'lblDrawDownDate.Text = lblDrawDownDate.Text.ToString("dd/MM/yyyy")
                'lblInvoiceDate.tex = lblInvoiceDate.Text
                'lblInvoiceDueDate = lblInvoiceDueDate.Text
                'lblPanelEffRate = lblEffectiveRate
                'lblPanelFixInterest = lblFixInterest
                'lblBungaBerjalan.Text = "0"

                textEffectiveRateNew.Text = "0"
                txtInvoiceDueDateNew.Text = Me.BusinessDate.ToString("dd/MM/yyyy")


                pnlRestruct.Visible = True
                lnkApplicationID.Text = lnkApplicationID.Text
                lnkApplicationID.NavigateUrl = "javascript:OpenApplicationId('" & "AccAcq" & "', '" & Server.UrlEncode(lnkApplicationID.Text.Trim) & "','" & Server.UrlEncode(lblBranchID.Text.Trim) & "')"
                Me.ApplicationID = lnkApplicationID.Text.Trim

                lblPanelAgreementNo.Text = lblPanelAgreementNo.Text
                lblPanelAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccAcq" & "', '" & Server.UrlEncode(lblPanelAgreementNo.Text.Trim) & "')"
                Me.AgreementNo = lblPanelAgreementNo.Text.Trim

            End If
        End If
    End Sub
#End Region

#Region "BindGridEntity"
    Sub BindGridEntity(ByVal cmdWhere As String)
        Dim dtEntity As DataTable = Nothing
        Dim oRestruct As New Parameter.FactoringInvoice

        With oRestruct
            .WhereCond = Me.CmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = Me.Sort
            .BranchId = Replace(Me.sesBranchId, "'", "")
            .strConnection = GetConnectionString()
        End With

        oRestruct = m_controller.RestruckFactoringPaging(oRestruct)

        If Not oRestruct Is Nothing Then
            dtEntity = oRestruct.Listdata
            recordCount = oRestruct.TotalRecord
        Else
            recordCount = 0
        End If

        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0

        dtgPaging.DataBind()
        PagingFooter()
    End Sub
#End Region

#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblTotRec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGoPage.Click
        If IsNumeric(txtGoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtGoPage.Text, Int32)
                BindGridEntity(Me.CmdWhere)
            End If
        End If
    End Sub
#End Region
#Region "InitialDefaultPanel"
    Private Sub InitialDefaultPanel()
        pnlList.Visible = True
        pnlRestruct.Visible = False
        lblMessage.Text = ""
        lblMessage.Visible = False

        txtInvoiceDueDateNew.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
        txtInvoiceDueDateNew.IsRequired = True
    End Sub
#End Region

#Region "imbCancel"
    Private Sub imbCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        pnlRestruct.Visible = False
        pnlList.Visible = True
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim oParameter As New Parameter.FactoringInvoice
        Dim errMessage As String = ""

        With oparameter
            .AgreementNo = lblPanelAgreementNo.Text
            .InvoiceNo = lblPanelInvoiceNo.Text
            .EffectiveRateNew = textEffectiveRateNew.Text
            .InvoiceDueDateNew = txtInvoiceDueDateNew.Text
            .strConnection = GetConnectionString()
        End With
        oParameter = m_controller.RestructFactoringSave(oParameter)
    End Sub
#End Region

End Class