﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="RestructFactAndMUExec.aspx.vb" Inherits="Maxiloan.Webform.Implementasi.RestructFactAndMUExec" %>
 

<%@ Register TagPrefix="uc1" TagName="ucApprovalRequest" Src="../../Webform.UserController/ucApprovalRequest.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ValidDate" Src="../../Webform.UserController/ValidDate.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcPaymentInfo" Src="../../Webform.UserController/UcPaymentInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcFullPrepayInfo" Src="../../Webform.UserController/UcFullPrepayInfo.ascx" %>
<%@ Register Src="../../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat"TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>RestructFactAndMUExec</title>
    <link rel="Stylesheet" type="text/css" href="../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../Include/Buttons.css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';
    </script>
</head>
    <body>
        <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h4>
                PENGAJUAN RESCHEDULING FACT AND MDKJ 
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                No Kontrak
            </label>
            <asp:HyperLink ID="lblAgreementNo" runat="server"></asp:HyperLink>
        </div>
        <div class="form_right">
            <label>
                Nama Customer
            </label>
            <asp:HyperLink ID="lblCustName" runat="server"></asp:HyperLink>
        </div>
    </div>
    <div class="form_title">
        <div class="form_single">
            <h4>
                DATA SAAT INI
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Jenis Margin
            </label>
            <asp:Label ID="lblInterestType" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Produk
            </label>
            <asp:Label ID="lblProduct" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Skema Angsuran
            </label>
            <asp:Label ID="lblInstallScheme" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Package
            </label>
            <asp:Label ID="lblPackage" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Jenis Finance
            </label>
            <asp:Label ID="lblFinanceType" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Penjamin
            </label>
            <asp:Label ID="lblGuarantor" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                suku Margin Effective (%)
            </label>
            <asp:Label ID="lblEffRate" runat="server"></asp:Label>
        </div>
        <div class="form_right">
          <label>
                lama Angsuran
            </label>
            <asp:Label ID="lblInstallmentNo" runat="server"></asp:Label> 
        </div>
    </div> 
    <div class="form_box">
        <div class="form_left">
            <label>
                Pola Pembayaran
            </label>
            <asp:Label ID="lblPaymentFreq" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                No Rescheduling
            </label>
            <asp:Label ID="lblReschedNo" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_title">
        <div class="form_single">
            <h4>
                A/R PER TANGGAL
                <asp:Label ID="lbljudul" runat="server"></asp:Label>
            </h4>
        </div>
    </div>
    <uc1:ucfullprepayinfo id="oPaymentInfo" runat="server">
                </uc1:ucfullprepayinfo>
    <div class="form_box">
        <div class="form_single">
            <b>Total Sisa A/R
                <asp:Label ID="lblStopAccruedAmount" runat="server"></asp:Label></b>
        </div>
    </div>
    <div class="form_title">
        <div class="form_left">
            <h4>
                JUMLAH BAYAR DIMUKA
            </h4>
        </div>
        <div class="form_right">
            <h4>
                DISKON
            </h4>
        </div>
    </div>
              <div class="form_box">
        <div class="form_left">
            <label class="label_req">
                Outstanding Pokok
            </label>
            <asp:Label ID="lblOutstandingPrincipal" runat="server" CssClass="numberAlign label"></asp:Label>
        </div>
        <div class="form_right">
            <label class="label_req">
                Outstanding Pokok
            </label> 
            <asp:Label ID="lblDiscOutstandingPrincipal" runat="server" CssClass="numberAlign label"></asp:Label>
        </div>
    </div>

     <div class="form_box">
        <div class="form_left">
            <label class="label_req">
                Outstanding Margin
            </label>
            <asp:Label ID="lblOutstandingInterest" runat="server" CssClass="numberAlign label"></asp:Label>
        </div>
        <div class="form_right">
            <label class="label_req">
                Outstanding Margin
            </label> 
            <asp:Label ID="lblDiscOutstandingInterest" runat="server" CssClass="numberAlign label"></asp:Label>
        </div>
    </div>

    <div class="form_box">
        <div class="form_left">
            <label class="label_req">
                Outstanding Denda
            </label>
            <asp:Label ID="lblOutstandingLC" runat="server" CssClass="numberAlign label"></asp:Label>
        </div>
        <div class="form_right">
            <label class="label_req">
                Outstanding Denda
            </label> 
             <asp:Label ID="lblDiscOutstandingLC" runat="server" CssClass="numberAlign label"></asp:Label>
        </div>
    </div>
      <div class="form_box">
        <div class="form_left">
            <label class="label_req">
                Margin Berjalan
            </label>
            <asp:Label ID="lblAccruedInterest" runat="server" CssClass="numberAlign label"></asp:Label>
        </div>
        <div class="form_right">
            <label class="label_req">
                Margin Berjalan
            </label> 
             <asp:Label ID="lblDiscAccruedInterest" runat="server" CssClass="numberAlign label"></asp:Label>
        </div>
    </div>
        <div class="form_box">
        <div class="form_left">
            <label class="label_req">
                Margin Jatuh Tempo
            </label>
            <asp:Label ID="lblInstallmentDue" runat="server" CssClass="numberAlign label"></asp:Label>
        </div>
        <div class="form_right">
            <label class="label_req">
                Margin Jatuh Tempo
            </label> 
             <asp:Label ID="lblDiscInstallmentDue" runat="server" CssClass="numberAlign label"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left"> 
             <label>
                Surat Perjanjian
            </label>
            <asp:Label ID="lblPerjanjianNo" runat="server" CssClass="numberAlign label"></asp:Label> 
        </div>
        <div class="form_right">
            <label class="label_req">
                PPh (Perjanjian Penyelesaian Hutang)
            </label>
             <asp:Label ID="lblPPh" runat="server" CssClass="numberAlign label"></asp:Label> 
        </div>
    </div>

     <div class="form_box">
        <div class="form_left"> 
        </div>
        <div class="form_right">
            <label>
                Total Discount
            </label>
            <asp:Label ID="lblTotDiscAmt" runat="server" CssClass="numberAlign label"></asp:Label>
        </div>
    </div>

      <div class="form_box">
        <div class="form_left">
            <label class="label_req">
                Pelunasan Sebagian
            </label>
            <asp:Label ID="lblPartialPrepayment" runat="server"></asp:Label>
        </div>
        <div class="form_right"> 
        </div>
    </div>
      <div class="form_box">
        <div class="form_left">
            <label class="label_req">
                Biaya Administrasi
            </label>
             <asp:Label ID="lblAdmFee" runat="server"></asp:Label>
            <label>  Masukan ke Pokok Hutang</label><asp:CheckBox runat="server" ID="chkAdminFee" Enabled="False" /> 
        </div>
        <div class="form_right">
            <label class="label_req"></label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Total Dibayar Dimuka
            </label>
            <asp:Label ID="lblTotAdvAmt" runat="server"></asp:Label>
        </div>
        <div class="form_right"> 
        </div>
    </div>

     <div class="form_box">
        <div class="form_left">
            <label>
                Total Pokok Hutang
            </label>
            <asp:Label ID="lblTotAdvAmount" runat="server"></asp:Label>
        </div>
        <div class="form_right"> 
        </div>
    </div>
 <div class="form_box_hide"> 
    <div class="form_box">
        <div class="form_left"> 
        </div>
        <div class="form_right">
            <label>
                Denda Keterlambatan Angsuran
            </label>
            <asp:Label ID="lblInstallLC" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left"> 
        </div>
        <div class="form_right">
            <label>
                Biaya Tagih Angsuran
            </label>
            <asp:Label ID="lblInstallCF" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
        </div>
        <div class="form_right">
            <label>
                Denda Keterlambatan Asuransi
            </label>
            <asp:Label ID="lblInsuranceLC" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
        </div>
        <div class="form_right">
            <label>
                Biaya Tagih Asuransi
            </label>
            <asp:Label ID="lblInsuranceCF" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
        </div>
        <div class="form_right">
            <label>
                Biaya Tolakan PDC
            </label>
            <asp:Label ID="lblPDCBounceFee" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
        </div>
        <div class="form_right">
            <label>
                Biaya Perpanjangan STNK/BBN
            </label>
            <asp:Label ID="lblSTNKRenewal" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
        </div>
        <div class="form_right">
            <label>
                Biaya Klaim Asuransi
            </label>
            <asp:Label ID="InsuranceCF" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
        </div>
        <div class="form_right">
            <label>
                Biaya Tarik
            </label>
            <asp:Label ID="lblRepossesionFee" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left"> 
        </div>
        <div class="form_right"> 
        </div>
    </div>
     </div> 
    <div class="form_box">
    </div>
    <div class="form_title">
        <div class="form_single">
            <h4>
                STRUKTUR FINANCIAL BARU
            </h4>
        </div>
    </div>
    <div class="form_box_hide">
        <div class="form_left"> 
        </div>
        <div class="form_right">
            <label>
                <asp:Label ID="lblNumberOfStep" runat="server">Number of Step</asp:Label>
            </label>
            <asp:Label ID="lblStep" runat="server" Columns="4"></asp:Label>

        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Jumlah Pokok Baru
            </label>
            <asp:Label ID="lblNPA" runat="server"></asp:Label>
        </div>
        <div class="form_right">  
            <label>
                Jangka Waktu
            </label>
            <asp:Label ID="lblTenor" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box_hide">
        <div class="form_left">
            <label>
                Suku Margin Effective (%)
            </label>
         <%--   <asp:Label ID="lblEffectiveRate" runat="server"></asp:Label>--%>
        </div>
        <div class="form_right">
            <label>
                Suku Margin Flat
            </label> 
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Suku Margin Effective (%)
            </label>   
             <asp:Label ID="lblEffectiveRate" runat="server"></asp:Label>
        </div> 
        <div class="form_right">
            <label>
                Suku Margin Flat
            </label>
             <asp:Label ID="lblFlatRate" runat="server"></asp:Label>
        </div>
    </div>

    <div class="form_box">
        <div class="form_left">
            <label>
                Pola Pembayaran
            </label>
           <asp:Label ID="lblPaymentFrequency" runat="server"></asp:Label>
        </div>
        <div class="form_right"> 
        </div>
    </div>

    <div class="form_box_hide">
        <div class="form_left">
            <label>
                Pola Pembayaran
            </label>
           <%-- <asp:Label ID="lblPaymentFrequency" runat="server"></asp:Label>--%>
        </div>
        <div class="form_right">
            <label>
                <asp:Label ID="lblCummulative" runat="server">Devide Mulai Angsuran Ke</asp:Label>
            </label>
            <asp:Label ID="lbCumm" runat="server" Columns="4"></asp:Label>
        </div>
    </div>
    <div class="form_box_hide">
        <div class="form_left">
            <label>
                Lama Angsuran
            </label>
            <asp:Label ID="lblInstallmentNum" runat="server"></asp:Label>
        </div>
        <div class="form_right"> 
        </div>
    </div>
    <div class="form_box_hide">
        <div class="form_left">
            <label>
                Jumlah Angsuran
            </label>
            <asp:Label ID="lblInstallmentAmount" runat="server"></asp:Label>
        </div>
        <div class="form_right">
        </div>
    </div>
    <asp:Panel ID="pnlViewST" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    VIEW ANGSURAN
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgViewInstallment" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:BoundColumn DataField="InsSeqNo" HeaderText="No"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="Installment">
                                <ItemTemplate>
                                    <asp:Label ID="lblInstallment" runat="server" Text='<%# formatnumber(DataBinder.Eval(Container, "DataItem.InstallmentAmount"),2) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>                            
                            <asp:Label ID="lblTotInstallmentAmount" runat="server"></asp:Label>
                        </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="POKOK">
                                <ItemTemplate>
                                    <asp:Label ID="lblPrincipalAmount" runat="server" Text='<%# formatnumber(DataBinder.Eval(Container, "DataItem.PrincipalAmount"),2) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                 <FooterTemplate>
                                      <asp:Label ID="lblTotPrincipalAmount" runat="server"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="MARGIN">
                                <ItemTemplate>
                                    <asp:Label ID="lblInterestAmount" runat="server" Text='<%# formatnumber(DataBinder.Eval(Container, "DataItem.InterestAmount"),2) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                        <asp:Label ID="lblTotINTERESTAMOUNT" runat="server"></asp:Label>
                                    </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="SISA POKOK">
                                <ItemTemplate>
                                    <asp:Label ID="lblOsPrincipal" runat="server" Text='<%# formatnumber(DataBinder.Eval(Container, "DataItem.OutStandingPrincipal"),2) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                  <FooterTemplate>
                                        <asp:Label ID="lblTotOSPrincipal" runat="server"></asp:Label>
                                    </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="SISA MARGIN">
                                <ItemTemplate>
                                    <asp:Label ID="LblOSInterest" runat="server" Text='<%# formatnumber(DataBinder.Eval(Container, "DataItem.OutStandingInterest"),2) %>'>
                                    </asp:Label> 
                                   <%-- <asp:Label ID="lblSeq" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Seq") %>'
                                        Visible="false">
                                    </asp:Label>--%>
                                </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblTotOSInterest" runat="server"></asp:Label>
                                    </FooterTemplate> 
                            </asp:TemplateColumn>
                       <%--     <asp:BoundColumn DataField="DueDate" HeaderText="TGL JT" Visible="false"></asp:BoundColumn>--%>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
    </asp:Panel>
    <div class="form_box_hide">
    <div class="form_title">
        <div class="form_single">
            <h4>
                CROSS DEFAULT
            </h4>
        </div>
    </div>
    <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgCrossDefault" runat="server" CssClass="grid_general" AutoGenerateColumns="False">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="NO">
                            <ItemTemplate>
                                <asp:Label ID="lblCDNo" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="Name" HeaderText="ANO KONTRAKT">
                            <ItemTemplate>
                                <asp:Label ID="lblCDAgreementNo" runat="server" Text='<%#Container.DataItem("Agreement")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="NAMA CUSTOMER">
                            <ItemTemplate>
                                <asp:Label ID="lblCDName" Text='<%#Container.DataItem("Name")%>' runat="server">
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="TGL KONTRAK">
                            <ItemTemplate>
                                <asp:Label ID="lblCDAgreementDate" runat="server" Text='<%#Container.DataItem("AgreementDate")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="STATUS DEFAULT">
                            <ItemTemplate>
                                <asp:Label ID="lblCDDefaultStatus" Text='<%#Container.DataItem("DefaultStatus")%>'
                                    runat="server">
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="STATUS KONTRAK">
                            <ItemTemplate>
                                <asp:Label ID="lblCDContractStatus" Text='<%#Container.DataItem("ContractStatus")%>'
                                    runat="server">
                                </asp:Label>
                                <asp:Label ID="lblCDApplicationID" Text='<%#Container.DataItem("CrossDefaultApplicationId")%>'
                                    runat="server" Visible="false">
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="DELETE">
                            <ItemTemplate>
                                <asp:ImageButton ID="imbCDDelete" CommandName="CDDelete" runat="server" ImageUrl="../../images/icondelete.gif"
                                    CausesValidation="False"></asp:ImageButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
        </div>
    </div>
    <div class="form_title">
        <div class="form_single">
            <h4>
                SYARAT DAN KONDISI
            </h4>
        </div>
    </div>
    <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgTC" runat="server" PageSize="3" DataKeyField="TCName" CssClass="grid_general"
                    AutoGenerateColumns="False">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="NO">
                            <ItemTemplate>
                                <asp:Label ID="lblTCNo" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="TCName" HeaderText="DOKUMEN"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="PERIKSA">
                            <ItemTemplate>
                                <asp:CheckBox disabled ID="chkTCChecked" Checked='<%# DataBinder.eval(Container, "DataItem.Checked") %>'
                                    runat="server"></asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="IsMandatory" HeaderText="MANDATORY"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="CATATAN">
                            <ItemTemplate>
                                <asp:Label ID="lblTCNotes" Text='<%# DataBinder.eval(Container, "DataItem.Notes") %>'
                                    runat="server">
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn Visible="False" DataField="MasterTCID" HeaderText="MasterTCID">
                        </asp:BoundColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
        </div>
    </div>
    <div class="form_title">
        <div class="form_single">
            <h4>
                SYARAT DAN KONDISI CHECK LIST
            </h4>
        </div>
    </div>
    <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgTC2" runat="server" PageSize="3" DataKeyField="TCName" CssClass="grid_general"
                    AutoGenerateColumns="False">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="NO">
                            <ItemTemplate>
                                <asp:Label ID="lblTCNo2" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="TCName" HeaderText="DOKUMEN"></asp:BoundColumn>
                        <asp:BoundColumn DataField="CheckList" HeaderText="CHECKLIST"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="PERIKSA">
                            <ItemTemplate>
                                <asp:CheckBox disabled ID="chkTCCheck2" Checked='<%# DataBinder.eval(Container, "DataItem.Checked") %>'
                                    runat="server"></asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="ISMandatory" HeaderText="MANDATORY"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="CATATAN">
                            <ItemTemplate>
                                <asp:Label ID="lblTCNotes2" Text='<%# DataBinder.eval(Container, "DataItem.Notes") %>'
                                    runat="server">
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn Visible="False" DataField="MasterTCID" HeaderText="MasterTCID">
                        </asp:BoundColumn>
                        <asp:BoundColumn Visible="False" DataField="MSTCCLSequenceNo" HeaderText="MSTCCLSequenceNo">
                        </asp:BoundColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
        </div>
    </div>
    </div>
    <asp:Panel ID="pnlApproval" runat="server">
        <div class="form_box_title">
            <div class="form_single">
                <h4>
                    DETAIL RESCHEDULING
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Alasan
                </label>
                <asp:Label ID="lblReason" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Disetujui Oleh
                </label>
                <asp:Label ID="lblToBe" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Catatan
                </label>
                <asp:Label ID="lblUcNotes" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
            </div>
            <div class="form_right">
                <label>
                    Total yg Harus dibayar
                </label>
                <asp:Label ID="lblTotalAmountToBePaid" runat="server" CssClass="numberAlign label"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
            </div>
            <div class="form_right">
                <label>
                    Jumlah Prepaid
                </label>
                <asp:Label ID="lblPrepaidAmount" runat="server" CssClass="numberAlign label"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
            </div>
            <div class="form_right">
                <label>
                    Selisih
                </label>
                <asp:Label ID="lblBalanceAmount" runat="server" CssClass="numberAlign label"></asp:Label>
            </div>
        </div>
    </asp:Panel>
    <div class="form_button">  
        <asp:Button ID="imbExecute" runat="server" Text="Execute" CssClass="small button blue"
            CausesValidation="True"></asp:Button>
        <asp:Button ID="imbSave" runat="server" Text="Save" CssClass="small button blue"
            CausesValidation="True"></asp:Button>
        <asp:Button ID="imbCancel" runat="server" Text="Cancel" CssClass="small button gray"
            CausesValidation="False"></asp:Button> 
           <asp:Button ID="imbBack" runat="server" Text="Back" CssClass="small button gray"
            CausesValidation="False"></asp:Button>
    </div>
    </form>
    </body> 
</html>