﻿#Region "Imports"
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController

#End Region


Public Class RestructFactAndMU
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Protected WithEvents oSearchBy As UcSearchBy
    Private cContract As New GeneralPagingController
    Private oContract As New Parameter.GeneralPaging
#End Region

#Region "Page Load"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not Me.IsPostBack Then
            Me.FormID = "RestructFactAndMU"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If Request.QueryString("Message") = MessageHelper.MESSAGE_UPDATE_SUCCESS Then _
                ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
                oSearchBy.ListData = "Agreementno, No Kontrak-Name, Nama Konsumen"
                oSearchBy.BindData()
                Me.SearchBy = ""
                Me.SortBy = ""
            End If
        End If
    End Sub

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        With oContract
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
            .SpName = "spRestructFactAndMDKJList"
        End With
        oContract = cContract.GetGeneralPaging(oContract)
        recordCount = oContract.TotalRecords
        DtgAgree.DataSource = oContract.ListData

        Try
            DtgAgree.DataBind()
        Catch
            DtgAgree.CurrentPageIndex = 0
            DtgAgree.DataBind()
        End Try
        PagingFooter()
        pnlList.Visible = True
        pnlDatagrid.Visible = True
        lblMessage.Visible = False
    End Sub
#End Region

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            ShowMessage(lblMessage, "Data Tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imgbtnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub

    Private Sub DtgAgree_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgree.ItemDataBound
        Dim hypReceive As HyperLink
        Dim lblApplicationid As HyperLink
        Dim lblCustID As Label
        Dim lblName As HyperLink
        Dim lblAgreementNo As HyperLink
        Dim lblInvoiceNo As Label
        Dim lblInvoiceSeqNo As Label
        If e.Item.ItemIndex >= 0 Then
            lblApplicationid = CType(e.Item.FindControl("lblApplicationid"), HyperLink)
            hypReceive = CType(e.Item.FindControl("HypReceive"), HyperLink)
            lblInvoiceNo = CType(e.Item.FindControl("lblInvoiceNo"), Label)
            lblInvoiceSeqNo = CType(e.Item.FindControl("lblInvoiceSeqNo"), Label)
            hypReceive.NavigateUrl = "RestructFactAndMUStep1.aspx?applicationid=" & lblApplicationid.Text.Trim & "&branchid=" & Me.sesBranchId.Replace("'", "").Trim & "&InvoiceNo=" & lblInvoiceNo.Text.Trim & "&InvoiceSeqNo=" & lblInvoiceSeqNo.Text.Trim

            lblAgreementNo = CType(e.Item.FindControl("lblAgreementNo"), HyperLink)
            If lblAgreementNo.Text.Trim.Length > 0 Then
                lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(lblApplicationid.Text.Trim) & "')"
            End If

            If lblApplicationid.Text.Trim.Length > 0 Then
                lblApplicationid.NavigateUrl = "javascript:OpenApplicationId('" & "AccMnt" & "', '" & Server.UrlEncode(lblApplicationid.Text.Trim) & "')"
            End If
            lblName = CType(e.Item.FindControl("lblName"), HyperLink)
            lblCustID = CType(e.Item.FindControl("lblCustID"), Label)
            If lblCustID.Text.Trim.Length > 0 Then
                lblName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(lblCustID.Text.Trim) & "')"
            End If
        End If
    End Sub
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles DtgAgree.SortCommand
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

#End Region

    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbReset.Click
        Me.SearchBy = ""
        oSearchBy.Text = ""
        oSearchBy.BindData()
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imgsearch.Click
        Me.SearchBy = " branchid = '" & Me.sesBranchId.Replace("'", "").Trim & "' "

        If oSearchBy.Text.Trim <> "" Then
            Me.SearchBy = Me.SearchBy & " and " & oSearchBy.ValueID & " like '%" & oSearchBy.Text.Trim.Replace("'", "''") & "%'"
        End If
        pnlDatagrid.Visible = True
        pnlList.Visible = True
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub




    'Private Sub DoProses()
    '    Dim SequenceNo As Integer = 0
    '    Dim list As New List(Of Parameter.UploadAngsuran)
    '    If ValidasiFile() Then
    '        list = UploadFile()
    '    Else
    '        ShowMessage(lblMessage, "File pernah di upload!", True)
    '        Exit Sub
    '    End If
    '    Try
    '        TotalAgreement = list.Count
    '        SaveLog(SequenceNo)
    '        If list.Count > 0 Then
    '            For index = 0 To list.Count - 1
    '                Dim payment As New Parameter.UploadAngsuran
    '                payment = list(index)
    '                payment.UploadDate = BusinessDate
    '                payment.SequenceNo = SequenceNo
    '                SaveUploadAngsuran(payment)
    '            Next
    '        End If
    '        UpdateLog(SequenceNo)
    '        ShowMessage(lblMessage, "Upload Data Berhasil", False)
    '    Catch ex As Exception
    '        ShowMessage(lblMessage, ex.Message, True)
    '    End Try
    'End Sub

    'Function ValidasiFile() As Boolean
    '    Dim result As Boolean = True
    '    objCon = New SqlConnection(GetConnectionString)
    '    Try
    '        myDS = New DataSet
    '        myCmd.Parameters.Clear()
    '        myCmd.CommandText = "spGetUploadRestructFactandMUValidFile"
    '        myCmd.CommandType = CommandType.StoredProcedure
    '        myCmd.Parameters.Add(New SqlParameter("@FileName", SqlDbType.VarChar, 200) With {.Value = strFileTemp.Trim})
    '        myCmd.Connection = objCon
    '        myAdapter.SelectCommand = myCmd
    '        myAdapter.Fill(myDS)

    '        If myDS.Tables(0).Rows.Count > 0 Then
    '            result = False
    '        End If
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message)
    '        ShowMessage(lblMessage, ex.Message, True)
    '    End Try
    '    Return result
    'End Function
    'Function UploadFile() As List(Of Parameter.UploadAngsuran)
    '    Dim list As New List(Of Parameter.UploadAngsuran)
    '    Dim data As New DataTable
    '    Dim NoRekening As String = ""
    '    Dim TglTransaksi As Date = Nothing

    '    Try
    '        'data = CSVToDataTable(Me.FileName)--> kalau pakai csv
    '        data = FileToDataTable()

    '        For index = 0 To data.Rows.Count - 1
    '            If (index >= 0) Then
    '                Dim Payment As New Parameter.UploadAngsuran

    '                Payment.TglTransaksi = ConvertDate2(IIf(data.Rows(index).Item(1).ToString().Trim = "", "01/01/1900", data.Rows(index).Item(1).ToString().Trim))
    '                Payment.WaktuTransaksi = Now.Hour.ToString("00") & ":" & Now.Minute.ToString("00") & ":" & Now.Second.ToString("00")
    '                Payment.NoKontrak = data.Rows(index).Item(2).ToString()
    '                Payment.NamaNasabah = data.Rows(index).Item(3).ToString()
    '                Payment.NilaiTransaksi = data.Rows(index).Item(4).ToString()
    '                Payment.NoJournal = data.Rows(index).Item(5).ToString()
    '                Payment.Keterangan = data.Rows(index).Item(6).ToString()
    '                Payment.Post = "N"

    '                Me._TglTransaksi = Payment.TglTransaksi
    '                Me._NoKontrak = Payment.NoKontrak

    '                Dim query As New Parameter.UploadAngsuran
    '                If list.Count > 0 Then
    '                    query = list.Find(AddressOf PredicateFunction)
    '                Else
    '                    query = Nothing
    '                End If

    '                If query Is Nothing Then
    '                    TotalAmountReceive = TotalAmountReceive + Payment.NilaiTransaksi
    '                    list.Add(Payment)
    '                End If
    '            End If

    '        Next
    '    Catch ex As Exception
    '        Throw New ApplicationException(ex.Message)
    '    End Try

    '    Return list
    'End Function

    'Public Function PredicateFunction(ByVal custom As Parameter.UploadAngsuran) As Boolean
    '    Return custom.TglTransaksi = Me._TglTransaksi And custom.NoKontrak = Me._NoKontrak
    'End Function

    'Private Function FileToDataTable() As DataTable
    '    Dim dt As New DataTable()

    '    Using doc As SpreadsheetDocument = SpreadsheetDocument.Open(FileName, False)
    '        Dim sheet As Sheet = doc.WorkbookPart.Workbook.Sheets.GetFirstChild(Of Sheet)()

    '        Dim worksheet As Worksheet = TryCast(doc.WorkbookPart.GetPartById(sheet.Id.Value), WorksheetPart).Worksheet

    '        Dim rows As IEnumerable(Of Row) = worksheet.GetFirstChild(Of SheetData)().Descendants(Of Row)()

    '        For Each row As Row In rows
    '            If row.RowIndex.Value = 1 Then
    '                Dim clm As Integer = 0
    '                For Each cell As Cell In row.Descendants(Of Cell)()
    '                    If GetValue(doc, cell).Trim <> "" Then
    '                        dt.Columns.Add(GetValue(doc, cell))
    '                    Else
    '                        dt.Columns.Add(GetValue(doc, cell).Trim & clm.ToString)
    '                    End If
    '                    clm += 1
    '                Next
    '            Else
    '                dt.Rows.Add()
    '                Dim i As Integer = 0
    '                For Each cell As Cell In row.Descendants(Of Cell)()
    '                    dt.Rows(dt.Rows.Count - 1)(i) = GetValue(doc, cell)
    '                    i += 1
    '                Next
    '            End If
    '        Next
    '    End Using
    '    Return dt
    'End Function
    'Private Function GetValue(doc As SpreadsheetDocument, cell As Cell) As String
    '    Dim value As String = cell.CellValue.InnerText
    '    If cell.DataType IsNot Nothing AndAlso cell.DataType.Value = CellValues.SharedString Then
    '        Return doc.WorkbookPart.SharedStringTablePart.SharedStringTable.ChildElements.GetItem(Integer.Parse(value)).InnerText
    '    End If
    '    Return value
    'End Function

    'Sub SaveLog(ByRef SequenceNo As Integer)
    '    Try
    '        objCon = New SqlConnection(GetConnectionString)
    '        myCmd.Parameters.Clear()
    '        myCmd.CommandText = "spUploadRestructFactAndMULogSave"
    '        myCmd.CommandType = CommandType.StoredProcedure
    '        myCmd.Parameters.Add(New SqlParameter("@UploadDate", SqlDbType.DateTime) With {.Value = BusinessDate})
    '        myCmd.Parameters.Add(New SqlParameter("@ValutaDate", SqlDbType.DateTime) With {.Value = BusinessDate})
    '        myCmd.Parameters.Add(New SqlParameter("@FileName", SqlDbType.VarChar, 200) With {.Value = strFileTemp})
    '        myCmd.Parameters.Add(New SqlParameter("@FileDirectory", SqlDbType.VarChar, 8000) With {.Value = FileDirectory})
    '        myCmd.Parameters.Add(New SqlParameter("@TotalAgreement", SqlDbType.BigInt) With {.Value = TotalAgreement})
    '        myCmd.Parameters.Add(New SqlParameter("@TotalAmountReceive", SqlDbType.Decimal) With {.Value = TotalAmountReceive})
    '        Dim prmErr = New SqlParameter("@SequenceNo", SqlDbType.SmallInt) With {.Direction = ParameterDirection.Output}
    '        myCmd.Parameters.Add(prmErr)
    '        objCon.Open()
    '        myCmd.Connection = objCon
    '        myCmd.CommandTimeout = 0
    '        myCmd.ExecuteNonQuery()
    '        objCon.Close()
    '        SequenceNo = CDbl(prmErr.Value)
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message)
    '        ShowMessage(lblMessage, ex.Message, True)
    '    End Try
    'End Sub
    'Sub SaveUploadAngsuran(ByVal payment As Parameter.UploadAngsuran)
    '    Try
    '        objCon = New SqlConnection(GetConnectionString)
    '        myCmd.Parameters.Clear()
    '        myCmd.CommandText = " " 'spUploadPaymentBatchSave
    '        myCmd.CommandType = CommandType.StoredProcedure
    '        myCmd.Parameters.Add(New SqlParameter("@UploadDate", SqlDbType.DateTime) With {.Value = payment.UploadDate})
    '        myCmd.Parameters.Add(New SqlParameter("@ValutaDate", SqlDbType.Date) With {.Value = payment.TglTransaksi})
    '        myCmd.Parameters.Add(New SqlParameter("@WaktuTransaksi", SqlDbType.Time) With {.Value = payment.WaktuTransaksi})
    '        myCmd.Parameters.Add(New SqlParameter("@SequenceNo", SqlDbType.SmallInt) With {.Value = payment.SequenceNo})
    '        myCmd.Parameters.Add(New SqlParameter("@AgreementNo", SqlDbType.Char, 20) With {.Value = payment.NoKontrak})
    '        myCmd.Parameters.Add(New SqlParameter("@CustomerName", SqlDbType.VarChar, 50) With {.Value = payment.NamaNasabah})
    '        myCmd.Parameters.Add(New SqlParameter("@AmountReceive", SqlDbType.Decimal) With {.Value = payment.NilaiTransaksi})
    '        myCmd.Parameters.Add(New SqlParameter("@JournalNo", SqlDbType.Char, 20) With {.Value = payment.NoJournal})
    '        myCmd.Parameters.Add(New SqlParameter("@StatusLog", SqlDbType.Char, 1) With {.Value = payment.Post})
    '        myCmd.Parameters.Add(New SqlParameter("@Keterangan", SqlDbType.VarChar, 100) With {.Value = payment.Keterangan})

    '        objCon.Open()
    '        myCmd.Connection = objCon
    '        myCmd.ExecuteNonQuery()
    '        objCon.Close()
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message)
    '        ShowMessage(lblMessage, ex.Message, True)
    '    End Try
    'End Sub
    'Sub UpdateLog(ByRef SequenceNo As Integer)
    '    Try
    '        objCon = New SqlConnection(GetConnectionString)
    '        myCmd.Parameters.Clear()
    '        myCmd.CommandText = " " 'spUploadPaymentBybatchLogUpdate
    '        myCmd.CommandType = CommandType.StoredProcedure
    '        myCmd.Parameters.Add(New SqlParameter("@UploadDate", SqlDbType.DateTime) With {.Value = BusinessDate})
    '        myCmd.Parameters.Add(New SqlParameter("@SequenceNo", SqlDbType.SmallInt) With {.Value = SequenceNo})

    '        objCon.Open()
    '        myCmd.Connection = objCon
    '        myCmd.CommandTimeout = 0
    '        myCmd.ExecuteNonQuery()
    '        objCon.Close()
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message)
    '        ShowMessage(lblMessage, ex.Message, True)
    '    End Try
    'End Sub

End Class