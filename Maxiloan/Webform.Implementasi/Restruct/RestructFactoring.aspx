﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="RestructFactoring.aspx.vb" Inherits="Maxiloan.Webform.Implementasi.RestructFactoring" %>

<%@ Register TagPrefix="uc1" Src="../../webform.UserController/ucDateCE.ascx" TagName="ucDateCE"  %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Restruct Factoring</title>
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="JavaScript" type="text/javascript">
        function OpenCust(CustomerID, pStyle) {
            alert(pStyle);
            var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            var x = screen.width;
            var y = screen.height - 100;
            window.open('http://<%# Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.LoanOrg/Credit/ViewPersonalCustomer.aspx?Style=' + pStyle + '&CustomerID=' + CustomerID, 'UserLookup', 'left = 0, top = 0, width = ' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
            
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3> RESTRUCT FACTORING</h3>
        </div>
    </div>
     <asp:Panel ID="pnlList" runat="server">
          <div class="form_title">
            <div class="form_single">
                <h4> DAFTAR FACTORING  </h4>
            </div>
        </div>
        <div class="form_box_header">
             <div class="form_single">
                  <asp:DataGrid ID="dtgPaging" runat="server" DataKeyField="ApplicationID"
                        CellSpacing="1" CellPadding="3" BorderWidth="0px" AutoGenerateColumns="False"
                        AllowSorting="True" CssClass="grid_general">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <Columns>
                         <asp:TemplateColumn HeaderText="PILIH">
                            <ItemStyle HorizontalAlign="Center" Width="5%"></ItemStyle>
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkAction" runat="server" CommandName="Restruct" Text='RESTRUCT'
                                    CausesValidation="false"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="ApplicationID" HeaderText="DATA ID" HeaderStyle-Width="150px">
                                    <HeaderStyle></HeaderStyle>
                                    <ItemTemplate>  
                                        <asp:HyperLink runat="server" ID="lnkApplicationID" CausesValidation="False" CommandName="View"
                                            Text='<%# DataBinder.Eval(Container, "DataItem.ApplicationID")%>' >
                                        </asp:HyperLink> 
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK"  HeaderStyle-Width="150px">
                                    <HeaderStyle></HeaderStyle>
                                    <ItemTemplate>
                                        <asp:HyperLink runat="server" ID="lnkAgreementNo" CausesValidation="False" CommandName="View"
                                            Text='<%# DataBinder.eval(Container,"DataItem.AgreementNo")%>' >
                                        </asp:HyperLink> 
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="Name" HeaderText="NAMA CUSTOMER" HeaderStyle-Width="250px">
                                <ItemStyle CssClass="name_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Image ID="imgBadType" runat="server"></asp:Image>
                                    <asp:HyperLink ID="lnkCustName" runat="server" CausesValidation="False" CommandName="View"
                                         Text='<%# DataBinder.Eval(Container, "DataItem.Name") %>'>
                                    </asp:HyperLink>
                                     <asp:Label ID="lblCustomerID" runat="server" Visible="false" Text='<%# DataBinder.eval(Container,"DataItem.CustomerID") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="InvoiceNo" HeaderText="NO FAKTUR" HeaderStyle-Width="250px">
                                <ItemStyle CssClass=""></ItemStyle>
                                <ItemTemplate> 
                                    <asp:label ID="lblInvoiceNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.InvoiceNo") %>'>
                                </asp:label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="BranchID" visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblBranchID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BranchID") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                         <asp:TemplateColumn HeaderText="DrawDownDate" Visible="False">
                            <ItemStyle HorizontalAlign="right" Width="20%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblDrawDownDate" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.DrawDownDate")%>'>
                                </asp:Label> 
                            </ItemTemplate>
                        </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="InvoiceDate" Visible="False">
                            <ItemStyle HorizontalAlign="right" Width="20%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblInvoiceDate" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.InvoiceDate")%>'>
                                </asp:Label> 
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="InvoiceDueDate" Visible="False">
                            <ItemStyle HorizontalAlign="right" Width="20%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblInvoiceDueDate" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.InvoiceDueDate")%>'>
                                </asp:Label> 
                            </ItemTemplate>
                        </asp:TemplateColumn>
                         <asp:TemplateColumn HeaderText="EffectiveRate" Visible="False">
                            <ItemStyle HorizontalAlign="right"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblEffectiveRate" runat="server" Text='<%#FormatNumber(Container.DataItem("EffectiveRate"), 0)%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="FixInterest" Visible="False">
                            <ItemStyle HorizontalAlign="right"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblFixInterest" runat="server" Text='<%#FormatNumber(Container.DataItem("InterestFix"), 0)%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        
                    </Columns> 
                  </asp:DataGrid>
                  <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                        CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                        CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                        CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                        CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                    </asp:ImageButton>
                    Page
                    <asp:TextBox ID="txtGoPage" runat="server">1</asp:TextBox>
                    <asp:Button ID="btnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                        EnableViewState="False"></asp:Button>
                    <asp:RangeValidator ID="Rangevalidator1" runat="server" ControlToValidate="txtGopage"
                        MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer"
                        CssClass="validator_general"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo" runat="server" Display="Dynamic" ControlToValidate="txtGopage"
                        ErrorMessage="No Halaman Salah" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtGoPage" Type="integer"
                        MinimumValue="1" ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general"></asp:RangeValidator>
                </div>
                <div class="label_gridnavigation">
                    <asp:Label ID="lblPage" runat="server"></asp:Label>of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                    <asp:Label ID="lblTotRec" runat="server"></asp:Label>record(s)
                </div>

             </div> 
        </div> 

        <div class="form_title">
        <div class="form_single">
            <h4>
                CARI DATA</h4>
        </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cari Berdasarkan
                </label>
                <asp:DropDownList ID="cboSearch" runat="server">
                    <asp:ListItem Value="Name">Nama Customer</asp:ListItem>
                    <asp:ListItem Value="AgreementNo">No. Kontrak</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="small button blue"
                CausesValidation="False" />
            <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="small button gray"
                CausesValidation="False" />
        </div> 
     </asp:Panel>

    <asp:Panel ID="pnlRestruct" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4> PROSES RESTRUCT FACTORING</h4>
            </div>
        </div>
           <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        No Kontrak
                    </label>
                    <asp:HyperLink ID="lblPanelAgreementNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AgreementNo")%>' ></asp:HyperLink>
                </div>
                <div class="form_right">
                    <label>
                        No Faktur
                    </label>
                     <asp:Label ID="lblPanelInvoiceNo" runat="server"></asp:Label>           
                </div>
            </div>
        </div>
         <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                       Tgl Cair
                    </label>
                    <%--<uc1:ucdatece id="txtDrawDownDate" runat="server"></uc1:ucdatece>--%>     
                    <asp:Label ID="lblDrawDownDate" runat="server" ></asp:Label>   
                </div>
                <div class="form_right">
                    <label>
                        Tgl Faktur
                    </label>
                     <%--<uc1:ucdatece id="txtInvoiceDate" runat="server"></uc1:ucdatece>--%>     
                    <asp:Label ID="lblInvoiceDate" runat="server" ></asp:Label>          
                </div>
            </div>
        </div>
          <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                       Effective Rate
                    </label> 
                    <asp:Label ID="lblPanelEffRate" runat="server" ></asp:Label><asp:Label runat="server"> %</asp:Label>   
                </div>
                <div class="form_right">
                    <label>
                        Margin Fix
                    </label>     
                    <asp:Label ID="lblPanelFixInterest" runat="server"></asp:Label><asp:Label runat="server"> %</asp:Label>          
                </div>
            </div>
        </div>
         <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                       Margin Berjalan
                    </label> 
                    <asp:Label ID="lblAccruedInterest" runat="server"></asp:Label><asp:Label runat="server"> %</asp:Label>   
                </div>
                <div class="form_right"> 
                     <label> 
                    Tgl Jatuh Tempo
                    </label>                    
                    <%--<uc1:ucdatece id="txtInvoiceDueDate" runat="server"></uc1:ucdatece>--%>     
                    <asp:Label ID="lblInvoiceDueDate" runat="server" ></asp:Label>   
                    </div>
            </div>
        </div>

     <div class="form_title">
            <div class="form_single">
                <h4> RESTRUCT </h4>
            </div>
      </div>
        <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Effective Rate
                </label>
                <asp:TextBox ID="textEffectiveRateNew" runat="server" width="30px" MaxLength="5"></asp:TextBox><asp:Label>%</asp:Label>                
            </div>
            <div class="form_right">
                <label> 
                    Tgl Jatuh Tempo
                </label>                    
                <uc1:ucdatece id="txtInvoiceDueDateNew" runat="server"></uc1:ucdatece>       
            </div>
        </div>
    </div>

         <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Margin
                </label>
                <asp:TextBox ID="txtBunga" runat="server" width="30px" MaxLength="5"></asp:TextBox><asp:Label>%</asp:Label>                
            </div>
            <div class="form_right">      
            </div>
        </div>
    </div>

 
          <div class="form_title">
            <div class="form_single">
                <h4> UPLOAD </h4>
            </div>
      </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    File</label>
                <asp:FileUpload ID="FileVA" runat="server" />
            </div>

             <div class="form_right">
                  <asp:Button ID="btnUpload" runat="server" Text="Upload" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="Button1" runat="server" Text="Reset" CssClass="small button blue"
                CausesValidation="false"></asp:Button>
            <asp:Label runat="server" ID="lblLoading" class="progress_imgLoading"></asp:Label>
             </div>
        </div>
  <%--      <div class="form_button">
            <asp:Button ID="btnUpload" runat="server" Text="Upload" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="Button1" runat="server" Text="Reset" CssClass="small button blue"
                CausesValidation="false"></asp:Button>
            <asp:Label runat="server" ID="lblLoading" class="progress_imgLoading"></asp:Label>
        </div>--%>



    <div class="form_button">
            <asp:Button ID="btnSave" runat="server" CausesValidation="true" Text="Save" CssClass="small button blue" />
            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray" />
        </div>

    </asp:Panel>

    </form>
</body>
</html>
