﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class RestructFactAndMUStep1
    Inherits Maxiloan.Webform.AccMntWebBased

#Region "Constanta"
    Protected WithEvents oCust As UcCustExposure
    Protected WithEvents oPaymentInfo As UcPaymentInfo
    'Protected WithEvents oPaymentInfo As UcFullPrepayInfo
    Protected WithEvents oAgree As UcAgreeExposure
    Private oControllerInfo As New UCPaymentInfoController
    Private oCustomClassInfo As New Parameter.AccMntBase
#End Region
#Region "Property"
    Private Property strCustomerid() As String
        Get
            Return (CType(ViewState("strCustomerid"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("strCustomerid") = Value
        End Set
    End Property

    Private Property InvoiceNo() As String
        Get
            Return CType(ViewState("InvoiceNo"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("InvoiceNo") = Value
        End Set
    End Property
    Private Property InvoiceSeqNo() As Int64
        Get
            Return CType(ViewState("InvoiceSeqNo"), Int64)
        End Get
        Set(ByVal Value As Int64)
            ViewState("InvoiceSeqNo") = Value
        End Set
    End Property
#End Region

#Region "Page Load"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        Me.FormID = "RestructFactAndMU"
        If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
            If Not IsPostBack() Then
                oPaymentInfo.isTitle = True
                Me.ApplicationID = Request.QueryString("ApplicationId")
                Me.BranchID = Request.QueryString("BranchID")
                Me.InvoiceNo = Request.QueryString("InvoiceNo")
                Me.InvoiceSeqNo = Request.QueryString("InvoiceSeqNo")
                DoBind()
            End If
        End If
    End Sub
    Sub DoBind()
        lbljudul.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
        oCustomClassInfo.ApplicationID = Me.ApplicationID
        oCustomClassInfo.strConnection = GetConnectionString()
        oCustomClassInfo.ValueDate = Me.BusinessDate
        oCustomClassInfo.InvoiceNo = Me.InvoiceNo
        oCustomClassInfo.InvoiceSeqNo = Me.InvoiceSeqNo
        'oCustomClassInfo = oControllerInfo.GetPaymentInfo(oCustomClassInfo)
        oCustomClassInfo = oControllerInfo.GetPaymentInfoFactAndMDKJ(oCustomClassInfo)
        lblInvoiceNo.Text = Me.InvoiceNo
        With oCustomClassInfo
            lblAgreementNo.Text = .Agreementno
            Me.strCustomerid = .CustomerID
            lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(Me.ApplicationID.Trim) & "')"
            lblCustName.Text = .CustomerName
            lblCustName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(Me.strCustomerid.Trim) & "')"
        End With
        With oPaymentInfo
            .IsTitle = True
            .ValueDate = Me.BusinessDate
            .ApplicationID = Me.ApplicationID
            .InvoiceNo = Me.InvoiceNo
            .InvoiceSeqNo = Me.InvoiceSeqNo
            '.PaymentInfo()
            .PaymentInfoFactAndMDKJ()
        End With
        With oAgree
            .ApplicationID = Me.ApplicationID
            .AgreementExposure()
        End With

        With oCust
            .CustomerID = Me.strCustomerid
            .CustomerExposure()
        End With
    End Sub
    Private Sub imbCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles imbCancel.Click
        Response.Redirect("RestructFactAndMU.aspx")
    End Sub

    Private Sub imbNext_Click(ByVal sender As Object, ByVal e As EventArgs) Handles imbNext.Click
        If CheckFeature(Me.Loginid, Me.FormID, "NEXT", Me.AppId) Then
            Response.Redirect("RestructFactAndMUStep2.aspx?Applicationid=" & Me.ApplicationID & "&branchid=" & Me.BranchID.Trim & "&invoiceno=" & Me.InvoiceNo.Trim & "&invoiceseqno=" & Me.InvoiceSeqNo)
        End If
    End Sub
#End Region


End Class