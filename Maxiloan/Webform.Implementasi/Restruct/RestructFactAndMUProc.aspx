﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="RestructFactAndMUProc.aspx.vb" Inherits="Maxiloan.Webform.Implementasi.RestructFactAndMUProc" %>


<%@ Register TagPrefix="uc1" TagName="UcFullPrepayInfo" Src="../../Webform.UserController/UcFullPrepayInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcPaymentInfo" Src="../../Webform.UserController/UcPaymentInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucApprovalRequest" Src="../../Webform.UserController/ucApprovalRequest.ascx" %>
<%@ Register Src="../../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat"
    TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>RestructFactAndMUProc</title>
    <link rel="Stylesheet" type="text/css" href="../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../Include/Buttons.css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';
    </script>
    <script language="javascript" type="text/javascript">
		function Freq(){
			var payfreq = document.Form1.hdnPaymentFreq.value;
			var oPayment = window.document.forms[0].cbopaymentFreq
			var cbopayment = oPayment.options[oPayment.selectedIndex].value;
			var tenor = document.forms[0].hdnTenor.value;
			var InsNum = tenor/cbopayment
			<%=lblInstallmentNum.ClientID%>.innerHTML = InsNum;
		}
		
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>   
    <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h4>
                PENGAJUAN RESCHEDULING FACT DAN MODAL USAHA
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                No Kontrak
            </label>
            <asp:HyperLink ID="lblAgreementNo" runat="server"></asp:HyperLink>
        </div>
        <div class="form_right">
            <label>
                Nama Customer
            </label>
            <asp:HyperLink ID="lblCustName" runat="server"></asp:HyperLink>
        </div>
    </div>
    <div class="form_title">
        <div class="form_single">
            <h4>
                DATA SAAT INI
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Jenis Margin
            </label>
            <asp:Label ID="lblInterestType" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Produk
            </label>
            <asp:Label ID="lblProduct" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Skema Angsuran
            </label>
            <asp:Label ID="lblInstallScheme" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Package
            </label>
            <asp:Label ID="lblPackage" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Jenis Finance
            </label>
            <asp:Label ID="lblFinanceType" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Penjamin
            </label>
            <asp:Label ID="lblGuarantor" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Suku Margin Effective
            </label>
            <asp:Label ID="lblEffRate" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Angsuran Ke
            </label>
            <asp:Label ID="lblInstallmentNo" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Pola Pembayaran
            </label>
            <asp:Label ID="lblPaymentFreq" runat="server"></asp:Label><input id="hdnPaymentFreq"
                type="hidden" name="hdnPaymentFreq" runat="server">
        </div>
        <div class="form_right">
            <label>
                No Rescheduling
            </label>
            <asp:Label ID="lblReschedNo" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_title">
        <div class="form_single">
            <h4>
                A/R PER TANGGAL
                <asp:Label ID="lbljudul" runat="server"></asp:Label>
            </h4>
        </div>
    </div>
    <uc1:ucfullprepayinfo id="oPaymentInfo" runat="server">
                </uc1:ucfullprepayinfo>
    <div class="form_box">
        <div class="form_left">
            <label>
                Total Sisa A/R
            </label>
            <asp:Label ID="lblStopAccruedAmount" runat="server" CssClass="numberAlign label"></asp:Label>
        </div>
        <div class="form_right">
        </div>
    </div>
    <div class="form_title">
        <div class="form_left">
            <h4>
                JUMLAH BAYAR DIMUKA
            </h4>
        </div>
        <div class="form_right">
            <h4>
                DISKON
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label class="label_req">
                Outstanding Pokok
            </label>
            <asp:Label ID="lblOutstandingPrincipal" runat="server" CssClass="numberAlign label"></asp:Label>
        </div>
        <div class="form_right">
            <label class="label_req">
                Outstanding Pokok
            </label>
            <uc1:ucnumberformat id="txtOutstandingPrincipal" runat="server" />
        </div>
    </div>

     <div class="form_box">
        <div class="form_left">
            <label class="label_req">
                Outstanding Margin
            </label>
            <asp:Label ID="lblOutstandingInterest" runat="server" CssClass="numberAlign label"></asp:Label>
        </div>
        <div class="form_right">
            <label class="label_req">
                Outstanding Margin
            </label>
            <uc1:ucnumberformat id="txtOutstandingInterest" runat="server" Enabled="False" />
        </div>
    </div>

    <div class="form_box">
        <div class="form_left">
            <label class="label_req">
                Outstanding Ta'widh
            </label>
            <asp:Label ID="lblOutstandingLC" runat="server" CssClass="numberAlign label"></asp:Label>
        </div>
        <div class="form_right">
            <label class="label_req">
                Outstanding Ta'widh
            </label>
            <uc1:ucnumberformat id="txtOutstandingLC" runat="server" />
        </div>
    </div>
      <div class="form_box">
        <div class="form_left">
            <label class="label_req">
                Margin Berjalan
            </label>
            <asp:Label ID="lblAccruedInterest" runat="server" CssClass="numberAlign label"></asp:Label>
        </div>
        <div class="form_right">
            <label class="label_req">
                Margin Berjalan
            </label>
            <uc1:ucnumberformat id="txtAccruedInterest" runat="server" />
        </div>
    </div>
        <div class="form_box">
        <div class="form_left">
            <label class="label_req">
                Margin Jatuh Tempo
            </label>
            <asp:Label ID="lblInstallmentDue" runat="server" CssClass="numberAlign label"></asp:Label>
        </div>
        <div class="form_right">
            <label class="label_req">
                Margin Jatuh Tempo
            </label>
            <uc1:ucnumberformat id="txtInstallmentDue" runat="server" />
        </div>
    </div>
    <div class="form_box">
        <div class="form_left"> 
             <label>
                Surat Perjanjian
            </label>
            <asp:TextBox ID="txtPerjanjianNo" runat="server"  Width="225px" ></asp:TextBox>
        </div>
        <div class="form_right">
            <label class="label_req">
                PPh (Perjanjian Penyelesaian Hutang)
            </label>
             <uc1:ucnumberformat id="txtPPh" runat="server" /> 
        </div>
    </div>
     <div class="form_box">
        <div class="form_left"> 
        </div>
        <div class="form_right">
            <label>
                Total Discount
            </label>
            <asp:Label ID="lblTotDiscAmt" runat="server"></asp:Label>
        </div>
    </div>

      <div class="form_box">
        <div class="form_left">
            <label class="label_req">
                Pelunasan Sebagian
            </label>
            <uc1:ucnumberformat id="txtPartialPay" runat="server" />
        </div>
        <div class="form_right"> 
        </div>
    </div>
      <div class="form_box">
        <div class="form_left">
            <label class="label_req">
                Biaya Administrasi
            </label>
            <uc1:ucnumberformat id="txtAdminFee" runat="server" />
            <label>  Masukan ke Pokok Hutang</label><asp:CheckBox runat="server" ID="chkAdminFee" /> 
        </div>
        <div class="form_right">
            <label class="label_req"></label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Total Dibayar Dimuka
            </label>
            <asp:Label ID="lblTotAdvAmt" runat="server"></asp:Label>
        </div>
        <div class="form_right"> 
        </div>
    </div>

     <div class="form_box">
        <div class="form_left">
            <label>
                Total Pokok Hutang
            </label>
            <asp:Label ID="lblTotAdvAmount" runat="server"></asp:Label>
        </div>
        <div class="form_right"> 
        </div>
    </div>
<%--modify by nofi 22022019 buat ulang untuk ada diskon sesuai diskusi terbaru dg pak indra --%>
    <div class="form_box_hide">
    
    <div class="form_box">
        <div class="form_left">
       <%--     <label class="label_req">
                Pelunasan Sebagian
            </label>
            <uc1:ucnumberformat id="txtPartialPay" runat="server" />--%>
        </div>
        <div class="form_right">
            <label class="label_req">
                Ta'widh Keterlambatan Angsuran
            </label>
            <uc1:ucnumberformat id="txtInstallLC" runat="server" />
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <%--<label class="label_req">
                Biaya Administrasi
            </label>
            <uc1:ucnumberformat id="txtAdminFee" runat="server" />--%>
        </div>
        <div class="form_right">
            <label class="label_req">
                Biaya Tagih Angsuran
            </label>
            <uc1:ucnumberformat id="txtInstallCollFee" runat="server" />
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
        </div>
        <div class="form_right">
            <label class="label_req">
                Ta'widh Keterlambatan Asuransi
            </label>
            <uc1:ucnumberformat id="txtInsuranceLC" runat="server" />
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
        </div>
        <div class="form_right">
            <label class="label_req">
                Biaya Tagih Asuransi
            </label>
            <uc1:ucnumberformat id="txtInsurCollFee" runat="server" />
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
        </div>
        <div class="form_right">
            <label class="label_req">
                Biaya Tolakan PDC
            </label>
            <uc1:ucnumberformat id="txtPDCBounceFee" runat="server" />
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
        </div>
        <div class="form_right">
            <label class="label_req">
                Biaya Perpanjangan STNK/BBN
            </label>
            <uc1:ucnumberformat id="txtSTNKFee" runat="server" />
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
        </div>
        <div class="form_right">
            <label class="label_req">
                Biaya Klaim Asuransi
            </label>
            <uc1:ucnumberformat id="txtInsuranceClaim" runat="server" />
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
        </div>
        <div class="form_right">
            <label class="label_req">
                Biaya Tarik
            </label>
            <uc1:ucnumberformat id="txtReposessFee" runat="server" />
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <%--<label>
                Total Dibayar Dimuka
            </label>
            <asp:Label ID="lblTotAdvAmt" runat="server"></asp:Label>--%>
        </div>
        <div class="form_right">
         <%--   <label>
                Total Discount
            </label>
            <asp:Label ID="lblTotDiscAmt" runat="server"></asp:Label>--%>
        </div>
    </div>
    </div>
    <div class="form_box_hide">
    <div class="form_title">
        <div class="form_single">
            <h4>
                PERJANJIAN PENYELESAIAN HUTANG
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label class="label_req">
               PPH Accrued Interest
            </label>
            <uc1:ucnumberformat id="txtpphaccrued" runat="server" />
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label class="label_req">
               PPH Ta'widh Keterlambatan
            </label>
            <uc1:ucnumberformat id="txtpphdenda" runat="server" />
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Total PPH
            </label>
            <asp:Label ID="lbltotalpph" runat="server"></asp:Label>
        </div>
    </div>
    </div>
    <div class="form_button">
        <asp:Button ID="imbCalculate" runat="server" Text="Calculate" CssClass="small button blue"
            CausesValidation="false"></asp:Button>
    </div>
    <asp:Panel ID="panelstruktur" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    STRUKTUR FINANCIAL BARU
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Jumlah Pokok Baru
                </label>
                <asp:Label ID="lblNPA" runat="server" CssClass="numberAlign label"></asp:Label>
            </div>
            <div class="form_right">
                <label class="label_req">
                    <asp:Label ID="lblNumberOfStep" runat="server">Number of Step</asp:Label>
                </label>
                <uc1:ucnumberformat id="txtStep" runat="server" />
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label class="label_req">
                    Suku Margin Effective (%)
                </label>
                <uc1:ucnumberformat id="txtEffRate" runat="server" />
            </div>
            <div class="form_right">
                <label>
                    Suku Margin Flat
                </label>
                <asp:Label ID="lblFlatRate" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Pola Pembayaran
                </label>
                <asp:DropDownList ID="cbopaymentFreq" runat="server" onchange="Freq();">
                    <asp:ListItem Value="1">Monthly</asp:ListItem>
                    <asp:ListItem Value="2">Bimonthly</asp:ListItem>
                    <asp:ListItem Value="3">Quarterly</asp:ListItem>
                    <asp:ListItem Value="6">Semi Annualy </asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="form_right">
                <label class="label_req">
                    <asp:Label ID="lblCummulative" runat="server">Mulai dari Angs Ke</asp:Label>
                </label>
                <uc1:ucnumberformat id="txtCummulative" runat="server" />
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Lama Angsuran
                </label>
                <asp:Label ID="lblInstallmentNum" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Jangka Waktu
                </label>
                <asp:Label ID="lblTenor" runat="server"></asp:Label>
                <input id="hdnTenor" type="hidden" name="hdnTenor" runat="server" />
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label class="label_req">
                    Jumlah Angsuran
                </label>
                <uc1:ucnumberformat id="txtInstallmentAmt" runat="server" />
            </div>
            <div class="form_right">
            </div>
        </div>
    </asp:Panel>
    <div class="form_button">
        <asp:Button ID="imbCalAmor" runat="server" Text="Calculate Amortization" CssClass="small button blue"
            CausesValidation="false"></asp:Button>
        <asp:Button ID="imbCalRate" runat="server" Text="Calculate Rate" CssClass="small button blue"
            CausesValidation="false"></asp:Button>
        <div style="display: none;">
        <asp:Button ID="imbPrint" runat="server" Text="Print" CssClass="hidden"
            CausesValidation="false"></asp:Button>
        </div>
        <asp:Button ID="imbUpload" runat="server" Text="Upload" CssClass="small button blue"
            CausesValidation="false"></asp:Button>
        <asp:FileUpload ID="FileVA" runat="server" />
        <asp:Label runat="server" ID="lblLoading" class="progress_imgLoading"></asp:Label>
        <asp:Button ID="imbReset" runat="server" Text="Reset" CssClass="small button blue"
            CausesValidation="false"></asp:Button>
    </div>
    <asp:Panel ID="pnlEntryInstallment" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    ANGSURAN
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgEntryInstallment" runat="server" CssClass="grid_general" AutoGenerateColumns="False"
                        AllowSorting="True">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:BoundColumn DataField="No" HeaderText="NO"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="LAMA ANGSURAN">
                                <ItemTemplate>
                                    <uc1:ucnumberformat id="txtNoStep" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="JUMLAH ANGSURAN">
                                <ItemTemplate>
                                    <uc1:ucnumberformat id="txtEntryInstallment" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <asp:ImageButton ID="ImgEntryInstallment" runat="server" ImageUrl="../../Images/ButtonView.gif">
                </asp:ImageButton>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlViewST" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    VIEW ANGSURAN
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgViewInstallment" runat="server" AutoGenerateColumns="False"
                        AllowSorting="True" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:BoundColumn DataField="InsSeqNo" HeaderText="No"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="Installment">
                                <ItemTemplate>
                                    <asp:Label ID="lblIA" runat="server" Text='<%#FormatNumber(DataBinder.Eval(Container, "DataItem.InstallmentAmount"), 0) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="POKOK">
                                <ItemTemplate>
                                    <asp:Label ID="lblPA" runat="server" Text='<%#FormatNumber(DataBinder.Eval(Container, "DataItem.PrincipalAmount"), 0) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="MARGIN">
                                <ItemTemplate>
                                    <asp:Label ID="lblInterest" runat="server" Text='<%#FormatNumber(DataBinder.Eval(Container, "DataItem.InterestAmount"), 0) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="SISA POKOK">
                                <ItemTemplate>
                                    <asp:Label ID="lblOP" runat="server" Text='<%#FormatNumber(DataBinder.Eval(Container, "DataItem.OutStandingPrincipal"), 0) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="SISA MARGIN">
                                <ItemTemplate>
                                    <asp:Label ID="lblOI" runat="server" Text='<%#FormatNumber(DataBinder.Eval(Container, "DataItem.OutStandingInterest"), 0) %>'>
                                    </asp:Label>
                                    <asp:Label ID="lblSeq" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Seq") %>'
                                        Visible="false">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="DueDate" HeaderText="TGL JT" Visible="false"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>

                    <asp:DataGrid ID="dtgViewInstallment2" runat="server" AutoGenerateColumns="False"
                        AllowSorting="True" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:BoundColumn DataField="No" HeaderText="No"></asp:BoundColumn>
                            <asp:BoundColumn DataField="DueDate" HeaderText="TGL JT"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="INSTALLMENT">
                                <ItemTemplate>
                                    <asp:Label ID="lblPA" runat="server" Text='<%#FormatNumber(DataBinder.Eval(Container, "DataItem.Installment"), 0) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="POKOK">
                                <ItemTemplate>
                                    <asp:Label ID="lblInterest" runat="server" Text='<%#FormatNumber(DataBinder.Eval(Container, "DataItem.Principal"), 0) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="MARGIN">
                                <ItemTemplate>
                                    <asp:Label ID="lblOP" runat="server" Text='<%#FormatNumber(DataBinder.Eval(Container, "DataItem.Interest"), 0) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="SISA POKOK">
                                <ItemTemplate>
                                    <asp:Label ID="lblOI" runat="server" Text='<%#FormatNumber(DataBinder.Eval(Container, "DataItem.PrincBalance"), 0) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="SISA MARGIN">
                                <ItemTemplate>
                                    <asp:Label ID="lblOP" runat="server" Text='<%#FormatNumber(DataBinder.Eval(Container, "DataItem.PrincInterest"), 0) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
    </asp:Panel>
<div class="form_box_hide">
    <div class="form_title">
        <div class="form_single">
            <h4>
                CROSS DEFAULT
            </h4>
        </div>
    </div>
    <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgCrossDefault" runat="server" CssClass="grid_general" AutoGenerateColumns="False">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="NO">
                            <ItemTemplate>
                                <asp:Label ID="lblCDNo" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="Name" HeaderText="NO KONTRAK">
                            <ItemTemplate>
                                <asp:Label ID="lblCDAgreementNo" runat="server"></asp:Label>
                                <asp:TextBox ID="txtCDAgreement" runat="server" Columns="25" MaxLength="20" ></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="NAMA CUSTOMER">
                            <ItemTemplate>
                                <asp:Label ID="lblCDName" Text='<%#Container.DataItem("Name")%>' runat="server">
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="TGL KONTRAK">
                            <ItemTemplate>
                                <asp:Label ID="lblCDAgreementDate" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="STATUS DEFAULT">
                            <ItemTemplate>
                                <asp:Label ID="lblCDDefaultStatus" Text='<%#Container.DataItem("DefaultStatus")%>'
                                    runat="server">
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="STATUS CONTRACT">
                            <ItemTemplate>
                                <asp:Label ID="lblCDContractStatus" Text='<%#Container.DataItem("ContractStatus")%>'
                                    runat="server">
                                </asp:Label>
                                <asp:Label ID="lblCDApplicationID" Text='<%#Container.DataItem("CDApplicationID")%>'
                                    runat="server" Visible="false">
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="DELETE">
                            <ItemTemplate>
                                <asp:ImageButton ID="imbCDDelete" CommandName="CDDelete" runat="server" ImageUrl="../../images/icondelete.gif"
                                    CausesValidation="False"></asp:ImageButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="imbCDAdd" runat="server" Text="Add" CssClass="small button blue"
            CausesValidation="False"></asp:Button>
        <asp:Button ID="imbCDShow" runat="server" Text="Show" CssClass="small button blue"
            CausesValidation="False"></asp:Button>
    </div>
    <div class="form_title">
        <div class="form_single">
            <h4>
                SYARAT DAN KONDISI
            </h4>
        </div>
    </div>
    <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgTC" runat="server" CssClass="grid_general" AutoGenerateColumns="False"
                    DataKeyField="TCName" PageSize="3">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="NO">
                            <ItemTemplate>
                                <asp:Label ID="lblTCNo" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="TCName" HeaderText="DOKUMEN"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="PERIKSA">
                            <ItemTemplate>
                                <asp:CheckBox ID="chkTCChecked" Checked='<%# DataBinder.Eval(Container, "DataItem.Checked") %>'
                                    runat="server"></asp:CheckBox>
                                <asp:Label ID="lblVTCChecked" runat="server" ForeColor="Red">Must be checked!</asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="IsMandatory" HeaderText="MANDATORY"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="CATATAN">
                            <ItemTemplate>
                                <asp:TextBox ID="txtTCNotes" Text='<%# DataBinder.Eval(Container, "DataItem.Notes") %>'
                                    runat="server" Width="95%" >
                                </asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn Visible="False" DataField="MasterTCID" HeaderText="MasterTCID">
                        </asp:BoundColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
        </div>
    </div>
    <div class="form_title">
        <div class="form_single">
            <h4>
                SYARAT DAN KONDISI CHECK LIST
            </h4>
        </div>
    </div>
    <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgTC2" runat="server" CssClass="grid_general" AutoGenerateColumns="False"
                    DataKeyField="TCName" PageSize="3">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="NO">
                            <ItemTemplate>
                                <asp:Label ID="lblTCNo2" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="TCName" HeaderText="DOKUMEN"></asp:BoundColumn>
                        <asp:BoundColumn DataField="CheckList" HeaderText="CHECKLIST"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="PERIKSA">
                            <ItemTemplate>
                                <asp:CheckBox ID="chkTCCheck2" Checked='<%# DataBinder.Eval(Container, "DataItem.Checked") %>'
                                    runat="server"></asp:CheckBox>
                                <asp:Label ID="lblVTC2Checked" runat="server" ForeColor="Red">Must be Checked!</asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="ISMandatory" HeaderText="MANDATORY"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="CATATAN">
                            <ItemTemplate>
                                <asp:TextBox ID="txtTCNotes2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Notes") %>'
                                    >
                                </asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn Visible="False" DataField="MasterTCID" HeaderText="MasterTCID">
                        </asp:BoundColumn>
                        <asp:BoundColumn Visible="False" DataField="MSTCCLSequenceNo" HeaderText="MSTCCLSequenceNo">
                        </asp:BoundColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
        </div>
    </div>
    </div>
    <asp:Panel ID="pnlApproval" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DETAIL RESCHEDULING
                </h4>
            </div>
        </div>
        <uc1:ucapprovalrequest id="oApprovalRequest" runat="server"></uc1:ucapprovalrequest>
        <div class="form_box">
            <div class="form_left">
            </div>
            <div class="form_right">
                <label>
                    Total yang harus dibayar
                </label>
                <asp:Label ID="lblTotalAmountToBePaid" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
            </div>
            <div class="form_right">
                <label>
                    Jumlah Prepaid
                </label>
                <asp:Label ID="lblPrepaidAmount" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
            </div>
            <div class="form_right">
                <label>
                    Selisih
                </label>
                <asp:Label ID="lblBalanceAmount" runat="server"></asp:Label>
            </div>
        </div>
    </asp:Panel>
    <div class="form_button">
        <asp:Button ID="imbNext" runat="server" Text="Next" CssClass="small button green"
            CausesValidation="True"></asp:Button>
        <asp:Button ID="imbCancel" runat="server" Text="Cancel" CssClass="small button gray"
            CausesValidation="False"></asp:Button>
    </div>
    </form>
</body>
</html>

