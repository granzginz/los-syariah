﻿Imports CrystalDecisions.Shared
Imports Maxiloan.Controller
Imports Maxiloan.Webform.LoanOrg

Public Class SimulasiPerhitunganPembiayaanViewer
    Inherits Maxiloan.Webform.WebBased

#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("cmdwhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("cmdwhere") = Value
        End Set
    End Property
    Private Property MultiAgreementNo() As String
        Get
            Return CType(ViewState("MultiAgreementNo"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("MultiAgreementNo") = Value
        End Set
    End Property
    Private Property ApplicationID() As String
        Get
            Return CType(ViewState("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
#End Region

#Region "Constanta"
    Friend Shared ReportSource As TransparansiTingkatSukuBungadanDenda
#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If SessionInvalid() Then
            Exit Sub
        End If

        bindReport()
        'createData()
    End Sub
#End Region

#Region "BindReport"
    Private Sub bindReport()
        Dim oController As New SimulasiPerhitunganPembiayaanController
        Dim oParameter As New Parameter.SimulasiPerhitunganPembiayaan

        GetCookies()

        Dim AgreementList = Split(Me.MultiAgreementNo, ",").ToList

        Me.MultiAgreementNo = ""

        For Each agreeno As String In AgreementList

            Dim oDataSet As New DataSet
            Dim oDataTable As New DataTable
            Dim oReport As TransparansiTingkatSukuBungadanDenda = New TransparansiTingkatSukuBungadanDenda


            With oParameter
                .strConnection = GetConnectionString()
                .AgreementNo = agreeno
                .BranchId = Me.sesBranchId.Replace("'", "")
            End With

            oParameter = oController.GenerateSimulasiPerhitunganPembiayaan(oParameter)
            oDataSet = oParameter.ListReport

            oReport.SetDataSource(oDataSet.Tables(0))

            CrystalReportViewer1.ReportSource = oReport
            CrystalReportViewer1.Visible = True
            CrystalReportViewer1.DataBind()

            With oReport.ExportOptions
                .ExportDestinationType = ExportDestinationType.DiskFile
                .ExportFormatType = ExportFormatType.PortableDocFormat
            End With

            Dim strFileLocation As String

            strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
            strFileLocation += Me.Session.SessionID + Me.Loginid + "rpt_SimulasiFee_" & agreeno & ".pdf"

            Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions

            DiskOpts.DiskFileName = strFileLocation

            With oReport
                .ExportOptions.DestinationOptions = DiskOpts
                .Export()
                .Close()
                .Dispose()
            End With

            If Me.MultiAgreementNo = "" Then
                Me.MultiAgreementNo = Me.Session.SessionID + Me.Loginid + "rpt_SimulasiFee_" & agreeno
            Else
                Me.MultiAgreementNo = Me.MultiAgreementNo & "," & (Me.Session.SessionID + Me.Loginid + "rpt_SimulasiFee_" & agreeno)
            End If
        Next

        Response.Redirect("SimulasiPerhitunganPembiayaan.aspx?strFileLocation=" & Me.MultiAgreementNo)

    End Sub

    Function GetCompanyName() As String
        Dim result As String

        Dim oConInst As New InstallRcvController
        Dim oInstal As New Parameter.InstallRcv
        oInstal.strConnection = GetConnectionString()
        oInstal.SPName = "spGetCompanyFullName"
        oInstal.WhereCond = ""
        If oConInst.GetSP(oInstal).ListData.Rows.Count > 0 Then
            result = oConInst.GetSP(oInstal).ListData.Rows(0).Item("CompanyFullName").ToString.Trim
        End If

        Return result
    End Function
#End Region

#Region "CreateData"
    'Private Sub createData()
    '    Dim oController As New PPKController
    '    Dim oDataSet As New DataSet
    '    Dim oParameter As New Parameter.PPK

    '    oParameter.strConnection = GetConnectionString()
    '    oParameter = oController.ListReportPPKreditor(oParameter)
    '    oDataSet = oParameter.ListReport
    '    oDataSet.WriteXmlSchema("D:\project\Maxiloan\AccAcq\Credit\Print\xmd_PPKreditor.xml")
    'End Sub
#End Region

#Region "GetCookies"
    Private Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies("RptSimulasiFee")

        Me.ApplicationID = cookie.Values("ApplicationID")
        Me.SortBy = cookie.Values("sortby")
        Me.MultiAgreementNo = cookie.Values("MultiAgreementNo")
    End Sub
#End Region

End Class