﻿Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports System.Web.Services
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Exceptions

Public Class TerimaPembayaranTDP
    Inherits Maxiloan.Webform.WebBased

    Protected WithEvents txtJumlah As ucNumberFormat

    Private cController As New ImplementasiControler
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private m_controller As New DataUserControlController
    Dim customClass As New Parameter.Implementasi
    Private oDataUserCtlr As New DataUserControlController
    Private Property ApplicationID() As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            ApplicationID = Request("Applicationid")

            Dim cmdWhere As String
            cmdWhere = " and a.ApplicationID = '" & Me.ApplicationID & "'"
            With cmbBankAccount
                .DataSource = oDataUserCtlr.GetBankAccount(GetConnectionString, Me.sesBranchId, "B", "EC")
                .DataTextField = "Name"
                .DataValueField = "ID"
                .DataBind()
                If Not IsNothing(Session("BankSelected")) Then .SelectedValue = Session("BankSelected").ToString.Trim
            End With
            display(cmdWhere)
            lblMessage.Visible = False
        End If
    End Sub
    Sub display(ByVal cmdwhere As String)
        Try
            lblMessage.Visible = False
            Dim dtslist As DataTable
            Dim cReceive As New GeneralPagingController
            Dim oReceive As New Parameter.GeneralPaging
            With oReceive
                .strConnection = GetConnectionString()
                .WhereCond = cmdwhere
                .CurrentPage = currentPage
                .PageSize = pageSize
                .SortBy = ""
                .SpName = "spAgreementTDPList"
            End With
            oReceive = cReceive.GetGeneralPaging(oReceive)
            dtslist = oReceive.ListData
            If dtslist.Rows.Count > 0 Then
                lblCustomerName.Text = CStr(dtslist.Rows(0).Item("Name")).Trim
                lblgolive.Text = CStr(dtslist.Rows(0).Item("GoLiveDate")).Trim
                lblDP.Text = FormatNumber(CStr(dtslist.Rows(0).Item("DownPaymentMf")).Trim, 0)
                lblDPKaroseri.Text = FormatNumber(CStr(dtslist.Rows(0).Item("DPKaroseriAmountMF")).Trim, 0)
                lbladmin.Text = FormatNumber(CStr(dtslist.Rows(0).Item("AdminFeeMf")).Trim, 0)
                lblFiducia.Text = FormatNumber(CStr(dtslist.Rows(0).Item("FiduciaFeeMf")).Trim, 0)
                lblotherfee.Text = FormatNumber(CStr(dtslist.Rows(0).Item("OtherFeeMf")).Trim, 0)
                lblprovisi.Text = FormatNumber(CStr(dtslist.Rows(0).Item("ProvisionFeeMf")).Trim, 0)
                lblAdvanceAngsuran.Text = FormatNumber(CStr(dtslist.Rows(0).Item("InstallmentAmountFirstMF")).Trim, 0)
                lblAsuransi.Text = FormatNumber(CStr(dtslist.Rows(0).Item("PaidAmountByCustMF")).Trim, 0)
                lbltotalmf.Text = FormatNumber(CStr(dtslist.Rows(0).Item("totalmf")).Trim, 0)
                lblApplicationID.Text = CStr(dtslist.Rows(0).Item("Applicationid")).Trim
                lblagreementno.Text = CStr(dtslist.Rows(0).Item("AgreementNo")).Trim
                lblHandlingFee.Text = FormatNumber(CStr(dtslist.Rows(0).Item("HandlingFee")).Trim, 0)
                lblTerbayar.Text = FormatNumber(CStr(dtslist.Rows(0).Item("TotalTerbayar")).Trim, 0)
            End If

            txtJumlah.RangeValidatorEnable = True
            txtJumlah.RangeValidatorMinimumValue = 1
            txtJumlah.RangeValidatorMaximumValue = dtslist.Rows(0).Item("totalmf") - dtslist.Rows(0).Item("TotalTerbayar") + 1

        Catch ex As Exception

        End Try
    End Sub

    Private Sub cboRekKoran_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboRekKoran.SelectedIndexChanged
        pnlcontainer.Visible = True
        If cboRekKoran.SelectedValue.Trim = "SUSPEND" Then
            pnlsuspend.Visible = True
            pnlBank.Visible = False
            pnlTDP.Visible = False
            pnlKOREKSI.Visible = False
        ElseIf cboRekKoran.SelectedValue.Trim = "BANK" Then
            pnlsuspend.Visible = False
            pnlBank.Visible = True
            pnlTDP.Visible = False
            pnlKOREKSI.Visible = False
        ElseIf cboRekKoran.SelectedValue.Trim = "TDP" Then
            pnlsuspend.Visible = False
            pnlBank.Visible = False
            pnlTDP.Visible = True
            pnlKOREKSI.Visible = False
        ElseIf cboRekKoran.SelectedValue.Trim = "KOREKSI" Then
            pnlsuspend.Visible = False
            pnlBank.Visible = False
            pnlTDP.Visible = False
            pnlKOREKSI.Visible = True
            txtJumlah.RangeValidatorEnable = False
        Else
            pnlcontainer.Visible = False
            pnlsuspend.Visible = False
			pnlBank.Visible = False
			pnlKOREKSI.Visible = False
		End If
    End Sub

    Private Sub btnback_Click(sender As Object, e As EventArgs) Handles btnback.Click
        Response.Redirect("pembayaranTDP.aspx")
    End Sub

    Private Sub btnsave_Click(sender As Object, e As EventArgs) Handles btnsave.Click
        Dim refNo_ As String
        If SessionInvalid() Then
            Exit Sub
        End If
		lblMessage.Visible = False
        If cboRekKoran.SelectedValue.Trim = "SUSPEND" Then
            refNo_ = txtsuspendno.Text
            If CDbl(txtAmountSuspend.Text) <> CDbl(txtJumlah.Text) Then
                ShowMessage(lblMessage, "Jumlah amount suspend tidak sesuai", True)
                Exit Sub
            End If
        ElseIf cboRekKoran.SelectedValue.Trim = "TDP" Then
            refNo_ = txtTDPReceiveno.Text
            If CDbl(txtAmountTDP.Text) <> CDbl(txtJumlah.Text) Then
                ShowMessage(lblMessage, "Jumlah amount TDP tidak sesuai", True)
                Exit Sub
            End If
        ElseIf cboRekKoran.SelectedValue.Trim = "KOREKSI" Then
            refNo_ = txtKOREKSIReceiveno.Text
            If CDbl(txtAmountKOREKSI.Text) <> CDbl(txtJumlah.Text) Then
                ShowMessage(lblMessage, "Jumlah amount KOREKSI tidak sesuai", True)
                Exit Sub
            End If
        Else
            refNo_ = txtjurnalbankno.Text
			If txtjurnalbankno.Text = "" Then
				ShowMessage(lblMessage, "Jurnal Bank belum diisi", True)
				Exit Sub
			End If
		End If

		Try
            With customClass
                .strConnection = GetConnectionString()
                .valutadate = ConvertDate2(txttglvaluta.Text)
                .ReffNo = refNo_
                .amount = txtJumlah.Text
                .SourceRK = cboRekKoran.SelectedValue.Trim
                .ApplicationID = lblApplicationID.Text
                .bankaccountid = cmbBankAccount.SelectedValue.Trim
                .BranchId = Me.sesBranchId.Replace("'", "")
            End With
            cController.SavePembayaranTDP(customClass)
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message.ToString, True)
        Finally
            Response.Redirect("pembayaranTDP.aspx")
        End Try


    End Sub
End Class