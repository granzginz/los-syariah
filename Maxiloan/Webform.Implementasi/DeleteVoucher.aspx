﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="DeleteVoucher.aspx.vb"
    Inherits="Maxiloan.Webform.Implementasi.DeleteVoucher" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="sm1">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
        onclick="hideMessage();"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                DELETE VOUCHER
            </h3>
        </div>
    </div>
    <asp:Panel ID="pnl1" runat="server">
        <div class="form_box">
            <div class="form_single">
                <label>
                    No Voucher</label>
                <asp:TextBox ID="txtVoucherNo" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ID="rfv1" ControlToValidate ="txtVoucherNo" InitialValue ="" ErrorMessage="Isi nomor voucher!"></asp:RequiredFieldValidator> 
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnNext" runat="server" Text="Delete" CssClass="small button blue">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnl2" runat="server" Visible="false">
        <div class="form_box">
            <div class="form_single">
                <asp:Label ID="lblNoVoucher" runat = "server"></asp:Label>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="small button red">
            </asp:Button>
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
