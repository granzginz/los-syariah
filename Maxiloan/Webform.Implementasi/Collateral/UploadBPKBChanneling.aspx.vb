﻿#Region "Imports"
Imports Maxiloan.Controller
Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.Webform.UserController
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data.OleDb
Imports System.Data.Odbc
Imports System.Text
Imports System.Data.SqlClient
Imports System.IO
Imports System.Web.UI.WebControls.FileUpload
Imports System.Text.RegularExpressions
Imports System.Diagnostics
Imports System.Drawing
Imports System.Runtime.InteropServices
Imports DocumentFormat.OpenXml.Packaging
Imports DocumentFormat.OpenXml.Spreadsheet
#End Region

Public Class UploadBPKBChanneling
    Inherits Maxiloan.Webform.WebBased
    Private FileName As String
    Private AgreementNo As String
    Private myCmd As New SqlCommand
    Private myAdapter As New SqlDataAdapter
    Private myDS As New DataSet
    Private myReader As DataTableReader
    Private strFileTemp As String
    Private strFile As String
    Private FileDirectory As String
    Private TotalAgreement As Double
    Private TotalAmountReceive As Decimal
    Private FileType As String
    Private objCon As SqlConnection
    Private TextFile As String



    Public Property FilaName() As String
        Get
            Return CType(ViewState("FilaName"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("FilaName") = Value
        End Set
    End Property

    Public Function PredicateFunction(ByVal custom As Parameter.UploadBPKBChanneling) As Boolean
        Return custom.BranchId = Me.BranchID And custom.agreementno = Me.agreementno
    End Function

#Region "Pageload"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblLoading.Visible = False
        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not Me.IsPostBack Then

            Me.FormID = "UploadBPKB"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) = False Then
                Exit Sub
            End If
        End If
    End Sub
#End Region

#Region "Reset"

    Private Sub btnReset_Click(sender As Object, e As System.EventArgs) Handles btnReset.Click
        Response.Redirect("UploadBPKBChanneling.aspx")
    End Sub


#End Region

#Region "Upload"

    Private Sub btnUpload_Click(sender As Object, e As System.EventArgs) Handles btnUpload.Click
        lblLoading.Visible = True
        btnUpload.Enabled = False
        Try
            If FileVA.HasFile Then
                strFileTemp = FileVA.FileName
                FileName = Context.Request.MapPath("~\\xml\\") + strFileTemp
                File.Delete(FileName)
                FileVA.SaveAs(FileName)
                Dim larr As Array = CType(strFileTemp.ToString.Split(CChar(".")), Array)
                TextFile = strFileTemp
                FileDirectory = Context.Request.MapPath("~\\xml\\").ToString.Trim
                FileType = larr.GetValue(1).ToString.Trim()

                Me.FileName = FileName
                DoProses()

            Else
                lblLoading.Visible = False
                ShowMessage(lblMessage, "File belum dipilih!", True)
                Exit Sub
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try

        btnUpload.Enabled = True
        lblLoading.Visible = False
    End Sub

    Private Sub DoProses()
        Dim SequenceNo As Integer = 0
        Dim list As New List(Of Parameter.UploadBPKBChanneling)
        If ValidasiFile() Then
            list = UploadFile()
        Else
            ShowMessage(lblMessage, "File pernah di upload!", True)
            Exit Sub
        End If
        Try
            Dim dt As DataTable = createDataUpload()
            TotalAgreement = list.Count
            If list.Count > 0 Then
                For index = 0 To list.Count - 1
                    Dim payment As New Parameter.UploadBPKBChanneling
                    payment = list(index)
                    payment.uploaddate = BusinessDate
                    payment.uploadno = SequenceNo
                    dt.Rows.Add(payment.BranchId, payment.uploaddate, payment.uploadno, payment.agreementno, strFileTemp, FileDirectory, payment.NoRangka, payment.NoMesin,
                                payment.NamaDiBPKB, payment.TglTerimaBPKB, payment.NoBPKB, payment.NoPolisi, payment.NoFaktur, payment.Keterangan, payment.Address,
                               payment.nasabah, payment.RAKId, payment.FillingId)
                Next
            End If

            UploadBPKBChannelingByBatchSave(dt)
            ShowMessage(lblMessage, "Upload Collateral BPKB Berhasil", False)

        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

    Function ValidasiFile() As Boolean
        Dim result As Boolean = True
        objCon = New SqlConnection(GetConnectionString)
        Try
            myDS = New DataSet
            myCmd.Parameters.Clear()
            myCmd.CommandText = "spGetUploadCollateralBPKBValidasi"
            myCmd.CommandType = CommandType.StoredProcedure
            myCmd.Parameters.Add(New SqlParameter("@FileName", SqlDbType.VarChar, 50) With {.Value = strFileTemp.Trim})
            myCmd.Connection = objCon
            myAdapter.SelectCommand = myCmd
            myAdapter.Fill(myDS)

            If myDS.Tables(0).Rows.Count > 0 Then
                result = False
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message)
            ShowMessage(lblMessage, ex.Message, True)
        End Try
        Return result
    End Function

    Function UploadFile() As List(Of Parameter.UploadBPKBChanneling)
        Dim list As New List(Of Parameter.UploadBPKBChanneling)
        Dim data As New DataTable
        Dim BranchID As String = Nothing

        Try
            data = FileToDataTable()

            For index = 0 To data.Rows.Count - 1
                If (index >= 0) Then
                    Dim Payment As New Parameter.UploadBPKBChanneling

                    Payment.BranchId = data.Rows(index).Item(0).ToString()
                    Payment.agreementno = data.Rows(index).Item(1).ToString()
                    Payment.nasabah = data.Rows(index).Item(2).ToString()
                    Payment.NoRangka = data.Rows(index).Item(3).ToString()
                    Payment.NoMesin = data.Rows(index).Item(4).ToString()
                    Payment.NamaDiBPKB = data.Rows(index).Item(5).ToString()
                    Payment.TglTerimaBPKB = ConvertDate2(IIf(data.Rows(index).Item(6).ToString().Trim = "", "01/01/1900", data.Rows(index).Item(6).ToString().Trim))
                    Payment.NoBPKB = data.Rows(index).Item(7).ToString()
                    Payment.NoPolisi = data.Rows(index).Item(8).ToString()
                    Payment.NoFaktur = data.Rows(index).Item(9).ToString()
                    Payment.Address = data.Rows(index).Item(10).ToString()
                    Payment.Keterangan = data.Rows(index).Item(11).ToString()
                    Payment.RAKId = data.Rows(index).Item(12).ToString()
                    Payment.FillingId = data.Rows(index).Item(13).ToString()

                    Me.BranchID = Payment.BranchId
                    Me.AgreementNo = Payment.agreementno

                    Dim query As New Parameter.UploadBPKBChanneling
                    If list.Count > 0 Then
                        query = list.Find(AddressOf PredicateFunction)
                    Else
                        query = Nothing
                    End If

                    If query Is Nothing Then
                        list.Add(Payment)
                    End If
                End If

            Next
        Catch ex As Exception
            Throw New ApplicationException(ex.Message)
        End Try

        Return list
    End Function

    Private Function createDataUpload() As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("BranchId")
        dt.Columns.Add("UploadDate")
        dt.Columns.Add("uploadno")
        dt.Columns.Add("AgreementNo")
        dt.Columns.Add("strFileTemp")
        dt.Columns.Add("FileDirectory")
        dt.Columns.Add("NoRangka")
        dt.Columns.Add("NoMesin")
        dt.Columns.Add("NamaDiBPKB")
        dt.Columns.Add("TglTerimaBPKB")
        dt.Columns.Add("NoBPKB")
        dt.Columns.Add("NoPolisi")
        dt.Columns.Add("NoFaktur")
        dt.Columns.Add("Keterangan")
        dt.Columns.Add("Address")
        dt.Columns.Add("Nasabah")
        dt.Columns.Add("RAKId")
        dt.Columns.Add("FillingId")
        Return dt
    End Function

    Private Function FileToDataTable() As DataTable
        'Create a new DataTable.
        Dim dt As New DataTable()

        Using doc As SpreadsheetDocument = SpreadsheetDocument.Open(FileName, False)
            'Read the first Sheet from Excel file.
            Dim sheet As Sheet = doc.WorkbookPart.Workbook.Sheets.GetFirstChild(Of Sheet)()

            'Get the Worksheet instance.
            Dim worksheet As Worksheet = TryCast(doc.WorkbookPart.GetPartById(sheet.Id.Value), WorksheetPart).Worksheet

            'Fetch all the rows present in the Worksheet.
            Dim rows As IEnumerable(Of Row) = worksheet.GetFirstChild(Of SheetData)().Descendants(Of Row)()

            'Loop through the Worksheet rows.
            For Each row As Row In rows
                'Use the first row to add columns to DataTable.
                If row.RowIndex.Value = 1 Then
                    Dim clm As Integer = 0
                    For Each cell As Cell In row.Descendants(Of Cell)()
                        If GetValue(doc, cell).Trim <> "" Then
                            dt.Columns.Add(GetValue(doc, cell))
                        Else
                            dt.Columns.Add(GetValue(doc, cell).Trim & clm.ToString)
                        End If
                        clm += 1
                    Next
                Else
                    'Add rows to DataTable.
                    dt.Rows.Add()
                    Dim i As Integer = 0
                    For Each cell As Cell In row.Descendants(Of Cell)()
                        dt.Rows(dt.Rows.Count - 1)(i) = GetValue(doc, cell)
                        i += 1
                    Next
                End If
            Next
        End Using
        Return dt
    End Function

    Sub UploadBPKBChannelingByBatchSave(ByRef udtTable As DataTable)
        Try
            objCon = New SqlConnection(GetConnectionString)
            myCmd.Parameters.Clear()
            myCmd.CommandText = "SPUploadCollateralBPKB"
            myCmd.CommandType = CommandType.StoredProcedure
            myCmd.Parameters.Add(New SqlParameter("@udtTable", SqlDbType.Structured) With {.Value = udtTable})


            objCon.Open()
            myCmd.Connection = objCon
            myCmd.CommandTimeout = 0
            myCmd.ExecuteNonQuery()
            objCon.Close()
        Catch ex As Exception
            Throw New Exception(ex.Message)
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

#End Region

    Private Function GetValue(doc As SpreadsheetDocument, cell As Cell) As String
        Dim value As String = cell.CellValue.InnerText
        If cell.DataType IsNot Nothing AndAlso cell.DataType.Value = CellValues.SharedString Then
            Return doc.WorkbookPart.SharedStringTablePart.SharedStringTable.ChildElements.GetItem(Integer.Parse(value)).InnerText
        End If
        Return value
    End Function

    Private Function Text_To_DataTable(ByVal path As String, ByVal delimitter As Char, ByVal header As Boolean) As DataTable
    Dim source As String = String.Empty
    Dim dt As DataTable = New DataTable

    If IO.File.Exists(path) Then
        source = IO.File.ReadAllText(path)
    Else
        Throw New IO.FileNotFoundException("Could not find the file at " & path, path)
    End If

    Dim rows() As String = source.Split({Environment.NewLine}, StringSplitOptions.None)

    For i As Integer = 0 To rows(0).Split(delimitter).Length - 1
        Dim column As String = rows(0).Split(delimitter)(i)
        dt.Columns.Add(If(header, column, "column" & i + 1))
    Next

    For i As Integer = If(header, 1, 0) To rows.Length - 1
        Dim dr As DataRow = dt.NewRow

        For x As Integer = 0 To rows(i).Split(delimitter).Length - 1
            If x <= dt.Columns.Count - 1 Then
                dr(x) = rows(i).Split(delimitter)(x)
            Else
                Throw New Exception("The number of columns on row " & i + If(header, 0, 1) & " is greater than the amount of columns in the " & If(header, "header.", "first row."))
            End If
        Next

        dt.Rows.Add(dr)
    Next

    Return dt
End Function

End Class