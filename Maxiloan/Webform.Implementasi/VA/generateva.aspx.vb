﻿
Imports System.IO
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports System.Data.SqlClient

Public Class generateva
    Inherits WebBased
    Protected WithEvents txtPeriodeFrom As ucDateCE
    Protected WithEvents txtTglDenda As ucDateCE


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If

        If IsHoBranch = False Then
            NotAuthorized()
            Exit Sub
        End If

        FormID = "GAVA"
        lblMessage.Text = ""
        If Not IsPostBack Then
            If Not CheckForm(Loginid, FormID, AppId) Then Exit Sub
            Dim result = BitConverter.ToInt64(Guid.NewGuid.ToByteArray(), 8)
            txtPeriodeFrom.Text = BusinessDate.ToString("dd/MM/yyyy")
            txtTglDenda.Text = BusinessDate.AddDays(1).ToString("dd/MM/yyyy")

            pnlUpload.Visible = True
            pnlInfo.Visible = False
            pnlGenerate.Visible = False
            pnlNotReady.Visible = False

            getFiles()
        End If
        If Not (resultLogs Is Nothing) Then
            showPanel()
        End If
    End Sub
    Protected Sub NotAuthorized()
        Dim strHTTPServer As String
        Dim StrHTTPApp As String
        Dim strNameServer As String
        strHTTPServer = Request.ServerVariables("PATH_INFO")
        strNameServer = Request.ServerVariables("SERVER_NAME")
        StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
        Response.Redirect("http://" & strNameServer & "/" & StrHTTPApp & "/error_notauthorized.aspx")
    End Sub

    'Private cnn = "Server=118.97.60.93\Nasmoco,1845;Database=AFIRCFS;UID=secmgr.afi;Pwd=9B8955696N063760EO10RkM"
    Protected Sub btnFind_Click(sender As Object, e As EventArgs) Handles btnFind.Click
        If (txtPeriodeFrom.Text.Trim = "") Then
            Return
        End If
        lblMessage.Text = ""
        getFiles()
    End Sub
    Protected Sub btnGenerate_Click(sender As Object, e As EventArgs) Handles btnGenerate.Click
        Try
            If DateTime.Compare(ConvertDate2(txtPeriodeFrom.Text.Trim), ConvertDate2(txtTglDenda.Text.Trim)) > 0 Then
                ShowMessage(lblMessage, "Tanggal Denda Tidak boleh lebih besar.", True)
                Return
            End If
            Dim Controller = New ImplementasiVaController()
            Dim result = Controller.GeneratePaymentGatewayFile(GetConnectionString(), resultSettingLogs(0), pathFile, ConvertDate2(txtPeriodeFrom.Text.Trim))
            'Dim result = Controller.GeneratePaymentGatewayFile(cnn, ConvertDate2(txtPeriodeFrom.Text.Trim), pathFile)

            If (result) Then
                'getFiles() 
                resultLogs = Controller.GetVirtualAccountInsAgreementLog(GetConnectionString(), resultSettingLogs(0))
                pnlInfo.Visible = True
                showPanel()
                pnlGenerate.Visible = False
                Return
            End If

            ShowMessage(lblMessage, "Maaf, Telah terjadi kesalahan generate file.", True)
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

    Private Property resultLogs As VirtualAccountInsAgreementLog
        Get
            Return (CType(ViewState("VIRTUALACCOUNTINSAGREEMENTLOG"), VirtualAccountInsAgreementLog))
        End Get
        Set(ByVal Value As VirtualAccountInsAgreementLog)
            ViewState("VIRTUALACCOUNTINSAGREEMENTLOG") = Value
        End Set
    End Property

    Private Property resultSettingLogs As String()
        Get
            Return (CType(ViewState("resultSettingLogs "), String()))
        End Get
        Set(ByVal Value As String())
            ViewState("resultSettingLogs ") = Value
        End Set
    End Property
    Protected WithEvents link As LinkButton
    Sub getFiles()

        Dim Controller = New ImplementasiVaController()

        'resultLogs = Controller.GetVirtualAccountInsAgreementLog(GetConnectionString(), ConvertDate2(txtPeriodeFrom.Text.Trim))
        'resultLogs = Controller.GetVirtualAccountInsAgreementLog(cnn, ConvertDate2(txtPeriodeFrom.Text.Trim))

        resultSettingLogs = Controller.VaGenerateSettingLog(GetConnectionString(), New Date() {ConvertDate2(txtPeriodeFrom.Text.Trim), ConvertDate2(txtTglDenda.Text.Trim)})

        pnlInfo.Visible = False
        pnlGenerate.Visible = False
        pnlNotReady.Visible = False

        If resultSettingLogs Is Nothing Then
            pnlNotReady.Visible = True
            Return
        End If

        resultLogs = Controller.GetVirtualAccountInsAgreementLog(GetConnectionString(), resultSettingLogs(0))

        If resultLogs Is Nothing Then
            pnlGenerate.Visible = True
            Return
        End If

        pnlInfo.Visible = True
        showPanel()
    End Sub


    Sub showPanel()

        Dim FileDirectory = resultLogs.FileDirectory
        lblTglUpload.Text = resultLogs.DateOfUpload.ToString("dd/MM/yyyy")

        Dim oBankVaDataTable As DataTable = Me.GetBankVaCount(resultSettingLogs(0))
        'Dim installmentCount As Integer = resultLogs.InstallmentCount / Convert.ToInt32(oBankVaDataTable.Rows(0)(0))
        'Dim totalInstallment As Decimal = resultLogs.TotalInstallment / Convert.ToDecimal(oBankVaDataTable.Rows(0)(1))

        lblJumlahTagihan.Text = FormatNumber(Convert.ToString(oBankVaDataTable.Rows(0)(0)), 0)
        lblTotalTagihan.Text = FormatNumber(Convert.ToString(oBankVaDataTable.Rows(0)(1)), 0)

        pnlUserdata.Controls.Clear()
        For Each v In resultLogs.VaAgreementLogDetails
            Dim divcontrol = New HtmlGenericControl()
            divcontrol.Attributes("class") = "form_single"
            divcontrol.TagName = "div"
            pnlUserdata.Controls.Add(divcontrol)
            Dim info = New Label()
            info.Text = v.VaBankName
            divcontrol.Controls.Add(info)
            link = New LinkButton()
            link.ID = v.VaBankId
            link.Text = v.FileName
            link.CommandName = v.FileName
            link.CommandArgument = String.Format("{0}{1}", FileDirectory.Trim, v.FileName.Trim)
            AddHandler link.Command, AddressOf lnkClickMe_Click
            divcontrol.Controls.Add(link)
        Next

    End Sub



    Protected Sub lnkClickMe_Click(sender As Object, e As CommandEventArgs) Handles link.Command
        Try
            Dim fs As FileStream = Nothing
            fs = File.Open(e.CommandArgument, System.IO.FileMode.Open)

            Dim btFile(fs.Length - 1) As Byte
            fs.Read(btFile, 0, fs.Length)
            fs.Close()
            With Response
                .AddHeader("Content-disposition", "attachment;filename=" & e.CommandName)
                .ContentType = "application/octet-stream"
                .BinaryWrite(btFile)
                .End()
            End With
        Catch ex As Exception
            ' Throw New Exception()
        End Try
    End Sub

    Protected Function pathFile() As String
        Dim strDirectory As String = ""
        strDirectory = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\Implementasi\VA\"
        If Not Directory.Exists(strDirectory) Then
            Directory.CreateDirectory(strDirectory)
        End If
        Return strDirectory
    End Function

    Private Function GetBankVaCount(ByVal settingId As String) As DataTable
        Dim sqlText As String = "select COUNT(applicationid) AS TotalApplication,
		                                    SUM(Installment) + SUM(pinalty) AS TotalInstallment
                                    FROM PaymentAgreementAccountLog with (nolock)
                                    WHERE id = @SettingId"
        Dim oDataTable As New DataTable

        Dim constr As String = GetConnectionString()
        Using con As New SqlConnection(constr)
            Using cmd As New SqlCommand(sqlText)
                cmd.CommandType = CommandType.Text
                cmd.Connection = con
                cmd.Parameters.AddWithValue("@SettingId", settingId)
                con.Open()
                Using sdr As SqlDataReader = cmd.ExecuteReader()
                    oDataTable.Load(sdr)
                End Using
                con.Close()
            End Using
        End Using

        Return oDataTable

    End Function
End Class