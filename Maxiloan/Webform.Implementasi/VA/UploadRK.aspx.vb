﻿#Region "Imports"
Imports System.Data.SqlClient
Imports System.IO
Imports DocumentFormat.OpenXml.Packaging
Imports DocumentFormat.OpenXml.Spreadsheet

#End Region

Public Class UploadRK
    Inherits Maxiloan.Webform.WebBased

    Private myCmd As New SqlCommand
    Private myAdapter As New SqlDataAdapter
    Private myDS As New DataSet
    Private myReader As DataTableReader
    Private FileName As String
    Private strFileTemp As String
    Private strFile As String
    Private _KodePerusahaan As String
    Private _KodeSubCompany As String
    Private _NoPelanggan As String
    Private FileDirectory As String
    Private TotalAgreement As Double
    Private TotalInstallment As Decimal
    Private FileType As String
    Private _TglTransaksi As String
    Private _VirtualAccountID As String
    Private _BranchID As String
    Private _AgreementNo As String
    Private _JenisTransaksi As String
    Private objCon As SqlConnection
    Private TextFile As String

    Public Property FilaName() As String
        Get
            Return CType(ViewState("FilaName"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("FilaName") = Value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblLoading.Visible = False
        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not Me.IsPostBack Then
            If Not Me.IsHoBranch Then
                ShowMessage(lblMessage, "Harus Login Kantor Pusat", True)
                pnlHeader.Visible = False
                Exit Sub
            Else
                Me.FormID = "UploadRKVA"
                If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                    GetCombo()
                End If
            End If
        End If
    End Sub
    Private Sub btnUpload_Click(sender As Object, e As System.EventArgs) Handles btnUpload.Click
        lblLoading.Visible = True
        Try
            If FileVA.HasFile Then
                strFileTemp = FileVA.FileName
                FileName = Context.Request.MapPath("~\\xml\\") + strFileTemp
                File.Delete(FileName)
                FileVA.SaveAs(FileName)
                Dim larr As Array = CType(strFileTemp.ToString.Split(CChar(".")), Array)
                TextFile = strFileTemp
                FileDirectory = Context.Request.MapPath("~\\xml\\").ToString.Trim
                FileType = larr.GetValue(1).ToString.Trim()

                Me.FileName = FileName

                ProsesRK()
                lblLoading.Visible = False
            Else
                lblLoading.Visible = False
                ShowMessage(lblMessage, "File belum dipilih!", True)
                Exit Sub
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub
    Sub GetCombo()
        objCon = New SqlConnection(GetConnectionString)
        myDS = New DataSet
        myCmd.Parameters.Clear()
        myCmd.CommandText = "spGetComboVirtualAccount"
        myCmd.Connection = objCon
        myAdapter.SelectCommand = myCmd
        myAdapter.Fill(myDS)

        If myDS.Tables(0).Rows.Count > 0 Then
            cbVirtualAccount.DataTextField = "VirtualAccountName"
            cbVirtualAccount.DataValueField = "VirtualAccountID"
            cbVirtualAccount.DataSource = myDS.Tables(0)
            cbVirtualAccount.DataBind()
        End If
    End Sub
    Private Function Text_To_DataTable(ByVal path As String, ByVal delimitter As Char, ByVal header As Boolean) As DataTable
        Dim source As String = String.Empty
        Dim dt As DataTable = New DataTable

        If IO.File.Exists(path) Then
            source = IO.File.ReadAllText(path)
        Else
            Throw New IO.FileNotFoundException("Could not find the file at " & path, path)
        End If

        Dim rows() As String = source.Split({Environment.NewLine}, StringSplitOptions.None)

        For i As Integer = 0 To rows(0).Split(delimitter).Length - 1
            Dim column As String = rows(0).Split(delimitter)(i)
            dt.Columns.Add(If(header, column, "column" & i + 1))
        Next

        For i As Integer = If(header, 1, 0) To rows.Length - 1
            Dim dr As DataRow = dt.NewRow

            For x As Integer = 0 To rows(i).Split(delimitter).Length - 1
                If x <= dt.Columns.Count - 1 Then
                    dr(x) = rows(i).Split(delimitter)(x)
                Else
                    Throw New Exception("The number of columns on row " & i + If(header, 0, 1) & " is greater than the amount of columns in the " & If(header, "header.", "first row."))
                End If
            Next

            dt.Rows.Add(dr)
        Next

        Return dt
    End Function

    Function UploadFileRK(ByVal VirtualAccountID As String) As List(Of Parameter.VirtualAccountRK)
        Dim list As New List(Of Parameter.VirtualAccountRK)
        Dim CountData As Integer
        Dim data As New DataTable
        Dim TglTransaksi As Date = Nothing

        If VirtualAccountID = "BCA" Then
            data = FileToDataTable()
            CountData = data.Rows.Count
            For index = 0 To data.Rows.Count - 1
                'If (Not index = 0) And (Not index = 1) And (Not index = 3) And (Not index = 4) And (Not index = CountData) And (Not index = CountData - 1) And (Not index = CountData - 2) And (Not index = CountData - 3) Then
                Dim RK As New Parameter.VirtualAccountRK

                If index = 2 Then
                    TglTransaksi = ConvertDate2(IIf(CStr(data.Rows(index).ItemArray(0)).Substring(10, 10).Trim = "", "01/01/1900", CStr(data.Rows(index).ItemArray(0)).Substring(10, 10)))
                End If

                If index > 4 And index < (CountData - 3) Then
                    If (Len(CStr(data.Rows(index).ItemArray(1)).Trim) = 78 Or Len(CStr(data.Rows(index).ItemArray(1)).Trim) = 76) Then
                        RK.VirtualAccountID = VirtualAccountID
                        RK.TglTransaksi = TglTransaksi

                        'RK.BranchId = CStr(data.Rows(index).ItemArray(1)).Substring(66, 3).Trim
                        'RK.AgreementNo = CStr(data.Rows(index).ItemArray(1)).Substring(66, 12).Trim                        

                        RK.BranchId = "-"
                        RK.AgreementNo = Right(data.Rows(index).ItemArray(1).trim, 12).Trim

                        RK.NilaiTransaksi = CDec(CStr(data.Rows(index).ItemArray(3)).Replace("CR", "").Replace("DB", ""))
                        RK.JenisTransaksi = IIf(CStr(data.Rows(index).ItemArray(3)).Contains("CR"), "Kredit", "Debet").ToString.Trim

                        RK.Keterangan = CStr(data.Rows(index).ItemArray(1))

                        Me._TglTransaksi = RK.TglTransaksi
                        Me._VirtualAccountID = RK.VirtualAccountID
                        Me._BranchID = RK.BranchId
                        Me._AgreementNo = RK.AgreementNo

                        Dim query As New Parameter.VirtualAccountRK
                        If list.Count > 0 Then
                            query = list.Find(AddressOf PredicateFunctionRK)
                        Else
                            query = Nothing
                        End If

                        If query Is Nothing Then
                            list.Add(RK)
                        End If
                    End If
                End If
            Next
        End If

        If VirtualAccountID = "BRI" Then

            data = CSVToDataTable(Me.FileName)
            CountData = data.Rows.Count

            For index = 0 To data.Rows.Count - 1
                Dim RK As New Parameter.VirtualAccountRK

                Dim arr As String() = data.Rows(index).Item(0).ToString().Split(New Char() {""""c})
                'Dim parts As String() = arr(0).ToString().Split(New Char() {","c})

                If (arr(0).Contains("TRF BERSAMA")) Then
                    RK.VirtualAccountID = VirtualAccountID
                    RK.TglTransaksi = ConvertDate2(arr(0).Substring(35, 8).Trim)
                    RK.BranchId = "-"
                    RK.AgreementNo = arr(0).Substring(53, 12).Trim
                    RK.JenisTransaksi = "Kredit"
                    RK.NilaiTransaksi = Convert.ToDecimal(arr(1).Trim)
                    RK.Keterangan = "TRF BERSAMA"
                ElseIf (arr(0).Contains("TRF PRIMA")) Then
                    RK.VirtualAccountID = VirtualAccountID
                    RK.TglTransaksi = ConvertDate2(arr(0).Substring(35, 8).Trim)
                    RK.BranchId = "-"
                    RK.AgreementNo = arr(0).Substring(72, 12).Trim
                    RK.JenisTransaksi = "Kredit"
                    RK.NilaiTransaksi = Convert.ToDecimal(arr(1).Trim)
                    RK.Keterangan = "TRF PRIMA"
                Else
                    RK.VirtualAccountID = VirtualAccountID
                    RK.TglTransaksi = ConvertDate2(arr(0).Substring(35, 8).Trim)
                    RK.BranchId = "-"
                    RK.AgreementNo = arr(0).Substring(72, 12).Trim
                    RK.JenisTransaksi = "Kredit"
                    RK.NilaiTransaksi = Convert.ToDecimal(arr(1).Trim)
                    RK.Keterangan = "ATMLTRBCA"
                End If

                Me._TglTransaksi = RK.TglTransaksi
                Me._VirtualAccountID = RK.VirtualAccountID
                Me._BranchID = RK.BranchId
                Me._AgreementNo = RK.AgreementNo
                Me._JenisTransaksi = RK.JenisTransaksi

                Dim query As New Parameter.VirtualAccountRK
                If list.Count > 0 Then
                    query = list.Find(AddressOf PredicateFunctionRK)
                Else
                    query = Nothing
                End If

                If query Is Nothing Then
                    list.Add(RK)
                End If

            Next
        End If

        Return list
    End Function
    Private Sub ProsesRK()
        Dim VirtualAccountID As String = cbVirtualAccount.Items(cbVirtualAccount.SelectedIndex).Value.Trim
        Dim list As New List(Of Parameter.VirtualAccountRK)
        Try
            Dim seq = SaveLog(VirtualAccountID)
            list = UploadFileRK(VirtualAccountID)

            Dim duplicates = list.GroupBy(Function(x) x.AgreementNo).Where(Function(x) x.Count > 1).Select(Function(x) x)
            If (duplicates.Count > 0) Then
                Dim sb As New StringBuilder
                For Each value In duplicates
                    sb.AppendLine(String.Format("Ada Nomor Kontrak {0} Yang Sama Pada File Yang Akan Di Upload <br>", value(0).AgreementNo))
                Next
                ShowMessage(lblMessage, sb.ToString(), True)
                Exit Sub
            End If

            If list.Count > 0 Then
                For index = 0 To list.Count - 1
                    Dim RK As New Parameter.VirtualAccountRK
                    RK = list(index)
                    RK.SequenceNo = seq
                    SaveRK(RK)
                Next
            End If
            ShowMessage(lblMessage, "Upload Data RK Berhasil", False)
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub
    Public Function PredicateFunctionRK(ByVal custom As Parameter.VirtualAccountRK) As Boolean
        Return custom.TglTransaksi = Me._TglTransaksi And custom.VirtualAccountID = Me._VirtualAccountID And custom.BranchId = Me._BranchID And custom.AgreementNo = Me._AgreementNo And custom.JenisTransaksi = Me._JenisTransaksi
    End Function
    Sub SaveRK(ByVal RK As Parameter.VirtualAccountRK)
        Try
            objCon = New SqlConnection(GetConnectionString)
            myCmd.Parameters.Clear()
            myCmd.CommandText = "spVirtualAccountRKSave"
            myCmd.CommandType = CommandType.StoredProcedure
            myCmd.Parameters.Add(New SqlParameter("@TglTransaksi", SqlDbType.Date) With {.Value = RK.TglTransaksi})
            myCmd.Parameters.Add(New SqlParameter("@VirtualAccountID", SqlDbType.Char, 20) With {.Value = RK.VirtualAccountID})
            myCmd.Parameters.Add(New SqlParameter("@BranchID", SqlDbType.Char, 3) With {.Value = RK.BranchId})
            myCmd.Parameters.Add(New SqlParameter("@AgreementNo", SqlDbType.Char, 20) With {.Value = RK.AgreementNo})
            myCmd.Parameters.Add(New SqlParameter("@JenisTransaksi", SqlDbType.VarChar, 10) With {.Value = RK.JenisTransaksi})
            myCmd.Parameters.Add(New SqlParameter("@NilaiTransaksi", SqlDbType.Decimal) With {.Value = RK.NilaiTransaksi})
            myCmd.Parameters.Add(New SqlParameter("@Keterangan", SqlDbType.VarChar, 200) With {.Value = RK.Keterangan})

            myCmd.Parameters.Add(New SqlParameter("@VaRkLogId", SqlDbType.SmallInt) With {.Value = RK.SequenceNo})

            objCon.Open()
            myCmd.Connection = objCon
            myCmd.ExecuteNonQuery()
            objCon.Close()
        Catch ex As Exception
            Throw New Exception(ex.Message)
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub
    Private Sub btnReset_Click(sender As Object, e As System.EventArgs) Handles btnReset.Click
        Response.Redirect("UploadRK.aspx")
    End Sub
    Private Function GetValue(doc As SpreadsheetDocument, cell As Cell) As String
        Dim value As String = cell.CellValue.InnerText
        If cell.DataType IsNot Nothing AndAlso cell.DataType.Value = CellValues.SharedString Then
            Return doc.WorkbookPart.SharedStringTablePart.SharedStringTable.ChildElements.GetItem(Integer.Parse(value)).InnerText
        End If
        Return value
    End Function
    Private Function FileToDataTable() As DataTable
        'Create a new DataTable.
        Dim dt As New DataTable()

        Using doc As SpreadsheetDocument = SpreadsheetDocument.Open(FileName, False)
            'Read the first Sheet from Excel file.
            Dim sheet As Sheet = doc.WorkbookPart.Workbook.Sheets.GetFirstChild(Of Sheet)()

            'Get the Worksheet instance.
            Dim worksheet As Worksheet = TryCast(doc.WorkbookPart.GetPartById(sheet.Id.Value), WorksheetPart).Worksheet

            'Fetch all the rows present in the Worksheet.
            Dim rows As IEnumerable(Of Row) = worksheet.GetFirstChild(Of SheetData)().Descendants(Of Row)()

            'Loop through the Worksheet rows.
            For Each row As Row In rows
                'Use the first row to add columns to DataTable.
                If row.RowIndex.Value = 1 Then
                    Dim clm As Integer = 0
                    For Each cell As Cell In row.Descendants(Of Cell)()
                        If GetValue(doc, cell).Trim <> "" Then
                            dt.Columns.Add(GetValue(doc, cell))
                        Else
                            dt.Columns.Add(GetValue(doc, cell).Trim & clm.ToString)
                        End If
                        clm += 1
                    Next
                Else
                    'Add rows to DataTable.
                    dt.Rows.Add()
                    Dim i As Integer = 0
                    For Each cell As Cell In row.Descendants(Of Cell)()
                        dt.Rows(dt.Rows.Count - 1)(i) = GetValue(doc, cell)
                        i += 1
                    Next
                End If
            Next
        End Using
        Return dt
    End Function

    Private Function CSVToDataTable(ByVal path As String) As DataTable

        Dim lines = IO.File.ReadAllLines(path)
        Dim tbl As New DataTable
        tbl.Columns.Add(New DataColumn("ColumnA", GetType(String)))

        For Each line In lines
            If (line.Length > 0) Then
                If (line.Substring(0, 4).Equals("TIME")) Then
                    Dim oRow As DataRow
                    oRow = tbl.NewRow
                    tbl.Rows.Add(line)
                End If
            End If
        Next

        Return tbl
    End Function

    Function SaveLog(ByVal VirtualAccountID As String) As Integer

        Try
            objCon = New SqlConnection(GetConnectionString)
            myCmd.Parameters.Clear()
            myCmd.CommandText = "spVirtualAccountRkLogSave"
            myCmd.CommandType = CommandType.StoredProcedure
            myCmd.Parameters.Add(New SqlParameter("@UploadDate", SqlDbType.DateTime) With {.Value = BusinessDate})
            myCmd.Parameters.Add(New SqlParameter("@VirtualAccountID", SqlDbType.VarChar, 10) With {.Value = VirtualAccountID})
            myCmd.Parameters.Add(New SqlParameter("@FileName", SqlDbType.VarChar, 200) With {.Value = strFileTemp})
            myCmd.Parameters.Add(New SqlParameter("@FileDirectory", SqlDbType.VarChar, 8000) With {.Value = FileDirectory})
            Dim prmErr = New SqlParameter("@SequenceNo", SqlDbType.SmallInt) With {.Direction = ParameterDirection.Output}
            myCmd.Parameters.Add(prmErr)
            objCon.Open()
            myCmd.Connection = objCon
            myCmd.CommandTimeout = 0
            myCmd.ExecuteNonQuery()
            objCon.Close()
            Return CDbl(prmErr.Value)
        Catch ex As Exception
            Throw New Exception(ex.Message)
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Function

End Class