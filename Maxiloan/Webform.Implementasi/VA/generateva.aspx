﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="generateva.aspx.vb" Inherits="Maxiloan.Webform.Implementasi.generateva" %>

 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="../../webform.UserController/ucDateCE.ascx" TagName="ucDateCE" TagPrefix="uc1" %>
<%@ Register Src="../../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %>  
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
      <title></title>
     <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <script  type="text/javascript" src="../../js/jquery-1.9.1.min.js"></script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
        <style> .lblNum { float: right; padding-right: 290px; } </style>
         
</head>
<body>
    <form id="form1" runat="server">
    <script type="text/JavaScript">
        function doDownload(a, e) {
            alert(a);
        }

         </script>
    <asp:ScriptManager runat="server" ID="ScriptManager1" />
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>GENERATE ANGSURAN VIRTUAL ACCOUNT</h3>
        </div>
    </div>

     <asp:Panel ID="pnlUpload" runat="server">
            <div class="form_box">
                <div class="form_single">
                <label>Tanggal Upload</label>
                <uc1:ucDateCE ID="txtPeriodeFrom" runat="server" />             
            </div></div>
     
            <div class="form_box">
                <div class="form_single">
                <label>Tanggal Denda</label>
                <uc1:ucDateCE ID="txtTglDenda" runat="server" />             
            </div></div>

            <div class="form_button">
                <asp:Button ID="btnFind" runat="server" Text="Search"  CssClass="small button green" />
                <asp:Button ID="btnCancelC" runat="server" Text="Cancel" CssClass="small button gray"  CausesValidation="False" />
            </div>
    </asp:Panel>

         <asp:Panel ID="pnlNotReady" runat="server">
           
                <div class="form_single">
                    <h3>Data belum tersedia</h3>
            </div> 
        </asp:Panel>

      <asp:Panel ID="pnlGenerate" runat="server">
            <div class="form_single">
                    <h3>File belum tersedia</h3>
            </div>
            <div class="form_button">
                <asp:Button ID="btnGenerate" runat="server" Text="Create file"  CssClass="small button green" /> 
            </div>
        </asp:Panel>

     <asp:Panel ID="pnlInfo" runat="server">
     
    <div class="form_title">
          <%--<div class="title_strip"> </div>--%>
        <div class="form_single"> <h4>File Tagihan Agreement Virtual Account</h4> </div>
    </div>
        

        
       <div class="form_box">
            <div class="form_single">
            <label>Tanggal Upload</label>
            <asp:Label ID="lblTglUpload" runat="server" />
        </div> </div>

      
        <div class="form_box">
             <div class="form_single">
            <label>Jumlah Tagihan</label>
            <asp:Label ID="lblJumlahTagihan" runat="server" />
        </div></div>

        <div class="form_box">
             <div class="form_single">
            <label>Total Tagihan</label>
            
            <asp:Label ID="lblTotalTagihan" runat="server" />
        </div></div>

          
           <div class="form_box"> 
        <asp:Panel runat="server" CssClass="sxpnl" ID="pnlUserdata" >
         </asp:Panel> </div>
        
         
     </asp:Panel>
    </form>
</body>
</html>
