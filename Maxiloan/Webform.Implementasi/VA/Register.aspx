﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Register.aspx.vb" Inherits="Maxiloan.Webform.Implementasi.Register" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register TagPrefix="uc1" TagName="UcBranchAll" Src="../../Webform.UserController/UcBranchAll.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../../Webform.UserController/UcSearchBy.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucdatece" Src="../../Webform.UserController/ucdatece.ascx" %>
<%@ Register Src="../../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Register Virtual Account</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../js/jquery-1.9.1.min.js"></script>
    <script src="../../js/jquery-ui-1.10.3.custom.min.js"></script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $('#btnRegister').click(function () {

                if ($('input:checkbox[id^="DtgAsset_chkUpdateStatus_"]:checked').length <= 0) {
                    alert("Silahkan pilih data terlebih dahulu");
                    return false;
                }
                return true;
            });
        });
    </script>
</head>
<body>

    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <asp:Label ID="lblMessage" Visible="false" runat="server" />
        <div class="form_title">
            <div class="form_single">
                <h4>CARI KOTRAK</h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">Cabang</label>
                <asp:DropDownList ID="cbobranch" runat="server">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="rfvcbobranchid" runat="server" InitialValue="0" ErrorMessage="Harap Pilih Cabang"
                    ControlToValidate="cbobranch" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>

        <div class="form_box_uc">
            <uc1:UcSearchBy ID="oSearBy" runat="server"></uc1:UcSearchBy>
        </div>

        <div class="form_box">
            <div class="form_single">
                <label class="label_general">Status Virtual Account</label>
                <asp:RadioButtonList ID="rboStatusVa" runat="server" RepeatDirection="Horizontal" CssClass="opt_single">
                    <asp:ListItem Selected="True" Value="False">Belum Ada</asp:ListItem>
                    <asp:ListItem Value="True">Sudah Ada</asp:ListItem>
                </asp:RadioButtonList>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>Tanggal kontrak dari</label>
                <uc1:ucdatece ID="tglkontrak" runat="server" />&nbsp; S/D
                <uc1:ucdatece ID="tglkontrakTo" runat="server" />
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="small button blue" CausesValidation="False" />
            <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="small button gray" CausesValidation="False" />
        </div>



        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>DAFTAR KONTRAK</h3>
            </div>
        </div>
        <asp:Panel ID="pnlDatagrid" runat="server">
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ns">
                        <asp:GridView ID="DtgAsset" runat="server" AllowSorting="True" AutoGenerateColumns="False" 
                            OnSortCommand="SortGrid" DataKeyField="AgreementNo" CssClass="grid_general" >
                            <HeaderStyle CssClass="th" />
                            <FooterStyle CssClass="item_grid" />
                            <RowStyle CssClass="item_grid" />
                            
                            <Columns>
                                <asp:TemplateField HeaderText="PILIH" >
                                    <HeaderStyle CssClass="th" />
                                    <ItemTemplate>
                                        <asp:CheckBox runat="server" ID="chkUpdateStatus" class="checkBoxClass" ToolTip="Select this contract " />
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField SortExpression="ApplicationId" HeaderText="NO KONTRAK" Visible="false">
                                    <HeaderStyle CssClass="th" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblApplicationId" runat="server" Text='<%#Container.DataItem("ApplicationId")%>' />
                                        <asp:Label ID="lblCustomerId" runat="server" Text='<%#Container.DataItem("Customerid")%>' />
                                        <asp:Label ID="lblBranchId" runat="server" Text='<%#Container.DataItem("BranchId")%>' />
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField SortExpression="AgreementNo" HeaderText="NO KONTRAK">
                                    <HeaderStyle CssClass="th" />
                                    <ItemTemplate>
                                        <itemstyle cssclass="item_grid" />
                                        <asp:HyperLink ID="hyAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>' />
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:BoundField DataField="NoVirtualAcc" HeaderText="NO VIRTUAL ACCOUNT" ><HeaderStyle CssClass="th" /></asp:BoundField>
                                <asp:BoundField DataField="VaBankInitial" HeaderText="BANK" ><HeaderStyle CssClass="th" /></asp:BoundField>
                                <asp:BoundField DataField="CustomerName" HeaderText="NAMA CUSTOMER" ><HeaderStyle CssClass="th" /></asp:BoundField>
                                <asp:BoundField SortExpression="AssetDesc" DataField="AssetDesc" HeaderText="NAMA ASSET" ><HeaderStyle CssClass="th" /></asp:BoundField>
                                <asp:BoundField SortExpression="licenseplate" DataField="licenseplate" HeaderText="NO POLISI" ><HeaderStyle CssClass="th" /></asp:BoundField>
                                <asp:BoundField SortExpression="InstallmentAmount" DataField="InstallmentAmount" HeaderText="ANGSURAN" DataFormatString="{0:#,0}"><HeaderStyle CssClass="th" /></asp:BoundField>
                                <asp:BoundField SortExpression="ContractStatus" DataField="ContractStatus" HeaderText="STS" ><HeaderStyle CssClass="th" /></asp:BoundField>
                                    
                            </Columns>
                                
                        </asp:GridView>
                        <uc2:ucGridNav ID="GridNavigator" runat="server" />

                    </div>
                </div>
            </div>
        </asp:Panel>



        <asp:Panel ID="pnlList" runat="server" Visible="false">            
            <div class="form_box" style="display: none;">
                <div class="form_single">
                    <label class="label_req">Rekening Bank</label>
                    <asp:DropDownList ID="ddlRekening" runat="server"></asp:DropDownList>
                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" InitialValue="0" ErrorMessage="Harap Pilih Rekening Bank" ControlToValidate="ddlRekening" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                </div>
            </div>
            <div class="form_box_header" style="display: none;">
                <div class="form_single">
                    <div class="grid_wrapper_ns">
                        <asp:DataGrid ID="dtgBankName" runat="server" AutoGenerateColumns="False"
                            BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <Columns>
                                <asp:TemplateColumn HeaderText="PILIH BANK VA">
                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chk" runat="server" Checked="true"></asp:CheckBox>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="NO.REF" Visible="false">
                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblBankId" runat="server" Enabled="True" Text='<%# Container.DataItem("ID") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>                      
                                <asp:BoundColumn DataField="ID" HeaderText ="KODE BANK"></asp:BoundColumn>                            
                                <asp:BoundColumn DataField="Name" HeaderText ="NAMA BANK"></asp:BoundColumn>                  
                            </Columns>
                        </asp:DataGrid>
                    </div>
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="btnRegister" runat="server" Text="Register" CssClass="small button blue"></asp:Button>
                <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray"></asp:Button>
            </div>
        </asp:Panel>

    </form>
</body>
</html>
