﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Otorisasi.aspx.vb" Inherits="Maxiloan.Webform.Implementasi.Otorisasi" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <style type="text/css">
        .progress_imgLoading
        {                        
            width : 4%; 
            display: block;
            padding-top: 10px;
            padding-bottom: 20px;
            padding-left: 70px;            
            background-image: url('../../Images/pic-loaderblack.gif');
            background-repeat: no-repeat;
            background-position:right;
            display: block;            
        }
    </style>
</head>
<body>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
                onclick="hideMessage();"></asp:Label>
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>
                        OTORISASI VIRTUAL ACCOUNT CABANG                  
                    </h3>                    
                </div>
            </div>
            <asp:Panel runat="server" ID="pnlHeader">
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Jenis Virtual Account</label>
                        <asp:DropDownList ID="cbVirtualAccount" runat="server">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" Display="Dynamic"
                            InitialValue="Selected One" ControlToValidate="cbVirtualAccount" ErrorMessage="Harap pilih jenis"
                            CssClass="validator_general"></asp:RequiredFieldValidator>  
                    </div>
                </div>  
                <div class="form_button">
                    <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="small button blue"></asp:Button>                    
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlDatagrid" runat="server" Visible="false">                
                <div class="form_box">        
                    <div class="form_single">
                        <label>
                            Cari Berdasarkan</label>
                        <asp:DropDownList ID="cboSearchBy" runat="server">
                            <%--<asp:ListItem Value="">Select One</asp:ListItem>--%>
                            <asp:ListItem Value="Agreement.AgreementNo" Selected="True">No Kontrak</asp:ListItem>
                            <%--<asp:ListItem Value="Customer.Name">Nama Customer</asp:ListItem>--%>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="Dynamic"
                            InitialValue="" ControlToValidate="cboSearchBy" ErrorMessage="Harap pilih"
                            CssClass="validator_general"></asp:RequiredFieldValidator>  
                        <asp:TextBox ID="txtSearchBy" runat="server" Width="260px"></asp:TextBox>
                       <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Display="Dynamic"
                            InitialValue="" ControlToValidate="txtSearchBy" ErrorMessage="Harap isi"
                            CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                    </div>                    
                </div> 
                <div class="form_button">
                    <asp:Button ID="btnCari" runat="server" Text="Cari" CssClass="small button blue"></asp:Button>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ws">
                            <asp:DataGrid ID="DtgAgree" runat="server" AutoGenerateColumns="False" BorderWidth="0"
                                OnSortCommand="SortGrid" BorderStyle="None" CssClass="grid_general" DataKeyField="ApplicationID"
                                AllowSorting="True">
                                <ItemStyle CssClass="item_grid"></ItemStyle>
                                <HeaderStyle CssClass="th"></HeaderStyle>
                                <Columns>
                                    <asp:TemplateColumn>
                                        <HeaderStyle Width="3%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <%--<HeaderTemplate>
                                            <asp:CheckBox AutoPostBack="True" ID="chkAll" runat="server" OnCheckedChanged="SelectAll">
                                            </asp:CheckBox>
                                        </HeaderTemplate>--%>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkItem" runat="server"></asp:CheckBox>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>    
                                    <asp:BoundColumn DataField="UploadDate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="TGL UPLOAD"
                                        SortExpression="UploadDate"></asp:BoundColumn>                                                                        
                                    <asp:TemplateColumn SortExpression="VirtualAccountID" HeaderText="VA ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblVirtualAccountID" runat="server" Text='<%#Container.DataItem("VirtualAccountID")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="SequenceNo" Visible="false" HeaderText="SEQUENCE NO">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSequenceNo" runat="server" Text='<%#Container.DataItem("SequenceNo")%>'></asp:Label>
                                            <asp:Label ID="lblUploadDate" runat="server" Visible="false" Text='<%#Container.DataItem("UploadDate")%>'></asp:Label>
                                            <asp:Label ID="lblValueDate" runat="server" Visible="false" Text='<%#Container.DataItem("ValueDate")%>'></asp:Label>
                                            <asp:Label ID="lblBranchID" runat="server" Visible="false" Text='<%#Container.DataItem("BranchID")%>'></asp:Label>
                                            <asp:Label ID="lblApplicationID" runat="server" Visible="false" Text='<%#Container.DataItem("ApplicationID")%>'></asp:Label>
                                            <asp:Label ID="lblBankAccountID" runat="server" Visible="false" Text='<%#Container.DataItem("BankAccountID")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>        
                                    <asp:BoundColumn DataField="Name" HeaderText="NAMA"
                                        SortExpression="Name"></asp:BoundColumn> 
                                    <asp:TemplateColumn SortExpression="NoPelanggan" HeaderText="VA NO">
                                        <ItemTemplate> 
                                            <asp:Label ID="lblNoPelanggan" runat="server" Text='<%#Container.DataItem("NoPelanggan")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>                              
                                    <asp:TemplateColumn SortExpression="NilaiTransaksi" HeaderText="NILAI TRANSAKSI">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNilaiTransaksi" runat="server" Text='<%#formatnumber(Container.DataItem("NilaiTransaksi"),0)%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>          
                                    <asp:BoundColumn DataField="ValueDate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="TGL RK"
                                        SortExpression="ValueDate"></asp:BoundColumn>   
                                    <asp:BoundColumn DataField="NextInstallmentNumber" HeaderText="ANGSKE"
                                        SortExpression="NextInstallmentNumber"></asp:BoundColumn> 
                                    <asp:BoundColumn DataField="NextInstallmentDate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="TGL JATUH TEMPO"
                                        SortExpression="NextInstallmentDate"></asp:BoundColumn>                                                                                                 
                                </Columns>
                            </asp:DataGrid>
                             <div class="form_box_hide">
                            <div class="button_gridnavigation">
                                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                                    CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="First">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                                    CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Prev">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                                    CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Next">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                                    CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Last">
                                </asp:ImageButton>
                                Page&nbsp;
                                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                                <asp:Button ID="imgbtnPageNumb" runat="server" CssClass="small buttongo blue" Text="Go"
                                    EnableViewState="False"></asp:Button>
                                <asp:RangeValidator ID="rgvGo" runat="server" ErrorMessage="No Halaman Salah" ControlToValidate="txtPage"
                                    Type="Integer" MaximumValue="999999999" MinimumValue="1" CssClass="validator_general"
                                    Display="Dynamic"></asp:RangeValidator>
                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" Display="Dynamic"
                                    ErrorMessage="No Halaman Salah" ControlToValidate="txtPage" CssClass="validator_general"></asp:RequiredFieldValidator>
                            </div>
                         
                               <asp:Label ID="lblPage" runat="server"></asp:Label>of 
                                <asp:Label ID="lblTotPage" runat="server"></asp:Label> </div>
                              <div class="label_gridnavigation">Total
                                <asp:Label ID="lblrecord" runat="server"></asp:Label> record(s)
                            </div>                            
                        </div>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue"></asp:Button>
                    <asp:Button ID="btnReset" runat="server" Text="Reset" CausesValidation="false" CssClass="small button blue"></asp:Button>
                </div>
                 <div class="form_title">
                    <div class="form_single">
                        <h4>
                            DAFTAR REKENING KORAN
                        </h4>
                    </div>
                </div>
                <%--<div class="form_box">        
                    <div class="form_left">
                        <label>
                            Cari Berdasarkan</label>
                        <asp:DropDownList ID="cboSearchByRK" runat="server">
                            <asp:ListItem Value="">Select One</asp:ListItem>
                            <asp:ListItem Value="Agreement.AgreementNo">No Kontrak</asp:ListItem>
                            <asp:ListItem Value="Customer.Name">Nama Customer</asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtSearchByRK" runat="server" Width="260px"></asp:TextBox>
                    </div>
                    <div class="form_right">
                        <asp:Button ID="btnCariRK" runat="server" Text="Cari" CssClass="small button blue"></asp:Button>
                    </div>
                </div>--%>                
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ws">
                            <asp:DataGrid ID="DtgAgreeRK" runat="server" AutoGenerateColumns="False" BorderWidth="0"
                                OnSortCommand="SortGrid" BorderStyle="None" CssClass="grid_general" DataKeyField="ApplicationID"
                                AllowSorting="True">
                                <ItemStyle CssClass="item_grid"></ItemStyle>
                                <HeaderStyle CssClass="th"></HeaderStyle>
                                <Columns>                                        
                                    <asp:BoundColumn DataField="TglTransaksi" DataFormatString="{0:dd/MM/yyyy}" HeaderText="TGL RK"
                                        SortExpression="TglTransaksi"></asp:BoundColumn>                                                                        
                                    <asp:TemplateColumn SortExpression="VirtualAccountID" HeaderText="VA ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblVirtualAccountID" runat="server" Text='<%#Container.DataItem("VirtualAccountID")%>'></asp:Label>
                                            <asp:Label ID="lblBranchID" runat="server" Visible="false" Text='<%#Container.DataItem("BranchID")%>'></asp:Label>
                                            <asp:Label ID="lblApplicationID" runat="server" Visible="false" Text='<%#Container.DataItem("ApplicationID")%>'></asp:Label>                                            
                                        </ItemTemplate>
                                    </asp:TemplateColumn>                                           
                                    <asp:BoundColumn DataField="Name" HeaderText="NAMA"
                                        SortExpression="Name"></asp:BoundColumn> 
                                    <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="AGREEMENT NO">
                                        <ItemTemplate> 
                                            <asp:Label ID="lblAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>                              
                                    <asp:TemplateColumn SortExpression="NilaiTransaksi" HeaderText="NILAI TRANSAKSI">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNilaiTransaksi" runat="server" Text='<%#formatnumber(Container.DataItem("NilaiTransaksi"),0)%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>     
                                    <asp:TemplateColumn SortExpression="JenisTransaksi" HeaderText="JENIS TRANSAKSI">
                                        <ItemTemplate> 
                                            <asp:Label ID="lblJenisTransaksi" runat="server" Text='<%#Container.DataItem("JenisTransaksi")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>    
                                    <asp:TemplateColumn SortExpression="Keterangan" HeaderText="KETERANGAN">
                                        <ItemTemplate> 
                                            <asp:Label ID="lblKeterangan" runat="server" Text='<%#Container.DataItem("Keterangan")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>                                                                                                                                   
                                </Columns>
                            </asp:DataGrid>


                             <div class="form_box_hide"> 
                            <div class="button_gridnavigation">
                                <asp:ImageButton ID="imbFirstPageRK" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                                    CausesValidation="False" OnCommand="NavigationLinkRK_Click" CommandName="First">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbPrevPageRK" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                                    CausesValidation="False" OnCommand="NavigationLinkRK_Click" CommandName="Prev">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbNextPageRK" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                                    CausesValidation="False" OnCommand="NavigationLinkRK_Click" CommandName="Next">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbLastPageRK" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                                    CausesValidation="False" OnCommand="NavigationLinkRK_Click" CommandName="Last">
                                </asp:ImageButton>
                                Page&nbsp;
                                <asp:TextBox ID="txtPageRK" runat="server">1</asp:TextBox>
                                <asp:Button ID="imgbtnPageNumbRK" runat="server" CssClass="small buttongo blue" Text="Go"
                                    EnableViewState="False"></asp:Button>
                                <asp:RangeValidator ID="rgvGoRK" runat="server" ErrorMessage="No Halaman Salah" ControlToValidate="txtPageRK"
                                    Type="Integer" MaximumValue="999999999" MinimumValue="1" CssClass="validator_general"
                                    Display="Dynamic"></asp:RangeValidator>
                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator1RK" runat="server" Display="Dynamic"
                                    ErrorMessage="No Halaman Salah" ControlToValidate="txtPageRK" CssClass="validator_general"></asp:RequiredFieldValidator>
                            </div> 
                          <asp:Label ID="lblPageRK" runat="server"></asp:Label>of
                                <asp:Label ID="lblTotPageRK" runat="server"></asp:Label> </div>
                                 <div class="label_gridnavigation">
                                 Total
                                <asp:Label ID="lblrecordRK" runat="server"></asp:Label> record(s)
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>                         
        </ContentTemplate>
    </asp:UpdatePanel>    
    </form>
</body>
</html>
