﻿#Region "Imports"
Imports Maxiloan.Controller
Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.Webform.UserController
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data.OleDb
Imports System.Data.Odbc
Imports System.Text
Imports System.Data.SqlClient
Imports System.IO
Imports System.Web.UI.WebControls.FileUpload
Imports System.Text.RegularExpressions
Imports System.Diagnostics
Imports System.Drawing
Imports System.Runtime.InteropServices
Imports Maxiloan.Parameter
Imports Maxiloan.Framework.SQLEngine
#End Region

Public Class Otorisasi
    Inherits Maxiloan.Webform.WebBased

    Private myCmd As New SqlCommand
    Private myAdapter As New SqlDataAdapter
    Private myDS As New DataSet
    Private myReader As DataTableReader
    Private objCon As SqlConnection
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 5
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private currentPageRK As Int32 = 1
    Private pageSizeRK As Int16 = 5
    Private currentPageNumberRK As Int16 = 1
    Private totalPagesRK As Double = 1
    Private recordCountRK As Int64 = 1

    Private oCustomClass As New Parameter.AgreementList
    Private oController As New AgreementListController

    Private Property SearchByRK() As String
        Get
            Return CType(ViewState("SearchByRK"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("SearchByRK") = Value
        End Set
    End Property
    Private Property SortRK() As String
        Get
            Return CType(ViewState("SortRK"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("SortRK") = Value
        End Set
    End Property


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If        
        If Not Me.IsPostBack Then
            If Me.IsHoBranch Then
                ShowMessage(lblMessage, "Harus Login Kantor Cabang", True)
                pnlDatagrid.Visible = False
                pnlHeader.Visible = False
                Exit Sub
            Else
                Me.FormID = "OtorisasiVA"
                If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                    GetCombo()
                    Me.SortBy = "Name,NoPelanggan ASC"
                    SortRK = "Name,AgreementNo ASC"
                    btnSave.Visible = False
                End If
            End If
        End If
    End Sub
    Sub GetCombo()
        objCon = New SqlConnection(GetConnectionString)
        myDS = New DataSet
        myCmd.Parameters.Clear()
        myCmd.CommandText = "spGetComboVirtualAccount"
        myCmd.Connection = objCon
        myAdapter.SelectCommand = myCmd
        myAdapter.Fill(myDS)

        If myDS.Tables(0).Rows.Count > 0 Then
            cbVirtualAccount.DataTextField = "VirtualAccountName"
            cbVirtualAccount.DataValueField = "VirtualAccountID"
            cbVirtualAccount.DataSource = myDS.Tables(0)
            cbVirtualAccount.DataBind()
        End If
    End Sub
    Private Sub DoBind(ByVal strSort As String, ByVal strSearch As String)
        Dim cContract As New InstallRcvController
        Dim oContract As New Parameter.GeneralPaging

        With oContract
            .strConnection = GetConnectionString()
            .WhereCond = strSearch
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = strSort            
        End With
        oContract = GetData(oContract)

        recordCount = oContract.TotalRecords
        DtgAgree.DataSource = oContract.ListData

        Try
            DtgAgree.DataBind()            
            PagingFooter()
        Catch ex As Exception
            DtgAgree.CurrentPageIndex = 0
            DtgAgree.DataBind()
            ShowMessage(lblMessage, ex.Message, True)
        End Try        
    End Sub
    Private Sub DoBindRK(ByVal strSort As String, ByVal strSearch As String)
        Dim cContract As New InstallRcvController
        Dim oContract As New Parameter.GeneralPaging

        With oContract
            .strConnection = GetConnectionString()
            .WhereCond = strSearch
            .CurrentPage = currentPageRK
            .PageSize = pageSizeRK
            .SortBy = strSort
        End With
        oContract = GetDataRK(oContract)

        recordCount = oContract.TotalRecords
        DtgAgreeRK.DataSource = oContract.ListData

        Try
            DtgAgreeRK.DataBind()
            PagingFooterRK()
        Catch ex As Exception
            DtgAgreeRK.CurrentPageIndex = 0
            DtgAgreeRK.DataBind()
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub
    Public Function GetData(ByVal oCustomClass As Parameter.GeneralPaging) As Parameter.GeneralPaging
        Dim oReturnValue As New Parameter.GeneralPaging
        Dim params(5) As SqlParameter
        params(0) = New SqlParameter("@VirtualAccountID", SqlDbType.Char, 10)
        params(0).Value = cbVirtualAccount.SelectedValue.Trim
        params(1) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(1).Value = oCustomClass.CurrentPage
        params(2) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(2).Value = oCustomClass.PageSize
        params(3) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(3).Value = oCustomClass.WhereCond
        params(4) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(4).Value = oCustomClass.SortBy
        params(5) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(5).Direction = ParameterDirection.Output
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spVirtualAccountOtorPaging", params).Tables(0)
            oReturnValue.TotalRecords = CType(params(5).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function GetDataRK(ByVal oCustomClass As Parameter.GeneralPaging) As Parameter.GeneralPaging
        Dim oReturnValue As New Parameter.GeneralPaging
        Dim params(5) As SqlParameter
        params(0) = New SqlParameter("@VirtualAccountID", SqlDbType.Char, 10)
        params(0).Value = cbVirtualAccount.SelectedValue.Trim
        params(1) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(1).Value = oCustomClass.CurrentPage
        params(2) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(2).Value = oCustomClass.PageSize
        params(3) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(3).Value = oCustomClass.WhereCond
        params(4) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(4).Value = oCustomClass.SortBy
        params(5) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(5).Direction = ParameterDirection.Output
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spVirtualAccountRKPaging", params).Tables(0)
            oReturnValue.TotalRecords = CType(params(5).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.GetData")
        End Try
    End Function
    Public Sub SelectAll(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim chkSender As CheckBox = CType(sender, CheckBox)
        Dim chkItem As CheckBox
        Dim x As Integer

        For x = 0 To DtgAgree.Items.Count - 1
            chkItem = CType(DtgAgree.Items(x).FindControl("chkItem"), CheckBox)
            chkItem.Checked = chkSender.Checked
        Next
    End Sub
#Region "Navigation"
    Private Sub PagingFooter()
        'lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
                'Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
                'Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SortBy, Me.SearchBy)
    End Sub
    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles imgbtnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SortBy, Me.SearchBy)
            End If
        End If
    End Sub
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles DtgAgree.SortCommand
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
            DoBind(e.SortExpression, Me.SearchBy)
        Else
            Me.SortBy = e.SortExpression + " DESC"
            DoBind(e.SortExpression + " DESC", Me.SearchBy)
        End If
    End Sub

    Private Sub PagingFooterRK()
        'lblPageRK.Text = currentPageRK.ToString()
        totalPagesRK = Math.Ceiling(CType((recordCountRK / CType(pageSizeRK, Integer)), Double))
        If totalPagesRK = 0 Then

            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPageRK.Text = "1"
            rgvGoRK.MaximumValue = "1"
        Else
            lblTotPageRK.Text = CType(totalPagesRK, String)
            rgvGoRK.MaximumValue = CType(totalPagesRK, String)
        End If
        lblrecordRK.Text = CType(recordCount, String)

        If currentPageRK = 1 Then
            imbPrevPageRK.Enabled = False
            imbFirstPageRK.Enabled = False
            If totalPagesRK > 1 Then
                imbNextPageRK.Enabled = True
                imbLastPageRK.Enabled = True
            Else
                imbPrevPageRK.Enabled = False
                imbNextPageRK.Enabled = False
                imbLastPageRK.Enabled = False
                imbFirstPageRK.Enabled = False
            End If
        Else
            imbPrevPageRK.Enabled = True
            imbFirstPageRK.Enabled = True
            If currentPageRK = totalPagesRK Then
                imbNextPageRK.Enabled = False
                imbLastPageRK.Enabled = False
            Else
                imbLastPageRK.Enabled = True
                imbNextPageRK.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLinkRK_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPageRK = 1
            Case "Last" : currentPageRK = Int32.Parse(lblTotPageRK.Text)
                'Case "Next" : currentPageRK = Int32.Parse(lblPageRK.Text) + 1
                'Case "Prev" : currentPageRK = Int32.Parse(lblPageRK.Text) - 1
        End Select
        If Me.SortRK Is Nothing Then
            Me.SortRK = ""
        End If
        DoBindRK(SortRK, SearchByRK)
    End Sub
    Private Sub imbGoPageRK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles imgbtnPageNumbRK.Click
        If IsNumeric(txtPageRK.Text) Then
            If CType(lblTotPageRK.Text, Integer) > 1 And CType(txtPageRK.Text, Integer) <= CType(lblTotPageRK.Text, Integer) Then
                currentPageRK = CType(txtPageRK.Text, Int16)                
                DoBindRK(SortBy, SearchByRK)
            End If
        End If
    End Sub
    Public Sub SortGridRK(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles DtgAgreeRK.SortCommand
        If InStr(SortBy, "DESC") > 0 Then
            SortBy = e.SortExpression
            DoBindRK(e.SortExpression, SearchByRK)
        Else
            SortBy = e.SortExpression + " DESC"
            DoBindRK(e.SortExpression + " DESC", SearchByRK)
        End If
    End Sub
#End Region
    Sub SaveRK(ByVal RK As Parameter.VirtualAccountRK)
        Try
            objCon = New SqlConnection(GetConnectionString)
            myCmd.Parameters.Clear()
            myCmd.CommandText = "spVirtualAccountRKSave"
            myCmd.CommandType = CommandType.StoredProcedure
            myCmd.Parameters.Add(New SqlParameter("@TglTransaksi", SqlDbType.Date) With {.Value = RK.TglTransaksi})
            myCmd.Parameters.Add(New SqlParameter("@VirtualAccountID", SqlDbType.Char, 10) With {.Value = RK.VirtualAccountID})
            myCmd.Parameters.Add(New SqlParameter("@BranchID", SqlDbType.Char, 3) With {.Value = RK.BranchId})
            myCmd.Parameters.Add(New SqlParameter("@AgreementNo", SqlDbType.Char, 20) With {.Value = RK.AgreementNo})
            myCmd.Parameters.Add(New SqlParameter("@NilaiTransaksi", SqlDbType.Decimal) With {.Value = RK.NilaiTransaksi})
            myCmd.Parameters.Add(New SqlParameter("@Keterangan", SqlDbType.VarChar, 200) With {.Value = RK.Keterangan})

            objCon.Open()
            myCmd.Connection = objCon
            myCmd.ExecuteNonQuery()
            objCon.Close()
        Catch ex As Exception
            Throw New Exception(ex.Message)
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub
    Private Sub btnSearch_Click(sender As Object, e As System.EventArgs) Handles btnSearch.Click
        If cbVirtualAccount.SelectedValue <> "" Then
            Me.SearchBy = " where  VirtualAccountLog.VirtualAccountID='" & cbVirtualAccount.SelectedValue.Trim & "' and Agreement.BranchID='" & Me.sesBranchId.Replace("'", "") & "'"
            SearchByRK = " where  VirtualAccountRK.VirtualAccountID='" & cbVirtualAccount.SelectedValue.Trim & "' and Agreement.BranchID='" & Me.sesBranchId.Replace("'", "") & "'"
        End If

        DoBind(Me.SortBy, Me.SearchBy)
        DoBindRK(SortRK, SearchByRK)
        btnSave.Visible = True
        pnlHeader.Visible = False
        pnlDatagrid.Visible = True
    End Sub
    Private Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click        
        Dim lblVirtualAccountID As Label
        Dim lblSequenceNo As Label
        Dim lblUploadDate As Label
        Dim lblValueDate As Label
        Dim lblBranchID As Label
        Dim lblApplicationID As Label
        Dim lblBankAccountID As Label
        Dim lblNoPelanggan As Label
        Dim lblNilaiTransaksi As Label
        Dim listCheked As New List(Of Integer)

        Dim chkDtList As CheckBox        
        For i = 0 To DtgAgree.Items.Count - 1
            chkDtList = CType(DtgAgree.Items(i).FindControl("chkItem"), CheckBox)
            If Not IsNothing(chkDtList) Then
                If chkDtList.Enabled Then
                    If DtgAgreeRK.Items.Count = 0 Then
                        ShowMessage(lblMessage, "Data Rekening Korannya tidak ada", True)
                        Exit Sub
                    Else
                        If chkDtList.Checked Then
                            listCheked.Add(i)
                            lblVirtualAccountID = CType(DtgAgree.Items(i).FindControl("lblVirtualAccountID"), Label)
                            lblSequenceNo = CType(DtgAgree.Items(i).FindControl("lblSequenceNo"), Label)
                            lblUploadDate = CType(DtgAgree.Items(i).FindControl("lblUploadDate"), Label)
                            lblValueDate = CType(DtgAgree.Items(i).FindControl("lblValueDate"), Label)
                            lblBranchID = CType(DtgAgree.Items(i).FindControl("lblBranchID"), Label)
                            lblApplicationID = CType(DtgAgree.Items(i).FindControl("lblApplicationID"), Label)
                            lblBankAccountID = CType(DtgAgree.Items(i).FindControl("lblBankAccountID"), Label)
                            lblNoPelanggan = CType(DtgAgree.Items(i).FindControl("lblNoPelanggan"), Label)
                            lblNilaiTransaksi = CType(DtgAgree.Items(i).FindControl("lblNilaiTransaksi"), Label)
                            Try
                                Dim Param As New VirtualAccountOtor
                                With Param
                                    .VirtualAccountID = lblVirtualAccountID.Text.Trim
                                    .SequenceNo = CInt(lblSequenceNo.Text)
                                    .UploadDate = CDate(lblUploadDate.Text)
                                    .ValueDate = CDate(lblValueDate.Text)
                                    .BranchId = lblBranchID.Text.Trim
                                    .ApplicationID = lblApplicationID.Text.Trim
                                    .BankAccountID = lblBankAccountID.Text.Trim
                                    .NoPelanggan = lblNoPelanggan.Text.Trim
                                    .NilaiTransaksi = CDec(lblNilaiTransaksi.Text)
                                    .BusinessDate = Me.BusinessDate
                                End With
                                Save(Param)
                            Catch ex As Exception
                                ShowMessage(lblMessage, ex.Message, True)
                            End Try
                        End If
                    End If
                End If
            End If
        Next

        If listCheked.Count = 0 Then
            ShowMessage(lblMessage, "Belum dipilih!", True)
            Exit Sub
        Else
            DoBind(Me.SortBy, Me.SearchBy)
            ShowMessage(lblMessage, "Posting Berhasil", False)
        End If
    End Sub
    Sub Save(ByRef Param As VirtualAccountOtor)
        Try
            objCon = New SqlConnection(GetConnectionString)
            myCmd.Parameters.Clear()
            myCmd.CommandText = "spPostingVirtualAccountCabang"
            myCmd.CommandType = CommandType.StoredProcedure            
            myCmd.Parameters.Add(New SqlParameter("@BranchAgreement", SqlDbType.VarChar, 3) With {.Value = Param.BranchId})
            myCmd.Parameters.Add(New SqlParameter("@Applicationid", SqlDbType.VarChar, 20) With {.Value = Param.ApplicationID})
            myCmd.Parameters.Add(New SqlParameter("@valuedate", SqlDbType.Date) With {.Value = Param.ValueDate})
            myCmd.Parameters.Add(New SqlParameter("@businessdate", SqlDbType.Date) With {.Value = Param.BusinessDate})
            myCmd.Parameters.Add(New SqlParameter("@BankAccountID", SqlDbType.VarChar, 10) With {.Value = Param.BankAccountID})
            myCmd.Parameters.Add(New SqlParameter("@AmountReceive", SqlDbType.Decimal) With {.Value = Param.NilaiTransaksi})
            myCmd.Parameters.Add(New SqlParameter("@UploadDate", SqlDbType.Date) With {.Value = Param.UploadDate})
            myCmd.Parameters.Add(New SqlParameter("@VirtualAccountID", SqlDbType.VarChar, 10) With {.Value = Param.VirtualAccountID})
            myCmd.Parameters.Add(New SqlParameter("@SequenceNo", SqlDbType.SmallInt) With {.Value = Param.SequenceNo})
            myCmd.Parameters.Add(New SqlParameter("@NoPelanggan", SqlDbType.VarChar, 20) With {.Value = Param.NoPelanggan})

            objCon.Open()
            myCmd.Connection = objCon
            myCmd.CommandTimeout = 0
            myCmd.ExecuteNonQuery()
            objCon.Close()
        Catch ex As Exception
            Throw New Exception(ex.Message)
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub
    Private Sub btnReset_Click(sender As Object, e As System.EventArgs) Handles btnReset.Click
        Response.Redirect("Otorisasi.aspx")
    End Sub
    'Private Sub btnCariRK_Click(sender As Object, e As System.EventArgs) Handles btnCariRK.Click
    '    SearchByRK = " where  VirtualAccountID='" & cbVirtualAccount.SelectedValue.Trim & "' and Agreement.BranchID='" & Me.sesBranchId.Replace("'", "") & "'"
    '    If cboSearchByRK.SelectedItem.Value <> "" And txtSearchByRK.Text <> "" Then
    '        Me.SearchByRK += " and " & cboSearchByRK.SelectedItem.Value.Trim & " LIKE '%" & txtSearchByRK.Text.Trim & "%'"
    '    End If
    '    DoBindRK(SortRK, SearchByRK)
    'End Sub
    Private Sub btnCari_Click(sender As Object, e As System.EventArgs) Handles btnCari.Click
        Me.SearchBy = " where  VirtualAccountLog.VirtualAccountID='" & cbVirtualAccount.SelectedValue.Trim & "' and Agreement.BranchID='" & Me.sesBranchId.Replace("'", "") & "'"
        If cboSearchBy.SelectedItem.Value <> "" And txtSearchBy.Text <> "" Then
            Me.SearchBy += " and " & cboSearchBy.SelectedItem.Value.Trim & " LIKE '%" & txtSearchBy.Text.Trim & "%'"
        End If
        DoBind(Me.SortBy, Me.SearchBy)
        SearchByRK = " where  VirtualAccountRK.VirtualAccountID='" & cbVirtualAccount.SelectedValue.Trim & "' and Agreement.BranchID='" & Me.sesBranchId.Replace("'", "") & "'"
        If cboSearchBy.SelectedItem.Value <> "" And txtSearchBy.Text <> "" Then
            Me.SearchByRK += " and " & cboSearchBy.SelectedItem.Value.Trim & " LIKE '%" & txtSearchBy.Text.Trim & "%'"
        End If
        DoBindRK(SortRK, SearchByRK)
    End Sub
End Class