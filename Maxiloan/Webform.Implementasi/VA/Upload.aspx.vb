﻿#Region "Imports"
Imports Maxiloan.Controller
Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.Webform.UserController
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data.OleDb
Imports System.Data.Odbc
Imports System.Text
Imports System.Data.SqlClient
Imports System.IO
Imports System.Web.UI.WebControls.FileUpload
Imports System.Text.RegularExpressions
Imports System.Diagnostics
Imports System.Drawing
Imports System.Runtime.InteropServices
Imports DocumentFormat.OpenXml.Packaging
Imports DocumentFormat.OpenXml.Spreadsheet

#End Region

Public Class Upload
    Inherits Maxiloan.Webform.WebBased

    Private myCmd As New SqlCommand
    Private myAdapter As New SqlDataAdapter
    Private myDS As New DataSet
    Private myReader As DataTableReader
    Private FileName As String
    Private strFileTemp As String
    Private strFile As String        
    Private _NoPelanggan As String
    Private FileDirectory As String
    Private TotalAgreement As Double
    Private TotalInstallment As Decimal
    Private FileType As String
    Private _TglTransaksi As String
    Private _NoRekening As String
    Private _BranchID As String
    Private _AgreementNo As String
    Private objCon As SqlConnection
    Private TextFile As String

    Public Property FilaName() As String
        Get
            Return CType(ViewState("FilaName"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("FilaName") = Value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblLoading.Visible = False
        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not Me.IsPostBack Then
            If Not Me.IsHoBranch Then
                ShowMessage(lblMessage, "Harus Login Kantor Pusat", True)
                pnlHeader.Visible = False
                Exit Sub
            Else
                Me.FormID = "UploadVA"
                If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                    GetCombo()
                End If
            End If
        End If
    End Sub
    Private Sub btnUpload_Click(sender As Object, e As System.EventArgs) Handles btnUpload.Click
        lblLoading.Visible = True        
        btnUpload.Enabled = False
        Try
            If FileVA.HasFile Then                
                strFileTemp = FileVA.FileName
                FileName = Context.Request.MapPath("~\\xml\\") + strFileTemp
                File.Delete(FileName)
                FileVA.SaveAs(FileName)
                Dim larr As Array = CType(strFileTemp.ToString.Split(CChar(".")), Array)
                TextFile = strFileTemp
                FileDirectory = Context.Request.MapPath("~\\xml\\").ToString.Trim
                FileType = larr.GetValue(1).ToString.Trim()

                Me.FileName = FileName

                Dim VirtualAccountID As String = cbVirtualAccount.Items(cbVirtualAccount.SelectedIndex).Value.Trim
                'If VirtualAccountID = "BCA" Then
                '    ProsesBCA(VirtualAccountID)
                'End If
                'If VirtualAccountID = "BRI" Then
                '    ProsesBRI(VirtualAccountID)
                'End If
                If VirtualAccountID = "BNI" Then
                    ProsesBNI(VirtualAccountID)
                End If
            Else
                lblLoading.Visible = False
                ShowMessage(lblMessage, "File belum dipilih!", True)
                Exit Sub
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
            
        btnUpload.Enabled = True
        lblLoading.Visible = False
    End Sub
    Sub GetCombo()
        objCon = New SqlConnection(GetConnectionString)
        myDS = New DataSet
        myCmd.Parameters.Clear()
        myCmd.CommandText = "spGetComboVirtualAccount"
        myCmd.Connection = objCon
        myAdapter.SelectCommand = myCmd
        myAdapter.Fill(myDS)

        If myDS.Tables(0).Rows.Count > 0 Then
            cbVirtualAccount.DataTextField = "VirtualAccountName"
            cbVirtualAccount.DataValueField = "VirtualAccountID"
            cbVirtualAccount.DataSource = myDS.Tables(0)
            cbVirtualAccount.DataBind()
        End If
    End Sub
    Function ValidasiFile() As Boolean
        Dim result As Boolean = True
        objCon = New SqlConnection(GetConnectionString)
        Try
            myDS = New DataSet
            myCmd.Parameters.Clear()
            myCmd.CommandText = "spGetVirtualAccountValidFile"
            myCmd.CommandType = CommandType.StoredProcedure
            myCmd.Parameters.Add(New SqlParameter("@FileName", SqlDbType.VarChar, 200) With {.Value = strFileTemp.Trim})
            myCmd.Connection = objCon
            myAdapter.SelectCommand = myCmd
            myAdapter.Fill(myDS)

            If myDS.Tables(0).Rows.Count > 0 Then
                result = False
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message)            
            ShowMessage(lblMessage, ex.Message, True)
        End Try
        Return result
    End Function
    Private Function Text_To_DataTable(ByVal path As String, ByVal delimitter As Char, ByVal header As Boolean) As DataTable
        Dim source As String = String.Empty
        Dim dt As DataTable = New DataTable

        If IO.File.Exists(path) Then
            source = IO.File.ReadAllText(path)
        Else
            Throw New IO.FileNotFoundException("Could not find the file at " & path, path)
        End If

        Dim rows() As String = source.Split({Environment.NewLine}, StringSplitOptions.None)

        For i As Integer = 0 To rows(0).Split(delimitter).Length - 1
            Dim column As String = rows(0).Split(delimitter)(i)
            dt.Columns.Add(If(header, column, "column" & i + 1))
        Next

        For i As Integer = If(header, 1, 0) To rows.Length - 1
            Dim dr As DataRow = dt.NewRow

            For x As Integer = 0 To rows(i).Split(delimitter).Length - 1
                If x <= dt.Columns.Count - 1 Then
                    dr(x) = rows(i).Split(delimitter)(x)
                Else
                    Throw New Exception("The number of columns on row " & i + If(header, 0, 1) & " is greater than the amount of columns in the " & If(header, "header.", "first row."))
                End If
            Next

            dt.Rows.Add(dr)
        Next

        Return dt
    End Function

    Private Function Text_To_DataTable_2(ByVal path As String, ByVal delimitter As Char, ByVal header As Boolean) As DataTable
        Dim source As String = String.Empty
        Dim dt As DataTable = New DataTable

        If IO.File.Exists(path) Then
            source = IO.File.ReadAllText(path)
        Else
            Throw New IO.FileNotFoundException("Could not find the file at " & path, path)
        End If

        Dim rows() As String = source.Split({Environment.NewLine}, StringSplitOptions.None)

        For i As Integer = 0 To rows(0).Split(delimitter).Length - 1
            Dim column As String = rows(0).Split(delimitter)(i)
            dt.Columns.Add(If(header, column, "column" & i + 1))
        Next

        For i As Integer = 0 To rows.Length - 1
            Dim dr As DataRow = dt.NewRow

            For x As Integer = 0 To rows(i).Split(delimitter).Length - 1
                If x <= dt.Columns.Count - 1 Then
                    Dim str As String = rows(i).Split(delimitter)(x).Trim

                    Try
                        Dim number As Decimal
                        number = Convert.ToDecimal(str.Substring(3, 20))
                        dr(x) = str
                        dt.Rows.Add(dr)
                    Catch ex As Exception
                        Continue For
                    End Try
                Else
                    Throw New Exception("The number of columns on row " & i + If(header, 0, 1) & " is greater than the amount of columns in the " & If(header, "header.", "first row."))
                End If
            Next
        Next

        Return dt
    End Function

    'Private Sub ProsesBCA(ByVal VirtualAccountID As String)
    '    Dim SequenceNo As Integer = 0
    '    Dim list As New List(Of Parameter.VirtualAccountBCA)
    '    If ValidasiFile() Then
    '        Dim formatText As String
    '        formatText = cbFormatText.SelectedValue.Trim

    '        If (formatText.Equals("1")) Then
    '            list = UploadFileBCA()
    '        ElseIf (formatText.Equals("2")) Then
    '            list = UploadFileBCA_2()
    '        End If
    '    Else
    '        ShowMessage(lblMessage, "File pernah di upload!", True)
    '        Exit Sub
    '    End If
    '    Try
    '        TotalAgreement = list.Count
    '        SaveLog(VirtualAccountID, SequenceNo)
    '        If list.Count > 0 Then
    '            For index = 0 To list.Count - 1
    '                Dim payment As New Parameter.VirtualAccountBCA
    '                payment = list(index)
    '                payment.UploadDate = BusinessDate
    '                payment.VirtualAccountID = VirtualAccountID
    '                payment.SequenceNo = SequenceNo
    '                SaveVaBCA(payment)
    '            Next
    '        End If
    '        UpdateLog(VirtualAccountID, SequenceNo)
    '        ShowMessage(lblMessage, "Upload Data VA Berhasil", False)
    '    Catch ex As Exception
    '        ShowMessage(lblMessage, ex.Message, True)
    '    End Try
    'End Sub
    'Private Sub ProsesBRI(ByVal VirtualAccountID As String)
    '    Dim SequenceNo As Integer = 0
    '    Dim list As New List(Of Parameter.VirtualAccountBRI)
    '    If ValidasiFile() Then
    '        list = UploadFileBRI()
    '    Else
    '        ShowMessage(lblMessage, "File pernah di upload!", True)
    '        Exit Sub
    '    End If
    '    Try
    '        TotalAgreement = list.Count
    '        SaveLog(VirtualAccountID, SequenceNo)
    '        If list.Count > 0 Then
    '            For index = 0 To list.Count - 1
    '                Dim payment As New Parameter.VirtualAccountBRI
    '                payment = list(index)
    '                payment.UploadDate = BusinessDate
    '                payment.VirtualAccountID = VirtualAccountID
    '                payment.SequenceNo = SequenceNo
    '                SaveVaBRI(payment)
    '            Next
    '        End If
    '        UpdateLog(VirtualAccountID, SequenceNo)
    '        ShowMessage(lblMessage, "Upload Data VA Berhasil", False)
    '    Catch ex As Exception
    '        ShowMessage(lblMessage, ex.Message, True)
    '    End Try
    'End Sub
    Private Sub ProsesBNI(ByVal VirtualAccountID As String)
        Dim SequenceNo As Integer = 0
        Dim list As New List(Of Parameter.VirtualAccountBRI)
        If ValidasiFile() Then
            list = UploadFileBNI()
        Else
            ShowMessage(lblMessage, "File pernah di upload!", True)
            Exit Sub
        End If
        Try
            TotalAgreement = list.Count
            SaveLog(VirtualAccountID, SequenceNo)
            If list.Count > 0 Then
                For index = 0 To list.Count - 1
                    Dim payment As New Parameter.VirtualAccountBRI
                    payment = list(index)
                    payment.UploadDate = BusinessDate
                    payment.VirtualAccountID = VirtualAccountID
                    payment.SequenceNo = SequenceNo
                    SaveVaBNI(payment)
                Next
            End If
            UpdateLog(VirtualAccountID, SequenceNo)
            ShowMessage(lblMessage, "Upload Data VA Berhasil", False)
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub
    Function UploadFileBCA_2() As List(Of Parameter.VirtualAccountBCA)
        Dim list As New List(Of Parameter.VirtualAccountBCA)
        Dim data As New DataTable

        If (FileType.ToUpper) = "TXT" Then
            data = Text_To_DataTable_2(FileName, "\t", True)

            If data.Rows.Count > 0 Then
                For index = 0 To data.Rows.Count - 1
                    If data.Rows(index).Item(0).ToString.Trim <> "" Then
                        Dim Payment As New Parameter.VirtualAccountBCA

                        'Payment.BranchId = CStr(data.Rows(index).Item(0)).Substring(11, 3).Trim
                        Payment.BranchId = "-"
                        Payment.NoPelanggan = CStr(data.Rows(index).Item(0)).Substring(3, 20).Trim
                        Payment.NamaPelanggan = CStr(data.Rows(index).Item(0)).Substring(23, 18).Trim
                        Payment.MataUang = CStr(data.Rows(index).Item(0)).Substring(41, 13).Trim
                        Payment.NilaiTransaksi = CDec(data.Rows(index).Item(0).Substring(53, 14))
                        Payment.TglTransaksi = ConvertDate2(IIf(data.Rows(index).Item(0).Substring(73, 10).Trim = "", "01/01/1900", data.Rows(index).Item(0).Substring(68, 10)))
                        Payment.WaktuTransaksi = ConvertDate2(IIf(data.Rows(index).Item(0).Substring(83, 10).Trim = "", "00:00:00", data.Rows(index).Item(0).Substring(78, 10))).ToString("HH:mm:ss")
                        Payment.Lokasi = "-"
                        Payment.Berita1 = CStr(data.Rows(index).Item(0)).Substring(95, 17).Trim
                        Payment.Berita2 = "-"

                        Me._TglTransaksi = Payment.TglTransaksi
                        Me._NoPelanggan = Payment.NoPelanggan

                        Dim query As New Parameter.VirtualAccountBCA
                        If list.Count > 0 Then
                            query = list.Find(AddressOf PredicateFunctionBCA)
                        Else
                            query = Nothing
                        End If

                        If query Is Nothing Then
                            TotalInstallment = TotalInstallment + Payment.NilaiTransaksi
                            list.Add(Payment)
                        End If
                    End If
                Next
            End If
        Else
            ShowMessage(lblMessage, "Format Harus TXT", True)
        End If

        Return list
    End Function

    Function UploadFileBCA() As List(Of Parameter.VirtualAccountBCA)
        Dim list As New List(Of Parameter.VirtualAccountBCA)
        Dim data As New DataTable
        
        If (FileType.ToUpper) = "TXT" Then         
            data = Text_To_DataTable(FileName, "\t", True)

            If data.Rows.Count > 0 Then
                For index = 5 To data.Rows.Count - 1
                    If data.Rows(index).Item(0).ToString.Trim <> "" Then
                        Dim Payment As New Parameter.VirtualAccountBCA

                        'Payment.BranchId = CStr(data.Rows(index).Item(0)).Substring(11, 3).Trim
                        Payment.BranchId = "-"
                        Payment.NoPelanggan = CStr(data.Rows(index).Item(0)).Substring(11, 19).Trim
                        Payment.NamaPelanggan = CStr(data.Rows(index).Item(0)).Substring(45, 31).Trim
                        Payment.MataUang = CStr(data.Rows(index).Item(0)).Substring(76, 25).Trim
                        Payment.NilaiTransaksi = CDec(data.Rows(index).Item(0).Substring(121, 12))
                        Payment.TglTransaksi = ConvertDate2(IIf(data.Rows(index).Item(0).Substring(136, 11).Trim = "", "01/01/1900", data.Rows(index).Item(0).Substring(136, 11)))
                        Payment.WaktuTransaksi = ConvertDate2(IIf(data.Rows(index).Item(0).Substring(147, 11).Trim = "", "00:00:00", data.Rows(index).Item(0).Substring(147, 11))).ToString("HH:mm:ss")
                        Payment.Lokasi = "-"
                        Payment.Berita1 = CStr(data.Rows(index).Item(0)).Substring(158, 37).Trim
                        Payment.Berita2 = "-"

                        Me._TglTransaksi = Payment.TglTransaksi
                        Me._NoPelanggan = Payment.NoPelanggan

                        Dim query As New Parameter.VirtualAccountBCA
                        If list.Count > 0 Then
                            query = list.Find(AddressOf PredicateFunctionBCA)
                        Else
                            query = Nothing
                        End If

                        If query Is Nothing Then
                            TotalInstallment = TotalInstallment + Payment.NilaiTransaksi
                            list.Add(Payment)
                        End If
                    End If
                Next
            End If
        Else
            Data = FileToDataTable()
            Dim NoRekening As String = ""
            Dim TglTransaksi As Date = Nothing

            For index = 0 To data.Rows.Count - 1
                Dim Payment As New Parameter.VirtualAccountBCA

                Payment.BranchId = CStr(data.Rows(index).ItemArray(2).ToString.Trim).Substring(0, 3)
                Payment.NoPelanggan = CStr(data.Rows(index).ItemArray(2)).Trim
                Payment.NamaPelanggan = CStr(data.Rows(index).ItemArray(3)).Trim
                Payment.MataUang = CStr(data.Rows(index).ItemArray(4)).Trim
                Payment.NilaiTransaksi = CDec(IIf(CStr(data.Rows(index).ItemArray(5)).Trim = "", "0", data.Rows(index).ItemArray(5)).Trim)
                Payment.TglTransaksi = ConvertDate2(IIf(CStr(data.Rows(index).ItemArray(6)).Trim = "", "01/01/1900", CStr(data.Rows(index).ItemArray(6)).Trim))
                Payment.WaktuTransaksi = ConvertDate2(IIf(CStr(data.Rows(index).ItemArray(7)).Trim = "", "00:00:00", CStr(data.Rows(index).ItemArray(7)).Trim)).ToString("HH:mm:ss")
                Payment.Lokasi = CStr(data.Rows(index).ItemArray(8)).Trim
                Payment.Berita1 = CStr(data.Rows(index).ItemArray(9)).Trim
                Payment.Berita2 = CStr(data.Rows(index).ItemArray(10)).Trim

                Me._TglTransaksi = Payment.TglTransaksi
                Me._NoPelanggan = Payment.NoPelanggan

                Dim query As New Parameter.VirtualAccountBCA
                If list.Count > 0 Then
                    query = list.Find(AddressOf PredicateFunctionBCA)
                Else
                    query = Nothing
                End If

                If query Is Nothing Then
                    TotalInstallment = TotalInstallment + Payment.NilaiTransaksi
                    list.Add(Payment)
                End If
            Next
        End If

        Return list
    End Function
    Function UploadFileBRI() As List(Of Parameter.VirtualAccountBRI)
        Dim list As New List(Of Parameter.VirtualAccountBRI)
        Dim data As New DataTable
        Dim NoRekening As String = ""
        Dim TglTransaksi As Date = Nothing

        Try
            data = CSVToDataTable(Me.FileName)
            For index = 0 To data.Rows.Count - 1
                If (index > 0) Then
                    Dim Payment As New Parameter.VirtualAccountBRI

                    Dim arr As String() = data.Rows(index).Item(0).ToString().Split(New Char() {""""c})
                    Dim parts As String() = arr(0).ToString().Split(New Char() {","c})
                    Dim deskripsi As String() = parts(2).Split()

                    Dim namaPelanggan As New StringBuilder
                    For i = 1 To deskripsi.Count - 1
                        namaPelanggan.Append(deskripsi(i))
                        namaPelanggan.Append(" ")
                    Next

                    Payment.TglTransaksi = ConvertDate2(IIf(parts(1).Substring(0, 10).Trim = "", "01/01/1900", parts(1).Substring(0, 10).Trim))
                    Payment.WaktuTransaksi = ConvertDate2(IIf(parts(1).Substring(11, 8).Trim = "", "00:00:00", parts(1).Substring(11, 8).Trim)).ToString("HH:mm:ss")
                    Payment.BranchId = "-"
                    Payment.NoPelanggan = deskripsi(0).ToString().Trim()
                    Payment.NamaPelanggan = namaPelanggan.ToString().Trim
                    Payment.NilaiTransaksi = CDec(arr(1).ToString().Trim)
                    Payment.Keterangan = CStr(parts(3)).Trim
                    Payment.Berita1 = "-"
                    Payment.Berita2 = "-"

                    Me._TglTransaksi = Payment.TglTransaksi
                    Me._NoPelanggan = Payment.NoPelanggan

                    Dim query As New Parameter.VirtualAccountBRI
                    If list.Count > 0 Then
                        query = list.Find(AddressOf PredicateFunctionBRI)
                    Else
                        query = Nothing
                    End If

                    If query Is Nothing Then
                        TotalInstallment = TotalInstallment + Payment.NilaiTransaksi
                        list.Add(Payment)
                    End If
                End If

            Next
        Catch ex As Exception
            Throw New ApplicationException(ex.Message)
        End Try

        Return list
    End Function
    Function UploadFileBNI() As List(Of Parameter.VirtualAccountBRI)
        Dim list As New List(Of Parameter.VirtualAccountBRI)
        Dim data As New DataTable
        Dim NoRekening As String = ""
        Dim TglTransaksi As Date = Nothing

        Try
            data = CSVToDataTable(Me.FileName)
            For index = 0 To data.Rows.Count - 1
                If (index > 0) Then
                    Dim Payment As New Parameter.VirtualAccountBRI

                    Dim arr As String() = data.Rows(index).Item(0).ToString().Split(New Char() {""""c})
                    Dim parts As String() = arr(0).ToString().Split(New Char() {","c})

                    Payment.TglTransaksi = ConvertDate2(IIf(parts(1).Substring(0, 10).Trim = "", "01/01/1900", parts(1).Substring(0, 10).Trim))
                    Payment.WaktuTransaksi = Now.Hour.ToString("00") & ":" & Now.Minute.ToString("00") & ":" & Now.Second.ToString("00")
                    Payment.NoPelanggan = parts(2)
                    Payment.NamaPelanggan = parts(3)
                    Payment.Journal = parts(4)
                    Payment.BankBranchVA = parts(5)
                    Payment.NilaiTransaksi = parts(6)
                    Payment.Post = parts(7)
                    Payment.Keterangan = parts(8)
                    Payment.Keterangan2 = parts(9)

                    Me._TglTransaksi = Payment.TglTransaksi
                    Me._NoPelanggan = Payment.NoPelanggan

                    Dim query As New Parameter.VirtualAccountBRI
                    If list.Count > 0 Then
                        query = list.Find(AddressOf PredicateFunctionBRI)
                    Else
                        query = Nothing
                    End If

                    If query Is Nothing Then
                        TotalInstallment = TotalInstallment + Payment.NilaiTransaksi
                        list.Add(Payment)
                    End If
                End If

            Next
        Catch ex As Exception
            Throw New ApplicationException(ex.Message)
        End Try

        Return list
    End Function
    Public Function PredicateFunctionBCA(ByVal custom As Parameter.VirtualAccountBCA) As Boolean
        Return custom.TglTransaksi = Me._TglTransaksi And custom.NoPelanggan = Me._NoPelanggan
    End Function
    Public Function PredicateFunctionBRI(ByVal custom As Parameter.VirtualAccountBRI) As Boolean
        Return custom.TglTransaksi = Me._TglTransaksi And custom.NoPelanggan = Me._NoPelanggan
    End Function
    Sub SaveVaBCA(ByVal payment As Parameter.VirtualAccountBCA)
        Try
            objCon = New SqlConnection(GetConnectionString)
            myCmd.Parameters.Clear()
            myCmd.CommandText = "spVirtualAccountBCASave"
            myCmd.CommandType = CommandType.StoredProcedure
            myCmd.Parameters.Add(New SqlParameter("@UploadDate", SqlDbType.DateTime) With {.Value = payment.UploadDate})
            myCmd.Parameters.Add(New SqlParameter("@VirtualAccountID", SqlDbType.VarChar, 10) With {.Value = payment.VirtualAccountID})
            myCmd.Parameters.Add(New SqlParameter("@SequenceNo", SqlDbType.SmallInt) With {.Value = payment.SequenceNo})
            myCmd.Parameters.Add(New SqlParameter("@BranchID", SqlDbType.Char, 3) With {.Value = payment.BranchId})
            myCmd.Parameters.Add(New SqlParameter("@NoPelanggan", SqlDbType.Char, 12) With {.Value = payment.NoPelanggan})
            myCmd.Parameters.Add(New SqlParameter("@NamaPelanggan", SqlDbType.VarChar, 50) With {.Value = payment.NamaPelanggan})
            myCmd.Parameters.Add(New SqlParameter("@MataUang", SqlDbType.Char, 3) With {.Value = payment.MataUang})
            myCmd.Parameters.Add(New SqlParameter("@NilaiTransaksi", SqlDbType.Decimal) With {.Value = payment.NilaiTransaksi})
            myCmd.Parameters.Add(New SqlParameter("@TglTransaksi", SqlDbType.Date) With {.Value = payment.TglTransaksi})
            myCmd.Parameters.Add(New SqlParameter("@WaktuTransaksi", SqlDbType.Time) With {.Value = payment.WaktuTransaksi})
            myCmd.Parameters.Add(New SqlParameter("@Lokasi", SqlDbType.Char, 5) With {.Value = payment.Lokasi})
            myCmd.Parameters.Add(New SqlParameter("@Berita1", SqlDbType.VarChar, 50) With {.Value = payment.Berita1})
            myCmd.Parameters.Add(New SqlParameter("@Berita2", SqlDbType.VarChar, 50) With {.Value = payment.Berita2})

            objCon.Open()
            myCmd.Connection = objCon
            myCmd.ExecuteNonQuery()
            objCon.Close()
        Catch ex As Exception
            Throw New Exception(ex.Message)
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub
    Sub SaveVaBRI(ByVal payment As Parameter.VirtualAccountBRI)
        Try
            objCon = New SqlConnection(GetConnectionString)
            myCmd.Parameters.Clear()
            myCmd.CommandText = "spVirtualAccountBRISave"
            myCmd.CommandType = CommandType.StoredProcedure
            myCmd.Parameters.Add(New SqlParameter("@UploadDate", SqlDbType.DateTime) With {.Value = payment.UploadDate})
            myCmd.Parameters.Add(New SqlParameter("@VirtualAccountID", SqlDbType.VarChar, 10) With {.Value = payment.VirtualAccountID})
            myCmd.Parameters.Add(New SqlParameter("@SequenceNo", SqlDbType.SmallInt) With {.Value = payment.SequenceNo})
            myCmd.Parameters.Add(New SqlParameter("@BranchID", SqlDbType.Char, 3) With {.Value = payment.BranchId})
            myCmd.Parameters.Add(New SqlParameter("@NoPelanggan", SqlDbType.Char, 12) With {.Value = payment.NoPelanggan})
            myCmd.Parameters.Add(New SqlParameter("@TglTransaksi", SqlDbType.Date) With {.Value = payment.TglTransaksi})
            myCmd.Parameters.Add(New SqlParameter("@NamaPelanggan", SqlDbType.VarChar, 50) With {.Value = payment.NamaPelanggan})
            myCmd.Parameters.Add(New SqlParameter("@NilaiTransaksi", SqlDbType.Decimal) With {.Value = payment.NilaiTransaksi})
            myCmd.Parameters.Add(New SqlParameter("@WaktuTransaksi", SqlDbType.Time) With {.Value = payment.WaktuTransaksi})
            myCmd.Parameters.Add(New SqlParameter("@Keterangan", SqlDbType.Char, 100) With {.Value = payment.Keterangan})
            myCmd.Parameters.Add(New SqlParameter("@Berita1", SqlDbType.VarChar, 50) With {.Value = payment.Berita1})
            myCmd.Parameters.Add(New SqlParameter("@Berita2", SqlDbType.VarChar, 50) With {.Value = payment.Berita2})

            objCon.Open()
            myCmd.Connection = objCon
            myCmd.ExecuteNonQuery()
            objCon.Close()
        Catch ex As Exception
            Throw New Exception(ex.Message)
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub
    Sub SaveVaBNI(ByVal payment As Parameter.VirtualAccountBRI)
        Try
            objCon = New SqlConnection(GetConnectionString)
            myCmd.Parameters.Clear()
            myCmd.CommandText = "spVirtualAccountBNISave"
            myCmd.CommandType = CommandType.StoredProcedure
            myCmd.Parameters.Add(New SqlParameter("@UploadDate", SqlDbType.DateTime) With {.Value = payment.UploadDate})
            myCmd.Parameters.Add(New SqlParameter("@VirtualAccountID", SqlDbType.VarChar, 10) With {.Value = payment.VirtualAccountID})
            myCmd.Parameters.Add(New SqlParameter("@TglTransaksi", SqlDbType.Date) With {.Value = payment.TglTransaksi})
            myCmd.Parameters.Add(New SqlParameter("@WaktuTransaksi", SqlDbType.Time) With {.Value = payment.WaktuTransaksi})
            myCmd.Parameters.Add(New SqlParameter("@SequenceNo", SqlDbType.SmallInt) With {.Value = payment.SequenceNo})
            myCmd.Parameters.Add(New SqlParameter("@NoPelanggan", SqlDbType.Char, 20) With {.Value = payment.NoPelanggan})
            myCmd.Parameters.Add(New SqlParameter("@NamaPelanggan", SqlDbType.VarChar, 50) With {.Value = payment.NamaPelanggan})
            myCmd.Parameters.Add(New SqlParameter("@Journal", SqlDbType.Char, 20) With {.Value = payment.Journal})
            myCmd.Parameters.Add(New SqlParameter("@BankBranchVA", SqlDbType.Char, 10) With {.Value = payment.BankBranchVA})
            myCmd.Parameters.Add(New SqlParameter("@NilaiTransaksi", SqlDbType.Decimal) With {.Value = payment.NilaiTransaksi})
            myCmd.Parameters.Add(New SqlParameter("@Post", SqlDbType.Char, 1) With {.Value = payment.Post})
            myCmd.Parameters.Add(New SqlParameter("@Keterangan1", SqlDbType.VarChar, 100) With {.Value = payment.Keterangan})
            myCmd.Parameters.Add(New SqlParameter("@Keterangan2", SqlDbType.VarChar, 100) With {.Value = payment.Keterangan2})


            objCon.Open()
            myCmd.Connection = objCon
            myCmd.ExecuteNonQuery()
            objCon.Close()
        Catch ex As Exception
            Throw New Exception(ex.Message)
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub
    Sub SaveLog(ByVal VirtualAccountID As String, ByRef SequenceNo As Integer)
        Try
            objCon = New SqlConnection(GetConnectionString)
            myCmd.Parameters.Clear()
            myCmd.CommandText = "spVirtualAccountLogSave"
            myCmd.CommandType = CommandType.StoredProcedure
            myCmd.Parameters.Add(New SqlParameter("@UploadDate", SqlDbType.DateTime) With {.Value = BusinessDate})
            myCmd.Parameters.Add(New SqlParameter("@VirtualAccountID", SqlDbType.VarChar, 10) With {.Value = VirtualAccountID})
            myCmd.Parameters.Add(New SqlParameter("@FileName", SqlDbType.VarChar, 200) With {.Value = strFileTemp})
            myCmd.Parameters.Add(New SqlParameter("@FileDirectory", SqlDbType.VarChar, 8000) With {.Value = FileDirectory})
            myCmd.Parameters.Add(New SqlParameter("@TotalAgreement", SqlDbType.BigInt) With {.Value = TotalAgreement})
            myCmd.Parameters.Add(New SqlParameter("@TotalInstallment", SqlDbType.Decimal) With {.Value = TotalInstallment})
            Dim prmErr = New SqlParameter("@SequenceNo", SqlDbType.SmallInt) With {.Direction = ParameterDirection.Output}
            myCmd.Parameters.Add(prmErr)
            objCon.Open()
            myCmd.Connection = objCon
            myCmd.CommandTimeout = 0
            myCmd.ExecuteNonQuery()
            objCon.Close()
            SequenceNo = CDbl(prmErr.Value)
        Catch ex As Exception
            Throw New Exception(ex.Message)
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub
    Sub UpdateLog(ByRef VirtualAccountID As String, ByRef SequenceNo As Integer)
        Try
            objCon = New SqlConnection(GetConnectionString)
            myCmd.Parameters.Clear()
            myCmd.CommandText = "spVirtualAccountLogUpdate"
            myCmd.CommandType = CommandType.StoredProcedure
            myCmd.Parameters.Add(New SqlParameter("@UploadDate", SqlDbType.DateTime) With {.Value = BusinessDate})
            myCmd.Parameters.Add(New SqlParameter("@VirtualAccountID", SqlDbType.VarChar, 10) With {.Value = VirtualAccountID})
            myCmd.Parameters.Add(New SqlParameter("@SequenceNo", SqlDbType.SmallInt) With {.Value = SequenceNo})

            objCon.Open()
            myCmd.Connection = objCon
            myCmd.CommandTimeout = 0
            myCmd.ExecuteNonQuery()
            objCon.Close()
        Catch ex As Exception
            Throw New Exception(ex.Message)
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub
    Private Sub btnReset_Click(sender As Object, e As System.EventArgs) Handles btnReset.Click
        Response.Redirect("Upload.aspx")
    End Sub
    Private Function GetValue(doc As SpreadsheetDocument, cell As Cell) As String
        Dim value As String = cell.CellValue.InnerText
        If cell.DataType IsNot Nothing AndAlso cell.DataType.Value = CellValues.SharedString Then
            Return doc.WorkbookPart.SharedStringTablePart.SharedStringTable.ChildElements.GetItem(Integer.Parse(value)).InnerText
        End If
        Return value
    End Function
    Private Function FileToDataTable() As DataTable
        'Create a new DataTable.
        Dim dt As New DataTable()

        Using doc As SpreadsheetDocument = SpreadsheetDocument.Open(FileName, False)
            'Read the first Sheet from Excel file.
            Dim sheet As Sheet = doc.WorkbookPart.Workbook.Sheets.GetFirstChild(Of Sheet)()

            'Get the Worksheet instance.
            Dim worksheet As Worksheet = TryCast(doc.WorkbookPart.GetPartById(sheet.Id.Value), WorksheetPart).Worksheet

            'Fetch all the rows present in the Worksheet.
            Dim rows As IEnumerable(Of Row) = worksheet.GetFirstChild(Of SheetData)().Descendants(Of Row)()

            'Loop through the Worksheet rows.
            For Each row As Row In rows
                'Use the first row to add columns to DataTable.
                If row.RowIndex.Value = 1 Then
                    Dim clm As Integer = 0
                    For Each cell As Cell In row.Descendants(Of Cell)()
                        If GetValue(doc, cell).Trim <> "" Then
                            dt.Columns.Add(GetValue(doc, cell))
                        Else
                            dt.Columns.Add(GetValue(doc, cell).Trim & clm.ToString)
                        End If
                        clm += 1
                    Next
                Else
                    'Add rows to DataTable.
                    dt.Rows.Add()
                    Dim i As Integer = 0
                    For Each cell As Cell In row.Descendants(Of Cell)()
                        dt.Rows(dt.Rows.Count - 1)(i) = GetValue(doc, cell)
                        i += 1
                    Next
                End If
            Next
        End Using
        Return dt
    End Function

    Private Function CSVToDataTable(ByVal path As String) As DataTable

        Dim lines = IO.File.ReadAllLines(path)
        Dim tbl As New DataTable
        tbl.Columns.Add(New DataColumn("ColumnA", GetType(String)))

        For Each line In lines
            If (line.Length > 0) Then
                Dim oRow As DataRow
                oRow = tbl.NewRow
                tbl.Rows.Add(line)
            End If
        Next

        Return tbl
    End Function

    Protected Sub cbVirtualAccount_SelectedIndexChanged(sender As Object, e As EventArgs)
        Dim VirtualAccountID As String = cbVirtualAccount.Items(cbVirtualAccount.SelectedIndex).Value.Trim
        'If (VirtualAccountID.Equals("BRI")) Then
        '    RequiredFieldValidator1.Enabled = False
        '    cbFormatText.Enabled = False
        'Else
        '    RequiredFieldValidator1.Enabled = True
        '    cbFormatText.Enabled = True
        'End If
    End Sub
End Class