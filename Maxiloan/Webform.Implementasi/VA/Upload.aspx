﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Upload.aspx.vb" Inherits="Maxiloan.Webform.Implementasi.Upload" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <style type="text/css">
        .progress_imgLoading
        {
            width: 4%;
            display: block;
            padding-top: 10px;
            padding-bottom: 20px;
            padding-left: 70px;
            background-image: url('../../Images/pic-loaderblack.gif'); 
            background-repeat: no-repeat;
            background-position: right;
            display: block;
        }
    </style>
</head>
<body>
    <%--<asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>--%>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <%--<asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>--%>
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
        onclick="hideMessage();"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                UPLOAD ANGSURAN VA
            </h3>
        </div>
    </div>
    <asp:Panel runat="server" ID="pnlHeader">
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    Jenis Virtual Account</label>
                <asp:DropDownList ID="cbVirtualAccount" runat="server" AutoPostBack="true" OnSelectedIndexChanged="cbVirtualAccount_SelectedIndexChanged">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" Display="Dynamic"
                    InitialValue="Selected One" ControlToValidate="cbVirtualAccount" ErrorMessage="Harap pilih jenis"
                    CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
<%--        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    Jenis Format Text</label>
                <asp:DropDownList ID="cbFormatText" runat="server">
                    <asp:ListItem Text="Selected One" Value="0" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="Format Text HARI INI" Value="1"></asp:ListItem>
                    <asp:ListItem Text="Format Text KEMARIN" Value="2"></asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic"
                    InitialValue="0" ControlToValidate="cbFormatText" ErrorMessage="Harap Pilih Format Text"
                    CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>--%>
        <div class="form_box">
            <div class="form_single">
                <label>
                    File</label>
                <asp:FileUpload ID="FileVA" runat="server" />
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnUpload" runat="server" Text="Upload" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="small button blue"
                CausesValidation="false"></asp:Button>
            <asp:Label runat="server" ID="lblLoading" class="progress_imgLoading"></asp:Label>
        </div>
    </asp:Panel>
    <%--</ContentTemplate>
    </asp:UpdatePanel>  --%>
    </form>
</body>
</html>
