﻿
Imports System.IO
Imports DocumentFormat.OpenXml
Imports DocumentFormat.OpenXml.Packaging
Imports DocumentFormat.OpenXml.Spreadsheet
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController


Public Class Register
    Inherits WebBased
    Protected WithEvents oBranch As ucBranchAll
    Protected WithEvents oSearBy As UcSearchBy
    Protected WithEvents GridNavigator As ucGridNav
    Protected WithEvents tglkontrak As ucDateCE
    Protected WithEvents tglkontrakTo As ucDateCE

    Private Property FileExcelName() As String
        Get
            Return (CType(ViewState("ParamReport"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("ParamReport") = Value
        End Set
    End Property


    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 15
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private gPagingController As New GeneralPagingController
    Private implementasiVaController As New ImplementasiVaController
    Private m_controller As New DataUserControlController
    '' GAVA 
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        FormID = "REGVA"
        If CheckForm(Loginid, FormID, AppId) Then
            lblMessage.Visible = False
            If SessionInvalid() Then
                Exit Sub
            End If
            AddHandler GridNavigator.PageChanged, AddressOf PageNavigation

            If Not IsPostBack Then
                If Request.QueryString("message") <> "" And Request.QueryString("msg") = "success" Then
                    ShowMessage(lblMessage, Request.QueryString("message"), False)
                End If

                oSearBy.ListData = "Name, Customer Name-Agreementno, No Kontrak-AssetDesc, Nama Asset-Licenseplate, License Plate"
                oSearBy.BindData()
                tglkontrak.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
                tglkontrak.IsRequired = True

                tglkontrakTo.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
                tglkontrakTo.IsRequired = True

                Dim dtbranch As New DataTable
                dtbranch = m_controller.GetBranchAll(GetConnectionString)
                With cbobranch
                    .DataTextField = "Name"
                    .DataValueField = "ID"
                    .DataSource = dtbranch
                    .DataBind()
                    .Items.Insert(0, "Select One")
                    .Items(0).Value = "0"
                End With
                cbobranch.Items.FindByValue(sesBranchId.Replace("'", "").Trim).Selected = True


                If CheckForm(Loginid, FormID, AppId) Then
                    SearchBy = " branchid = '" & cbobranch.SelectedItem.Value.Trim & "' and agreementdate >= cast('" & ConvertDate2(tglkontrak.Text) & "' as smalldatetime) and agreementdate <= cast('" & ConvertDate2(tglkontrakTo.Text) & "' as smalldatetime)"
                    SortBy = ""
                End If
                GetCombo()
                DoBind(False)
                FillBankNameToDataGrid()
            End If
        End If
    End Sub
    Protected Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        currentPage = e.CurrentPage
        DoBind(True)
        GridNavigator.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
    End Sub
    Private Sub GetCombo()
        '------------------
        'CBO DEPARTEMEN
        '------------------
        Dim dtVitualAcc As New DataTable

        dtVitualAcc = implementasiVaController.GetVirtualAccount(GetConnectionString).Tables(0)

        ddlRekening.DataTextField = "Name"
        ddlRekening.DataValueField = "ID"
        ddlRekening.DataSource = dtVitualAcc
        ddlRekening.DataBind()

        ddlRekening.Items.Insert(0, "Select One")
        ddlRekening.Items(0).Value = "0"

    End Sub

    Private Sub FillBankNameToDataGrid()

        Dim dtVitualAcc As New DataTable

        dtVitualAcc = implementasiVaController.GetVirtualAccount(GetConnectionString).Tables(0)

        dtgBankName.DataSource = dtVitualAcc
        dtgBankName.DataBind()

    End Sub

    Sub DoBind(Optional isFrNav As Boolean = False)

        Dim prm = New Parameter.GeneralPaging()

        prm.strConnection = GetConnectionString()
        prm.WhereCond = SearchBy
        prm.CurrentPage = currentPage
        prm.PageSize = pageSize
        prm.SortBy = SortBy
        ' prm  .BranchId =  cbobranch.SelectedItem.Value.Trim 
        prm.SpName = "spRegisterVirtualAccList"

        gPagingController = New GeneralPagingController
        Dim oPaging = gPagingController.GetGeneralPaging(prm)
        recordCount = IIf(Not oPaging Is Nothing, oPaging.TotalRecords, 0)
        DtgAsset.DataSource = oPaging.ListData.DefaultView

        DtgAsset.DataBind()

        If (isFrNav = False) Then
            GridNavigator.Initialize(recordCount, pageSize)
        End If
        pnlList.Visible = (recordCount > 0)


        'Dim filepath = FileExcelName
        'FileExcelName = ""
        'If filepath = Nothing Then Return
        'If (isFrNav = False And filepath.Trim() <> "") Then
        '    Dim resp As HttpResponse = HttpContext.Current.Response
        '    resp.ClearContent()
        '    resp.Clear()
        '    resp.ContentType = "application/msexcel"
        '    resp.AddHeader("Content-Disposition", "attachment; filename=" + filepath + ";")
        '    resp.TransmitFile(filepath)
        '    resp.Flush()
        '    resp.End()
        'End If

    End Sub

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        SortBy = String.Format("{0} {1}", e.SortExpression, IIf(InStr(SortBy, "DESC") > 0, "", "DESC"))
        DoBind()
    End Sub
	'modify ario 
    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Response.Redirect("Register.aspx")
    End Sub
	'end modify
    Private Sub btnsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        If SessionInvalid() Then
            Exit Sub
        End If

        'SearchBy = " branchid = '" & cbobranch.SelectedItem.Value.Trim & "' and agreementdate >= cast('" & ConvertDate2(tglkontrak.Text) & "' as smalldatetime) and agreementdate <= cast('" & ConvertDate2(tglkontrakTo.Text) & "' as smalldatetime)"
        'Modify by 20180321
        SearchBy = " branchid = '" & cbobranch.SelectedItem.Value.Trim & "' "

        If oSearBy.Text.Trim <> "" Then
            SearchBy = String.Format("{0} and {1} like '%{2}%'", SearchBy, oSearBy.ValueID, oSearBy.Text.Trim)
        End If

        SearchBy = String.Format("{0} and NoVirtualAcc {1} ''", SearchBy, IIf(rboStatusVa.SelectedValue = "True", "<>", "="))

        DoBind(False)
        '  End If
    End Sub

    Private Sub GetSelectedAgreement()
        Dim oDataTable As New DataTable
        oDataTable.Columns.Add("ApplicationId", GetType(String))
        oDataTable.Columns.Add("BranchId", GetType(String))

        For Each row As GridViewRow In DtgAsset.Rows
            If row.RowType = DataControlRowType.DataRow Then
                Dim chkRow As CheckBox = TryCast(row.Cells(0).FindControl("chkRow"), CheckBox)
                If chkRow.Checked Then
                    Dim oApplicationId As String = TryCast(row.Cells(1).FindControl("lblApplicationId"), Label).Text
                    Dim oBranchId As String = TryCast(row.Cells(1).FindControl("lblBranchId"), Label).Text

                    Dim oDataRow = oDataTable.NewRow
                    oDataRow("ApplicationId") = oApplicationId
                    oDataRow("BranchId") = oBranchId

                    oDataTable.Rows.Add(oDataRow)
                End If
            End If
        Next
    End Sub

    Private Sub doApprove(ByVal sender As Object, ByVal e As EventArgs) Handles btnRegister.Click
        Dim oDataTable As New DataTable
        oDataTable.Columns.Add("id", GetType(String))
        oDataTable.Columns.Add("value1", GetType(String))
        oDataTable.Columns.Add("value2", GetType(String))

        Dim oNumber As Int32
        oNumber = 1

        For Each row As GridViewRow In DtgAsset.Rows
            If row.RowType = DataControlRowType.DataRow Then
                Dim chkRow As CheckBox = TryCast(row.Cells(0).FindControl("chkUpdateStatus"), CheckBox)
                If chkRow.Checked Then
                    Dim oBranchId As String = TryCast(row.Cells(1).FindControl("lblBranchId"), Label).Text
                    Dim oApplicationId As String = TryCast(row.Cells(1).FindControl("lblApplicationId"), Label).Text

                    Dim oDataRow = oDataTable.NewRow
                    oDataRow("id") = oNumber.ToString
                    oDataRow("value1") = oBranchId
                    oDataRow("value2") = oApplicationId

                    oDataTable.Rows.Add(oDataRow)

                    oNumber += 1
                End If
            End If
        Next

        If (oDataTable.Rows.Count <= 0) Then
            ShowMessage(lblMessage, "Silahkan Pilih Kontrak yang akan di generate", False)
            Return
        End If

        Dim bankIds = (From msgRow As DataGridItem In dtgBankName.Items
                       Where (CType(msgRow.FindControl("chk"), CheckBox).Checked)
                       Select CType(msgRow.FindControl("lblBankId"), WebControls.Label).Text).ToList()

        If (bankIds.Count <= 0) Then
            ShowMessage(lblMessage, "Silahkan Pilih Nama Bank", False)
            Return
        End If

        Dim result = implementasiVaController.GenerateVitualAccount(GetConnectionString, bankIds.Cast(Of String)().ToList(), oDataTable)
        'generateExcell(result)
        generateCsv(result)
        ShowMessage(lblMessage, "Berhasil Register VA", False)
        DoBind(False)

    End Sub
    Function pathFile() As String
        Dim strDirectory As String = ""
        strDirectory = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\Implementasi\VA\"
        If Not Directory.Exists(strDirectory) Then
            Directory.CreateDirectory(strDirectory)
        End If
        Return strDirectory
    End Function

    Sub generateExcell(datas As IList(Of Parameter.GeneratedVaToExcellLog))
        Dim fName = String.Format("{0}VirtualAcc.xls", Guid.NewGuid())
        Dim filepath As String = String.Format("{0}{1}", pathFile(), fName)
        Dim spreadsheetDocument As SpreadsheetDocument = SpreadsheetDocument.Create(filepath, SpreadsheetDocumentType.Workbook)
        Dim workbookpart As WorkbookPart = spreadsheetDocument.AddWorkbookPart
        workbookpart.Workbook = New Workbook
        Dim worksheetPart As WorksheetPart = workbookpart.AddNewPart(Of WorksheetPart)()
        worksheetPart.Worksheet = New Worksheet(New SheetData())
        Dim sheets As Sheets = spreadsheetDocument.WorkbookPart.Workbook.AppendChild(New Sheets())
        Dim sheet As Sheet = New Sheet
        sheet.Id = spreadsheetDocument.WorkbookPart.GetIdOfPart(worksheetPart)
        sheet.SheetId = 1
        sheet.Name = "Sheet1"
        sheets.Append(sheet)
        Dim HeaderString As String = "nomor, nama, nomorvirtualbca, nomorvirtualbri, nokontrak, masaberlakudari, masaberlakusampai"
        Dim HeaderWords As String() = HeaderString.Split(New Char() {","c})
        Dim Row = New Row()
        Dim cell As Cell
        For Each Header As String In HeaderWords
            cell = New Cell()
            cell.DataType = CellValues.String
            cell.CellValue = New CellValue(Trim(Header))
            Row.AppendChild(cell)
        Next
        spreadsheetDocument.WorkbookPart.WorksheetParts.First().Worksheet.First().AppendChild(Row)

        For Each DataPart In datas
            Dim newRow = New Row()

            'For Each col As String In HeaderWords
            cell = New Cell()
            cell.DataType = CellValues.Number
            cell.CellValue = New CellValue(DataPart.Nomor.ToString())
            newRow.AppendChild(cell)

            cell = New Cell()
            cell.DataType = CellValues.String
            cell.CellValue = New CellValue(DataPart.Nama)
            newRow.AppendChild(cell)

            cell = New Cell()
            cell.DataType = CellValues.String
            cell.CellValue = New CellValue(DataPart.NomorVirtualBCA)
            newRow.AppendChild(cell)

            cell = New Cell()
            cell.DataType = CellValues.String
            cell.CellValue = New CellValue(DataPart.NomorVirtualBRI)
            newRow.AppendChild(cell)

            cell = New Cell()
            cell.DataType = CellValues.String
            cell.CellValue = New CellValue(DataPart.NoKontrak)
            newRow.AppendChild(cell)

            cell = New Cell()
            cell.DataType = CellValues.String
            cell.CellValue = New CellValue(DataPart.MasaBerlakuDari.ToString("dd/MM/yyyy"))
            newRow.AppendChild(cell)


            cell = New Cell()
            cell.DataType = CellValues.String
            cell.CellValue = New CellValue(DataPart.MasaBerlakuSampai.ToString("dd/MM/yyyy"))
            newRow.AppendChild(cell)

            'Next
            spreadsheetDocument.WorkbookPart.WorksheetParts.First().Worksheet.First().AppendChild(newRow)
        Next

        workbookpart.Workbook.Save()

        ' Close the document.
        spreadsheetDocument.Close()

        FileExcelName = filepath
        'Dim resp As System.Web.HttpResponse = System.Web.HttpContext.Current.Response
        ''resp.ClearContent()
        ''resp.Clear()
        'resp.ContentType = "application/msexcel"
        'resp.AddHeader("Content-Disposition", "attachment; filename=" + filepath + ";")
        'resp.TransmitFile(filepath)
        'resp.Flush()
        'resp.End()



        Dim strHTTPServer = Request.ServerVariables("PATH_INFO")
        Dim strNameServer = Request.ServerVariables("SERVER_NAME")
        Dim StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
        Dim strFileLocation = "http://" & strNameServer & "/" & StrHTTPApp & "/XML/Implementasi/VA/" & fName
        Response.Write("<script language = javascript> var x = 10; var y =  10; window.open('" & strFileLocation & "','accacq', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0')   </script>")

    End Sub

    Sub generateCsv(datas As IList(Of Parameter.GeneratedVaToExcellLog))
        Dim csvFileName As String
        Dim csvTime As String
        Dim csvFileCreation As String
        Dim csvFileCreation2 As String
        Dim csvRowsTotal As Integer
        Dim csvBankAccountTo As String

        csvTime = "_" & DateTime.Now.ToString("HHmmss")
        csvFileCreation = Date.Now.ToString("yyyymmdd") & "_" & DateTime.Now.ToString("HHmmss")
        csvFileCreation2 = Date.Now.ToString("yyyyMMdd")
        csvFileName = "UPLOUD TGL " & csvFileCreation2
        csvRowsTotal = datas.Count

        Dim strDirectory As String = ""
        strDirectory = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\Implementasi\VA\"
        If Not Directory.Exists(strDirectory) Then
            Directory.CreateDirectory(strDirectory)
        End If

        Dim FileName As String
        FileName = Request.ServerVariables("APPL_PHYSICAL_PATH") & "xml\Implementasi\VA\"
        FileName += csvFileName + ".csv"
        Response.Clear()
        Dim sw As New StreamWriter(FileName + "", False)

        'write header

        sw.Write(csvFileCreation & "," & csvFileName)
        sw.Write(sw.NewLine)
        sw.Write("P" & "," & csvRowsTotal)
        sw.Write(sw.NewLine)

        For Each DataPart In datas
            Dim newRow = New Row()

            sw.WriteLine(DataPart.NomorVA & "," & DataPart.NamaVA & "," & DataPart.IDVA & "," & DataPart.Code1 & "," & DataPart.Code2 & "," & DataPart.JenisInstruksi)
        Next
        sw.Close()

        Dim strHTTPServer = Request.ServerVariables("PATH_INFO")
        Dim strNameServer = Request.ServerVariables("SERVER_NAME")
        Dim StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
        Dim strFileLocation = "http://" & strNameServer & "/" & StrHTTPApp & "/xml/Implementasi/VA/" & csvFileName + ".csv"
        Response.Write("<script language = javascript> var x = 10; var y =  10; window.open('" & strFileLocation & "','accacq', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0')   </script>")
    End Sub
End Class
