﻿#Region "Import"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class UpdateNilaiDendaTetap
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oSearchBy As UcSearchBy
    Protected WithEvents oBranch As ucBranchAll
    Protected WithEvents txtNDT As ucNumberFormat
#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private mController As New ImplementasiControler
    Private cContract As New GeneralPagingController
    Private oContract As New Parameter.GeneralPaging
#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.FormID = "UPNDT"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If Request.QueryString("Message") <> "" Then

                    ShowMessage(lblMessage, Request.QueryString("Message"), True)
                End If
                oSearchBy.ListData = "Agreementno, Agreement No-Name, Name-Address, Address-InstallmentAmount, Installment"
                oSearchBy.BindData()
                Me.SearchBy = ""
                Me.SortBy = ""
                pnlDatagrid.Visible = False
                pnlList.Visible = True
                pnlUpdate.Visible = False
            End If
        End If
    End Sub

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        lblMessage.Visible = False
        lblMessage.Text = ""
        Dim intloop As Integer
        Dim hypID As HyperLink

        With oContract
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
            .SpName = "spPrepayRequestList"
        End With
        oContract = cContract.GetGeneralPaging(oContract)
        recordCount = oContract.TotalRecords
        DtgAgree.DataSource = oContract.ListData

        Try
            DtgAgree.DataBind()
        Catch
            DtgAgree.CurrentPageIndex = 0
            DtgAgree.DataBind()
        End Try
        PagingFooter()
        pnlDatagrid.Visible = True
        pnlList.Visible = True
        pnlUpdate.Visible = False
    End Sub

#End Region

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub

    Private Sub DtgAgree_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgree.ItemDataBound
        'Dim hypReceive As HyperLink
        Dim lblApplicationid As HyperLink
        Dim lblCustID As Label
        Dim lblName As HyperLink
        Dim lblAgreementNo As HyperLink
        If e.Item.ItemIndex >= 0 Then
            lblApplicationid = CType(e.Item.FindControl("lblApplicationid"), HyperLink)
            'hypReceive = CType(e.Item.FindControl("HypReceive"), HyperLink)
            'hypReceive.NavigateUrl = "PrepayRequest.aspx?applicationid=" & lblApplicationid.Text.Trim & "&branchid=" & oBranch.BranchID.Trim

            lblAgreementNo = CType(e.Item.FindControl("lblAgreementNo"), HyperLink)
            If lblAgreementNo.Text.Trim.Length > 0 Then
                lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(lblApplicationid.Text.Trim) & "')"
            End If

            If lblApplicationid.Text.Trim.Length > 0 Then
                lblApplicationid.NavigateUrl = "javascript:OpenApplicationId('" & "AccMnt" & "', '" & Server.UrlEncode(lblApplicationid.Text.Trim) & "')"
            End If
            lblName = CType(e.Item.FindControl("lblName"), HyperLink)
            lblCustID = CType(e.Item.FindControl("lblCustID"), Label)
            If lblCustID.Text.Trim.Length > 0 Then
                lblName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(lblCustID.Text.Trim) & "')"
            End If
        End If
    End Sub
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles DtgAgree.SortCommand
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

#End Region

    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        Me.SearchBy = ""
        oSearchBy.Text = ""
        oSearchBy.BindData()
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Buttonsearch.Click
        Me.SearchBy = " branchid = '" & oBranch.BranchID.Trim & "' "

        If oSearchBy.Text.Trim <> "" Then
            Me.SearchBy = Me.SearchBy & " and " & oSearchBy.ValueID & " LIKE '%" & oSearchBy.Text.Trim.Replace("'", "''") & "%'"
        End If
        pnlDatagrid.Visible = True
        pnlList.Visible = True
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
    Private Sub DtgAgree_itemcommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgAgree.ItemCommand
        If e.CommandName = "Update" Then
            With txtNDT
                .RequiredFieldValidatorEnable = True
                .RangeValidatorEnable = True
                .TextCssClass = "numberAlign medium_text"
            End With

            pnlDatagrid.Visible = False
            pnlList.Visible = False
            pnlUpdate.Visible = True

            Dim lblApplicationid As HyperLink
            Dim lblAgreementNo As HyperLink
            Dim lblName As HyperLink
            lblApplicationid = CType(DtgAgree.Items(e.Item.ItemIndex).FindControl("lblApplicationid"), HyperLink)
            lblAgreementNo = CType(DtgAgree.Items(e.Item.ItemIndex).FindControl("lblAgreementNo"), HyperLink)
            lblName = CType(DtgAgree.Items(e.Item.ItemIndex).FindControl("lblName"), HyperLink)


            Dim imp As New Parameter.Implementasi
            With imp
                .strConnection = GetConnectionString()
                .WhereCond = " BranchID='" & oBranch.BranchID.Trim & "' and Applicationid='" & lblApplicationid.Text.Trim & "'"
                .SPName = "spImGetOutstandingPrincipal"
            End With

            imp = mController.GetSP(imp)
            If imp.Listdata.Rows.Count > 0 Then
                txtNDT.Text = FormatNumber(imp.Listdata.Rows(0).Item("PrepaymentPenaltyFixed"), 0)
            End If

            hdApplicationID.Value = lblApplicationid.Text.Trim
            hdBranchID.Value = oBranch.BranchID.Trim
            lblNoKontrak.Text = lblAgreementNo.Text.Trim
            lblNama.Text = lblName.Text.Trim
        End If
    End Sub
    Private Sub ButtonSave_Click(sender As Object, e As System.EventArgs) Handles ButtonSave.Click
        Dim imp As New Parameter.Implementasi

        Try
            With imp
                .strConnection = GetConnectionString()
                .ApplicationID = hdApplicationID.Value
                .BranchId = hdBranchID.Value
                .NilaiNDT = CDec(txtNDT.Text)
            End With

            mController.UpdateNDT(imp)
            ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)

            pnlDatagrid.Visible = False
            pnlList.Visible = True
            pnlUpdate.Visible = False
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

    Private Sub ButtonCancel_Click(sender As Object, e As System.EventArgs) Handles ButtonCancel.Click
        Response.Redirect("UpdateNilaiDendaTetap.aspx")
    End Sub

End Class