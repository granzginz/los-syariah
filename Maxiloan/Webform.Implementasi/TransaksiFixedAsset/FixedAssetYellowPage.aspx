﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FixedAssetYellowPage.aspx.vb" Inherits="Maxiloan.Webform.Implementasi.FixedAssetYellowPage" %>

<!DOCTYPE html>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %>  
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Cetak Form Kuning Fixed Asset</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script src="../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script> 
    <script src="../../js/jquery-2.1.1.js" type="text/javascript"></script>	
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" />
    <asp:Label ID="lblMessage" runat="server" ToolTip="Click to close"  Visible="false" />
            <div class="form_title">
                <div class="title_strip"> </div>
                <div class="form_single">
                    <h3> CETAK FORM KUNING FIXED ASSET</h3>
                </div>
            </div>
            <asp:Panel ID="pnlSearch" runat="server">
                <div class="form_box">
                    <div class="form_single">
                    <label>
                        Jenis
                    </label>
                    <asp:DropDownList runat="server" ID="ddJenis"  onchange="change()">
                        <asp:ListItem runat="server" Value="PAYREQ" Text="Payment Request" Selected="True"></asp:ListItem>
                        <%--<asp:ListItem runat="server" Value="REQREC" Text="Request Receive" Enabled="false"></asp:ListItem>--%>
                        <asp:ListItem runat="server" Value="SELLFA" Text="Penjualan"></asp:ListItem>
                        <asp:ListItem runat="server" Value="DELFA" Text="Penghapusan"></asp:ListItem>
                    </asp:DropDownList>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">                   
                        <div id="ddsrcpayreqf">
                            <label>
                                Cari Berdasarkan
                            </label> 
                                <asp:DropDownList ID="ddsrcpayreq" runat="server" >
                                    <asp:ListItem Value="">Semua</asp:ListItem>
                                    <asp:ListItem Value="FAP.InvoiceNo">Invoice</asp:ListItem>
                                </asp:DropDownList>
                                <asp:TextBox runat="server" ID="txtsrcbypayreq"></asp:TextBox>
                        </div>
                        <div id="ddsrcreqrecf">
                            <label>
                                Cari Berdasarkan
                            </label> 
                                <asp:DropDownList ID="ddsrcreqrec" runat="server" >
                                    <asp:ListItem Value="">Semua</asp:ListItem>
                                    <asp:ListItem Value="RFA.AktivaId">Aktiva Id</asp:ListItem>
                                </asp:DropDownList>
                                <asp:TextBox runat="server" ID="txtsrcbyreqrec"></asp:TextBox>
                        </div>
                        <div id="ddsrcsellfaf">
                            <label>
                                Cari Berdasarkan
                            </label> 
                                <asp:DropDownList ID="ddsrcsellfa" runat="server" >
                                    <asp:ListItem Value="">Semua</asp:ListItem>
                                    <asp:ListItem Value="RFA.AktivaId">Aktiva Id</asp:ListItem>
                                </asp:DropDownList>
                                <asp:TextBox runat="server" ID="txtsrcbysellfa"></asp:TextBox>
                        </div>
                        <div id="ddsrcdelfaf">
                            <label>
                                Cari Berdasarkan
                            </label> 
                                <asp:DropDownList ID="ddsrcdelfa" runat="server" >
                                    <asp:ListItem Value="">Semua</asp:ListItem>
                                    <asp:ListItem Value="RFA.AktivaId">Aktiva Id</asp:ListItem>
                                </asp:DropDownList>
                                <asp:TextBox runat="server" ID="txtsrcbydelfa"></asp:TextBox>
                        </div>
                    </div>
                </div>             
                <div class="form_button">
                    <asp:Button ID="Buttonsearch" runat="server" Text="Search" CssClass="small button blue" />
                    <asp:Button ID="ButtonReset" runat="server" Text="Reset" CssClass="small button gray" CausesValidation="False" />
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlDatagrid" runat="server" Visible="false">
                 <div class="form_box_title" style="float:left">
                 &nbsp&nbsp&nbsp<asp:Label ID="label1" runat="server">Request</asp:Label><asp:DropDownList ID="author1" runat="server" Width ="7%" CssClass="medium_text"/> 
                 &nbsp&nbsp&nbsp<asp:Label ID="label2" runat="server">BM</asp:Label><asp:DropDownList ID="author2" runat="server" Width ="7%" CssClass="medium_text"/> 
                 &nbsp&nbsp&nbsp<asp:Label ID="label3" runat="server">GM</asp:Label><asp:DropDownList ID="author3" runat="server" Width ="7%" CssClass="medium_text"/>
                 &nbsp&nbsp&nbsp<asp:Label ID="label4" runat="server">Direksi</asp:Label><asp:DropDownList ID="author4" runat="server" Width ="7%" CssClass="medium_text"/>
                 </div>
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            DAFTAR TRANSAKSI FIXED ASSET
                        </h4>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="DtgPayreq" runat="server" CssClass="grid_general" AutoGenerateColumns="False"
                                OnSortCommand="SortGrid" AllowSorting="True" Visible="False">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <FooterStyle CssClass="item_grid" />
                                <Columns>
                                 <asp:TemplateColumn>
                                    <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Left" Width="1%"></ItemStyle>
                                    <HeaderTemplate>
                                            <asp:CheckBox ID="cbAll" Visible="True" runat="server" OnCheckedChanged="checkAll" AutoPostBack="True" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="cbCheck" Visible="True" runat="server" />
                                    </ItemTemplate>
                                   </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="InvoiceNo" HeaderText="NO INVOICE">
                                        <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="5%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblInvoiceNo" runat="server" Text='<%#Container.DataItem("InvoiceNo")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="InvoiceDate" SortExpression="InvoiceDate" HeaderText="TGL INVOICE"
                                    DataFormatString="{0:dd/MM/yyyy}" Visible="True" ItemStyle-Width="10%"></asp:BoundColumn>
                                    <asp:TemplateColumn Visible="True" SortExpression="SupplierBankId" HeaderText="BANK ID">
                                        <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="5%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSupplierBankId" runat="server" Text='<%#Container.DataItem("SupplierBankId")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="Notes" HeaderText="NOTES">
                                        <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="10%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblNotes" runat="server" Text='<%#Container.DataItem("Notes")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn Visible="False" SortExpression="SupplierBankAccountNo" HeaderText="BANK ACCOUNT NO">
                                        <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="10%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSupplierBankAccountNo" runat="server" Text='<%#Container.DataItem("SupplierBankAccountNo")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="SupplierBankAccountName" HeaderText="BANK ACCOUNT NAME">
                                        <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="10%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSupplierBankAccountName" runat="server" Text='<%#Container.DataItem("SupplierBankAccountName")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="PaymentOrderDate" SortExpression="PaymentOrderDate" HeaderText="TGL PAYMENT ORDER"
                                    DataFormatString="{0:dd/MM/yyyy}" Visible="True" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                                    <asp:TemplateColumn SortExpression="Jumlah" HeaderText="JUMLAH">
                                        <HeaderStyle CssClass="item_grid_right" HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                        <ItemStyle CssClass="item_grid_right" HorizontalAlign="Right" Width="5%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblJumlah" runat="server" Text='<%#FormatNumber(Container.DataItem("Jumlah"), 0)%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="Status" HeaderText="STATUS">
                                        <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center" Width="5%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblStatus" runat="server" Text='<%#Container.DataItem("ALLSTATUS")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                                <PagerStyle Visible="false" HorizontalAlign="Left" Mode="NumericPages"></PagerStyle>
                            </asp:DataGrid>

                            <asp:DataGrid ID="DtgReqrec" runat="server" CssClass="grid_general" AutoGenerateColumns="False"
                                OnSortCommand="SortGrid" AllowSorting="True" Visible="False">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <FooterStyle CssClass="item_grid" />
                                <Columns>
                                 <asp:TemplateColumn>
                                    <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Left" Width="1%"></ItemStyle>
                                    <HeaderTemplate>
                                            <asp:CheckBox ID="cbAll" Visible="True" runat="server" OnCheckedChanged="checkAll" AutoPostBack="True" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="cbCheck" Visible="True" runat="server" />
                                    </ItemTemplate>
                                   </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="WaiveNo" HeaderText="AKTIVA ID">
                                        <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="13%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblAktivaId" runat="server" Text='<%#Container.DataItem("AktivaId")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn Visible="False" SortExpression="Buyer" HeaderText="BUYER">
                                        <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="15%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblBuyer" runat="server" Text='<%#Container.DataItem("Buyer")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="InternalMemoNo" HeaderText="NO MEMO INTERNAL">
                                        <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="10%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:label ID="lblInternalMemoNo" runat="server" Text='<%#Container.DataItem("InternalMemoNo")%>'>
                                            </asp:label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="Notes" HeaderText="NOTES">
                                        <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="10%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:label ID="lblNotes" runat="server" Text='<%#Container.DataItem("Notes")%>'>
                                            </asp:label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="Amount" HeaderText="JUMLAH">
                                        <HeaderStyle CssClass="item_grid_right" HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                        <ItemStyle CssClass="item_grid_right" HorizontalAlign="Right" Width="5%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblAmount" runat="server" Text='<%#FormatNumber(Container.DataItem("Amount"), 0)%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="Status" HeaderText="STATUS">
                                        <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center" Width="5%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblStatus" runat="server" Text='<%#Container.DataItem("ALLSTATUS")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                                <PagerStyle Visible="False" HorizontalAlign="Left" Mode="NumericPages"></PagerStyle>
                            </asp:DataGrid>

                            <asp:DataGrid ID="DtgSellfa" runat="server" CssClass="grid_general" AutoGenerateColumns="False"
                                OnSortCommand="SortGrid" AllowSorting="True" Visible="False">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <FooterStyle CssClass="item_grid" />
                                <Columns>
                                 <asp:TemplateColumn>
                                    <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Left" Width="1%"></ItemStyle>
                                    <HeaderTemplate>
                                            <asp:CheckBox ID="cbAll" Visible="True" runat="server" OnCheckedChanged="checkAll" AutoPostBack="True" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="cbCheck" Visible="True" runat="server" />
                                    </ItemTemplate>
                                   </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="WaiveNo" HeaderText="AKTIVA ID">
                                        <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="13%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblAktivaId" runat="server" Text='<%#Container.DataItem("AktivaId")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn Visible="False" SortExpression="Buyer" HeaderText="BUYER">
                                        <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="15%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblBuyer" runat="server" Text='<%#Container.DataItem("Buyer")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="InternalMemoNo" HeaderText="NO MEMO INTERNAL">
                                        <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="10%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:label ID="lblInternalMemoNo" runat="server" Text='<%#Container.DataItem("InternalMemoNo")%>'>
                                            </asp:label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="Notes" HeaderText="NOTES">
                                        <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="10%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:label ID="lblNotes" runat="server" Text='<%#Container.DataItem("Notes")%>'>
                                            </asp:label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="Amount" HeaderText="JUMLAH">
                                        <HeaderStyle CssClass="item_grid_right" HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                        <ItemStyle CssClass="item_grid_right" HorizontalAlign="Right" Width="5%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblAmount" runat="server" Text='<%#FormatNumber(Container.DataItem("Amount"), 0)%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="Status" HeaderText="STATUS">
                                        <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center" Width="5%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblStatus" runat="server" Text='<%#Container.DataItem("ALLSTATUS")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                                <PagerStyle Visible="False" HorizontalAlign="Left" Mode="NumericPages"></PagerStyle>
                            </asp:DataGrid>

                            <asp:DataGrid ID="DtgDelfa" runat="server" CssClass="grid_general" AutoGenerateColumns="False"
                                OnSortCommand="SortGrid" AllowSorting="True" Visible="False">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <FooterStyle CssClass="item_grid" />
                                <Columns>
                                 <asp:TemplateColumn>
                                    <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Left" Width="1%"></ItemStyle>
                                    <HeaderTemplate>
                                            <asp:CheckBox ID="cbAll" Visible="True" runat="server" OnCheckedChanged="checkAll" AutoPostBack="True" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="cbCheck" Visible="True" runat="server" />
                                    </ItemTemplate>
                                   </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="WaiveNo" HeaderText="AKTIVA ID">
                                        <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="13%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblAktivaId" runat="server" Text='<%#Container.DataItem("AktivaId")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn Visible="False" SortExpression="Buyer" HeaderText="BUYER">
                                        <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="15%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblBuyer" runat="server" Text='<%#Container.DataItem("Buyer")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="InternalMemoNo" HeaderText="NO MEMO INTERNAL">
                                        <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="10%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:label ID="lblInternalMemoNo" runat="server" Text='<%#Container.DataItem("InternalMemoNo")%>'>
                                            </asp:label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="Notes" HeaderText="NOTES">
                                        <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="10%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:label ID="lblNotes" runat="server" Text='<%#Container.DataItem("Notes")%>'>
                                            </asp:label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="Amount" HeaderText="JUMLAH">
                                        <HeaderStyle CssClass="item_grid_right" HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                        <ItemStyle CssClass="item_grid_right" HorizontalAlign="Right" Width="5%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblAmount" runat="server" Text='<%#FormatNumber(Container.DataItem("Amount"), 0)%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="Status" HeaderText="STATUS">
                                        <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center" Width="5%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblStatus" runat="server" Text='<%#Container.DataItem("ALLSTATUS")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                                <PagerStyle Visible="False" HorizontalAlign="Left" Mode="NumericPages"></PagerStyle>
                            </asp:DataGrid>
                            <uc2:ucGridNav id="GridNav" runat="server"/>
                        </div>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="BtnNext" runat="server" CausesValidation="False" Visible="True" Text="Next" CssClass="small button green"></asp:Button>
                </div>
            </asp:Panel>
    </form>
</body>
<script type="text/javascript">
    $(document).ready(function () {
        change();
    });

    function change() {
        var DropdownList = document.getElementById('<%=ddJenis.ClientID %>');
        var SelectedIndex = DropdownList.selectedIndex;
        var ddsrcpayreq = document.getElementById('ddsrcpayreqf');
        var ddsrcreqrec = document.getElementById('ddsrcreqrecf');
        var ddsrcsellfa = document.getElementById('ddsrcsellfaf');
        var ddsrcdelfa = document.getElementById('ddsrcdelfaf');

        console.log(SelectedIndex)

        if (SelectedIndex = ddsrcpayreq) {
            ddsrcpayreq.style.display = "block";
            ddsrcreqrec.style.display = "none";
            ddsrcsellfa.style.display = "none";
            ddsrcdelfa.style.display = "none";

        }
        if (SelectedIndex = ddsrcreqrec) {
            ddsrcpayreq.style.display = "none";
            ddsrcreqrec.style.display = "block";
            ddsrcsellfa.style.display = "none";
            ddsrcdelfa.style.display = "none";

        }
        if (SelectedIndex = ddsrcsellfa) {
            ddsrcsellfa.style.display = "block";
            ddsrcpayreq.style.display = "none";
            ddsrcreqrec.style.display = "none";
            ddsrcdelfa.style.display = "none";

        }
        if (SelectedIndex = ddsrcdelfa) {
            ddsrcdelfa.style.display = "block";
            ddsrcpayreq.style.display = "none";
            ddsrcreqrec.style.display = "none";
            ddsrcsellfa.style.display = "none";
            
        }
    }
</script>  
</html>
