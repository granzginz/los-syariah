﻿Imports System.Data.SqlClient
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General
Imports Maxiloan.General.CommonCacheHelper

Public Class PenjualanFA
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents txtAmount As ucNumberFormat
    Protected WithEvents txtTanggalTransaksi As ucDateCE
    Protected WithEvents UcBranchAddress As ucBankAccountOther
#Region "Private Constanta"
    Private oController As New DataUserControlController
    Private m_controller As New PaymentRequestController
    Dim oCustomClass As New Parameter.PaymentRequest
    Dim tempTotalPRAmount As Double
    Private Const SCHEME_ID As String = "PYR"
#End Region
#Region "Property"
    Private Property Amount() As Double
        Get
            Return CDbl(txtAmount.Text)
        End Get
        Set(value As Double)
            txtAmount.Text = value
        End Set
    End Property
    Private Property Tanggal() As String
        Get
            Return txtTanggalTransaksi.Text
        End Get
        Set(ByVal value As String)
            txtTanggalTransaksi.Text = value
        End Set
    End Property
    Private Property TotPRAmount() As Double
        Get
            Return (CType(ViewState("TotPRAmount"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("TotPRAmount") = Value
        End Set
    End Property

    Private Property PRAmount() As Double
        Get
            Return (CType(ViewState("PRAmount"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("PRAmount") = Value
        End Set
    End Property

    Private Property BankAccount() As String
        Get
            Return (CType(ViewState("BankAccount"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("BankAccount") = Value
        End Set
    End Property

    Private Property Departement() As String
        Get
            Return (CType(ViewState("Departement"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("Departement") = Value
        End Set
    End Property

    Private Property Description() As String
        Get
            Return txtDescription.Text
        End Get
        Set(ByVal Value As String)
            txtDescription.Text = Value
        End Set
    End Property
    Private Property Memo() As String
        Get
            Return NoMemo.Text + txtMemo.Text
        End Get
        Set(value As String)
            txtMemo.Text = value
        End Set
    End Property
    Private Property PRDataTable() As DataTable
        Get
            Return (CType(ViewState("PRDataTable"), DataTable))
        End Get
        Set(ByVal Value As DataTable)
            ViewState("PRDataTable") = Value
        End Set
    End Property

    Public Property IsRequired As Boolean
        Get
            Return CBool(ViewState("Validation"))
        End Get
        Set(ByVal Value As Boolean)
            ViewState("Validation") = Value
        End Set
    End Property
    Private Property BankID() As String
        Get
            Return (CType(ViewState("BankID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("BankID") = Value
        End Set
    End Property
    Private Property InvoiceNo() As String
        Get
            Return (CType(ViewState("InvoiceNo"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("InvoiceNo") = Value
        End Set
    End Property

    ''New props
    Private Property SupplierId() As String
        Get
            Return (CType(ViewState("SupplierId"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("SupplierId") = Value
        End Set
    End Property
    Private Property SupplierBankId() As String
        Get
            Return (CType(ViewState("SupplierBankId"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("SupplierBankId") = Value
        End Set
    End Property
    Private Property SupplierBankBranch() As String
        Get
            Return (CType(ViewState("SupplierBankBranch"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("SupplierBankBranch") = Value
        End Set
    End Property
    Private Property SupplierBankAccountNo() As String
        Get
            Return (CType(ViewState("SupplierBankAccountNo"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("SupplierBankAccountNo") = Value
        End Set
    End Property
    Private Property SupplierBankAccountName() As String
        Get
            Return (CType(ViewState("SupplierBankAccountName"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("SupplierBankAccountName") = Value
        End Set
    End Property
    Private Property VendorInvoiceNo() As String
        Get
            Return (CType(ViewState("VendorInvoiceNo"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("VendorInvoiceNo") = Value
        End Set
    End Property
    Private Property InvoiceDate() As Date
        Get
            Return (CType(ViewState("InvoiceDate"), Date))
        End Get
        Set(ByVal Value As Date)
            ViewState("InvoiceDate") = Value
        End Set
    End Property
    Private Property PaymentOrderDate() As String
        Get
            Return (CType(ViewState("PaymentOrderDate"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("PaymentOrderDate") = Value
        End Set
    End Property
    Private Property CaraBayar() As String
        Get
            Return (CType(ViewState("CaraBayar"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("CaraBayar") = Value
        End Set
    End Property
    Private Property Jumlah() As Double
        Get
            Return (CType(ViewState("Jumlah"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("Jumlah") = Value
        End Set
    End Property
    Private Property RefferenceNo() As String
        Get
            Return (CType(ViewState("RefferenceNo"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("RefferenceNo") = Value
        End Set
    End Property
    Private Property Notes() As String
        Get
            Return (CType(ViewState("Notes"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("Notes") = Value
        End Set
    End Property
    Private Property PaymentAllocationId() As String
        Get
            Return (CType(ViewState("PaymentAllocationId"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("PaymentAllocationId") = Value
        End Set
    End Property
    Private Property RequestDate() As Date
        Get
            Return (CType(ViewState("RequestDate"), Date))
        End Get
        Set(ByVal Value As Date)
            ViewState("RequestDate") = Value
        End Set
    End Property
    Private Property Status() As String
        Get
            Return (CType(ViewState("Status"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("Status") = Value
        End Set
    End Property
#End Region
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        If Me.IsHoBranch = False Then
            NotAuthorized()
            Exit Sub
        End If
        Me.FormID = "SELLFA"
        If Not Me.IsPostBack Then
            If Request.QueryString("strFileLocation") <> "" Then
                Dim strHTTPServer As String
                Dim StrHTTPApp As String
                Dim strNameServer As String
                Dim strFileLocation As String

                strHTTPServer = Request.ServerVariables("PATH_INFO")
                strNameServer = Request.ServerVariables("SERVER_NAME")
                StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                strFileLocation = "http://" & strNameServer & "/" & StrHTTPApp & "/XML/" & Request.QueryString("strFileLocation") & ".pdf"

                Response.Write("<script language = javascript>" & vbCrLf _
                & "var x = screen.width; " & vbCrLf _
                & "var y = screen.height; " & vbCrLf _
                & "window.open('" & strFileLocation & "','Installment', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes') " & vbCrLf _
                & "</script>")

            End If
            If Request.QueryString("msg") = "ok" Then
                ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_SUCCESS, False)
            End If
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If SessionInvalid() Then
                    Exit Sub
                End If
                Me.SearchBy = ""
                GetCombo()
                BindBankAccount()
                fillRekBankCbo()
                Dim pstrFile As String
                pstrFile = Me.Session.SessionID + Me.Loginid + Me.BranchID
                oCustomClass.hpsxml = "1"
                oCustomClass = m_controller.GetTableTransaction(oCustomClass, pstrFile)


                ''''GET INVOICE NO
                Me.InvoiceNo = Request.QueryString("InvoiceNo")
                GetPaymentRequest()

                'pnlGrid.Visible = False
                txtAmount.RequiredFieldValidatorEnable = True
                txtAmount.RangeValidatorEnable = True
                txtAmount.TextCssClass = "numberAlign regular_text"
                txtTanggalTransaksi.Text = Me.BusinessDate.ToString("dd/MM/yyyy")

            End If
            NoMemo.Text = Me.sesBranchId.Replace("'", "") + "/" + "PD/" + Me.BusinessDate.ToString("yyyyMM") + "/"
        End If
    End Sub
    Private Sub GetPaymentRequest()
        Dim oCustomClass As New Parameter.TransFixedAsset
        Dim oController As New ImplementasiControler
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        Dim dtEntity As New DataTable
        Dim currentPage As Int32 = 1
        Dim pageSize As Int16 = 10

        If SessionInvalid() Then
            Exit Sub
        End If

        Me.SearchBy = Me.SearchBy & " b.AktivaID = '" & Me.InvoiceNo.Trim & "' "

        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereCond = Me.SearchBy
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = ""
        End With

        oCustomClass = oController.PenjualanFAList(oCustomClass)

        DtUserList = oCustomClass.ListData

        ''''Get All data FA_PaymentRequest''''
        Me.InvoiceNo = DtUserList.Rows(0)("InvoiceNo").ToString().Trim
        'Me.SupplierId = DtUserList.Rows(0)("VendorId").ToString().Trim
        'Me.SupplierBankId = DtUserList.Rows(0)("VendorBankId").ToString().Trim
        'Me.SupplierBankBranch = DtUserList.Rows(0)("VendorBankBranch").ToString().Trim
        'Me.SupplierBankAccountNo = DtUserList.Rows(0)("VendorBankAccountNo").ToString().Trim
        'Me.SupplierBankAccountName = DtUserList.Rows(0)("VendorBankAccountName").ToString().Trim
        'Me.VendorInvoiceNo = DtUserList.Rows(0)("VendorInvoiceNo").ToString().Trim
        Me.InvoiceDate = DtUserList.Rows(0)("InvoiceDate").ToString().Trim
        'Me.PaymentOrderDate = DtUserList.Rows(0)("PaymentOrderDate").ToString().Trim
        'Me.CaraBayar = DtUserList.Rows(0)("CaraBayar").ToString().Trim
        Me.Jumlah = DtUserList.Rows(0)("Jumlah").ToString().Trim
        Me.RefferenceNo = DtUserList.Rows(0)("RefferenceNo").ToString().Trim
        Me.Notes = DtUserList.Rows(0)("Notes").ToString().Trim
        Me.PaymentAllocationId = DtUserList.Rows(0)("PaymentAllocationId").ToString().Trim
        Me.RequestDate = DtUserList.Rows(0)("RequestDate").ToString().Trim
        Me.Status = DtUserList.Rows(0)("Status").ToString().Trim



        TxtNamaRekening.Text = DtUserList.Rows(0)("VendorBankAccountName").ToString().Trim
        TxtNoRekening.Text = DtUserList.Rows(0)("VendorBankAccountNo").ToString().Trim
        txtAmount.Text = FormatNumber(DtUserList.Rows(0)("Jumlah").ToString().Trim, 0)
        cboBankName.SelectedValue = DtUserList.Rows(0)("VendorBankId").ToString().Trim
        txtDescription.Text = DtUserList.Rows(0)("Notes").ToString().Trim


    End Sub
    Private Sub InitialDefaultPanel()
        txtAmount_.Visible = False
        txtDepartement_.Visible = False
        txtTanggalTransaksi_.Visible = False
        txtDescription_.Visible = False
        txtMemo.Enabled = True
        txtSandiBank.Visible = False
        TxtNamaRekening.Enabled = False
        TxtNoRekening.Enabled = False
        cboBankName.Enabled = False
        txtAmount.Visible = True
        cboDepartement.Visible = True
        cboBankName.Visible = True
        txtDescription.Visible = True
        txtTanggalTransaksi.Visible = True
        pnlLookup.Visible = True
        Panel2.Visible = True
        pnlGrid.Visible = True
        pnlApprovalReq.Visible = True
        ButtonDelete.Visible = True
        pnlbtn.Visible = True
    End Sub

#Region "GetCombo"
    Private Sub GetCombo()
        Dim dtDepartement As New DataTable
        dtDepartement = oController.GetDepartement(GetConnectionString)
        cboDepartement.DataTextField = "Name"
        cboDepartement.DataValueField = "ID"
        cboDepartement.DataSource = dtDepartement
        cboDepartement.DataBind()

        cboDepartement.Items.Insert(0, "Select One")
        cboDepartement.Items(0).Value = "0"

        cboDepartement.Items.Insert(1, "None")
        cboDepartement.Items(1).Value = "-"
    End Sub

    Public Sub BindBankAccount()
        Dim DtBankName As DataTable
        DtBankName = oController.GetBankName(GetConnectionString)
        cboBankName.DataValueField = "ID"
        cboBankName.DataTextField = "Name"
        cboBankName.DataSource = DtBankName
        cboBankName.DataBind()
        cboBankName.Items.Insert(0, "Select One")
        cboBankName.Items(0).Value = "0"
        cboBankName.SelectedIndex = 0
    End Sub

    Sub fillRekBankCbo()
        Dim oController As New APDisbAppController
        Dim ds = oController.LoadComboRekeningBank(GetConnectionString, "BA")
        ddlRekeningBank.DataSource = ds
        ddlRekeningBank.DataTextField = "Text"
        ddlRekeningBank.DataValueField = "Value"
        ddlRekeningBank.DataBind()
        ddlRekeningBank.Items.Insert("0", "Select One")
    End Sub
#End Region

#Region "DtgPRList_ItemDataBound"
    Private Sub DtgPRList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgPRList.ItemDataBound
        If SessionInvalid() Then
            Exit Sub
        End If
        Dim lblTotPRAmount As New TextBox
        Dim lblPRAmount As TextBox
        With oCustomClass
            If e.Item.ItemIndex >= 0 Then
                lblPRAmount = CType(e.Item.FindControl("txtAmountTrans"), TextBox)
                tempTotalPRAmount += CDbl(lblPRAmount.Text)
            End If

            If e.Item.ItemType = ListItemType.Footer Then
                lblTotPRAmount = CType(e.Item.FindControl("txtTotTransAmount"), TextBox)
                lblTotPRAmount.Text = FormatNumber(tempTotalPRAmount.ToString, 2)
                Me.TotPRAmount = CDbl(lblTotPRAmount.Text.Trim)
            End If

        End With
    End Sub
#End Region
    Private Sub btnJlookup_Click(sender As Object, e As System.EventArgs) Handles btnJLookup.Click
        pnlGrid.Visible = True
        If cboDepartement.SelectedValue.Trim <> "0" Then
            Dim Pays() As String = vsPay.Value.Split(",")
            Dim Descs() As String = vsDesc.Value.Split(",")
            Dim Jumlahs() As String = vsJumlah.Value.Split(",")

            If Pays.Length > 0 Then
                txtJLookup.Text = "Sudah dipilih!"
                For index = 0 To Pays.Length - 1
                    Dim Pay As String = Pays(index).Replace("'", "")
                    Dim Desc As String = Descs(index).Replace("'", "")
                    Dim Jumlah As String = Jumlahs(index).Replace("'", "")

                    AddRecord(Pay, Desc, CInt(Jumlah))
                Next
                RFVtxtJLookup.Visible = False
            Else
                txtJLookup.Text = "Belum dipilih!"
            End If

        Else
            ShowMessage(lblMessage, "Departemen Harus diisi!", True)
            Exit Sub
        End If

    End Sub
    Private Sub Jlookup_Click(sender As Object, e As System.EventArgs) Handles Jlookup.Click
        pnlGrid.Visible = True
        If cboDepartement.SelectedValue.Trim <> "0" Then
            Dim Pays() As String = vsPay.Value.Split(",")
            Dim Descs() As String = vsDesc.Value.Split(",")
            Dim Jumlahs() As String = vsJumlah.Value.Split(",")

            If Pays.Length > 0 Then
                txtJLookup.Text = "Sudah dipilih!"
                For index = 0 To Pays.Length - 1
                    Dim Pay As String = Pays(index).Replace("'", "")
                    Dim Desc As String = Descs(index).Replace("'", "")
                    Dim Jumlah As String = Jumlahs(index).Replace("'", "")

                    AddRecord(Pay, Desc, CInt(Jumlah))
                Next
                RFVtxtJLookup.Visible = False
            Else
                txtJLookup.Text = "Belum dipilih!"
            End If

        Else
            ShowMessage(lblMessage, "Departemen Harus diisi!", True)
            Exit Sub
        End If

    End Sub
    Public Sub AddRecord(ByVal ID As String, ByVal Desc As String, ByVal i As Integer)
        Dim objectDataTable As New DataTable
        Dim intLoopGrid As Integer
        Dim oRow As DataRow

        Dim chk As New CheckBox
        Dim lblDepartmentID As New Label
        Dim lblDepartmentName As New Label
        Dim lblPaymentAllocID As New Label
        Dim lblPaymentAllocDesc As New Label
        Dim txtKeterangan As New TextBox
        Dim txtAmountTrans As New TextBox
        Dim chkIsPotong As New CheckBox

        With objectDataTable
            .Columns.Add(New DataColumn("chk", GetType(String)))
            .Columns.Add(New DataColumn("DepartmentID", GetType(String)))
            .Columns.Add(New DataColumn("DepartmentName", GetType(String)))
            .Columns.Add(New DataColumn("PaymentAllocationID", GetType(String)))
            .Columns.Add(New DataColumn("PaymentAllocationName", GetType(String)))
            .Columns.Add(New DataColumn("txtKeterangan", GetType(String)))
            .Columns.Add(New DataColumn("txtAmountTrans", GetType(String)))
            .Columns.Add(New DataColumn("chkIsPotong", GetType(String)))
        End With

        For intLoopGrid = 0 To DtgPRList.Items.Count - 1
            chk = CType(DtgPRList.Items(intLoopGrid).FindControl("chk"), CheckBox)
            lblDepartmentID = CType(DtgPRList.Items(intLoopGrid).FindControl("lblDepartmentID"), Label)
            lblDepartmentName = CType(DtgPRList.Items(intLoopGrid).FindControl("lblDepartmentName"), Label)
            lblPaymentAllocID = CType(DtgPRList.Items(intLoopGrid).FindControl("lblPaymentAllocID"), Label)
            lblPaymentAllocDesc = CType(DtgPRList.Items(intLoopGrid).FindControl("lblPaymentAllocDesc"), Label)
            txtKeterangan = CType(DtgPRList.Items(intLoopGrid).FindControl("txtKeterangan"), TextBox)
            txtAmountTrans = CType(DtgPRList.Items(intLoopGrid).FindControl("txtAmountTrans"), TextBox)
            chkIsPotong = CType(DtgPRList.Items(intLoopGrid).FindControl("chkIsPotong"), CheckBox)



            '----- Add row -------'
            oRow = objectDataTable.NewRow()
            oRow("chk") = CType(chk.Checked, String)
            oRow("DepartmentID") = CType(lblDepartmentID.Text, String)
            oRow("DepartmentName") = CType(lblDepartmentName.Text, String)
            oRow("PaymentAllocationID") = CType(lblPaymentAllocID.Text, String)
            oRow("PaymentAllocationName") = CType(lblPaymentAllocDesc.Text, String)
            oRow("txtKeterangan") = CType(txtKeterangan.Text, String)
            oRow("txtAmountTrans") = CType(txtAmountTrans.Text, String)
            oRow("chkIsPotong") = CType(chkIsPotong.Checked, String)

            objectDataTable.Rows.Add(oRow)
        Next

        If i = 0 Then
            i = i + 1
        End If


        For index = 1 To i
            oRow = objectDataTable.NewRow()
            oRow("chk") = "False"
            oRow("DepartmentID") = cboDepartement.SelectedValue
            oRow("DepartmentName") = cboDepartement.SelectedItem.Text
            oRow("PaymentAllocationID") = ID
            oRow("PaymentAllocationName") = Desc
            oRow("txtKeterangan") = ""
            oRow("txtAmountTrans") = "0"
            oRow("chkIsPotong") = "False"

            objectDataTable.Rows.Add(oRow)
        Next

        DtgPRList.DataSource = objectDataTable
        DtgPRList.DataBind()

        For intLoopGrid = 0 To DtgPRList.Items.Count - 1
            chk = CType(DtgPRList.Items(intLoopGrid).FindControl("chk"), CheckBox)
            lblDepartmentID = CType(DtgPRList.Items(intLoopGrid).FindControl("lblDepartmentID"), Label)
            lblDepartmentName = CType(DtgPRList.Items(intLoopGrid).FindControl("lblDepartmentName"), Label)
            lblPaymentAllocID = CType(DtgPRList.Items(intLoopGrid).FindControl("lblPaymentAllocID"), Label)
            lblPaymentAllocDesc = CType(DtgPRList.Items(intLoopGrid).FindControl("lblPaymentAllocDesc"), Label)
            txtKeterangan = CType(DtgPRList.Items(intLoopGrid).FindControl("txtKeterangan"), TextBox)
            txtAmountTrans = CType(DtgPRList.Items(intLoopGrid).FindControl("txtAmountTrans"), TextBox)
            chkIsPotong = CType(DtgPRList.Items(intLoopGrid).FindControl("chkIsPotong"), CheckBox)

            chk.Checked = CBool(objectDataTable.Rows(intLoopGrid).Item(0).ToString.Trim)
            lblDepartmentID.Text = objectDataTable.Rows(intLoopGrid).Item(1).ToString.Trim
            lblDepartmentName.Text = objectDataTable.Rows(intLoopGrid).Item(2).ToString.Trim
            lblPaymentAllocID.Text = objectDataTable.Rows(intLoopGrid).Item(3).ToString.Trim
            lblPaymentAllocDesc.Text = objectDataTable.Rows(intLoopGrid).Item(4).ToString.Trim
            txtKeterangan.Text = objectDataTable.Rows(intLoopGrid).Item(5).ToString.Trim
            txtAmountTrans.Text = FormatNumber(objectDataTable.Rows(intLoopGrid).Item(6).ToString.Trim, 0)
            chkIsPotong.Checked = CBool(objectDataTable.Rows(intLoopGrid).Item(7).ToString.Trim)
            'chkIsPotong.Attributes.Add("OnChange", " klikpotong('" & chkIsPotong.ClientID & "','" & txtAmountTrans.ClientID & "')")
        Next

        ''* add otomatis
        txtAmountTrans.Text = txtAmount.Text
        chk.Checked = True
        chkIsPotong.Checked = True
        chkIsPotong.Enabled = False

        ScriptManager.RegisterStartupScript(DtgPRList, GetType(DataGrid), DtgPRList.ClientID, String.Format(" total(); ", DtgPRList.ClientID), True)
    End Sub


    Private Sub btnNext1_Click(sender As Object, e As EventArgs) Handles btnNext1.Click
        If cboDepartement.SelectedIndex <> 0 Then

            '''' Menampilkan APVENDOR ''''
            Dim Pays() As String = vsPay.Value.Split(",")
            Dim Descs() As String = vsDesc.Value.Split(",")
            Dim Jumlahs() As String = vsJumlah.Value.Split(",")
            pnlGrid.Visible = True
            If Pays.Length > 0 Then
                txtJLookup.Text = "Sudah dipilih!"
                For index = 0 To Pays.Length - 1
                    Dim Pay As String = "APVENDOR"
                    Dim Desc As String = "Account Payable - Vendor"
                    Dim Jumlah As String = 1

                    AddRecord(Pay, Desc, CInt(Jumlah))
                Next
                RFVtxtJLookup.Visible = False
            Else
                txtJLookup.Text = "Belum dipilih!"
            End If
            btnNext1.Visible = False
            lblMessage.Visible = False

            cboDepartement.Enabled = False
            ddlRekeningBank.Enabled = False
            txtAmount.Enabled = False
            cboDepartement.Enabled = False
            cboBankName.Enabled = False
            txtDescription.Enabled = False
            txtTanggalTransaksi.Enabled = False
            txtMemo.Enabled = False
            TxtNamaRekening.Enabled = False
            TxtNoRekening.Enabled = False

            ButtonRevisi.Visible = True
            pnlApprovalReq.Visible = True
        Else
            ShowMessage(lblMessage, "Departemen Harus diisi!", True)
            Exit Sub
        End If
    End Sub

    Private Sub ButtonRevisi_Click(sender As Object, e As EventArgs) Handles ButtonRevisi.Click
        pnlGrid.Visible = False
        pnlApprovalReq.Visible = False
        txtAmount.Enabled = True
        cboDepartement.Enabled = True
        cboBankName.Enabled = True
        txtDescription.Enabled = True
        txtTanggalTransaksi.Enabled = True
        txtMemo.Enabled = True
        TxtNamaRekening.Enabled = True
        TxtNoRekening.Enabled = True

        BtnNext1.Visible = True

        ''* call hapus baris coa karena hanya butuh 1 baris coa *''
        DeleteBaris(CInt(0))

    End Sub

    Public Sub DeleteBaris(ByVal Index As Integer)
        Dim intLoopGrid As Integer
        Dim oRow As DataRow
        Dim chk As New CheckBox
        Dim lblDepartmentID As New Label
        Dim lblDepartmentName As New Label
        Dim lblPaymentAllocID As New Label
        Dim lblPaymentAllocDesc As New Label
        Dim txtKeterangan As New TextBox
        Dim txtAmountTrans As New TextBox
        Dim chkIsPotong As New CheckBox

        Dim oNewDataTable As New DataTable


        With oNewDataTable
            .Columns.Add(New DataColumn("chk", GetType(String)))
            .Columns.Add(New DataColumn("DepartmentID", GetType(String)))
            .Columns.Add(New DataColumn("DepartmentName", GetType(String)))
            .Columns.Add(New DataColumn("PaymentAllocationID", GetType(String)))
            .Columns.Add(New DataColumn("PaymentAllocationName", GetType(String)))
            .Columns.Add(New DataColumn("txtKeterangan", GetType(String)))
            .Columns.Add(New DataColumn("txtAmountTrans", GetType(String)))
            .Columns.Add(New DataColumn("chkIsPotong", GetType(String)))
        End With

        For intLoopGrid = 0 To DtgPRList.Items.Count - 1
            chk = CType(DtgPRList.Items(intLoopGrid).FindControl("chk"), CheckBox)
            lblDepartmentID = CType(DtgPRList.Items(intLoopGrid).FindControl("lblDepartmentID"), Label)
            lblDepartmentName = CType(DtgPRList.Items(intLoopGrid).FindControl("lblDepartmentName"), Label)
            lblPaymentAllocID = CType(DtgPRList.Items(intLoopGrid).FindControl("lblPaymentAllocID"), Label)
            lblPaymentAllocDesc = CType(DtgPRList.Items(intLoopGrid).FindControl("lblPaymentAllocDesc"), Label)
            txtKeterangan = CType(DtgPRList.Items(intLoopGrid).FindControl("txtKeterangan"), TextBox)
            txtAmountTrans = CType(DtgPRList.Items(intLoopGrid).FindControl("txtAmountTrans"), TextBox)
            chkIsPotong = CType(DtgPRList.Items(intLoopGrid).FindControl("chkIsPotong"), CheckBox)

            '----- Add row -------'
            If intLoopGrid <> Index And Not chk.Checked Then
                oRow = oNewDataTable.NewRow()
                oRow("chk") = CType(chk.Checked, String)
                oRow("DepartmentID") = CType(lblDepartmentID.Text, String)
                oRow("DepartmentName") = CType(lblDepartmentName.Text, String)
                oRow("PaymentAllocationID") = CType(lblPaymentAllocID.Text, String)
                oRow("PaymentAllocationName") = CType(lblPaymentAllocDesc.Text, String)
                oRow("txtKeterangan") = CType(txtKeterangan.Text, String)
                oRow("txtAmountTrans") = CType(txtAmountTrans.Text, String)
                oRow("chkIsPotong") = CType(chkIsPotong.Checked, String)

                oNewDataTable.Rows.Add(oRow)
            End If
        Next
        DtgPRList.DataSource = oNewDataTable
        DtgPRList.DataBind()

        For intLoopGrid = 0 To DtgPRList.Items.Count - 1
            chk = CType(DtgPRList.Items(intLoopGrid).FindControl("chk"), CheckBox)
            lblDepartmentID = CType(DtgPRList.Items(intLoopGrid).FindControl("lblDepartmentID"), Label)
            lblDepartmentName = CType(DtgPRList.Items(intLoopGrid).FindControl("lblDepartmentName"), Label)
            lblPaymentAllocID = CType(DtgPRList.Items(intLoopGrid).FindControl("lblPaymentAllocID"), Label)
            lblPaymentAllocDesc = CType(DtgPRList.Items(intLoopGrid).FindControl("lblPaymentAllocDesc"), Label)
            txtKeterangan = CType(DtgPRList.Items(intLoopGrid).FindControl("txtKeterangan"), TextBox)
            txtAmountTrans = CType(DtgPRList.Items(intLoopGrid).FindControl("txtAmountTrans"), TextBox)
            chkIsPotong = CType(DtgPRList.Items(intLoopGrid).FindControl("chkIsPotong"), CheckBox)

            chk.Checked = CBool(oNewDataTable.Rows(intLoopGrid).Item(0).ToString.Trim)
            lblDepartmentID.Text = oNewDataTable.Rows(intLoopGrid).Item(1).ToString.Trim
            lblDepartmentName.Text = oNewDataTable.Rows(intLoopGrid).Item(2).ToString.Trim
            lblPaymentAllocID.Text = oNewDataTable.Rows(intLoopGrid).Item(3).ToString.Trim
            lblPaymentAllocDesc.Text = oNewDataTable.Rows(intLoopGrid).Item(4).ToString.Trim
            txtKeterangan.Text = oNewDataTable.Rows(intLoopGrid).Item(5).ToString.Trim
            txtAmountTrans.Text = FormatNumber(oNewDataTable.Rows(intLoopGrid).Item(6).ToString.Trim, 0)
            chkIsPotong.Checked = CBool(oNewDataTable.Rows(intLoopGrid).Item(7).ToString.Trim)
        Next

        ScriptManager.RegisterStartupScript(DtgPRList, GetType(DataGrid), DtgPRList.ClientID, String.Format(" total(); ", DtgPRList.ClientID), True)
    End Sub

    Private Sub imgCancelApproval_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCancelApproval.Click
        Dim pstrFile As String
        pstrFile = Me.Session.SessionID + Me.Loginid + Me.BranchID
        oCustomClass.hpsxml = "1"
        oCustomClass = m_controller.GetTableTransaction(oCustomClass, pstrFile)

        Server.Transfer("PenjualanFA.aspx")

    End Sub

    Private Sub ButtonSaveApproval_Click(sender As Object, e As EventArgs) Handles ButtonSaveApproval.Click
        If SessionInvalid() Then
            Exit Sub
        End If
        If CheckFeature(Me.Loginid, Me.FormID, "SAVE", Me.AppId) Then
            Dim strRequestNo As String
            Dim CustomClass As New Parameter.PaymentRequest
            CustomClass.BranchId = Me.sesBranchId.Replace("'", "")
            CustomClass.BusinessDate = Convert.ToDateTime(ConvertDate2(txtTanggalTransaksi.Text))
            CustomClass.ID = "SELLFA"
            CustomClass.strConnection = GetConnectionString()
            strRequestNo = m_controller.Get_RequestNo(CustomClass)

            Dim oData As New DataTable
            Dim txtAmountTrans As TextBox
            Dim totalGrid As Double
            Dim lblDepartmentID As New Label
            Dim lblDepartmentName As New Label
            Dim lblPaymentAllocID As New Label
            Dim lblPaymentAllocDesc As New Label
            Dim txtKeterangan As New TextBox
            Dim chkPotong As New CheckBox

            For Each item As DataGridItem In DtgPRList.Items
                lblDepartmentID = CType(item.FindControl("lblDepartmentID"), Label)
                lblDepartmentName = CType(item.FindControl("lblDepartmentName"), Label)
                lblPaymentAllocID = CType(item.FindControl("lblPaymentAllocID"), Label)
                lblPaymentAllocDesc = CType(item.FindControl("lblPaymentAllocDesc"), Label)
                txtKeterangan = CType(item.FindControl("txtKeterangan"), TextBox)
                txtAmountTrans = CType(item.FindControl("txtAmountTrans"), TextBox)
                chkPotong = CType(item.FindControl("chkIsPotong"), CheckBox)

                totalGrid += CDbl(txtAmountTrans.Text)
            Next

            If Amount <> totalGrid Then
                ShowMessage(lblMessage, "Total Nilai harus sama dengan Total Permintaan pembayaran", True)
                Exit Sub
            End If

            Dim oCustomClass As New Parameter.TransFixedAsset
            Dim oController As New ImplementasiControler
            oCustomClass.strConnection = GetConnectionString()

            Dim bank As String() = ddlRekeningBank.SelectedValue.Split(";")

            With oCustomClass
                .InvoiceNo = Me.InvoiceNo
                .BranchId = Me.sesBranchId.Replace("'", "")
                .PaymentAllocationID = Me.PaymentAllocationId
                If chkPotong.Checked = True Then
                    .Post = "C"
                Else
                    .Post = "D"
                End If
                .Amount = CDbl(txtAmount.Text.Trim())
                .RefDesc = txtKeterangan.Text.Trim()
                .VoucherDesc = txtDescription.Text.Trim()
                .ValueDate = Convert.ToDateTime(ConvertDate2(txtTanggalTransaksi.Text))
                .LoginId = Me.Loginid
                .BankAccountID = bank(1)
                .Departement = lblDepartmentName.Text
                .ReceivedFrom = TxtNamaRekening.Text.Trim()
                .ReferenceNo = Me.Memo
                .ApprovalNo = strRequestNo
            End With

            Dim ErrMsg As String
            oController.PaymentRequestFASave(oCustomClass)
            With oCustomClass
                ErrMsg = .strError
            End With
            If ErrMsg = "" Then
                ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_SUCCESS, False)
            ElseIf ErrMsg = "Double" Then
                ShowMessage(lblMessage, "Nomor Memo sudah digunakan, harap periksa kembali...!!", True)
                InitialDefaultPanel()
                Exit Sub
            Else
                ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_FAILED, True)
                InitialDefaultPanel()
                Exit Sub
            End If

            Response.Redirect("PenjualanFAList.aspx?msg=ok")
        End If
    End Sub
    Sub NotAuthorized()
        Dim strHTTPServer As String
        Dim StrHTTPApp As String
        Dim strNameServer As String
        strHTTPServer = Request.ServerVariables("PATH_INFO")
        strNameServer = Request.ServerVariables("SERVER_NAME")
        StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
        Response.Redirect("http://" & strNameServer & "/" & StrHTTPApp & "/error_notauthorized.aspx")
    End Sub
End Class