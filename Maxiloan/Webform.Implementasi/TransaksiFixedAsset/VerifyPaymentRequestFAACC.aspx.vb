﻿

Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.General.CommonCookiesHelper

Public Class VerifyPaymentRequestFAACC
    Inherits Maxiloan.Webform.AccMntWebBased
    Protected WithEvents GridNavigator As ucGridNav
    Protected WithEvents oBranch As UcBranch
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.PaymentRequest
    Private oController As New Controller.PaymentRequestController
    'Protected WithEvents oTrans As ucLookUpTransaction
    Private m_TotalAmount As Double
    Private Const SCHEME_ID As String = "PRFA"
    Private oClass As New Parameter.ControlsRS

#Region "Property"
    Private Property TransactionName() As String
        Get
            Return (CType(ViewState("TransactionName"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("TransactionName") = Value
        End Set
    End Property
    Private Property PaymentAllocationID() As String
        Get
            Return (CType(ViewState("PaymentAllocationID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("PaymentAllocationID") = Value
        End Set
    End Property
    Private Property AddEdit() As String
        Get
            Return CType(ViewState("vwsAddEdt"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsAddEdt") = Value
        End Set
    End Property
    Private Property NoIndex() As Integer
        Get
            Return CType(ViewState("NoIndex"), Integer)
        End Get
        Set(value As Integer)
            ViewState("NoIndex") = value
        End Set
    End Property
    Private Property Amount() As Double
        Get
            Return (CType(ViewState("Amount"), Double))
        End Get
        Set(value As Double)
            ViewState("Amount") = value
        End Set
    End Property
    Private Property JenisRequest() As String
        Get
            Return (CType(ViewState("JenisRequest"), String))
        End Get
        Set(value As String)
            ViewState("JenisRequest") = value
        End Set
    End Property
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If

        Me.FormID = "VERPAYREQUESTFAACC"
        If Me.IsHoBranch = False Then
            NotAuthorized()
            Exit Sub
        End If
        AddHandler GridNavigator.PageChanged, AddressOf PageNavigation

        If Not IsPostBack Then
            If Request.QueryString("message") <> "" Then
                ShowMessage(lblMessage, Request.QueryString("message"), True)
            End If



            With cboParent
                .DataSource = (New DataUserControlController).GetBranchAll(GetConnectionString)
                .DataValueField = "ID"
                .DataTextField = "Name"
                .DataBind()
                .Items.Insert(0, "ALL")
                .Items(0).Value = "ALL"
            End With

            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.SearchBy = ""
                Me.SortBy = ""
            End If

            'oTrans.IsPaymentRequest = "1"
            'oTrans.BindData()

            If Request("thingstodo") = "1" Then
                Dim par As String
                par = ""

                Dim strFilterBy As String
                strFilterBy = ""
                SearchBy = ""
                Me.BranchID = cboParent.SelectedItem.Value.Trim
                Me.SearchBy = String.Format(" PR.Status = 'N' ")
                'strFilterBy = String.Format("Status = CAB ")
                Me.SearchBy = String.Format(" PR.Status = 'N' ")

                pnlDatagrid.Visible = True
                DtgAgree.Visible = True
                pnlSearch.Visible = True
                PanelKoreksi.Visible = False
                DoBind()
            End If

            If Request.QueryString("Page") <> "" Then
                cboParent.SelectedValue = Request.QueryString("BranchID")
                txtDate.Text = Request.QueryString("TglReq")
                imgsearch_Click(sender, e)
            End If

        End If
    End Sub
    Sub NotAuthorized()
        Dim strHTTPServer As String
        Dim StrHTTPApp As String
        Dim strNameServer As String
        strHTTPServer = Request.ServerVariables("PATH_INFO")
        strNameServer = Request.ServerVariables("SERVER_NAME")
        StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
        Response.Redirect("http://" & strNameServer & "/" & StrHTTPApp & "/error_notauthorized.aspx")
    End Sub

    Private Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        currentPage = e.CurrentPage
        DoBind(True)
        GridNavigator.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
    End Sub


    Sub DoBind(Optional isFrNav As Boolean = False)
        Try
            With oCustomClass
                .strConnection = GetConnectionString()
                .WhereCond = Me.SearchBy
                .CurrentPage = currentPage
                .PageSize = pageSize
                .SortBy = SortBy
            End With

            If Me.JenisRequest = "Request" Then
                Dim listData = oController.GetPaymentRequestPagingFA(oCustomClass).ListData

                If listData Is Nothing Then
                    Throw New Exception("No record found. Search conditions: " & Me.SearchBy)
                End If

                recordCount = oCustomClass.TotalRecord
                listData.DefaultView.Sort = Me.SortBy
                DtgAgree.DataSource = listData.DefaultView

            ElseIf Me.JenisRequest = "Receive" Then
                Dim listData = oController.GetPaymentReceivePaging(oCustomClass).ListData

                If listData Is Nothing Then
                    Throw New Exception("No record found. Search conditions: " & Me.SearchBy)
                End If

                recordCount = oCustomClass.TotalRecord
                listData.DefaultView.Sort = Me.SortBy
                DtgAgree.DataSource = listData.DefaultView

            ElseIf Me.JenisRequest = "Penjualan" Then
                Dim listData = oController.GetPenjualanPaging(oCustomClass).ListData

                If listData Is Nothing Then
                    Throw New Exception("No record found. Search conditions: " & Me.SearchBy)
                End If

                recordCount = oCustomClass.TotalRecord
                listData.DefaultView.Sort = Me.SortBy
                DtgAgree.DataSource = listData.DefaultView

            ElseIf Me.JenisRequest = "Penghapusan" Then
                Dim listData = oController.GetPenghapusanPaging(oCustomClass).ListData

                If listData Is Nothing Then
                    Throw New Exception("No record found. Search conditions: " & Me.SearchBy)
                End If

                recordCount = oCustomClass.TotalRecord
                listData.DefaultView.Sort = Me.SortBy
                DtgAgree.DataSource = listData.DefaultView

            End If


            Try
                DtgAgree.DataBind()
            Catch
                DtgAgree.CurrentPageIndex = 0
                DtgAgree.DataBind()
            End Try

            If (isFrNav = False) Then
                GridNavigator.Initialize(recordCount, pageSize)
            End If

            pnlSearch.Visible = True
            pnlDatagrid.Visible = True


        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message + " " + ex.Source + " " + ex.StackTrace, True)
        End Try
    End Sub


    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        Me.SearchBy = ""
        oBranch.DataBind()
        cboSearchBy.SelectedIndex = 0
        txtSearchBy.Text = ""
        pnlDatagrid.Visible = False
    End Sub


    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Buttonsearch.Click
        Dim par As String
        par = ""

        Dim strFilterBy As String
        strFilterBy = ""
        SearchBy = ""
        Me.BranchID = cboParent.SelectedItem.Value.Trim
        Me.JenisRequest = cboJenisRequest.SelectedItem.Value
        Me.SearchBy = String.Format(" PR.StatusPembayaran = 'R' ", Me.BranchID)
        'strFilterBy = String.Format("Status = CAB ")

        If cboSearchBy.SelectedItem.Value <> "0" Then
            Me.SearchBy = String.Format("{0} and {1} Like '%{2}%' ", Me.SearchBy, cboSearchBy.SelectedItem.Value.Trim, txtSearchBy.Text.Trim)
            strFilterBy = cboSearchBy.SelectedItem.Text & " = " & txtSearchBy.Text.Trim

        End If

        If cboParent.SelectedValue.Trim <> "ALL" Then
            Me.SearchBy = String.Format(" PR.branchid = '{0}' AND PR.StatusPembayaran = 'R' ", Me.BranchID)
        End If

        If txtDate.Text.Trim.Length > 0 Then
            Me.SearchBy = String.Format("{0} and RequestDate = '{1}'", Me.SearchBy, ConvertDate2(txtDate.Text.Trim))
            strFilterBy = strFilterBy & ", RequestDate = " & txtDate.Text.Trim
        End If
        pnlDatagrid.Visible = True
        DtgAgree.Visible = True
        pnlSearch.Visible = True
        PanelKoreksi.Visible = False
        DoBind()
    End Sub

    Private Sub DtgAgree_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles DtgAgree.SortCommand
        Me.SortBy = String.Format("{0} {1}", e.SortExpression, IIf(InStr(Me.SortBy, "DESC") > 0, "ASC", "DESC"))
    End Sub

    Private Sub DtgAgree_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgree.ItemDataBound
        Dim lblTemp As Label

        If e.Item.ItemIndex >= 0 Then
            lblTemp = CType(e.Item.FindControl("lblAmount"), Label)
            If Not lblTemp Is Nothing Then
                m_TotalAmount += CType(lblTemp.Text, Double)
            End If
        End If
        If e.Item.ItemType = ListItemType.Footer Then
            lblTemp = CType(e.Item.FindControl("lblTotalAmount"), Label)
            If Not lblTemp Is Nothing Then
                lblTemp.Text = FormatNumber(m_TotalAmount.ToString, 2)
            End If
        End If
    End Sub
    Private Sub dtgList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgAgree.ItemCommand
        Try
            If e.CommandName.Trim = "Verifikasi" Then
                Dim hyRequestNo = CType(e.Item.FindControl("lblRequestNo"), Label).Text.Trim
                pnlVerify.Visible = True
                PanelKoreksi.Visible = False
                pnlSearch.Visible = False
                pnlDatagrid.Visible = False
                SetValueToLabel(hyRequestNo)
                pnlbutton.Visible = False
                lblMessage.Attributes.Add("style", "display:none")
                cboStatus.SelectedValue = "1"
                Me.BranchID = CType(e.Item.FindControl("lblBranchID"), Label).Text.Trim
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

    Private Sub SetValueToLabel(RequestNo As String)
        Try
            lblMessage.Text = ""
            With oCustomClass
                .BranchId = Me.BranchID.Trim.Replace("'", "")
                .RequestNo = RequestNo
                .strConnection = GetConnectionString()
            End With

            If Me.JenisRequest = "Request" Then
                oCustomClass = oController.GetPaymentRequestHeaderAndDetailFA(oCustomClass)
            ElseIf Me.JenisRequest = "Receive" Then
                oCustomClass = oController.GetPaymentReceiveHeaderAndDetailFA(oCustomClass)
            ElseIf Me.JenisRequest = "Penjualan" Then
                oCustomClass = oController.GetPenjualanHeaderAndDetailFA(oCustomClass)
            ElseIf Me.JenisRequest = "Penghapusan" Then
                oCustomClass = oController.GetPenghapusanHeaderAndDetailFA(oCustomClass)
            End If

            If oCustomClass Is Nothing Then
                ShowMessage(lblMessage, "No Record Found..", True)
                Exit Sub
            End If

            With oCustomClass
                '*** HEADER
                lblRequestNo.Text = .RequestNo
                lblBranchRequest.Text = .BranchName
                lblBankAccount.Text = .BankAccount
                lblNamaRek.Text = .NamaRekening
                lblNoRek.Text = .NoRekening
                lblDescription.Text = .Description
                lblDepartement.Text = .Departement
                lblDate.Text = .RequestDate.ToString("dd/MM/yyyy")
                lblAmount.Text = FormatNumber(.TotalAmount, 2)
                lblMemo.Text = .NoMemo
                '*** DETAIL
                FillGrid(.ListData)

                '*** FOOTER
                'lblTransferRefVoucherNo.Text = .TransferRefVoucherNo
                'lblTransferRefNo.Text = .TransferReferenceNo
                'lblTransferDate.Text = .TransferDate.ToString("dd/MM/yyyy")
                'lblTransferAmount.Text = FormatNumber(.TransferAmount, 2)
                lblStatus.Text = .Status
                lblRequestBy.Text = .RequestBy
                lblRequestNo1.Text = .RequestNo
                lblStatusDate.Text = .StatusDate.ToString("dd/MM/yyyy")

                '*** DETAIL HISTORY REJECT
                If .Num <> 0 Then
                    panelHistory.Visible = True
                    FillGridHistory(.ListDataHistory)
                Else
                    panelHistory.Visible = False
                End If


                '*** DETAIL HISTORY APPROVE
                If .NumApprove <> 0 Then
                    panelHistory.Visible = True
                    FillGridHistoryApprove(.ListDataHistoryApproval)
                Else
                    panelHistoryApprove.Visible = False
                End If

            End With
            Amount = oCustomClass.TotalAmount
            Dim oData = Get_UserApproval(SCHEME_ID, Replace(Me.sesBranchId, "'", ""), oCustomClass.TotalAmount)
            cboApprovedBy.DataSource = oData.DefaultView
            cboApprovedBy.DataTextField = "Name"
            cboApprovedBy.DataValueField = "ID"
            cboApprovedBy.DataBind()
            cboApprovedBy.Items.Insert(0, "Select One")
            cboApprovedBy.Items(0).Value = "0"

        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message + " " + ex.StackTrace, True)
        End Try
    End Sub
    Private Sub DtgAgreeVerify_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgAgreeVerify.ItemCommand
        Dim err As String
        Dim lbljumlahgrid As Label
        Dim lblKetNotes As Label
        Dim lblPaymentAllocId As Label
        Dim lblTransName As Label
        Dim lblTanggal As Label
        Dim lblCOA As Label

        If e.CommandName = "Edit" Then
            Me.AddEdit = "EDIT"
            lblRequestNo1.Text = lblRequestNo.Text
            lblTglStatus.Text = lblDate.Text
            lblKet.Text = lblDescription.Text
            lbljumlahgrid = CType(e.Item.FindControl("lblRequestAmount"), Label)
            lblKetNotes = CType(e.Item.FindControl("lblNotes"), Label)
            lblPaymentAllocId = CType(e.Item.FindControl("lblPaymentAllocation"), Label)
            lblTransName = CType(e.Item.FindControl("hyPettyCashNo"), Label)
            lblTanggal = CType(e.Item.FindControl("lbltgl"), Label)
            lblJum.Text = lbljumlahgrid.Text
            lblCOA = CType(e.Item.FindControl("lblCOA"), Label)
            'oTrans.Amount = lbljumlahgrid.Text
            'oTrans.EnableAmount = False
            'oTrans.Description = lblKetNotes.Text
            'oTrans.Transaction = lblTransName.Text
            'oTrans.TransactionID = lblPaymentAllocId.Text
            'oTrans.EnabledValutaDate = False
            'oTrans.ValutaDate = lblTanggal.Text
            'oTrans.COA = lblCOA.Text

            Me.NoIndex = e.Item.ItemIndex
        End If
        pnlSearch.Visible = False
        pnlDatagrid.Visible = False
        pnlVerify.Visible = False
        PanelKoreksi.Visible = True
    End Sub
    Sub FillGrid(ByRef oTable As DataTable)
        Dim DtUserList As DataTable
        Dim DvUserList As DataView
        Try
            DtUserList = oTable
            DvUserList = DtUserList.DefaultView
            DvUserList.Sort = Me.SortBy
            DtgAgreeVerify.DataSource = DvUserList
            Try
                DtgAgreeVerify.DataBind()
            Catch
                DtgAgreeVerify.CurrentPageIndex = 0
                DtgAgreeVerify.DataBind()
            End Try
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message + " " + ex.StackTrace, True)
        End Try
    End Sub
    Sub FillGridHistory(ByRef oTable As DataTable)
        Dim DtUserList As DataTable
        Dim DvUserList As DataView
        Try
            DtUserList = oTable
            DvUserList = DtUserList.DefaultView
            DvUserList.Sort = Me.SortBy
            DtgAgreeHistory.DataSource = DvUserList
            Try
                DtgAgreeHistory.DataBind()
            Catch
                DtgAgreeHistory.CurrentPageIndex = 0
                DtgAgreeHistory.DataBind()
            End Try
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message + " " + ex.StackTrace, True)
        End Try
    End Sub

    Sub FillGridHistoryApprove(ByRef oTable As DataTable)
        Dim DtUserList As DataTable
        Dim DvUserList As DataView
        Try
            DtUserList = oTable
            DvUserList = DtUserList.DefaultView
            DvUserList.Sort = Me.SortBy
            DtgAgreeHistoryApprove.DataSource = DvUserList
            Try
                DtgAgreeHistoryApprove.DataBind()
            Catch
                DtgAgreeHistoryApprove.CurrentPageIndex = 0
                DtgAgreeHistoryApprove.DataBind()
            End Try
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message + " " + ex.StackTrace, True)
        End Try
    End Sub

    Dim m_TotalTransferAmount = 0
    Dim m_TotalRequestAmount = 0
    Private Sub DtgAgreeVerify_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgreeVerify.ItemDataBound
        Dim lbltemp As Label
        Dim lblTotal As Label

        If e.Item.ItemIndex >= 0 Then
            Dim hyPaymetRequestNo = CType(e.Item.FindControl("lblRequestNo"), HyperLink)
            hyPaymetRequestNo.NavigateUrl = String.Format("javascript:OpenWinViewPRInquiryEdit('ACCMNT','{0}')", hyPaymetRequestNo.Text.Trim)

            lbltemp = CType(e.Item.FindControl("lblTransferAmountDetail"), Label)
            If Not lbltemp Is Nothing Then
                m_TotalTransferAmount += CType(lbltemp.Text, Double)
            End If
            lbltemp = CType(e.Item.FindControl("lblRequestAmount"), Label)
            If Not lbltemp Is Nothing Then
                m_TotalRequestAmount += CType(lbltemp.Text, Double)
            End If
        End If
        If e.Item.ItemType = ListItemType.Footer Then
            lblTotal = CType(e.Item.FindControl("lblTotalTransferAmount"), Label)
            If Not lblTotal Is Nothing Then
                lblTotal.Text = FormatNumber(m_TotalTransferAmount.ToString, 2)
            End If
            lblTotal = CType(e.Item.FindControl("lblTotalRequestAmount"), Label)
            If Not lblTotal Is Nothing Then
                lblTotal.Text = FormatNumber(m_TotalRequestAmount.ToString, 2)
            End If
        End If
    End Sub

    Private Sub BtnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonClose.Click
        If SessionInvalid() Then
            Exit Sub
        End If
        pnlVerify.Visible = False
        pnlSearch.Visible = True
        pnlDatagrid.Visible = True
        imgsearch_Click(Nothing, Nothing)
        ClearForm()
    End Sub


    Private Sub BtnVerify_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVerifikasi.Click
        If SessionInvalid() Then
            Exit Sub
        End If

        Dim oPCReimburse As New Parameter.PaymentRequest With {
           .strConnection = GetConnectionString().Trim,
           .RequestNo = lblRequestNo.Text.Trim
       }

        Dim Ndttable As New DataTable

        Ndttable = GetStructPC()
        If DtgAgreeVerify.Items.Count > 0 Then

            Dim strIDTrans As String
            Dim strNote As String
            Dim strSeqNo As String
            Dim strAmount As Double
            Dim strTanggal As DateTime


            For intLoopGrid = 0 To DtgAgreeVerify.Items.Count - 1
                strIDTrans = CType(DtgAgreeVerify.Items(intLoopGrid).FindControl("lblPaymentAllocation"), Label).Text
                strNote = CType(DtgAgreeVerify.Items(intLoopGrid).FindControl("lblNotes"), Label).Text
                strSeqNo = CType(DtgAgreeVerify.Items(intLoopGrid).FindControl("lblSequenceNo"), Label).Text
                strAmount = CType(DtgAgreeVerify.Items(intLoopGrid).FindControl("lblRequestAmount"), Label).Text
                strTanggal = ConvertDate2(CType(DtgAgreeVerify.Items(intLoopGrid).FindControl("lbltgl"), Label).Text)


                Ndttable.Rows.Add(lblRequestNo.Text, strIDTrans, strNote, strSeqNo, strAmount, strTanggal)
            Next


            With oCustomClass
                .RequestNo = lblRequestNo.Text
                .ListData = Ndttable
                .strConnection = GetConnectionString()
            End With

            Try

                Dim oEntitiesApproval As New Parameter.Approval
                With oEntitiesApproval
                    .BranchId = Me.sesBranchId.Trim
                    .SchemeID = SCHEME_ID
                    .RequestDate = strTanggal
                    .ApprovalNote = txtRecommendation.Text.Trim
                    .ApprovalValue = Me.Amount
                    .UserRequest = Me.Loginid
                    .UserApproval = cboApprovedBy.SelectedValue
                    .AprovalType = Parameter.Approval.ETransactionType.PaymentRequest_approval
                    .Argumentasi = txtArgumentasi.Text.Trim
                    .TransactionNo = lblRequestNo.Text.Trim
                End With

                oPCReimburse.Approval = oEntitiesApproval
                If Me.JenisRequest = "Request" Then
                    Dim result = oController.saveApprovalPaymentRequestFA(oPCReimburse, "A")
                ElseIf Me.JenisRequest = "Receive" Then
                    Dim result = oController.saveApprovalPaymentReceiveFA(oPCReimburse, "A")
                ElseIf Me.JenisRequest = "Penjualan" Then
                    Dim result = oController.saveApprovalPenjualanFA(oPCReimburse, "A")
                ElseIf Me.JenisRequest = "Penghapusan" Then
                    Dim result = oController.saveApprovalPenghapusanFA(oPCReimburse, "A")
                End If


                pnlVerify.Visible = False
                PanelKoreksi.Visible = False
                pnlSearch.Visible = True
                pnlDatagrid.Visible = True
                imgsearch_Click(Nothing, Nothing)
                ShowMessage(lblMessage, "Proses Verifikasi Berhasil", False)

            Catch ex As Exception
                ShowMessage(lblMessage, "Proses Verifikasi  Gagal", True)
                Exit Sub
            End Try
        End If
    End Sub
    Private Sub BtnUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnUpdate.Click

        'CType(DtgAgreeVerify.Items(Me.NoIndex).FindControl("lblNotes"), Label).Text = oTrans.Description
        'CType(DtgAgreeVerify.Items(Me.NoIndex).FindControl("lblCOA"), Label).Text = oTrans.COA
        'CType(DtgAgreeVerify.Items(Me.NoIndex).FindControl("lblRequestAmount"), Label).Text = oTrans.Amount
        'CType(DtgAgreeVerify.Items(Me.NoIndex).FindControl("lbltgl"), Label).Text = oTrans.ValutaDate
        'CType(DtgAgreeVerify.Items(Me.NoIndex).FindControl("lblPaymentAllocation"), Label).Text = oTrans.TransactionID
        'CType(DtgAgreeVerify.Items(Me.NoIndex).FindControl("hyPettyCashNo"), Label).Text = oTrans.Transaction

        pnlSearch.Visible = False
        pnlDatagrid.Visible = False
        pnlVerify.Visible = True
        PanelKoreksi.Visible = False

    End Sub
    'Private Sub ButtonUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonUpdate.Click

    '    If SessionInvalid() Then
    '        Exit Sub
    '    End If

    '    Dim Ndttable As New DataTable

    '    Ndttable = GetStructPC()
    '    If DtgAgreeVerify.Items.Count > 0 Then

    '        Dim strIDTrans As String
    '        Dim strNote As String
    '        Dim strSeqNo As String
    '        Dim strAmount As Integer
    '        Dim strTanggal As DateTime


    '        For intLoopGrid = 0 To DtgAgreeVerify.Items.Count - 1
    '            strIDTrans = CType(DtgAgreeVerify.Items(intLoopGrid).FindControl("lblPaymentAllocation"), Label).Text
    '            strNote = CType(DtgAgreeVerify.Items(intLoopGrid).FindControl("lblNotes"), Label).Text
    '            strSeqNo = CType(DtgAgreeVerify.Items(intLoopGrid).FindControl("lblSequenceNo"), Label).Text
    '            strAmount = CType(DtgAgreeVerify.Items(intLoopGrid).FindControl("lblRequestAmount"), Label).Text
    '            strTanggal = ConvertDate2(CType(DtgAgreeVerify.Items(intLoopGrid).FindControl("lbltgl"), Label).Text)


    '            Ndttable.Rows.Add(lblRequestNo.Text, strIDTrans, strNote, strSeqNo, strAmount, strTanggal)
    '        Next


    '        With oCustomClass
    '            .RequestNo = lblRequestNo.Text
    '            .ListData = Ndttable
    '            .strConnection = GetConnectionString()
    '        End With
    '        Try
    '            Dim result = oController.PRCOAUpdate(oCustomClass)
    '            If result <> "OK" Then
    '                ShowMessage(lblMessage, result, True)
    '                Exit Sub
    '            Else
    '                ShowMessage(lblMessage, "Proses Update Payment Request Berhasil", False)
    '            End If
    '        Catch es As Exception
    '            ShowMessage(lblMessage, "Proses Update Payment Request  Gagal", True)
    '            Exit Sub
    '        End Try

    '        lblRequestNo.Text = oCustomClass.RequestNo
    '    Else
    '        ShowMessage(lblMessage, "Tidak ada data", True)
    '        Exit Sub

    '    End If

    'End Sub
    Private Function GetStructPC() As DataTable
        Dim lObjDataTable As New DataTable

        lObjDataTable.Columns.Add("RequestNo")
        lObjDataTable.Columns.Add("TransactionID")
        lObjDataTable.Columns.Add("Note")
        lObjDataTable.Columns.Add("SequenceNo")
        lObjDataTable.Columns.Add("Amount")
        lObjDataTable.Columns.Add("StatusDate")


        Return lObjDataTable
    End Function
    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose.Click
        'pnlSearch.Visible = False
        'pnlDatagrid.Visible = False
        'pnlVerify.Visible = True
        'PanelKoreksi.Visible = False
        Response.Redirect("VerifyPaymentRequestACC.aspx?page=" & currentPage & "&BranchID=" & cboParent.SelectedValue.Trim & "&TglReq=" & txtDate.Text)
    End Sub
    Private Sub cboStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboStatus.SelectedIndexChanged
        If cboStatus.SelectedItem.Value = "RJ" Then
            Panel1.Visible = True
            pnlbutton.Visible = False
            Panel3.Visible = False
        ElseIf cboStatus.SelectedItem.Value = "AP" Then
            Panel1.Visible = False
            pnlbutton.Visible = True
            Panel3.Visible = False
        End If
    End Sub
    Private Sub ButtonCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        If SessionInvalid() Then
            Exit Sub
        End If
        pnlVerify.Visible = False
        pnlSearch.Visible = True
        pnlDatagrid.Visible = True
        Panel1.Visible = False
        Panel3.Visible = False
        pnlbutton.Visible = False
        imgsearch_Click(Nothing, Nothing)
        ClearForm()
    End Sub
    Private Sub BtnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        If SessionInvalid() Then
            Exit Sub
        End If
        pnlVerify.Visible = False
        pnlSearch.Visible = True
        pnlDatagrid.Visible = True
        Panel1.Visible = False
        pnlbutton.Visible = False
        Panel3.Visible = False
        imgsearch_Click(Nothing, Nothing)
        ClearForm()
    End Sub
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If SessionInvalid() Then
            Exit Sub
        End If


        Dim oPayReqReject As New Parameter.PaymentRequest With {
           .strConnection = GetConnectionString(),
           .RequestNo = lblRequestNo.Text.Trim,
           .PaymentStatus = "REJ",
           .notes = ("Reject Accounting - " + txtnotes.Text.Trim),
           .RequestBy = Me.Loginid,
           .RequestDate = Me.BusinessDate,
           .BranchId = Me.sesBranchId.Trim.Replace("'", "")
       }
        Try
            Dim result = oController.PayReqStatusReject(oPayReqReject, "HQ")
            ShowMessage(lblMessage, "Proses Reject Permintaan Pembayaran Berhasil", False)
            pnlVerify.Visible = False
            pnlSearch.Visible = True
            pnlDatagrid.Visible = True
            pnlbutton.Visible = False
            Panel1.Visible = False
            Panel3.Visible = False
            imgsearch_Click(Nothing, Nothing)
            'ClearForm()
            Response.Redirect("VerifyPaymentRequestACC.aspx?page=" & currentPage & "&BranchID=" & cboParent.SelectedValue.Trim & "&TglReq=" & txtDate.Text)

        Catch ex As Exception
            ShowMessage(lblMessage, "Proses Reject Permintaan Pembayaran  Gagal", True)
            Exit Sub
        End Try

    End Sub
    Sub ClearForm()
        cboStatus.SelectedIndex = 0
    End Sub

    Private Sub btnhistrans_Click(sender As Object, e As EventArgs) Handles btnhistrans.Click
        oClass.SelectedBranch = Me.BranchID
        oClass.AmountTransfer = lblAmount.Text.Replace(",", "")
        oClass.AreaID = "ALL"
        oClass.Memo = lblMemo.Text.Trim
        oClass.Notes = lblDescription.Text.Trim

        Response.AppendCookie(Webform.UserController.RSConfig.setCookies(Request.Cookies("RSCOOKIES"), oClass))

        Dim strReportFile As String = "../../Webform.Reports/ucRSViewerPopup.aspx?formid=CEKTRANS&rsname=/BataviaReport/CashMgt/hisTransaksiPembayaran"

        Response.Write("<script language = javascript>" & vbCrLf _
                        & "var x = screen.width; " & vbCrLf _
                        & "var y = screen.height; " & vbCrLf _
                           & "window.open('" & strReportFile & "','_blank', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes, resizable=1') " & vbCrLf _
                           & "</script>")
    End Sub
End Class