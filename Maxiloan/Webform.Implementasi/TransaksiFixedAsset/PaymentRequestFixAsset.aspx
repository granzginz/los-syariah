﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PaymentRequestFixAsset.aspx.vb" Inherits="Maxiloan.Webform.Implementasi.PaymentRequestFixAsset" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>paymentRequestFixAsset</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" type="text/css" />
	<script src="../../js/jquery-2.1.1.js" type="text/javascript"></script>
	<script src="../../js/jquery-ui-1.10.3.custom.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
        <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>

    <asp:Panel ID="pnlList" runat="server">
         <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    Permintaan Pembayaran Fix Asset
                </h3>
            </div>
        </div>
    
        <div class="form_box">
            <div class="form_single">
                <label>
                    Tanggal Invoice &lt;=</label>
                <asp:TextBox runat="server" ID="txtValidDate"></asp:TextBox>
                <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtValidDate"
                    Format="dd/MM/yyyy">
                </asp:CalendarExtender>
            </div>
        </div>

         <div class="form_button">
            <asp:Button ID="Btnsearch" runat="server" CausesValidation="True" Text="Find" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlGrid" runat="server" Visible="true">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DAFTAR TRANSAKSI FIX ASSET
                </h4>
            </div>
        </div>
         <div class="form_box">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgPC" runat="server" ShowFooter="True"  CssClass="grid_general"
                        AutoGenerateColumns="False" AllowSorting="True">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <FooterStyle CssClass="item_grid" />
                        <Columns>
                        <%--    <asp:TemplateColumn>
                                <HeaderTemplate>
                                    <asp:CheckBox ID="cbAll" Visible="True" runat="server" OnCheckedChanged="checkAll"
                                        AutoPostBack="True"></asp:CheckBox>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="cbCheck" Visible="True" runat="server"></asp:CheckBox>
                                </ItemTemplate>
                            </asp:TemplateColumn>--%>
                              <asp:TemplateColumn HeaderText="PILIH">
                                  <ItemTemplate>  
                                    <asp:LinkButton ID="HypReceive" CausesValidation="False" runat="server" Enabled="True"
                                        Text="PROSES" CommandName="PROSES">PROSES</asp:LinkButton>
                                      </ItemTemplate>
                            </asp:TemplateColumn>
                           <asp:TemplateColumn SortExpression="InvoiceNo" HeaderText="No Invoice" >
                                <ItemTemplate>
                                    <asp:Label ID="lblInvoiceNo" runat="server" Text='<%#Container.DataItem("InvoiceNo")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="SupplierId" HeaderText="Id Supplier">
                                <ItemTemplate>
                                    <asp:Label ID="lblSupplierId" runat="server" Text='<%#Container.DataItem("VendorId")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="SupplierBankId" HeaderText="Id SupplierBank">
                                <ItemTemplate>
                                    <asp:Label ID="lblSupplierBankId" runat="server" Text='<%#Container.DataItem("VendorBankId")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="SupplierBankBranch" HeaderText="SupplierBankBranch">
                                <ItemTemplate>
                                    <asp:Label ID="lblSupplierBankBranch" runat="server" Text='<%#Container.DataItem("VendorBankBranch")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="SupplierBankAccountNo" HeaderText="SupplierBankAccountNo">
                                <ItemTemplate>
                                    <asp:Label ID="lblSupplierBankAccountNo" runat="server" Text='<%#Container.DataItem("VendorBankAccountNo")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="SupplierBankAccountName" HeaderText="SupplierBankAccountName">
                                <ItemTemplate>
                                    <asp:Label ID="lblSupplierBankAccountName" runat="server" Text='<%#Container.DataItem("VendorBankAccountName")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="VendorInvoiceNo" HeaderText="VendorInvoiceNo">
                                <ItemTemplate>
                                    <asp:Label ID="lblVendorInvoiceNo" runat="server" Text='<%#Container.DataItem("VendorInvoiceNo")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Jumlah" HeaderText="Jumlah">
                                <ItemTemplate>
                                    <asp:Label ID="lblAmount" runat="server" Text='<%#FormatNumber(Container.DataItem("Jumlah"), 2)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="InvoiceDate" SortExpression="InvoiceDate" HeaderText="TGL INVOICE"
                                DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
                            <asp:TemplateColumn SortExpression="Notes" HeaderText="Notes" >
                                <ItemTemplate>
                                    <asp:Label ID="lblNotes" runat="server" Text='<%#Container.DataItem("Notes")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                        <PagerStyle Visible="False" HorizontalAlign="Left"  
                            Mode="NumericPages"></PagerStyle>
                    </asp:DataGrid>
                    <div class="button_gridnavigation">
                            <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                                CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                                CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                                CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                                CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                            </asp:ImageButton>
                            <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                            <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                                EnableViewState="False"></asp:Button>
                            <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" MinimumValue="1"
                                ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"></asp:RangeValidator>
                            <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                                ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general">
                            </asp:RequiredFieldValidator>
                        </div>
                        <div class="label_gridnavigation">
                            <asp:Label ID="lblPage" runat="server"></asp:Label>of
                            <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                            <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                        </div>
                </div>
            </div>
        </div>
        <div class="form_box_hide" runat="server">
            <div class="form_button">
            <asp:Button ID="BtnNext" runat="server" CausesValidation="False" Visible="True" Text="Next"
                CssClass="small button green"></asp:Button>
        </div>
        </div> 
    </asp:Panel>


    <asp:Panel ID="pnlPaymentRequest" runat="server" Visible="false">

    </asp:Panel>


    </form> 
</body>