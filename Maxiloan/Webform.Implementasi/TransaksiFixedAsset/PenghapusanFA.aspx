﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PenghapusanFA.aspx.vb" Inherits="Maxiloan.Webform.Implementasi.PenghapusanFA" %>
<%@ Register TagPrefix="uc1" TagName="ucLookUpTransaction" Src="../../Webform.UserController/ucLookUpTransaction.ascx" %>
<%@ Register Src="../../Webform.UserController/ucDateCE.ascx" TagName="ucDateCE" TagPrefix="uc2" %>
<%@ Register TagPrefix="uc1" TagName="ucNumberFormat" Src="../../Webform.UserController/ucNumberFormat.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucBankAccountBranch" Src="../../Webform.UserController/ucBankAccountBranch.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucOtherBankAccount" Src="../../Webform.UserController/ucOtherBankAccount.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucBankAccountOther" Src="../../Webform.UserController/ucBankAccountOther.ascx" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Penghapusan Fixed Asset</title>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
    <script language="JavaScript" type="text/javascript">
                function DeleteConfirm() {
                    if (confirm("Apakah Yakin mau hapus data ini ? ")) {
                        return true;
                    }
                    else {
                        return false;
                    }
                }
                //function click() {
                //    if (event.button == 2) {
                //        alert('Anda tidak Berhak');
                //    }
                //}

                //document.onmousedown = click

                function klikpotong(n, x) {
                    var amount = $('#' + x).val();
                    var a = amount.replace(/\s*,\s*/g, '');

                    if ($('#' + n).is(":checked")) {
                        amount = parseFloat(a) * -1;
                    } else {
                        amount = Math.abs(parseFloat(a));
                    }

                    $('#' + x).val(number_format(amount));
                    total();
                }
                function total(e) {
                    var grid = document.getElementById('DtgPRList');
                    var rowCount = grid.rows.length - 2;
                    var total = 0;

                    for (i = 0; i < rowCount; i++) {
                        var Amount = document.getElementById('DtgPRList_txtAmountTrans_' + i).value.replace(/,/gi, "");

                        total = total + parseFloat(Amount);
                    }

                    $('#DtgPRList_txtTotTransAmount').val(number_format(total));
                    $('#hdTotal').val(number_format(total));
                }
                function number_format(number, decimals, dec_point, thousands_sep) {

                    number = (number + '')
                    .replace(/[^0-9+\-Ee.]/g, '');
                    var n = !isFinite(+number) ? 0 : +number,
                    prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
                    sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
                    dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
                    s = '',
                    toFixedFix = function (n, prec) {
                        var k = Math.pow(10, prec);
                        return '' + (Math.round(n * k) / k)
                        .toFixed(prec);
                    };
                    // Fix for IE parseFloat(0.55).toFixed(0) = 0;
                    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
                    .split('.');
                    if (s[0].length > 3) {
                        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
                    }
                    if ((s[1] || '')
                    .length < prec) {
                        s[1] = s[1] || '';
                        s[1] += new Array(prec - s[1].length + 1)
                      .join('0');
                    }
                    return s.join(dec);
                }
                function DoPostBack() {
                    __doPostBack('Jlookup', '');
                    __doPostBack('btnJLookup', '');

                }

                function selectAllCheckbox(val) {
                    $('#DtgPRList input:checkbox').prop('checked', $(val).is(':checked'));
                }
                function OpenLookup() {
                    if ($('#cboDepartement').val() == "0") {
                        alert("Departement harap diisi")
                        return true;
                    } else {
                        return false;
                    }
                }

                function validation() {
                    if ($('#chk').val() == "true") {
                        document.getElementById("chk1").checked = true;
                        document.getElementById("chk").checked = false;
                    } else {
                        document.getElementById("chk1").checked = false;
                        document.getElementById("chk").checked = true;
                    }
                    document.getElementById("txtBankAccountIDLain").value = "";
                    document.getElementById("txtBankAccountNameLain").value = "";
                }
                function validation1() {
                    if ($('#chk1').val() == "true") {
                        document.getElementById("chk1").checked = false;
                        document.getElementById("chk").checked = true;
                    } else {
                        document.getElementById("chk1").checked = true;
                        document.getElementById("chk").checked = false;
                    }
                    document.getElementById("txtBankAccountIDCabang").value = "";
                    document.getElementById("txtBankAccountNameCabang").value = "";
                }
        </script>
        <script type="text/javascript">
            var submit = 0;
            function CheckDouble() {
                if (++submit > 1) {
                    alert('This sometimes takes a few seconds - please be patient.');
                    return false;
                }
            }
         </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" />
        <div runat="server" id="jlookupContent" />
        <asp:Label ID="lblMessage" runat="server" Visible="false" Font-Bold="true"  Font-Size="Medium" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
        <div class="form_title">
            <div class="title_strip"></div>
            <div class="form_single">
                <h3>
                    PENGHAPUSAN FIXED ASSET
                </h3>
            </div>
        </div>

        <asp:Panel ID="Panel2" runat="server">
            <div class="form_box_header">
                <div>
                     <div class="form_single">
                         <label><b>AKTIVA</b></label>
                     </div>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                 <label> Kode Aktiva</label>
                 <asp:TextBox ID="txtKodeAktiva" runat="server" disabled="true" Width="250px" ></asp:TextBox>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                 <label> Nama Aktiva</label>
                 <asp:TextBox ID="txtNamaAktiva" runat="server" disabled="true" Width="250px" ></asp:TextBox>
                </div>
            </div>  
            <div class="form_box">
                <div class="form_single">
                 <label> Tanggal Invoice</label>
                 <asp:TextBox ID="txtInvoiceDate" runat="server" disabled="true" Width="250px" ></asp:TextBox>
                </div>
            </div>  
            <div class="form_box">
                <div class="form_single">
                 <label> No Memo Internal</label>
                 <asp:TextBox ID="txtInternalMemoNo" runat="server" disabled="true" Width="250px"></asp:TextBox>
                </div>
            </div> 
            <div class="form_box">
                <div class="form_single">
                 <label> Alasan Penghapusan</label>
                 <asp:TextBox ID="txtDeleteReason" runat="server" Width="250px"></asp:TextBox>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                 <label> Keterangan</label>
                 <asp:TextBox ID="txtNotes" runat="server" Width="250px"></asp:TextBox>
                </div>
            </div>
        <div class="form_button">
            <asp:Button ID="ButtonSaveApproval" runat="server" Text="Save" CssClass="small button blue" OnClientClick="return CheckDouble();">
            </asp:Button>
            <asp:Button ID="ButtonCancelApproval" runat="server" Text="Cancel" CssClass="small button gray"
                CausesValidation="False"></asp:Button>
            <asp:Button ID="ButtonRevisi" runat="server" visible="false" CausesValidation="False" Text="Revisi"
            CssClass="small button green" ></asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
