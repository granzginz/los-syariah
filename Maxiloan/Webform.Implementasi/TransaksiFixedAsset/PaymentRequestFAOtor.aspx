﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PaymentRequestFAOtor.aspx.vb" Inherits="Maxiloan.Webform.Implementasi.PaymentRequestFAOtor" %>

<%@ Register TagName="ucNumberFormat" TagPrefix="uc1" Src="../../webform.UserController/ucNumberFormat.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="aspajax" %>
<%@ Register TagPrefix="uc1" TagName="UcBankAccountID" Src="../../Webform.UserController/UcBankAccountID.ascx" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Otorisasi Payment Request Fixed Asset</title>
    <link rel="Stylesheet" type="text/css" href="../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../Include/Buttons.css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>     
    <script src="../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <link href="../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" type="text/css" />
	<script src="../../js/jquery-2.1.1.js" type="text/javascript"></script>
	<script src="../../js/jquery-ui-1.10.3.custom.js" type="text/javascript"></script>
    <script src="../../js/jquery.loading.block.js" type="text/javascript"></script>
</head>
<script type="text/javascript" charset="utf-8">
    function preventMultipleSubmissions2() {
        $(document).ready(function () {
            $.loadingBlockShow({
                imgPath: '../../Images/imgloading/default.svg',
                text: 'Sedang diproses ...',
                style: {
                    position: 'fixed',
                    width: '100%',
                    height: '100%',
                    background: 'rgba(0, 0, 0, .8)',
                    left: 0,
                    top: 0,
                    zIndex: 10000
                }
            });

            setTimeout($.loadingBlockHide, 3000);
        });
    }

    function preventMultipleSubmissions() {
        $('#<%=BtnSave.ClientID %>').prop('disabled', true).toggleClass("gray").toggleClass("prs_img_t");
    }
    window.onbeforeunload = preventMultipleSubmissions;

        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';

        function OpenWinMain(TDPNo, branchid) {

            var x = screen.width; var y = screen.height - 100;
            window.open(ServerName + App + '/Webform.LoanMnt/TDP/TDPInqView.aspx?TDPNo=' + TDPNo + '&branchid=' + branchid, null, 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0,scrollbars=1')
        }
        function fConfirm() {
            if (window.confirm("Apakah yakin mau hapus data ini ? "))
                return true;
            else
                return false;
        }
        function fback() {
            history.go(-1);
            return false;
        }
        function click() {
            if (event.button == 2) {
                alert('Anda tidak Berhak');
            }
        }
        document.onmousedown = click
</script>	
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h4>
                    OTORISASI PEMBAYARAN FIXED ASSET   
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label class="label_req">
                    Tanggal Request
                </label>
                <asp:TextBox ID="txtTglJatuhTempo" runat="server" />
                <aspajax:CalendarExtender ID="calExTglJatuhTempo" runat="server" TargetControlID="txtTglJatuhTempo"
                    Format="dd/MM/yyyy" />
                <asp:RequiredFieldValidator ID="rfvTglJatuhTempo" runat="server" ControlToValidate="txtTglJatuhTempo"
                    ErrorMessage="Harap isi dengan tanggal jatuh tempo" Enabled="false" Display="Dynamic" />
            </div>
            <div class="form_right">
                <label>
                    No Invoice
                </label>
                <asp:TextBox ID="txtNoInvoice" runat="server"  MaxLength="20"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Jumlah Terima
                </label>               
                <uc1:ucnumberformat id="txt_AmountFrom" runat="server" />S/D
                <uc1:ucnumberformat id="txt_amountto" runat="server" />
            </div>
        </div>
        <div class="form_box">
              <div class="form_left">
                  <label> Jenis Request</label>
                  <asp:DropDownList ID="cboJenisRequest" runat="server" >
                      <asp:ListItem Value="Request">Request</asp:ListItem>
                      <%--<asp:ListItem Value="Receive">Receive</asp:ListItem>--%>
                      <asp:ListItem Value="Penjualan">Penjualan</asp:ListItem>
                      <asp:ListItem Value="Penghapusan">Penghapusan</asp:ListItem>
                  </asp:DropDownList>
              </div>
        </div>
        <div class="form_button">
            <asp:Button ID="imgSearch" runat="server" Text="Search" CssClass="small button blue">
            </asp:Button>&nbsp;
            <asp:Button ID="imbReset" runat="server" Text="Reset" CssClass="small button gray"
                CausesValidation="False"></asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlDatagrid" runat="server" Visible="false">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DAFTAR OTORISASI TDP
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgAgree" runat="server" CssClass="grid_general" DataKeyField="InvoiceNo"
                        AutoGenerateColumns="False" OnSortCommand="SortGrid" AllowSorting="True" >
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <FooterStyle  ></FooterStyle>
                        <Columns>
                            <asp:TemplateColumn>
                                <HeaderStyle Width="3%"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <HeaderTemplate>
                                    <asp:CheckBox ID="chkAll" AutoPostBack="True" runat="server" OnCheckedChanged="SelectAll">
                                    </asp:CheckBox>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkItem" runat="server"></asp:CheckBox>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="InvoiceNo" HeaderText="InvoiceNo" Visible="True">
                                <ItemTemplate>
                                    <asp:Label ID="lblInvoiceNo" runat="server" Text='<%#Container.DataItem("InvoiceNo")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="RefferenceNo" HeaderText="No Reff/Memo" Visible="True">
                                <ItemTemplate>
                                    <asp:Label ID="lblReferenceNo" runat="server" Text='<%#Container.DataItem("RefferenceNo")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AMOUNT" HeaderText="Amount">
                                <ItemTemplate>
                                    <asp:Label ID="lblAMOUNT" runat="server" Text='<%#FormatNumber(Container.DataItem("TotalAmount"), 2)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="VendorBankAccountNo" HeaderText="Transfer To" Visible="True">
                                <ItemTemplate>
                                    <asp:Label ID="lblVendorBankAccountNo" runat="server" Text='<%#Container.DataItem("VendorBankAccountNo")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="VendorBankAccountName" HeaderText="Bank Account Name To" Visible="True">
                                <ItemTemplate>
                                    <asp:Label ID="lblVendorBankAccountName" runat="server" Text='<%#Container.DataItem("VendorBankAccountName")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="RequestDate" SortExpression="RequestDate" HeaderText="Fixed Asset Request Date"
                                DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="ValueDate" SortExpression="ValueDate" HeaderText="Accepted Request Date"
                                DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
                            <asp:TemplateColumn SortExpression="BankAccountName" HeaderText="Bank Account Name" Visible="True">
                                <ItemTemplate>
                                    <asp:Label ID="lblBankAccountName" runat="server" Text='<%#Container.DataItem("BankAccountName")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                        <PagerStyle Visible="False" HorizontalAlign="Left"  
                            Mode="NumericPages"></PagerStyle>
                    </asp:DataGrid>            
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="BtnSave" runat="server" Text="Save" CssClass="small button blue"></asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
