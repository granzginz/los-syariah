﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.General.CommonCookiesHelper
#End Region
Public Class InquiryFA
	Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
	Private currentPage As Int32 = 1
	Private pageSize As Int16 = 10
	Private currentPageNumber As Int16 = 1
	Private totalPages As Double = 1
	Private recordCount As Int64 = 1

	Private oCustomClass As New Parameter.GeneralPaging
	Private oController As New GeneralPagingController

	Protected WithEvents GridNav As ucGridNav
#End Region

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If SessionInvalid() Then
			Exit Sub
		End If

		If Not IsPostBack Then
			Dim err As String = Request.QueryString("message")
			If err <> String.Empty Then
				ShowMessage(lblMessage, err, True)
			End If

			Me.FormID = "InquiryFA"
			If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
				Me.SearchBy = ""
				Me.SortBy = ""
			End If
		End If
	End Sub

	Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String, Optional isFrNav As Boolean = False)
		Dim DtUserList As New DataTable
		Dim DvUserList As New DataView

		If ddJenis.SelectedIndex = 0 Then
			With oCustomClass
				.strConnection = GetConnectionString()
				.WhereCond = cmdWhere
				.CurrentPage = currentPage
				.PageSize = pageSize
				.SortBy = SortBy
				.SpName = "spPaymentFAInquiryPaging"
			End With
			oCustomClass = oController.GetGeneralPaging(oCustomClass)
			DtUserList = oCustomClass.ListData
			DvUserList = DtUserList.DefaultView
			recordCount = oCustomClass.TotalRecords
			DvUserList.Sort = Me.SortBy

			If (isFrNav = False) Then
				GridNav.Initialize(recordCount, pageSize)
			End If

			DtgPayreq.DataSource = DvUserList
			Try
				DtgPayreq.DataBind()
			Catch
				DtgPayreq.CurrentPageIndex = 0
				DtgPayreq.DataBind()
			End Try
			pnlList.Visible = True
			DtgPayreq.Visible = True
			DtgReqrec.Visible = False
		ElseIf ddJenis.SelectedIndex = 1 Then
			With oCustomClass
				.strConnection = GetConnectionString()
				.WhereCond = cmdWhere
				.CurrentPage = currentPage
				.PageSize = pageSize
				.SortBy = SortBy
				.SpName = "spReceiveFAInquiryPaging"
			End With
			oCustomClass = oController.GetGeneralPaging(oCustomClass)
			DtUserList = oCustomClass.ListData
			DvUserList = DtUserList.DefaultView
			recordCount = oCustomClass.TotalRecords
			DvUserList.Sort = Me.SortBy

			If (isFrNav = False) Then
				GridNav.Initialize(recordCount, pageSize)
			End If

			DtgReqrec.DataSource = DvUserList
			Try
				DtgReqrec.DataBind()
			Catch
				DtgReqrec.CurrentPageIndex = 0
				DtgReqrec.DataBind()
			End Try
			pnlList.Visible = True
			DtgReqrec.Visible = True
			DtgPayreq.Visible = False
		End If

		lblMessage.Visible = False
	End Sub

	Private Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs)
		currentPage = e.CurrentPage
		DoBind(Me.SearchBy, Me.SortBy)
		GridNav.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
	End Sub

	Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
		If InStr(Me.SortBy, "DESC") > 0 Then
			Me.SortBy = e.SortExpression
		Else
			Me.SortBy = e.SortExpression + " DESC"
		End If
		DoBind(Me.SearchBy, Me.SortBy)
	End Sub

	Private Sub Btnsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btnsearch.Click

		Me.SearchBy = ""

		If txtTanggalValuta.Text <> "" Then
			If Me.SearchBy = "" Then
				Me.SearchBy = Me.SearchBy & " ValutaDate = '" & ConvertDate2(txtTanggalValuta.Text) & "'"
			Else
				Me.SearchBy = Me.SearchBy & "and ValutaDate = '" & ConvertDate2(txtTanggalValuta.Text) & "' "
			End If
		End If

		If cmbStatus.SelectedValue <> "" Then
			If Me.SearchBy = "" Then
				Me.SearchBy = Me.SearchBy & " Status = '" & cmbStatus.SelectedValue.Trim & "'"
			Else
				Me.SearchBy = Me.SearchBy & "and Status = '" & cmbStatus.SelectedValue.Trim & "' "
			End If
		ElseIf cmbStatus.SelectedValue = "1" Then
			If Me.SearchBy = "" Then
				Me.SearchBy = Me.SearchBy & " Status = 'E' and TFA.IsOtor = 1 "
			Else
				Me.SearchBy = Me.SearchBy & "and Status = 'E' and TFA.IsOtor = 1 "
			End If
		End If

		If ddsrcpayreq.SelectedValue <> "" Then
			If Me.SearchBy = "" Then
				Me.SearchBy = Me.SearchBy & " " & ddsrcpayreq.SelectedValue.Trim & " = '" & txtsrcbypayreq.Text.Trim & "'"
			Else
				Me.SearchBy = Me.SearchBy & "and " & ddsrcpayreq.SelectedValue.Trim & " = '" & txtsrcbypayreq.Text.Trim & "' "
			End If
		End If

		If ddsrcreqrec.SelectedValue <> "" Then
			If Me.SearchBy = "" Then
				Me.SearchBy = Me.SearchBy & " " & ddsrcreqrec.SelectedValue.Trim & " = '" & txtsrcbyreqrec.Text.Trim & "'"
			Else
				Me.SearchBy = Me.SearchBy & "and " & ddsrcreqrec.SelectedValue.Trim & " = '" & txtsrcbyreqrec.Text.Trim & "' "
			End If
		End If

		pnlDatagrid.Visible = True
		pnlList.Visible = True
		DoBind(Me.SearchBy, Me.SortBy)

	End Sub

	Private Sub BtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnReset.Click
		Server.Transfer("InquiryFA.aspx")
	End Sub

End Class