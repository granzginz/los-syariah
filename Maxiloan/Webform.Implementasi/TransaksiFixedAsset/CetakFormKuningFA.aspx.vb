﻿#Region "Imports"
Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Webform.Implementasi
Imports CrystalDecisions.Shared
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports CrystalDecisions.Web
Imports Maxiloan.General.CommonCookiesHelper
Imports Maxiloan.Webform.LoanMnt
#End Region

Public Class CetakFormKuningFA
    Inherits Webform.WebBased
#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("cmdwhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("cmdwhere") = Value
        End Set
    End Property
    Private Property GM1() As String
        Get
            Return CType(ViewState("GM1"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("GM1") = Value
        End Set
    End Property
    Private Property GM2() As String
        Get
            Return CType(ViewState("GM2"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("GM2") = Value
        End Set
    End Property
    Private Property GM3() As String
        Get
            Return CType(ViewState("GM3"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("GM3") = Value
        End Set
    End Property
    Private Property Direksi() As String
        Get
            Return CType(ViewState("Direksi"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Direksi") = Value
        End Set
    End Property
    Private Property MultiApplicationID() As String
        Get
            Return CType(ViewState("MultiApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("MultiApplicationID") = Value
        End Set
    End Property
    Private Property Jenis() As String
        Get
            Return CType(ViewState("Jenis"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Jenis") = Value
        End Set
    End Property
    Private Property FileName() As String
        Get
            Return CType(ViewState("FileName"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("FileName") = Value
        End Set
    End Property
    Private Property oReports()
        Get
            Return ViewState("oReports")
        End Get
        Set()
            ViewState("oReports") = Value
        End Set
    End Property
#End Region
#Region "Constanta"
    Private oParameter As New Parameter.APDisbApp
    Private oController As New Controller.APDisbAppController
    Friend Shared ReportSource As RptPaymentRequest
    Friend Shared ReportSource2 As RptRequestReceive
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        bindReport()
    End Sub
#Region "BindReport"
    Sub bindReport()
        GetCookies()

        Dim oDataSet As New DataSet
        Dim oDataTable As New DataTable
        Dim oReport As RptPaymentRequest = New RptPaymentRequest
        Dim oReport2 As RptRequestReceive = New RptRequestReceive
        Dim oReport3 As RptPenjualan = New RptPenjualan
        Dim oReport4 As RptPenghapusan = New RptPenghapusan
        Dim dtEntity As New DataTable

        If Me.Jenis.Trim = "PAYREQ" Then
            Me.oReports = oReport
        ElseIf Me.Jenis.Trim = "REQREC" Then
            Me.oReports = oReport2
        ElseIf Me.Jenis.Trim = "SELLFA" Then
            Me.oReports = oReport2
        ElseIf Me.Jenis.Trim = "DELFA" Then
            Me.oReports = oReport4
        End If

        If Not oParameter Is Nothing Then
            oDataTable = oParameter.RequestNoTemp
        End If
        With oParameter
            .strConnection = GetConnectionString()
            .RequestNo = Me.MultiApplicationID
            .SortBy = Me.SortBy
            .GM1 = Me.GM1
            .GM2 = Me.GM2
            .GM3 = Me.GM3
            .Direksi = Me.Direksi
        End With

        oParameter = CetakBuktiBayar(oParameter)
        oDataSet = oParameter.ListReport

        oParameter.strConnection = GetConnectionString()
        oParameter.BranchId = Me.sesBranchId.Replace("'", "")
        oParameter.LoginId = Me.Loginid
        oParameter = CetakBuktiBayar(oParameter)
        oDataSet = oParameter.ListReport

        Me.oReports.SetDataSource(oDataSet.Tables(0))
        CrystalReportViewer1.ReportSource = Me.oReports

        CrystalReportViewer1.Visible = True
        CrystalReportViewer1.DataBind()

        With Me.oReports.ExportOptions
            .ExportDestinationType = ExportDestinationType.DiskFile
            .ExportFormatType = ExportFormatType.PortableDocFormat
        End With

        Dim strFileLocation As String

        If Me.Jenis = "PAYREQ" Then
            Me.FileName = "rpt_PmtPembayaranFixedAsset.pdf"
        ElseIf Me.Jenis = "REQREC" Then
            Me.FileName = "rpt_PmtPenerimaanFixedAsset.pdf"
        ElseIf Me.Jenis = "SELLFA" Then
            Me.FileName = "rpt_PenjualanFixedAsset.pdf"
        ElseIf Me.Jenis = "DELFA" Then
            Me.FileName = "rpt_PenghapusanFixedAsset.pdf"
        End If

        strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
        strFileLocation += Me.Session.SessionID + Me.Loginid + Me.FileName

        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions =
        New CrystalDecisions.Shared.DiskFileDestinationOptions

        DiskOpts.DiskFileName = strFileLocation

        With Me.oReports
            .ExportOptions.DestinationOptions = DiskOpts
            .Export()
            .Close()
            .Dispose()
        End With

        Response.Redirect("FixedAssetYellowPage.aspx?strFileLocation=" & Me.Session.SessionID & Me.Loginid & Me.FileName.Substring(0, Me.FileName.Length - 4))

    End Sub

    Public Function CetakBuktiBayar(ByVal customclass As Parameter.APDisbApp) As Parameter.APDisbApp
        Dim params() As SqlParameter = New SqlParameter(4) {}
        params(0) = New SqlParameter("@InvoiceNo", SqlDbType.VarChar, 30)
        params(0).Value = customclass.RequestNo
        params(1) = New SqlParameter("@GM1", SqlDbType.Char, 50)
        params(1).Value = customclass.GM1
        params(2) = New SqlParameter("@GM2", SqlDbType.Char, 50)
        params(2).Value = customclass.GM2
        params(3) = New SqlParameter("@GM3", SqlDbType.Char, 50)
        params(3).Value = customclass.GM3
        params(4) = New SqlParameter("@Direksi", SqlDbType.Char, 50)
        params(4).Value = customclass.Direksi

        If Me.Jenis.Trim = "PAYREQ" Then
            customclass.ListReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spFormKuningPaymenFAP", params)
        ElseIf Me.Jenis.Trim = "REQREC" Then
            customclass.ListReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spFormKuningPaymenFAR", params)
        ElseIf Me.Jenis.Trim = "SELLFA" Then
            customclass.ListReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spFormKuningPaymenFAR", params)
        ElseIf Me.Jenis.Trim = "DELFA" Then
            customclass.ListReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spFormKuningdelFA", params)
        End If

        Return customclass
    End Function
#End Region
#Region "GetCookies"
    Private Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies("RptPaymentRequest")
        Me.Jenis = cookie.Values("Jenis")
        Me.GM1 = cookie.Values("GM1")
        Me.GM2 = cookie.Values("GM2")
        Me.GM3 = cookie.Values("GM3")
        Me.Direksi = cookie.Values("Direksi")

        If Me.Jenis.Trim = "PAYREQ" Then
            Me.MultiApplicationID = cookie.Values("Invoiceno")
        ElseIf Me.Jenis.Trim = "REQREC" Then
            Me.MultiApplicationID = cookie.Values("AktivaId")
        ElseIf Me.Jenis.Trim = "SELLFA" Then
            Me.MultiApplicationID = cookie.Values("AktivaId")
        ElseIf Me.Jenis.Trim = "DELFA" Then
            Me.MultiApplicationID = cookie.Values("AktivaId")
        End If

    End Sub
#End Region

End Class