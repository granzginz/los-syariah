﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports System.Data.SqlClient
#End Region

Public Class PaymentRequestFAOtor
	Inherits Maxiloan.Webform.WebBased

	Protected WithEvents txt_AmountFrom As ucNumberFormat
	Protected WithEvents txt_amountto As ucNumberFormat

#Region "Constanta"
	Private currentPage As Int32 = 1
	Private pageSize As Int16 = 10
	Private currentPageNumber As Int16 = 1
	Private totalPages As Double = 1
	Private recordCount As Int64 = 1

	Private oCustomClass As New Parameter.TransFixedAsset
	Private oController As New ImplementasiControler
#End Region
#Region "Property"
    Private Property JenisRequest() As String
        Get
            Return (CType(ViewState("JenisRequest"), String))
        End Get
        Set(value As String)
            ViewState("JenisRequest") = value
        End Set
    End Property
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If SessionInvalid() Then
			Exit Sub
		End If
		If Me.IsHoBranch = False Then
			NotAuthorized()
			Exit Sub
		End If
		If Not IsPostBack Then
			If Request.QueryString("message") <> "" Then
				ShowMessage(lblMessage, Request.QueryString("message"), IIf(Request.QueryString("message") = MessageHelper.MESSAGE_UPDATE_SUCCESS, False, True))
			End If
			Me.FormID = "PaymentFAOtor"
			If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
				Me.SearchBy = ""
				Me.SortBy = ""
			End If

		End If
	End Sub
	Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
		Dim DtUserList As New DataTable
		Dim DvUserList As New DataView

		With oCustomClass
			.strConnection = GetConnectionString()
			.WhereCond = cmdWhere
			.CurrentPage = currentPage
			.PageSize = pageSize
			.SortBy = SortBy
		End With

        If Me.JenisRequest = "Request" Then
            oCustomClass = oController.PaymentRequestFAOtor(oCustomClass)
        ElseIf Me.JenisRequest = "Receive" Then
            oCustomClass = oController.RequestReceiveFAOtor(oCustomClass)
        ElseIf Me.JenisRequest = "Penjualan" Then
            oCustomClass = oController.PenjualanFAOtor(oCustomClass)
        ElseIf Me.JenisRequest = "Penghapusan" Then
            oCustomClass = oController.PenghapusanFAOtor(oCustomClass)
        End If


        DtUserList = oCustomClass.ListData
		DvUserList = DtUserList.DefaultView
		recordCount = oCustomClass.TotalRecord
		DvUserList.Sort = Me.SortBy
		DtgAgree.DataSource = DvUserList

		Try
			DtgAgree.DataBind()
		Catch
			DtgAgree.CurrentPageIndex = 0
			DtgAgree.DataBind()
		End Try
		pnlList.Visible = True
		pnlDatagrid.Visible = True

		If recordCount <> 0 Then
			ShowMessage(lblMessage, "Data Ditemukan", False)
		Else
			ShowMessage(lblMessage, "Data Tidak Ditemukan", True)
		End If
	End Sub
	Public Sub SelectAll(ByVal sender As Object, ByVal e As System.EventArgs)
		Dim chkSender As CheckBox = CType(sender, CheckBox)
		Dim chkItem As CheckBox
		Dim x As Integer

		For x = 0 To DtgAgree.Items.Count - 1
			chkItem = CType(DtgAgree.Items(x).FindControl("chkItem"), CheckBox)
			chkItem.Checked = chkSender.Checked
		Next
	End Sub
	Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
		If InStr(Me.SortBy, "DESC") > 0 Then
			Me.SortBy = e.SortExpression
		Else
			Me.SortBy = e.SortExpression + " DESC"
		End If
		DoBind(Me.SearchBy, Me.SortBy)
	End Sub
	Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imgSearch.Click
		If SessionInvalid() Then
			Exit Sub
		End If

        Me.JenisRequest = cboJenisRequest.SelectedItem.Value

        lblMessage.Visible = False
		Me.SearchBy = ""
        Me.SearchBy = Me.SearchBy & " Status in ('A') "

        If txtTglJatuhTempo.Text <> "" Then
			Me.SearchBy = Me.SearchBy & " AND RequestDate BETWEEN'" & ConvertDate2(txtTglJatuhTempo.Text) & " 00:00:00.000" & "' and '" & ConvertDate2(txtTglJatuhTempo.Text) & " 23:59:59.000" & "' "
		End If

		If txtNoInvoice.Text.Trim <> "" Then
			Me.SearchBy = Me.SearchBy & " AND fa.InvoiceNo = '" & txtNoInvoice.Text.Trim & "' "
		End If

		If txt_AmountFrom.Text.Trim > "0" Then
			Me.SearchBy = Me.SearchBy & " AND Jumlah >= '" & CDbl(txt_AmountFrom.Text.Trim) & "' "
		End If

		If txt_amountto.Text.Trim > "0" Then
			Me.SearchBy = Me.SearchBy & " AND Jumlah <= '" & CDbl(txt_amountto.Text.Trim) & "' "
		End If

		pnlDatagrid.Visible = True
		DtgAgree.Visible = True
		pnlList.Visible = True

		DoBind(Me.SearchBy, Me.SortBy)
		'End If
	End Sub

#Region "save"
	Private Sub BtnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSave.Click

		Try
			System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, GetType(Page), "Script", "preventMultipleSubmissions2();", True)

			oCustomClass.strConnection = GetConnectionString()
			For n As Integer = 0 To DtgAgree.DataKeys.Count - 1
				Dim chkItem As CheckBox = CType(DtgAgree.Items(n).FindControl("chkItem"), CheckBox)
				If chkItem.Checked = True Then
					Dim lblInvoiceNo As Label = CType(DtgAgree.Items(n).FindControl("lblInvoiceNo"), Label)
					oCustomClass.InvoiceNo = lblInvoiceNo.Text.Trim
                    oCustomClass.LoginId = Me.Loginid

                    If Me.JenisRequest = "Request" Then
                        oController.PaymentRequestFAOtorSave(oCustomClass)
                    ElseIf Me.JenisRequest = "Receive" Then
                        oController.RequestReceiveFAOtorSave(oCustomClass)
                    ElseIf Me.JenisRequest = "Penjualan" Then
                        oController.PenjualanFAOtorSave(oCustomClass)
                    ElseIf Me.JenisRequest = "Penghapusan" Then
                        oController.PenghapusanFAOtorSave(oCustomClass)
                    End If
                Else
                    ShowMessage(lblMessage, "Harap Pilih Invoice", True)
				End If
			Next
			DoBind(Me.SearchBy, Me.SortBy)
			ShowMessage(lblMessage, "Data Berhasil di simpan", False)
		Catch ex As Exception

			ShowMessage(lblMessage, ex.Message, True)
		End Try

	End Sub

#End Region

	Sub NotAuthorized()
		Dim strHTTPServer As String
		Dim StrHTTPApp As String
		Dim strNameServer As String
		strHTTPServer = Request.ServerVariables("PATH_INFO")
		strNameServer = Request.ServerVariables("SERVER_NAME")
		StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
		Response.Redirect("http://" & strNameServer & "/" & StrHTTPApp & "/error_notauthorized.aspx")
	End Sub
End Class