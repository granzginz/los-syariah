﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region


Public Class PaymentRequestFixAsset
    Inherits Maxiloan.Webform.WebBased

#Region "Property"
    Public Property isCheckAll() As Boolean
        Get
            Return CBool(ViewState("isCheckAll"))
        End Get
        Set(ByVal Value As Boolean)
            ViewState("isCheckAll") = Value
        End Set
    End Property
    Public Property InvoiceNo() As String
        Get
            Return CType(ViewState("InvoiceNo"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("InvoiceNo") = Value
        End Set
    End Property
#End Region

#Region "Private Constanta"
    Private oController As New DataUserControlController
    Private m_controller As New PaymentRequestController
    Private x_controller As New ImplementasiControler
    Dim oCustomClass As New Parameter.PaymentRequest
    'Dim tempTotalPRAmount As Double
    'Private Const SCHEME_ID As String = "PYR"

    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.FormID = "REQFA"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If Request.QueryString("message") <> "" Then

                    ShowMessage(lblMessage, Request.QueryString("message"), False)
                End If
                With txtValidDate
                    .Text = Me.BusinessDate.ToString("dd/MM/yyyy")
                End With

                pnlList.Visible = True
                pnlGrid.Visible = False
            End If
        End If
    End Sub

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim oController As New ImplementasiControler
        Dim DtUserList As DataTable
        Dim DvUserList As DataView
        Dim dtEntity As New DataTable
        'Dim currentPage As Int32 = 1
        Dim pageSize As Int16 = 10

        Dim oCustomClass As New Parameter.TransFixedAsset
        Dim PCList As New Parameter.TransFixedAsset

        Try
            With oCustomClass
                .strConnection = GetConnectionString()
                .WhereCond = Me.SearchBy
                .CurrentPage = currentPage
                .PageSize = pageSize
                .SortBy = ""
                .BusinessDate = Me.BusinessDate
            End With

            oCustomClass = oController.PaymentRequestFAList(oCustomClass)

            DtUserList = oCustomClass.ListData
            DvUserList = DtUserList.DefaultView
            recordCount = oCustomClass.TotalRecord
            DvUserList.Sort = Me.SortBy
            DtgPC.DataSource = DvUserList

            Try
                DtgPC.DataBind()
            Catch ex As Exception

            End Try

            If DtgPC.Items.Count <= 0 Then
                BtnNext.Visible = False
            Else
                BtnNext.Visible = True
            End If

            PagingFooter()
            pnlList.Visible = True
            pnlGrid.Visible = True
            lblMessage.Text = ""
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub


    Private Sub BtnNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnNext.Click
        If CheckFeature(Me.Loginid, Me.FormID, "NEXT", Me.AppId) Then
            lblMessage.Text = ""

        End If
    End Sub

#Region "CheckAllCheckBox"
    Public Sub checkAll(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim loopitem As Int16
        Dim Chk As CheckBox

        For loopitem = 0 To CType(DtgPC.Items.Count - 1, Int16)
            Chk = New CheckBox
            Chk = CType(DtgPC.Items(loopitem).FindControl("cbCheck"), CheckBox)

            If Chk.Enabled Then
                If CType(sender, CheckBox).Checked = True Then
                    Chk.Checked = True
                    Me.isCheckAll = True
                Else
                    Chk.Checked = False
                    Me.isCheckAll = False
                End If
            Else
                Chk.Checked = False
            End If
        Next
    End Sub
#End Region

#Region "Process Reset And Search"
    Private Sub BtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Server.Transfer("PaymentRequestFixAsset.aspx")
    End Sub
    Private Sub Btnsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btnsearch.Click
        If CheckFeature(Me.Loginid, Me.FormID, "SRCH", Me.AppId) Then
            lblMessage.Text = ""
            Me.SearchBy = ""
            Me.SearchBy = Me.SearchBy & " Status in ('A') and "
            If txtValidDate.Text <> "" Then
                Me.SearchBy = Me.SearchBy & " cast(RequestDate as date) <= '" & ConvertDate2(txtValidDate.Text) & "'  "
            End If
            Me.SortBy = ""
            pnlList.Visible = True
            pnlGrid.Visible = True
            DoBind(Me.SearchBy, Me.SortBy)
            With txtValidDate
                .Text = Me.BusinessDate.ToString("dd/MM/yyyy")
            End With
        End If
    End Sub
#End Region

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles DtgPC.SortCommand
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    'Private Sub DtgPC_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgPC.ItemDataBound
    '    If SessionInvalid() Then
    '        Exit Sub
    '    End If

    '    Dim hypReceive As LinkButton
    '    Dim lblInvoiceNo As Label
    '    Dim strHTTPServer As String
    '    Dim StrHTTPApp As String
    '    Dim strNameServer As String
    '    strHTTPServer = Request.ServerVariables("PATH_INFO")
    '    strNameServer = Request.ServerVariables("SERVER_NAME")
    '    StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)

    '    If e.Item.ItemIndex >= 0 Then
    '        hypReceive = CType(e.Item.FindControl("hypReceive"), LinkButton)
    '        lblInvoiceNo = CType(e.Item.FindControl("lblInvoiceNo"), Label)

    '        hypReceive.Attributes.Add("OnClick", "Response.Redirect('../../Webform.LoanMnt/PaymentRequest/PaymentRequestMulti.aspx'")

    '        'Response.Redirect("../../Webform.LoanMnt/PaymentRequest/PaymentRequestMulti.aspx")

    '        'hypReceive.Attributes.Add("OnClick", "return parent.window.location.href='http://" & strNameServer & "/" & StrHTTPApp & "/Webform.LoanMnt/PaymentRequest/PaymentRequestMulti.aspx'")
    '        'hypReceive.Attributes.Add("OnClick", "return parent.window.location.href='http://" & strNameServer & "/" & StrHTTPApp & "/Webform.LoanMnt/PaymentRequest/PaymentRequestMulti.aspx?InvoiceNo=" & lblInvoiceNo.Text.Trim & "'")
    '        'lnkNewApp.Attributes.Add("OnClick", "return parent.window.location.href='http://" & strNameServer & "/" & StrHTTPApp & "/Webform.LoanOrg/Credit/CreditProcess/NewApplication/Application_003.aspx?id=" & lblCust.Text.Trim & "&name=" & lnkCustomer.Text.Trim & "&type=P&page=Add&flag=0'")

    '    End If

    'End Sub

    Private Sub DtgPC_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgPC.ItemCommand

        If e.CommandName = "PROSES" Then
            Dim lblInvoiceNo As Label
            lblInvoiceNo = CType(DtgPC.Items(e.Item.ItemIndex).FindControl("lblInvoiceNo"), Label)
            'Response.Redirect("../../Webform.LoanMnt/PaymentRequest/PaymentRequestMulti.aspx?lblInvoiceNo=" & lblInvoiceNo.Text.Trim)
            Response.Redirect("../../Webform.LoanMnt/PaymentRequest/PaymentRequestMulti.aspx?lblInvoiceNo=" & lblInvoiceNo.Text.Trim & "&Flag=FA")
        End If

    End Sub

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan!", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                'DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub
#End Region


End Class