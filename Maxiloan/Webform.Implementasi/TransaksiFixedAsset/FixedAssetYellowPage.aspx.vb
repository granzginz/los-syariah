﻿Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.General.CommonCookiesHelper
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Public Class FixedAssetYellowPage
	Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
	Private currentPage As Int32 = 1
	Private pageSize As Int16 = 10
	Private currentPageNumber As Int16 = 1
	Private totalPages As Double = 1
	Private recordCount As Int64 = 1

	Private oCustomClass As New Parameter.GeneralPaging
	Private oController As New GeneralPagingController

	Private m_TotalAmount As Double
	Protected WithEvents GridNav As ucGridNav
#End Region
    Private Property Amount() As Double
        Get
            Return (CType(ViewState("Amount"), Double))
        End Get
        Set(value As Double)
            ViewState("Amount") = value
        End Set
    End Property
    Private Property ddjeniscetak() As String
        Get
            Return (CType(ViewState("ddjeniscetak"), String))
        End Get
        Set(value As String)
            ViewState("ddjeniscetak") = value
        End Set
    End Property
    Private Property InvoiceNo() As String
		Get
			Return (CType(ViewState("InvoiceNo"), String))
		End Get
		Set(value As String)
			ViewState("InvoiceNo") = value
		End Set
	End Property
	Private Property TempDataTable() As DataTable
		Get
			Return CType(ViewState("TempDataTable"), DataTable)
		End Get
		Set(ByVal Value As DataTable)
			ViewState("TempDataTable") = Value
		End Set
	End Property
	Private Property Jenis() As String
		Get
			Return CType(ViewState("Jenis"), String)
		End Get
		Set(ByVal Value As String)
			ViewState("Jenis") = Value
		End Set
	End Property
	Public Property GM1 As String
	Public Property GM2 As String
	Public Property GM3 As String
	Public Property Direksi As String

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If SessionInvalid() Then
			Exit Sub
		End If

		AddHandler GridNav.PageChanged, AddressOf PageNavigation
		If Not IsPostBack Then
			Dim err As String = Request.QueryString("message")

			If err <> String.Empty Then
				ShowMessage(lblMessage, err, True)
			End If

			If Request.QueryString("strFileLocation") <> "" Then
				Dim strHTTPServer = Request.ServerVariables("PATH_INFO")
				Dim strFileLocation = String.Format("http://{0}/{1}/XML/{2}.pdf", Request.ServerVariables("SERVER_NAME"), strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1), Request.QueryString("strFileLocation"))
				Response.Write("<script language = javascript>" & vbCrLf & "var x = screen.width; " & vbCrLf & "var y = screen.heigth; " & vbCrLf & "window.open('" & strFileLocation & "','Installment', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes') " & vbCrLf & "</script>")
			End If

			Me.FormID = "CetakFormKuningFA"

			If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
				Me.SearchBy = ""
				Me.SortBy = ""
			End If

		End If
	End Sub

	Private Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs)
		currentPage = e.CurrentPage
		DoBind(Me.SearchBy, Me.SortBy, True)
		GridNav.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
	End Sub

	Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String, Optional isFrNav As Boolean = False)
		Dim DtUserList As New DataTable
		Dim DvUserList As New DataView

        If ddjeniscetak = "PAYREQ" Then
            With oCustomClass
                .strConnection = GetConnectionString()
                .WhereCond = cmdWhere
                .CurrentPage = currentPage
                .PageSize = pageSize
                .SortBy = SortBy
                .SpName = "spPaymentFAInquiryPaging"
            End With
            oCustomClass = oController.GetGeneralPaging(oCustomClass)
            DtUserList = oCustomClass.ListData
            DvUserList = DtUserList.DefaultView
            recordCount = oCustomClass.TotalRecords
            DvUserList.Sort = Me.SortBy

            If (isFrNav = False) Then
                GridNav.Initialize(recordCount, pageSize)
            End If

            DtgPayreq.DataSource = DvUserList
            Try
                DtgPayreq.DataBind()
            Catch
                DtgPayreq.CurrentPageIndex = 0
                DtgPayreq.DataBind()
            End Try
            pnlDatagrid.Visible = True
            DtgPayreq.Visible = True
            DtgReqrec.Visible = False
            DtgSellfa.Visible = False
            DtgDelfa.Visible = False
        ElseIf ddjeniscetak = "REQREC" Then
            With oCustomClass
                .strConnection = GetConnectionString()
                .WhereCond = cmdWhere
                .CurrentPage = currentPage
                .PageSize = pageSize
                .SortBy = SortBy
                .SpName = "spReceiveFAInquiryPaging"
            End With
            oCustomClass = oController.GetGeneralPaging(oCustomClass)
            DtUserList = oCustomClass.ListData
            DvUserList = DtUserList.DefaultView
            recordCount = oCustomClass.TotalRecords
            DvUserList.Sort = Me.SortBy

            If (isFrNav = False) Then
                GridNav.Initialize(recordCount, pageSize)
            End If

            DtgReqrec.DataSource = DvUserList
            Try
                DtgReqrec.DataBind()
            Catch
                DtgReqrec.CurrentPageIndex = 0
                DtgReqrec.DataBind()
            End Try
            pnlDatagrid.Visible = True
            DtgReqrec.Visible = True
            DtgPayreq.Visible = False
            DtgSellfa.Visible = False
            DtgDelfa.Visible = False
        ElseIf ddjeniscetak = "SELLFA" Then
            With oCustomClass
                .strConnection = GetConnectionString()
                .WhereCond = cmdWhere
                .CurrentPage = currentPage
                .PageSize = pageSize
                .SortBy = SortBy
                .SpName = "spSellFAInquiryPaging"
            End With
            oCustomClass = oController.GetGeneralPaging(oCustomClass)
            DtUserList = oCustomClass.ListData
            DvUserList = DtUserList.DefaultView
            recordCount = oCustomClass.TotalRecords
            DvUserList.Sort = Me.SortBy

            If (isFrNav = False) Then
                GridNav.Initialize(recordCount, pageSize)
            End If

            DtgSellfa.DataSource = DvUserList
            Try
                DtgSellfa.DataBind()
            Catch
                DtgSellfa.CurrentPageIndex = 0
                DtgSellfa.DataBind()
            End Try
            pnlDatagrid.Visible = True
            DtgReqrec.Visible = False
            DtgPayreq.Visible = False
            DtgSellfa.Visible = True
            DtgDelfa.Visible = False
        ElseIf ddjeniscetak = "DELFA" Then
            With oCustomClass
                .strConnection = GetConnectionString()
                .WhereCond = cmdWhere
                .CurrentPage = currentPage
                .PageSize = pageSize
                .SortBy = SortBy
                .SpName = "spDellFAInquiryPaging"
            End With
            oCustomClass = oController.GetGeneralPaging(oCustomClass)
            DtUserList = oCustomClass.ListData
            DvUserList = DtUserList.DefaultView
            recordCount = oCustomClass.TotalRecords
            DvUserList.Sort = Me.SortBy

            If (isFrNav = False) Then
                GridNav.Initialize(recordCount, pageSize)
            End If

            DtgDelfa.DataSource = DvUserList
            Try
                DtgDelfa.DataBind()
            Catch
                DtgDelfa.CurrentPageIndex = 0
                DtgDelfa.DataBind()
            End Try
            pnlDatagrid.Visible = True
            DtgReqrec.Visible = False
            DtgPayreq.Visible = False
            DtgSellfa.Visible = False
            DtgDelfa.Visible = True
        End If

		lblMessage.Visible = False

	End Sub

	Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
		If InStr(Me.SortBy, "DESC") > 0 Then
			Me.SortBy = e.SortExpression
		Else
			Me.SortBy = e.SortExpression + " DESC"
		End If
		DoBind(Me.SearchBy, Me.SortBy)
	End Sub

	Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
		Me.SearchBy = ""
		txtsrcbypayreq.Text = ""
		txtsrcbyreqrec.Text = ""
		pnlDatagrid.Visible = False
	End Sub

	Public Sub checkAll(ByVal sender As Object, ByVal e As System.EventArgs)
		Dim loopitem As Int16
		If ddJenis.SelectedIndex = 0 Then
			For loopitem = 0 To CType(DtgPayreq.Items.Count - 1, Int16)
				Dim Chk = CType(DtgPayreq.Items(loopitem).FindControl("cbCheck"), CheckBox)
				If Chk.Enabled Then
					Chk.Checked = CType(sender, CheckBox).Checked
				Else
					Chk.Checked = False
				End If
			Next
		ElseIf ddJenis.SelectedIndex = 1 Then
			For loopitem = 0 To CType(DtgReqrec.Items.Count - 1, Int16)
				Dim Chk = CType(DtgReqrec.Items(loopitem).FindControl("cbCheck"), CheckBox)
				If Chk.Enabled Then
					Chk.Checked = CType(sender, CheckBox).Checked
				Else
					Chk.Checked = False
				End If
			Next
		End If
	End Sub
	'Private Sub DtgPayreq_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgPayreq.ItemDataBound
	'	Dim lblTemp As Label
	'	Dim lblRequestNo As New Label

	'	Me.InvoiceNo = lblRequestNo.Text.Trim
	'	If e.Item.ItemIndex >= 0 Then
	'		lblTemp = CType(e.Item.FindControl("lblAmount"), Label)
	'		lblRequestNo = CType(e.Item.FindControl("lblInvoiceNo"), Label)
	'		If Not lblTemp Is Nothing Then
	'			m_TotalAmount += CType(lblTemp.Text, Double)
	'		End If
	'	End If
	'	If e.Item.ItemType = ListItemType.Footer Then
	'		lblTemp = CType(e.Item.FindControl("lblTotalAmount"), Label)
	'		lblRequestNo = CType(e.Item.FindControl("lblInvoiceNo"), Label)
	'		If Not lblTemp Is Nothing Then
	'			lblTemp.Text = FormatNumber(m_TotalAmount.ToString, 2)
	'		End If
	'	End If
	'End Sub
	'Private Sub dtgList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgPayreq.ItemCommand
	'	Try
	'		Dim RequestNo As String = CType(e.Item.FindControl("lblInvoiceNo"), Label).Text.Trim
	'		If e.CommandName.Trim = "Verifikasi" Then
	'			Dim hyRequestNo = CType(e.Item.FindControl("lblInvoiceNo"), Label).Text.Trim

	'			pnlSearch.Visible = False
	'			pnlDatagrid.Visible = False

	'		End If
	'	Catch ex As Exception
	'		ShowMessage(lblMessage, ex.Message, True)
	'	End Try
	'End Sub

	Private Sub Buttonsearch_Click(sender As Object, e As EventArgs) Handles Buttonsearch.Click
        Me.SearchBy = ""

        Me.ddjeniscetak = ddJenis.SelectedItem.Value

        If ddsrcpayreq.SelectedValue <> "" Then
			If Me.SearchBy = "" Then
				Me.SearchBy = Me.SearchBy & " " & ddsrcpayreq.SelectedValue.Trim & " = '" & txtsrcbypayreq.Text.Trim & "'"
			Else
				Me.SearchBy = Me.SearchBy & "and " & ddsrcpayreq.SelectedValue.Trim & " = '" & txtsrcbypayreq.Text.Trim & "' "
			End If
		End If

        If ddsrcreqrec.SelectedValue <> "" Then
            If Me.SearchBy = "" Then
                Me.SearchBy = Me.SearchBy & " " & ddsrcreqrec.SelectedValue.Trim & " = '" & txtsrcbyreqrec.Text.Trim & "'"
            Else
                Me.SearchBy = Me.SearchBy & "and " & ddsrcreqrec.SelectedValue.Trim & " = '" & txtsrcbyreqrec.Text.Trim & "' "
            End If
        End If

        If ddsrcsellfa.SelectedValue <> "" Then
            If Me.SearchBy = "" Then
                Me.SearchBy = Me.SearchBy & " " & ddsrcsellfa.SelectedValue.Trim & " = '" & txtsrcbyreqrec.Text.Trim & "'"
            Else
                Me.SearchBy = Me.SearchBy & "and " & ddsrcsellfa.SelectedValue.Trim & " = '" & txtsrcbyreqrec.Text.Trim & "' "
            End If
        End If

        If ddsrcdelfa.SelectedValue <> "" Then
            If Me.SearchBy = "" Then
                Me.SearchBy = Me.SearchBy & " " & ddsrcdelfa.SelectedValue.Trim & " = '" & txtsrcbyreqrec.Text.Trim & "'"
            Else
                Me.SearchBy = Me.SearchBy & "and " & ddsrcdelfa.SelectedValue.Trim & " = '" & txtsrcbyreqrec.Text.Trim & "' "
            End If
        End If

        pnlDatagrid.Visible = True
		DoBind(Me.SearchBy, Me.SortBy)
		FillCbo(author1, 1)
		FillCbo(author2, 2)
		FillCbo(author3, 3)
		FillCbo(author4, 4)
	End Sub

	Public Function GetEmployee(ByVal CustomClass As Parameter.APDisbApp) As Parameter.APDisbApp
		Dim params() As SqlParameter = New SqlParameter(1) {}
		params(0) = New SqlParameter("@BranchID", SqlDbType.VarChar, 30)
		params(0).Value = CustomClass.BranchId
		params(1) = New SqlParameter("@EmployeePosition", SqlDbType.VarChar, 30)
		params(1).Value = CustomClass.EmployeePosition


		Try
			CustomClass.listDataReport = SqlHelper.ExecuteDataset(CustomClass.strConnection, CommandType.StoredProcedure, CustomClass.SpName, params).Tables(0)
			Return CustomClass
		Catch exp As Exception
			Throw New Exception(exp.Message)
		End Try
	End Function
	Protected Sub FillCbo(ByVal cboName As DropDownList, ByVal EmployeePosition As String)
		Dim customClass As New Parameter.APDisbApp
		With customClass
			.strConnection = GetConnectionString()
			.BranchId = Replace(Me.sesBranchId, "'", "")
			.EmployeePosition = EmployeePosition
			.CurrentPage = 1
			.PageSize = 100
			.SortBy = ""
			.SpName = "spCboGetEmployee"
		End With
		customClass = GetEmployee(customClass)
		Dim dt As DataTable
		dt = customClass.listDataReport
		With cboName
			.DataTextField = "EmployeeName"
			.DataValueField = "EmployeeID"
			.DataSource = dt
			.DataBind()
			.Items.Insert(0, "Select One")
			.Items(0).Value = ""
			.SelectedIndex = 0
		End With
	End Sub

	Private Sub BtnNext_Click(sender As Object, e As EventArgs) Handles BtnNext.Click
		Dim j = 0
		Dim dtChoosen As IList(Of String) = New List(Of String)
		Dim oDataTable As New DataTable
		Dim lblInvoiceNo As String
		Dim lblAktivaId As String
		Dim cbCheck As CheckBox

		Me.Jenis = ddJenis.SelectedValue.ToString.Trim


        If Me.Jenis.Trim = "PAYREQ" Then
            '*PAYMENT REQUEST PARSING COOKIES*'
            With oDataTable
                .Columns.Add(New DataColumn("InvoiceNo", GetType(String)))
            End With

            For intloop = 0 To DtgPayreq.Items.Count - 1
                cbCheck = CType(DtgPayreq.Items(intloop).Cells(0).FindControl("cbCheck"), CheckBox)
                If cbCheck.Checked Then
                    lblInvoiceNo = CType(DtgPayreq.Items(intloop).Cells(1).FindControl("lblInvoiceNo"), Label).Text.Trim
                    GM1 = author1.SelectedValue.Trim
                    GM2 = author2.SelectedValue.Trim
                    GM3 = author3.SelectedValue.Trim
                    Direksi = author4.SelectedValue.Trim

                    Me.InvoiceNo = lblInvoiceNo
                    Me.GM1 = GM1
                    Me.GM2 = GM2
                    Me.GM3 = GM3
                    Me.Direksi = Direksi
                End If
            Next

            For i = 0 To DtgPayreq.Items.Count - 1
                Dim cb = CType(DtgPayreq.Items(i).FindControl("cbCheck"), CheckBox)
                If cb.Checked = True Then
                    j += 1
                    dtChoosen.Add(CType(DtgPayreq.Items(i).FindControl("lblInvoiceNo"), Label).Text.Trim)
                End If
            Next

            If j <= 0 Then
                ShowMessage(lblMessage, "Harap Check Item", True)
                Exit Sub
            End If

        ElseIf Me.Jenis.Trim = "REQREC" Then
            '*REQUEST RECEIVE PARSING COOKIES*'

            With oDataTable
                .Columns.Add(New DataColumn("AktivaId", GetType(String)))
            End With

            For intloop = 0 To DtgReqrec.Items.Count - 1
                cbCheck = CType(DtgReqrec.Items(intloop).Cells(0).FindControl("cbCheck"), CheckBox)
                If cbCheck.Checked Then
                    lblAktivaId = CType(DtgReqrec.Items(intloop).Cells(1).FindControl("lblAktivaId"), Label).Text.Trim
                    GM1 = author1.SelectedValue.Trim
                    GM2 = author2.SelectedValue.Trim
                    GM3 = author3.SelectedValue.Trim
                    Direksi = author4.SelectedValue.Trim

                    Me.InvoiceNo = lblAktivaId
                    Me.GM1 = GM1
                    Me.GM2 = GM2
                    Me.GM3 = GM3
                    Me.Direksi = Direksi
                End If
            Next

            For i = 0 To DtgReqrec.Items.Count - 1
                Dim cb = CType(DtgReqrec.Items(i).FindControl("cbCheck"), CheckBox)
                If cb.Checked = True Then
                    j += 1
                    dtChoosen.Add(CType(DtgReqrec.Items(i).FindControl("lblAktivaId"), Label).Text.Trim)
                End If
            Next

            If j <= 0 Then
                ShowMessage(lblMessage, "Harap Check Item", True)
                Exit Sub
            End If

        ElseIf Me.Jenis.Trim = "SELLFA" Then
        '*REQUEST RECEIVE PARSING COOKIES*'

        With oDataTable
            .Columns.Add(New DataColumn("AktivaId", GetType(String)))
        End With

            For intloop = 0 To DtgSellfa.Items.Count - 1
                cbCheck = CType(DtgSellfa.Items(intloop).Cells(0).FindControl("cbCheck"), CheckBox)
                If cbCheck.Checked Then
                    lblAktivaId = CType(DtgSellfa.Items(intloop).Cells(1).FindControl("lblAktivaId"), Label).Text.Trim
                    GM1 = author1.SelectedValue.Trim
                    GM2 = author2.SelectedValue.Trim
                    GM3 = author3.SelectedValue.Trim
                    Direksi = author4.SelectedValue.Trim

                    Me.InvoiceNo = lblAktivaId
                    Me.GM1 = GM1
                    Me.GM2 = GM2
                    Me.GM3 = GM3
                    Me.Direksi = Direksi
                End If
            Next

            For i = 0 To DtgSellfa.Items.Count - 1
                Dim cb = CType(DtgSellfa.Items(i).FindControl("cbCheck"), CheckBox)
                If cb.Checked = True Then
                    j += 1
                    dtChoosen.Add(CType(DtgSellfa.Items(i).FindControl("lblAktivaId"), Label).Text.Trim)
                End If
            Next

            If j <= 0 Then
            ShowMessage(lblMessage, "Harap Check Item", True)
            Exit Sub
        End If

        ElseIf Me.Jenis.Trim = "DELFA" Then
        '*REQUEST RECEIVE PARSING COOKIES*'

        With oDataTable
            .Columns.Add(New DataColumn("AktivaId", GetType(String)))
        End With

            For intloop = 0 To DtgDelfa.Items.Count - 1
                cbCheck = CType(DtgDelfa.Items(intloop).Cells(0).FindControl("cbCheck"), CheckBox)
                If cbCheck.Checked Then
                    lblAktivaId = CType(DtgDelfa.Items(intloop).Cells(1).FindControl("lblAktivaId"), Label).Text.Trim
                    GM1 = author1.SelectedValue.Trim
                    GM2 = author2.SelectedValue.Trim
                    GM3 = author3.SelectedValue.Trim
                    Direksi = author4.SelectedValue.Trim

                    Me.InvoiceNo = lblAktivaId
                    Me.GM1 = GM1
                    Me.GM2 = GM2
                    Me.GM3 = GM3
                    Me.Direksi = Direksi
                End If
            Next

            For i = 0 To DtgDelfa.Items.Count - 1
                Dim cb = CType(DtgDelfa.Items(i).FindControl("cbCheck"), CheckBox)
                If cb.Checked = True Then
                    j += 1
                    dtChoosen.Add(CType(DtgDelfa.Items(i).FindControl("lblAktivaId"), Label).Text.Trim)
                End If
            Next

            If j <= 0 Then
            ShowMessage(lblMessage, "Harap Check Item", True)
            Exit Sub
        End If

        End If


        Session(COOKIES_PETTY_CASH_VOUCHER) = dtChoosen
		Dim cookie As HttpCookie = Request.Cookies("RptCetakFixedAsset")

        If Me.Jenis.Trim = "PAYREQ" Then
            If Not cookie Is Nothing Then
                cookie.Values("Invoiceno") = Me.InvoiceNo
                cookie.Values("Jenis") = Me.Jenis
                cookie.Values("GM1") = Me.GM1
                cookie.Values("GM2") = Me.GM2
                cookie.Values("GM3") = Me.GM3
                cookie.Values("Direksi") = Me.Direksi
                Response.AppendCookie(cookie)
            Else
                Dim cookieNew As New HttpCookie("RptPaymentRequest")
                cookieNew.Values("Invoiceno") = Me.InvoiceNo
                cookieNew.Values("Jenis") = Me.Jenis
                cookieNew.Values("GM1") = Me.GM1
                cookieNew.Values("GM2") = Me.GM2
                cookieNew.Values("GM3") = Me.GM3
                cookieNew.Values("Direksi") = Me.Direksi
                Response.AppendCookie(cookieNew)
            End If
        ElseIf Me.Jenis.Trim = "REQREC" Then
            If Not cookie Is Nothing Then
                cookie.Values("AktivaId") = Me.InvoiceNo
                cookie.Values("Jenis") = Me.Jenis
                cookie.Values("GM1") = Me.GM1
                cookie.Values("GM2") = Me.GM2
                cookie.Values("GM3") = Me.GM3
                cookie.Values("Direksi") = Me.Direksi
                Response.AppendCookie(cookie)
            Else
                Dim cookieNew As New HttpCookie("RptRequestReceive")
                cookieNew.Values("AktivaId") = Me.InvoiceNo
                cookieNew.Values("Jenis") = Me.Jenis
                cookieNew.Values("GM1") = Me.GM1
                cookieNew.Values("GM2") = Me.GM2
                cookieNew.Values("GM3") = Me.GM3
                cookieNew.Values("Direksi") = Me.Direksi
                Response.AppendCookie(cookieNew)
            End If
        ElseIf Me.Jenis.Trim = "SELLFA" Then
            If Not cookie Is Nothing Then
                cookie.Values("AktivaId") = Me.InvoiceNo
                cookie.Values("Jenis") = Me.Jenis
                cookie.Values("GM1") = Me.GM1
                cookie.Values("GM2") = Me.GM2
                cookie.Values("GM3") = Me.GM3
                cookie.Values("Direksi") = Me.Direksi
                Response.AppendCookie(cookie)
            Else
                Dim cookieNew As New HttpCookie("RptRequestReceive")
                cookieNew.Values("AktivaId") = Me.InvoiceNo
                cookieNew.Values("Jenis") = Me.Jenis
                cookieNew.Values("GM1") = Me.GM1
                cookieNew.Values("GM2") = Me.GM2
                cookieNew.Values("GM3") = Me.GM3
                cookieNew.Values("Direksi") = Me.Direksi
                Response.AppendCookie(cookieNew)
            End If
        ElseIf Me.Jenis.Trim = "DELFA" Then
            If Not cookie Is Nothing Then
                cookie.Values("AktivaId") = Me.InvoiceNo
                cookie.Values("Jenis") = Me.Jenis
                cookie.Values("GM1") = Me.GM1
                cookie.Values("GM2") = Me.GM2
                cookie.Values("GM3") = Me.GM3
                cookie.Values("Direksi") = Me.Direksi
                Response.AppendCookie(cookie)
            Else
                Dim cookieNew As New HttpCookie("RptPenghapusan")
                cookieNew.Values("AktivaId") = Me.InvoiceNo
                cookieNew.Values("Jenis") = Me.Jenis
                cookieNew.Values("GM1") = Me.GM1
                cookieNew.Values("GM2") = Me.GM2
                cookieNew.Values("GM3") = Me.GM3
                cookieNew.Values("Direksi") = Me.Direksi
                Response.AppendCookie(cookieNew)
            End If
        End If
		Response.Redirect("CetakFormKuningFA.aspx")
	End Sub
End Class