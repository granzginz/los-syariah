﻿Imports System.Data.SqlClient
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General
Imports Maxiloan.General.CommonCacheHelper
Public Class RequestReceiveFA
	Inherits Maxiloan.Webform.WebBased
	Protected WithEvents txtAmount As ucNumberFormat
	Protected WithEvents txtTanggalValuta As ucDateCE
	Protected WithEvents oBankAccount As UcBankAccountID

#Region "Private Constanta"
	Private oController As New ImplementasiControler
	Dim oCustomClass As New Parameter.TransFixedAsset
	Dim tempTotalPRAmount As Double
	Private Const SCHEME_ID As String = "PYR"
#End Region

#Region "Property"
	Private Property KodeAktiva() As String
		Get
			Return (CType(ViewState("AktivaId"), String))
		End Get
		Set(ByVal Value As String)
			ViewState("AktivaId") = Value
		End Set
	End Property
	Private Property Coa() As String
		Get
			Return (CType(ViewState("Coa"), String))
		End Get
		Set(ByVal Value As String)
			ViewState("Coa") = Value
		End Set
	End Property
	Private Property Buyer() As String
		Get
			Return (CType(ViewState("Buyer"), String))
		End Get
		Set(ByVal Value As String)
			ViewState("Buyer") = Value
		End Set
	End Property
	Private Property Amount() As Double
		Get
			Return (CType(ViewState("Jumlah"), Double))
		End Get
		Set(ByVal Value As Double)
			ViewState("Jumlah") = Value
		End Set
	End Property
	Private Property ReffNo() As String
		Get
			Return (CType(ViewState("ReffNo"), String))
		End Get
		Set(ByVal Value As String)
			ViewState("ReffNo") = Value
		End Set
	End Property
	Private Property InternalMemo() As String
		Get
			Return (CType(ViewState("InternalMemoNo"), String))
		End Get
		Set(ByVal Value As String)
			ViewState("InternalMemoNo") = Value
		End Set
	End Property
	Private Property Notes() As String
		Get
			Return (CType(ViewState("Notes"), String))
		End Get
		Set(ByVal Value As String)
			ViewState("Notes") = Value
		End Set
	End Property
#End Region

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If SessionInvalid() Then
			Exit Sub
		End If
        'If Me.IsHoBranch = False Then
        '	NotAuthorized()
        '	Exit Sub
        'End If
        Me.FormID = "PAYREQ"
		If Not Me.IsPostBack Then
			If Request.QueryString("strFileLocation") <> "" Then
				Dim strHTTPServer As String
				Dim StrHTTPApp As String
				Dim strNameServer As String
				Dim strFileLocation As String

				strHTTPServer = Request.ServerVariables("PATH_INFO")
				strNameServer = Request.ServerVariables("SERVER_NAME")
				StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
				strFileLocation = "http://" & strNameServer & "/" & StrHTTPApp & "/XML/" & Request.QueryString("strFileLocation") & ".pdf"

				Response.Write("<script language = javascript>" & vbCrLf _
				& "var x = screen.width; " & vbCrLf _
				& "var y = screen.height; " & vbCrLf _
				& "window.open('" & strFileLocation & "','Installment', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes') " & vbCrLf _
				& "</script>")

			End If
			If Request.QueryString("msg") = "ok" Then
				ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_SUCCESS, False)
			End If
			If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
				If SessionInvalid() Then
					Exit Sub
				End If

				Me.KodeAktiva = Request.QueryString("AktivaId")

				GetBankAccount()
				InitialDefaultPanel()
				GetRequestReceive()

				Me.SearchBy = ""
				Me.SortBy = ""

				txtAmount.RequiredFieldValidatorEnable = True
				txtAmount.RangeValidatorEnable = True
				txtAmount.TextCssClass = "numberAlign regular_text"
				txtTanggalValuta.Text = Me.BusinessDate.ToString("dd/MM/yyyy")

			End If
		End If
	End Sub
	Private Sub InitialDefaultPanel()

		TxtKodeAktiva.Enabled = False
		TxtCoa.Enabled = False
		TxtBuyer.Enabled = False
		txtAmount.Enabled = False
		txtReffNo.Enabled = False
		txtInternalMemo.Enabled = True
		txtNotes.Enabled = False

	End Sub

	Sub GetBankAccount()

		oBankAccount.BankPurpose = ""
		oBankAccount.BankType = ""
		oBankAccount.BindBankAccount()

		If Not IsNothing(Session("BankSelected")) Then oBankAccount.BankAccountID = Session("BankSelected").ToString.Trim

	End Sub

	Private Sub GetRequestReceive()
		Dim DtUserList As New DataTable

		If SessionInvalid() Then
			Exit Sub
		End If

		'* Belum Ada Parameter Apa untuk Get *'
		Me.SearchBy = " AktivaId = '" & Me.KodeAktiva.Trim & "' "

		With oCustomClass
			.strConnection = GetConnectionString()
			.WhereCond = Me.SearchBy
			.CurrentPage = 1
			.PageSize = 10
			.SortBy = ""
		End With

		oCustomClass = oController.RequestReceiveFAList(oCustomClass)

		DtUserList = oCustomClass.ListData

		'* Get data from tabel FA_RequestReceive *'
		Me.Coa = DtUserList.Rows(0)("Coa").ToString().Trim
		Me.Buyer = DtUserList.Rows(0)("Buyer").ToString().Trim
		Me.Amount = DtUserList.Rows(0)("Amount").ToString().Trim
		Me.ReffNo = DtUserList.Rows(0)("ReffNo").ToString().Trim
		Me.InternalMemo = DtUserList.Rows(0)("InternalMemoNo").ToString().Trim
		Me.Notes = DtUserList.Rows(0)("Notes").ToString().Trim

		'* Parse To Detail *'
		TxtKodeAktiva.Text = Me.KodeAktiva.Trim
		TxtCoa.Text = Me.Coa.Trim
		TxtBuyer.Text = Me.Buyer.Trim
		txtAmount.Text = FormatNumber(Me.Amount)
		txtReffNo.Text = Me.ReffNo.Trim
		txtInternalMemo.Text = Me.InternalMemo.Trim
		txtNotes.Text = Me.Notes.Trim

	End Sub

	Private Sub ButtonSaveApproval_Click(sender As Object, e As EventArgs) Handles ButtonSaveApproval.Click
		If SessionInvalid() Then
			Exit Sub
		End If
		If CheckFeature(Me.Loginid, Me.FormID, "SAVE", Me.AppId) Then

			If CDbl(txtAmount.Text) <= 0 Then
				ShowMessage(lblMessage, "Jumlah Terima harus > 0", True)
				Exit Sub
			End If

			Session.Add("BankSelected", oBankAccount.BankAccountID)

			With oCustomClass
				.strConnection = GetConnectionString()
				.BranchId = Me.sesBranchId.Replace("'", "")
				.LoginId = Me.Loginid
				.Amount = CDbl(txtAmount.Text)
				.ValueDate = ConvertDate2(txtTanggalValuta.Text)
				.ReferenceNo = txtInternalMemo.Text
				.BankAccountID = oBankAccount.BankAccountID
				.AktivaId = Me.KodeAktiva.Trim
			End With

			Dim ErrMsg As String
				oController.RequestReceiveFASave(oCustomClass)
				With oCustomClass
					ErrMsg = .strError
				End With
				If ErrMsg = "" Then
					ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_SUCCESS, False)
				ElseIf ErrMsg = "Double" Then
					ShowMessage(lblMessage, "Nomor Memo sudah digunakan, harap periksa kembali...!!", True)
					InitialDefaultPanel()
					Exit Sub
				Else
					ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_FAILED, True)
					InitialDefaultPanel()
					Exit Sub
				End If

				Response.Redirect("RequestReceiveFAList.aspx?msg=ok")
			End If
    End Sub
	Sub NotAuthorized()
		Dim strHTTPServer As String
		Dim StrHTTPApp As String
		Dim strNameServer As String
		strHTTPServer = Request.ServerVariables("PATH_INFO")
		strNameServer = Request.ServerVariables("SERVER_NAME")
		StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
		Response.Redirect("http://" & strNameServer & "/" & StrHTTPApp & "/error_notauthorized.aspx")
	End Sub
End Class