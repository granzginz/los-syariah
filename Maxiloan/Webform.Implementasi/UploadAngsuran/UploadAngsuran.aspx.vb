﻿#Region "Imports"
Imports Maxiloan.Controller
Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.Webform.UserController
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data.OleDb
Imports System.Data.Odbc
Imports System.Text
Imports System.Data.SqlClient
Imports System.IO
Imports System.Web.UI.WebControls.FileUpload
Imports System.Text.RegularExpressions
Imports System.Diagnostics
Imports System.Drawing
Imports System.Runtime.InteropServices
Imports DocumentFormat.OpenXml.Packaging
Imports DocumentFormat.OpenXml.Spreadsheet

#End Region

Public Class UploadAngsuran
    Inherits Maxiloan.Webform.WebBased

    Private myCmd As New SqlCommand
    Private myAdapter As New SqlDataAdapter
    Private myDS As New DataSet
    Private myReader As DataTableReader
    Private FileName As String
    Private strFileTemp As String
    Private strFile As String
    Private _NoKontrak As String
    Private FileDirectory As String
    Private TotalAgreement As Double
    Private TotalAmountReceive As Decimal
    Private FileType As String
    Private _TglTransaksi As String
    Private _NoRekening As String
    Private _BranchID As String
    Private _AgreementNo As String
    Private objCon As SqlConnection
    Private TextFile As String

    Public Property FilaName() As String
        Get
            Return CType(ViewState("FilaName"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("FilaName") = Value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblLoading.Visible = False
        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not Me.IsPostBack Then

            Me.FormID = "UploadAngsuran"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) = False Then
                Exit Sub
            End If
        End If
    End Sub
    Private Sub btnUpload_Click(sender As Object, e As System.EventArgs) Handles btnUpload.Click
        lblLoading.Visible = True
        btnUpload.Enabled = False
        Try
            If FileVA.HasFile Then
                strFileTemp = FileVA.FileName
                FileName = Context.Request.MapPath("~\\xml\\") + strFileTemp
                File.Delete(FileName)
                FileVA.SaveAs(FileName)
                Dim larr As Array = CType(strFileTemp.ToString.Split(CChar(".")), Array)
                TextFile = strFileTemp
                FileDirectory = Context.Request.MapPath("~\\xml\\").ToString.Trim
                FileType = larr.GetValue(1).ToString.Trim()

                Me.FileName = FileName
                DoProses()

            Else
                lblLoading.Visible = False
                ShowMessage(lblMessage, "File belum dipilih!", True)
                Exit Sub
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try

        btnUpload.Enabled = True
        lblLoading.Visible = False
    End Sub
    Function ValidasiFile() As Boolean
        Dim result As Boolean = True
        objCon = New SqlConnection(GetConnectionString)
        Try
            myDS = New DataSet
            myCmd.Parameters.Clear()
            myCmd.CommandText = "spGetUploadPaymentBybatchValidFile"
            myCmd.CommandType = CommandType.StoredProcedure
            myCmd.Parameters.Add(New SqlParameter("@FileName", SqlDbType.VarChar, 200) With {.Value = strFileTemp.Trim})
            myCmd.Connection = objCon
            myAdapter.SelectCommand = myCmd
            myAdapter.Fill(myDS)

            If myDS.Tables(0).Rows.Count > 0 Then
                result = False
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message)
            ShowMessage(lblMessage, ex.Message, True)
        End Try
        Return result
    End Function
    Private Function Text_To_DataTable(ByVal path As String, ByVal delimitter As Char, ByVal header As Boolean) As DataTable
        Dim source As String = String.Empty
        Dim dt As DataTable = New DataTable

        If IO.File.Exists(path) Then
            source = IO.File.ReadAllText(path)
        Else
            Throw New IO.FileNotFoundException("Could not find the file at " & path, path)
        End If

        Dim rows() As String = source.Split({Environment.NewLine}, StringSplitOptions.None)

        For i As Integer = 0 To rows(0).Split(delimitter).Length - 1
            Dim column As String = rows(0).Split(delimitter)(i)
            dt.Columns.Add(If(header, column, "column" & i + 1))
        Next

        For i As Integer = If(header, 1, 0) To rows.Length - 1
            Dim dr As DataRow = dt.NewRow

            For x As Integer = 0 To rows(i).Split(delimitter).Length - 1
                If x <= dt.Columns.Count - 1 Then
                    dr(x) = rows(i).Split(delimitter)(x)
                Else
                    Throw New Exception("The number of columns on row " & i + If(header, 0, 1) & " is greater than the amount of columns in the " & If(header, "header.", "first row."))
                End If
            Next

            dt.Rows.Add(dr)
        Next

        Return dt
    End Function

    Private Function Text_To_DataTable_2(ByVal path As String, ByVal delimitter As Char, ByVal header As Boolean) As DataTable
        Dim source As String = String.Empty
        Dim dt As DataTable = New DataTable

        If IO.File.Exists(path) Then
            source = IO.File.ReadAllText(path)
        Else
            Throw New IO.FileNotFoundException("Could not find the file at " & path, path)
        End If

        Dim rows() As String = source.Split({Environment.NewLine}, StringSplitOptions.None)

        For i As Integer = 0 To rows(0).Split(delimitter).Length - 1
            Dim column As String = rows(0).Split(delimitter)(i)
            dt.Columns.Add(If(header, column, "column" & i + 1))
        Next

        For i As Integer = 0 To rows.Length - 1
            Dim dr As DataRow = dt.NewRow

            For x As Integer = 0 To rows(i).Split(delimitter).Length - 1
                If x <= dt.Columns.Count - 1 Then
                    Dim str As String = rows(i).Split(delimitter)(x).Trim

                    Try
                        Dim number As Decimal
                        number = Convert.ToDecimal(str.Substring(3, 20))
                        dr(x) = str
                        dt.Rows.Add(dr)
                    Catch ex As Exception
                        Continue For
                    End Try
                Else
                    Throw New Exception("The number of columns on row " & i + If(header, 0, 1) & " is greater than the amount of columns in the " & If(header, "header.", "first row."))
                End If
            Next
        Next

        Return dt
    End Function

    'Private Sub DoProses()
    '    Dim SequenceNo As Integer = 0
    '    Dim list As New List(Of Parameter.UploadAngsuran)
    '    If ValidasiFile() Then
    '        list = UploadFile()
    '    Else
    '        ShowMessage(lblMessage, "File pernah di upload!", True)
    '        Exit Sub
    '    End If
    '    Try
    '        TotalAgreement = list.Count
    '        SaveLog(SequenceNo)
    '        If list.Count > 0 Then
    '            For index = 0 To list.Count - 1
    '                Dim payment As New Parameter.UploadAngsuran
    '                payment = list(index)
    '                payment.UploadDate = BusinessDate
    '                payment.SequenceNo = SequenceNo
    '                SaveUploadAngsuran(payment)
    '            Next
    '        End If
    '        UpdateLog(SequenceNo)
    '        ShowMessage(lblMessage, "Upload Data Angsuran Berhasil", False)
    '    Catch ex As Exception
    '        ShowMessage(lblMessage, ex.Message, True)
    '    End Try
    'End Sub

    Private Function createDataUpload() As DataTable
        Dim dt As New DataTable

        dt.Columns.Add("UploadDate")
        dt.Columns.Add("ValutaDate")
        dt.Columns.Add("FileName")
        dt.Columns.Add("FileDirectory")
        dt.Columns.Add("TotalAgreement")
        dt.Columns.Add("TotalAmountReceive")
        dt.Columns.Add("sequenceno")

        dt.Columns.Add("WaktuTransaksi")
        dt.Columns.Add("AgreementNo")
        dt.Columns.Add("CustomerName")
        dt.Columns.Add("AmountReceive")
        dt.Columns.Add("JournalNo")
        dt.Columns.Add("StatusLog")
        dt.Columns.Add("Keterangan")

        Return dt
    End Function
    'modify Nofi 2018-10-24
    Private Sub DoProses()
        Dim SequenceNo As Integer = 0
        Dim list As New List(Of Parameter.UploadAngsuran)
        If ValidasiFile() Then
            list = UploadFile()
        Else
            ShowMessage(lblMessage, "File pernah di upload!", True)
            Exit Sub
        End If
        Try
            Dim dt As DataTable = createDataUpload()
            TotalAgreement = list.Count
            If list.Count > 0 Then
                For index = 0 To list.Count - 1
                    Dim payment As New Parameter.UploadAngsuran
                    payment = list(index)
                    payment.UploadDate = BusinessDate
                    payment.SequenceNo = SequenceNo
                    dt.Rows.Add(payment.UploadDate, payment.TglTransaksi, strFileTemp, FileDirectory, TotalAgreement, TotalAmountReceive, SequenceNo, payment.WaktuTransaksi, payment.NoKontrak,
                                payment.NamaNasabah, payment.NilaiTransaksi, payment.NoJournal, payment.Post, payment.Keterangan)
                Next
            End If

            UploadAngsuranByBatchSave(dt)
            ShowMessage(lblMessage, "Upload Data Angsuran Berhasil", False)

        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub


    Sub UploadAngsuranByBatchSave(ByRef udtTable As DataTable)
        Try
            objCon = New SqlConnection(GetConnectionString)
            myCmd.Parameters.Clear()
            myCmd.CommandText = "SPUploadAngsuranByBatchSave"
            myCmd.CommandType = CommandType.StoredProcedure
            myCmd.Parameters.Add(New SqlParameter("@udtTable", SqlDbType.Structured) With {.Value = udtTable})


            objCon.Open()
            myCmd.Connection = objCon
            myCmd.CommandTimeout = 0
            myCmd.ExecuteNonQuery()
            objCon.Close()
        Catch ex As Exception
            Throw New Exception(ex.Message)
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub


    Function UploadFile() As List(Of Parameter.UploadAngsuran)
        Dim list As New List(Of Parameter.UploadAngsuran)
        Dim data As New DataTable
        Dim NoRekening As String = ""
        Dim TglTransaksi As Date = Nothing

        Try
            'data = CSVToDataTable(Me.FileName)--> kalau pakai csv
            data = FileToDataTable()

            For index = 0 To data.Rows.Count - 1
                If (index >= 0) Then
                    Dim Payment As New Parameter.UploadAngsuran

                    'Dim arr As String() = data.Rows(index).Item(0).ToString().Split(New Char() {""""c})
                    'Dim parts As String() = arr(0).ToString().Split(New Char() {","c})

                    Payment.TglTransaksi = ConvertDate2(IIf(data.Rows(index).Item(1).ToString().Trim = "", "01/01/1900", data.Rows(index).Item(1).ToString().Trim))
                    Payment.WaktuTransaksi = Now.Hour.ToString("00") & ":" & Now.Minute.ToString("00") & ":" & Now.Second.ToString("00")
                    Payment.NoKontrak = data.Rows(index).Item(2).ToString()
                    Payment.NamaNasabah = data.Rows(index).Item(3).ToString()
                    Payment.NilaiTransaksi = data.Rows(index).Item(4).ToString()
                    Payment.NoJournal = data.Rows(index).Item(5).ToString()
                    Payment.Keterangan = data.Rows(index).Item(6).ToString()
                    Payment.Post = "N"

                    Me._TglTransaksi = Payment.TglTransaksi
                    Me._NoKontrak = Payment.NoKontrak

                    Dim query As New Parameter.UploadAngsuran
                    If list.Count > 0 Then
                        query = list.Find(AddressOf PredicateFunction)
                    Else
                        query = Nothing
                    End If

                    If query Is Nothing Then
                        TotalAmountReceive = TotalAmountReceive + Payment.NilaiTransaksi
                        list.Add(Payment)
                    End If
                End If

            Next
        Catch ex As Exception
            Throw New ApplicationException(ex.Message)
        End Try

        Return list
    End Function
    Public Function PredicateFunction(ByVal custom As Parameter.UploadAngsuran) As Boolean
        Return custom.TglTransaksi = Me._TglTransaksi And custom.NoKontrak = Me._NoKontrak
    End Function

    Sub SaveUploadAngsuran(ByVal payment As Parameter.UploadAngsuran)
        Try
            objCon = New SqlConnection(GetConnectionString)
            myCmd.Parameters.Clear()
            myCmd.CommandText = "spUploadPaymentBatchSave"
            myCmd.CommandType = CommandType.StoredProcedure
            myCmd.Parameters.Add(New SqlParameter("@UploadDate", SqlDbType.DateTime) With {.Value = payment.UploadDate})
            myCmd.Parameters.Add(New SqlParameter("@ValutaDate", SqlDbType.Date) With {.Value = payment.TglTransaksi})
            myCmd.Parameters.Add(New SqlParameter("@WaktuTransaksi", SqlDbType.Time) With {.Value = payment.WaktuTransaksi})
            myCmd.Parameters.Add(New SqlParameter("@SequenceNo", SqlDbType.SmallInt) With {.Value = payment.SequenceNo})
            myCmd.Parameters.Add(New SqlParameter("@AgreementNo", SqlDbType.Char, 20) With {.Value = payment.NoKontrak})
            myCmd.Parameters.Add(New SqlParameter("@CustomerName", SqlDbType.VarChar, 50) With {.Value = payment.NamaNasabah})
            myCmd.Parameters.Add(New SqlParameter("@AmountReceive", SqlDbType.Decimal) With {.Value = payment.NilaiTransaksi})
            myCmd.Parameters.Add(New SqlParameter("@JournalNo", SqlDbType.Char, 20) With {.Value = payment.NoJournal})
            myCmd.Parameters.Add(New SqlParameter("@StatusLog", SqlDbType.Char, 1) With {.Value = payment.Post})
            myCmd.Parameters.Add(New SqlParameter("@Keterangan", SqlDbType.VarChar, 100) With {.Value = payment.Keterangan})

            objCon.Open()
            myCmd.Connection = objCon
            myCmd.ExecuteNonQuery()
            objCon.Close()
        Catch ex As Exception
            Throw New Exception(ex.Message)
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub
    Sub SaveLog(ByRef SequenceNo As Integer)
        Try

            objCon = New SqlConnection(GetConnectionString)
            myCmd.Parameters.Clear()
            myCmd.CommandText = "spUploadPaymentBybatchLogSave"
            myCmd.CommandType = CommandType.StoredProcedure
            myCmd.Parameters.Add(New SqlParameter("@UploadDate", SqlDbType.DateTime) With {.Value = BusinessDate})
            myCmd.Parameters.Add(New SqlParameter("@ValutaDate", SqlDbType.DateTime) With {.Value = BusinessDate})
            myCmd.Parameters.Add(New SqlParameter("@FileName", SqlDbType.VarChar, 200) With {.Value = strFileTemp})
            myCmd.Parameters.Add(New SqlParameter("@FileDirectory", SqlDbType.VarChar, 8000) With {.Value = FileDirectory})
            myCmd.Parameters.Add(New SqlParameter("@TotalAgreement", SqlDbType.BigInt) With {.Value = TotalAgreement})
            myCmd.Parameters.Add(New SqlParameter("@TotalAmountReceive", SqlDbType.Decimal) With {.Value = TotalAmountReceive})
            Dim prmErr = New SqlParameter("@SequenceNo", SqlDbType.SmallInt) With {.Direction = ParameterDirection.Output}
            myCmd.Parameters.Add(prmErr)
            objCon.Open()
            myCmd.Connection = objCon
            myCmd.CommandTimeout = 0
            myCmd.ExecuteNonQuery()
            objCon.Close()
            SequenceNo = CDbl(prmErr.Value)
        Catch ex As Exception
            Throw New Exception(ex.Message)
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub
    Sub UpdateLog(ByRef SequenceNo As Integer)
        Try
            objCon = New SqlConnection(GetConnectionString)
            myCmd.Parameters.Clear()
            myCmd.CommandText = "spUploadPaymentBybatchLogUpdate"
            myCmd.CommandType = CommandType.StoredProcedure
            myCmd.Parameters.Add(New SqlParameter("@UploadDate", SqlDbType.DateTime) With {.Value = BusinessDate})
            myCmd.Parameters.Add(New SqlParameter("@SequenceNo", SqlDbType.SmallInt) With {.Value = SequenceNo})

            objCon.Open()
            myCmd.Connection = objCon
            myCmd.CommandTimeout = 0
            myCmd.ExecuteNonQuery()
            objCon.Close()
        Catch ex As Exception
            Throw New Exception(ex.Message)
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub
    Private Sub btnReset_Click(sender As Object, e As System.EventArgs) Handles btnReset.Click
        Response.Redirect("UploadAngsuran.aspx")
    End Sub
    Private Function GetValue(doc As SpreadsheetDocument, cell As Cell) As String
        Dim value As String = cell.CellValue.InnerText
        If cell.DataType IsNot Nothing AndAlso cell.DataType.Value = CellValues.SharedString Then
            Return doc.WorkbookPart.SharedStringTablePart.SharedStringTable.ChildElements.GetItem(Integer.Parse(value)).InnerText
        End If
        Return value
    End Function
    Private Function FileToDataTable() As DataTable
        'Create a new DataTable.
        Dim dt As New DataTable()

        Using doc As SpreadsheetDocument = SpreadsheetDocument.Open(FileName, False)
            'Read the first Sheet from Excel file.
            Dim sheet As Sheet = doc.WorkbookPart.Workbook.Sheets.GetFirstChild(Of Sheet)()

            'Get the Worksheet instance.
            Dim worksheet As Worksheet = TryCast(doc.WorkbookPart.GetPartById(sheet.Id.Value), WorksheetPart).Worksheet

            'Fetch all the rows present in the Worksheet.
            Dim rows As IEnumerable(Of Row) = worksheet.GetFirstChild(Of SheetData)().Descendants(Of Row)()

            'Loop through the Worksheet rows.
            For Each row As Row In rows
                'Use the first row to add columns to DataTable.
                If row.RowIndex.Value = 1 Then
                    Dim clm As Integer = 0
                    For Each cell As Cell In row.Descendants(Of Cell)()
                        If GetValue(doc, cell).Trim <> "" Then
                            dt.Columns.Add(GetValue(doc, cell))
                        Else
                            dt.Columns.Add(GetValue(doc, cell).Trim & clm.ToString)
                        End If
                        clm += 1
                    Next
                Else
                    'Add rows to DataTable.
                    dt.Rows.Add()
                    Dim i As Integer = 0
                    For Each cell As Cell In row.Descendants(Of Cell)()
                        dt.Rows(dt.Rows.Count - 1)(i) = GetValue(doc, cell)
                        i += 1
                    Next
                End If
            Next
        End Using
        Return dt
    End Function

    Private Function CSVToDataTable(ByVal path As String) As DataTable

        Dim lines = IO.File.ReadAllLines(path)
        Dim tbl As New DataTable
        tbl.Columns.Add(New DataColumn("ColumnA", GetType(String)))

        For Each line In lines
            If (line.Length > 0) Then
                Dim oRow As DataRow
                oRow = tbl.NewRow
                tbl.Rows.Add(line)
            End If
        Next

        Return tbl
    End Function

End Class