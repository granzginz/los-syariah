﻿#Region "Imports"
Imports Maxiloan.Controller
Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.Webform.UserController
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data.OleDb
Imports System.Data.Odbc
Imports System.Text
Imports System.Data.SqlClient
Imports System.IO
Imports System.Web.UI.WebControls.FileUpload
Imports System.Text.RegularExpressions
Imports System.Diagnostics
Imports System.Drawing
Imports System.Runtime.InteropServices
Imports Maxiloan.Parameter
Imports Maxiloan.Framework.SQLEngine
#End Region

Public Class EksekusiUploadAngsuran
    Inherits Maxiloan.Webform.WebBased

    Private myCmd As New SqlCommand
    Private myAdapter As New SqlDataAdapter
    Private myDS As New DataSet
    Private myReader As DataTableReader
    Private objCon As SqlConnection
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 25
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private currentPageRK As Int32 = 1
    Private pageSizeRK As Int16 = 25
    Private currentPageNumberRK As Int16 = 1
    Private totalPagesRK As Double = 1
    Private recordCountRK As Int64 = 1

    Private oCustomClass As New Parameter.AgreementList
    Private oController As New AgreementListController
    Private oController2 As New APDisbAppController
    Private bController As New DataUserControlController
    Protected WithEvents oBranch As ucBranchAll
    Private dtBankAccount As New DataTable

    Private Property SearchByRK() As String
        Get
            Return CType(ViewState("SearchByRK"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("SearchByRK") = Value
        End Set
    End Property
    Private Property SortRK() As String
        Get
            Return CType(ViewState("SortRK"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("SortRK") = Value
        End Set
    End Property
    Private Property ListBankAccount() As DataTable
        Get
            Return (CType(ViewState("ListBankAccount"), DataTable))
        End Get
        Set(ByVal Value As DataTable)
            ViewState("ListBankAccount") = Value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not Me.IsPostBack Then
            If Me.IsHoBranch Then
                ShowMessage(lblMessage, "Harus Login Kantor Cabang", True)
                pnlDatagrid.Visible = False
                pnlHeader.Visible = False
                Exit Sub
            Else
                Me.FormID = "OtorUploadAngs"
                If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                    'GetCombo()
                    Me.SortBy = "Name,NoPelanggan ASC"
                    SortRK = "Name,AgreementNo ASC"
                    btnSave.Visible = False
                    oBranch.IsAll = False
                    oBranch.ViewObranch()
                    'fillRekBankCbo()
                    BindBankAccount()
                    'loadComboBankAccount("Type = 'BA'")
                End If
            End If
        End If
    End Sub
    'Sub GetCombo()
    '    objCon = New SqlConnection(GetConnectionString)
    '    myDS = New DataSet
    '    myCmd.Parameters.Clear()
    '    myCmd.CommandText = "spGetComboVirtualAccount"
    '    myCmd.Connection = objCon
    '    myAdapter.SelectCommand = myCmd
    '    myAdapter.Fill(myDS)

    '    If myDS.Tables(0).Rows.Count > 0 Then
    '        cbVirtualAccount.DataTextField = "VirtualAccountName"
    '        cbVirtualAccount.DataValueField = "VirtualAccountID"
    '        cbVirtualAccount.DataSource = myDS.Tables(0)
    '        cbVirtualAccount.DataBind()
    '    End If
    'End Sub
    Private Sub DoBind(ByVal strSort As String, ByVal strSearch As String)
        Dim cContract As New InstallRcvController
        Dim oContract As New Parameter.GeneralPaging

        With oContract
            .strConnection = GetConnectionString()
            .WhereCond = strSearch
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = strSort
        End With
        oContract = GetData(oContract)

        recordCount = oContract.TotalRecords
        DtgAgree.DataSource = oContract.ListData

        Try
            DtgAgree.DataBind()
            PagingFooter()
        Catch ex As Exception
            DtgAgree.CurrentPageIndex = 0
            DtgAgree.DataBind()
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

    Public Function GetData(ByVal oCustomClass As Parameter.GeneralPaging) As Parameter.GeneralPaging
        Dim oReturnValue As New Parameter.GeneralPaging
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spUploadAngsuranOtorPaging", params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Sub SelectAll(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim chkSender As CheckBox = CType(sender, CheckBox)
        Dim chkItem As CheckBox
        Dim x As Integer

        For x = 0 To DtgAgree.Items.Count - 1
            chkItem = CType(DtgAgree.Items(x).FindControl("chkItem"), CheckBox)
            chkItem.Checked = chkSender.Checked
        Next
    End Sub

    Private Sub PagingFooter()
        'lblPage.Text = currentPage.ToString()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SortBy, Me.SearchBy)
    End Sub
    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles imgbtnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SortBy, Me.SearchBy)
            End If
        End If
    End Sub
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles DtgAgree.SortCommand
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
            DoBind(e.SortExpression, Me.SearchBy)
        Else
            Me.SortBy = e.SortExpression + " DESC"
            DoBind(e.SortExpression + " DESC", Me.SearchBy)
        End If
    End Sub

    Private Sub btnSearch_Click(sender As Object, e As System.EventArgs) Handles btnSearch.Click

        Me.SearchBy = " where Agreement.BranchID='" & Me.sesBranchId.Replace("'", "") & "'"
        SearchByRK = " where Agreement.BranchID='" & Me.sesBranchId.Replace("'", "") & "'"


        DoBind(Me.SortBy, Me.SearchBy)
        btnSave.Visible = True
        pnlHeader.Visible = False
        pnlDatagrid.Visible = True
    End Sub



    Private Sub btnReset_Click(sender As Object, e As System.EventArgs) Handles btnReset.Click
        Response.Redirect("EksekusiUploadAngsuran.aspx")
    End Sub

    Private Sub btnCari_Click(sender As Object, e As System.EventArgs) Handles btnCari.Click
        Me.SearchBy = " where  Agreement.BranchID='" & Me.sesBranchId.Replace("'", "") & "'"
        If cboSearchBy.SelectedItem.Value <> "" And txtSearchBy.Text <> "" Then
            Me.SearchBy += " and " & cboSearchBy.SelectedItem.Value.Trim & " LIKE '%" & txtSearchBy.Text.Trim & "%'"
        End If
        DoBind(Me.SortBy, Me.SearchBy)
    End Sub
    Sub fillRekBankCbo()
        Dim ds = oController2.LoadComboRekeningBank(GetConnectionString, "BA")
        ddlRekeningBank.DataSource = ds
        ddlRekeningBank.DataTextField = "Text"
        ddlRekeningBank.DataValueField = "Value"
        ddlRekeningBank.DataBind()
        'ddlRekeningBank.Items.Insert("0", "Select One")
    End Sub
    Private Sub BindBankAccount()
        'Me.Cache.Remove(CACHE_BANKACCOUNT & Me.sesBranchId.Replace("'", ""))
        dtBankAccount = CType(Me.Cache.Item(CACHE_BANKACCOUNT & Me.sesBranchId.Replace("'", "")), DataTable)
        If dtBankAccount Is Nothing Then
            Dim dtBankAccountCache As New DataTable
            dtBankAccountCache = bController.GetBankAccount(GetConnectionString, Me.sesBranchId, "", "")
            Me.Cache.Insert(CACHE_BANKACCOUNT & Me.sesBranchId.Replace("'", ""), dtBankAccountCache, Nothing, DateTime.Now.AddHours(DEFAULT_TIME_DURATION_CACHE_HOUR), TimeSpan.Zero)
            dtBankAccount = CType(Me.Cache.Item(CACHE_BANKACCOUNT & Me.sesBranchId.Replace("'", "")), DataTable)
        End If
        Me.ListBankAccount = dtBankAccount

        With ddlRekeningBank
            .Items.Clear()
            .DataValueField = "ID"
            .DataTextField = "Name"
            .DataSource = bController.GetBankAccount(GetConnectionString, Me.sesBranchId, "", "")
            .DataBind()
            '.Items.Insert(0, "Select One")
            '.Items(0).Value = "0"
        End With


        'scriptJava.InnerHtml = GenerateScript(dtBankAccount)
    End Sub
    Protected Sub loadComboBankAccount(ByVal strSelect As String)
        Dim dv1 As New DataView(Me.ListBankAccount, strSelect, "", DataViewRowState.Added)

        With ddlRekeningBank
            .Items.Clear()
            .DataValueField = "ID"
            .DataTextField = "Name"
            .DataSource = dv1
            .DataBind()
            '.Items.Insert(0, "Select One")
            '.Items(0).Value = "0"
        End With
    End Sub

    Private Sub DtgAgree_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgree.ItemDataBound
        Dim lnkApplicationID As HyperLink
        Dim lnkAgreementNo As HyperLink
        Dim lblBranchID As Label

        If e.Item.ItemIndex >= 0 Then
            lnkAgreementNo = CType(e.Item.FindControl("lnkAgreementNo"), HyperLink)
            lblBranchID = CType(e.Item.FindControl("lblBranchID"), Label)

            If lnkAgreementNo.Text.Trim.Length > 0 Then
                lnkAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccAcq" & "', '" & Server.UrlEncode(lnkAgreementNo.Text.Trim) & "')"
            End If
            lnkApplicationID = CType(e.Item.FindControl("lnkApplicationID"), HyperLink)
            If lnkApplicationID.Text.Trim.Length > 0 Then
                lnkApplicationID.NavigateUrl = "javascript:OpenApplicationId('" & "AccAcq" & "', '" & Server.UrlEncode(lnkApplicationID.Text.Trim) & "','" & Server.UrlEncode(lblBranchID.Text.Trim) & "')"
            End If
        End If
    End Sub

    Private Sub btnDelete_Click(sender As Object, e As System.EventArgs) Handles btnDelete.Click
        Dim lblSequenceNo As Label
        Dim lblUploadDate As Label
        Dim lblValueDate As Label
        Dim lblBranchID As Label
        Dim lblBankAccountID As Label
        Dim lnkApplicationID As HyperLink
        Dim lnkAgreementNo As HyperLink
        Dim lblNilaiTransaksi As Label
        Dim listCheked As New List(Of Integer)

        Dim chkDtList As CheckBox
        For i = 0 To DtgAgree.Items.Count - 1
            chkDtList = CType(DtgAgree.Items(i).FindControl("chkItem"), CheckBox)
            If Not IsNothing(chkDtList) Then
                If chkDtList.Enabled Then

                    If chkDtList.Checked Then
                        listCheked.Add(i)
                        lblSequenceNo = CType(DtgAgree.Items(i).FindControl("lblSequenceNo"), Label)
                        lblUploadDate = CType(DtgAgree.Items(i).FindControl("lblUploadDate"), Label)
                        lblValueDate = CType(DtgAgree.Items(i).FindControl("lblValueDate"), Label)
                        lblBranchID = CType(DtgAgree.Items(i).FindControl("lblBranchID"), Label)
                        lnkApplicationID = CType(DtgAgree.Items(i).FindControl("lnkApplicationID"), HyperLink)
                        lblBankAccountID = CType(DtgAgree.Items(i).FindControl("lblBankAccountID"), Label)
                        lnkAgreementNo = CType(DtgAgree.Items(i).FindControl("lnkAgreementNo"), HyperLink)
                        lblNilaiTransaksi = CType(DtgAgree.Items(i).FindControl("lblNilaiTransaksi"), Label)
                        Try
                            Dim Param As New VirtualAccountOtor
                            With Param
                                .SequenceNo = CInt(lblSequenceNo.Text)
                                .UploadDate = CDate(lblUploadDate.Text)
                                .ValueDate = CDate(lblValueDate.Text)
                                .BranchId = lblBranchID.Text.Trim
                                .ApplicationID = lnkApplicationID.Text.Trim
                                .BankAccountID = ddlRekeningBank.SelectedValue.Trim
                                .NoPelanggan = lnkAgreementNo.Text.Trim
                                .NilaiTransaksi = CDec(lblNilaiTransaksi.Text)
                                .BusinessDate = Me.BusinessDate
                            End With
                            SaveUpdateStatus(Param)
                        Catch ex As Exception
                            ShowMessage(lblMessage, ex.Message, True)
                        End Try
                    End If
                    'End If
                End If
            End If
        Next

        If listCheked.Count = 0 Then
            ShowMessage(lblMessage, "Belum dipilih!", True)
            Exit Sub
        Else
            DoBind(Me.SortBy, Me.SearchBy)
            ShowMessage(lblMessage, "Delete Berhasil", False)
        End If
    End Sub
    Sub SaveUpdateStatus(ByRef Param As VirtualAccountOtor)
        Try
            objCon = New SqlConnection(GetConnectionString)
            myCmd.Parameters.Clear()
            myCmd.CommandText = "spPostingUploadAngsuranForUpdateStatus"
            myCmd.CommandType = CommandType.StoredProcedure
            myCmd.Parameters.Add(New SqlParameter("@BranchAgreement", SqlDbType.VarChar, 3) With {.Value = Param.BranchId})
            myCmd.Parameters.Add(New SqlParameter("@Applicationid", SqlDbType.VarChar, 20) With {.Value = Param.ApplicationID})
            myCmd.Parameters.Add(New SqlParameter("@valuedate", SqlDbType.Date) With {.Value = Param.ValueDate})
            myCmd.Parameters.Add(New SqlParameter("@businessdate", SqlDbType.Date) With {.Value = Param.BusinessDate})
            myCmd.Parameters.Add(New SqlParameter("@BankAccountID", SqlDbType.VarChar, 10) With {.Value = Param.BankAccountID})
            myCmd.Parameters.Add(New SqlParameter("@AmountReceive", SqlDbType.Decimal) With {.Value = Param.NilaiTransaksi})
            myCmd.Parameters.Add(New SqlParameter("@UploadDate", SqlDbType.Date) With {.Value = Param.UploadDate})
            myCmd.Parameters.Add(New SqlParameter("@SequenceNo", SqlDbType.SmallInt) With {.Value = Param.SequenceNo})
            myCmd.Parameters.Add(New SqlParameter("@AgreementNo", SqlDbType.VarChar, 20) With {.Value = Param.NoPelanggan})

            objCon.Open()
            myCmd.Connection = objCon
            myCmd.CommandTimeout = 0
            myCmd.ExecuteNonQuery()
            objCon.Close()
        Catch ex As Exception
            Throw New Exception(ex.Message)
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub
    Private Function createNewDT() As DataTable
        Dim dt As New DataTable

        dt.Columns.Add("branchagreement")
        dt.Columns.Add("applicationid")
        dt.Columns.Add("valuedate")
        dt.Columns.Add("businessdate")
        dt.Columns.Add("bankaccountid")
        dt.Columns.Add("amountreceive")
        dt.Columns.Add("uploaddate")
        dt.Columns.Add("sequenceno")
        dt.Columns.Add("AgreementNo")
        Return dt
    End Function

    Private Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click
        Dim lblSequenceNo As Label
        Dim lblUploadDate As Label
        Dim lblValueDate As Label
        Dim lblBranchID As Label
        Dim lblBankAccountID As Label
        Dim lnkApplicationID As HyperLink
        Dim lnkAgreementNo As HyperLink
        Dim lblNilaiTransaksi As Label
        Dim listCheked As New List(Of Integer)


        Try
            Dim dt As DataTable = createNewDT()

            Dim chkDtList As CheckBox
            For i = 0 To DtgAgree.Items.Count - 1
                chkDtList = CType(DtgAgree.Items(i).FindControl("chkItem"), CheckBox)
                If Not IsNothing(chkDtList) Then
                    If chkDtList.Enabled Then
                        'If DtgAgreeRK.Items.Count = 0 Then
                        '    ShowMessage(lblMessage, "Data Rekening Korannya tidak ada", True)
                        '    Exit Sub
                        'Else
                        If chkDtList.Checked Then
                            listCheked.Add(i)
                            lblSequenceNo = CType(DtgAgree.Items(i).FindControl("lblSequenceNo"), Label)
                            lblUploadDate = CType(DtgAgree.Items(i).FindControl("lblUploadDate"), Label)
                            lblValueDate = CType(DtgAgree.Items(i).FindControl("lblValueDate"), Label)
                            lblBranchID = CType(DtgAgree.Items(i).FindControl("lblBranchID"), Label)
                            lnkApplicationID = CType(DtgAgree.Items(i).FindControl("lnkApplicationID"), HyperLink)
                            lblBankAccountID = CType(DtgAgree.Items(i).FindControl("lblBankAccountID"), Label)
                            lnkAgreementNo = CType(DtgAgree.Items(i).FindControl("lnkAgreementNo"), HyperLink)
                            lblNilaiTransaksi = CType(DtgAgree.Items(i).FindControl("lblNilaiTransaksi"), Label)
                            Try
                                Dim Param As New VirtualAccountOtor
                                With Param
                                    .SequenceNo = CInt(lblSequenceNo.Text)
                                    .UploadDate = CDate(lblUploadDate.Text)
                                    .ValueDate = CDate(lblValueDate.Text)
                                    .BranchId = lblBranchID.Text.Trim
                                    .ApplicationID = lnkApplicationID.Text.Trim
                                    .BankAccountID = ddlRekeningBank.SelectedValue.Trim
                                    .NoPelanggan = lnkAgreementNo.Text.Trim
                                    .NilaiTransaksi = CDec(lblNilaiTransaksi.Text)
                                    .BusinessDate = Me.BusinessDate
                                End With

                                dt.Rows.Add(Param.BranchId, Param.ApplicationID, Param.ValueDate, Param.BusinessDate, Param.BankAccountID, Param.NilaiTransaksi, Param.UploadDate, Param.SequenceNo, Param.NoPelanggan)
                            Catch ex As Exception
                                ShowMessage(lblMessage, ex.Message, True)
                            End Try
                        End If
                        'End If

                    End If
                End If
            Next


            If listCheked.Count = 0 Then
                ShowMessage(lblMessage, "Belum dipilih!", True)
                Exit Sub
            Else
                Save(dt)
                DoBind(Me.SortBy, Me.SearchBy)
                ShowMessage(lblMessage, "Posting Berhasil", False)
            End If
        Catch ex As Exception

        End Try
    End Sub
    Sub Save(ByRef udtTable As DataTable)
        Try
            objCon = New SqlConnection(GetConnectionString)
            myCmd.Parameters.Clear()
            myCmd.CommandText = "spPostingUploadAngsuranWithUDT"
            myCmd.CommandType = CommandType.StoredProcedure
            myCmd.Parameters.Add(New SqlParameter("@udt", SqlDbType.Structured) With {.Value = udtTable})


            objCon.Open()
            myCmd.Connection = objCon
            myCmd.CommandTimeout = 0
            myCmd.ExecuteNonQuery()
            objCon.Close()
        Catch ex As Exception
            Throw New Exception(ex.Message)
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub
End Class