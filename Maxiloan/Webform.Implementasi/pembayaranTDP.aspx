﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="pembayaranTDP.aspx.vb" Inherits="Maxiloan.Webform.Implementasi.pembayaranTDP" %>
<%@ Register Src="../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %>   
<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../webform.UserController/UcSearchBy.ascx" %>

<%@ Register TagPrefix="uc1" TagName="UcBranchAll" Src="../webform.UserController/UcBranchAll.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   <title>Terima Dokumen Bank</title>
    <link href="../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
        var ServerName = 'http://<%= Request.ServerVariables("SERVER_NAME")%>/';
    </script>
    <script src="../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" Visible="false" runat="server" />
     <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>TERIMA PEBAYARAN TDP</h3>
        </div>
    </div>
    <asp:Panel ID="pnlDatagrid" runat="server" Visible="false">        
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgAsset" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="SortGrid" BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                             <asp:TemplateColumn SortExpression="BranchFullName" HeaderText="Cabang">
                                <ItemTemplate>
                                    <asp:label ID="lblCabang" runat="server" Text='<%#Container.DataItem("BranchFullName")%>'>
                                    </asp:label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="PILIH">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="Hyterima" runat="server" Text='TERIMA' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                             <asp:TemplateColumn SortExpression="Customerid" HeaderText="Customerid" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblCustomerId" runat="server" Text='<%#Container.DataItem("Customerid")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                           <asp:TemplateColumn SortExpression="Name" HeaderText="NAMA CUSTOMER">
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyCustomerName" runat="server" Text='<%#Container.DataItem("Name")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                             <asp:TemplateColumn SortExpression="InvoiceNo" HeaderText="NO Faktur">
                                <ItemTemplate> 
                                    <asp:Label ID="lblInvoiceNo" runat="server"  Text='<%#Container.DataItem("InvoiceNo")%>'> </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn> 
                             <asp:TemplateColumn SortExpression="InvoiceSeqNo" Visible="false" HeaderText="INVOICE SEQ NO" HeaderStyle-Width="50">
                                    <ItemTemplate>
                                        <asp:Label ID="lblInvoiceSeqNo" runat="server"  Text='<%#Container.DataItem("InvoiceSeqNo")%>'> </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                             <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn> 
                            <asp:BoundColumn HeaderText="DP" DataField="DownPaymentMf" SortExpression="DownPaymentMf"  DataFormatString="{0:N0}"  />
                            <asp:BoundColumn HeaderText="DP Karoseri" DataField="DPKaroseriAmountMf" SortExpression="DPKaroseriAmountMf"  DataFormatString="{0:N0}"  />
                            <asp:BoundColumn HeaderText="Admin" DataField="AdminFeeMf" SortExpression="AdminFeeMf"  DataFormatString="{0:N0}" />
                            <asp:BoundColumn HeaderText="Fiducia" DataField="FiduciaFeeMf" SortExpression="FiduciaFeeMf"  DataFormatString="{0:N0}" />
                            <asp:BoundColumn HeaderText="UppingAdmin" DataField="OtherFeeMf" SortExpression="OtherFeeMf"  DataFormatString="{0:N0}" />
                            <asp:BoundColumn HeaderText="Provisi" DataField="ProvisionFeeMf" SortExpression="ProvisionFeeMf"  DataFormatString="{0:N0}" />
                            <asp:BoundColumn HeaderText="Advance" DataField="InstallmentAmountFirstMF" SortExpression="InstallmentAmountFirstMF"  DataFormatString="{0:N0}" />
                            <asp:BoundColumn HeaderText="Asuransi" DataField="PaidAmountByCustMF" SortExpression="PaidAmountByCustMF"  DataFormatString="{0:N0}" />
                            <asp:BoundColumn HeaderText="Handling" DataField="HandlingFee" SortExpression="HandlingFee"  DataFormatString="{0:N0}" />
                            <asp:BoundColumn HeaderText="Terbayar" DataField="TotalTerbayar" SortExpression="TotalTerbayar"  DataFormatString="{0:N0}" />
                            <asp:TemplateColumn SortExpression="ApplicationId" HeaderText="NO APLIKASI" Visible="false" >
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyApplicationId" runat="server" Text='<%#Container.DataItem("ApplicationId")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>                        
                    </asp:DataGrid>
                    <uc2:ucGridNav id="GridNavigator" runat="server"/> 
                  
                </div>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_box_title">
            <div class="form_single">
                <h4> CARI KONTRAK </h4>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Cabang
                    </label>
                    <%--untuk cabang yg sedang dipilih jika dicabang dan buka semua cabang diHO--%>
                    <%--<asp:DropDownList ID="cboParent" runat="server" onchange="<%#BranchIDChange()%>">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="rfvcbobranchid" runat="server" InitialValue="0" ErrorMessage="Harap pilih Cabang"
                    ControlToValidate="cboParent" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                    <%--untuk buka semua cabang--%>
                    <uc1:ucbranchall id="oBranch" runat="server" onchange="<%#BranchIDChange()%>"></uc1:ucbranchall>
                    <%--untuk buka cabang yg sedang dipilih--%>
                    <%--<asp:DropDownList ID="oBranch" runat="server">
                    </asp:DropDownList>--%>
                </div>
            </div>
        </div>
        <div class="form_box_uc">
                <uc1:ucsearchby id="oSearBy" runat="server"></uc1:ucsearchby>            
        </div>
        <div class="form_button">
            <asp:Button ID="btnsearch" runat="server" Text="Find" CssClass="small button blue" />
            <asp:Button ID="btnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray" />
        </div>
    </asp:Panel>
    </form>
</body>