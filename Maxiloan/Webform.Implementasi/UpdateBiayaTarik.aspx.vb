﻿#Region "Import"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCacheHelper
#End Region

Public Class UpdateBiayaTarik
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oCGID As UcBranchCollection        
    Protected WithEvents txtBiayaTarik As ucNumberFormat

#Region "properties"
    Public Property CustomerID() As String
        Get
            Return CType(ViewState("CustomerID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property
    Public Property BankType() As String
        Get
            Return CType(ViewState("BankType"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("BankType") = Value
        End Set
    End Property

    Public Property BankPurpose() As String
        Get
            Return CType(ViewState("BankPurpose"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("BankPurpose") = Value
        End Set
    End Property
    Public Property cgid() As String
        Get
            Return CType(ViewState("cgid"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("cgid") = Value
        End Set
    End Property
    Public Property AgreementNo() As String
        Get
            Return CType(ViewState("AgreementNo"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("AgreementNo") = Value
        End Set
    End Property
    Public Property RALNo() As String
        Get
            Return CType(ViewState("RALNo"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("RALNo") = Value
        End Set
    End Property
    Public Property ApplicationID() As String
        Get
            Return CType(ViewState("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
#End Region

#Region " Private Const "
    Dim m_CollExpense As New CollExpenseController
    Private oController As New DataUserControlController
    Private mController As New ImplementasiControler
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region
#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If

    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        Bindgrid(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        If txtPage.Text = "" Then
            txtPage.Text = "0"
        Else
            If IsNumeric(txtPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                    currentPage = CType(txtPage.Text, Int32)
                    Bindgrid(Me.SearchBy, Me.SortBy)
                End If
            End If
        End If


    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        Bindgrid(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        Page.Header.DataBind()

        If SessionInvalid() Then
            Exit Sub
        End If

        If Not Page.IsPostBack Then
            Me.FormID = "UPBIAYATARIK"
            Me.cgid = Me.GroubDbID

            If CheckForm(Me.Loginid, Me.FormID, "Maxiloan") Then
                InitialDefaultPanel()
            Else
                Dim strHTTPServer As String
                Dim strHTTPApp As String
                Dim strNameServer As String
                strHTTPServer = Request.ServerVariables("PATH_INFO")
                strNameServer = Request.ServerVariables("SERVER_NAME")
                strHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                Response.Redirect("http://" & strNameServer & "/" & strHTTPApp & "/NotAuthorized.html")
            End If
            pnlDtGrid.Visible = False
            pnlsearch.Visible = True
            pnlUpdate.Visible = False
        End If
    End Sub


    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Buttonsearch.Click
        Me.SearchBy = ""
        Me.SortBy = ""
        If oCGID.BranchID.Trim <> "" Then
            Me.SearchBy = "CGID='" & oCGID.BranchID & "'"
            Me.cgid = oCGID.BranchID
        End If
        If cboSearchBy.SelectedIndex <> 0 And txtSearchBy.Text <> "" Then
            If Me.SearchBy = "" Then
                Me.SearchBy = cboSearchBy.SelectedItem.Value.Trim & " like '%" + txtSearchBy.Text.Trim + "%'"
            Else
                Me.SearchBy = Me.SearchBy & " and " & cboSearchBy.SelectedItem.Value.Trim & " like '%" + txtSearchBy.Text.Trim + "%'"
            End If
        End If

        Bindgrid(Me.SearchBy, Me.SortBy)
    End Sub
    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        lblMessage.Visible = False
        lblMessage.Text = ""
        InitialDefaultPanel()
        cboSearchBy.ClearSelection()
        txtSearchBy.Text = ""
    End Sub
    Private Sub InitialDefaultPanel()
        pnlDtGrid.Visible = False
        pnlsearch.Visible = True
        pnlUpdate.Visible = False
    End Sub
    Sub Bindgrid(ByVal cmdWhere As String, ByVal cmSort As String)
        lblMessage.Visible = False
        lblMessage.Text = ""
        Dim dtvEntity As New DataView
        Dim dtsEntity As New DataTable
        Dim oCollExpense As New Parameter.CollExpense

        With oCollExpense
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oCollExpense = m_CollExpense.CollExpenseList(oCollExpense)

        With oCollExpense
            lblrecord.Text = CType(.TotalRecord, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecord
        End With

        dtsEntity = oCollExpense.listData
        dtsEntity.TableName = "Table"

        dtvEntity = dtsEntity.DefaultView

        dtgCollExpenseList.DataSource = dtvEntity

        Try
            dtgCollExpenseList.DataBind()
        Catch ex As Exception

        End Try

        PagingFooter()
        pnlDtGrid.Visible = True
        pnlsearch.Visible = True
        pnlUpdate.Visible = False
    End Sub

    Private Sub dtgCollExpenseList_itemcommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgCollExpenseList.ItemCommand        
        If e.CommandName = "Update" Then
            With txtBiayaTarik
                .RequiredFieldValidatorEnable = True
                .RangeValidatorEnable = True
                .TextCssClass = "numberAlign medium_text"
            End With

            pnlDtGrid.Visible = False
            pnlsearch.Visible = False
            pnlUpdate.Visible = True

            Dim lblBranchID As Label
            Dim lblAgreementNo As Label
            Dim lblCustomerName As Label
            lblBranchID = CType(dtgCollExpenseList.Items(e.Item.ItemIndex).FindControl("lblBranchID"), Label)
            lblAgreementNo = CType(dtgCollExpenseList.Items(e.Item.ItemIndex).FindControl("lblAgreementNo"), Label)
            lblCustomerName = CType(dtgCollExpenseList.Items(e.Item.ItemIndex).FindControl("lblCustomerName"), Label)


            Dim imp As New Parameter.Implementasi
            With imp
                .strConnection = GetConnectionString()
                .WhereCond = " BranchID='" & lblBranchID.Text.Trim & "' and AgreementNo='" & lblAgreementNo.Text.Trim & "'"
                .SPName = "spImGetOutstandingPrincipal"
            End With

            imp = mController.GetSP(imp)
            If imp.Listdata.Rows.Count > 0 Then
                txtBiayaTarik.Text = FormatNumber(imp.Listdata.Rows(0).Item("CollectionExpense"), 0)                
                hdApplicationID.Value = imp.Listdata.Rows(0).Item("ApplicationID").ToString.Trim
                hdBranchID.Value = imp.Listdata.Rows(0).Item("BranchID").ToString.Trim
            End If

            lblNoKontrak.Text = lblAgreementNo.Text.Trim
            lblNama.Text = lblCustomerName.Text.Trim
        End If
    End Sub    

    Private Sub ButtonSave_Click(sender As Object, e As System.EventArgs) Handles ButtonSave.Click
        Dim imp As New Parameter.Implementasi

        Try
            With imp
                .strConnection = GetConnectionString()
                .ApplicationID = hdApplicationID.Value
                .BranchId = hdBranchID.Value
                .BiayaTarik = CDec(txtBiayaTarik.Text)
            End With

            mController.UpdateBiayaTarik(imp)
            ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)

            InitialDefaultPanel()
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

    Private Sub ButtonCancel_Click(sender As Object, e As System.EventArgs) Handles ButtonCancel.Click
        Response.Redirect("UpdateBiayaTarik.aspx")
    End Sub
End Class
