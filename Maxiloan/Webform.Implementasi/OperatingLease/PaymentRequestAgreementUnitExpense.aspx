﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PaymentRequestAgreementUnitExpense.aspx.vb" Inherits="Maxiloan.Webform.Implementasi.PaymentRequestAgreementUnitExpense" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Payment Request Agreement Unit Expense</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" type="text/css" />
	<script src="../../js/jquery-2.1.1.js" type="text/javascript"></script>
	<script src="../../js/jquery-ui-1.10.3.custom.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
        <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>

     <asp:Panel ID="pnlList" runat="server">
         <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    Permintaan Pembayaran Agreement Unit Expense
                </h3>
            </div>
        </div>
    
        <div class="form_box">
            <div class="form_single">
                <label>
                    Tanggal Invoice &lt;=</label>
                <asp:TextBox runat="server" ID="txtValidDate"></asp:TextBox>
                <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtValidDate"
                    Format="dd/MM/yyyy">
                </asp:CalendarExtender>
            </div>
        </div>

         <div class="form_button">
            <asp:Button ID="Btnsearch" runat="server" CausesValidation="True" Text="Find" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>

      <asp:Panel ID="pnlGrid" runat="server" Visible="true">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DAFTAR TRANSAKSI EXPENSE
                </h4>
            </div>
        </div>
         <div class="form_box">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgPC" runat="server" ShowFooter="True"  CssClass="grid_general"
                        AutoGenerateColumns="False" AllowSorting="True">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <FooterStyle CssClass="item_grid" />
                        <Columns> 
                              <asp:TemplateColumn HeaderText="PILIH">
                                  <ItemTemplate>  
                                    <asp:LinkButton ID="HypReceive" CausesValidation="False" runat="server" Enabled="True"
                                        Text="PROSES" CommandName="PROSES">PROSES</asp:LinkButton>
                                      </ItemTemplate>
                            </asp:TemplateColumn>
                           <asp:TemplateColumn SortExpression="InvoiceNo" HeaderText="No Invoice" >
                                <ItemTemplate>
                                    <asp:Label ID="lblInvoiceNo" runat="server" Text='<%#Container.DataItem("InvoiceNo")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AgreementUnitNo" HeaderText="No Kontrak">
                                <ItemTemplate>
                                    <asp:Label ID="lblAgreementUnitNo" runat="server" Text='<%#Container.DataItem("AgreementUnitNo")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                             <asp:TemplateColumn SortExpression="UnitID" HeaderText="Unit ID" >
                                <ItemTemplate>
                                    <asp:Label ID="lblUnitID" runat="server" Text='<%#Container.DataItem("unitID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                              <asp:BoundColumn DataField="InvoiceDate" SortExpression="InvoiceDate" HeaderText="TGL INVOICE"
                                DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
                            <asp:TemplateColumn SortExpression="Notes" HeaderText="Notes" >
                                <ItemTemplate>
                                    <asp:Label ID="lblNotes" runat="server" Text='<%#Container.DataItem("Notes")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="BankName" HeaderText="Nama Bank">
                                <ItemTemplate>
                                    <asp:Label ID="lblBankName" runat="server" Text='<%#Container.DataItem("BankName")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                             <asp:TemplateColumn SortExpression="AccountName" HeaderText="Nama Account">
                                <ItemTemplate>
                                    <asp:Label ID="lblAccountName" runat="server" Text='<%#Container.DataItem("AccountName")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AccountNo" HeaderText="No Account">
                                <ItemTemplate>
                                    <asp:Label ID="lblAccountNo" runat="server" Text='<%#Container.DataItem("AccountNo")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="ExpenseDescription" HeaderText="Keterangan" >
                                <ItemTemplate>
                                    <asp:Label ID="lblExpenseDescription" runat="server" Text='<%#Container.DataItem("ExpenseDescription")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Jumlah" HeaderText="Jumlah">
                                <ItemTemplate>
                                    <asp:Label ID="lblAmount" runat="server" Text='<%#FormatNumber(Container.DataItem("Jumlah"), 2)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                             
                        </Columns>
                        <PagerStyle Visible="False" HorizontalAlign="Left"  
                            Mode="NumericPages"></PagerStyle>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
        <div class="form_box_hide" runat="server">
            <div class="form_button">
            <asp:Button ID="BtnNext" runat="server" CausesValidation="False" Visible="True" Text="Next"
                CssClass="small button green"></asp:Button>
        </div>
        </div> 
    </asp:Panel>

    </form>
</body>
</html>
