﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region


Public Class PaymentRequestAgreementUnitExpense
    Inherits Maxiloan.Webform.WebBased

#Region "Property"
    Public Property isCheckAll() As Boolean
        Get
            Return CBool(ViewState("isCheckAll"))
        End Get
        Set(ByVal Value As Boolean)
            ViewState("isCheckAll") = Value
        End Set
    End Property
    Public Property InvoiceNo() As String
        Get
            Return CType(ViewState("InvoiceNo"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("InvoiceNo") = Value
        End Set
    End Property
#End Region

#Region "Private Constanta"
    Private oController As New DataUserControlController
    Private m_controller As New PaymentRequestController
    Private x_controller As New ImplementasiControler
    Dim oCustomClass As New Parameter.PaymentRequest
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.FormID = "REQOPLEXPENSE"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If Request.QueryString("message") <> "" Then

                    ShowMessage(lblMessage, Request.QueryString("message"), False)
                End If
                With txtValidDate
                    .Text = Me.BusinessDate.ToString("dd/MM/yyyy")
                End With

                pnlList.Visible = True
                pnlGrid.Visible = False
            End If
        End If
    End Sub

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim oController As New ImplementasiControler
        Dim DtUserList As DataTable
        Dim DvUserList As DataView
        Dim dtEntity As New DataTable
        Dim currentPage As Int32 = 1
        Dim pageSize As Int16 = 10

        Dim oCustomClass As New Parameter.AgreementUnitExpense
        Dim PCList As New Parameter.AgreementUnitExpense

        Try
            With oCustomClass
                .strConnection = GetConnectionString()
                .WhereCond = Me.SearchBy
                .CurrentPage = currentPage
                .PageSize = pageSize
                .SortBy = ""
                .BusinessDate = Me.BusinessDate
            End With

            oCustomClass = oController.PaymentRequestAgreementUnitExpenseList(oCustomClass)

            DtUserList = oCustomClass.ListData
            DvUserList = DtUserList.DefaultView
            DvUserList.Sort = Me.SortBy
            DtgPC.DataSource = DvUserList

            Try
                DtgPC.DataBind()
            Catch ex As Exception

            End Try

            If DtgPC.Items.Count <= 0 Then
                BtnNext.Visible = False
            Else
                BtnNext.Visible = True
            End If
            pnlList.Visible = True
            pnlGrid.Visible = True
            lblMessage.Text = ""
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

#Region "CheckAllCheckBox"
    Public Sub checkAll(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim loopitem As Int16
        Dim Chk As CheckBox

        For loopitem = 0 To CType(DtgPC.Items.Count - 1, Int16)
            Chk = New CheckBox
            Chk = CType(DtgPC.Items(loopitem).FindControl("cbCheck"), CheckBox)

            If Chk.Enabled Then
                If CType(sender, CheckBox).Checked = True Then
                    Chk.Checked = True
                    Me.isCheckAll = True
                Else
                    Chk.Checked = False
                    Me.isCheckAll = False
                End If
            Else
                Chk.Checked = False
            End If
        Next
    End Sub
#End Region

    Private Sub BtnNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnNext.Click
        If CheckFeature(Me.Loginid, Me.FormID, "NEXT", Me.AppId) Then
            lblMessage.Text = ""
        End If
    End Sub

#Region "Process Reset And Search"
    Private Sub BtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Server.Transfer("PaymentRequestAgreementUnitExpense.aspx")
    End Sub
    Private Sub Btnsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btnsearch.Click
        If CheckFeature(Me.Loginid, Me.FormID, "SRCH", Me.AppId) Then
            lblMessage.Text = ""
            Me.SearchBy = ""
            Me.SearchBy = Me.SearchBy & " Status in ('L') and "
            If txtValidDate.Text <> "" Then
                Me.SearchBy = Me.SearchBy & " cast(ReqDate as date) <= '" & ConvertDate2(txtValidDate.Text) & "'  "
            End If
            Me.SortBy = ""
            pnlList.Visible = True
            pnlGrid.Visible = True
            DoBind(Me.SearchBy, Me.SortBy)
            With txtValidDate
                .Text = Me.BusinessDate.ToString("dd/MM/yyyy")
            End With
        End If
    End Sub
#End Region

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles DtgPC.SortCommand
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub DtgPC_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgPC.ItemCommand

        If e.CommandName = "PROSES" Then
            Dim lblInvoiceNo As Label
            lblInvoiceNo = CType(DtgPC.Items(e.Item.ItemIndex).FindControl("lblInvoiceNo"), Label)
            Response.Redirect("../../Webform.LoanMnt/PaymentRequest/PaymentRequestMulti.aspx?lblInvoiceNo=" & lblInvoiceNo.Text.Trim & "&Flag=EP")
        End If

    End Sub


End Class