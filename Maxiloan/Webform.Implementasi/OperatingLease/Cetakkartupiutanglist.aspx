﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Cetakkartupiutanglist.aspx.vb" Inherits="Maxiloan.Webform.Implementasi.Cetakkartupiutanglist" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Cetak Kartu Piutang List</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script src="../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        var AppInfo = window.location.pathname;
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
        var ServerName = window.location.protocol + "//" + window.location.host + '/';
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server" ID="ScriptManager1">
        </asp:ScriptManager>
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
        <asp:Panel ID="pnlsearch" runat="server">
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>Cetak Kartu Piutang List</h3>
                </div>
            </div>
            <div class="form_box">
            <div class="form_single">
                    <label>
                        Cari Berdasarkan</label>
                    <asp:DropDownList ID="cboSearchBy" runat="server">
                        <asp:ListItem Value="">Select One</asp:ListItem>
                        <asp:ListItem Value="AgreementNo">No Agreement</asp:ListItem>
                        <asp:ListItem Value="CustomerName">Nama Customer</asp:ListItem>
                    </asp:DropDownList>
                    <asp:TextBox ID="txtSearchBy" runat="server"></asp:TextBox>
             </div>
             </div>
            <div class="form_button">
                <asp:Button ID="Btnsearch" runat="server" Text="Find" CssClass="small button blue"></asp:Button>
                <asp:Button ID="BtnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray"></asp:Button>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlDtGrid" runat="server">
        <div class="form_title">
                <div class="form_single">
                    <h4>DAFTAR KONTRAK</h4>
                </div>
        </div>
        <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ns">
                        <asp:DataGrid ID="CetakKartuPiutang" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                             OnSortCommand="Sorting" DataKeyField="AgreementUnitno" BorderStyle="None" BorderWidth="0"
                            CssClass="grid_general">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="PILIH">
                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkPrint" runat="server"></asp:CheckBox>
                                    </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AgreementUnitno" HeaderText="No Agreement">
                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:label ID="AgreementUnitNo" runat="server" Text='<%#Container.DataItem("AgreementUnitno")%>'>
                                        </asp:label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="CustomerName" HeaderText="Nama Customer">
                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:label ID="lblCustomerName" runat="server" Text='<%#Container.DataItem("CustomerName")%>'>
                                        </asp:label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="CustomerUsed" HeaderText="Customer Used">
                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:label ID="lblCustomerUsed" runat="server" Text='<%#Container.DataItem("CustomerUsed")%>'>
                                        </asp:label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Tenor" HeaderText="Tenor">
                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:label ID="lblTenor" runat="server" Text='<%#Container.DataItem("Tenor")%>'>
                                        </asp:label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AssetName" HeaderText="Nama Asset">
                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:label ID="lblAssetName" runat="server" Text='<%#Container.DataItem("AssetName")%>'>
                                        </asp:label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="NoRangka" HeaderText="Nomor Rangka">
                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:label ID="lblNoRangka" runat="server" Text='<%#Container.DataItem("NoRangka")%>'>
                                        </asp:label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="NoMesin" HeaderText="Nomor Mesin">
                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:label ID="lblNoMesin" runat="server" Text='<%#Container.DataItem("NoMesin")%>'>
                                        </asp:label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="NoPolisi" HeaderText="Nomor Polisi">
                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:label ID="lblNoPolisi" runat="server" Text='<%#Container.DataItem("NoPolisi")%>'>
                                        </asp:label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                        </Columns>
                        <PagerStyle Visible="false" HorizontalAlign="Left" ForeColor="#000066" BackColor="White"
                                Mode="NumericPages"></PagerStyle>
                        </asp:DataGrid>
                        <div class="button_gridnavigation">
                            <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                                CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False"></asp:ImageButton>
                            <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                                CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False"></asp:ImageButton>
                            <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                                CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False"></asp:ImageButton>
                            <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                                CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False"></asp:ImageButton>
                            <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                            <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                                EnableViewState="False"></asp:Button>
                            <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" MinimumValue="1"
                                ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"></asp:RangeValidator>
                            <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                                ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        </div>
                        <div class="label_gridnavigation">
                            <asp:Label ID="lblPage" runat="server"></asp:Label>of
                                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                        </div>
                    </div>
                </div>
            </div>
        <div class="form_button">
                <asp:Button ID="BtnPrint" runat="server" Text="Print" CssClass="small button blue"></asp:Button>
            </div>
        </asp:Panel>
    
    </form>
</body>
</html>
