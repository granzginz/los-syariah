﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region




Public Class Cetakkartupiutanglist
    Inherits Maxiloan.Webform.WebBased

#Region "constanta"
    Private oCustomClass As New Parameter.CetakKartuPiutang
    Private oController As New Controller.ImplementasiControler
    Private Baris As Integer
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region

#Region "Property"
    Private Property PageState() As Int32
        Get
            Return CType(ViewState("PageState"), String)
        End Get
        Set(ByVal Value As Int32)
            ViewState("PageState") = Value
        End Set
    End Property
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property
    Private Property TempDataTable() As DataTable
        Get
            Return CType(ViewState("TempDataTable"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            ViewState("TempDataTable") = Value
        End Set
    End Property
    Private Property AgreementUnitNo() As String
        Get
            Return CType(ViewState("AgreementUnitNo"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("AgreementUnitNo") = Value
        End Set
    End Property
    Public Function PredicateFunction(ByVal custom As Parameter.CetakKartuPiutang) As Boolean
        Return custom.AgreementUnitNo = Me.AgreementUnitNo
    End Function

#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            'txtPeriod1.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
            'TxtPeriod2.Text = Me.BusinessDate.ToString("dd/MM/yyyy")

            If Request.QueryString("strFileLocation") <> "" Then
                Dim strFileLocationList = Split(Request.QueryString("strFileLocation"), ",").ToList

                For Each strFileLocation As String In strFileLocationList
                    strFileLocation = "../../XML/" & strFileLocation & ".pdf"

                    Response.Write("<script language = javascript>" & vbCrLf _
                    & "var x = screen.width; var y = screen.height;  " & vbCrLf _
                    & " window.open('" & strFileLocation & "','" & strFileLocation & ".pdf', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes'); " & vbCrLf _
                    & "</script>")
                Next
            End If
            Me.FormID = "CetakKartuPiutang"
            Me.CmdWhere = " au.BranchID = '" & Me.sesBranchId.Replace("'", "").Trim & "' And"
            'Me.CmdWhere += " GoLiveDate >= '" & ConvertDate2(txtPeriod1.Text.Trim) & "'" & " And GoLiveDate<= '" & ConvertDate2(TxtPeriod2.Text.Trim) & "'"
            Me.Sort = "AgreementNo ASC"
        End If
    End Sub
#End Region

#Region "BindGridEntity"
    Sub BindGridEntity()
        Dim dtEntity As New DataTable
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        Dim intloop As Integer = 0

        Dim DataList As New List(Of Parameter.CetakKartuPiutang)
        Dim custom As New Parameter.CetakKartuPiutang
        If Not TempDataTable Is Nothing Then
            For index = 0 To TempDataTable.Rows.Count - 1
                custom = New Parameter.CetakKartuPiutang


                custom.AgreementUnitNo = TempDataTable.Rows(index).Item("AgreementUnitno").ToString.Trim


                DataList.Add(custom)
            Next
        End If

        With oCustomClass
            .strConnection = GetConnectionString()
            .CurrentPage = currentPage
            .PageSize = pageSize
            .WhereCond = Me.CmdWhere
            .SortBy = Me.Sort
        End With

        'oCustomClass = oController.CetakKartuPiutangPaging(oCustomClass)

        If Not oCustomClass Is Nothing Then
            dtEntity = oCustomClass.listdata
            recordCount = oCustomClass.TotalRecord
        Else
            recordCount = 0
        End If

        CetakKartuPiutang.DataSource = dtEntity.DefaultView
        CetakKartuPiutang.CurrentPageIndex = 0
        CetakKartuPiutang.DataBind()
        PagingFooter()

        pnlDtGrid.Visible = True
        pnlsearch.Visible = True

        Dim oRow As DataRow
        For intLoopGrid = 0 To CetakKartuPiutang.Items.Count - 1
            Dim chek As CheckBox
            chek = CType(CetakKartuPiutang.Items(intLoopGrid).FindControl("chkPrint"), CheckBox)
            Dim AgreementUnitNo As String = CType(CetakKartuPiutang.Items(intLoopGrid).FindControl("AgreementUnitno"), Label).Text.Trim


            Me.AgreementUnitNo = AgreementUnitNo


            Dim query As New Parameter.CetakKartuPiutang
            If DataList.Count > 0 Then
                query = DataList.Find(AddressOf PredicateFunction)
            Else
                query = Nothing
            End If


            If chek.Checked And query Is Nothing Then
                oRow = TempDataTable.NewRow()
                oRow("AgreementUnitno") = AgreementUnitNo
                TempDataTable.Rows.Add(oRow)
            End If
        Next

    End Sub
#End Region

    Sub BindDataPPK()
        Dim DataList As New List(Of Parameter.CetakKartuPiutang)
        Dim application As New Parameter.CetakKartuPiutang

        If TempDataTable Is Nothing Then
            TempDataTable = New DataTable
            With TempDataTable
                .Columns.Add(New DataColumn("AgreementUnitno", GetType(String)))
            End With
        Else
            For index = 0 To TempDataTable.Rows.Count - 1
                application = New Parameter.CetakKartuPiutang


                application.AgreementUnitNo = TempDataTable.Rows(index).Item("AgreementUnitno").ToString.Trim
                DataList.Add(application)
            Next
        End If

        Dim oRow As DataRow

        For intLoopGrid = 0 To CetakKartuPiutang.Items.Count - 1
            Dim chek As CheckBox
            chek = CType(CetakKartuPiutang.Items(intLoopGrid).FindControl("chkPrint"), CheckBox)
            Dim AgreementUnitNo As String = CType(CetakKartuPiutang.Items(intLoopGrid).FindControl("AgreementUnitno"), Label).Text.Trim


            Me.AgreementUnitNo = AgreementUnitNo


            Dim query As New Parameter.CetakKartuPiutang
            If DataList.Count > 0 Then
                query = DataList.Find(AddressOf PredicateFunction)
            Else
                query = Nothing
            End If


            If chek.Checked And query Is Nothing Then
                oRow = TempDataTable.NewRow()
                oRow("AgreementUnitno") = AgreementUnitNo
                TempDataTable.Rows.Add(oRow)
            End If
        Next
    End Sub
    Sub BindClearPPK()
        TempDataTable = New DataTable
        With TempDataTable
            .Columns.Add(New DataColumn("CustomerID", GetType(String)))
            .Columns.Add(New DataColumn("ApplicationID", GetType(String)))
            .Columns.Add(New DataColumn("AgreementUnitno", GetType(String)))
        End With
    End Sub
#Region "Search"
    Private Sub Btnsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btnsearch.Click
        Me.CmdWhere = " au.BranchID = '" & Me.sesBranchId.Replace("'", "") & "'"

        Dim SearchTemp As String

        If cboSearchBy.SelectedItem.Value <> "" And txtSearchBy.Text <> "" Then
            SearchTemp = cboSearchBy.SelectedItem.Value.Trim & " Like '%" & txtSearchBy.Text.Trim & "%'"
            Me.CmdWhere = Me.CmdWhere & " And " & SearchTemp
        End If

        Me.Sort = "AgreementNo Asc"
        BindGridEntity()
    End Sub
#End Region

#Region "Reset"
    Private Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        'txtPeriod1.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
        'TxtPeriod2.Text = Me.BusinessDate.ToString("dd/MM/yyyy")

        Me.CmdWhere = " au.BranchID = '" & Me.sesBranchId.Replace("'", "").Trim & "'"

        Me.Sort = "AgreementNo ASC"
        BindClearPPK()
        BindGridEntity()
        txtSearchBy.Text = ""
        txtPage.Text = 1
        'cboPrinted.ClearSelection()
        cboSearchBy.ClearSelection()
    End Sub
#End Region


#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""

        End If
        lblMessage.Text = ""
        Me.PageState = currentPage
        txtPage.Text = CType(Me.PageState, String)
        BindDataPPK()
        BindGridEntity()
    End Sub
    Private Sub imgbtnPageNumb_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int32)
                BindDataPPK()
                BindGridEntity()
            End If
        End If
    End Sub
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles CetakKartuPiutang.SortCommand
        If InStr(Me.Sort, "Desc") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " Desc"
        End If
        BindDataPPK()
        BindGridEntity()
    End Sub
#End Region

#Region "Print"
    Private Sub BtnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnPrint.Click
        'If CheckFeature(Me.Loginid, Me.FormID, "Print", "MAXILOAN") Then
        Dim oDataTable As New DataTable
        Dim oRow As DataRow
        Dim intloop As Integer = 0
        Dim hasil As Integer = 0
        Dim cmdwhere As String = ""

        If TempDataTable Is Nothing Then
            BindDataPPK()
        Else
            If TempDataTable.Rows.Count = 0 Then
                BindDataPPK()
            End If
        End If

        With oDataTable
            .Columns.Add(New DataColumn("AgreementUnitno", GetType(String)))
        End With


        If TempDataTable.Rows.Count = 0 Then
            ShowMessage(lblMessage, "Harap periksa item", True)
            Exit Sub
        End If

        'Dim MultiApplicationID = ""
        Dim MultiAgreementNo = ""

        For index = 0 To TempDataTable.Rows.Count - 1
            If MultiAgreementNo = "" Then
                MultiAgreementNo = TempDataTable.Rows(index).Item("AgreementUnitno").ToString.Trim
            Else
                MultiAgreementNo = MultiAgreementNo & "," & TempDataTable.Rows(index).Item("AgreementUnitno").ToString.Trim
            End If
        Next

        'ApplicationId = Me.ApplicationID
        Dim cookie As HttpCookie = Request.Cookies("RptCetakKartuPiutang")
        If Not cookie Is Nothing Then
            cookie.Values("MultiAgreementNo") = MultiAgreementNo
            'cookie.Values("ApplicationID") = ApplicationId
            'cookie.Values("BusinessDate") = Me.BusinessDate.ToString("dd/MM/yyyy")
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie("RptCetakKartuPiutang")
            'cookie.Values.Add("MultiAgreementNo", MultiAgreementNo)
            cookieNew.Values.Add("MultiAgreementNo", MultiAgreementNo)
            'cookieNew.Values.Add("ApplicationID", ApplicationId)
            'cookieNew.Values.Add("BusinessDate", Me.BusinessDate.ToString("dd/MM/yyyy"))
            Response.AppendCookie(cookieNew)
        End If
        Response.Redirect("StatementCetakKartuPiutang.aspx")
        ''End If
    End Sub
#End Region


End Class