﻿Imports Maxiloan.Controller
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.IO
Imports Maxiloan.Parameter
Imports Maxiloan.General.CommonCacheHelper
Imports System.Data.SqlClient
Imports Maxiloan.Webform.UserController

Public Class StatementCetakKartuPiutang
    Inherits Maxiloan.Webform.WebBased

    Public Property AgreementUnitNo() As String
        Get
            Return CStr(ViewState("AgreementUnitNo"))
        End Get
        Set(ByVal Value As String)
            ViewState("AgreementUnitNo") = Value
        End Set
    End Property


    Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies("RptCetakKartuPiutang")
        Me.AgreementUnitNo = cookie.Values("MultiAgreementNo")
    End Sub

    Private Sub bindReport()
        Dim oController As New SimulasiPerhitunganPembiayaanController
        Dim oParameter As New Parameter.CetakKartuPiutang

        GetCookies()

        Dim AgreementList = Split(Me.AgreementUnitNo, ",").ToList

        Me.AgreementUnitNo = ""

        For Each agreeno As String In AgreementList

            Dim oDataSet As New DataSet
            Dim oDataTable As New DataTable
            Dim oReport As StatementCetakKartuPiutangprint = New StatementCetakKartuPiutangprint


            With oParameter
                .strConnection = GetConnectionString()
                .AgreementUnitNo = agreeno
                .BranchId = Me.sesBranchId.Replace("'", "")
            End With

            oParameter = oController.CetakKartuPiutangList(oParameter)
            oDataSet = oParameter.ListReport

            oReport.SetDataSource(oDataSet.Tables(0))

            CrystalReportViewer1.ReportSource = oReport
            CrystalReportViewer1.Visible = True
            CrystalReportViewer1.DataBind()

            With oReport.ExportOptions
                .ExportDestinationType = ExportDestinationType.DiskFile
                .ExportFormatType = ExportFormatType.PortableDocFormat
            End With

            Dim strFileLocation As String

            strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
            strFileLocation += Me.Session.SessionID + Me.Loginid + "RptCetakKartuPiutang" & agreeno & ".pdf"

            Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions

            DiskOpts.DiskFileName = strFileLocation

            With oReport
                .ExportOptions.DestinationOptions = DiskOpts
                .Export()
                .Close()
                .Dispose()
            End With

            If Me.AgreementUnitNo = "" Then
                Me.AgreementUnitNo = Me.Session.SessionID + Me.Loginid + "RptCetakKartuPiutang" & agreeno
            Else
                Me.AgreementUnitNo = Me.AgreementUnitNo & "," & (Me.Session.SessionID + Me.Loginid + "RptCetakKartuPiutang" & agreeno)
            End If
        Next

        Response.Redirect("CetakKartuPiutangList.aspx?strFileLocation=" & Me.AgreementUnitNo)

    End Sub

    'Sub BindReportAmortisasi()
    '    Dim ds As New DataSet
    '    Dim objreport As ReportDocument

    '    objreport = New StatementCetakKartuPiutangprint

    '    ds = GetData()
    '    objreport.SetDataSource(ds)

    '    CrystalReportViewer1.ReportSource = objreport
    '    CrystalReportViewer1.Visible = True
    '    CrystalReportViewer1.DataBind()

    '    Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions

    '    objreport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
    '    objreport.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat

    '    Dim strFileLocation As String

    '    strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
    '    strFileLocation += Me.Session.SessionID & Me.Loginid & "StatementCetakKartuPiutang.pdf"
    '    DiskOpts.DiskFileName = strFileLocation
    '    objreport.ExportOptions.DestinationOptions = DiskOpts
    '    objreport.Export()
    '    objreport.Close()
    '    objreport.Dispose()
    '    Response.Redirect("CetakKartuPiutangList.aspx")
    'End Sub

    'Public Function GetData() As DataSet
    '    Dim ds As New DataSet
    '    Dim adapter As New SqlDataAdapter
    '    Dim objCommand As New SqlCommand
    '    Dim objcon As New SqlConnection(GetConnectionString)

    '    Try
    '        objCommand.CommandType = CommandType.StoredProcedure
    '        objCommand.Connection = objcon
    '        objCommand.CommandText = "spGetDataCetakKartuPiutang"  'spGetKartuPiutang
    '        objCommand.Parameters.Add("@agreementunitno", SqlDbType.Char, 20).Value = Me.AgreementUnitNo
    '        objCommand.CommandTimeout = 60

    '        adapter.SelectCommand = objCommand
    '        adapter.Fill(ds)
    '    Catch ex As Exception
    '        Throw New Exception("Error On DataAccess.GetData")
    '    End Try

    '    Return ds
    'End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then Exit Sub
        'GetCookies()
        'If Me.Status = "C" Then
        '    BindReportCustomer()
        'Else
        '    BindReport()
        'End If
        'BindReportAmortisasi()
        bindReport()
    End Sub

End Class