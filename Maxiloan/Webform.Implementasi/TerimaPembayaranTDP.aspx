﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="TerimaPembayaranTDP.aspx.vb" Inherits="Maxiloan.Webform.Implementasi.TerimaPembayaranTDP" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat"
    TagPrefix="uc4" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   <title>Terima Dokumen Bank</title>
    <link href="../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../Include/Buttons.css" rel="stylesheet" type="text/css" />
  

    <script src="../Maxiloan.js" type="text/javascript"></script>
    <link href="../include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../js/jquery-1.9.1.min.js"></script>
    <script src="../js/jquery-ui-1.10.3.custom.min.js"></script>
   
</head>
<body>
    
   
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
        <div runat="server" id="jlookupContent" />
    <asp:UpdatePanel runat="server" ID="UpdatePanel1">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
        onclick="hideMessage();"></asp:Label>
       
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>TERIMA PEBAYARAN TDP</h3>
        </div>
    </div>
      <div class="form_box">
        <div class="form_left">
            <label>ApplicationID</label>
            <asp:Label ID="lblApplicationID" runat="server" />
        </div>
        <div class="form_right">
            <label>No. Kontrak</label>
            <asp:Label ID="lblagreementno" runat="server" />
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>Nama</label>
            <asp:Label ID="lblCustomerName" runat="server" />
        </div>
        <div class="form_right">
            <label>Tanggal Golive</label>
            <asp:Label ID="lblgolive" runat="server" />
        </div>
    </div>
   <div class="form_box">
       <div class="form_left">
           <label>DP </label>
           <asp:Label ID="lblDP" runat="server" />
       </div>
       <div class="form_right">
           <label>Admin</label>
           <asp:Label ID="lbladmin" runat="server" />
       </div>

   </div>
   <div class="form_box">
       <div class="form_left">
          <label>DP Karoseri</label>
           <asp:Label ID="lblDPKaroseri" runat="server" />
       </div>
       <div class="form_right">
           <label>Provisi</label>
           <asp:Label ID="lblprovisi" runat="server" />
       </div>
   </div>
    <div class="form_box">
       <div class="form_left">
           <label>Fiducia</label>
           <asp:Label ID="lblFiducia" runat="server" />
       </div>
       <div class="form_right">
           <label>Asuransi</label>
           <asp:Label ID="lblAsuransi" runat="server" />
       </div>
   </div>
    <div class="form_box">
       <div class="form_left">
            <label>Other Fee / Upping Admin</label>
           <asp:Label ID="lblotherfee" runat="server" />
       </div>
         <div class="form_right">
            <label>Handling Fee</label>
            <asp:Label ID="lblHandlingFee" runat="server" />
        </div>
   </div>
    <div class="form_box">
        <div class="form_left">
            <label>Advance</label>
           <asp:Label ID="lblAdvanceAngsuran" runat="server" />
        </div>
        <div class="form_right">
            <label>Total TDP</label>
            <asp:Label ID="lbltotalmf" runat="server" />
        </div>
    </div>
    <div class="form_box">
        <div class="form_left"></div>
        <div class="form_right">
            <label>Total Terbayar</label>
            <asp:Label ID="lblTerbayar" runat="server" />
        </div>
    </div>
   <div class="form_title">
        <div class="form_single" >
            <h4>Detail Pembayaran</h4>
        </div>
    </div>
     <div class="form_box">
         <div class="form_left">
             <label>Rekening Koran</label>
             <asp:DropDownList ID="cboRekKoran" runat="server" AutoPostBack="true" >
                 <asp:ListItem Value="">PILIH SALAH SATU</asp:ListItem>
                 <asp:ListItem Value="SUSPEND">SUSPEND / PREPAID</asp:ListItem>
                 <asp:ListItem Value="BANK">BANK ESCROW</asp:ListItem>
                 <asp:ListItem Value="TDP">Titipan TDP Receive</asp:ListItem>
                 <asp:ListItem Value="KOREKSI">Titipan KOREKSI</asp:ListItem>
             </asp:DropDownList>
         </div>
         
     </div>
     <asp:Panel ID="pnlcontainer" runat="server" visible="false">
     <asp:panel ID="pnlBank" runat="server" Visible="false">
     <div class="form_box">
          <div class="form_single">
             <label  class="label_req">No. Jurnal Bank</label>
             <asp:TextBox ID="txtjurnalbankno" runat="server" />
         </div>
     </div>
        
     <div class="form_box">
         <div class="form_single">
             <label>Rekening Bank</label>
             <asp:DropDownList ID="cmbBankAccount" runat="server">
             </asp:DropDownList>
         </div>
     </div>
     </asp:panel>
     <asp:Panel ID="pnlsuspend" runat="server" Visible ="false">
         <div class="form_box">
              <div class="form_single">
                 <label>No Suspend</label>
                 <asp:TextBox ID="txtsuspendno" runat="server" Enabled="false" />
                  <asp:TextBox ID="txtAmountSuspend" runat="server" Enabled="false" CssClass="numberAlign" />
                  <button class="small buttongo blue" style="display:inline-block"
                             onclick ="OpenJLookup('<%= ResolveClientUrl("~/webform.Utility/jLookup/suspend.aspx?SuspendClientID=" & txtsuspendno.ClientID & "&AmountSuspendClientID=" & txtAmountSuspend.ClientID)%>','Daftar Suspand','<%= jlookupContent.ClientID %>');return false;">...</button>  
             </div>
         </div>
     </asp:Panel>
     <asp:Panel ID="pnlTDP" runat="server" Visible ="false">
         <div class="form_box">
              <div class="form_single">
                 <label>No TDP Receive</label>
                 <asp:TextBox ID="txtTDPReceiveno" runat="server" Enabled="false" />
                  <asp:TextBox ID="txtAmountTDP" runat="server" Enabled="false" CssClass="numberAlign" />
                  <button class="small buttongo blue" style="display:inline-block"
                             onclick ="OpenJLookup('<%= ResolveClientUrl("~/webform.Utility/jLookup/TDP.aspx?TDPClientID=" & txtTDPReceiveno.ClientID & "&AmountTDPClientID=" & txtAmountTDP.ClientID)%>','Daftar TDP','<%= jlookupContent.ClientID %>');return false;">...</button>  
             </div>
         </div>
     </asp:Panel>
     <asp:Panel ID="pnlKOREKSI" runat="server" Visible ="false">
         <div class="form_box">
              <div class="form_single">
                 <label>No Voucher</label>
                 <asp:TextBox ID="txtKOREKSIReceiveno" runat="server" Enabled="false" />
                  <asp:TextBox ID="txtAmountKOREKSI" runat="server" Enabled="false" CssClass="numberAlign" />
                  <button class="small buttongo blue" style="display:inline-block"
                             onclick ="OpenJLookup('<%= ResolveClientUrl("~/webform.Utility/jLookup/KOREKSI.aspx?KOREKSIClientID=" & txtKOREKSIReceiveno.ClientID & "&AmountKOREKSIClientID=" & txtAmountKOREKSI.ClientID)%>','Daftar KOREKSI','<%= jlookupContent.ClientID %>');return false;">...</button>  
             </div>
         </div>
     </asp:Panel>
     <div class="form_box">
         <div class="form_single">
             <label>Tanggal Value</label>
             <asp:TextBox runat="server" ID="txttglvaluta" AutoPostBack="true"></asp:TextBox>
                <asp:CalendarExtender runat="server" ID="ceTanggalAktivasi" TargetControlID="txttglvaluta" Format="dd/MM/yyyy">
                </asp:CalendarExtender>
         </div>
     </div>
     <div class="form_box">
         <div class="form_single">
             <label>Jumlah</label>
             <uc4:ucnumberformat id="txtJumlah" runat="server" />
         </div>
     </div>
     <div class="form_button">
         <asp:Button ID="btnsave" runat="server" Text="Save" CssClass="small button blue" CausesValidation="true" />
         <asp:Button ID="btnback" runat="server" Text="Back" CssClass="small button gray" CausesValidation="false"   />
     </div>
     </asp:Panel>
             </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
