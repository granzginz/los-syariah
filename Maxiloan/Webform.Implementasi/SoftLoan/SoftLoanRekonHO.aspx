﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SoftLoanRekonHO.aspx.vb" Inherits="Maxiloan.Webform.Implementasi.SoftLoanRekonHO" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <style type="text/css">
        .progress_imgLoading
        {
            width: 4%;
            display: block;
            padding-top: 10px;
            padding-bottom: 20px;
            padding-left: 70px;
            background-image: url('../../Images/pic-loaderblack.gif');
            background-repeat: no-repeat;
            background-position: right;
            display: block;
        }
    </style>
</head>
<body>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
                onclick="hideMessage();"></asp:Label>
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>
                        REKONSILIASI POTONG GAJI
                    </h3>
                </div>
            </div>
            <asp:Panel ID="pnlHeader" runat="server">
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Rekening Bank</label>
                        <asp:DropDownList ID="cbSoftLoan" runat="server">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" Display="Dynamic"
                            InitialValue="" ControlToValidate="cbSoftLoan" ErrorMessage="Harap Pilih Rekening"
                            CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="btnReset" runat="server" Text="Reset" CausesValidation="false" CssClass="small button blue">
                    </asp:Button>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlDatagrid" runat="server">
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            DAFTAR
                        </h4>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ws">
                            <asp:DataGrid ID="DtgAgree" runat="server" AutoGenerateColumns="False" BorderWidth="0"
                                OnSortCommand="SortGrid" BorderStyle="None" CssClass="grid_general" DataKeyField="BranchID"
                                AllowSorting="True">
                                <ItemStyle CssClass="item_grid"></ItemStyle>
                                <HeaderStyle CssClass="th"></HeaderStyle>
                                <Columns>
                                    <asp:TemplateColumn>
                                        <HeaderStyle></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <HeaderTemplate>
                                            <asp:CheckBox AutoPostBack="True" ID="chkAll" runat="server" OnCheckedChanged="SelectAll">
                                            </asp:CheckBox>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkItem" runat="server"></asp:CheckBox>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="DETAIL KONTRAK">
                                        <ItemStyle HorizontalAlign="center"></ItemStyle>
                                        <ItemTemplate>
                                            <a href='DetailKontrakSoftLoan.aspx?BankAccountID=<%#Container.DataItem("BankAccountID")%>&BranchId=<%#Container.DataItem("BranchID")%>'>
                                                <asp:Image ID="imgRate" ImageUrl="../../images/icondetail.gif" runat="server"></asp:Image>
                                            </a>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="TglTransaksi" DataFormatString="{0:dd/MM/yyyy}" HeaderText="TGL TRANSAKSI"
                                        SortExpression="TglTransaksi"></asp:BoundColumn>
                                    <asp:TemplateColumn SortExpression="BankAccountName" HeaderText="NAMA BANK">
                                        <ItemTemplate>
                                            <asp:Label ID="lblBankAccountName" runat="server" Text='<%#Container.DataItem("BankAccountName")%>'></asp:Label>
                                            <asp:Label ID="lblBankAccountID" Visible="false" runat="server" Text='<%#Container.DataItem("BankAccountID")%>'></asp:Label>
                                            <asp:Label ID="lblTglTransaksi" Visible="false" runat="server" Text='<%#Container.DataItem("TglTransaksi")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="BranchFullName" HeaderText="NAMA CABANG">
                                        <ItemTemplate>
                                            <asp:Label ID="lblBranchFullName" runat="server" Text='<%#Container.DataItem("BranchFullName")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="AgreementCount" HeaderText="JUMLAH KONTRAK">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAgreementCount" runat="server" Text='<%#Container.DataItem("AgreementCount")%>'></asp:Label>
                                            <asp:Label ID="lblBranchID" Visible="false" runat="server" Text='<%#Container.DataItem("BranchID")%>'></asp:Label>                                            
                                        </ItemTemplate>
                                    </asp:TemplateColumn>                                                                                                            
                                    <asp:TemplateColumn SortExpression="AgreementTotal" HeaderText="TOTAL">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAgreementTotal" runat="server" Text='<%#formatnumber(Container.DataItem("AgreementTotal"),0)%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                            <div class="button_gridnavigation">
                                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                                    CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="First">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                                    CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Prev">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                                    CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Next">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                                    CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Last">
                                </asp:ImageButton>
                                Page&nbsp;
                                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                                <asp:Button ID="imgbtnPageNumb" runat="server" CssClass="small buttongo blue" Text="Go"
                                    EnableViewState="False"></asp:Button>
                                <asp:RangeValidator ID="rgvGo" runat="server" ErrorMessage="No Halaman Salah" ControlToValidate="txtPage"
                                    Type="Integer" MaximumValue="999999999" MinimumValue="1" CssClass="validator_general"
                                    Display="Dynamic"></asp:RangeValidator>
                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" Display="Dynamic"
                                    ErrorMessage="No Halaman Salah" ControlToValidate="txtPage" CssClass="validator_general"></asp:RequiredFieldValidator>
                            </div>
                            <div class="label_gridnavigation">
                                <asp:Label ID="lblPage" runat="server"></asp:Label>of
                                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue">
                    </asp:Button>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
