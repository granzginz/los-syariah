﻿#Region "Imports"
Imports Maxiloan.Controller
Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.Webform.UserController
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data.OleDb
Imports System.Data.Odbc
Imports System.Text
Imports System.Data.SqlClient
Imports System.IO
Imports System.Web.UI.WebControls.FileUpload
Imports Microsoft.Office.Interop
Imports System.Text.RegularExpressions
Imports System.Diagnostics
Imports System.Drawing
Imports System.Runtime.InteropServices

#End Region

Public Class SoftLoanRekonHO
    Inherits Maxiloan.Webform.WebBased

    Private myCmd As New SqlCommand
    Private myAdapter As New SqlDataAdapter
    Private myDS As New DataSet
    Private myReader As DataTableReader
    Private objCon As SqlConnection
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.AgreementList
    Private oController As New AgreementListController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If
        Me.SortBy = "UploadDate,BranchID ASC"
        If Not Me.IsPostBack Then
            pnlDatagrid.Visible = False
            If Not Me.IsHoBranch Then
                ShowMessage(lblMessage, "Harus Login Kantor Pusat", True)
                pnlHeader.Visible = False
                Exit Sub
            Else
                Me.FormID = "REKHOSL"
                If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                    GetCombo()                                        
                End If
                If Request.QueryString("BankAccountID") <> "" Or Request.QueryString("BranchId") <> "" Then                    
                    cbSoftLoan.SelectedIndex = cbSoftLoan.Items.IndexOf(cbSoftLoan.Items.FindByValue(Request.QueryString("BankAccountID").Trim))
                    Me.SearchBy = " BranchId ='" & Request.QueryString("BranchId") & "' and BankAccountID = '" & Request.QueryString("BankAccountID") & "'"
                    DoBind(Me.SortBy, Me.SearchBy)
                End If
            End If        
        End If
    End Sub
    Sub GetCombo()
        objCon = New SqlConnection(GetConnectionString)
        myDS = New DataSet
        myCmd.Parameters.Clear()
        myCmd.CommandText = "spGetComboSL"
        myCmd.Connection = objCon
        myAdapter.SelectCommand = myCmd
        myAdapter.Fill(myDS)

        If myDS.Tables(0).Rows.Count > 0 Then
            cbSoftLoan.DataTextField = "Name"
            cbSoftLoan.DataValueField = "ID"
            cbSoftLoan.DataSource = myDS.Tables(0)
            cbSoftLoan.DataBind()
        End If
    End Sub
    Private Sub DoBind(ByVal strSort As String, ByVal strSearch As String)
        pnlDatagrid.Visible = True
        Dim cContract As New InstallRcvController
        Dim oContract As New Parameter.GeneralPaging

        With oContract
            .strConnection = GetConnectionString()
            .WhereCond = strSearch
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = strSort
            .SpName = "spSoftLoanPaging"
        End With

        oContract = cContract.GetGeneralPaging(oContract)

        recordCount = oContract.TotalRecords
        DtgAgree.DataSource = oContract.ListData

        Try
            DtgAgree.DataBind()
        Catch
            DtgAgree.CurrentPageIndex = 0
            DtgAgree.DataBind()
        End Try
        PagingFooter()
    End Sub
    Public Sub SelectAll(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim chkSender As CheckBox = CType(sender, CheckBox)
        Dim chkItem As CheckBox
        Dim x As Integer

        For x = 0 To DtgAgree.Items.Count - 1
            chkItem = CType(DtgAgree.Items(x).FindControl("chkItem"), CheckBox)
            chkItem.Checked = chkSender.Checked
        Next
    End Sub
#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SortBy, Me.SearchBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles imgbtnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SortBy, Me.SearchBy)
            End If
        End If
    End Sub

    Private Sub DtgAgree_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgree.ItemDataBound

        If e.Item.ItemIndex >= 0 Then

        End If
    End Sub
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles DtgAgree.SortCommand
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
            DoBind(e.SortExpression, Me.SearchBy)
        Else
            Me.SortBy = e.SortExpression + " DESC"
            DoBind(e.SortExpression + " DESC", Me.SearchBy)
        End If

    End Sub

#End Region
    Private Sub btnSearch_Click(sender As Object, e As System.EventArgs) Handles btnSearch.Click
        Me.SearchBy = ""
        If cbSoftLoan.SelectedValue <> "" Then
            Me.SearchBy = " BankAccountID='" & cbSoftLoan.SelectedValue.Trim & "'"
        End If

        DoBind(Me.SortBy, Me.SearchBy)
        btnSave.Visible = True
    End Sub
    Private Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click
        Dim chkDtList As CheckBox
        Dim Status As New List(Of Integer)
        For i = 0 To DtgAgree.Items.Count - 1
            chkDtList = CType(DtgAgree.Items(i).FindControl("chkItem"), CheckBox)
            If Not IsNothing(chkDtList) Then
                If chkDtList.Enabled Then
                    If chkDtList.Checked Then
                        Dim param As New Parameter.SoftLoan
                        param.BranchId = CType(DtgAgree.Items(i).FindControl("lblBranchID"), Label).Text.Trim
                        param.TglTransaksi = CDate(IIf(CType(DtgAgree.Items(i).FindControl("lblTglTransaksi"), Label).Text.Trim = "", "01/01/1900", CType(DtgAgree.Items(i).FindControl("lblTglTransaksi"), Label).Text.Trim))
                        param.BusinessDate = Me.BusinessDate
                        param.BankAccountID = CType(DtgAgree.Items(i).FindControl("lblBankAccountID"), Label).Text.Trim
                        param.NilaiTransaksi = CDec(IIf(CType(DtgAgree.Items(i).FindControl("lblAgreementTotal"), Label).Text.Trim = "", 0, CType(DtgAgree.Items(i).FindControl("lblAgreementTotal"), Label).Text.Trim))
                        Try
                            Save(param)
                        Catch ex As Exception
                            ShowMessage(lblMessage, ex.Message, True)
                        End Try
                        Status.Add(1)
                    End If
                End If            
            End If
        Next

        If Status.Count = 0 Then
            ShowMessage(lblMessage, "Belum dipilih!", True)
            Exit Sub
        Else            DoBind(Me.SortBy, Me.SearchBy)
            ShowMessage(lblMessage, "Proses Rekon Success", False)
        End If
    End Sub
    Sub Save(ByRef param As Parameter.SoftLoan)
        Try
            objCon = New SqlConnection(GetConnectionString)
            myCmd.Parameters.Clear()
            myCmd.CommandText = "spPostingSoftLoanHO"
            myCmd.CommandType = CommandType.StoredProcedure
            myCmd.Parameters.Add(New SqlParameter("@BranchIDAgreement", SqlDbType.VarChar, 3) With {.Value = param.BranchId})
            myCmd.Parameters.Add(New SqlParameter("@ValueDate", SqlDbType.VarChar, 10) With {.Value = param.TglTransaksi})
            myCmd.Parameters.Add(New SqlParameter("@BusinessDate", SqlDbType.DateTime) With {.Value = param.BusinessDate})
            myCmd.Parameters.Add(New SqlParameter("@BankAccountID", SqlDbType.VarChar, 10) With {.Value = param.BankAccountID})
            myCmd.Parameters.Add(New SqlParameter("@AmountReceive", SqlDbType.Decimal) With {.Value = param.NilaiTransaksi})

            objCon.Open()
            myCmd.Connection = objCon
            myCmd.CommandTimeout = 0
            myCmd.ExecuteNonQuery()
            objCon.Close()
        Catch ex As Exception
            Throw New Exception(ex.Message)
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub
    Private Sub btnReset_Click(sender As Object, e As System.EventArgs) Handles btnReset.Click
        Response.Redirect("SoftLoanRekonHO.aspx")
    End Sub
End Class