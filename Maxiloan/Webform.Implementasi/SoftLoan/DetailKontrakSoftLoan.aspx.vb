﻿#Region "Imports"
Imports Maxiloan.Controller
Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.Webform.UserController
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data.OleDb
Imports System.Data.Odbc
Imports System.Text
Imports System.Data.SqlClient
Imports System.IO
Imports System.Web.UI.WebControls.FileUpload
Imports Microsoft.Office.Interop
Imports System.Text.RegularExpressions
Imports System.Diagnostics
Imports System.Drawing
Imports System.Runtime.InteropServices

#End Region

Public Class DetailKontrakSoftLoan
    Inherits Maxiloan.Webform.WebBased

    Private myCmd As New SqlCommand
    Private myAdapter As New SqlDataAdapter
    Private myDS As New DataSet
    Private myReader As DataTableReader
    Private objCon As SqlConnection
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.AgreementList
    Private oController As New AgreementListController

    Private Property Total() As Decimal
        Get
            Return CType(ViewState("Total"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            ViewState("Total") = Value
        End Set
    End Property
    Private Property BankAccountID() As String
        Get
            Return CType(ViewState("BankAccountID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("BankAccountID") = Value
        End Set
    End Property
    Private Property TmpBranchId() As String
        Get
            Return CType(ViewState("TmpBranchId"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("TmpBranchId") = Value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not Me.IsPostBack Then            
            If Not Me.IsHoBranch Then
                ShowMessage(lblMessage, "Harus Login Kantor Pusat", True)                
                Exit Sub
            Else
                Me.FormID = "REKHOSL"
                If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then                    
                    Me.SortBy = "SoftLoan.UploadDate,SoftLoan.AgreementNo ASC"
                    If Request.QueryString("BankAccountID") <> "" And Request.QueryString("BranchId") <> "" Then
                        TmpBranchId = Request.QueryString("BranchId")
                        BankAccountID = Request.QueryString("BankAccountID")
                        Me.SearchBy = " SoftLoan.BranchId ='" & TmpBranchId.Trim & "' and SoftLoan.BankAccountID = '" & BankAccountID.Trim & "'"
                        DoBind(Me.SortBy, Me.SearchBy)
                    End If
                End If                
            End If
        End If
    End Sub    
    Private Sub DoBind(ByVal strSort As String, ByVal strSearch As String)        
        Dim cContract As New InstallRcvController
        Dim oContract As New Parameter.GeneralPaging

        With oContract
            .strConnection = GetConnectionString()
            .WhereCond = strSearch
            .CurrentPage = currentPage
            .PageSize = 5000 'pageSize
            .SortBy = strSort
            .SpName = "spSoftLoanDetailPaging"
        End With

        oContract = cContract.GetGeneralPaging(oContract)

        recordCount = oContract.TotalRecords
        DtgAgree.DataSource = oContract.ListData

        Try
            DtgAgree.DataBind()
        Catch
            DtgAgree.CurrentPageIndex = 0
            DtgAgree.DataBind()
        End Try        
    End Sub            
    Private Sub btnBack_Click(sender As Object, e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("SoftLoanRekonHO.aspx?BranchId=" & TmpBranchId & "&BankAccountID=" & BankAccountID)
    End Sub

    Private Sub DtgAgree_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgree.ItemDataBound
        Dim hycustomername As HyperLink
        Dim hyAgreementNo As HyperLink
        Dim lblCustomerID As Label
        Dim lblApplicationID As Label
        If e.Item.ItemIndex > -1 Then
            hycustomername = CType(e.Item.FindControl("hyCustomerName"), HyperLink)
            hyAgreementNo = CType(e.Item.FindControl("hyAgreementNo"), HyperLink)
            lblCustomerID = CType(e.Item.FindControl("lblCustomerID"), Label)
            lblApplicationID = CType(e.Item.FindControl("lblApplicationID"), Label)


            '*** Customer Link            
            hycustomername.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(lblCustomerID.Text.Trim) & "')"
            '*** Agreement No link            
            hyAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(lblApplicationID.Text.Trim) & "')"



            Total = Total + CDbl(CType(e.Item.FindControl("lblNilaiTransaksi"), Label).Text)
        End If
        If e.Item.ItemType = ListItemType.Footer Then
            CType(e.Item.FindControl("lblTotal"), Label).Text = FormatNumber(Total, 0)
        End If
    End Sub
End Class