﻿#Region "Imports"
Imports Maxiloan.Controller
Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.Webform.UserController
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data.OleDb
Imports System.Data.Odbc
Imports System.Text
Imports System.Data.SqlClient
Imports System.IO
Imports System.Web.UI.WebControls.FileUpload
Imports System.Text.RegularExpressions
Imports System.Diagnostics
Imports System.Drawing
Imports System.Runtime.InteropServices
Imports System.Data
Imports System.Collections.Generic
Imports DocumentFormat.OpenXml.Packaging
Imports DocumentFormat.OpenXml.Spreadsheet

#End Region

Public Class SoftLoanUpload
    Inherits Maxiloan.Webform.WebBased

    Private myCmd As New SqlCommand
    Private myAdapter As New SqlDataAdapter
    Private myDS As New DataSet
    Private myReader As DataTableReader
    Private FileName As String
    Private strFileTemp As String
    Private strFile As String
    Private FileDirectory As String
    Private TotalAgreement As Double
    Private TotalInstallment As Decimal
    Private FileType As String
    Private _TglTransaksi As String
    Private _BranchID As String
    Private _AgreementNo As String
    Private objCon As SqlConnection
    Private TextFile As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblLoading.Visible = False
        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not Me.IsPostBack Then
            If Not Me.IsHoBranch Then
                ShowMessage(lblMessage, "Harus Login Kantor Pusat", True)
                pnlHeader.Visible = False
                Exit Sub
            Else
                Me.FormID = "UploadSoftLoan"
                If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                    GetCombo()
                End If
            End If
        End If
    End Sub
    Private Sub btnUpload_Click(sender As Object, e As System.EventArgs) Handles btnUpload.Click
        lblLoading.Visible = True
        btnUpload.Enabled = False
        Try
            If FileSoftLoan.HasFile Then
                strFileTemp = FileSoftLoan.FileName
                FileName = Context.Request.MapPath("~\\xml\\") + strFileTemp
                File.Delete(FileName)
                FileSoftLoan.SaveAs(FileName)
                Dim larr As Array = CType(strFileTemp.ToString.Split(CChar(".")), Array)
                TextFile = strFileTemp
                FileDirectory = Context.Request.MapPath("~\\xml\\").ToString.Trim
                FileType = larr.GetValue(1).ToString.Trim()

                Dim BankAccountID As String = cbSoftLoan.Items(cbSoftLoan.SelectedIndex).Value.Trim
                Proses(BankAccountID)
            Else
                lblLoading.Visible = False
                ShowMessage(lblMessage, "File belum dipilih!", True)
                Exit Sub
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try

        btnUpload.Enabled = True
        lblLoading.Visible = False
    End Sub
    Sub GetCombo()
        objCon = New SqlConnection(GetConnectionString)
        myDS = New DataSet
        myCmd.Parameters.Clear()
        myCmd.CommandText = "spGetComboSL"
        myCmd.Connection = objCon
        myAdapter.SelectCommand = myCmd
        myAdapter.Fill(myDS)

        If myDS.Tables(0).Rows.Count > 0 Then
            cbSoftLoan.DataTextField = "Name"
            cbSoftLoan.DataValueField = "ID"
            cbSoftLoan.DataSource = myDS.Tables(0)
            cbSoftLoan.DataBind()
        End If
    End Sub
    Function ValidasiFile() As Boolean
        Dim result As Boolean = True
        objCon = New SqlConnection(GetConnectionString)
        Try
            myDS = New DataSet
            myCmd.Parameters.Clear()
            myCmd.CommandText = "spGetVirtualAccountValidFile"
            myCmd.CommandType = CommandType.StoredProcedure
            myCmd.Parameters.Add(New SqlParameter("@FileName", SqlDbType.VarChar, 200) With {.Value = strFileTemp.Trim})
            myCmd.Connection = objCon
            myAdapter.SelectCommand = myCmd
            myAdapter.Fill(myDS)

            If myDS.Tables(0).Rows.Count > 0 Then
                result = False
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message)
            ShowMessage(lblMessage, ex.Message, True)
        End Try
        Return result
    End Function
    Private Function Text_To_DataTable(ByVal path As String, ByVal delimitter As Char, ByVal header As Boolean) As DataTable
        Dim source As String = String.Empty
        Dim dt As DataTable = New DataTable

        If IO.File.Exists(path) Then
            source = IO.File.ReadAllText(path)
        Else
            Throw New IO.FileNotFoundException("Could not find the file at " & path, path)
        End If

        Dim rows() As String = source.Split({Environment.NewLine}, StringSplitOptions.None)

        For i As Integer = 0 To rows(0).Split(delimitter).Length - 1
            Dim column As String = rows(0).Split(delimitter)(i)
            dt.Columns.Add(If(header, column, "column" & i + 1))
        Next

        For i As Integer = If(header, 1, 0) To rows.Length - 1
            Dim dr As DataRow = dt.NewRow

            For x As Integer = 0 To rows(i).Split(delimitter).Length - 1
                If x <= dt.Columns.Count - 1 Then
                    dr(x) = rows(i).Split(delimitter)(x)
                Else
                    Throw New Exception("The number of columns on row " & i + If(header, 0, 1) & " is greater than the amount of columns in the " & If(header, "header.", "first row."))
                End If
            Next

            dt.Rows.Add(dr)
        Next

        Return dt
    End Function
    Private Sub Proses(ByVal BankAccountID As String)
        Dim SequenceNo As Integer = 0
        Dim list As New List(Of Parameter.SoftLoan)
        Try            
            list = UploadFile()
            TotalAgreement = list.Count
            If TotalAgreement > 0 Then
                For index = 0 To list.Count - 1
                    Dim _SoftLoan As New Parameter.SoftLoan
                    _SoftLoan = list(index)
                    _SoftLoan.UploadDate = BusinessDate
                    _SoftLoan.BankAccountID = BankAccountID
                    SaveSoftLoan(_SoftLoan)
                Next
            End If
            ShowMessage(lblMessage, "Upload Data Berhasil", False)
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub
    Private Function GetValue(doc As SpreadsheetDocument, cell As Cell) As String
        Dim value As String = cell.CellValue.InnerText
        If cell.DataType IsNot Nothing AndAlso cell.DataType.Value = CellValues.SharedString Then
            Return doc.WorkbookPart.SharedStringTablePart.SharedStringTable.ChildElements.GetItem(Integer.Parse(value)).InnerText
        End If
        Return value
    End Function
    Private Function FileToDataTable() As DataTable
        'Create a new DataTable.
        Dim dt As New DataTable()

        Using doc As SpreadsheetDocument = SpreadsheetDocument.Open(FileName, False)
            'Read the first Sheet from Excel file.
            Dim sheet As Sheet = doc.WorkbookPart.Workbook.Sheets.GetFirstChild(Of Sheet)()

            'Get the Worksheet instance.
            Dim worksheet As Worksheet = TryCast(doc.WorkbookPart.GetPartById(sheet.Id.Value), WorksheetPart).Worksheet

            'Fetch all the rows present in the Worksheet.
            Dim rows As IEnumerable(Of Row) = worksheet.GetFirstChild(Of SheetData)().Descendants(Of Row)()

            'Loop through the Worksheet rows.
            For Each row As Row In rows
                'Use the first row to add columns to DataTable.
                If row.RowIndex.Value = 1 Then
                    Dim clm As Integer = 0
                    For Each cell As Cell In row.Descendants(Of Cell)()
                        If GetValue(doc, cell).Trim <> "" Then
                            dt.Columns.Add(GetValue(doc, cell))
                        Else
                            dt.Columns.Add(GetValue(doc, cell).Trim & clm.ToString)
                        End If
                        clm += 1
                    Next
                Else
                    'Add rows to DataTable.
                    dt.Rows.Add()
                    Dim i As Integer = 0
                    For Each cell As Cell In row.Descendants(Of Cell)()
                        dt.Rows(dt.Rows.Count - 1)(i) = GetValue(doc, cell)
                        i += 1
                    Next
                End If
            Next
        End Using
        Return dt
    End Function
    Function UploadFile() As List(Of Parameter.SoftLoan)
        Dim list As New List(Of Parameter.SoftLoan)
        Dim data As New DataTable

        Try            
            data = FileToDataTable()
            For index = 0 To data.Rows.Count - 1                
                Dim _SoftLoan As New Parameter.SoftLoan

                _SoftLoan.BranchId = CStr(data.Rows(index).ItemArray(0)).Trim
                _SoftLoan.TglTransaksi = ConvertDate2(IIf(CStr(data.Rows(index).ItemArray(1)).Trim = "", "01/01/1900", data.Rows(index).ItemArray(1)).Trim)                
                _SoftLoan.AgreementNo = CStr(data.Rows(index).ItemArray(2)).Trim                
                _SoftLoan.NamaPelanggan = CStr(data.Rows(index).ItemArray(3)).Trim                
                _SoftLoan.NilaiTransaksi = CDec(IIf(CStr(data.Rows(index).ItemArray(4)).Trim = "", "0", data.Rows(index).ItemArray(4)).Trim)

                Me._BranchID = _SoftLoan.BranchId
                Me._AgreementNo = _SoftLoan.AgreementNo
                Me._TglTransaksi = _SoftLoan.TglTransaksi

                Dim query As New Parameter.SoftLoan
                If list.Count > 0 Then
                    query = list.Find(AddressOf PredicateFunction)
                Else
                    query = Nothing
                End If

                If query Is Nothing Then
                    TotalInstallment = TotalInstallment + _SoftLoan.NilaiTransaksi
                    list.Add(_SoftLoan)
                End If
            Next
        Catch ex As Exception
            Throw New ApplicationException(ex.Message)
        End Try

        Return list
    End Function    
    Public Function PredicateFunction(ByVal custom As Parameter.SoftLoan) As Boolean
        Return custom.AgreementNo = Me._AgreementNo And custom.BranchId = Me._BranchID And custom.TglTransaksi = Me._TglTransaksi
    End Function
    Sub SaveSoftLoan(ByVal _SoftLoan As Parameter.SoftLoan)
        Try
            objCon = New SqlConnection(GetConnectionString)
            myCmd.Parameters.Clear()
            myCmd.CommandText = "spSoftLoanSave"
            myCmd.CommandType = CommandType.StoredProcedure
            myCmd.Parameters.Add(New SqlParameter("@UploadDate", SqlDbType.DateTime) With {.Value = _SoftLoan.UploadDate})
            myCmd.Parameters.Add(New SqlParameter("@BankAccountID", SqlDbType.VarChar, 10) With {.Value = _SoftLoan.BankAccountID})
            myCmd.Parameters.Add(New SqlParameter("@BranchID", SqlDbType.Char, 3) With {.Value = _SoftLoan.BranchId})
            myCmd.Parameters.Add(New SqlParameter("@AgreementNo", SqlDbType.Char, 20) With {.Value = _SoftLoan.AgreementNo})
            myCmd.Parameters.Add(New SqlParameter("@NamaPelanggan", SqlDbType.VarChar, 50) With {.Value = _SoftLoan.NamaPelanggan})
            myCmd.Parameters.Add(New SqlParameter("@NilaiTransaksi", SqlDbType.Decimal) With {.Value = _SoftLoan.NilaiTransaksi})
            myCmd.Parameters.Add(New SqlParameter("@TglTransaksi", SqlDbType.DateTime) With {.Value = _SoftLoan.TglTransaksi})

            objCon.Open()
            myCmd.Connection = objCon
            myCmd.ExecuteNonQuery()
            objCon.Close()
        Catch ex As Exception
            Throw New Exception(ex.Message)
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub
    Private Sub btnReset_Click(sender As Object, e As System.EventArgs) Handles btnReset.Click
        Response.Redirect("SoftLoanUpload.aspx")
    End Sub
End Class