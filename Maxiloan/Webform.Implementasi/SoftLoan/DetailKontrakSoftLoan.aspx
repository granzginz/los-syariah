﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="DetailKontrakSoftLoan.aspx.vb"
    Inherits="Maxiloan.Webform.Implementasi.DetailKontrakSoftLoan" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <style type="text/css">
        .progress_imgLoading
        {
            width: 4%;
            display: block;
            padding-top: 10px;
            padding-bottom: 20px;
            padding-left: 70px;
            background-image: url('../../Images/pic-loaderblack.gif');
            background-repeat: no-repeat;
            background-position: right;
            display: block;
        }
    </style>
</head>
<body>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
                onclick="hideMessage();"></asp:Label>
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>
                        REKONSILIASI POTONG GAJI
                    </h3>
                </div>
            </div>
            <div class="form_title">
                <div class="form_single">
                    <h4>
                        DAFTAR KONTRAK
                    </h4>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ws">
                        <asp:DataGrid ID="DtgAgree"  runat="server" CssClass="grid_general" BorderWidth="0"
                                AutoGenerateColumns="false" AllowSorting="true" ShowFooter="true">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <FooterStyle CssClass="item_grid" />
                            <Columns>
                                <asp:BoundColumn DataField="TglTransaksi" DataFormatString="{0:dd/MM/yyyy}" HeaderText="TGL TRANSAKSI"
                                        SortExpression="TglTransaksi"></asp:BoundColumn>
                                <asp:TemplateColumn SortExpression="BankAccountName" HeaderText="NAMA BANK">
                                    <ItemTemplate>
                                        <asp:Label ID="lblBankAccountName" runat="server" Text='<%#Container.DataItem("BankAccountName")%>'></asp:Label>                                        
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="BranchFullName" HeaderText="NAMA CABANG">
                                    <ItemTemplate>
                                        <asp:Label ID="lblBranchFullName" runat="server" Text='<%#Container.DataItem("BranchFullName")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>                              
                                <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="hyAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>'>
                                        </asp:HyperLink>                                        
                                        <asp:Label ID="lblBankAccountID" Visible="false" runat="server" Text='<%#Container.DataItem("BankAccountID")%>'></asp:Label>                                            
                                        <asp:Label ID="lblApplicationID" Visible="false" runat="server" Text='<%#Container.DataItem("ApplicationID")%>'></asp:Label>       
                                        <asp:Label ID="lblBranchID" Visible="false" runat="server" Text='<%#Container.DataItem("BranchID")%>'></asp:Label>       
                                        <asp:Label ID="lblCustomerID" Visible="false" runat="server" Text='<%#Container.DataItem("CustomerID")%>'></asp:Label>       
                                    </ItemTemplate>
                                </asp:TemplateColumn>                                                                                                            
                                <asp:TemplateColumn SortExpression="NamaPelanggan" HeaderText="NAMA">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="hyCustomerName" runat="server" Text='<%#Container.DataItem("NamaPelanggan")%>'>
                                        </asp:HyperLink>                                        
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        Total
                                    </FooterTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="NilaiTransaksi" HeaderText="JUMLAH">
                                    <ItemTemplate>
                                        <asp:Label ID="lblNilaiTransaksi" runat="server" Text='<%#formatnumber(Container.DataItem("NilaiTransaksi"),0)%>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label runat="server" ID="lblTotal"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>                        
                    </div>
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="small button blue">
                </asp:Button>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
