﻿Imports Maxiloan.Framework.SQLEngine
Imports System.Data.SqlClient

Public Class DeleteVoucher
    Inherits Maxiloan.Webform.WebBased


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If        
        If Not IsPostBack Then
            Me.FormID = "DELVCR"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                'skip
            End If
        End If

    End Sub


    Private Sub btnDelete_Click(sender As Object, e As System.EventArgs) Handles btnDelete.Click
        Dim params() As SqlParameter = New SqlParameter(0) {}

        params(0) = New SqlParameter("@VoucherNo", SqlDbType.VarChar, 20)
        params(0).Value = txtVoucherNo.Text.Trim



        Try
            SqlHelper.ExecuteNonQuery(GetConnectionString, CommandType.StoredProcedure, "spBatalTraksaksi", params)
            pnl2.Visible = False
            pnl1.Visible = True
            ShowMessage(lblMessage, "Hapus berhasil.", False)
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try

    End Sub

    Private Sub btnNext_Click(sender As Object, e As System.EventArgs) Handles btnNext.Click
        pnl2.Visible = True
        pnl1.Visible = False
        lblNoVoucher.Text = "Nomor voucher " & txtVoucherNo.Text.Trim & " akan dihapus?"
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As System.EventArgs) Handles btnCancel.Click
        pnl2.Visible = False
        pnl1.Visible = True
        lblMessage.Visible = False
    End Sub
End Class