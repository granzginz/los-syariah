﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region
Public Class TDPSplit
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents obankaccount As UcBankAccountID
    Protected WithEvents txtAmountFrom As ucNumberFormat
    Protected WithEvents txtamountto As ucNumberFormat
    Protected WithEvents txtAmountFrom0 As ucNumberFormat
    Protected WithEvents txtamountto0 As ucNumberFormat

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.TerimaTDP
    Private oController As New ImplementasiControler

#End Region
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not IsPostBack Then
            Me.FormID = "TDPSPLIT"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                oBankAccount.BankPurpose = ""
                oBankAccount.BankType = ""
                oBankAccount.IsAll = True
                oBankAccount.BindBankAccount()
                Me.SearchBy = ""
                Me.SortBy = ""
            End If

        End If
    End Sub
    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView

        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oCustomClass = oController.TDPReversalList(oCustomClass)

        DtUserList = oCustomClass.ListData
        DvUserList = DtUserList.DefaultView
        recordCount = oCustomClass.TotalRecord
        DvUserList.Sort = Me.SortBy
        DtgAgree.DataSource = DvUserList

        Try
            DtgAgree.DataBind()
        Catch
            DtgAgree.CurrentPageIndex = 0
            DtgAgree.DataBind()
        End Try

        PagingFooter()
        pnlList.Visible = True
        pnlDatagrid.Visible = True
    End Sub

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            ShowMessage(lblMessage, "Data Tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        lblMessage.Visible = False
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imgbtnPageNumb.Click
        If IsNumeric(txtGoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtGoPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                lblMessage.Visible = False
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub
#End Region

    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbReset.Click
        Server.Transfer("TDPSPlit.aspx")
    End Sub
    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imgSearch.Click
        If SessionInvalid() Then
            Exit Sub
        End If
        'If CheckFeature(Me.Loginid, Me.FormID, "SRCH", Me.AppId) Then
        lblMessage.Visible = False
        Me.SearchBy = " tdp.branchid = '" & Me.sesBranchId.Replace("'", "") & "' and "
        Me.SearchBy = Me.SearchBy & " tdp.tdpreceivestatus = 'R' and  tdp.otorisasi = 'A'  "


        If oBankAccount.BankAccountID <> "ALL" Then
            Me.SearchBy = Me.SearchBy & " AND tdp.bankaccountid = '" & obankaccount.BankAccountID.Trim & "' "
        End If

        If txtTglJatuhTempo.Text <> "" Then
            Me.SearchBy = Me.SearchBy & " AND tdp.valuedate ='" & ConvertDate2(txtTglJatuhTempo.Text) & "'  "
        End If

        If txtdesc.Text.Trim <> "" Then
            Me.SearchBy = Me.SearchBy & " AND tdp.description Like '%" & txtdesc.Text.Trim & "%'  "
        End If

        If txtRefNo.Text.Trim <> "" Then
            Me.SearchBy = Me.SearchBy & " AND tdp.TransferRefNo LIKE '%" & txtRefNo.Text.Trim & "%'"
        End If

        If IsNumeric(txtAmountFrom0.Text.Trim) And IsNumeric(txtamountto0.Text.Trim) Then
            If CDbl(txtAmountFrom0.Text.Trim) > 0 And CDbl(txtamountto0.Text.Trim) > 0 Then
                Me.SearchBy = Me.SearchBy & "AND (tdp.amount BETWEEN " & CDbl(txtAmountFrom0.Text.Trim) & " AND " & CDbl(txtamountto0.Text.Trim) & ")"
            End If
        End If

        If IsNumeric(txtAmountFrom.Text.Trim) And IsNumeric(txtamountto.Text.Trim) Then
            If CDbl(txtAmountFrom.Text.Trim) > 0 And CDbl(txtamountto.Text.Trim) > 0 Then
                Me.SearchBy = Me.SearchBy & " OR ( tdp.branchid = '" & Me.sesBranchId.Replace("'", "") &
                "' AND tdp.tdpreceivestatus = 'R' AND tdp.amount BETWEEN " & CDbl(txtAmountFrom.Text.Trim) & " AND " & CDbl(txtamountto.Text.Trim) & ")"
            End If
        End If

        pnlDatagrid.Visible = True
        DtgAgree.Visible = True
        pnlList.Visible = True
        DoBind(Me.SearchBy, Me.SortBy)
        'End If
    End Sub

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
    Private Sub DtgAgree_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgree.ItemDataBound
        Dim NlblTDPReceiveNo As HyperLink
        Dim Nbranchid As Label
        Dim NhyReverse As HyperLink
        ' Me.FormID = "SUSPENDALLOCATION"
        'HyReverse()
        If e.Item.ItemIndex >= 0 Then
            NlblTDPReceiveNo = CType(e.Item.FindControl("lblTDPReceiveNo"), HyperLink)
            Nbranchid = CType(e.Item.FindControl("lblbranchid"), Label)
            ' If CheckFeature(Me.Loginid, Me.FormID, "CHG", Me.AppId) Then

            NhyReverse = CType(e.Item.FindControl("HyReverse"), HyperLink)
            NhyReverse.NavigateUrl = "TDPSplitTransaction.aspx?TDPReceiveNo=" & NlblTDPReceiveNo.Text.Trim & "&branchid=" & Nbranchid.Text.Trim
            NlblTDPReceiveNo.NavigateUrl = "javascript:OpenWinMain('" & NlblTDPReceiveNo.Text.Trim & "','" & Nbranchid.Text.Trim & "')"
            'End If

        End If
    End Sub
End Class