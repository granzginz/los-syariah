﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class TerimaTDP
    Inherits Maxiloan.Webform.WebBased
    ' Protected WithEvents sdate As ValidDate
    Protected WithEvents obankaccount As UcBankAccountID
    Protected WithEvents txtAmount As ucNumberFormat

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.TerimaTDP
    Private oController As New ImplementasiControler

#End Region

#Region "Property"


    'Private Property FromBankAccount() As String
    '    Get
    '        Return (CType(Viewstate("FromBankAccount"), String))
    '    End Get
    '    Set(ByVal Value As String)
    '        Viewstate("FromBankAccount") = Value
    '    End Set
    'End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not IsPostBack Then
            Me.FormID = "TerimaTDP"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If Request.QueryString("message") <> "" Then
                    ShowMessage(lblMessage, Request.QueryString("message"), IIf(Request.QueryString("message") = MessageHelper.MESSAGE_INSERT_SUCCESS, False, True))
                End If
                'sdate.dateValue = CStr(Day(Me.BusinessDate)) + "/" + CStr(Month(Me.BusinessDate)) + "/" + CStr(Year(Me.BusinessDate))
                'sdate.isCalendarPostBack = False
                'sdate.FillRequired = True
                'sdate.FieldRequiredMessage = "Harap isi tanggal Valuta"


                obankaccount.BankPurpose = ""
                obankaccount.BankType = ""
                obankaccount.BindBankAccount()

                If Not IsNothing(Session("BankSelected")) Then obankaccount.BankAccountID = Session("BankSelected").ToString.Trim
                Me.SearchBy = ""
                Me.SortBy = ""
            End If
        End If
    End Sub

    Private Sub imgSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles imgSave.Click
        If CheckFeature(Me.Loginid, Me.FormID, "SAVE", Me.AppId) Then
            If txtTglValuta.Text = "" Then

                ShowMessage(lblMessage, "Harap isi Tanggal Valuta", True)
                Exit Sub
            End If
            If CDbl(txtAmount.Text) <= 0 Then

                ShowMessage(lblMessage, "Jumlah Terima harus > 0", True)
                Exit Sub
            End If

            Session.Add("BankSelected", obankaccount.BankAccountID)

            With (oCustomClass)
                .BranchId = Me.sesBranchId.Replace("'", "")
                .LoginId = Me.Loginid
                .BusinessDate = Me.BusinessDate
                .AmountRec = CDbl(txtAmount.Text)
                .ValueDate = ConvertDate2(txtTglValuta.Text)
                .ReferenceNo = txtRefNo.Text
                .description = txtdesc.Text.Trim
                .BankAccountID = obankaccount.BankAccountID
                .strConnection = GetConnectionString()
                .CoyID = Me.SesCompanyID
            End With
            Try
                oController.SaveTerimaTDP(oCustomClass)
                Server.Transfer("TerimaTDP.aspx?message=" & MessageHelper.MESSAGE_INSERT_SUCCESS)
            Catch exp As Exception

                ShowMessage(lblMessage, exp.Message, True)
            End Try
        End If
    End Sub
End Class