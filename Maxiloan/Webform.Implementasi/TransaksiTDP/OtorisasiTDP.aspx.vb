﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports System.Data.SqlClient

#End Region

Public Class OtorisasiTDP
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents obankaccount As UcBankAccountID
    Protected WithEvents txtAmountFrom As ucNumberFormat
    Protected WithEvents txtamountto As ucNumberFormat
    Protected WithEvents txtAmountFrom0 As ucNumberFormat
    Protected WithEvents txtamountto0 As ucNumberFormat
#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.SuspendReceive
    Private oController As New SuspendReversalController

#End Region

#Region "Property"

    Private Property ApplicationID() As String
        Get
            Return (CType(ViewState("ApplicationID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property

    Private Property SuspendNo() As String
        Get
            Return (CType(ViewState("SuspendNo"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("SuspendNo") = Value
        End Set
    End Property


#End Region
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not IsPostBack Then
            If Request.QueryString("message") <> "" Then
                ShowMessage(lblMessage, Request.QueryString("message"), IIf(Request.QueryString("message") = MessageHelper.MESSAGE_UPDATE_SUCCESS, False, True))
            End If
            Me.FormID = "OTORTDP"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                obankaccount.BankPurpose = ""
                obankaccount.BankType = ""
                obankaccount.IsAll = True
                obankaccount.BindBankAccount()



                Me.SearchBy = ""
                Me.SortBy = ""
            End If

        End If
    End Sub
    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim objCommand As New SqlCommand
        Dim objread As SqlDataReader
        Dim objconnection As New SqlConnection(GetConnectionString)


        If objconnection.State = ConnectionState.Closed Then objconnection.Open()
        objCommand.CommandType = CommandType.StoredProcedure
        objCommand.Connection = objconnection
        objCommand.CommandText = "spOtorTDPPaging"
        objCommand.Parameters.Add("@WhereCond", SqlDbType.VarChar, 2000).Value = cmdWhere
        objCommand.Parameters.Add("@SortBy", SqlDbType.VarChar, 100).Value = SortBy
        objread = objCommand.ExecuteReader()
        DtgAgree.DataSource = objread

        Try
            DtgAgree.DataBind()
            objread.Close()
        Catch ex As Exception
            Response.Write(ex.StackTrace)
            'lblMessage.Text = e.Message
            ShowMessage(lblMessage, ex.Message, True)
        Finally
            objCommand.Parameters.Clear()
            objCommand.Dispose()
            If objconnection.State = ConnectionState.Open Then objconnection.Close()
            objconnection.Dispose()
        End Try
        pnlList.Visible = True
        pnlDatagrid.Visible = True
    End Sub




    Private Sub DtgAgree_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgree.ItemDataBound
        Dim NlblTDPReceiveNo As Label
        Dim Nbranchid As Label

        If e.Item.ItemIndex >= 0 Then
            NlblTDPReceiveNo = CType(e.Item.FindControl("lblTDPReceiveNo"), Label)
            Nbranchid = CType(e.Item.FindControl("lblbranchid"), Label)

            'NlblTDPReceiveNo.NavigateUrl = "javascript:OpenWinMain('" & NlblTDPReceiveNo.Text.Trim & "','" & Nbranchid.Text.Trim & "')"


        End If
    End Sub
    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbReset.Click
        Server.Transfer("OtorisasiTDP.aspx")
    End Sub
    Public Sub SelectAll(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim chkSender As CheckBox = CType(sender, CheckBox)
        Dim chkItem As CheckBox
        Dim x As Integer

        For x = 0 To DtgAgree.Items.Count - 1
            chkItem = CType(DtgAgree.Items(x).FindControl("chkItem"), CheckBox)
            chkItem.Checked = chkSender.Checked
        Next
    End Sub
    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imgSearch.Click
        If SessionInvalid() Then
            Exit Sub
        End If
        'If CheckFeature(Me.Loginid, Me.FormID, "SRCH", Me.AppId) Then
        lblMessage.Visible = False
        Me.SearchBy = " where tdp.branchid = '" & Me.sesBranchId.Replace("'", "") & "' and "
        Me.SearchBy = Me.SearchBy & " tdp.TDPReceiveStatus = 'R' and isnull(tdp.Otorisasi,'')= 'N' "


        If obankaccount.BankAccountID <> "ALL" Then
            Me.SearchBy = Me.SearchBy & " AND tdp.bankaccountid = '" & obankaccount.BankAccountID.Trim & "' "
        End If

        If txtTglJatuhTempo.Text <> "" Then
            Me.SearchBy = Me.SearchBy & " AND tdp.valuedate ='" & ConvertDate2(txtTglJatuhTempo.Text) & "'  "
        End If

        If txtdesc.Text.Trim <> "" Then
            Me.SearchBy = Me.SearchBy & " AND tdp.description Like '%" & txtdesc.Text.Trim & "%'  "
        End If

        If txtRefNo.Text.Trim <> "" Then
            Me.SearchBy = Me.SearchBy & " AND tdp.TransferRefNo LIKE '%" & txtRefNo.Text.Trim & "%'"
        End If

        If IsNumeric(txtAmountFrom0.Text.Trim) And IsNumeric(txtamountto0.Text.Trim) Then
            If CDbl(txtAmountFrom0.Text.Trim) >= 0 And CDbl(txtamountto0.Text.Trim) > 0 Then
                Me.SearchBy = Me.SearchBy & "AND (tdp.amount BETWEEN " & CDbl(txtAmountFrom0.Text.Trim) & " AND " & CDbl(txtamountto0.Text.Trim) & ")"
            End If
        End If

        If IsNumeric(txtAmountFrom.Text.Trim) And IsNumeric(txtamountto.Text.Trim) Then
            If CDbl(txtAmountFrom.Text.Trim) >= 0 And CDbl(txtamountto.Text.Trim) > 0 Then
                Me.SearchBy = Me.SearchBy & " OR ( tdp.branchid = '" & Me.sesBranchId.Replace("'", "") &
                "' AND tdp.TDPReceiveStatus = 'R' and  isnull(tdp.Otorisasi,'')= 'N' AND tdp.amount BETWEEN " & CDbl(txtAmountFrom.Text.Trim) & " AND " & CDbl(txtamountto.Text.Trim) & ")"
            End If
        End If

        pnlDatagrid.Visible = True
        DtgAgree.Visible = True
        pnlList.Visible = True

        DoBind(Me.SearchBy, Me.SortBy)
        'End If
    End Sub

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#Region "save"
    Private Sub BtnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSave.Click

        Try
            System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, GetType(Page), "Script", "preventMultipleSubmissions2();", True)

            Dim oOtorisasiTDPController As New ImplementasiControler
            Dim oEntities As New Parameter.TerimaTDP
            oEntities.strConnection = GetConnectionString()
            For n As Integer = 0 To DtgAgree.DataKeys.Count - 1
                Dim chkItem As CheckBox = CType(DtgAgree.Items(n).FindControl("chkItem"), CheckBox)
                If chkItem.Checked = True Then
                    Dim lblTDPReceiveNo As Label = CType(DtgAgree.Items(n).FindControl("lblTDPReceiveNo"), Label)
                    oEntities.TDPReceiveNo = lblTDPReceiveNo.Text.Trim
                    oEntities.LoginId = Me.Loginid
                    oOtorisasiTDPController.SaveOtorisasiTDP(oEntities)
                End If
            Next
            DoBind(Me.SearchBy, Me.SortBy)
            ShowMessage(lblMessage, "Data Berhasil di simpan", False)
        Catch ex As Exception

            ShowMessage(lblMessage, ex.Message, True)
        End Try

    End Sub
#End Region
End Class