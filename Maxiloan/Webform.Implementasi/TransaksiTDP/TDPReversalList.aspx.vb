﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class TDPReversalList
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private oCustomClass As New Parameter.TerimaTDP
    Private oController As New ImplementasiControler
    Dim strbankaccountid As String
#End Region

#Region "Property"


    Private Property TDPReceiveNo() As String
        Get
            Return (CType(ViewState("TDPReceiveNo"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("TDPReceiveNo") = Value
        End Set
    End Property

    Private Property BankAccountid() As String
        Get
            Return (CType(ViewState("BankAccountid"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("BankAccountid") = Value
        End Set
    End Property


#End Region



    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not IsPostBack Then
            Me.FormID = "RevTDP"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.TDPReceiveNo = Request.QueryString("TDPReceiveNo")
                Me.BranchID = Request.QueryString("branchid")

                Me.SearchBy = ""
                Me.SortBy = ""
                DoBind()
            End If

        End If
    End Sub

#Region "DoBind"
    Sub DoBind()

        oCustomClass.TDPReceiveNo = Me.TDPReceiveNo
        oCustomClass.strConnection = GetConnectionString()
        oCustomClass.BranchId = Me.BranchID
        oCustomClass = oController.ReverseTDP(oCustomClass)
        With oCustomClass
            lblTDPReceiveNo.Text = Me.TDPReceiveNo
            lblPostingDate.Text = .postingdate
            lblbankAccount.Text = .BankAccountName
            lblRefNO.Text = .ReferenceNo
            'txtReversalReffNo.Text = .ReferenceNo
            lblTDPDate.Text = .ValueDate
            lblAmount.Text = FormatNumber(.AmountRec, 2)
            lbldesc.Text = .description
            strbankaccountid = .BankAccountID
            Me.BankAccountid = strbankaccountid
        End With

    End Sub
#End Region

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
        If SessionInvalid() Then
            Exit Sub
        End If
        If CheckFeature(Me.Loginid, Me.FormID, "SAVE", Me.AppId) Then
            btnSave.Attributes.Add("Onclick", "return checksubmit()")
            With oCustomClass
                .LoginId = Me.Loginid
                .BranchId = Me.BranchID
                .strConnection = GetConnectionString()
                .BusinessDate = Me.BusinessDate
                .TDPReceiveNo = Me.TDPReceiveNo
                .description = txtCorrection.Text
                .BankAccountID = Me.BankAccountid
                .AmountRec = CDbl(lblAmount.Text)
                .ValueDate = lblTDPDate.Text
                .ReferenceNo = txtReversalReffNo.Text.Trim
                .CoyID = Me.SesCompanyID
            End With
            Try
                oController.SaveTDPReverse(oCustomClass)
                Server.Transfer("ReversalTDP.aspx?message=" & MessageHelper.MESSAGE_UPDATE_SUCCESS)

                ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
            Catch exp As Exception

                ShowMessage(lblMessage, exp.Message, True)
            End Try
        End If
    End Sub

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCancel.Click
        Server.Transfer("ReversalTDP.aspx")
    End Sub

End Class