﻿#Region " Import"
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCacheHelper

#End Region

Public Class FundingInstallmentPayment
    Inherits Maxiloan.Webform.WebBased

#Region "uccomponent"

    Protected WithEvents txtAgingDate As ucDateCE
    Protected WithEvents txtAgingDateto As ucDateCE
    Protected WithEvents txtValueDate As ucDateCE
#End Region
#Region " Declares"
    Dim Total As Double
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 1000
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    'Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    'Protected WithEvents Datagrid1 As System.Web.UI.WebControls.DataGrid
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.PembayaranAngsuranFunding
    'Protected WithEvents Requiredfieldvalidator1 As System.Web.UI.WebControls.RequiredFieldValidator
    'Protected WithEvents Requiredfieldvalidator4 As System.Web.UI.WebControls.RequiredFieldValidator

    Private oController As New PembayaranAngsuranFundingController
    Private dtBankAccount As New DataTable
    Private m_controller As New DataUserControlController
    Protected WithEvents oBGNODate As ucBGNoDate
    'Protected WithEvents lblTagihanNet As System.Web.UI.WebControls.Label
    'Protected WithEvents lblRefundBunga As System.Web.UI.WebControls.Label
#End Region

#Region " Property"
    Private Property PrincipalPaidAmount() As Double
        Get
            Return (CType(ViewState("PrincipalPaidAmount"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("PrincipalPaidAmount") = Value
        End Set
    End Property

    Private Property InterestPaidAmount() As Double
        Get
            Return (CType(ViewState("InterestPaidAmount"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("InterestPaidAmount") = Value
        End Set
    End Property

    Private Property RefundInterest() As Double
        Get
            Return (CType(ViewState("RefundInterest"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("RefundInterest") = Value
        End Set
    End Property

    Private Property ListBankAccount() As DataTable
        Get
            Return (CType(ViewState("ListBankAccount"), DataTable))
        End Get
        Set(ByVal Value As DataTable)
            ViewState("ListBankAccount") = Value
        End Set
    End Property

    Private Property SelectMode() As Boolean
        Get
            Return (CType(ViewState("SelectMode"), Boolean))
        End Get
        Set(ByVal Value As Boolean)
            ViewState("SelectMode") = Value
        End Set
    End Property

    Private Property cmdWhere() As String
        Get
            Return CType(ViewState("cmdwhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("cmdwhere") = Value
        End Set
    End Property

    Private Property SelectedApplicationIDs() As String
        Get
            Return CType(ViewState("SelectedApplicationIDs"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("SelectedApplicationIDs") = Value
        End Set
    End Property


#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then Exit Sub
        If Not Page.IsPostBack Then
            txtAgingDate.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
            txtAgingDateto.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
            txtValueDate.Text = Me.BusinessDate.ToString("dd/MM/yyyy")

            'Direct print
            If Request.QueryString("strFileLocation") <> "" Then
                Dim strHTTPServer As String
                Dim StrHTTPApp As String
                Dim strNameServer As String
                Dim strFileLocation As String

                strHTTPServer = Request.ServerVariables("PATH_INFO")
                strNameServer = Request.ServerVariables("SERVER_NAME")
                StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                strFileLocation = "http://" & strNameServer & "/" & StrHTTPApp & "/XML/" & Request.QueryString("strFileLocation") & ".pdf"

                Response.Write("<script language = javascript>" & vbCrLf _
                & "window.open('" & strFileLocation & "','accacq', 'menubar=0, scrollbars=yes, resizable=1') " & vbCrLf _
                & "</script>")
            End If

            bindFundingCompany()
            bindWayOfPayment()
            Me.SelectMode = True
        End If
    End Sub

    Sub DoBind(ByVal strWhere As String, ByVal SortBy As String)
        Dim oDataTable As New DataTable
        Dim oDataView As New DataView

        'not use this parameter
        'strWhere = IIf(strWhere.Trim = "", "", strWhere)

        With oCustomClass
            .strConnection = GetConnectionString()
            .FundingCoyID = drdCompany.SelectedValue
            .FundingContractNo = drdContract.SelectedValue
            .InstallmentDueDate = ConvertDate2(txtAgingDate.Text).ToString("yyyy/MM/dd")
            .InstallmentDueDateTo = ConvertDate2(txtAgingDateto.Text).ToString("yyyy/MM/dd")
        End With

        oCustomClass = oController.getFundingAngsJthTempoSummary(oCustomClass)

        Me.PrincipalPaidAmount = oCustomClass.JumlahPokok
        Me.InterestPaidAmount = oCustomClass.JumlahBunga
        Me.RefundInterest = oCustomClass.RefundBunga

        lblAmountRec.Text = FormatNumber(Me.PrincipalPaidAmount + Me.InterestPaidAmount - Me.RefundInterest, 2)
        lblJumlahKontrak.Text = FormatNumber(oCustomClass.JumlahKontrak, 0)
        lblJumlahPokok.Text = FormatNumber(Me.PrincipalPaidAmount, 2)
        lblJumlahAngsuran.Text = FormatNumber(oCustomClass.JumlahAngsuran, 2)
        lblJumlahBunga.Text = FormatNumber(Me.InterestPaidAmount, 2)
        lblRefundBunga.Text = FormatNumber(Me.RefundInterest, 2)
        lblTagihanNet.Text = FormatNumber(oCustomClass.JumlahAngsuran - Me.RefundInterest, 2)

        pnlView.Visible = True
        lblMessage.Text = ""
    End Sub

    Private Sub bindFundingCompany()
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim cContract As New GeneralPagingController
        Dim oContract As New Parameter.GeneralPaging
        With oContract
            .strConnection = GetConnectionString()
            .WhereCond = ""
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = ""
            .SpName = "spFundingGetComboCompany"
        End With
        oContract = cContract.GetGeneralPaging(oContract)
        Try
            dtsEntity = oContract.ListData
            dtvEntity = dtsEntity.DefaultView
            drdCompany.DataValueField = "FundingCoyId"
            drdCompany.DataTextField = "FundingCoyName"
            drdCompany.DataSource = dtvEntity
            drdCompany.DataBind()
            drdCompany.Items.Insert(0, "Select One")
            drdCompany.Items(0).Value = "0"
        Catch e As Exception

            ShowMessage(lblMessage, e.Message, True)
        End Try
    End Sub

    Private Sub bindFundindContract()
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim cContract As New GeneralPagingController
        Dim oContract As New Parameter.GeneralPaging
        With oContract
            .strConnection = GetConnectionString()
            .WhereCond = " where FundingCoyID = '" & drdCompany.SelectedValue.Trim & "'"
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = ""
            .SpName = "spFundingContractList"
        End With
        oContract = cContract.GetGeneralPaging(oContract)
        Try
            dtsEntity = oContract.ListData
            dtvEntity = dtsEntity.DefaultView
            drdContract.DataValueField = "FundingContractNo"
            drdContract.DataTextField = "ContractName"
            drdContract.DataSource = dtvEntity
            drdContract.DataBind()
            drdContract.Items.Insert(0, "Select One")
            drdContract.Items(0).Value = "0"
        Catch e As Exception
            ShowMessage(lblMessage, e.Message, True)
        End Try
    End Sub
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If

        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub btnFind_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFind.Click
        DoBind(Me.cmdWhere, Me.SortBy)
    End Sub

    Private Sub drdCompany_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles drdCompany.SelectedIndexChanged
        bindFundindContract()
    End Sub

    Private Sub bindWayOfPayment()
        Dim oDataTable As DataTable = New DataTable
        Dim oRow As DataRow
        Dim splitListData() As String
        Dim splitRow() As String
        Dim i As Integer
        Dim strListData As String

        oDataTable.Columns.Add("ID", System.Type.GetType("System.String"))
        oDataTable.Columns.Add("Description", System.Type.GetType("System.String"))

        oRow = oDataTable.NewRow()
        oRow("ID") = ""
        oRow("Description") = ""

        'only bank account
        strListData = "BA,Bank"
        splitListData = Split(strListData, "-")

        For i = 0 To UBound(splitListData)
            splitRow = Split(splitListData(i), ",")
            oRow("ID") = splitRow(0)
            oRow("Description") = splitRow(1)
            oDataTable.Rows.Add(oRow)
            oRow = oDataTable.NewRow()
        Next

        cmbWayOfPayment.DataValueField = "ID"
        cmbWayOfPayment.DataTextField = "Description"
        cmbWayOfPayment.DataSource = oDataTable
        cmbWayOfPayment.DataBind()
        cmbWayOfPayment.Items.Insert(0, "Select One")
        cmbWayOfPayment.Items(0).Value = "0"
    End Sub

    Private Sub btnNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNext.Click        
        oBGNODate.BindBGNODate(" bankaccountid = '" & cmbBankAccount.SelectedValue.Trim & "'  and branchid = '" & Me.sesBranchId.Replace("'", "") & "' ")

        If cmbWayOfPayment.SelectedValue = "BA" Then
            oBGNODate.Visible = True
        Else
            oBGNODate.Visible = False
        End If

        pnlTop.Visible = False
        pnlView.Visible = False
        pnlTransfer.Visible = True
    End Sub

    Private Sub cmbWayOfPayment_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbWayOfPayment.SelectedIndexChanged
        bindBankAccount(cmbWayOfPayment.SelectedValue)
        If cmbWayOfPayment.SelectedValue = "BA" Then
            oBGNODate.Visible = True
        Else
            oBGNODate.Visible = False
        End If
    End Sub

    Private Sub bindBankAccount(ByVal strBankType As String)
        dtBankAccount = CType(Me.Cache.Item(CACHE_BANKACCOUNT & Me.sesBranchId.Replace("'", "") & strBankType), DataTable)

        If dtBankAccount Is Nothing Then
            Dim dtBankAccountCache As New DataTable

            dtBankAccountCache = m_controller.GetBankAccount(GetConnectionString, Me.sesBranchId, strBankType, "FD")
            Me.Cache.Insert(CACHE_BANKACCOUNT & Me.sesBranchId.Replace("'", "") & strBankType, dtBankAccountCache, Nothing, DateTime.Now.AddHours(DEFAULT_TIME_DURATION_CACHE_HOUR), TimeSpan.Zero)
            dtBankAccount = CType(Me.Cache.Item(CACHE_BANKACCOUNT & Me.sesBranchId.Replace("'", "") & strBankType), DataTable)
        End If

        Me.ListBankAccount = dtBankAccount
        cmbBankAccount.DataSource = Me.ListBankAccount
        cmbBankAccount.DataTextField = "Name"
        cmbBankAccount.DataValueField = "ID"
        cmbBankAccount.DataBind()
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        pnlTop.Visible = True
        pnlView.Visible = True
        pnlTransfer.Visible = False
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim oCustomClass1 As New Parameter.DisbursePembayaranAngsuranFunding
        With oCustomClass1
            .strConnection = GetConnectionString()
            .BranchId = Replace(sesBranchId, "'", "").Trim
            .LoginId = Me.Loginid
            .CompanyId = SesCompanyID
            .BusinessDate = Me.BusinessDate
            .FundingCoyID = drdCompany.SelectedValue.Trim
            .FundingContractNo = drdContract.SelectedValue.Trim
            .PrincipalPaidAmount = Me.PrincipalPaidAmount
            .InterestPaidAmount = Me.InterestPaidAmount - Me.RefundInterest
            .PenaltyPaidAmount = 0
            .BankAccountId = cmbBankAccount.SelectedValue
            .ValueDate = ConvertDate2(txtAgingDate.Text)
            .ReferenceNo = txtReferenceNo.Text.Trim
            .PPHPaid = 0
            .SelectedApplicationIDs = Me.SelectedApplicationIDs
            .InstallmentDueDate = ConvertDate2(txtAgingDate.Text).ToString("yyyy/MM/dd")
            .InstallmentDueDateTo = ConvertDate2(txtAgingDateto.Text).ToString("yyyy/MM/dd")
        End With
        Try
            oController.disburseFundingAngsuranJthTempo(oCustomClass1)
            pnlTop.Visible = True
            pnlView.Visible = False
            pnlTransfer.Visible = False
            'directPrint()
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

    'Private Sub directPrint()
    '    Dim cookie As HttpCookie = Request.Cookies("BonHijauFundingDisburse")

    '    If Not cookie Is Nothing Then
    '        cookie.Values("referenceNo") = txtReferenceNo.Text.Trim
    '        Response.AppendCookie(cookie)
    '    Else
    '        Dim cookieNew As New HttpCookie("BonHijauFundingDisburse")

    '        cookieNew.Values.Add("referenceNo", txtReferenceNo.Text.Trim)
    '        Response.AppendCookie(cookieNew)
    '    End If

    '    Response.Redirect("BonHijauFundingDisbViewer.aspx")
    'End Sub
End Class