﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FundingInstallmentPayment.aspx.vb"
    Inherits="Maxiloan.Webform.Implementasi.FundingInstallmentPayment" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ucBGNoDate" Src="../../Webform.UserController/ucBGNoDate.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucDateCE" Src="../../Webform.UserController/ucDateCE.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>FundingInstallmentPayment</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function fclose() {
            window.close();
        }
    </script>
    <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function total(e, parTotal) {
            //        var rowCount = document.getElementById(e).rows.length - 1;
            //            var Valtotal = 0;
            //            for (i = 0; i < rowCount; i++) {
            //                var Amount = document.getElementById(e + i).value.replace(/,/gi, "");
            //                Valtotal = Valtotal + parseFloat(Amount);
            //            }
            document.getElementById('Datagrid1_lblSum').innerHTML = number_format(parTotal);
        }

        function number_format(number, decimals, dec_point, thousands_sep) {
            number = (number + '')
            .replace(/[^0-9+\-Ee.]/g, '');
            var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function (n, prec) {
                var k = Math.pow(10, prec);
                return '' + (Math.round(n * k) / k)
                .toFixed(prec);
            };
            // Fix for IE parseFloat(0.55).toFixed(0) = 0;
            s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
            .split('.');
            if (s[0].length > 3) {
                s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
            }
            if ((s[1] || '')
            .length < prec) {
                s[1] = s[1] || '';
                s[1] += new Array(prec - s[1].length + 1)
              .join('0');
            }
            return s.join(dec);
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
        onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlTop" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    PEMBAYARAN ANGSURAN PER JATUH TEMPO
                </h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    Funding Bank</label>
                <asp:DropDownList ID="drdCompany" runat="server" AutoPostBack="True">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" InitialValue="0"
                    Enabled="True" Display="Dynamic" ControlToValidate="drdCompany" ErrorMessage="Harap pilih Funding Bank"
                    Visible="True" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    No. Fasilitas</label>
                <asp:DropDownList ID="drdContract" runat="server">
                </asp:DropDownList>
                <%--<asp:RequiredFieldValidator ID="Requiredfieldvalidator4" runat="server" InitialValue="0"
                    Enabled="True" Display="Dynamic" ControlToValidate="drdContract" ErrorMessage="Harap pilih No Kontrak"
                    Visible="True" CssClass="validator_general" ></asp:RequiredFieldValidator>--%>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Tgl Jatuh Tempo</label>
                <%--<asp:TextBox runat="server" ID="txtAgingDate"></asp:TextBox>
                <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtAgingDate"
                    Format="dd/MM/yyyy">
                </asp:CalendarExtender>--%>
                <uc1:ucdatece id="txtAgingDate" runat="server"></uc1:ucdatece>
                <label class="label_auto">
                    s/d</label>
                <uc1:ucdatece id="txtAgingDateto" runat="server"></uc1:ucdatece>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnFind" runat="server" Text="Find" CssClass="small button blue">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlView" runat="server" Visible="False">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    SUMMARY PEMBAYARAN ANGSURAN
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Jumlah Fasilitas
                </label>
                <asp:Label ID="lblJumlahKontrak" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Jumlah Tagihan
                </label>
                <asp:Label ID="lblJumlahAngsuran" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Jumlah Pokok
                </label>
                <asp:Label ID="lblJumlahPokok" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Refund Margin
                </label>
                <asp:Label ID="lblRefundBunga" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Jumlah Margin
                </label>
                <asp:Label ID="lblJumlahBunga" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Tagihan Net
                </label>
                <asp:Label ID="lblTagihanNet" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnNext" runat="server" Text="Next" CssClass="small button green">
            </asp:Button>
            <asp:Button ID="btnclose" runat="server" OnClientClick="fclose();" Text="Close" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlTransfer" runat="server" Visible="False">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    PEMBAYARAN ANGSURAN PER JATUH TEMPO
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    No Bukti Kas Keluar</label>
                <asp:TextBox ID="txtReferenceNo" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvRef" runat="server" Enabled="True" Display="Dynamic"
                    ControlToValidate="txtReferenceNo" ErrorMessage="Harap isi No Bukti Kas Keluar"
                    Visible="True" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
            <div class="form_right">
                <label class="label_req">
                    Cara Pembayaran</label>
                <asp:DropDownList ID="cmbWayOfPayment" runat="server" AutoPostBack="True">
                </asp:DropDownList>
                <asp:Button ID="btnRefreshBankAccount" runat="server" Text="Refresh" CausesValidation="False">
                </asp:Button>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" InitialValue="0"
                    Enabled="True" Display="Dynamic" ControlToValidate="cmbWayOfPayment" ErrorMessage="Harap pilih Cara Pembayaran"
                    Visible="True" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Tanggal Valuta</label>
                <%--<asp:TextBox runat="server" ID="txtValueDate"></asp:TextBox>
                <asp:CalendarExtender runat="server" ID="CalendarExtender2" TargetControlID="txtValueDate"
                    Format="dd/MM/yyyy">
                </asp:CalendarExtender>--%>
                <uc1:ucdatece id="txtValueDate" runat="server"></uc1:ucdatece>
            </div>
            <div class="form_right">
                <label class="label_req">
                    Rekening Bank</label>
                <asp:DropDownList ID="cmbBankAccount" runat="server">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator3" runat="server" InitialValue="0"
                    Enabled="True" Display="Dynamic" ControlToValidate="cmbBankAccount" ErrorMessage="Harap Pilih rekening Bank"
                    Visible="True" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Jumlah</label>
                <asp:Label ID="lblAmountRec" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Catatan</label>
                <asp:TextBox ID="txtNotes" runat="server" CssClass="long_text" ></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <uc1:ucbgnodate id="oBgNoDate" runat="server"></uc1:ucbgnodate>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
