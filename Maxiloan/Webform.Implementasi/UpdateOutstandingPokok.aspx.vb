﻿#Region "Imports"
Imports System.Threading
Imports System.Text
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region
Public Class UpdateOutstandingPokok
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oSearchBy As UcSearchBy
    Protected WithEvents oBranch As ucBranchAll
    Protected WithEvents txtAngsuran1 As ucNumberFormat
    Protected WithEvents txtAngsuran2 As ucNumberFormat
    Protected WithEvents txtNilaiOSP As ucNumberFormat

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 6
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.AgreementList
    Private oController As New AgreementListController
    Private oDataUserCtlr As New DataUserControlController
    Private mController As New ImplementasiControler
#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            If Request.QueryString("filekwitansi") <> "" Then
                Dim strHTTPServer As String
                Dim StrHTTPApp As String
                Dim strNameServer As String
                Dim strFileLocation As String

                strHTTPServer = Request.ServerVariables("PATH_INFO")
                strNameServer = Request.ServerVariables("SERVER_NAME")
                StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                strFileLocation = "http://" & strNameServer & "/" & StrHTTPApp & "/XML/" & Request.QueryString("filekwitansi") & ".pdf"

                Response.Write("<script language = javascript>" & vbCrLf _
                & "window.open('" & strFileLocation & "','Installment', 'left=0, top=0, width=600, height=480, menubar=0, scrollbars=yes') " & vbCrLf _
                & "</script>")
            End If

            If Request.QueryString("Message") <> "" Then
                ShowMessage(lblMessage, Request.QueryString("Message"), True)
            End If

            Me.FormID = "UPDOUTSPOKOK"
            oSearchBy.ListData = "Name, Nama-Agreementno, No. Kontrak-Address, Alamat-LicensePlate, No Polisi"
            oSearchBy.BindData()

            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.SearchBy = ""
                Me.SortBy = ""
            End If

            pnlDatagrid.Visible = False
            pnlList.Visible = True
            pnlUpdate.Visible = False
        End If
    End Sub

    Private Sub DoBind(ByVal strSort As String, ByVal strSearch As String)
        lblMessage.Visible = False
        lblMessage.Text = ""

        pnlDatagrid.Visible = True
        pnlList.Visible = True
        pnlUpdate.Visible = False

        Dim oCustom As New Parameter.GeneralPaging

        With oCustom
            .strConnection = GetConnectionString()
            .WhereCond = strSearch
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = strSort
            .SPName = "spOutstandingPokokPaging"
        End With

        oCustom = mController.GetGeneralPaging(oCustom)

        If oCustom.ListData.Rows.Count > 0 Then
            recordCount = oCustom.TotalRecords
            DtgAgree.DataSource = oCustom.ListData
        End If

        Try
            DtgAgree.DataBind()
        Catch
            DtgAgree.CurrentPageIndex = 0
            DtgAgree.DataBind()
        End Try
        PagingFooter()
        pnlList.Visible = True
        pnlDatagrid.Visible = True
    End Sub

#End Region

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SortBy, Me.SearchBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles imgbtnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SortBy, Me.SearchBy)
            End If
        End If
    End Sub

    Private Sub DtgAgree_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgree.ItemDataBound
        'Dim hypReceive As HyperLink
        Dim lblApplicationid As HyperLink
        Dim lblCustID As Label
        Dim lblName As HyperLink
        Dim lblAgreementNo As HyperLink

        If e.Item.ItemIndex >= 0 Then
            lblApplicationid = CType(e.Item.FindControl("lblApplicationid"), HyperLink)
            'hypReceive = CType(e.Item.FindControl("HypReceive"), HyperLink)
            'hypReceive.NavigateUrl = "InstallRcv.aspx?applicationid=" & lblApplicationid.Text.Trim & "&branchid=" & oBranch.BranchID.Trim

            lblAgreementNo = CType(e.Item.FindControl("lblAgreementNo"), HyperLink)
            If lblAgreementNo.Text.Trim.Length > 0 Then
                lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(lblApplicationid.Text.Trim) & "')"
            End If

            If lblApplicationid.Text.Trim.Length > 0 Then
                lblApplicationid.NavigateUrl = "javascript:OpenApplicationId('" & "AccMnt" & "', '" & Server.UrlEncode(lblApplicationid.Text.Trim) & "')"
            End If
            lblName = CType(e.Item.FindControl("lblName"), HyperLink)
            lblCustID = CType(e.Item.FindControl("lblCustID"), Label)
            If lblCustID.Text.Trim.Length > 0 Then
                lblName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(lblCustID.Text.Trim) & "')"
            End If
        End If
    End Sub
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles DtgAgree.SortCommand
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
            DoBind(e.SortExpression, Me.SearchBy)
        Else
            Me.SortBy = e.SortExpression + " DESC"
            DoBind(e.SortExpression + " DESC", Me.SearchBy)
        End If

    End Sub

#End Region

    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbReset.Click
        Dim strSearch As New StringBuilder
        strSearch.Append(" branchid = '" & oBranch.BranchID.Trim & "'")

        Me.SearchBy = ""
        oSearchBy.Text = ""
        oSearchBy.BindData()

        txtAngsuran1.Text = ""
        txtAngsuran2.Text = ""

        Me.SearchBy = strSearch.ToString
        DoBind("", Me.SearchBy)
    End Sub

    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imgsearch.Click
        Dim strSearch As New StringBuilder
        strSearch.Append(" branchid = '" & oBranch.BranchID.Trim & "'")
        'Me.SearchBy = " branchid = '" & oBranch.BranchID.Trim & "' "

        If oSearchBy.Text.Trim <> "" Then
            strSearch.Append(" and " & oSearchBy.ValueID & " like  '%" & oSearchBy.Text.Trim.Replace("'", "''") & "%'")
            'Me.SearchBy &= " and " & oSearchBy.ValueID & " = '" & oSearchBy.Text.Trim.Replace("'", "''") & "'"
        End If
        If txtAngsuran1.Text > 0 Then
            If txtAngsuran2.Text > 0 Then
                strSearch.Append(" and InstallmentAmount between '" & CDbl(txtAngsuran1.Text) & "' and '" & CDbl(txtAngsuran2.Text) & "' ")
            Else
                strSearch.Append(" and InstallmentAmount = '" & CDbl(txtAngsuran1.Text) & "' ")
            End If
        End If
        Me.SearchBy = strSearch.ToString
        pnlDatagrid.Visible = True
        pnlList.Visible = True
        DoBind(Me.SortBy, Me.SearchBy)
    End Sub

    Private Sub imbPrint_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If SessionInvalid() Then
            Exit Sub
        End If
        If CheckFeature(Me.Loginid, Me.FormID, "Print", Me.AppId) Then
            ' If Me.checkCashier() Then

            Dim cookie As HttpCookie = Request.Cookies(Me.FormID)
            If Not cookie Is Nothing Then
                cookie.Values("where") = Me.SearchBy
                cookie.Values("sortby") = Me.SortBy
                Response.AppendCookie(cookie)
            Else
                Dim cookieNew As New HttpCookie(Me.FormID)
                cookieNew.Values.Add("where", Me.SearchBy)
                cookieNew.Values.Add("SortBy", Me.SortBy)
                Response.AppendCookie(cookieNew)
            End If
            Response.Redirect("Report/AgreementListReport.aspx")
            'End If
        End If
    End Sub

    Private Sub DtgAgree_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgAgree.ItemCommand
        Select Case e.CommandName
            Case "Update"
                With txtNilaiOSP
                    .RequiredFieldValidatorEnable = True
                    .RangeValidatorEnable = True
                    .TextCssClass = "numberAlign medium_text"
                End With

                pnlDatagrid.Visible = False
                pnlList.Visible = False
                pnlUpdate.Visible = True

                Dim lblApplicationid As HyperLink
                Dim lblName As HyperLink
                Dim lblAgreementNo As HyperLink
                lblApplicationid = CType(DtgAgree.Items(e.Item.ItemIndex).FindControl("lblApplicationid"), HyperLink)
                lblName = CType(DtgAgree.Items(e.Item.ItemIndex).FindControl("lblName"), HyperLink)
                lblAgreementNo = CType(DtgAgree.Items(e.Item.ItemIndex).FindControl("lblAgreementNo"), HyperLink)


                Dim imp As New Parameter.Implementasi
                With imp
                    .strConnection = GetConnectionString()
                    .WhereCond = " BranchID='" & oBranch.BranchID.Trim & "' and Applicationid='" & lblApplicationid.Text.Trim & "'"
                    .SPName = "spImGetOutstandingPrincipal"
                End With

                imp = mController.GetSP(imp)
                If imp.Listdata.Rows.Count > 0 Then
                    txtNilaiOSP.Text = FormatNumber(imp.Listdata.Rows(0).Item("OutstandingPrincipal"), 0)
                End If

                lblNoKontrak.Text = lblAgreementNo.Text.Trim
                lblNama.Text = lblName.Text.Trim
                hdApplicationID.Value = lblApplicationid.Text.Trim
        End Select
    End Sub


    Private Sub ButtonSave_Click(sender As Object, e As System.EventArgs) Handles ButtonSave.Click
        Dim imp As New Parameter.Implementasi

        Try
            With imp
                .strConnection = GetConnectionString()
                .ApplicationID = hdApplicationID.Value
                .BranchId = oBranch.BranchID.Trim
                .NilaiOSP = CDec(txtNilaiOSP.Text)
            End With

            mController.UpdateOutstandingPokok(imp)
            ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)

            pnlDatagrid.Visible = False
            pnlList.Visible = True
            pnlUpdate.Visible = False
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

    Private Sub ButtonCancel_Click(sender As Object, e As System.EventArgs) Handles ButtonCancel.Click
        Response.Redirect("UpdateOutstandingPokok.aspx")
    End Sub
End Class