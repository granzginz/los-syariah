﻿Imports Maxiloan.Parameter
Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.Win32
Imports Maxiloan.Controller
Imports System.Configuration

Public Class am_com_001
    Private oController As New LoginController

    Public Function getUser(ByVal LStrUser As String, ByVal LStrPwd As String, ByVal LStrAppID As String) As Int16
        'CREATE REGISTRY BUAT LOGIN ....
        If isSuperUser(LStrUser) Then
            If isValid(LStrUser, LStrPwd) Then
                getUser = 1
            Else
                getUser = 0
            End If
        Else
            Dim objDecent As New Decrypt.am_futility
            Dim oCustomClass As New Maxiloan.Parameter.Login
            Dim LStrPassWord As String

            oCustomClass.LoginId = LStrUser
            oCustomClass.Applicationid = LStrAppID         
            oCustomClass = oController.GetUser(oCustomClass)
            LStrPassWord = objDecent.encryptto(oCustomClass.Password, "1")

            If oCustomClass.LoginId <> LStrUser Then
                getUser = -2
            ElseIf LStrPassWord.Trim = LStrPwd.Trim Then
                getUser = 2
                If oCustomClass.IsActive = False Then
                    getUser = -3
                End If
            Else
                getUser = -1
            End If

            If oCustomClass.LoginId = "" And oCustomClass.Password = "" Then
                getUser = 0
            End If
        End If
    End Function

    Private Function isSuperUser(ByVal LStrUser As String) As Boolean
        Dim LStrCek As String = UCase(LStrUser)
        If LStrCek = "ADINS" Or LStrCek = "ADMIN" Or LStrCek = "SYSAPP" Then
            isSuperUser = True
        Else
            isSuperUser = False
        End If
    End Function

    Private Function isValid(ByVal LStrUser As String, ByVal LStrPwd As String) As Boolean        
        Dim LStrkey As RegistryKey = Registry.LocalMachine.OpenSubKey("Software\" & ConfigurationManager.AppSettings("RegKey"))
        Dim objFUtil As New Decrypt.am_futility
        Dim LStrPassKey As String = CType(LStrkey.GetValue(LStrUser), String)

        If LStrPassKey <> "" Then
            LStrPassKey = objFUtil.encryptto(LStrPassKey, "1")
        End If

        If UCase(Trim(LStrPassKey)) = UCase(Trim(LStrPwd)) Then
            isValid = True
        Else
            isValid = False
        End If
    End Function

    Public Function CheckPassword(ByVal LStrUserNm As String, ByVal LStrPassword As String) As Int16
        Dim oController As New LoginController
        Dim oCustomclass As New Maxiloan.Parameter.Login
        Dim objutility As New Decrypt.am_futility
        Dim oAm As New am_com_001
        Dim oDt As New DataTable
        Dim oDs As New DataSet
        Dim strResult As Int16 = 1
        Dim intI As Integer
        Dim strPasswordOld As String

        Try

            If LStrUserNm.ToLower.Trim = LStrPassword.ToLower.Trim Then
                strResult = 2
            End If

            If strResult = 1 Then
                With oCustomclass
                    .LoginId = LStrUserNm.Trim
                End With

                oCustomclass = oController.ListPasswordHistory(oCustomclass)
                oDt = oCustomclass.ListPassword

                If oDt.Rows.Count > 0 Then
                    For intI = 0 To oDt.Rows.Count - 1
                        strPasswordOld = CType(oDt.Rows(intI)("PasswordNew"), String)
                        If LStrPassword.ToLower = objutility.encryptto(strPasswordOld, "1").ToLower Then
                            strResult = 3
                            Exit For
                        End If
                    Next
                End If
            End If

            If strResult = 1 Then
                If LStrUserNm.ToLower.IndexOf(LStrPassword.ToLower) <> -1 Then
                    strResult = 4
                End If
            End If

            If strResult = 1 Then
                If LStrPassword.Trim.Length < oCustomclass.PwdLength Then
                    strResult = 5
                End If
            End If
            Return strResult
        Catch exp As Exception
            Return 0
        End Try
    End Function

    Public Function isValidSuperUser(ByVal LstrLoginid As String, ByVal Lstrpass As String) As Boolean
        isValidSuperUser = False
        If isSuperUser(LstrLoginid) Then
            If isValid(LstrLoginid, Lstrpass) Then
                isValidSuperUser = True
            End If
        End If
    End Function
End Class

