﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="LookupTransaction.aspx.vb" Inherits="Maxiloan.Webform.Utility.LookUpTransaction" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
	<script src="../../js/jquery-1.9.1.min.js"></script>
	<script src="../../js/jquery-ui-1.10.3.custom.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%= dtgPaging.ClientID %> td > a").click(function (e) {

                e.preventDefault();
                var transactionID = $(this).parents('tr').find('.transactionID').html();
                var transaction = $(this).parents('tr').find('.transaction').html();
                var COA = $(this).parents('tr').find('.COA').html();
                var dataArg_Temp = [
                { name: '<%= PaymentAllocationID %>', value: transactionID },
                { name: '<%= Description %>', value: transaction },
                { name: '<%= COA %>', value: COA }
                ];
                var dataArg = new Array();

                $.each(dataArg_Temp, function (index, value) {
                    dataArg.push({ name: value.name, value: value.value });

                });
                window.parent.BindFromJLookup(dataArg);
                window.parent.CloseJLookup();
            });


        });

       
    </script>
</head>
<body>
    <form id="form1" runat="server" class="ui-lookup">
    <asp:Label ID="lblMessage" runat="server" ForeColor="red"></asp:Label>
    <div class="form_box_title">
        <div class="form_single">
            <h4>
                DAFTAR TRANSAKSI
            </h4>
        </div>
    </div>
         <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ws">
                        <asp:DataGrid ID="dtgPaging" runat="server" Width="100%" AllowPaging="True" AllowSorting="True"
                            AutoGenerateColumns="False" DataKeyField="PaymentAllocationID" BorderStyle="None"
                            BorderWidth="1px" OnSortCommand="Sorting" CellPadding="0" CssClass="grid_general">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <Columns>
                                <asp:TemplateColumn HeaderText="Select">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lblSelect" runat="server" Text='Select'>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                       <asp:TemplateColumn HeaderText="Payment Allocation">
                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblPaymentAllocationID" CssClass="transactionID" runat="server"
                                            Text='<%#Container.DataItem("PaymentAllocationID")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Description">
                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblDescription" CssClass="transaction" runat="server"
                                            Text='<%#Container.DataItem("Description")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                              
                                <asp:TemplateColumn HeaderText="COA">
                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblCOA" CssClass="COA" runat="server" Text='<%#Container.DataItem("COA")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                             
                            </Columns>
                            <PagerStyle Visible="False" HorizontalAlign="Left" ForeColor="#000066" BackColor="White"
                                Mode="NumericPages"></PagerStyle>
                        </asp:DataGrid>
                    </div>
                    <div class="button_gridnavigation">
                        <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                            CausesValidation="False" CommandName="First" OnCommand="NavigationLink_Click">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                            CausesValidation="False" CommandName="Prev" OnCommand="NavigationLink_Click">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                            CausesValidation="False" CommandName="Next" OnCommand="NavigationLink_Click">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                            CausesValidation="False" CommandName="Last" OnCommand="NavigationLink_Click">
                        </asp:ImageButton>
                        Page
                        <asp:TextBox ID="txtGoPage" runat="server" Width="34px" CssClass="txtPage">1</asp:TextBox>
                        <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtGoPage" MinimumValue="1"
                            MaximumValue="999" ErrorMessage="Page No. is not valid" Type="Double" Display="Dynamic"></asp:RangeValidator>
                        <asp:Button ID="btnGoPage" runat="server" EnableViewState="False" Text="Go" CssClass="small buttongo blue" />
                        <asp:RangeValidator ID="Rangevalidator1" runat="server" Type="Integer" MaximumValue="999999999"
                            ErrorMessage="Page No. is not valid" MinimumValue="1" ControlToValidate="txtGoPage"
                            Display="Dynamic"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="rfvGo" runat="server" ErrorMessage="Page No. is not valid"
                            ControlToValidate="txtGoPage" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                    <div class="navigasi_totalrecord">
                        <asp:Label ID="lblPage" runat="server"></asp:Label>of
                        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                        <asp:Label ID="lblTotRec" runat="server"></asp:Label>record(s)
                    </div>
                </div>
            </div>
    <div class="form_box_title">
        <div class="form_single">
            <h4>
                FIND
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Transaction
            </label>
            <asp:Label ID="lblProcessId" runat="server">.........</asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Find by
            </label>
            <asp:DropDownList ID="cboSearchBy" runat="server">
                <asp:ListItem Value="Description">Description</asp:ListItem>
                <asp:ListItem Value="COA">COA</asp:ListItem>
            </asp:DropDownList>
            <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="buttonSearch" runat="server" CssClass="small button blue" Text="Search" />
        <asp:Button ID="btnReset" runat="server" CssClass="small button gray" Text="Reset" />
    </div>
    <input id="hdnTransaction" type="hidden" name="hdnTransaction" runat="server" />
    <input id="hdnDescription" type="hidden" name="hdnDescription" runat="server" />
    <input id="hdnCOA" type="hidden" name="hdnCOA" runat="server" />
    </form>
</body>
</html>
