﻿#Region "Imports"
'Imports Maxiloan.Parameter.GL
Imports Maxiloan.Controller
'Imports Maxiloan.Parameter
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Parameter

#End Region
Public Class MasterAccIsLeaf
    Inherits WebBased
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private _recordCount As Integer = 1
    Protected WithEvents GridNavigator As ucGridNav
    Private ReadOnly _coaController As New COAController
    Private x_controller As New DataUserControlController
    Protected WithEvents oBranch As ucBranchAll
#Region "Property"

    Public Property KodeCID() As String
        Get
            Return CType(ViewState("kodeCID"), String)
        End Get
        Set(ByVal value As String)
            ViewState("kodeCID") = value
        End Set
    End Property
    Public Property NamaCID() As String
        Get
            Return CType(ViewState("namaCID"), String)
        End Get
        Set(ByVal value As String)
            ViewState("namaCID") = value
        End Set
    End Property
    Property isDetail As Boolean
        Set(ByVal value As Boolean)
            ViewState("isDetail") = value
        End Set
        Get
            Return CBool(ViewState("isDetail"))
        End Get
    End Property
    Public Property Cabang() As String
        Get
            Return CType(ViewState("Cabang"), String)
        End Get
        Set(ByVal value As String)
            ViewState("Cabang") = value
        End Set
    End Property
    Property IsLeaf As Boolean
        Set(ByVal value As Boolean)
            ViewState("IsLeaf") = value
        End Set
        Get
            Return CBool(ViewState("IsLeaf"))
        End Get
    End Property
#End Region
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'txtGoPage.Text = "1"
        AddHandler GridNavigator.PageChanged, AddressOf Navevent
        'If (BranchID = "") Then
        '    BranchID = sesBranchId.Replace("'", "")
        'End If

        'If Me.IsHoBranch = False Then
        '    NotAuthorized()
        '    Exit Sub
        'End If

        If Not Page.IsPostBack Then
            KodeCID = Request("kodeCID")
            NamaCID = Request("namaCID")
            isDetail = False
            Dim reqLeft = Request("isDt")
            Cabang = Request("Cabang").Replace("'", "")
            IsLeaf = Request("isDt")

            If (String.IsNullOrEmpty(reqLeft)) Then
                ddlAccountType.Visible = False
                hdnTypeId.Value = Request("tp")
                lblTypeName.Text = Request("tpn")
            Else
                lblTypeName.Visible = False
                isDetail = True
                With ddlAccountType
                    .DataValueField = "Value"
                    .DataTextField = "Text"
                    .DataSource = ModuleGlHelper.GetEnumMasterAccTypeList()
                    .DataBind()
                End With
            End If

            pnlGrid.Style.Add("display", "none")

            With ddlCabang
                .DataSource = x_controller.GetBranchAll(GetConnectionString)
                .DataValueField = "ID"
                .DataTextField = "Name"
                .DataBind()
                .Items.Insert(0, "ALL")
                .Items(0).Value = "ALL"
            End With

            'With ddlCabang
            '    .DataSource = x_controller.GetBranchName(GetConnectionString, Cabang)
            '    .DataValueField = "ID"
            '    .DataTextField = "Name"
            '    .DataBind()
            '    .Enabled = True
            '    .SelectedValue = "900"
            'End With
        End If
    End Sub

    Sub NotAuthorized()
        Dim strHTTPServer As String
        Dim StrHTTPApp As String
        Dim strNameServer As String
        strHTTPServer = Request.ServerVariables("PATH_INFO")
        strNameServer = Request.ServerVariables("SERVER_NAME")
        StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
        Response.Redirect("http://" & strNameServer & "/" & StrHTTPApp & "/error_notauthorized.aspx")
    End Sub

    Private Sub Navevent(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        BindGridFind(e.CurrentPage)
        GridNavigator.ReInitialize(e.CurrentPage, _recordCount, e.TotalPage)
    End Sub


    Sub BindGridFind(currPage As Integer)

        Dim acctype
        If (ddlAccountType.Visible) Then
            acctype = ddlAccountType.SelectedIndex.ToString

        Else
            acctype = hdnTypeId.Value
        End If

        Dim opW = ddlCari.SelectedValue
        Dim sWhare = txtCari.Text.Trim

        BranchID = ddlCabang.SelectedValue.Trim

        Dim _dt As IList(Of MasterAccountObject) = _coaController.SelectDataIsLeaf(_recordCount, GetConnectionString(), IsLeaf, isDetail, New Object() {SesCompanyID, BranchID, currPage, pageSize, acctype, opW, sWhare})
        dtgPaging.DataSource = _dt
        dtgPaging.DataBind()
    End Sub
    Private Sub btnSearch_Click(sender As Object, e As System.EventArgs) Handles buttonSearch.Click

        Const alwayFirstPage As Integer = 1
        BindGridFind(alwayFirstPage)
        GridNavigator.Initialize(_recordCount, pageSize)
        pnlGrid.Style.Add("display", "inherit")
    End Sub

End Class