﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="MasterAccDetail.aspx.vb" Inherits="Maxiloan.Webform.Utility.MasterAccDetail" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link href="../../include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
	<script src="../../js/jquery-1.9.1.min.js"></script>
	<script src="../../js/jquery-ui-1.10.3.custom.min.js"></script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%= dtgPaging.ClientID %> td > a").click(function (e) {
                e.preventDefault();
                var kode = $(this).parents('tr').find('.kode').html().replace(/&nbsp;/g, "");
                var nama = $(this).parents('tr').find('.nama').html().replace(/&nbsp;/g, "");
     
                var dataArg_Temp = [
                { name: '<%= KodeClientID %>', value: kode },
                { name: '<%= NamaClientID %>', value: nama }
                ];
                var dataArg = new Array();
                $.each(dataArg_Temp, function (index, value) {
                    dataArg.push({ name: value.name, value: value.value });
                });
                window.parent.BindFromJLookup(dataArg);
                window.parent.CloseJLookup();
            });


        });

    
    </script>
</head>

<body>

    <form id="form1" runat="server">
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
                <asp:Panel ID="pnlGrid" runat="server">
                    <div class="form_box_title">
                        <div class="title_strip">
                        </div>
                        
                    </div>
                    <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ws">
                            <asp:DataGrid ID="dtgPaging" runat="server" CellPadding="0" OnSortCommand="SortGrid"
                                BackColor="White" BorderWidth="1px" BorderStyle="None" BorderColor="#CCCCCC"
                                DataKeyField="CoAId" AutoGenerateColumns="False" AllowSorting="True"
                                AllowPaging="True" Width="100%" Visible="True" CssClass="grid_general">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="Select">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lblSelect" runat="server" Text='Select'  >
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText = "Account No.">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblDisplay"  Text='<%# eval("coaid")  %>' />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>                     
                                   <asp:BoundColumn DataField="Description" HeaderText="Account Name" ItemStyle-CssClass="nama" />
                                   <asp:TemplateColumn HeaderText="Account Type">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblAccountType" Visible="false"   />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="Currency" HeaderText="Currency" />
                                    <asp:TemplateColumn HeaderStyle-BorderWidth="0">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblAccountTypeID" Visible="false"  Text='<%# eval("Type")  %>' />
                                            <asp:Label runat="server" ID="lblBranchId"  Text='<%# eval("BranchId") %>' CssClass="branch" style="display:none" />
                                            <asp:Label runat="server" ID="lblCoAId" Text='<%# eval("CoAId") %>' CssClass="kode"  style="display:none" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
 
                         
                                </Columns>
                                <PagerStyle Visible="False" HorizontalAlign="Left" ForeColor="#000066" BackColor="White"
                                    Mode="NumericPages"></PagerStyle>
                            </asp:DataGrid>
                        </div>
                        <div class="button_gridnavigation">
                            <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton01.png"
                                OnCommand="NavigationLink_Click" CommandName="First"></asp:ImageButton>
                            <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton02.png"
                                OnCommand="NavigationLink_Click" CommandName="Prev"></asp:ImageButton>
                            <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton03.png"
                                OnCommand="NavigationLink_Click" CommandName="Next"></asp:ImageButton>
                            <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton04.png"
                                OnCommand="NavigationLink_Click" CommandName="Last"></asp:ImageButton>
                            Page
                            <asp:TextBox ID="txtGoPage" runat="server" CssClass="small_text">1</asp:TextBox>
                            <asp:Button ID="btnGoPage" runat="server" EnableViewState="False" Text="Go" CssClass="small buttongo blue">
                            </asp:Button>
                            <asp:RangeValidator ID="Rangevalidator1" runat="server" Type="Integer" MaximumValue="999999999"
                                ErrorMessage="Page No. is not valid" MinimumValue="1" ControlToValidate="txtGoPage"
                                Display="Dynamic"></asp:RangeValidator>
                            <asp:RequiredFieldValidator ID="rfvGo" runat="server" ErrorMessage="Page No. is not valid"
                                ControlToValidate="txtGoPage" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                            <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtGoPage" MinimumValue="1"
                                MaximumValue="999" ErrorMessage="Page No. is not valid" Type="Double" Display="Dynamic"></asp:RangeValidator>
                        </div>
                        <div class="label_gridnavigation">
                            <asp:Label ID="lblPage" runat="server"></asp:Label>of
                            <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                            <asp:Label ID="lblTotRec" runat="server"></asp:Label>record(s)
                        </div>
                    </div>
                </div>
                </asp:Panel>
                <asp:Panel runat="server" ID="PanelSearch">
                    <div class="form_box_title">
                        <div class="form_single">
                            <h4>
                                CARI AKUN</h4>
                        </div>
                    </div>
 
                    <div class="form_box">
                        <div class="form_single">
                            <label>
                                Account Type</label>
                            <asp:DropDownList ID="ddlAccountType" runat="server">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label>
                                Cari Berdasarkan</label>
                            <asp:DropDownList ID="ddlCari" runat="server">
                                <asp:ListItem Text="Account Name" Value="description" />
                                <asp:ListItem Text="Account No" Value="coaid" />
                            </asp:DropDownList>
                            <asp:TextBox ID="txtCari" runat="server" placeholder="Kata kunci pencarian"></asp:TextBox>
                        </div>
                    </div>

                    <div class="form_button">
                        <asp:Button ID="buttonSearch" runat="server" Text="Find" CssClass="small button blue"
                            CausesValidation="false" />
                        <asp:Button ID="buttonReset" runat="server" Text="Reset" CssClass="small button gray"
                            CausesValidation="false"   />
                    </div>
                </asp:Panel>

    </form>

</body>
</html>
