﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CustomerGlobal.aspx.vb" Inherits="Maxiloan.Webform.Utility.CustomerGlobal" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
	<script src="../../js/jquery-1.9.1.min.js"></script>
	<script src="../../js/jquery-ui-1.10.3.custom.min.js"></script>
    <script type="text/javascript">
        function OpenCust(CustomerID, pStyle) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            var x = screen.width;
            var y = screen.height - 100;
            window.open('http://<%= Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.LoanOrg/Credit/ViewPersonalCustomer.aspx?Style=' + pStyle + '&CustomerID=' + CustomerID, 'UserLookup', 'left = 0, top = 0, width = ' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }

        $(document).ready(function () {
            $("#<%= dtgPaging.ClientID %> td > a").click(function (e) {
                e.preventDefault();
                var kode = $(this).parents('tr').find('.kode').html();
                var nama = $(this).parents('tr').find('.nama').html();
                var dataArg_Temp = [
                { name: '<%= KodeClientID %>', value: kode },
                { name: '<%= NamaClientID %>', value: nama }
                ];
                var dataArg = new Array();
                $.each(dataArg_Temp, function (index, value) {
                    dataArg.push({ name: value.name, value: value.value });
                });
                window.parent.BindFromJLookup(dataArg);
                window.parent.CloseJLookup();
            });


        });

    </script>   
</head>

<body>

    <form id="form1" runat="server" class="ui-lookup">
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
                <asp:Panel ID="pnlGrid" runat="server">
                    <div class="form_box_title">
                        <div class="title_strip">
                        </div>
                        <div class="form_single">
                        <h4>
                            DAFTAR CUSTOMER
                        </h4>
                    </div>
                    </div>
                    <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ws">
                            <asp:DataGrid ID="dtgPaging" runat="server" CellPadding="0" OnSortCommand="SortGrid"
                                BackColor="White" BorderWidth="1px" BorderStyle="None" BorderColor="#CCCCCC"
                                DataKeyField="CustomerID" AutoGenerateColumns="False" AllowSorting="True"
                                AllowPaging="True" Width="100%" Visible="True" CssClass="grid_general">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                        <asp:TemplateColumn HeaderText="">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkNewApp" runat="server" CausesValidation="false" Text="New Application"> </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>                             
                                    <asp:TemplateColumn Visible="false">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblGridKode" CssClass="kode" Text='<%#Container.DataItem("CustomerID")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>                                                                         
                                    <asp:TemplateColumn HeaderText="NAMA" SortExpression="Name">
                                        <ItemStyle CssClass="name_col"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkCustomer" runat="server" CssClass="nama"  CausesValidation="false" Text='<%# DataBinder.eval(Container,"DataItem.Name") %>'>
                                            </asp:LinkButton>
                                            <asp:Label ID="lblCust" runat="server" Visible="false" Text='<%# DataBinder.eval(Container,"DataItem.CustomerID") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="CustomerType" ItemStyle-HorizontalAlign="center" SortExpression="CustomerType"
                                        HeaderText="JENIS" ItemStyle-CssClass="short_col"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="Address" SortExpression="Address" HeaderText="ALAMAT">
                                        <ItemStyle CssClass="address_col"></ItemStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="CustomerID" Visible="False" HeaderText="CustomerID">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="BranchFullName" HeaderText="CABANG">
                                    </asp:BoundColumn>                                
                                </Columns>
                                <PagerStyle Visible="False" HorizontalAlign="Left" ForeColor="#000066" BackColor="White"
                                    Mode="NumericPages"></PagerStyle>
                            </asp:DataGrid>
                        </div>
                        <div class="button_gridnavigation">
                            <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton01.png"
                                OnCommand="NavigationLink_Click" CommandName="First"></asp:ImageButton>
                            <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton02.png"
                                OnCommand="NavigationLink_Click" CommandName="Prev"></asp:ImageButton>
                            <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton03.png"
                                OnCommand="NavigationLink_Click" CommandName="Next"></asp:ImageButton>
                            <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton04.png"
                                OnCommand="NavigationLink_Click" CommandName="Last"></asp:ImageButton>
                            Page
                            <asp:TextBox ID="txtGoPage" runat="server" CssClass="small_text">1</asp:TextBox>
                            <asp:Button ID="btnGoPage" runat="server" EnableViewState="False" Text="Go" CssClass="small buttongo blue">
                            </asp:Button>
                            <asp:RangeValidator ID="Rangevalidator1" runat="server" Type="Integer" MaximumValue="999999999"
                                ErrorMessage="Page No. is not valid" MinimumValue="1" ControlToValidate="txtGoPage"
                                Display="Dynamic"></asp:RangeValidator>
                            <asp:RequiredFieldValidator ID="rfvGo" runat="server" ErrorMessage="Page No. is not valid"
                                ControlToValidate="txtGoPage" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                            <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtGoPage" MinimumValue="1"
                                MaximumValue="999" ErrorMessage="Page No. is not valid" Type="Double" Display="Dynamic"></asp:RangeValidator>
                        </div>
                        <div class="label_gridnavigation">
                            <asp:Label ID="lblPage" runat="server"></asp:Label>of
                            <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                            <asp:Label ID="lblTotRec" runat="server"></asp:Label>record(s)
                        </div>
                    </div>
                </div>
                </asp:Panel>
                <asp:Panel runat="server" ID="PanelSearch">
                    <div class="form_box_title">
                        <div class="form_single">
                            <h4>
                                CARI CUSTOMER</h4>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label>
                                Find By</label>
                            <asp:DropDownList ID="cboSearchBy" runat="server">
                                <asp:ListItem Value="Name">Customer Name</asp:ListItem>
                                <asp:ListItem Value="Address">Customer Address</asp:ListItem>
                            </asp:DropDownList>
                            <asp:TextBox ID="txtSearchBy" runat="server"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form_button">
                        <asp:Button ID="buttonSearch" runat="server" Text="Find" CssClass="small button blue"
                            CausesValidation="false" />
                        <asp:Button ID="buttonReset" runat="server" Text="Reset" CssClass="small button gray"
                            CausesValidation="false"   />
                    </div>
                </asp:Panel>

    </form>

</body>
</html>
