﻿Imports System.Data.SqlClient
Imports System.Web.Security
Imports System.Drawing
Imports System.IO
Imports System.Collections.Generic
Imports Maxiloan.Controller

Public Class UploadDokument
    Inherits Maxiloan.Webform.WebBased
    Dim pathFolder As String = ""
    Public Property dok() As String
        Get
            Return ViewState("dok").ToString
        End Get
        Set(value As String)
            ViewState("dok") = value
        End Set
    End Property
    Public Property Cus() As String
        Get
            Return ViewState("Cus").ToString
        End Get
        Set(value As String)
            ViewState("Cus") = value
        End Set
    End Property


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.dok = Request("doc")
        Me.Cus = Request("CustomerID")
        Dim ShowUpload As String = Request("Upload")
        If ShowUpload = "false" Then
            btnUpload.Visible = False
            FileUpload.Visible = False
        End If

        cekImage()
    End Sub
    Private Function pathFile(ByVal Group As String, ByVal CustomerID As String) As String
        Dim strDirectory As String = ""
        strDirectory = getpathFolder() & sesBranchId.Replace("'", "") & "\" & Group & "\" & CustomerID & "\"
        If Not System.IO.Directory.Exists(strDirectory) Then
            System.IO.Directory.CreateDirectory(strDirectory)
        End If
        Return strDirectory
    End Function
    Private Function getpathFolder() As String
        If pathFolder = "" Then
            Dim oClass As New Parameter.GeneralSetting
            Dim oCtr As New GeneralSettingController
            With oClass
                .strConnection = GetConnectionString()
                .GSID = "UploadFolder"
                .ModuleID = "GEN"
            End With
            Try
                Return oCtr.GetGeneralSettingValue(oClass).Trim
            Catch ex As Exception
                Return ""
            End Try
        Else
            Return pathFolder
        End If
    End Function
    Private Function getDownloadPath(filePath As String) As String
        Return Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd("/"c) + "/Webform.Utility/GetImage.aspx?file=" & Server.UrlEncode(getpathFolder() & filePath)
    End Function
    Sub cekImage()
        If File.Exists(pathFile("Dokumen", Me.Cus) + Me.dok + ".jpg") Then
            img.ImageUrl = ResolveClientUrl(getDownloadPath(sesBranchId.Replace("'", "") & "/Dokumen/" + Me.Cus + "/" & Me.dok & ".jpg"))
        Else
            img.ImageUrl = ResolveClientUrl("../../xml/NoFoto.png")
        End If
    End Sub
    Private Sub btnUpload_Click(sender As Object, e As System.EventArgs) Handles btnUpload.Click
        Dim FileName As String = ""
        Dim strExtension As String = ""

        If FileUpload.HasFile Then
            If FileUpload.PostedFile.ContentType = "image/jpeg" Then
                FileName = Me.dok
                strExtension = Path.GetExtension(FileUpload.PostedFile.FileName)
                FileUpload.PostedFile.SaveAs(pathFile("Dokumen", Me.Cus) + FileName + strExtension)
            End If
        End If
        cekImage()
    End Sub
End Class