﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="LookupBankAccountCabang.aspx.vb"
    Inherits="Maxiloan.Webform.Utility.LookupBankAccountCabang" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../js/jquery-1.9.1.min.js"></script>
    <script src="../../js/jquery-ui-1.10.3.custom.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%= dtgPaging.ClientID %> td > a").click(function (e) {

                e.preventDefault();
                var BankAccountID = $(this).parents('tr').find('.BankAccountID').html();
                var BankAccountName = $(this).parents('tr').find('.BankAccountName').html();
                var AccountName = $(this).parents('tr').find('.AccountName').html();
                var AccountNo = $(this).parents('tr').find('.AccountNo').html();
                var BankId = $(this).parents('tr').find('.BankId').html();
                var dataArg_Temp = [
                { name: '<%= BankAccountID %>', value: BankAccountID },
                { name: '<%= BankAccountName %>', value: BankAccountName },
                { name: '<%= AccountName %>', value: AccountName },
                { name: '<%= AccountNo %>', value: AccountNo },
                { name: '<%= BankId %>', value: BankId}
                ];
                var dataArg = new Array();

                $.each(dataArg_Temp, function (index, value) {
                    dataArg.push({ name: value.name, value: value.value });

                });
                window.parent.BindFromJLookup(dataArg);
                window.parent.CloseJLookup();
            });


        });

       
    </script>
  
</head>
<body>
    <form id="form1" runat="server" class="ui-lookup">
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
    <asp:Panel ID="pnlGrid" runat="server">
        <div class="form_box_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h4>
                    DAFTAR BANK ACCOUNT
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ws">
                    <asp:DataGrid ID="dtgPaging" runat="server" AutoGenerateColumns="False" DataKeyField="BankAccountID"
                        Width="100%" CssClass="grid_general" AllowPaging="true" PageSize="10" PagerStyle-Mode="NumericPages">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <PagerStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="Select">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lblSelect" runat="server" Text='Select'>
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Sandi Bank" SortExpression="BankID_">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblGridBankId" CssClass="BankId" Text='<%#Container.DataItem("BankID_") %>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Bank Account Name" SortExpression="BankAccountName">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblGridBankAccountName" CssClass="BankAccountName" Text='<%#Container.DataItem("BankAccountName")%>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Account Name" SortExpression="AccountName">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblGridAccountName" CssClass="AccountName" Text='<%#Container.DataItem("AccountName")%>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="AccountNo" SortExpression="AccountNo_">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblGridAccountNo" CssClass="AccountNo" Text='<%#Container.DataItem("AccountNo_")%>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="" SortExpression="BankAccountID">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblGridNama" CssClass="BankAccountID" Text='<%#Container.DataItem("BankAccountID")%>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                        <PagerStyle Visible="False" HorizontalAlign="Left" ForeColor="#000066" BackColor="White"
                            Mode="NumericPages"></PagerStyle>
                    </asp:DataGrid>
                </div>
                <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton01.png"
                        OnCommand="NavigationLink_Click" CommandName="First"></asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton02.png"
                        OnCommand="NavigationLink_Click" CommandName="Prev"></asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton03.png"
                        OnCommand="NavigationLink_Click" CommandName="Next"></asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton04.png"
                        OnCommand="NavigationLink_Click" CommandName="Last"></asp:ImageButton>
                    Page
                    <asp:TextBox ID="txtGoPage" runat="server" CssClass="small_text">1</asp:TextBox>
                    <asp:Button ID="btnGoPage" runat="server" EnableViewState="False" Text="Go" CssClass="small buttongo blue">
                    </asp:Button>
                    <asp:RangeValidator ID="Rangevalidator1" runat="server" Type="Integer" MaximumValue="999999999"
                        ErrorMessage="Page No. is not valid" MinimumValue="1" ControlToValidate="txtGoPage"
                        Display="Dynamic"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo" runat="server" ErrorMessage="Page No. is not valid"
                        ControlToValidate="txtGoPage" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtGoPage" MinimumValue="1"
                        MaximumValue="999" ErrorMessage="Page No. is not valid" Type="Double" Display="Dynamic"></asp:RangeValidator>
                </div>
                <div class="label_gridnavigation">
                    <asp:Label ID="lblPage" runat="server"></asp:Label>of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                    <asp:Label ID="lblTotRec" runat="server"></asp:Label>record(s)
                </div>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel runat="server" ID="PanelSearch">
        <div class="form_box_title">
            <div class="form_single">
                <h4>
                    CARI BANK ACCOUNT</h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cari Berdasarkan</label>
                <asp:DropDownList ID="cboSearchBy" runat="server">
                    <asp:ListItem Value="BankAccountName">Bank Account Name</asp:ListItem>
                    <asp:ListItem Value="AccountName">Account Name</asp:ListItem>
                    <asp:ListItem Value="AccountNo">Account No</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearch" runat="server" CssClass="long_text"></asp:TextBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="buttonSearch" runat="server" Text="Find" CssClass="small button blue"
                CausesValidation="false" />
            <asp:Button ID="buttonReset" runat="server" Text="Reset" CssClass="small button gray"
                CausesValidation="false" />
        </div>
    </asp:Panel>
    </form>
</body>
</html>
