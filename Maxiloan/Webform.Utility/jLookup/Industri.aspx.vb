﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Imports Maxiloan.General.CommonCacheHelper
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Webform.UserController
#End Region
Public Class Industri
    Inherits Maxiloan.Webform.WebBased
    Private oController As New LookUpZipCodeController
    Private currentPage As Integer = 1
    'Private pageSize As Int16 = 20
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private ZipCodeDS As DataTable
    Protected WithEvents GridNavigator As ucGridNav
#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property

    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property
    Public Property KodeClientID() As String
        Get
            Return CType(ViewState("KodeClientID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("KodeClientID") = Value
        End Set
    End Property
    Public Property NamaClientID() As String
        Get
            Return CType(ViewState("NamaClientID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("NamaClientID") = Value
        End Set
    End Property

    Public Property Header() As String
        Get
            Return CType(ViewState("Header"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Header") = Value
        End Set
    End Property
#End Region
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'txtGoPage.Text = "1"


        AddHandler GridNavigator.PageChanged, AddressOf PageNavigation
        If Not Page.IsPostBack Then
            ' pnlGrid.Visible = False


            Me.KodeClientID = Request("kode")
            Me.NamaClientID = Request("nama")
            Me.Header = Request("header")
            Me.Sort = "kodeindustri"
            'Me.CmdWhere = String.Format(" header='{0}'", Me.Header)
            Me.CmdWhere = ""
            BindGridEntity()
        End If

    End Sub

    Protected Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        currentPage = e.CurrentPage
        BindGridEntity(True)
        GridNavigator.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
    End Sub

    'Public Function GetData(ByVal oCustomClass As Parameter.LookUpTransaction) As Parameter.LookUpTransaction
    '    Dim oReturnValue As New Parameter.LookUpTransaction


    '    Dim params(4) As SqlParameter
    '    params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
    '    params(0).Value = oCustomClass.CurrentPage
    '    params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
    '    params(1).Value = oCustomClass.PageSize
    '    params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
    '    params(2).Value = oCustomClass.WhereCond
    '    params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
    '    params(3).Value = oCustomClass.SortBy
    '    params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
    '    params(4).Direction = ParameterDirection.Output
    '    Try
    '        oReturnValue.ListData = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spPagingTblIndustri", params).Tables(0)
    '        oReturnValue.TotalRecords = CType(params(4).Value, Int64)
    '        Return oReturnValue
    '    Catch ex As Exception
    '        Throw New Exception("Error On DataAccess.GetData")
    '    End Try
    'End Function
    Public Sub BindGridEntity(Optional isFrNav As Boolean = False)


        Dim dtEntity As DataTable = Nothing
        'Dim oCustom As New Parameter.LookUpTransaction



        'pnlGrid.Visible = True
        'oCustom.strConnection = GetConnectionString()
        'oCustom.WhereCond = CmdWhere
        'oCustom.CurrentPage = currentPage
        'oCustom.PageSize = pageSize
        'oCustom.SortBy = Me.Sort
        'oCustom = GetData(oCustom)




        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = currentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = pageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = Me.CmdWhere
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = Me.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)

        params(4).Direction = ParameterDirection.Output
        Try

            Dim oCustom = SqlHelper.ExecuteDataset(GetConnectionString(), CommandType.StoredProcedure, "spPagingTblIndustri", params).Tables(0)

            If Not oCustom Is Nothing Then
                dtEntity = oCustom
                recordCount = CType(params(4).Value, Int64)
            Else
                recordCount = 0
            End If

            dtgPaging.DataSource = dtEntity.DefaultView
            dtgPaging.CurrentPageIndex = 0
            dtgPaging.DataBind()

            If (isFrNav = False) Then
                GridNavigator.Initialize(recordCount, pageSize)
            End If

        Catch ex As Exception
            Throw New Exception("Error On DataAccess.GetData")
        End Try


        'PagingFooter()
    End Sub
    Protected Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGridEntity()
    End Sub
    Protected Sub buttonSearch_Click(ByVal sender As Object, ByVal e As EventArgs) Handles buttonSearch.Click

        If txtSearchBy.Text <> "" Then
            Dim tmptxtSearchBy As String = txtSearchBy.Text.Replace("%", "")
            If cboSearchBy.SelectedItem.Value = "NameIndustri" Then
                Me.CmdWhere = "NameIndustri like '%" + tmptxtSearchBy + "%'"
            Else
                Me.CmdWhere = "KodeIndustri like '%" + tmptxtSearchBy + "%'"
            End If
            'Me.CmdWhere = String.Format(" header='{0}' and {1} like '%{2}%'", Me.Header, cboSearchBy.SelectedItem.Value, tmptxtSearchBy)
        Else
            Me.CmdWhere = ""
        End If

        BindGridEntity()

    End Sub

    '#Region " Navigation "
    '    Private Sub PagingFooter()
    '        lblPage.Text = currentPage.ToString()
    '        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
    '        If totalPages = 0 Then
    '            lblTotPage.Text = "1"
    '            rgvGo.MaximumValue = "1"
    '        Else
    '            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
    '            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
    '        End If
    '        lblTotRec.Text = recordCount.ToString

    '        If currentPage = 1 Then
    '            imbPrevPage.Enabled = False
    '            imbFirstPage.Enabled = False
    '            If totalPages > 1 Then
    '                imbNextPage.Enabled = True
    '                imbLastPage.Enabled = True
    '            Else
    '                imbPrevPage.Enabled = False
    '                imbNextPage.Enabled = False
    '                imbLastPage.Enabled = False
    '                imbFirstPage.Enabled = False
    '            End If
    '        Else
    '            imbPrevPage.Enabled = True
    '            imbFirstPage.Enabled = True
    '            If currentPage = totalPages Then
    '                imbNextPage.Enabled = False
    '                imbLastPage.Enabled = False
    '            Else
    '                imbLastPage.Enabled = True
    '                imbNextPage.Enabled = True
    '            End If
    '        End If
    '    End Sub
    '    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
    '        Select Case e.CommandName
    '            Case "First" : currentPage = 1
    '            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
    '            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
    '            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
    '        End Select
    '        BindGridEntity(Me.CmdWhere)
    '    End Sub
    '    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGoPage.Click
    '        If IsNumeric(txtGoPage.Text) Then
    '            If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
    '                currentPage = CType(txtGoPage.Text, Int32)
    '                BindGridEntity(Me.CmdWhere)
    '            End If
    '        End If
    '    End Sub
    '#End Region

    'Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
    '    'If e.Item.ItemIndex >= 0 Then
    '    '    Dim Kode As String, Nama As String

    '    '    Kode = e.Item.Cells(1).Text.Trim
    '    '    Nama = e.Item.Cells(2).Text.Trim
    '    '    'Dim gKELURAHAN As Label
    '    '    'Dim gKECAMATAN As Label
    '    '    'Dim gCity As Label
    '    '    'Dim gZIPCODE As Label

    '    '    'gKELURAHAN = CType(e.Item.FindControl("gKELURAHAN"), Label)
    '    '    'gKECAMATAN = CType(e.Item.FindControl("gKECAMATAN"), Label)
    '    '    'gCity = CType(e.Item.FindControl("gCity"), Label)
    '    '    'gZIPCODE = CType(e.Item.FindControl("gZIPCODE"), Label)

    '    '    Dim lblSelect As LinkButton
    '    '    lblSelect = CType(e.Item.FindControl("lblSelect"), LinkButton)
    '    '    lblSelect.Attributes.Add("OnClick", "SelectIndustri('" & KodeClientID & "','" & Kode & "','" & NamaClientID & "','" & Nama & "');")
    '    'End If
    'End Sub

    'Private Sub dtgPaging_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtgPaging.SelectedIndexChanged
    '    Dim i As Integer = dtgPaging.SelectedIndex
    '    'Response.Write("<script language='javascript'> parent.$('#<%=txtKelurahan.ClientID%>').val('haiii'); </script>")



    'End Sub



    Private Sub buttonReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles buttonReset.Click
        txtSearchBy.Text = ""
        Me.CmdWhere = ""
        BindGridEntity()
    End Sub
End Class