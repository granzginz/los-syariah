﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="loopupStokOpmaneNo.aspx.vb" Inherits="Maxiloan.Webform.Utility.loopupStokOpmaneNo" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="../../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %>  
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../css/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
	<script src="../../js/jquery-1.9.1.min.js"></script>
	<script src="../../js/jquery-ui-1.10.3.custom.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
        <asp:Panel ID="pnlGrid" runat="server">
                <div class="form_box_title">
                    <div class="title_strip"> </div>
                    <div class="form_single"> <h4> DAFTAR STOK OPNAME
                        </h4>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ws">
                            <asp:DataGrid ID="dtgPaging" runat="server" CellPadding="0"  BackColor="White" BorderWidth="1px" BorderStyle="None" BorderColor="#CCCCCC" DataKeyField="coaid" AutoGenerateColumns="False" AllowSorting="True" Width="100%" Visible="True" CssClass="grid_general">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="Select">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lblSelect" runat="server" Text='Select'  />
                                    </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="BranchOnStatus" HeaderText="Branch" ItemStyle-CssClass="nama" />                
                                    <asp:BoundColumn DataField="StockOpnameNo" HeaderText="Stok Opmane No." ItemStyle-CssClass="nama" />
                                    <asp:BoundColumn DataField="OpnameDate" HeaderText="Tanggal" ItemStyle-CssClass="nama"  DataFormatString='{0:dd/MM/yyyy}'/>
                                </Columns>
                            </asp:DataGrid>
                        </div>
                         <uc2:ucGridNav id="GridNavigator" runat="server"/> 
                    </div> 
                </div>

                </asp:Panel>
                <asp:Panel runat="server" ID="PanelSearch">
                    <div class="form_box_title">
                        <div class="form_single">
                            <h4> CARI STOK OPNAME</h4>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label>TANGGAL OPNAME </label>
                             <asp:TextBox runat="server" ID="txtTglOpname" CssClass="small_text"></asp:TextBox>
                            <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtTglOpname" Format="dd/MM/yyyy" />
                            <br />   
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="Fomat tanggal salah" ValidationExpression="(0[1-9]|[12][0-9]|3[01])/(0[1-9]|1[012])/\d{4}" ControlToValidate="txtTglOpname" SetFocusOnError="true" Display="Dynamic" CssClass="validator_general vleft" />
                            <asp:RequiredFieldValidator ID="rfvRentFinish" runat="server" ErrorMessage="Harap isi tanggal" ControlToValidate="txtTglOpname" CssClass="validator_general vleft" SetFocusOnError="true"></asp:RequiredFieldValidator>
                            
                        </div>
                    </div> 
                    <div class="form_button">
                        <asp:Button ID="buttonSearch" runat="server" Text="Search" CssClass="small button blue"  CausesValidation="false" />
                    </div>
                </asp:Panel> 
    </form>
</body>
</html>
