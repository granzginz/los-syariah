﻿Option Strict On
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.General.CommonVariableHelper
Imports System.Linq

Public Class LookupKodePos
    Inherits Maxiloan.Webform.WebBased

    Private oController As New LookUpZipCodeController
    Private currentPage As Integer = DEFAULT_CURRENT_PAGE
    Private pageSize As Integer = DEFAULT_PAGE_SIZE
    Private currentPageNumber As Int32 = DEFAULT_CURRENT_PAGE
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private ZipCodeDS As DataTable

#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property

    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property
    Public Property KodeClientID() As String
        Get
            Return CType(ViewState("KodeClientID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("KodeClientID") = Value
        End Set
    End Property
    Public Property CityClientID() As String
        Get
            Return CType(ViewState("CityClientID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CityClientID") = Value
        End Set
    End Property

    Public Property KecamatanClientID() As String
        Get
            Return CType(ViewState("KecamatanClientID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("KecamatanClientID") = Value
        End Set
    End Property
    Public Property KelurahanClientID() As String
        Get
            Return CType(ViewState("KelurahanClientID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("KelurahanClientID") = Value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not Me.IsPostBack Then
          
            txtGoPage.Text = "1"
            Me.SortBy = "City ASC"
            If Request("cond") <> "" Then
                Me.SearchBy = Request("cond")
            Else
                Me.SearchBy = "ALL"
            End If

            Dim dttCitySearch As New DataTable

            dttCitySearch = oController.GetCitySearch(GetConnectionString)
            CboCity.DataValueField = "City"
            CboCity.DataTextField = "City"
            CboCity.DataSource = dttCitySearch.DefaultView
            CboCity.DataBind()
            CboCity.Items.Insert(1, "ALL")
            CboCity.Items(1).Value = "ALL"
            CboCity.Items.Insert(0, "Select One")
            CboCity.Items(0).Value = "0"

            Me.KodeClientID = Request("kode")
            Me.CityClientID = Request("city")
            Me.KecamatanClientID = Request("kecamatan")
            Me.KelurahanClientID = Request("kelurahan")
            pnlGrid.Visible = False

        End If
    End Sub

   

    

    Public Sub BindData()


        pnlGrid.Visible = False
    End Sub

    Protected Sub BindGridEntity(ByVal cmdWhere As String, Optional ByVal PageIndex As Integer = 0)
        Dim dttEntity As DataTable = Nothing
        Dim oCustomClass As New Maxiloan.Parameter.LookUpZipCode
        pnlGrid.Visible = True
        oCustomClass = oController.GetListData(GetConnectionString, cmdWhere, currentPage, pageSize, Me.SortBy)

        If Not oCustomClass Is Nothing Then
            dttEntity = oCustomClass.ListData
            recordCount = oCustomClass.TotalRecords
        Else
            recordCount = 0
        End If
        dtgPaging.DataSource = dttEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()
        PagingFooter()

        If recordCount > 0 Then
            pnlGrid.Visible = True
        Else
            pnlGrid.Visible = False
        End If

        'ZipCodeDS = CType(Cache.Item("CACHE_ZIPCODE"), DataTable)

        'If ZipCodeDS Is Nothing Then
        '    Dim dtCache As New DataTable
        '    dtCache = oController.GetListData(GetConnectionString, "ALL", 1, 80000, Me.SortBy).ListData
        '    Me.Cache.Insert("CACHE_ZIPCODE", dtCache, Nothing, DateTime.Now.AddHours(DEFAULT_TIME_DURATION_CACHE_HOUR), TimeSpan.Zero)
        '    ZipCodeDS = CType(Me.Cache.Item("CACHE_ZIPCODE"), DataTable)
        'End If


        'Dim dv As DataView = New DataView(ZipCodeDS)

        'dv.RowFilter = cmdWhere.Replace("ALL", "")

        'dtgPaging.DataSource = dv
        'dtgPaging.CurrentPageIndex = PageIndex
        'dtgPaging.DataBind()

        'pnlGrid.Visible = True
        'Me.mpLookupZipCode.Show()



        'cmdWhere = " WHERE " & cmdWhere.Replace("ALL", " 1 = 1")
        'SqlDataSource1.ConnectionString = Me.GetConnectionString
        'SqlDataSource1.SelectCommand = "SELECT Kelurahan, Kecamatan, City, ZipCode FROM Kelurahan" & cmdWhere

        'dtgPaging.DataSource = SqlDataSource1
        'dtgPaging.CurrentPageIndex = PageIndex
        'dtgPaging.DataBind()

        'pnlGrid.Visible = True
        'Me.mpLookupZipCode.Show()

    End Sub

#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            lblMessage.Text = "Data tidak ditemukan ....."
            lblTotPage.Text = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblTotRec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        txtGoPage.Text = CStr(Me.currentPage)
        BindGridEntity(Me.SearchBy)
    End Sub
    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGoPage.Click
        If IsNumeric(txtGoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtGoPage.Text, Int32)
                BindGridEntity(Me.SearchBy)
            End If
        End If
    End Sub
#End Region

    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonSearch.Click
        Me.SearchBy = "ALL"
        If CboCity.SelectedIndex > 0 Then
            If CboCity.SelectedValue = "ALL" Then
                If txtSearch.Text.Trim <> "" Then
                    'If Right(txtSearch.Text.Trim, 1) = "%" Then
                    '    Me.SearchBy = cboSearchBy.SelectedItem.Value + " like '" + Replace(txtSearch.Text.Trim, "'", "''") + "'"
                    'Else
                    '    Me.SearchBy = cboSearchBy.SelectedItem.Value + " = '" + Replace(txtSearch.Text.Trim, "'", "''") + "'"
                    'End If

                    Me.SearchBy = cboSearchBy.SelectedItem.Value + " LIKE '%" + Replace(txtSearch.Text.Trim, "'", "''") + "%'"
                End If
            Else
                Me.SearchBy = " City = '" & CboCity.SelectedItem.Value & "'"
                If txtSearch.Text.Trim <> "" And cboSearchBy.SelectedValue <> "" Then
                    'If Right(txtSearch.Text.Trim, 1) = "%" Then
                    '    Me.SearchBy &= " And " & cboSearchBy.SelectedItem.Value + " like '" + Replace(txtSearch.Text.Trim, "'", "''") + "'"
                    'Else
                    '    Me.SearchBy &= " And " & cboSearchBy.SelectedItem.Value + " = '" + Replace(txtSearch.Text.Trim, "'", "''") + "'"
                    'End If

                    Me.SearchBy &= " AND " & cboSearchBy.SelectedItem.Value + " LIKE '%" + Replace(txtSearch.Text.Trim, "'", "''") + "%'"

                End If
            End If
            BindGridEntity(Me.SearchBy)

        End If
    End Sub

    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonReset.Click
        CboCity.SelectedIndex = 0
        cboSearchBy.SelectedIndex = 0
        txtSearch.Text = ""
        BindGridEntity("ALL")
        pnlGrid.Visible = False
    End Sub

   

    Private Sub dtgPaging_PageIndexChanged(source As Object, e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dtgPaging.PageIndexChanged
        BindGridEntity(Me.SearchBy, e.NewPageIndex)

    End Sub

    'Private Sub dtgPaging_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtgPaging.SelectedIndexChanged
    '    Dim i As Integer = dtgPaging.SelectedIndex
    '    txtKelurahan.Text = dtgPaging.Items(i).Cells(1).Text.Trim
    '    txtKecamatan.Text = dtgPaging.Items(i).Cells(2).Text.Trim
    '    txtCity.Text = dtgPaging.Items(i).Cells(3).Text.Trim
    '    txtZipCode.Text = dtgPaging.Items(i).Cells(4).Text.Trim
    '    Me.mpLookupZipCode.Hide()
    'End Sub
   
End Class