﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
'Imports Maxiloan.Parameter
Imports Maxiloan.General.CommonCacheHelper
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
#End Region
Public Class MasterAccDetail
    Inherits Maxiloan.Webform.WebBased

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1


#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property

    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property
 
    Public Property KodeClientID() As String
        Get
            Return CType(ViewState("KodeClientID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("KodeClientID") = Value
        End Set
    End Property
    Public Property NamaClientID() As String
        Get
            Return CType(ViewState("NamaClientID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("NamaClientID") = Value
        End Set
    End Property
#End Region
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'txtGoPage.Text = "1"
        If Not Page.IsPostBack Then
            pnlGrid.Style.Add("display", "none")
            Me.KodeClientID = Request("kode")
            Me.NamaClientID = Request("nama")

            Me.Sort = "coaid ASC"

            Dim dtItems As String() = [Enum].GetNames(GetType(EnumMasterAccType))
            Dim itm As System.Web.UI.WebControls.ListItem
            itm = New System.Web.UI.WebControls.ListItem("ALL", "0")
            ddlAccountType.Items.Add(itm)
            For Each dt As String In dtItems
                itm = New System.Web.UI.WebControls.ListItem(dt.Replace("_", " "), CStr([Enum].Parse(GetType(EnumMasterAccType), dt)))
                ddlAccountType.Items.Add(itm)
            Next
        End If
    End Sub

    Public Sub BindGridEntity(ByVal cmdWhere As String)

        Dim p_Class As New Parameter.GeneralPaging
        Dim c_Class As New Controller.GeneralPagingController
        pnlGrid.Style.Add("display", "inherit")
        With p_Class
            .strConnection = GetConnectionString()
            .CurrentPage = currentPage
            .PageSize = pageSize
            .WhereCond = cmdWhere
            .SortBy = Sort
            .SpName = "spPagingMasterAccDetail"
        End With
        p_Class = c_Class.GetGeneralPaging(p_Class)

        If Not p_Class Is Nothing Then
            recordCount = p_Class.TotalRecords
        Else
            recordCount = 0
        End If


        dtgPaging.DataSource = p_Class.ListData.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()
        PagingFooter()
    End Sub

    Protected Sub buttonSearch_Click(ByVal sender As Object, ByVal e As EventArgs) Handles buttonSearch.Click
        Me.CmdWhere = " isleaf=1 and isactive=1 "
        If ddlAccountType.SelectedValue <> "0" Then
            Me.CmdWhere = " and type ='" & ddlAccountType.SelectedValue & "'"
        End If
        If txtCari.Text <> "" Then
            Me.CmdWhere += " and " & ddlCari.SelectedItem.Value + " like '%" + txtCari.Text + "%'"
        End If

        BindGridEntity(Me.CmdWhere)

    End Sub

#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblTotRec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGoPage.Click
        If IsNumeric(txtGoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtGoPage.Text, Int32)
                BindGridEntity(Me.CmdWhere)
            End If
        End If
    End Sub
#End Region

    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            'Dim Kode As String, Nama As String
            Dim lblAccountTypeID As Label
            Dim lblAccountType As Label

            lblAccountTypeID = CType(e.Item.FindControl("lblAccountTypeID"), Label)
            lblAccountType = CType(e.Item.FindControl("lblAccountType"), Label)

            'Kode = e.Item.Cells(1).Text.Trim
            'Nama = e.Item.Cells(2).Text.Trim
            lblAccountType.Text = ddlAccountType.Items.FindByValue(lblAccountTypeID.Text).Text
            'Dim lblSelect As LinkButton
            'lblSelect = CType(e.Item.FindControl("lblSelect"), LinkButton)
            'lblSelect.Attributes.Add("OnClick", "SelectIndustri('" & KodeClientID & "','" & Kode & "','" & NamaClientID & "','" & Nama & "');")
        End If
    End Sub

    Protected Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If


        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub buttonReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles buttonReset.Click
        BindGridEntity(Me.CmdWhere)
    End Sub
End Class