﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="UploadDokument.aspx.vb" Inherits="Maxiloan.Webform.Utility.UploadDokument" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
	<script src="../../js/jquery-1.9.1.min.js"></script>
	<script src="../../js/jquery-ui-1.10.3.custom.min.js"></script>
    <script type="text/javascript">
        function showimagepreview(input, imgClient) {
            if (input.files && input.files[0]) {
                var filerdr = new FileReader();
                filerdr.onload = function (e) {
                    $('#' + imgClient).attr('src', e.target.result);
                }
                filerdr.readAsDataURL(input.files[0]);
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:UpdatePanel ID="updPanel1"  UpdateMode="Always">
            <ContentTemplate>
                <asp:FileUpload ID="FileUpload" runat="server" onchange="showimagepreview(this,'img')"  />
                <asp:Button ID="btnUpload" runat="server" Text="Upload" />
                <div>
                    <asp:Image ID="img" runat="server" Width="700px" Height="400px" />
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="" />
            </Triggers>

        </asp:UpdatePanel>
    </div>
    </form>
</body>
</html>
