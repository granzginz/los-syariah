﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Imports Maxiloan.General.CommonCacheHelper
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
#End Region
Public Class COA
    Inherits Maxiloan.Webform.WebBased
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Public Property KodeCID() As String
        Get
            Return CType(ViewState("KodeCID"), String)
        End Get
        Set(ByVal value As String)
            ViewState("KodeCID") = value
        End Set
    End Property
    Public Property NamaCID() As String
        Get
            Return CType(ViewState("NamaCID"), String)
        End Get
        Set(ByVal value As String)
            ViewState("NamaCID") = value
        End Set
    End Property
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property

    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            pnlGrid.Visible = False
            Me.KodeCID = Request("kode")
            Me.NamaCID = Request("nama")
            Me.CmdWhere = "all"
            Me.Sort = "CoAId"
        End If
    End Sub
    Public Sub BindGridEntity(ByVal cmdWhere As String)
        Dim dtEntity As DataTable = Nothing
        Dim oCOA As New Parameter.Implementasi
        Dim m_controller As New ImplementasiControler
        pnlGrid.Visible = True
        oCOA.strConnection = GetConnectionString()
        oCOA.WhereCond = cmdWhere
        oCOA.CurrentPage = currentPage
        oCOA.PageSize = pageSize
        oCOA.SortBy = Me.Sort
        oCOA = m_controller.GetChartOfAccount(oCOA)

        If Not oCOA Is Nothing Then
            dtEntity = oCOA.Listdata
            recordCount = oCOA.Totalrecords
        Else
            recordCount = 0
        End If

        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()
        PagingFooter()
    End Sub
#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblTotRec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGoPage.Click
        If IsNumeric(txtGoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtGoPage.Text, Int32)
                BindGridEntity(Me.CmdWhere)
            End If
        End If
    End Sub
#End Region

    Private Sub btnSearch_Click(sender As Object, e As System.EventArgs) Handles buttonSearch.Click

        If txtCari.Text <> "" Then
            Me.CmdWhere = ddlCari.SelectedItem.Value + " like '%" + txtCari.Text.Replace("%", "") + "%'"
        End If

        BindGridEntity(Me.CmdWhere)
    End Sub
End Class