﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="LookupKodePos.aspx.vb"
    Inherits="Maxiloan.Webform.Utility.LookupKodePos" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../js/jquery-1.9.1.min.js"></script>
    <script src="../../js/jquery-ui-1.10.3.custom.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%= dtgPaging.ClientID %> td > a").click(function (e) {

                e.preventDefault();
                var kode = $(this).parents('tr').find('.kode').html();
                var city = $(this).parents('tr').find('.city').html();
                var kecamatan = $(this).parents('tr').find('.kecamatan').html();
                var kelurahan = $(this).parents('tr').find('.kelurahan').html();
                var dataArg_Temp = [
                { name: '<%= KodeClientID %>', value: kode },
                { name: '<%= CityClientID %>', value: city },
                { name: '<%= KecamatanClientID %>', value: kecamatan },
                { name: '<%= KelurahanClientID %>', value: kelurahan }
                ];
                var dataArg = new Array();

                $.each(dataArg_Temp, function (index, value) {
                    dataArg.push({ name: value.name, value: value.value });

                });
                window.parent.BindFromJLookup(dataArg);
                window.parent.CloseJLookup();
            });


        });

       
    </script>
  
</head>
<body>
    <form id="form1" runat="server" class="ui-lookup">
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
    <asp:Panel ID="pnlGrid" runat="server">
        <div class="form_box_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h4>
                    DAFTAR KODE POS
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ws">
                    <asp:DataGrid ID="dtgPaging" runat="server" AutoGenerateColumns="False" DataKeyField="ZIPCODE"
                        Width="100%" CssClass="grid_general" AllowPaging="true" PageSize="10" PagerStyle-Mode="NumericPages">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <PagerStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="Select">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lblSelect" runat="server" Text='Select'>
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="KELURAHAN" SortExpression="Kelurahan">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblGridKelurahan" CssClass="kelurahan" Text='<%#Container.DataItem("KELURAHAN")%>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="KECAMATAN" SortExpression="Kecamatan">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblGridKecamatan" CssClass="kecamatan" Text='<%#Container.DataItem("KECAMATAN")%>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="CITY" SortExpression="City">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblGridCity" CssClass="city" Text='<%#Container.DataItem("CITY")%>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="ZIP CODE" SortExpression="ZipCode">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblGridNama" CssClass="kode" Text='<%#Container.DataItem("ZIPCODE")%>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                        <PagerStyle Visible="False" HorizontalAlign="Left" ForeColor="#000066" BackColor="White"
                            Mode="NumericPages"></PagerStyle>
                    </asp:DataGrid>
                </div>
                <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton01.png"
                        OnCommand="NavigationLink_Click" CommandName="First"></asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton02.png"
                        OnCommand="NavigationLink_Click" CommandName="Prev"></asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton03.png"
                        OnCommand="NavigationLink_Click" CommandName="Next"></asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton04.png"
                        OnCommand="NavigationLink_Click" CommandName="Last"></asp:ImageButton>
                    Page
                    <asp:TextBox ID="txtGoPage" runat="server" CssClass="small_text">1</asp:TextBox>
                    <asp:Button ID="btnGoPage" runat="server" EnableViewState="False" Text="Go" CssClass="small buttongo blue">
                    </asp:Button>
                    <asp:RangeValidator ID="Rangevalidator1" runat="server" Type="Integer" MaximumValue="999999999"
                        ErrorMessage="Page No. is not valid" MinimumValue="1" ControlToValidate="txtGoPage"
                        Display="Dynamic"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo" runat="server" ErrorMessage="Page No. is not valid"
                        ControlToValidate="txtGoPage" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtGoPage" MinimumValue="1"
                        MaximumValue="999" ErrorMessage="Page No. is not valid" Type="Double" Display="Dynamic"></asp:RangeValidator>
                </div>
                <div class="label_gridnavigation">
                    <asp:Label ID="lblPage" runat="server"></asp:Label>of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                    <asp:Label ID="lblTotRec" runat="server"></asp:Label>record(s)
                </div>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel runat="server" ID="PanelSearch">
        <div class="form_box_title">
            <div class="form_single">
                <h4>
                    CARI KODE POS</h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Kota</label>
                <asp:DropDownList ID="CboCity" runat="server">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Please Choose City"
                    Display="Dynamic" ControlToValidate="CboCity" Enabled="False" InitialValue="0"
                    CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cari Berdasarkan</label>
                <asp:DropDownList ID="cboSearchBy" runat="server">
                    <asp:ListItem Value=""></asp:ListItem>
                    <asp:ListItem Value="Kelurahan">Kelurahan</asp:ListItem>
                    <asp:ListItem Value="Kecamatan">Kecamatan</asp:ListItem>
                    <asp:ListItem Value="ZipCode">Zip Code</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="buttonSearch" runat="server" Text="Find" CssClass="small button blue"
                CausesValidation="false" />
            <asp:Button ID="buttonReset" runat="server" Text="Reset" CssClass="small button gray"
                CausesValidation="false" />
        </div>
    </asp:Panel>
    </form>
</body>
</html>
