﻿Imports Maxiloan.Parameter.GL
Imports Maxiloan.Controller
'Imports Maxiloan.Parameter
Imports Maxiloan.Webform.UserController

Public Class loopupStokOpmaneNo
    Inherits WebBased

    Public Property KodeCID() As String
        Get
            Return CType(ViewState("kodeCID"), String)
        End Get
        Set(ByVal value As String)
            ViewState("kodeCID") = value
        End Set
    End Property
    Public Property NamaCID() As String
        Get
            Return CType(ViewState("namaCID"), String)
        End Get
        Set(ByVal value As String)
            ViewState("namaCID") = value
        End Set
    End Property

    Private pageSize As Int16 = 10
    Private _recordCount As Integer = 1
    Protected WithEvents GridNavigator As ucGridNav

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        AddHandler GridNavigator.PageChanged, AddressOf Navevent
        BranchID = sesBranchId.Replace("'", "")

        If Not Page.IsPostBack Then
            KodeCID = Request("kodeCID")
            NamaCID = Request("namaCID")
            txtTglOpname.Text = BusinessDate.ToString("dd/MM/yyyy")
        End If

    End Sub
    Private Sub Navevent(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        BindGridFind(e.CurrentPage)
    End Sub
    Sub BindGridFind(currPage As Integer)
         

        'Dim opW = ddlCari.SelectedValue
        'Dim sWhare = txtCari.Text.Trim

        'Dim _dt As IList(Of MasterAccountObject) = _coaController.SelectData(_recordCount, GetConnectionString(), True, isDetail, New Object() {SesCompanyID, BranchID, currPage, pageSize, acctype, opW, sWhare})
        'dtgPaging.DataSource = _dt
        'dtgPaging.DataBind()
    End Sub
End Class