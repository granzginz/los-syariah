﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Imports Maxiloan.General.CommonCacheHelper
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
#End Region
Public Class CustomerGlobal
    Inherits Maxiloan.Webform.WebBased
    Private oController As New LookUpZipCodeController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 5
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property

    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property
    Public Property KodeClientID() As String
        Get
            Return CType(ViewState("KodeClientID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("KodeClientID") = Value
        End Set
    End Property
    Public Property NamaClientID() As String
        Get
            Return CType(ViewState("NamaClientID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("NamaClientID") = Value
        End Set
    End Property
#End Region
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'txtGoPage.Text = "1"
        If Not Page.IsPostBack Then
            pnlGrid.Visible = False
            Me.KodeClientID = Request("kode")
            Me.NamaClientID = Request("nama")
            Me.CmdWhere = "all"
            Me.Sort = "Name"
        End If

    End Sub
    
    Public Sub BindGridEntity(ByVal cmdWhere As String)
        Dim dtEntity As DataTable = Nothing
        Dim oCustomer As New Parameter.Customer
        Dim m_controller As New CustomerController
        pnlGrid.Visible = True
        oCustomer.strConnection = GetConnectionString()
        oCustomer.WhereCond = cmdWhere
        oCustomer.CurrentPage = currentPage
        oCustomer.PageSize = pageSize
        oCustomer.SortBy = Me.Sort
        oCustomer = m_controller.GetCustomer(oCustomer)

        If Not oCustomer Is Nothing Then
            dtEntity = oCustomer.listdata
            recordCount = oCustomer.totalrecords
        Else
            recordCount = 0
        End If

        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()
        PagingFooter()
    End Sub

    Protected Sub buttonSearch_Click(ByVal sender As Object, ByVal e As EventArgs) Handles buttonSearch.Click

        If txtSearchBy.Text <> "" Then
            Me.CmdWhere = cboSearchBy.SelectedItem.Value + " like '%" + txtSearchBy.Text.Replace("%", "") + "%'"
        End If

        BindGridEntity(Me.CmdWhere)

    End Sub

#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblTotRec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGoPage.Click
        If IsNumeric(txtGoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtGoPage.Text, Int32)
                BindGridEntity(Me.CmdWhere)
            End If
        End If
    End Sub
#End Region

    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim strHTTPServer As String
            Dim StrHTTPApp As String
            Dim strNameServer As String
            strHTTPServer = Request.ServerVariables("PATH_INFO")
            strNameServer = Request.ServerVariables("SERVER_NAME")
            StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)


            Dim lnkCustomer As LinkButton
            Dim lblCust As Label

            Dim lnkNewApp As LinkButton

            lnkCustomer = CType(e.Item.FindControl("lnkCustomer"), LinkButton)
            lblCust = CType(e.Item.FindControl("lblCust"), Label)
            lnkNewApp = CType(e.Item.FindControl("lnkNewApp"), LinkButton)

            lnkCustomer.Attributes.Add("OnClick", "return OpenCust('" & lblCust.Text & "','accacq');")
            lnkNewApp.Attributes.Add("OnClick", "return parent.window.location.href='http://" & strNameServer & "/" & StrHTTPApp & "/Webform.LoanOrg/Credit/CreditProcess/NewApplication/Application_003.aspx?id=" & lblCust.Text.Trim & "&name=" & lnkCustomer.Text.Trim & "&type=P&page=Add&flag=0'")
        End If
    End Sub

    Protected Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If


        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub buttonReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles buttonReset.Click
        BindGridEntity(Me.CmdWhere)
    End Sub
End Class