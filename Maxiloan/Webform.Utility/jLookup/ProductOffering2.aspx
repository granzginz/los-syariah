﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ProductOffering2.aspx.vb"
    Inherits="Maxiloan.Webform.Utility.ProductOffering2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../js/jquery-1.9.1.min.js"></script>
    <script src="../../js/jquery-ui-1.10.3.custom.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%= dtgPaging.ClientID %> td > a").click(function (e) {

                e.preventDefault();
                var productID = $(this).parents('tr').find('.productID').html();
                var productOfferingID = $(this).parents('tr').find('.productOfferingID').html();
                var assetTypeID = $(this).parents('tr').find('.assetTypeID').html();
                var desc = $(this).parents('tr').find('.desc').html();
                var KegiatanUsahaID = $(this).parents('tr').find('.KegiatanUsahaID').html();
                var JenisPembiayaanID = $(this).parents('tr').find('.JenisPembiayaanID').html();
                var KegiatanUsaha = $(this).parents('tr').find('.KegiatanUsaha').html();
                var JenisPembiayaan = $(this).parents('tr').find('.JenisPembiayaan').html();

                var dataArg_Temp = [
                { name: '<%= ProductClientID %>', value: productID },
                { name: '<%= ProductOfferingClientID %>', value: productOfferingID },
                { name: '<%= AssetTypeClientID %>', value: assetTypeID },
                { name: '<%= DescClientID %>', value: desc },
                { name: '<%= KegiatanUsahaIDClientID %>', value: KegiatanUsahaID },
                { name: '<%= JenisPembiayaanIDClientID %>', value: JenisPembiayaanID },
                { name: '<%= KegiatanUsahaClientID %>', value: KegiatanUsaha },
                { name: '<%= JenisPembiayaanClientID %>', value: JenisPembiayaan }
                ];
                var dataArg = new Array();

                $.each(dataArg_Temp, function (index, value) {
                    dataArg.push({ name: value.name, value: value.value });

                });
                window.parent.BindFromJLookup(dataArg);
                window.parent.CloseJLookup();
            });


        });

       
    </script>
</head>
<body>
    <form id="form1" runat="server" class="ui-lookup">
  
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
        <asp:Panel ID="pnlGrid" runat="server">
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h4>
                        DAFTAR PRODUCT OFFERING
                    </h4>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ws">
                        <asp:DataGrid ID="dtgPaging" runat="server" Width="100%" AllowPaging="True" AllowSorting="True"
                            AutoGenerateColumns="False" DataKeyField="productofferingid" BorderStyle="None"
                            BorderWidth="1px" OnSortCommand="SortGrid" CellPadding="0" CssClass="grid_general">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <Columns>
                                <asp:TemplateColumn HeaderText="Select">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lblSelect" runat="server" Text='Select'>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="PRODUCT ID">
                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblProductID" CssClass="productID" runat="server" Text='<%#Container.DataItem("ProductID")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="PRODUCT OFFERING">
                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblProductOfferingID" CssClass="productOfferingID" runat="server"
                                            Text='<%#Container.DataItem("ProductOfferingId")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="description" HeaderText="DESCRIPTION">
                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblDescription" CssClass="desc" runat="server" Text='<%#Container.DataItem("Description")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>                                
                                <asp:TemplateColumn HeaderText="AssetTypeID" Visible="False">
                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblAssetTypeID" CssClass="assetTypeID" runat="server" Text='<%#Container.DataItem("AssetTypeID")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="KegiatanUsahaID">
                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblKegiatanUsahaID" CssClass="KegiatanUsahaID" runat="server" Text='<%#Container.DataItem("KegiatanUsahaID")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="JenisPembiayaanID">
                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblJenisPembiayaanID" CssClass="JenisPembiayaanID" runat="server" Text='<%#Container.DataItem("JenisPembiayaanID")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="KegiatanUsaha" >
                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblKegiatanUsaha" CssClass="KegiatanUsaha" runat="server" Text='<%#Container.DataItem("KegiatanUsaha")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="JenisPembiayaan" >
                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblJenisPembiayaan" CssClass="JenisPembiayaan" runat="server" Text='<%#Container.DataItem("JenisPembiayaan")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                            <PagerStyle Visible="False" HorizontalAlign="Left" ForeColor="#000066" BackColor="White"
                                Mode="NumericPages"></PagerStyle>
                        </asp:DataGrid>
                    </div>
                    <div class="button_gridnavigation">
                        <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                            CausesValidation="False" CommandName="First" OnCommand="NavigationLink_Click">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                            CausesValidation="False" CommandName="Prev" OnCommand="NavigationLink_Click">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                            CausesValidation="False" CommandName="Next" OnCommand="NavigationLink_Click">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                            CausesValidation="False" CommandName="Last" OnCommand="NavigationLink_Click">
                        </asp:ImageButton>
                        Page
                        <asp:TextBox ID="txtGoPage" runat="server" Width="34px" CssClass="txtPage">1</asp:TextBox>
                        <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtGoPage" MinimumValue="1"
                            MaximumValue="999" ErrorMessage="Page No. is not valid" Type="Double" Display="Dynamic"></asp:RangeValidator>
                        <asp:Button ID="btnGoPage" runat="server" EnableViewState="False" Text="Go" CssClass="small buttongo blue" />
                        <asp:RangeValidator ID="Rangevalidator1" runat="server" Type="Integer" MaximumValue="999999999"
                            ErrorMessage="Page No. is not valid" MinimumValue="1" ControlToValidate="txtGoPage"
                            Display="Dynamic"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="rfvGo" runat="server" ErrorMessage="Page No. is not valid"
                            ControlToValidate="txtGoPage" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                    <div class="navigasi_totalrecord">
                        <asp:Label ID="lblPage" runat="server"></asp:Label>of
                        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                        <asp:Label ID="lblTotRec" runat="server"></asp:Label>record(s)
                    </div>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel runat="server" ID="PanelSearch">
        <div class="form_box_title">
               <div class="form_single">
                    <h4>
                        CARI PRODUCT OFFERING</h4>
                </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Product</label>
                <asp:DropDownList ID="cboProductBranch" runat="server">
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSearch" runat="server" Text="Find" CssClass="small button blue"
                CausesValidation="False" />
            <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="small button gray"
                CausesValidation="False" />
        </div>
   
    </asp:Panel>
    </form>
</body>
</html>
