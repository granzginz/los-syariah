﻿Option Strict On
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.General.CommonVariableHelper
Imports System.Linq

Public Class LookupBankAccountOther
    Inherits Maxiloan.Webform.WebBased
    Private oController As New LookUpZipCodeController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property

    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property
    Public Property BankAccountName() As String
        Get
            Return CType(ViewState("BankAccountName"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("BankAccountName") = Value
        End Set
    End Property
    Public Property AccountName() As String
        Get
            Return CType(ViewState("AccountName"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("AccountName") = Value
        End Set
    End Property
    Public Property BankAccountID() As String
        Get
            Return CType(ViewState("BankAccountID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("BankAccountID") = Value
        End Set
    End Property
    Public Property AccountNo() As String
        Get
            Return CType(ViewState("AccountNo"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("AccountNo") = Value
        End Set
    End Property
    Public Property BankId() As String
        Get
            Return (CType(ViewState("BankId"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("BankId") = Value
        End Set
    End Property

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not Me.IsPostBack Then
            Dim bank As String = "014"
            txtGoPage.Text = "1"
            Me.SortBy = "BranchID ASC"
            Me.CmdWhere = "ALL"

            Me.BankAccountID = Request("BankAccountID")
            Me.BankAccountName = Request("BankAccountName")
            Me.AccountName = Request("AccountName")
            Me.AccountNo = Request("AccountNo")
            Me.BankId = Request("BankId")

            pnlGrid.Visible = False

        End If
    End Sub
    Public Sub BindData()
        pnlGrid.Visible = False
    End Sub

    Protected Sub BindGridEntity(ByVal cmdWhere As String)
        Dim dtEntity As DataTable = Nothing
        Dim oBankAccount As New Parameter.PaymentRequest
        Dim m_controller As New PaymentRequestController
        pnlGrid.Visible = True
        oBankAccount.strConnection = GetConnectionString()
        oBankAccount.WhereCond = cmdWhere
        oBankAccount.CurrentPage = currentPage
        oBankAccount.PageSize = pageSize
        oBankAccount.SortBy = Me.Sort
        oBankAccount.BranchId = Me.sesBranchId.Replace("'", "")
        oBankAccount = m_controller.GetBankAccountOther(oBankAccount)

        If Not oBankAccount Is Nothing Then
            dtEntity = oBankAccount.ListData
            recordCount = oBankAccount.TotalRecords
        Else
            recordCount = 0
        End If

        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()
        PagingFooter()
    End Sub

#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            lblMessage.Text = "Data tidak ditemukan ....."
            lblTotPage.Text = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblTotRec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        txtGoPage.Text = CStr(Me.currentPage)
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGoPage.Click
        If IsNumeric(txtGoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtGoPage.Text, Int32)
                BindGridEntity(Me.CmdWhere)
            End If
        End If
    End Sub
#End Region

    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonSearch.Click
        If txtSearch.Text <> "" Then
            Me.CmdWhere = cboSearchBy.SelectedItem.Value + " like '%" + txtSearch.Text.Replace("%", "") + "%'"
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub

    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonReset.Click
        cboSearchBy.SelectedIndex = 0
        txtSearch.Text = ""
        BindGridEntity("ALL")
        pnlGrid.Visible = False
    End Sub


End Class