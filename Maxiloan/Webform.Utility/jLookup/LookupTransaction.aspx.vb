﻿#Region "Imports"
Option Strict On
Imports Maxiloan.Controller
Imports Maxiloan.General.CommonVariableHelper
#End Region
Public Class LookUpTransaction
    Inherits Maxiloan.Webform.WebBased

    Private m_controller As New LookUpTransactionController
    Private currentPage As Integer = 1
    Private pageSize As Integer = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private _IsAgreement As String
    Private _IsPettyCash As String
    Private _IsHOTransaction As String
    Private _IsPaymentReceive As String

    Public Property Style() As String
        Get
            Return CStr(viewstate("style"))
        End Get
        Set(ByVal Value As String)
            viewstate("style") = Value
        End Set
    End Property
    Public Property PaymentAllocationID() As String
        Get
            Return CType(ViewState("PaymentAllocationID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("PaymentAllocationID") = Value
        End Set
    End Property

    Public Property Description() As String
        Get
            Return CType(ViewState("Description"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Description") = Value
        End Set
    End Property

    Public Property COA() As String
        Get
            Return CType(ViewState("COA"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("COA") = Value
        End Set
    End Property


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        lblMessage.Text = ""

        'request flag for query 

        _IsAgreement = CType(Request("IsAgreement"), String)
        _IsPettyCash = CType(Request("IsPettyCash"), String)
        _IsHOTransaction = CType(Request("IsHOTransaction"), String)
        _IsPaymentReceive = CType(Request("IsPaymentReceive"), String)

        If sessioninvalid() Then
            Exit Sub
        End If
        If Not Me.IsPostBack Then
            txtGoPage.Text = "1"
            Me.SortBy = "PaymentAllocationID ASC"
            Me.PaymentAllocationID = Request("transactionID")
            Me.Description = Request("transaction")
            Me.COA = "oTrans_hdnCOA" 'Request("COA")
            'If Request("IsAgreement") <> "" And Request("IsPettyCash") <> "" And Request("IsHOTransaction") <> "" And Request("IsPaymentReceive") <> "" Then
            If Request("IsAgreement") <> "" Or Request("IsPettyCash") <> "" Or Request("IsHOTransaction") <> "" Or Request("IsPaymentReceive") <> "" Then
                Me.SearchBy = CreateSqlSyntax()
            Else
                Me.SearchBy = "ALL"
            End If

            'fill lblProcessId with data
            Dim strProcessId As String
            strProcessId = Request("ProcessID")
            lblProcessId.Text = m_controller.GetProcessID(GetConnectionString, strProcessId)
            BindGridEntity(Me.SearchBy)
        End If

    End Sub
    Sub BindGridEntity(ByVal cmdWhere As String)
        Dim dttEntity As DataTable = Nothing
        Dim oCustomClass As New Maxiloan.Parameter.LookUpTransaction

        oCustomClass = m_controller.GetListData(GetConnectionString, cmdWhere, currentPage, pageSize, Me.SortBy)

        If Not oCustomClass Is Nothing Then
            dttEntity = oCustomClass.ListData
            recordCount = oCustomClass.TotalRecords
        Else
            recordCount = 0
        End If

        dtgPaging.DataSource = dttEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()
        PagingFooter()

       

    End Sub

#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            lblMessage.Text = "Data tidak ditemukan ....."
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblTotRec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridEntity(Me.SearchBy)
    End Sub
    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGoPage.Click
        If IsNumeric(txtGoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtGoPage.Text, Int32)
                BindGridEntity(Me.SearchBy)
            End If
        End If
    End Sub
#End Region

    Private Function CreateSqlSyntax() As String
        Dim strSql As New StringBuilder
        If _IsAgreement <> "" Then
            strSql.Append(" IsAgreement = ")
            strSql.Append("'" & _IsAgreement & "' ")
        End If
        If _IsPettyCash <> "" Then
            If strSql.ToString = "" Then
                If _IsAgreement = "" Then
                    strSql.Append(" IsPettyCash = ")
                    strSql.Append("'" & _IsPettyCash & "' ")
                Else
                    strSql.Append(" And IsPettyCash = ")
                    strSql.Append("'" & _IsPettyCash & "' ")
                End If
            End If
            strSql.Append(" And IsPettyCash = ")
            strSql.Append("'" & _IsPettyCash & "' ")
        End If
        If _IsHOTransaction <> "" Then
            If strSql.ToString = "" Then
                If _IsAgreement = "" And _IsPettyCash = "" Then
                    strSql.Append(" IsHOTransaction = ")
                    strSql.Append("'" & _IsHOTransaction & "'")
                Else
                    strSql.Append(" And IsHOTransaction = ")
                    strSql.Append("'" & _IsHOTransaction & "'")
                End If
            End If
        End If
        If _IsPaymentReceive <> "" Then
            If strSql.ToString = "" Then
                strSql.Append(" IsPaymentReceive = ")
                strSql.Append("'" & _IsPaymentReceive & "'")
            Else
                strSql.Append(" And IsPaymentReceive = ")
                strSql.Append("'" & _IsPaymentReceive & "'")
            End If
        End If
        If strSql.ToString = "" Then
            strSql.Append(" IsSystem = ")
            strSql.Append("'0'")
        Else
            strSql.Append(" And IsSystem = ")
            strSql.Append("'0'")
        End If
        Return strSql.ToString()
    End Function

    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonSearch.Click
        If txtSearch.Text.Trim <> "" Then
            'If (CreateSqlSyntax() <> "") Then
            '    Me.SearchBy = CreateSqlSyntax() & " And " & cboSearchBy.SelectedItem.Value + " LIKE '%" + Replace(txtSearch.Text.Trim, "'", "''") + "%'"
            'Else

            '    Me.SearchBy = cboSearchBy.SelectedItem.Value + " LIKE '%" + Replace(txtSearch.Text.Trim, "'", "''") + "%'"
            'End If

            Me.SearchBy = "IsAgreement = '0' AND isSystem = '0' and " + cboSearchBy.SelectedItem.Value + " LIKE '%" + Replace(txtSearch.Text.Trim, "'", "''") + "%'"

            'Me.CmdWhere = cboSearchBy.SelectedItem.Value + " = '" + Replace(txtSearch.Text.Trim, "'", "''") + "'"
        Else
            Me.SearchBy = "IsAgreement = '0' AND isSystem = '0'  " 'CreateSqlSyntax()
        End If
        BindGridEntity(Me.SearchBy)
    End Sub

    Public Shared Function EncodeSQLBooleanValue(ByVal sValue As Boolean) As Byte
        If (sValue) Then
            Return 1
        Else
            Return 0
        End If
    End Function

    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        cboSearchBy.SelectedIndex = 0
        txtSearch.Text = ""
        'BindGridEntity("ALL")
    End Sub

 

    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.SearchBy)
    End Sub
End Class