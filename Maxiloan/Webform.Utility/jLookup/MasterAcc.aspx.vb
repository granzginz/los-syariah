﻿#Region "Imports" 
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
'Imports Maxiloan.Parameter
Imports Maxiloan.Webform.UserController 

#End Region
Public Class MasterAcc
    Inherits WebBased 
    Private pageSize As Int16 = 10 
    Private _recordCount As Integer = 1
    Protected WithEvents GridNavigator As ucGridNav
    Private ReadOnly _coaController As New COAController
#Region "Property"
 
    Public Property KodeCID() As String
        Get
            Return CType(ViewState("kodeCID"), String)
        End Get
        Set(ByVal value As String)
            ViewState("kodeCID") = value
        End Set
    End Property
    Public Property NamaCID() As String
        Get
            Return CType(ViewState("namaCID"), String)
        End Get
        Set(ByVal value As String)
            ViewState("namaCID") = value
        End Set
    End Property
    Public Property AddFree() As String
        Get
            Return CType(ViewState("AddFree"), String)
        End Get
        Set(ByVal value As String)
            ViewState("AddFree") = value
        End Set
    End Property
    Public Property AddFreeDesc() As String
        Get
            Return CType(ViewState("AddFreeDesc"), String)
        End Get
        Set(ByVal value As String)
            ViewState("AddFreeDesc") = value
        End Set
    End Property
    Property isDetail As Boolean
        Set(ByVal value As Boolean)
            ViewState("isDetail") = value
        End Set
        Get
            Return CBool(ViewState("isDetail"))
        End Get
    End Property
#End Region
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'txtGoPage.Text = "1"
        AddHandler GridNavigator.PageChanged, AddressOf Navevent
        If (BranchID = "") Then
            BranchID = sesBranchId.Replace("'", "")
        End If

        If Not Page.IsPostBack Then 
            KodeCID = Request("kodeCID")
            NamaCID = Request("namaCID")
            AddFree = Request("AddFree")
            AddFreeDesc = Request("AddFreeDesc")
            isDetail = False
            Dim reqLeft = Request("isDt")

            If (String.IsNullOrEmpty(reqLeft)) Then
                ddlAccountType.Visible = False
                hdnTypeId.Value = Request("tp")
                lblTypeName.Text = Request("tpn")
            Else
                lblTypeName.Visible = False
                isDetail = True
                With ddlAccountType
                    .DataValueField = "Value"
                    .DataTextField = "Text"
                    .DataSource = ModuleGlHelper.GetEnumMasterAccTypeList()
                    .DataBind()
                End With
            End If

            pnlGrid.Style.Add("display", "none")

        End If
    End Sub


    Private Sub Navevent(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        BindGridFind(e.CurrentPage)
    End Sub
    

    Sub BindGridFind(currPage As Integer)

        Dim acctype
        If (ddlAccountType.Visible) Then
            acctype = ddlAccountType.SelectedValue
        Else
            acctype = hdnTypeId.Value
        End If
       
        Dim opW = ddlCari.SelectedValue
        Dim sWhare = txtCari.Text.Trim

        Dim _dt As IList(Of MasterAccountObject) = _coaController.SelectData(_recordCount, GetConnectionString(), True, isDetail, New Object() {SesCompanyID, BranchID, currPage, pageSize, acctype, opW, sWhare})
        dtgPaging.DataSource = _dt
        dtgPaging.DataBind()
    End Sub
    Private Sub btnSearch_Click(sender As Object, e As System.EventArgs) Handles buttonSearch.Click

        Const alwayFirstPage As Integer = 1
        BindGridFind(alwayFirstPage)
        GridNavigator.Initialize(_recordCount, pageSize)
        pnlGrid.Style.Add("display", "inherit")
    End Sub
     
End Class