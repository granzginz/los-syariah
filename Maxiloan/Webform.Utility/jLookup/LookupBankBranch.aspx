﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="LookupBankBranch.aspx.vb"
    Inherits="Maxiloan.Webform.Utility.LookupBankBranch" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../js/jquery-1.9.1.min.js"></script>
    <script src="../../js/jquery-ui-1.10.3.custom.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%= dtgPaging.ClientID %> td > a").click(function (e) {

                e.preventDefault();
                var transactionID = $(this).parents('tr').find('.transactionID').html();
                var transaction = $(this).parents('tr').find('.transaction').html();
                var dataArg_Temp = [
                { name: '<%= PaymentAllocationID %>', value: transactionID },
                { name: '<%= Description %>', value: transaction }
                ];
                var dataArg = new Array();

                $.each(dataArg_Temp, function (index, value) {
                    dataArg.push({ name: value.name, value: value.value });

                });
                window.parent.BindFromJLookup(dataArg);
                window.parent.CloseJLookup();
            });


        });

       
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div class="wp">
        <asp:Label ID="lblMessage" Visible="false" runat="server"></asp:Label>
      
        <div class="form_box_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h4>
                    DAFTAR BANK
                </h4>
            </div>
        </div>
        <asp:Panel ID="pnlList" runat="server">
            <asp:Panel ID="pnlSubList" runat="server" Visible="false">
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtgPaging" runat="server" CellPadding="0" OnSortCommand="SortGrid"
                                BorderStyle="None" DataKeyField="BankBranchID" AutoGenerateColumns="False" AllowSorting="True"
                                AllowPaging="True" Width="100%" CssClass="grid_general">
                                <ItemStyle CssClass="item_grid"></ItemStyle>
                                <HeaderStyle CssClass="th"></HeaderStyle>
                                <Columns>
                                    <asp:ButtonColumn Text="Select" CommandName="Select" ButtonType="LinkButton"></asp:ButtonColumn>
                                    <asp:BoundColumn ReadOnly="True" DataField="BankCode" SortExpression="BankCode" HeaderText="Bank Code">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn ReadOnly="True" DataField="BankBranchName" SortExpression="BankBranchName"
                                        HeaderText="Bank Branch Name"></asp:BoundColumn>
                                    <asp:BoundColumn ReadOnly="True" DataField="Address" SortExpression="Address" HeaderText="Address">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn ReadOnly="True" DataField="City" SortExpression="City" HeaderText="City">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn ReadOnly="True" DataField="BankID" Visible="False"></asp:BoundColumn>
                                </Columns>
                                <%--  <Columns>
                                <asp:TemplateColumn HeaderText="Select">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lblSelect" runat="server" Text='Select'>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                       <asp:TemplateColumn HeaderText="Payment Allocation">
                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblBankCode" CssClass="bankCodeBank" runat="server"
                                            Text='<%#Container.DataItem("BankCode")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Description">
                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblBankBranchName" CssClass="bankBranchName" runat="server"
                                            Text='<%#Container.DataItem("BankBranchName")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                              
                                <asp:TemplateColumn HeaderText="COA" >
                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblbankCodeCabang" CssClass="bankCodeCabang" runat="server" Text='<%#Container.DataItem("bankCodeCabang")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                             
                            </Columns>--%>
                                <PagerStyle Visible="False" HorizontalAlign="Left" ForeColor="#000066" BackColor="White"
                                    Mode="NumericPages"></PagerStyle>
                            </asp:DataGrid>
                        </div>
                        <div class="button_gridnavigation">
                            <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                                CausesValidation="False" CommandName="First" OnCommand="NavigationLink_Click">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                                CausesValidation="False" CommandName="Prev" OnCommand="NavigationLink_Click">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                                CausesValidation="False" CommandName="Next" OnCommand="NavigationLink_Click">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                                CausesValidation="False" CommandName="Last" OnCommand="NavigationLink_Click">
                            </asp:ImageButton>
                            Page
                            <asp:TextBox ID="txtGoPage" runat="server" Width="34px" CssClass="txtPage">1</asp:TextBox>
                            <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtGoPage" MinimumValue="1"
                                MaximumValue="999" ErrorMessage="Page No. is not valid" Type="Double" Display="Dynamic"></asp:RangeValidator>
                            <asp:Button ID="btnGoPage" runat="server" EnableViewState="False" Text="Go" CssClass="small buttongo blue" />
                            <asp:RangeValidator ID="Rangevalidator1" runat="server" Type="Integer" MaximumValue="999999999"
                                ErrorMessage="Page No. is not valid" MinimumValue="1" ControlToValidate="txtGoPage"
                                Display="Dynamic"></asp:RangeValidator>
                            <asp:RequiredFieldValidator ID="rfvGo" runat="server" ErrorMessage="Page No. is not valid"
                                ControlToValidate="txtGoPage" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        </div>
                        <div class="navigasi_totalrecord">
                            <asp:Label ID="lblPage" runat="server"></asp:Label>of
                            <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                            <asp:Label ID="lblTotRec" runat="server"></asp:Label>record(s)
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <div class="form_box_title">
                <div class="form_single">
                    <h4>
                        Find By Bank
                    </h4>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <asp:DropDownList ID="cboSearchBy" runat="server">
                        <asp:ListItem Value="BankCode">Bank Code</asp:ListItem>
                        <asp:ListItem Value="BankBranchName">Bank Branch Name</asp:ListItem>
                        <asp:ListItem Value="Address">Address</asp:ListItem>
                    </asp:DropDownList>
                    <asp:TextBox ID="txtSearchBy" runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="btnSearch" runat="server" CausesValidation="False" Text="Find" CssClass="small button blue" />
                <asp:Button ID="btnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray" />
            </div>
        </asp:Panel>
    </div>
    </form>
</body>
</html>
