﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Imports Maxiloan.General.CommonCacheHelper
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
#End Region
Public Class SupplierGroup
    Inherits Maxiloan.Webform.WebBased
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property

    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property
    Public Property KodeClientID() As String
        Get
            Return CType(ViewState("KodeClientID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("KodeClientID") = Value
        End Set
    End Property
    Public Property NamaClientID() As String
        Get
            Return CType(ViewState("NamaClientID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("NamaClientID") = Value
        End Set
    End Property
#End Region
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            pnlGrid.Visible = False
            Me.KodeClientID = Request("kode")
            Me.NamaClientID = Request("nama")
            Me.Sort = "SupplierGroupID ASC"
        End If
    End Sub
    Public Function GetData(ByVal oCustomClass As Parameter.SupplierGroup) As Parameter.SupplierGroup
        Dim oReturnValue As New Parameter.SupplierGroup
        Dim params(4) As SqlParameter
        params(0) = New SqlParameter("@CurrentPage", SqlDbType.Int)
        params(0).Value = oCustomClass.CurrentPage
        params(1) = New SqlParameter("@PageSize", SqlDbType.Int)
        params(1).Value = oCustomClass.PageSize
        params(2) = New SqlParameter("@WhereCond", SqlDbType.VarChar)
        params(2).Value = oCustomClass.WhereCond
        params(3) = New SqlParameter("@SortBy", SqlDbType.VarChar)
        params(3).Value = oCustomClass.SortBy
        params(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        params(4).Direction = ParameterDirection.Output
        Try
            oReturnValue.Listdata = SqlHelper.ExecuteDataset(oCustomClass.strConnection, CommandType.StoredProcedure, "spPagingSupplierGroup", params).Tables(0)
            oReturnValue.TotalRecords = CType(params(4).Value, Int64)
            Return oReturnValue
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.GetData")
        End Try
    End Function
    Public Sub BindGridEntity(ByVal cmdWhere As String)
        Dim dtEntity As DataTable = Nothing
        Dim oCustom As New Parameter.SupplierGroup
        pnlGrid.Visible = True
        oCustom.strConnection = GetConnectionString()
        oCustom.WhereCond = cmdWhere
        oCustom.CurrentPage = currentPage
        oCustom.PageSize = pageSize
        oCustom.SortBy = Me.Sort
        oCustom = GetData(oCustom)

        If Not oCustom Is Nothing Then
            dtEntity = oCustom.ListData
            recordCount = oCustom.TotalRecords
        Else
            recordCount = 0
        End If

        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()
        PagingFooter()
    End Sub

    Protected Sub buttonSearch_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonSearch.Click

        If txtSearch.Text <> "" Then
            Dim tmptxtSearchBy As String = cboSearch.Text.Replace("%", "")
            Me.CmdWhere = cboSearch.SelectedItem.Value + " like '%" + tmptxtSearchBy + "%'"
        Else
            Me.CmdWhere = "ALL"
        End If

        BindGridEntity(Me.CmdWhere)

    End Sub

#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGoPage.Click
        If IsNumeric(txtGoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtGoPage.Text, Int32)
                BindGridEntity(Me.CmdWhere)
            End If
        End If
    End Sub
#End Region
    Protected Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If


        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub buttonReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        BindGridEntity(Me.CmdWhere)
    End Sub
End Class