﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SupplierGroup.aspx.vb" Inherits="Maxiloan.Webform.Utility.SupplierGroup" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
	<script src="../../js/jquery-1.9.1.min.js"></script>
	<script src="../../js/jquery-ui-1.10.3.custom.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%= dtgPaging.ClientID %> td > a").click(function (e) {
                e.preventDefault();
                var kode = $(this).parents('tr').find('.kode').html();
                var nama = $(this).parents('tr').find('.nama').html();
                var dataArg_Temp = [
                { name: '<%= KodeClientID %>', value: kode },
                { name: '<%= NamaClientID %>', value: nama }
                ];
                var dataArg = new Array();
                $.each(dataArg_Temp, function (index, value) {
                    dataArg.push({ name: value.name, value: value.value });
                });
                window.parent.BindFromJLookup(dataArg);
                window.parent.CloseJLookup();
            });


        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
        <asp:Panel ID="pnlGrid" runat="server" class="ui-lookup">
         <div class="form_box_title">
                        <div class="title_strip">
                        </div>
                        
                    </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtgPaging" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                                OnSortCommand="SortGrid" DataKeyField="SupplierGroupID" BorderStyle="None" BorderWidth="0"
                                CssClass="grid_general">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="Select">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lblSelect" runat="server" Text='Select'>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText ="SupplierGroup" SortExpression ="SupplierGroupID">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblSupplierGroup" CssClass="kode" Text='<%#Container.DataItem("SupplierGroupID")%>' />
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText ="NAMA GROUP SUPPLIER" SortExpression ="SupplierGroupName">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblSupplierGroupName" CssClass="nama" Text='<%#Container.DataItem("SupplierGroupName")%>' />
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="Kota" SortExpression="Kota" HeaderText="KOTA"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="ContactPerson" SortExpression="ContactPerson" HeaderText="KONTAK"></asp:BoundColumn>
                                </Columns>
                            </asp:DataGrid>
                            <div class="button_gridnavigation">
                                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                                    CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                                    CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                                    CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                                    CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                Page
                                <asp:TextBox ID="txtGoPage" runat="server" CssClass="small_text">1</asp:TextBox>
                                <asp:Button ID="btnGoPage" runat="server" EnableViewState="False" Text="Go" CssClass="small buttongo blue">
                                </asp:Button>
                                <asp:RangeValidator ID="Rangevalidator1" runat="server" Type="Integer" MaximumValue="999999999"
                                    ErrorMessage="Page No. is not valid" MinimumValue="1" ControlToValidate="txtGoPage"
                                    Display="Dynamic"></asp:RangeValidator>
                                <asp:RequiredFieldValidator ID="rfvGo" runat="server" ErrorMessage="Page No. is not valid"
                                    ControlToValidate="txtGoPage" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                                <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtGoPage" MinimumValue="1"
                                    MaximumValue="999" ErrorMessage="Page No. is not valid" Type="Double" Display="Dynamic"></asp:RangeValidator>
                            </div>
                            <div class="label_gridnavigation">
                                <asp:Label ID="lblPage" runat="server"></asp:Label>of
                                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel runat="server" ID="PanelSearch">
                <div class="form_box_title">
                    <div class="form_single">
                        <h4>
                            CARI GROUP SUPPIER
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Cari Berdasarkan</label>
                        <asp:DropDownList ID="cboSearch" runat="server">
                            <asp:ListItem Value="SupplierGroupName">Nama Group Supplier</asp:ListItem>
                            <asp:ListItem Value="Kota">Kota</asp:ListItem>
                            <asp:ListItem Value="ContactPerson">Kontak</asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtSearch" runat="server" Width="20%" MaxLength="100"></asp:TextBox>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonSearch" runat="server" CausesValidation="False" Text="Search"
                        CssClass="small button blue"></asp:Button>&nbsp;
                    <asp:Button ID="ButtonReset" runat="server" CausesValidation="False" Text="Reset"
                        CssClass="small button gray"></asp:Button>
                </div>
            </asp:Panel>
    </form>
</body>
</html>
