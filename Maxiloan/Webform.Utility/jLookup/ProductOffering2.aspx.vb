﻿Option Strict On
Imports Maxiloan.Controller
Imports Maxiloan.General.CommonVariableHelper
Public Class ProductOffering2
    Inherits Maxiloan.Webform.WebBased
    Public Delegate Sub ProductOffSelectedHandler(ByVal strSelectedID1 As String, ByVal strSelectedID2 As String, ByVal strSelectedName1 As String)
    Public Event ProductOffSelected As ProductOffSelectedHandler

#Region "Properties and Form Scope Variables"
    'Private oController As New LookUpProductOfferingController
    Private oController As New ProductController
    Private currentPage As Integer = DEFAULT_CURRENT_PAGE
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("cmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("cmdWhere") = Value
        End Set
    End Property

    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property

    Public Property ProductClientID() As String
        Get
            Return CType(ViewState("ProductClientID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ProductClientID") = Value
        End Set
    End Property

    Public Property ProductOfferingClientID() As String
        Get
            Return CType(ViewState("ProductOfferingClientID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ProductOfferingClientID") = Value
        End Set
    End Property

    Public Property AssetTypeClientID() As String
        Get
            Return CType(ViewState("AssetTypeClientID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("AssetTypeClientID") = Value
        End Set
    End Property

    Public Property DescClientID() As String
        Get
            Return CType(ViewState("DescClientID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("DescClientID") = Value
        End Set
    End Property
    Public Property KegiatanUsaha() As String
        Get
            Return CType(ViewState("KegiatanUsaha"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("KegiatanUsaha") = Value
        End Set
    End Property
    Public Property JenisPembiayaan() As String
        Get
            Return CType(ViewState("JenisPembiayaan"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("JenisPembiayaan") = Value
        End Set
    End Property
    Public Property SelectedProductID As String

    Public Property KegiatanUsahaIDClientID() As String
        Get
            Return CType(ViewState("KegiatanUsahaIDClientID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("KegiatanUsahaIDClientID") = Value
        End Set
    End Property

    Public Property JenisPembiayaanIDClientID() As String
        Get
            Return CType(ViewState("JenisPembiayaanIDClientID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("JenisPembiayaanIDClientID") = Value
        End Set
    End Property

    Public Property KegiatanUsahaClientID() As String
        Get
            Return CType(ViewState("KegiatanUsahaClientID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("KegiatanUsahaClientID") = Value
        End Set
    End Property

    Public Property JenisPembiayaanClientID() As String
        Get
            Return CType(ViewState("JenisPembiayaanClientID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("JenisPembiayaanClientID") = Value
        End Set
    End Property
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            Me.KegiatanUsaha = Request("KegiatanUsaha")
            Me.JenisPembiayaan = Request("JenisPembiayaan")

            'Me.CmdWhere = " BranchId = '" & Me.sesBranchId.Replace("'", "") & "' and KegiatanUsaha = '" & Me.KegiatanUsaha.ToString.Trim & "' and JenisPembiayaan = '" & Me.JenisPembiayaan.ToString.Trim & "'"
            Me.CmdWhere = " BranchId = '" & Me.sesBranchId.Replace("'", "") & "'"
            doBind()

            Me.ProductClientID = Request("productID")
            Me.ProductOfferingClientID = Request("productOfferingID")
            Me.AssetTypeClientID = Request("assetTypeID")
            Me.DescClientID = Request("desc")
            Me.JenisPembiayaanIDClientID = Request("JenisPembiayaanID")
            Me.KegiatanUsahaIDClientID = Request("KegiatanUsahaID")
            Me.JenisPembiayaanClientID = Request("JenisPembiayaan")
            Me.KegiatanUsahaClientID = Request("KegiatanUsaha")
        End If
    End Sub

    Public Sub doBind()
        Dim pController As New ProductController
        Dim oCustom As New Parameter.Product
        Dim dttProductBranch As New DataTable
        pnlGrid.Visible = False
        Me.Sort = "ProductOfferingID ASC"

        oCustom.strConnection = GetConnectionString()
        oCustom.Branch_ID = Me.sesBranchId.Replace("'", "")
        oCustom.KegiatanUsaha = Me.KegiatanUsaha
        oCustom.JenisPembiayaan = Me.JenisPembiayaan

        dttProductBranch = pController.ProductByKUJP(oCustom).ListData
        cboProductBranch.DataValueField = "ProductId"
        cboProductBranch.DataTextField = "Description"
        cboProductBranch.DataSource = dttProductBranch.DefaultView
        cboProductBranch.DataBind()
    End Sub

    Private Sub BindGridEntity(ByVal strWhere As String)
        'Dim dttEntity As DataTable = Nothing
        ''Dim oCustomClass As New Parameter.LookUpProductOffering
        'Dim oCustomClass As New Parameter.Product
        ''SelectedProductID = cboProductBranch.SelectedItem.Value.ToString.Trim
        'With oCustomClass
        '    .PageSize = DEFAULT_PAGE_SIZE
        '    .WhereCond = strWhere
        '    '.BusDate = Me.BusinessDate
        '    .BusinessDate = Me.BusinessDate
        '    .CurrentPage = currentPage
        '    .SortBy = Me.Sort
        '    .strConnection = GetConnectionString()
        'End With
        'oCustomClass = oController.GetListProductOffering(oCustomClass)
        'If Not oCustomClass Is Nothing Then
        '    dttEntity = oCustomClass.ListData
        '    recordCount = oCustomClass.TotalRecords
        'Else
        '    recordCount = 0
        'End If
        'dtgPaging.DataSource = dttEntity.DefaultView
        'dtgPaging.CurrentPageIndex = 0
        'dtgPaging.DataBind()
        'PagingFooter()

    End Sub



#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(DEFAULT_PAGE_SIZE, Integer)), Double))
        If totalPages = 0 Then
            lblMessage.Text = "Data not found ....."
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblTotRec.Text = recordCount.ToString
        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = DEFAULT_CURRENT_PAGE
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridEntity(Me.CmdWhere)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGoPage.Click
        If IsNumeric(txtGoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtGoPage.Text, Int32)
                BindGridEntity(Me.CmdWhere)
            End If
        End If
    End Sub
#End Region

    Protected Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub

    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        cboProductBranch.SelectedIndex = 0

        pnlGrid.Visible = False
    End Sub

    Private Sub imbSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Me.CmdWhere = " BranchId = '" & Me.sesBranchId.Replace("'", "") & "' "
        If Not IsNothing(cboProductBranch.SelectedItem) Then
            Me.CmdWhere = Me.CmdWhere & " and ProductId = '" & cboProductBranch.SelectedItem.Value & "'"
        End If

        BindGridEntity(Me.CmdWhere)
        pnlGrid.Visible = True

    End Sub



    Private Sub dtgProductOffering_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtgPaging.SelectedIndexChanged
        Dim i As Integer = dtgPaging.SelectedIndex
        Dim lblDescription As Label = CType(dtgPaging.Items(i).FindControl("lblDescription"), Label)
        Dim lblProductID As Label = CType(dtgPaging.Items(i).FindControl("lblProductID"), Label)
        RaiseEvent ProductOffSelected(dtgPaging.DataKeys.Item(i).ToString, lblProductID.Text.Trim, lblDescription.Text.Trim)
    End Sub

End Class