﻿Imports System.Web.Services
Imports System.Web.Script.Services
Imports System.Data.SqlClient
Imports Maxiloan.Controller
Imports Decrypt
Imports System.Web.HttpException
Imports System.Runtime.Serialization
Imports Newtonsoft.Json
Imports Maxiloan.SQLEngine
Imports Maxiloan.Framework.SQLEngine
Imports System.Web.Script.Serialization
Imports System.IO
Imports Newtonsoft.Json.Linq

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org")>
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)>
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
<ScriptService()>
Public Class GetLoginUtility
    'Inherits System.Web.Services.WebService
    Inherits Maxiloan.Webform.WebBased

    'http
    <WebMethod(EnableSession:=True)>
    <ScriptMethod(UseHttpGet:=True, ResponseFormat:=ResponseFormat.Json)>
    Public Sub GetPassOfUser(ByVal username As String, ByVal password As String)
        Dim customers As New List(Of String)()
        Dim lstrPassword As String
        Dim oLogin As New LoginController
        Dim oLoginEntities As New Parameter.Login
        Dim objDecent As New Decrypt.am_futility
        'Dim o As Webform.Utility

        Dim dataEmployee As DataTable
        Dim m_controller As New DataUserControlController

        Dim ulog As New Result
        Dim details As New List(Of Result)()
        Dim a As New List(Of String)
        Context.Request.ContentType = "application/json"
        Context.Response.Clear()
        Context.Response.ContentType = "application/json"
        'Dim conDB As String = "Server=103.14.45.98,6698;Database=IFINS;UID=secmgr.bni;Pwd=N6053560D90576502C11RkM;Max Pool Size=;Pooling=true;Connection Lifetime=120"
        'Dim conAPPMGR As String = "Data Source=192.168.2.11,1433;Initial Catalog=IFINAPPMGR;User ID=rkm;Password=Rajawal1db"
        Dim conDB As String = "Server=172.16.0.44\IFINPROD;Database=IFINS;UID=secmgr.bni;Pwd=05006666041N6441U416RkM;Max Pool Size=;Pooling=true;Connection Lifetime=120"
        Dim conAPPMGR As String = "Data Source=172.16.0.44;Initial Catalog=IFINAPPMGR;User ID=rkm;Password=Rajawal1db"


        Using conn As New SqlConnection()
            conn.ConnectionString = conDB
            If conn.State = ConnectionState.Closed Then conn.Open()
            oLoginEntities.strConnection = conDB
            oLoginEntities.LoginId = SqlHelper.ExecuteScalar(oLoginEntities.strConnection, CommandType.Text, "SELECT ISNULL(employeeid,'') from IFINAPPMGR.dbo.sec_msUser where loginID = '" + username.Trim + "'").ToString.Trim

            lstrPassword = objDecent.encryptto(oLogin.GetEmployee(oLoginEntities), "1")
            If lstrPassword.Trim = password.Trim Then
                With ulog
                    .Result = True
                End With

            Else
                With ulog
                    .Result = False
                End With
            End If
            details.Add(ulog)
            conn.Close()
            Context.Response.Write(JsonConvert.SerializeObject(details))
        End Using
    End Sub

    <WebMethod(EnableSession:=True)>
    <ScriptMethod(UseHttpGet:=False, ResponseFormat:=ResponseFormat.Json)>
    Public Sub GetPassOfUserJson(ByVal usernameJ As String, ByVal passwordJ As String)

        Dim o As New JObject
        o("username") = usernameJ
        o("password") = passwordJ


        Dim dict = JsonConvert.DeserializeObject(Of UserLogin)(o.ToString)

        Dim customers As New List(Of String)()
        Dim lstrPassword As String
        Dim oLogin As New LoginController
        Dim oLoginEntities As New Parameter.Login
        Dim objDecent As New Decrypt.am_futility

        Dim m_controller As New DataUserControlController

        Dim ulog As New Result
        Dim details As New List(Of Result)()
        Dim a As New List(Of String)
        Context.Request.ContentType = "application/json"
        Context.Response.Clear()
        Context.Response.ContentType = "application/json"

        Context.SkipAuthorization = True
        Context.Response.AddHeader("WWW-Authenticate", "Basic Realm")


        Dim username As String = CType(usernameJ, Object) 'lookup("username")
        Dim password As String = CType(passwordJ, Object) 'lookup("password")

        'Dim conDB As String = "Server=103.14.45.98,6698;Database=IFINS;UID=secmgr.bni;Pwd=N6053560D90576502C11RkM;Max Pool Size=;Pooling=true;Connection Lifetime=120"
        'Dim conAPPMGR As String = "Data Source=192.168.2.11,1433;Initial Catalog=IFINAPPMGR;User ID=rkm;Password=Rajawal1db"
        Dim conDB As String = "Server=172.16.0.44\IFINPROD;Database=IFINS;UID=secmgr.bni;Pwd=05006666041N6441U416RkM;Max Pool Size=;Pooling=true;Connection Lifetime=120"
        Dim conAPPMGR As String = "Data Source=172.16.0.44;Initial Catalog=IFINAPPMGR;User ID=rkm;Password=Rajawal1db"

        Dim sample As String = GetConnectionString()
        Using conn As New SqlConnection()
            conn.ConnectionString = conDB
            If conn.State = ConnectionState.Closed Then conn.Open()
            oLoginEntities.strConnection = conDB
            oLoginEntities.LoginId = SqlHelper.ExecuteScalar(oLoginEntities.strConnection, CommandType.Text, "SELECT ISNULL(employeeid,'') from IFINAPPMGR.dbo.sec_msUser where loginID = '" + username.Trim + "'").ToString.Trim

            'lstrPassword = objDecent.encryptto(oLogin.GetEmployee(oLoginEntities), "1").ToLower()
            lstrPassword = objDecent.encryptto(oLogin.GetEmployee(oLoginEntities), "1")

            If lstrPassword.Trim = password.Trim Then
                With ulog
                    .Result = True
                End With

            Else
                With ulog
                    .Result = False
                End With
            End If
            details.Add(ulog)
            conn.Close()
            Context.Response.Write(JsonConvert.SerializeObject(ulog))
        End Using
    End Sub


End Class
