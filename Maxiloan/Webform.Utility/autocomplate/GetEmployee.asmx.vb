﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Web.Script.Services
Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports Maxiloan.Webform

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
<ScriptService()> _
Public Class GetEmployee
    Inherits System.Web.Services.WebService

    <WebMethod(EnableSession:=True)> _
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function GetData(ByVal prefix As String) As String()
        Dim customers As New List(Of String)()
        Dim BranchID As String
        Using conn As New SqlConnection()
            BranchID = Session("BranchID").ToString
            conn.ConnectionString = Session("ConString").ToString
            Using cmd As New SqlCommand()
                'cmd.CommandText = "select top 10  EmployeeID,EmployeeName from BranchEmployee where BranchID = '" & BranchID & "' and  " & "EmployeeName like @SearchText + '%'"
                cmd.CommandText = "select top 10  EmployeeID,EmployeeName from BranchEmployee where EmployeeName like @SearchText + '%'"
                cmd.Parameters.AddWithValue("@SearchText", prefix)
                cmd.Connection = conn
                conn.Open()
                Using sdr As SqlDataReader = cmd.ExecuteReader()
                    While sdr.Read()
                        customers.Add(String.Format("{0}-{1}", sdr("EmployeeName"), sdr("EmployeeID")))
                    End While
                End Using
                conn.Close()
            End Using
            Return customers.ToArray()
        End Using
    End Function

   

End Class