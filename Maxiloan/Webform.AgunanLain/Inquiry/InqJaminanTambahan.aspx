﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="InqJaminanTambahan.aspx.vb" Inherits="Maxiloan.Webform.AgunanLain.InqJaminanTambahan" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %>  

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Company</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../js/jquery-1.9.1.min.js" type="text/javascript"></script>	
    <script src="../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
	<script src="../../js/jquery-2.1.1.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function OpenCust(CustomerID, pStyle) {
            var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            var x = screen.width;
            var y = screen.height - 100;
            window.open('http://<%# Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.LoanOrg/Credit/ViewPersonalCustomer.aspx?Style=' + pStyle + '&CustomerID=' + CustomerID, 'UserLookup', 'left = 0, top = 0, width = ' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }

        //opencollateralid
        function OpenColl(CustomerID, CollateralID, pStyle) {
            var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            var x = screen.width;
            var y = screen.height - 100;
            window.open('http://<%# Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.AgunanLain/Transaksi/ViewCollateralLandBuilding.aspx?Style=' + pStyle + '&CollateralID=' + CollateralID + '&CustomerID=' + CustomerID, 'UserLookup', 'left = 0, top = 0, width = ' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
                }

        function windowClose() {
            window.close();
        }

        function fConfirm() {
            if (window.confirm("Apakah yakin mau hapus data ini ?"))
                return true;
            else
                return false;
        }

        function fback() {
            history.go(-1);
            return false;
        }

        function OpenWindowCompany(pCompanyID) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            //function window open error salah penulisan url
            //window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.Setup/Organization/CompanyView.aspx?CompanyID=' + pCompanyID, 'CompanyView', 'left=15, top=10, width=985, height=480, menubar=0, scrollbars=yes');
            //
            var x = screen.width; var y = screen.height - 100;
            window.open('CompanyView.aspx?CompanyID=' + pCompanyID, 'CompanyView', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
            <asp:Panel ID="pnlList" runat="server">
                <div class="form_title">
                    <div class="title_strip">
                    </div>
                    <div class="form_single">
                        <h3>
                            DAFTAR JAMINAN TAMBAHAN
                        </h3>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ws">
                            <asp:DataGrid ID="dtgCust" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                OnSortCommand="Sorting" DataKeyField="Name" BorderStyle="None" BorderWidth="0">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="NAMA" SortExpression="Name">
                                        <ItemStyle HorizontalAlign="Left" Width="18%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkCustomer" runat="server" CausesValidation="false" Text='<%# DataBinder.eval(Container,"DataItem.Name") %>'>
                                            </asp:LinkButton>
                                            <asp:Label ID="lblCust" runat="server" Visible="false" Text='<%# DataBinder.eval(Container,"DataItem.CustomerID") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="ID Collateral" SortExpression="CollateralID">
                                        <ItemStyle HorizontalAlign="Left" Width="18%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkCollateralID" runat="server" CausesValidation="false" Text='<%# DataBinder.Eval(Container, "DataItem.CollateralID") %>'>
                                            </asp:LinkButton>
                                            <asp:Label ID="lblColl" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container, "DataItem.CollateralID") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="ID Customer" SortExpression="CustomerID">
                                        <ItemStyle HorizontalAlign="Left" Width="18%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkCustomerID" runat="server" CausesValidation="false" Text='<%# DataBinder.Eval(Container, "DataItem.CustomerID") %>'>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>                                  
                                    <asp:TemplateColumn HeaderText="NILAI COLLATERAL" HeaderStyle-HorizontalAlign="Right" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Right">                        
                                        <ItemTemplate>
                                            <asp:Label ID="lblCollateralAmount" runat="server" Text='<%#FormatNumber(Container.DataItem("CollateralAmount"), 0) %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>                            
                                            <asp:Label ID="lblTotCollateralAmount" runat="server"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateColumn>                                    
                                    <asp:TemplateColumn HeaderText="STATUS" HeaderStyle-CssClass="widthCabang" ItemStyle-CssClass="widthCabang">                                        
                                        <ItemStyle HorizontalAlign="Left" Width="5%"></ItemStyle>
                                        <ItemTemplate>
                                            <%# DataBinder.Eval(Container, "DataItem.Status")  %>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                             <uc2:ucGridNav id="GridNavigator" runat="server"/>
                         
                        </div>
                    </div>
                </div>
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            CARI CUSTOMER
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Cari berdasarkan</label>
                        <asp:DropDownList ID="cboSearchBy" runat="server" Width="144px" Font-Names="Verdana">
                            <asp:ListItem Value="CustomerID" Selected="True">ID</asp:ListItem>
                            <asp:ListItem Value="Name">Nama Nasabah</asp:ListItem>
                            <asp:ListItem Value="CollateralID">ID Collateral</asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="TxtSearchByValue" runat="server" Width="168px" ></asp:TextBox>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="BtnSearchNew" runat="server" CausesValidation="False" Text="Find"
                        CssClass="small button blue"></asp:Button>
                    <asp:Button ID="BtnResetNew" runat="server" CausesValidation="False" Text="Reset"
                        CssClass="small button gray"></asp:Button>
                </div>

            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
