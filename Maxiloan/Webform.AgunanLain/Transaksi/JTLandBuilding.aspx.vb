﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Imports Maxiloan.Webform.UserController

#End Region
Public Class JTLandBuilding
    'Inherits AbsCustomerPersonal
    Inherits Maxiloan.Webform.WebBased
#Region "uc control declaration"
    Protected WithEvents ucLookupGroupCust1 As ucLookupGroupCust
    Protected WithEvents UcLegalAddress As UcCompanyAddress 'ucAddress
    Protected WithEvents UcResAddress As UcCompanyAddress 'ucAddress
    Protected WithEvents ucTinggalSejakBulan As ucMonthCombo
    Protected WithEvents ucDateCE As ucDateCE
    Protected WithEvents txtRentFinish As ucDateCE

	Protected WithEvents txtHargaRumah As ucNumberFormat
    Protected WithEvents txtPDependentNum As ucNumberFormat
    Private m_controller As New AgunanLainController
#End Region


#Region "Property"
    Private Property CustomerID() As String
        Get
            Return CType(ViewState("CustomerID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property

	Private Property CollateralID() As String
		Get
			Return CType(ViewState("CollateralID"), String)
		End Get
		Set(ByVal Value As String)
			ViewState("CollateralID") = Value
		End Set
	End Property

	Private Property Action() As String
		Get
			Return CType(ViewState("page"), String)
		End Get
		Set(ByVal Value As String)
			ViewState("page") = Value
		End Set
	End Property


	Private Property CollateralTypeID() As String
		Get
			Return CType(ViewState("CollateralTypeID"), String)
		End Get
		Set(ByVal Value As String)
			ViewState("CollateralTypeID") = Value
		End Set
	End Property
#End Region

#Region "Page_Load"
	Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
		Page.Header.DataBind()

		If CheckForm(Me.Loginid, "CltrlTambahanCst", "MAXILOAN") Then
			If SessionInvalid() Then
				Exit Sub
			End If
		End If

		If Not Me.IsPostBack Then
			Me.Action = Request("page").Trim
			Me.CustomerID = Request("CustomerID").Trim
			Me.CollateralID = Request("CollateralID").Trim
			Me.BranchID = Me.sesBranchId.Trim.Replace("'", "")
			DoBind()

			If Me.Action = "add" Then
				getDataLandBuilding()
				If lblIDAgunan.Text <> "" Then
					Me.Action = "edit"
					cTabs.SetNavigateUrl(Me.Action, Request("CollateralId"), Request("CustomerId"))
					cTabs.RefreshAttr(Request("pnl"))

					getDataLandBuilding()
				Else
					cTabs.SetNavigateUrl(Request("page"), Request("CollateralId"), Request("CustomerId"))
					cTabs.RefreshAttr(Request("pnl"))

					lblIDAgunan.Text = Me.CollateralID
				End If
			ElseIf Me.Action = "edit" Then
				getDataLandBuilding()
				If lblIDAgunan.Text = "" Then
					Me.Action = "add"
					cTabs.SetNavigateUrl(Me.Action, Request("CollateralId"), Request("CustomerId"))
					cTabs.RefreshAttr(Request("pnl"))

					getDataLandBuilding()
					lblIDAgunan.Text = Me.CollateralID
				Else
					cTabs.SetNavigateUrl(Request("page"), Request("CollateralId"), Request("CustomerId"))
					cTabs.RefreshAttr(Request("pnl"))
				End If
			End If
		End If
	End Sub

    Sub getDataLandBuilding()
		Dim oPar As New Parameter.AgunanLain

		With oPar
			.strConnection = GetConnectionString()
			.CustomerID = Me.CustomerID
            .CollateralID = Me.CollateralID
        End With

		oPar = m_controller.getDataLandBuildidng(oPar)

		With oPar
			Me.BranchID = .BranchId
			cboJenisAgunan.SelectedValue = .CollateralTypeID
			cboStatusAgunan.SelectedValue = .CollateralStatus

			lblIDAgunan.Text = .CollateralID
			If txtCurrency.Text = "" Then
				txtCurrency.Text = "IDR"
			End If
			lblAgunanSeqNo.Text = "1"
			txtPemilikAgunan.Text = .PemilikCollateral
            txtAlamatAgunan.Text = .AlamatPemilikCollateral
            txtStatusSertifikat.Text = .StatusSertifikat
            txtJenisSertifikat.Text = .JenisSertifikat

			txtNoSertifikat.Text = .NoSertifikat
            txtNoGambarSituasi.Text = .NoGambarSituasi
            txtDevelopr.Text = .Developer
            txtLuasTanah.Text = .LuasTanah
            txtLuasBangunan.Text = .LuasBangunan
			txtPrgKonstruksi.Text = .ProgresKonstruksi
			txtKecamatan.Text = .Kecamatan
            txtDesa.Text = .Desa
            txtAlamat.Text = .Alamat
            txtSandiLokasi.Text = .SandiLokasi
            txtMortgagetype.Text = .MortgageType
            txtPropertyType.Text = .PropertyType
			txtNilaiPasarDiakui.Text = FormatNumber(.NilaiPasarDiakui, 0)
			txtPIBDNilaiTanah.Text = FormatNumber(.PIBDNilaiTanah, 0)
            txtPIBDNilaiBangunan.Text = FormatNumber(.PIBDNilaiBangunan, 0)
			txtPIBDNilaiTanahBangunan.Text = FormatNumber(.PIBDNilaiTanahBangunan, 0)
			txtPIBFNilaiTanah.Text = FormatNumber(.PIBFNilaiTanah, 0)
            txtPIBFNilaiBangunan.Text = FormatNumber(.PIBFNilaiBangunan, 0)
			txtPIBFNilaiTanahBangunan.Text = FormatNumber(.PIBFNilaiTanahBangunan, 0)
			txtPEBDNilaiTanah.Text = FormatNumber(.PEBDNilaiTanah, 0)
            txtPEBDNilaiBangunan.Text = FormatNumber(.PEBDNilaiBangunan, 0)
			txtPEBDNilaiTanahBangunan.Text = FormatNumber(.PEBDNilaiTanahBangunan, 0)
			txtPEBFNilaiTanah.Text = FormatNumber(.PEBFNilaiTanah, 0)
            txtPEBFNilaiBangunan.Text = FormatNumber(.PEBFNilaiBangunan, 0)
            txtPEBFNilaiTanahBangunan.Text = FormatNumber(.PEBFNilaiTanahBangunan, 0)
			txtPJNoSKMHT.Text = .PJNoSKMHT
			txtPJNoAPHT.Text = .PJNoAPHT
			txtPJNoSHT.Text = .PJNoSHT
			txtPJTotalNilaiHT.Text = FormatNumber(.PJTotalNilaiHT, 0)
            txtPJRangkingHT.Text = .PJRangkingHT
            txtPJJenisPengikatan.Text = .PJJenisPengikatan
            txtPJNotaris.Text = .PJNotaris
            txtPJLokasiPenyimpananDokumen.Text = .PJLokasiPenyimpananDokumen
            txtPJKantorBPNWilayah.Text = .PJKantorBPNWilayah
            txtBgnFisikJaminan.Text = .BgnFisikJaminan
            txtBgnSuratIzin.Text = .BgnSuratIzin
            txtBgnNoSuratIzin.Text = .BgnNoSuratIzin
            txtBgnJenisSuratIzin.Text = .BgnJenisSuratIzin
            txtBgnPeruntukanBangunan.Text = .BgnPeruntukanBangunan
            txtBgnNoIzinLayakHuni.Text = .BgnNoIzinLayakHuni
            txtBgnPBBTahunTerakhir.Text = .BgnPBBTahunTerakhir
            txtBgnNilaiNJOP.Text = .BgnNilaiNJOP
            txtBgnCetakBiru.Text = .BgnCetakBiru
            txtBgnAktaBalikNama.Text = .BgnAktaBalikNama
			txtBgnNoAkta.Text = .BgnNoAkta
			txtBgnNotaris.Text = .BgnNotaris
            BgnParipasu.Text = .BgnParipasu
			BgnBangunanDiasuransikan.Text = .BgnBangunanDiasuransikan
			txtNilaiPledging.Text = FormatNumber(.NilaiPledging, 0)

			If .ThnPenerbitanSertifikat = ConvertDate2("01/01/1900") Or Me.Action = "add" Then
				txtThnPenerbitanSertifikat.Text = ""
			ElseIf .ThnPenerbitanSertifikat = ConvertDate2("01/01/0001") Then
				txtThnPenerbitanSertifikat.Text = ""
			Else
				txtThnPenerbitanSertifikat.Text = .ThnPenerbitanSertifikat.ToString("dd/MM/yyyy")
			End If

			If .BgnTglAkta = ConvertDate2("01/01/1900") Or Me.Action = "add" Then
				txtBgnTglAkta.Text = ""
			ElseIf .BgnTglAkta = ConvertDate2("01/01/0001") Then
				txtBgnTglAkta.Text = ""
			Else
				txtBgnTglAkta.Text = .BgnTglAkta.ToString("dd/MM/yyyy")
			End If

			If .PJTglIssuedSHT = ConvertDate2("01/01/1900") Or Me.Action = "add" Then
				txtPJTglIssuedSHT.Text = ""
			ElseIf .PJTglIssuedSHT = ConvertDate2("01/01/0001") Then
				txtPJTglIssuedSHT.Text = ""
			Else
				txtPJTglIssuedSHT.Text = .PJTglIssuedSHT.ToString("dd/MM/yyyy")
			End If

			If .PJTglAPHT = ConvertDate2("01/01/1900") Or Me.Action = "add" Then
				txtPJTglAPHT.Text = ""
			ElseIf .PJTglAPHT = ConvertDate2("01/01/0001") Then
				txtPJTglAPHT.Text = ""
			Else
				txtPJTglAPHT.Text = .PJTglAPHT.ToString("dd/MM/yyyy")
			End If

			If .PJTglSKMHT = ConvertDate2("01/01/1900") Or Me.Action = "add" Then
				txtPJTglSKMHT.Text = ""
			ElseIf .PJTglSKMHT = ConvertDate2("01/01/0001") Then
				txtPJTglSKMHT.Text = ""
			Else
				txtPJTglSKMHT.Text = .PJTglSKMHT.ToString("dd/MM/yyyy")
			End If

			If .PJTglJatuhTempoSKMHT = ConvertDate2("01/01/1900") Or Me.Action = "add" Then
				txtPJTglJatuhTempoSKMHT.Text = ""
			ElseIf .PJTglJatuhTempoSKMHT = ConvertDate2("01/01/0001") Then
				txtPJTglJatuhTempoSKMHT.Text = ""
			Else
				txtPJTglJatuhTempoSKMHT.Text = .PJTglJatuhTempoSKMHT.ToString("dd/MM/yyyy")
			End If

			If .PEBFTglPenilaian = ConvertDate2("01/01/1900") Or Me.Action = "add" Then
				txtPEBFTglPenilaian.Text = ""
			ElseIf .PEBFTglPenilaian = ConvertDate2("01/01/0001") Then
				txtPEBFTglPenilaian.Text = ""
			Else
				txtPEBFTglPenilaian.Text = .PEBFTglPenilaian.ToString("dd/MM/yyyy")
			End If

			If .PEBDTglPenilaian = ConvertDate2("01/01/1900") Or Me.Action = "add" Then
				txtPEBDTglPenilaian.Text = ""
			ElseIf .PEBDTglPenilaian = ConvertDate2("01/01/0001") Then
				txtPEBDTglPenilaian.Text = ""
			Else
				txtPEBDTglPenilaian.Text = .PEBDTglPenilaian.ToString("dd/MM/yyyy")
			End If

			If .PIBFTglPenilaian = ConvertDate2("01/01/1900") Or Me.Action = "add" Then
				txtPIBFTglPenilaian.Text = ""
			ElseIf .PIBFTglPenilaian = ConvertDate2("01/01/0001") Then
				txtPIBFTglPenilaian.Text = ""
			Else
				txtPIBFTglPenilaian.Text = .PIBFTglPenilaian.ToString("dd/MM/yyyy")
			End If

			If .TglPenilaian = ConvertDate2("01/01/1900") Or Me.Action = "add" Then
				txtTglPenilaian.Text = ""
			ElseIf .TglPenilaian = ConvertDate2("01/01/0001") Then
				txtTglPenilaian.Text = ""
			Else
				txtTglPenilaian.Text = .TglPenilaian.ToString("dd/MM/yyyy")
			End If

			If .TglupdAgunan = ConvertDate2("01/01/1900") Or Me.Action = "add" Then
				txtTglUpdAgunan.Text = ""
			ElseIf .TglupdAgunan = ConvertDate2("01/01/0001") Then
				txtTglUpdAgunan.Text = ""
			Else
				txtTglUpdAgunan.Text = .TglupdAgunan.ToString("dd/MM/yyyy")
			End If

			If .PIBDTglPenilaian = ConvertDate2("01/01/1900") Or Me.Action = "add" Then
				txtPIBDTglPenilaian.Text = ""
			ElseIf .PIBDTglPenilaian = ConvertDate2("01/01/0001") Then
				txtPIBDTglPenilaian.Text = ""
			Else
				txtPIBDTglPenilaian.Text = .PIBDTglPenilaian.ToString("dd/MM/yyyy")
			End If

			If .TglUpdKonstruksi = ConvertDate2("01/01/1900") Or Me.Action = "add" Then
				txtTglUpdKonstruksi.Text = ""
			ElseIf .TglUpdKonstruksi = ConvertDate2("01/01/0001") Then
				txtTglUpdKonstruksi.Text = ""
			Else
				txtTglUpdKonstruksi.Text = .TglUpdKonstruksi.ToString("dd/MM/yyyy")
			End If

		End With
    End Sub
#End Region

	Sub DoBind()

		FillCombo()
		FillCbo(cboJenisAgunan, "TblCollateralType")
	End Sub

	Private Sub FillCombo()
		Dim strConn As String
		Dim dtCombo As New DataTable

		strConn = GetConnectionString()
	End Sub

	Sub FillCbo(ByVal cboName As DropDownList, ByVal Initial As String)
		Dim oAssetData As New Parameter.AgunanLain
		Dim oData As New DataTable
		oAssetData.strConnection = GetConnectionString()
		oAssetData.Initial = Initial
		oAssetData = m_controller.GetCboCollateralType(oAssetData)
		oData = oAssetData.ListData
		cboName.DataSource = oData
		cboName.DataTextField = "ID"
		cboName.DataValueField = "CollateralTypeID"
		cboName.DataBind()
		'cboName.Items.Insert(0, "Select One")
		'cboName.Items(0).Value = "0"
	End Sub

	Private Sub imbPSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
		If Me.Action = "add" Then
			SaveAddDataLandBuilding()
			Response.Redirect("CustomerJaminanTambahanList.aspx?id=" & Me.CustomerID)
			'cTabs.SetNavigateUrl(Request("page"), Request("CollateralId"), Request("CustomerId"))
			'cTabs.RefreshAttr(Request("pnl"))
			ShowMessage(lblMessage, "Data Berhasil Disimpan .....", False)
			'Response.Redirect("JTLandBuilding.aspx?page=add&CustomerID=" & Me.CustomerID & "&CollateralID=" & Me.CollateralID & "&pnl=tabLandBuilding")
		ElseIf Me.Action = "edit" Then
			SaveEditDataLandBuilding()
			ShowMessage(lblMessage, "Data Berhasil Diupdate.....", False)
		Else
			Exit Sub
		End If
	End Sub

	Private Sub SaveEditDataLandBuilding()
		Dim oPar As New Parameter.AgunanLain

		With oPar
			.strConnection = GetConnectionString()
			.BranchId = Me.BranchID
			.CustomerID = Me.CustomerID
			.CollateralID = Me.CollateralID

			.CollateralTypeID = cboJenisAgunan.SelectedValue()
			.CollateralStatus = cboStatusAgunan.SelectedValue
			.PemilikCollateral = txtPemilikAgunan.Text
			.Currency = txtCurrency.Text

			.PemilikCollateral = txtPemilikAgunan.Text
			.AlamatPemilikCollateral = txtAlamatAgunan.Text
			.StatusSertifikat = txtStatusSertifikat.Text
			.JenisSertifikat = txtJenisSertifikat.Text
			.NoSertifikat = txtNoSertifikat.Text
			.NoGambarSituasi = txtNoGambarSituasi.Text
			.Developer = txtDevelopr.Text
			.LuasTanah = txtLuasTanah.Text
			.LuasBangunan = txtLuasBangunan.Text
			.ProgresKonstruksi = txtPrgKonstruksi.Text
			.Kecamatan = txtKecamatan.Text
			.Desa = txtDesa.Text
			.Alamat = txtAlamat.Text
			.SandiLokasi = txtSandiLokasi.Text
			.MortgageType = txtMortgagetype.Text
			.PropertyType = txtPropertyType.Text
			.NilaiPasarDiakui = txtNilaiPasarDiakui.Text
			.NilaiPledging = txtNilaiPledging.Text
			.PIBDNilaiTanah = txtPIBDNilaiTanah.Text
			.PIBDNilaiBangunan = txtPIBDNilaiBangunan.Text
			.PIBDNilaiTanahBangunan = txtPIBDNilaiTanahBangunan.Text
			.PIBFNilaiTanah = txtPIBFNilaiTanah.Text
			.PIBFNilaiBangunan = txtPIBFNilaiBangunan.Text
			.PIBFNilaiTanahBangunan = txtPIBFNilaiTanahBangunan.Text
			.PEBDNilaiTanah = txtPEBDNilaiTanah.Text
			.PEBDNilaiBangunan = txtPEBDNilaiBangunan.Text
			.PEBDNilaiTanahBangunan = txtPEBDNilaiTanahBangunan.Text
			.PEBFNilaiTanah = txtPEBFNilaiTanah.Text
			.PEBFNilaiBangunan = txtPEBFNilaiBangunan.Text
			.PEBFNilaiTanahBangunan = txtPEBFNilaiTanahBangunan.Text
			.PJNoSKMHT = txtPJNoSKMHT.Text
			.PJNoAPHT = txtPJNoAPHT.Text
			.PJNoSHT = txtPJNoSHT.Text
			.PJTotalNilaiHT = txtPJTotalNilaiHT.Text
			.PJRangkingHT = txtPJRangkingHT.Text
			.PJJenisPengikatan = txtPJJenisPengikatan.Text
			.PJNotaris = txtPJNotaris.Text
			.PJLokasiPenyimpananDokumen = txtPJLokasiPenyimpananDokumen.Text
			.PJKantorBPNWilayah = txtPJKantorBPNWilayah.Text
			.BgnFisikJaminan = txtBgnFisikJaminan.Text
			.BgnSuratIzin = txtBgnSuratIzin.Text
			.BgnNoSuratIzin = txtBgnNoSuratIzin.Text
			.BgnJenisSuratIzin = txtBgnJenisSuratIzin.Text
			.BgnPeruntukanBangunan = txtBgnPeruntukanBangunan.Text
			.BgnNoIzinLayakHuni = txtBgnNoIzinLayakHuni.Text
			.BgnPBBTahunTerakhir = txtBgnPBBTahunTerakhir.Text
			.BgnNilaiNJOP = txtBgnNilaiNJOP.Text
			.BgnCetakBiru = txtBgnCetakBiru.Text
			.BgnAktaBalikNama = txtBgnAktaBalikNama.Text
			.BgnNoAkta = txtBgnNoAkta.Text
			.BgnNotaris = txtBgnNotaris.Text
			.BgnParipasu = BgnParipasu.Text
			.BgnBangunanDiasuransikan = BgnBangunanDiasuransikan.Text
			.LoginId = Me.Loginid

			If txtTglUpdKonstruksi.Text <> "" Then
				.TglUpdKonstruksi = ConvertDate2(txtTglUpdKonstruksi.Text)
			Else
				.TglUpdKonstruksi = ConvertDate2("01/01/1900")
			End If

			If txtBgnTglAkta.Text <> "" Then
				.BgnTglAkta = ConvertDate2(txtBgnTglAkta.Text)
			Else
				.BgnTglAkta = ConvertDate2("01/01/1900")
			End If

			If txtPJTglIssuedSHT.Text <> "" Then
				.PJTglIssuedSHT = ConvertDate2(txtPJTglIssuedSHT.Text)
			Else
				.PJTglIssuedSHT = ConvertDate2("01/01/1900")
			End If

			If txtPJTglAPHT.Text <> "" Then
				.PJTglAPHT = ConvertDate2(txtPJTglAPHT.Text)
			Else
				.PJTglAPHT = ConvertDate2("01/01/1900")
			End If

			If txtPJTglSKMHT.Text <> "" Then
				.PJTglSKMHT = ConvertDate2(txtPJTglSKMHT.Text)
			Else
				.PJTglSKMHT = ConvertDate2("01/01/1900")
			End If

			If txtPJTglJatuhTempoSKMHT.Text <> "" Then
				.PJTglJatuhTempoSKMHT = ConvertDate2(txtPJTglJatuhTempoSKMHT.Text)
			Else
				.PJTglJatuhTempoSKMHT = ConvertDate2("01/01/1900")
			End If

			If txtPEBFTglPenilaian.Text <> "" Then
				.PEBFTglPenilaian = ConvertDate2(txtPEBFTglPenilaian.Text)
			Else
				.PEBFTglPenilaian = ConvertDate2("01/01/1900")
			End If

			If txtPEBDTglPenilaian.Text <> "" Then
				.PEBDTglPenilaian = ConvertDate2(txtPEBDTglPenilaian.Text)
			Else
				.PEBDTglPenilaian = ConvertDate2("01/01/1900")
			End If

			If txtPIBFTglPenilaian.Text <> "" Then
				.PIBFTglPenilaian = ConvertDate2(txtPIBFTglPenilaian.Text)
			Else
				.PIBFTglPenilaian = ConvertDate2("01/01/1900")
			End If

			If txtTglUpdAgunan.Text <> "" Then
				.TglupdAgunan = ConvertDate2(txtTglUpdAgunan.Text)
			Else
				.TglupdAgunan = ConvertDate2("01/01/1900")
			End If

			If txtPIBDTglPenilaian.Text <> "" Then
				.PIBDTglPenilaian = ConvertDate2(txtPIBDTglPenilaian.Text)
			Else
				.PIBDTglPenilaian = ConvertDate2("01/01/1900")
			End If

			If txtTglPenilaian.Text <> "" Then
				.TglPenilaian = ConvertDate2(txtTglPenilaian.Text)
			Else
				.TglPenilaian = ConvertDate2("01/01/1900")
			End If

			If txtThnPenerbitanSertifikat.Text <> "" Then
				.ThnPenerbitanSertifikat = ConvertDate2(txtThnPenerbitanSertifikat.Text)
			Else
				.ThnPenerbitanSertifikat = ConvertDate2("01/01/1900")
			End If
		End With
		oPar = m_controller.saveEditDataLandBuilding(oPar)
	End Sub

	Private Sub SaveAddDataLandBuilding()

		Dim oPar As New Parameter.AgunanLain

		With oPar
			.strConnection = GetConnectionString()
			.CustomerID = Me.CustomerID
			.CollateralID = Me.CollateralID
		End With

		oPar = m_controller.getDataBPKB(oPar)
		lblIDAgunan.Text = oPar.CollateralID
		Me.CollateralID = Request("CollateralID").Trim

		With oPar
			.strConnection = GetConnectionString()
			.BranchId = Me.sesBranchId.Trim.Replace("'", "")
			.CustomerID = Me.CustomerID

			If lblIDAgunan.Text = "" And Me.CollateralID = "" Then
				GetCollateralID()
				AddCollateral()
				.CollateralID = Me.CollateralID
			Else
				.CollateralID = Me.CollateralID
			End If

			.CollateralTypeID = cboJenisAgunan.SelectedValue()
			.CollateralStatus = cboStatusAgunan.SelectedValue()
			.Currency = "IDR"
			.PemilikCollateral = txtPemilikAgunan.Text
			.AlamatPemilikCollateral = txtAlamatAgunan.Text
			.StatusSertifikat = txtStatusSertifikat.Text
			.JenisSertifikat = txtJenisSertifikat.Text
			.NoSertifikat = txtNoSertifikat.Text
			.NoGambarSituasi = txtNoGambarSituasi.Text
			.Developer = txtDevelopr.Text
			.LuasTanah = txtLuasTanah.Text
			.LuasBangunan = txtLuasBangunan.Text
			.ProgresKonstruksi = txtPrgKonstruksi.Text
			.Kecamatan = txtKecamatan.Text
			.Desa = txtDesa.Text
			.Alamat = txtAlamat.Text
			.SandiLokasi = txtSandiLokasi.Text
			.MortgageType = txtMortgagetype.Text
			.PropertyType = txtPropertyType.Text
			.NilaiPasarDiakui = txtNilaiPasarDiakui.Text
			.NilaiPledging = txtNilaiPledging.Text
			.PIBDNilaiTanah = txtPIBDNilaiTanah.Text
			.PIBDNilaiBangunan = txtPIBDNilaiBangunan.Text
			.PIBDNilaiTanahBangunan = txtPIBDNilaiTanahBangunan.Text
			.PIBFNilaiTanah = txtPIBFNilaiTanah.Text
			.PIBFNilaiBangunan = txtPIBFNilaiBangunan.Text
			.PIBFNilaiTanahBangunan = txtPIBFNilaiTanahBangunan.Text
			.PEBDNilaiTanah = txtPEBDNilaiTanah.Text
			.PEBDNilaiBangunan = txtPEBDNilaiBangunan.Text
			.PEBDNilaiTanahBangunan = txtPEBDNilaiTanahBangunan.Text
			.PEBFNilaiTanah = txtPEBFNilaiTanah.Text
			.PEBFNilaiBangunan = txtPEBFNilaiBangunan.Text
			.PEBFNilaiTanahBangunan = txtPEBFNilaiTanahBangunan.Text
			.PJNoSKMHT = txtPJNoSKMHT.Text
			.PJNoAPHT = txtPJNoAPHT.Text
			.PJNoSHT = txtPJNoSHT.Text
			.PJTotalNilaiHT = txtPJTotalNilaiHT.Text
			.PJRangkingHT = txtPJRangkingHT.Text
			.PJJenisPengikatan = txtPJJenisPengikatan.Text
			.PJNotaris = txtPJNotaris.Text
			.PJLokasiPenyimpananDokumen = txtPJLokasiPenyimpananDokumen.Text
			.PJKantorBPNWilayah = txtPJKantorBPNWilayah.Text
			.BgnFisikJaminan = txtBgnFisikJaminan.Text
			.BgnSuratIzin = txtBgnSuratIzin.Text
			.BgnNoSuratIzin = txtBgnNoSuratIzin.Text
			.BgnJenisSuratIzin = txtBgnJenisSuratIzin.Text
			.BgnPeruntukanBangunan = txtBgnPeruntukanBangunan.Text
			.BgnNoIzinLayakHuni = txtBgnNoIzinLayakHuni.Text
			.BgnPBBTahunTerakhir = txtBgnPBBTahunTerakhir.Text
			.BgnNilaiNJOP = txtBgnNilaiNJOP.Text
			.BgnCetakBiru = txtBgnCetakBiru.Text
			.BgnAktaBalikNama = txtBgnAktaBalikNama.Text
			.BgnNoAkta = txtBgnNoAkta.Text
			.BgnNotaris = txtBgnNotaris.Text
			.BgnParipasu = BgnParipasu.Text
			.BgnBangunanDiasuransikan = BgnBangunanDiasuransikan.Text
			.LoginId = Me.Loginid

			If txtTglUpdKonstruksi.Text <> "" Then
				.TglUpdKonstruksi = ConvertDate2(txtTglUpdKonstruksi.Text)
			Else
				.TglUpdKonstruksi = ConvertDate2("01/01/1900")
			End If

			If txtThnPenerbitanSertifikat.Text <> "" Then
				.ThnPenerbitanSertifikat = ConvertDate2(txtThnPenerbitanSertifikat.Text)
			Else
				.ThnPenerbitanSertifikat = ConvertDate2("01/01/1900")
			End If

			If txtBgnTglAkta.Text <> "" Then
				.BgnTglAkta = ConvertDate2(txtBgnTglAkta.Text)
			Else
				.BgnTglAkta = ConvertDate2("01/01/1900")
			End If

			If txtPJTglIssuedSHT.Text <> "" Then
				.PJTglIssuedSHT = ConvertDate2(txtPJTglIssuedSHT.Text)
			Else
				.PJTglIssuedSHT = ConvertDate2("01/01/1900")
			End If

			If txtPJTglAPHT.Text <> "" Then
				.PJTglAPHT = ConvertDate2(txtPJTglAPHT.Text)
			Else
				.PJTglAPHT = ConvertDate2("01/01/1900")
			End If

			If txtPJTglSKMHT.Text <> "" Then
				.PJTglSKMHT = ConvertDate2(txtPJTglSKMHT.Text)
			Else
				.PJTglSKMHT = ConvertDate2("01/01/1900")
			End If

			If txtPJTglJatuhTempoSKMHT.Text <> "" Then
				.PJTglJatuhTempoSKMHT = ConvertDate2(txtPJTglJatuhTempoSKMHT.Text)
			Else
				.PJTglJatuhTempoSKMHT = ConvertDate2("01/01/1900")
			End If

			If txtPEBFTglPenilaian.Text <> "" Then
				.PEBFTglPenilaian = ConvertDate2(txtPEBFTglPenilaian.Text)
			Else
				.PEBFTglPenilaian = ConvertDate2("01/01/1900")
			End If

			If txtPEBDTglPenilaian.Text <> "" Then
				.PEBDTglPenilaian = ConvertDate2(txtPEBDTglPenilaian.Text)
			Else
				.PEBDTglPenilaian = ConvertDate2("01/01/1900")
			End If

			If txtPIBFTglPenilaian.Text <> "" Then
				.PIBFTglPenilaian = ConvertDate2(txtPIBFTglPenilaian.Text)
			Else
				.PIBFTglPenilaian = ConvertDate2("01/01/1900")
			End If

			If txtTglUpdAgunan.Text <> "" Then
				.TglupdAgunan = ConvertDate2(txtTglUpdAgunan.Text)
			Else
				.TglupdAgunan = ConvertDate2("01/01/1900")
			End If

			If txtPIBDTglPenilaian.Text <> "" Then
				.PIBDTglPenilaian = ConvertDate2(txtPIBDTglPenilaian.Text)
			Else
				.PIBDTglPenilaian = ConvertDate2("01/01/1900")
			End If

			If txtTglPenilaian.Text <> "" Then
				.TglPenilaian = ConvertDate2(txtTglPenilaian.Text)
			Else
				.TglPenilaian = ConvertDate2("01/01/1900")
			End If
		End With
		oPar = m_controller.saveAddDataLandBuilding(oPar)
	End Sub
	Private Sub GetCollateralID()
		Dim oPar As New Parameter.AgunanLain
		Me.BranchID = Replace(sesBranchId, "'", "")
		With oPar
			.strConnection = GetConnectionString()
			.BranchId = Me.BranchID
			.CustomerID = Me.CustomerID
			.ID = "CLTR"
		End With

		Me.CollateralID = m_controller.GetCollateralId(oPar)
	End Sub

	Private Sub AddCollateral()
		Dim oPar As New Parameter.AgunanLain

		With oPar
			.strConnection = GetConnectionString()
			.CustomerID = Me.CustomerID
			.CollateralID = Me.CollateralID
			.CollateralAmount = 0
			.Keterangan = 1 'As Status'
			.LoginId = Me.Loginid
		End With
		oPar = m_controller.AddCollateral(oPar)
	End Sub
	Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
		Response.Redirect("CustomerJaminanTambahanList.aspx?id=" & Me.CustomerID)
	End Sub
End Class