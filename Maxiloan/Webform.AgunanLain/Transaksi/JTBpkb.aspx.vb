﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Imports Maxiloan.Webform.UserController
#End Region

Public Class JTBpkb
	Inherits Maxiloan.Webform.WebBased
	Private m_controller As New AgunanLainController

#Region "Property"
	Private Property CustomerID() As String
		Get
			Return CType(ViewState("CustomerID"), String)
		End Get
		Set(ByVal Value As String)
			ViewState("CustomerID") = Value
		End Set
	End Property

	Private Property CollateralID() As String
		Get
			Return CType(ViewState("CollateralID"), String)
		End Get
		Set(ByVal Value As String)
			ViewState("CollateralID") = Value
		End Set
	End Property

	Private Property Action() As String
		Get
			Return CType(ViewState("page"), String)
		End Get
		Set(ByVal Value As String)
			ViewState("page") = Value
		End Set
	End Property
#End Region


	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		lblMessage.Visible = False
		Page.Header.DataBind()

		If CheckForm(Me.Loginid, "CltrlTambahanCst", "MAXILOAN") Then
			If SessionInvalid() Then
				Exit Sub
			End If
		End If

		If Not Me.IsPostBack Then
			Me.Action = Request("page").Trim
			Me.CustomerID = Request("CustomerID").Trim
			Me.CollateralID = Request("CollateralID").Trim
			Me.BranchID = Me.sesBranchId.Trim.Replace("'", "")
			DoBind()

			If Me.Action = "add" Then
				getDataBPKB()
				If lblIDAgunan.Text <> "" Then
					Me.Action = "edit"
					cTabs.SetNavigateUrl(Me.Action, Request("CollateralId"), Request("CustomerId"))
					cTabs.RefreshAttr(Request("pnl"))

					getDataBPKB()
				Else
					cTabs.SetNavigateUrl(Request("page"), Request("CollateralId"), Request("CustomerId"))
					cTabs.RefreshAttr(Request("pnl"))

					lblIDAgunan.Text = Me.CollateralID
				End If
			ElseIf Me.Action = "edit" Then
				getDataBPKB()
				If lblIDAgunan.Text = "" Then
					Me.Action = "add"
					cTabs.SetNavigateUrl(Me.Action, Request("CollateralId"), Request("CustomerId"))
					cTabs.RefreshAttr(Request("pnl"))

					getDataBPKB()
					lblIDAgunan.Text = Me.CollateralID
				Else
					cTabs.SetNavigateUrl(Request("page"), Request("CollateralId"), Request("CustomerId"))
					cTabs.RefreshAttr(Request("pnl"))
				End If
			End If
		End If
	End Sub
	Sub DoBind()
		FillCombo()
		FillCbo(cboJenisAgunan, "TblCollateralType")
	End Sub
	Private Sub FillCombo()
		Dim strConn As String
		Dim dtCombo As New DataTable

		strConn = GetConnectionString()
	End Sub

	Sub FillCbo(ByVal cboName As DropDownList, ByVal Initial As String)
		Dim oAssetData As New Parameter.AgunanLain
		Dim oData As New DataTable
		oAssetData.strConnection = GetConnectionString()
		oAssetData.Initial = Initial
		oAssetData = m_controller.GetCboCollateralType(oAssetData)
		oData = oAssetData.ListData
		cboName.DataSource = oData
		cboName.DataTextField = "ID"
		cboName.DataValueField = "CollateralTypeID"
		cboName.DataBind()
		'cboName.Items.Insert(0, "Select One")
		'cboName.Items(0).Value = ""
	End Sub

	Sub getDataBPKB()
		Dim oPar As New Parameter.AgunanLain

		With oPar
			.strConnection = GetConnectionString()
			.CustomerID = Me.CustomerID
			.CollateralID = Me.CollateralID
		End With

		oPar = m_controller.getDataBPKB(oPar)

		With oPar

			Me.BranchID = .BranchId
			cboJenisAgunan.SelectedValue = .CollateralTypeID
			cboStatusAgunan.SelectedValue = .CollateralStatus
			lblIDAgunan.Text = .CollateralID
			txtCurrency.Text = .Currency
			lblAgunanSeqNo.Text = "2"
			txtPemilikAgunan.Text = .PemilikCollateral
			txtNoRangka.Text = .NoRangka
			txtNoMesin.Text = .NoMesin
			txtNamaBpkb.Text = .NamaDiBPKB
			txtMerk.Text = .Merk
			txtTipe.Text = .Tipe
			txtWarna.Text = .Warna
			txtNamaDealer.Text = .NamaDealer
			txtAlamatDealer.Text = .AlamatDealer
			txtTeleponDealer.Text = .TelponDealer
			txtNoBpkb.Text = .NoBPKB
			txtNoPolisi.Text = .NoPolisi
			txtNoFaktur.Text = .NoFaktur
			txtNik.Text = .NoNIK
			txtNoFormA.Text = .NoFormA
			txtAktaFidusia.Text = .AktaFidusia
			txtSertifikatFidusia.Text = .SertifikatFidusia
			txtKeterangan.Text = .Keterangan
			txtNilaiBPKB.Text = FormatNumber(.NilaiInvoice, 0) 'AS NILAI BPKB'

			If txtCurrency.Text = "" Then
				txtCurrency.Text = "IDR"
			End If

			If .JTBPKB = ConvertDate2("01/01/0001") Or Me.Action = "add" Then
				txtTglJtTempo.Text = ""
			ElseIf .JTBPKB = ConvertDate2("01/01/1900") Then
				txtTglJtTempo.Text = ""
			Else
				txtTglJtTempo.Text = .JTBPKB.ToString("dd/MM/yyyy")
			End If

			If .TglTerimaBPKB = ConvertDate2("01/01/0001") Or Me.Action = "add" Then
				txtTglterima.Text = ""
			ElseIf .TglTerimaBPKB = ConvertDate2("01/01/1900") Then
				txtTglterima.Text = ""
			Else
				txtTglterima.Text = .TglTerimaBPKB.ToString("dd/MM/yyyy")
			End If

			If .TglBPKB = ConvertDate2("01/01/0001") Or Me.Action = "add" Then
				txtTglBpkb.Text = ""
			ElseIf .TglBPKB = ConvertDate2("01/01/1900") Then
				txtTglBpkb.Text = ""
			Else
				txtTglBpkb.Text = .TglBPKB.ToString("dd/MM/yyyy")
			End If

			If .TglKeluarBPKBSementara = ConvertDate2("01/01/0001") Or Me.Action = "add" Then
				txtTglKeluarBpkb.Text = ""
			ElseIf .TglKeluarBPKBSementara = ConvertDate2("01/01/1900") Then
				txtTglKeluarBpkb.Text = ""
			Else
				txtTglKeluarBpkb.Text = .TglKeluarBPKBSementara.ToString("dd/MM/yyyy")
			End If

			If .TglClose = ConvertDate2("01/01/0001") Or Me.Action = "add" Then
				txtTglClose.Text = ""
			ElseIf .TglClose = ConvertDate2("01/01/1900") Then
				txtTglClose.Text = ""
			Else
				txtTglClose.Text = .TglClose.ToString("dd/MM/yyyy")
			End If

		End With
	End Sub

	Private Sub saveEditDataBPKB()
		Dim oPar As New Parameter.AgunanLain

		With oPar
			.strConnection = GetConnectionString()
			.BranchId = Me.BranchID
			.CustomerID = Me.CustomerID
			.CollateralID = Me.CollateralID

			.CollateralTypeID = cboJenisAgunan.SelectedValue()
			.PemilikCollateral = txtPemilikAgunan.Text

			If txtTglJtTempo.Text <> "" Then
				.JTBPKB = ConvertDate2(txtTglJtTempo.Text)
			Else
				.JTBPKB = ConvertDate2("01/01/1900")
			End If
			If txtTglterima.Text <> "" Then
				.TglTerimaBPKB = ConvertDate2(txtTglterima.Text)
			Else
				.TglInvoice = ConvertDate2("01/01/1900")
			End If
			If txtTglKeluarBpkb.Text <> "" Then
				.TglKeluarBPKBSementara = ConvertDate2(txtTglKeluarBpkb.Text)
			Else
				.TglKeluarBPKBSementara = ConvertDate2("01/01/1900")
			End If
			If txtTglClose.Text <> "" Then
				.TglClose = ConvertDate2(txtTglClose.Text)
			Else
				.TglClose = ConvertDate2("01/01/1900")
			End If
			If txtTglBpkb.Text <> "" Then
				.TglBPKB = ConvertDate2(txtTglBpkb.Text)
			Else
				.TglBPKB = ConvertDate2("01/01/1900")
			End If

			.NoRangka = txtNoRangka.Text
			.NoMesin = txtNoMesin.Text
			.NamaDiBPKB = txtNamaBpkb.Text
			.Merk = txtMerk.Text
			.Tipe = txtTipe.Text
			.Warna = txtWarna.Text
			.NamaDealer = txtNamaDealer.Text
			.AlamatDealer = txtAlamatDealer.Text
			.TelponDealer = txtTeleponDealer.Text
			.NoBPKB = txtNoBpkb.Text
			.NoPolisi = txtNoPolisi.Text
			.NoFaktur = txtNoFaktur.Text
			.NoNIK = txtNik.Text
			.NoFormA = txtNoFormA.Text
			.AktaFidusia = txtAktaFidusia.Text
			.SertifikatFidusia = txtSertifikatFidusia.Text
			.Keterangan = txtKeterangan.Text
			.LoginId = Me.Loginid
			.NilaiInvoice = txtNilaiBPKB.Text
		End With
		oPar = m_controller.saveEditDataBPKB(oPar)
	End Sub

	Private Sub saveAddDataBPKB()

		Dim oPar As New Parameter.AgunanLain

		With oPar
			.strConnection = GetConnectionString()
			.CustomerID = Me.CustomerID
			.CollateralID = Me.CollateralID
		End With

		oPar = m_controller.getDataBPKB(oPar)
		lblIDAgunan.Text = oPar.CollateralID
		Me.CollateralID = Request("CollateralID").Trim

		With oPar
			.strConnection = GetConnectionString()
			.BranchId = Me.sesBranchId.Trim.Replace("'", "")
			.CustomerID = Me.CustomerID

			If lblIDAgunan.Text = "" And Me.CollateralID = "" Then
				GetCollateralID()
				AddCollateral()
				.CollateralID = Me.CollateralID
			Else
				.CollateralID = Me.CollateralID
			End If

			.CollateralTypeID = cboJenisAgunan.SelectedValue()
			.CollateralStatus = cboStatusAgunan.SelectedValue()
			.Currency = "IDR"

			If txtTglJtTempo.Text <> "" Then
				.JTBPKB = ConvertDate2(txtTglJtTempo.Text)
			Else
				.JTBPKB = ConvertDate2("01/01/1900")
			End If
			If txtTglterima.Text <> "" Then
				.TglTerimaBPKB = ConvertDate2(txtTglterima.Text)
			Else
				.TglTerimaBPKB = ConvertDate2("01/01/1900")
			End If
			If txtTglKeluarBpkb.Text <> "" Then
				.TglKeluarBPKBSementara = ConvertDate2(txtTglKeluarBpkb.Text)
			Else
				.TglKeluarBPKBSementara = ConvertDate2("01/01/1900")
			End If
			If txtTglClose.Text <> "" Then
				.TglClose = ConvertDate2(txtTglClose.Text)
			Else
				.TglClose = ConvertDate2("01/01/1900")
			End If
			If txtTglBpkb.Text <> "" Then
				.TglBPKB = ConvertDate2(txtTglBpkb.Text)
			Else
				.TglBPKB = ConvertDate2("01/01/1900")
			End If

			.PemilikCollateral = txtPemilikAgunan.Text
			.NoRangka = txtNoRangka.Text
			.NoMesin = txtNoMesin.Text
			.NamaDiBPKB = txtNamaBpkb.Text
			.Merk = txtMerk.Text
			.Tipe = txtTipe.Text
			.Warna = txtWarna.Text
			.NamaDealer = txtNamaDealer.Text
			.AlamatDealer = txtAlamatDealer.Text
			.TelponDealer = txtTeleponDealer.Text
			.NoBPKB = txtNoBpkb.Text
			.NoPolisi = txtNoPolisi.Text
			.NoFaktur = txtNoFaktur.Text
			.NoNIK = txtNik.Text
			.NoFormA = txtNoFormA.Text
			.AktaFidusia = txtAktaFidusia.Text
			.SertifikatFidusia = txtSertifikatFidusia.Text
			.Keterangan = txtKeterangan.Text
			.LoginId = Me.Loginid
			.NilaiInvoice = txtNilaiBPKB.Text
		End With
		oPar = m_controller.saveAddDataBPKB(oPar)
	End Sub

	Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
		If Me.Action = "add" Then
			saveAddDataBPKB()
			Response.Redirect("CustomerJaminanTambahanList.aspx?id=" & Me.CustomerID)
			'cTabs.SetNavigateUrl(Request("page"), Request("CollateralId"), Request("CustomerId"))
			'cTabs.RefreshAttr(Request("pnl"))
			ShowMessage(lblMessage, "Data Berhasil Disimpan .....", False)
			'Response.Redirect("JTBpkb.aspx?page=add&CustomerID=" & Me.CustomerID & "&CollateralID=" & Me.CollateralID & "&pnl=tabBPKB")
		ElseIf Me.Action = "edit" Then
			saveEditDataBPKB()
			ShowMessage(lblMessage, "Data Berhasil Diupdate.....", False)
		Else
			Exit Sub
		End If
	End Sub

	Private Sub GetCollateralID()
		Dim oPar As New Parameter.AgunanLain
		Me.BranchID = Replace(sesBranchId, "'", "")
		With oPar
			.strConnection = GetConnectionString()
			.BranchId = Me.BranchID
			.CustomerID = Me.CustomerID
			.ID = "CLTR"
		End With

		Me.CollateralID = m_controller.GetCollateralId(oPar)
	End Sub

	Private Sub AddCollateral()
		Dim oPar As New Parameter.AgunanLain

		With oPar
			.strConnection = GetConnectionString()
			.CustomerID = Me.CustomerID
			.CollateralID = Me.CollateralID
			.CollateralAmount = 0
			.Keterangan = 1 'As Status'
			.LoginId = Me.Loginid
		End With
		oPar = m_controller.AddCollateral(oPar)
	End Sub

	Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
		Response.Redirect("CustomerJaminanTambahanList.aspx?id=" & Me.CustomerID)
	End Sub
End Class