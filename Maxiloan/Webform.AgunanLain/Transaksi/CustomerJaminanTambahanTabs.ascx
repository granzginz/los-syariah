﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CustomerJaminanTambahanTabs.ascx.vb" Inherits="Maxiloan.Webform.AgunanLain.CustomerJaminanTambahanTabs" %>
 <div class="tab_container">
        <div id="tabLandBuilding" runat="server">
            <asp:HyperLink ID="hyLandBuilding" runat="server" Text="TANAH & BANGUNAN"></asp:HyperLink>
        </div>
        <div id="tabBPKB" runat="server">
            <asp:HyperLink ID="hyBPKB" runat="server" Text="BPKB"></asp:HyperLink>
        </div>
        <div id="tabInvoice" runat="server">
            <asp:HyperLink ID="hyInvoice" runat="server" Text="INVOICE"></asp:HyperLink>
        </div>
     <div class="form_box_hide">
        <div id="tabSertifikat" runat="server">
            <asp:HyperLink ID="hySertifikat" runat="server" Text="SERTFIKAT" Enabled="false"></asp:HyperLink>
        </div>
        <div id="tabPinjamanLainnya" runat="server">
            <asp:HyperLink ID="hyPinjamanLainnya" runat="server" Text="PINJAMAN LAINNYA" Enabled="false"></asp:HyperLink>
        </div>   
     </div>
</div>