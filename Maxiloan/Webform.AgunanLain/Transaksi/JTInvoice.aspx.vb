﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Imports Maxiloan.Webform.UserController
#End Region

Public Class JTInvoice
	Inherits Maxiloan.Webform.WebBased
	Private m_controller As New AgunanLainController

#Region "Property"
	Private Property CustomerID() As String
		Get
			Return CType(ViewState("CustomerID"), String)
		End Get
		Set(ByVal Value As String)
			ViewState("CustomerID") = Value
		End Set
	End Property

	Private Property CollateralID() As String
		Get
			Return CType(ViewState("CollateralID"), String)
		End Get
		Set(ByVal Value As String)
			ViewState("CollateralID") = Value
		End Set
	End Property

	Private Property Action() As String
		Get
			Return CType(ViewState("page"), String)
		End Get
		Set(ByVal Value As String)
			ViewState("page") = Value
		End Set
	End Property

#End Region
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		lblMessage.Visible = False
		Page.Header.DataBind()

		If CheckForm(Me.Loginid, "CltrlTambahanCst", "MAXILOAN") Then
			If SessionInvalid() Then
				Exit Sub
			End If
		End If

		If Not Me.IsPostBack Then
			Me.Action = Request("page").Trim
			Me.CustomerID = Request("CustomerID").Trim
			Me.CollateralID = Request("CollateralID").Trim
			Me.BranchID = Me.sesBranchId.Trim.Replace("'", "")
			DoBind()

			If Me.Action = "add" Then
				getDataInvoice()
				If lblIDAgunan.Text <> "" Then
					Me.Action = "edit"
					cTabs.SetNavigateUrl(Me.Action, Request("CollateralId"), Request("CustomerId"))
					cTabs.RefreshAttr(Request("pnl"))

					getDataInvoice()
				Else
					cTabs.SetNavigateUrl(Request("page"), Request("CollateralId"), Request("CustomerId"))
					cTabs.RefreshAttr(Request("pnl"))
					lblIDAgunan.Text = Me.CollateralID
				End If
			ElseIf Me.Action = "edit" Then
				getDataInvoice()
				If lblIDAgunan.Text = "" Then
					Me.Action = "add"
					cTabs.SetNavigateUrl(Me.Action, Request("CollateralId"), Request("CustomerId"))
					cTabs.RefreshAttr(Request("pnl"))

					getDataInvoice()
					lblIDAgunan.Text = Me.CollateralID
				Else
					cTabs.SetNavigateUrl(Request("page"), Request("CollateralId"), Request("CustomerId"))
					cTabs.RefreshAttr(Request("pnl"))
				End If
			End If
			End If
	End Sub

	Sub DoBind()
		FillCombo()
		FillCbo(cboJenisAgunan, "TblCollateralType")
	End Sub

	Private Sub FillCombo()
		Dim strConn As String
		Dim dtCombo As New DataTable

		strConn = GetConnectionString()
	End Sub

	Sub FillCbo(ByVal cboName As DropDownList, ByVal Initial As String)
		Dim oAssetData As New Parameter.AgunanLain
		Dim oData As New DataTable
		oAssetData.strConnection = GetConnectionString()
		oAssetData.Initial = Initial
		oAssetData = m_controller.GetCboCollateralType(oAssetData)
		oData = oAssetData.ListData
		cboName.DataSource = oData
		cboName.DataTextField = "ID"
		cboName.DataValueField = "CollateralTypeID"
		cboName.DataBind()
		'cboName.Items.Insert(0, "Select One")
		'cboName.Items(0).Value = "0"
	End Sub

	Sub getDataInvoice()
		Dim oPar As New Parameter.AgunanLain

		With oPar
			.strConnection = GetConnectionString()
			.CustomerID = Me.CustomerID
			.CollateralID = Me.CollateralID
		End With

		oPar = m_controller.getDataInvoice(oPar)

		With oPar
			Me.BranchID = .BranchId
			cboJenisAgunan.SelectedValue = .CollateralTypeID
			cboStatusAgunan.SelectedValue = .CollateralStatus
			lblIDAgunan.Text = .CollateralID
			txtCurrency.Text = .Currency
			lblAgunanSeqNo.Text = "3"
			txtPemilikAgunan.Text = .PemilikCollateral
			txtObjek.Text = .Objek
			txtNoInvoice.Text = .NoInvoice
			txtPenerbitInvoice.Text = .PenerbitInvoice
			txtPenerimaInvoice.Text = .PenerimaInvoice
			txtLuasTanah.Text = FormatNumber(.NilaiInvoice, 0)

			If txtCurrency.Text = "" Then
				txtCurrency.Text = "IDR"
			End If

			If .TglKontrak = ConvertDate2("01/01/0001") Or Me.Action = "add" Then
				txtTglKontrak.Text = ""
			ElseIf .TglKontrak = ConvertDate2("01/01/1900") Then
				txtTglKontrak.Text = ""
			Else
				txtTglKontrak.Text = .TglKontrak.ToString("dd/MM/yyyy")
			End If

			If .TglInvoice = ConvertDate2("01/01/0001") Or Me.Action = "add" Then
				txtTglInvoice.Text = ""
			ElseIf .TglInvoice = ConvertDate2("01/01/1900") Then '
				txtTglInvoice.Text = ""
			Else
				txtTglInvoice.Text = .TglInvoice.ToString("dd/MM/yyyy")
			End If

		End With

		DoBind()
	End Sub

	Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
		Dim oPar As New Parameter.AgunanLain

		If Me.Action = "add" Then

			With oPar
				.strConnection = GetConnectionString()
				.CustomerID = Me.CustomerID
				.CollateralID = Me.CollateralID
			End With

			oPar = m_controller.getDataBPKB(oPar)
			lblIDAgunan.Text = oPar.CollateralID
			Me.CollateralID = Request("CollateralID").Trim

			With oPar
				.strConnection = GetConnectionString()
				.BranchId = Me.sesBranchId.Trim.Replace("'", "")
				.CustomerID = Me.CustomerID

				If lblIDAgunan.Text = "" And Me.CollateralID = "" Then
					GetCollateralID()
					AddCollateral()
					.CollateralID = Me.CollateralID
				Else
					.CollateralID = Me.CollateralID
				End If

				.CollateralTypeID = cboJenisAgunan.SelectedValue()
				.CollateralStatus = cboStatusAgunan.SelectedValue()
				.Currency = "IDR"

				.PemilikCollateral = txtPemilikAgunan.Text
				If txtTglKontrak.Text <> "" Then
					.TglKontrak = ConvertDate2(txtTglKontrak.Text)
				Else
					.TglKontrak = ConvertDate2("01/01/1900")
				End If
				If txtTglInvoice.Text <> "" Then
					.TglInvoice = ConvertDate2(txtTglInvoice.Text)
				Else
					.TglInvoice = ConvertDate2("01/01/1900")
				End If
				.Objek = txtObjek.Text
				.NoInvoice = txtNoInvoice.Text
				.PenerbitInvoice = txtPenerbitInvoice.Text
				.PenerimaInvoice = txtPenerimaInvoice.Text
				.NilaiInvoice = CDbl(txtLuasTanah.Text)
				.LoginId = Me.Loginid
			End With
			oPar = m_controller.saveAddDataInvoice(oPar)
			ShowMessage(lblMessage, "Data Berhasil Disimpan .....", False)
			Response.Redirect("CustomerJaminanTambahanList.aspx?id=" & Me.CustomerID)

			'Response.Redirect("JTInvoice.aspx?page=add&CustomerID=" & Me.CustomerID & "&CollateralID=" & Me.CollateralID & "&pnl=tabInvoice")

			'Me.CollateralID = Me.CollateralID
			'cTabs.SetNavigateUrl(Request("page"), Me.CollateralID, Me.CustomerID)
			'cTabs.RefreshAttr(Request("pnl"))			

		ElseIf Me.Action = "edit" Then
			With oPar
				.strConnection = GetConnectionString()
				.BranchId = Me.BranchID
				.CustomerID = Me.CustomerID
				.CollateralID = Me.CollateralID

				.CollateralTypeID = cboJenisAgunan.SelectedValue()
				.PemilikCollateral = txtPemilikAgunan.Text
				.Currency = txtCurrency.Text

				.PemilikCollateral = txtPemilikAgunan.Text
				If txtTglKontrak.Text <> "" Then
					.TglKontrak = ConvertDate2(txtTglKontrak.Text)
				Else
					.TglKontrak = ConvertDate2("01/01/1900")
				End If
				If txtTglInvoice.Text <> "" Then
					.TglInvoice = ConvertDate2(txtTglInvoice.Text)
				Else
					.TglInvoice = ConvertDate2("01/01/1900")
				End If
				.Objek = txtObjek.Text
				.NoInvoice = txtNoInvoice.Text
				.PenerbitInvoice = txtPenerbitInvoice.Text
				.PenerimaInvoice = txtPenerimaInvoice.Text
				.NilaiInvoice = CDbl(txtLuasTanah.Text)
				.LoginId = Me.Loginid
			End With
			oPar = m_controller.saveEditDataInvoice(oPar)
			ShowMessage(lblMessage, "Data Berhasil Diupdate.....", False)
		End If

	End Sub

	Private Sub GetCollateralID()
		Dim oPar As New Parameter.AgunanLain
		Me.BranchID = Replace(sesBranchId, "'", "")
		With oPar
			.strConnection = GetConnectionString()
			.BranchId = Me.BranchID
			.CustomerID = Me.CustomerID
			.ID = "CLTR"
		End With

		Me.CollateralID = m_controller.GetCollateralId(oPar)
	End Sub

	Private Sub AddCollateral()
		Dim oPar As New Parameter.AgunanLain

		With oPar
			.strConnection = GetConnectionString()
			.CustomerID = Me.CustomerID
			.CollateralID = Me.CollateralID
			.CollateralAmount = 0
			.Keterangan = 1 'As Status'
			.LoginId = Me.Loginid
		End With
		oPar = m_controller.AddCollateral(oPar)
	End Sub

	Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
		Response.Redirect("CustomerJaminanTambahanList.aspx?id=" & Me.CustomerID)
	End Sub
End Class