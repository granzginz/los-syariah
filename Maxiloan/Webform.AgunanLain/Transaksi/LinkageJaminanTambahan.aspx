﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="LinkageJaminanTambahan.aspx.vb" Inherits="Maxiloan.Webform.AgunanLain.LinkageJaminanTambahan" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %>  

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Company</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../js/jquery-1.9.1.min.js" type="text/javascript"></script>	
    <script src="../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
	<script src="../../js/jquery-2.1.1.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function OpenCust(CustomerID, pStyle) {
            var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            var x = screen.width;
            var y = screen.height - 100;
            window.open('http://<%# Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.LoanOrg/Credit/ViewPersonalCustomer.aspx?Style=' + pStyle + '&CustomerID=' + CustomerID, 'UserLookup', 'left = 0, top = 0, width = ' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }

        function windowClose() {
            window.close();
        }

        function fConfirm() {
            if (window.confirm("Apakah yakin mau hapus data ini ?"))
                return true;
            else
                return false;
        }

        function fback() {
            history.go(-1);
            return false;
        }

        function OpenWindowCompany(pCompanyID) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            //function window open error salah penulisan url
            //window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.Setup/Organization/CompanyView.aspx?CompanyID=' + pCompanyID, 'CompanyView', 'left=15, top=10, width=985, height=480, menubar=0, scrollbars=yes');
            //
            var x = screen.width; var y = screen.height - 100;
            window.open('CompanyView.aspx?CompanyID=' + pCompanyID, 'CompanyView', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }

    function OpenAgreement(pAgreementNo, pApplicationID, pBranchAgreement, pTotalOTR, pNTF , pStyle) {
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        //        window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/General/LookUpAgreement.aspx?style=' + pStyle + '&agreementno=' + pAgreementNo + '&applicationid=' + pApplicationID + '&branchagreement=' + pBranchAgreement, 'AgreementLookup', 'left=50, top=10, width=900, height=600, menubar=0, scrollbars=yes');
        //window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.UserController/LookUpAgreement.aspx?style=' + pStyle + '&agreementno=' + pAgreementNo + '&applicationid=' + pApplicationID + '&branchagreement=' + pBranchAgreement + '&TotalOTR=' + pTotalOTR + '&NTF=' + pNTF, 'AgreementLookup', 'left=50, top=10, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.AgunanLain/Transaksi/LookupAgreementLinkage.aspx?style=' + pStyle + '&agreementno=' + pAgreementNo + '&applicationid=' + pApplicationID + '&branchagreement=' + pBranchAgreement + '&TotalOTR=' + pTotalOTR + '&NTF=' + pNTF, 'AgreementLookup', 'left=50, top=10, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
    }

        const openAgunanLainTab = (CustomerID, CollateralID) => {
            const AppInfo = '<%= Request.ServerVariables("PATH_INFO")%>';
            const App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
            window.open('http://<%# Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.AgunanLain/Transaksi/ViewCollateralLandBuilding.aspx?CollateralID=' + CollateralID + '&CustomerID=' + CustomerID, 'UserLookup', 'left = 0, top = 0, width = ' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }
       
    </script>
</head>
<body>
    <script type = "text/javascript">
    //function recalculatePledging() {

    //    var ntf = $('#txtNTF').val();
    //    var pledpercen = $('#txtPledgingPercentage').val();  

    //    var xtotal = parseInt(ntf.replace(/\s*,\s*/g, '')) * parseInt(uangmuka.replace(/\s*,\s*/g, ''));
    //    alert(xtotal);
    //    xtotal = xtotal /100
    //    $('#txtPledgingAmount').val(number_format(xtotal));
        //}
    </script>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="upnl1" UpdateMode="Conditional" ChildrenAsTriggers="false">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
            <asp:Panel runat="server" ID="pnlPencairan">
                <div class="form_title">
                    <div class="title_strip">
                    </div>
                    <div class="form_single">
                        <h3>
                            LINKAGE JAMINAN TAMBAHAN
                        </h3>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                         <label>Nama</label>
                        <asp:Label ID="lblNama" runat="server" ></asp:Label>
                     
                    </div>
                    <div class="form_right">
                         <label> ID Agunan</label>
                        <%--<asp:Label ID="lblCollateralID" runat="server"></asp:Label>--%>
                        <asp:HyperLink ID="hyCollateralID" runat="server" />
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                         <label>ID Customer</label>
                        <asp:Label ID="lblCustomerID" runat="server" ></asp:Label>
                    </div>
                    <div class="form_right">
                         
                    </div>
                </div>
                <div class="form_box_title">
                    <div class="form_left">
                        <h5>
                            JAMINAN TAMBAHAN</h5>
                    </div>
                    <div class="form_right">
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_left">
                        <asp:GridView runat="server" ID="gvAgunanNett" AllowPaging="false" AutoGenerateColumns="false"
                            DataKeyNames="TransID,ApplicationID" CssClass="grid_general" ShowFooter="true"
                            ShowHeader="false">
                            <HeaderStyle CssClass="th" />
                            <RowStyle CssClass="item_grid" />
                            <FooterStyle CssClass="item_grid ft" />
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblTransDesc1" Text='<%# Container.DataItem("TransDesc") %>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <label class="label_auto">
                                            Total Nilai Jaminan Tambahan</label>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField  HeaderStyle-CssClass="th_right" ItemStyle-CssClass="item_grid_right" FooterStyle-CssClass="item_grid_right ft">
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblNilai" Text='<%#FormatNumber(Container.DataItem("Nilai"), 0) %>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label runat="server" ID="lblTotalNilai"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="AgunanNett" />
                            </Columns>
                        </asp:GridView>
                    </div>
                    <div class="form_right">
                    </div>
                </div>
                <div class="form_title">
                    <div class="form_single">
                        <h4>Pemilihan Kontrak &nbsp;&nbsp;<asp:Label runat="server" ID="Label1" /></label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                         <label>Pilih Kontrak</label>
                            <asp:TextBox runat="server" ID="txtAgreementNo" Enabled="false"></asp:TextBox>
                            <asp:HyperLink ID="hpLookup" runat="server" ImageUrl="../../images/IconDetail.gif"></asp:HyperLink>
                    </div>
                    <div class="form_right"  style="background-color:#1eefbc">
                        <label><b>Sisa Yang Belum Dialokasi</b></label>
                        <b><asp:Label ID="lblSisaAlokasi" runat="server" ></asp:Label></b>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>Total OTR</label>
                        <asp:TextBox runat="server" ID="txtTotalOTR" Enabled="false"></asp:TextBox>
                    </div>
                    <div class="form_right">

                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>Total Pembiayaan (NTF) </label>
                        <asp:TextBox runat="server" ID="txtNTF" Enabled="false"></asp:TextBox>
                    </div>
                    <div class="form_right">

                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>Pledging Persentase</label>
                        <asp:TextBox runat="server" ID="txtPledgingPercentage" Width="35" MaxLength="5" OnKeyUp="recalculatePledging()"></asp:TextBox>
                        <label> %</label>
                    </div>
                    <div class="form_right">

                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Nilai Pledging</label>
                        <asp:TextBox runat="server" ID="txtPledgingAmount" onKeyUp="recalculatePledgingByRate()"></asp:TextBox>
                    </div>
                    <div class="form_right">
                    <asp:Button ID="BtnAddNew" runat="server" CausesValidation="False" Text="Add"
                        CssClass="small button green"></asp:Button>
                    </div>
                </div>
            <div class="form_box_title">
                <div class="form_single">
                    <h5>
                        Daftar Kontrak Yang Sudah Dijaminkan</h5>
                </div>
            </div>  
        <div class="form_box_header">
            <div class="form_single"><div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgPledging" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                  CssClass="grid_general"
                    ShowFooter="True">
                 <HeaderStyle CssClass="th" />
                      <ItemStyle CssClass="item_grid" />
                        <FooterStyle CssClass="item_grid" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="ID Customer" SortExpression="CustomerID">
                            <ItemStyle HorizontalAlign="Left" Width="18%"></ItemStyle>
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkCustomerID" runat="server" CausesValidation="false" Text='<%# DataBinder.Eval(Container, "DataItem.CustomerID") %>'>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="Name" HeaderText="Nama Konsumen">
                            <HeaderStyle HorizontalAlign="Center" Width="22%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" Width="22%"></ItemStyle>
                            <ItemTemplate>
                                <asp:HyperLink ID="hyCustomerName" runat="server" Text='<%#Container.DataItem("Name")%>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="ID Data" SortExpression="ApplicationID">
                            <ItemStyle HorizontalAlign="Left" Width="18%"></ItemStyle>
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkApplicationID" runat="server" CausesValidation="false" Text='<%# DataBinder.Eval(Container, "DataItem.ApplicationID") %>'>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="No Kontrak" SortExpression="AgreementNo">
                            <ItemStyle HorizontalAlign="Left" Width="18%"></ItemStyle>
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkAgreementNo" runat="server" CausesValidation="false" Text='<%# DataBinder.Eval(Container, "DataItem.AgreementNo") %>'>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="ID Collateral" SortExpression="CollateralID">
                            <ItemStyle HorizontalAlign="Left" Width="18%"></ItemStyle>
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkCollateralID" runat="server" CausesValidation="false" Text='<%# DataBinder.Eval(Container, "DataItem.CollateralID") %>'>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="NTF" HeaderText="NTF">
                            <HeaderStyle CssClass="th_right" Width="10%"></HeaderStyle>
                            <ItemStyle CssClass="item_grid_right" Width="10%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblNTF" runat="server" Text='<%#FormatNumber(Container.DataItem("NTF"), 2)%>'>
                                </asp:Label>
                            </ItemTemplate>
                           <FooterTemplate>
                               <label class="label_auto">
                                   Total </label>
                           </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="PercentagePledged" HeaderText="Pledging Persentase">
                            <HeaderStyle CssClass="th_right" Width="10%"></HeaderStyle>
                            <ItemStyle CssClass="item_grid_right" Width="10%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblPercentagePledged" runat="server" Text='<%#FormatNumber(Container.DataItem("PercentagePledged"), 2)%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Pledging Amount">
                            <HeaderStyle CssClass="th_right" Width="10%"></HeaderStyle>
                            <ItemStyle CssClass="item_grid_right" Width="10%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblPledgedAmount" runat="server" Text='<%#FormatNumber(Container.DataItem("AmountPledged"), 2)%>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterStyle HorizontalAlign="Right"></FooterStyle>
                            <FooterTemplate>
                                <asp:Label ID="lblTotalPledgingAmount" runat="server">Label</asp:Label>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <PagerStyle Visible="False" HorizontalAlign="Left"  
                        Mode="NumericPages"></PagerStyle>
                </asp:DataGrid>
            </div></div>
        </div>
                <input type="hidden" id="hdnApplicationID" runat="server" name="hdnApplicationID"
                    class="inptype" />
                <input type="hidden" id="hdnBranchAgreement" runat="server" name="hdnBracnhAgreement"
                    class="inptype" />
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="BtnAddNew" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
    </form>
    <script type="text/javascript">
        $(document).ready(function () {
            recalculatePledging()
        });

        function recalculatePledging()
        {
            
            //Get ntf
            let AmountString = document.getElementById('txtNTF').value;
            let Amount = parseInt(AmountString.replace(/\s*,\s*/g, ''));
            console.log(Amount)

            //Get pledpercent
            let PercentStr = document.getElementById('txtPledgingPercentage').value;
            let PercentAmount = parseFloat(PercentStr.replace(/\s*,\s*/g, ''));
            console.log(PercentAmount)
            
            let totalbiaya = Amount * PercentAmount / 100;
            console.log(totalbiaya);

            $('#txtPledgingAmount').val(number_format(totalbiaya, 0));
            <%--$('#<%=hdfPledgingAmount.ClientID %>').val(totalbiaya.toFixed(0));--%>

            if (totalbiaya > Amount) {
                let Rate = 0.00
                alert("Nilai Pledging Tidak Valid!")
                $('#txtPledgingAmount').val(number_format(Amount, 0));
                $('#txtPledgingPercentage').val(number_format(Rate, 0));
                recalculatePledgingByRate()
            }
            
            let SisaAlokasi = document.getElementById('lblSisaAlokasi').innerHTML
            let TotalSisaAlokasi = parseInt(SisaAlokasi.replace(/\s*,\s*/g, ''));
            console.log(TotalSisaAlokasi)

            if (totalbiaya > TotalSisaAlokasi) {
                let Rate = 0.00
                let Pledging = 0
                alert("Nilai Pledging Melebihi Dari Sisa Alokasi!")
                $('#txtPledgingPercentage').val(number_format(Rate, 2));
                $('#txtPledgingAmount').val(number_format(Rate, 0));
            }

        }

        function recalculatePledgingByRate()
        {
            
            //Get ntf
            let AmountString = document.getElementById('txtNTF').value;
            let Amount = parseInt(AmountString.replace(/\s*,\s*/g, ''));
            console.log(Amount)

            //Get PledgingAmount
            let PledgingAmount = document.getElementById('txtPledgingAmount').value;
            let Pledging = parseInt(PledgingAmount.replace(/\s*,\s*/g, ''));
            $('#txtPledgingAmount').val(number_format(Pledging, 0));
            console.log(Pledging)

            let Rate = (Pledging / Amount) * 100
            console.log(Rate)

            $('#txtPledgingPercentage').val(number_format(Rate, 2));
            <%--$('#<%=hdfPledgingAmount.ClientID %>').val(totalbiaya.toFixed(0));--%>

            if (Rate > 100) {
                //let Rate = 0
                //let Pledging = 0
                //alert("Nilai Pledging Tidak Valid!")
                //$('#txtPledgingPercentage').val(number_format(Rate, 2));
                //$('#txtPledgingAmount').val(number_format(Rate, 0));
                //console.log(Rate)
                let Rate = 0.00
                alert("Nilai Pledging Tidak Valid!")
                $('#txtPledgingAmount').val(number_format(Amount, 0));
                $('#txtPledgingPercentage').val(number_format(Rate, 0));
                recalculatePledging()
            }

            let SisaAlokasi = document.getElementById('lblSisaAlokasi').innerHTML
            let TotalSisaAlokasi = parseInt(SisaAlokasi.replace(/\s*,\s*/g, ''));
            console.log(TotalSisaAlokasi)

            if (Pledging > TotalSisaAlokasi) {
                let Rate = 0.00
                let Pledging = 0
                alert("Nilai Pledging Melebihi Dari Sisa Alokasi!")
                $('#txtPledgingPercentage').val(number_format(Rate, 2));
                $('#txtPledgingAmount').val(number_format(Rate, 0));
            }
        }

        function number_format(number, decimals, dec_point, thousands_sep) {

            number = (number + '')
            .replace(/[^0-9+\-Ee.]/g, '');
            var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function (n, prec) {
                var k = Math.pow(10, prec);
                return '' + (Math.round(n * k) / k)
                .toFixed(prec);
            };
            // Fix for IE parseFloat(0.55).toFixed(0) = 0;
            s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
            .split('.');
            if (s[0].length > 3) {
                s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
            }
            if ((s[1] || '')
            .length < prec) {
                s[1] = s[1] || '';
                s[1] += new Array(prec - s[1].length + 1)
              .join('0');
            }
            return s.join(dec);
        }
    </script>
</body>
</html>
