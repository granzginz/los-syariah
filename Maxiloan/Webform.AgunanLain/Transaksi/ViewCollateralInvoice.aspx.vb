﻿#Region "Imports"
Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.General
Imports Maxiloan.General.CommonCookiesHelper
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Imports Maxiloan.Webform.UserController

#End Region
Public Class ViewCollateralInvoice
	Inherits Maxiloan.Webform.WebBased
#Region "Property"
	Public Property Style() As String
		Get
			Return CType(ViewState("Style"), String)
		End Get
		Set(ByVal Value As String)
			ViewState("Style") = Value
		End Set
	End Property

	Private Property CustomerID() As String
		Get
			Return CType(ViewState("CustomerID"), String)
		End Get
		Set(ByVal Value As String)
			ViewState("CustomerID") = Value
		End Set
	End Property

	Private Property CollateralID() As String
		Get
			Return CType(ViewState("CollateralID"), String)
		End Get
		Set(ByVal Value As String)
			ViewState("CollateralID") = Value
		End Set
	End Property
	Dim c_AgunanLain As New AgunanLainController
	Protected WithEvents ucLandTab1 As ucAgunanLainInvoiceViewTab
#End Region
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		lblMessage.Visible = False
		Page.Header.DataBind()
		If SessionInvalid() Then
			Exit Sub
		End If
		If Not IsPostBack Then
			Me.Style = Request.QueryString("style")

			If Request.QueryString("CollateralID").Trim <> "" Then Me.CollateralID = Request.QueryString("CollateralID")
			If Request.QueryString("CustomerID").Trim <> "" Then Me.CustomerID = Request.QueryString("CustomerID")

			Dim oAgunanLain As New Parameter.AgunanLain

			With oAgunanLain
				.strConnection = GetConnectionString()
				.CollateralID = Me.CollateralID
				.CustomerID = Me.CustomerID
			End With

			ucLandTab1.CollateralID = Me.CollateralID
			ucLandTab1.CustomerID = Me.CustomerID
			ucLandTab1.setLink()
			ucLandTab1.selectedTab("Invoice")
			DoBind()

			oAgunanLain = c_AgunanLain.getDataInvoice(oAgunanLain)
			With oAgunanLain
				lblIDAgunan.Text = .CollateralID
				lbl_Currency.Text = .Currency
				lbl_AgunanSeqNo.Text = .CollateralSeqNo
				lbl_PemilikAgunan.Text = .PemilikCollateral
				'lbl_TglKontrak.Text = .TglKontrak
				lbl_Objek.Text = .Objek
				lbl_NoInvoice.Text = .NoInvoice
				'lbl_TglInvoice.Text = .TglInvoice
				lbl_PenerbitInvoice.Text = .PenerbitInvoice
				lbl_PenerimaInvoice.Text = .PenerimaInvoice
				lbl_LuasTanah.Text = .NilaiInvoice

				If .TglKontrak = ConvertDate2(" 01/01/0001") Then
					lbl_TglKontrak.Text = ""
				Else
					lbl_TglKontrak.Text = .TglKontrak.ToString("dd/MM/yyyy")
				End If

				If .TglInvoice = ConvertDate2(" 01/01/0001") Then
					lbl_TglInvoice.Text = ""
				Else
					lbl_TglInvoice.Text = .TglInvoice.ToString("dd/MM/yyyy")
				End If
			End With
		End If
	End Sub
	Sub DoBind()
		FillCombo()
		FillCbo(cboJenisAgunan, "TblCollateralType")
	End Sub
	Private Sub FillCombo()
		Dim strConn As String
		Dim dtCombo As New DataTable

		strConn = GetConnectionString()
	End Sub

	Sub FillCbo(ByVal cboName As DropDownList, ByVal Initial As String)
		Dim oAssetData As New Parameter.AgunanLain
		Dim oData As New DataTable
		oAssetData.strConnection = GetConnectionString()
		oAssetData.Initial = Initial
		oAssetData = c_AgunanLain.GetCboCollateralType(oAssetData)
		oData = oAssetData.ListData
		cboName.DataSource = oData
		cboName.DataTextField = "ID"
		cboName.DataValueField = "CollateralTypeID"
		cboName.DataBind()
		cboName.Items.Insert(0, "Select One")
		cboName.Items(0).Value = "0"
	End Sub
End Class