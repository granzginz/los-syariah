﻿#Region "Imports"
Imports System.IO
Imports Maxiloan
Imports Maxiloan.Exceptions
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
#End Region

Public Class CustomerJaminanTambahanList

    Inherits Maxiloan.Webform.WebBased



#Region " Private Const "
    Dim m_AgunanLain As New AgunanLainController

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Protected WithEvents GridNavigator As ucGridNav
    Dim filterBranch As String
    Private m_controller As New CustomerController

#End Region
#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property ProspectAppID() As String
        Get
            Return CType(ViewState("ProspectAppID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ProspectAppID") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property
	Private Property CustomerID() As String
		Get
			Return CType(ViewState("CustomerID"), String)
		End Get
		Set(ByVal Value As String)
			ViewState("CustomerID") = Value
		End Set
	End Property
	Private Property CollateralID() As String
		Get
			Return CType(ViewState("CollateralId"), String)
		End Get
		Set(ByVal Value As String)
			ViewState("CollateralId") = Value
		End Set
	End Property
#End Region

#Region "Page_Load"
	Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If
        AddHandler GridNavigator.PageChanged, AddressOf PageNavigation
        Me.CustomerID = Request("id")

        filterBranch = " vwCustomer.customerId like '" & Replace(Me.CustomerID, "'", "") & "%' "


        If Not Me.IsPostBack Then

            If CheckForm(Me.Loginid, "CltrlTambahanCst", "MAXILOAN") Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If

            If IsSingleBranch() And Me.IsHoBranch = False Then
                Me.Sort = "Name ASC"

                If Request("cond") <> "" Then
                    Me.CmdWhere = Request("cond")
                Else
                    Me.CmdWhere = filterBranch
                End If

                BindGridEntity(Me.CmdWhere)

                Me.SortBy = "Name ASC"

            Else
                Dim strHTTPServer As String
                Dim StrHTTPApp As String
                Dim strNameServer As String
                strHTTPServer = Request.ServerVariables("PATH_INFO")
                strNameServer = Request.ServerVariables("SERVER_NAME")
                StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                Response.Redirect("http: //" & strNameServer & "/" & StrHTTPApp & "/error_notauthorized.aspx")
            End If
        End If
    End Sub
#End Region

    Private Sub setBranchName()
        If ViewState("branchName") Is Nothing OrElse ViewState("branchName").ToString = String.Empty Then
            Dim con As New BranchController
            Dim data As New Parameter.Branch
            With data
                .strConnection = GetConnectionString()
                .BranchId = Replace(sesBranchId, "'", "")
            End With
            data = con.BranchList(data)
            BranchName = data.BranchName
        Else
            BranchName = ViewState("branchName").ToString

        End If

    End Sub

#Region "InitialDefaultPanel"
    Private Sub InitialDefaultPanel()
        pnlList.Visible = True
        lblMessage.Visible = False
    End Sub
#End Region

#Region "BindGridEntity"
    Sub BindGridEntity(ByVal cmdWhere As String, Optional isFrNav As Boolean = False)
        Dim dtEntity As DataTable = Nothing
        Dim oCustomClass As New Parameter.AgunanLain
        InitialDefaultPanel()


        oCustomClass.PageSize = CType(pageSize, Int16)
        oCustomClass.WhereCond = cmdWhere
        oCustomClass.SortBy = Me.Sort
        oCustomClass.CurrentPage = currentPage
        oCustomClass.strConnection = GetConnectionString()
        oCustomClass = m_AgunanLain.ListAgunan(oCustomClass)
        If Not oCustomClass Is Nothing Then
            dtEntity = oCustomClass.listdata
            recordCount = CType(oCustomClass.totalrecords, Integer)
        Else
            recordCount = 0
        End If
        dtgCust.DataSource = dtEntity.DefaultView
        dtgCust.CurrentPageIndex = 0
        dtgCust.DataBind()

        If (isFrNav = False) Then
            GridNavigator.Initialize(recordCount, pageSize)
        End If
    End Sub

    Private Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        currentPage = e.CurrentPage
        BindGridEntity(Me.CmdWhere, True)
        GridNavigator.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
    End Sub
#End Region

#Region "ItemCommand"
    Private Sub dtgEntity_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgCust.ItemCommand
        If e.CommandName = "ListJaminanTambahanList" Then
			'Response.Redirect("JTLandBuilding.aspx?id=" & e.Item.Cells(5).Text.Trim & "")
			Response.Redirect("JTLandBuilding.aspx?page=edit&pc=c&branchID=" & Me.BranchID & "&CustomerID=" & Me.CustomerID & "&CollateralID=" & e.Item.Cells(2).Text.Trim & "")
		End If
    End Sub
#End Region
#Region "Sorting"
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgCust.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub
#End Region

    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgCust.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim lnkCustomer As LinkButton
            Dim lblCustomer As Label
            Dim imgEditPersonal As ImageButton
            lnkCustomer = CType(e.Item.FindControl("lnkCustomer"), LinkButton)
            lblCustomer = CType(e.Item.FindControl("lblCust"), Label)
            lnkCustomer.Attributes.Add("OnClick", "return OpenCust('" & lblCustomer.Text & "','accacq');")
            imgEditPersonal = CType(e.Item.FindControl("imgEditPersonal"), ImageButton)

            If e.Item.Cells(3).Text.ToUpper.Trim = "C" Then
                imgEditPersonal.Visible = False
                Response.Redirect("Customer_002.aspx?pc=p&branchID=" & Me.BranchID & "&prospectappid=" & Me.ProspectAppID)
            End If

        End If
    End Sub

	Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnAdd.Click
		'Response.Redirect("JTLandBuilding.aspx?pc=c&branchID=" & Me.BranchID & "&prospectappid=" & Me.ProspectAppID)
		'GetCollateralID()
		'AddCollateral()
		Response.Redirect("JTLandBuilding.aspx?page=add&CustomerID=" & Me.CustomerID & "&CollateralID=" & Me.CollateralID)
	End Sub

	'Private Sub GetCollateralID()
	'	Dim oPar As New Parameter.AgunanLain
	'	Me.BranchID = Replace(sesBranchId, "'", "")
	'	With oPar
	'		.strConnection = GetConnectionString()
	'		.BranchId = Me.BranchID
	'		.CustomerID = Me.CustomerID
	'		.ID = "CLTR"
	'	End With

	'	Me.CollateralID = m_AgunanLain.GetCollateralId(oPar)
	'End Sub

	'Private Sub AddCollateral()
	'	Dim oPar As New Parameter.AgunanLain

	'	With oPar
	'		.strConnection = GetConnectionString()
	'		.CustomerID = Me.CustomerID
	'		.CollateralID = Me.CollateralID
	'		.CollateralAmount = 0
	'		.Keterangan = 1 'As Status'
	'		.LoginId = Me.Loginid
	'	End With
	'	oPar = m_AgunanLain.AddCollateral(oPar)
	'End Sub

	Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        Response.Redirect("CustomerJaminanTambahan.aspx")
    End Sub
End Class