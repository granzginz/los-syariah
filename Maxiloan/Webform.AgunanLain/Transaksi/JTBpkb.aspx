﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="JTBpkb.aspx.vb" Inherits="Maxiloan.Webform.AgunanLain.JTBpkb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ucAddress" Src="../../Webform.UserController/ucCompanyAddress.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcCompanyAdress" Src="../../Webform.UserController/ucCompanyAddress.ascx" %>
<%@ Register TagPrefix="uc6" TagName="ucDateCE" Src="../../Webform.UserController/ucDateCE.ascx" %>
<%@ Register Src="../../webform.UserController/ucMonthCombo.ascx" TagName="ucMonthCombo"
    TagPrefix="uc2" %>
<%@ Register Src="../../webform.UserController/ucLookupGroupCust.ascx" TagName="ucLookupGroupCust"
    TagPrefix="uc5" %>
<%@ Register TagPrefix="uc1" TagName="ucNumberFormat" Src="../../Webform.UserController/ucNumberFormat.ascx" %>

<%@ Register Src="CustomerJaminanTambahanTabs.ascx" TagName="tabs" TagPrefix="uct" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Customer Personal</title>
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>  
    <style type="text/css">
        .multiline_textbox {
            width: 60% !important;
        }
        .color_yellow {
            background-color: yellow;
        }
        .rightAlign {
            text-align: right
        }
    </style>
</head>
<body onload="gridGeneralSize('dtgTC');getGridGeneralSize('hdnGridGeneralSize');">
    <asp:UpdateProgress ID="upg1" runat="server" AssociatedUpdatePanelID="updatePanel1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
      <uct:tabs id='cTabs' runat='server'></uct:tabs> 
        <div runat="server" id="jlookupContent" />
    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
                onclick="hideMessage();"></asp:Label>
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h4> <asp:Label ID="lblTitle" runat="server"></asp:Label> BPKB </h4>
                </div>
            </div>
            <asp:Panel runat="server" ID="pnlIdentitas">
                <div class="form_box">
                    <div class="form_left">
                    <label>Jenis Agunan</label>
                        <asp:DropDownList ID="cboJenisAgunan" runat="server" onchange="cboPHomeStatus_IndexChanged(this.value);">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" Display="Dynamic"
                            InitialValue="Select One" ControlToValidate="cboJenisAgunan" ErrorMessage="Harap pilih jenis Agunan"
                            CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                    <div class="form_right">
                         <label> ID Agunan</label>
                        <asp:Label ID="lblIDAgunan" runat="server" CssClass="label_auto"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Status Agunan</label>
                        <asp:DropDownList ID="cboStatusAgunan" runat="server" onchange="StatusAgunan()">
                            <asp:ListItem Value="0">On Hand</asp:ListItem>
                            <asp:ListItem Value="1">Dijaminkan</asp:ListItem>
                            <asp:ListItem Value="2">Lunas</asp:ListItem>
                            <asp:ListItem Value="3">Ditarik</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="form_right">          
                        <label> Currency</label>
                        <asp:TextBox ID="txtCurrency" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Agunan Seq No</label>
<%--                        <asp:TextBox ID="txtAgunanSeqNo" runat="server" Width="40%"></asp:TextBox>--%>
                        <asp:label runat="server" ID="lblAgunanSeqNo"></asp:label>
                    </div>
                    <div class="form_right">

                    </div>
                </div>
            <div class="form_title">
                <div class="form_single">
                    <h4>Identitas Pemilik Agunan &nbsp;&nbsp;<asp:Label runat="server" ID="Label1" /></label>
                </div>
            </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Pemilik Agunan</label>
                        <asp:TextBox ID="txtPemilikAgunan" runat="server" Width="40%"></asp:TextBox>
                    </div>
                    <div class="form_right">          
                        
                    </div>
                </div> 
            <div class="form_title">
                <div class="form_single">
                    <h4>Data BPKB &nbsp;&nbsp;<asp:Label runat="server" ID="Label2" /></label>
                </div>
            </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Nama di BPKB</label>
                        <asp:TextBox ID="txtNamaBpkb" runat="server" Width="40%"></asp:TextBox>
                    </div>
                    <div class="form_right">
                        <label> No BPKB</label>
                        <asp:TextBox ID="txtNoBpkb" runat="server" ></asp:TextBox>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Tanggal BPKB</label>
                        <asp:TextBox ID="txtTglBpkb" runat="server" Width="10%"></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True" TargetControlID="txtTglBpkb" Format="dd/MM/yyyy" PopupPosition="BottomRight" DefaultView="Months"></asp:CalendarExtender>
                    </div>
                    <div class="form_right">          
                        <label> No Polisi</label>
                        <asp:TextBox ID="txtNoPolisi" runat="server" ></asp:TextBox>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> No Rangka</label>
                        <asp:TextBox ID="txtNoRangka" runat="server" ></asp:TextBox>
                    </div>
                    <div class="form_right">          
                        <label> No Mesin</label>
                        <asp:TextBox ID="txtNoMesin" runat="server" ></asp:TextBox>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Merk</label>
                        <asp:TextBox ID="txtMerk" runat="server" ></asp:TextBox>
                    </div>
                    <div class="form_right">          
                        <label> Tipe</label>
                        <asp:TextBox ID="txtTipe" runat="server" ></asp:TextBox>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Warna</label>
                        <asp:TextBox ID="txtWarna" runat="server" ></asp:TextBox>
                    </div>
                    <div class="form_right color_yellow">          
                        <label> Nilai BPKB</label>
                        <asp:TextBox ID="txtNilaiBPKB" runat="server" CssClass="rightAlign" onkeyup="number()"></asp:TextBox>
                    </div>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Tgl Jatuh Tempo BPKB</label>
                        <asp:TextBox ID="txtTglJtTempo" runat="server" Width="10%"></asp:TextBox>   
                        <asp:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True" TargetControlID="txtTglJtTempo" Format="dd/MM/yyyy" PopupPosition="BottomRight" DefaultView="Months"></asp:CalendarExtender>                     
                    </div>
                    <div class="form_right">          
                        <label> Tgl Terima BPKB</label>
                        <asp:TextBox ID="txtTglterima" runat="server" Width="10%"></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender3" runat="server" Enabled="True" TargetControlID="txtTglterima" Format="dd/MM/yyyy" PopupPosition="BottomRight" DefaultView="Months"></asp:CalendarExtender>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> No Faktur</label>
                        <asp:TextBox ID="txtNoFaktur" runat="server"></asp:TextBox>
                    </div>
                    <div class="form_right">          
                        <label> No NIK</label>
                        <asp:TextBox ID="txtNik" runat="server" ></asp:TextBox>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> No Form A</label>
                        <asp:TextBox ID="txtNoFormA" runat="server"></asp:TextBox>
                    </div>
                    <div class="form_right">          
                        <label> Akta Fidusia</label>
                        <asp:TextBox ID="txtAktaFidusia" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Sertifikat Fidusia</label>
                        <asp:TextBox ID="txtSertifikatFidusia" runat="server"></asp:TextBox>
                    </div>
                    <div class="form_right">          
                        <label> Tgl Keluar BPKB Sementara</label>
                        <asp:TextBox ID="txtTglKeluarBpkb" runat="server" Width="10%"></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender4" runat="server" Enabled="True" TargetControlID="txtTglKeluarBpkb" Format="dd/MM/yyyy" PopupPosition="BottomRight" DefaultView="Months"></asp:CalendarExtender>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Keterangan</label>
                        <asp:TextBox ID="txtKeterangan" runat="server"></asp:TextBox>
                    </div>
                    <div class="form_right">          
                        <label> Tgl Close</label>
                        <asp:TextBox ID="txtTglClose" runat="server" Width="10%"></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender5" runat="server" Enabled="True" TargetControlID="txtTglClose" Format="dd/MM/yyyy" PopupPosition="BottomRight" DefaultView="Months"></asp:CalendarExtender>
                    </div>
                </div> 
                <div class="form_box">
                    <div class="form_single" style="background-color:#f9f9f9">
                        <h4>Data Dealer &nbsp;&nbsp;<asp:Label runat="server" ID="Label4" /></label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Nama Dealer</label>
                        <asp:TextBox ID="txtNamaDealer" runat="server" Width="40%"></asp:TextBox>
                    </div>
                    <div class="form_right">          
                        <label> Alamat Dealer</label>
                        <asp:TextBox ID="txtAlamatDealer" runat="server" TextMode="MultiLine" CssClass="multiline_textbox"></asp:TextBox>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Telepon Dealer</label>
                        <asp:TextBox ID="txtTeleponDealer" runat="server"></asp:TextBox>
                    </div>
                    <div class="form_right">          

                    </div>
                </div>

                <div class="form_button">
                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="small button gray"
                        CausesValidation="False"></asp:Button>
                </div>                         
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
            <script type="text/javascript">
        $(document).ready(function () {
            number()
        });

        function number()
        {
            let NilaiInvoice = document.getElementById('txtNilaiBPKB').value;
            let NilaiInvoicetAmount = parseInt(NilaiInvoice.replace(/\s*,\s*/g, ''));

            $('#txtNilaiBPKB').val(number_format(NilaiInvoicetAmount, 0));

        }

        function number_format(number, decimals, dec_point, thousands_sep) {

            number = (number + '')
            .replace(/[^0-9+\-Ee.]/g, '');
            var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function (n, prec) {
                var k = Math.pow(10, prec);
                return '' + (Math.round(n * k) / k)
                .toFixed(prec);
            };
            // Fix for IE parseFloat(0.55).toFixed(0) = 0;
            s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
            .split('.');
            if (s[0].length > 3) {
                s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
            }
            if ((s[1] || '')
            .length < prec) {
                s[1] = s[1] || '';
                s[1] += new Array(prec - s[1].length + 1)
              .join('0');
            }
            return s.join(dec);
        }
    </script>
</body>
</html>

