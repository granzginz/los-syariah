﻿#Region "Imports"
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class ViewCollateralLandBuilding
	Inherits Maxiloan.Webform.WebBased
	Protected WithEvents ucLandTab1 As ucAgunanLainInvoiceViewTab

	Dim c_AgunanLain As New AgunanLainController
#Region "Property"
	Public Property Style() As String
		Get
			Return CType(ViewState("Style"), String)
		End Get
		Set(ByVal Value As String)
			ViewState("Style") = Value
		End Set
	End Property

	Private Property CustomerID() As String
		Get
			Return CType(ViewState("CustomerID"), String)
		End Get
		Set(ByVal Value As String)
			ViewState("CustomerID") = Value
		End Set
	End Property

	Private Property CollateralID() As String
		Get
			Return CType(ViewState("CollateralID"), String)
		End Get
		Set(ByVal Value As String)
			ViewState("CollateralID") = Value
		End Set
	End Property

#End Region

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		lblMessage.Visible = False
		Page.Header.DataBind()
		If SessionInvalid() Then
			Exit Sub
		End If
		If Not IsPostBack Then
			Me.Style = Request.QueryString("style")

			If Request.QueryString("CollateralID").Trim <> "" Then Me.CollateralID = Request.QueryString("CollateralID")
			If Request.QueryString("CustomerID").Trim <> "" Then Me.CustomerID = Request.QueryString("CustomerID")

			Dim oAgunanLain As New Parameter.AgunanLain

			With oAgunanLain
				.strConnection = GetConnectionString()
				.CollateralID = Me.CollateralID
				.CustomerID = Me.CustomerID
			End With

			ucLandTab1.CollateralID = Me.CollateralID
			ucLandTab1.CustomerID = Me.CustomerID
			ucLandTab1.setLink()
			ucLandTab1.selectedTab("LandBuilding")
			dobind()

			oAgunanLain = c_AgunanLain.getDataLandBuildidng(oAgunanLain)
			With oAgunanLain
				'.BranchId = reader("BranchID").ToString
				'.CollateralTypeID = reader("CollateralTypeID").ToString
				'.CollateralDescription = reader("CollateralDescription").ToString
				'.CollateralID = reader("CollateralID").ToString
				'.CollateralStatus = reader("CollateralStatus").ToString

				cboJenisAgunan.SelectedValue = .CollateralTypeID
				lblJenisAgunan.Text = cboJenisAgunan.SelectedItem.Text.ToString()
				cboStatusAgunan.SelectedValue = .CollateralStatus
				lblStatusAgunan.Text = cboStatusAgunan.SelectedItem.Text.ToString()

				lblIDAgunan.Text = .CollateralID
				lblCurrency.Text = .Currency
				lblAgunanSeqNo.Text = .CollateralSeqNo
				lblPemilikAgunan.Text = .PemilikCollateral
				lblAlamatAgunan.Text = .AlamatPemilikCollateral
				lblStatusSertifikat.Text = .StatusSertifikat
				lblJenisSertifikat.Text = .JenisSertifikat
				'lblThnPenerbitanSertifikat.Text = .ThnPenerbitanSertifikat.ToString("dd/MM/yyyy")
				lblNoSertifikat.Text = .NoSertifikat
				lblNoGambarSituasi.Text = .NoGambarSituasi
				lblDevelopr.Text = .Developer
				lblLuasTanah.Text = .LuasTanah
				lblLuasBangunan.Text = .LuasBangunan
				lblPrgKonstruksi.Text = .ProgresKonstruksi
				'lblTglUpdKonstruksi.Text = .TglUpdKonstruksi.ToString("dd/MM/yyyy")
				lblKecamatan.Text = .Kecamatan
				lblDesa.Text = .Desa
				lblAlamat.Text = .Alamat
				lblSandiLokasi.Text = .SandiLokasi
				lblMortgagetype.Text = .MortgageType
				lblPropertyType.Text = .PropertyType
				lblNilaiPasarDiakui.Text = FormatNumber(.NilaiPasarDiakui, 0)
				'lblTglPenilaian.Text = .TglPenilaian.ToString("dd/MM/yyyy")
				lblNilaiPledging.Text = FormatNumber(.NilaiPledging, 0)
				'lblTglUpdAgunan.Text = .TglupdAgunan.ToString("dd/MM/yyyy")
				'lblPIBDTglPenilaian.Text = .PIBDTglPenilaian.ToString("dd/MM/yyyy")
				lblPIBDNilaiTanah.Text = FormatNumber(.PIBDNilaiTanah, 0)
				lblPIBDNilaiBangunan.Text = FormatNumber(.PIBDNilaiBangunan, 0)
				lblPIBDNilaiTanahBangunan.Text = FormatNumber(.PIBDNilaiTanahBangunan, 0)
				'lblPIBFTglPenilaian.Text = .PIBFTglPenilaian.ToString("dd/MM/yyyy")
				lblPIBFNilaiTanah.Text = FormatNumber(.PIBFNilaiTanah, 0)
				lblPIBFNilaiBangunan.Text = FormatNumber(.PIBFNilaiBangunan, 0)
				lblPIBFNilaiTanahBangunan.Text = FormatNumber(.PIBFNilaiTanahBangunan, 0)
				'lblPEBDTglPenilaian.Text = .PEBDTglPenilaian.ToString("dd/MM/yyyy")
				lblPEBDNilaiTanah.Text = FormatNumber(.PEBDNilaiTanah, 0)
				lblPEBDNilaiBangunan.Text = FormatNumber(.PEBDNilaiBangunan, 0)
				lblPEBDNilaiTanahBangunan.Text = FormatNumber(.PEBDNilaiTanahBangunan, 0)
				'lblPEBFTglPenilaian.Text = .PEBFTglPenilaian.ToString("dd/MM/yyyy")
				lblPEBFNilaiTanah.Text = FormatNumber(.PEBFNilaiTanah, 0)
				lblPEBFNilaiBangunan.Text = FormatNumber(.PEBFNilaiBangunan, 0)
				lblPEBFNilaiTanahBangunan.Text = FormatNumber(.PEBFNilaiTanahBangunan, 0)
				lblPJNoSKMHT.Text = .PJNoSKMHT
				'lblPJTglSKMHT.Text = .PJTglSKMHT.ToString("dd/MM/yyyy")
				'lblPJTglJatuhTempoSKMHT.Text = .PJTglJatuhTempoSKMHT.ToString("dd/MM/yyyy")
				lblPJNoAPHT.Text = .PJNoAPHT
				'lblPJTglAPHT.Text = .PJTglAPHT.ToString("dd/MM/yyyy")
				lblPJNoSHT.Text = .PJNoSHT
				'lblPJTglIssuedSHT.Text = .PJTglIssuedSHT.ToString("dd/MM/yyyy")
				lblPJTotalNilaiHT.Text = FormatNumber(.PJTotalNilaiHT, 0)
				lblPJRangkingHT.Text = .PJRangkingHT
				lblPJJenisPengikatan.Text = .PJJenisPengikatan
				lblPJNotaris.Text = .PJNotaris
				lblPJLokasiPenyimpananDokumen.Text = .PJLokasiPenyimpananDokumen
				lblPJKantorBPNWilayah.Text = .PJKantorBPNWilayah
				lblBgnFisikJaminan.Text = .BgnFisikJaminan
				lblBgnSuratIzin.Text = .BgnSuratIzin
				lblBgnNoSuratIzin.Text = .BgnNoSuratIzin
				lblBgnJenisSuratIzin.Text = .BgnJenisSuratIzin
				lblBgnPeruntukanBangunan.Text = .BgnPeruntukanBangunan
				lblBgnNoIzinLayakHuni.Text = .BgnNoIzinLayakHuni
				lblBgnPBBTahunTerakhir.Text = .BgnPBBTahunTerakhir
				lblBgnNilaiNJOP.Text = .BgnNilaiNJOP
				lblBgnCetakBiru.Text = .BgnCetakBiru
				lblBgnAktaBalikNama.Text = .BgnAktaBalikNama
				lblBgnNoAkta.Text = .BgnNoAkta
				lblBgnTglAkta.Text = .BgnTglAkta.ToString("dd/MM/yyyy")
				lblBgnNotaris.Text = .BgnNotaris
				BgnParipasu.Text = .BgnParipasu
				BgnBangunanDiasuransikan.Text = .BgnBangunanDiasuransikan


				If .ThnPenerbitanSertifikat = ConvertDate2("01/01/0001") Then
					lblThnPenerbitanSertifikat.Text = ""
				Else
					lblThnPenerbitanSertifikat.Text = .ThnPenerbitanSertifikat.ToString("dd/MM/yyyy")
				End If

				If .BgnTglAkta = ConvertDate2("01/01/0001") Then
					lblBgnTglAkta.Text = ""
				Else
					lblBgnTglAkta.Text = .BgnTglAkta.ToString("dd/MM/yyyy")
				End If

				If .PJTglIssuedSHT = ConvertDate2("01/01/0001") Then
					lblPJTglIssuedSHT.Text = ""
				Else
					lblPJTglIssuedSHT.Text = .PJTglIssuedSHT.ToString("dd/MM/yyyy")
				End If

				If .PJTglAPHT = ConvertDate2("01/01/0001") Then
					lblPJTglAPHT.Text = ""
				Else
					lblPJTglAPHT.Text = .PJTglAPHT.ToString("dd/MM/yyyy")
				End If

				If .PJTglSKMHT = ConvertDate2("01/01/0001") Then
					lblPJTglSKMHT.Text = ""
				Else
					lblPJTglSKMHT.Text = .PJTglSKMHT.ToString("dd/MM/yyyy")
				End If

				If .PJTglJatuhTempoSKMHT = ConvertDate2("01/01/0001") Then
					lblPJTglJatuhTempoSKMHT.Text = ""
				Else
					lblPJTglJatuhTempoSKMHT.Text = .PJTglJatuhTempoSKMHT.ToString("dd/MM/yyyy")
				End If

				If .PEBFTglPenilaian = ConvertDate2("01/01/0001") Then
					lblPEBFTglPenilaian.Text = ""
				Else
					lblPEBFTglPenilaian.Text = .PEBFTglPenilaian.ToString("dd/MM/yyyy")
				End If

				If .PEBDTglPenilaian = ConvertDate2("01/01/0001") Then
					lblPEBDTglPenilaian.Text = ""
				Else
					lblPEBDTglPenilaian.Text = .PEBDTglPenilaian.ToString("dd/MM/yyyy")
				End If

				If .PIBFTglPenilaian = ConvertDate2("01/01/0001") Then
					lblPIBFTglPenilaian.Text = ""
				Else
					lblPIBFTglPenilaian.Text = .PIBFTglPenilaian.ToString("dd/MM/yyyy")
				End If

				If .TglPenilaian = ConvertDate2("01/01/0001") Then
					lblTglPenilaian.Text = ""
				Else
					lblTglPenilaian.Text = .TglPenilaian.ToString("dd/MM/yyyy")
				End If

				If .TglupdAgunan = ConvertDate2("01/01/0001") Then
					lblTglUpdAgunan.Text = ""
				Else
					lblTglUpdAgunan.Text = .TglupdAgunan.ToString("dd/MM/yyyy")
				End If

				If .PIBDTglPenilaian = ConvertDate2("01/01/0001") Then
					lblPIBDTglPenilaian.Text = ""
				Else
					lblPIBDTglPenilaian.Text = .PIBDTglPenilaian.ToString("dd/MM/yyyy")
				End If

				If .TglUpdKonstruksi = ConvertDate2("01/01/0001") Then
					lblTglUpdKonstruksi.Text = ""
				Else
					lblTglUpdKonstruksi.Text = .TglUpdKonstruksi.ToString("dd/MM/yyyy")
				End If
			End With
		End If
	End Sub
	Sub DoBind()
		FillCombo()
		FillCbo(cboJenisAgunan, "TblCollateralType")
	End Sub
	Private Sub FillCombo()
		Dim strConn As String
		Dim dtCombo As New DataTable

		strConn = GetConnectionString()
	End Sub

	Sub FillCbo(ByVal cboName As DropDownList, ByVal Initial As String)
		Dim oAssetData As New Parameter.AgunanLain
		Dim oData As New DataTable
		oAssetData.strConnection = GetConnectionString()
		oAssetData.Initial = Initial
		oAssetData = c_AgunanLain.GetCboCollateralType(oAssetData)
		oData = oAssetData.ListData
		cboName.DataSource = oData
		cboName.DataTextField = "ID"
		cboName.DataValueField = "CollateralTypeID"
		cboName.DataBind()
		cboName.Items.Insert(0, "Select One")
		cboName.Items(0).Value = "0"
	End Sub
End Class