﻿#Region "Imports"
Imports System.IO
Imports Maxiloan
Imports Maxiloan.Exceptions
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
#End Region

Public Class LinkageJaminanTambahan

    Inherits Maxiloan.Webform.WebBased



#Region " Private Const "
    Dim m_AgunanLain As New AgunanLainController

    Dim filterBranch As String
    Private m_controller As New AgunanLainController
    Private TotalAgunan As Double
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private m_TotalPledgingAmount As Double

#End Region
#Region "Property"

    Private Property CollateralID() As String
        Get
            Return CType(ViewState("CollateralID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CollateralID") = Value
        End Set
    End Property

    Private Property CustomerID() As String
        Get
            Return CType(ViewState("CustomerID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property

	Private Property CustomerName() As String
		Get
			Return CType(ViewState("CustomerName"), String)
		End Get
		Set(ByVal Value As String)
			ViewState("CustomerName") = Value
		End Set
	End Property

	Private Property TotalPledgingAmount() As Double
		Get
			Return CType(ViewState("TotalPledgingAmount"), Double)
		End Get
		Set(ByVal Value As Double)
			ViewState("TotalPledgingAmount") = Value
		End Set
	End Property
#End Region

#Region "Page_Load"
	Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If

        Me.CustomerID = Request("CustomerID")
        Me.CollateralID = Request("CollateralID")
        Me.CustomerName = Request("NamaNasabah")
        m_TotalPledgingAmount = 0
        filterBranch = " vwCustomer.customerId like '" & Replace(Me.sesBranchId, "'", "") & "%' "


        If Not Me.IsPostBack Then

            If CheckForm(Me.Loginid, "CltrlLinkage", "MAXILOAN") Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If

            If IsSingleBranch() And Me.IsHoBranch = False Then
                lblNama.Text = Me.CustomerName
				'lblCollateralID.Text = Me.CollateralID
				hyCollateralID.Text = Me.CollateralID
				lblCustomerID.Text = Me.CustomerID
				DoBind()
				showJaminanTambahan()
                hpLookup.NavigateUrl = "javascript:OpenAgreement('" & txtAgreementNo.ClientID & "','" & hdnApplicationID.ClientID & "','" & hdnBranchAgreement.ClientID & "','" & txtTotalOTR.ClientID & "','" & txtNTF.ClientID & "','" & "" & "')"
            Else
                Dim strHTTPServer As String
                Dim StrHTTPApp As String
                Dim strNameServer As String
                strHTTPServer = Request.ServerVariables("PATH_INFO")
                strNameServer = Request.ServerVariables("SERVER_NAME")
                StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                Response.Redirect("http: //" & strNameServer & "/" & StrHTTPApp & "/error_notauthorized.aspx")
            End If
        End If
        hpLookup.NavigateUrl = "javascript:OpenAgreement('" & txtAgreementNo.ClientID & "','" & hdnApplicationID.ClientID & "','" & hdnBranchAgreement.ClientID & "','" & txtTotalOTR.ClientID & "','" & txtNTF.ClientID & "','" & "" & "')"
		hyCollateralID.NavigateUrl = "javascript:openAgunanLainTab('" & lblCustomerID.Text & "','" & hyCollateralID.Text & "')"

	End Sub
#End Region

	Private Sub showJaminanTambahan()
        Dim oCustomClass As New Parameter.AgunanLain

        With oCustomClass
            .strConnection = GetConnectionString()
            .CustomerID = Me.CustomerID
            .CollateralID = Me.CollateralID
        End With

        Try
            oCustomClass = m_controller.getAgunanNett(oCustomClass)

            gvAgunanNett.DataSource = oCustomClass.Tabels.Tables(0)
            gvAgunanNett.DataBind()

        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try

    End Sub

    Private Sub gvAgunanNett_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvAgunanNett.RowDataBound

        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lblNilai As Label = CType(e.Row.Cells(1).FindControl("lblNilai"), Label)

            If e.Row.Cells(2).Text = "+" Then
                TotalAgunan += CDbl(lblNilai.Text)
            Else
                TotalAgunan -= CDbl(lblNilai.Text)
            End If

        ElseIf e.Row.RowType = DataControlRowType.Footer Then

            Dim lblTotalNilai As Label = CType(e.Row.Cells(1).FindControl("lblTotalNilai"), Label)
			lblTotalNilai.Text = FormatNumber(TotalAgunan, 0)

			'lblSisaAlokasi.Text = FormatNumber(TotalAgunan, 0)
			lblSisaAlokasi.Text = FormatNumber(lblTotalNilai.Text - Me.TotalPledgingAmount, 0)
		End If
    End Sub

    Private Sub BtnAddNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnAddNew.Click
		If CheckFeature(Me.Loginid, "CltrlLinkage", "Add", Me.AppId) Then
			If SessionInvalid() Then
				Exit Sub
			End If
			Add()
			DoBind()
			showJaminanTambahan()
		End If
	End Sub

#Region "ADD"
    Private Sub Add()

        Dim customClass As New Parameter.AgunanLain

        With customClass
            .strConnection = GetConnectionString()
            .BranchId = Me.sesBranchId.Trim.Replace("'", "")
            .ApplicationID = hdnApplicationID.Value.Trim
            .AgreementNo = txtAgreementNo.Text.Trim
            .CustomerID = Me.CustomerID.Trim
            .CollateralID = Me.CollateralID.Trim
            .PledgingPercentage = txtPledgingPercentage.Text.Trim
            .PledgingAmount = txtPledgingAmount.Text.Trim
            .CollateralStatus = "1"
            .LoginId = Me.Loginid.Trim
        End With

		Try
			If txtPledgingAmount.Text = 0 Or txtPledgingPercentage.Text = 0 Then
				ShowMessage(lblMessage, "Amount Tidak Boleh 0 ", True)
			ElseIf CDbl(txtPledgingAmount.Text) > CDbl(lblSisaAlokasi.Text) Then
				ShowMessage(lblMessage, "Nilai Pledging Tidak Boleh Lebih dari Sisa Alokasi ", True)
			Else
				m_AgunanLain.PledgingAdd(customClass)
				ShowMessage(lblMessage, "Data Berhasil disimpan ", False)
				Clear()
			End If
		Catch ex As Exception
			ShowMessage(lblMessage, ex.Message, True)
		End Try
	End Sub
#End Region

	Private Sub DoBind()
		Dim dtvEntity As DataView
		Dim dtsEntity As DataTable
		Dim oAgunanLain As New Parameter.AgunanLain

		Try
			With oAgunanLain
				.BranchId = Me.sesBranchId.Trim.Replace("'", "")
				.CustomerID = Me.CustomerID.Trim
				.CollateralID = Me.CollateralID.Trim
				.strConnection = GetConnectionString()
			End With

			oAgunanLain = m_AgunanLain.GetDetailPledging(oAgunanLain)

			dtsEntity = oAgunanLain.ListData
			dtvEntity = dtsEntity.DefaultView
			dtvEntity.Sort = Me.SortBy

			If dtvEntity.Count <= 0 Then
				ShowMessage(lblMessage, "Data kontrak yang dijaminkan belum ada .....", True)
			End If

			dtsEntity = oAgunanLain.ListData

			dtgPledging.DataSource = dtsEntity.DefaultView
			dtgPledging.DataBind()


			'Try
			'	dtgPledging.DataBind()
			'Catch
			'	dtgPledging.CurrentPageIndex = 0
			'	dtgPledging.DataBind()
			'End Try

		Catch ex As Exception
			DisplayError(ex.Message + " " + ex.StackTrace)
		End Try
	End Sub
	Sub Clear()
		txtAgreementNo.Text = ""
		txtTotalOTR.Text = ""
		txtNTF.Text = ""
		txtPledgingPercentage.Text = ""
		txtPledgingAmount.Text = ""
	End Sub

	Private Sub DisplayError(ByVal strErrMsg As String)
        With lblMessage
            .Text = strErrMsg
            .Visible = True
        End With
    End Sub

    Private Sub dtgPledging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPledging.ItemDataBound
        Dim lbltemp As Label
		Dim lblTotalPledgingAmount As Label


		If e.Item.ItemIndex >= 0 Then
			lbltemp = CType(e.Item.FindControl("lblPledgedAmount"), Label)
			If Not lbltemp Is Nothing Then
				m_TotalPledgingAmount = m_TotalPledgingAmount + CType(lbltemp.Text, Double)
			End If
		End If

		If e.Item.ItemType = ListItemType.Footer Then
			lblTotalPledgingAmount = CType(e.Item.FindControl("lblTotalPledgingAmount"), Label)
			If Not lblTotalPledgingAmount Is Nothing Then
				lblTotalPledgingAmount.Text = FormatNumber(m_TotalPledgingAmount.ToString, 2)
			End If
		End If

		Me.TotalPledgingAmount = m_TotalPledgingAmount

	End Sub
End Class