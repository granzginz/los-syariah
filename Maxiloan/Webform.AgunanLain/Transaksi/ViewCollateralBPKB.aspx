﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewCollateralBPKB.aspx.vb" Inherits="Maxiloan.Webform.AgunanLain.ViewCollateralBPKB" %>
<%@ Register Src="../../webform.UserController/ucAgunanLainInvoiceViewTab.ascx" TagName="ucLandBuildingTab"
    TagPrefix="uc7" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register TagPrefix="uc1" TagName="ucassetdata" Src="../../Webform.UserController/ViewApplication/UcAssetData.ascx" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ViewAsset</title>
    <link href="../../Include/Buttons.css" type="text/css" rel="Stylesheet"/>
    <link href="../../Include/General.css" type="text/css" rel="stylesheet" />
    <link href="../../Include/AccMnt.css" type="text/css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
    <uc7:ucLandBuildingTab id="ucLandTab1" runat="server" />
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h4> <asp:Label ID="lblTitle" runat="server"></asp:Label> BPKB </h4>
                </div>
            </div>
                <div class="form_box">
                    <div class="form_left">
                    <label>Jenis Agunan</label>
                        <div class="form_box_hide">
                        <asp:DropDownList ID="cboJenisAgunan" Enabled="false" runat="server" onchange="cboPHomeStatus_IndexChanged(this.value);">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" Display="Dynamic"
                            InitialValue="Select One" ControlToValidate="cboJenisAgunan" ErrorMessage="Harap pilih jenis Agunan"
                            CssClass="validator_general"></asp:RequiredFieldValidator>
                        </div>
                        <asp:Label runat="server" ID="lblJenisAgunan"></asp:Label>
                    </div>
                    <div class="form_right">
                         <label> ID Agunan</label>
                        <asp:Label ID="lblIDAgunan" runat="server" CssClass="label_auto"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Status Agunan</label>
                        <div class="form_box_hide">
                        <asp:DropDownList ID="cboStatusAgunan" runat="server" Enabled="false" onchange="StatusAgunan()">
                            <asp:ListItem Value="0">Iddle</asp:ListItem>
                            <asp:ListItem Value="1">Pledged</asp:ListItem>
                            <asp:ListItem Value="2">Lunas</asp:ListItem>
                            <asp:ListItem Value="3">Ditarik</asp:ListItem>
                        </asp:DropDownList>
                        </div>
                        <asp:Label runat="server" ID="lblStatusAgunan"></asp:Label>
                    </div>
                    <div class="form_right">          
                        <label> Currency</label>
                        <asp:label ID="lblCurrency" runat="server"></asp:label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Agunan Seq No</label>
                        <asp:label ID="lblAgunanSeqNo" runat="server"></asp:label>
                    </div>
                    <div class="form_right">

                    </div>
                </div>
            <div class="form_title">
                <div class="form_single">
                    <h4>Identitas Pemilik Agunan &nbsp;&nbsp;<asp:Label runat="server" ID="Label1" /></h4>
                </div>
            </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Pemilik Agunan</label>
                        <asp:label ID="lblPemilikAgunan" runat="server"></asp:label>
                    </div>
                    <div class="form_right">          
                        
                    </div>
                </div> 
            <div class="form_title">
                <div class="form_single">
                    <h4>Data BPKB &nbsp;&nbsp;<asp:Label runat="server" ID="Label2" /></h4>
                </div>
            </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Nama di BPKB</label>
                        <asp:label ID="lblNamaBpkb" runat="server"></asp:label>
                    </div>
                    <div class="form_right">
                        <label> No BPKB</label>
                        <asp:label ID="lblNoBpkb" runat="server" ></asp:label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Tanggal BPKB</label>
                        <asp:label ID="lblTglBpkb" runat="server"></asp:label>
                    </div>
                    <div class="form_right">          
                        <label> No Polisi</label>
                        <asp:label ID="lblNoPolisi" runat="server" ></asp:label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> No Rangka</label>
                        <asp:label ID="lblNoRangka" runat="server"></asp:label>
                    </div>
                    <div class="form_right">          
                        <label> No Mesin</label>
                        <asp:label ID="lblNoMesin" runat="server" ></asp:label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Merk</label>
                        <asp:label ID="lblMerk" runat="server" ></asp:label>
                    </div>
                    <div class="form_right">          
                        <label> Tipe</label>
                        <asp:label ID="lblTipe" runat="server"></asp:label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Warna</label>
                        <asp:label ID="lblWarna" runat="server" ></asp:label>
                    </div>
                    <div class="form_right">          

                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Tgl Jatuh Tempo BPKB</label>
                        <asp:label ID="lblTglJtTempo" runat="server" ></asp:label>                        
                    </div>
                    <div class="form_right">          
                        <label> Tgl Terima BPKB</label>
                        <asp:label ID="lblTglterima" runat="server"></asp:label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> No Faktur</label>
                        <asp:label ID="lblNoFaktur" runat="server"></asp:label>
                    </div>
                    <div class="form_right">          
                        <label> No NIK</label>
                        <asp:label ID="lblNik" runat="server" ></asp:label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> No Form A</label>
                        <asp:label ID="lblNoFormA" runat="server"></asp:label>
                    </div>
                    <div class="form_right">          
                        <label> Akta Fidusia</label>
                        <asp:label ID="lblAktaFidusia" runat="server" ></asp:label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Sertifikat Fidusia</label>
                        <asp:label ID="lblSertifikatFidusia" runat="server"></asp:label>
                    </div>
                    <div class="form_right">          
                        <label> Tgl Keluar BPKB Sementara</label>
                        <asp:label ID="lblTglKeluarBpkb" runat="server" ></asp:label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Keterangan</label>
                        <asp:label ID="lblKeterangan" runat="server"></asp:label>
                    </div>
                    <div class="form_right">          
                        <label> Tgl Close</label>
                        <asp:label ID="lblTglClose" runat="server" ></asp:label>
                    </div>
                </div> 
                <div class="form_box">
                    <div class="form_single" style="background-color:#f9f9f9">
                        <h4>Data Dealer &nbsp;&nbsp;<asp:Label runat="server" ID="Label4" /></h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Nama Dealer</label>
                        <asp:label ID="lblNamaDealer" runat="server"></asp:label>
                    </div>
                    <div class="form_right">          
                        <label> Alamat Dealer</label>
                        <asp:label ID="lblAlamatDealer" runat="server" ></asp:label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Telepon Dealer</label>
                        <asp:label ID="lblTeleponDealer" runat="server"></asp:label>
                    </div>
                    <div class="form_right">          

                    </div>
                </div>
                <div class="tab_container_form_space"><br /><br /><br /><br /></div>
                <div class="tab_container_button">
                    <div class="form_button">
                        <asp:Button ID="btnClose" OnClientClick="javascript:window.close();" runat="server"
                            Text="Close" CssClass="small button gray" CausesValidation="False"></asp:Button>
                    </div>
                </div>
    </form>
</body>
</html>

