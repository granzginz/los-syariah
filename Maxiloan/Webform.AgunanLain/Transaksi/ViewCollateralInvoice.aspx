﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewCollateralInvoice.aspx.vb" Inherits="Maxiloan.Webform.AgunanLain.ViewCollateralInvoice" %>
<%@ Register Src="../../webform.UserController/ucAgunanLainInvoiceViewTab.ascx" TagName="ucLandBuildingTab"
    TagPrefix="uc7" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register TagPrefix="uc1" TagName="ucassetdata" Src="../../Webform.UserController/ViewApplication/UcAssetData.ascx" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ViewAsset</title>
    <link href="../../Include/Buttons.css" type="text/css" rel="Stylesheet"/>
    <link href="../../Include/General.css" type="text/css" rel="stylesheet" />
    <link href="../../Include/AccMnt.css" type="text/css" rel="stylesheet" />
</head>
<body onload="gridGeneralSize('dtgTC');getGridGeneralSize('hdnGridGeneralSize');"
    onresize="gridGeneralSize('dtgTC');getGridGeneralSize('hdnGridGeneralSize');">

    <form id="form1" runat="server">
    <uc7:ucLandBuildingTab id="ucLandTab1" runat="server" />
    <input id="hdnGridGeneralSize" runat="server" type="hidden" />
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h4> <asp:Label ID="lblTitle" runat="server"></asp:Label> INVOICE </h4>
                </div>
            </div>
                <div class="form_box">
                    <div class="form_left">
                    <label>Jenis Agunan</label>
                        <div class="form_box_hide">
                        <asp:DropDownList ID="cboJenisAgunan" runat="server" Enabled="false" onchange="cboPHomeStatus_IndexChanged(this.value);">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" Display="Dynamic"
                            InitialValue="Select One" ControlToValidate="cboJenisAgunan" ErrorMessage="Harap pilih jenis Agunan"
                            CssClass="validator_general"></asp:RequiredFieldValidator>
                        </div>
                        <asp:Label runat="server" ID="lblJenisAgunan"></asp:Label>
                    </div>
                    <div class="form_right">
                         <label> ID Agunan</label>
                        <asp:Label ID="lblIDAgunan" runat="server" CssClass="label_auto"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Status Agunan</label>
                        <div class="form_box_hide">
                        <asp:DropDownList ID="cboStatusAgunan" Enabled="false" runat="server" onchange="StatusAgunan()">
                            <asp:ListItem Value="0">Iddle</asp:ListItem>
                            <asp:ListItem Value="1">Pledged</asp:ListItem>
                            <asp:ListItem Value="2">Lunas</asp:ListItem>
                            <asp:ListItem Value="3">Ditarik</asp:ListItem>
                        </asp:DropDownList>
                        </div>
                        <asp:Label runat="server" ID="lblStatusAgunan"></asp:Label>
                    </div>
                    <div class="form_right">          
                        <label> Currency</label>
                        <asp:Label ID="lbl_Currency" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Agunan Seq No</label>
                        <asp:Label ID="lbl_AgunanSeqNo" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label> Tanggal Kontrak</label>
                        <asp:Label ID="lbl_TglKontrak" runat="server"></asp:Label>
                    </div>
                </div>
            <div class="form_title">
                <div class="form_single">
                    <h4>Identitas Pemilik Agunan &nbsp;&nbsp;<asp:Label runat="server" ID="Label1" /></h4>
                </div>
            </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Pemilik Agunan</label>
                        <asp:Label ID="lbl_PemilikAgunan" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">          
   
                    </div>
                </div> 
            <div class="form_title">
                <div class="form_single">
                    <h4>Data Invoice &nbsp;&nbsp;<asp:Label runat="server" ID="Label2" /></label>
                </div>
            </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Objek</label>
                        <asp:Label ID="lbl_Objek" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">          
    
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> No Invoice</label>
                        <asp:Label ID="lbl_NoInvoice" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">          
                        <label> Tgl Invoice</label>
                        <asp:Label ID="lbl_TglInvoice" runat="server" ></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Penerbit Invoice</label>
                        <asp:Label ID="lbl_PenerbitInvoice" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">          
                        <label> Penerima Invoice</label>
                        <asp:Label ID="lbl_PenerimaInvoice" runat="server" ></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Nilai Invoice</label>
                        <asp:Label ID="lbl_LuasTanah" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">          

                    </div>
                </div>   
                <div class="tab_container_form_space"><br /><br /><br /><br /></div>
                <div class="tab_container_button">
                    <div class="form_button">
                        <asp:Button ID="btnClose" OnClientClick="javascript:window.close();" runat="server"
                            Text="Close" CssClass="small button gray" CausesValidation="False"></asp:Button>
                    </div>
                </div>
    </form>
</body>
</html>

