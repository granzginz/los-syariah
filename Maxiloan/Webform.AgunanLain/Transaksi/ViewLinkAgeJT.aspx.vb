﻿#Region "Imports"
Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.General
Imports Maxiloan.General.CommonCookiesHelper
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Imports Maxiloan.Webform.UserController
#End Region

Public Class ViewLinkAgeJT
	Inherits Maxiloan.Webform.WebBased
#Region "Property"

	Private Property CollateralID() As String
		Get
			Return CType(ViewState("CollateralID"), String)
		End Get
		Set(ByVal Value As String)
			ViewState("CollateralID") = Value
		End Set
	End Property

	Private Property CustomerID() As String
		Get
			Return CType(ViewState("CustomerID"), String)
		End Get
		Set(ByVal Value As String)
			ViewState("CustomerID") = Value
		End Set
	End Property

	Private Property TotalPledgingAmount() As Double
		Get
			Return CType(ViewState("TotalPledgingAmount"), Double)
		End Get
		Set(ByVal Value As Double)
			ViewState("TotalPledgingAmount") = Value
		End Set
	End Property
#End Region

	Dim c_AgunanLain As New AgunanLainController
	Protected WithEvents ucLandTab1 As ucAgunanLainInvoiceViewTab
	Private TotalAgunan As Double
	Private m_TotalPledgingAmount As Double

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		Page.Header.DataBind()
		If SessionInvalid() Then
			Exit Sub
		End If

		m_TotalPledgingAmount = 0

		If Not IsPostBack Then

			If Request.QueryString("CollateralID").Trim <> "" Then Me.CollateralID = Request.QueryString("CollateralID")
			If Request.QueryString("CustomerID").Trim <> "" Then Me.CustomerID = Request.QueryString("CustomerID")

			ucLandTab1.CollateralID = Me.CollateralID
			ucLandTab1.CustomerID = Me.CustomerID
			ucLandTab1.setLink()
			ucLandTab1.selectedTab("Linkage")

			lblCollateralID.Text = Me.CollateralID
			lblCustomerID.Text = Me.CustomerID
			showJaminanTambahan()
			DoBind()

		End If
	End Sub
	Private Sub showJaminanTambahan()
		Dim oCustomClass As New Parameter.AgunanLain

		With oCustomClass
			.strConnection = GetConnectionString()
			.CustomerID = Me.CustomerID
			.CollateralID = Me.CollateralID
		End With

		Try
			oCustomClass = c_AgunanLain.getAgunanNett(oCustomClass)

			gvAgunanNett.DataSource = oCustomClass.Tabels.Tables(0)
			gvAgunanNett.DataBind()

		Catch ex As Exception
			ShowMessage(lblMessage, ex.Message, True)
		End Try

	End Sub
	Private Sub gvAgunanNett_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvAgunanNett.RowDataBound

		If e.Row.RowType = DataControlRowType.DataRow Then
			Dim lblNilai As Label = CType(e.Row.Cells(1).FindControl("lblNilai"), Label)

			If e.Row.Cells(2).Text = "+" Then
				TotalAgunan += CDbl(lblNilai.Text)
			Else
				TotalAgunan -= CDbl(lblNilai.Text)
			End If

		ElseIf e.Row.RowType = DataControlRowType.Footer Then

			Dim lblTotalNilai As Label = CType(e.Row.Cells(1).FindControl("lblTotalNilai"), Label)
			lblTotalNilai.Text = FormatNumber(TotalAgunan, 0)

			'lblSisaAlokasi.Text = FormatNumber(TotalAgunan, 0)

			lblSisaAlokasi.Text = FormatNumber(lblTotalNilai.Text - Me.TotalPledgingAmount, 0)
		End If
	End Sub

	Private Sub dtgPledging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPledging.ItemDataBound
		Dim lbltemp As Label
		Dim lblTotalPledgingAmount As Label

		If e.Item.ItemIndex >= 0 Then
			lbltemp = CType(e.Item.FindControl("lblPledgedAmount"), Label)
			If Not lbltemp Is Nothing Then
				m_TotalPledgingAmount = m_TotalPledgingAmount + CType(lbltemp.Text, Double)
			End If
		End If
		If e.Item.ItemType = ListItemType.Footer Then
			lblTotalPledgingAmount = CType(e.Item.FindControl("lblTotalPledgingAmount"), Label)
			If Not lblTotalPledgingAmount Is Nothing Then
				lblTotalPledgingAmount.Text = FormatNumber(m_TotalPledgingAmount.ToString, 2)
			End If
		End If

		Me.TotalPledgingAmount = m_TotalPledgingAmount
	End Sub
	Private Sub DoBind()
		Dim dtvEntity As DataView
		Dim dtsEntity As DataTable
		Dim oAgunanLain As New Parameter.AgunanLain

		Try
			With oAgunanLain
				.BranchId = Me.sesBranchId.Trim.Replace("'", "")
				.CustomerID = Me.CustomerID.Trim
				.CollateralID = Me.CollateralID.Trim
				.strConnection = GetConnectionString()
			End With

			oAgunanLain = c_AgunanLain.GetDetailPledging(oAgunanLain)

			dtsEntity = oAgunanLain.ListData
			dtvEntity = dtsEntity.DefaultView
			dtvEntity.Sort = Me.SortBy

			If dtvEntity.Count <= 0 Then
				'ShowMessage(lblMessage, "Data kontrak yang dijaminkan belum ada .....", True)
			End If

			dtgPledging.DataSource = dtvEntity
			Try
				dtgPledging.DataBind()
			Catch
				dtgPledging.CurrentPageIndex = 0
				dtgPledging.DataBind()
			End Try

		Catch ex As Exception
			DisplayError(ex.Message + " " + ex.StackTrace)
		End Try
	End Sub
	Private Sub DisplayError(ByVal strErrMsg As String)
		With lblMessage
			.Text = strErrMsg
			.Visible = True
		End With
	End Sub
End Class