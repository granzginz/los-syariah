﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="JTLandBuilding.aspx.vb"
    Inherits="Maxiloan.Webform.AgunanLain.JTLandBuilding" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ucAddress" Src="../../Webform.UserController/ucCompanyAddress.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcCompanyAdress" Src="../../Webform.UserController/ucCompanyAddress.ascx" %>
<%@ Register TagPrefix="uc6" TagName="ucDateCE" Src="../../Webform.UserController/ucDateCE.ascx" %>
<%@ Register Src="../../webform.UserController/ucMonthCombo.ascx" TagName="ucMonthCombo"
    TagPrefix="uc2" %>
<%@ Register Src="../../webform.UserController/ucLookupGroupCust.ascx" TagName="ucLookupGroupCust"
    TagPrefix="uc5" %>
<%@ Register TagPrefix="uc1" TagName="ucNumberFormat" Src="../../Webform.UserController/ucNumberFormat.ascx" %>

<%@ Register Src="CustomerJaminanTambahanTabs.ascx" TagName="tabs" TagPrefix="uct" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Customer Personal</title>
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        function OpenLookup() {
            $('#dialog').remove();

            $('#content').prepend('<div id="dialog" style="padding: 3px 0px 0px 0px;"><iframe src="../../webform.UserController/jLookup/ZipCode.aspx" style="padding:0; margin: 0; display: block; width: 100%; height: 100%;" frameborder="no" scrolling="auto"></iframe></div>');

            $('#dialog').dialog({
                title: 'Daftar Kode POS',
                bgiframe: false,
                width: 1024,
                height: 550,
                resizable: false,
                modal: true,
                closeOnEscape: true,
                draggable: true
            });
        }
        function CloseDialog() {
            $('#dialog').dialog('close'); return false;
        }
        function isiData(kelurahan, kecamatan, zipcode, kota) {
            frm = document.forms[0];
            frm.UcLegalAddress_oLookUpZipCode_txtKelurahan.value = kelurahan;
            frm.UcLegalAddress_oLookUpZipCode_txtKecamatan.value = kecamatan;
            frm.UcLegalAddress_oLookUpZipCode_txtCity.value = kota;
            frm.UcLegalAddress_oLookUpZipCode_txtZipCode.value = zipcode;
            CloseDialog()
            return false;
        }
        function Nationality() {
            if (document.forms[0].cboPNationality.value == 'WNA') {
                document.forms[0].txtPWNA.disabled = false;
            }
            else {
                document.forms[0].txtPWNA.disabled = true;
                document.forms[0].txtPWNA.value = '';
            }
        }

        var Page_Validators = new Array();

        function LegalAddress() {
            ds = document.forms[0].UcLegalAddress_txtAddress.value;
            document.forms[0].UcResAddress_txtAddress.value = document.forms[0].UcLegalAddress_txtAddress.value;
            document.forms[0].UcResAddress_txtRT.value = document.forms[0].UcLegalAddress_txtRT.value;
            document.forms[0].UcResAddress_txtRW.value = document.forms[0].UcLegalAddress_txtRW.value;
            document.forms[0].UcResAddress_oLookUpZipCode_txtKelurahan.value = document.forms[0].UcLegalAddress_oLookUpZipCode_txtKelurahan.value;
            document.forms[0].UcResAddress_oLookUpZipCode_txtKecamatan.value = document.forms[0].UcLegalAddress_oLookUpZipCode_txtKecamatan.value;
            document.forms[0].UcResAddress_oLookUpZipCode_txtCity.value = document.forms[0].UcLegalAddress_oLookUpZipCode_txtCity.value;
            document.forms[0].UcResAddress_oLookUpZipCode_txtZipCode.value = document.forms[0].UcLegalAddress_oLookUpZipCode_txtZipCode.value;
            document.forms[0].UcResAddress_txtAreaPhone1.value = document.forms[0].UcLegalAddress_txtAreaPhone1.value;
            document.forms[0].UcResAddress_txtPhone1.value = document.forms[0].UcLegalAddress_txtPhone1.value;
            document.forms[0].UcResAddress_txtAreaPhone2.value = document.forms[0].UcLegalAddress_txtAreaPhone2.value;
            document.forms[0].UcResAddress_txtPhone2.value = document.forms[0].UcLegalAddress_txtPhone2.value;
            document.forms[0].UcResAddress_txtAreaFax.value = document.forms[0].UcLegalAddress_txtAreaFax.value;
            document.forms[0].UcResAddress_txtFax.value = document.forms[0].UcLegalAddress_txtFax.value;
            return false;
        }
        function cboPHomeStatus_IndexChanged(e) {
            if (e == 'KR') {
                $('#divTglSelesaiKontrak').css("visibility", "");
                $('#txtRentFinish_txtDateCE').removeAttr("style");

                if ($('#divTglSelesaiKontrak').valueOf != '') {
                    ValidatorEnable(txtRentFinish_rfvDateCE, true);
                    $('#txtRentFinish_rfvDateCE').hide();
                }
                else {
                    ValidatorEnable(txtRentFinish_rfvDateCE, true);
                    $('#txtRentFinish_rfvDateCE').show();
                }
            }
            else {
                $('#divTglSelesaiKontrak').css("visibility", "hidden");
                $('#txtRentFinish_txtDateCE').attr("style", "display : none");
                ValidatorEnable(txtRentFinish_rfvDateCE, false);
                $('#txtRentFinish_rfvDateCE').hide();
            }
        }
    </script>
    <style >
        .multiline_textbox {
            width: 60% !important;
        }
        .color_yellow {
            background-color: yellow;
        }
        .rightAlign {
            text-align: right;
        }
    </style>
</head>
<body onload="gridGeneralSize('dtgTC');getGridGeneralSize('hdnGridGeneralSize');"
    onresize="gridGeneralSize('dtgTC');getGridGeneralSize('hdnGridGeneralSize');">
    <asp:UpdateProgress ID="upg1" runat="server" AssociatedUpdatePanelID="updatePanel1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
      <uct:tabs id='cTabs' runat='server'></uct:tabs> 
        <div runat="server" id="jlookupContent" />
    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
                onclick="hideMessage();"></asp:Label>
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h4> <asp:Label ID="lblTitle" runat="server"></asp:Label> </h4>
                </div>
            </div>
            <asp:Panel runat="server" ID="pnlIdentitas">
                <div class="form_box">
                    <div class="form_left">
                    <label>Jenis Agunan</label>
                        <asp:DropDownList ID="cboJenisAgunan" runat="server">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" Display="Dynamic"
                            InitialValue="Select One" ControlToValidate="cboJenisAgunan" ErrorMessage="Harap pilih jenis Agunan"
                            CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                    <div class="form_right">
                         <label> ID Agunan</label>
                        <asp:Label ID="lblIDAgunan" runat="server" CssClass="label_auto"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Status Agunan</label>
                        <asp:DropDownList ID="cboStatusAgunan" runat="server" onchange="StatusAgunan()">
                            <asp:ListItem Value="0">On Hand</asp:ListItem>
                            <asp:ListItem Value="1">Dijaminkan</asp:ListItem>
                            <asp:ListItem Value="2">Lunas</asp:ListItem>
                            <asp:ListItem Value="3">Ditarik</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="form_right">          
                        <label> Currency</label>
                        <asp:TextBox ID="txtCurrency" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Agunan Seq No</label>
                        <%--<asp:TextBox ID="txtAgunanSeqNo" runat="server"></asp:TextBox>--%>
                        <asp:Label runat="server" ID="lblAgunanSeqNo"></asp:Label>
                    </div>
                    <div class="form_right">

                    </div>
                </div>
            <div class="form_title">
                <div class="form_single">
                    <h4>Identitas Pemilik Agunan &nbsp;&nbsp;<asp:Label runat="server" ID="Label1" /></label>
                </div>
            </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Pemilik Agunan</label>
                        <asp:TextBox ID="txtPemilikAgunan" runat="server" Width="40%"></asp:TextBox>
                    </div>
                    <div class="form_right">          
                        <label> Alamat </label>
                        <asp:TextBox ID="txtAlamatAgunan" runat="server" TextMode="MultiLine" CssClass="multiline_textbox"></asp:TextBox>
                    </div>
                </div> 
            <div class="form_title">
                <div class="form_single">
                    <h4>Sertifikat &nbsp;&nbsp;<asp:Label runat="server" ID="Label2" /></label>
                </div>
            </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Status Sertifikat</label>
                        <asp:TextBox ID="txtStatusSertifikat" runat="server"></asp:TextBox>
                    </div>
                    <div class="form_right">          
                        <label> Jenis Sertifikat</label>
                        <asp:TextBox ID="txtJenisSertifikat" runat="server" ></asp:TextBox>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Tahun Penerbitan Sertifikat</label>
                        <asp:TextBox ID="txtThnPenerbitanSertifikat" runat="server" Width="10%"></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender5" runat="server" Enabled="True" TargetControlID="txtThnPenerbitanSertifikat" Format="dd/MM/yyyy" PopupPosition="BottomRight" DefaultView="Months"></asp:CalendarExtender>               
                    </div>
                    <div class="form_right">          
                        <label> Nomor Sertifikat</label>
                        <asp:TextBox ID="txtNoSertifikat" runat="server" ></asp:TextBox>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Nomor Gambar Situasi</label>
                        <asp:TextBox ID="txtNoGambarSituasi" runat="server"></asp:TextBox>
                    </div>
                    <div class="form_right">          
                        <label> Developer</label>
                        <asp:TextBox ID="txtDevelopr" runat="server" ></asp:TextBox>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Luas Tanah</label>
                        <asp:TextBox ID="txtLuasTanah" runat="server"></asp:TextBox>
                    </div>
                    <div class="form_right">          
                        <label> Luas Bangunan</label>
                        <asp:TextBox ID="txtLuasBangunan" runat="server" ></asp:TextBox>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Progress Konstruksi</label>
                        <asp:TextBox ID="txtPrgKonstruksi" runat="server"></asp:TextBox>
                    </div>
                    <div class="form_right">          
                        <label> Tgl Update Konstruksi</label>
                        <asp:TextBox ID="txtTglUpdKonstruksi" runat="server" Width="10%"></asp:TextBox>
                         <asp:CalendarExtender ID="CalendarExtender6" runat="server" Enabled="True" TargetControlID="txtTglUpdKonstruksi" Format="dd/MM/yyyy" PopupPosition="BottomRight" DefaultView="Months"></asp:CalendarExtender> 
                    </div>
                </div>
            <div class="form_title">
                <div class="form_single">
                    <h4>Lokasi Agunan &nbsp;&nbsp;<asp:Label runat="server" ID="Label3" /></label>
                </div>
            </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Kecamatan</label>
                        <asp:TextBox ID="txtKecamatan" runat="server"></asp:TextBox>
                    </div>
                    <div class="form_right">          
                        <label> Desa</label>
                        <asp:TextBox ID="txtDesa" runat="server" ></asp:TextBox>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Persil / Alamat</label>
                        <asp:TextBox ID="txtAlamat" runat="server"></asp:TextBox>
                    </div>
                    <div class="form_right">          
                        <label> Sandi Lokasi</label>
                        <asp:TextBox ID="txtSandiLokasi" runat="server" ></asp:TextBox>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Mortgage Type</label>
                        <asp:TextBox ID="txtMortgagetype" runat="server"></asp:TextBox>
                    </div>
                    <div class="form_right">          
                        <label> Property Type</label>
                        <asp:TextBox ID="txtPropertyType" runat="server" ></asp:TextBox>
                    </div>
                </div>
            <div class="form_title">
                <div class="form_single">
                    <h4>Penilaian Agunan &nbsp;&nbsp;<asp:Label runat="server" ID="Label5" /></label>
                </div>
            </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Nilai Pasar Diakui</label>
                        <asp:TextBox ID="txtNilaiPasarDiakui" runat="server"></asp:TextBox>
                    </div>
                    <div class="form_right">          
                        <label> Nilai Telah Diagunkan/Pledging</label>
                        <asp:TextBox ID="txtNilaiPledging" runat="server" ></asp:TextBox>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Tgl Penilaian</label>
                        <asp:TextBox ID="txtTglPenilaian" runat="server" Width="10%"></asp:TextBox>
                         <asp:CalendarExtender ID="CalendarExtender7" runat="server" Enabled="True" TargetControlID="txtTglPenilaian" Format="dd/MM/yyyy" PopupPosition="BottomRight" DefaultView="Months"></asp:CalendarExtender> 
                    </div>
                    <div class="form_right">          
                        <label> Tgl Update Agunan</label>
                        <asp:TextBox ID="txtTglUpdAgunan" runat="server" Width="10%"></asp:TextBox>
                         <asp:CalendarExtender ID="CalendarExtender8" runat="server" Enabled="True" TargetControlID="txtTglUpdAgunan" Format="dd/MM/yyyy" PopupPosition="BottomRight" DefaultView="Months"></asp:CalendarExtender> 
                    </div>
                </div>
                <div class="form_box">
                <div class="form_left" style="background-color:#f9f9f9">
                    <h5>Penilaian Internal Berdasarkan Dokumen</h5>
                </div>
                <div class="form_right" style="background-color:#f9f9f9">
                    <h5>Penilaian Internal Berdasarkan Fisik</h5>
                </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Tgl Penilaian</label>
                        <asp:TextBox ID="txtPIBDTglPenilaian" runat="server" Width="10%"></asp:TextBox>
                         <asp:CalendarExtender ID="CalendarExtender9" runat="server" Enabled="True" TargetControlID="txtPIBDTglPenilaian" Format="dd/MM/yyyy" PopupPosition="BottomRight" DefaultView="Months"></asp:CalendarExtender> 
                    </div>
                    <div class="form_right">          
                        <label> Tgl Penilaian</label>
                        <asp:TextBox ID="txtPIBFTglPenilaian" runat="server" Width="10%"></asp:TextBox>
                         <asp:CalendarExtender ID="CalendarExtender10" runat="server" Enabled="True" TargetControlID="txtPIBFTglPenilaian" Format="dd/MM/yyyy" PopupPosition="BottomRight" DefaultView="Months"></asp:CalendarExtender> 
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Nilai Tanah</label>
                        <asp:TextBox ID="txtPIBDNilaiTanah" runat="server" ></asp:TextBox>
                    </div>
                    <div class="form_right">          
                        <label> Nilai Tanah</label>
                        <asp:TextBox ID="txtPIBFNilaiTanah" runat="server" ></asp:TextBox>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Nilai Bangunan</label>
                        <asp:TextBox ID="txtPIBDNilaiBangunan" runat="server"></asp:TextBox>
                    </div>
                    <div class="form_right">          
                        <label> Nilai Bangunan</label>
                        <asp:TextBox ID="txtPIBFNilaiBangunan" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Nilai Tanah & Bangunan</label>
                        <asp:TextBox ID="txtPIBDNilaiTanahBangunan" runat="server" ></asp:TextBox>
                    </div>
                    <div class="form_right">          
                        <label> Nilai Tanah & Bangunan</label>
                        <asp:TextBox ID="txtPIBFNilaiTanahBangunan" runat="server" ></asp:TextBox>
                    </div>
                </div>
                <div class="form_box">
                <div class="form_left" style="background-color:#f9f9f9">
                    <h5>Penilaian External Berdasarkan Dokumen</h5>
                </div>
                <div class="form_right" style="background-color:#f9f9f9">
                    <h5>Penilaian External Berdasarkan Fisik</h5>
                </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Tgl Penilaian</label>
                        <asp:TextBox ID="txtPEBDTglPenilaian" runat="server" Width="10%"></asp:TextBox>
                         <asp:CalendarExtender ID="CalendarExtender11" runat="server" Enabled="True" TargetControlID="txtPEBDTglPenilaian" Format="dd/MM/yyyy" PopupPosition="BottomRight" DefaultView="Months"></asp:CalendarExtender> 
                    </div>
                    <div class="form_right">
                        <label> Tgl Penilaian</label>
                        <asp:TextBox ID="txtPEBFTglPenilaian" runat="server" Width="10%"></asp:TextBox>
                         <asp:CalendarExtender ID="CalendarExtender12" runat="server" Enabled="True" TargetControlID="txtPEBFTglPenilaian" Format="dd/MM/yyyy" PopupPosition="BottomRight" DefaultView="Months"></asp:CalendarExtender> 
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Nilai Tanah</label>
                        <asp:TextBox ID="txtPEBDNilaiTanah" runat="server" ></asp:TextBox>
                    </div>
                    <div class="form_right">          
                        <label> Nilai Tanah</label>
                        <asp:TextBox ID="txtPEBFNilaiTanah" runat="server" ></asp:TextBox>
                    </div>
                </div>

                <div class="form_box">
                    <div class="form_left">
                        <label> Nilai Bangunan</label>
                        <asp:TextBox ID="txtPEBDNilaiBangunan" runat="server"></asp:TextBox>
                    </div>
                    <div class="form_right">
                        <label> Nilai Bangunan</label>
                        <asp:TextBox ID="txtPEBFNilaiBangunan" runat="server"></asp:TextBox>                                
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Nilai Tanah & Bangunan</label>
                        <asp:TextBox ID="txtPEBDNilaiTanahBangunan" runat="server" ></asp:TextBox>
                    </div>
                    <div class="form_right">          
                        <label> Nilai Tanah & Bangunan</label>
                        <asp:TextBox ID="txtPEBFNilaiTanahBangunan" runat="server" ></asp:TextBox>
                    </div>
                </div>
                <div class="form_title">
                    <div class="form_single">
                        <h4>Pengikatan Jaminan &nbsp;&nbsp;<asp:Label runat="server" ID="Label6" /></label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Nomor SKMHT</label>
                        <asp:TextBox ID="txtPJNoSKMHT" runat="server"></asp:TextBox>
                    </div>
                    <div class="form_right">          
                        <label> Tanggal SKMHT</label>
                        <asp:TextBox ID="txtPJTglSKMHT" runat="server" Width="10%"></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True" TargetControlID="txtPJTglSKMHT" Format="dd/MM/yyyy" PopupPosition="BottomRight" DefaultView="Months"></asp:CalendarExtender>               
                    </div>
                </div>
			    <div class="form_box">
                    <div class="form_left">
                        <label> Tgl Jatuh Tempo SKMHT</label>
                        <asp:TextBox ID="txtPJTglJatuhTempoSKMHT" runat="server"></asp:TextBox>
                    </div>
                    <div class="form_right">          
                        <label> Nomor APHT</label>
                        <asp:TextBox ID="txtPJNoAPHT" runat="server" ></asp:TextBox>
                    </div>
                </div>                
				<div class="form_box">
                    <div class="form_left">
                        <label> Tanggal APHT</label>
                        <asp:TextBox ID="txtPJTglAPHT" runat="server" Width="10%"></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True" TargetControlID="txtPJTglAPHT" Format="dd/MM/yyyy" PopupPosition="BottomRight" DefaultView="Months"></asp:CalendarExtender>               
                    </div>
                    <div class="form_right">          
                        <label> Nomor SHT</label>
                        <asp:TextBox ID="txtPJNoSHT" runat="server" ></asp:TextBox>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Tanggal Issued SHT</label>
                        <asp:TextBox ID="txtPJTglIssuedSHT" runat="server" Width="10%"></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender3" runat="server" Enabled="True" TargetControlID="txtPJTglIssuedSHT" Format="dd/MM/yyyy" PopupPosition="BottomRight" DefaultView="Months"></asp:CalendarExtender>               
                    </div>
                    <div class="form_right color_yellow">          
                        <label> Total Nilai HT</label>
                        <asp:TextBox ID="txtPJTotalNilaiHT" runat="server" CssClass="rightAlign" onKeyUp="number()"></asp:TextBox>
                    </div>
                </div>
			    <div class="form_box">
                    <div class="form_left">
                        <label> Rangking HT</label>
                        <asp:TextBox ID="txtPJRangkingHT" runat="server"></asp:TextBox>
                    </div>
                    <div class="form_right">          
                        <label> Jenis Pengikatan</label>
                        <asp:TextBox ID="txtPJJenisPengikatan" runat="server" ></asp:TextBox>
                    </div>
                </div>                
				<div class="form_box">
                    <div class="form_left">
                        <label> Notaris</label>
                        <asp:TextBox ID="txtPJNotaris" runat="server"></asp:TextBox>
                    </div>
                    <div class="form_right">          
                        <label> Lokasi Penyimpanan Dokumen</label>
                        <asp:TextBox ID="txtPJLokasiPenyimpananDokumen" runat="server" ></asp:TextBox>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Kantor BPN Wilayah</label>
                        <asp:TextBox ID="txtPJKantorBPNWilayah" runat="server"></asp:TextBox>
                    </div>
                    <div class="form_right">          

                    </div>
                </div>
                <div class="form_title">
                    <div class="form_single">
                        <h4>Bangunan &nbsp;&nbsp;<asp:Label runat="server" ID="Label4" /></label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Fisik Jaminan</label>
                        <asp:TextBox ID="txtBgnFisikJaminan" runat="server"></asp:TextBox>
                    </div>
                    <div class="form_right">          
                        <label> Surat Izin</label>
                        <asp:TextBox ID="txtBgnSuratIzin" runat="server" ></asp:TextBox>
                    </div>
                </div>
			    <div class="form_box">
                    <div class="form_left">
                        <label> Nomor Surat Izin</label>
                        <asp:TextBox ID="txtBgnNoSuratIzin" runat="server"></asp:TextBox>
                    </div>
                    <div class="form_right">          
                        <label> Jenis Surat Izin</label>
                        <asp:TextBox ID="txtBgnJenisSuratIzin" runat="server" ></asp:TextBox>
                    </div>
                </div>                
				<div class="form_box">
                    <div class="form_left">
                        <label> Peruntukan Bangunan</label>
                        <asp:TextBox ID="txtBgnPeruntukanBangunan" runat="server"></asp:TextBox>
                    </div>
                    <div class="form_right">          
                        <label> Nomor Izin Layak Huni</label>
                        <asp:TextBox ID="txtBgnNoIzinLayakHuni" runat="server" ></asp:TextBox>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> PBB Tahun Terakhir</label>
                        <asp:TextBox ID="txtBgnPBBTahunTerakhir" runat="server"></asp:TextBox>
                    </div>
                    <div class="form_right">          
                        <label> Nilai NJOP</label>
                        <asp:TextBox ID="txtBgnNilaiNJOP" runat="server" ></asp:TextBox>
                    </div>
                </div>
			    <div class="form_box">
                    <div class="form_left">
                        <label> Cetak Biru</label>
                        <asp:TextBox ID="txtBgnCetakBiru" runat="server"></asp:TextBox>
                    </div>
                    <div class="form_right">          
                        <label> Akta Balik Nama</label>
                        <asp:TextBox ID="txtBgnAktaBalikNama" runat="server" ></asp:TextBox>
                    </div>
                </div>                
				<div class="form_box">
                    <div class="form_left">
                        <label> Nomor Akta</label>
                        <asp:TextBox ID="txtBgnNoAkta" runat="server"></asp:TextBox>
                    </div>
                    <div class="form_right">          
                        <label> Tanggal Akta</label>
                        <asp:TextBox ID="txtBgnTglAkta" runat="server" Width="10%"></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender4" runat="server" Enabled="True" TargetControlID="txtBgnTglAkta" Format="dd/MM/yyyy" PopupPosition="BottomRight" DefaultView="Months"></asp:CalendarExtender>               
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Notaris</label>
                        <asp:TextBox ID="txtBgnNotaris" runat="server"></asp:TextBox>
                    </div>
                    <div class="form_right">          
                        <label> Paripasu (%)</label>
                        <asp:TextBox ID="BgnParipasu" runat="server" ></asp:TextBox>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Bangunan diasuransikan</label>
                        <asp:TextBox ID="BgnBangunanDiasuransikan" runat="server"></asp:TextBox>
                    </div>
                    <div class="form_right">          
                       
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="small button gray"
                        CausesValidation="False"></asp:Button>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
        <script type="text/javascript">
        $(document).ready(function () {
            number()
        });

        function number()
        {
            let nilaiPasarDiakui = document.getElementById('txtNilaiPasarDiakui').value;
            let nilaiPasarDiakuiA = parseInt(nilaiPasarDiakui.replace(/\s*,\s*/g, ''));
            $('#txtNilaiPasarDiakui').val(number_format(nilaiPasarDiakuiA, 0));

            let luasTanah = document.getElementById('txtLuasTanah').value;
            let luasTanahA = parseInt(luasTanah.replace(/\s*,\s*/g, ''));
            $('#txtLuasTanah').val(number_format(luasTanahA, 0));

            //total nilai
            let txPJTotalNilaiHT = document.getElementById('txtPJTotalNilaiHT').value;
            let PJTotalNilaiHT = parseInt(txPJTotalNilaiHT.replace(/\s*,\s*/g, ''));
            $('#txtPJTotalNilaiHT').val(number_format(PJTotalNilaiHT, 0));

        }

        function number_format(number, decimals, dec_point, thousands_sep) {

            number = (number + '')
            .replace(/[^0-9+\-Ee.]/g, '');
            var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function (n, prec) {
                var k = Math.pow(10, prec);
                return '' + (Math.round(n * k) / k)
                .toFixed(prec);
            };
            // Fix for IE parseFloat(0.55).toFixed(0) = 0;
            s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
            .split('.');
            if (s[0].length > 3) {
                s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
            }
            if ((s[1] || '')
            .length < prec) {
                s[1] = s[1] || '';
                s[1] += new Array(prec - s[1].length + 1)
              .join('0');
            }
            return s.join(dec);
        }
    </script>
</body>
</html>
