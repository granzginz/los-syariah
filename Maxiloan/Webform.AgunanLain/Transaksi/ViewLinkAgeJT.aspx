﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewLinkAgeJT.aspx.vb" Inherits="Maxiloan.Webform.AgunanLain.ViewLinkAgeJT" %>

<%@ Register Src="../../webform.UserController/ucAgunanLainInvoiceViewTab.ascx" TagName="ucLandBuildingTab"
    TagPrefix="uc7" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register TagPrefix="uc1" TagName="ucassetdata" Src="../../Webform.UserController/ViewApplication/UcAssetData.ascx" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ViewAsset</title>
    <link href="../../Include/Buttons.css" type="text/css" rel="Stylesheet"/>
    <link href="../../Include/General.css" type="text/css" rel="stylesheet" />
    <link href="../../Include/AccMnt.css" type="text/css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
    <uc7:ucLandBuildingTab id="ucLandTab1" runat="server" />
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h4> <asp:Label ID="lblTitle" runat="server"></asp:Label> LINKAGE JAMINAN TAMBAHAN </h4>
                </div>
            </div>

                <div class="form_box">
                    <div class="form_left">
                        <label> ID Agunan</label>
                        <asp:Label ID="lblCollateralID" runat="server" ></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>ID Customer</label>
                        <asp:Label ID="lblCustomerID" runat="server" ></asp:Label>
                    </div>
                </div>
                <div class="form_box_title">
                    <div class="form_left">
                        <h5>
                            JAMINAN TAMBAHAN</h5>
                    </div>
                    <div class="form_right">
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_left">
                        <asp:GridView runat="server" ID="gvAgunanNett" AllowPaging="false" AutoGenerateColumns="false"
                            DataKeyNames="TransID,ApplicationID" CssClass="grid_general" ShowFooter="true"
                            ShowHeader="false">
                            <HeaderStyle CssClass="th" />
                            <RowStyle CssClass="item_grid" />
                            <FooterStyle CssClass="item_grid ft" />
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblTransDesc1" Text='<%# Container.DataItem("TransDesc") %>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <label class="label_auto">
                                            Total Nilai Jaminan Tambahan</label>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField  HeaderStyle-CssClass="th_right" ItemStyle-CssClass="item_grid_right" FooterStyle-CssClass="item_grid_right ft">
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblNilai" Text='<%#FormatNumber(Container.DataItem("Nilai"), 0) %>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label runat="server" ID="lblTotalNilai"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="AgunanNett" />
                            </Columns>
                        </asp:GridView>
                    </div>
                    <div class="form_right">
                        <div class="form_right"  style="background-color:#1eefbc">
                        <label> Sisa Yang Belum Dialokasi</label>
                        <asp:Label ID="lblSisaAlokasi" runat="server" ></asp:Label>
                    </div>
                    </div>
                </div>  
            <div class="form_box_title">
                <div class="form_single">
                    <h5>
                        Daftar Kontrak Yang Sudah Dijaminkan</h5>
                </div>
            </div>  
        <div class="form_box_header">
            <div class="form_single"><div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgPledging" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                  CssClass="grid_general"
                    ShowFooter="True">
                 <HeaderStyle CssClass="th" />
                      <ItemStyle CssClass="item_grid" />
                        <FooterStyle CssClass="item_grid" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="ID Customer" SortExpression="CustomerID">
                            <ItemStyle HorizontalAlign="Left" Width="18%"></ItemStyle>
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkCustomerID" runat="server" CausesValidation="false" Text='<%# DataBinder.Eval(Container, "DataItem.CustomerID") %>'>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="Name" HeaderText="Nama Konsumen">
                            <HeaderStyle HorizontalAlign="Center" Width="22%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" Width="22%"></ItemStyle>
                            <ItemTemplate>
                                <asp:HyperLink ID="hyCustomerName" runat="server" Text='<%#Container.DataItem("Name")%>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="ID Data" SortExpression="ApplicationID">
                            <ItemStyle HorizontalAlign="Left" Width="18%"></ItemStyle>
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkApplicationID" runat="server" CausesValidation="false" Text='<%# DataBinder.Eval(Container, "DataItem.ApplicationID") %>'>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="No Kontrak" SortExpression="AgreementNo">
                            <ItemStyle HorizontalAlign="Left" Width="18%"></ItemStyle>
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkAgreementNo" runat="server" CausesValidation="false" Text='<%# DataBinder.Eval(Container, "DataItem.AgreementNo") %>'>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="ID Collateral" SortExpression="CollateralID">
                            <ItemStyle HorizontalAlign="Left" Width="18%"></ItemStyle>
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkCollateralID" runat="server" CausesValidation="false" Text='<%# DataBinder.Eval(Container, "DataItem.CollateralID") %>'>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="NTF" HeaderText="NTF">
                            <HeaderStyle CssClass="th_right" Width="10%"></HeaderStyle>
                            <ItemStyle CssClass="item_grid_right" Width="10%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblNTF" runat="server" Text='<%#FormatNumber(Container.DataItem("NTF"), 2)%>'>
                                </asp:Label>
                            </ItemTemplate>
                           <FooterTemplate>
                               <label class="label_auto">
                                   Total </label>
                           </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="PercentagePledged" HeaderText="Pledging Persentase">
                            <HeaderStyle CssClass="th_right" Width="10%"></HeaderStyle>
                            <ItemStyle CssClass="item_grid_right" Width="10%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblPercentagePledged" runat="server" Text='<%#FormatNumber(Container.DataItem("PercentagePledged"), 2)%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Pledging Amount">
                            <HeaderStyle CssClass="th_right" Width="10%"></HeaderStyle>
                            <ItemStyle CssClass="item_grid_right" Width="10%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblPledgedAmount" runat="server" Text='<%#FormatNumber(Container.DataItem("AmountPledged"), 2)%>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterStyle HorizontalAlign="Right"></FooterStyle>
                            <FooterTemplate>
                                <asp:Label ID="lblTotalPledgingAmount" runat="server">Label</asp:Label>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <PagerStyle Visible="False" HorizontalAlign="Left"  
                        Mode="NumericPages"></PagerStyle>
                </asp:DataGrid>
            </div></div>
        </div>

                <div class="tab_container_form_space"><br /><br /><br /><br /></div>
                <div class="tab_container_button">
                    <div class="form_button">
                        <asp:Button ID="btnClose" OnClientClick="javascript:window.close();" runat="server"
                            Text="Close" CssClass="small button gray" CausesValidation="False"></asp:Button>
                    </div>
                </div>
    </form>
</body>
</html>
