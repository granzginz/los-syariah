﻿Public Class CustomerJaminanTambahanTabs
    Inherits CustomerTambahanJaminanTabsBase
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If Not Page.IsPostBack Then

		End If
	End Sub
    Protected Overrides Sub initTabs()
        _tabs = New Dictionary(Of String, CustomerJaminanTambahanTab) From {
                           {"tabLandBuilding", New CustomerJaminanTambahanTab(tabLandBuilding, hyLandBuilding)},
                           {"tabBPKB", New CustomerJaminanTambahanTab(tabBPKB, hyBPKB)},
                           {"tabInvoice", New CustomerJaminanTambahanTab(tabInvoice, hyInvoice)},
                           {"tabSertifikat", New CustomerJaminanTambahanTab(tabSertifikat, hySertifikat)},
                           {"tabPinjamanLainnya", New CustomerJaminanTambahanTab(tabPinjamanLainnya, hyPinjamanLainnya)}}
    End Sub

	'Public Overrides Sub SetNavigateUrl(page As String, id As String)
	'	'MyBase.SetNavigateUrl(page, id)
	'	'_tabs("tabLandBuilding").Link.NavigateUrl = "JTLandBuilding.aspx?page=" + page + "&id=" + id + "&pnl=tabLandBuilding"
	'	'_tabs("tabBPKB").Link.NavigateUrl = "JTBPKB.aspx?page=" + page + "&id=" + id + "&pnl=tabBPKB"
	'	'_tabs("tabInvoice").Link.NavigateUrl = "JTInvoice.aspx?page=" + page + "&id=" + id + "&pnl=tabInvoice"
	'	'_tabs("tabSertifikat").Link.NavigateUrl = "JTSertifikat.aspx?page=" + page + "&id=" + id + "&pnl=tabSertifikat"
	'	'_tabs("tabPinjamanLainnya").Link.NavigateUrl = "CustomerPersonalPinjamanLainnya.aspx?page=" + page + "&id=" + id + "&pnl=tabPinjamanLainnya"
	'End Sub

	'*** abdi 31/08/2018 ***'
	Public Overrides Sub SetNavigateUrl(page As String, CollateralId As String, CustomerId As String)
		MyBase.SetNavigateUrl(page, CollateralId, CustomerId)
		_tabs("tabLandBuilding").Link.NavigateUrl = "JTLandBuilding.aspx?page=" + page + "&CustomerID=" + CustomerId + "&Collateralid=" + CollateralId + "&pnl=tabLandBuilding"
		_tabs("tabBPKB").Link.NavigateUrl = "JTBPKB.aspx?page=" + page + "&CustomerID=" + CustomerId + "&Collateralid=" + CollateralId + "&pnl=tabBPKB"
		_tabs("tabInvoice").Link.NavigateUrl = "JTInvoice.aspx?page=" + page + "&CustomerID=" + CustomerId + "&Collateralid=" + CollateralId + "&pnl=tabInvoice"
		_tabs("tabSertifikat").Link.NavigateUrl = "JTSertifikat.aspx?page=" + page + "&CustomerID=" + CustomerId + "&Collateralid=" + CollateralId + "&pnl=tabSertifikat"
		_tabs("tabPinjamanLainnya").Link.NavigateUrl = "CustomerPersonalPinjamanLainnya.aspx?page=" + page + "&CustomerID=" + CustomerId + "&Collateralid=" + CollateralId + "&pnl=tabPinjamanLainnya"
	End Sub

	Public Overrides Sub RefreshAttr(pnl As String)
        MyBase.RefreshAttr(pnl)
        If pnl = "" Then
            _tabs("tabLandBuilding").Tab.Attributes.Remove("class")
            _tabs("tabLandBuilding").Tab.Attributes.Add("class", "tab_selected")
        End If
    End Sub
End Class