﻿Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController

Public Class LookupAgreementLinkage
	Inherits Maxiloan.Webform.WebBased

	Protected WithEvents oBranch As ucBranchAll
	Protected WithEvents oSearchBy As UcSearchBy


#Region "Constanta"
	Private currentPage As Int32 = 1
	Private pageSize As Int16 = 10
	Private currentPageNumber As Int16 = 1
	Private totalPages As Double = 1
	Private recordCount As Int64 = 1

	Private oCustomClass As New Maxiloan.Parameter.AgreementList
	Private oController As New AgreementListController
#End Region

#Region "Page Load"


	Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
		'Put user code to initialize the page here
		If SessionInvalid() Then
			Exit Sub
		End If
		If Not IsPostBack Then

			oSearchBy.ListData = "Name, Name-ApplicationID,Application ID-Agreementno, Agreement No-Address, Address-InstallmentAmount, Installment"
			oSearchBy.BindData()
			Me.SearchBy = ""
			Me.SortBy = ""
			ImbSelect.Attributes.Add("onclick", "Select_Click()")
			imbExit.Attributes.Add("onclick", "Close_Window()")
		End If
	End Sub
#End Region

#Region "DoBind"
	Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
		Dim DtUserList As New DataTable
		Dim DvUserList As New DataView
		Dim intloop As Integer
		Dim hypID As HyperLink

		With oCustomClass
			.strConnection = GetConnectionString()
			.WhereCond = cmdWhere
			.CurrentPage = currentPage
			.PageSize = pageSize
			.SortBy = SortBy
		End With

		oCustomClass = oController.AgreementListLinkAge(oCustomClass)

		DtUserList = oCustomClass.ListAgreement
		DvUserList = DtUserList.DefaultView
		recordCount = oCustomClass.TotalRecord
		DvUserList.Sort = Me.SortBy
		DtgAgree.DataSource = DvUserList

		Try
			DtgAgree.DataBind()
		Catch
			DtgAgree.CurrentPageIndex = 0
			DtgAgree.DataBind()
		End Try
		PagingFooter()
		pnlList.Visible = True
		pnlDatagrid.Visible = True

		lblMessage.Text = ""
		lblMessage.Visible = False
	End Sub

#End Region

#Region "Sort"
	Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
		If InStr(Me.SortBy, "DESC") > 0 Then
			Me.SortBy = e.SortExpression
		Else
			Me.SortBy = e.SortExpression + " DESC"
		End If
		DoBind(Me.SearchBy, Me.SortBy)
	End Sub
#End Region

#Region "Navigation"
	Private Sub PagingFooter()
		lblPage.Text = currentPage.ToString()
		totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
		If totalPages = 0 Then
			ShowMessage(lblMessage, "Data Not Found .....", True)
			lblTotPage.Text = "1"
			rgvGo.MaximumValue = "1"
		Else
			lblTotPage.Text = CType(totalPages, String)
			rgvGo.MaximumValue = CType(totalPages, String)
		End If
		lblRecord.Text = CType(recordCount, String)

		If currentPage = 1 Then
			imbPrevPage.Enabled = False
			imbFirstPage.Enabled = False
			If totalPages > 1 Then
				imbNextPage.Enabled = True
				imbLastPage.Enabled = True
			Else
				imbPrevPage.Enabled = False
				imbNextPage.Enabled = False
				imbLastPage.Enabled = False
				imbFirstPage.Enabled = False
			End If
		Else
			imbPrevPage.Enabled = True
			imbFirstPage.Enabled = True
			If currentPage = totalPages Then
				imbNextPage.Enabled = False
				imbLastPage.Enabled = False
			Else
				imbLastPage.Enabled = True
				imbNextPage.Enabled = True
			End If
		End If
	End Sub

	Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
		Select Case e.CommandName
			Case "First" : currentPage = 1
			Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
			Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
			Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
		End Select
		If Me.SortBy Is Nothing Then
			Me.SortBy = ""
		End If
		DoBind(Me.SearchBy, Me.SortBy)
	End Sub

	Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles imbGoPage.Click
		If IsNumeric(txtPage.Text) Then
			If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
				currentPage = CType(txtPage.Text, Int16)
				If Me.SortBy Is Nothing Then
					Me.SortBy = ""
				End If
				DoBind(Me.SearchBy, Me.SortBy)
			End If
		End If
	End Sub
#End Region
#Region "DtgAgree ItemDataBound"

	Private Sub DtgAgree_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgree.ItemDataBound
		Dim lblTemp As Label

		If SessionInvalid() Then
			Exit Sub
		End If
		Dim m As Int32
		Dim hypReceive As HyperLink
		Dim lblApplicationid As HyperLink
		If e.Item.ItemIndex >= 0 Then
			lblApplicationid = CType(e.Item.FindControl("hyApplicationid"), HyperLink)
			lblTemp = CType(e.Item.FindControl("lblCustomerId"), Label)
			Dim hyTemp As HyperLink
			'*** Customer Link
			lblTemp = CType(e.Item.FindControl("lblCustomerId"), Label)
			hyTemp = CType(e.Item.FindControl("hyCustomerName"), HyperLink)
			hyTemp.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(lblTemp.Text.Trim) & "')"
			'*** Agreement No link
			hyTemp = CType(e.Item.FindControl("hyAgreementNo"), HyperLink)
			hyTemp.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(lblApplicationid.Text.Trim) & "')"
			'*** ApplicationId link
			hyTemp = CType(e.Item.FindControl("hyApplicationId"), HyperLink)
			hyTemp.NavigateUrl = "javascript:OpenApplicationId('" & "AccMnt" & "', '" & Server.UrlEncode(hyTemp.Text.Trim) & "')"

		End If
	End Sub
#End Region

#Region "Reset"
	Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles imbReset.Click
		Response.Redirect("LookupAgreementLinkage.aspx")
	End Sub
#End Region
#Region "Radio Script"
	Public Function Get_Radio(ByVal pApplicationID As String, ByVal pAgreementNo As String, ByVal pTotalOTR As Decimal, pNTF As Decimal) As String
		'Return "<input type=radio name=rboAgreeement onclick=""javascript:Agreement_Checked('" & Trim(pApplicationID) & "','" & Trim(pAgreementNo) & "','" & Trim(oBranch.BranchID) & "')"" value='" & pAgreementNo & "'>"
		Return "<input type=radio name=rboAgreeement onclick=""javascript:Agreement_Checked('" & Trim(pApplicationID) & "','" & Trim(pAgreementNo) & "','" & Trim(oBranch.BranchID) & "','" & Trim(pTotalOTR) & "','" & Trim(pNTF) & "')"" value='" & pAgreementNo & "'>"
	End Function
#End Region
#Region "Search"
	Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles imgsearch.Click
		If SessionInvalid() Then
			Exit Sub
		End If
		Me.SearchBy = " branchid = '" & oBranch.BranchID.Trim & "' "

		'If oSearchBy.Text.Trim <> "" Then
		'	Me.SearchBy = Me.SearchBy & " and " & oSearchBy.ValueID & " = '" & oSearchBy.Text.Trim.Replace("'", "''") & "'"
		'End If

		If oSearchBy.Text.Trim <> "" Then
			Me.SearchBy = Me.SearchBy & " and " & oSearchBy.ValueID & " LIKE '%" & oSearchBy.Text.Trim.Replace("'", "''") & "%'"
		End If
		pnlDatagrid.Visible = True
		pnlList.Visible = True
		DoBind(Me.SearchBy, Me.SortBy)
		DtgAgree.Visible = True
	End Sub
#End Region
End Class