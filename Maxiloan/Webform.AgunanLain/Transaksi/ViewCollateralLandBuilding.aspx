﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewCollateralLandBuilding.aspx.vb" Inherits="Maxiloan.Webform.AgunanLain.ViewCollateralLandBuilding" %>

<%@ Register Src="../../webform.UserController/ucAgunanLainInvoiceViewTab.ascx" TagName="ucLandBuildingTab"
    TagPrefix="uc7" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>View Jaminan Tambahan</title>
    <link rel="Stylesheet" type="text/css" href="../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../Include/Buttons.css" />
    <link  rel="Stylesheet" href="../../Include/default/easyui.css" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="../../js/jquery.easyui.min.js"></script>

    <style type="stylesheet">
        .multiline_textbox {
            width: 60% !important;
        }
    </style>

</head>

<body onload="gridGeneralSize('dtgTC');getGridGeneralSize('hdnGridGeneralSize');"
    onresize="gridGeneralSize('dtgTC');getGridGeneralSize('hdnGridGeneralSize');">

    <asp:UpdateProgress ID="upg1" runat="server" AssociatedUpdatePanelID="upnl1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    <form id="form1" runat="server">
    <uc7:ucLandBuildingTab id="ucLandTab1" runat="server" />
    <asp:ScriptManager runat="server" ID="ScriptManager1"></asp:ScriptManager>
    <div runat="server" id="jlookupContent" />

    <asp:UpdatePanel runat="server" ID="upnl1" UpdateMode="Conditional" ChildrenAsTriggers="false">
        <ContentTemplate>
            <input id="hdnGridGeneralSize" runat="server" type="hidden" />
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" />
            <asp:Label ID="Label7" runat="server"></asp:Label>
                <div class="form_title">
                    <div class="title_strip">
                    </div>
                    <div class="form_single">
                        <h4> <asp:Label ID="lblTitle" runat="server"></asp:Label> Tanah dan Bangunan </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                    <label>Jenis Agunan</label>
                        <div class="form_box_hide">
                        <asp:DropDownList ID="cboJenisAgunan" Enabled="false" runat="server" onchange="cboPHomeStatus_IndexChanged(this.value);">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" Display="Dynamic"
                            InitialValue="Select One" ControlToValidate="cboJenisAgunan" ErrorMessage="Harap pilih jenis Agunan"
                            CssClass="validator_general"></asp:RequiredFieldValidator>
                        </div>
                        <asp:Label runat="server" ID="lblJenisAgunan"></asp:Label>
                    </div>
                    <div class="form_right">
                         <label> ID Agunan</label>
                        <asp:Label ID="lblIDAgunan" runat="server" CssClass="label_auto"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Status Agunan</label>
                        <div class="form_box_hide">
                        <asp:DropDownList ID="cboStatusAgunan" runat="server" Enabled="false" onchange="StatusAgunan()">
                            <asp:ListItem Value="0">Iddle</asp:ListItem>
                            <asp:ListItem Value="1">Pledged</asp:ListItem>
                            <asp:ListItem Value="2">Lunas</asp:ListItem>
                            <asp:ListItem Value="3">Ditarik</asp:ListItem>
                        </asp:DropDownList>
                        </div>
                        <asp:Label runat="server" ID="lblStatusAgunan"></asp:Label>
                    </div>
                    <div class="form_right">          
                        <label> Currency</label>
                        <asp:Label ID="lblCurrency" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Agunan Seq No</label>
                        <asp:Label ID="lblAgunanSeqNo" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">

                    </div>
                </div>
            <div class="form_title">
                <div class="form_single">
                    <h4>Identitas Pemilik Agunan &nbsp;&nbsp;<asp:Label runat="server" ID="Label1" /></h4>
                </div>
            </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Pemilik Agunan</label>
                        <asp:Label ID="lblPemilikAgunan" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">          
                        <label> Alamat </label>
                        <asp:Label ID="lblAlamatAgunan" runat="server" TextMode="MultiLine" CssClass="multiline_textbox"></asp:Label>
                    </div>
                </div> 
            <div class="form_title">
                <div class="form_single">
                    <h4>Sertifikat &nbsp;&nbsp;<asp:Label runat="server" ID="Label2" /></h4>
                </div>
            </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Status Sertifikat</label>
                        <asp:Label ID="lblStatusSertifikat" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">          
                        <label> Jenis Sertifikat</label>
                        <asp:Label ID="lblJenisSertifikat" runat="server" ></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Tahun Penerbitan Sertifikat</label>
                        <asp:Label ID="lblThnPenerbitanSertifikat" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">          
                        <label> Nomor Sertifikat</label>
                        <asp:Label ID="lblNoSertifikat" runat="server" ></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Nomor Gambar Situasi</label>
                        <asp:Label ID="lblNoGambarSituasi" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">          
                        <label> Developer</label>
                        <asp:Label ID="lblDevelopr" runat="server" ></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Luas Tanah</label>
                        <asp:Label ID="lblLuasTanah" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">          
                        <label> Luas Bangunan</label>
                        <asp:Label ID="lblLuasBangunan" runat="server" ></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Progress Konstruksi</label>
                        <asp:Label ID="lblPrgKonstruksi" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">          
                        <label> Tgl Update Konstruksi</label>
                        <asp:Label ID="lblTglUpdKonstruksi" runat="server" ></asp:Label>
                    </div>
                </div>
            <div class="form_title">
                <div class="form_single">
                    <h4>Lokasi Agunan &nbsp;&nbsp;<asp:Label runat="server" ID="Label3" /></h4>
                </div>
            </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Kecamatan</label>
                        <asp:Label ID="lblKecamatan" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">          
                        <label> Desa</label>
                        <asp:Label ID="lblDesa" runat="server" ></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Persil / Alamat</label>
                        <asp:Label ID="lblAlamat" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">          
                        <label> Sandi Lokasi</label>
                        <asp:Label ID="lblSandiLokasi" runat="server" ></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Mortgage Type</label>
                        <asp:Label ID="lblMortgagetype" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">          
                        <label> Property Type</label>
                        <asp:Label ID="lblPropertyType" runat="server" ></asp:Label>
                    </div>
                </div>
            <div class="form_title">
                <div class="form_single">
                    <h4>Penilaian Agunan &nbsp;&nbsp;<asp:Label runat="server" ID="Label5" /></h4>
                </div>
            </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Nilai Pasar Diakui</label>
                        <asp:Label ID="lblNilaiPasarDiakui" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">          
                        <label> Nilai Telah Diagunkan/Pledging</label>
                        <asp:Label ID="lblNilaiPledging" runat="server" ></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Tgl Penilaian</label>
                        <asp:Label ID="lblTglPenilaian" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">          
                        <label> Tgl Update Agunan</label>
                        <asp:Label ID="lblTglUpdAgunan" runat="server" ></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                <div class="form_left" style="background-color:#f9f9f9">
                    <h5>Penilaian Internal Berdasarkan Dokumen</h5>
                </div>
                <div class="form_right" style="background-color:#f9f9f9">
                    <h5>Penilaian Internal Berdasarkan Fisik</h5>
                </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Tgl Penilaian</label>
                        <asp:Label ID="lblPIBDTglPenilaian" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">          
                        <label> Tgl Penilaian</label>
                        <asp:Label ID="lblPIBFTglPenilaian" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Nilai Tanah</label>
                        <asp:Label ID="lblPIBDNilaiTanah" runat="server" ></asp:Label>
                    </div>
                    <div class="form_right">          
                        <label> Nilai Tanah</label>
                        <asp:Label ID="lblPIBFNilaiTanah" runat="server" ></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Nilai Bangunan</label>
                        <asp:Label ID="lblPIBDNilaiBangunan" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">          
                        <label> Nilai Bangunan</label>
                        <asp:Label ID="lblPIBFNilaiBangunan" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Nilai Tanah & Bangunan</label>
                        <asp:Label ID="lblPIBDNilaiTanahBangunan" runat="server" ></asp:Label>
                    </div>
                    <div class="form_right">          
                        <label> Nilai Tanah & Bangunan</label>
                        <asp:Label ID="lblPIBFNilaiTanahBangunan" runat="server" ></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                <div class="form_left" style="background-color:#f9f9f9">
                    <h5>Penilaian External Berdasarkan Dokumen</h5>
                </div>
                <div class="form_right" style="background-color:#f9f9f9">
                    <h5>Penilaian External Berdasarkan Fisik</h5>
                </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Tgl Penilaian</label>
                        <asp:Label ID="lblPEBDTglPenilaian" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label> Tgl Penilaian</label>
                        <asp:Label ID="lblPEBFTglPenilaian" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Nilai Tanah</label>
                        <asp:Label ID="lblPEBDNilaiTanah" runat="server" ></asp:Label>
                    </div>
                    <div class="form_right">          
                        <label> Nilai Tanah</label>
                        <asp:Label ID="lblPEBFNilaiTanah" runat="server" ></asp:Label>
                    </div>
                </div>

                <div class="form_box">
                    <div class="form_left">
                        <label> Nilai Bangunan</label>
                        <asp:Label ID="lblPEBDNilaiBangunan" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label> Nilai Bangunan</label>
                        <asp:Label ID="lblPEBFNilaiBangunan" runat="server"></asp:Label>                                
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Nilai Tanah & Bangunan</label>
                        <asp:Label ID="lblPEBDNilaiTanahBangunan" runat="server" ></asp:Label>
                    </div>
                    <div class="form_right">          
                        <label> Nilai Tanah & Bangunan</label>
                        <asp:Label ID="lblPEBFNilaiTanahBangunan" runat="server" ></asp:Label>
                    </div>
                </div>
                <div class="form_title">
                    <div class="form_single">
                        <h4>Pengikatan Jaminan &nbsp;&nbsp;<asp:Label runat="server" ID="Label6" /></label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Nomor SKMHT</label>
                        <asp:Label ID="lblPJNoSKMHT" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">          
                        <label> Tanggal SKMHT</label>
                        <asp:Label ID="lblPJTglSKMHT" runat="server" ></asp:Label>
                    </div>
                </div>
			    <div class="form_box">
                    <div class="form_left">
                        <label> Tgl Jatuh Tempo SKMHT</label>
                        <asp:Label ID="lblPJTglJatuhTempoSKMHT" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">          
                        <label> Nomor APHT</label>
                        <asp:Label ID="lblPJNoAPHT" runat="server" ></asp:Label>
                    </div>
                </div>                
				<div class="form_box">
                    <div class="form_left">
                        <label> Tanggal APHT</label>
                        <asp:Label ID="lblPJTglAPHT" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">          
                        <label> Nomor SHT</label>
                        <asp:Label ID="lblPJNoSHT" runat="server" ></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Tanggal Issued SHT</label>
                        <asp:Label ID="lblPJTglIssuedSHT" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">          
                        <label> Total Nilai HT</label>
                        <asp:Label ID="lblPJTotalNilaiHT" runat="server" ></asp:Label>
                    </div>
                </div>
			    <div class="form_box">
                    <div class="form_left">
                        <label> Rangking HT</label>
                        <asp:Label ID="lblPJRangkingHT" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">          
                        <label> Jenis Pengikatan</label>
                        <asp:Label ID="lblPJJenisPengikatan" runat="server" ></asp:Label>
                    </div>
                </div>                
				<div class="form_box">
                    <div class="form_left">
                        <label> Notaris</label>
                        <asp:Label ID="lblPJNotaris" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">          
                        <label> Lokasi Penyimpanan Dokumen</label>
                        <asp:Label ID="lblPJLokasiPenyimpananDokumen" runat="server" ></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Kantor BPN Wilayah</label>
                        <asp:Label ID="lblPJKantorBPNWilayah" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">          

                    </div>
                </div>
                <div class="form_title">
                    <div class="form_single">
                        <h4>Bangunan &nbsp;&nbsp;<asp:Label runat="server" ID="Label4" /></h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Fisik Jaminan</label>
                        <asp:Label ID="lblBgnFisikJaminan" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">          
                        <label> Surat Izin</label>
                        <asp:Label ID="lblBgnSuratIzin" runat="server" ></asp:Label>
                    </div>
                </div>
			    <div class="form_box">
                    <div class="form_left">
                        <label> Nomor Surat Izin</label>
                        <asp:Label ID="lblBgnNoSuratIzin" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">          
                        <label> Jenis Surat Izin</label>
                        <asp:Label ID="lblBgnJenisSuratIzin" runat="server" ></asp:Label>
                    </div>
                </div>                
				<div class="form_box">
                    <div class="form_left">
                        <label> Peruntukan Bangunan</label>
                        <asp:Label ID="lblBgnPeruntukanBangunan" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">          
                        <label> Nomor Izin Layak Huni</label>
                        <asp:Label ID="lblBgnNoIzinLayakHuni" runat="server" ></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> PBB Tahun Terakhir</label>
                        <asp:Label ID="lblBgnPBBTahunTerakhir" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">          
                        <label> Nilai NJOP</label>
                        <asp:Label ID="lblBgnNilaiNJOP" runat="server" ></asp:Label>
                    </div>
                </div>
			    <div class="form_box">
                    <div class="form_left">
                        <label> Cetak Biru</label>
                        <asp:Label ID="lblBgnCetakBiru" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">          
                        <label> Akta Balik Nama</label>
                        <asp:Label ID="lblBgnAktaBalikNama" runat="server" ></asp:Label>
                    </div>
                </div>                
				<div class="form_box">
                    <div class="form_left">
                        <label> Nomor Akta</label>
                        <asp:Label ID="lblBgnNoAkta" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">          
                        <label> Tanggal Akta</label>
                        <asp:Label ID="lblBgnTglAkta" runat="server" ></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Notaris</label>
                        <asp:Label ID="lblBgnNotaris" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">          
                        <label> Paripasu (%)</label>
                        <asp:Label ID="BgnParipasu" runat="server" ></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Bangunan diasuransikan</label>
                        <asp:Label ID="BgnBangunanDiasuransikan" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">          
                       
                    </div>
                </div>

                <div class="tab_container_form_space"><br /><br /><br /><br /></div>
                <div class="tab_container_button">
                    <div class="form_button">
                        <asp:Button ID="btnClose" OnClientClick="javascript:window.close();" runat="server"
                            Text="Close" CssClass="small button gray" CausesValidation="False"></asp:Button>
                    </div>
                </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>