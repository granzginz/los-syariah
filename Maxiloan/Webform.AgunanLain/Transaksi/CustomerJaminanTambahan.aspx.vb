﻿#Region "Imports"
Imports System.IO
Imports Maxiloan
Imports Maxiloan.Exceptions
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
#End Region

Public Class CustomerJaminanTambahan

    Inherits Maxiloan.Webform.WebBased



#Region " Private Const "
    Dim m_AgunanLain As New AgunanLainController

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Protected WithEvents GridNavigator As ucGridNav
    Dim filterBranch As String
    Private m_controller As New CustomerController

#End Region
#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property ProspectAppID() As String
        Get
            Return CType(ViewState("ProspectAppID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ProspectAppID") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property

#End Region

#Region "Page_Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If
        AddHandler GridNavigator.PageChanged, AddressOf PageNavigation
        filterBranch = " vwCustomer.customerId like '" & Replace(Me.sesBranchId, "'", "") & "%' "


        If Not Me.IsPostBack Then

            If CheckForm(Me.Loginid, "CltrlTambahanCst", "MAXILOAN") Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If

            If IsSingleBranch() And Me.IsHoBranch = False Then
                Me.Sort = "Name ASC"

                If Request("cond") <> "" Then
                    Me.CmdWhere = Request("cond")
                Else
                    Me.CmdWhere = filterBranch
                End If

                BindGridEntity(Me.CmdWhere)

                Me.SortBy = "Name ASC"

            Else
                Dim strHTTPServer As String
                Dim StrHTTPApp As String
                Dim strNameServer As String
                strHTTPServer = Request.ServerVariables("PATH_INFO")
                strNameServer = Request.ServerVariables("SERVER_NAME")
                StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                Response.Redirect("http: //" & strNameServer & "/" & StrHTTPApp & "/error_notauthorized.aspx")
            End If
        End If
    End Sub
#End Region

    Private Sub setBranchName()
        If ViewState("branchName") Is Nothing OrElse ViewState("branchName").ToString = String.Empty Then
            Dim con As New BranchController
            Dim data As New Parameter.Branch
            With data
                .strConnection = GetConnectionString()
                .BranchId = Replace(sesBranchId, "'", "")
            End With
            data = con.BranchList(data)
            BranchName = data.BranchName
        Else
            BranchName = ViewState("branchName").ToString

        End If

    End Sub

#Region "InitialDefaultPanel"
    Private Sub InitialDefaultPanel()
        pnlList.Visible = True
        lblMessage.Visible = False
    End Sub
#End Region

#Region "BindGridEntity"
    Sub BindGridEntity(ByVal cmdWhere As String, Optional isFrNav As Boolean = False)
        Dim dtEntity As DataTable = Nothing
        Dim oCustomClass As New Parameter.Customer
        InitialDefaultPanel()


        oCustomClass.PageSize = CType(pageSize, Int16)
        oCustomClass.WhereCond = cmdWhere
        oCustomClass.SortBy = Me.Sort
        oCustomClass.CurrentPage = currentPage
        oCustomClass.strConnection = GetConnectionString()
        oCustomClass = m_controller.GetCustomer(oCustomClass)
        If Not oCustomClass Is Nothing Then
            dtEntity = oCustomClass.listdata
            recordCount = CType(oCustomClass.totalrecords, Integer)
        Else
            recordCount = 0
        End If
        dtgCust.DataSource = dtEntity.DefaultView
        dtgCust.CurrentPageIndex = 0
        dtgCust.DataBind()

        If (isFrNav = False) Then
            GridNavigator.Initialize(recordCount, pageSize)
        End If
    End Sub

    Private Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        currentPage = e.CurrentPage
        BindGridEntity(Me.CmdWhere, True)
        GridNavigator.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
    End Sub
#End Region

#Region "ItemCommand"
    Private Sub dtgEntity_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgCust.ItemCommand
        If e.CommandName = "ListJaminanTambahan" Then
            Response.Redirect("CustomerJaminanTambahanList.aspx?id=" & e.Item.Cells(4).Text.Trim & "")
        End If
    End Sub
#End Region
#Region "Sorting"
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgCust.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub
#End Region

    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgCust.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim lnkCustomer As LinkButton
            Dim lblCustomer As Label
            Dim imgEditPersonal As ImageButton
            lnkCustomer = CType(e.Item.FindControl("lnkCustomer"), LinkButton)
            lblCustomer = CType(e.Item.FindControl("lblCust"), Label)
            lnkCustomer.Attributes.Add("OnClick", "return OpenCust('" & lblCustomer.Text & "','accacq');")
            imgEditPersonal = CType(e.Item.FindControl("imgEditPersonal"), ImageButton)

            If e.Item.Cells(3).Text.ToUpper.Trim = "C" Then
                imgEditPersonal.Visible = False
                Response.Redirect("Customer_002.aspx?pc=p&branchID=" & Me.BranchID & "&prospectappid=" & Me.ProspectAppID)
            End If

        End If
    End Sub

#Region "Search"
    Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSearchNew.Click

        Me.CmdWhere = filterBranch

        If Not (TxtSearchByValue.Text.Trim = String.Empty) Then
			Me.CmdWhere = String.Format("{0} and vwCustomer.{1} LIKE '%{2}%' ", Me.CmdWhere, cboSearchBy.SelectedItem.Value, Replace(TxtSearchByValue.Text.Trim, "'", "''"))
		End If

        BindGridEntity(Me.CmdWhere)
    End Sub
#End Region

#Region "Reset"
    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnResetNew.Click
        cboSearchBy.SelectedIndex = 0
        TxtSearchByValue.Text = ""
        Me.CmdWhere = filterBranch ' "ALL"
        BindGridEntity(Me.CmdWhere)
    End Sub
#End Region
End Class