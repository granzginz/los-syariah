﻿Public MustInherit Class CustomerTambahanJaminanTabsBase
    Inherits System.Web.UI.UserControl
    Protected _tabs As Dictionary(Of String, CustomerJaminanTambahanTab)

    Public Sub EnabledLink(link As String, enable As Boolean)
        _tabs(link).Link.Enabled = enable
    End Sub

    Protected MustOverride Sub initTabs()

	'Public Overridable Sub SetNavigateUrl(page As String, id As String)
	'	If Object.ReferenceEquals(_tabs, Nothing) Then
	'		initTabs()
	'	End If
	'End Sub

	Public Overridable Sub SetNavigateUrl(page As String, CollateralId As String, CustomerId As String)
		If Object.ReferenceEquals(_tabs, Nothing) Then
			initTabs()
		End If
	End Sub


	Public Overridable Sub RefreshAttr(pnl As String)
        If Object.ReferenceEquals(_tabs, Nothing) Then
            initTabs()
        End If

        pnl = CStr(IIf(String.IsNullOrEmpty(pnl), "", pnl))
        For Each t In _tabs
            Dim r = t.Value.Tab
            r.Attributes.Remove("class")
            r.Attributes.Add("class", "tab_notselected")
        Next

        If pnl.Trim <> "" Then
            _tabs(pnl).Tab.Attributes.Remove("class")
            _tabs(pnl).Tab.Attributes.Add("class", "tab_selected")
        End If
    End Sub
End Class
