﻿#Region "Imports"
Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.General
Imports Maxiloan.General.CommonCookiesHelper
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Imports Maxiloan.Webform.UserController

#End Region
Public Class ViewCollateralBPKB
	Inherits Maxiloan.Webform.WebBased
#Region "Property"
	Public Property Style() As String
		Get
			Return CType(ViewState("Style"), String)
		End Get
		Set(ByVal Value As String)
			ViewState("Style") = Value
		End Set
	End Property

	Private Property CustomerID() As String
		Get
			Return CType(ViewState("CustomerID"), String)
		End Get
		Set(ByVal Value As String)
			ViewState("CustomerID") = Value
		End Set
	End Property

	Private Property CollateralID() As String
		Get
			Return CType(ViewState("CollateralID"), String)
		End Get
		Set(ByVal Value As String)
			ViewState("CollateralID") = Value
		End Set
	End Property
	Dim c_AgunanLain As New AgunanLainController
	Protected WithEvents ucLandTab1 As ucAgunanLainInvoiceViewTab
#End Region
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		lblMessage.Visible = False
		Page.Header.DataBind()
		If SessionInvalid() Then
			Exit Sub
		End If
		If Not IsPostBack Then
			Me.Style = Request.QueryString("style")

			If Request.QueryString("CollateralID").Trim <> "" Then Me.CollateralID = Request.QueryString("CollateralID")
			If Request.QueryString("CustomerID").Trim <> "" Then Me.CustomerID = Request.QueryString("CustomerID")

			Dim oAgunanLain As New Parameter.AgunanLain

			With oAgunanLain
				.strConnection = GetConnectionString()
				.CollateralID = Me.CollateralID
				.CustomerID = Me.CustomerID
			End With

			ucLandTab1.CollateralID = Me.CollateralID
			ucLandTab1.CustomerID = Me.CustomerID
			ucLandTab1.setLink()
			ucLandTab1.selectedTab("BPKB")
			DoBind()

			oAgunanLain = c_AgunanLain.getDataBPKB(oAgunanLain)
			With oAgunanLain

				cboJenisAgunan.SelectedValue = .CollateralTypeID
				lblJenisAgunan.Text = cboJenisAgunan.SelectedItem.Text.ToString()
				cboStatusAgunan.SelectedValue = .CollateralStatus
				lblStatusAgunan.Text = cboStatusAgunan.SelectedItem.Text.ToString()

				lblIDAgunan.Text = .CollateralID
				lblCurrency.Text = .Currency
				lblAgunanSeqNo.Text = .CollateralSeqNo
				lblPemilikAgunan.Text = .PemilikCollateral
				lblNamaBpkb.Text = .NamaDiBPKB
				lblNoBpkb.Text = .NoBPKB
				'lblTglBpkb.Text = .TglBPKB
				lblNoPolisi.Text = .NoPolisi
				lblNoRangka.Text = .NoRangka
				lblNoMesin.Text = .NoMesin
				lblMerk.Text = .Merk
				lblTipe.Text = .Tipe
				lblWarna.Text = .Warna
				'lblTglJtTempo.Text = .JTBPKB
				'lblTglterima.Text = .TglTerimaBPKB
				lblNoFaktur.Text = .NoFaktur
				lblNik.Text = .NoNIK
				lblNoFormA.Text = .NoFormA
				lblAktaFidusia.Text = .AktaFidusia
				lblSertifikatFidusia.Text = .SertifikatFidusia
				'lblTglKeluarBpkb.Text = .TglKeluarBPKBSementara
				lblKeterangan.Text = .Keterangan
				'lblTglClose.Text = .TglClose
				lblNamaDealer.Text = .NamaDealer
				lblAlamatDealer.Text = .AlamatDealer
				lblTeleponDealer.Text = .TelponDealer

				If .JTBPKB = ConvertDate2(" 01/01/0001") Then
					lblTglJtTempo.Text = ""
				Else
					lblTglJtTempo.Text = .JTBPKB.ToString("dd/MM/yyyy")
				End If

				If .TglTerimaBPKB = ConvertDate2(" 01/01/0001") Then
					lblTglterima.Text = ""
				Else
					lblTglterima.Text = .TglTerimaBPKB.ToString("dd/MM/yyyy")
				End If

				If .TglBPKB = ConvertDate2(" 01/01/0001") Then
					lblTglBpkb.Text = ""
				Else
					lblTglBpkb.Text = .TglBPKB.ToString("dd/MM/yyyy")
				End If

				If .TglKeluarBPKBSementara = ConvertDate2(" 01/01/0001") Then
					lblTglKeluarBpkb.Text = ""
				Else
					lblTglKeluarBpkb.Text = .TglKeluarBPKBSementara.ToString("dd/MM/yyyy")
				End If

				If .TglClose = ConvertDate2(" 01/01/0001") Then
					lblTglClose.Text = ""
				Else
					lblTglClose.Text = .TglClose.ToString("dd/MM/yyyy")
				End If

			End With
		End If
	End Sub
	Sub DoBind()
		FillCombo()
		FillCbo(cboJenisAgunan, "TblCollateralType")
	End Sub
	Private Sub FillCombo()
		Dim strConn As String
		Dim dtCombo As New DataTable

		strConn = GetConnectionString()
	End Sub

	Sub FillCbo(ByVal cboName As DropDownList, ByVal Initial As String)
		Dim oAssetData As New Parameter.AgunanLain
		Dim oData As New DataTable
		oAssetData.strConnection = GetConnectionString()
		oAssetData.Initial = Initial
		oAssetData = c_AgunanLain.GetCboCollateralType(oAssetData)
		oData = oAssetData.ListData
		cboName.DataSource = oData
		cboName.DataTextField = "ID"
		cboName.DataValueField = "CollateralTypeID"
		cboName.DataBind()
		cboName.Items.Insert(0, "Select One")
		cboName.Items(0).Value = "0"
	End Sub
End Class