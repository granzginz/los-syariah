﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="JenisJaminanTambahan.aspx.vb" Inherits="Maxiloan.Webform.AgunanLain.JenisJaminanTambahan" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Company</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../js/jquery-1.9.1.min.js" type="text/javascript"></script>	
    <script src="../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
	<script src="../../js/jquery-2.1.1.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function windowClose() {
            window.close();
        }

        function fConfirm() {
            if (window.confirm("Apakah yakin mau hapus data ini ?"))
                return true;
            else
                return false;
        }

        function fback() {
            history.go(-1);
            return false;
        }

        function OpenWindowCompany(pCompanyID) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            //function window open error salah penulisan url
            //window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.Setup/Organization/CompanyView.aspx?CompanyID=' + pCompanyID, 'CompanyView', 'left=15, top=10, width=985, height=480, menubar=0, scrollbars=yes');
            //
            var x = screen.width; var y = screen.height - 100;
            window.open('CompanyView.aspx?CompanyID=' + pCompanyID, 'CompanyView', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
            <asp:Panel ID="pnlList" runat="server">
                <div class="form_title">
                    <div class="title_strip">
                    </div>
                    <div class="form_single">
                        <h3>
                            DAFTAR JENIS COLLATERAL
                        </h3>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtg" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                OnSortCommand="Sorting" BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="EDIT">
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ImbEdit" runat="server" ImageUrl="../../Images/iconedit.gif"
                                                CommandName="Edit" CausesValidation="False"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="DELETE" Visible="false">
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ImbDelete" runat="server" ImageUrl="../../Images/icondelete.gif"
                                                CommandName="Delete" CausesValidation="False" Visible="false"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn Visible="False" DataField="CollateralTypeID"></asp:BoundColumn>
                                    <asp:TemplateColumn SortExpression="CollateralTypeID" HeaderText="ID">
                                        <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lnkCollateralTypeID" runat="server" Enabled="True">
												<%# Container.DataItem("CollateralTypeID") %>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn Visible="False" DataField="Description"></asp:BoundColumn>
                                    <asp:TemplateColumn SortExpression="Description" HeaderText="JENIS COLLATERAL">
                                        <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        <ItemTemplate>
                                                <asp:Label ID="Description" runat="server" Enabled="True">
												<%# Container.DataItem("Description") %>
                                            </asp:Label>
                                      <%--  <asp:LinkButton ID="HyDescription" runat="server" Enabled="True" CausesValidation="False">
												<%# Container.DataItem("Description") %>
                                            </asp:LinkButton>--%>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="Initial" SortExpression="Initial" HeaderText="INITIAL">
                                        <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="Status" SortExpression="Status" HeaderText="Status">
                                        <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:BoundColumn>
                                </Columns>
                            </asp:DataGrid>
                        </div>
                        <div class="button_gridnavigation">
                            <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton01.png"
                                OnCommand="NavigationLink_Click" CommandName="First"></asp:ImageButton>
                            <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton02.png"
                                OnCommand="NavigationLink_Click" CommandName="Prev"></asp:ImageButton>
                            <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton03.png"
                                OnCommand="NavigationLink_Click" CommandName="Next"></asp:ImageButton>
                            <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton04.png"
                                OnCommand="NavigationLink_Click" CommandName="Last"></asp:ImageButton>
                            Page&nbsp;
                            <asp:TextBox ID="txtGoPage" runat="server" Width="34px" >1</asp:TextBox>
                            <asp:Button ID="btnGoPage" runat="server" Text="Go" CssClass="small buttongo blue" />
                            <asp:RangeValidator ID="rgvGo" runat="server" Type="Integer" MaximumValue="999999999"
                                ErrorMessage="No Halaman Salah" MinimumValue="1" ControlToValidate="txtGopage" CssClass="validator_general" ></asp:RangeValidator>
                            <asp:RequiredFieldValidator ID="rfvGo" runat="server" ErrorMessage="No Halaman Salah"
                                ControlToValidate="txtGopage" Display="Dynamic" CssClass="validator_general" ></asp:RequiredFieldValidator>
                        </div>
                        <div class="label_gridnavigation">
                            <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                            <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                            <asp:Label ID="lblTotRec" runat="server"></asp:Label>&nbsp;record(s)
                        </div>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="BtnAddNew" runat="server" CausesValidation="False" Text="Add" CssClass="small button green">
                    </asp:Button>
                    <asp:Button ID="btnPrint" runat="server" Enabled="true" Text="Print" CssClass="small button blue" Visible="false">
                    </asp:Button>
                </div>
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            CARI JENIS COLLATERAL
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Cari berdasarkan</label>
                        <asp:DropDownList ID="cboSearchBy" runat="server" Width="144px" Font-Names="Verdana">
                            <asp:ListItem Value="CollateralTypeID" Selected="True">ID</asp:ListItem>
                            <asp:ListItem Value="Description">JENIS COLLATERAL</asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="TxtSearchByValue" runat="server" Width="168px" ></asp:TextBox>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="BtnSearchNew" runat="server" CausesValidation="False" Text="Find"
                        CssClass="small button blue"></asp:Button>
                    <asp:Button ID="BtnResetNew" runat="server" CausesValidation="False" Text="Reset"
                        CssClass="small button gray"></asp:Button>
                </div>

            </asp:Panel>
            <asp:Panel ID="pnlAddEdit" runat="server">
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            JENIS COLLATERAL -&nbsp;
                            <asp:Label ID="lblMenuAddEdit" runat="server" Enabled="True"></asp:Label>
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class ="label_req">
                            ID Jenis Collateral</label>
                        <asp:TextBox ID="txtID" runat="server" Width="48px"  MaxLength="3"></asp:TextBox>
                        <asp:Label ID="lblID" runat="server" Width="100px" Visible="False"></asp:Label>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Width="184px"
                            Display="Dynamic" ErrorMessage="Harap isi ID Jenis COllateral" ControlToValidate="txtID" CssClass="validator_general" ></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class ="label_req">
                            Description</label>
                        <asp:TextBox ID="txtDescription" runat="server" Width="300px"  MaxLength="60"></asp:TextBox>
                        <asp:Label ID="Label1" runat="server" Width="129px" Visible="False"></asp:Label>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Width="184px"
                            Display="Dynamic" ErrorMessage="Harap isi Description" ControlToValidate="txtID" CssClass="validator_general" ></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class ="label_req">
                            Initial</label>
                        <asp:TextBox ID="txtInitial" runat="server" Width="80px"  MaxLength="10"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfv1" runat="server" Width="184px" Display="Dynamic"
                            ErrorMessage="Harap isi Initial" ControlToValidate="txtInitial" CssClass="validator_general" ></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Aktif</label>
                        <asp:CheckBox ID="chkisActive" runat="server" Enabled="True"></asp:CheckBox>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="BtnSave" runat="server" Text="Save" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="BtnCancel" runat="server" OnClientClick="windowClose();" CausesValidation="False"
                        Text="Cancel" CssClass="small button gray"></asp:Button>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
