﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="am_main.aspx.vb" Inherits="Maxiloan.Webform.am_main" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Maxiloan</title>
    <link rel="icon" type="image/ico" href="favicon.ico" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <!-- Global stylesheets -->
    <link href="lib/limitless/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css" />
    <link href="lib/limitless/assets/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="lib/limitless/assets/css/core.css" rel="stylesheet" type="text/css" />
    <link href="lib/limitless/assets/css/components.css" rel="stylesheet" type="text/css" />
    <link href="lib/limitless/assets/css/colors.css" rel="stylesheet" type="text/css" />
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <%--<script type="text/javascript" src="lib/limitless/assets/js/plugins/loaders/pace.min.js"></script>--%>
    <script type="text/javascript" src="lib/limitless/assets/js/core/libraries/jquery.min.js"></script>
    <script type="text/javascript" src="lib/limitless/assets/js/core/libraries/bootstrap.min.js"></script>
    <script type="text/javascript" src="lib/limitless/assets/js/plugins/loaders/blockui.min.js"></script>
    <script type="text/javascript" src="lib/limitless/assets/js/core/app.js"></script>

    <script type="text/javascript" src="lib/limitless/assets/js/plugins/notifications/pnotify.min.js"></script>
    <script type="text/javascript" src="lib/limitless/assets/js/pages/components_notifications_pnotify.js"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->



    <%--<link rel="Stylesheet" href="../../Include/General.css" type="text/css" />--%>
    <link href="Include/Main.css" type="text/css" rel="stylesheet" />
    <%--<link href="Include/dhtmlxmenu_dhx_skeleton.css" type="text/css" rel="stylesheet" />--%>
    <link href="Include/dhtmlxmenu_dhx_skyblueMod.css" type="text/css" rel="stylesheet" />
    <script src="dhtmlxcommon.js" type="text/javascript"></script>
    <script src="dhtmlxmenuMod2.js" type="text/javascript"></script>
    <script src="dhtmlxmenu_ext.js" type="text/javascript"></script>




    <script type="text/javascript">
        function setContentHeight() {
            parent.document.getElementById('icontent').height = document['body'].offsetHeight - parent.document.getElementById('menuObj').offsetHeight;
        }

        function cutUrlForIframe() {
            var base = document.getElementById("icontent").contentWindow.location.href;
            var cleanBase = base.replace(document.referrer, "").split("/", 7);
            var result = "";
            for (var i = 0; i < cleanBase.length - 1; i++) {
                result = result + "../";
            }
            return result;
        }

        //iframe gak bisa di inject dari depan
        //function addScriptAddOn() {
        //    //cutUrlForIframe()
        //    //link css
        //    var styles = document.createElement('link');
        //    styles.href = cutUrlForIframe() + "lib/limitless/assets/css/icons/icomoon/styles.css";
        //    styles.rel = "stylesheet"
        //    styles.type = "text/css";


        //    var bootstrap = document.createElement('link');
        //    bootstrap.href = cutUrlForIframe() + "lib/limitless/assets/css/bootstrap.min.css";
        //    bootstrap.rel = "stylesheet"
        //    bootstrap.type = "text/css";


        //    var core = document.createElement('link');
        //    core.href = cutUrlForIframe() + "lib/limitless/assets/css/core.min.css";
        //    core.rel = "stylesheet"
        //    core.type = "text/css";


        //    var components = document.createElement('link');
        //    components.href = cutUrlForIframe() + "lib/limitless/assets/css/components.min.css";
        //    components.rel = "stylesheet"
        //    components.type = "text/css";


        //    var colors = document.createElement('link');
        //    colors.href = cutUrlForIframe() + "lib/limitless/assets/css/colors.min.css";
        //    colors.rel = "stylesheet"
        //    colors.type = "text/css";


        //    //JS


        //    var pace = document.createElement('script');
        //    pace.src = cutUrlForIframe() + "lib/limitless/assets/js/plugins/loaders/pace.min.js";
        //    pace.type = "text/javascript";
        //    pace.async = true

        //    var jquery = document.createElement('script');
        //    jquery.src = cutUrlForIframe() + "lib/limitless/assets/js/core/libraries/jquery.min.js";
        //    jquery.type = "text/javascript";
        //    jquery.async = true

        //    var bootstrap = document.createElement('script');
        //    bootstrap.src = cutUrlForIframe() + "lib/limitless/assets/js/core/libraries/bootstrap.min.js";
        //    bootstrap.type = "text/javascript";
        //    bootstrap.async = true

        //    var blockui = document.createElement('script');
        //    blockui.src = cutUrlForIframe() + "lib/limitless/assets/js/plugins/loaders/blockui.min.js";
        //    blockui.type = "text/javascript";
        //    blockui.async = true

        //    var app = document.createElement('script');
        //    app.src = cutUrlForIframe() + "lib/limitless/assets/js/core/app.min.js";
        //    app.type = "text/javascript";
        //    app.async = true

        //    var pnotify = document.createElement('script');
        //    pnotify.src = cutUrlForIframe() + "lib/limitless/assets/js/plugins/notifications/pnotify.min.js";
        //    pnotify.type = "text/javascript";
        //    pnotify.async = true

        //    var components_notifications_pnotify = document.createElement('script');
        //    components_notifications_pnotify.src = cutUrlForIframe() + "lib/limitless/assets/js/pages/components_notifications_pnotify.js";
        //    components_notifications_pnotify.type = "text/javascript";
        //    components_notifications_pnotify.async = true

        //    var doc = document.getElementById('icontent').contentWindow.document.head;


        //    doc.append(styles);
        //    doc.append(bootstrap);
        //    doc.append(core);
        //    doc.append(components);
        //    doc.append(colors);

        //    //doc.append(pace);
        //    doc.append(jquery);

        //    doc.append(pnotify);
        //    doc.append(components_notifications_pnotify);

        //    doc.append(blockui);
        //    doc.append(app);
        //    doc.append(bootstrap);







        //}

    </script>
</head>
<body onload="initMenu();" class="">


    <%--<div class="row">--%>
    <div class="navbar " style="background-color: #000063; min-height: 26px;">
    </div>
    <%--</div>--%>
    <%--<div class="row">--%>
    <div class="navbar navbar-default" style="box-shadow: 0px 4px 0px 0px black; background-color: #311a91;">
        <div class="navbar-header">
            <a href="Webform.AppMgt/forms/am_inbox_004.aspx" target="content">
                <img src="Images/Login/islamic-ornament.png" alt="company_logo" style="height: 110px; width: 110px; /*background-color: #534bae; border-radius: 10px; */ position: relative; bottom: 23px; left: 7px;" />
            </a>

        </div>

        <div class="navbar-right">
            <ul class="nav navbar-nav">
            </ul>
        </div>


    </div>

    <%--jangan dihapus--%>
    <div class="page-container">
        <div class="page-content">
            <div class="content-wrapper">

                <form id="form1" runat="server">
                    <asp:ScriptManager ID="ScriptManager1" runat="server">
                    </asp:ScriptManager>

                    <div class="row" style="background-color: gold; position: relative; padding-right: 10px; padding-left: 10px;">
                        <div class="col">
                            <div id="menuObj" style="height: 1px;"></div>
                        </div>
                    </div>

                    <div class="row" style="margin-left: 0px !important;">
                        <div class="col">
                            <iframe name="content" id="icontent" src="Webform.AppMgt/Forms/am_inbox_004.aspx"
                                scrolling="yes" frameborder="0" height="100%" width="100%"></iframe>
                        </div>
                    </div>


                </form>

            </div>
        </div>
    </div>
    <%--end jangan dihapus--%>

    <div class="navbar navbar-default navbar-fixed-bottom">
        <ul class="nav navbar-nav no-border visible-xs-block">
            <li><a class="text-center collapsed" data-toggle="collapse" data-target="#navbar-second"><i class="icon-circle-up2"></i></a></li>
        </ul>

        <div class="navbar-collapse collapse" id="navbar-second">
            <div class="navbar-left">
                <ul class="nav navbar-nav">
                    <li>
                        <i class="icon-info3 navbar-text"></i>
                        <%--<img src="Images/Treemas.jfif" alt="company_logo" style="height: 27px; width: 62px;" />--%>

                    </li>
                    <li>
                        <div class="navbar-text">
                            © 2021. IDeaLoan - SYARIAH
                        </div>
                    </li>
                </ul>
            </div>


            <div class="navbar-right">
                <ul class="nav navbar-nav">
                    <li>
                        <asp:Label ID="lblDate" runat="server" CssClass="text-left navbar-text"></asp:Label>
                    </li>
                    <li><i class="icon-seven-segment-1 navbar-text"></i></li>
                    <li>
                        <span class="text-left navbar-text"><%= Session("BranchName")%></span>
                    </li>
                    <li><i class="icon-seven-segment-1 navbar-text"></i></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            <i class="icon-home"></i>
                            <span>
                                <label>&nbsp;<%= Session("FullName")%></label></span>
                            <span class="visible-xs-inline-block position-right">Settings</span>
                            <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu dropdown-menu-right">
                            <li><a href="am_login.aspx"><i class="icon-switch2"></i>Logout</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>


    </div>


    <div id="outtext" runat="server">
    </div>




</body>
</html>
