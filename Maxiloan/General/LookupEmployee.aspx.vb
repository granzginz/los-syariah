﻿Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller

Public Class LookupEmployee
    Inherits WebBased


#Region "Constanta"
    Private oController As New umUserController
    Private oCustomClass As New Maxiloan.Parameter.am_user001
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Put user code to initialize the page here
        If SessionInvalid() Then
            Exit Sub
        End If


        If Not IsPostBack Then
            Me.SearchBy = " BranchID = '" + Request.QueryString("branchid") + "' "
            Me.SortBy = ""
            InitialDefaultPanel()
            doBind(Me.SearchBy, Me.SortBy)
        End If

        btnSelect.Attributes.Add("onclick", "Select_Click()")
        btnCancel.Attributes.Add("onclick", "Close_Window()")
    End Sub

    Private Sub InitialDefaultPanel()
        pnlList.Visible = True
        pnlDtGrid.Visible = False
        lblMessage.Text = ""
    End Sub

    Sub doBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        With oCustomClass
            .strConnection = GetConnectionString
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With
        oCustomClass = oController.ListEmployee(oCustomClass)
        DtUserList = oCustomClass.ListEmployee
        pnlDtGrid.Visible = True
        DvUserList = DtUserList.DefaultView
        recordCount = oCustomClass.TotalRecord
        DvUserList.Sort = Me.SortBy
        dtgEmployee.DataSource = DvUserList
        Try
            dtgEmployee.DataBind()
        Catch
            dtgEmployee.CurrentPageIndex = 0
            dtgEmployee.DataBind()
        End Try
        PagingFooter()
    End Sub

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            lblMessage.Text = "Data Not Found ....."
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblRecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        doBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGoPage.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                doBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub
    Protected Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dtgEmployee.SortCommand
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If

        doBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

    Public Function Get_Radio(ByVal pStrId As String, ByVal pStrValue As String) As String
        Return "<input type=radio name=rbtAsset onclick=""javascript:Employee_Checked('" & Trim(pStrId) & "','" & Trim(pStrValue) & "')"" value='" & pStrValue & "'>"
    End Function

    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsearch.Click
        Me.SearchBy = " BranchID = '" + Request.QueryString("branchid") + "' "

        If txtEmployeeID_src.Text <> "" Then
            Me.SearchBy = Me.SearchBy & " And employeeid = '" & txtEmployeeID_src.Text.Trim.Replace("'", "''") & "'"
        End If
        If txtEmployeeName_src.Text.Trim <> "" Then
            If Me.SearchBy = "" Then
                Me.SearchBy = " employeename = '" & txtEmployeeName_src.Text.Trim.Replace("'", "''") & "'"
            Else
                Me.SearchBy = Me.SearchBy & " and employeename = '" & txtEmployeeName_src.Text.Trim.Replace("'", "''") & "'"
            End If
        End If
        pnlDtGrid.Visible = True
        doBind(Me.SearchBy, Me.SortBy)
    End Sub

End Class