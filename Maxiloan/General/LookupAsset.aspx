﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="LookupAsset.aspx.vb" Inherits="Maxiloan.Webform.LookupAsset" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Lookup Asset</title>
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function Close_Window() {
            window.close();
        }

        function Asset_Checked(pAssetCode, pAssetName) {
            with (document.forms['form1']) {
                hdnAssetCode.value = pAssetCode;
                hdnAssetName.value = pAssetName;
            }
        }

        function Select_Click() {
            with (document.forms['form1']) {
                var lObjName = '<%= Request.QueryString("AssetCode")%>';
                if (eval('opener.document.forms[0].' + lObjName)) {
                    eval('opener.document.forms[0].' + lObjName).value = hdnAssetCode.value;
                }
                var lObjName = '<%= Request.QueryString("AssetName")%>';
                if (eval('opener.document.forms[0].' + lObjName)) {
                    eval('opener.document.forms[0].' + lObjName).value = hdnAssetName.value;
                }
            }

            window.close();
        }	
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <%--    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>--%>
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h4>
                DAFTAR ASSET</h4>
        </div>
    </div>
    <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgPaging" runat="server" CellPadding="0" OnSortCommand="SortGrid"
                    BorderStyle="None" DataKeyField="AssetCode" AutoGenerateColumns="False" AllowSorting="True"
                    AllowPaging="True" Width="100%" Visible="True" CssClass="grid_general">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="SELECT">
                            <HeaderStyle HorizontalAlign="Center" Width="5%" />
                            <ItemStyle HorizontalAlign="Center" Width="10%" />
                            <ItemTemplate>
                                <%# get_radio(Container.DataItem("AssetCode"),Container.DataItem("Description"))%>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn ReadOnly="True" DataField="AssetCode" SortExpression="AssetCode"
                            HeaderText="ASSET CODE"></asp:BoundColumn>
                        <asp:BoundColumn ReadOnly="True" DataField="Description" SortExpression="Description"
                            HeaderText="DESCRIPTION"></asp:BoundColumn>
                        <asp:BoundColumn ReadOnly="True" DataField="Karoseri" SortExpression="Karoseri" HeaderText="KAROSERI"
                            Visible="false"></asp:BoundColumn>
                    </Columns>
                    <PagerStyle Visible="False"></PagerStyle>
                </asp:DataGrid>
            </div>
            <div class="button_gridnavigation">
                <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" ImageUrl="../Images/grid_navbutton01.png"
                    OnCommand="NavigationLink_Click" CommandName="First"></asp:ImageButton>
                <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" ImageUrl="../Images/grid_navbutton02.png"
                    OnCommand="NavigationLink_Click" CommandName="Prev"></asp:ImageButton>
                <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" ImageUrl="../Images/grid_navbutton03.png"
                    OnCommand="NavigationLink_Click" CommandName="Next"></asp:ImageButton>
                <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" ImageUrl="../Images/grid_navbutton04.png"
                    OnCommand="NavigationLink_Click" CommandName="Last"></asp:ImageButton>
                Page
                <asp:TextBox ID="txtGoPage" runat="server" Width="34px">1</asp:TextBox>
                <asp:Button ID="btnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                    EnableViewState="False" />
                <asp:RangeValidator ID="Rangevalidator1" runat="server" Font-Size="11px" Type="Integer"
                    MaximumValue="999999999" ErrorMessage="Page No. is not valid" MinimumValue="1"
                    ControlToValidate="txtGoPage"></asp:RangeValidator>
                <asp:RequiredFieldValidator ID="rfvGo" runat="server" ErrorMessage="Page No. is not valid"
                    ControlToValidate="txtGoPage" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtGoPage" MinimumValue="1"
                    MaximumValue="999" ErrorMessage="Page No. is not valid" Type="Double"></asp:RangeValidator>
            </div>
            <div class="label_gridnavigation">
                <asp:Label ID="lblPage" runat="server"></asp:Label>of
                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                <asp:Label ID="lblTotRec" runat="server"></asp:Label>record(s)
            </div>
        </div>
    </div>
    <div class="form_button">
        <asp:Button runat="server" ID="buttonSelect" Text="Select" CssClass="small button blue" />
        <asp:Button runat="server" ID="buttonCancel" Text="Cancel" CssClass="small button gray"
            CausesValidation="false" />
        <a href="javascript:window.close();"></a>
    </div>
    <div class="form_box_title">
        <div class="form_single">
            <h4>
                CARI ASSET</h4>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Cari Berdasarkan
            </label>
            <asp:DropDownList ID="cboSearchBy" runat="server">
                <asp:ListItem Value="AssetCode">Asset Code</asp:ListItem>
                <asp:ListItem Value="Description" Selected="True">Description</asp:ListItem>
            </asp:DropDownList>
            <asp:TextBox ID="txtSearchBy" runat="server"></asp:TextBox>
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="btnSearch" runat="server" CausesValidation="False" Text="Find" CssClass="small button blue" />
        <asp:Button ID="btnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray" />
    </div>
    <input type="hidden" id="hdnAssetCode" name="hdnAssetCode" runat="server" />
    <input type="hidden" id="hdnAssetName" name="hdnAssetName" runat="server" />
    <%--</ContentTemplate> </asp:UpdatePanel>--%>
    </form>
</body>
</html>
