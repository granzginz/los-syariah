﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="LookupZipCode.aspx.vb"
    Inherits="Maxiloan.Webform.LookUpZipCode" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function Close_Window() {
            window.close();
        }
        function ZipCode_Checked(pZipCode, pKelurahan, pKecamatan, pCity) {
            with (document.forms['form1']) {
                hdnZipCode.value = pZipCode;
                hdnKelurahan.value = pKelurahan;
                hdnKecamatan.value = pKecamatan;
                hdnCity.value = pCity;
            }
        }
        function Select_Click() {
            valid = 0;
            //untuk menjaga bila radio bukan berupa array of radiobutton
            if (document.forms[0].rbtZipCode.length == undefined) {
                valid = 1;
            }
            for (var i = 0; i < document.forms[0].rbtZipCode.length; i++) {
                if (document.forms[0].rbtZipCode[i].checked == "1") {
                    valid = 1;
                }
            }
            if (valid == 1) {
                with (document.forms[0]) {
                    var lObjName = '<%= Request.QueryString("ZipCode")%>';
                    if (eval('opener.document.forms[0].' + lObjName)) {
                        eval('opener.document.forms[0].' + lObjName).value = hdnZipCode.value;
                    }
                    var lObjName = '<%= Request.QueryString("Kelurahan")%>';
                    if (eval('opener.document.forms[0].' + lObjName)) {
                        eval('opener.document.forms[0].' + lObjName).value = hdnKelurahan.value;
                    }
                    var lObjName = '<%= Request.QueryString("Kecamatan")%>';
                    if (eval('opener.document.forms[0].' + lObjName)) {
                        eval('opener.document.forms[0].' + lObjName).value = hdnKecamatan.value;
                    }
                    var lObjName = '<%= Request.QueryString("City")%>';
                    if (eval('opener.document.forms[0].' + lObjName)) {
                        eval('opener.document.forms[0].' + lObjName).value = hdnCity.value;
                    }
                };
                window.close();
            } else {
                alert("Please Choose Zip Code !! ");
                return false;
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
        onclick="hideMessage();"></asp:Label>
    <div class="form_box_title">
        <div class="form_single">
            <h5>
                DAFTAR KODE POS</h5>
        </div>
    </div>
    <asp:Panel ID="pnlGrid" runat="server">
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_general_ws">
                    <asp:DataGrid ID="dtgPaging" runat="server" EnableViewState="False" AutoGenerateColumns="False"
                        BorderStyle="None" CellPadding="0" CssClass="grid_general">
                        <ItemStyle CssClass="item_grid"></ItemStyle>
                        <HeaderStyle CssClass="th"></HeaderStyle>
                        <Columns>
                            <asp:TemplateColumn HeaderText="Select">
                                <ItemStyle HorizontalAlign="Center" Width="5%"></ItemStyle>
                                <HeaderTemplate>
                                    SELECT
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <%# Get_Radio(Container.DataItem("ZIPCODE"),Container.DataItem("KELURAHAN"),Container.DataItem("KECAMATAN"),Container.DataItem("CITY"))%>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="KELURAHAN" HeaderText="KELURAHAN"></asp:BoundColumn>
                            <asp:BoundColumn DataField="KECAMATAN" HeaderText="KECAMATAN"></asp:BoundColumn>
                            <asp:BoundColumn DataField="CITY" HeaderText="CITY"></asp:BoundColumn>
                            <asp:BoundColumn DataField="ZIPCODE" HeaderText="ZIP CODE"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
                <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../Images/grid_navbutton01.png"
                        CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../Images/grid_navbutton02.png"
                        CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../Images/grid_navbutton03.png"
                        CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../Images/grid_navbutton04.png"
                        CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                    </asp:ImageButton>
                    Page
                    <asp:TextBox ID="txtGoPage" runat="server" Width="34px">1</asp:TextBox>
                    <asp:Button ID="btnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                        EnableViewState="False" />
                    </asp:Button>
                    <asp:RangeValidator ID="rgvGo" runat="server" Type="Integer" MaximumValue="999999999"
                        ErrorMessage="Page No. is not valid" MinimumValue="1" ControlToValidate="txtGoPage"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo" runat="server" Display="Dynamic" ErrorMessage="Page No. is not valid"
                        ControlToValidate="txtGoPage"></asp:RequiredFieldValidator>
                </div>
                <div class="label_gridnavigation">
                    <asp:Label ID="lblPage" runat="server"></asp:Label>of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                    <asp:Label ID="lblTotRec" runat="server"></asp:Label>record(s)
                </div>
            </div>
        </div>
        <div class="form_button">
            <%-- <asp:ImageButton ID="ImbSelect" runat="server" CausesValidation="False" ImageUrl="../Images/ButtonSelect.gif">
            </asp:ImageButton>
            <asp:ImageButton ID="imbCancel" runat="server" CausesValidation="False" ImageUrl="../Images/ButtonCancel.gif">
            </asp:ImageButton><a href="javascript:window.close();"></a>--%>
            <asp:Button ID="buttonSelect" runat="server" Text="Select" CssClass="small button blue"/>
            <asp:Button ID="buttonCancel" runat="server" Text="Cancel" CssClass="small button gray"
                CausesValidation="false" />
            <a href="javascript:window.close();"></a>
        </div>
    </asp:Panel>
    <div class="form_box_title">
        <div class="form_single">
            <h5>
                CARI KODE POS
            </h5>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Kota</label>
            <asp:DropDownList ID="CboCity" runat="server">
            </asp:DropDownList>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Please Choose City"
                Display="Dynamic" ControlToValidate="CboCity" Enabled="False" InitialValue="0"></asp:RequiredFieldValidator>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Find by</label>
            <asp:DropDownList ID="cboSearchBy" runat="server">
                <asp:ListItem Value=""></asp:ListItem>
                <asp:ListItem Value="Kelurahan">Kelurahan</asp:ListItem>
                <asp:ListItem Value="Kecamatan">Kecamatan</asp:ListItem>
                <asp:ListItem Value="ZipCode">Zip Code</asp:ListItem>
            </asp:DropDownList>
            <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
        </div>
    </div>
    <div class="form_button">
        <%--<asp:ImageButton ID="btnSearch" runat="server" ImageUrl="../Images/ButtonSearch.gif">
        </asp:ImageButton>
        <asp:ImageButton ID="imbReset" runat="server" ImageUrl="../Images/ButtonReset.gif">
        </asp:ImageButton>--%>
        <asp:Button ID="buttonSearch" runat="server" Text="Find" CssClass="small button blue"
            CausesValidation="false" />
        <asp:Button ID="buttonReset" runat="server" Text="Reset" CssClass="small button gray"
            CausesValidation="false" />
    </div>
    <input id="hdnZipCode" type="hidden" name="hdnZipCode" runat="server" />
    <input id="hdnKelurahan" type="hidden" name="hdnKelurahan" runat="server" />
    <input id="hdnCity" type="hidden" name="hdnCity" runat="server" />
    <input id="hdnKecamatan" type="hidden" name="hdnKecamatan" runat="server" />
    </form>
</body>
</html>
