﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
#End Region

Public Class LookupCustomer
    Inherits WebBased

    Private m_controller As New LookUpCustomerController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Decimal = 1

    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property

    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        lblMessage.Text = ""
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not Me.IsPostBack Then
            Me.Sort = "Name ASC"
            Dim strstyle As String = Request.QueryString("style")

            Me.CmdWhere = "ALL"
            BindGridEntity(Me.CmdWhere)
        End If

        ImbSelect.Attributes.Add("onclick", "Select_Click()")
        imbExit.Attributes.Add("onclick", "Close_Window()")
    End Sub

    Sub BindGridEntity(ByVal cmdWhere As String)
        Dim dttEntity As DataTable
        Dim oCustomClass As New Maxiloan.Parameter.LookUpCustomer

        oCustomClass.PageSize = pageSize
        oCustomClass.WhereCond = cmdWhere
        oCustomClass.CurrentPage = currentPage
        oCustomClass.SortBy = Me.Sort
        oCustomClass.strConnection = GetConnectionString()

        oCustomClass = m_controller.GetListData(oCustomClass)

        If Not oCustomClass Is Nothing Then
            dttEntity = oCustomClass.ListData
            recordCount = oCustomClass.TotalRecords
        Else
            recordCount = 0
        End If

        dtgCustomer.DataSource = dttEntity.DefaultView
        dtgCustomer.CurrentPageIndex = 0
        dtgCustomer.DataBind()
        PagingFooter()
    End Sub

#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            lblMessage.Text = "Data not found ....."
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblTotRec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If

    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbGoPage.Click
        If IsNumeric(txtGoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtGoPage.Text, Int32)
                BindGridEntity(Me.CmdWhere)
            End If
        End If
    End Sub
#End Region

#Region "dtgPaging_ItemDataBound"
    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgCustomer.ItemDataBound
        Dim imgBadType As System.Web.UI.WebControls.Image
        Dim lblBadType As Label
        Dim BadType As String
        If e.Item.ItemIndex >= 0 Then
            lblBadType = CType(e.Item.FindControl("lblBadType"), Label)
            BadType = lblBadType.Text
            imgBadType = CType(e.Item.FindControl("imgBadType"), System.Web.UI.WebControls.Image)
            If BadType = "B" Then
                imgBadType.ImageUrl = "../images/red.gif"
            ElseIf BadType = "W" Then
                imgBadType.ImageUrl = "../images/yellow.gif"
            Else
                imgBadType.ImageUrl = "../images/green.gif"
            End If
        End If
    End Sub
#End Region

    Protected Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.Sort = e.SortExpression + " ASC"
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub

    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbReset.Click
        cboSearchBy.SelectedIndex = 0
        txtSearchBy.Text = ""
        txtGoPage.Text = ""
        Me.CmdWhere = "ALL"
        BindGridEntity(Me.CmdWhere)
    End Sub

    Public Function Get_Radio(ByVal pStrCustomerID As String, ByVal pStrCustomerName As String) As String
        Return "<input type=radio name=rbtCustomer onclick=""javascript:Customer_Checked('" & Trim(pStrCustomerID) & "','" & Trim(pStrCustomerName) & "')"" value='" & pStrCustomerName & "'>"
    End Function

    Private Sub imbSearch_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbSearch.Click
        Dim strSearchBy As String = txtSearchBy.Text.Trim

        If strSearchBy <> "" Then
            strSearchBy = Replace(strSearchBy, "'", "''")
        End If

        If cboSearchBy.SelectedItem.Value = "CustomerName" Then
            Me.CmdWhere = "Name = '" & strSearchBy & "'"
        ElseIf cboSearchBy.SelectedItem.Value = "CustomerAddress" Then
            Me.CmdWhere = "Address = '" & strSearchBy & "'"
        End If

        BindGridEntity(Me.CmdWhere)
    End Sub

End Class