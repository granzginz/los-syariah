﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="LookupBankBranch.aspx.vb"
    Inherits="Maxiloan.Webform.LookupBankBranch" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript">
        function Close_Window() {
            window.close();
        }

        function lookup_checked(pBankBranchId, pBankCode, pCity, pBankBranchName) {
            with (document.forms['form1']) {
                hdnBankBranchId.value = pBankBranchId;
                hdnBankCode.value = pBankCode;
                hdnCity.value = pCity;
                hdnBankBranchName.value = pBankBranchName;
            }
        }

        function Select_Click() {
            valid = 0;
            //untuk menjaga bila radio bukan berupa array of radiobutton
            if (document.forms[0].rbtZipCode.length == undefined) {
                valid = 1;
            }
            for (var i = 0; i < document.forms[0].rbtZipCode.length; i++) {
                if (document.forms[0].rbtZipCode[i].checked == "1") {
                    valid = 1;
                }
            }
            if (valid == 1) {
                with (document.forms[0]) {
                    var lObjName = '<%= Request.QueryString("BankBranchId")%>';
                    if (eval('opener.document.forms[0].' + lObjName)) {
                        eval('opener.document.forms[0].' + lObjName).value = hdnBankBranchId.value;
                    }
                    var lObjName = '<%= Request.QueryString("BankCode")%>';
                    if (eval('opener.document.forms[0].' + lObjName)) {
                        eval('opener.document.forms[0].' + lObjName).value = hdnBankCode.value;
                    }
                    var lObjName = '<%= Request.QueryString("City")%>';
                    if (eval('opener.document.forms[0].' + lObjName)) {
                        eval('opener.document.forms[0].' + lObjName).value = hdnCity.value;
                    }
                    var lObjName = '<%= Request.QueryString("BankBranchName")%>';
                    if (eval('opener.document.forms[0].' + lObjName)) {
                        eval('opener.document.forms[0].' + lObjName).value = hdnBankBranchName.value;
                    }
                };
                window.close();
            } else {
                alert("Please select bank branch!");
                return false;
            }
        }			
			
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <table border="0" cellspacing="0" cellpadding="0" width="95%">
        <tr>
            <td>
                <asp:Label ID="lblMessage" runat="server" ForeColor="red"></asp:Label>
            </td>
        </tr>
    </table>    
    <asp:Panel ID="pnlGrid" runat="server">
        <table cellspacing="0" cellpadding="0" width="95%" border="0">
            <tr>
                <td class="tdtopi" align="center">
                    LIST OF BANK BRANCH
                </td>
            </tr>
        </table>
        <table class="tablegrid" cellspacing="0" cellpadding="0" width="95%" border="0">
            <tr class="tdjudul" align="center">
                <td>
                    <asp:DataGrid ID="dtgPaging" runat="server" EnableViewState="False" Width="100%"
                        AutoGenerateColumns="False" BorderStyle="None" BorderColor="#CCCCCC" BackColor="White"
                        BorderWidth="1px" CellPadding="0">
                        <SelectedItemStyle CssClass="tdgenap"></SelectedItemStyle>
                        <AlternatingItemStyle CssClass="tdgenap"></AlternatingItemStyle>
                        <ItemStyle CssClass="tdganjil"></ItemStyle>
                        <HeaderStyle CssClass="tdjudul" Height="30px"></HeaderStyle>
                        <FooterStyle ForeColor="#000066" BackColor="White"></FooterStyle>
                        <Columns>
                            <asp:TemplateColumn HeaderText="Select">
                                <HeaderStyle Font-Bold="True" HorizontalAlign="Center" Width="5%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="5%"></ItemStyle>
                                <HeaderTemplate>
                                    SELECT
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <%# Get_Radio(Container.DataItem("BankBranchId"), Container.DataItem("BankCode"), Container.DataItem("City"), Container.DataItem("BankBranchName"))%>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="BankBranchID" HeaderText="Bank Branch ID" Visible="False">
                                <HeaderStyle Font-Bold="True" HorizontalAlign="Center"></HeaderStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="BankCode" HeaderText="Bank Code">
                                <HeaderStyle Font-Bold="True" HorizontalAlign="Center"></HeaderStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="BankBranchName" HeaderText="Bank Branch Name">
                                <HeaderStyle Font-Bold="True" HorizontalAlign="Center"></HeaderStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="Address" HeaderText="Address">
                                <HeaderStyle Font-Bold="True" HorizontalAlign="Center"></HeaderStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="City" HeaderText="City">
                                <HeaderStyle Font-Bold="True" HorizontalAlign="Center"></HeaderStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="BankID" HeaderText="BankID" Visible="False">
                                <HeaderStyle Font-Bold="True" HorizontalAlign="Center"></HeaderStyle>
                            </asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                </td>
            </tr>
        </table>
        <table class="nav_tbl" cellspacing="0" cellpadding="0" align="center">
            <tr>
                <td class="nav_tbl_td1">
                    <table class="nav_command_tbl" cellspacing="0" cellpadding="0">
                        <tr>
                            <td>
                                <asp:ImageButton ID="ImbSelect" runat="server" CausesValidation="False" ImageUrl="../Images/ButtonSelect.gif">
                                </asp:ImageButton>
                            </td>
                            <td>
                                <asp:ImageButton ID="imbCancel" runat="server" CausesValidation="False" ImageUrl="../Images/ButtonCancel.gif">
                                </asp:ImageButton><a href="javascript:window.close();"></a><a href="javascript:window.close();"></a>
                            </td>
                        </tr>
                    </table>
                </td>
                <td class="nav_tbl_td2">
                    <table class="nav_page_tbl" cellspacing="0" cellpadding="0">
                        <tr>
                            <td>
                                <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" ImageUrl="../Images/butkiri1.gif"
                                    CommandName="First" OnCommand="NavigationLink_Click"></asp:ImageButton><font face="Verdana"></font>
                            </td>
                            <td>
                                <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" ImageUrl="../Images/butkiri.gif"
                                    CommandName="Prev" OnCommand="NavigationLink_Click"></asp:ImageButton><font face="Verdana"></font>
                            </td>
                            <td>
                                <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" ImageUrl="../Images/butkanan.gif"
                                    CommandName="Next" OnCommand="NavigationLink_Click"></asp:ImageButton><font face="Verdana"></font>
                            </td>
                            <td>
                                <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" ImageUrl="../Images/butkanan1.gif"
                                    CommandName="Last" OnCommand="NavigationLink_Click"></asp:ImageButton><font face="Verdana"></font>
                            </td>
                            <td>
                                Page
                                <asp:TextBox ID="txtGoPage" runat="server" Width="34px" >1</asp:TextBox>
                            </td>
                            <td>
                                <asp:ImageButton ID="imbGoPage" runat="server" EnableViewState="False" ImageUrl="../Images/butgo.gif">
                                </asp:ImageButton>
                            </td>
                            <asp:RangeValidator ID="rgvGo" runat="server" Font-Size="11px" font-name="Verdana"
                                ForeColor="#993300" Type="Integer" MaximumValue="999999999" ErrorMessage="Page No. is not valid"
                                MinimumValue="1" ControlToValidate="txtGoPage"></asp:RangeValidator>
                            <asp:RequiredFieldValidator ID="rfvGo" runat="server" Font-Size="11px" font-name="Verdana"
                                ForeColor="#993300" ErrorMessage="Page No. is not valid" ControlToValidate="txtGoPage"
                                Display="Dynamic"></asp:RequiredFieldValidator></tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="nav_totpage" colspan="2">
                    <asp:Label ID="lblPage" runat="server"></asp:Label>of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                    <asp:Label ID="lblTotRec" runat="server"></asp:Label>record(s)
                </td>
            </tr>
        </table>
    </asp:Panel>
    <table border="0" cellspacing="0" cellpadding="0" width="95%">
        <tr>
            <td class="tdtopi" align="center">
                Find Bank Branch
            </td>
        </tr>
    </table>
    <table class="tablegrid" border="0" cellspacing="1" cellpadding="2" width="95%">
        <tr>
            <td class="tdgenap" width="75">
                Find By
            </td>
            <td class="tdganjil">
                <asp:DropDownList ID="cboSearchBy" runat="server">
                    <asp:ListItem Value=""></asp:ListItem>
                    <asp:ListItem Value="BankCode">Bank Code</asp:ListItem>
                    <asp:ListItem Value="BankBranchName">Bank Branch Name</asp:ListItem>
                    <asp:ListItem Value="Address">Address</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearch" runat="server" ></asp:TextBox>
            </td>
        </tr>
    </table>
    <table border="0" cellspacing="0" cellpadding="0" width="95%">
        <tr>
            <td width="50%" align="left">
                <asp:ImageButton ID="btnSearch" runat="server" ImageUrl="../Images/ButtonSearch.gif">
                </asp:ImageButton>
                <asp:ImageButton ID="imbReset" runat="server" ImageUrl="../Images/ButtonReset.gif">
                </asp:ImageButton>
            </td>
        </tr>
    </table>
    <input id="hdnBankBranchId" type="hidden" name="hdnBankBranchId" runat="server" />
    <input id="hdnBankCode" type="hidden" name="hdnBankCode" runat="server" />
    <input id="hdnBankBranchName" type="hidden" name="hdnBankBranchName" runat="server" />
    <input id="hdnCity" type="hidden" name="hdnCity" runat="server" />
    </form>
</body>
</html>
