﻿Imports Maxiloan.Controller

Public Class LookupCMO
    Inherits WebBased

    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private cContract As New GeneralPagingController
    Private oContract As New Maxiloan.Parameter.GeneralPaging

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not Page.IsPostBack Then            
            Me.SearchBy = ""
            Me.SortBy = ""

            buttonSelect.Attributes.Add("onclick", "javascript:return Select_Click()")
            buttonCancel.Attributes.Add("onclick", "Close_Window()")
        End If
    End Sub

    Private Sub InitialDefaultPanel()
        lblMessage.Text = ""
    End Sub

    Public Sub bindData(ByVal cmdWhere As String, ByVal strSortBy As String)
        With oContract
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = strSortBy
            .SpName = "spAOPaging"
        End With

        oContract = cContract.GetGeneralPaging(oContract)
        recordCount = oContract.TotalRecords
        dtgAO.DataSource = oContract.ListData

        Try
            dtgAO.DataBind()
        Catch
            dtgAO.CurrentPageIndex = 0
            dtgAO.DataBind()
        End Try

        PagingFooter()
    End Sub

#Region "Sort"
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        bindData(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            lblMessage.Text = "Data Not Found ....."
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblRecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        bindData(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGoPage.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                bindData(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub
#End Region


    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        cboSearchBy.SelectedIndex = 0
        txtSearchBy.Text = ""
        Me.SortBy = ""
        Me.SearchBy = " Branchid = '" & Me.sesBranchId.Replace("'", "").Trim & "' "
        bindData(Me.SearchBy, Me.SortBy)
    End Sub


    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Me.SearchBy = " Branchid = '" & Me.sesBranchId.Replace("'", "").Trim & "' "

        If cboSearchBy.Text.Trim <> "" Then
            Me.SearchBy = Me.SearchBy & " AND " & cboSearchBy.SelectedValue & " LIKE '%" & txtSearchBy.Text.Trim.Replace("'", "''") & "%'"
        End If

        bindData(Me.SearchBy, Me.SortBy)
    End Sub


    Public Function Get_Radio(ByVal pStr1 As String, ByVal pStr2 As String) As String
        Return "<input type=radio name=rbtSelect onclick=""javascript:Lookup_Checked('" & Trim(pStr1) & "','" & Trim(pStr2) & "')"" value='" & pStr1 & "'>"
    End Function

End Class