﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="LookupKabupaten.aspx.vb"
    Inherits="Maxiloan.Webform.LookupKabupaten" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Lookup Kabupaten</title>
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function Close_Window() {
            window.close();
        }
        function ProductOffering_Checked(pStrOfferingID, pStrValue) {
            with (document.forms['form1']) {
                hdnProductOfferingID.value = pStrOfferingID;
                hdnProductOfferingDescription.value = pStrValue;
            }
        }

        function Select_Click() {
            with (document.forms['form1']) {
                var lObjName = '<%= Request.QueryString("ProductOfferingId")%>';
                if (eval('opener.document.forms[0].' + lObjName)) {
                    eval('opener.document.forms[0].' + lObjName).value = hdnProductOfferingID.value;
                }
                var lObjName = '<%= Request.QueryString("Description")%>';
                if (eval('opener.document.forms[0].' + lObjName)) {
                    eval('opener.document.forms[0].' + lObjName).value = hdnProductOfferingDescription.value;
                }
            }
            window.close();
        }	
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="sc1">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="UpdatePanel1">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server"></asp:Label>
            <div class="form_box_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h4>
                        DAFTAR KOTA/KABUPATEN
                    </h4>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ws">
                        <asp:DataGrid ID="dtgPaging" runat="server" CellPadding="0" OnSortCommand="SortGrid"
                            BackColor="White" BorderWidth="1px" BorderStyle="None" DataKeyField="DatiID"
                            AutoGenerateColumns="False" AllowSorting="True" AllowPaging="True" Visible="True"
                            Width="100%" CssClass="grid_general">
                            <ItemStyle CssClass="item_grid"></ItemStyle>
                            <HeaderStyle CssClass="th"></HeaderStyle>
                            <Columns>
                                <asp:TemplateColumn HeaderText="SELECT">
                                    <HeaderStyle HorizontalAlign="Center" Width="5%"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                                    <ItemTemplate>
                                        <%# Get_Radio(Container.DataItem("DatiID"), Container.DataItem("NamaKabKot"))%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn ReadOnly="True" DataField="NamaKabKot" SortExpression="NamaKabKot"
                                    HeaderText="KOTA / KABUPATEN" ItemStyle-CssClass="name_col"></asp:BoundColumn>
                                <asp:BoundColumn ReadOnly="True" DataField="Propinsi" SortExpression="Propinsi" HeaderText="PROVINSI">
                                </asp:BoundColumn>
                                <asp:BoundColumn ReadOnly="True" DataField="Type" SortExpression="Type" HeaderText="KOTA/KABUPATEN">
                                </asp:BoundColumn>
                            </Columns>
                            <PagerStyle Visible="False"></PagerStyle>
                        </asp:DataGrid>
                    </div>
                    <div class="button_gridnavigation">
                        <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" ImageUrl="../Images/grid_navbutton01.png"
                            OnCommand="NavigationLink_Click" CommandName="First"></asp:ImageButton>
                        <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" ImageUrl="../Images/grid_navbutton02.png"
                            OnCommand="NavigationLink_Click" CommandName="Prev"></asp:ImageButton>
                        <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" ImageUrl="../Images/grid_navbutton03.png"
                            OnCommand="NavigationLink_Click" CommandName="Next"></asp:ImageButton>
                        <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" ImageUrl="../Images/grid_navbutton04.png"
                            OnCommand="NavigationLink_Click" CommandName="Last"></asp:ImageButton>
                        Page
                        <asp:TextBox ID="txtGoPage" runat="server" Width="34px">1</asp:TextBox>
                        <asp:Button ID="btnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                            EnableViewState="False" />
                        <asp:RangeValidator ID="Rangevalidator1" runat="server" Type="Integer" MaximumValue="999999999"
                            ErrorMessage="Page No. is not valid" MinimumValue="1" ControlToValidate="txtGoPage"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="rfvGo" runat="server" ErrorMessage="Page No. is not valid"
                            ControlToValidate="txtGoPage" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtGoPage" MinimumValue="1"
                            MaximumValue="999" ErrorMessage="Page No. is not valid" Type="Double" Display="Dynamic"></asp:RangeValidator>
                    </div>
                    <div class="label_gridnavigation">
                        <asp:Label ID="lblPage" runat="server"></asp:Label>of
                        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                        <asp:Label ID="lblTotRec" runat="server"></asp:Label>record(s)
                    </div>
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="buttonSelect" runat="server" Text="Select" CssClass="small button blue" />
                <asp:Button ID="buttonCancel" runat="server" Text="Cancel" CssClass="small button gray"
                    CausesValidation="false" />
                <a href="javascript:window.close();"></a>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label class="label_auto">
                        Find By</label>
                    <asp:DropDownList ID="cboSearchBy" runat="server">
                        <asp:ListItem Value="NamaKabKot">Nama Kab/Kot</asp:ListItem>
                        <asp:ListItem Value="Propinsi">Propinsi</asp:ListItem>
                    </asp:DropDownList>
                    <asp:TextBox ID="txtSearchBy" runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="btnSearch" runat="server" CausesValidation="False" Text="Find" CssClass="small button blue">
                </asp:Button>
                <asp:Button ID="btnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
                </asp:Button>
            </div>
            <input id="hdnProductOfferingID" type="hidden" name="hdnProductOfferingID" runat="server" />            
            <input id="hdnProductOfferingDescription" type="hidden" name="hdnProductOfferingDescription"
                runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
