﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="LookupCustomer.aspx.vb"
    Inherits="Maxiloan.Webform.LookupCustomer" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href='../Include/<%= request("style") %>.css' type="text/css" rel="stylesheet" />
    <script type="text/javascript">
        function Close_Window() {
            window.close();
        }
        function Customer_Checked(pStrCustomerID, pStrCustomerName) {
            with (document.forms['form1']) {
                hdnCustomerID.value = pStrCustomerID;
                hdnCustomerName.value = pStrCustomerName;
            }
        }

        function Select_Click() {
            with (document.forms['form1']) {
                var lObjName = '<%= Request.QueryString("CustomerID")%>';
                if (eval('opener.document.forms[0].' + lObjName)) {
                    eval('opener.document.forms[0].' + lObjName).value = hdnCustomerID.value;
                }
                var lObjName = '<%= Request.QueryString("CustomerName")%>';
                if (eval('opener.document.forms[0].' + lObjName)) {
                    eval('opener.document.forms[0].' + lObjName).value = hdnCustomerName.value;
                }
            }
            window.close();
        }	
				
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:RangeValidator ID="rgvGo" runat="server" Font-Size="8" Font-Name="Verdana" ControlToValidate="txtGoPage"
        MinimumValue="1" MaximumValue="999" ErrorMessage="Page No. is not valid" Type="Double"
        Style="z-index: 101; left: 16px; position: absolute; top: 16px"></asp:RangeValidator>
    <asp:Label ID="lblMessage" Font-Name="Verdana" Font-Size="8" ForeColor="red" runat="server"
        Style="z-index: 102; left: 16px; position: absolute; top: 0px"></asp:Label>
    <input id="hdnCustomerID" type="hidden" name="hdnCustomerId" runat="server" style="z-index: 103;
        left: 136px; position: absolute; top: 8px">
    <input id="hdnCustomerName" type="hidden" name="hdnCustomerName" runat="server" style="z-index: 104;
        left: 288px; position: absolute; top: 8px">
    <br>
    <asp:Panel ID="pnlList" runat="server" HorizontalAlign="center" Width="100%">
        <asp:Panel ID="pnlSupplierList" runat="server">
            <table cellspacing="0" cellpadding="0" width="95%" border="0">
                <tr class="trtopi">
                    <td class="tdtopikiri" width="10" height="20">
                        &nbsp;
                    </td>
                    <td class="tdtopi" align="center">
                        List Of Customer
                    </td>
                    <td class="tdtopikanan" width="10">
                        &nbsp;
                    </td>
                </tr>
            </table>
            <table class="tablegrid" cellspacing="0" cellpadding="0" width="95%" align="center"
                border="0">
                <tr>
                    <td>
                        <asp:DataGrid ID="dtgCustomer" runat="server" CellPadding="0" OnSortCommand="SortGrid"
                            BackColor="White" BorderWidth="1px" BorderStyle="None" BorderColor="#CCCCCC"
                            DataKeyField="CustomerID" AutoGenerateColumns="False" AllowSorting="True" AllowPaging="True"
                            Width="100%">
                            <SelectedItemStyle CssClass="tdgenap"></SelectedItemStyle>
                            <AlternatingItemStyle CssClass="tdgenap"></AlternatingItemStyle>
                            <ItemStyle CssClass="tdganjil"></ItemStyle>
                            <HeaderStyle CssClass="tdjudul"></HeaderStyle>
                            <FooterStyle ForeColor="#000066" BackColor="White"></FooterStyle>
                            <Columns>
                                <asp:TemplateColumn HeaderText="SELECT">
                                    <HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                                    <ItemTemplate>
                                        <%# Get_Radio(Container.DataItem("CustomerId"),Container.DataItem("Name"))%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="Name" HeaderText="CUSTOMER NAME">
                                    <HeaderStyle Font-Underline="True" Height="30px"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Left" Width="30%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Image ID="imgBadType" runat="server"></asp:Image>
                                        <asp:Label ID="lblCustomerName" runat="server" Text='<%#Container.DataItem("Name")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="Address" HeaderText="CUSTOMER ADDRESS">
                                    <HeaderStyle Font-Underline="True" Height="30px"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Left" Width="50%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblAddress" runat="server" Text='<%#Container.DataItem("Address")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn Visible="False" HeaderText="CUSTOMERID">
                                    <HeaderStyle Font-Underline="True" Height="30px"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Left" Width="65%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblCustomerID" runat="server" Text='<%#Container.DataItem("CustomerID")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="BadType" Visible="False">
                                    <HeaderStyle Font-Underline="True" Height="30px"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Left" Width="15%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblBadType" runat="server" Text='<%#Container.DataItem("BadType")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                            <PagerStyle Visible="False" HorizontalAlign="Left" ForeColor="#000066" BackColor="White"
                                Mode="NumericPages"></PagerStyle>
                        </asp:DataGrid>
                    </td>
                </tr>
            </table>
            <table class="nav_tbl" cellspacing="0" cellpadding="0" align="center">
                <tr>
                    <td class="nav_tbl_td1">
                        <table class="nav_command_tbl" cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                    <asp:ImageButton ID="ImbSelect" runat="server" CausesValidation="False" ImageUrl="../Images/ButtonSelect.gif">
                                    </asp:ImageButton>
                                </td>
                                <td>
                                    <asp:ImageButton ID="imbExit" runat="server" CausesValidation="False" ImageUrl="../Images/ButtonExit.gif">
                                    </asp:ImageButton><a href="javascript:window.close();"></a>&nbsp;<a href="javascript:window.close();"></a>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td class="nav_tbl_td2">
                        <table class="nav_page_tbl" cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                    <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" ImageUrl="../Images/butkiri1.gif"
                                        OnCommand="NavigationLink_Click" CommandName="First"></asp:ImageButton><font face="Verdana"></font>
                                </td>
                                <td>
                                    <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" ImageUrl="../Images/butkiri.gif"
                                        OnCommand="NavigationLink_Click" CommandName="Prev"></asp:ImageButton><font face="Verdana"></font>
                                </td>
                                <td>
                                    <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" ImageUrl="../Images/butkanan.gif"
                                        OnCommand="NavigationLink_Click" CommandName="Next"></asp:ImageButton><font face="Verdana"></font>
                                </td>
                                <td>
                                    <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" ImageUrl="../Images/butkanan1.gif"
                                        OnCommand="NavigationLink_Click" CommandName="Last"></asp:ImageButton><font face="Verdana"></font>
                                </td>
                                <td>
                                    Page&nbsp;
                                    <asp:TextBox ID="txtGoPage" runat="server" Width="34px" >1</asp:TextBox>
                                </td>
                                <td>
                                    <asp:ImageButton ID="imbGoPage" runat="server" ImageUrl="../Images/butgo.gif" EnableViewState="False">
                                    </asp:ImageButton>
                                </td>
                                <asp:RangeValidator ID="Rangevalidator1" runat="server" Font-Size="11px" font-name="Verdana"
                                    ForeColor="#993300" Type="Integer" MaximumValue="999999999" ErrorMessage="Page No. is not valid"
                                    MinimumValue="1" ControlToValidate="txtGoPage"></asp:RangeValidator>
                                <asp:RequiredFieldValidator ID="rfvGo" runat="server" Font-Size="11px" font-name="Verdana"
                                    ForeColor="#993300" ErrorMessage="Page No. is not valid" ControlToValidate="txtGoPage"
                                    Display="Dynamic"></asp:RequiredFieldValidator></tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="nav_totpage" colspan="2">
                        <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                        <asp:Label ID="lblTotRec" runat="server"></asp:Label>&nbsp;record(s)
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <table cellspacing="0" cellpadding="0" width="95%" border="0">
            <tr class="trtopi">
                <td class="tdtopikiri">                    
                </td>
                <td class="tdtopi" align="center">
                    Find Customer
                </td>
                <td class="tdtopikanan">
                    &nbsp;
                </td>
            </tr>
        </table>
        <table class="tablegrid" cellspacing="1" cellpadding="2" width="95%" border="0">
            <tr>
                <td class="tdgenap" width="20%">
                    Find&nbsp;By
                </td>
                <td class="tdganjil" width="81%">
                    &nbsp;
                    <asp:DropDownList ID="cboSearchBy" runat="server">
                        <asp:ListItem Value="CustomerName">Customer Name</asp:ListItem>
                        <asp:ListItem Value="CustomerAddress">Customer Address</asp:ListItem>
                    </asp:DropDownList>
                    <asp:TextBox ID="txtSearchBy" runat="server" ></asp:TextBox>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="95%" border="0">
            <tr>
                <td align="left">
                    <asp:ImageButton ID="imbSearch" runat="server" CausesValidation="False" ImageUrl="../Images/ButtonSearch.gif">
                    </asp:ImageButton>&nbsp;
                    <asp:ImageButton ID="imbReset" runat="server" CausesValidation="False" ImageUrl="../Images/ButtonReset.gif">
                    </asp:ImageButton>
                </td>
            </tr>
        </table>
    </asp:Panel>
    </form>
</body>
</html>
