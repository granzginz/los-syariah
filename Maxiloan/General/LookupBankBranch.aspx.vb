﻿Imports Maxiloan.Controller

Public Class LookupBankBranch
    Inherits Maxiloan.Webform.WebBased

    Private m_controller As New LookUpBankBranchController
    Private currentPage As Integer = 1
    Private pageSize As Integer = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If

        lblMessage.Text = ""

        If Not Me.IsPostBack Then
            txtGoPage.Text = "1"
            Me.SortBy = "BankBranchName ASC"

            If Request("cond") <> "" Then
                Me.SearchBy = Request("cond")
            Else
                Me.SearchBy = ""
            End If

            BindGridEntity(Me.SearchBy)
        End If

        ImbSelect.Attributes.Add("onclick", "javascript:return Select_Click()")
        imbCancel.Attributes.Add("onclick", "Close_Window()")
    End Sub

    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        If txtSearch.Text.Trim <> "" Then
            If Right(txtSearch.Text.Trim, 1) = "%" Then
                Me.SearchBy = cboSearchBy.SelectedItem.Value + " like '" + Replace(txtSearch.Text.Trim, "'", "''") + "'"
            Else
                Me.SearchBy = cboSearchBy.SelectedItem.Value + " = '" + Replace(txtSearch.Text.Trim, "'", "''") + "'"
            End If
        Else
            Me.SearchBy = ""
        End If

        BindGridEntity(Me.SearchBy)
    End Sub

    Sub BindGridEntity(ByVal cmdWhere As String)
        Dim dttEntity As DataTable
        Dim oCustomClass As New Maxiloan.Parameter.LookUpBankBranch

        If cmdWhere <> "" Then cmdWhere = cmdWhere & " and "
        If Request("BankID") <> "" Then cmdWhere = cmdWhere & " BankID = '" & Request("BankID") & "'"

        pnlGrid.Visible = True
        oCustomClass = m_controller.GetListData(GetConnectionString, cmdWhere, currentPage, pageSize, Me.SortBy)

        If Not oCustomClass Is Nothing Then
            dttEntity = oCustomClass.ListData
            recordCount = oCustomClass.TotalRecords
        Else
            recordCount = 0
        End If

        dtgPaging.DataSource = dttEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()

        PagingFooter()
    End Sub

    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbReset.Click
        cboSearchBy.SelectedIndex = 0
        txtSearch.Text = ""
        BindGridEntity("")
    End Sub

    Public Function Get_Radio(ByVal pBankBranchId As String, ByVal pBankCode As String, ByVal pCity As String, ByVal pBankBranchName As String) As String
        Return "<input type=radio name=rbtZipCode onclick=""javascript:lookup_checked('" & Trim(pBankBranchId) & "','" & Trim(pBankCode) & "','" & Trim(pCity) & "','" & Trim(pBankBranchName) & "')"" value='" & pBankCode & "'>"
    End Function

#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            lblMessage.Text = "Data tidak ditemukan ....."
            lblTotPage.Text = "1"            
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()            
        End If
        lblTotRec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridEntity(Me.SearchBy)
    End Sub
    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbGoPage.Click
        If IsNumeric(txtGoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtGoPage.Text, Int32)
                BindGridEntity(Me.SearchBy)
            End If
        End If
    End Sub
#End Region
End Class
