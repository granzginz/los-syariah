﻿
#Region "Imports"
Imports Maxiloan.Controller
#End Region

Public Class LookUpZipCode
    Inherits WebBased

    Private m_controller As New LookUpZipCodeController
    Private currentPage As Integer = 1
    Private pageSize As Integer = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If
        lblMessage.Text = ""
        If Not Me.IsPostBack Then
            txtGoPage.Text = "1"
            Me.SortBy = "City ASC"
            If Request("cond") <> "" Then
                Me.SearchBy = Request("cond")
            Else
                Me.SearchBy = "ALL"
            End If
            'fill cboCity with data
            Dim dttCitySearch As New DataTable
            dttCitySearch = m_controller.GetCitySearch(GetConnectionString)
            CboCity.DataValueField = "City"
            CboCity.DataTextField = "City"
            CboCity.DataSource = dttCitySearch.DefaultView

            CboCity.DataBind()
            CboCity.Items.Insert(1, "ALL")
            CboCity.Items(1).Value = "ALL"
            CboCity.Items.Insert(0, "Select One")
            CboCity.Items(0).Value = "0"
            pnlGrid.Visible = False
            'agar pertama kali page load tidak berat krn hrs tarik data banyak
            'BindGridEntity(Me.CmdWhere)

        End If
        buttonSelect.Attributes.Add("onclick", "javascript:return Select_Click()")
        buttonCancel.Attributes.Add("onclick", "Close_Window()")

    End Sub

    Sub BindGridEntity(ByVal cmdWhere As String)
        Dim dttEntity As DataTable
        Dim oCustomClass As New Maxiloan.Parameter.LookUpZipCode
        pnlGrid.Visible = True
        oCustomClass = m_controller.GetListData(GetConnectionString, cmdWhere, currentPage, pageSize, Me.SortBy)

        If Not oCustomClass Is Nothing Then
            dttEntity = oCustomClass.ListData
            recordCount = oCustomClass.TotalRecords
        Else
            recordCount = 0
        End If

        dtgPaging.DataSource = dttEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()
        PagingFooter()
    End Sub

#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            lblMessage.Text = "Data tidak ditemukan ....."
            lblTotPage.Text = "1"
            'rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            'rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblTotRec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridEntity(Me.SearchBy)
    End Sub
    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles btnGoPage.Click
        If IsNumeric(txtGoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtGoPage.Text, Int32)
                BindGridEntity(Me.SearchBy)
            End If
        End If
    End Sub
#End Region

    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles buttonSearch.Click
        Me.SearchBy = "ALL"
        If CboCity.SelectedIndex > 0 Then
            If CboCity.SelectedValue = "ALL" Then
                If txtSearch.Text.Trim <> "" Then
                    'If Right(txtSearch.Text.Trim, 1) = "%" Then
                    '    Me.SearchBy = cboSearchBy.SelectedItem.Value + " like '" + Replace(txtSearch.Text.Trim, "'", "''") + "'"
                    'Else
                    '    Me.SearchBy = cboSearchBy.SelectedItem.Value + " = '" + Replace(txtSearch.Text.Trim, "'", "''") + "'"
                    'End If

                    Me.SearchBy = cboSearchBy.SelectedItem.Value + " LIKE '%" + Replace(txtSearch.Text.Trim, "'", "''") + "%'"
                End If
            Else
                Me.SearchBy = " City = '" & CboCity.SelectedItem.Value & "'"
                If txtSearch.Text.Trim <> "" Then
                    'If Right(txtSearch.Text.Trim, 1) = "%" Then
                    '    Me.SearchBy &= " And " & cboSearchBy.SelectedItem.Value + " like '" + Replace(txtSearch.Text.Trim, "'", "''") + "'"
                    'Else
                    '    Me.SearchBy &= " And " & cboSearchBy.SelectedItem.Value + " = '" + Replace(txtSearch.Text.Trim, "'", "''") + "'"
                    'End If

                    Me.SearchBy &= " AND " & cboSearchBy.SelectedItem.Value + " LIKE '%" + Replace(txtSearch.Text.Trim, "'", "''") + "%'"
                End If
            End If
            BindGridEntity(Me.SearchBy)
        End If
    End Sub

    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles buttonReset.Click
        CboCity.SelectedIndex = 0
        cboSearchBy.SelectedIndex = 0
        txtSearch.Text = ""
        BindGridEntity("ALL")
    End Sub

    Public Function Get_Radio(ByVal pZipCode As String, ByVal pKelurahan As String, ByVal pKecamatan As String, ByVal pCity As String) As String
        Return "<input type=radio name=rbtZipCode onclick=""javascript:ZipCode_Checked('" & Trim(pZipCode) & "','" & Trim(pKelurahan) & "','" & Trim(pKecamatan) & "','" & Trim(pCity) & "')"" value='" & pZipCode & "'>"
    End Function
End Class
