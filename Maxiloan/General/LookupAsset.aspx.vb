﻿Imports Maxiloan.Parameter
Imports Maxiloan.Controller

Public Class LookupAsset
    Inherits WebBased


#Region "Variables & Constants"
    Private m_controller As New LookUpAssetController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region

#Region "Properties"
    Public Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property

    Public Property ApplicationId() As String
        Get
            Return CType(ViewState("ApplicationId"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationId") = Value
        End Set
    End Property

    Public Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property

    Public Property AssetTypeID() As String
        Get
            Return CType(ViewState("AssetTypeID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("AssetTypeID") = Value
        End Set
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then Exit Sub
        If Not IsPostBack Then
            lblMessage.Text = ""
            Me.CmdWhere = ""
            AssetTypeID = Request("AssetTypeID")

            buttonSelect.Attributes.Add("onclick", "javascript:return Select_Click()")
            buttonCancel.Attributes.Add("onclick", "Close_Window()")
        End If

    End Sub

#End Region

    Public Sub BindGridEntity(ByVal cmdWhere As String)
        Dim dttEntity As DataTable = Nothing
        Dim oCustomClass As New Maxiloan.Parameter.LookUpAsset

        oCustomClass.PageSize = pageSize
        oCustomClass.WhereCond = Me.CmdWhere
        oCustomClass.BranchId = Replace(Me.sesBranchId, "'", "")
        oCustomClass.AssetTypeID = AssetTypeID
        oCustomClass.CurrentPage = currentPage
        oCustomClass.strConnection = GetConnectionString()
        oCustomClass = m_controller.GetListDataMulti(oCustomClass)

        If Not oCustomClass Is Nothing Then
            dttEntity = oCustomClass.ListData
            recordCount = oCustomClass.TotalRecords
        Else
            recordCount = 0
        End If

        dtgPaging.DataSource = dttEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()
        PagingFooter()
    End Sub

#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            lblMessage.Text = "Data not found....."
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblTotRec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select

        BindGridEntity(Me.CmdWhere)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGoPage.Click
        If IsNumeric(txtGoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtGoPage.Text, Int32)
                BindGridEntity(Me.CmdWhere)
            End If
        End If
    End Sub
#End Region

    Protected Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If

        BindGridEntity(Me.CmdWhere)
    End Sub

    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        cboSearchBy.SelectedIndex = 0
        txtSearchBy.Text = ""
        Me.CmdWhere = txtSearchBy.Text        
        BindGridEntity(Me.CmdWhere)        
    End Sub

    Private Sub imbSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            If txtSearchBy.Text <> "" Then
                If cboSearchBy.SelectedItem.Value = "AssetCode" Then
                    Me.CmdWhere = "AssetCode LIKE '%" & txtSearchBy.Text & "'%"
                ElseIf cboSearchBy.SelectedItem.Value = "Description" Then
                    Me.CmdWhere = "Description LIKE '%" & txtSearchBy.Text & "%'"
                End If                

                BindGridEntity(Me.CmdWhere)
            Else
                Me.CmdWhere = txtSearchBy.Text                
                BindGridEntity(Me.CmdWhere)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub

    Public Function Get_Radio(ByVal pAssetCode As String, ByVal pAssetName As String) As String
        Return "<input type=radio name=rbtAsset onclick=""javascript:Asset_Checked('" & Trim(pAssetCode) & "','" & Trim(pAssetName) & "')"" value='" & pAssetCode & "'>"
    End Function
End Class