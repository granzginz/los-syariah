﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="LookupTransaction.aspx.vb"
    Inherits="Maxiloan.Webform.LookupTransaction" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        function Close_Window() {
            window.close();
        }
        function Transaction_Checked(pAllocationID, pDescription) {
            with (document.forms['form1']) {
                hdnTransaction.value = pAllocationID;
                hdnDescription.value = pDescription;
            }
        }
        function Select_Click() {
            valid = 0;
            //untuk menjaga bila radio bukan berupa array of radiobutton
            if (document.forms['form1'].rbtAllocationID.length == undefined) {
                valid = 1;
            }

            for (var i = 0; i < document.forms['form1'].rbtAllocationID.length; i++) {
                if (document.forms['form1'].rbtAllocationID[i].checked == "1") {
                    valid = 1;
                }
            }
            if (valid == 1) {
                with (document.forms['form1']) {
                    var lObjName = '<%= Request.QueryString("Transaction")%>';
                    if (eval('opener.document.forms["form1"].' + lObjName)) {
                        eval('opener.document.forms["form1"].' + lObjName).value = hdnTransaction.value;
                    }
                    var lObjName = '<%= Request.QueryString("Description")%>';
                    if (eval('opener.document.forms["form1"].' + lObjName)) {
                        eval('opener.document.forms["form1"].' + lObjName).value = hdnDescription.value;
                    }
                };
                window.close();
            } else {
                alert("Please Choose Transaction !! ");
                return false;
            }
        }
        function click() {
            if (event.button == 2) {
                alert('Anda tidak diperbolehkan!');
            }
        }
        document.onmousedown = click
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" ForeColor="red"></asp:Label>
    <div class="form_box_title">
        <div class="form_single">
            <h4>
                DAFTAR TRANSAKSI
            </h4>
        </div>
    </div>
    <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgPaging" runat="server" Width="100%" OnSortCommand="Sorting"
                    AutoGenerateColumns="False" AllowSorting="True" CssClass="grid_general">
                    <ItemStyle CssClass="item_grid"></ItemStyle>
                    <HeaderStyle CssClass="th"></HeaderStyle>
                    <Columns>
                        <asp:TemplateColumn HeaderText="Select">
                            <HeaderStyle HorizontalAlign="Center" Width="5%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            <HeaderTemplate>
                                SELECT
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%# Get_Radio(Container.DataItem("PaymentAllocationID"),Container.DataItem("Description"))%>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="PaymentAllocationID" HeaderText="PAYMENT ALLOCATION ID"
                            SortExpression="PaymentAllocationID" Visible="false">
                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="Description" HeaderText="DESCRIPTION" SortExpression="Description">
                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="COA" HeaderText="COA" SortExpression="COA">
                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                        </asp:BoundColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
            <div class="button_gridnavigation">
                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../Images/grid_navbutton01.png"
                    CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                </asp:ImageButton>
                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../Images/grid_navbutton02.png"
                    CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                </asp:ImageButton>
                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../Images/grid_navbutton03.png"
                    CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                </asp:ImageButton>
                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../Images/grid_navbutton04.png"
                    CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False" />
                Page
                <asp:TextBox ID="txtGoPage" runat="server" CssClass="small_text">1</asp:TextBox>
                <asp:Button ID="btnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                    EnableViewState="False" />
                <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtGoPage" MinimumValue="1"
                    ErrorMessage="Page No. is not valid" MaximumValue="999999999" Type="Integer"
                    CssClass="validator_general"></asp:RangeValidator>
                <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtGoPage"
                    ErrorMessage="Page No. is not valid" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
            <div class="label_gridnavigation">
                <asp:Label ID="lblPage" runat="server"></asp:Label>of
                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                <asp:Label ID="lblTotRec" runat="server"></asp:Label>record(s)
            </div>
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="btnSelect" runat="server" Text ="Select" CssClass ="small button blue" CausesValidation="False" />
        <asp:Button ID="btnCancel" runat="server"  Text ="Cancel" CssClass ="small button gray" CausesValidation="False"></asp:Button>        
    </div>
    <div class="form_box_title">
        <div class="form_single">
            <h4>
                FIND
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Transaction
            </label>
            <asp:Label ID="lblProcessId" runat="server">.........</asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Find by
            </label>
            <asp:DropDownList ID="cboSearchBy" runat="server">
                <asp:ListItem Value="Description">Description</asp:ListItem>
                <asp:ListItem Value="COA">COA</asp:ListItem>
            </asp:DropDownList>
            <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="buttonSearch" runat="server" CssClass="small button blue" Text="Search" />
        <asp:Button ID="btnReset" runat="server" CssClass="small button gray" Text="Reset" />
    </div>
    <input id="hdnTransaction" type="hidden" name="hdnTransaction" runat="server" />
    <input id="hdnDescription" type="hidden" name="hdnDescription" runat="server" />
    </form>
</body>
</html>
