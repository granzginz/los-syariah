﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="LookupProductOffering.aspx.vb"
    Inherits="Maxiloan.Webform.LookupProductOffering" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Lookup Product Offering</title>
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function Close_Window() {
            window.close();
        }
        function ProductOffering_Checked(pStrOfferingID, pStrValue, pStrAssetTypeID) {
            with (document.forms['form1']) {
                hdnProductOfferingID.value = pStrOfferingID;
                hdnProductOfferingDescription.value = pStrValue;
                hdnAssetTypeID.value = pStrAssetTypeID;
            }
        }

        function Select_Click() {
            with (document.forms['form1']) {
                var lObjName = '<%= Request.QueryString("ProductOfferingId")%>';
                if (eval('opener.document.forms[0].' + lObjName)) {
                    eval('opener.document.forms[0].' + lObjName).value = hdnProductOfferingID.value;
                }
                var lObjName = '<%= Request.QueryString("Description")%>';
                if (eval('opener.document.forms[0].' + lObjName)) {
                    eval('opener.document.forms[0].' + lObjName).value = hdnProductOfferingDescription.value;
                }
                var lObjName = '<%= Request.QueryString("ProductId")%>';
                if (eval('opener.document.forms[0].' + lObjName)) {
                    eval('opener.document.forms[0].' + lObjName).value = hdnProductID.value;                    
                }
                var lObjName = '<%= Request.QueryString("AssetTypeID")%>';
                if (eval('opener.document.forms[0].' + lObjName)) {
                    eval('opener.document.forms[0].' + lObjName).value = hdnAssetTypeID.value;
                }
            }
            window.close();
        }	
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <%--    <asp:ScriptManager runat="server" ID="sc1">
    </asp:ScriptManager>--%>
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
    <%--<asp:UpdateProgress runat="server" ID="upg1" AssociatedUpdatePanelID="upProdOff1">
        <ProgressTemplate>
            <img src="../Images/pic-loader.gif" alt="Loading..." />
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel runat="server" ID="upProdOff1" UpdateMode="Conditional">
        <ContentTemplate>--%>
    <asp:Panel runat="server" ID="Panel1">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h4>
                    DAFTAR PRODUCT OFFERING
                </h4>
            </div>
        </div>
        <asp:Panel ID="pnlList" runat="server">
            <asp:Panel ID="pnlProductOfferingList" runat="server">
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ws">
                            <asp:DataGrid ID="dtgProductOffering" runat="server" AllowPaging="True" AllowSorting="True"
                                AutoGenerateColumns="False" DataKeyField="productofferingid" BorderStyle="None"
                                BorderWidth="1px" OnSortCommand="SortGrid" CellPadding="0" CssClass="grid_general">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="SELECT">
                                        <HeaderStyle HorizontalAlign="Center" Width="5%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                                        <ItemTemplate>
                                            <%# Get_Radio(Container.DataItem("ProductOfferingId"),Container.DataItem("Description"),Container.DataItem("assetTypeID"))%>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="productofferingid" HeaderText="PRODUCT OFFERING">
                                        <ItemStyle HorizontalAlign="Left" Width="30%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hynProductOffering" runat="server" Text='<%#Container.DataItem("ProductOfferingId")%>'>
                                            </asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="description" HeaderText="DESCRIPTION">
                                        <ItemStyle HorizontalAlign="Left" Width="60%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDescription" runat="server" Text='<%#Container.DataItem("Description")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PRODUCT OFFERING" Visible="False">
                                        <ItemStyle HorizontalAlign="Left" Width="30%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblProductOfferingID" runat="server" Text='<%#Container.DataItem("ProductOfferingId")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PRODUCT ID" Visible="False">
                                        <ItemStyle HorizontalAlign="Left" Width="30%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblProductID" runat="server" Text='<%#Container.DataItem("ProductID")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="AssetTypeID" Visible="False">
                                        <ItemStyle HorizontalAlign="Left" Width="30%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblAssetTypeID" runat="server" Text='<%#Container.DataItem("AssetTypeID")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                                <PagerStyle Visible="False"></PagerStyle>
                            </asp:DataGrid>
                        </div>
                        <div class="button_gridnavigation">
                            <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../Images/grid_navbutton01.png"
                                CausesValidation="False" CommandName="First" OnCommand="NavigationLink_Click">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../Images/grid_navbutton02.png"
                                CausesValidation="False" CommandName="Prev" OnCommand="NavigationLink_Click">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../Images/grid_navbutton03.png"
                                CausesValidation="False" CommandName="Next" OnCommand="NavigationLink_Click">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../Images/grid_navbutton04.png"
                                CausesValidation="False" CommandName="Last" OnCommand="NavigationLink_Click">
                            </asp:ImageButton>
                            Page
                            <asp:TextBox ID="txtGoPage" runat="server" Width="34px" CssClass="txtPage">1</asp:TextBox>
                            <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtGoPage" MinimumValue="1"
                                MaximumValue="999" ErrorMessage="Page No. is not valid" Type="Double" Display="Dynamic"></asp:RangeValidator>
                            <asp:Button ID="btnGoPage" runat="server" EnableViewState="False" Text="Go" CssClass="small buttongo blue" />
                            <asp:RangeValidator ID="Rangevalidator1" runat="server" Type="Integer" MaximumValue="999999999"
                                ErrorMessage="Page No. is not valid" MinimumValue="1" ControlToValidate="txtGoPage"
                                Display="Dynamic"></asp:RangeValidator>
                            <asp:RequiredFieldValidator ID="rfvGo" runat="server" ErrorMessage="Page No. is not valid"
                                ControlToValidate="txtGoPage" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        </div>
                        <div class="navigasi_totalrecord">
                            <asp:Label ID="lblPage" runat="server"></asp:Label>of
                            <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                            <asp:Label ID="lblTotRec" runat="server"></asp:Label>record(s)
                        </div>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="buttonSelect" runat="server" Text="Select" CssClass="small button blue" />
                    <asp:Button ID="buttonCancel" runat="server" Text="Cancel" CssClass="small button gray"
                        CausesValidation="false" />
                    <a href="javascript:window.close();"></a>
                </div>
            </asp:Panel>
            <asp:Panel runat="server" ID="PanelSearch">
                <div class="form_box_title">
                    <div class="form_single">
                        <h4>
                            CARI PRODUCT OFFERING</h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Product</label>
                        <asp:DropDownList ID="cboProductBranch" runat="server">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="btnSearch" runat="server" Text="Find" CssClass="small button blue"
                        CausesValidation="False" />
                    <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="small button gray"
                        CausesValidation="False" />
                    <%--<asp:UpdateProgress runat="server" ID="upgLookupProdOff1" AssociatedUpdatePanelID="upProdOff1">
                                <ProgressTemplate>
                                    <img alt="Loading..." src="../Images/pic-loader.gif" />
                                </ProgressTemplate>
                            </asp:UpdateProgress>--%>
                </div>
            </asp:Panel>
        </asp:Panel>
    </asp:Panel>
    <%--        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="imbFirstPage" EventName="Command" />
            <asp:AsyncPostBackTrigger ControlID="imbPrevPage" EventName="Command" />
            <asp:AsyncPostBackTrigger ControlID="imbNextPage" EventName="Command" />
            <asp:AsyncPostBackTrigger ControlID="imbLastPage" EventName="Command" />
            <asp:AsyncPostBackTrigger ControlID="btnGoPage" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>--%>
    <input id="hdnProductOfferingID" type="hidden" name="hdnProductOfferingID" runat="server" />
    <input id="hdnProductID" type="hidden" name="hdnProductID" runat="server" />
    <input id="hdnAssetTypeID" type="hidden" name="hdnAssetTypeID" runat="server" />
    <input id="hdnProductOfferingDescription" type="hidden" name="hdnProductOfferingDescription"
        runat="server" />
    </form>
</body>
</html>
