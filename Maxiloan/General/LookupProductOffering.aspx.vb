﻿Imports Maxiloan.Controller
Imports Maxiloan.General.CommonVariableHelper

Public Class LookupProductOffering
    Inherits WebBased

    Private oController As New LookUpProductOfferingController
    Private currentPage As Integer = DEFAULT_CURRENT_PAGE
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("cmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("cmdWhere") = Value
        End Set
    End Property

    Private Property Sort() As String
        Get
            Return CType(viewstate("Sort"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Sort") = Value
        End Set
    End Property

    

    Public Property SelectedProductID As String
        Get
            Return CType(ViewState("SelectedProductID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("SelectedProductID") = Value
        End Set
    End Property


    Public Sub doBind()
        Dim dttProductBranch As New DataTable        

        Me.Sort = "ProductOfferingID ASC"

        dttProductBranch = oController.GetProductBranch(Me.sesBranchId.Replace("'", ""), GetConnectionString)
        cboProductBranch.DataValueField = "ProductId"
        cboProductBranch.DataTextField = "Description"
        cboProductBranch.DataSource = dttProductBranch.DefaultView
        cboProductBranch.DataBind()
    End Sub


    Private Sub BindGridEntity(ByVal strWhere As String)
        Dim dttEntity As DataTable = Nothing
        Dim oCustomClass As New Parameter.LookUpProductOffering
        hdnProductID.Value = cboProductBranch.SelectedItem.Value.ToString.Trim
        With oCustomClass
            .PageSize = DEFAULT_PAGE_SIZE
            .WhereCond = strWhere
            .BusDate = Me.BusinessDate
            .CurrentPage = currentPage
            .SortBy = Me.Sort
            .strConnection = GetConnectionString()
        End With

        oCustomClass = oController.GetListData(oCustomClass)

        If Not oCustomClass Is Nothing Then
            dttEntity = oCustomClass.ListData
            recordCount = oCustomClass.TotalRecords
        Else
            recordCount = 0
        End If

        dtgProductOffering.DataSource = dttEntity.DefaultView
        dtgProductOffering.CurrentPageIndex = 0
        dtgProductOffering.DataBind()
        PagingFooter()        
    End Sub

    Private Sub dtgProductOffering_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgProductOffering.ItemDataBound
        Dim hynProductOffering As New HyperLink

        Dim lblProductOfferingID As Label
        Dim lblProductID As Label

        If e.Item.ItemIndex >= 0 Then
            lblProductOfferingID = CType(e.Item.FindControl("lblProductOfferingID"), Label)
            lblProductID = CType(e.Item.FindControl("lblProductID"), Label)

            hynProductOffering = CType(e.Item.FindControl("hynProductOffering"), HyperLink)
            hynProductOffering.NavigateUrl = LinkToProductOffering(lblProductID.Text.Trim, Me.sesBranchId.Replace("'", ""), lblProductOfferingID.Text.Trim, "AccAcq")
        End If
    End Sub

    Function LinkToProductOffering(ByVal strProductID As String, ByVal strBranchID As String, ByVal strProductOfferingID As String, ByVal strStyle As String) As String
        Return "javascript:OpenWinProductOffering('" & strProductID & "','" & strBranchID & "','" & strProductOfferingID & "','" & strStyle & "')"
    End Function

#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(DEFAULT_PAGE_SIZE, Integer)), Double))
        If totalPages = 0 Then
            lblMessage.Text = "Data not found ....."
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblTotRec.Text = recordCount.ToString
        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = DEFAULT_CURRENT_PAGE
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridEntity(Me.CmdWhere)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGoPage.Click
        If IsNumeric(txtGoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtGoPage.Text, Int32)
                BindGridEntity(Me.CmdWhere)
            End If
        End If
    End Sub
#End Region

    Protected Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub

    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        cboProductBranch.SelectedIndex = 0
        pnlProductOfferingList.Visible = False
    End Sub

    Private Sub imbSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Me.CmdWhere = " BranchId = '" & Me.sesBranchId.Replace("'", "") & "' and ProductId = '" & cboProductBranch.SelectedItem.Value & "'"
        BindGridEntity(Me.CmdWhere)
        pnlProductOfferingList.Visible = True
    End Sub

    Public Function Get_Radio(ByVal pStrOfferingId As String, ByVal pStrValue As String, ByVal pStrAssetTypeID As String) As String
        Return "<input type=radio name=rbtProductOffering onclick=""javascript:ProductOffering_Checked('" & Trim(pStrOfferingId) & "','" & Trim(pStrValue) & "','" & Trim(pStrAssetTypeID) & "')"" value='" & pStrValue & "'>"
    End Function

    Private Sub LookupProductOffering_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then Exit Sub

        If Not IsPostBack Then
            doBind()

            buttonSelect.Attributes.Add("onclick", "javascript:return Select_Click()")
            buttonCancel.Attributes.Add("onclick", "Close_Window()")
        End If
    End Sub
End Class