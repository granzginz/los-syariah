﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="LookupAssetWithAssetType.aspx.vb"
    Inherits="Maxiloan.Webform.LookupAssetWithAssetType" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href='../Include/<%= request("style") %>.css' type="text/css" rel="stylesheet" />
    <script language="javascript">
        function Close_Window() {
            window.close();
        }
        function Asset_Checked(pStrAssetCode, pStrDescription, pStrAssetTypeId) {
            with (document.forms['form1']) {
                hdnAssetCode.value = pStrAssetCode;
                hdnDescription.value = pStrDescription;
                hdnAssetTypeID.value = pStrAssetTypeId;
            }
        }

        function Select_Click() {
            with (document.forms['form1']) {
                var lObjName = '<%= Request.QueryString("assetcode")%>';
                if (eval('opener.document.forms[0].' + lObjName)) {
                    eval('opener.document.forms[0].' + lObjName).value = hdnAssetCode.value;
                }
                var lObjName = '<%= Request.QueryString("description")%>';
                if (eval('opener.document.forms[0].' + lObjName)) {
                    eval('opener.document.forms[0].' + lObjName).value = hdnDescription.value;
                }
                var lObjName = '<%= Request.QueryString("assettypeid")%>';
                if (eval('opener.document.forms[0].' + lObjName)) {
                    eval('opener.document.forms[0].' + lObjName).value = hdnAssetTypeID.value;
                }
            }
            window.close();
        }	
				
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:RangeValidator ID="rgvGo" runat="server" Type="Double" 
        ControlToValidate="txtGoPage"></asp:RangeValidator>
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <input id="hdnAssetCode" type="hidden" name="hdnAssetCode" runat="server" />
    <input id="hdnAssetTypeID" type="hidden" name="hdnAssetTypeID" runat="server" />
    <input id="hdnDescription" type="hidden" name="hdnDescription" runat="server" />
    <asp:Panel ID="pnlList" runat="server" HorizontalAlign="center" Width="100%">
        <asp:Panel ID="pnlAssetList" runat="server">
            <table>
                <tr>
                    <td>
                    </td>
                    <td>
                        List Of Asset
                    </td>
                    <td>
                    </td>
                </tr>
            </table>
            <table>
                <tr>
                    <td>
                        <asp:DataGrid ID="dtgAsset" runat="server" Visible="True" Width="100%" AllowPaging="True"
                            AllowSorting="True" AutoGenerateColumns="False" DataKeyField="AssetCode" BorderColor="#CCCCCC"
                            BorderStyle="None" BorderWidth="1px" BackColor="White" OnSortCommand="SortGrid"
                            CellPadding="0">
                            <SelectedItemStyle></SelectedItemStyle>
                            <AlternatingItemStyle></AlternatingItemStyle>
                            <ItemStyle></ItemStyle>
                            <HeaderStyle></HeaderStyle>
                            <FooterStyle ForeColor="#000066" BackColor="White"></FooterStyle>
                            <Columns>
                                <asp:TemplateColumn HeaderText="SELECT">
                                    <HeaderStyle HorizontalAlign="Center" Width="5%"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                                    <ItemTemplate>
                                        <%# Get_Radio(Container.DataItem("AssetCode"),Container.DataItem("Description"),Container.DataItem("AssetTypeId"))%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="AssetCode" HeaderText="ASSET CODE">
                                    <HeaderStyle Font-Underline="True" Height="30px"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Left" Width="30%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblAssetCode" runat="server" Text='<%#Container.DataItem("AssetCode")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="description" HeaderText="DESCRIPTION">
                                    <HeaderStyle Font-Underline="True" Height="30px"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Left" Width="60%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblDescription" runat="server" Text='<%#Container.DataItem("Description")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="assettypeid" HeaderText="assettypeid" Visible="false">
                                    <HeaderStyle Font-Underline="True" Height="30px"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Left" Width="60%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblassettypeid" runat="server" Text='<%#Container.DataItem("AssetTypeId")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                            <PagerStyle Visible="False" HorizontalAlign="Left" ForeColor="#000066" BackColor="White"
                                Mode="NumericPages"></PagerStyle>
                        </asp:DataGrid>
                    </td>
                </tr>
            </table>
            <table>
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td>
                                    <asp:ImageButton ID="ImbSelect" runat="server" CausesValidation="False" ImageUrl="../Images/ButtonSelect.gif">
                                    </asp:ImageButton>
                                </td>
                                <td>
                                    <asp:ImageButton ID="imbExit" runat="server" CausesValidation="False" ImageUrl="../Images/ButtonExit.gif">
                                    </asp:ImageButton><a href="javascript:window.close();"></a><a href="javascript:window.close();"></a>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        <table>
                            <tr>
                                <td>
                                    <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../Images/butkiri1.gif"
                                        CausesValidation="False" CommandName="First" OnCommand="NavigationLink_Click">
                                    </asp:ImageButton><font face="Verdana"></font>
                                </td>
                                <td>
                                    <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../Images/butkiri.gif"
                                        CausesValidation="False" CommandName="Prev" OnCommand="NavigationLink_Click">
                                    </asp:ImageButton><font face="Verdana"></font>
                                </td>
                                <td>
                                    <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../Images/butkanan.gif"
                                        CausesValidation="False" CommandName="Next" OnCommand="NavigationLink_Click">
                                    </asp:ImageButton><font face="Verdana"></font>
                                </td>
                                <td>
                                    <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../Images/butkanan1.gif"
                                        CausesValidation="False" CommandName="Last" OnCommand="NavigationLink_Click">
                                    </asp:ImageButton><font face="Verdana"></font>
                                </td>
                                <td>
                                    Page
                                    <asp:TextBox ID="txtGoPage" runat="server" Width="34px">1</asp:TextBox>
                                </td>
                                <td>
                                    <asp:ImageButton ID="imbGoPage" runat="server" ImageUrl="../Images/butgo.gif" EnableViewState="False">
                                    </asp:ImageButton>
                                </td>
                                <asp:RangeValidator ID="Rangevalidator1" runat="server" Font-Size="11px" font-name="Verdana"
                                    ForeColor="#993300" Type="Integer" MaximumValue="999999999" ErrorMessage="Page No. is not valid"
                                    MinimumValue="1" ControlToValidate="txtGoPage"></asp:RangeValidator>
                                <asp:RequiredFieldValidator ID="rfvGo" runat="server" Font-Size="11px" font-name="Verdana"
                                    ForeColor="#993300" ErrorMessage="Page No. is not valid" ControlToValidate="txtGoPage"
                                    Display="Dynamic"></asp:RequiredFieldValidator></tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblPage" runat="server"></asp:Label>of
                        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                        <asp:Label ID="lblTotRec" runat="server"></asp:Label>record(s)
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <table>
            <tr>
                <td>
                </td>
                <td>
                    Find Asset
                </td>
                <td>
                </td>
            </tr>
        </table>
        <table>
            <tr>
                <td>
                    FindBy
                </td>
                <td>
                    <asp:DropDownList ID="cboSearchBy" runat="server">
                        <asp:ListItem Value="AssetCode">Asset Code</asp:ListItem>
                        <asp:ListItem Value="Description">Description</asp:ListItem>
                    </asp:DropDownList>
                    <asp:TextBox ID="txtSearchBy" runat="server"></asp:TextBox>
                </td>
            </tr>
        </table>
        <table>
            <tr>
                <td>
                    <asp:ImageButton ID="imbSearch" runat="server" ImageUrl="../Images/ButtonSearch.gif"
                        CausesValidation="False"></asp:ImageButton>
                    <asp:ImageButton ID="imbReset" runat="server" ImageUrl="../Images/ButtonReset.gif"
                        CausesValidation="False"></asp:ImageButton>
                </td>
            </tr>
        </table>
    </asp:Panel>
    </form>
</body>
</html>
