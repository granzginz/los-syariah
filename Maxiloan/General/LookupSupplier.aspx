﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="LookupSupplier.aspx.vb"
    Inherits="Maxiloan.Webform.LookupSupplier" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Lookup Supplier</title>
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function Close_Window() {
            window.close();
        }

        function Supplier_Checked(pSupplierID, pSupplierName, pPrivate) {
            with (document.forms['form1']) {
                hdnSupplierID.value = pSupplierID;
                hdnSupplierName.value = pSupplierName;
                hdnPrivate.value = pPrivate;
            }
        }

        function Select_Click() {
            with (document.forms['form1']) {                
                var lObjName = '<%= Request.QueryString("SupplierID")%>';
                if (eval('opener.document.forms[0].' + lObjName)) {
                    eval('opener.document.forms[0].' + lObjName).value = hdnSupplierID.value;
                }
                var lObjName = '<%= Request.QueryString("SupplierName")%>';
                if (eval('opener.document.forms[0].' + lObjName)) {
                    eval('opener.document.forms[0].' + lObjName).value = hdnSupplierName.value;
                }
                var lObjName = '<%= Request.QueryString("Private")%>';
                if (eval('opener.document.forms[0].' + lObjName)) {
                    eval('opener.document.forms[0].' + lObjName).value = hdnPrivate.value;
                }
            }

            window.close();
        }	
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label runat="server" ID="lblMessage"></asp:Label>
    <div class="form_box_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h4>
                DAFTAR SUPPLIER
            </h4>
        </div>
    </div>
    <asp:Panel ID="pnlSupplierList" runat="server" Visible="false">
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ws">
                    <asp:DataGrid ID="dtgSupplier" runat="server" CellPadding="0" OnSortCommand="SortGrid"
                        BorderStyle="None" DataKeyField="SupplierID" AutoGenerateColumns="False" AllowSorting="True"
                        AllowPaging="True" Visible="true" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="SELECT">
                                <HeaderStyle HorizontalAlign="Center" Width="5%" />
                                <ItemStyle HorizontalAlign="Center" Width="10%" />
                                <ItemTemplate>
                                    <%# get_radio(Container.DataItem("SupplierID"),Container.DataItem("SupplierName"),Container.DataItem("PF"))%>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="SupplierId" HeaderText="SUPPLIER ID" Visible="False">
                                <ItemTemplate>
                                    <asp:HyperLink ID="hynSupplier" runat="server" Text='<%#Container.DataItem("SupplierId")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="SupplierInitialName" HeaderText="SUPPLIER INITIAL NAME"
                                Visible="false">
                                <ItemTemplate>
                                    <asp:HyperLink ID="hynSupplierInitialName" runat="server" Text='<%#Container.DataItem("SupplierInitialName")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="suppliername" HeaderText="SUPPLIER NAME" ItemStyle-CssClass="name_col">
                                <ItemTemplate>
                                    <asp:Label ID="lblSupplierName" runat="server" Text='<%#Container.DataItem("SupplierName")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="address" HeaderText="SUPPLIER ADDRESS">
                                <ItemTemplate>
                                    <asp:Label ID="lblsupplieraddress" runat="server" Text='<%#Container.DataItem("Address")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="phone" HeaderText="KONTAK">
                                <ItemTemplate>
                                    <asp:Label ID="lblContactPerson1" runat="server" Text='<%#Container.DataItem("ContactPersonName")%>'>
                                    </asp:Label>
                                    <label class="label_auto">
                                        Telp.
                                    </label>
                                    <asp:Label ID="lblsupplierphone" runat="server" Text='<%#Container.DataItem("Phone")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="contactpersonname" HeaderText="CONTACT PERSON"
                                Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblContactPerson" runat="server" Text='<%#Container.DataItem("ContactPersonName")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="SUPPLIER ID" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblSupplierID" runat="server" Text='<%#Container.DataItem("SupplierId")%>'>
                                    </asp:Label>
                                    <asp:Label ID="lblNewAmountFee" runat="server" Text='<%#Container.DataItem("NewAmountFee")%>'>
                                    </asp:Label>
                                    <asp:Label ID="lblUsedAmountFee" runat="server" Text='<%#Container.DataItem("UsedAmountFee")%>'>
                                    </asp:Label>
                                    <asp:Label ID="lblPF" runat="server" Text='<%#Container.DataItem("PF")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                        <PagerStyle Visible="false" />
                    </asp:DataGrid>
                </div>
                <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" ImageUrl="../Images/grid_navbutton01.png"
                        OnCommand="NavigationLink_Click" CommandName="First"></asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" ImageUrl="../Images/grid_navbutton02.png"
                        OnCommand="NavigationLink_Click" CommandName="Prev"></asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" ImageUrl="../Images/grid_navbutton03.png"
                        OnCommand="NavigationLink_Click" CommandName="Next"></asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" ImageUrl="../Images/grid_navbutton04.png"
                        OnCommand="NavigationLink_Click" CommandName="Last"></asp:ImageButton>
                    Page
                    <asp:TextBox ID="txtGoPage" runat="server" Width="34px">1</asp:TextBox>
                    <asp:Button ID="btnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                        EnableViewState="False" />
                    <asp:RangeValidator ID="Rangevalidator1" runat="server" Font-Size="11px" Type="Integer"
                        MaximumValue="999999999" ErrorMessage="Page No. is not valid" MinimumValue="1"
                        ControlToValidate="txtGoPage" Display="Dynamic"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo" runat="server" Font-Size="11px" ErrorMessage="Page No. is not valid"
                        ControlToValidate="txtGoPage" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtGoPage" MinimumValue="1"
                        MaximumValue="999" ErrorMessage="Page No. is not valid!" Type="Double" Display="Dynamic"></asp:RangeValidator>
                </div>
                <div class="label_gridnavigation">
                    <asp:Label ID="lblPage" runat="server"></asp:Label>of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                    <asp:Label ID="lblTotRec" runat="server"></asp:Label>record(s)
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button runat="server" ID="buttonSelect" Text="Select" CssClass="small button blue" />
            <asp:Button runat="server" ID="buttonCancel" Text="Cancel" CssClass="small button gray"
                CausesValidation="false" />
            <a href="javascript:window.close();"></a>
        </div>
    </asp:Panel>
    <asp:Panel runat="server" ID="PanelSearch">
        <div class="form_box">
            <div class="form_single">
                <h4>
                    CARI SUPPLIER</h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cari Berdasarkan
                </label>
                <asp:DropDownList ID="cboSearchBy" runat="server">
                    <asp:ListItem Value="SupplierName">Supplier Name</asp:ListItem>
                    <asp:ListItem Value="SupplierInitialName">Supplier Initial Name</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearchBy" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSearch" runat="server" CausesValidation="False" Text="Find" CssClass="small button blue" />
            <asp:Button ID="btnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray" />
        </div>
    </asp:Panel>
    <input type="hidden" id="hdnSupplierID" name="hdnSupplierID" runat="server" />
    <input type="hidden" id="hdnSupplierName" name="hdnSupplierName" runat="server" />
    <input type="hidden" id="hdnPrivate" name="hdnPrivate" runat="server" />
    </form>
</body>
</html>
