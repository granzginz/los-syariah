﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="LookupCMO.aspx.vb" Inherits="Maxiloan.Webform.LookupCMO" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../Webform.UserController/UcSearchBy.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Lookup CMO</title>
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function Close_Window() {
            window.close();
        }

        function Lookup_Checked(pStr1, pStr2) {
            with (document.forms['form1']) {
                hdn1.value = pStr1;
                hdn2.value = pStr2;
            }
        }

        function Select_Click() {
            with (document.forms['form1']) {
                var lObjName = '<%= Request.QueryString("pStr1")%>';
                if (eval('opener.document.forms[0].' + lObjName)) {
                    eval('opener.document.forms[0].' + lObjName).value = hdn1.value;
                }
                var lObjName = '<%= Request.QueryString("pStr2")%>';
                if (eval('opener.document.forms[0].' + lObjName)) {
                    eval('opener.document.forms[0].' + lObjName).value = hdn2.value;
                }
            }

            window.close();
        }	
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h4>
                DAFTAR CMO
            </h4>
        </div>
    </div>
    <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgAO" runat="server" Width="100%" AllowSorting="True" OnSortCommand="SortGrid"
                    AutoGenerateColumns="False" DataKeyField="EmployeeID" CellPadding="0" CssClass="grid_general">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="SELECT">
                            <HeaderStyle HorizontalAlign="Center" Width="5%" />
                            <ItemStyle HorizontalAlign="Center" Width="10%" />
                            <ItemTemplate>
                                <%# get_radio(Container.DataItem("EmployeeID"), Container.DataItem("EmployeeName"))%>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="EmployeeID" HeaderText="CMO ID">
                            <ItemTemplate>
                                <asp:Label ID="lblEmployeeID" runat="server" Text='<%#Container.DataItem("EmployeeID")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="EmployeeName" HeaderText="EMPLOYEE NAME">
                            <ItemTemplate>
                                <asp:Label ID="lblEmployeeName" runat="server" Text='<%#Container.DataItem("EmployeeName")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <PagerStyle Visible="False"></PagerStyle>
                </asp:DataGrid>
            </div>
            <div class="button_gridnavigation">
                <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                    CommandName="First" ImageUrl="../Images/grid_navbutton01.png"></asp:ImageButton>
                <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                    CommandName="Prev" ImageUrl="../Images/grid_navbutton02.png"></asp:ImageButton>
                <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                    CommandName="Next" ImageUrl="../Images/grid_navbutton03.png"></asp:ImageButton>
                <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                    CommandName="Last" ImageUrl="../Images/grid_navbutton04.png"></asp:ImageButton>
                Page
                <asp:TextBox ID="txtPage" runat="server" Width="34px">1</asp:TextBox>
                <asp:Button ID="btnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                    EnableViewState="False" />
                <asp:RangeValidator ID="rgvGo" runat="server" font-name="Verdana" Font-Size="11px"
                    Type="Integer" MaximumValue="999999999" ErrorMessage="Page No. is not valid"
                    MinimumValue="1" ControlToValidate="txtPage"></asp:RangeValidator>
                <asp:RequiredFieldValidator ID="rfvGo" runat="server" font-name="Verdana" Font-Size="11px"
                    ErrorMessage="Page No. is not valid" ControlToValidate="txtPage" Display="Dynamic"
                    CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
            <div class="label_gridnavigation">
                <asp:Label ID="lblPage" runat="server"></asp:Label>of
                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                <asp:Label ID="lblRecord" runat="server"></asp:Label>record(s)
            </div>
        </div>
    </div>
    <div class="form_button">
        <asp:Button runat="server" ID="buttonSelect" Text="Select" CssClass="small button blue" />
        <asp:Button runat="server" ID="buttonCancel" Text="Cancel" CssClass="small button gray"
            CausesValidation="false" />
        <a href="javascript:window.close();"></a>
    </div>
    <div class="form_box_title">
        <div class="form_single">
            <h4>
                CARI CMO
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Cari Berdasarkan
            </label>
            <asp:DropDownList ID="cboSearchBy" runat="server">
                <asp:ListItem Value="EmployeeID">CMO ID</asp:ListItem>
                <asp:ListItem Value="EmployeeName" Selected="True">Employee Name</asp:ListItem>
            </asp:DropDownList>
            <asp:TextBox ID="txtSearchBy" runat="server"></asp:TextBox>
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="btnSearch" runat="server" CausesValidation="False" Text="Find" CssClass="small button blue" />
        <asp:Button ID="btnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray" />
    </div>
    <input type="hidden" id="hdn1" name="hdn1" runat="server" />
    <input type="hidden" id="hdn2" name="hdn2" runat="server" />
    </form>
</body>
</html>
