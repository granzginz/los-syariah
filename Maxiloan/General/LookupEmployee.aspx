﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="LookupEmployee.aspx.vb"
    Inherits="Maxiloan.Webform.LookupEmployee" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Lookup Employee</title>
    <%--<link href='../include/<%= request("style") %>.css' type="text/css" rel="stylesheet" />--%>
    <link href='../include/AccMnt.css' type="text/css" rel="stylesheet" />
    <link href='../include/Buttons.css' type="text/css" rel="stylesheet" />
    <script language="javascript" type="text/javascript">
        function Close_Window() {
            window.close();
        }
        function Employee_Checked(pStrID, pStrValue) {
            with (document.forms[0]) {
                hdnEmployeeID.value = pStrID;
                hdnEmployeeName.value = pStrValue;
            }
        }

        function Select_Click() {
            with (document.forms[0]) {
                var lObjName = '<%= Request.QueryString("employeeid")%>';
                if (eval('opener.document.forms[0].' + lObjName)) {
                    eval('opener.document.forms[0].' + lObjName).value = hdnEmployeeID.value;
                }
                var lObjName = '<%= Request.QueryString("employeename")%>';
                if (eval('opener.document.forms[0].' + lObjName)) {
                    eval('opener.document.forms[0].' + lObjName).value = hdnEmployeeName.value;
                }
            }
            window.close();
        }
        function click() {
            if (event.button == 2) {
                alert('You Are Not Authorize!');
            }
        }
        document.onmousedown = click
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" 
        ></asp:Label>
    <input id="hdnEmployeeID" type="hidden" name="hdnEmployeeID" runat="server" />
    <input id="hdnEmployeeName" type="hidden" name="hdnEmployeeName" runat="server" />
    <asp:Panel ID="pnlList" runat="server">
        <asp:Panel ID="pnlDtGrid" runat="server">
            <div class="form_box_title">
                <div class="form_single">
                    <h4>
                        EMPLOYEE LISTING
                    </h4>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ns">
                        <asp:DataGrid ID="dtgEmployee" runat="server" Width="100%" AllowPaging="True" AllowSorting="True"
                            AutoGenerateColumns="False" DataKeyField="employeeid" BorderColor="#CCCCCC" BorderStyle="None"
                            BorderWidth="1px" BackColor="White" OnSortCommand="SortGrid" CellPadding="0"
                            CssClass="grid_general">
                            <ItemStyle CssClass="item_grid"></ItemStyle>
                            <HeaderStyle CssClass="th"></HeaderStyle>
                            <Columns>
                                <asp:TemplateColumn HeaderText="SELECT">
                                    <HeaderStyle HorizontalAlign="Center" Width="5%"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center" Width="5%"></ItemStyle>
                                    <ItemTemplate>
                                        <%# Get_Radio(Container.DataItem("EmployeeId"),Container.DataItem("EmployeeName"))%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="employeeid" HeaderText="EMPLOYEE ID">
                                    <HeaderStyle  Height="30px" ></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Left" Width="15%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblemployeeid" runat="server" Text='<%#Container.DataItem("employeeid")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="employeename" HeaderText="EMPLOYEE NAME">
                                    <HeaderStyle  Height="30px" ></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Left" Width="15%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblemployeename" runat="server" Text='<%#Container.DataItem("employeename")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="employeeposition" HeaderText="POSITION">
                                    <HeaderStyle Height="30px" ></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Left" Width="15%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblemployee" runat="server" Text='<%#Container.DataItem("EmployeePosition")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                            <PagerStyle Visible="False" HorizontalAlign="Left" ForeColor="#000066" BackColor="White"
                                Mode="NumericPages"></PagerStyle>
                        </asp:DataGrid>
                    </div>
                    <div class="button_gridnavigation">
                        <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../Images/grid_navbutton01.png"
                            CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../Images/grid_navbutton02.png"
                            CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../Images/grid_navbutton03.png"
                            CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../Images/grid_navbutton04.png"
                            CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False" />
                        Page
                        <asp:TextBox ID="txtPage" runat="server" CssClass="small_text">1</asp:TextBox>
                        <asp:Button ID="btnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                            EnableViewState="False" />
                        <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" MinimumValue="1"
                            ErrorMessage="Page No. is not valid" MaximumValue="999999999" Type="Integer" CssClass="validator_general"
                             ></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                            ErrorMessage="Page No. is not valid" 
                             Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                    <div class="label_gridnavigation">
                        <asp:Label ID="lblPage" runat="server"></asp:Label>of
                        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                        <asp:Label ID="lblRecord" runat="server"></asp:Label>record(s)
                    </div>
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="btnSelect" runat="server" Text="Select" CssClass="small button blue" />
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="small button gray"
                    CausesValidation="False" /><a href="javascript:window.close();"></a>
            </div>
        </asp:Panel>
        <div class="form_box_title">
            <div class="form_single">
                <h4>
                    FIND</h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Employee ID
                </label>
                <asp:TextBox ID="txtEmployeeID_src" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Employee Name
                </label>
                <asp:TextBox ID="txtEmployeeName_src" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnsearch" runat="server" Text="Search" CssClass="small button blue"
                CausesValidation="false" />
        </div>
    </asp:Panel>
    </form>
</body>
</html>
