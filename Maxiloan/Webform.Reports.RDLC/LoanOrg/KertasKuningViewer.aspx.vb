﻿Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Exceptions
Imports Maxiloan.General
Imports Microsoft.Reporting.WebForms
Imports Maxiloan.Framework.SQLEngine

Public Class KertasKuningViewer
    Inherits WebBased
    Private Property oDataSet As DataSet
        Get
            Return CType(ViewState("oDataSet"), DataSet)
        End Get
        Set(value As DataSet)
            ViewState("oDataSet") = value
        End Set
    End Property
    Private Property ApplicationID As String
        Get
            Return CType(ViewState("ApplicationID"), String)
        End Get
        Set(value As String)
            ViewState("ApplicationID") = value
        End Set
    End Property
    Private Property isSPPL As Boolean
        Get
            Return CType(ViewState("isSPPL"), Boolean)
        End Get
        Set(value As Boolean)
            ViewState("isSPPL") = value
        End Set
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        If Not Page.IsPostBack Then
            Me.ApplicationID = Request("ApplicationID")
            Me.isSPPL = CBool(Request("isSPPL"))

            If isSPPL Then
                GenerateRDLCSPPL()
            Else
                GenerateRDLCRefund()
            End If

            Dim reportType As String = "pdf"
            Dim mimeType As String
            Dim encoding As String
            Dim fileNameExtension As String




            Dim deviceInfo As String = (Convert.ToString("<DeviceInfo>" + "  <OutputFormat>") & "pdf") + "</OutputFormat>" + "  <PageWidth>8.27in</PageWidth>" + "  <PageHeight>10.69in</PageHeight>" + "  <MarginTop>0.19685in</MarginTop>" + "  <MarginLeft>0.19685in</MarginLeft>" + "  <MarginRight>0.19685in</MarginRight>" + "  <MarginBottom>0.19685in</MarginBottom>" + "</DeviceInfo>"
            Dim warnings As Warning()
            Dim streams As String()
            Dim renderedBytes As Byte()
            renderedBytes = ReportViewer1.LocalReport.Render(reportType, deviceInfo, mimeType, encoding, fileNameExtension, streams, warnings)
            Response.Clear()

            Response.ContentType = "application/pdf"
            Response.AddHeader("Content-Disposition", "inline; filename=KertasKuningSPPL.pdf")
            Response.BinaryWrite(renderedBytes)

            Response.End()
        End If
    End Sub
    Private Sub GenerateRDLCRefund()
        Dim dtSet As New DataSet

        Dim reportPath As String
        reportPath = Request.ServerVariables("APPL_PHYSICAL_PATH") + "Webform.Reports.RDLC\LoanOrg\Refund.rdlc"

        dtSet = LoadDataset()

        Dim DS As ReportDataSource = New ReportDataSource("DataSet1", dtSet.Tables(0))
        '   Dim DS2 As ReportDataSource = New ReportDataSource("DataSet2", dtSet.Tables(1))

        ReportViewer1.LocalReport.DataSources.Clear()
        ReportViewer1.LocalReport.ReportPath = reportPath
        ReportViewer1.LocalReport.DataSources.Add(DS)
        ' ReportViewer1.LocalReport.DataSources.Add(DS2)



        AddHandler ReportViewer1.LocalReport.SubreportProcessing, AddressOf ReportViewer1_SubreportProcessing
        ReportViewer1.DataBind()
        ReportViewer1.LocalReport.Refresh()


    End Sub

    Public Sub ReportViewer1_SubreportProcessing(ByVal sender As Object, ByVal e As SubreportProcessingEventArgs)
        Dim Applicationid As String = e.Parameters(0).Values(0).ToString
        Dim SupplierEmployeeID As String = e.Parameters(1).Values(0).ToString

        Dim dtSet As New DataSet
        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(Me.GetConnectionString)


        Try
            Dim params() As SqlParameter = New SqlParameter(1) {}

            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            params(0) = New SqlParameter("@ApplicationID", SqlDbType.Char, 20)
            params(0).Value = Applicationid

            params(1) = New SqlParameter("@SupplierEmployeeID", SqlDbType.Char, 20)
            params(1).Value = SupplierEmployeeID

            dtSet = SqlHelper.ExecuteDataset(Me.GetConnectionString, CommandType.StoredProcedure, "spCetakPermintaanDanaRefundDetail", params)
            Dim DS As ReportDataSource = New ReportDataSource("DataSetDetail", dtSet.Tables(0))

            e.DataSources.Add(DS)

        Catch ex As Exception

        Finally
            objConnection.Close()
        End Try
    End Sub
    Private Sub GenerateRDLCSPPL()
        Dim dtSet As New DataSet

        Dim reportPath As String
        reportPath = Request.ServerVariables("APPL_PHYSICAL_PATH") + "Webform.Reports.RDLC\LoanOrg\KertasKuning.rdlc"

        dtSet = LoadDataset()

        Dim DS As ReportDataSource = New ReportDataSource("DataSet1", dtSet.Tables(0))
        Dim DS2 As ReportDataSource = New ReportDataSource("DataSet2", dtSet.Tables(1))
        Dim DS3 As ReportDataSource = New ReportDataSource("DataSet3", dtSet.Tables(2))

        ReportViewer1.LocalReport.DataSources.Clear()
        ReportViewer1.LocalReport.ReportPath = reportPath
        ReportViewer1.LocalReport.DataSources.Add(DS)
        ReportViewer1.LocalReport.DataSources.Add(DS2)
        ReportViewer1.LocalReport.DataSources.Add(DS3)
        ReportViewer1.LocalReport.Refresh()
    End Sub
    Private Function LoadDataset() As DataSet
        Dim dtSet As New DataSet
        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(Me.GetConnectionString)
        Dim sp As String

        If Me.isSPPL Then
            sp = "spCetakPermintaanDanaSPPL"
        Else
            sp = "spCetakPermintaanDanaRefund"
        End If

        Try
            Dim params() As SqlParameter = New SqlParameter(0) {}

            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            params(0).Value = Me.ApplicationID

            dtSet = SqlHelper.ExecuteDataset(Me.GetConnectionString, CommandType.StoredProcedure, sp, params)

            Return dtSet
        Catch ex As Exception
        Finally
            objConnection.Close()
        End Try

    End Function
End Class