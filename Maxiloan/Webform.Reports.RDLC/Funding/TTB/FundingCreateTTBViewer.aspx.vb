﻿Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Exceptions
Imports Maxiloan.General
Imports Microsoft.Reporting.WebForms
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Webform.UserController
Public Class FundingCreateTTBViewer
    Inherits WebBased

    Private BatchNo, ContractNo, FundingCoyID As String


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Me.BatchNo = Request("FundingBatchNo")
            Me.ContractNo = Request("FundingContractId")
            Me.FundingCoyID = Request("CompanyID")

            GenerateRDLC()
            Dim reportType As String = "pdf"
            Dim mimeType As String
            Dim encoding As String
            Dim fileNameExtension As String

            Dim deviceInfo As String = (Convert.ToString("<DeviceInfo>" + "  <OutputFormat>") & "pdf") + "</OutputFormat>" + "  <PageWidth>11.69in</PageWidth>" + "  <PageHeight>8.27in</PageHeight>" + "  <MarginTop>0.19685in</MarginTop>" + "  <MarginLeft>0.19685in</MarginLeft>" + "  <MarginRight>0in</MarginRight>" + "  <MarginBottom>0.19685in</MarginBottom>" + "</DeviceInfo>"
            Dim warnings As Warning()
            Dim streams As String()
            Dim renderedBytes As Byte()
            renderedBytes = ReportViewer1.LocalReport.Render(reportType, deviceInfo, mimeType, encoding, fileNameExtension, streams, warnings)
            Response.Clear()

            Response.ContentType = "application/pdf"
            Response.AddHeader("Content-Disposition", "inline; filename=FundingJatuhTempo.pdf")
            Response.BinaryWrite(renderedBytes)

            Response.End()
        End If
    End Sub
    Private Sub GenerateRDLC()
        Dim dtSet As New DataSet
        Dim reportPath As String
        reportPath = Request.ServerVariables("APPL_PHYSICAL_PATH") + "Webform.Reports.RDLC\Funding\TTB\rptFundingTTB.rdlc"

        dtSet = LoadDatasetfund()

        Dim DS As ReportDataSource = New ReportDataSource("DataSet1", dtSet.Tables(0))


        ReportViewer1.LocalReport.DataSources.Clear()
        ReportViewer1.LocalReport.ReportPath = reportPath
        ReportViewer1.LocalReport.DataSources.Add(DS)
        ReportViewer1.LocalReport.Refresh()
    End Sub
    Private Function LoadDatasetfund() As DataSet
        Dim dtSet As New DataSet
        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(Me.GetConnectionString)
        Dim sp As String
        sp = "spDaftarKontrakPerBatch"

        Try
            Dim params() As SqlParameter = New SqlParameter(2) {}

            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            params(0) = New SqlParameter("@FundingCoyID", SqlDbType.VarChar, 20)
            params(0).Value = Me.FundingCoyID

            params(1) = New SqlParameter("@FundingContractNo", SqlDbType.VarChar, 20)
            params(1).Value = Me.ContractNo

            params(2) = New SqlParameter("@FundingBatchNo", SqlDbType.VarChar, 20)
            params(2).Value = Me.BatchNo


            dtSet = SqlHelper.ExecuteDataset(Me.GetConnectionString, CommandType.StoredProcedure, sp, params)

            Return dtSet
        Catch ex As Exception
        Finally
            objConnection.Close()
        End Try

    End Function
End Class