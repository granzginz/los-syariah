﻿Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Exceptions
Imports Maxiloan.General
Imports Microsoft.Reporting.WebForms
Imports Maxiloan.Framework.SQLEngine
Imports Maxiloan.Webform.UserController

Public Class FundingJatuhTempoViewer
    Inherits WebBased

#Region "Property"
    Private Property StartDate As DateTime
        Get
            Return ViewState("StartDate")
        End Get
        Set(value As DateTime)
            ViewState("StartDate") = value
        End Set
    End Property
    Private Property EndDate As DateTime
        Get
            Return ViewState("EndDate")
        End Get
        Set(value As DateTime)
            ViewState("EndDate") = value
        End Set
    End Property
    Private Property NoBatch As String
        Get
            Return CType(ViewState("NoBatch"), String)
        End Get
        Set(value As String)
            ViewState("NoBatch") = value
        End Set
    End Property
    Private Property Bank As String
        Get
            Return CType(ViewState("Bank"), String)
        End Get
        Set(value As String)
            ViewState("Bank") = value
        End Set
    End Property
    Private Property Kontrak As String
        Get
            Return CType(ViewState("Kontrak"), String)
        End Get
        Set(value As String)
            ViewState("Kontrak") = value
        End Set
    End Property
    Private Property InsSeqno As String
        Get
            Return CType(ViewState("InsSeqno"), String)
        End Get
        Set(value As String)
            ViewState("InsSeqno") = value
        End Set
    End Property

    Private Property ContractNo As String
        Get
            Return CType(ViewState("ContractNo"), String)
        End Get
        Set(value As String)
            ViewState("ContractNo") = value
        End Set
    End Property
    Private Property _listData As DataTable
    Private Property ListData() As DataTable
        Get
            Return _listData
        End Get
        Set(value As DataTable)
            _listData = value
        End Set
    End Property

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Page.IsPostBack Then
            Dim oCustomClass As New Parameter.FundingContract

            Dim cookie As HttpCookie = Request.Cookies("FUNDCOOKIE")

            'Me.StartDate = Request("StartDate")
            'Me.EndDate = Request("EndDate")
            'Me.NoBatch = Request("NoBatch")
            'Me.Bank = Request("Bank")
            'Me.Kontrak = Request("Kontrak")
            'Me.InsSeqno = Request("InsSeqno")
            'Me.ContractNo = Request("ContractNo")


            Me.StartDate = cookie.Values("StartDate")
            Me.EndDate = cookie.Values("EndDate")
            Me.NoBatch = cookie.Values("NoBatch")
            Me.Bank = cookie.Values("BankCompany")
            Me.Kontrak = cookie.Values("Kontrak")
            Me.InsSeqno = cookie.Values("InsSeqno")
            Me.ContractNo = cookie.Values("FundingContractNo")

            cookie.Expires = DateTime.Now.AddDays(-1)

            'cookie.Values.Remove("RSCOOKIES")
            'cookie.Expires = DateTime.Now.AddMilliseconds(1)

            GenerateRDLCFundJT()

            'kalo mau di export ke pdf remark nya diilangin
            'Dim reportType As String = "pdf"
            'Dim mimeType As String
            'Dim encoding As String
            'Dim fileNameExtension As String

            'Dim deviceInfo As String = (Convert.ToString("<DeviceInfo>" + "  <OutputFormat>") & "pdf") + "</OutputFormat>" + "  <PageWidth>11.69in</PageWidth>" + "  <PageHeight>8.27in</PageHeight>" + "  <MarginTop>0.19685in</MarginTop>" + "  <MarginLeft>0.19685in</MarginLeft>" + "  <MarginRight>0in</MarginRight>" + "  <MarginBottom>0.19685in</MarginBottom>" + "</DeviceInfo>"
            'Dim warnings As Warning()
            'Dim streams As String()
            'Dim renderedBytes As Byte()
            'renderedBytes = ReportViewer1.LocalReport.Render(reportType, deviceInfo, mimeType, encoding, fileNameExtension, streams, warnings)
            'Response.Clear()

            'Response.ContentType = "application/pdf"
            'Response.AddHeader("Content-Disposition", "inline; filename=FundingJatuhTempo.pdf")
            'Response.BinaryWrite(renderedBytes)

            'Response.End()
        End If
    End Sub

    Private Sub GenerateRDLCFundJT()
        Dim dtSet As New DataSet
        Dim reportPath As String
        reportPath = Request.ServerVariables("APPL_PHYSICAL_PATH") + "Webform.Reports.RDLC\Funding\rptFundingJatuhTempo.rdlc"

        dtSet = LoadDatasetfund()

        Dim DS As ReportDataSource = New ReportDataSource("DataSet2", dtSet.Tables(0))
        'Dim DS2 As ReportDataSource = New ReportDataSource("DataSet2", dtSet.Tables(1))

        ReportViewer1.LocalReport.DataSources.Clear()
        ReportViewer1.LocalReport.ReportPath = reportPath
        ReportViewer1.LocalReport.DataSources.Add(DS)
        'ReportViewer1.LocalReport.DataSources.Add(DS2)
        ReportViewer1.LocalReport.Refresh()
    End Sub

    Private Function LoadDatasetfund() As DataSet
        Dim dtSet As New DataSet
        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(Me.GetConnectionString)
        Dim sp As String
        sp = "spDaftarAngsuranPerJatuhTempo2"

        Try
            Dim params() As SqlParameter = New SqlParameter(6) {}

            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            params(0) = New SqlParameter("@StartDate", SqlDbType.DateTime)
            params(0).Value = Me.StartDate

            params(1) = New SqlParameter("@EndDate", SqlDbType.DateTime)
            params(1).Value = Me.EndDate

            params(2) = New SqlParameter("@NoBatch", SqlDbType.VarChar, 2000)
            params(2).Value = Me.NoBatch

            params(3) = New SqlParameter("@Bank", SqlDbType.VarChar, 2000)
            params(3).Value = Me.Bank

            params(4) = New SqlParameter("@Kontrak", SqlDbType.VarChar, -1)
            params(4).Value = Me.Kontrak

            params(5) = New SqlParameter("@InsSeqno", SqlDbType.VarChar, 2000)
            params(5).Value = Me.InsSeqno

            params(6) = New SqlParameter("@ContractNo", SqlDbType.VarChar, 2000)
            params(6).Value = Me.ContractNo

            dtSet = SqlHelper.ExecuteDataset(Me.GetConnectionString, CommandType.StoredProcedure, sp, params)

            Return dtSet
        Catch ex As Exception
        Finally
            objConnection.Close()
        End Try

    End Function
End Class
