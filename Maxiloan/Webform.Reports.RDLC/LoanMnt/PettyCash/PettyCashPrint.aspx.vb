﻿Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Exceptions
Imports Maxiloan.General
Imports Microsoft.Reporting.WebForms
Imports Maxiloan.Framework.SQLEngine
Public Class PettyCashPrint
    Inherits WebBased
    Private Property oDataSet As DataSet
        Get
            Return CType(ViewState("oDataSet"), DataSet)
        End Get
        Set(value As DataSet)
            ViewState("oDataSet") = value
        End Set
    End Property
    Private Property PettyCashNo As String
        Get
            Return CType(ViewState("PettyCashNo"), String)
        End Get
        Set(value As String)
            ViewState("PettyCashNo") = value
        End Set
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        If Not Page.IsPostBack Then
            Me.PettyCashNo = Request("PettyCashNo")

            GenerateRDLC()

            Dim reportType As String = "pdf"
            Dim mimeType As String
            Dim encoding As String
            Dim fileNameExtension As String




            Dim deviceInfo As String = (Convert.ToString("<DeviceInfo>" + "  <OutputFormat>") & "pdf") + "</OutputFormat>" + "  <PageWidth>8.5in</PageWidth>" + "  <PageHeight>14in</PageHeight>" + "  <MarginTop>0.19685in</MarginTop>" + "  <MarginLeft>0.19685in</MarginLeft>" + "  <MarginRight>0.19685in</MarginRight>" + "  <MarginBottom>0.19685in</MarginBottom>" + "</DeviceInfo>"
            Dim warnings As Warning()
            Dim streams As String()
            Dim renderedBytes As Byte()
            renderedBytes = ReportViewer1.LocalReport.Render(reportType, deviceInfo, mimeType, encoding, fileNameExtension, streams, warnings)
            Response.Clear()

            Response.ContentType = "application/pdf"
            Response.AddHeader("Content-Disposition", "inline; filename=PettyCash-" & Me.PettyCashNo.Trim & ".pdf")
            Response.BinaryWrite(renderedBytes)

            Response.End()
        End If
    End Sub
    Private Sub GenerateRDLC()
        Dim dtSet As New DataSet
        Dim dtTable As New DataTable
        Dim dtRow As DataRow

        Dim reportPath As String
        reportPath = Request.ServerVariables("APPL_PHYSICAL_PATH") + "Webform.Reports.RDLC\LoanMnt\PettyCash\PettyCashTransaction.rdlc"

        dtSet = LoadDataset()

        dtTable = dtSet.Tables(1)

        If dtTable.Rows.Count <= 10 Then
            For i As Integer = 1 To 10 - dtTable.Rows.Count
                dtRow = dtTable.NewRow
                dtTable.Rows.Add(dtRow)
            Next
        End If

        Dim DS As ReportDataSource = New ReportDataSource("DataSet1", dtSet.Tables(0))
        Dim DS2 As ReportDataSource = New ReportDataSource("DataSet2", dtTable)

        ReportViewer1.LocalReport.DataSources.Clear()
        ReportViewer1.LocalReport.ReportPath = reportPath
        ReportViewer1.LocalReport.DataSources.Add(DS)
        ReportViewer1.LocalReport.DataSources.Add(DS2)



        ReportViewer1.DataBind()
        ReportViewer1.LocalReport.Refresh()


    End Sub



    Private Function LoadDataset() As DataSet
        Dim dtSet As New DataSet
        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(Me.GetConnectionString)
        Dim sp As String

        sp = "spCetakPettyCashTransaction"

        Try
            Dim params() As SqlParameter = New SqlParameter(0) {}

            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            params(0) = New SqlParameter("@PettyCashNo", SqlDbType.VarChar, 20)
            params(0).Value = Me.PettyCashNo

            dtSet = SqlHelper.ExecuteDataset(Me.GetConnectionString, CommandType.StoredProcedure, sp, params)

            Return dtSet
        Catch ex As Exception
        Finally
            objConnection.Close()
        End Try

    End Function

End Class