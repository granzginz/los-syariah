﻿Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Exceptions
Imports Maxiloan.General
Imports Microsoft.Reporting.WebForms
Imports Maxiloan.Framework.SQLEngine
Public Class TandaTerimaBPKBViewer
    Inherits WebBased
    Private Property oDataSet As DataSet
        Get
            Return CType(ViewState("oDataSet"), DataSet)
        End Get
        Set(value As DataSet)
            ViewState("oDataSet") = value
        End Set
    End Property
    Private Property ApplicationID As String
        Get
            Return CType(ViewState("ApplicationID"), String)
        End Get
        Set(value As String)
            ViewState("ApplicationID") = value
        End Set
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim dtSet As New DataSet

        If Not Page.IsPostBack Then
            Me.ApplicationID = Request("ApplicationID")

            Dim reportPath As String
            reportPath = Request.ServerVariables("APPL_PHYSICAL_PATH") + "Webform.Reports.RDLC\CollateralMgt\TandaTerimaBPKB.rdlc"

            dtSet = LoadDataset()

            Dim DS As ReportDataSource = New ReportDataSource("DataSet1", dtSet.Tables(0))

            ReportViewer1.LocalReport.DataSources.Clear()
            ReportViewer1.LocalReport.ReportPath = reportPath
            ReportViewer1.LocalReport.DataSources.Add(DS)
            ReportViewer1.LocalReport.Refresh()
            Dim reportType As String = "pdf"
            Dim mimeType As String
            Dim encoding As String
            Dim fileNameExtension As String



            'Dim deviceInfo As String = "<DeviceInfo><PageHeight>8.5in</PageHeight><PageWidth>11in</PageWidth></DeviceInfo>"
            Dim deviceInfo As String = (Convert.ToString("<DeviceInfo>" + "  <OutputFormat>") & "pdf") + "</OutputFormat>" + "  <PageWidth>8.27in</PageWidth>" + "  <PageHeight>10.69in</PageHeight>" + "  <MarginTop>0.19685in</MarginTop>" + "  <MarginLeft>0.19685in</MarginLeft>" + "  <MarginRight>0.19685in</MarginRight>" + "  <MarginBottom>0.19685in</MarginBottom>" + "</DeviceInfo>"
            Dim warnings As Warning()
            Dim streams As String()
            Dim renderedBytes As Byte()
            renderedBytes = ReportViewer1.LocalReport.Render(reportType, deviceInfo, mimeType, encoding, fileNameExtension, streams, warnings)
            Response.Clear()

            Response.ContentType = "application/pdf"
            Response.AddHeader("Content-Disposition", "inline; filename=TandaTerimaBPKB.pdf")
            Response.BinaryWrite(renderedBytes)

            Response.End()
        End If
    End Sub
    Private Function LoadDataset() As DataSet
        Dim dtSet As New DataSet
        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(Me.GetConnectionString)
        Try
            Dim params() As SqlParameter = New SqlParameter(0) {}

            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            params(0) = New SqlParameter("@ApplicationID", SqlDbType.VarChar, 20)
            params(0).Value = Me.ApplicationID

            dtSet = SqlHelper.ExecuteDataset(Me.GetConnectionString, CommandType.StoredProcedure, "spPrintTerimaBPKB", params)

            Return dtSet
        Catch ex As Exception
        Finally
            objConnection.Close()
        End Try

    End Function

End Class