﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SupplierEmployeeMasterTransaksi.aspx.vb"
    Inherits="Maxiloan.Webform.SalesMarketing.SupplierEmployeeMasterTransaksi" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register TagPrefix="uc1" TagName="UcBankAccount" Src="../Webform.UserController/UcBankAccount.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UCMasterTransaksi" Src="../Webform.UserController/UCMasterTransaksi.ascx" %>
<%@ Register TagPrefix="uc2" TagName="ucNumberFormat" Src="../Webform.UserController/ucNumberFormat.ascx" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>SupplierEmployeeMasterTransaksi</title>
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />    
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Panel ID="pnlTitle" runat="server">
            <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>
                        <asp:Label ID="lblTitle" runat="server"></asp:Label>
                    </h3>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label class="label_req">
                        Nama Karyawan
                    </label>
                    <asp:TextBox ID="txtEmpName" runat="server" MaxLength="50"></asp:TextBox>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Jabatan
                    </label>
                    <asp:DropDownList ID="cboEmpPosition" runat="server">
                        <%--<asp:ListItem Value="CO">Supplier Office</asp:ListItem>--%>
                        <asp:ListItem Value="GM">General Manager</asp:ListItem>
                        <asp:ListItem Value="OM">Operation Manager</asp:ListItem>
                        <asp:ListItem Value="BM">Branch Manager</asp:ListItem>
                        <%--<asp:ListItem Value="AH">Administration Head</asp:ListItem>--%>
                        <asp:ListItem Value="SV">Sales Supervisor</asp:ListItem>
                        <asp:ListItem Value="SL">Sales Person</asp:ListItem>
                        <asp:ListItem Value="AM">Supplier Admin</asp:ListItem>                        
                        <asp:ListItem Value="FI">F & I</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <%--<div class="form_box">
                <div class="form_single">
                    <label>
                        Status</label>
                    <asp:CheckBox ID="chkActive" runat="server" Text="Active"></asp:CheckBox>
                </div>
            </div>--%>
        </asp:Panel>
        <asp:Panel ID="pnlList" runat="server">
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ns">
                        <asp:DataGrid ID="dtgPaging" runat="server" Width="100%" AllowSorting="True" DataKeyField="SupplierEmployeeID"
                            AutoGenerateColumns="False" OnSortCommand="Sorting" BorderStyle="None" BorderWidth="0"
                            CssClass="grid_general">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <Columns>
                                <asp:TemplateColumn HeaderText="EDIT">
                                    <ItemStyle CssClass="command_col"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" ImageUrl="../Images/iconedit.gif"
                                            CommandName="Edit"></asp:ImageButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="DELETE">
                                    <ItemStyle CssClass="command_col"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgDelete" runat="server" CausesValidation="False" ImageUrl="../Images/icondelete.gif"
                                            CommandName="Delete"></asp:ImageButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn Visible="False" DataField="IsActive"></asp:BoundColumn>
                                <asp:TemplateColumn HeaderText="SupplierID" Visible="false">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkSupplierID" runat="server" Text='<%# DataBinder.eval(Container,"DataItem.SupplierID") %>'>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="SupplierEmployeeID" Visible="false">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkSupplierEmployeeID" runat="server" Text='<%# DataBinder.eval(Container,"DataItem.SupplierEmployeeID") %>'>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="NAMA" Visible="false" SortExpression="SupplierEmployeeAccountName">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lblSupplierEmployeeAccountName" CausesValidation="False" runat="server"
                                            Text='<%# DataBinder.eval(Container,"DataItem.SupplierEmployeeAccountName") %>'>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="SupplierEmployeePosition" Visible="false" SortExpression="SupplierEmployeePosition">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSupplierEmployeePosition" CausesValidation="False" runat="server"
                                            Text='<%# DataBinder.eval(Container,"DataItem.SupplierEmployeePosition") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="SupplierEmployeePositionDesc" Visible="false" SortExpression="SupplierEmployeePositionDesc">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSupplierEmployeePositionDesc" CausesValidation="False" runat="server"
                                            Text='<%# DataBinder.eval(Container,"DataItem.SupplierEmployeePositionDesc") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Master Transaksi ID" SortExpression="TransID">
                                    <ItemTemplate>
                                        <asp:Label ID="lblTransIDBaru" CausesValidation="False" runat="server"
                                            Text='<%# DataBinder.eval(Container,"DataItem.TransID") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="MASTER TRANSAKSI" SortExpression="TransDesc">
                                    <ItemTemplate>
                                        <asp:Label ID="lblTransDesc" CausesValidation="False" runat="server" Text='<%# DataBinder.eval(Container,"DataItem.TransDesc") %>'></asp:Label>
                                        <asp:Label ID="lblTransID" Visible="false" CausesValidation="False" runat="server"
                                            Text='<%# DataBinder.eval(Container,"DataItem.TransID") %>'></asp:Label>
                                        <asp:Label ID="lblTransIDheader" Visible="false" CausesValidation="False" runat="server"
                                            Text='<%# DataBinder.eval(Container,"DataItem.TransID") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Persentase" SortExpression="PersenAlokasi" HeaderStyle-CssClass="th_right"
                                    ItemStyle-CssClass="item_grid_right">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPersenAlokasi" runat="server" Text='<%# DataBinder.eval(Container,"DataItem.PersenAlokasi") %>'></asp:Label>
                                        <asp:Label ID="lblPersenAlokasiheader" Visible="false" runat="server" Text='<%# DataBinder.eval(Container,"DataItem.PersenAlokasi") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Nilai" SortExpression=" NilaiAlokasi" HeaderStyle-CssClass="th_right"
                                    ItemStyle-CssClass="item_grid_right">
                                    <ItemTemplate>
                                        <asp:Label ID="lblNilaiAlokasi" runat="server" Text='<%# formatnumber(DataBinder.eval(Container,"DataItem.NilaiAlokasi"),0) %>'></asp:Label>
                                        <asp:Label ID="lblNilaiAlokasiheader" Visible="false" runat="server" Text='<%# DataBinder.eval(Container,"DataItem.NilaiAlokasi") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="SupplierCompanyName" SortExpression=" SupplierCompanyName"
                                    Visible="false" HeaderStyle-CssClass="th_right" ItemStyle-CssClass="item_grid_right">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSupplierCompanyName" Visible="false" runat="server" Text='<%# DataBinder.eval(Container,"DataItem.SupplierCompanyName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                        <div class="button_gridnavigation">
                            <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../Images/grid_navbutton01.png"
                                CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../Images/grid_navbutton02.png"
                                CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../Images/grid_navbutton03.png"
                                CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../Images/grid_navbutton04.png"
                                CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                            </asp:ImageButton>
                            <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                            <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                                EnableViewState="False"></asp:Button>
                            <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" MinimumValue="1"
                                ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"></asp:RangeValidator>
                            <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                                ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        </div>
                        <div class="label_gridnavigation">
                            <asp:Label ID="lblPage" runat="server"></asp:Label>of
                            <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                            <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                        </div>
                    </div>
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="btnAdd" runat="server" Text="Add" CssClass="small button blue" CausesValidation="False">
                </asp:Button>
                <asp:Button ID="btnPrint" runat="server" Enabled="true" Text="Print" CssClass="small button blue">
                </asp:Button>
                <asp:Button ID="btnBack" runat="server" CausesValidation="False" Text="Back" CssClass="small button gray">
                </asp:Button>
            </div>
        </asp:Panel>
    </div>
    <asp:Panel ID="pnlAddEdit" runat="server">
        <div class="form_box_header">
            <div class="form_single">
                <h4>
                    Transaksi
                </h4>
            </div>
        </div>
        <div class="form_box_uc">
            <uc1:ucmastertransaksi id="UCMasterTransaksi" runat="server"></uc1:ucmastertransaksi>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Persentase</label>
                <uc2:ucnumberformat id="txtPersentase" runat="server" />
                %
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Nilai</label>
                <uc2:ucnumberformat id="txtNilai" runat="server" />
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonSave" runat="server" Text="Save" CausesValidation="False" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="ButtonCancel" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
