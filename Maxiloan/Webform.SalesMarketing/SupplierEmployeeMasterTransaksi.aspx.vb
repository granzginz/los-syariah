﻿#Region "Imports"
Imports System.IO
'Imports Maxiloan.cbse
'Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class SupplierEmployeeMasterTransaksi
    Inherits Maxiloan.Webform.WebBased

    Protected WithEvents UCMasterTransaksi As UCMasterTransaksi
    Protected WithEvents txtPersentase As ucNumberFormat
    Protected WithEvents txtNilai As ucNumberFormat

#Region "Property"
    Private Property AddEdit() As String
        Get
            Return CType(ViewState("AddEdit"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("AddEdit") = Value
        End Set
    End Property

    Private Property SupplierCompanyName() As String
        Get
            Return CType(ViewState("SupplierCompanyName"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("SupplierCompanyName") = Value
        End Set
    End Property
    Private Property SupplierEmployeeID() As String
        Get
            Return CType(ViewState("SupplierEmployeeID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("SupplierEmployeeID") = Value
        End Set
    End Property
    Private Property SupplierID() As String
        Get
            Return CType(ViewState("SupplierID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("SupplierID") = Value
        End Set
    End Property
    Private Property EmployeeID() As String
        Get
            Return CType(ViewState("EmployeeID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("EmployeeID") = Value
        End Set
    End Property
    Private Property TransIDx() As String
        Get
            Return CType(ViewState("TransIDx"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("TransIDx") = Value
        End Set
    End Property
    Private Property TransIDxn() As String
        Get
            Return CType(ViewState("TransIDxn"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("TransIDxn") = Value
        End Set
    End Property

    Private Property NilaiAlokasix() As Decimal
        Get
            Return CType(ViewState("NilaiAlokasix"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            ViewState("NilaiAlokasix") = Value
        End Set
    End Property
    Private Property PersenAlokasix() As Decimal
        Get
            Return CType(ViewState("PersenAlokasix"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            ViewState("PersenAlokasix") = Value
        End Set
    End Property

    Private Property SupplierEmployeePosition() As String
        Get
            Return CType(ViewState("SupplierEmployeePosition"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("SupplierEmployeePosition") = Value
        End Set
    End Property
    Private Property isActive() As Boolean
        Get
            Return CType(ViewState("isActive"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            ViewState("isActive") = Value
        End Set
    End Property


    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property WhereCond2() As String
        Get
            Return CType(ViewState("WhereCond2"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("WhereCond2") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property
    Private Property Name() As String
        Get
            Return CType(ViewState("Name"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Name") = Value
        End Set
    End Property

    Private Property SupplierEmployeeName() As String
        Get
            Return CType(ViewState("SupplierEmployeeName"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("SupplierEmployeeName") = Value
        End Set
    End Property
    Private Property SupplierEmployeePositionID() As String
        Get
            Return CType(ViewState("SupplierEmployeePositionID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("SupplierEmployeePositionID") = Value
        End Set
    End Property
#End Region

#Region "Constanta"
    Private m_controller As New SupplierController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Dim oRow As DataRow
#End Region
#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGrid(Me.CmdWhere)
    End Sub
    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int32)
                BindGrid(Me.CmdWhere)
            End If
        End If
    End Sub
#End Region
#Region "Sorting"

    Private Sub dtgPaging_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand

        Me.TransIDx = CType(e.Item.FindControl("lblTransID"), Label).Text.Trim

        If e.CommandName = "Edit" Then

            Me.AddEdit = "Edit"
            disable()
            lblTitle.Text = "KARYAWAN SUPPLIER - DETAIL TRANSAKSI"

            If CType(e.Item.FindControl("lblNilaiAlokasi"), Label).Text.Trim = "" Then
                Me.NilaiAlokasix = 0
            Else
                Me.NilaiAlokasix = CDbl(CType(e.Item.FindControl("lblNilaiAlokasi"), Label).Text)
            End If

            If CType(e.Item.FindControl("lblPersenAlokasiheader"), Label).Text.Trim = "" Then
                Me.PersenAlokasix = 0
            Else
                Me.PersenAlokasix = CDbl(CType(e.Item.FindControl("lblPersenAlokasiheader"), Label).Text)
            End If


            BindEdit(Me.SupplierID.Trim, Me.SupplierEmployeeID.Trim, Me.Name.Trim, Me.SupplierEmployeePositionID.Trim, _
                     TransIDx.Trim, Me.NilaiAlokasix.ToString, Me.PersenAlokasix.ToString)

        ElseIf e.CommandName = "Delete" Then
            If SessionInvalid() Then
                Exit Sub
            End If
            Dim oSupplierEmployee As New Parameter.Supplier
            With oSupplierEmployee
                .FormID = "SeMasterTransaksi"
                .strConnection = GetConnectionString()
                .SupplierID = Me.SupplierID
                .ID = CType(e.Item.FindControl("lnkSupplierEmployeeID"), LinkButton).Text.Trim
                .TransID = CType(e.Item.FindControl("lblTransID"), Label).Text.Trim
            End With

            Dim err As String
            err = m_controller.SupplierEmployeeDelete(oSupplierEmployee)
            If err = "" Then
                InitialPanel()
                BindGrid(Me.CmdWhere)
            Else
                ShowMessage(lblMessage, err, True)
            End If
        End If
    End Sub
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGrid(Me.CmdWhere)
    End Sub
#End Region
#Region "BindEdit"
    Sub BindEdit(ByVal SupplierID As String, ByVal SupplierEmployeeID As String, ByVal Name As String, ByVal SupplierEmployeePosition As String, ByVal TransIDx As String, ByVal NilaiAlokasix As Decimal, ByVal PersenAlokasix As Decimal)

        Dim oData As New DataTable
        Dim oEntity As New Parameter.Supplier

        pnlList.Visible = False
        pnlTitle.Visible = True
        pnlAddEdit.Visible = True

        txtEmpName.Text = Name
        cboEmpPosition.ClearSelection()
        cboEmpPosition.Items.FindByValue(Me.SupplierEmployeePositionID).Selected = True

        txtNilai.Text = FormatNumber(NilaiAlokasix, 0)
        txtPersentase.Text = FormatNumber(PersenAlokasix, 0)
        UCMasterTransaksi.TransID = TransIDx
        '   chkActive.Checked = IsActive
    End Sub
#End Region
    Sub BindAdd()
        pnlList.Visible = False
        pnlTitle.Visible = True
        pnlAddEdit.Visible = True

    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not Me.IsPostBack Then
            Me.FormID = "SeMasterTransaksi"
            If Request("cond") <> "" Then
                Me.CmdWhere = Request("cond")
            Else
                Me.CmdWhere = "ALL"
            End If

            Me.SupplierEmployeeID = Request("id").Trim
            Me.Name = Request("name").Trim
            Me.SupplierID = Request("SupplierID").Trim
            Me.SupplierEmployeeName = Request("SupplierEmployeeName").Trim
            Me.SupplierEmployeePositionID = Request("SupplierEmployeePositionID").Trim
            Me.WhereCond2 = "SEDT.SupplierID='" & Me.SupplierID & "' AND  SEDT.SupplierEmployeeID = '" & Me.SupplierEmployeeID & "'"
            Me.Sort = "SupplierEmployeeID ASC"

            InitialPanel()
            disable()
            UCMasterTransaksi.cboTransID()
            BindGrid(Me.CmdWhere)
            txtEmpName.Text = Me.SupplierEmployeeName
            cboEmpPosition.SelectedValue = Me.SupplierEmployeePositionID
        End If
    End Sub
    Sub BindGrid(ByVal cmdWhere As String)
        Dim dtEntity As DataTable
        Dim oCustomClass As New Parameter.Supplier
        With oCustomClass
            .FormID = "SeMasterTransaksi"
            .PageSize = pageSize
            .WhereCond = cmdWhere
            .SortBy = Me.Sort
            .WhereCond2 = Me.WhereCond2
            .CurrentPage = currentPage
            .strConnection = GetConnectionString()
        End With
        oCustomClass = m_controller.GetSupplierEmployee(oCustomClass)
        If Not oCustomClass Is Nothing Then
            dtEntity = oCustomClass.ListData
            recordCount = oCustomClass.TotalRecords
        Else
            recordCount = 0
        End If
        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()
        PagingFooter()


    End Sub
    Sub InitialPanel()
        pnlTitle.Visible = True
        pnlList.Visible = True
        pnlAddEdit.Visible = False
        txtPage.Text = "1"
        lblTitle.Text = " DAFTAR MASTER TRANSAKSI SUPPLIER EMPLOYEE"
    End Sub
    Sub disable()
        txtEmpName.Enabled = False
        cboEmpPosition.Enabled = False
        ' chkActive.Enabled = False
    End Sub
    Sub clear()
        txtNilai.Text = 0
        txtPersentase.Text = 0
        UCMasterTransaksi.cboTransID()
    End Sub
    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBack.Click
        Response.Redirect("SupplierEmployee.aspx?id=" & Me.SupplierID & "&name=" & Me.SupplierCompanyName & "")
    End Sub

    Protected Sub ButtonSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonSave.Click
        Dim oSupplier As New Parameter.Supplier
        Dim oReturn As New Parameter.Supplier

        With oSupplier
            .strConnection = GetConnectionString()
            .AddEdit = Me.AddEdit
            .SupplierID = Me.SupplierID.Trim

            If Me.AddEdit = "Add" Then
                .SupplierID = Me.SupplierID.Trim
                .ID = Me.SupplierEmployeeID.Trim
            ElseIf Me.AddEdit = "Edit" Then
                .SupplierID = Me.SupplierID.Trim
                .ID = Me.SupplierEmployeeID.Trim
            End If


            .FormID = "SeMasterTransaksi"
            .NilaiAlokasi = txtNilai.Text.Trim
            .PersenAlokasi = txtPersentase.Text.Trim
            .TransID = Me.TransIDx
            .TransIDNew = UCMasterTransaksi.TransID
        End With

        oReturn = m_controller.SupplierEmployeeSave(oSupplier)

        If oReturn.Output = "" Then
            InitialPanel()
            BindGrid(Me.CmdWhere)
        Else
            ShowMessage(lblMessage, oReturn.Output, True)
        End If
        BindGrid(Me.CmdWhere)
    End Sub

    Protected Sub ButtonCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonCancel.Click
        InitialPanel()
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAdd.Click
        disable()
        pnlList.Visible = False
        pnlAddEdit.Visible = True
        BindAdd()
        clear()
        Me.AddEdit = "Add"
        lblTitle.Text = "ADD"
    End Sub
End Class