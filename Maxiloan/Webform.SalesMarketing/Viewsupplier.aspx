﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Viewsupplier.aspx.vb"
    Inherits="Maxiloan.Webform.SalesMarketing.Viewsupplier" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>View Supplier</title>
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">   
        <asp:Label ID="lblMessage" runat="server" Visible ="false" ></asp:Label>         
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">                   
                <h3>
                    VIEW SUPPLIER
                </h3>
            </div>
        </div>          
        <div class="form_box">
	        <div class="form_single">
                <label>Nama Lengkap Supplier</label>
                <asp:Label ID="lblName" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>NPWP</label>
                <asp:Label ID="lblNPWP" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>TDP</label>
                <asp:Label ID="lblTDP" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>SIUP</label>
                <asp:Label ID="lblSIUP" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Mulai PKS</label>
                <asp:Label ID="lblPKSDate" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>No. PKS</label>
                <asp:Label ID="lblPKSNo" runat="server"></asp:Label>
	        </div>
        </div>

        <div class="form_box_header">
            <div class="form_single">              
                <h4>
                    ALAMAT
                </h4>
            </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Alamat</label>
                <asp:Label ID="lblAddress" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>RT/RW</label>
                <asp:Label ID="lblRT" runat="server"></asp:Label>&nbsp;/
                <asp:Label ID="lblRW" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Kelurahan</label>
                <asp:Label ID="lblKelurahan" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Kecamatan</label>
                <asp:Label ID="lblKecamatan" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Kota</label>
                <asp:Label ID="lblCity" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Kode Pos</label>
                <asp:Label ID="lblZip" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>No Telepon-1</label>
                <asp:Label ID="lblAPhone1" runat="server"></asp:Label>&nbsp;-
                <asp:Label ID="lblPhone1" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>No Telepon-2</label>
                <asp:Label ID="lblAPhone2" runat="server"></asp:Label>&nbsp;-
                <asp:Label ID="lblPhone2" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>No Fax</label>
                <asp:Label ID="lblAFax" runat="server"></asp:Label>&nbsp;-
                <asp:Label ID="lblFax" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">              
                <h4>
                    KONTAK
                </h4>
            </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Nama</label>
                <asp:Label ID="lblCPName" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Jabatan</label>
                <asp:Label ID="lblCPJobTitle" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>e-mail</label>
                <asp:Label ID="lblCPEmail" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>HandPhone</label>
                <asp:Label ID="lblCPMobile" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">              
                <h4>
                    DATA SUPPLIER
                </h4>
            </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Mulai Tanggal</label>
                <asp:Label ID="lblStartDate" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Bad Status</label>
                <asp:Label ID="lblBadStatus" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Level</label>
                <asp:Label ID="lblLevel" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Jual Asset Sell Automotive?</label>
                <asp:Label ID="lblSell" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">              
                <h4>
                    DATA AUTOMOTIVE
                </h4>
            </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Registrasi BPKB</label>
                <asp:Label ID="lblBPKB" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Kategori</label>
                <asp:Label ID="lblCategory" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Jual Asset</label>
                <asp:Label ID="lblAsset" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonClose" OnClientClick="Close();" runat="server" CausesValidation="False" Text="Close"  CssClass ="small button gray">
            </asp:Button>
        </div>  
    </form>
</body>
</html>
