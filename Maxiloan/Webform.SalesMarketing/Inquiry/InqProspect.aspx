﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="InqProspect.aspx.vb"
    Inherits="Maxiloan.Webform.SalesMarketing.InqProspect" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Inquiry Prospect Application</title>
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function OpenWinViewProspectInquiry(pProspectAppId) {
            var x = screen.width; var y = screen.height - 100;
            window.open(ServerName + App + '/Webform.SalesMarketing/Inquiry/ViewProspect.aspx?ProspectAppId=' + pProspectAppId, 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0,scrollbars=1')
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    INQUIRY APLIKASI PROSPECT</h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
               <label class ="label_req">
                    Cabang
                </label>
                <asp:DropDownList ID="cbobranch" runat="server">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="rfvcbobranchid" runat="server" Display="Dynamic"
                    ControlToValidate="cbobranch" ErrorMessage="Harap pilih Cabang" InitialValue="0" CssClass="validator_general" ></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cari Berdasarkan
                </label>
                <asp:DropDownList ID="cboSearch" runat="server">
                    <asp:ListItem Value="Name">Name</asp:ListItem>
                    <asp:ListItem Value="ProspectAppID">No Aplikasi</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSearch" runat="server" CssClass="small button blue" Text="Find"
                CausesValidation="False" />
            <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="small button gray"
                CausesValidation="False" />
        </div>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                DAFTAR INITIAL APLIKASI</h3>
        </div>
    </div>
    <asp:Panel runat="server" ID="pnlList">
        <div class="form_box_header">
            <div class="form_single">
                <asp:DataGrid ID="dtgPaging" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                    OnSortCommand="Sorting" BorderWidth="0px" CellPadding="3" CellSpacing="1" CssClass="grid_general">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <Columns>
                        <asp:TemplateColumn SortExpression="ProspectAppID" HeaderText="DATA ID">                            
                            <ItemTemplate>
                                <asp:HyperLink ID="hyApplicationId" runat="server" Text='<%#DataBinder.eval(Container,"DataItem.ProspectAppID")%>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="Name" HeaderText="Name">                            
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblName" Text='<%# DataBinder.eval(Container,"DataItem.Name")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="CustomerType" HeaderText="Type">                            
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblType" Text='<%# DataBinder.eval(Container,"DataItem.CustomerType")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="ProspectAppDate" HeaderText="Date">                            
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblDate" Text='<%# DataBinder.eval(Container,"DataItem.ProspectAppDate")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="AssetType" HeaderText="ASSET TYPE">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblAssetType" Text='<%# DataBinder.eval(Container,"DataItem.AssetType")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>                
                        <asp:TemplateColumn SortExpression="AssetBrand" HeaderText="ASSET BRAND">                            
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblAssetBrand" Text='<%# DataBinder.eval(Container,"DataItem.AssetBrand")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="ProspectSource" HeaderText="SOURCE">                            
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblProspectSource" Text='<%# DataBinder.eval(Container,"DataItem.ProspectSource")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="ProspectStep" HeaderText="STEP PROSPECT">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblProspectStep" Text='<%# DataBinder.eval(Container,"DataItem.ProspectStep")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
                <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                        CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="First">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                        CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Prev">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                        CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Next">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                        CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Last">
                    </asp:ImageButton>
                    Page
                    <asp:TextBox ID="txtGoPage" runat="server" cssClass="small_text">1</asp:TextBox>
                    <asp:Button ID="btnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                        EnableViewState="False" />
                    <asp:RangeValidator ID="Rangevalidator1" runat="server" ControlToValidate="txtGopage"
                        MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer"
                         CssClass="validator_general"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtGopage"
                        ErrorMessage="No Halaman Salah" 
                        Display="Dynamic"  CssClass="validator_general"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="rgvGo" runat="server" ErrorMessage="No Halaman Salah" MinimumValue="1"
                        Type="integer" ControlToValidate="txtGoPage" CssClass="validator_general"></asp:RangeValidator>
                </div>
                <div class="label_gridnavigation">
                    <asp:Label ID="lblPage" runat="server"></asp:Label>of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                    <asp:Label ID="lblTotRec" runat="server"></asp:Label>record(s)
                </div>
            </div>
        </div>
    </asp:Panel>
    </form>
</body>
</html>