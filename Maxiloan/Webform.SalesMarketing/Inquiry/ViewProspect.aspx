﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewProspect.aspx.vb"
    Inherits="Maxiloan.Webform.SalesMarketing.ViewProspect" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ViewPettyCashReimburse</title>
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function fclose() {
            window.close();
        }
    </script>
</head>
<body>
    <form id="Form1" method="post" runat="server">
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                VIEW - PROSPECT
            </h3>
        </div>
    </div>
    <div class="form_title">
        <div class="form_single">
            <h3>
                DATA APLIKASI</h3>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Data ID
            </label>
            <asp:Label ID="lblProspectAppId" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Tanggal Aplikasi
            </label>
            <asp:Label ID="lbltanggalAplikasi" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Nama
            </label>
            <asp:Label ID="lblNama" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Tempat Lahir
            </label>
            <asp:Label ID="lblTempatLahir" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Tanggal Lahir
            </label>
            <asp:Label ID="lblTanggalLahir" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Jenis Dokumen
            </label>
            <asp:Label ID="lblIDTypeP" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Nomor Dokumen 
            </label>
            <asp:Label ID="lblIDNumberP" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box_title">
        <div class="form_single">
            <h4>
               DATA ASSET</h4>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Supplier/Dealer
            </label>
            <asp:Label ID="lblSupplierName" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Jenis Asset
            </label>
            <asp:Label ID="lblAssetType" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Merk Asset
            </label>
            <asp:Label ID="lblAssetBrand" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box_title">
        <div class="form_single">
            <h4>
               DATA PEMBIAYAAN</h4>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Harga OTR
            </label>
            <asp:Label ID="lblOTR" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Uang Muka Tunai
            </label>
            <asp:Label ID="lblUangMuka" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Uang Muka Trade In
            </label>
            <asp:Label ID="lblUangMukaTradeIn" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Total Uang Muka
            </label>
            <asp:Label ID="lblTotalUangMuka" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Pokok Hutang
            </label>
            <asp:Label ID="lblPokokHutang" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Angsuran Bulanan
            </label>
            <asp:Label ID="lblAngsuranBulanan" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Jangka Waktu
            </label>
            <asp:Label ID="lblTenor" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box" runat="server" id="divAkhirKontrak">
        <div class="form_single">
            <label>
                 Usia Akhir Kontrak
            </label>
            <asp:Label ID="lblusiaakhirkontrak" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="btnCLose" runat="server" Text="Close" CssClass="small button green"></asp:Button>
    </div>
    </form>
</body>
</html>
