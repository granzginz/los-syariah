﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class ViewProspect

    '''<summary>
    '''Form1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Form1 As Global.System.Web.UI.HtmlControls.HtmlForm

    '''<summary>
    '''lblProspectAppId control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblProspectAppId As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lbltanggalAplikasi control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbltanggalAplikasi As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblNama control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblNama As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblTempatLahir control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblTempatLahir As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblTanggalLahir control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblTanggalLahir As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblIDTypeP control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblIDTypeP As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblIDNumberP control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblIDNumberP As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblSupplierName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblSupplierName As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblAssetType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblAssetType As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblAssetBrand control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblAssetBrand As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblOTR control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblOTR As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblUangMuka control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblUangMuka As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblUangMukaTradeIn control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblUangMukaTradeIn As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblTotalUangMuka control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblTotalUangMuka As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblPokokHutang control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPokokHutang As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblAngsuranBulanan control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblAngsuranBulanan As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblTenor control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblTenor As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''divAkhirKontrak control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents divAkhirKontrak As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''lblusiaakhirkontrak control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblusiaakhirkontrak As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''btnCLose control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnCLose As Global.System.Web.UI.WebControls.Button
End Class
