﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class ViewProspect
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private oController As New ProspectController
#End Region

#Region "Property"

    Property RequestNo() As String
        Get
            Return viewstate("RequestNo").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("RequestNo") = Value
        End Set
    End Property
    Property CSSStyle() As String
        Get
            Return viewstate("CSSStyle").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("CSSStyle") = Value
        End Set
    End Property
    Private Property CmdWhere() As String
        Get
            Return CType(viewstate("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property ProspectAppID() As String
        Get
            Return CType(ViewState("ProspectAppID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ProspectAppID") = Value
        End Set
    End Property

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.FormID = "InqProspect"

            Me.ProspectAppID = Request("ProspectAppID").ToString

            Dim oCustomclass As New Parameter.Prospect
            With oCustomclass
                .strConnection = GetConnectionString()
                .ProspectAppID = Me.ProspectAppID
            End With
            BindEdit()

        End If

    End Sub

#Region "BindEdit"
    Sub BindEdit()
        Dim oApplication As New Parameter.Prospect
        Dim oData As New DataTable
        Dim oRow As DataRow

        oApplication.strConnection = GetConnectionString()
        oApplication.BranchId = Replace(Me.sesBranchId, "'", "").ToString
        oApplication.ProspectAppID = Me.ProspectAppID
        oApplication = oController.GetViewProspect(oApplication)

        If Not oApplication Is Nothing Then
            oData = oApplication.listdata
        End If

        If oData.Rows.Count > 0 Then
            oRow = oData.Rows(0)
            lblProspectAppId.Text = oRow("ProspectAppId").ToString
            lbltanggalAplikasi.Text = Format(oRow("ProspectAppDate"), "dd/MM/yyyy")
            lblNama.Text = oRow("Name").ToString
            lblTempatLahir.Text = oRow("BirthPlace").ToString
            lblTanggalLahir.Text = Format(oRow("BirthDate"), "dd/MM/yyyy")
            lblIDTypeP.Text = oRow("IDType").ToString
            lblIDNumberP.Text = oRow("IDNumber").ToString.Trim
            lblSupplierName.Text = oRow("SupplierName").ToString
            lblAssetType.Text = oRow("AssetTypeDescription").ToString
            lblAssetBrand.Text = oRow("AssetBrandDescription").ToString
            lblOTR.Text = FormatNumber(oRow("OTRPrice"), 0)
            lblUangMuka.Text = FormatNumber(oRow("DPAmount"), 0)
            lblUangMukaTradeIn.Text = FormatNumber(oRow("DPAmountTradeIn"), 0)
            lblTotalUangMuka.Text = FormatNumber(CDec(oRow("DPAmount")) + CDec(oRow("DPAmountTradeIn")), 0)
            lblPokokHutang.Text = FormatNumber(oRow("NTF"), 0)
            lblAngsuranBulanan.Text = FormatNumber(oRow("InstallmentAmount"), 0)
            lblTenor.Text = oRow("Tenor").ToString
            lblusiaakhirkontrak.Text = getAge(ConvertDate(oRow("BirthDate")), CInt(oRow("Tenor").ToString))
            If oRow("CustomerType").ToString = "C" Then
                divAkhirKontrak.Visible = False
            Else
                divAkhirKontrak.Visible = True
            End If
        End If
    End Sub
#End Region

    Private Function getAge(ByVal dt As String, ByVal m As Integer) As String
        Dim rtn As String = ""
        Dim dob As DateTime
        dob = New DateTime(dt.Substring(0, 4), dt.Substring(6, 2), dt.Substring(4, 2))
        Dim tday As TimeSpan = DateTime.Now.AddMonths(m).Subtract(dob)
        Dim years As Integer, months As Integer, days As Integer
        months = 12 * (DateTime.Now.AddMonths(m).Year - dob.Year) + (DateTime.Now.AddMonths(m).Month - dob.Month)

        If DateTime.Now.AddMonths(m).Day < dob.Day Then
            months -= 1
            days = DateTime.DaysInMonth(dob.Year, dob.Month) - dob.Day + DateTime.Now.AddMonths(m).Day
        Else
            days = DateTime.Now.AddMonths(m).Day - dob.Day
        End If
        years = Math.Floor(months / 12)
        months -= years * 12

        rtn = years & " tahun, " & months & " bulan and " & days & " hari"
        Return rtn
    End Function


    Protected Sub imbClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCLose.Click
        ScriptManager.RegisterStartupScript(Me, Page.GetType, "Script", "fclose();", True)
    End Sub
End Class