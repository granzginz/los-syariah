﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SupplierPrivate.aspx.vb" Inherits="Maxiloan.Webform.SalesMarketing.SupplierPrivate" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ucCompanyAddress" Src="../Webform.UserController/ucCompanyAddress.ascx" %>
<%@ Register TagPrefix="uc2" TagName="UcBankAccount" Src="../Webform.UserController/UcBankAccount.ascx" %>
<%@ Register Src="../Webform.UserController/ucDateCE.ascx" TagName="ucDateCE" TagPrefix="uc2" %>
<%@ Register Src="../Webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat"
    TagPrefix="uc3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Supplier Private</title>
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%#Request.servervariables("SERVER_NAME")%>/';

//        function SellAutomotiveAsset() {
//            var oSell = document.forms[0].rboSell;
//            var oBPKB = document.forms[0].rboBPKB;
//            var oCategory = document.forms[0].rboCategory;
//            var oAssetSold = document.forms[0].rboAssetSold;

//            alert(oSell.checked);

//            if (oSell.options[oSell.selectedIndex].value == "0") {
//                alert('Value rboSell = 0');
//                oBPKB.options[oBPKB.selectedIndex].value = "0";
//                //alert(oBPKB.options[oSell.selectedIndex].value);
//                oBPKB.disabled = true;

//                oCategory.options[oCategory.selectedIndex].value = "OT";
//                //alert(oCategory.options[oSell.selectedIndex].value);
//                oCategory.disabled = true;

//                oAssetSold.options[oAssetSold.selectedIndex].value = "M";
//                //alert(oAssetSold.options[oSell.selectedIndex].value);
//                oAssetSold.disabled = true;
//            }
    //    }							
    </script>
 <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <link href="../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false"  ToolTip="Click to close"
                onclick="hideMessage();" ></asp:Label>
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>
                        DAFTAR PRIVATE SUPPLIER
                    </h3>
                </div>
            </div>
            <asp:Panel ID="pnlList" runat="server" Width="100%">
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtgPaging" runat="server" Width="100%" CssClass="grid_general"
                                DataKeyField="Name" BorderStyle="None" BorderWidth="0" OnSortCommand="Sorting"
                                AutoGenerateColumns="False" AllowSorting="True">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="EDIT">
                                        <ItemStyle CssClass="command_col"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" ImageUrl="../Images/iconedit.gif"
                                                CommandName="Edit"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="DELETE">
                                        <ItemStyle CssClass="command_col"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgDelete" runat="server" CausesValidation="False" ImageUrl="../Images/icondelete.gif"
                                                CommandName="Delete"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn Visible="False" SortExpression="supplierID" HeaderText="ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lnkID" runat="server" Text='<%# DataBinder.eval(Container,"DataItem.supplierID") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="Name" HeaderText="NAMA SUPPLIER">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="lnkName" runat="server" Text='<%# DataBinder.eval(Container,"DataItem.Name") %>'>
                                            </asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn Visible="False" DataField="CPName" SortExpression="CPName" HeaderText="KONTAK">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn Visible="False" DataField="Phone" SortExpression="Phone" HeaderText="TELEPON">
                                    </asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="CABANG">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkBranch" runat="server" CommandName="Branch" CausesValidation="False">Branch</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PEMILIK">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkOwner" runat="server" CommandName="Owner" CausesValidation="False">Owner</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn Visible="False" HeaderText="BUDGET">
                                        <ItemTemplate>
                                            <asp:Label ID="lblBudget" runat="server">Budget</asp:Label>
                                            <asp:LinkButton ID="lnkBudget" Visible="false" runat="server" CommandName="Budget"
                                                CausesValidation="False">Budget</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn Visible="False" HeaderText="FORECAST">
                                        <ItemTemplate>
                                            <asp:Label ID="lblForecast" runat="server">ForeCast</asp:Label>
                                            <asp:LinkButton ID="lnkForecast" runat="server" Visible="False" CommandName="ForeCast"
                                                CausesValidation="False">ForeCast</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="KARYAWAN">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkEmployee" runat="server" CommandName="Employee" CausesValidation="False">Employee</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="REKENING BANK">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkBankAcc" runat="server" CommandName="Account" CausesValidation="False">Bank Account</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn Visible="False" HeaderText="TANDATANGAN">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkSignature" runat="server" CommandName="Signature" CausesValidation="False">Signature</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                            <div class="button_gridnavigation">
                                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../Images/grid_navbutton01.png"
                                    CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../Images/grid_navbutton02.png"
                                    CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../Images/grid_navbutton03.png"
                                    CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../Images/grid_navbutton04.png"
                                    CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                                    EnableViewState="False"></asp:Button>
                                <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" MinimumValue="1"
                                    ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"></asp:RangeValidator>
                                <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                                    ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                            </div>
                            <div class="label_gridnavigation">
                                <asp:Label ID="lblPage" runat="server"></asp:Label>of
                                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="imbButtonAdd" runat="server" CausesValidation="False" Text="Add"
                        CssClass="small button blue"></asp:Button>
                 
                </div>
                <div class="form_box_title">
                    <div class="form_single">
                        <h4>
                            CARI SUPPLIER
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Cari Berdasarkan</label>
                        <asp:DropDownList ID="cboSearch" runat="server">
                            <asp:ListItem Value="Name">Nama Supplier</asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtSearch" runat="server" Width="20%"></asp:TextBox>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="imbButtonSearch" runat="server" CausesValidation="False" CssClass="small button blue"
                        Text="Search"></asp:Button>
                    <asp:Button ID="imbButtonReset" runat="server" CausesValidation="False" Text="Reset"
                        CssClass="small button gray"></asp:Button>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlAddEdit" runat="server" Width="100%">
                <div class="form_box_header">
                    <div class="form_single">
                        <h4>
                            <asp:Label ID="lblTitle" runat="server"></asp:Label>
                        </h4>
                    </div>
                </div>
            <%--    <div class="form_box">
                    <div class="form_single">
                        <label>
                            Group Supplier</label>
                        <asp:DropDownList ID="cboGroup" runat="server">
                            <asp:ListItem Value="G">Group</asp:ListItem>
                            <asp:ListItem Value="N">Non Group</asp:ListItem>
                            <asp:ListItem Value="A">Afiliasi Group</asp:ListItem>
                        </asp:DropDownList>
                        
                    </div>
                </div>--%>
                <asp:Label ID="lblsuppID" runat="server" Visible="False"></asp:Label>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Nama Supplier
                        </label>
                        <asp:TextBox ID="txtName" runat="server" Width="20%" MaxLength="50"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="harap isi Nama Supplier"
                            ControlToValidate="txtName" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
             <%--   <div class="form_box">
                    <div class="form_single">
                        <label>
                            Nama Pendek</label>
                        <asp:TextBox ID="txtShortName" runat="server" MaxLength="10" Columns="15"></asp:TextBox>
                    </div>
                </div>--%>
            <%--    <div class="form_box">
                    <div class="form_single">
                        <label>
                            Nama Singkat</label>
                        <asp:TextBox ID="txtInitialName" runat="server" MaxLength="10" Columns="10"></asp:TextBox>
                    </div>
                </div>--%>
                 
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            NPWP</label>
                        <asp:TextBox ID="txtNPWP" runat="server" MaxLength="20" Columns="35" onkeypress="return numbersonly2(event)"></asp:TextBox> 
                    </div>
                    <div class="form_right">
                            <label>
                                Peseorangan</label>
                            <asp:CheckBox ID="chkPerseorangan" runat="server" />
                        </div>
                </div>
                </asp:Panel>
                <asp:Panel ID="pnlAdd" runat="server">
                <div class="form_box_header">
                    <div class="form_single">
                        <h4>
                            Alamat
                        </h4>
                    </div>
                </div>
                <div class="form_box_uc">
                    <uc1:uccompanyaddress id="ucAddress" runat="server"></uc1:uccompanyaddress>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <h4>
                            <strong>KONTAK</strong>
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Jabatan</label>
                        <asp:DropDownList ID="cboEmpPosition" runat="server">
                            <asp:ListItem Value="SL">Sales Person</asp:ListItem>
                            <asp:ListItem Value="CO">Supplier Office</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            E-mail</label>
                        <asp:TextBox ID="txtCPEmail" runat="server" MaxLength="30" Columns="35"></asp:TextBox>&nbsp;
                        <asp:RegularExpressionValidator CssClass="validator_general" ID="RegularExpressionValidator1"
                            runat="server" ErrorMessage="Email Address salah" ControlToValidate="txtCPEmail"
                            Display="Dynamic" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            No HandPhone</label>
                        <asp:TextBox ID="txtCPMobile" runat="server" MaxLength="20" Columns="25"></asp:TextBox>&nbsp;
                        <asp:RegularExpressionValidator CssClass="validator_general" ID="RegularExpressionValidator2"
                            runat="server" ErrorMessage="Harap isi dengan angka" ControlToValidate="txtCPMobile"
                            Display="Dynamic" ValidationExpression="\d*"></asp:RegularExpressionValidator>
                    </div>
                </div>
                 <div class="form_box">
                    <div class="form_single">
                        <label class="label_split_req">
                            No KTP</label>
                        <asp:TextBox ID="txtKTP" runat="server" MaxLength="20" Columns="25"></asp:TextBox>&nbsp;
                        <asp:RegularExpressionValidator CssClass="validator_general" ID="RegularExpressionValidator3"
                            runat="server" ErrorMessage="Harap isi dengan angka" ControlToValidate="txtKTP"
                            Display="Dynamic" ValidationExpression="\d*"></asp:RegularExpressionValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtKTP"
                ErrorMessage="Harap isi nomor KTP" Display="Dynamic" CssClass ="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>

                 <div class="form_box_header">
                    <div class="form_single">
                        <h4>
                            <strong>BANK ACCOUNT</strong>
                        </h4>
                    </div>
                </div>
                <div class="form_box_uc">
                    <uc2:ucbankaccount id="UcBankAccount" runat="server"></uc2:ucbankaccount>
                </div>


                
            </asp:Panel>
            <asp:Panel id="pnlBtnSaveEdit" runat="server">
            <div class="form_button">
                    <asp:Button ID="imbButtonSave" runat="server" CausesValidation="true" Text="Save"
                        CssClass="small button blue"></asp:Button>
                    <asp:Button ID="imbButtonCancel" runat="server" CausesValidation="false" Text="Cancel"
                        CssClass="small button gray"></asp:Button>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>