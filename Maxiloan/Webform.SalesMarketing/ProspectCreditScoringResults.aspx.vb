﻿Imports Maxiloan.Parameter 
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports System.Threading
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Web


Public Class ProspectCreditScoringResults
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents GridNavigator As ucGridNav

    Protected controller As InstallRcv3rdController
    Protected currentPage As Int32 = 1
    Protected pageSize As Int16 = 20
    Protected currentPageNumber As Int16 = 1
    Protected totalPages As Double = 1
    Protected recordCount As Int64 = 1


    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("prosCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("prosCmdWhere") = Value
        End Set
    End Property


    Private oController As New ProspectController



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If

        AddHandler GridNavigator.PageChanged, AddressOf PageNavigation
        If Not Page.IsPostBack Then


            'If IsSingleBranch() And CInt(Replace(Me.sesBranchId, "'", "")) >= 1 And Me.IsHoBranch = False Then

            Me.FormID = "PRNCRDSCR"
            'If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
            If SessionInvalid() Then
                Exit Sub
            End If
            'End If
            CmdWhere = String.Format("Prospect.CreditDispPrn=0 and Prospect.IsScored=1 and Prospect.IsProceeded=1 and Prospect.BranchID ='{0}'", sesBranchId.Replace("'", "").Trim)
            Sort = "ProspectAppId ASC"
            BindGrid()
            'Else
            '    Dim strHTTPServer As String
            '    Dim StrHTTPApp As String
            '    Dim strNameServer As String
            '    strHTTPServer = Request.ServerVariables("PATH_INFO")
            '    strNameServer = Request.ServerVariables("SERVER_NAME")
            '    StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
            '    Response.Redirect("http://" & strNameServer & "/" & StrHTTPApp & "/error_notauthorized.aspx")
            'End If
        End If
    End Sub
    Protected Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        currentPage = e.CurrentPage
        BindGrid(True)
        GridNavigator.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
    End Sub
    Sub BindGrid(Optional isFrNav As Boolean = False)

        Dim oController As New ProspectController

        Dim oCustomClass As New Parameter.Prospect

        oCustomClass.PageSize = pageSize
        oCustomClass.WhereCond = CmdWhere
        oCustomClass.SortBy = Me.Sort
        oCustomClass.CurrentPage = currentPage
        oCustomClass.strConnection = GetConnectionString()
        oCustomClass = oController.GetProspectScorePolicyResult(oCustomClass)
        recordCount = oCustomClass.TotalRecords
        dtgPaging.DataSource = oCustomClass.listdata

        Try
            dtgPaging.DataBind()
        Catch
            dtgPaging.CurrentPageIndex = 0
            dtgPaging.DataBind()
        End Try

        If (isFrNav = False) Then
            GridNavigator.Initialize(recordCount, pageSize)
        End If
    End Sub

    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        Sort = String.Format("{0} {1}", e.SortExpression, IIf(InStr(Sort, "DESC") > 0, "", "DESC"))
        BindGrid()
    End Sub


    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        cboSearch.SelectedIndex = 0
        txtSearch.Text = ""
        CmdWhere = String.Format("Prospect.IsScored=1 and Prospect.IsProceeded=1 and Prospect.BranchID ='{0}'", sesBranchId.Replace("'", "").Trim)
        BindGrid()
    End Sub

    Private Sub imbSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
 
        CmdWhere = String.Format(" Prospect.IsScored=1 and Prospect.IsProceeded=1 and Prospect.BranchID ='{0}'", sesBranchId.Replace("'", "").Trim)
        If txtSearch.Text.Trim <> "" Then
            CmdWhere = String.Format(" Prospect.IsScored=1 and isProceeded=1 and {0} LIKE '%{1}%' and  Prospect.BranchID ='{2}'", "Prospect." + cboSearch.SelectedItem.Value, txtSearch.Text.Replace("%", ""), sesBranchId.Replace("'", "").Trim)
        End If

        CmdWhere = String.Format("Prospect.CreditDispPrn {0} 0 and  {1}", IIf(cboPrinted.SelectedItem.Value = "No", "=", ">"), CmdWhere)
        BindGrid()
    End Sub

     

    Private Sub imbPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProses.Click



        Dim oDataSet As New DataSet
        Dim oReport As KreditDisposal = New KreditDisposal
        Dim chekedIds As New List(Of String)

        For Each item In dtgPaging.Items
            If (CType(item.FindControl("ItemCheckBox"), CheckBox).Checked) Then
                chekedIds.Add(item.Cells(1).Text.Trim())
            End If
        Next

  
        Dim ds = oController.DispositionCreditRpt(GetConnectionString(), chekedIds.ToArray, "A")
 

        oReport.SetDataSource(ds)
 

        crViewer.ReportSource = oReport
        crViewer.Visible = True
        crViewer.DataBind()

        With oReport.ExportOptions
            .ExportDestinationType = ExportDestinationType.DiskFile
            .ExportFormatType = ExportFormatType.PortableDocFormat
        End With

        Dim strFileLocation As String
        Dim fName As String = Me.Session.SessionID + Me.Loginid + "KreditDisposal.pdf"

        strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
        strFileLocation += fName

        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = _
        New CrystalDecisions.Shared.DiskFileDestinationOptions

        DiskOpts.DiskFileName = strFileLocation

        With oReport
            .ExportOptions.DestinationOptions = DiskOpts
            .Export()
            .Close()
            .Dispose()
        End With

        strFileLocation = "../XML/" & fName
        imbSearch_Click(Nothing, Nothing)
        Response.Write("<script language = javascript>" & vbCrLf _
        & "window.open('" & strFileLocation & "','accacq', 'menubar=0, scrollbars=yes, resizable=1') " & vbCrLf _
        & "</script>")


    End Sub
End Class