﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.cbse
Imports Maxiloan.Exceptions
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller

#End Region

Public Class NegativeCustomer
    Inherits Maxiloan.Webform.WebBased
    Dim Control As New CTKonfirmasiEbankExec
    Dim dtCSV, dtSQL As New DataTable

#Region "Constanta"
    Private oController As New NegativeCustomerController
    Private oCustomclass As New Parameter.NegativeCustomer

    Protected WithEvents ucAddress As UcCompanyAddress

    Protected WithEvents txtBirthDate As ucDateCE

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region

#Region "Property"
    Private Property Process() As String
        Get
            Return CType(ViewState("Process"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Process") = Value
        End Set
    End Property
    Private Property NegativeCustID() As String
        Get
            Return CType(ViewState("NegativeCustID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("NegativeCustID") = Value
        End Set
    End Property

    Private Property IDType() As String
        Get
            Return CType(ViewState("IDType"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("IDType") = Value
        End Set
    End Property
#End Region

    Protected WithEvents GridNavigator As ucGridNav
#Region "pageLoad"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load, Me.Load
        lblMessage.Visible = False
        AddHandler GridNavigator.PageChanged, AddressOf PageNavigation
        If Not IsPostBack Then
            If SessionInvalid() Then
                Exit Sub
            End If
            If Request.QueryString("strFileLocation") <> "" Then
                Dim strHTTPServer As String
                Dim StrHTTPApp As String
                Dim strNameServer As String
                Dim strFileLocation As String

                strHTTPServer = Request.ServerVariables("PATH_INFO")
                strNameServer = Request.ServerVariables("SERVER_NAME")
                StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                strFileLocation = "http://" & strNameServer & "/" & StrHTTPApp & "/XML/" & Request.QueryString("strFileLocation") & ".pdf"
                '& "history.back(-1) " & vbCrLf _
                'Call file PDF 
                Response.Write("<script language = javascript>" & vbCrLf _
                & "window.open('" & strFileLocation & "','Marketing', 'left=0, top=0, width=800, height=600, menubar=0, scrollbars=1 resizable=1') " & vbCrLf _
                & "</script>")

            End If
            Me.FormID = "NegativeCust"
            If CheckForm(Me.Loginid, Me.FormID, "MAXILOAN") Then
                pnlLoadCSV.Visible = False
                pnlList.Visible = True
                pnlAdd.Visible = False
                Me.SearchBy = ""
                Me.SortBy = ""
                DoBind(Me.SearchBy, Me.SortBy)
                ucAddress.Style = "Marketing"
                ucAddress.ValidatorFalse()
                ucAddress.Phone1ValidatorEnabled(False)
            End If
        End If
    End Sub
#End Region

    Private Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        currentPage = e.CurrentPage
        DoBind(Me.SearchBy, Me.SortBy, True)
        GridNavigator.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
    End Sub
#Region "DoBind"
    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String, Optional isFrNav As Boolean = False)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        Dim intloop As Integer
        Dim hypID As HyperLink

        With oCustomclass
            .strConnection = GetConnectionString
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oCustomclass = oController.ListNegativeCustomer(oCustomclass)

        DtUserList = oCustomclass.ListNegative
        DvUserList = DtUserList.DefaultView
        recordCount = oCustomclass.TotalRecord
        DvUserList.Sort = Me.SortBy
        dtgList.DataSource = DvUserList

        Try
            dtgList.DataBind()
        Catch
            dtgList.CurrentPageIndex = 0
            dtgList.DataBind()
        End Try

        If (isFrNav = False) Then
            GridNavigator.Initialize(recordCount, pageSize)
        End If
        If recordCount = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
        End If

    End Sub
#End Region

#Region "Sort"
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

#Region "Add"
    Private Sub imbAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonAdd.Click
        If CheckFeature(Me.Loginid, Me.FormID, "Add", "MAXILOAN") Then
            pnlAdd.Visible = True
            pnlList.Visible = False
            pnlBadAdd.Visible = True
            pnlBadEdt.Visible = False
            'lblTitle.Text = "ADD"
            Me.Process = "ADD"
            FillCboDataSource()
            FillCboIDType()

            txtNameAdd.Text = ""
            cboIDTypeAdd.ClearSelection()
            txtIDNumberAdd.Text = ""
            txtBirthPlaceAdd.Text = ""
            txtBirthDate.Text = ""
            'txtAddress.Text = ""
            'txtAddress.Text = ""
            'txtRT.Text = ""
            'txtRW.Text = ""
            'ucLookupZipCode.Kecamatan = ""
            'ucLookupZipCode.Kelurahan = ""
            'ucLookupZipCode.City = ""
            'ucLookupZipCode.ZipCode = ""
            'txtArea1.Text = ""
            'txtArea2.Text = ""
            'txtPhone1.Text = ""
            'txtPhone2.Text = ""
            'txtFax.Text = ""
            'txtAreaFax.Text = ""
            txtMobilePhoneAdd.Text = ""
            ucAddress.Address = ""
            ucAddress.RT = ""
            ucAddress.RW = ""
            ucAddress.Kelurahan = ""
            ucAddress.Kecamatan = ""
            ucAddress.City = ""
            ucAddress.ZipCode = ""
            ucAddress.AreaPhone1 = ""
            ucAddress.Phone1 = ""
            ucAddress.AreaPhone2 = ""
            ucAddress.Phone2 = ""
            ucAddress.AreaFax = ""
            ucAddress.Fax = ""
            ucAddress.BindAddress()
            txtEmailAdd.Text = ""
            cboBadTypeAdd.ClearSelection()
            cboBadTypeEdt.ClearSelection()
            cboDataSourceAdd.ClearSelection()
            cboDataSourceEdt.ClearSelection()
            txtNotes.Text = ""
        End If
    End Sub
#End Region

#Region "Cancel"
    Private Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        pnlList.Visible = True
        pnlAdd.Visible = False
        Me.SearchBy = ""
        Me.SortBy = ""
        txtNameAdd.Text = ""
        cboIDTypeAdd.ClearSelection()
        txtIDNumberAdd.Text = ""
        txtBirthPlaceAdd.Text = ""
        txtBirthDate.Text = ""
        ucAddress.Address = ""
        ucAddress.RT = ""
        ucAddress.RW = ""
        ucAddress.Kelurahan = ""
        ucAddress.Kecamatan = ""
        ucAddress.City = ""
        ucAddress.ZipCode = ""
        ucAddress.AreaPhone1 = ""
        ucAddress.Phone1 = ""
        ucAddress.AreaPhone2 = ""
        ucAddress.Phone2 = ""
        ucAddress.AreaFax = ""
        ucAddress.Fax = ""
        txtMobilePhoneAdd.Text = ""
        txtEmailAdd.Text = ""
        cboBadTypeAdd.ClearSelection()
        cboBadTypeEdt.ClearSelection()
        cboDataSourceAdd.ClearSelection()
        cboDataSourceEdt.ClearSelection()
        txtNotes.Text = ""
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

#Region "Search"
    Private Sub ButtonSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonSearch.Click
        If txtSearch.Text <> "" Then
            Me.SearchBy = cboSearch.SelectedItem.Value & " like '%" & txtSearch.Text.Trim & "%'"
        Else
            Me.SearchBy = ""
        End If
        Me.SortBy = ""
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

#Region "Reset"
    Private Sub imbReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        Me.SearchBy = ""
        Me.SortBy = ""
        DoBind(Me.SearchBy, Me.SortBy)
        txtSearch.Text = ""
        cboSearch.ClearSelection()
        pnlAdd.Visible = False
    End Sub
#End Region

#Region "dtgList ItemCommand"
    Private Sub dtgList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgList.ItemCommand
        Try
            If e.CommandName.Trim = "Edit" Then
                If CheckFeature(Me.Loginid, Me.FormID, "Edit", "MAXILOAN") Then
                    Dim InlblType As New Label
                    pnlAdd.Visible = True
                    pnlList.Visible = False
                    pnlBadAdd.Visible = False
                    pnlBadEdt.Visible = True
                    Dim oDataTable As New DataTable
                    With oCustomclass
                        .strConnection = GetConnectionString()
                        .NegativeCustomerID = dtgList.Items(e.Item.ItemIndex).Cells(1).Text.Trim
                        .strKey = ""
                    End With
                    Me.NegativeCustID = dtgList.Items(e.Item.ItemIndex).Cells(1).Text.Trim
                    InlblType = CType(e.Item.FindControl("lblIDType"), Label)
                    Me.IDType = InlblType.Text.Trim
                    oCustomclass = oController.NegativeCustomerView(oCustomclass)
                    oDataTable = oCustomclass.ListNegative
                    Me.Process = "EDIT"
                    'lblTitle.Text = "EDIT"
                    FillData(oDataTable)
                    cboIDTypeAdd.SelectedIndex = cboIDTypeAdd.Items.IndexOf(cboIDTypeAdd.Items.FindByValue(Me.IDType))
                End If
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try

    End Sub
#End Region

#Region "Save"
    Private Sub ButtonSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
        Dim hasil As Integer
        With oCustomclass
            .strConnection = GetConnectionString()
            .CustomerName = txtNameAdd.Text.Trim
            .IDType = cboIDTypeAdd.SelectedItem.Value.Trim
            .IDNumber = txtIDNumberAdd.Text.Trim
            If txtBirthDate.Text <> "" Then
                .BirthDate = ConvertDate(txtBirthDate.Text)
            Else
                .BirthDate = Nothing
            End If
            'If txtBirthDate.Text <> "" Then
            '    .BirthDate = txtBirthDate.Text
            'Else
            '    .BirthDate = Nothing
            'End If
            .BirthPlace = txtBirthPlaceAdd.Text.Trim
            .Address = ucAddress.Address.Trim
            .Kelurahan = ucAddress.Kelurahan.Trim
            .Kecamatan = ucAddress.Kecamatan.Trim
            .City = ucAddress.City.Trim
            .ZipCode = ucAddress.ZipCode.Trim
            .LegalAreaPhone1 = ucAddress.AreaPhone1.Trim
            .LegalPhone1 = ucAddress.Phone1.Trim
            .LegalAreaPhone2 = ucAddress.AreaPhone2.Trim
            .LegalPhone2 = ucAddress.Phone2.Trim
            .LegalAreaFax = ucAddress.AreaFax.Trim
            .LegalFax = ucAddress.Fax.Trim
            .RT = ucAddress.RT.Trim
            .RW = ucAddress.RW.Trim
            .MobilePhone = txtMobilePhoneAdd.Text.Trim
            .Email = txtEmailAdd.Text.Trim
            .Notes = txtNotes.Text.Trim
            .BusinessDate = Me.BusinessDate
            .BranchId = Replace(Me.sesBranchId, "'", "").Trim
        End With
        If Me.Process = "ADD" Then
            oCustomclass.BadType = cboBadTypeAdd.SelectedItem.Value.Trim
            oCustomclass.DataSource = cboDataSourceAdd.SelectedItem.Value.Trim
            oCustomclass = oController.NegativeCustomerAdd(oCustomclass)
        ElseIf Me.Process = "EDIT" Then
            oCustomclass.NegativeCustomerID = Me.NegativeCustID.Trim
            oCustomclass.BadType = cboBadTypeEdt.SelectedItem.Value.Trim
            oCustomclass.DataSource = cboDataSourceEdt.SelectedItem.Value.Trim
            oCustomclass.IsActive = CBool(rdoActive.SelectedItem.Value)
            oCustomclass = oController.NegativeCustomerEdit(oCustomclass)
        End If
        hasil = oCustomclass.Hasil
        If hasil = 0 Then
            ShowMessage(lblMessage, "Simpan data gagal.....", True)
            Exit Sub
        Else
            Me.SearchBy = ""
            Me.SortBy = ""
            DoBind(Me.SearchBy, Me.SortBy)
            pnlAdd.Visible = False
            pnlList.Visible = True
            If Me.Process = "ADD" Then
                ShowMessage(lblMessage, "Simpan data berhasil.....", False)
            Else
                ShowMessage(lblMessage, "Updated data berhasil .....", False)
            End If
        End If
    End Sub
#End Region

#Region "Fill Data"
    Private Sub FillData(ByVal oTable As DataTable)
        Dim oRow As DataRow
        oRow = oTable.Rows(0)
        txtNameAdd.Text = CType(oRow("Customername"), String).Trim
        txtBirthPlaceAdd.Text = CType(oRow("BirthPlace"), String).Trim
        txtIDNumberAdd.Text = CType(oRow("IDNUmber"), String).Trim
        txtBirthDate.Text = Format(CDate(oRow("BirthDate")), "dd/MM/yyyy")
        If Format(CDate(oRow("BirthDate")), "dd/MM/yyyy") <> "01/01/1900" Then
            txtBirthDate.Text = Format(CDate(oRow("BirthDate")), "dd/MM/yyyy")
        Else
            txtBirthDate.Text = ""
        End If
        ucAddress.Kelurahan = CType(oRow("LegalKelurahan"), String).Trim
        ucAddress.Address = CType(oRow("LegalAddress"), String).Trim
        ucAddress.RT = CType(oRow("LegalRT"), String).Trim
        ucAddress.RW = CType(oRow("LegalRW"), String).Trim
        ucAddress.Kecamatan = CType(oRow("LegalKecamatan"), String).Trim
        ucAddress.City = CType(oRow("LegalCity"), String).Trim
        ucAddress.ZipCode = CType(oRow("LegalZipCode"), String).Trim
        ucAddress.AreaPhone1 = CType(oRow("LegalAreaPhone1"), String).Trim
        ucAddress.AreaPhone2 = CType(oRow("LegalAreaPhone2"), String).Trim
        ucAddress.Phone1 = CType(oRow("LegalPhone1"), String).Trim
        ucAddress.Phone2 = CType(oRow("LegalPhone2"), String).Trim
        ucAddress.AreaFax = CType(oRow("LegalAreaFax"), String).Trim
        ucAddress.Fax = CType(oRow("LegalFax"), String).Trim
        ucAddress.BindAddress()
        txtMobilePhoneAdd.Text = CType(oRow("Mobilephone"), String).Trim
        txtEmailAdd.Text = CType(oRow("Email"), String).Trim
        txtNotes.Text = CType(oRow("NOtes"), String).Trim
        lblSKT.Text = CType(oRow("NumOfSKT"), String).Trim
        lblAfter.Text = CType(oRow("NumOfRejectAfterSurvey"), String).Trim
        lblBefore.Text = CType(oRow("NumOfRejectBeforeSurvey"), String).Trim
        cboBadTypeEdt.SelectedIndex = cboBadTypeEdt.Items.IndexOf(cboBadTypeEdt.Items.FindByValue(IIf(oRow("BadType").ToString.Trim = "W", "W", "B").ToString))
        rdoActive.SelectedIndex = rdoActive.Items.IndexOf(rdoActive.Items.FindByValue(IIf(oRow("IsActive").ToString.Trim = "True", "1", "0").ToString))
        FillCboIDType()
        FillCboDataSource()

        cboDataSourceEdt.SelectedValue = oRow("DataSource").ToString

        Dim approvalNo As String = getApprovalNo(oRow("NegativeCustomerId").ToString)
        Set_History(approvalNo)
    End Sub
#End Region

#Region "FillCboDataSource"
    Private Sub FillCboDataSource()
        Dim oDataTable As New DataTable
        With oCustomclass
            .strConnection = GetConnectionString
        End With
        oCustomclass = oController.ListdataSource(oCustomclass)
        oDataTable = oCustomclass.ListNegative
        If Me.Process = "ADD" Then
            With cboDataSourceAdd
                .DataSource = oDataTable
                .DataTextField = "Description"
                .DataValueField = "NCdataSourceID"
                .DataBind()
            End With
        Else
            With cboDataSourceEdt
                .DataSource = oDataTable
                .DataTextField = "Description"
                .DataValueField = "NCdataSourceID"
                .DataBind()
            End With
        End If
    End Sub
#End Region

#Region "FillCboIDType"
    Private Sub FillCboIDType()
        Dim oDataTable As New DataTable
        With oCustomclass
            .strConnection = GetConnectionString
        End With
        oCustomclass = oController.ListIDType(oCustomclass)
        oDataTable = oCustomclass.ListNegative
        With cboIDTypeAdd
            .DataSource = oDataTable
            .DataTextField = "Description"
            .DataValueField = "ID"
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = ""
        End With
    End Sub

#End Region
    Private Function getApprovalNo(ByVal NegativeCustomerId As String) As String
        With oCustomclass
            .strConnection = GetConnectionString()
            .NegativeCustomerID = NegativeCustomerId

        End With

        Return oController.GetApprovalNumberForHistory(oCustomclass)
    End Function

    Private Sub Set_History(ByVal pStrApprovalNo As String)
        Dim approvalData As New Parameter.ApprovalScreen
        Dim approvalCont As New ApprovalScreenController
        With approvalData
            .strConnection = GetConnectionString()
            .ApprovalNo = pStrApprovalNo
            .BusinessDate = Me.BusinessDate
        End With
        dtgHistory.DataSource = approvalCont.ApprovalListHistory(approvalData)
        dtgHistory.DataBind()
    End Sub

    

    Protected Sub Button_Search_Click(sender As Object, e As EventArgs) Handles Button_Search.Click
        pnlLoadCSV.Visible = True
        pnlList.Visible = False
        pnlAdd.Visible = False

    End Sub
    Protected Sub Btn_Upload_Click(sender As Object, e As EventArgs) Handles Btn_Upload.Click
        PopulateFromCSV()
        pnlLoadCSV.Visible = False
        pnlList.Visible = True
        pnlAdd.Visible = False
    End Sub

    Private Sub PopulateFromCSV()
        Dim appPath As String = HttpContext.Current.Request.ApplicationPath

        Dim directory As String = HttpContext.Current.Request.MapPath(appPath) & "\xml\"
        Dim FileName As String = files.PostedFile.FileName
        Dim fullFileName As String = directory & FileName
        Dim dt As New DataTable
        files.PostedFile.SaveAs(fullFileName)
        Dim sr As New IO.StreamReader(fullFileName)
        Try
            Dim originalLine As String
            dt.Columns.Add("NegativeCustomerID")
            dt.Columns.Add("CustomerID")
            dt.Columns.Add("CustomerName")
            dt.Columns.Add("IDType")
            dt.Columns.Add("IDNumber")
            dt.Columns.Add("BirthPlace")
            dt.Columns.Add("BirthDate")
            dt.Columns.Add("LegalAddress")
            dt.Columns.Add("LegalRT")
            dt.Columns.Add("LegalRW")
            dt.Columns.Add("LegalKelurahan")
            dt.Columns.Add("LegalKecamatan")
            dt.Columns.Add("LegalCity")
            dt.Columns.Add("LegalZipCode")
            dt.Columns.Add("LegalAreaPhone1")
            dt.Columns.Add("LegalPhone1")
            dt.Columns.Add("MobilePhone")
            dt.Columns.Add("BadType")
            dt.Columns.Add("DataSource")
            dt.Columns.Add("Notes")
            dt.Columns.Add("IsActive")


            While (Not sr.EndOfStream)
                originalLine = sr.ReadLine.ToString
                Dim OriSplit() As String = Split(originalLine, ",")

                If (OriSplit.Length <> 21) Then
                    Throw New Exception("Format csv tidak sesuai")
                End If
                If (OriSplit(0).Contains("NegativeCustomerID")) Then
                    Continue While
                End If

                Dim newrow As DataRow = dt.NewRow
                newrow.ItemArray = {OriSplit(0).Replace("""", ""), OriSplit(1).Replace("""", ""), OriSplit(2).Replace("""", ""), OriSplit(3).Replace("""", ""), OriSplit(4).Replace("""", ""), OriSplit(5).Replace("""", ""), OriSplit(6).Replace("""", ""), OriSplit(7).Replace("""", ""), OriSplit(8).Replace("""", ""), OriSplit(9).Replace("""", ""), OriSplit(10).Replace("""", ""), OriSplit(11).Replace("""", ""), OriSplit(12).Replace("""", ""), OriSplit(13).Replace("""", ""), OriSplit(14).Replace("""", ""), OriSplit(15).Replace("""", ""), OriSplit(16).Replace("""", ""), OriSplit(17).Replace("""", ""), OriSplit(18).Replace("""", ""), OriSplit(19).Replace("""", ""), OriSplit(20).Replace("""", "")}
                dt.Rows.Add(newrow)

            End While

            If (dt.Rows.Count > 0) Then
                oController.DoUploadNegativeCsv(GetConnectionString(), DateTime.Now, Replace(Me.sesBranchId, "'", ""), dt)
                DoBind(Me.SearchBy, Me.SortBy)
            End If

        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)

        Finally
            sr.Dispose()
        End Try
    End Sub

End Class