﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SupplierBankAcc.aspx.vb"
    Inherits="Maxiloan.Webform.SalesMarketing.SupplierBankAcc" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcBankAccount" Src="../Webform.UserController/UcBankAccount.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>MENAMBAH REKENING BANK GROUP SUPPLIER</title>
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <script language="JavaScript" type="text/javascript">
        function fBack() {
            history.back(-1);
            return false; 
        }
        function DeleteConfirm() {
            if (confirm("Apakah yakin mau hapus data ini ?")) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
    <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" ForeColor="red"></asp:Label>
    <table >
        <tr>
            <td> 
                &nbsp;&nbsp;&nbsp;&nbsp;
                <asp:RangeValidator ID="rgvGo" Style="z-index: 101" runat="server" ControlToValidate="txtGoPage"
                    Type="integer" MinimumValue="1" ErrorMessage="No Halaman Salah"></asp:RangeValidator>
            </td>
        </tr>
    </table>
    <asp:Panel ID="pnlList" runat="server" >
        <table >
            <tr>
                <td>
                    MENAMBAH REKENING BANK GROUP SUPPLIER
                </td>
            </tr>
        </table>
        <table>
            <tr>
                <td>
                    <asp:DataGrid ID="dtgPaging" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="Sorting" DataKeyField="GroupSupplierAccountID"
                        CssClass="tablegrid">
                        <AlternatingItemStyle CssClass="tdgenap"></AlternatingItemStyle>
                        <ItemStyle CssClass="tdganjil"></ItemStyle>
                        <HeaderStyle HorizontalAlign="Center" CssClass="tdjudul"></HeaderStyle>
                        <Columns>
                            <asp:TemplateColumn HeaderText="EDIT">
                                <ItemStyle Width="10%" HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" ImageUrl="../Images/iconedit.gif"
                                        CommandName="Edit"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DELETE">
                                <ItemStyle Width="10%" HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbDelete" runat="server" CausesValidation="False" ImageUrl="../Images/iconDelete.gif"
                                        CommandName="Delete"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="GroupSupplierAccountBankName" SortExpression="GroupSupplierAccountBankName" HeaderText="NAMA BANK">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="GroupSupplierAccountBankBranch" SortExpression="GroupSupplierAccountBankBranch" HeaderText="NAMA CABANG">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="GroupSupplierAccountNo" SortExpression="GroupSupplierAccountNo" HeaderText="NO REKENING">
                            </asp:BoundColumn>


                            <asp:TemplateColumn HeaderText="DEFAULT" SortExpression="DefaultAccount">
                                <ItemStyle Width="10%" HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>                                    
                                    <input id="hdncheckDefault" type="hidden" name="hdncheckDefault" runat="server" value =<%# DataBinder.eval(Container,"DataItem.DefaultAccount") %>/>
                                    <asp:CheckBox ID="checkDefault" Enabled="false" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="GroupSupplierAccountName" SortExpression="GroupSupplierAccountName" HeaderText="ATAS NAMA">
                            </asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                </td>
            </tr>
        </table>
        <table>
            <tr>
                <td class="nav_tbl_td1">
                    <table class="nav_command_tbl">
                        <tr>
                            <td>
                                <asp:ImageButton ID="imbAdd" runat="server" ImageUrl="../Images/ButtonAdd.gif"
                                    CausesValidation="False"></asp:ImageButton>
                            </td>
                            <td>
                                <asp:ImageButton ID="imbPrint" Visible="false"  runat="server" Enabled="true" ImageUrl="../Images/ButtonPrint.gif">
                                </asp:ImageButton>
                            </td>
                        </tr>
                    </table>
                </td>
                <td class="nav_tbl_td2">
                    <table class="nav_page_tbl">
                        <tr>
                            <td>
                                <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" ImageUrl="../Images/butkiri1.gif"
                                    OnCommand="NavigationLink_Click" CommandName="First"></asp:ImageButton><font face="Verdana"></font>
                            </td>
                            <td>
                                <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" ImageUrl="../Images/butkiri.gif"
                                    OnCommand="NavigationLink_Click" CommandName="Prev"></asp:ImageButton><font face="Verdana"></font>
                            </td>
                            <td>
                                <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" ImageUrl="../Images/butkanan.gif"
                                    OnCommand="NavigationLink_Click" CommandName="Next"></asp:ImageButton><font face="Verdana"></font>
                            </td>
                            <td>
                                <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" ImageUrl="../Images/butkanan1.gif"
                                    OnCommand="NavigationLink_Click" CommandName="Last"></asp:ImageButton><font face="Verdana"></font>
                            </td>
                            <td>
                                Page&nbsp;
                                <asp:TextBox ID="txtgoPage" runat="server" Width="34px" CssClass="InpType">1</asp:TextBox>
                            </td>
                            <td>
                                <asp:ImageButton ID="imbGoPage" runat="server" ImageUrl="../Images/butgo.gif"
                                    EnableViewState="False"></asp:ImageButton>
                            </td>
                            <asp:RangeValidator ID="Rangevalidator2" runat="server" ControlToValidate="txtGopage"
                                MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999"
                                Type="Integer" ForeColor="#993300" font-name="Verdana" Font-Size="11px"></asp:RangeValidator>
                            <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtGopage"
                                ErrorMessage="No Halaman Salah" ForeColor="#993300" font-name="Verdana"
                                Font-Size="11px" Display="Dynamic"></asp:RequiredFieldValidator></tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="nav_totpage" colspan="2">
                    <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                    <asp:Label ID="lbltotrec" runat="server"></asp:Label>&nbsp;record(s)
                </td>
            </tr>
        </table>
        <br/>
        <table>
            <tr>
                <td>
                    CARI GROUP SUPPLIER
                </td>
            </tr>
        </table>
        <table>
            <tr>
                <td>
                    Nama Supplier
                </td>
                <td>
                    <asp:Label ID="lblCariNmSupplier" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Cari Berdasarkan
                </td>
                <td>
                    <asp:DropDownList ID="cboSearch" runat="server">
                        <asp:ListItem Value="GroupSupplierAccountBankName">NAMA BANK</asp:ListItem>
                        <asp:ListItem Value="GroupSupplierAccountBankBranch">NAMA CABANG</asp:ListItem>
                        <asp:ListItem Value="GroupSupplierAccountNo">NO REKENING</asp:ListItem>
                        <asp:ListItem Value="DefaultAccount">DEFAULT</asp:ListItem>
                        <asp:ListItem Value="GroupSupplierAccountName">ATAS NAMA</asp:ListItem>
                    </asp:DropDownList>
                    &nbsp;
                    <asp:TextBox ID="txtSearch" runat="server" Width="75%" CssClass="inptype" MaxLength="100"></asp:TextBox>
                </td>
            </tr>
        </table>
        <table>
            <tr>
                <td>
                    <asp:ImageButton ID="imbSearch" runat="server" CausesValidation="False" ImageUrl="../Images/ButtonSearch.gif">
                    </asp:ImageButton>&nbsp;
                    <asp:ImageButton ID="imbReset" runat="server" CausesValidation="False" ImageUrl="../Images/ButtonReset.gif">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbBackMenu" runat="server" CausesValidation="False" ImageUrl="../Images/Buttonback.gif">
                    </asp:ImageButton>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="pnlAddEdit" runat="server">
        <table>
            <tr>
                <td>
                    MENAMBAHKAN REKENING BANK GROUP SUPPLIER -&nbsp;
                    <asp:Label ID="lblTitleAddEdit" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
         <table>
            <tr>
                <td>
                    REKENING BANK *)
                </td>
            </tr>
        </table>
        <table>
            <tr>
                <td>
                    <uc1:UcBankAccount id="UcBankAccount" runat="server"></uc1:UcBankAccount>
                </td>
            </tr>        
            <tr>
                <td>
                    Rekening Default untuk Pembayaran
                </td>
                <td>
                    <asp:CheckBox ID="checkDefault" runat="server" />
                    <asp:CustomValidator ID="CVcheckDefault" runat="server" 
                    ErrorMessage="Hanya bisa pilih satu Rekening Default"></asp:CustomValidator>
                </td>
            </tr>    
             <tr>
                <td>
                    Untuk Pembayaran
                </td>
                <td>
                    <asp:TextBox ID="txtForPayment" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                    ControlToValidate="txtForPayment"></asp:RequiredFieldValidator>
                </td>
            </tr> 
             <tr>
                <td>
                    Efektif Tanggal
                </td>
                <td>                     
                    <asp:TextBox ID="EffectiveDate" runat="server"></asp:TextBox>                   
                     <asp:CalendarExtender ID="EffectiveDate_CalendarExtender" runat="server" 
                         Enabled="True" TargetControlID="EffectiveDate" Format ="dd/MM/yyyy"></asp:CalendarExtender>
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                    ControlToValidate="EffectiveDate"></asp:RequiredFieldValidator>
                </td>
            </tr> 
        </table>
        <table>
            <tr>
                <td>
                    <asp:ImageButton ID="imbSave" runat="server" CausesValidation="true" ImageUrl="../Images/ButtonSave.gif">
                    </asp:ImageButton>&nbsp;
                    <asp:ImageButton ID="imbCancel" runat="server" CausesValidation="true" ImageUrl="../Images/ButtonCancel.gif">
                    </asp:ImageButton>&nbsp;
                    <asp:ImageButton ID="imbBack" runat="server" CausesValidation="true" ImageUrl="../Images/Buttonback.gif">
                    </asp:ImageButton>&nbsp;
                    <asp:ImageButton ID="imbClose" runat="server" CausesValidation="False" ImageUrl="../images/buttonclose.gif">
                    </asp:ImageButton>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <input id="hdnSupplierGroupID" type="hidden" name="hdnSupplierGroupID" runat="server" />
    <input id="hdnGroupSupplierAccount" type="hidden" name="hdnGroupSupplierAccount" runat="server" />
    </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
