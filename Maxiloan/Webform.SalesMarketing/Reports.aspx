﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Reports.aspx.vb" Inherits="Maxiloan.Webform.SalesMarketing.Reports" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%--<%@ Register Src="../Webform.UserController/ucRSViewer.ascx" TagName="ucRSViewer"
    TagPrefix="uc1" %>--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Reports</title>
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="sm1">
    </asp:ScriptManager>
    <%--<uc1:ucrsviewer id="ucRSViewer1" runat="server" />--%>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h4>
                FILTER REPORT</h4>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label class="label_general">
                Branch
            </label>
            <%--<asp:DropDownList runat="server" ID="cboBranch">                
            </asp:DropDownList>
            --%>
            <div class="checkbox_wrapper_ws">
                <asp:CheckBoxList runat="server" ID="chkBranch" RepeatDirection="Vertical" RepeatLayout="Table"
                    CssClass="opt_single">
                </asp:CheckBoxList>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Date
            </label>
            <asp:TextBox runat="server" ID="txtStartDate" CssClass="small_text"></asp:TextBox>
            <asp:CalendarExtender runat="server" ID="ce1" TargetControlID="txtStartDate" Format="dd/MM/yyyy">
            </asp:CalendarExtender>
            <label class="label_auto">
                s/d</label>
            <asp:TextBox runat="server" ID="txtEndDate" CssClass="small_text"></asp:TextBox>
            <asp:CalendarExtender runat="server" ID="ce2" TargetControlID="txtEndDate" Format="dd/MM/yyyy">
            </asp:CalendarExtender>
        </div>
    </div>
    <div class="form_button">
        <asp:Button runat="server" ID="btnView" Text="View Report" CssClass="small button blue" />
    </div>
    </form>
</body>
</html>
