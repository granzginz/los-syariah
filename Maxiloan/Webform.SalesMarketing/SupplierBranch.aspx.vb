﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.cbse
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class SupplierBranch
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private m_controller As New SupplierController
    Dim oSupplier As New Parameter.Supplier
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1

    Private recordCount As Int64 = 1
    Dim oRow As DataRow
    Protected WithEvents txtRefundBungaPersen As ucNumberFormat
    Protected WithEvents txtRefundBungaAmount As ucNumberFormat
    Protected WithEvents txtRefundPremiPersen As ucNumberFormat
    Protected WithEvents txtRefundPremiAmount As ucNumberFormat
    'Protected WithEvents txtRefundBungaMultiplier As ucNumberFormat
    Protected WithEvents txtRewardUnit As ucNumberFormat
    Protected WithEvents txtBukBonus As ucNumberFormat
    'Protected WithEvents txtRefundAdminPersen As ucNumberFormat
    'Protected WithEvents txtRefundAdminAmount As ucNumberFormat
    'Protected WithEvents txtRefundProvisiPersen As ucNumberFormat
    'Protected WithEvents txtRefundProvisiAmount As ucNumberFormat


#End Region
#Region "Property"
    Private Property BID() As String
        Get
            Return CType(ViewState("BID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("BID") = Value
        End Set
    End Property
    Private Property SupplierID() As String
        Get
            Return CType(ViewState("SupplierID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("SupplierID") = Value
        End Set
    End Property
    Private Property Name() As String
        Get
            Return CType(ViewState("Name"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Name") = Value
        End Set
    End Property
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property WhereCond2() As String
        Get
            Return CType(ViewState("WhereCond2"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("WhereCond2") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property
#End Region

    Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)

        For Each aControl As Control In Me.Controls
            Me.Page.ClientScript.RegisterForEventValidation(aControl.UniqueID.ToString)
        Next aControl

        MyBase.Render(writer)
    End Sub

#Region "Page Load & Bindgrid"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not Page.IsPostBack Then

            If CheckForm(Me.Loginid, "SupplierBranch", "MAXILOAN") Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            If Request("back") = "1" Then
                GetCookies()
            Else
                If Request("cond") <> "" Then
                    Me.CmdWhere = Request("cond")
                Else
                    Me.CmdWhere = "ALL"
                End If
                Me.SupplierID = Request("id")
                Me.Name = Request("name")
                Me.WhereCond2 = "SupplierID = '" & Me.SupplierID & "'"
            End If
            '            lnkID.Text = Me.SupplierID
            lblName.Text = Me.Name
            Me.Sort = "BranchFullName ASC"
            FillIncentiveCard()
            InitialPanel()
            BindGrid(Me.CmdWhere)
            rboIncentive.Attributes.Add("OnClick", "return Incentive();")
            'lnkID.Attributes.Add("OnClick", "return OpenWin('" & Me.SupplierID & "','Marketing');")
            lblName.NavigateUrl = "javascript:OpenWin('" & Me.SupplierID & "','Marketing');"
            'lblName.Attributes.Add("OnClick", "return OpenWin('" & Me.SupplierID & "','Marketing');")
        End If
    End Sub
#Region "GetCookies"
    Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies("Supplier")
        Me.CmdWhere = cookie.Values("where")
        Me.WhereCond2 = cookie.Values("where2")
        Me.Name = cookie.Values("name")
        Me.SupplierID = cookie.Values("id")
    End Sub
#End Region
    Sub BindGrid(ByVal cmdWhere As String)
        Dim dtEntity As DataTable
        Dim oCustomClass As New Parameter.Supplier
        oCustomClass.PageSize = pageSize
        oCustomClass.WhereCond = cmdWhere
        oCustomClass.SortBy = Me.Sort
        oCustomClass.WhereCond2 = Me.WhereCond2
        oCustomClass.CurrentPage = currentPage
        oCustomClass.strConnection = GetConnectionString()
        oCustomClass = m_controller.GetSupplierBranch(oCustomClass)
        If Not oCustomClass Is Nothing Then
            dtEntity = oCustomClass.ListData
            recordCount = oCustomClass.TotalRecords
        Else
            recordCount = 0
        End If
        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()
        PagingFooter()
    End Sub
    Sub InitialPanel()
        pnlList.Visible = True
        pnlAdd.Visible = False
        pnlEdit.Visible = False
        rgvGo.Enabled = True
        txtPage.Text = "1"
    End Sub
#End Region
#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGrid(Me.CmdWhere)
    End Sub
    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int32)
                BindGrid(Me.CmdWhere)
            End If
        End If
    End Sub
#End Region
#Region "Sorting"
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGrid(Me.CmdWhere)
    End Sub
#End Region
#Region "Reset-Search"
    Private Sub ButtonReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        cboSearch.SelectedIndex = 0
        txtSearch.Text = ""
        Me.CmdWhere = "ALL"
        BindGrid(Me.CmdWhere)
    End Sub
    Private Sub ButtonSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonSearch.Click
        If txtSearch.Text.Trim <> "" Then
            Dim Search As String
            Search = txtSearch.Text.Trim
            Me.CmdWhere = cboSearch.SelectedItem.Value + " = '" + Search + "'"
        Else
            Me.CmdWhere = "ALL"
        End If
        BindGrid(Me.CmdWhere)
    End Sub
#End Region
#Region "Add"
    Private Sub ButtonAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonAdd.Click
        pnlList.Visible = False
        pnlAdd.Visible = True
        rgvGo.Enabled = False
        ButtonOK.Visible = False
        BindAdd()
    End Sub
    Sub BindAdd()
        Dim oData As New DataTable
        Dim oCustomClass As New Parameter.Supplier
        oCustomClass.strConnection = GetConnectionString()
        oCustomClass.SupplierID = Me.SupplierID
        'lnkSupplierIDAdd.Text = Me.SupplierID
        lblNameAdd.Text = Me.Name
        lblNameAdd.NavigateUrl = "javascript:OpenWin('" & Me.SupplierID & "','Marketing');"

        oCustomClass = m_controller.GetSupplierBranchAdd(oCustomClass)
        If Not oCustomClass Is Nothing Then
            oData = oCustomClass.ListData
        End If
        dtgAdd.DataSource = oData.DefaultView
        dtgAdd.DataBind()
    End Sub
    Private Sub dtgAdd_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgAdd.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim chk As CheckBox
            chk = CType(e.Item.FindControl("chkAssign"), CheckBox)
            ' chk.Checked = True
            ' Indra
            chk.Checked = False
        End If
    End Sub
#End Region
#Region "Save & Back Add"
    Private Sub ButtonBackAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonBackAdd.Click
        InitialPanel()
    End Sub
    Private Sub ButtonSaveAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSaveAdd.Click
        Dim oSupplier As New Parameter.Supplier
        Dim oDataBranch As New DataTable
        Dim intLoop As Integer
        Dim chk As CheckBox

        oDataBranch.Columns.Add("BranchID", GetType(String))

        For intLoop = 0 To dtgAdd.Items.Count - 1
            chk = CType(dtgAdd.Items(intLoop).Cells(0).FindControl("chkAssign"), CheckBox)
            If chk.Checked Then
                oRow = oDataBranch.NewRow
                oRow("BranchID") = dtgAdd.DataKeys.Item(intLoop).ToString.Trim
                oDataBranch.Rows.Add(oRow)
            End If
        Next
        If oDataBranch.Rows.Count = 0 Then
            ShowMessage(lblMessage, "Harap pilih minimum 1 cabang .....", True)
            Exit Sub
        End If
        oSupplier.strConnection = GetConnectionString()
        oSupplier.SupplierID = Me.SupplierID
        oSupplier.BusinessDate = Me.BusinessDate
        m_controller.SupplierSaveBranch(oSupplier, oDataBranch)
        InitialPanel()
        txtPage.Text = "1"
        Me.Sort = "BranchFullName ASC"
        Me.CmdWhere = "ALL"
        BindGrid(Me.CmdWhere)
    End Sub
#End Region
#Region "BindEdit"
    Sub FillIncentiveCard()
        Dim oSupplierBranch As New Parameter.Supplier
        Dim oData As New DataTable
        oSupplierBranch.strConnection = GetConnectionString()
        oSupplierBranch = m_controller.GetSupplierBranchIncentiveCard(oSupplierBranch)
        oData = oSupplierBranch.ListData
        cboNew.DataSource = oData.DefaultView
        cboNew.DataTextField = oData.Columns(1).ToString.Trim
        cboNew.DataValueField = oData.Columns(0).ToString.Trim
        cboNew.DataBind()
        cboNew.Items.Insert(0, "Select One")
        cboNew.Items(0).Value = ""
        cboUsed.DataSource = oData.DefaultView
        cboUsed.DataTextField = oData.Columns(1).ToString.Trim
        cboUsed.DataValueField = oData.Columns(0).ToString.Trim
        cboUsed.DataBind()
        cboUsed.Items.Insert(0, "Select One")
        cboUsed.Items(0).Value = ""
        cboRO.DataSource = oData.DefaultView
        cboRO.DataTextField = oData.Columns(1).ToString.Trim
        cboRO.DataValueField = oData.Columns(0).ToString.Trim
        cboRO.DataBind()
        cboRO.Items.Insert(0, "Select One")
        cboRO.Items(0).Value = ""
    End Sub

    Public Function chknilai(ByVal nilai As String) As Boolean
        Dim ni As Boolean
        Dim value As Decimal = CDec(nilai)
        If value = 0 Then
            ni = False
        ElseIf value <> 0 Then
            ni = True
        End If
        Return ni
    End Function

    Sub BindEdit(ByVal BranchName As String)
        Dim oData As New DataTable
        Dim oData2 As New DataTable
        Dim oEntity As New Parameter.Supplier
        lblReqAO.Visible = True
        lblTitle.Text = "EDIT"
        EditView(False, True)
        lblBranchEdit.Text = BranchName
        ButtonOK.Visible = False
        pnlList.Visible = False
        pnlAdd.Visible = False
        pnlEdit.Visible = True
        rgvGo.Enabled = False
        ButtonSaveEdit.Visible = True
        oEntity.strConnection = GetConnectionString()
        oEntity.SupplierID = Me.SupplierID
        oEntity.BranchId = Me.BID

        Try
            oEntity = m_controller.GetSupplierBranchEdit(oEntity)
        Catch ex As Exception
            Exit Sub
        End Try


        oData = oEntity.ListData
        oData2 = oEntity.ListData2

        cboAO.DataSource = oData2.DefaultView
        cboAO.DataTextField = oData2.Columns(0).ToString.Trim
        cboAO.DataValueField = oData2.Columns(1).ToString.Trim
        cboAO.DataBind()
        cboAO.Items.Insert(0, "Select One")
        cboAO.Items(0).Value = ""

        oRow = oData.Rows(0)
        cboNew.ClearSelection()
        cboUsed.ClearSelection()
        cboRO.ClearSelection()
        cboAO.SelectedIndex = cboAO.Items.IndexOf(cboAO.Items.FindByValue(oRow(0).ToString))
        cboNew.SelectedIndex = cboNew.Items.IndexOf(cboNew.Items.FindByValue(oRow(1).ToString.Trim))
        cboUsed.SelectedIndex = cboUsed.Items.IndexOf(cboUsed.Items.FindByValue(oRow(2).ToString))
        cboRO.SelectedIndex = cboRO.Items.IndexOf(cboRO.Items.FindByValue(oRow(3).ToString))
        rboIncentive.SelectedIndex = rboIncentive.Items.IndexOf(rboIncentive.Items.FindByValue(oRow(4).ToString))

        If oRow(4).ToString.Trim = "P" Then
            txtFGM.Enabled = False
            txtFBM.Enabled = False
            txtFADH.Enabled = False
            txtFSalesSpv.Enabled = False
            txtFSalesPerson.Enabled = False
            txtFSuppAdm.Enabled = False
        Else
            txtPCompany.Enabled = False
            txtPGM.Enabled = False
            txtPBM.Enabled = False
            txtPADH.Enabled = False
            txtPSalesSpv.Enabled = False
            txtPSalesPerson.Enabled = False
            txtPSuppAdm.Enabled = False
        End If
        txtPCompany.Text = oRow(5).ToString.Trim
        txtPGM.Text = oRow(6).ToString.Trim
        txtPBM.Text = oRow(7).ToString.Trim
        txtPADH.Text = oRow(8).ToString.Trim
        txtPSalesSpv.Text = oRow(9).ToString.Trim
        txtPSalesPerson.Text = oRow(10).ToString.Trim
        txtPSuppAdm.Text = oRow(11).ToString.Trim
        txtFGM.Text = oRow(12).ToString.Trim
        txtFBM.Text = oRow(13).ToString.Trim
        txtFADH.Text = oRow(14).ToString.Trim
        txtFSalesSpv.Text = oRow(15).ToString.Trim
        txtFSalesPerson.Text = oRow(16).ToString.Trim
        txtFSuppAdm.Text = oRow(17).ToString.Trim
        txtRefundBungaPersen.Text = String.Format("{0:#,##0}", oRow("RefundInterestPercent"))
        txtRefundBungaAmount.Text = String.Format("{0:#,##0}", oRow("RefundInterestAmount"))
        txtRefundPremiPersen.Text = String.Format("{0:#,##0}", oRow("RefundPremiPercent"))
        txtRefundPremiAmount.Text = String.Format("{0:#,##0}", oRow("RefundPremiAmount"))

        chkRefundBungaPersen.Checked = chknilai(oRow("RefundInterestPercent").ToString)
        chkRefundBungaAmount.Checked = chknilai(oRow("RefundInterestAmount").ToString)
        chkRefundPremiPersen.Checked = chknilai(oRow("RefundPremiPercent").ToString)
        chkRefundPremiAmount.Checked = chknilai(oRow("RefundPremiAmount").ToString)

        txtRefundBungaPersen.Enabled = chknilai(oRow("RefundInterestPercent").ToString)
        txtRefundBungaAmount.Enabled = chknilai(oRow("RefundInterestAmount").ToString)
        txtRefundPremiPersen.Enabled = chknilai(oRow("RefundPremiPercent").ToString)
        txtRefundPremiAmount.Enabled = chknilai(oRow("RefundPremiAmount").ToString)

        'txtRefundBungaMultiplier.Text = oRow.Item("RefundInterestMultiply").ToString.Trim
        txtRewardUnit.Text = String.Format("{0:#,##0}", oRow("RewardUnit"))
        txtBukBonus.Text = String.Format("{0:#,##0}", oRow("BukBonus"))

        rboEUnit.ClearSelection()
        rboEAF.ClearSelection()
        rboERefund.ClearSelection()
        rboCUnit.ClearSelection()
        rboCAF.ClearSelection()
        rboCRefund.ClearSelection()

        rboEUnit.SelectedIndex = rboEUnit.Items.IndexOf(rboEUnit.Items.FindByValue(oRow(18).ToString))

        rboEAF.SelectedIndex = rboEAF.Items.IndexOf(rboEAF.Items.FindByValue(oRow(19).ToString))

        rboERefund.SelectedIndex = rboERefund.Items.IndexOf(rboERefund.Items.FindByValue(oRow(20).ToString))
        'rboERefund.Items.FindByValue(oRow(20).ToString.Trim).Selected = True

        rboCUnit.SelectedIndex = rboCUnit.Items.IndexOf(rboCUnit.Items.FindByValue(oRow(21).ToString))
        'rboCUnit.Items.FindByValue(oRow(21).ToString.Trim).Selected = True

        rboCAF.SelectedIndex = rboCAF.Items.IndexOf(rboCAF.Items.FindByValue(oRow(22).ToString))
        'rboCAF.Items.FindByValue(oRow(22).ToString.Trim).Selected = True

        rboCRefund.SelectedIndex = rboCRefund.Items.IndexOf(rboCRefund.Items.FindByValue(oRow(23).ToString))


        'cboRefundBUnga.SelectedIndex = cboRefundBUnga.Items.IndexOf(cboRefundBUnga.Items.FindByValue(oRow("RefundInterestBy").Replace(" ", "")))
        cbo_RefundPremi.SelectedIndex = cbo_RefundPremi.Items.IndexOf(cbo_RefundPremi.Items.FindByValue(oRow("RefundPremiBy").Replace(" ", "")))

        'cboRefundBUnga.SelectedIndex = cboRefundBUnga.Items.IndexOf(cboRefundBUnga.Items.FindByValue(.RefundBungaBy.Replace(" ", "")))

        grdRefundBunga.DataSource = ViewBranchRefund("NPV").DefaultView
        grdRefundBunga.DataBind()

        grdRefundPremi.DataSource = ViewBranchRefund("TDI").DefaultView
        grdRefundPremi.DataBind()

    End Sub

    Public Function ViewBranchRefund(ByVal Where As String) As DataTable
        Dim whr As String = " sbr.BranchID = '" & Me.BID & "' and sbr.SupplierID='" & Me.SupplierID & "' and sbr.AlokasiType='" & Where & "' where es.ID IN ('SL','SO','BM','AM','GM','OT','SV','FM') "
        Dim oEntityBranchRefund As New Parameter.Supplier
        oEntityBranchRefund.strConnection = GetConnectionString()
        oEntityBranchRefund.WhereCond = whr
        Dim dtBranchRefund As DataTable
        dtBranchRefund = m_controller.GetSupplierBranchRefund(oEntityBranchRefund).ListData
        Return dtBranchRefund
    End Function

#End Region
#Region "Visible"
    Sub EditView(ByVal Label As Boolean, ByVal Text As Boolean)
        lblAO.Visible = Label
        lblNew.Visible = Label
        lblUsed.Visible = Label
        lblRO.Visible = Label
        lblIncentive.Visible = Label
        lblFGM.Visible = Label
        lblFBM.Visible = Label
        lblFADH.Visible = Label
        lblFSalesSpv.Visible = Label
        lblFSalesPerson.Visible = Label
        lblFSuppAdm.Visible = Label
        lblPCompany.Visible = Label
        lblPGM.Visible = Label
        lblPBM.Visible = Label
        lblPADH.Visible = Label
        lblPSalesSpv.Visible = Label
        lblPSalesPerson.Visible = Label
        lblPSuppAdm.Visible = Label
        lblPCompany.Visible = Label
        lblPGM.Visible = Label
        lblPBM.Visible = Label
        lblPADH.Visible = Label
        lblPSalesSpv.Visible = Label
        lblPSalesPerson.Visible = Label
        lblPSuppAdm.Visible = Label
        lblFGM.Visible = Label
        lblFBM.Visible = Label
        lblFADH.Visible = Label
        lblFSalesSpv.Visible = Label
        lblFSalesPerson.Visible = Label
        lblFSuppAdm.Visible = Label
        lblEUnit.Visible = Label
        lblEAF.Visible = Label
        lblERefund.Visible = Label
        lblCUnit.Visible = Label
        lblCAF.Visible = Label
        lblCRefund.Visible = Label

        cboAO.Visible = Text
        cboNew.Visible = Text
        cboUsed.Visible = Text
        cboRO.Visible = Text
        rboIncentive.Visible = Text
        txtFGM.Visible = Text
        txtFBM.Visible = Text
        txtFADH.Visible = Text
        txtFSalesSpv.Visible = Text
        txtFSalesPerson.Visible = Text
        txtFSuppAdm.Visible = Text
        txtPCompany.Visible = Text
        txtPGM.Visible = Text
        txtPBM.Visible = Text
        txtPADH.Visible = Text
        txtPSalesSpv.Visible = Text
        txtPSalesPerson.Visible = Text
        txtPSuppAdm.Visible = Text
        txtPCompany.Visible = Text
        txtPGM.Visible = Text
        txtPBM.Visible = Text
        txtPADH.Visible = Text
        txtPSalesSpv.Visible = Text
        txtPSalesPerson.Visible = Text
        txtPSuppAdm.Visible = Text
        txtFGM.Visible = Text
        txtFBM.Visible = Text
        txtFADH.Visible = Text
        txtFSalesSpv.Visible = Text
        txtFSalesPerson.Visible = Text
        txtFSuppAdm.Visible = Text
        rboEUnit.Visible = Text
        rboEAF.Visible = Text
        rboERefund.Visible = Text
        rboCUnit.Visible = Text
        rboCAF.Visible = Text
        rboCRefund.Visible = Text
    End Sub
#End Region
#Region "View"
    Sub BindView(ByVal BranchID As String)
        lblReqAO.Visible = False
        lblTitle.Text = "VIEW"
        EditView(True, False)
        lblBranchEdit.Text = BranchName
        pnlList.Visible = False
        pnlAdd.Visible = False
        pnlEdit.Visible = True
        ButtonSaveEdit.Visible = False
        ButtonCancelEdit.Visible = False
        ButtonOK.Visible = True
        Dim oData As New DataTable
        Dim oSupplier As New Parameter.Supplier
        oSupplier.Title = "SupplierBranch"
        oSupplier.WhereCond = "BranchID = '" & BranchID & "'"
        oSupplier.WhereCond2 = Me.WhereCond2
        oSupplier.strConnection = GetConnectionString()
        oSupplier = m_controller.GetSupplierReport(oSupplier)
        If Not oSupplier Is Nothing Then
            oData = oSupplier.ListData
        End If
        If oData.Rows.Count > 0 Then
            oRow = oData.Rows(0)
            lblAO.Text = oRow(1).ToString
            lblNew.Text = oRow(2).ToString
            lblUsed.Text = oRow(3).ToString
            lblRO.Text = oRow(4).ToString
            lblIncentive.Text = oRow(5).ToString
            lblPCompany.Text = oRow(6).ToString
            lblPGM.Text = oRow(7).ToString
            lblPBM.Text = oRow(8).ToString
            lblPADH.Text = oRow(9).ToString
            lblPSalesSpv.Text = oRow(10).ToString
            lblPSalesPerson.Text = oRow(11).ToString
            lblPSuppAdm.Text = oRow(12).ToString
            lblFGM.Text = oRow(13).ToString
            lblFBM.Text = oRow(14).ToString
            lblFADH.Text = oRow(15).ToString
            lblFSalesSpv.Text = oRow(16).ToString
            lblFSalesPerson.Text = oRow(17).ToString
            lblFSuppAdm.Text = oRow(18).ToString
            lblEUnit.Text = oRow(19).ToString.Trim
            lblEAF.Text = oRow(20).ToString.Trim
            lblERefund.Text = oRow(21).ToString.Trim
            lblCUnit.Text = oRow(22).ToString.Trim
            lblCAF.Text = oRow(23).ToString.Trim
            lblCRefund.Text = oRow(24).ToString.Trim
            lnkBudget.NavigateUrl = "javascript:OpenWinSupplier('" & Server.UrlEncode(BranchID) & "',' " & Me.SupplierID & "')"
        End If
    End Sub
#End Region
#Region "ItemCommand"
    Private Sub dtgPaging_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        If e.CommandName = "Edit" Then
            If CheckFeature(Me.Loginid, "SupplierBranch", "Edit", "MAXILOAN") Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            'FillIncentiveCard()
            Me.BID = CType(e.Item.FindControl("hplBranchID"), LinkButton).Text.Trim
            BindEdit(e.Item.Cells(3).Text.Trim)
            divRefound.Visible = True
            divinsentiv.Visible = False
        ElseIf e.CommandName = "Incentive" Then
            If CheckFeature(Me.Loginid, "SupplierBranch", "Edit", "MAXILOAN") Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            'FillIncentiveCard()
            Me.BID = CType(e.Item.FindControl("hplBranchID"), LinkButton).Text.Trim
            BindEdit(e.Item.Cells(3).Text.Trim)
            divRefound.Visible = False
            divinsentiv.Visible = True
        ElseIf e.CommandName = "Delete" Then
            If CheckFeature(Me.Loginid, "SupplierBranch", "Del", "MAXILOAN") Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            Dim oSupplierBranch As New Parameter.Supplier
            oSupplierBranch.SupplierID = Me.SupplierID
            oSupplierBranch.BranchId = CType(e.Item.FindControl("hplBranchID"), LinkButton).Text.Trim
            oSupplierBranch.strConnection = GetConnectionString()
            Dim Err As String
            Err = m_controller.GetSupplierBranchDelete(oSupplierBranch)
            If Err = "" Then
                InitialPanel()
                txtPage.Text = "1"
                Me.Sort = "BranchFullName ASC"
                BindGrid(Me.CmdWhere)
            Else
                ShowMessage(lblMessage, Err, True)
            End If
        ElseIf e.CommandName = "BranchID" Then
            BindView(CType(e.Item.FindControl("hplBranchID"), LinkButton).Text.Trim)
        ElseIf e.CommandName = "LnkStatement" Then
            Dim cookie As HttpCookie = Request.Cookies("Supplier")
            Dim BranchID As String
            BranchID = CType(e.Item.FindControl("hplBranchID"), LinkButton).Text.Trim
            If Not cookie Is Nothing Then
                cookie.Values("SupplierId") = Me.SupplierID
                cookie.Values("BranchId") = BranchID
                cookie.Values("Name") = Me.Name
                Response.AppendCookie(cookie)
            Else
                Dim cookieNew As New HttpCookie("Supplier")
                cookieNew.Values.Add("SupplierId", Me.SupplierID)
                cookieNew.Values.Add("BranchId", BranchID)
                cookieNew.Values.Add("Name", Me.Name)
                Response.AppendCookie(cookieNew)
            End If
            Response.Redirect("RptStatementSalesViewer.aspx")
        End If
    End Sub
#End Region

#Region "Save & Cancel Edit"
    Private Sub ButtonCancelEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCancelEdit.Click
        InitialPanel()
    End Sub
    Private Sub ButtonSaveEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSaveEdit.Click
        'Dim oSupplier As New Parameter.Supplier
        'Dim TotalP As Double

        'oSupplier.SupplierID = Me.SupplierID
        'oSupplier.BranchId = Me.BID
        'oSupplier.AOID = cboAO.SelectedValue
        'oSupplier.NewVehicleIC = cboNew.SelectedValue
        'oSupplier.UsedVehicleIC = cboUsed.SelectedValue
        'oSupplier.ROIC = cboRO.SelectedValue
        'oSupplier.IncentiveDistributionMethod = rboIncentive.SelectedValue
        'If rboIncentive.SelectedValue = "P" Then
        '    '  TotalP = CDbl(IIf(IsDBNull(txtPCompany.Text), 0, txtPCompany.Text)) + _
        '    'CDbl(IIf(IsDBNull(txtPGM.Text), 0, txtPGM.Text)) + _
        '    'CDbl(IIf(IsDBNull(txtPBM.Text), 0, txtPBM.Text)) + _
        '    'CDbl(IIf(IsDBNull(txtPADH.Text), 0, txtPADH.Text)) + _
        '    'CDbl(IIf(IsDBNull(txtPSalesSpv.Text), 0, txtPSalesSpv.Text)) + _
        '    'CDbl(IIf(IsDBNull(txtPSalesPerson.Text), 0, txtPSalesPerson.Text)) + _
        '    'CDbl(IIf(IsDBNull(txtPSuppAdm.Text), 0, txtPSuppAdm.Text))
        '    '  If TotalP <> 100 Then
        '    '      ShowMessage(lblMessage, "Total prosentase harus 100", True)
        '    '      Exit Sub
        '    '  End If
        '    oSupplier.CompanyPercentage = CDec(IIf(txtPCompany.Text.Trim <> "", txtPCompany.Text.Trim, "0"))
        '    oSupplier.GMPercentage = CDec(IIf(txtPGM.Text.Trim <> "", txtPGM.Text.Trim, "0"))
        '    oSupplier.BMPercentage = CDec(IIf(txtPBM.Text.Trim <> "", txtPBM.Text.Trim, "0"))
        '    oSupplier.ADHPercentage = CDec(IIf(txtPADH.Text.Trim <> "", txtPADH.Text.Trim, "0"))
        '    oSupplier.SPVPercentage = CDec(IIf(txtPSalesSpv.Text.Trim <> "", txtPSalesSpv.Text.Trim, "0"))
        '    oSupplier.SPPercentage = CDec(IIf(txtPSalesPerson.Text.Trim <> "", txtPSalesPerson.Text.Trim, "0"))
        '    oSupplier.ADPercentage = CDec(IIf(txtPSuppAdm.Text.Trim <> "", txtPSuppAdm.Text.Trim, "0"))
        '    oSupplier.GMAmount = 0
        '    oSupplier.BMAmount = 0
        '    oSupplier.ADHAmount = 0
        '    oSupplier.SPVAmount = 0
        '    oSupplier.SPAmount = 0
        '    oSupplier.ADAmount = 0
        'ElseIf rboIncentive.SelectedValue = "F" Then
        '    oSupplier.CompanyPercentage = 0
        '    oSupplier.GMPercentage = 0
        '    oSupplier.BMPercentage = 0
        '    oSupplier.ADHPercentage = 0
        '    oSupplier.SPVPercentage = 0
        '    oSupplier.SPPercentage = 0
        '    oSupplier.ADPercentage = 0
        '    oSupplier.GMAmount = CDec(IIf(txtFGM.Text.Trim <> "", txtFGM.Text.Trim, "0"))
        '    oSupplier.BMAmount = CDec(IIf(txtFBM.Text.Trim <> "", txtFBM.Text.Trim, "0"))
        '    oSupplier.ADHAmount = CDec(IIf(txtFADH.Text.Trim <> "", txtFADH.Text.Trim, "0"))
        '    oSupplier.SPVAmount = CDec(IIf(txtFSalesSpv.Text.Trim <> "", txtFSalesSpv.Text.Trim, "0"))
        '    oSupplier.SPAmount = CDec(IIf(txtFSalesPerson.Text.Trim <> "", txtFSalesPerson.Text.Trim, "0"))
        '    oSupplier.ADAmount = CDec(IIf(txtFSuppAdm.Text.Trim <> "", txtFSuppAdm.Text.Trim, "0"))
        'End If
        'oSupplier.EmployeeUnitWayICPayment = rboEUnit.SelectedValue
        'oSupplier.EmployeeAFWayICPayment = rboEAF.SelectedValue
        'oSupplier.EmployeeRAWayICPayment = rboERefund.SelectedValue
        'oSupplier.CompanyUnitWayICPayment = rboCUnit.SelectedValue
        'oSupplier.CompanyAFWayICPayment = rboCAF.SelectedValue
        'oSupplier.CompanyRAWayICPayment = rboCRefund.SelectedValue
        'oSupplier.strConnection = GetConnectionString()
        'm_controller.SupplierBranchSaveEdit(oSupplier)

        'Dim oSupplier As New Parameter.Supplier
        'oSupplier.BranchId = Me.BID
        'oSupplier.SupplierID = Me.SupplierID
        'oSupplier.CMO = cboAO.SelectedValue
        'oSupplier.RefundInterestPercent = txtRefundBungaPersen.Text
        'oSupplier.RefundInterestAmount = txtRefundBungaAmount.Text
        'oSupplier.RefundPremiPercent = txtRefundPremiPersen.Text
        'oSupplier.RefundPremiAmount = txtRefundPremiAmount.Text
        ''oSupplier.RefundAdminPercent = txtRefundAdminPersen.Text
        ''oSupplier.RefundAdminAmount = txtRefundAdminAmount.Text
        ''oSupplier.RefundProvisiPercent = txtRefundProvisiPersen.Text
        ''oSupplier.RefundProvisiAmount = txtRefundProvisiAmount.Text

        'oSupplier.strConnection = GetConnectionString()

        ''For Each DemoGridItem In grdRefundBunga.Items


        ''    'Dim lblCountryCode As Label = CType(DemoGridItem.Item.FindControl("lblCountryCode"), Label)
        ''    oSupplier.SupplierEmployeePositionId = DemoGridItem.Cells(1).Controls(1).ToString()

        ''Next
        ''m_controller.SupplierBranchRefundHeaderSaveEdit(oSupplier)
        ''For i = 0 To grdRefundBunga.Items.Count - 1
        ''    'oSupplier.SupplierEmployeePositionId = grdRefundBunga.Items(i).FindControl("txtInfo").ToString()
        ''    'Dim lblCountryCode As Label = CType(grdRefundBunga.Item(i).FindControl("lblCountryCode"), Label)

        ''Next
        'oSupplier.strConnection = GetConnectionString()
        'For n As Integer = 0 To grdRefundBunga.DataKeys.Count - 1
        '    oSupplier.AlokasiType = "NPV"
        '    oSupplier.SupplierEmployeePositionId = grdRefundBunga.DataKeys(n).ToString()
        '    Dim BungaPersen As ucNumberFormat = CType(grdRefundBunga.Items(n).FindControl("txtBungaPersen"), ucNumberFormat)
        '    Dim BungaAmount As ucNumberFormat = CType(grdRefundBunga.Items(n).FindControl("txtBungaAmount"), ucNumberFormat)
        '    oSupplier.NilaiPersentase = BungaPersen.Text
        '    oSupplier.NilaiAmount = BungaAmount.Text
        '    m_controller.SupplierBranchRefundSaveEdit(oSupplier)
        'Next

        'For n As Integer = 0 To grdRefundPremi.DataKeys.Count - 1
        '    oSupplier.AlokasiType = "TDI"
        '    oSupplier.SupplierEmployeePositionId = grdRefundPremi.DataKeys(n).ToString()
        '    Dim PremiPersen As ucNumberFormat = CType(grdRefundPremi.Items(n).FindControl("txtPremiPersen"), ucNumberFormat)
        '    Dim PremiAmount As ucNumberFormat = CType(grdRefundPremi.Items(n).FindControl("txtPremiAmount"), ucNumberFormat)
        '    oSupplier.NilaiPersentase = PremiPersen.Text
        '    oSupplier.NilaiAmount = PremiAmount.Text
        '    m_controller.SupplierBranchRefundSaveEdit(oSupplier)
        'Next

        'For n As Integer = 0 To grdRefundAdmin.DataKeys.Count - 1
        '    oSupplier.AlokasiType = "TDS"
        '    oSupplier.SupplierEmployeePositionId = grdRefundAdmin.DataKeys(n).ToString()
        '    Dim AdminPersen As ucNumberFormat = CType(grdRefundAdmin.Items(n).FindControl("txtAdminPersen"), ucNumberFormat)
        '    Dim AdminAmount As ucNumberFormat = CType(grdRefundAdmin.Items(n).FindControl("txtAdminAmount"), ucNumberFormat)
        '    oSupplier.NilaiPersentase = AdminPersen.Text
        '    oSupplier.NilaiAmount = AdminAmount.Text
        '    m_controller.SupplierBranchRefundSaveEdit(oSupplier)
        'Next

        'For n As Integer = 0 To grdRefundProvisi.DataKeys.Count - 1
        '    oSupplier.AlokasiType = "PRS"
        '    oSupplier.SupplierEmployeePositionId = grdRefundProvisi.DataKeys(n).ToString()
        '    Dim ProvisiPersen As ucNumberFormat = CType(grdRefundProvisi.Items(n).FindControl("txtProvisiPersen"), ucNumberFormat)
        '    Dim ProvisiAmount As ucNumberFormat = CType(grdRefundProvisi.Items(n).FindControl("txtProvisiAmount"), ucNumberFormat)
        '    oSupplier.NilaiPersentase = ProvisiPersen.Text
        '    oSupplier.NilaiAmount = ProvisiAmount.Text
        '    m_controller.SupplierBranchRefundSaveEdit(oSupplier)
        'Next

        'm_controller.SupplierBranchRefundHeaderSaveEdit(oSupplier)
        'InitialPanel()
        'txtPage.Text = "1"
        'Me.Sort = "BranchFullName ASC"
        'BindGrid(Me.CmdWhere)

        Dim oSupplier As New Parameter.Supplier
        Dim chk As CheckBox
        oSupplier.BranchId = Me.BID
        oSupplier.SupplierID = Me.SupplierID
        oSupplier.CMO = cboAO.SelectedValue
        oSupplier.RefundInterestPercent = txtRefundBungaPersen.Text
        oSupplier.RefundInterestAmount = txtRefundBungaAmount.Text
        oSupplier.RefundPremiPercent = txtRefundPremiPersen.Text
        oSupplier.RefundPremiAmount = txtRefundPremiAmount.Text
        'modify by nofi date 30Jan2018 hilangkan cboRefundBUnga dan txtRefundBungaMultiplier
        'oSupplier.RefundBungaBy = cboRefundBUnga.SelectedValue
        'oSupplier.RefundBungaMultiplier = txtRefundBungaMultiplier.Text
        oSupplier.RefundPremiBy = cbo_RefundPremi.SelectedValue
        oSupplier.RewardUnit = txtRewardUnit.Text
        oSupplier.BukBonus = txtBukBonus.Text

        If chkRefundBungaAmount.Checked = True Then
            oSupplier.RefundInterestAlokasi = "A"
        ElseIf chkRefundBungaPersen.Checked = True Then
            oSupplier.RefundInterestAlokasi = "P"
        End If

        If chkRefundPremiAmount.Checked = True Then
            oSupplier.RefundPremiAlokasi = "A"
        ElseIf chkRefundPremiPersen.Checked = True Then
            oSupplier.RefundPremiAlokasi = "P"
        End If

        oSupplier.strConnection = GetConnectionString()
        For n As Integer = 0 To grdRefundBunga.DataKeys.Count - 1
            oSupplier.AlokasiType = "NPV"
            oSupplier.SupplierEmployeePositionId = grdRefundBunga.DataKeys(n).ToString()
            Dim BungaPersen As ucNumberFormat = CType(grdRefundBunga.Items(n).FindControl("txtBungaPersen"), ucNumberFormat)
            Dim BungaAmount As ucNumberFormat = CType(grdRefundBunga.Items(n).FindControl("txtBungaAmount"), ucNumberFormat)
            oSupplier.NilaiPersentase = BungaPersen.Text
            oSupplier.NilaiAmount = BungaAmount.Text
            m_controller.SupplierBranchRefundSaveEdit(oSupplier)
        Next

        For n As Integer = 0 To grdRefundPremi.DataKeys.Count - 1
            oSupplier.AlokasiType = "TDI"
            oSupplier.SupplierEmployeePositionId = grdRefundPremi.DataKeys(n).ToString()
            Dim PremiPersen As ucNumberFormat = CType(grdRefundPremi.Items(n).FindControl("txtPremiPersen"), ucNumberFormat)
            Dim PremiAmount As ucNumberFormat = CType(grdRefundPremi.Items(n).FindControl("txtPremiAmount"), ucNumberFormat)
            oSupplier.NilaiPersentase = PremiPersen.Text
            oSupplier.NilaiAmount = PremiAmount.Text
            m_controller.SupplierBranchRefundSaveEdit(oSupplier)
        Next

        m_controller.SupplierBranchRefundHeaderSaveEdit(oSupplier)
        InitialPanel()
        txtPage.Text = "1"
        Me.Sort = "BranchFullName ASC"
        BindGrid(Me.CmdWhere)
    End Sub
#End Region
#Region "ButtonBack"
    Private Sub ButtonBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonBack.Click
        Response.Redirect(Request("apps") & ".aspx?currentPage=" & Request("currentPage") & "")
    End Sub
#End Region
#Region "ItemDataBound"
    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        Dim hypBudget As HyperLink
        Dim hypForeCast As HyperLink
        Dim imbDelete As ImageButton
        Dim hplBranchID As LinkButton
        If e.Item.ItemIndex >= 0 Then

            imbDelete = CType(e.Item.FindControl("imgDelete"), ImageButton)
            hypBudget = CType(e.Item.FindControl("hypBudget"), HyperLink)
            hypForeCast = CType(e.Item.FindControl("hypForeCast"), HyperLink)
            hplBranchID = CType(e.Item.FindControl("hplBranchID"), LinkButton)

            hypForeCast.NavigateUrl = "SupplierForecast.aspx?id=" & Me.SupplierID.Trim & _
                                     "&name=" & Me.Name.Trim & "&branchid=" & hplBranchID.Text.Trim

            hypBudget.NavigateUrl = "SupplierBudget.aspx?id=" & Me.SupplierID.Trim & _
                                     "&name=" & Me.Name.Trim & "&branchid=" & hplBranchID.Text.Trim

            imbDelete.Attributes.Add("OnClick", "return DeleteConfirm();")
        End If
    End Sub
#End Region
#Region "ButtonOK"
    Private Sub ButtonOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonOK.Click
        InitialPanel()
    End Sub
#End Region

    Protected Sub grdRefundBunga_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grdRefundBunga.ItemDataBound


        If e.Item.ItemType = DataControlRowType.Footer Then
            Dim txtTotalBungaPersen As TextBox = DirectCast(e.Item.FindControl("txtTotalBungaPersen"), TextBox)
            txtTotalBungaPersen.Text = txtRefundBungaPersen.Text

            Dim txtTotalBungaAmount As TextBox = DirectCast(e.Item.FindControl("txtTotalBungaAmount"), TextBox)
            txtTotalBungaAmount.Text = txtRefundBungaAmount.Text
        End If



    End Sub


    Protected Sub grdRefundPremi_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grdRefundPremi.ItemDataBound

        If e.Item.ItemType = DataControlRowType.Footer Then
            Dim txtTotalPremiPersen As TextBox = DirectCast(e.Item.FindControl("txtTotalPremiPersen"), TextBox)
            txtTotalPremiPersen.Text = txtRefundPremiPersen.Text

            Dim txtTotalPremiAmount As TextBox = DirectCast(e.Item.FindControl("txtTotalPremiAmount"), TextBox)
            txtTotalPremiAmount.Text = txtRefundPremiAmount.Text
        End If
    End Sub


    'Protected Sub grdRefundAdmin_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grdRefundAdmin.ItemDataBound

    '    If e.Item.ItemType = DataControlRowType.Footer Then
    '        Dim txtTotalAdminPersen As TextBox = DirectCast(e.Item.FindControl("txtTotalAdminPersen"), TextBox)
    '        txtTotalAdminPersen.Text = txtRefundAdminPersen.Text

    '        Dim txtTotalAdminAmount As TextBox = DirectCast(e.Item.FindControl("txtTotalAdminAmount"), TextBox)
    '        txtTotalAdminAmount.Text = txtRefundAdminAmount.Text
    '    End If
    'End Sub


    'Protected Sub grdRefundProvisi_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grdRefundProvisi.ItemDataBound

    '    If e.Item.ItemType = DataControlRowType.Footer Then
    '        Dim txtTotalProvisiPersen As TextBox = DirectCast(e.Item.FindControl("txtTotalProvisiPersen"), TextBox)
    '        txtTotalProvisiPersen.Text = txtRefundProvisiPersen.Text

    '        Dim txtTotalProvisiAmount As TextBox = DirectCast(e.Item.FindControl("txtTotalProvisiAmount"), TextBox)
    '        txtTotalProvisiAmount.Text = txtRefundProvisiAmount.Text
    '    End If
    'End Sub


End Class