﻿#Region "Imports"
Imports Maxiloan
Imports Maxiloan.Exceptions
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
#End Region

Public Class NegativeCustomerView
    Inherits Maxiloan.Webform.WebBased

    Private oController As New NegativeCustomerController
    Private oCustomclass As New Parameter.NegativeCustomer

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load, Me.Load
        If Not IsPostBack Then
            If SessionInvalid() Then
                Exit Sub
            End If
            ViewData(Request.QueryString("NegativeCustomerID"))            
        End If        
    End Sub

    Private Sub ViewData(ByVal strID As String)
        Dim odatatable As New DataTable
        With oCustomclass
            .strConnection = GetConnectionString
            .NegativeCustomerID = strID
            .strkey = "View"
        End With
        oCustomclass = oController.NegativeCustomerView(oCustomclass)
        odatatable = oCustomclass.ListNegative
        If Not IsDBNull(odatatable.Rows(0)("CustomerName")) Then
            lblName.Text = CType(odatatable.Rows(0)("CustomerName"), String)
        End If
        If Not IsDBNull(odatatable.Rows(0)("IDType")) Then
            lblIDType.Text = CType(odatatable.Rows(0)("IDType"), String)
        End If
        If Not IsDBNull(odatatable.Rows(0)("IDNumber")) Then
            lblIDNumber.Text = CType(odatatable.Rows(0)("IDNumber"), String)
        End If
        If Not IsDBNull(odatatable.Rows(0)("Birth")) Then
            lblBirth.Text = CType(odatatable.Rows(0)("Birth"), String)
        End If
        If Not IsDBNull(odatatable.Rows(0)("LegalADdress")) Then
            lblAddress.Text = CType(odatatable.Rows(0)("LegalADdress"), String)
        End If
        If Not IsDBNull(odatatable.Rows(0)("LegalRT")) Then
            lblRT.Text = CType(odatatable.Rows(0)("LegalRT"), String)
        End If
        If Not IsDBNull(odatatable.Rows(0)("LegalRW")) Then
            lblRW.Text = CType(odatatable.Rows(0)("LegalRW"), String)
        End If
        If Not IsDBNull(odatatable.Rows(0)("LegalKelurahan")) Then
            lblKelurahan.Text = CType(odatatable.Rows(0)("LegalKelurahan"), String)
        End If
        If Not IsDBNull(odatatable.Rows(0)("LegalKecamatan")) Then
            lblKecamatan.Text = CType(odatatable.Rows(0)("LegalKecamatan"), String)
        End If
        If Not IsDBNull(odatatable.Rows(0)("LegalCity")) Then
            lblCity.Text = CType(odatatable.Rows(0)("LegalCity"), String)
        End If
        If Not IsDBNull(odatatable.Rows(0)("LegalZipCode")) Then
            lblZipCode.Text = CType(odatatable.Rows(0)("LegalZipCode"), String)
        End If
        If Not IsDBNull(odatatable.Rows(0)("Phone1")) Then
            lblPhone1.Text = CType(odatatable.Rows(0)("Phone1"), String)
        End If
        If Not IsDBNull(odatatable.Rows(0)("Phone2")) Then
            lblPhone2.Text = CType(odatatable.Rows(0)("Phone2"), String)
        End If
        If Not IsDBNull(odatatable.Rows(0)("FaxNo")) Then
            lblFaxNo.Text = CType(odatatable.Rows(0)("FaxNo"), String)
        End If
        If Not IsDBNull(odatatable.Rows(0)("MobilePhone")) Then
            lblMobilePhone.Text = CType(odatatable.Rows(0)("MobilePhone"), String)
        End If
        If Not IsDBNull(odatatable.Rows(0)("Email")) Then
            lblEmail.Text = CType(odatatable.Rows(0)("Email"), String)
        End If
        If Not IsDBNull(odatatable.Rows(0)("BadType")) Then
            lblBadType.Text = CType(odatatable.Rows(0)("BadType"), String)
        End If
        If Not IsDBNull(odatatable.Rows(0)("DataSource")) Then
            lblDataSource.Text = CType(odatatable.Rows(0)("DataSource"), String)
        End If
        If Not IsDBNull(odatatable.Rows(0)("isActive")) Then
            lblisActive.Text = CType(odatatable.Rows(0)("isActive"), String)
        End If
        If Not IsDBNull(odatatable.Rows(0)("NumOfSKT")) Then
            lblSKT.Text = CType(odatatable.Rows(0)("NumOfSKT"), String)
        End If
        If Not IsDBNull(odatatable.Rows(0)("NumOfRejectAfterSurvey")) Then
            lblAfter.Text = CType(odatatable.Rows(0)("NumOfRejectAfterSurvey"), String)
        End If
        If Not IsDBNull(odatatable.Rows(0)("NumOfRejectBeforeSurvey")) Then
            lblBefore.Text = CType(odatatable.Rows(0)("NumOfRejectBeforeSurvey"), String)
        End If
        If Not IsDBNull(odatatable.Rows(0)("Notes")) Then
            lblNotes.Text = CType(odatatable.Rows(0)("Notes"), String)
        End If

    End Sub

    'Protected Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imbCancel.Click
    '    Response.Redirect("NegativeCustomer.aspx")
    'End Sub
End Class