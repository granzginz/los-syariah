﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewSupplierOwnerId.aspx.vb"
    Inherits="Maxiloan.Webform.SalesMarketing.ViewSupplierOwnerId" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ViewSupplierOwnerId</title>
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />
    <script type="text/javascript" language="JavaScript">
        function fClose() {
            window.close();
            return false;
        }			
	</script>
</head>
<body>
    <form id="form1" runat="server">    
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">                         
                <h3>
                    VIEW - PEMILIK SUPPLIER
                </h3>
            </div>
        </div> 
        <div class="form_box">
	        <div class="form_single">
                <label>Nama</label>
                <asp:Label ID="lblName" runat="server"></asp:Label>
                <asp:Label ID="lblID" runat="server" Visible="False"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Jabatan</label>
                <asp:Label ID="lblTitle" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Tempat / Tanggal Lahir</label>
                <asp:Label ID="lblBirthPlace" runat="server"></asp:Label>/
                <asp:Label ID="lblBirthDate" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>NPWP</label>
                <asp:Label ID="lblNPWP" runat="server"></asp:Label>                           
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>ID Jenis Dokumen</label>                
                <asp:Label ID="lblIDType" runat="server"></asp:Label>     
            </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>No Dokumen ID</label>
                <asp:Label ID="lblIDNumber" runat="server"></asp:Label>
	        </div>
        </div>   
        <div class="form_box_header">
            <div class="form_single">              
                <h4>
                    ALAMAT
                </h4>
            </div>
        </div>      
        <div class="form_box">
	        <div class="form_single">
                <label>Alamat</label>
                <asp:Label ID="lblAddress" runat="server"></asp:Label>
	        </div>
        </div> 
        <div class="form_box">
	        <div class="form_single">
                <label>RT/RW</label>
                <asp:Label ID="lblRT" runat="server"></asp:Label>/
                <asp:Label ID="lblRW" runat="server"></asp:Label>
	        </div>
        </div> 
        <div class="form_box">
	        <div class="form_single">
                <label>Kelurahan</label>
                <asp:Label ID="lblKelurahan" runat="server"></asp:Label>
	        </div>
        </div> 
        <div class="form_box">
	        <div class="form_single">
                <label>Kecamatan</label>
                <asp:Label ID="lblKecamatan" runat="server"></asp:Label>
	        </div>
        </div> 
        <div class="form_box">
	        <div class="form_single">
                <label>Kota</label>
                <asp:Label ID="lblcity" runat="server"></asp:Label>
	        </div>
        </div> 
        <div class="form_box">
	        <div class="form_single">
                <label>Kode Pos</label>
                <asp:Label ID="lblZip" runat="server"></asp:Label>
	        </div>
        </div> 
        <div class="form_box">
	        <div class="form_single">
                <label>Telepon-1</label>
                <asp:Label ID="lblAPhone1" runat="server"></asp:Label>-
                <asp:Label ID="lblPhone1" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Telepon-2</label>
                <asp:Label ID="lblAPhone2" runat="server"></asp:Label>-
                <asp:Label ID="lblPhone2" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>No Fax</label>
                <asp:Label ID="lblAFax" runat="server"></asp:Label>-
                <asp:Label ID="lblFax" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>No HandPhone</label>
                <asp:Label ID="lblMobile" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>e-mail</label>
                <asp:Label ID="lblEmail" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_button">
            <asp:Button ID="imbClose" OnClientClick="fClose();" runat="server" CausesValidation="false" Text="Close" CssClass ="small button gray">
            </asp:Button>                 
        </div>              
    </form>
</body>
</html>
