﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.cbse
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class SupplierEmployee
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents UcBankAccount As UcBankAccount
    Protected WithEvents txtRefundBungaPersen As ucNumberFormat
    Protected WithEvents txtRefundBungaAmount As ucNumberFormat
 


#Region "Constanta"
    Private m_controller As New SupplierController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Dim oRow As DataRow
#End Region
#Region "Property"
    Private Property AddEdit() As String
        Get
            Return CType(ViewState("AddEdit"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("AddEdit") = Value
        End Set
    End Property
    Private Property SupplierID() As String
        Get
            Return CType(ViewState("SupplierID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("SupplierID") = Value
        End Set
    End Property
    Private Property EmployeeID() As String
        Get
            Return CType(ViewState("EmployeeID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("EmployeeID") = Value
        End Set
    End Property

    Private Property SupplierEmployeeID() As String
        Get
            Return CType(ViewState("SupplierEmployeeID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("SupplierEmployeeID") = Value
        End Set
    End Property

    Private Property TransID() As String
        Get
            Return CType(ViewState("TransID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("TransID") = Value
        End Set
    End Property
    Private Property NilaiAlokasi() As Decimal
        Get
            Return CType(ViewState("NilaiAlokasi"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            ViewState("NilaiAlokasi") = Value
        End Set
    End Property
    Private Property PersenAlokasi() As Decimal
        Get
            Return CType(ViewState("PersenAlokasi"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            ViewState("PersenAlokasi") = Value
        End Set
    End Property
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property WhereCond2() As String
        Get
            Return CType(ViewState("WhereCond2"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("WhereCond2") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property
    Private Property Name() As String
        Get
            Return CType(ViewState("Name"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Name") = Value
        End Set
    End Property

    Private Property SupplierEmployeeName() As String
        Get
            Return CType(ViewState("SupplierEmployeeName"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("SupplierEmployeeName") = Value
        End Set
    End Property
    Private Property Suppid() As String
        Get
            Return CType(ViewState("Suppid"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Suppid") = Value
        End Set
    End Property
    Private Property EmployeePositionID() As String
        Get
            Return CType(ViewState("EmployeePositionID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("EmployeePositionID") = Value
        End Set
    End Property

    Private Property AlamatNPWP() As String
        Get
            Return CType(ViewState("AlamatNPWP"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("AlamatNPWP") = Value
        End Set
    End Property
#End Region
#Region "Page Load & Bindgrid"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If
        Me.FormID = "SupplierEmployee"
        If Not Me.IsPostBack Then
            If CheckForm(Me.Loginid, Me.FormID, "MAXILOAN") Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            If Request("back") = "1" Then
                GetCookies()
            Else
                If Request("cond") <> "" Then
                    Me.CmdWhere = Request("cond")
                Else
                    Me.CmdWhere = "ALL"
                End If

                Me.SupplierID = Request("id")
                Me.Name = Request("name")
                Me.WhereCond2 = "SupplierID = '" & Me.SupplierID & "'"
            End If

            lnkID.Text = Me.SupplierID
            lblName.Text = Me.Name
            Me.Sort = "SupplierEmployeeID ASC"
            InitialPanel()
            With UcBankAccount
                .ValidatorTrue()
                .Style = "Marketing"
                .BindBankAccount()

            End With
            BindSupplierPosition()
            BindGrid(Me.CmdWhere)
            lblName.NavigateUrl = "javascript:OpenWin('" & lnkID.Text & "','Marketing');"
        End If
    End Sub
#Region "GetCookies"
    Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies("Supplier")
        Me.CmdWhere = cookie.Values("where")
        Me.WhereCond2 = cookie.Values("where2")
        Me.Name = cookie.Values("name")
        Me.SupplierID = cookie.Values("id")
    End Sub
#End Region
    Sub BindSupplierPosition()
        Dim dtEntity As New DataTable        
        Dim m_Insentif As New RefundInsentifController
        Dim oCustom As New Parameter.RefundInsentif
        Dim tarif As String = ""

        With oCustom
            .strConnection = GetConnectionString()
            .WhereCond = ""
            .SPName = "spGetTblEmployeeSupplier"
        End With

        oCustom = m_Insentif.GetSPBy(oCustom)

        dtEntity = oCustom.ListDataTable

        cboPosition.DataSource = dtEntity.DefaultView
        cboPosition.DataTextField = "Description"
        cboPosition.DataValueField = "ID"
        cboPosition.DataBind()
        cboPosition.Items.Insert(0, "Select One")
        cboPosition.Items(0).Value = "Select One"

        cboEmpPosition.DataSource = dtEntity.DefaultView
        cboEmpPosition.DataTextField = "Description"
        cboEmpPosition.DataValueField = "ID"
        cboEmpPosition.DataBind()
        cboEmpPosition.Items.Insert(0, "Select One")
        cboEmpPosition.Items(0).Value = "Select One"


    End Sub
    Sub BindGrid(ByVal cmdWhere As String)
        Dim dtEntity As DataTable
        Dim oCustomClass As New Parameter.Supplier
        With oCustomClass
            .FormID = ""
            .PageSize = pageSize
            .WhereCond = cmdWhere
            .SortBy = Me.Sort
            .WhereCond2 = Me.WhereCond2

            .CurrentPage = currentPage
            .strConnection = GetConnectionString()
        End With
        oCustomClass = m_controller.GetSupplierEmployee(oCustomClass)
        If Not oCustomClass Is Nothing Then
            dtEntity = oCustomClass.ListData
            recordCount = oCustomClass.TotalRecords
        Else
            recordCount = 0
        End If
        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()
        PagingFooter()

        If dtEntity.DefaultView.Count > 0 Then
            Me.SupplierEmployeeID = IIf(IsDBNull(dtEntity.DefaultView(0)("SupplierEmployeeID")), "", dtEntity.DefaultView(0)("SupplierEmployeeID")).ToString

        Else
            Me.SupplierEmployeeID = ""
        End If
    End Sub
    Sub InitialPanel()
        pnlList.Visible = True
        pnlAddEdit.Visible = False
        pnlView.Visible = False
        txtPage.Text = "1"

    End Sub
#End Region
#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            'rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            'rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGrid(Me.CmdWhere)
    End Sub
    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int32)
                BindGrid(Me.CmdWhere)
            End If
        End If
    End Sub
#End Region
#Region "Sorting"
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGrid(Me.CmdWhere)
    End Sub
#End Region
#Region "Reset-Search"
    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        cboSearch.SelectedIndex = 0
        txtSearch.Text = ""
        cboPosition.ClearSelection()
        Me.WhereCond2 = "SupplierID = '" & Me.SupplierID & "'"
        Me.CmdWhere = "ALL"
        BindGrid(Me.CmdWhere)
    End Sub
    Private Sub imbSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonSearch.Click
        Dim Search As String = ""
        If txtSearch.Text.Trim <> "" Then
            If txtSearch.Text.Trim <> "" Then
                Me.CmdWhere = cboSearch.SelectedItem.Value + " like '%" + txtSearch.Text.Replace("%", "").Trim + "%'"
            End If
        Else
            Me.CmdWhere = "ALL"
        End If
        If cboPosition.SelectedIndex <> 0 Then
            Me.WhereCond2 = "SupplierID = '" & Me.SupplierID & "' and SupplierEmployeePosition='" & cboPosition.SelectedValue & "'"
        Else
            Me.WhereCond2 = "SupplierID = '" & Me.SupplierID & "'"
        End If
        BindGrid(Me.CmdWhere)
    End Sub
#End Region
#Region "Add"
    Private Sub imbAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonAdd.Click
        If CheckFeature(Me.Loginid, "SupplierEmployee", "Add", "MAXILOAN") Then
            If SessionInvalid() Then
                Exit Sub
            End If
        End If
        pnlList.Visible = False
        pnlAddEdit.Visible = True
        BindAdd()
        Me.AddEdit = "Add"
        lblTitle.Text = "ADD"
    End Sub
    Sub BindAdd()
        lblEmployeeID.Visible = False
        lblEmployeeID.Text = ""
        txtEmpName.Text = ""
        'cboEmpPosition.ClearSelection()
        'cboEmpPosition.Items.FindByValue("SL").Selected = True        
        UcBankAccount.BindBankAccount()
        UcBankAccount.BankID = ""
        UcBankAccount.BankBranch = ""
        UcBankAccount.AccountNo = ""
        UcBankAccount.AccountName = ""
        chkActive.Checked = False
        txtNoKTP.Text = ""
        txtNoNPWP.Text = ""
        ' rboBadStatus.SelectedIndex = 0
        txtAlamatNPWP.Text = ""
    End Sub
#End Region
#Region "BindEdit"
    Sub BindEdit(ByVal Name As String, ByVal Position As String, ByVal BankID As String, _
    ByVal BankBranch As String, ByVal accountno As String, ByVal accountname As String, ByVal IsActive As Boolean, _
    ByVal BankBranchID As String, ByVal TransID As String, ByVal NilaiAlokasi As Decimal, ByVal PersenAlokasi As Decimal, ByVal NoNpwp As String, ByVal IDnumber As String)
        Dim oData As New DataTable
        Dim oEntity As New Parameter.Supplier

        lblEmployeeID.Text = Me.EmployeeID
        pnlList.Visible = False
        pnlAddEdit.Visible = True
        lblEmployeeID.Visible = False
        txtEmpName.Text = Name.Trim
        txtNoKTP.Text = IDnumber.Trim
        txtNoNPWP.Text = NoNpwp.Trim
        '  cboEmpPosition.ClearSelection()
        cboEmpPosition.SelectedIndex = cboEmpPosition.Items.IndexOf(cboEmpPosition.Items.FindByValue(Position.Trim))
        'rboStatus.SelectedIndex = rboStatus.Items.IndexOf(rboStatus.Items.FindByValue(oRow.Item("SupplierSaleStatus").ToString))
        'rboBadStatus.SelectedIndex = rboBadStatus.Items.IndexOf(rboBadStatus.Items.FindByValue(oRow.Item("SupplierSaleStatus").ToString))
        txtAlamatNPWP.Text = Me.AlamatNPWP
        With UcBankAccount
            .BankBranchId = BankBranchID.Trim
            .BindBankAccount()
            .BankID = Replace(BankID.Trim, "&nbsp;", "")
            .BankBranch = Replace(BankBranch.Trim, "&nbsp;", "")
            .AccountNo = Replace(accountno.Trim, "&nbsp;", "")
            .AccountName = Replace(accountname.Trim, "&nbsp;", "")

        End With
        chkActive.Checked = IsActive
    End Sub
#End Region
#Region "BindEditMasterTransaksiSupplierEmpoyee"
    Sub BindEditMasterTransaksiSupplierEmpoyee(ByVal Name As String, ByVal Position As String, ByVal BankID As String, _
    ByVal BankBranch As String, ByVal accountno As String, ByVal accountname As String, ByVal IsActive As Boolean, _
    ByVal BankBranchID As String, ByVal TransID As String, ByVal NilaiAlokasi As Decimal, ByVal PersenAlokasi As Decimal)
        Dim oData As New DataTable
        Dim oEntity As New Parameter.Supplier

        lblEmployeeID.Text = Me.EmployeeID
        pnlList.Visible = False
        pnlAddEdit.Visible = True
        lblEmployeeID.Visible = False
        txtEmpName.Text = Name
        'cboEmpPosition.ClearSelection()
        cboEmpPosition.SelectedIndex = cboEmpPosition.Items.IndexOf(cboEmpPosition.Items.FindByValue(Position.Trim))


        With UcBankAccount
            .BankBranchId = BankBranchID.Trim
            .BindBankAccount()
            .BankID = Replace(BankID.Trim, "&nbsp;", "")
            .BankBranch = Replace(BankBranch.Trim, "&nbsp;", "")
            .AccountNo = Replace(accountno.Trim, "&nbsp;", "")
            .AccountName = Replace(accountname.Trim, "&nbsp;", "")

        End With
        chkActive.Checked = IsActive
    End Sub
#End Region
#Region "BindView"
    Sub BindView(ByVal ID As String, ByVal Name As String, ByVal Position As String, ByVal BankID As String, _
    ByVal BankBranch As String, ByVal accountno As String, ByVal accountname As String, ByVal IsActive As Boolean)
        Dim oData As New DataTable
        Dim oEntity As New Parameter.Supplier
        pnlList.Visible = False
        pnlAddEdit.Visible = False
        pnlView.Visible = True
        lblEmpName.Text = Name
        lblPosition.Text = Position
        lblBankName.Text = Replace(BankID.Trim, "&nbsp;", "")
        lblBranch.Text = Replace(BankBranch.Trim, "&nbsp;", "")
        lblANo.Text = Replace(accountno.Trim, "&nbsp;", "")
        lblAName.Text = Replace(accountname.Trim, "&nbsp;", "")
        If IsActive = True Then
            lblActive.Text = "Active"
        Else
            lblActive.Text = "Not Active"
        End If
    End Sub
#End Region
#Region "ItemCommand"
    Private Sub dtgPaging_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        Dim ID As String
        Dim Name As String

        If e.CommandName = "Edit" Then
            If CheckFeature(Me.Loginid, "SupplierEmployee", "Edit", "MAXILOAN") Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            Me.AddEdit = "Edit"
            lblTitle.Text = "EDIT"
            Dim TransIDx, npwp As String
            Dim NilaiAlokasix As Double
            Dim PersenAlokasix As Double
            Dim IDnumber As String
            Dim EmployeePositionID As LinkButton = CType(e.Item.FindControl("lnkSupplierEmployeePositionID"), LinkButton)

            TransIDx = CType(e.Item.FindControl("lblTransIDheader"), Label).Text.Trim
            If TransIDx.Trim = "&nbsp;" Then
                TransIDx = "Null"
            End If
            If CType(e.Item.FindControl("lblNilaiAlokasiheader"), Label).Text.Trim = "" Then
                NilaiAlokasix = 0
            Else
                NilaiAlokasix = CDbl(CType(e.Item.FindControl("lblNilaiAlokasiheader"), Label).Text)
            End If
            If CType(e.Item.FindControl("lblPersenAlokasiheader"), Label).Text.Trim = "" Then
                PersenAlokasix = 0
            Else
                PersenAlokasix = CDbl(CType(e.Item.FindControl("lblPersenAlokasiheader"), Label).Text)
            End If
            If CType(e.Item.FindControl("lblIDNumber"), Label).Text.Trim = "" Then
                IDnumber = 0
            Else
                IDnumber = CType(e.Item.FindControl("lblIDNumber"), Label).Text
            End If

            npwp = CType(e.Item.FindControl("lblNPWP"), Label).Text
            Me.EmployeeID = CType(e.Item.FindControl("lnkSupplierEmployeeID"), LinkButton).Text.Trim

            BindEdit(CType(e.Item.FindControl("lblSupplierEmployeeName"), LinkButton).Text, EmployeePositionID.Text, _
            e.Item.Cells(5).Text.Trim, e.Item.Cells(6).Text.Trim, e.Item.Cells(7).Text.Trim, e.Item.Cells(8).Text.Trim, _
            CType(e.Item.Cells(9).Text.Trim, Boolean), CType(e.Item.FindControl("lblBankBranchId"), Label).Text, TransIDx, NilaiAlokasix, PersenAlokasix, npwp, IDnumber)
            Me.AlamatNPWP = CType(e.Item.FindControl("lblAlamatNPWP"), Label).Text

        ElseIf e.CommandName = "Delete" Then
            If CheckFeature(Me.Loginid, "SupplierEmployee", "Del", "MAXILOAN") Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            Dim oSupplierEmployee As New Parameter.Supplier
            oSupplierEmployee.SupplierID = Me.SupplierID
            oSupplierEmployee.ID = CType(e.Item.FindControl("lnkSupplierEmployeeID"), LinkButton).Text.Trim
            oSupplierEmployee.strConnection = GetConnectionString()
            Dim err As String
            err = m_controller.SupplierEmployeeDelete(oSupplierEmployee)
            If err = "" Then
                InitialPanel()
                BindGrid(Me.CmdWhere)
            Else
                ShowMessage(lblMessage, err, True)
            End If
        ElseIf e.CommandName = "View" Then
            If CheckFeature(Me.Loginid, "SupplierEmployee", "View", "MAXILOAN") Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            BindView(CType(e.Item.FindControl("lnkSupplierEmployeeID"), LinkButton).Text.Trim, CType(e.Item.FindControl("lblSupplierEmployeeName"), LinkButton).Text, e.Item.Cells(4).Text.Trim, e.Item.Cells(5).Text.Trim, e.Item.Cells(6).Text.Trim, e.Item.Cells(7).Text.Trim, e.Item.Cells(8).Text.Trim, CType(e.Item.Cells(9).Text.Trim, Boolean))



        ElseIf e.CommandName = "EmployeeMasterTransaksi" Then
            Dim Target As String = "Supplier" + e.CommandName
            ID = CType(e.Item.FindControl("lnkSupplierEmployeeID"), LinkButton).Text
            Name = CType(e.Item.FindControl("lblSupplierEmployeeName"), LinkButton).Text.Trim
            Suppid = CType(e.Item.FindControl("lnkSupplierID"), LinkButton).Text.Trim
            SupplierEmployeeName = CType(e.Item.FindControl("lblSupplierEmployeeName"), LinkButton).Text.Trim
            EmployeePositionID = CType(e.Item.FindControl("lnkSupplierEmployeePositionID"), LinkButton).Text.Trim


            Response.Redirect("" & Target & ".aspx?id=" & ID & "&name=" & Name & "&SupplierID=" & Suppid & "&SupplierEmployeeName=" & SupplierEmployeeName & "&SupplierEmployeePositionID=" & EmployeePositionID & "")
        End If


    End Sub
#End Region

#Region "Back, Save & Cancel"
    Private Sub imbCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        InitialPanel()
    End Sub

    Private Sub imbBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonBack.Click
        Response.Redirect(Request("apps") & ".aspx?currentPage=" & Request("currentPage") & "")
    End Sub
#End Region
#Region "ItemDataBound"
    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim imbDelete As ImageButton
            imbDelete = CType(e.Item.FindControl("imgDelete"), ImageButton)
            imbDelete.Attributes.Add("OnClick", "return DeleteConfirm();")
        End If
    End Sub
#End Region
#Region "imbClose"
    Private Sub imbClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonClose.Click
        InitialPanel()
    End Sub
#End Region


    Protected Sub ButtonSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonSave.Click
        Dim oSupplier As New Parameter.Supplier
        Dim oReturn As New Parameter.Supplier

        oSupplier.strConnection = GetConnectionString()
        oSupplier.AddEdit = Me.AddEdit
        oSupplier.SupplierID = Me.SupplierID.Trim

        If Me.AddEdit = "Add" Then
            oSupplier.ID = ""
        ElseIf Me.AddEdit = "Edit" Then
            oSupplier.ID = Me.EmployeeID
        End If

        oSupplier.EmployeeName = txtEmpName.Text.Trim
        oSupplier.EmployeePosition = cboEmpPosition.SelectedItem.Value        
        oSupplier.NoKTP = txtNoKTP.Text.Trim
        oSupplier.NoNPWP = txtNoNPWP.Text.Trim
        ' oSupplier.SupplierLevelStatus = rboStatus.SelectedValue
        oSupplier.BankID = UcBankAccount.BankID.Trim
        oSupplier.BankBranch = UcBankAccount.BankBranch.Trim
        oSupplier.AccountNo = UcBankAccount.AccountNo.Trim
        oSupplier.AccountName = UcBankAccount.AccountName.Trim
        oSupplier.IsActive = chkActive.Checked
        oSupplier.BankBranchId = UcBankAccount.BankBranchId
        oSupplier.SupplierSaleStatus = rboBadStatus.SelectedValue
        oSupplier.AlamatNPWP = txtAlamatNPWP.Text
        oReturn = m_controller.SupplierEmployeeSave(oSupplier)
        If oReturn.Output = "" Then
            InitialPanel()
            BindGrid(Me.CmdWhere)
        Else
            ShowMessage(lblMessage, oReturn.Output, True)
        End If
        BindGrid(Me.CmdWhere)
    End Sub
End Class


