﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SupplierAccount.aspx.vb"
    Inherits="Maxiloan.Webform.SalesMarketing.SupplierAccount" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcBankAccount" Src="../Webform.UserController/UcBankAccount.ascx" %>
<%@ Register src="../Webform.UserController/ucDateCE.ascx" tagname="ucDateCE" tagprefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>SupplierAccount</title>
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;
        function OpenWin(SupplierID, pStyle) {
            var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            window.open('http://<%# Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.SalesMarketing/Viewsupplier.aspx?style=' + pStyle + '&SupplierID=' + SupplierID, 'UserLookup', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>
                        DAFTAR REKENING BANK SUPPLIER
                    </h3>
                </div>
            </div>
            <asp:Panel ID="pnlList" runat="server" Width="100%">
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtgPaging" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                                OnSortCommand="Sorting" BorderStyle="None" BorderWidth="0" DataKeyField="SupplierAccID"
                                CssClass="grid_general">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="EDIT">
                                        <ItemStyle CssClass="command_col"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" ImageUrl="../Images/iconedit.gif"
                                                CommandName="Edit"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="DELETE">
                                        <ItemStyle CssClass="command_col"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgDelete" runat="server" CausesValidation="False" ImageUrl="../Images/icondelete.gif"
                                                CommandName="Delete"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="SupplierAccID" SortExpression="SupplierAccID" HeaderText="ID REKENING"
                                        Visible="False"></asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="NAMA BANK" SortExpression="BankName">
                                        <ItemTemplate>
                                            <asp:Label ID="lblBankName" runat="server" Text='<%# DataBinder.eval(Container,"DataItem.BankName") %>'>
                                            </asp:Label>
                                            <asp:Label ID="lblBankBranchId" runat="server" Visible="False" Text='<%# DataBinder.eval(Container,"DataItem.SupplierBankBranchID") %>'>
                                            </asp:Label>
                                            <asp:Label ID="lblSupplierBankID" runat="server" Visible="False" Text='<%# DataBinder.eval(Container,"DataItem.SupplierBankID") %>'>
                                            </asp:Label>
                                            <asp:Label ID="lblUntukBayar" runat="server" Visible="False" Text='<%# DataBinder.eval(Container,"DataItem.UntukBayar") %>'>
                                            </asp:Label>
                                            <asp:Label ID="lblTanggalEfektif" runat="server" Visible="False" Text='<%# DataBinder.eval(Container,"DataItem.TanggalEfektif") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="SupplierBankBranch" SortExpression="SupplierBankBranch"
                                        HeaderText="CABANG BANK"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="SupplierAccountNo" SortExpression="SupplierAccountNo"
                                        HeaderText="NO REKENING"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="SupplierAccountName" SortExpression="SupplierAccountName"
                                        HeaderText="NAMA REKENING"></asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="DEFAULT" SortExpression="DefaultAccount">
                                        <ItemTemplate>
                                            <input id="hdncheckDefault" type="hidden" name="hdncheckDefault" runat="server" value='<%# DataBinder.eval(Container,"DataItem.DefaultAccount") %>' />
                                            <asp:CheckBox ID="checkDefault" Enabled="false" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="SupplierName" SortExpression="SupplierName" HeaderText="ATAS NAMA">
                                    </asp:BoundColumn>
                                </Columns>
                            </asp:DataGrid>
                            <div class="button_gridnavigation">
                                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../Images/grid_navbutton01.png"
                                    CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../Images/grid_navbutton02.png"
                                    CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../Images/grid_navbutton03.png"
                                    CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../Images/grid_navbutton04.png"
                                    CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                                    EnableViewState="False"></asp:Button>
                                <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" MinimumValue="1"
                                    ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"></asp:RangeValidator>
                                <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                                    ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                            </div>
                            <div class="label_gridnavigation">
                                <asp:Label ID="lblPage" runat="server"></asp:Label>of
                                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonAdd" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="ButtonPrint" Visible="false" runat="server" Enabled="true" Text="Print"
                        CssClass="small button blue"></asp:Button>
                    <asp:Button ID="ButtonBack" runat="server" CausesValidation="False" Text="Back" CssClass="small button gray">
                    </asp:Button>
                </div>
                <div class="form_box_title">
                    <div class="form_single">
                        <h4>
                            CARI REKENING BANK SUPPLIER
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Nama Supplier</label>
                        <asp:HyperLink ID="lblName" runat="server"></asp:HyperLink>
                        <asp:LinkButton ID="lnkID" runat="server" CausesValidation="False" Visible="False"></asp:LinkButton>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Cari Berdasarkan</label>
                        <asp:DropDownList ID="cboSearch" runat="server">
                            <asp:ListItem Value="BankName">Nama Bank</asp:ListItem>
                            <asp:ListItem Value="SupplierAccountName">Nama Rekening</asp:ListItem>
                            <asp:ListItem Value="SupplierName">ATAS NAMA</asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtSearch" runat="server" Width="20%"></asp:TextBox>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonSearch" runat="server" CausesValidation="False" Text="Search"
                        CssClass="small button blue"></asp:Button>
                    <asp:Button ID="ButtonReset" runat="server" CausesValidation="False" Text="Reset"
                        CssClass="small button gray"></asp:Button>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlAddEdit" runat="server">                            
                <div class="form_box_uc">
                    <uc1:ucbankaccount id="UcBankAccount" runat="server"></uc1:ucbankaccount>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Rekening Default untuk Pembayaran</label>
                        <asp:CheckBox ID="checkDefault" runat="server" />
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Untuk Pembayaran</label>
                        <asp:TextBox ID="txtForPayment" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                            ControlToValidate="txtForPayment" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Efektif Tanggal</label>

                        <%--<asp:TextBox ID="EffectiveDate" runat="server"></asp:TextBox>
                        <asp:CalendarExtender ID="EffectiveDate_CalendarExtender" runat="server" Enabled="True"
                            TargetControlID="EffectiveDate" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                        <asp:RequiredFieldValidator CssClass="validator_general" ID="RequiredFieldValidator1"
                            runat="server" ErrorMessage="*" ControlToValidate="EffectiveDate"></asp:RequiredFieldValidator>--%>
                        <uc2:ucDateCE ID="txtEffectiveDate" runat="server" />
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonSave" runat="server" Text="Save" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="ButtonCancel" runat="server" CausesValidation="False" Text="Cancel"
                        CssClass="small button gray"></asp:Button>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
