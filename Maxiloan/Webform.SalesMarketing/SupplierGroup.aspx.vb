﻿
#Region "Imports"
Imports System.IO
Imports Maxiloan.Controller
Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.Webform.UserController
#End Region

Public Class SupplierGroup
    Inherits Maxiloan.Webform.WebBased


    Protected WithEvents UcCompanyAddress As UcCompanyAddress
    Protected WithEvents txtJmlPlafondGroup As ucNumberFormat


    Protected WithEvents uplProsedurKYC As FileUpload
    'Private m_controller As New SupplierGroupController

    'Private currentPage As Integer = 1
    'Private pageSize As Int16 = 10
    'Private currentPageNumber As Int32 = 1
    'Private totalPages As Double = 1
    'Private recordCount As Int64 = 1
#Region "Constanta"
    Private m_controller As New SupplierGroupController
    Dim oSupplierGroup As New Parameter.SupplierGroup
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Dim oRow As DataRow
    Dim Control As New CTKonfirmasiEbankExec
    Dim dtCSV, dtSQL As New DataTable
#End Region

#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property AddEdit() As String
        Get
            Return CType(ViewState("vwsAddEdt"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsAddEdt") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property
    Private Property WhereCond() As String
        Get
            Return CType(ViewState("WhereCond"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("WhereCond") = Value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not Me.IsPostBack Then
            BindJobPosition()
            Me.FormID = "SupplierGroup"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                With txtJmlPlafondGroup
                    .RangeValidatorEnable = True
                    .RequiredFieldValidatorEnable = True
                End With

                'modify by Amri 20180116, add for view odf
                If Request.QueryString("strFileLocation") <> "" Then
                    Dim strFileLocation As String
                    strFileLocation = "../XML/" & Request.QueryString("strFileLocation") & ".pdf"
                    Response.Write("<script language = javascript>" & vbCrLf _
                & "var x = screen.width; var y = screen.height;  window.open('" & strFileLocation & "','accacq', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes'); " & vbCrLf _
                & "</script>")
                End If

                txtPage.Text = "1"
                Me.Sort = "SupplierGroupID ASC"
                If Request("cond") <> "" Then
                    Me.CmdWhere = Request("cond")
                Else
                    Me.CmdWhere = "ALL"
                End If
                BindGridEntity(Me.CmdWhere)
            End If
        End If

    End Sub
    Private Sub InitialDefaultPanel()
        pnlList.Visible = True
        pnlAddEdit.Visible = False
    End Sub
    Sub BindGridEntity(ByVal cmdWhere As String)
        Dim dtEntity As DataTable = Nothing
        Dim oSupplierGroup As New Parameter.SupplierGroup
        InitialDefaultPanel()
        oSupplierGroup.strConnection = GetConnectionString()
        oSupplierGroup.WhereCond = cmdWhere
        oSupplierGroup.CurrentPage = currentPage
        oSupplierGroup.PageSize = pageSize
        oSupplierGroup.SortBy = Me.Sort
        oSupplierGroup = m_controller.GetSupplierGroup(oSupplierGroup)

        If Not oSupplierGroup Is Nothing Then
            dtEntity = oSupplierGroup.Listdata
            recordCount = oSupplierGroup.Totalrecords
        Else
            recordCount = 0
        End If
       
        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()
        PagingFooter()
    End Sub
#Region "Bind Combo JobPosition"
    Private Sub BindJobPosition()
        Dim oSupplierGroup As New Parameter.SupplierGroup
        Dim JobPosition As New DataTable
        oSupplierGroup.strConnection = GetConnectionString()
        JobPosition = m_controller.GetJobPositionCombo(oSupplierGroup)
        cboJobPosition.DataTextField = "Description"
        cboJobPosition.DataValueField = "ID"
        cboJobPosition.DataSource = JobPosition
        cboJobPosition.DataBind()
    End Sub
#End Region
    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        Dim imbDelete As ImageButton
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imbDelete"), ImageButton)
            imbDelete.Attributes.Add("Onclick", "return DeleteConfirm()")

        End If
    End Sub
#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub imgbtnPageNumb_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int32)
                BindGridEntity(Me.CmdWhere)
            End If
        End If
    End Sub
#End Region
    Private Sub dtgEntity_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        Dim err As String
        Dim oSupplierGroup As New Parameter.SupplierGroup
        If e.CommandName = "Edit" Then
            If CheckFeature(Me.Loginid, Me.FormID, "Edit", Me.AppId) Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            Me.AddEdit = "EDIT"
            pnlAddEdit.Visible = True
            pnlList.Visible = False
            ButtonBack.Visible = False
            ButtonCancel.Visible = True
            ButtonSave.Visible = True
            ButtonClose.Visible = False
            ButtonCancel.CausesValidation = False

            lblTitleAddEdit.Text = Me.AddEdit
            oSupplierGroup.SupplierGroupID = dtgPaging.DataKeys.Item(e.Item.ItemIndex).ToString
            oSupplierGroup.strConnection = GetConnectionString()
            oSupplierGroup = m_controller.GetSupplierGroupList(oSupplierGroup)

            'Bind
            txtNamaGroupSupplier.Text = oSupplierGroup.SupplierGroupName.Trim
            txtNamaGroupSupplier.Enabled = False
            hdnEditSupplierGroupID.Value = oSupplierGroup.SupplierGroupID.Trim
            txtNPWP.Text = oSupplierGroup.NPWP.Trim
            txtTDP.Text = oSupplierGroup.TDP.Trim
            txtSIUP.Text = oSupplierGroup.SIUP.Trim
            txtJmlPlafondGroup.Text = FormatNumber(oSupplierGroup.SupplierGroupPlafond, 0)

            With UcCompanyAddress
                .Address = oSupplierGroup.Alamat.Trim
                .RT = oSupplierGroup.RT.Trim
                .RW = oSupplierGroup.RW.Trim
                .Kelurahan = oSupplierGroup.Kelurahan.Trim
                .Kecamatan = oSupplierGroup.Kecamatan.Trim
                .City = oSupplierGroup.Kota.Trim
                .ZipCode = oSupplierGroup.KodePos.Trim
                .AreaPhone1 = oSupplierGroup.AreaPhone1.Trim
                .Phone1 = oSupplierGroup.Phone1.Trim
                .AreaPhone2 = oSupplierGroup.AreaPhone2.Trim
                .Phone2 = oSupplierGroup.Phone2.Trim
                .AreaFax = oSupplierGroup.AreaFax.Trim
                .Fax = oSupplierGroup.Fax.Trim
                .Style = "Setting"
                .BindAddress()
            End With



            txtContactPerson.Text = oSupplierGroup.ContactPerson.Trim
            cboJobPosition.SelectedValue = oSupplierGroup.JabatanID.Trim
            txtEmail.Text = oSupplierGroup.Email.Trim
            txtNoHP.Text = oSupplierGroup.NoHP.Trim
            txtNotes.Text = oSupplierGroup.Notes.Trim


        ElseIf e.CommandName = "Delete" Then
            If CheckFeature(Me.Loginid, Me.FormID, "Del", Me.AppId) Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            Dim customClass As New Parameter.SupplierGroup
            With customClass
                .SupplierGroupID = dtgPaging.DataKeys.Item(e.Item.ItemIndex).ToString
                .strConnection = GetConnectionString()
            End With
            err = m_controller.SupplierGroupDelete(customClass)
            If err <> "" Then
                ShowMessage(lblMessage, err, True)
            Else
                ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, False)
            End If
            BindGridEntity(Me.CmdWhere)
            txtPage.Text = "1"
        End If
    End Sub
    Private Sub imbSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
        Dim customClass As New Parameter.SupplierGroup
        Dim ErrMessage As String = ""

        Try
            With customClass
                .SupplierGroupName = txtNamaGroupSupplier.Text.Trim
                .NPWP = txtNPWP.Text.Trim
                .TDP = txtTDP.Text.Trim
                .SIUP = txtSIUP.Text.Trim
                .SupplierGroupPlafond = CDec(txtJmlPlafondGroup.Text.Trim)
                .Alamat = UcCompanyAddress.Address.Trim
                .RT = UcCompanyAddress.RT.Trim
                .RW = UcCompanyAddress.RW.Trim
                .Kelurahan = UcCompanyAddress.Kelurahan.Trim
                .Kecamatan = UcCompanyAddress.Kecamatan.Trim
                .Kota = UcCompanyAddress.City.Trim
                .KodePos = UcCompanyAddress.ZipCode.Trim
                .AreaPhone1 = UcCompanyAddress.AreaPhone1.Trim
                .Phone1 = UcCompanyAddress.Phone1.Trim
                .AreaPhone2 = UcCompanyAddress.AreaPhone2.Trim
                .Phone2 = UcCompanyAddress.Phone2.Trim
                .AreaFax = UcCompanyAddress.AreaFax.Trim
                .Fax = UcCompanyAddress.Fax.Trim
                .ContactPerson = txtContactPerson.Text.Trim
                .JabatanID = cboJobPosition.SelectedValue.Trim
                .Email = txtEmail.Text.Trim
                .NoHP = txtNoHP.Text.Trim
                .Notes = txtNotes.Text.Trim
                .BranchId = Me.sesBranchId.Replace("'", "").Trim
                .BusinessDate = Me.BusinessDate

                .strConnection = GetConnectionString()
            End With

            If Me.AddEdit = "ADD" Then
                ErrMessage = m_controller.SupplierGroupSaveAdd(customClass)
                If ErrMessage <> "" Then
                    ShowMessage(lblMessage, ErrMessage, True)
                    Exit Sub
                Else
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_SUCCESS, False)
                    BindGridEntity(Me.CmdWhere)
                End If
            ElseIf Me.AddEdit = "EDIT" Then
                customClass.SupplierGroupID = hdnEditSupplierGroupID.Value
                m_controller.SupplierGroupSaveEdit(customClass)
                ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
                BindGridEntity(Me.CmdWhere)
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try

        cekimage()
        cekimageApproval()
    End Sub
    Private Sub imbAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonAdd.Click
        If CheckFeature(Me.Loginid, Me.FormID, "Add", Me.AppId) Then
            If SessionInvalid() Then
                Exit Sub
            End If
        End If
        pnlAddEdit.Visible = True
        pnlList.Visible = False
        ButtonBack.Visible = False
        ButtonCancel.Visible = True
        ButtonSave.Visible = True
        ButtonClose.Visible = False
        ButtonCancel.CausesValidation = False
        Me.AddEdit = "ADD"

        lblTitleAddEdit.Text = Me.AddEdit
        txtNamaGroupSupplier.Enabled = True
        'Bind
        txtNamaGroupSupplier.Text = String.Empty
        txtNPWP.Text = String.Empty
        txtTDP.Text = String.Empty
        txtSIUP.Text = String.Empty
        txtJmlPlafondGroup.Text = String.Empty
        With UcCompanyAddress
            .Address = String.Empty
            .RT = String.Empty
            .RW = String.Empty
            .Kelurahan = String.Empty
            .Kecamatan = String.Empty
            .City = String.Empty
            .ZipCode = String.Empty
            .AreaPhone1 = String.Empty
            .Phone1 = String.Empty
            .AreaPhone2 = String.Empty
            .Phone2 = String.Empty
            .AreaFax = String.Empty
            .Fax = String.Empty
            .Style = "Setting"
            .BindAddress()
        End With
        txtContactPerson.Text = String.Empty
        cboJobPosition.SelectedIndex = 0
        txtEmail.Text = String.Empty
        txtNoHP.Text = String.Empty
        txtNotes.Text = String.Empty

    End Sub
    
    Sub SendCookies()
        Dim cookie As HttpCookie = Request.Cookies("SupplierGroup")
        If Not cookie Is Nothing Then
            cookie.Values("where") = Me.CmdWhere
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie("SupplierGroup")
            cookieNew.Values.Add("where", Me.CmdWhere)
            Response.AppendCookie(cookieNew)
        End If
    End Sub
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        cboSearch.SelectedIndex = 0
        txtSearch.Text = ""
        Me.CmdWhere = "ALL"
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub imbSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonSearch.Click
        If txtSearch.Text.Trim <> "" Then
            Me.CmdWhere = cboSearch.SelectedItem.Value + " = '" + txtSearch.Text.Trim + "'"
        Else
            Me.CmdWhere = "ALL"
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub imbBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonBack.Click
        Response.Redirect("SupplierGroup.aspx")
    End Sub
    Private Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        Response.Redirect("SupplierGroup.aspx")
    End Sub
    Private Function pathFile(ByVal Group As String, ByVal ExecutorID As String) As String
        Dim strDirectory As String = ""
        strDirectory = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\" & sesBranchId.Replace("'", "") & "\" & Group & "\" & ExecutorID & "\"
        If Not System.IO.Directory.Exists(strDirectory) Then
            System.IO.Directory.CreateDirectory(strDirectory)
        End If
        Return strDirectory
    End Function
    Private Sub btnupload_Click(sender As Object, e As System.EventArgs) Handles btnupload.Click
        Dim FileName As String = ""
        Dim strExtension As String = ""
        'uplProsedurKYC.
        If uplProsedurKYC.HasFile Then
            If uplProsedurKYC.PostedFile.ContentType = "application/pdf" Then
                FileName = "PKS"
                strExtension = Path.GetExtension(uplProsedurKYC.PostedFile.FileName)
                uplProsedurKYC.PostedFile.SaveAs(pathFile("SupplierGroup", lblsuppgroupID.Text.Replace(" ", "").Replace("'", "")) + FileName + strExtension)
            End If
        End If
        cekimage()
    End Sub
    Private Sub cekimage()
        If File.Exists(pathFile("SupplierGroup", lblsuppgroupID.Text.Trim) + "PKS.pdf") Then
            hypProsedurKYC.NavigateUrl = ResolveClientUrl("../xml/" & sesBranchId.Replace("'", "") & "/SupplierGroup/" + lblsuppgroupID.Text.Replace(" ", "").Replace("'", "") + "/PKS.pdf")
            hypProsedurKYC.Visible = True
            hypProsedurKYC.Text = "PKS.pdf"
            hypProsedurKYC.Target = "_blank"
        Else
            hypProsedurKYC.Visible = False
        End If
    End Sub
    Private Sub btnuploadApproval_Click(sender As Object, e As System.EventArgs) Handles btnuploadApproval.Click
        Dim FileName As String = ""
        Dim strExtension As String = ""
        If uplApproval.HasFile Then
            If uplApproval.PostedFile.ContentType = "application/pdf" Then
                FileName = "MemoApproval"
                strExtension = Path.GetExtension(uplApproval.PostedFile.FileName)
                uplApproval.PostedFile.SaveAs(pathFile("SupplierGroup", lblsuppgroupID.Text.Replace(" ", "").Replace("'", "")) + FileName + strExtension)
            End If
        End If
        cekimageApproval()
    End Sub
    Private Sub cekimageApproval()
        If File.Exists(pathFile("SupplierGroup", lblsuppgroupID.Text.Trim) + "MemoApproval.pdf") Then
            hyApproval.NavigateUrl = ResolveClientUrl("../xml/" & sesBranchId.Replace("'", "") & "/SupplierGroup/" + lblsuppgroupID.Text.Replace(" ", "").Replace("'", "") + "/MemoApproval.pdf")
            hyApproval.Visible = True
            hyApproval.Text = "MemoApproval.pdf"
            hyApproval.Target = "_blank"
        Else
            hyApproval.Visible = False
        End If
    End Sub
    Private Sub dtgPaging_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        Dim Id As String
        Dim Name As String
        Dim AssetTypeID As String
        Dim Err As String = ""
        If e.CommandName = "Edit" Then
            If CheckFeature(Me.Loginid, "SupplierGroup", "Edit", Me.AppId) Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            '          Id = CType(e.Item.FindControl("lnkID"), Label).Text
            '          Me.AddEdit = "Edit"
            '          bindEdit(Id)
            '          'Response.Redirect("CustomerPersonal.aspx?page=Edit&id=" & e.Item.Cells(4).Text.Trim & "")
            '      ElseIf e.CommandName = "Delete" Then
            '          If CheckFeature(Me.Loginid, "Supplier", "Del", Me.AppId) Then
            '              If SessionInvalid() Then
            '                  Exit Sub
            '              End If
            '          End If
            '          Dim oSupplierGroup As New Parameter.SupplierGroup
            '          Id = CType(e.Item.FindControl("lnkID"), Label).Text
            '          oSupplierGroup.strConnection = GetConnectionString()
            '          oSupplierGroup.SupplierGroupID = Id

            '          Err = m_controller.DeleteSupplierGroup(oSupplierGroup)
            '          If Err = "" Then
            '              txtPage.Text = "1"
            '              BindGridEntity(Me.CmdWhere)
            '          Else
            '              ShowMessage(lblMessage, Err, True)
            '          End If
            '      ElseIf e.CommandName = "Branch" Or e.CommandName = "Owner" Or e.CommandName = "Budget" Or e.CommandName = "Forecast" _
            'Or e.CommandName = "Employee" Or e.CommandName = "Account" Or e.CommandName = "Signature" Or e.CommandName = "Relasi" Then
            '          Dim Target As String = "Supplier" + e.CommandName
            '          Id = CType(e.Item.FindControl("lnkID"), Label).Text
            '          Name = CType(e.Item.FindControl("lnkName"), HyperLink).Text.Trim
            '          'AssetTypeID = CType(e.Item.FindControl("lnkProduk"), Label).Text.Trim
            '          'Name = e.Item.Cells(1).Text.Trim
            '          Response.Redirect("" & Target & ".aspx?id=" & Id & "&name=" & Name & "&AssetTypeID=" & AssetTypeID & "&apps=Supplier&currentPage=" & txtPage.Text & "")

        ElseIf e.CommandName = "Print" Then
            Me.ID = CType(e.Item.FindControl("lnkID"), Label).Text
            Me.WhereCond = "SupplierGroup.supplierGroupID = '" & Me.ID & "'"

            Dim cookie As HttpCookie = Request.Cookies("PerjanjianKerjasamaSupplierPrint")
            If Not cookie Is Nothing Then
                cookie.Values("CmdWhere") = Me.ID.Trim
                Response.AppendCookie(cookie)
            Else
                Dim cookieNew As New HttpCookie("PerjanjianKerjasamaSupplierPrint")
                cookieNew.Values.Add("CmdWhere", Me.ID.Trim)
                Response.AppendCookie(cookieNew)
            End If
            Response.Redirect("PerjanjianKerjasamaSuppGroupViewer.aspx")

        End If
    End Sub
End Class