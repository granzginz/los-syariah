﻿#Region "Imports"
Imports Maxiloan.cbse
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region


Public Class SupplierForecast
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private m_controller As New SupplierController
    Dim oSupplier As New Parameter.Supplier
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Dim oRow As DataRow
#End Region
#Region "Property"
    Private Property AddEdit() As String
        Get
            Return CType(viewstate("AddEdit"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AddEdit") = Value
        End Set
    End Property
    Private Property CmdWhere() As String
        Get
            Return CType(viewstate("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property SupplierId() As String
        Get
            Return CType(viewstate("SupplierId"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("SupplierId") = Value
        End Set
    End Property
    Private Property SupplierName() As String
        Get
            Return CType(viewstate("SupplierName"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("SupplierName") = Value
        End Set
    End Property

    Private Property DtTbl() As DataTable
        Get
            Return CType(viewstate("DtTbl"), datatable)
        End Get
        Set(ByVal Value As DataTable)
            viewstate("DtTbl") = Value
        End Set
    End Property

    Private Property BMonth() As String
        Get
            Return CType(viewstate("BMonth"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("BMonth") = Value
        End Set
    End Property

    Private Property pageFrom() As String
        Get
            Return CType(viewstate("pageFrom"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("pageFrom") = Value
        End Set
    End Property
    Private Property FilterBy() As String
        Get
            Return CType(viewstate("FilterBy"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("FilterBy") = Value
        End Set
    End Property
#End Region

#Region "PageLoad"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        If sessioninvalid() Then
            Exit Sub
        End If
        Me.FormID = "SUPPLIERFORECAST"        
        If Not Me.IsPostBack Then
            If checkform(Me.Loginid, Me.FormID, Me.AppId) Then                
                If Request.QueryString("message") <> "" Then                    
                    ShowMessage(lblMessage, Request.QueryString("message"), True)
                End If
                inyear.Value = CStr(Year(Me.BusinessDate))
                If Request("back") = "1" Then
                    GetCookies()
                Else
                    Me.SupplierId = Request.QueryString("id")
                    Me.SupplierName = Request.QueryString("name")
                    Me.BranchID = Request.QueryString("branchid")
                    Me.SearchBy = "  branchid = '" & Me.BranchID & "'  and " & _
                         " supplierid = '" & Me.SupplierId & "'"
                    Me.SortBy = ""
                    Dim par As String
                    par = ""
                    par = par & "Branch : " & Me.BranchID & " "
                    If par <> "" Then
                        par = par & " , Supplier : " & Me.SupplierName.Trim & " "
                    Else
                        par = par & "Supplier : " & Me.SupplierName.Trim & " "
                    End If
                    Me.FilterBy = par
                End If
                Initialpanel()
                lblSupplierId.Text = Me.SupplierId
                hySupplierName.Text = Me.SupplierName
                hySupplierName.NavigateUrl = "javascript:OpenWinSupplier('" & "Marketing" & "', '" & Me.SupplierId.Trim & "')"
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub
#End Region
#Region "Initialpanel"
    Sub Initialpanel()
        pnlList.Visible = True
        pnlAdd.Visible = False
        pnlDatagrid.Visible = True
        pnlEdit.Visible = False
        rgvGo.Enabled = True
        txtPage.Text = "1"
    End Sub
#End Region
#Region "GetCookies"
    Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies("Supplier")
        Me.SupplierName = cookie.Values("name")
        Me.SupplierId = cookie.Values("id")
    End Sub
#End Region
#Region "DoBind"
    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        Dim oPaging As New Parameter.GeneralPaging
        Dim m_controller As New GeneralPagingController
        With oPaging
            .strConnection = GetConnectionString
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
            .SpName = "spSupplierForecastPaging"
        End With
        oPaging = m_controller.GetGeneralPaging(oPaging)
        If Not oPaging Is Nothing Then
            DtUserList = oPaging.ListData
            recordCount = oPaging.TotalRecords
        Else
            recordCount = 0
        End If
        DtgSupplier.DataSource = DtUserList.DefaultView
        DtgSupplier.CurrentPageIndex = 0
        DtgSupplier.DataBind()
        PagingFooter()
        pnlList.Visible = True
        pnlDatagrid.Visible = True
    End Sub
#End Region
#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblRecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub
#End Region
#Region "Reset"
    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        Response.Redirect("SupplierForecast.aspx?id=" & Me.SupplierId.Trim & "&name=" & Me.SupplierName.Trim & "&branchid=" & Me.BranchID.Trim & "")
    End Sub
#End Region
#Region "Search"
    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Buttonsearch.Click
        Dim par As String
        Me.SearchBy = ""

        Me.SearchBy = "  branchid = '" & Me.BranchID & "'  and " & _
                        " supplierid = '" & Me.SupplierId & "'"
        par = par & "Branch : " & Me.BranchID & " "
        If par <> "" Then
            par = par & " , Supplier : " & Me.SupplierName.Trim & " "
        Else
            par = par & "Supplier : " & Me.SupplierName.Trim & " "
        End If

        If txtyear.Text.Trim <> "" Then
            Me.SearchBy = Me.SearchBy & " and ForecastYear = '" & txtyear.Text.Trim & "'"
            If par <> "" Then
                par = par & " , Year : " & txtyear.Text.Trim & " "
            Else
                par = par & "Year : " & txtyear.Text.Trim & " "
            End If
        End If
        If cboMonth.SelectedItem.Value <> "ALL" Then
            Me.SearchBy = Me.SearchBy & " and ForecastMonth = '" & cboMonth.SelectedItem.Value & "'"
            If par <> "" Then
                par = par & " , Month : " & cboMonth.SelectedItem.Text & " "
            Else
                par = par & "Month : " & cboMonth.SelectedItem.Text & " "
            End If
        End If

        If txtyear.Text.Trim <> "" And cboMonth.SelectedItem.Value = "ALL" Then
            Me.SortBy = " ForecastYear DESC, ForecastMonth DESC"
        ElseIf txtyear.Text.Trim = "" And cboMonth.SelectedItem.Value = "ALL" Then
            Me.SortBy = " ForecastYear DESC, ForecastMonth  DESC"
        End If

        If cboStatus.SelectedItem.Value <> "ALL" Then
            Me.SearchBy = Me.SearchBy & " and AssetUsedNew = '" & cboStatus.SelectedItem.Value & "'"
            If par <> "" Then
                par = par & " , AssetUsedNew : " & cboStatus.SelectedItem.Text & " "
            Else
                par = par & "AssetUsedNew : " & cboStatus.SelectedItem.Text & " "
            End If
        End If
        Me.FilterBy = par

        DtgSupplier.Visible = True
        pnlList.Visible = True
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region
#Region "Sort"
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region
#Region "ItemDataBound"
    Private Sub DtgSupplier_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgSupplier.ItemDataBound
        Dim NimgEdit As New ImageButton
        Dim NMonth As New Label
        Dim Nyear As New HyperLink
        If e.Item.ItemIndex >= 0 Then
            NimgEdit = CType(e.Item.FindControl("imgEdit"), ImageButton)
            Nyear = CType(e.Item.FindControl("HyYear"), HyperLink)
            NMonth = CType(e.Item.FindControl("lblBMonth"), Label)
            If CInt(Nyear.Text) > CInt(Year(Me.BusinessDate)) Then
                NimgEdit.Visible = True
            ElseIf CInt(Nyear.Text) < CInt(Year(Me.BusinessDate)) Then
                NimgEdit.Visible = False
            ElseIf CInt(Nyear.Text) = CInt(Year(Me.BusinessDate)) Then
                If CInt(NMonth.Text) <= CInt(Month(Me.BusinessDate)) Then
                    NimgEdit.Visible = False
                Else
                    NimgEdit.Visible = True
                End If
            End If
        End If
    End Sub
#End Region
#Region "Add"
    Private Sub imbAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonAdd.Click
        If CheckFeature(Me.Loginid, Me.FormID, "ADD", Me.AppId) Then
            If SessionInvalid() Then
                Exit Sub
            End If
            pnlAdd.Visible = True
            txtyearAdd.Text = ""
            pnlList.Visible = False
            pnlDatagrid.Visible = False
            pnlEdit.Visible = False
            HYSupplierID2.Text = Me.SupplierId
            lblNameAdd.Text = Me.SupplierName

            Dim i As Integer
            Dim dt As New DataTable
            Dim dtrow As DataRow

            dt.Columns.Add("No", GetType(String))
            dt.Columns.Add("InMonth", GetType(String))
            dt.Columns.Add("Unit", GetType(Integer))
            dt.Columns.Add("Amount", GetType(Double))

            For i = 0 To 13
                dtrow = dt.NewRow
                dtrow("No") = i + 1
                Select Case i
                    Case 0
                        dtrow("InMonth") = "January"
                        dtrow("Unit") = 0
                        dtrow("Amount") = 0
                    Case 1
                        dtrow("InMonth") = "February"
                        dtrow("Unit") = 0
                        dtrow("Amount") = 0
                    Case 2
                        dtrow("InMonth") = "March"
                        dtrow("Unit") = 0
                        dtrow("Amount") = 0
                    Case 3
                        dtrow("InMonth") = "April"
                        dtrow("Unit") = 0
                        dtrow("Amount") = 0
                    Case 4
                        dtrow("InMonth") = "May"
                        dtrow("Unit") = 0
                        dtrow("Amount") = 0
                    Case 5
                        dtrow("InMonth") = "June"
                        dtrow("Unit") = 0
                        dtrow("Amount") = 0
                    Case 6
                        dtrow("InMonth") = "July"
                        dtrow("Unit") = 0
                        dtrow("Amount") = 0
                    Case 7
                        dtrow("InMonth") = "August"
                        dtrow("Unit") = 0
                        dtrow("Amount") = 0
                    Case 8
                        dtrow("InMonth") = "September"
                        dtrow("Unit") = 0
                        dtrow("Amount") = 0
                    Case 9
                        dtrow("InMonth") = "October"
                        dtrow("Unit") = 0
                        dtrow("Amount") = 0
                    Case 10
                        dtrow("InMonth") = "November"
                        dtrow("Unit") = 0
                        dtrow("Amount") = 0
                    Case 11
                        dtrow("InMonth") = "December"
                        dtrow("Unit") = 0
                        dtrow("Amount") = 0
                End Select
                dt.Rows.Add(dtrow)
            Next

            DtgForecastAdd.DataSource = dt
            DtgForecastAdd.DataBind()
            DtgForecastAdd.Visible = True
        End If
    End Sub
#End Region
#Region "SaveAdd"
    Private Sub imbSaveAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonSaveAdd.Click
        If CInt(txtyearAdd.Text) < Me.BusinessDate.Year Then
            ShowMessage(lblMessage, "Tahun > Tahun BusinessDate", True)
            Exit Sub
        End If

        If DtgForecastAdd.Items.Count > 0 Then
            Dim i As Integer
            Dim dtAdd As New DataTable
            Dim dr As DataRow
            Dim inlblNo As New Label
            Dim intxtUnit As New TextBox
            Dim intxtamount As New TextBox
            Dim intxtUnitUsed As New TextBox
            Dim intxtamountUsed As New TextBox

            dtAdd.Columns.Add("No", GetType(Integer))
            dtAdd.Columns.Add("Unit", GetType(String))
            dtAdd.Columns.Add("Amount", GetType(String))
            dtAdd.Columns.Add("UnitUsed", GetType(String))
            dtAdd.Columns.Add("AmountUsed", GetType(String))

            For i = 0 To DtgForecastAdd.Items.Count - 1
                inlblNo = CType(DtgForecastAdd.Items(i).FindControl("lblNo"), Label)
                If CInt(txtyearAdd.Text.Trim) = Me.BusinessDate.Year Then
                    If CInt(inlblNo.Text.Trim) > Me.BusinessDate.Month Then
                        dr = dtAdd.NewRow()

                        intxtUnit = CType(DtgForecastAdd.Items(i).FindControl("txtUnit"), TextBox)
                        intxtamount = CType(DtgForecastAdd.Items(i).FindControl("txtAmount"), TextBox)
                        intxtUnitUsed = CType(DtgForecastAdd.Items(i).FindControl("txtUnitUsed"), TextBox)
                        intxtamountUsed = CType(DtgForecastAdd.Items(i).FindControl("txtAmountUsed"), TextBox)


                        dr("No") = CInt(inlblNo.Text.Trim)
                        dr("Unit") = intxtUnit.Text.Trim
                        dr("Amount") = intxtamount.Text.Trim
                        dr("UnitUsed") = intxtUnitUsed.Text.Trim
                        dr("AmountUsed") = intxtamountUsed.Text.Trim
                        dtAdd.Rows.Add(dr)
                    End If
                End If
                If CInt(txtyearAdd.Text.Trim) > Me.BusinessDate.Year Then
                    dr = dtAdd.NewRow()
                    inlblNo = CType(DtgForecastAdd.Items(i).FindControl("lblNo"), Label)
                    intxtUnit = CType(DtgForecastAdd.Items(i).FindControl("txtUnit"), TextBox)
                    intxtamount = CType(DtgForecastAdd.Items(i).FindControl("txtAmount"), TextBox)
                    intxtUnitUsed = CType(DtgForecastAdd.Items(i).FindControl("txtUnitUsed"), TextBox)
                    intxtamountUsed = CType(DtgForecastAdd.Items(i).FindControl("txtAmountUsed"), TextBox)
                    dr("No") = CInt(inlblNo.Text.Trim)
                    dr("Unit") = intxtUnit.Text.Trim
                    dr("Amount") = intxtamount.Text.Trim
                    dr("UnitUsed") = intxtUnitUsed.Text.Trim
                    dr("AmountUsed") = intxtamountUsed.Text.Trim
                    dtAdd.Rows.Add(dr)
                End If

            Next

            With oSupplier
                .SupplierID = Me.SupplierId
                .BranchId = Me.BranchID
                .ListData = dtAdd
                .ForecastYear = CInt(txtyearAdd.Text)
                .strConnection = GetConnectionString()
            End With

            Try

                m_controller.AddSupplierForecast(oSupplier)
                If oSupplier.strError <> "" Then
                    ShowMessage(lblMessage, "Data Sudah Ada .....", True)
                    Exit Sub
                Else

                    Initialpanel()
                    DoBind(Me.SearchBy, Me.SortBy)
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_SUCCESS, False)
                    ' Response.Redirect("SupplierBudget.aspx?message=" & MessageHelper.MESSAGE_INSERT_SUCCESS)
                End If
            Catch ex As Exception
                ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_FAILED, True)
                Exit Sub
            End Try
        End If
    End Sub
#End Region
#Region "ItemCommand"
    Private Sub DtgSupplier_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgSupplier.ItemCommand
        If e.CommandName = "Edit" Then
            If CheckFeature(Me.Loginid, Me.FormID, "EDIT", Me.AppId) Then
                If sessioninvalid() Then
                    Exit Sub
                End If
                pnlList.Visible = False
                pnlAdd.Visible = False
                rgvGo.Enabled = False
                pnlDatagrid.Visible = False

                txtPage.Text = "2"

                pnlEdit.Visible = True
                Dim HyYearEdit As New HyperLink
                Dim MonthEdit As New Label
                Dim StatusEdit As New Label
                Dim BMonthEdit As New Label
                Dim inunitEdit As New Label
                Dim inamountedit As New Label
                HySupplierIDEdit.Text = Me.SupplierId
                lblSupNameEdit.Text = Me.SupplierName
                HyYearEdit = CType(e.Item.FindControl("HyYear"), HyperLink)
                MonthEdit = CType(e.Item.FindControl("lblMONTH"), Label)
                StatusEdit = CType(e.Item.FindControl("lblAssetStatus"), Label)
                BMonthEdit = CType(e.Item.FindControl("lblBMonth"), Label)
                inunitEdit = CType(e.Item.FindControl("lblUnit"), Label)
                inamountedit = CType(e.Item.FindControl("lblAmount"), Label)
                Me.BMonth = BMonthEdit.Text.Trim
                lblYearEdit.Text = HyYearEdit.Text.Trim
                lblMonthEdit.Text = MonthEdit.Text.Trim
                lblStatusEdit.Text = StatusEdit.Text.Trim
                txtUnitEdit.Text = inunitEdit.Text
                txtAmountEdit.Text = Replace(FormatNumber(inamountedit.Text, 0), ",", "")
            End If
        End If
    End Sub
#End Region
#Region "SaveEdit"
    Private Sub imgSaveEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonSaveEdit.Click
        With oSupplier
            .BranchId = Me.BranchID
            .SupplierID = Me.SupplierId
            .ForecastYear = CInt(lblYearEdit.Text)
            .ForecastMonth = CInt(Me.BMonth)
            .Status = lblStatusEdit.Text
            .Unit = CInt(txtUnitEdit.Text)
            .Amount = CDbl(txtAmountEdit.Text)
            .strConnection = GetConnectionString()
        End With
        Try
            m_controller.EditSupplierForecast(oSupplier)
            If oSupplier.strError <> "" Then                
                ShowMessage(lblMessage, "Update Forecast Supplier gagal .....", True)
                Exit Sub
            Else
                Initialpanel()
                DoBind(Me.SearchBy, Me.SortBy)                
                ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
                ' Response.Redirect("SupplierBudget.aspx?message=" & MessageHelper.MESSAGE_UPDATE_SUCCESS)
            End If
        Catch ex As Exception            
            ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_FAILED, True)
            Exit Sub
        End Try
    End Sub
#End Region
#Region "CancelEdit"
    Private Sub imbCancelEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonCancelEdit.Click
        Initialpanel()
    End Sub
#End Region
#Region "BackAdd"
    Private Sub imbBackAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonBackAdd.Click        
        Initialpanel()
    End Sub
#End Region
#Region "Back"
    Private Sub imbBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonBack.Click
        Response.Redirect("SupplierBranch.aspx?id=" & Me.SupplierId & "&name=" & Me.SupplierName & "")
    End Sub
#End Region

    Private Sub imbPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonPrint.Click
        If CheckFeature(Me.Loginid, Me.FormID, "Print", Me.AppId) Then
            If SessionInvalid() Then
                Exit Sub
            End If

            Dim cookie As HttpCookie = Request.Cookies("SupplierF")
            If Not cookie Is Nothing Then
                cookie.Values("pagefrom") = "1"
                cookie.Values("FilterBy") = Me.FilterBy
                cookie.Values("where") = Me.SearchBy
                cookie.Values("Supplierid") = Me.SupplierId
                cookie.Values("Suppliername") = Me.SupplierName
                cookie.Values("branchid") = Me.BranchID
                cookie.Values("SortBy") = Me.SortBy
                Response.AppendCookie(cookie)
            Else
                Dim cookieNew As New HttpCookie("SupplierF")
                cookieNew.Values.Add("pagefrom", "1")
                cookieNew.Values.Add("FilterBy", Me.FilterBy)
                cookieNew.Values.Add("where", Me.SearchBy)
                cookieNew.Values.Add("Supplierid", Me.SupplierId)
                cookieNew.Values.Add("Suppliername", Me.SupplierName)
                cookieNew.Values.Add("branchid", Me.BranchID)
                cookieNew.Values.Add("SortBy", Me.SortBy)
                Response.AppendCookie(cookieNew)
            End If
            Response.Redirect("SupplierBudgetReport.aspx")
        End If
    End Sub

End Class