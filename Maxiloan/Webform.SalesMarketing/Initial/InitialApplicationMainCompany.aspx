﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="InitialApplicationMainCompany.aspx.vb"
    Inherits="Maxiloan.Webform.SalesMarketing.InitialApplicationMainCompany" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat" TagPrefix="uc1" %>
<%@ Register Src="../../webform.UserController/ucDateCE.ascx" TagName="ucDateCE" TagPrefix="uc1" %>
<%@ Register TagPrefix="uc1" TagName="ucAddress" Src="../../Webform.UserController/ucCompanyAddress.ascx" %>

<%@ Register TagPrefix="uc1" TagName="ucProdOffering" Src="../../Webform.UserController/UcLookUpPdctOffering.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Prospect Maintenance</title>
    <link rel="Stylesheet" type="text/css" href="../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../Include/Buttons.css" />
    
       <!-- Global stylesheets -->
    <link href="../../lib/limitless/assets/css/fonts.css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css" />
    <link href="../../lib/limitless/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css" />
    <link href="../../lib/limitless/assets/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="../../lib/limitless/assets/css/core.css" rel="stylesheet" type="text/css" />
    <link href="../../lib/limitless/assets/css/components.css" rel="stylesheet" type="text/css" />
    <link href="../../lib/limitless/assets/css/colors.css" rel="stylesheet" type="text/css" />
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <%--<script type="text/javascript" src="lib/limitless/assets/js/plugins/loaders/pace.min.js"></script>--%>
    <script type="text/javascript" src="../../lib/limitless/assets/js/core/libraries/jquery.min.js"></script>
    <script type="text/javascript" src="../../lib/limitless/assets/js/core/libraries/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../lib/limitless/assets/js/plugins/loaders/blockui.min.js"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="../../lib/limitless/assets/js/plugins/forms/styling/uniform.min.js"></script>

    <script type="text/javascript" src="../../lib/limitless/assets/js/plugins/notifications/pnotify.min.js"></script>
    <script type="text/javascript" src="../../lib/limitless/assets/js/pages/components_notifications_pnotify.js"></script>

    <!-- /theme JS files -->

    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <%--<script src="../../js/jquery-1.9.1.min.js"></script>--%>
    <script src="../../js/jquery-ui-1.10.3.custom.min.js"></script>    
	<%--<script src="../../../../js/jquery-2.1.1.js" type="text/javascript"></script>--%>	
     <script language="javascript" type="text/javascript">
         function recalculateAngs() {

             var otr = $('#ucOTR_txtNumber').val();
             var uangmuka = $('#ucUangMuka_txtNumber').val();
             var uangmukatradein = $('#ucUangMukaTradeIn_txtNumber').val();

             var xtotal = parseInt(otr.replace(/\s*,\s*/g, '')) -
             //                         parseInt(uangmuka.replace(/\s*,\s*/g, '')) +
                         parseInt(uangmuka.replace(/\s*,\s*/g, '')) -
                         parseInt(uangmukatradein.replace(/\s*,\s*/g, ''));
             var totaluangmuka = parseInt(uangmuka.replace(/\s*,\s*/g, '')) +
                         parseInt(uangmukatradein.replace(/\s*,\s*/g, ''));

             $('#lblTotalUangMuka').html(number_format(totaluangmuka));
             $('#lblPokokHutang').html(number_format(xtotal));
         }
         function number_format(number, decimals, dec_point, thousands_sep) {

             number = (number + '')
            .replace(/[^0-9+\-Ee.]/g, '');
             var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function (n, prec) {
                var k = Math.pow(10, prec);
                return '' + (Math.round(n * k) / k)
                .toFixed(prec);
            };
             // Fix for IE parseFloat(0.55).toFixed(0) = 0;
             s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
            .split('.');
             if (s[0].length > 3) {
                 s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
             }
             if ((s[1] || '')
            .length < prec) {
                 s[1] = s[1] || '';
                 s[1] += new Array(prec - s[1].length + 1)
              .join('0');
             }
             return s.join(dec);
         }

         function getAge(e) {
             //var dateString = $('#tanggalLahir').val()
             var dateStringfull = $('#txtTanggalLahir').val();
             var dateStringyear = dateStringfull.substring(6, 10);
             var dateStringmth = dateStringfull.substring(3, 5);
             var dateStringday = dateStringfull.substring(0, 2);
             var dateString = dateStringyear + dateStringmth + dateStringday;
             //            var Instmonth = parseInt($('#ucTenor_txtNumber').val());
             //var Instmonth = parseInt($('#ucTenor_txtNumber').val()); 
             var now = new Date();
             //now.setMonth(now.getMonth() + Instmonth);
             now.setMonth(now.getMonth() + parseInt(e));
             var today = new Date(now.getYear(), now.getMonth(), now.getDate());

             var yearNow = now.getYear();
             var monthNow = now.getMonth();
             var dateNow = now.getDate();

             var dob = new Date(dateString.substring(0, 4),
                     dateString.substring(7, 8) - 1,
                     dateString.substring(5, 6)
                     );

             var yearDob = dob.getYear();
             var monthDob = dob.getMonth();
             var dateDob = dob.getDate();
             var age = {};
             var ageString = "";
             var yearString = "";
             var monthString = "";
             var dayString = "";

             yearAge = yearNow - yearDob;

             if (monthNow >= monthDob)
                 var monthAge = monthNow - monthDob;
             else {
                 yearAge--;
                 var monthAge = 12 + monthNow - monthDob;
             }

             if (dateNow >= dateDob)
                 var dateAge = dateNow - dateDob;
             else {
                 monthAge--;
                 var dateAge = 31 + dateNow - dateDob;

                 if (monthAge < 0) {
                     monthAge = 11;
                     yearAge--;
                 }
             }

             age = {
                 years: yearAge,
                 months: monthAge,
                 days: dateAge
             };

             if (age.years > 1) yearString = " tahun";
             else yearString = " tahun";
             if (age.months > 1) monthString = " bulan";
             else monthString = " bulan";
             if (age.days > 1) dayString = " hari";
             else dayString = " hari";


             if ((age.years > 0) && (age.months > 0) && (age.days > 0))
                 ageString = age.years + yearString + ", " + age.months + monthString + ", dan " + age.days + dayString + " ";
             else if ((age.years == 0) && (age.months == 0) && (age.days > 0))
                 ageString = "Only " + age.days + dayString + " ";
             else if ((age.years > 0) && (age.months == 0) && (age.days == 0))
                 ageString = age.years + yearString + " ";
             else if ((age.years > 0) && (age.months > 0) && (age.days == 0))
                 ageString = age.years + yearString + " dan " + age.months + monthString + " ";
             else if ((age.years == 0) && (age.months > 0) && (age.days > 0))
                 ageString = age.months + monthString + " dan " + age.days + dayString + " ";
             else if ((age.years > 0) && (age.months == 0) && (age.days > 0))
                 ageString = age.years + yearString + " dan " + age.days + dayString + " ";
             else if ((age.years == 0) && (age.months > 0) && (age.days == 0))
                 ageString = age.months + monthString + " ";
             else ageString = "Oops! Tidak dapat menghitung usia akhir kontrak!";

             $('#lblusiaakhirkontrak').html(ageString);
             //modify ario
             $('#hdnusia').val(ageString);
         }


    </script>
</head>
<body>
    <asp:UpdateProgress ID="upg1" runat="server" AssociatedUpdatePanelID="upnl1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <script type = "text/javascript">
        function preventMultipleSubmissions() {
            $('#<%=btnSave.ClientID %>').prop('disabled', true).toggleClass("gray").toggleClass("prs_img_t");
        }
        window.onbeforeunload = preventMultipleSubmissions;
    </script>
    <script type = "text/javascript">
        function preventMultipleSubmissions() {
            $('#<%=btnProceed.ClientID %>').prop('disabled', true).toggleClass("gray").toggleClass("prs_img_t");
        }
        window.onbeforeunload = preventMultipleSubmissions;
    </script>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <div runat="server" id="jlookupContent" />
    <asp:UpdatePanel runat="server" ID="upnl1" UpdateMode="Conditional" ChildrenAsTriggers="false">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
                onclick="hideMessage();"></asp:Label>            
            <div class="form_title">
                <div class="form_single">
                    <h3>
                        DATA APLIKASI COMPANY</h3>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="label_req">Tanggal Aplikasi</label>
                        <asp:TextBox ID="txttanggalAplikasi" runat="server" CssClass="small_text" ></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txttanggalAplikasi"
                            Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="label_req">
                           Nama
                        </label>
                        <asp:TextBox ID="txtNama" runat="server" CssClass="medium_text"  MaxLength="50"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" display="Dynamic"
                            ControlToValidate="txtNama" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
<%--            <div class="form_box">
                <div>
                    <div class="form_single">
                         <label class="label_req">
                            Tempat / Tanggal Penerbitan</label>
                        <asp:TextBox ID="txtTempatLahir" runat="server" MaxLength="20" CssClass="medium_text"></asp:TextBox> / 
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" display="Dynamic"
                            ControlToValidate="txtTempatLahir" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
                        <asp:TextBox ID="txtTanggalLahir" runat="server" CssClass="small_text"></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtTanggalLahir"
                            Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator3" runat="server" display="Dynamic"
                            ControlToValidate="txtTanggalLahir" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>--%>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="">
                           Jenis Dokumen
                        </label>
                        <asp:DropDownList ID="cboIDTypeP" runat="server" CssClass="select">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Harap pilih Jenis Identitas"
                            ControlToValidate="cboIDTypeP" Display="Dynamic" InitialValue="Select One" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="label_req">
                           Nomor Dokumen 
                        </label>
                        <asp:TextBox ID="txtIDNumberP" runat="server" MaxLength="25" CssClass="medium_text"
                            onkeypress="return numbersonly2(event)"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegEx1" ControlToValidate="txtIDNumberP" runat="server"
                            ErrorMessage="Masukkan angka saja" ValidationExpression="\d+" CssClass="validator_general"></asp:RegularExpressionValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Harap isi No Identitas"
                            ControlToValidate="txtIDNumberP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                         <label class="label_req">
                            Tempat / Tanggal Penerbitan</label>
                        <asp:TextBox ID="txtTempatLahir" runat="server" MaxLength="20" CssClass="medium_text"></asp:TextBox> / 
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" display="Dynamic"
                            ControlToValidate="txtTempatLahir" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
                        <asp:TextBox ID="txtTanggalLahir" runat="server" CssClass="small_text"></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtTanggalLahir"
                            Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator3" runat="server" display="Dynamic"
                            ControlToValidate="txtTanggalLahir" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
            <div class="form_box_title">
                <div class="form_single">
                    <h4>
                       PEMBIAYAAN</h4>
                </div>
            </div>
             <div class="form_box">
                <div>
                    <div class="form_left">
                        <label class="label_req">Tujuan Pembiayaan</label>  
                        <asp:DropDownList ID="cboKegiatanUsaha" runat="server" AutoPostBack="true"  Width="300px"/>
                         <asp:RequiredFieldValidator ID="Requiredfieldvalidator65" runat="server" 
                            display="Dynamic"
                            ControlToValidate="cboKegiatanUsaha" CssClass="validator_general" ErrorMessage="*" InitialValue="SelectOne"></asp:RequiredFieldValidator>
                    </div>
                    <div class="form_right">
                       
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label class="label_req">
                            Akad/Skema Pembiayaan
                        </label>
                        <asp:DropDownList runat="server" ID="cboAkad"  Width="300px" AutoPostBack="true">
                            <%--<asp:listitem value="Murabahah">Murabahah</asp:listitem>--%>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator13" runat="server" 
                            display="Dynamic"
                            ControlToValidate="cboAkad" CssClass="validator_general" ErrorMessage="*" InitialValue="Select One"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
           
            <div class="form_box">
            <uc1:ucProdOffering id="ucLookupProductOffering1" runat="server"></uc1:ucProdOffering>
            </div>

            <div class="form_box_title">
                <div class="form_single">
                    <h4>
                       DATA PINJAMAN</h4>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="label_req">
                           Harga OTR
                        </label>
                        <uc1:ucnumberformat runat="server" id="ucOTR" TextCssClass="numberAlign reguler_text" onclientchange="recalculateAngs()" ></uc1:ucnumberformat>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="">
                           URBUN Tunai
                        </label>
                        <uc1:ucnumberformat runat="server" id="ucUangMuka" TextCssClass="numberAlign reguler_text" onclientchange="recalculateAngs()" ></uc1:ucnumberformat>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="">
                           URBUN Trade In
                        </label>
                        <uc1:ucnumberformat runat="server" id="ucUangMukaTradeIn" TextCssClass="numberAlign reguler_text"  onclientchange="recalculateAngs()" ></uc1:ucnumberformat>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="">
                           Total URBUN
                        </label>
                        <asp:Label runat="server" ID="lblTotalUangMuka"  CssClass="numberAlign regular_text"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="">
                           Pokok Hutang
                        </label>
                        <asp:Label runat="server" ID="lblPokokHutang"  CssClass="numberAlign regular_text"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="label_req">
                           Angsuran Bulanan
                        </label>
                        <uc1:ucnumberformat runat="server" id="ucAngsuranBulanan" TextCssClass="numberAlign reguler_text" ></uc1:ucnumberformat>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="label_req">
                           Jangka Waktu
                        </label>
                        <asp:HiddenField runat="server" ID="tanggalLahir" />
                        <uc1:ucnumberformat runat="server" id="ucTenor" TextCssClass="numberAlign reguler_text" onclientchange="getAge()" ></uc1:ucnumberformat>
                    </div>
                </div>
            </div>
            <asp:Panel ID="PanelInfo" runat="server">
                <div class="form_box_title">
                    <div class="form_single">
                        <h4>
                            Similar Customer Data</h4>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ws">
                            <asp:DataGrid id="dtgList1" runat="server" Width="100%" AutoGenerateColumns="False" BorderWidth="0"
									cellpadding="3" cellspacing="1" DataKeyField="Name" CssClass="grid_general">
									<HeaderStyle CssClass="th"></HeaderStyle>
                                    <ItemStyle CssClass="item_grid" />
									<Columns>
										<asp:TemplateColumn HeaderText="NO">
											<ItemStyle HorizontalAlign="CENTER"></ItemStyle>
											<ItemTemplate>
												<asp:Label ID="lblNo1" Runat="server"></asp:Label>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="NAME">
											<ItemStyle HorizontalAlign="left"></ItemStyle>
											<ItemTemplate>
												<asp:linkbutton id="lnkCustName" runat="server" CausesValidation=false text='<%# DataBinder.Eval(Container, "DataItem.Name") %>'>
												</asp:linkbutton>
												<asp:Label ID="lblCust" Runat=server Visible=False text='<%# DataBinder.eval(Container,"DataItem.CustomerID") %>'>
												</asp:Label>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:BoundColumn DataField="IDType" HeaderText="ID TYPE"></asp:BoundColumn>
										<asp:BoundColumn DataField="IDNumber" HeaderText="ID NUMBER"></asp:BoundColumn>
										<asp:BoundColumn DataField="BirthDate" ItemStyle-HorizontalAlign="Center" HeaderText="BIRTH DATE"
											DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
										<asp:BoundColumn DataField="Type" ItemStyle-HorizontalAlign="left" HeaderText="TYPE"></asp:BoundColumn>
									</Columns>
								</asp:DataGrid>
                        </div>
                    </div>
                </div>
                <div class="form_box_title">
                    <div class="form_single">
                        <h4>
                            Negative List</h4>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ws">
                            <asp:DataGrid id="dtgList2" runat="server" Width="100%" AutoGenerateColumns="False" BorderWidth="0"
									cellpadding="3" cellspacing="1" DataKeyField="Name" CssClass="grid_general">
									<HeaderStyle CssClass="th"></HeaderStyle>
                                    <ItemStyle CssClass="item_grid" />
									<Columns>
										<asp:TemplateColumn HeaderText="NO">
											<ItemStyle HorizontalAlign="CENTER"></ItemStyle>
											<ItemTemplate>
												<asp:Label ID="lblNo2" Runat="server"></asp:Label>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="NAME">
											<ItemStyle HorizontalAlign="left"></ItemStyle>
											<ItemTemplate>
												<asp:linkbutton id="lnkCustName2" runat="server" CausesValidation=false text='<%# DataBinder.eval(Container,"DataItem.Name") %>'>
												</asp:linkbutton>
												<asp:Label ID="lblCust2" Runat=server Visible=False text='<%# DataBinder.eval(Container,"DataItem.CustomerID") %>'>
												</asp:Label>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:BoundColumn DataField="IDType" HeaderText="ID TYPE"></asp:BoundColumn>
										<asp:BoundColumn DataField="IDNumber" HeaderText="ID NUMBER"></asp:BoundColumn>
										<asp:BoundColumn ItemStyle-HorizontalAlign="Center" DataField="BirthDate" HeaderText="BIRTH DATE"
											DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
										<asp:BoundColumn DataField="Type" HeaderText="TYPE"></asp:BoundColumn>
									</Columns>
								</asp:DataGrid>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <div class="form_button">
                <asp:Button ID="btnProceed" runat="server" Text="Proceed" CssClass="small button green" 
                    CausesValidation="True" Visible="false"></asp:Button>
                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue"
                    CausesValidation="True"></asp:Button>
                <asp:Button ID="btnCancel" runat="server" Text="Back" CssClass="small button gray"
                    CausesValidation="False"></asp:Button>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnProceed" EventName="Click" />            
            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />  
             <asp:AsyncPostBackTrigger ControlID="cboKegiatanUsaha" EventName="SelectedIndexChanged" />
            <%--<asp:AsyncPostBackTrigger ControlID="cboJenisPembiyaan" EventName="SelectedIndexChanged" />--%>
            <asp:AsyncPostBackTrigger ControlID="cboAkad" EventName="SelectedIndexChanged" />           
        </Triggers>
    </asp:UpdatePanel>
    </form>
</body>
</html>
