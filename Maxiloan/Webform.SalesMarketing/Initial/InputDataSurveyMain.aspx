﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="InputDataSurveyMain.aspx.vb"
    Inherits="Maxiloan.Webform.SalesMarketing.InputDataSurveyMain" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat" TagPrefix="uc1" %>
<%@ Register Src="../../webform.UserController/ucDateCE.ascx" TagName="ucDateCE" TagPrefix="uc1" %>
<%@ Register TagPrefix="uc1" TagName="ucAddress" Src="../../Webform.UserController/ucCompanyAddress2.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Input Data Survey</title>
    <link rel="Stylesheet" type="text/css" href="../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../Include/Buttons.css" />
    <link href="../../Include/<%= request("style") %>.css" type="text/css" rel="stylesheet" />

       <!-- Global stylesheets -->
    <link href="../../lib/limitless/assets/css/fonts.css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css" />
    <link href="../../lib/limitless/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css" />
    <link href="../../lib/limitless/assets/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="../../lib/limitless/assets/css/core.css" rel="stylesheet" type="text/css" />
    <link href="../../lib/limitless/assets/css/components.css" rel="stylesheet" type="text/css" />
    <link href="../../lib/limitless/assets/css/colors.css" rel="stylesheet" type="text/css" />
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <%--<script type="text/javascript" src="lib/limitless/assets/js/plugins/loaders/pace.min.js"></script>--%>
    <script type="text/javascript" src="../../lib/limitless/assets/js/core/libraries/jquery.min.js"></script>
    <script type="text/javascript" src="../../lib/limitless/assets/js/core/libraries/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../lib/limitless/assets/js/plugins/loaders/blockui.min.js"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="../../lib/limitless/assets/js/plugins/forms/styling/uniform.min.js"></script>

    <script type="text/javascript" src="../../lib/limitless/assets/js/plugins/notifications/pnotify.min.js"></script>
    <script type="text/javascript" src="../../lib/limitless/assets/js/pages/components_notifications_pnotify.js"></script>

    <!-- /theme JS files -->

     <script language="javascript" type="text/javascript">
        function OpenWinViewProspectInquiry(pProspectAppId) {
            var x = screen.width; var y = screen.height - 100;
            window.open(ServerName + App + '/Webform.SalesMarketing/Inquiry/ViewProspect.aspx?ProspectAppId=' + pProspectAppId, '&Style=<%=Request("style") %>', 'left=15, top=10, width=1000, height=800, menubar=0, scrollbars=yes');
        }
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <%--<script src="../../js/jquery-1.9.1.min.js"></script>--%>
    <script src="../../js/jquery-ui-1.10.3.custom.min.js"></script>    
	<%--<script src="../../../../js/jquery-2.1.1.js" type="text/javascript"></script>--%>	

</head>
<body>
    <asp:UpdateProgress ID="upg1" runat="server" AssociatedUpdatePanelID="upnl1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <script type = "text/javascript">
        function preventMultipleSubmissions() {
            $('#<%=btnSave.ClientID %>').prop('disabled', true).toggleClass("gray").toggleClass("prs_img_t");
        }
        window.onbeforeunload = preventMultipleSubmissions;
    </script>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <div runat="server" id="jlookupContent" />
    <asp:UpdatePanel runat="server" ID="upnl1" UpdateMode="Conditional" ChildrenAsTriggers="false">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
                onclick="hideMessage();"></asp:Label>            
    <div class="form_title">
        <div class="form_single">
            <h3>
                DATA APLIKASI</h3>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Tanggal Aplikasi
            </label>
            <asp:Label ID="lbltanggalAplikasi" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                No Aplikasi
            </label>
            <asp:HyperLink ID="lblProspectAppId" runat="server" onclientclick="OpenWinViewProspectInquiry(this)"></asp:HyperLink>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Nama
            </label>
            <asp:Label ID="lblNama" runat="server"></asp:Label>
        </div>
    </div>
             <%--modify Nofi 2Mar2018--%>
<%--    <div class="form_box_title">
        <div class="form_single">
            <h4>
               DATA ASSET</h4>
        </div>
    </div>--%>
<%--    <div class="form_box">
        <div class="form_single">
                <div style="float:left">
                    <label class="label_req">
                        Supplier/Dealer</label>                            
                    <asp:HiddenField runat="server" ID="hdfProspectAppID" />      
                    <div style="display:none">
                    <asp:TextBox runat="server" ID="txtSupplierCode" Enabled="false" CssClass="small_text"></asp:TextBox>              
                    </div>
                    <asp:TextBox runat="server" ID="txtSupplierName" Enabled="false" CssClass="medium_text" AutoPostBack="true" ></asp:TextBox>
                </div>
                <div style="margin-top:2px; float:left">
                    <asp:Panel ID="pnlLookupSupplier" runat="server">
                    <button class="small buttongo blue" 
                    onclick ="OpenJLookup('<%= ResolveClientUrl("~/webform.Utility/jLookup/SupplierProspect.aspx?kode=" & txtSupplierCode.ClientID & "&nama=" & txtSupplierName.ClientID &"&ProspectAppID="& hdfProspectAppID.Value) %>','Daftar Supplier','<%= jlookupContent.ClientID %>');return false;">...</button>                                                                            
                    <asp:RequiredFieldValidator ID="rfvtxtSupplier" runat="server" ErrorMessage="*" Display="Dynamic" 
                        CssClass="validator_general" ControlToValidate="txtSupplierCode"></asp:RequiredFieldValidator>
                    </asp:Panel>
                </div>
                <div style="display: inline-block; padding-left:20px; margin-top:4px;">
                    <asp:Label runat="server" ID="lblSupplierStatus" ></asp:Label>
                </div>
        </div>
    </div>--%>
    <div class="form_box_hide">
        <div>
            <div class="form_single">
                <label class="">
                   Supplier/Dealer
                </label>
                   <asp:Label runat="server" ID="lblSupplierName" ></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box_hide">
        <div>
            <div class="form_single">
                <label class="">
                   Jenis Asset
                </label>
                   <asp:Label runat="server" ID="lblJenisAsset" ></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box_hide">
        <div>
            <div class="form_single">
                <label class="">
                   Merk Asset
                </label>
                   <asp:Label runat="server" ID="lblMerkAsset" ></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box_title">
        <div class="form_single">
            <h4>
               ADDRESS</h4>
        </div>
    </div>
    <div class="form_box">
            <uc1:ucAddress id="UCMailingAddress" runat="server"></uc1:ucAddress>
    </div>
    <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                   Mobile Phone
                </label>
                <asp:TextBox ID="txtMobilePhone" runat="server" CssClass="medium_text"  MaxLength="20" onkeypress="return numbersonly2(event)"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtMobilePhone" ErrorMessage="Harap isi Mobile Phone" Display="Dynamic" CssClass ="validator_general" />
                <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="txtMobilePhone" CssClass="validator_general"
                            ID="RegularExpressionValidator2" ValidationExpression="^\d{1,}$|^\d{1,}[.{1}]\d{1,}$" runat="server"
                            ErrorMessage="Input angka saja!!!"></asp:RegularExpressionValidator>

            </div>
    </div>
    <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                   Nama Ibu Kandung
                </label>
                <asp:TextBox ID="txtMotherName" runat="server" CssClass="medium_text"  MaxLength="50"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtMotherName" ErrorMessage="Harap isi Nama Ibu Kandung" Display="Dynamic" CssClass ="validator_general" />
            </div>
    </div>

    <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                   Cara Survey
                </label>
                <asp:DropDownList ID="cboCaraSurvey" runat="server">
                    <asp:ListItem Value="">Select One</asp:ListItem>
                    <asp:ListItem Value="A">WEB</asp:ListItem>
                    <asp:ListItem Value="M">Mobile</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="cboCaraSurvey" ErrorMessage="Harap isi Cara Survey" Display="Dynamic" CssClass ="validator_general" />
            </div>
    </div>

    <div class="form_box_hide">
            <div class="form_single">
                <label class="label_req">
                   Nomor Rumah
                </label>
                <asp:TextBox ID="txtNoRumah" runat="server" CssClass="small_text"  MaxLength="3" ></asp:TextBox>
            </div>
    </div>
        <div class="form_box_hide">
            <div class="form_single">
                <label class="label_req">
                   Nama Jalan
                </label>
                <asp:TextBox ID="txtStreetName" runat="server" CssClass="medium_text"  MaxLength="150" ></asp:TextBox>
            </div>
    </div>
    <div class="form_box_hide">
            <div class="form_single">
                <label class="label_req">
                   Negara
                </label>
                <asp:TextBox ID="txtNegara" runat="server" CssClass="medium_text"  MaxLength="150" enabled=false></asp:TextBox>
            </div>
    </div>
    <div class="form_title">
        <div class="form_single">
            <h3>
                KARYAWAN</h3>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label class="label_req label_auto">
                CMO &nbsp;&nbsp;</label>
                <asp:DropDownList ID="cbocmo" runat="server" >
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Harap pilih CMO"
                            ControlToValidate="cbocmo" Display="Dynamic" InitialValue="Select One" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
    </div>
            <div class="form_button">
                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue"
                    CausesValidation="True"></asp:Button>
                <asp:Button ID="btnCancel" runat="server" Text="Back" CssClass="small button gray"
                    CausesValidation="False"></asp:Button>
            </div>
        </ContentTemplate>
        <Triggers>                        
            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
    </form>
</body>
</html>
