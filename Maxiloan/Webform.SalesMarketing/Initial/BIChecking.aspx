﻿<%@ Page EnableEventValidation="false" Language="vb" AutoEventWireup="false" CodeBehind="BIChecking.aspx.vb"
    Inherits="Maxiloan.Webform.SalesMarketing.BIChecking" %>

<%@ Register Src="../../webform.UserController/ObjectMarket/UcFileDocumentUploader.ascx" TagName="ucFileUpload" TagPrefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Initial Prospect Maintenance</title>
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <%--<link href="../../Include/<%= request("style") %>.css" type="text/css" rel="stylesheet" />--%>

       <!-- Global stylesheets -->
    <link href="../../lib/limitless/assets/css/fonts.css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css" />
    <link href="../../lib/limitless/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css" />
    <link href="../../lib/limitless/assets/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="../../lib/limitless/assets/css/core.css" rel="stylesheet" type="text/css" />
    <link href="../../lib/limitless/assets/css/components.css" rel="stylesheet" type="text/css" />
    <link href="../../lib/limitless/assets/css/colors.css" rel="stylesheet" type="text/css" />
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <%--<script type="text/javascript" src="lib/limitless/assets/js/plugins/loaders/pace.min.js"></script>--%>
    <script type="text/javascript" src="../../lib/limitless/assets/js/core/libraries/jquery.min.js"></script>
    <script type="text/javascript" src="../../lib/limitless/assets/js/core/libraries/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../lib/limitless/assets/js/plugins/loaders/blockui.min.js"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="../../lib/limitless/assets/js/plugins/forms/styling/uniform.min.js"></script>

    <script type="text/javascript" src="../../lib/limitless/assets/js/pages/login.js"></script>
    <script type="text/javascript" src="../../lib/limitless/assets/js/plugins/notifications/pnotify.min.js"></script>
    <script type="text/javascript" src="../../lib/limitless/assets/js/pages/components_notifications_pnotify.js"></script>

    <!-- /theme JS files -->

     <script language="javascript" type="text/javascript">
        function OpenWinViewProspectInquiry(pProspectAppId) {
            var x = screen.width; var y = screen.height - 100;
            window.open(ServerName + App + '/Webform.SalesMarketing/Inquiry/ViewProspect.aspx?ProspectAppId=' + pProspectAppId, '&Style=<%=Request("style") %>', 'left=15, top=10, width=1000, height=800, menubar=0, scrollbars=yes');
        }
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
   
     <script language="javascript" type="text/javascript">
         function recalculateAngs() {

             var otr = $('#lblOTR_txtNumber').val();
             var uangmuka = $('#lblUangMuka_txtNumber').val();
             var uangmukatradein = $('#lblUangMukaTradeIn_txtNumber').val();

             var xtotal = parseInt(otr.replace(/\s*,\s*/g, '')) -
                         parseInt(uangmuka.replace(/\s*,\s*/g, '')) +
                         parseInt(uangmukatradein.replace(/\s*,\s*/g, ''));
             var totaluangmuka = parseInt(uangmuka.replace(/\s*,\s*/g, '')) +
                         parseInt(uangmukatradein.replace(/\s*,\s*/g, ''));

             $('#lblTotalUangMuka').html(number_format(totaluangmuka));
             $('#lblPokokHutang').html(number_format(xtotal));
         }

         function showimagepreview(input, imgClient) {
             if (input.files && input.files[0]) {
                 var filerdr = new FileReader();
                 filerdr.onload = function (e) {
                     $('#' + imgClient).attr('src', e.target.result);
                 }
                 filerdr.readAsDataURL(input.files[0]);
             }
         }

         function number_format(number, decimals, dec_point, thousands_sep) {

             number = (number + '')
            .replace(/[^0-9+\-Ee.]/g, '');
             var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function (n, prec) {
                var k = Math.pow(10, prec);
                return '' + (Math.round(n * k) / k)
                .toFixed(prec);
            };
             // Fix for IE parseFloat(0.55).toFixed(0) = 0;
             s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
            .split('.');
             if (s[0].length > 3) {
                 s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
             }
             if ((s[1] || '')
            .length < prec) {
                 s[1] = s[1] || '';
                 s[1] += new Array(prec - s[1].length + 1)
              .join('0');
             }
             return s.join(dec);
         }

         function getAge() {
             var dateString = $('#tanggalLahir').val()
             var Instmonth = parseInt($('#ucTenor_txtNumber').val());
             var now = new Date();
             now.setMonth(now.getMonth() + Instmonth);
             var today = new Date(now.getYear(), now.getMonth(), now.getDate());

             var yearNow = now.getYear();
             var monthNow = now.getMonth();
             var dateNow = now.getDate();


             var dob = new Date(dateString.substring(0, 4),
                     dateString.substring(7, 8) - 1,
                     dateString.substring(5, 6)
                     );

             var yearDob = dob.getYear();
             var monthDob = dob.getMonth();
             var dateDob = dob.getDate();
             var age = {};
             var ageString = "";
             var yearString = "";
             var monthString = "";
             var dayString = "";


             yearAge = yearNow - yearDob;

             if (monthNow >= monthDob)
                 var monthAge = monthNow - monthDob;
             else {
                 yearAge--;
                 var monthAge = 12 + monthNow - monthDob;
             }

             if (dateNow >= dateDob)
                 var dateAge = dateNow - dateDob;
             else {
                 monthAge--;
                 var dateAge = 31 + dateNow - dateDob;

                 if (monthAge < 0) {
                     monthAge = 11;
                     yearAge--;
                 }
             }

             age = {
                 years: yearAge,
                 months: monthAge,
                 days: dateAge
             };

             if (age.years > 1) yearString = " tahun";
             else yearString = " tahun";
             if (age.months > 1) monthString = " bulan";
             else monthString = " bulan";
             if (age.days > 1) dayString = " hari";
             else dayString = " hari";


             if ((age.years > 0) && (age.months > 0) && (age.days > 0))
                 ageString = age.years + yearString + ", " + age.months + monthString + ", dan " + age.days + dayString + " ";
             else if ((age.years == 0) && (age.months == 0) && (age.days > 0))
                 ageString = "Only " + age.days + dayString + " ";
             else if ((age.years > 0) && (age.months == 0) && (age.days == 0))
                 ageString = age.years + yearString + " ";
             else if ((age.years > 0) && (age.months > 0) && (age.days == 0))
                 ageString = age.years + yearString + " dan " + age.months + monthString + " ";
             else if ((age.years == 0) && (age.months > 0) && (age.days > 0))
                 ageString = age.months + monthString + " dan " + age.days + dayString + " ";
             else if ((age.years > 0) && (age.months == 0) && (age.days > 0))
                 ageString = age.years + yearString + " dan " + age.days + dayString + " ";
             else if ((age.years == 0) && (age.months > 0) && (age.days == 0))
                 ageString = age.months + monthString + " ";
             else ageString = "Oops! Tidak dapat menghitung usia akhir kontrak!";

             $('#lblusiaakhirkontrak').html(ageString);
         }

         function showOnloadAllImage() {
             showimagepreview(document.getElementById('FotoPasangan'), 'FotoPasangan')
         }
    </script>
</head>
<body onload="showOnloadAllImage();">
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
    <asp:Panel runat="server" ID="pnlSearch">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    CARI APLIKASI</h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cari Berdasarkan
                </label>
                <asp:DropDownList ID="cboSearch" runat="server">
                    <asp:ListItem Value="Name">Name</asp:ListItem>
                    <asp:ListItem Value="ProspectAppID">No Aplikasi</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSearch" runat="server" CssClass="small button blue" Text="Find" />
            <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="small button gray" />
        </div>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlList">
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                DAFTAR APLIKASI SLIK CHECKING</h3>
        </div>
    </div>    
        <div class="form_box_header">
            <div class="form_single">
                <asp:DataGrid ID="dtgPaging" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                    OnSortCommand="Sorting" BorderWidth="0px" CellPadding="3" CellSpacing="1" CssClass="grid_general">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <Columns>
                            <asp:TemplateColumn HeaderText="PILIH">
                                <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
<%--                                    <asp:ImageButton ID="imbClaim" ImageUrl="../../Images/IconLetter.gif" CommandName="Approve"
                                        runat="server" OnClientClick="return confirm('Apakah Anda yakin aplikasi ini akan diapprove ?')" >
                                        </asp:ImageButton>--%>
                                    <asp:ImageButton ID="imbChoose" ImageUrl="../../Images/IconLetter.gif" CommandName="Approve"
                                        runat="server"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
<%--                            <asp:TemplateColumn>
                                <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkReturn" runat="server" CommandName="Return" Text='Return'
                                        CausesValidation="false" OnClientClick="return confirm('Apakah Anda yakin aplikasi ini akan direturn ?')" >
                                        </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>--%>
<%--                            <asp:TemplateColumn HeaderText="REJECT">
                                <ItemStyle HorizontalAlign="Center" Width="3%" VerticalAlign="Middle"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbEdit" ImageUrl="../../Images/IconNegCov.gif" CommandName="Reject"
                                        runat="server" OnClientClick="return confirm('Apakah Anda yakin aplikasi ini akan direject ?')" >
                                        </asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>--%>
                            <asp:TemplateColumn HeaderText="DTL">
                                <ItemStyle HorizontalAlign="Center" Width="3%" VerticalAlign="Middle"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbPrint" ImageUrl="../../Images/icondetail.gif" CommandName="Detail"
                                        runat="server"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="ProspectAppID" HeaderText="DATA ID">                            
                            <ItemTemplate>
                                <asp:HyperLink runat="server" ID="lblProspectAppID" Text='<%# DataBinder.eval(Container,"DataItem.ProspectAppID")%>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="Name" HeaderText="Name">                            
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblName" Text='<%# DataBinder.eval(Container,"DataItem.Name")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="CustomerType" HeaderText="Type">                            
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblType" Text='<%# DataBinder.eval(Container,"DataItem.CustomerType")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="ProspectAppDate" HeaderText="Date">                            
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblDate" Text='<%# DataBinder.eval(Container,"DataItem.ProspectAppDate")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                       <asp:TemplateColumn SortExpression="Description" HeaderText="Product">                            
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblProductDescription" Text='<%# DataBinder.Eval(Container, "DataItem.Description")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="ProspectSource" HeaderText="SOURCE">                            
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblProspectSource" Text='<%# DataBinder.Eval(Container, "DataItem.ProspectSource")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>             
                    </Columns>
                </asp:DataGrid>
                <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                        CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="First">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                        CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Prev">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                        CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Next">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                        CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Last">
                    </asp:ImageButton>
                    Page
                    <asp:TextBox ID="txtGoPage" runat="server" cssClass="small_text">1</asp:TextBox>
                    <asp:Button ID="btnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                        EnableViewState="False" />
                    <asp:RangeValidator ID="Rangevalidator1" runat="server" ControlToValidate="txtGopage"
                        MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer"
                         CssClass="validator_general"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtGopage"
                        ErrorMessage="No Halaman Salah" 
                        Display="Dynamic"  CssClass="validator_general"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="rgvGo" runat="server" ErrorMessage="No Halaman Salah" MinimumValue="1"
                        Type="integer" ControlToValidate="txtGoPage" CssClass="validator_general"></asp:RangeValidator>
                </div>
                <div class="label_gridnavigation">
                    <asp:Label ID="lblPage" runat="server"></asp:Label>of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                    <asp:Label ID="lblTotRec" runat="server"></asp:Label>record(s)
                </div>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel runat="server" ID="PnlViewDetail">
    <div class="form_title">
    <div class="form_single">
            <h3>
                VIEW - DETAIL INITIAL
            </h3>
        </div>
    </div>
    <div class="form_title">
        <div class="form_single">
            <h3>
                DATA APLIKASI</h3>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Data ID
            </label>
            <asp:HyperLink ID="lblProspectAppId" runat="server"></asp:HyperLink>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Tanggal Aplikasi
            </label>
            <asp:Label ID="lbltanggalAplikasi" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Nama
            </label>
            <asp:Label ID="lblNama" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Tempat Lahir
            </label>
            <asp:Label ID="lblTempatLahir" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Tanggal Lahir
            </label>
            <asp:Label ID="lblTanggalLahir" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Jenis Dokumen
            </label>
            <asp:Label ID="lblIDTypeP" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Nomor Dokumen 
            </label>
            <asp:Label ID="lblIDNumberP" runat="server"></asp:Label>
        </div>
    </div>
        <%--modify Nofi 2Mar2018--%>
  <%--  <div class="form_box_title">
        <div class="form_single">
            <h4>
               DATA ASSET</h4>
        </div>
    </div>--%>
    <div class="form_box_hide">
        <div class="form_single">
            <label>
                Supplier/Dealer
            </label>
            <asp:Label ID="lblSupplierName" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box_hide">
        <div class="form_single">
            <label>
                Jenis Asset
            </label>
            <asp:Label ID="lblAssetType" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box_hide">
        <div class="form_single">
            <label>
                Merk Asset
            </label>
            <asp:Label ID="lblAssetBrand" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box_title">
        <div class="form_single">
            <h4>
               DATA PEMBIAYAAN</h4>
        </div>
    </div>

    <div class="form_box">
        <div class="form_single">
            <label>
                Tujuan Pembiayaan
            </label>
            <asp:Label ID="lblTujuanPembiayaan" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Akad / Skema
            </label>
            <asp:Label ID="lblAkadSkema" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Product Jual
            </label>
            <asp:Label ID="lblProductJual" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Harga OTR
            </label>
            <asp:Label ID="lblOTR" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                URBUN Tunai
            </label>
            <asp:Label ID="lblUangMuka" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box_hide">
        <div class="form_single">
            <label>
                URBUN Trade In
            </label>
            <asp:Label ID="lblUangMukaTradeIn" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Total URBUN
            </label>
            <asp:Label ID="lblTotalUangMuka" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Pokok Hutang
            </label>
            <asp:Label ID="lblPokokHutang" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Angsuran Bulanan
            </label>
            <asp:Label ID="lblAngsuranBulanan" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Jangka Waktu
            </label>
            <asp:Label ID="lblTenor" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box" runat="server" id="divAkhirKontrak">
        <div class="form_single">
            <label>
                 Usia Akhir Kontrak
            </label>
            <asp:Label ID="lblusiaakhirkontrak" runat="server"></asp:Label>
        </div>
    </div>

    
<%--    <div class="form_box_title">
        <div class="form_single">
            <h4>FOTO Customer & Pasangan</h4>
        </div>
    </div>              --%>
         <%--<div class="form_box">--%>
         <div class="form_box_hide">
             <div class="form_left">
                   <div>
                       <asp:Image ID="FotoCustomer" runat="server" />
                   </div>
             </div>
             <div class="form_right">
                   <div>
                       <asp:Image ID="FotoPasangan" runat="server" />
                   </div>
             </div>
             
         </div>
    <div class="form_button">
        <asp:Button ID="btnCLose" runat="server" Text="Close" CssClass="small button green"></asp:Button>
    </div>
    </asp:Panel>

    <asp:UpdatePanel runat="server" ID="PnlDecision" UpdateMode="Conditional" ChildrenAsTriggers="false">
    <ContentTemplate>
        <div class="form_title">
        <div class="form_single">
            <h3>
                DECISION </h3>
        </div>
        </div>
        
        <div class="form_box">
            <div class="form_left">
                <label class="label_req">
                    Upload IDEB File
                </label>
                <uc1:ucFileUpload ID="fileUpload" runat="server" />
            </div>
        </div>

        <div class="form_box">
            <div class="form_left">
                <label class="label_req">
                    Decision
                </label>
                <asp:DropDownList ID="cboDecision" runat="server" >
                    <asp:ListItem Selected="True" Value="Select One">Select One</asp:ListItem>
                    <asp:ListItem Value="A">Approved</asp:ListItem>
                    <asp:ListItem Value="R">Return</asp:ListItem>
                    <asp:ListItem Value="J">Reject</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="rvlDecision" runat="server" Display="Dynamic"
                            InitialValue="Select One" ControlToValidate="cboDecision" ErrorMessage="Harap pilih Decision"
                            CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
            </div>
        <div class="form_box">
            <div class="form_left">
                <label class="label_req">
                    Hubungan Bank dan Leasing
                </label>
                <asp:DropDownList ID="cboHubBankLeasing" runat="server" >
                    <asp:ListItem Selected="True" Value="Sangat Baik">SANGAT BAIK</asp:ListItem>
                    <asp:ListItem Value="Baik">BAIK</asp:ListItem>
                    <asp:ListItem Value="Cukup">CUKUP</asp:ListItem>
                    <asp:ListItem Value="Kurang">KURANG</asp:ListItem>
                    <asp:ListItem Value="Sangat Kurang">SANGAT KURANG</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="Dynamic"
                            InitialValue="Select One" ControlToValidate="cboHubBankLeasing" ErrorMessage="Harap pilih Salah Satu"
                            CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                        <label class="label_req">Remarks Decision</label>  
                        <asp:TextBox runat="server" ID="txtNotes" TextMode="MultiLine" Width="1000px" MaxLength="50" Text=""></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Harap isi Remarks Decision "
                        ControlToValidate="txtNotes" CssClass="validator_general" ></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue" CausesValidation="True"/>
                <asp:Button ID="btnCancelSave" runat="server" Text="Cancel" CssClass="small button gray"
                    CausesValidation="False" />
            </div>
        
        </ContentTemplate>
        <Triggers>
            <%--<asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />--%>
            <asp:PostBackTrigger ControlID="btnSave" />
            <asp:PostBackTrigger ControlID="btnCancelSave" />
        </Triggers>

    </asp:UpdatePanel>
    </form>
</body>
</html>