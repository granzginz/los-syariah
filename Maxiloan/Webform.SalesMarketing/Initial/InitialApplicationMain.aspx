﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="InitialApplicationMain.aspx.vb"
    Inherits="Maxiloan.Webform.SalesMarketing.InitialApplicationMain" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat" TagPrefix="uc1" %>
<%@ Register Src="../../webform.UserController/ucDateCE.ascx" TagName="ucDateCE" TagPrefix="uc1" %>
<%@ Register TagPrefix="uc1" TagName="ucAddress" Src="../../Webform.UserController/ucCompanyAddress.ascx" %>

<%@ Register TagPrefix="uc1" TagName="ucProdOffering" Src="../../Webform.UserController/UcLookUpPdctOffering.ascx" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Prospect Maintenance</title>
    <link rel="Stylesheet" type="text/css" href="../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../Include/Buttons.css" />
    
       <!-- Global stylesheets -->
    <link href="../../lib/limitless/assets/css/fonts.css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css" />
    <link href="../../lib/limitless/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css" />
    <link href="../../lib/limitless/assets/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="../../lib/limitless/assets/css/core.css" rel="stylesheet" type="text/css" />
    <link href="../../lib/limitless/assets/css/components.css" rel="stylesheet" type="text/css" />
    <link href="../../lib/limitless/assets/css/colors.css" rel="stylesheet" type="text/css" />
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <%--<script type="text/javascript" src="lib/limitless/assets/js/plugins/loaders/pace.min.js"></script>--%>
    <script type="text/javascript" src="../../lib/limitless/assets/js/core/libraries/jquery.min.js"></script>
    <script type="text/javascript" src="../../lib/limitless/assets/js/core/libraries/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../lib/limitless/assets/js/plugins/loaders/blockui.min.js"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="../../lib/limitless/assets/js/plugins/forms/styling/uniform.min.js"></script>

    <script type="text/javascript" src="../../lib/limitless/assets/js/plugins/notifications/pnotify.min.js"></script>
    <script type="text/javascript" src="../../lib/limitless/assets/js/pages/components_notifications_pnotify.js"></script>

    <!-- /theme JS files -->

    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <%--<script src="../../js/jquery-1.9.1.min.js"></script>--%>
    <script src="../../js/jquery-ui-1.10.3.custom.min.js"></script>    
	<%--<script src="../../../../js/jquery-2.1.1.js" type="text/javascript"></script>--%>	
     <script language="javascript" type="text/javascript">
         function recalculateAngs() {

             var otr = $('#ucOTR_txtNumber').val();
             var uangmuka = $('#ucUangMuka_txtNumber').val();
             //var uangmukatradein = $('#ucUangMukaTradeIn_txtNumber').val();             

             var xtotal = parseInt(otr.replace(/\s*,\s*/g, '')) -
             //                         parseInt(uangmuka.replace(/\s*,\s*/g, '')) +
                         parseInt(uangmuka.replace(/\s*,\s*/g, '')) ; //-
                         //parseInt(uangmukatradein.replace(/\s*,\s*/g, ''));
             var totaluangmuka = parseInt(uangmuka.replace(/\s*,\s*/g, '')); //+ parseInt(uangmukatradein.replace(/\s*,\s*/g, ''));

             //$('#lblTotalUangMuka').html(number_format(totaluangmuka)); 
             //$('#lblPokokHutang').html(number_format(xtotal));
             $('#txtTotalUangMuka').val(number_format(totaluangmuka));
             $('#txtPokokHutang').val(number_format(xtotal));
         }
         function number_format(number, decimals, dec_point, thousands_sep) {

             number = (number + '')
            .replace(/[^0-9+\-Ee.]/g, '');
             var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function (n, prec) {
                var k = Math.pow(10, prec);
                return '' + (Math.round(n * k) / k)
                .toFixed(prec);
            };
             // Fix for IE parseFloat(0.55).toFixed(0) = 0;
             s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
            .split('.');
             if (s[0].length > 3) {
                 s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
             }
             if ((s[1] || '')
            .length < prec) {
                 s[1] = s[1] || '';
                 s[1] += new Array(prec - s[1].length + 1)
              .join('0');
             }
             return s.join(dec);
         }

//         function getAge() {
//             var dateString = $('#tanggalLahir').val()
//             var Instmonth = parseInt($('#ucTenor_txtNumber').val());
//             var now = new Date();
//             now.setMonth(now.getMonth() + Instmonth);
//             var today = new Date(now.getYear(), now.getMonth(), now.getDate());

//             var yearNow = now.getYear();
//             var monthNow = now.getMonth();
//             var dateNow = now.getDate();


//             var dob = new Date(dateString.substring(0, 4),
//                     dateString.substring(7, 8) - 1,
//                     dateString.substring(5, 6)
//                     );

//             var yearDob = dob.getYear();
//             var monthDob = dob.getMonth();
//             var dateDob = dob.getDate();
//             var age = {};
//             var ageString = "";
//             var yearString = "";
//             var monthString = "";
//             var dayString = "";


//             yearAge = yearNow - yearDob;

//             if (monthNow >= monthDob)
//                 var monthAge = monthNow - monthDob;
//             else {
//                 yearAge--;
//                 var monthAge = 12 + monthNow - monthDob;
//             }

//             if (dateNow >= dateDob)
//                 var dateAge = dateNow - dateDob;
//             else {
//                 monthAge--;
//                 var dateAge = 31 + dateNow - dateDob;

//                 if (monthAge < 0) {
//                     monthAge = 11;
//                     yearAge--;
//                 }
//             }

//             age = {
//                 years: yearAge,
//                 months: monthAge,
//                 days: dateAge
//             };

//             if (age.years > 1) yearString = " tahun";
//             else yearString = " tahun";
//             if (age.months > 1) monthString = " bulan";
//             else monthString = " bulan";
//             if (age.days > 1) dayString = " hari";
//             else dayString = " hari";


//             if ((age.years > 0) && (age.months > 0) && (age.days > 0))
//                 ageString = age.years + yearString + ", " + age.months + monthString + ", dan " + age.days + dayString + " ";
//             else if ((age.years == 0) && (age.months == 0) && (age.days > 0))
//                 ageString = "Only " + age.days + dayString + " ";
//             else if ((age.years > 0) && (age.months == 0) && (age.days == 0))
//                 ageString = age.years + yearString + " ";
//             else if ((age.years > 0) && (age.months > 0) && (age.days == 0))
//                 ageString = age.years + yearString + " dan " + age.months + monthString + " ";
//             else if ((age.years == 0) && (age.months > 0) && (age.days > 0))
//                 ageString = age.months + monthString + " dan " + age.days + dayString + " ";
//             else if ((age.years > 0) && (age.months == 0) && (age.days > 0))
//                 ageString = age.years + yearString + " dan " + age.days + dayString + " ";
//             else if ((age.years == 0) && (age.months > 0) && (age.days == 0))
//                 ageString = age.months + monthString + " ";
//             else ageString = "Oops! Tidak dapat menghitung usia akhir kontrak!";

//             $('#lblusiaakhirkontrak').html(ageString);
         //         }

         function getAge(e) {
             //var dateString = $('#tanggalLahir').val()
             var dateStringfull = $('#txtTanggalLahir').val();
             var dateStringyear = dateStringfull.substring(6, 10);
             var dateStringmth = dateStringfull.substring(3, 5);
             var dateStringday = dateStringfull.substring(0, 2);
             var dateString = dateStringyear + dateStringmth + dateStringday;
             //            var Instmonth = parseInt($('#ucTenor_txtNumber').val());
             //var Instmonth = parseInt($('#ucTenor_txtNumber').val()); 
             var now = new Date();
             //now.setMonth(now.getMonth() + Instmonth);
             now.setMonth(now.getMonth() + parseInt(e));
             var today = new Date(now.getYear(), now.getMonth(), now.getDate());

             var yearNow = now.getYear();
             var monthNow = now.getMonth();
             var dateNow = now.getDate();             

             var dob = new Date(dateString.substring(0, 4),
                     dateString.substring(7, 8) - 1,
                     dateString.substring(5, 6)
                     );

             var yearDob = dob.getYear();
             var monthDob = dob.getMonth();
             var dateDob = dob.getDate();
             var age = {};
             var ageString = "";
             var yearString = "";
             var monthString = "";
             var dayString = "";

             yearAge = yearNow - yearDob;

             if (monthNow >= monthDob)
                 var monthAge = monthNow - monthDob;
             else {
                 yearAge--;
                 var monthAge = 12 + monthNow - monthDob;
             }

             if (dateNow >= dateDob)
                 var dateAge = dateNow - dateDob;
             else {
                 monthAge--;
                 var dateAge = 31 + dateNow - dateDob;

                 if (monthAge < 0) {
                     monthAge = 11;
                     yearAge--;
                 }
             }

             age = {
                 years: yearAge,
                 months: monthAge,
                 days: dateAge
             };

             if (age.years > 1) yearString = " tahun";
             else yearString = " tahun";
             if (age.months > 1) monthString = " bulan";
             else monthString = " bulan";
             if (age.days > 1) dayString = " hari";
             else dayString = " hari";


             if ((age.years > 0) && (age.months > 0) && (age.days > 0))
                 ageString = age.years + yearString + ", " + age.months + monthString + ", dan " + age.days + dayString + " ";
             else if ((age.years == 0) && (age.months == 0) && (age.days > 0))
                 ageString = "Only " + age.days + dayString + " ";
             else if ((age.years > 0) && (age.months == 0) && (age.days == 0))
                 ageString = age.years + yearString + " ";
             else if ((age.years > 0) && (age.months > 0) && (age.days == 0))
                 ageString = age.years + yearString + " dan " + age.months + monthString + " ";
             else if ((age.years == 0) && (age.months > 0) && (age.days > 0))
                 ageString = age.months + monthString + " dan " + age.days + dayString + " ";
             else if ((age.years > 0) && (age.months == 0) && (age.days > 0))
                 ageString = age.years + yearString + " dan " + age.days + dayString + " ";
             else if ((age.years == 0) && (age.months > 0) && (age.days == 0))
                 ageString = age.months + monthString + " ";
             else ageString = "Oops! Tidak dapat menghitung usia akhir kontrak!";

             $('#lblusiaakhirkontrak').html(ageString);
			 //modify ario
             $('#hdnusia').val(ageString);
                  }

         //function getAmount(e) { 
         //    var OTR = $('#txtPokokHutang').val();
         //    var ExpectedMargin = $('#txtExpectedMargin').val();
         //    var Tenor = $('#txtTenor').val();
         //    var TotalBunga = $('#lblTotalBungaDisplay').val(); 

         //    var AmountMargin = parseInt(OTR.replace(/\s*,\s*/g, '')) * parseInt(ExpectedMargin.replace(/\s*,\s*/g, '')) / 100
         //    var AngsuranBulanan = parseInt(OTR.replace(/\s*,\s*/g, '')) * parseInt(ExpectedMargin.replace(/\s*,\s*/g, '')) / parseInt(Tenor.replace(/\s*,\s*/g, ''))

         //    $('#lblAmountMargin').html(number_format(AmountMargin)); 
         //    $('#txtAngsuranBulanan').val(number_format(TotalBunga));
         //    getAge(this.value);
         //}
			//end modify

		$(function() {
			$( "#datepicker" ).datepicker();
		});

		window.onload = function () { 
			$('#datepicker').on('change', function() {
				var dob = new Date(this.value);
		        var today = new Date();
		        var age = Math.floor((today-dob) / (365.25 * 24 * 60 * 60 * 1000));
				$('#umur').val(age);
			});
			var tenor = $('#txtTenor').val();
			if (tenor > 0) {
			    getAge(tenor);
			}
		}
		function DisableMyText() {
		    $('#lblusiaakhirkontrak').prop("disabled", true);
		}
		function EnableTrueMyText() {
		    $('#lblusiaakhirkontrak').prop("disabled", false);
		} 
    </script>
</head>
<body>
    <asp:UpdateProgress ID="upg1" runat="server" AssociatedUpdatePanelID="upnl1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <script type = "text/javascript">
        function preventMultipleSubmissions() {
            $('#<%=btnSave.ClientID %>').prop('disabled', true).toggleClass("gray").toggleClass("prs_img_t");
        }
        window.onbeforeunload = preventMultipleSubmissions;
    </script>
    <script type = "text/javascript">
        function preventMultipleSubmissions() {
            $('#<%=btnProceed.ClientID %>').prop('disabled', true).toggleClass("gray").toggleClass("prs_img_t");
        }
        window.onbeforeunload = preventMultipleSubmissions;
    </script>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <div runat="server" id="jlookupContent" /> 
    <%--modify ario--%>
    <asp:HiddenField ID="hdnusia" runat="server"></asp:HiddenField> 
	<%--end modify--%>
    <asp:UpdatePanel runat="server" ID="upnl1" UpdateMode="Conditional" ChildrenAsTriggers="false">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
                onclick="hideMessage();"></asp:Label>            
            
            <panel id="pnlPerorangan">
            <div class="form_title">
                <div class="form_single">
                    <h3>
                        DATA NASABAH PERORANGAN</h3>
                </div>
            </div>
            <div class="form_box">
                <div> 
                    <div class="form_single">
                        <%--update by Nofi--%>
                        <label class="label_req">Tanggal Aplikasi</label>
                        <asp:TextBox ID="txttanggalAplikasi" runat="server" CssClass="small_text" ></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txttanggalAplikasi"
                            Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                          <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ErrorMessage="Format tanggal salah"
                                        ValidationExpression="(0[1-9]|[12][0-9]|3[01])/(0[1-9]|1[012])/\d{4}" ControlToValidate="txttanggalAplikasi"
                                        SetFocusOnError="true" Display="Dynamic" CssClass="validator_general vleft"> 
                          </asp:RegularExpressionValidator>
                          <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="Harap isi tanggal"
                                        ControlToValidate="txttanggalAplikasi" CssClass="validator_general vleft" SetFocusOnError="true">
                          </asp:RequiredFieldValidator> 
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="label_req">
                           Nama
                        </label>
                        <asp:TextBox ID="txtNama" runat="server" CssClass="medium_text"  MaxLength="50"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" display="Dynamic"
                            ControlToValidate="txtNama" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                         <label class="label_req">
                            Tempat / Tanggal Lahir</label>
                        <asp:TextBox ID="txtTempatLahir" runat="server" MaxLength="20" CssClass="medium_text"></asp:TextBox> / 
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" display="Dynamic"
                            ControlToValidate="txtTempatLahir" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
                        <asp:TextBox ID="txtTanggalLahir" runat="server" CssClass="small_text" AutoPostBack="true"></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtTanggalLahir"
                            Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator3" runat="server" display="Dynamic"
                            ControlToValidate="txtTanggalLahir" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
                         <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="Format tanggal salah"
                                        ValidationExpression="(0[1-9]|[12][0-9]|3[01])/(0[1-9]|1[012])/\d{4}" ControlToValidate="txtTanggalLahir"
                                        SetFocusOnError="true" Display="Dynamic" CssClass="validator_general vleft"> 
                          </asp:RegularExpressionValidator> 
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator9" runat="server" display="Dynamic"
                            ControlToValidate="txtTanggalLahir" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="">
                           Jenis Dokumen
                        </label>
                        <asp:DropDownList ID="cboIDTypeP" runat="server" CssClass="select">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Harap pilih Jenis Identitas"
                            ControlToValidate="cboIDTypeP" Display="Dynamic" InitialValue="Select One" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="label_req">
                           Nomor Dokumen 
                        </label>
                        <asp:TextBox ID="txtIDNumberP" runat="server" MaxLength="16" CssClass="medium_text"
                            onkeypress="return numbersonly2(event)"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegEx1" ControlToValidate="txtIDNumberP" runat="server"
                            ErrorMessage="Masukkan angka saja" ValidationExpression="\d+" CssClass="validator_general"></asp:RegularExpressionValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Harap isi No Identitas"
                            ControlToValidate="txtIDNumberP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="label_general">
                           Karyawan Internal Perusahaan
                        </label>
                        <asp:RadioButtonList ID="rboIsRelatedBNI" runat="server" RepeatDirection="Horizontal"
                            CssClass="opt_single">
                            <asp:ListItem Value="1">Ya</asp:ListItem>
                            <asp:ListItem Selected="True" Value="0">Tidak</asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                </div>
            </div>
         
            <div class="form_box_hide"> 
                <div class="form_left">
                        <div style="float:left">
                            <label class="label_req">
                                Supplier/Dealer</label>                            
                            <asp:HiddenField runat="server" ID="hdfProspectAppID" />      
                            <div style="display:none">
                            <asp:TextBox runat="server" ID="txtSupplierCode" Enabled="false" CssClass="small_text"></asp:TextBox>              
                            </div>
                            <asp:TextBox runat="server" ID="txtSupplierName" Enabled="false" CssClass="medium_text" AutoPostBack="true"></asp:TextBox>
                        </div>
                        <div style="margin-top:2px; float:left">
                            <asp:Panel ID="pnlLookupSupplier" runat="server">
                            <button class="small buttongo blue" 
                            onclick ="OpenJLookup('<%= ResolveClientUrl("~/webform.Utility/jLookup/SupplierProspect.aspx?kode=" & txtSupplierCode.ClientID & "&nama=" & txtSupplierName.ClientID &"&ProspectAppID="& hdfProspectAppID.Value) %>','Daftar Supplier','<%= jlookupContent.ClientID %>');return false;">...</button>                                                                            
                            <%--<asp:RequiredFieldValidator ID="rfvtxtSupplier" runat="server" ErrorMessage="*" Display="Dynamic"
                                CssClass="validator_general" ControlToValidate="txtSupplierCode"></asp:RequiredFieldValidator>--%>
                            </asp:Panel>
                        </div>
                    <%--Nofi 06Feb2018--%>
                 </div>
             <%--    <div class="form_right">
                    <div style="margin-top:2px; float:left">
                            <asp:Label runat="server" ID="NamaDealer" ></asp:Label> 
                        </div>
                   
                        <div style="display: inline-block; padding-left:20px; margin-top:4px;">
                            <label class="label">
                                Nama Dealer :</label>  
                            <asp:Label runat="server" ID="lblSupplierStatus" ></asp:Label>
                        </div>
                </div>--%>
            </div>
            <div class="form_box_hide">
                 <div> 
                <div class="form_left">
                        <div style="float:left">
                        <label class="label_req">
                            Jenis Asset</label>                     
                        <asp:HiddenField runat="server" ID="hdfAssettypeCode" />                                        
                        <asp:HiddenField runat="server" ID="hdfAssetTypeID2" />     
                        
                            <asp:TextBox runat="server" ID="txtAssetType" Enabled="false" CssClass="medium_text"></asp:TextBox>
                        </div>
                        <div style="margin-top:2px">
                            <asp:Panel ID="pnlLookupAssetType" runat="server">
                            <button class="small buttongo blue" 
                            onclick ="OpenJLookup('<%= ResolveClientUrl("~/webform.Utility/jLookup/AssetType.aspx?kode=" & hdfAssettypeCode.ClientID & "&nama=" & txtAssetType.ClientID & "&AssetTypeID=" & hdfAssetTypeID2.Value) %>','Daftar Supplier','<%= jlookupContent.ClientID %>');return false;">...</button>                                                                            
                           <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="*" Display="Dynamic"
                                CssClass="validator_general" ControlToValidate="txtAssetType"></asp:RequiredFieldValidator>--%>
                            </asp:Panel>
                        </div> 
                    <%--Anjar 22122017--%>
                 </div>
                 <%--<div class="form_right">
                    <div style="margin-top:2px; float:left">
                            <asp:Label runat="server" ID="JenisAsset" ></asp:Label> 
                        </div> 
                        <div style="display: inline-block; padding-left:20px; margin-top:4px;">
                            <label class="label">
                                Jenis Asset :</label>  
                            <asp:Label runat="server" ID="lblJenisAsset" ></asp:Label>
                        </div>
                 </div>--%>
            </div>
            </div>            
            <div class="form_box_hide">
                 <div> 
                <div class="form_left">
                        <div style="float:left">
                        <label class="label_req">
                            Merk Asset</label>                     
                        <asp:HiddenField runat="server" ID="hdfAssetBrandCode" />                                        
                        <asp:HiddenField runat="server" ID="hdfAssetTypeID" />     
                        
                            <asp:TextBox runat="server" ID="txtAssetBrand" Enabled="false" CssClass="medium_text"></asp:TextBox>
                        </div>
                        <div style="margin-top:2px">
                            <asp:Panel ID="pnlLookupAssetBrand" runat="server">
                            <button class="small buttongo blue" 
                            onclick ="OpenJLookup('<%= ResolveClientUrl("~/webform.Utility/jLookup/AssetBrand.aspx?kode=" & hdfAssetBrandCode.ClientID & "&nama=" & txtAssetBrand.ClientID &"&AssetTypeID="& hdfAssetTypeID.Value) %>','Daftar Supplier','<%= jlookupContent.ClientID %>');return false;">...</button>                                                                            
                          <%--   <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="*" Display="Dynamic"
                                CssClass="validator_general" ControlToValidate="txtAssetBrand"></asp:RequiredFieldValidator> --%>
                            </asp:Panel>
                        </div> 
                    <%--Anjar 22122017--%>
                    </div>
                <%--    <div class="form_right">
                    <div style="margin-top:2px; float:left">
                            <asp:Label runat="server" ID="MerkAsset" ></asp:Label> 
                        </div> 
                        <div style="display: inline-block; padding-left:20px; margin-top:4px;">
                            <label class="label">
                                Merk Asset :</label>  
                            <asp:Label runat="server" ID="lblMerkAsset" ></asp:Label>
                        </div>
                 </div>--%>
            </div>
            </div>

            </panel>
            <div class="form_box_title">
                <div class="form_single">
                    <h4>
                       PEMBIAYAAN</h4>
                </div>
            </div>

            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label class="label_req">Tujuan Pembiayaan</label>  
                        <asp:DropDownList ID="cboKegiatanUsaha" runat="server" AutoPostBack="true"  Width="300px"/>
                         <asp:RequiredFieldValidator ID="Requiredfieldvalidator65" runat="server" 
                            display="Dynamic"
                            ControlToValidate="cboKegiatanUsaha" CssClass="validator_general" ErrorMessage="*" InitialValue="SelectOne"></asp:RequiredFieldValidator>
                    </div>
                    <div class="form_right">
                       <label class="label_req">Jenis Aplikasi</label>
                        <asp:DropDownList ID="cboJenisAplikasi" runat="server"></asp:DropDownList>
                        <asp:RequiredFieldValidator ID="rfvJenisAplikasi" runat="server" Display="Dynamic" ControlToValidate="cboJenisAplikasi" CssClass="validator_general"
                            ErrorMessage="*" InitialValue="SelectOne"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label class="label_req">
                            Akad/Skema Pembiayaan
                        </label>
                        <asp:DropDownList runat="server" ID="cboAkad"  Width="300px" AutoPostBack="true">
                            <%--<asp:listitem value="Murabahah">Murabahah</asp:listitem>--%>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator13" runat="server" 
                            display="Dynamic"
                            ControlToValidate="cboAkad" CssClass="validator_general" ErrorMessage="*" InitialValue="Select One"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
           
            <div class="form_box">
            <uc1:ucProdOffering id="ucLookupProductOffering1" runat="server"></uc1:ucProdOffering>
            </div>

            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="label_req">
                           Harga Asset 
                        </label>
                        <uc1:ucnumberformat runat="server" id="ucOTR" TextCssClass="numberAlign reguler_text" onclientchange="recalculateAngs()" ></uc1:ucnumberformat>  
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="">
                           URBUN Tunai
                        </label>
                        <uc1:ucnumberformat runat="server" id="ucUangMuka" TextCssClass="numberAlign reguler_text" onclientchange="recalculateAngs()" ></uc1:ucnumberformat>
                    </div>
                </div>
            </div>
            <%--<div class="form_box">--%>
            <div class="form_box_hide">
                <div>
                    <div class="form_single">
                        <label class="">
                           URBUN Trade In
                        </label>
                        <uc1:ucnumberformat runat="server" id="ucUangMukaTradeIn" TextCssClass="numberAlign reguler_text"  onclientchange="recalculateAngs()" ></uc1:ucnumberformat>
                    </div>
                </div>
            </div>
            <%--<div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="">
                           Total URBUN
                        </label> --%>
                        <%--<asp:Label runat="server" ID="lblTotalUangMuka"  CssClass="numberAlign regular_text"></asp:Label>--%>
                        <%--<asp:TextBox ID="txtTotalUangMuka" runat="server" Width="11%" CssClass="numberAlign regular_text" Enabled="false"></asp:TextBox>
                    </div>
                </div>
            </div>--%>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="">
                           Pokok Pembiayaan
                        </label>
                       <%-- <asp:Label runat="server" ID="lblPokokHutang"  CssClass="numberAlign regular_text"></asp:Label>--%>
                        <asp:TextBox ID="txtPokokHutang" runat="server" Width="11%" CssClass="numberAlign regular_text" Enabled="false"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="label_req">
                           Jangka Waktu
                        </label>
                        <asp:HiddenField runat="server" ID="tanggalLahir" /> 
                         <asp:TextBox ID="txtTenor" runat="server" CssClass="numberAlign" onChange="getAge(this.value);" Width="3%" onkeypress="return numbersonly2(event)" Text="0" onfocus="this.value=resetNumber(this.value);"></asp:TextBox>
                         <asp:RangeValidator ID="Rangevalidator2" runat="server" MaximumValue="180" MinimumValue="0"
                            Type="Integer" ControlToValidate="txtTenor" ErrorMessage="Input angka 0 sampai 180"
                            Display="Dynamic" CssClass="validator_general"></asp:RangeValidator> 
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtTenor" InitialValue="0"
                        ErrorMessage="Input angka 0 sampai 180" Display="Dynamic" CssClass ="validator_general"></asp:RequiredFieldValidator>   
                        <%-- <asp:FilteredTextBoxExtender ID="TextBox1_FilteredTextBoxExtender" runat="server"
                            Enabled="True" TargetControlID="txtTenor" FilterType="Numbers"> </asp:FilteredTextBoxExtender>--%>
                        <%--<uc1:ucnumberformat runat="server" id="ucTenor" TextCssClass="numberAlign reguler_text" onclientchange="getAge()" ></uc1:ucnumberformat>--%>
                    </div>
                </div>
            </div>
            <%--<div class="form_box" >
                <div>
                    <div class="form_single" visible="false">   
                        <label class="label_req">
                           Expected Margin
                        </label> 

                         <asp:TextBox ID="txtExpectedMargin" runat="server" CssClass="numberAlign" onChange="getAmount(this.value);" Width="3%" onkeypress="return numbersonly2(event)" Text="0" onfocus="this.value=resetNumber(this.value);"></asp:TextBox>                         </asp:TextBox> %
                        <asp:Label ID="lblAmountMargin" runat="server"></asp:Label>
                          <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="txtAngsuranBulanan" InitialValue="0"
                        ErrorMessage="Input angka > 0" Display="Dynamic" CssClass ="validator_general"></asp:RequiredFieldValidator>      
                    </div>
                </div>
            </div>--%>
            
            <div class="form_box">
                <div>
                    <div class="form_left">
                            <label>Total Margin</label>
                                <asp:TextBox ID="lblTotalBungaDisplay" runat="server" CssClass="numberAlign2 bigger_text" Enabled ="false"></asp:TextBox>
                                <asp:HiddenField ID="lblTotalBunga" runat="server"></asp:HiddenField>
                                <label class="label_auto numberAlign2"> % </label>
                                <asp:TextBox ID="txtEffectiveRate" runat="server" MaxLength="15" CssClass="numberAlign2 smaller_text" Enabled ="false"></asp:TextBox>
                                <asp:requiredfieldvalidator id="RequiredFieldValidator10" runat="server" ErrorMessage="Please fill Effective Rate!"
			        								ControlToValidate="txtEffectiveRate" Display="Dynamic" CssClass="validator_general"></asp:requiredfieldvalidator><asp:rangevalidator id="Rangevalidator3" runat="server" ControlToValidate="txtEffectiveRate" Display="Dynamic" CssClass="validator_general"
			        								MaximumValue="100" MinimumValue="0" Type="Double"></asp:rangevalidator>
                                <label class="label_auto numberAlign2"> Efektif </label>
                                <label class="label_auto numberAlign2"> % </label>
                                <asp:TextBox runat="server" ID="txtFlatRate" CssClass="numberAlign2 smaller_text" autopostback="true"></asp:TextBox>
                                <label class="label_auto numberAlign2"> Flat </label>    
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" Display="Dynamic"
                                ControlToValidate="txtEffectiveRate" ErrorMessage="Harap isi Margin Effective"
                                CssClass="validator_general"></asp:RequiredFieldValidator>
                            <asp:RangeValidator ID="RVPOEffectiveRate" runat="server" Display="Dynamic" ControlToValidate="txtEffectiveRate"
                                Type="Double" MinimumValue="0" MaximumValue="100" CssClass="validator_general"></asp:RangeValidator>
                            <asp:RangeValidator ID="RVPBEffectiveRate" runat="server" Display="Dynamic" ControlToValidate="txtEffectiveRate"
                                Type="Double" MinimumValue="0" MaximumValue="100" CssClass="validator_general"></asp:RangeValidator>
                            <asp:RangeValidator ID="RVPEffectiveRate" runat="server" Display="Dynamic" ControlToValidate="txtEffectiveRate"
                                Type="Double" MinimumValue="0" MaximumValue="100" CssClass="validator_general"></asp:RangeValidator>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" Display="Dynamic"
                                ControlToValidate="txtFlatRate" ErrorMessage="Harap isi Margin Flat!" CssClass="validator_general"></asp:RequiredFieldValidator>
                            <asp:RangeValidator ID="RangeValidator1" runat="server" Display="Dynamic" ControlToValidate="txtFlatRate"
                                Type="Double" MinimumValue="0" MaximumValue="100" CssClass="validator_general"
                                ErrorMessage="Margin flat tidak valid!"></asp:RangeValidator>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="label_req">
                           Kemampuan Mengangsur
                        </label>
                        <%--<uc1:ucnumberformat runat="server" id="ucAngsuranBulanan" TextCssClass="numberAlign reguler_text" ></uc1:ucnumberformat>--%>
                         <asp:TextBox ID="txtAngsuranBulanan" runat="server" CssClass="numberAlign" onkeypress="return numbersonly2(event)" Text="0"
                              onkeyup="extractNumber(this,7,true);" onfocus="this.value=resetNumber(this.value);"
                             onblur="extractNumber(this,7,true);blockInvalid(this);this.value=numberFormat(this.value);this.value=blankToZero(this.value);" OnChange="javascript:void(0)" autocomplete="off">
                         </asp:TextBox> 
                          <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtAngsuranBulanan" InitialValue="0"
                        ErrorMessage="Input angka > 0" Display="Dynamic" CssClass ="validator_general"></asp:RequiredFieldValidator>      
                    </div>
                </div>
            </div>
            
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="">
                           Usia Pemohon di Akhir Kontrak
                        </label>
                        <asp:Label runat="server" ID="lblusiaakhirkontrak"  CssClass="regular_text"></asp:Label>
                    </div>
                </div>
            </div>
            <asp:Panel ID="PanelInfo" runat="server">
                <div class="form_box_title">
                    <div class="form_single">
                        <h4>
                            Similar Customer Data</h4>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ws">
                            <asp:DataGrid id="dtgList1" runat="server" Width="100%" AutoGenerateColumns="False" BorderWidth="0"
									cellpadding="3" cellspacing="1" DataKeyField="Name" CssClass="grid_general">
									<HeaderStyle CssClass="th"></HeaderStyle>
                                    <ItemStyle CssClass="item_grid" />
									<Columns>
										<asp:TemplateColumn HeaderText="NO">
											<ItemStyle HorizontalAlign="CENTER"></ItemStyle>
											<ItemTemplate>
												<asp:Label ID="lblNo1" Runat="server"></asp:Label>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="NAME">
											<ItemStyle HorizontalAlign="left"></ItemStyle>
											<ItemTemplate>
												<asp:linkbutton id="lnkCustName" runat="server" CausesValidation="false" text='<%# DataBinder.eval(Container,"DataItem.Name") %>'>
												</asp:linkbutton>
												<asp:Label ID="lblCust" Runat="server" Visible="False" text='<%# DataBinder.eval(Container,"DataItem.CustomerID") %>'>
												</asp:Label>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:BoundColumn DataField="IDType" HeaderText="ID TYPE"></asp:BoundColumn>
										<asp:BoundColumn DataField="IDNumber" HeaderText="ID NUMBER"></asp:BoundColumn>
										<asp:BoundColumn DataField="BirthDate" ItemStyle-HorizontalAlign="Center" HeaderText="BIRTH DATE"
											DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
										<asp:BoundColumn DataField="Type" ItemStyle-HorizontalAlign="left" HeaderText="TYPE"></asp:BoundColumn>
									</Columns>
								</asp:DataGrid>
                        </div>
                    </div>
                </div>
                <div class="form_box_title">
                    <div class="form_single">
                        <h4>
                            Negative List</h4>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ws">
                            <asp:DataGrid id="dtgList2" runat="server" Width="100%" AutoGenerateColumns="False" BorderWidth="0"
									cellpadding="3" cellspacing="1" DataKeyField="Name" CssClass="grid_general">
									<HeaderStyle CssClass="th"></HeaderStyle>
                                    <ItemStyle CssClass="item_grid" />
									<Columns>
										<asp:TemplateColumn HeaderText="NO">
											<ItemStyle HorizontalAlign="CENTER"></ItemStyle>
											<ItemTemplate>
												<asp:Label ID="lblNo2" Runat="server"></asp:Label>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="NAME">
											<ItemStyle HorizontalAlign="left"></ItemStyle>
											<ItemTemplate>
												<asp:linkbutton id="lnkCustName2" runat="server" CausesValidation="false" text='<%# DataBinder.eval(Container,"DataItem.Name") %>'>
												</asp:linkbutton>
												<asp:Label ID="lblCust2" Runat="server" Visible="False" text='<%# DataBinder.eval(Container,"DataItem.CustomerID") %>'>
												</asp:Label>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:BoundColumn DataField="IDType" HeaderText="ID TYPE"></asp:BoundColumn>
										<asp:BoundColumn DataField="IDNumber" HeaderText="ID NUMBER"></asp:BoundColumn>
										<asp:BoundColumn ItemStyle-HorizontalAlign="Center" DataField="BirthDate" HeaderText="BIRTH DATE"
											DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
										<asp:BoundColumn DataField="Type" HeaderText="TYPE"></asp:BoundColumn>
									</Columns>
								</asp:DataGrid>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <div class="form_button">
                <asp:Button ID="btnNext" runat="server" Text="Next" CssClass="small button green"
                    CausesValidation="True" ></asp:Button>
                <asp:Button ID="btnProceed" runat="server" Text="Proceed" CssClass="small button green" 
                    CausesValidation="True" Visible="false"></asp:Button>
                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue"
                    CausesValidation="True"></asp:Button>
                <asp:Button ID="btnCancel" runat="server" Text="Back" CssClass="small button gray"
                    CausesValidation="False"></asp:Button>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnNext" EventName="Click" />    
           <%-- <asp:PostBackTrigger   ControlID="btnNext" /> --%>        
            <asp:AsyncPostBackTrigger ControlID="btnProceed" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />   
            <asp:AsyncPostBackTrigger ControlID="txtTanggalLahir" EventName="TextChanged" />
            <asp:AsyncPostBackTrigger ControlID="txtFlatRate" EventName="TextChanged" /> 
            <asp:AsyncPostBackTrigger ControlID="cboKegiatanUsaha" EventName="SelectedIndexChanged" />
            <%--<asp:AsyncPostBackTrigger ControlID="cboJenisPembiyaan" EventName="SelectedIndexChanged" />--%>
            <asp:AsyncPostBackTrigger ControlID="cboAkad" EventName="SelectedIndexChanged" />  
        </Triggers>
    </asp:UpdatePanel>
    </form>
</body>
</html>
