﻿#Region "Imports"
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
Imports System.IO
#End Region

Public Class BIChecking
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private oController As New ProspectController
    Private oCustomclass As New Parameter.Prospect
    Protected WithEvents fileUpload As UcFileDocumentUploader

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private time As String

    Dim pathFolder As String = ""
#End Region

#Region "Property"
    Private Property Err() As String
        Get
            Return CType(ViewState("Err"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Err") = Value
        End Set
    End Property
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("prosCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("prosCmdWhere") = Value
        End Set
    End Property

    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property

    Private Property ProspectAppID() As String
        Get
            Return CType(ViewState("ProspectAppID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ProspectAppID") = Value
        End Set
    End Property

    Property ActivityDateStart() As DateTime
        Get
            Return ViewState("ActivityDateStart").ToString
        End Get
        Set(ByVal Value As DateTime)
            ViewState("ActivityDateStart") = Value
        End Set
    End Property

    Property ActivityDateEnd() As DateTime
        Get
            Return ViewState("ActivityDateEnd").ToString
        End Get
        Set(ByVal Value As DateTime)
            ViewState("ActivityDateEnd") = Value
        End Set
    End Property

    Protected Property LinkFotoKtpCust() As String
        Get
            Return CType(ViewState("LinkFotoKtpCust"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("LinkFotoKtpCust") = Value
        End Set
    End Property

    Protected Property LinkFotoKtpPasangan() As String
        Get
            Return CType(ViewState("LinkFotoKtpPasangan"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("LinkFotoKtpPasangan") = Value
        End Set
    End Property

    Private Property CustomerID() As String
        Get
            Return (CType(ViewState("CustomerID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property

#End Region

    Private Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If

        lblMessage.Text = ""
        lblMessage.Visible = False
        PnlViewDetail.Visible = False
        PnlDecision.Visible = False
        pnlList.Visible = True
        pnlSearch.Visible = True


        If Not Page.IsPostBack Then
            'time = Now.Hour.ToString("00") & ":" & Now.Minute.ToString("00") & ":" & Now.Second.ToString("00")
            'Me.ActivityDateStart = Me.BusinessDate + " " + time

            If Request.QueryString("Err") <> "" Then
                Me.Err = Request.QueryString("Err")
                ShowMessage(lblMessage, Me.Err, True)
            End If

            txtGoPage.Text = "1"

            cboDecision.SelectedIndex = 0
            txtNotes.Text = ""

            Me.FormID = "Prospect"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            If Request("cond") <> "" Then
                Me.CmdWhere = Request("cond")
            Else
                CmdWhere = String.Format("Prospect.IsProceeded=1 and Prospect.BranchID ='{0}' and Prospect.ProspectStep='INT' ", sesBranchId.Replace("'", "").Trim)
            End If

            Me.Sort = "ProspectAppId ASC"
            BindGrid(Me.CmdWhere)
        End If
    End Sub

    Sub BindGrid(ByVal cmdWhere As String)
        Dim dtEntity As DataTable
        Dim oCustomClass As New Parameter.Prospect
        pnlList.Visible = True
        oCustomClass.PageSize = pageSize

        oCustomClass.WhereCond = cmdWhere
        oCustomClass.SortBy = Me.Sort
        oCustomClass.CurrentPage = currentPage
        oCustomClass.strConnection = GetConnectionString()
        oCustomClass = oController.GetInitial(oCustomClass)
        If Not oCustomClass Is Nothing Then
            dtEntity = oCustomClass.listdata
            recordCount = oCustomClass.TotalRecords
        Else
            recordCount = 0
        End If
        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()
        PagingFooter()
    End Sub

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan!", True)

            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblTotRec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGrid(Me.CmdWhere)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGoPage.Click
        If IsNumeric(txtGoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtGoPage.Text, Int32)
                BindGrid(Me.CmdWhere)
            End If
        End If
    End Sub


    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGrid(Me.CmdWhere)
    End Sub


    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        cboSearch.SelectedIndex = 0
        txtSearch.Text = ""
        'modify nofi 20Feb2018
        'Me.CmdWhere = "ALL"
        Me.CmdWhere = String.Format("Prospect.IsProceeded=1 and Prospect.BranchID ='{0}' and Prospect.ProspectStep='INT' ", sesBranchId.Replace("'", "").Trim)
        BindGrid(Me.CmdWhere)
    End Sub

    Private Sub imbSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        CmdWhere = String.Format("Prospect.IsProceeded=1 and Prospect.BranchID ='{0}' and Prospect.ProspectStep='INT' ", sesBranchId.Replace("'", "").Trim)
        If txtSearch.Text.Trim <> "" Then
            CmdWhere = String.Format("isProceeded=1 and {0} LIKE '%{1}%' and  Prospect.BranchID ='{2}' and Prospect.ProspectStep='INT' ", cboSearch.SelectedItem.Value, txtSearch.Text.Replace("%", ""), sesBranchId.Replace("'", "").Trim)
        End If
        BindGrid(Me.CmdWhere)
    End Sub

    Private Sub dtgEntity_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand

        If e.Item.ItemIndex >= 0 Then
            time = Now.Hour.ToString("00") & ":" & Now.Minute.ToString("00") & ":" & Now.Second.ToString("00")
            Me.ActivityDateStart = Me.BusinessDate + " " + time

            Me.ProspectAppID = CType(e.Item.FindControl("lblProspectAppID"), HyperLink).Text.Trim

            If e.CommandName = "Approve" Then
                If CheckFeature(Me.Loginid, Me.FormID, "Edit", Me.AppId) Then
                    If SessionInvalid() Then
                        Exit Sub
                    End If
                End If

                PnlViewDetail.Visible = True
                btnCLose.Visible = False
                PnlDecision.Visible = True
                pnlList.Visible = False
                pnlSearch.Visible = False

                Dim oCustomclass As New Parameter.Prospect
                With oCustomclass
                    .strConnection = GetConnectionString()
                    .ProspectAppID = Me.ProspectAppID
                End With
                BindEdit()
            End If

            'If e.CommandName = "Return" Then
            '    If CheckFeature(Me.Loginid, Me.FormID, "Edit", Me.AppId) Then
            '        If SessionInvalid() Then
            '            Exit Sub
            '        End If
            '    End If

            '    PnlViewDetail.Visible = True
            '    PnlDecision.Visible = True
            '    pnlList.Visible = False
            '    pnlSearch.Visible = False

            '    Dim oCustomclass As New Parameter.Prospect
            '    With oCustomclass
            '        .strConnection = GetConnectionString()
            '        .ProspectAppID = Me.ProspectAppID
            '    End With
            '    BindEdit()

            '    ProsesReturn()
            'End If

            'If e.CommandName = "Reject" Then
            '    If CheckFeature(Me.Loginid, Me.FormID, "Edit", Me.AppId) Then
            '        If SessionInvalid() Then
            '            Exit Sub
            '        End If
            '    End If

            '    PnlViewDetail.Visible = True
            '    PnlDecision.Visible = True
            '    pnlList.Visible = False
            '    pnlSearch.Visible = False

            '    Dim oCustomclass As New Parameter.Prospect
            '    With oCustomclass
            '        .strConnection = GetConnectionString()
            '        .ProspectAppID = Me.ProspectAppID
            '    End With
            '    BindEdit()

            '    ProsesReject()
            'End If

            If e.CommandName = "Detail" Then
                If CheckFeature(Me.Loginid, Me.FormID, "Edit", Me.AppId) Then
                    If SessionInvalid() Then
                        Exit Sub
                    End If
                End If
                PnlViewDetail.Visible = True
                PnlDecision.Visible = False
                btnCLose.Visible = True
                pnlList.Visible = False
                pnlSearch.Visible = False

                Dim oCustomclass As New Parameter.Prospect
                With oCustomclass
                    .strConnection = GetConnectionString()
                    .ProspectAppID = Me.ProspectAppID
                End With
                BindEdit()
            End If
        End If
    End Sub

    Protected Sub imbClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCLose.Click
        Response.Redirect("BIChecking.aspx")
    End Sub

#Region "BindEdit"
    Sub BindEdit()
        Dim oApplication As New Parameter.Prospect
        Dim oData As New DataTable
        Dim oRow As DataRow

        oApplication.strConnection = GetConnectionString()
        oApplication.BranchId = Replace(Me.sesBranchId, "'", "").ToString
        oApplication.ProspectAppID = Me.ProspectAppID
        oApplication = oController.GetViewProspect(oApplication)

        If Not oApplication Is Nothing Then
            oData = oApplication.listdata
        End If

        If oData.Rows.Count > 0 Then
            oRow = oData.Rows(0)
            lblProspectAppId.Text = oRow("ProspectAppId").ToString
            lblProspectAppId.Attributes.Add("onClick", "javascript:OpenWinViewProspectInquiry('" + lblProspectAppId.Text.Trim() + "');")
            lbltanggalAplikasi.Text = Format(oRow("ProspectAppDate"), "dd/MM/yyyy")
            lblNama.Text = oRow("Name").ToString
            lblTempatLahir.Text = oRow("BirthPlace").ToString
            lblTanggalLahir.Text = Format(oRow("BirthDate"), "dd/MM/yyyy")
            lblIDTypeP.Text = oRow("IDType").ToString
            lblIDNumberP.Text = oRow("IDNumber").ToString.Trim
            lblSupplierName.Text = oRow("SupplierName").ToString
            lblAssetType.Text = oRow("AssetTypeDescription").ToString
            lblAssetBrand.Text = oRow("AssetBrandDescription").ToString
            lblOTR.Text = FormatNumber(oRow("OTRPrice"), 0)
            lblUangMuka.Text = FormatNumber(oRow("DPAmount"), 0)
            lblUangMukaTradeIn.Text = FormatNumber(oRow("DPAmountTradeIn"), 0)
            lblTotalUangMuka.Text = FormatNumber(CDec(oRow("DPAmount")) + CDec(oRow("DPAmountTradeIn")), 0)
            lblPokokHutang.Text = FormatNumber(oRow("NTF"), 0)
            lblAngsuranBulanan.Text = FormatNumber(oRow("InstallmentAmount"), 0)
            lblTenor.Text = oRow("Tenor").ToString
            lblusiaakhirkontrak.Text = getAge(ConvertDate(oRow("BirthDate")), CInt(oRow("Tenor").ToString))
            If oRow("CustomerType").ToString = "C" Then
                divAkhirKontrak.Visible = False
            Else
                divAkhirKontrak.Visible = True
            End If

            Me.LinkFotoKtpCust = oRow("FotoCustomer").ToString
            SaveImageFromMobile(Me.LinkFotoKtpCust.ToString, "LampiranFotoKtpCustomer")

            Me.LinkFotoKtpPasangan = oRow("FotoPasangan").ToString
            SaveImageFromMobile(Me.LinkFotoKtpPasangan.ToString, "LampiranFotoKtpPasangan")

            lblTujuanPembiayaan.Text = IIf(String.IsNullOrEmpty(oRow("KegiatanUsaha").ToString()), "-", oRow("KegiatanUsaha").ToString())
            lblAkadSkema.Text = IIf(String.IsNullOrEmpty(oRow("AkadPembiayaan").ToString()), "-", oRow("AkadPembiayaan").ToString())
            lblProductJual.Text = IIf(String.IsNullOrEmpty(oRow("ProductJual").ToString()), "-", oRow("ProductJual").ToString())



            cekimage()
        End If
    End Sub
#End Region

    Private Function getAge(ByVal dt As String, ByVal m As Integer) As String
        Dim rtn As String = ""
        Dim dob As DateTime
        dob = New DateTime(dt.Substring(0, 4), dt.Substring(6, 2), dt.Substring(4, 2))
        Dim tday As TimeSpan = DateTime.Now.AddMonths(m).Subtract(dob)
        Dim years As Integer, months As Integer, days As Integer
        months = 12 * (DateTime.Now.AddMonths(m).Year - dob.Year) + (DateTime.Now.AddMonths(m).Month - dob.Month)

        If DateTime.Now.AddMonths(m).Day < dob.Day Then
            months -= 1
            days = DateTime.DaysInMonth(dob.Year, dob.Month) - dob.Day + DateTime.Now.AddMonths(m).Day
        Else
            days = DateTime.Now.AddMonths(m).Day - dob.Day
        End If
        years = Math.Floor(months / 12)
        months -= years * 12

        rtn = years & " tahun, " & months & " bulan and " & days & " hari"
        Return rtn
    End Function

    Sub ProsesLulus()
        Try

            Dim oApplication As New Parameter.Prospect
            oApplication.strConnection = GetConnectionString()
            oApplication.BranchId = Replace(Me.sesBranchId, "'", "").ToString
            oApplication.ProspectAppID = Me.ProspectAppID
            oApplication.BICheckingStatus = 1
            oApplication.ActivityDateStart = Me.ActivityDateStart

            time = Now.Hour.ToString("00") & ":" & Now.Minute.ToString("00") & ":" & Now.Second.ToString("00")
            Me.ActivityDateEnd = Me.BusinessDate + " " + time

            oApplication.ActivityDateEnd = Me.ActivityDateEnd
            oApplication.ActivityUser = Me.Loginid
            oApplication.ActivitySeqNo = 2
            oApplication.SIDNotes = txtNotes.Text.Trim

            oApplication.HubBankLeasing = cboHubBankLeasing.SelectedValue


            Dim ErrorMessage As String = ""
            Dim oReturn As New Parameter.Prospect

            If fileUpload.UploadToPathAsset(fileUpload.FileUploadObj.FileName, GetConnectionString(), Me.ProspectAppID, Replace(Me.sesBranchId, "'", "").ToString) Then
                oReturn = oController.DoBIChecking(oApplication)
            Else
                ShowMessage(lblMessage, "File upload must be .Pdf", True)
                Exit Sub
            End If


            If oReturn.Err <> "" Then
                ShowMessage(lblMessage, ErrorMessage, True)
            Else
                ShowMessage(lblMessage, "Data saved!", False)
                'Response.Redirect("BIChecking.aspx")
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

    Sub ProsesReject()
        Try

            Dim oApplication As New Parameter.Prospect
            oApplication.strConnection = GetConnectionString()
            oApplication.BranchId = Replace(Me.sesBranchId, "'", "").ToString
            oApplication.ProspectAppID = Me.ProspectAppID
            oApplication.BICheckingStatus = 0
            oApplication.ActivityDateStart = Me.ActivityDateStart

            time = Now.Hour.ToString("00") & ":" & Now.Minute.ToString("00") & ":" & Now.Second.ToString("00")
            Me.ActivityDateEnd = Me.BusinessDate + " " + time

            oApplication.ActivityDateEnd = Me.ActivityDateEnd
            oApplication.ActivityUser = Me.Loginid
            oApplication.ActivitySeqNo = 2
            oApplication.SIDNotes = txtNotes.Text.Trim

            oApplication.HubBankLeasing = cboHubBankLeasing.SelectedValue
            Dim ErrorMessage As String = ""
            Dim oReturn As New Parameter.Prospect

            oReturn = oController.DoBIChecking(oApplication)

            If oReturn.Err <> "" Then
                ShowMessage(lblMessage, ErrorMessage, True)
            Else
                ShowMessage(lblMessage, "Data saved!", False)
                Response.Redirect("BIChecking.aspx")
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

    Sub ProsesReturn()
        Dim oApplication As New Parameter.Prospect
        Dim msg As String
        oApplication.strConnection = GetConnectionString()
        oApplication.BusinessDate = Me.BusinessDate
        oApplication.BranchId = Me.sesBranchId.Replace("'", "").Trim
        oApplication.ProspectAppID = Me.ProspectAppID
        oApplication.Notes = txtNotes.Text.Trim
        oApplication.HubBankLeasing = cboHubBankLeasing.SelectedValue

        Try
            'msg = oController.ProspectReturnUpdate(oApplication)
            oController.ProspectReturnUpdate(oApplication)
            'If msg <> "" Then
            '    ShowMessage(lblMessage, "Data Not Return!", True)
            'Server.Transfer("BIChecking.aspx", True)
            'Else
            ShowMessage(lblMessage, "Data has been return!", False)
            '    Response.Redirect("BIChecking.aspx", True)
            'End If
            BindGrid(Me.CmdWhere)
            PnlViewDetail.Visible = False
            'PnlDecision.Visible = True
            pnlList.Visible = True
            pnlSearch.Visible = True
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try

    End Sub

    Private Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click
        If cboDecision.SelectedValue = "A" Then
            ProsesLulus()
        ElseIf cboDecision.SelectedValue = "R" Then
            ProsesReturn()
        ElseIf cboDecision.SelectedValue = "J" Then
            ProsesReject()
        End If
    End Sub


    Public Function SaveImageFromMobile(ImgStr As String, ImgName As String) As Boolean

        Dim path__1 As [String] = HttpContext.Current.Server.MapPath("~/ImageStorage")
        'Path
        'Check if directory exist
        If Not System.IO.Directory.Exists(path__1) Then
            'Create directory if it doesn't exist
            System.IO.Directory.CreateDirectory(path__1)
        End If

        Dim imageName As String = ImgName & Convert.ToString(".jpg")

        'set the image path
        Dim imgPath As String = Path.Combine(path__1, imageName)
        Dim imgPathx As String = pathFile("HasilMobileProspect", Me.ProspectAppID) + imageName
        Dim imageBytes As Byte() = Convert.FromBase64String(ImgStr)

        'File.WriteAllBytes(imgPath, imageBytes)
        File.WriteAllBytes(imgPathx, imageBytes)
        Return True
    End Function

    Private Function pathFile(ByVal Group As String, ByVal ExecutorID As String) As String
        Dim strDirectory As String = ""
        'strDirectory = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\" & sesBranchId.Replace("'", "") & "\" & Group & "\" & ExecutorID & "\"
        strDirectory = getpathFolder() & sesBranchId.Replace("'", "") & "\" & Group & "\" & ExecutorID & "\"
        If Not System.IO.Directory.Exists(strDirectory) Then
            System.IO.Directory.CreateDirectory(strDirectory)
        End If
        Return strDirectory
    End Function


    Private Function getDownloadPath(filePath As String) As String
        Return Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd("/"c) + "/Webform.Utility/GetImage.aspx?file=" & Server.UrlEncode(getpathFolder() & filePath)
    End Function

    Private Function getpathFolder() As String
        If pathFolder = "" Then
            Dim oClass As New Parameter.GeneralSetting
            Dim oCtr As New GeneralSettingController
            With oClass
                .strConnection = GetConnectionString()
                .GSID = "UploadFolder"
                .ModuleID = "GEN"
            End With
            Try
                Return oCtr.GetGeneralSettingValue(oClass).Trim
            Catch ex As Exception
                Return ""
            End Try
        Else
            Return pathFolder
        End If
    End Function


    Private Sub cekimage()
        If File.Exists(pathFile("HasilMobileProspect", Me.ProspectAppID) + "LampiranFotoKtpCustomer.jpg") Then
            FotoCustomer.ImageUrl = ResolveClientUrl(getDownloadPath(sesBranchId.Replace("'", "") & "/HasilMobileProspect/" + Me.ProspectAppID + "/LampiranFotoKtpCustomer.jpg"))
        Else
            FotoCustomer.ImageUrl = ResolveClientUrl("../../../xml/NoFoto.png")
        End If

        If File.Exists(pathFile("HasilMobileProspect", Me.ProspectAppID) + "LampiranFotoKtpPasangan.jpg") Then
            FotoPasangan.ImageUrl = ResolveClientUrl(getDownloadPath(sesBranchId.Replace("'", "") & "/HasilMobileProspect/" + Me.ProspectAppID + "/LampiranFotoKtpPasangan.jpg"))
        Else
            FotoPasangan.ImageUrl = ResolveClientUrl("../../../xml/NoFoto.png")
        End If

        FotoCustomer.Height = 200
        FotoCustomer.Width = 300

        FotoPasangan.Height = 200
        FotoPasangan.Width = 300
    End Sub

    Private Sub dtgPaging_ItemDataBound(sender As Object, e As DataGridItemEventArgs) Handles dtgPaging.ItemDataBound

        Dim hyTemp As HyperLink
        Dim HyappId As HyperLink

        If e.Item.ItemIndex >= 0 Then

            HyappId = CType(e.Item.FindControl("lblProspectAppID"), HyperLink)
            hyTemp = CType(e.Item.FindControl("lblProspectAppID"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenWinViewProspectInquiry('" & hyTemp.Text.Trim & "')"
        End If
    End Sub
End Class