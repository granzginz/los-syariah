﻿Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
Imports System.IO
Imports System.Collections.Generic

Public Class InitialApplicationMainCompany
    Inherits Maxiloan.Webform.WebBased

#Region "Controls"
    Private oController As New ProspectController
    Private m_controller As New CustomerController
    Private oCustomer As New Parameter.Customer
    Protected WithEvents ucJarakSurvey As ucNumberFormat
    Protected WithEvents ucOTR As ucNumberFormat
    Protected WithEvents ucUangMuka As ucNumberFormat
    Protected WithEvents ucUangMukaTradeIn As ucNumberFormat
    Protected WithEvents ucAngsuranBulanan As ucNumberFormat
    Protected WithEvents ucTenor As ucNumberFormat
    Private time As String


    Private m_controllerP As New ProductController

    Protected WithEvents ucLookupProductOffering1 As UcLookUpPdctOffering

#End Region

#Region "Properties"
    Property ProspectAppID() As String
        Get
            Return ViewState("ProspectAppID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ProspectAppID") = Value
        End Set
    End Property

    Property PageMode() As String
        Get
            Return ViewState("PageMode").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("PageMode") = Value
        End Set
    End Property

    Property ActivityDateStart() As DateTime
        Get
            Return ViewState("ActivityDateStart").ToString
        End Get
        Set(ByVal Value As DateTime)
            ViewState("ActivityDateStart") = Value
        End Set
    End Property

    Property ActivityDateEnd() As DateTime
        Get
            Return ViewState("ActivityDateEnd").ToString
        End Get
        Set(ByVal Value As DateTime)
            ViewState("ActivityDateEnd") = Value
        End Set
    End Property

    Property ProspectSource() As String
        Get
            Return ViewState("ProspectSource").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ProspectSource") = Value
        End Set
    End Property
    Public Property KegiatanUsahaS As IList(Of Parameter.KegiatanUsaha)
        Get
            Return CType(ViewState("KegiatanUsaha"), IList(Of Parameter.KegiatanUsaha))
        End Get
        Set(value As IList(Of Parameter.KegiatanUsaha))
            ViewState("KegiatanUsaha") = value
        End Set
    End Property
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Visible = False

        If SessionInvalid() Then Exit Sub
        'Me.ProspectSource = "ARIUM"
        If Not Page.IsPostBack Then
            initObjects()
            Me.PageMode = Request("page")
            If Me.PageMode = "Edit" Then
                Dim oCustomclass As New Parameter.Prospect
                Me.ProspectAppID = Request("id")
                If Request("prospectsource") Is Nothing Then
                    Me.ProspectSource = Request("prospectsource")
                Else
                    Me.ProspectSource = "ARIUM"
                End If
                With oCustomclass
                    .strConnection = GetConnectionString()
                    .ProspectAppID = Me.ProspectAppID
                End With
                BindEdit()
                time = Now.Hour.ToString("00") & ":" & Now.Minute.ToString("00") & ":" & Now.Second.ToString("00")
                Me.ActivityDateStart = Me.BusinessDate + " " + time
            Else
                'Me.ProspectAppID = ""
                time = Now.Hour.ToString("00") & ":" & Now.Minute.ToString("00") & ":" & Now.Second.ToString("00")
                Me.ActivityDateStart = Me.BusinessDate + " " + time
                Me.ProspectAppID = Replace(Me.sesBranchId, "'", "").ToString
                'hdfProspectAppID.Value = Me.ProspectAppID
            End If
            LoadingKegiatanUsaha()
            GetCboSkePemb_("TblSkemaPembiayaan", cboAkad)
        End If
        btnProceed.Enabled = (PageMode = "Edit")
    End Sub


#Region "Init Objects"
    Protected Sub initObjects()
        'btnProceed.Visible = False
        btnSave.Visible = True
        PanelInfo.Visible = False
        fillCboIDType()
    End Sub

    Sub fillCboIDType()
        Dim oCustomer As New Parameter.Customer
        Dim dtList As DataTable
        oCustomer.strConnection = GetConnectionString()
        oCustomer = m_controller.CustomerCompanyGetIDType(oCustomer)
        dtList = oCustomer.listdata
        cboIDTypeP.DataSource = dtList
        cboIDTypeP.DataTextField = "Description"
        cboIDTypeP.DataValueField = "ID"
        cboIDTypeP.DataBind()
        cboIDTypeP.Items.Insert(0, "Select One")
        cboIDTypeP.Items(0).Value = "Select One"
    End Sub
#End Region

#Region "BindEdit"
    Sub BindEdit()
        Dim oApplication As New Parameter.Prospect
        Dim oData As New DataTable
        Dim oRow As DataRow

        oApplication.strConnection = GetConnectionString()
        oApplication.BranchId = Replace(Me.sesBranchId, "'", "").ToString
        oApplication.ProspectAppID = Me.ProspectAppID
        oApplication = oController.GetViewProspect(oApplication)
        'hdfProspectAppID.Value = Me.ProspectAppID

        If Not oApplication Is Nothing Then
            oData = oApplication.listdata
        End If

        If oData.Rows.Count > 0 Then
            oRow = oData.Rows(0)
            txttanggalAplikasi.Text = Format(oRow("ProspectAppDate"), "dd/MM/yyyy")
            txtNama.Text = oRow("Name").ToString
            txtTempatLahir.Text = oRow("BirthPlace").ToString
            txtTanggalLahir.Text = Format(oRow("BirthDate"), "dd/MM/yyyy")
            cboIDTypeP.SelectedIndex = cboIDTypeP.Items.IndexOf(cboIDTypeP.Items.FindByValue(oRow("IDType").ToString.Trim))
            txtIDNumberP.Text = oRow("IDNumber").ToString.Trim
            'hdfAssettypeCode.Value = oRow("AssetType").ToString
            'txtAssetType.Text = oRow("AssetTypeDescription").ToString
            'hdfAssetBrandCode.Value = oRow("AssetBrand").ToString
            'txtAssetBrand.Text = oRow("AssetBrandDescription").ToString
            ucOTR.Text = FormatNumber(oRow("OTRPrice"), 0)
            ucUangMuka.Text = FormatNumber(oRow("DPAmount"), 0)
            ucUangMukaTradeIn.Text = FormatNumber(oRow("DPAmountTradeIn"), 0)
            lblTotalUangMuka.Text = FormatNumber(CDec(oRow("DPAmount")) + CDec(oRow("DPAmountTradeIn")), 0)
            lblPokokHutang.Text = FormatNumber(oRow("NTF"), 0)
            ucAngsuranBulanan.Text = FormatNumber(oRow("InstallmentAmount"), 0)
            ucTenor.Text = oRow("Tenor").ToString
            tanggalLahir.Value = ConvertDate(oRow("BirthDate"))
        End If
    End Sub
#End Region


    Private Sub LoadingKegiatanUsaha()

        Me.KegiatanUsahaS = m_controllerP.LoadKegiatanUsaha(GetConnectionString())
        cboKegiatanUsaha.DataSource = KegiatanUsahaS
        cboKegiatanUsaha.DataValueField = "Value"
        cboKegiatanUsaha.DataTextField = "Text"
        cboKegiatanUsaha.DataBind()
        cboKegiatanUsaha.Items.Insert(0, "Select One")
        cboKegiatanUsaha.Items(0).Value = "SelectOne"
        cboKegiatanUsaha.SelectedIndex = 0


        Dim def = New List(Of Parameter.CommonValueText)
        def.Add(New Parameter.CommonValueText("SelectOne", "Select One"))

        'cboJenisPembiyaan.DataValueField = "Value"
        'cboJenisPembiyaan.DataTextField = "Text"
        'cboJenisPembiyaan.DataSource = def
        'cboJenisPembiyaan.Items.Insert(0, "Select One")
        'cboJenisPembiyaan.Items(0).Value = "SelectOne"
        'cboJenisPembiyaan.DataBind()

        'If cboJenisPembiyaan.SelectedValue.ToString.Trim = "FL" Then
        '    chkHakOpsi.Enabled = True
        'End If
    End Sub


#Region "Save"
    Private Sub imbSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If Me.Page.IsValid Then
                lblMessage.Visible = False

                time = Now.Hour.ToString("00") & ":" & Now.Minute.ToString("00") & ":" & Now.Second.ToString("00")
                Me.ActivityDateEnd = Me.BusinessDate + " " + time

                Dim oApplication As New Parameter.Prospect
                oApplication.strConnection = GetConnectionString()
                oApplication.BusinessDate = Me.BusinessDate
                oApplication.BranchId = Replace(Me.sesBranchId, "'", "").ToString
                oApplication.ProductID = ""
                oApplication.CustomerID = ""
                oApplication.CustomerType = "C"
                oApplication.Name = txtNama.Text
                oApplication.ProspectAppDate = IIf(txttanggalAplikasi.Text <> "", ConvertDate(txttanggalAplikasi.Text), "").ToString
                oApplication.BirthPlace = txtTempatLahir.Text
                oApplication.BirthDate = IIf(txtTanggalLahir.Text <> "", ConvertDate(txtTanggalLahir.Text), "").ToString
                oApplication.IDType = cboIDTypeP.SelectedValue
                oApplication.IDNumber = txtIDNumberP.Text.Trim
                oApplication.Mode = Me.PageMode
                oApplication.LoginId = Me.Loginid
                oApplication.AssetType = "" ' hdfAssettypeCode.Value
                oApplication.AssetBrand = "" 'hdfAssetBrandCode.Value
                oApplication.OTRPrice = CDec(ucOTR.Text)
                oApplication.DPAmount = CDec(ucUangMuka.Text)
                oApplication.DPAmountTradeIn = CDec(ucUangMukaTradeIn.Text)
                oApplication.NTF = oApplication.OTRPrice - (oApplication.DPAmount + oApplication.DPAmountTradeIn)
                oApplication.InstallmentAmount = CDec(ucAngsuranBulanan.Text)
                oApplication.Tenor = CDec(ucTenor.Text)
                oApplication.ProspectSource = "" 'Me.ProspectSource
                oApplication.SupplierID = "" 'txtSupplierCode.Text.Trim

                If Me.PageMode <> "Edit" Then
                    oApplication.ProspectAppID = ""
                Else
                    oApplication.ProspectAppID = Me.ProspectAppID
                End If

                Dim ErrorMessage As String = ""
                Dim oReturn As New Parameter.Prospect

                oApplication.KegiatanUsaha = cboKegiatanUsaha.SelectedValue
                oApplication.AkadPembiayaan = cboAkad.SelectedValue
                oApplication.ProductID = ucLookupProductOffering1.ProductID
                oApplication.ProductOfferingID = ucLookupProductOffering1.ProductOfferingID


                oReturn = oController.InitialSaveAdd(oApplication)

                If oReturn.Err <> "" Then
                    ShowMessage(lblMessage, ErrorMessage, True)                    
                Else
                    btnSave.Visible = False
                    btnProceed.Visible = True
                    btnProceed.Enabled = True
                    Me.ProspectAppID = oReturn.ProspectAppID
                    ShowMessage(lblMessage, "Data Saved !", False)
                    'Response.Redirect("InitialApplication.aspx?id=" & oReturn.ProspectAppID.Trim & "&page=Edit&Msg=Data Saved !")
                    Me.PageMode = "Edit"
                End If
            Else
                ShowMessage(lblMessage, "Data Sudah Ada", True)
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub
#End Region

    Protected Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("InitialApplication.aspx")
    End Sub

    Private Sub BindGrid1()
        Dim dtEntity As New DataTable
        With oCustomer
            .Name = txtNama.Text
            .IDType = cboIDTypeP.SelectedValue
            .IDNumber = txtIDNumberP.Text
            .BirthDate = IIf(txtTanggalLahir.Text <> "", ConvertDateSql(txtTanggalLahir.Text), "").ToString
            .strConnection = GetConnectionString()
            oCustomer = m_controller.BindCustomer1_002(oCustomer)
            If Not oCustomer Is Nothing Then
                dtEntity = oCustomer.listdata
            End If
        End With
        dtgList1.DataSource = dtEntity.DefaultView
        dtgList1.CurrentPageIndex = 0
        dtgList1.DataBind()
    End Sub

    Private Sub BindGrid2()
        Dim dtEntity As New DataTable
        With oCustomer
            .Name = txtNama.Text
            .IDType = cboIDTypeP.SelectedValue
            .IDNumber = txtIDNumberP.Text
            .BirthDate = IIf(txtTanggalLahir.Text <> "", ConvertDateSql(txtTanggalLahir.Text), "").ToString
            .strConnection = GetConnectionString()
            oCustomer = m_controller.BindCustomer2_002(oCustomer)
            If Not oCustomer Is Nothing Then
                dtEntity = oCustomer.listdata
            End If
        End With

        If Not oCustomer Is Nothing Then
            dtEntity = oCustomer.listdata
            For Each r As DataRow In dtEntity.Rows
                If r.Item("type").ToString.Trim = "Name + Birth Date + ID Number" Then
                    btnSave.Visible = False
                End If
            Next
        End If
        dtgList2.DataSource = dtEntity.DefaultView
        dtgList2.CurrentPageIndex = 0
        dtgList2.DataBind()
    End Sub

    Private Sub dtgList1_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgList1.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            CType(e.Item.FindControl("lblNo1"), Label).Text = CStr(e.Item.ItemIndex + 1)
            Dim lnkCustomer As LinkButton
            Dim lblCustomer As Label
            lnkCustomer = CType(e.Item.FindControl("lnkCustName"), LinkButton)
            lblCustomer = CType(e.Item.FindControl("lblCust"), Label)
            lnkCustomer.Attributes.Add("OnClick", "return OpenCust('" & lblCustomer.Text & "','Marketing');")
        End If
    End Sub

    Private Sub dtgList2_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgList2.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            CType(e.Item.FindControl("lblNo2"), Label).Text = CStr(e.Item.ItemIndex + 1)
            Dim lnkCustomer As LinkButton
            Dim lblCustomer As Label
            lnkCustomer = CType(e.Item.FindControl("lnkCustName2"), LinkButton)
            lblCustomer = CType(e.Item.FindControl("lblCust2"), Label)
            lnkCustomer.Attributes.Add("OnClick", "return OpenCust('" & lblCustomer.Text & "','Marketing');")
        End If
    End Sub

    Private Function getAge(ByVal dt As String, ByVal m As Integer) As String
        Dim rtn As String = ""
        Dim dob As DateTime
        dob = New DateTime(dt.Substring(0, 4), dt.Substring(6, 2), dt.Substring(4, 2))
        Dim tday As TimeSpan = DateTime.Now.AddMonths(m).Subtract(dob)
        Dim years As Integer, months As Integer, days As Integer
        months = 12 * (DateTime.Now.AddMonths(m).Year - dob.Year) + (DateTime.Now.AddMonths(m).Month - dob.Month)

        If DateTime.Now.AddMonths(m).Day < dob.Day Then
            months -= 1
            days = DateTime.DaysInMonth(dob.Year, dob.Month) - dob.Day + DateTime.Now.AddMonths(m).Day
        Else
            days = DateTime.Now.AddMonths(m).Day - dob.Day
        End If
        years = Math.Floor(months / 12)
        months -= years * 12

        rtn = years & " tahun, " & months & " bulan and " & days & " hari"
        Return rtn
    End Function

    Protected Sub btnProceed_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProceed.Click

        Try
            'imbSave_Click(Nothing, Nothing)
            'If (oController.DoProceeded(GetConnectionString, ProspectAppID)) Then
            If (oController.DoProceeded(GetConnectionString, Me.ProspectAppID.Trim)) Then
                ProspectLog()
                'ShowMessage(lblMessage, String.Format("Prospect Id {0} berhasil di Proceed.", ProspectAppID), False)
                ShowMessage(lblMessage, String.Format("Prospect Id {0} berhasil di Proceed.", Me.ProspectAppID.Trim), False)
            Else
                'Throw New Exception(String.Format("Prospect Id {0} belum berhasil di Proceed. Silahkan coba lagi.", ProspectAppID))
                Throw New Exception(String.Format("Prospect Id {0} belum berhasil di Proceed. Silahkan coba lagi.", Me.ProspectAppID.Trim))
            End If
            Response.Redirect("InitialApplication.aspx")
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try

    End Sub

    Sub ProspectLog()
        time = Now.Hour.ToString("00") & ":" & Now.Minute.ToString("00") & ":" & Now.Second.ToString("00")
        Me.ActivityDateEnd = Me.BusinessDate + " " + time

        Dim oApplication As New Parameter.Prospect
        oApplication.strConnection = GetConnectionString()
        oApplication.BranchId = Replace(Me.sesBranchId, "'", "").ToString
        oApplication.ProspectAppID = Me.ProspectAppID
        oApplication.ActivityType = "INT"
        oApplication.ActivityDateStart = Me.ActivityDateStart
        oApplication.ActivityDateEnd = Me.ActivityDateEnd
        oApplication.ActivityUser = Me.Loginid
        oApplication.ActivitySeqNo = 1

        Dim ErrorMessage As String = ""
        Dim oReturn As New Parameter.Prospect

        oReturn = oController.ProspectLogSave(oApplication)

        If oReturn.Err <> "" Then
            ShowMessage(lblMessage, ErrorMessage, True)
            Exit Sub
        End If
    End Sub


    Private Sub cboKegiatanUsaha_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cboKegiatanUsaha.SelectedIndexChanged
        Dim cbo = CType(sender, DropDownList)
        Dim value = cbo.SelectedValue
        If cbo.SelectedIndex = 0 Then
            refresh_cboJenisPembiayaan("")
        Else
            ucLookupProductOffering1.KegiatanUsaha = value
            refresh_cboJenisPembiayaan(value)
        End If
        'If cboJenisPembiyaan.SelectedValue.ToString.Trim = "FL" Then
        '    chkHakOpsi.Enabled = True
        'End If
    End Sub

    Protected Sub GetCboSkePemb_(ByVal table As String, cboReference As DropDownList)
        Dim dtEntity As DataTable = Nothing
        Dim oCboSkePemb As New Parameter.Product
        oCboSkePemb.strConnection = GetConnectionString()
        oCboSkePemb.Table = table
        oCboSkePemb = m_controllerP.GetCboSkePemb(oCboSkePemb)
        If Not oCboSkePemb Is Nothing Then
            dtEntity = oCboSkePemb.ListData
        End If

        cboReference.DataSource = dtEntity.DefaultView
        cboReference.DataTextField = "Description"
        cboReference.DataValueField = "ID"
        cboReference.DataBind()
        cboReference.Items.Insert(0, "Select One")
        cboReference.Items(0).Value = "Select One"

    End Sub


    Private Sub cboAkad_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cboAkad.SelectedIndexChanged
        Dim cbo = CType(sender, DropDownList)
        Dim value = cbo.SelectedValue
        If cbo.SelectedIndex = 0 Then
            refresh_cboAkad("")
        Else
            ucLookupProductOffering1.Akad = value
            refresh_cboAkad(value)
        End If
    End Sub

    Sub refresh_cboAkad(key As String)
        Dim def = New List(Of Parameter.CommonValueText)
        def.Add(New Parameter.CommonValueText("SelectOne", "Select One"))
        cboAkad.DataValueField = "Value"
        cboAkad.DataTextField = "Text"

        'If key = String.Empty Then
        '    cboAkad.DataSource = def
        '    cboAkad.DataBind()
        'Else
        '    Dim k = KegiatanUsahaS.Where(Function(x) x.Value = key).SingleOrDefault()
        '    cboJenisPembiyaan.DataSource = k.JenisPembiayaan
        'End If
        cboAkad.DataBind()
        'cboAkad.SelectedIndex = cboAkad.Items.IndexOf(cboAkad.Items.FindByValue("IF"))
        ucLookupProductOffering1.Akad = cboAkad.SelectedValue
    End Sub


    Sub refresh_cboJenisPembiayaan(key As String)
        Dim def = New List(Of Parameter.CommonValueText)
        def.Add(New Parameter.CommonValueText("SelectOne", "Select One"))
        'cboJenisPembiyaan.DataValueField = "Value"
        'cboJenisPembiyaan.DataTextField = "Text"

        'If key = String.Empty Then
        '    cboJenisPembiyaan.DataSource = def
        '    cboJenisPembiyaan.DataBind()
        'Else
        '    Dim k = KegiatanUsahaS.Where(Function(x) x.Value = key).SingleOrDefault()
        '    cboJenisPembiyaan.DataSource = k.JenisPembiayaan
        'End If
        'cboJenisPembiyaan.DataBind()
        'cboJenisPembiyaan.SelectedIndex = cboJenisPembiyaan.Items.IndexOf(cboJenisPembiyaan.Items.FindByValue("IF"))
        'ucLookupProductOffering1.JenisPembiayaan = cboKegiatanUsaha.SelectedValue
    End Sub


End Class