﻿#Region "Imports"
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
#End Region

Public Class ReInputDataSurvey
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private oController As New ProspectController
    Private oCustomclass As New Parameter.Prospect

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region
#Region "Property"
    Private Property Err() As String
        Get
            Return CType(ViewState("Err"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Err") = Value
        End Set
    End Property
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("prosCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("prosCmdWhere") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property

    Private Property ProspectAppID() As String
        Get
            Return CType(ViewState("ProspectAppID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ProspectAppID") = Value
        End Set
    End Property

    Private Property Msg() As String
        Get
            Return CType(ViewState("Msg"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Msg") = Value
        End Set
    End Property
#End Region
    Private Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If

        lblMessage.Text = ""
        lblMessage.Visible = False
        If Not Page.IsPostBack Then
            If Request.QueryString("Err") <> "" Then
                Me.Err = Request.QueryString("Err")
                ShowMessage(lblMessage, Me.Err, True)
            End If

            If Request.QueryString("Msg") <> "" Then
                Me.Err = Request.QueryString("Msg")
                ShowMessage(lblMessage, Me.Msg, False)
            End If

            txtGoPage.Text = "1"
            Me.FormID = "Prospect"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            If Request("cond") <> "" Then
                Me.CmdWhere = Request("cond")
            Else
                CmdWhere = String.Format("Prospect.IsProceeded=0 and Prospect.BranchID ='{0}' and Prospect.ProspectStep='IDS' ", sesBranchId.Replace("'", "").Trim)
            End If
            Me.Sort = "ProspectAppId ASC"
            BindGrid(Me.CmdWhere)
        End If
    End Sub

    Sub BindGrid(ByVal cmdWhere As String)
        Dim dtEntity As DataTable
        Dim oCustomClass As New Parameter.Prospect
        pnlSearch.Visible = True
        pnlList.Visible = True
        oCustomClass.PageSize = pageSize

        oCustomClass.WhereCond = cmdWhere
        oCustomClass.SortBy = Me.Sort
        oCustomClass.CurrentPage = currentPage
        oCustomClass.strConnection = GetConnectionString()
        oCustomClass = oController.GetInitial(oCustomClass)
        If Not oCustomClass Is Nothing Then
            dtEntity = oCustomClass.listdata
            recordCount = oCustomClass.TotalRecords
        Else
            recordCount = 0
        End If
        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()
        PagingFooter()
    End Sub

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan!", True)

            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblTotRec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGrid(Me.CmdWhere)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGoPage.Click
        If IsNumeric(txtGoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtGoPage.Text, Int32)
                BindGrid(Me.CmdWhere)
            End If
        End If
    End Sub


    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGrid(Me.CmdWhere)
    End Sub


    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        cboSearch.SelectedIndex = 0
        txtSearch.Text = ""
        Me.CmdWhere = "ALL"
        BindGrid(Me.CmdWhere)
    End Sub

    Private Sub imbSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        CmdWhere = String.Format("Prospect.IsProceeded=0 and Prospect.BranchID ='{0}' and Prospect.ProspectStep='IDS' ", sesBranchId.Replace("'", "").Trim)
        If txtSearch.Text.Trim <> "" Then
            CmdWhere = String.Format("isProceeded=0 and {0} LIKE '%{1}%' and  Prospect.BranchID ='{2}' and Prospect.ProspectStep='IDS' ", cboSearch.SelectedItem.Value, txtSearch.Text.Replace("%", ""), sesBranchId.Replace("'", "").Trim)
        End If
        BindGrid(Me.CmdWhere)
    End Sub

    Private Sub dtgEntity_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        If e.Item.ItemIndex >= 0 Then
            If e.CommandName = "Modify" Then
                Dim ProspectAppID As String = CType(e.Item.FindControl("lblApplication"), Label).Text.Trim
                If CheckFeature(Me.Loginid, Me.FormID, "Edit", Me.AppId) Then
                    If SessionInvalid() Then
                        Exit Sub
                    End If
                End If
                Response.Redirect("ReInputDataSurveyMain.aspx?id=" & ProspectAppID.Trim & "&page=Edit")
            End If
            If e.CommandName = "Return" Then
                Dim ProspectAppID As String = CType(e.Item.FindControl("lblApplication"), Label).Text.Trim
                Me.ProspectAppID = ProspectAppID
                If CheckFeature(Me.Loginid, Me.FormID, "Edit", Me.AppId) Then
                    If SessionInvalid() Then
                        Exit Sub
                    End If
                End If
                pnlReturn.Visible = True
                pnlSearch.Visible = False
                pnlList.Visible = False
                BindEdit()
                txtAlasanReturn.Text = ""
                'ProsesReturn()
            End If
        End If
    End Sub

    Sub ProsesReturn()
        Dim oApplication As New Parameter.Prospect

        oApplication.strConnection = GetConnectionString()
        oApplication.BusinessDate = Me.BusinessDate
        oApplication.BranchId = Me.sesBranchId.Replace("'", "").Trim
        oApplication.ProspectAppID = Me.ProspectAppID
        oApplication.Notes = txtAlasanReturn.Text.Trim

        Try
            oController.ProspectReturnUpdate(oApplication)

            If Request("cond") <> "" Then
                Me.CmdWhere = Request("cond")
            Else
                CmdWhere = String.Format("Prospect.IsProceeded=0 and Prospect.BranchID ='{0}' and Prospect.ProspectStep='IDS' ", sesBranchId.Replace("'", "").Trim)
            End If
            Me.Sort = "ProspectAppId ASC"
            BindGrid(Me.CmdWhere)
            pnlList.Visible = True
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try

    End Sub

    Protected Sub btnReturn_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnReturn.Click
        ProsesReturn()
        'Me.CmdWhere = ""
        If Request("cond") <> "" Then
            Me.CmdWhere = Request("cond")
        Else
            CmdWhere = String.Format("Prospect.IsProceeded=0 and Prospect.BranchID ='{0}' and Prospect.ProspectStep='IDS' ", sesBranchId.Replace("'", "").Trim)
        End If
        Me.Sort = "ProspectAppId ASC"
        BindGrid(Me.CmdWhere)
        pnlList.Visible = True
        pnlSearch.Visible = True
        pnlReturn.Visible = False
    End Sub

    Protected Sub ButtonCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonCancel.Click
        If Request("cond") <> "" Then
            Me.CmdWhere = Request("cond")
        Else
            CmdWhere = String.Format("Prospect.IsProceeded=0 and Prospect.BranchID ='{0}' and Prospect.ProspectStep='IDS' ", sesBranchId.Replace("'", "").Trim)
        End If
        Me.Sort = "ProspectAppId ASC"
        BindGrid(Me.CmdWhere)

        pnlList.Visible = True
        pnlSearch.Visible = True
        pnlReturn.Visible = False
    End Sub

#Region "BindEdit"
    Sub BindEdit()
        Dim oApplication As New Parameter.Prospect
        Dim oData As New DataTable
        Dim oRow As DataRow

        oApplication.strConnection = GetConnectionString()
        oApplication.BranchId = Replace(Me.sesBranchId, "'", "").ToString
        oApplication.ProspectAppID = Me.ProspectAppID
        oApplication = oController.GetViewProspect(oApplication)

        If Not oApplication Is Nothing Then
            oData = oApplication.listdata
        End If

        If oData.Rows.Count > 0 Then
            oRow = oData.Rows(0)
            lbltanggalAplikasi.Text = Format(oRow("ProspectAppDate"), "dd/MM/yyyy")
            lblProspectAppId.Text = oRow("ProspectAppID").ToString
            lblSupplierName.Text = oRow("SupplierName").ToString

            lblNama.Text = oRow("Name").ToString
            lblJenisAsset.Text = oRow("AssetTypeDescription").ToString
            lblMerkAsset.Text = oRow("AssetBrandDescription").ToString
        End If
    End Sub
#End Region
End Class