﻿Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
Imports System.IO
Imports System.Collections.Generic

Public Class ReInputDataSurveyMain
    Inherits Maxiloan.Webform.WebBased

#Region "Controls"
    Private oController As New ProspectController
    Private m_controller As New CustomerController
    Private m_controller2 As New AssetDataController
    Private oCustomer As New Parameter.Customer
    Protected WithEvents ucMailingAddress As UcCompanyAddress2
    Private time As String
#End Region

#Region "Properties"
    Property ProspectAppID() As String
        Get
            Return ViewState("ProspectAppID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ProspectAppID") = Value
        End Set
    End Property
    Property PageMode() As String
        Get
            Return ViewState("PageMode").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("PageMode") = Value
        End Set
    End Property
    Property AOID() As String
        Get
            Return ViewState("AOID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("AOID") = Value
        End Set
    End Property

    Property SupplierID() As String
        Get
            Return ViewState("SupplierID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("SupplierID") = Value
        End Set
    End Property

    Property ActivityDateStart() As DateTime
        Get
            Return ViewState("ActivityDateStart").ToString
        End Get
        Set(ByVal Value As DateTime)
            ViewState("ActivityDateStart") = Value
        End Set
    End Property

    Property ActivityDateEnd() As DateTime
        Get
            Return ViewState("ActivityDateEnd").ToString
        End Get
        Set(ByVal Value As DateTime)
            ViewState("ActivityDateEnd") = Value
        End Set
    End Property
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Visible = False

        If SessionInvalid() Then Exit Sub

        If Not Page.IsPostBack Then
            initObjects()
            Me.PageMode = Request("page")
            If Me.PageMode = "Edit" Then
                ucMailingAddress.Phone1ValidatorEnabled(False)
                Dim oCustomclass As New Parameter.Prospect
                Me.ProspectAppID = Request("id")
                With oCustomclass
                    .strConnection = GetConnectionString()
                    .ProspectAppID = Me.ProspectAppID
                End With

                Dim WhereSup As String = "BranchID = '" & Me.sesBranchId.Replace("'", "") & "' and EmployeePosition = 'AO'"
                FillCboEmp("BranchEmployee", cbocmo, WhereSup)
                BindEdit()
                time = Now.Hour.ToString("00") & ":" & Now.Minute.ToString("00") & ":" & Now.Second.ToString("00")
                Me.ActivityDateStart = Me.BusinessDate + " " + time
            Else
                Me.ProspectAppID = ""
            End If
        End If
    End Sub


#Region "Init Objects"
    Protected Sub initObjects()
        btnSave.Visible = True
        ucMailingAddress.ValidatorTrue2()
        ucMailingAddress.showMandatoryAll2()
    End Sub

#End Region

#Region "BindEdit"
    Sub BindEdit()
        Dim oApplication As New Parameter.Prospect
        Dim oData As New DataTable
        Dim oRow As DataRow

        oApplication.strConnection = GetConnectionString()
        oApplication.BranchId = Replace(Me.sesBranchId, "'", "").ToString
        oApplication.ProspectAppID = Me.ProspectAppID
        oApplication = oController.GetViewProspect(oApplication)
        'hdfProspectAppID.Value = Me.ProspectAppID

        If Not oApplication Is Nothing Then
            oData = oApplication.listdata
        End If

        If oData.Rows.Count > 0 Then
            oRow = oData.Rows(0)
            lbltanggalAplikasi.Text = Format(oRow("ProspectAppDate"), "dd/MM/yyyy")
            lblProspectAppId.Text = oRow("ProspectAppID").ToString
            'Me.SupplierID = oRow("SupplierID").ToString

            'txtSupplierCode.Text = oRow("SupplierID").ToString
            'txtSupplierName.Text = oRow("SupplierName").ToString
            'Me.SupplierID = txtSupplierCode.Text.ToString.Trim
            lblSupplierName.Text = oRow("SupplierName").ToString

            lblNama.Text = oRow("Name").ToString
            With (ucMailingAddress)
                .Address = oRow("Address").ToString.Trim
                .RT = oRow("RT").ToString.Trim
                .RW = oRow("RW").ToString.Trim
                .Kelurahan = oRow("Kelurahan").ToString.Trim
                .Kecamatan = oRow("Kecamatan").ToString.Trim
                .City = oRow("City").ToString.Trim
                .ZipCode = oRow("ZipCode").ToString.Trim
                .AreaPhone1 = oRow("AreaPhone1").ToString.Trim
                .Phone1 = oRow("Phone1").ToString.Trim
                .AreaPhone2 = oRow("AreaPhone2").ToString.Trim
                .Phone2 = oRow("Phone2").ToString.Trim
                .AreaFax = oRow("AreaFax").ToString.Trim
                .Fax = oRow("Fax").ToString.Trim
                .Provinsi = oRow("Provinsi").ToString.Trim
                .BindAddress()
            End With
            txtMobilePhone.Text = oRow("MobilePhone").ToString
            txtMotherName.Text = oRow("MotherName").ToString
            txtNoRumah.Text = oRow("No_Rumah").ToString
            txtStreetName.Text = oRow("Street1").ToString
            'txtNegara.Text = oRow("Negara").ToString
            txtNegara.Text = "INDONESIA"
            Me.AOID = oRow("AOID").ToString
            cbocmo.SelectedIndex = cbocmo.Items.IndexOf(cbocmo.Items.FindByValue(Me.AOID.Trim))
            lblJenisAsset.Text = oRow("AssetTypeDescription").ToString
            lblMerkAsset.Text = oRow("AssetBrandDescription").ToString
        End If
    End Sub
#End Region

#Region "Save"
    Private Sub imbSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If Me.Page.IsValid Then
                lblMessage.Visible = False

                time = Now.Hour.ToString("00") & ":" & Now.Minute.ToString("00") & ":" & Now.Second.ToString("00")
                Me.ActivityDateEnd = Me.BusinessDate + " " + time

                Dim oApplication As New Parameter.Prospect
                oApplication.strConnection = GetConnectionString()
                oApplication.BusinessDate = Me.BusinessDate
                oApplication.BranchId = Replace(Me.sesBranchId, "'", "").ToString
                oApplication.ProductID = ""
                oApplication.CustomerID = ""
                oApplication.CustomerType = "P"
                oApplication.Name = lblNama.Text()
                oApplication.ProspectAppDate = IIf(lbltanggalAplikasi.Text <> "", ConvertDate(lbltanggalAplikasi.Text), "").ToString
                oApplication.Address = ucMailingAddress.Address
                oApplication.RT = ucMailingAddress.RT
                oApplication.RW = ucMailingAddress.RW
                oApplication.Kecamatan = ucMailingAddress.Kecamatan
                oApplication.Kelurahan = ucMailingAddress.Kelurahan
                oApplication.ZipCode = ucMailingAddress.ZipCode
                oApplication.City = ucMailingAddress.City
                oApplication.Provinsi = ucMailingAddress.Provinsi
                oApplication.AreaPhone1 = ucMailingAddress.AreaPhone1
                oApplication.AreaPhone2 = ucMailingAddress.AreaPhone2
                oApplication.AreaFax = ucMailingAddress.AreaFax
                oApplication.Phone1 = ucMailingAddress.Phone1
                oApplication.Phone2 = ucMailingAddress.Phone2
                oApplication.Fax = ucMailingAddress.Fax
                oApplication.MobilePhone = txtMobilePhone.Text
                oApplication.MotherName = txtMotherName.Text
                oApplication.AOID = cbocmo.SelectedValue.ToString
                oApplication.OldAOID = Me.AOID.Trim
                oApplication.LoginId = Me.Loginid
                oApplication.Mode = Me.PageMode
                oApplication.NoRumah = txtNoRumah.Text
                oApplication.StreetName = txtStreetName.Text
                oApplication.Country = txtNegara.Text
                'oApplication.SupplierID = txtSupplierCode.Text.Trim
                oApplication.ActivityDateStart = Me.ActivityDateStart
                oApplication.ActivityDateEnd = Me.ActivityDateEnd
                oApplication.ActivityUser = Me.Loginid
                oApplication.ActivitySeqNo = 3

                If Me.PageMode <> "Edit" Then
                    oApplication.ProspectAppID = ""
                Else
                    oApplication.ProspectAppID = Me.ProspectAppID
                End If

                Dim ErrorMessage As String = ""
                Dim oReturn As New Parameter.Prospect

                oReturn = oController.DataSurveySaveEdit(oApplication)

                If oReturn.Err <> "" Then
                    ShowMessage(lblMessage, ErrorMessage, True)
                Else
                    Response.Redirect("ReInputDataSurvey.aspx?id=" & oReturn.ProspectAppID.Trim & "&page=Edit&Msg=Data Saved !")
                    Me.PageMode = "Edit"
                End If
            Else
                ShowMessage(lblMessage, "Data Sudah Ada", True)
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub
#End Region

    Protected Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("ReInputDataSurvey.aspx")
    End Sub

    Private Function getAge(ByVal dt As String, ByVal m As Integer) As String
        Dim rtn As String = ""
        Dim dob As DateTime
        dob = New DateTime(dt.Substring(0, 4), dt.Substring(6, 2), dt.Substring(4, 2))
        Dim tday As TimeSpan = DateTime.Now.AddMonths(m).Subtract(dob)
        Dim years As Integer, months As Integer, days As Integer
        months = 12 * (DateTime.Now.AddMonths(m).Year - dob.Year) + (DateTime.Now.AddMonths(m).Month - dob.Month)

        If DateTime.Now.AddMonths(m).Day < dob.Day Then
            months -= 1
            days = DateTime.DaysInMonth(dob.Year, dob.Month) - dob.Day + DateTime.Now.AddMonths(m).Day
        Else
            days = DateTime.Now.AddMonths(m).Day - dob.Day
        End If
        years = Math.Floor(months / 12)
        months -= years * 12

        rtn = years & " tahun, " & months & " bulan and " & days & " hari"
        Return rtn
    End Function

    Sub FillCboEmp(ByVal Table As String, ByVal cboName As DropDownList, ByVal Where As String)
        Dim oAssetData As New Parameter.AssetData
        Dim oData As New DataTable
        oAssetData.strConnection = GetConnectionString()
        oAssetData.Where = Where
        oAssetData.Table = Table
        oAssetData = m_controller2.GetCboEmp(oAssetData)
        oData = oAssetData.ListData
        cboName.DataSource = oData
        cboName.DataTextField = "Name"
        cboName.DataValueField = "ID"
        cboName.DataBind()
        cboName.Items.Insert(0, "Select One")
        cboName.Items(0).Value = "Select One"
    End Sub
End Class