﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SupplierGroupBankAccount.aspx.vb"
    Inherits="Maxiloan.Webform.SalesMarketing.SupplierGroupBankAccount" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcBankAccount" Src="../Webform.UserController/UcBankAccount.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucDateCe" Src="../Webform.UserController/ucDateCe.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>MENAMBAH REKENING BANK GROUP SUPPLIER</title>
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <script language="JavaScript" type="text/javascript">
        function fBack() {
            history.back(-1);
            return false; 
        }
        function DeleteConfirm() {
            if (confirm("Apakah yakin mau hapus data ini ?")) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
    <ContentTemplate>
    <asp:Label ID="lblMessage" runat="server" Visible ="false" ></asp:Label>          
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">               
            <h3>
                MENAMBAH REKENING BANK GROUP SUPPLIER
            </h3>
        </div>
    </div>                 
    <asp:Panel ID="pnlList" runat="server" >
    <div class="form_box_header">
    <div class="form_single">
        <div class="grid_wrapper_ns">
            <asp:DataGrid ID="dtgPaging" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="Sorting" DataKeyField="GroupSupplierAccountID" BorderStyle="None" BorderWidth="0"
                        CssClass="grid_general">
                        <HeaderStyle CssClass="th" />    
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="EDIT">
                                <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" ImageUrl="../Images/iconedit.gif"
                                        CommandName="Edit"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DELETE">
                                <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbDelete" runat="server" CausesValidation="False" ImageUrl="../Images/iconDelete.gif"
                                        CommandName="Delete"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="GroupSupplierAccountBankName" SortExpression="GroupSupplierAccountBankName" HeaderText="NAMA BANK">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="GroupSupplierAccountBankBranch" SortExpression="GroupSupplierAccountBankBranch" HeaderText="NAMA CABANG">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="GroupSupplierAccountNo" SortExpression="GroupSupplierAccountNo" HeaderText="NO REKENING">
                            </asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="DEFAULT" SortExpression="DefaultAccount">                                
                                <ItemTemplate>                                    
                                    <input id="hdncheckDefault" type="hidden" name="hdncheckDefault" runat="server" value =<%# DataBinder.eval(Container,"DataItem.DefaultAccount") %>/>
                                    <asp:CheckBox ID="checkDefault" Enabled="false" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="GroupSupplierAccountName" SortExpression="GroupSupplierAccountName" HeaderText="ATAS NAMA">
                            </asp:BoundColumn>
                        </Columns>
            </asp:DataGrid>                      
            <div class="button_gridnavigation">
	                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../Images/grid_navbutton01.png"
		                CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../Images/grid_navbutton02.png"
		                CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../Images/grid_navbutton03.png"
		                CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../Images/grid_navbutton04.png"
		                CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
	                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
		                EnableViewState="False"></asp:Button>
	                <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage"
		                MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer"
		                CssClass="validator_general"></asp:RangeValidator>
	                <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
		                ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>
            <div class="label_gridnavigation">
	                <asp:Label ID="lblPage" runat="server"></asp:Label>of
	                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
	                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                </div>
        </div>
    </div>
    </div>
    <div class="form_button">
        <asp:Button ID="ButtonAdd" runat="server"  Text="Add" CssClass ="small button blue" CausesValidation="False">
        </asp:Button>
        <asp:Button ID="ButtonPrint" runat="server" Enabled="true"  Text="Print" CssClass ="small button blue">
        </asp:Button>
    </div>
    <div class="form_box_title">
        <div class="form_single">               
            <h4>
                CARI GROUP SUPPLIER
            </h4>
        </div>
    </div> 
    <div class="form_box">
	    <div class="form_single">
            <label>Nama Supplier</label>
            <asp:Label ID="lblCariNmSupplier" runat="server"></asp:Label>
	    </div>
    </div>
    <div class="form_box">
	    <div class="form_single">
            <label>Cari Berdasarkan</label>
            <asp:DropDownList ID="cboSearch" runat="server">
                <asp:ListItem Value="GroupSupplierAccountBankName">NAMA BANK</asp:ListItem>
                <asp:ListItem Value="GroupSupplierAccountBankBranch">NAMA CABANG</asp:ListItem>
                <asp:ListItem Value="GroupSupplierAccountNo">NO REKENING</asp:ListItem>
                <asp:ListItem Value="DefaultAccount">DEFAULT</asp:ListItem>
                <asp:ListItem Value="GroupSupplierAccountName">ATAS NAMA</asp:ListItem>
            </asp:DropDownList>
            <asp:TextBox ID="txtSearch" runat="server" Width="20%"  MaxLength="100"></asp:TextBox>
	    </div>
    </div>   
    <div class="form_button">
        <asp:Button ID="ButtonSearch" runat="server" CausesValidation="False"  Text="Search" CssClass ="small button blue">
        </asp:Button>&nbsp;
        <asp:Button ID="ButtonReset" runat="server" CausesValidation="False"  Text="Reset" CssClass ="small button gray">
        </asp:Button>
        <asp:Button ID="ButtonBackMenu" runat="server" CausesValidation="False"  Text="Back" CssClass ="small button gray">
        </asp:Button>
    </div>                       
    </asp:Panel>
    <asp:Panel ID="pnlAddEdit" runat="server">
    <div class="form_box_header">
        <div class="form_single">               
            <h4>
                <asp:Label ID="lblTitleAddEdit" runat="server"></asp:Label>
            </h4>
        </div>
    </div> 
    <div class="form_box_header">
        <div class="form_single">               
            <h4>
                REKENING BANK 
            </h4>
        </div>
    </div> 
    <div class="form_box_uc">
            <uc1:UcBankAccount id="UcBankAccount" runat="server"></uc1:UcBankAccount>	    
    </div>
    <div class="form_box">
	    <div class="form_single">
            <label>Rekening Default untuk Pembayaran</label>
            <asp:CheckBox ID="checkDefault" runat="server" />
            <asp:CustomValidator ID="CVcheckDefault" runat="server"  CssClass="validator_general"
            ErrorMessage="Hanya bisa pilih satu Rekening Default" Display="Dynamic"></asp:CustomValidator>
	    </div>
    </div> 
    <div class="form_box">
	    <div class="form_single">
            <label class ="label_req">
			Untuk Pembayaran</label>
            <asp:TextBox ID="txtForPayment" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*" CssClass="validator_general"
            ControlToValidate="txtForPayment"></asp:RequiredFieldValidator>
	    </div>
    </div> 
    <div class="form_box">
	    <div class="form_single">
             <label class ="label_req">
			 Efektif Tanggal</label>
            <%--<asp:TextBox ID="EffectiveDate" runat="server"></asp:TextBox>                   
            <asp:CalendarExtender ID="EffectiveDate_CalendarExtender" runat="server" 
                Enabled="True" TargetControlID="EffectiveDate" Format ="dd/MM/yyyy"></asp:CalendarExtender>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*" CssClass="validator_general"
            ControlToValidate="EffectiveDate"></asp:RequiredFieldValidator>--%>
            <uc1:ucdatece id="EffectiveDate" runat="server"></uc1:ucdatece>
	    </div>
    </div> 
    <div class="form_button">
        <asp:Button ID="ButtonSave" runat="server" CausesValidation="true"  Text="Save" CssClass ="small button blue">
        </asp:Button>&nbsp;
        <asp:Button ID="ButtonCancel" runat="server" CausesValidation="true"  Text="Cancel" CssClass ="small button gray">
        </asp:Button>&nbsp;
        <asp:Button ID="ButtonBack" runat="server" CausesValidation="true"  Text="Back" CssClass ="small button gray">
        </asp:Button>&nbsp;
        <asp:Button ID="ButtonClose" runat="server" CausesValidation="False"  Text="Close" CssClass ="small button gray">
        </asp:Button>
    </div> 
    </asp:Panel>
    <input id="hdnSupplierGroupID" type="hidden" name="hdnSupplierGroupID" runat="server" />
    <input id="hdnGroupSupplierAccount" type="hidden" name="hdnGroupSupplierAccount" runat="server" />
    </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
