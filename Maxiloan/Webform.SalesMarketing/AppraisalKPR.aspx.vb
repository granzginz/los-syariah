﻿Option Strict On
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController

Imports System.Data
Imports System.Data.OleDb
Imports System.IO
Imports System.Data.SqlClient
Imports System.Configuration
Imports System.Web.Services
Imports Maxiloan.Webform.LoanOrg.Modules.CreditProcess
Imports Maxiloan.Webform.LoanOrg

Public Class AppraisalKPR
    Inherits Maxiloan.Webform.WebBased

#Region "Controls"
    Private oAssetDataController As New AssetDataController
    Protected WithEvents ucViewApplication1 As ucViewApplicationKPR
    Protected WithEvents ucViewCustomerDetail1 As ucViewCustomerDetail
    Protected WithEvents ucApplicationTab1 As ucApplicationTabKPR
    Protected WithEvents txtRefundBungaN As ucNumberFormat
    Protected WithEvents txtRefundBunga As ucNumberFormat
    Protected WithEvents txtSubsidiBungaDealer As ucNumberFormat
    Protected WithEvents txtRefundPremiN As ucNumberFormat
    Protected WithEvents txtRefundPremi As ucNumberFormat
    Protected WithEvents txtSubsidiBungaATPM As ucNumberFormat
    Protected WithEvents lblPendapatanPremiN As ucNumberFormat
    Protected WithEvents lblPendapatanPremi As ucNumberFormat
    Protected WithEvents txtSubsidiAngsuran As ucNumberFormat
    Protected WithEvents txtSubsidiAngsuranATPM As ucNumberFormat
    Protected WithEvents txtSubsidiUangMuka As ucNumberFormat
    Protected WithEvents txtRefundLainN As ucNumberFormat
    Protected WithEvents txtRefundLain As ucNumberFormat
    Protected WithEvents txtSubsidiUangMukaATPM As ucNumberFormat
    Protected WithEvents txtDealerDiscount As ucNumberFormat
    Protected WithEvents txtBiayaMaintenance As ucNumberFormat
    Protected WithEvents txtRefundBiayaProvisiN As ucNumberFormat
    Protected WithEvents txtRefundBiayaProvisi As ucNumberFormat
    Protected WithEvents txtSubsidiRiskATPM As ucNumberFormat
    Protected WithEvents txtSubsidiRisk As ucNumberFormat
    Protected WithEvents tglSurvey As ucDateCE
    Protected WithEvents lblPendapatanPremiJKN As ucNumberFormat
    Protected WithEvents txtRefundPremiCPN As ucNumberFormat
    Protected WithEvents txtRefundPremiCP As ucNumberFormat
    Protected WithEvents UcHargaPermTanah As ucNumberFormat
    Protected WithEvents UcNilaiPasarTanah As ucNumberFormat
    Protected WithEvents UcNilaiLukuidasiTanah As ucNumberFormat
    Protected WithEvents UcHargaPermBangunan As ucNumberFormat

    Protected WithEvents UcNilaiPasar As ucNumberFormat

    Protected WithEvents txtNilaiPasarTtl As ucNumberFormat
    Protected WithEvents txtNilaiPermTtl As ucNumberFormat
    Protected WithEvents txtNilaiLikuidasiTtl As ucNumberFormat
    Protected WithEvents txtLuasMTtl As ucNumberFormat
    Protected WithEvents txtTotal As ucNumberFormat
    Protected WithEvents txtHargaInternetTtl As ucNumberFormat
    Protected WithEvents UcNilaiLukuidasiBangunan As ucNumberFormat


    Private noController As New ApplicationDetailTransactionController
    Private appraiseController As New ProspectAssignAppraiserController
#End Region


#Region "Constanta"
    Private Const style As String = "ACCACQ"
    Dim objrow As DataRow
    Dim intLoop As Integer
    Dim myDataTable As DataTable
    Dim myDataColumn As DataColumn
    Dim myDataRow As DataRow
    Dim myDataSet As New DataSet
    Dim RateIRR As Double
    Dim ExcludeAmount As Double
    '-- Income
    Dim AdminFee, SubsidiBunga, IncomePremi, ProvisionFee, OtherFee, UppingBunga As Double
    '-- Cost
    Dim IncentiveDealer, InsuranceRefund, ProvisionRefund, OtherRefund, RefundUppingBunga As Double

    Dim prosResult_controller As New ProspectResultAppraiserController
    Dim m_controller As New FinancialDataController
    Dim Entities As New Parameter.FinancialData
    Dim oApplication As New Parameter.Application
    Dim m_ControllerApp As New ApplicationController

    Private oRefundInsentifController As New RefundInsentifController
    Private oController As New ApplicationDetailTransactionController

    Private TotalInsGrid As Double
    Private TotalProsentase As Double
    Private TotalBungaNett As Decimal
    Private TotalPencairan As Double
    Private TotalBungaNettAmount As Double

#End Region

#Region "Property"
    Property Status() As Boolean
        Get
            Return CType(ViewState("Status"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            ViewState("Status") = Value
        End Set
    End Property
    Property InstallmentAmount() As Double
        Get
            Return CDbl(ViewState("InstallmentAmount"))
        End Get
        Set(ByVal Value As Double)
            ViewState("InstallmentAmount") = Value
        End Set
    End Property

    Property ProspectAppId() As String
        Get
            Return ViewState("ProspectAppId").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ProspectAppId") = Value
        End Set
    End Property
    Property StepUpDownType() As String
        Get
            Return ViewState("StepUpDownType").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("StepUpDownType") = Value
        End Set
    End Property
    Property Supplier() As String
        Get
            Return ViewState("Supplier").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("Supplier") = Value
        End Set
    End Property
    Property CustomerID() As String
        Get
            Return ViewState("CustomerID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property
    Property CustName() As String
        Get
            Return ViewState("CustName").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("CustName") = Value
        End Set
    End Property
    Property InterestType() As String
        Get
            Return ViewState("InterestType").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("InterestType") = Value
        End Set
    End Property
    Property InstallmentScheme() As String
        Get
            Return ViewState("InstallmentScheme").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("InstallmentScheme") = Value
        End Set
    End Property
    Property IsIncentiveSupplier() As Boolean
        Get
            Return CType(ViewState("IsIncentiveSupplier"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            ViewState("IsIncentiveSupplier") = Value
        End Set
    End Property
    Property RefundInterest As Double
        Get
            Return CType(ViewState("RefundInterest"), Double)
        End Get
        Set(ByVal value As Double)
            ViewState("RefundInterest") = value
        End Set
    End Property
    Property Provisi As Double
        Get
            Return CType(ViewState("Provisi"), Double)
        End Get
        Set(ByVal value As Double)
            ViewState("Provisi") = value
        End Set
    End Property
    Private Property dtDistribusiNilaiInsentif As DataTable
        Get
            Return CType(ViewState("dtDistribusiNilaiInsentif"), DataTable)
        End Get
        Set(ByVal value As DataTable)
            ViewState("dtDistribusiNilaiInsentif") = value
        End Set
    End Property
    Private Property ApplicationDetailTransactionDT As DataTable
        Get
            Return CType(ViewState("ApplicationDetailTransactionDT"), DataTable)
        End Get
        Set(ByVal value As DataTable)
            ViewState("ApplicationDetailTransactionDT") = value
        End Set
    End Property
    Private Property SupplierID() As String
        Get
            Return CType(ViewState("SupplierID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("SupplierID") = Value
        End Set
    End Property
    Private Property NilaiTransaksi() As Decimal
        Get
            Return CType(ViewState("NilaiTransaksi"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            ViewState("NilaiTransaksi") = Value
        End Set
    End Property
    Private Property EffectiveRate() As Decimal
        Get
            Return CType(ViewState("EffectiveRate"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            ViewState("EffectiveRate") = Value
        End Set
    End Property
    Private Property FlatRate() As Decimal
        Get
            Return CType(ViewState("FlatRate"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            ViewState("FlatRate") = Value
        End Set
    End Property

    Property RefundBungaAmount As Double
        Get
            Return CType(ViewState("RefundBungaAmount"), Double)
        End Get
        Set(ByVal value As Double)
            ViewState("RefundBungaAmount") = value
        End Set
    End Property

    Public Property Balon() As DataTable
        Get
            Return CType(ViewState("Balon"), DataTable)
        End Get
        Set(ByVal value As DataTable)
            ViewState("Balon") = value
        End Set
    End Property


    Public Property FirstInstallment As String
        Get
            Return CStr(ViewState("FirstInstallment"))
        End Get
        Set(value As String)
            ViewState("FirstInstallment") = value
        End Set
    End Property

    Public Property Tenor As Integer
        Get
            Return CInt(ViewState("Tenor"))
        End Get
        Set(value As Integer)
            ViewState("Tenor") = value
        End Set
    End Property

    Public Property OtrNtf() As String
        Get
            Return CStr(ViewState("OtrNtf"))
        End Get
        Set(value As String)
            ViewState("OtrNtf") = value
        End Set
    End Property

    Public Property ntf() As Double
        Get
            Return CDbl(ViewState("NTF"))
        End Get
        Set(value As Double)
            ViewState("NTF") = value
        End Set
    End Property

#End Region


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not Page.IsPostBack Then
            Session.Remove("balon")
            Me.ProspectAppId = Request("id")

            Me.EffectiveRate = 0

            Dim WhereBranch As String = "BranchID = '" & Replace(Me.sesBranchId, "'", "") & "'"
            FillCboEmp("BranchEmployee", DropAppriser, WhereBranch + " and (EmployeePosition='SVY' or EmployeePosition='AO') ")
            FillCbo_(DropPerusahaanAppraisal)
            initObjects()

            switchDropAppraisal()
            LoadData(Me.ProspectAppId)
        End If
    End Sub

    Private Sub LoadData(ByVal prosID As String)
        Dim oAssetData As New Parameter.ProspectAssignAppraiser
        Dim oData As New DataTable
        Dim oRow As DataRow
        oAssetData.strConnection = GetConnectionString()
        oAssetData.BranchId = Replace(Me.sesBranchId, "'", "")
        oAssetData.ProspectAppID = prosID
        oAssetData = appraiseController.GetDataAppraisal(oAssetData)

        If oAssetData IsNot Nothing Then
            oData = oAssetData.listdata

        End If

        If oData.Rows.Count > 0 Then
            oRow = oData.Rows(0)

            DropAppraisal.SelectedIndex = DropAppraisal.Items.IndexOf(DropAppraisal.Items.FindByValue(CStr(oRow.Item("SelectedAppraisal"))))
            DropPerusahaanAppraisal.SelectedIndex = DropPerusahaanAppraisal.Items.IndexOf(DropPerusahaanAppraisal.Items.FindByValue(CStr(oRow.Item("PerusahaanAppraisal"))))
            txtAppraisal.Text = oRow.Item("Appraisal").ToString.Trim
            DropAppriser.SelectedIndex = DropAppriser.Items.IndexOf(DropAppriser.Items.FindByValue(CStr(oRow.Item("Appriser"))))
            tglSurvey.Text = CDate(oRow.Item("TangggalSurvey").ToString.Trim).ToString("dd/MM/yyyy")
            txtJam.Text = oRow.Item("jam").ToString.Trim

            lblLuasTanah.Text = oRow.Item("LuastanahApplikasi").ToString.Trim

            lblPXLTanah.Text = oRow.Item("PanjangXLebarTanahApplikasi").ToString.Trim
            TxtLuasTanahAppraisal.Text = oRow.Item("LuasTanahAppraisal").ToString.Trim
            txtPanjangXLebar.Text = oRow.Item("PanjangXLebarTanah").ToString.Trim
            UcHargaPermTanah.Text = oRow.Item("HargaPerMTanah").ToString.Trim
            UcNilaiPasarTanah.Text = oRow.Item("NilaiPasarTanah").ToString.Trim
            txtLukuidasi.Text = oRow.Item("LikuidasiTanah").ToString.Trim
            UcNilaiLukuidasiTanah.Text = oRow.Item("NilaiLikuidasiTanah").ToString.Trim
            txtNamaContactPerson.Text = oRow.Item("NamaContact").ToString.Trim
            txtHubunganContact.Text = oRow.Item("HubunganContact").ToString.Trim
            txtNoTelepon.Text = oRow.Item("NoTelp").ToString.Trim
            txtNoHP.Text = oRow.Item("NoHP").ToString.Trim
            lblLuasBangunan.Text = oRow.Item("LuasBangunanApplikasi").ToString.Trim

            lblPXLBangunan.Text = oRow.Item("PanjangXLebarBangunanApplikasi").ToString.Trim
            txtLuasBangunan.Text = oRow.Item("LuasBangunan").ToString.Trim
            txtPanjangXLebarBangunan.Text = oRow.Item("PanjangXLebarBangunan").ToString.Trim
            txtLantai.Text = oRow.Item("Lantai").ToString.Trim
            UcHargaPermBangunan.Text = oRow.Item("HargaPermBangunan").ToString.Trim
            txtLukuidasiBangunan.Text = oRow.Item("NilaiLikuidasiBangunan").ToString.Trim
            txtNilaiPasarTtl.Text = oRow.Item("NilaiPasarSurvey").ToString.Trim '
            txtNilaiPermTtl.Text = oRow.Item("NilaiPermSurvey").ToString.Trim
            txtLuasMTtl.Text = oRow.Item("LuasmSurvey").ToString.Trim
            txtSumberPerbandingan.Text = oRow.Item("SumberPembandingan").ToString.Trim
            txtHargaInternetTtl.Text = oRow.Item("HargaInternetSurvey").ToString.Trim
            txtSumbardataInternet.Text = oRow.Item("SumberdataInternet").ToString.Trim
            txtTotal.Text = oRow.Item("TotalSurvey").ToString.Trim
            txtKeterangan.Text = oRow.Item("Keterangan").ToString.Trim

            UcNilaiPasar.Text = oRow.Item("NilaiPasarBangunan").ToString.Trim
            txtNilaiLikuidasiTtl.Text = oRow.Item("NilaiLikuidasiSurvey").ToString.Trim
            UcNilaiLukuidasiBangunan.Text = oRow.Item("TotalNilaiLikuidasiBangunan").ToString.Trim
        End If

    End Sub

    Protected Sub FillCbo_(cboReference As DropDownList)
        Dim dtEntity As DataTable = Nothing
        Dim oCustomer As New Parameter.FinancialData
        oCustomer.strConnection = GetConnectionString()
        oCustomer.Table = "AppraisalCompany"
        oCustomer = m_controller.GetDataDrop(oCustomer)
        If Not oCustomer Is Nothing Then
            dtEntity = oCustomer.ListData
        End If

        cboReference.DataSource = dtEntity.DefaultView
        cboReference.DataTextField = "Description"
        cboReference.DataValueField = "ID"
        cboReference.DataBind()
        cboReference.Items.Insert(0, "Select One")
        cboReference.Items(0).Value = "Select One"

    End Sub
    Protected Sub FillCboEmp(ByVal Table As String, ByVal cboName As DropDownList, ByVal Where As String)
        Dim oAssetData As New Parameter.AssetData
        Dim oData As New DataTable

        oAssetData.strConnection = GetConnectionString()
        oAssetData.Where = Where
        oAssetData.Table = Table
        oAssetData = oAssetDataController.GetCboEmp(oAssetData)

        oData = oAssetData.ListData
        cboName.DataSource = oData
        cboName.DataTextField = "Name"
        cboName.DataValueField = "ID"
        cboName.DataBind()
        cboName.Items.Insert(0, "Select One")
        cboName.Items(0).Value = "Select One"
    End Sub


#Region "Load Financial Data"

    Private Sub CalculateTotalBayarPertama()
        Dim um, ba, bf, at, ap, ot, bp, bpr As Double


    End Sub

    Private Sub calculateNPV()
        Dim ntf As Double
        Dim refundRate As Decimal
        Dim flatRate As Decimal
        Dim TenorYear As Integer



        Me.RefundBungaAmount = CreditProcess.getNPV(ntf, refundRate, flatRate, TenorYear)
        txtRefundBungaN.Text = FormatNumber(Me.RefundBungaAmount, 0)
    End Sub


#End Region


#Region "Save"

    Private Function IsValidValidation(ByVal textbox As TextBox, ByVal CompareText As ucNumberFormat, ByVal Message As String) As Boolean
        If textbox.Text.Trim = "" Then
            ShowMessage(lblMessage, Message & " Harap diisi", True)
            Return False
        ElseIf CDbl(textbox.Text) = 0 Then
            ShowMessage(lblMessage, Message & " Harap isi > 0 ", True)
            Return False
        ElseIf CDbl(textbox.Text) > CDbl(CompareText.Text) Then
            ShowMessage(lblMessage, Message & " Harap isi < Jangka Waktu Angsuran", True)
            Return False
        End If
        Return True
    End Function

#End Region

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim ofinancialData As New Parameter.ProspectResultAppraiser
        Try

            With ofinancialData
                .strConnection = GetConnectionString()
                .BusinessDate = Me.BusinessDate
                .BranchId = Replace(Me.sesBranchId, "'", "")
            .ProspectAppID = Me.ProspectAppId
                .PerusahaanAppraisal = DropPerusahaanAppraisal.SelectedValue
                .Appraisal = txtAppraisal.Text
                .Appriser = DropAppriser.SelectedValue
                .TangggalSurvey = ConvertDate2(tglSurvey.Text)
                .jam = txtJam.Text
                .SelectedAppraisal = DropAppraisal.SelectedValue


                If lblLuasTanah.Text = "" Then
                    .LuastanahApplikasi = 0
                Else
                    .LuastanahApplikasi = CInt(lblLuasTanah.Text)
                End If

                .PanjangXLebarTanahApplikasi = lblPXLTanah.Text
                .LuasTanahAppraisal = CInt(IIf(String.IsNullOrEmpty(TxtLuasTanahAppraisal.Text), 0, TxtLuasTanahAppraisal.Text))
                .PanjangXLebarTanah = txtPanjangXLebar.Text
                .HargaPerMTanah = CDec(IIf(String.IsNullOrEmpty(UcHargaPermTanah.Text), 0, UcHargaPermTanah.Text))
                .NilaiPasarTanah = CDec(IIf(String.IsNullOrEmpty(UcNilaiPasarTanah.Text), 0, UcNilaiPasarTanah.Text))
                .LikuidasiTanah = CInt(IIf(String.IsNullOrEmpty(txtLukuidasi.Text), 0, txtLukuidasi.Text))
                .NilaiLikuidasiTanah = CDec(IIf(String.IsNullOrEmpty(UcNilaiLukuidasiTanah.Text), 0, UcNilaiLukuidasiTanah.Text))
                .NamaContact = txtNamaContactPerson.Text
                .HubunganContact = txtHubunganContact.Text
                .NoTelp = txtNoTelepon.Text
                .NoHP = txtNoHP.Text
                If lblLuasBangunan.Text = "" Then
                    .LuasBangunanApplikasi = "0"
                Else
                    .LuasBangunanApplikasi = lblLuasBangunan.Text
                End If
                If lblLuasBangunan.Text = "" Then
                    .PanjangXLebarBangunanApplikasi = ""
                Else
                    .PanjangXLebarBangunanApplikasi = lblPXLBangunan.Text
                End If

                .LuasBangunan = CInt(IIf(String.IsNullOrEmpty(txtLuasBangunan.Text), 0, txtLuasBangunan.Text))
                .PanjangXLebarBangunan = txtPanjangXLebarBangunan.Text
                .Lantai = CInt(IIf(String.IsNullOrEmpty(txtLantai.Text), 0, txtLantai.Text))
                .HargaPermBangunan = CDec(IIf(String.IsNullOrEmpty(UcHargaPermBangunan.Text), 0, UcHargaPermBangunan.Text))
                .NilaiPasarBangunan = CDec(IIf(String.IsNullOrEmpty(UcNilaiPasar.Text), 0, UcNilaiPasar.Text))
                .NilaiLikuidasiBangunan = CDec(IIf(String.IsNullOrEmpty(txtLukuidasiBangunan.Text), 0, txtLukuidasiBangunan.Text))
                .NilaiPasarTtl = CDec(IIf(String.IsNullOrEmpty(txtNilaiPasarTtl.Text), 0, txtNilaiPasarTtl.Text))
                .NilaiPermTtl = CDec(IIf(String.IsNullOrEmpty(txtNilaiPermTtl.Text), 0, txtNilaiPermTtl.Text))
                .NilaiLikuidasiTtl = CDec(IIf(String.IsNullOrEmpty(txtNilaiLikuidasiTtl.Text), 0, txtNilaiLikuidasiTtl.Text))
                .LuasMTtl = CDec(IIf(String.IsNullOrEmpty(txtLuasMTtl.Text), 0, txtLuasMTtl.Text))
                .SumberPerbandingan = txtSumberPerbandingan.Text
                .HargaInternetTtl = CDec(IIf(String.IsNullOrEmpty(txtHargaInternetTtl.Text), 0, txtHargaInternetTtl.Text))
                .SumbardataInternet = txtSumbardataInternet.Text
                .TotalSurvey = CDec(IIf(String.IsNullOrEmpty(txtTotal.Text), 0, txtTotal.Text))
                .Keterangan = txtKeterangan.Text
                .TotalNilaiLikuidasiBangunan = CDec(IIf(String.IsNullOrEmpty(UcNilaiLukuidasiBangunan.Text), 0, UcNilaiLukuidasiBangunan.Text))
            End With

            ofinancialData = prosResult_controller.SaveProspectAssignAppraisal(ofinancialData)

            ShowMessage(lblMessage, "Data saved!", False)

        Catch ex As Exception
            ShowMessage(lblMessage, "error - " & ex.Message, True)
        End Try

    End Sub

    '#Region "SendCookies"
    '    Sub SendCookies()
    '        Dim cookie As New HttpCookie("Incentive")
    '        cookie.Values.Add("ApplicationID", Me.ApplicationID)
    '        cookie.Values.Add("CustomerID", Me.CustomerID)
    '        cookie.Values.Add("CustomerName", Me.CustName)

    '        Response.AppendCookie(cookie)
    '    End Sub
    '#End Region

    Protected Sub initObjects()


        lblMessage.Text = ""
        lblMessage.Visible = False

    End Sub

#Region "incentive"



    Private Function validateinsentif() As String
        Dim errmsg As String = String.Empty
        'loop rincian transaksi get each transaction total

        For i As Integer = 0 To ApplicationDetailTransactionDT.Rows.Count - 1
            If CBool(ApplicationDetailTransactionDT.Rows(i)("isDistributable")) = False Then Continue For
            Dim transAmount As Decimal = CDec(ApplicationDetailTransactionDT.Rows(i)("txtTransAmount"))
            Dim transID As String = ApplicationDetailTransactionDT.Rows(i)("TransID").ToString

            'errmsg = validateInsentifPerTransaction(transID, transAmount)
            If errmsg <> String.Empty Then Return errmsg
        Next


        Return String.Empty
    End Function

    Private Function validateInsentifPerTransaction(ByVal transID As String, ByVal transAmount As Decimal) As String
        Dim dblTotalInsentifSupplierEmployee As Double

        For i As Integer = 0 To Me.dtDistribusiNilaiInsentif.Rows.Count - 1
            If dtDistribusiNilaiInsentif.Rows(i).Item("transId").ToString.Trim.ToLower = transID.Trim.ToLower Then
                dblTotalInsentifSupplierEmployee += CDbl(Me.dtDistribusiNilaiInsentif.Rows(i).Item("IncentiveForRecepient"))
                TotalInsGrid = TotalInsGrid + dblTotalInsentifSupplierEmployee
            End If

        Next

        If transAmount <> dblTotalInsentifSupplierEmployee Then
            Return "Total distribusi insentif " & transID & "tidak sama dengan nilai insentif!"
        End If

        Return String.Empty
    End Function
#End Region

    Public Sub switchDropAppraisal()
        If DropAppraisal.SelectedValue = "IN" Then
            txtAppraisal.Enabled = False
            DropPerusahaanAppraisal.Enabled = False
            DropAppriser.Enabled = True
        Else
            txtAppraisal.Enabled = True
            DropPerusahaanAppraisal.Enabled = True
            DropAppriser.Enabled = False
        End If
    End Sub

    'Public Sub showPencairan()
    '    Dim oClass As New Parameter.ApplicationDetailTransaction

    '    With oClass
    '        .strConnection = GetConnectionString()
    '        .ApplicationID = Me.ApplicationID
    '    End With

    '    Try
    '        oClass = noController.getBungaNettKPR(oClass)



    '    Catch ex As Exception
    '        ShowMessage(lblMessage, ex.Message, True)
    '    End Try

    'End Sub





    Function getRateDetailTransaksi(TransAmount As Double) As Decimal
        Dim xrate As Decimal
        If Me.FirstInstallment = "AD" Then
            xrate = RateConvertion.GetEffectiveRateAdv(CInt(Tenor), CDbl(Me.InstallmentAmount), TransAmount, 1)
        Else
            xrate = RateConvertion.GetEffectiveRateArr(CInt(Tenor), CDbl(Me.InstallmentAmount), TransAmount, 1)
        End If

        Return Math.Round(Math.Abs(Me.EffectiveRate - xrate), 2)
    End Function




    'Private Sub saveBungaNett()
    '    Try
    '        Dim oBungaNet As New Parameter.FinancialData

    '        With oBungaNet
    '            .strConnection = GetConnectionString()
    '            .ApplicationID = Me.ApplicationID
    '            .FirstInstallment = Me.FirstInstallment
    '            .EffectiveRate = TotalBungaNett / 100
    '            .Tenor = Tenor
    '            .BungaNettEff = TotalBungaNett
    '            .BungaNettFlat = cEffToFlat(oBungaNet)
    '        End With

    '        oBungaNet = m_controller.saveBungaNett(oBungaNet)

    '    Catch ex As Exception
    '        'skip
    '        ShowMessage(lblMessage, ex.Message, True)
    '    End Try
    'End Sub

    'Private Function saveBungaNett() As Boolean
    '    Try
    '        showPencairan()
    '        Dim oBungaNet As New Parameter.FinancialData

    '        With oBungaNet
    '            .strConnection = GetConnectionString()
    '            .ApplicationID = Me.ApplicationID
    '            .FirstInstallment = Me.FirstInstallment
    '            .EffectiveRate = TotalBungaNett / 100
    '            .Tenor = Tenor
    '            .BungaNettEff = TotalBungaNett
    '            .BungaNettFlat = cEffToFlat(oBungaNet)
    '        End With

    '        oBungaNet = m_controller.saveBungaNett(oBungaNet)

    '    Catch ex As Exception
    '        ShowMessage(lblMessage, ex.Message, True)
    '        Return False
    '    End Try
    '    Return True
    'End Function


    'Private Function saveData() As Boolean
    '    Dim oFinancialData As New Parameter.ProspectResultAppraiser
    '    Try
    '        With oFinancialData
    '            .strConnection = GetConnectionString()
    '            .BusinessDate = Me.BusinessDate
    '            .BranchId = Replace(Me.sesBranchId, "'", "")
    '            .ProspectAppID = Me.ProspectAppId

    '            .SubsidiBungaATPM = CDbl(txtSubsidiBungaATPM.Text)
    '            .SubsidiAngsuranATPM = CDbl(txtSubsidiAngsuranATPM.Text)
    '            .SubsidiUangMuka = CDbl(txtSubsidiUangMuka.Text)
    '            .SubsidiUangMukaATPM = CDbl(txtSubsidiUangMukaATPM.Text)
    '            .DealerDiscount = CDbl(txtDealerDiscount.Text)
    '            .BiayaMaintenance = CDbl(txtBiayaMaintenance.Text)
    '            .RefundBungaPercent = CDec(txtRefundBunga.Text)
    '            .RefundBungaAmount = CDbl(txtRefundBungaN.Text)
    '            .RefundPremiPercent = CDec(txtRefundPremi.Text)
    '            .RefundPremiAmount = CDbl(txtRefundPremiN.Text)
    '            .RefundProvisiPercent = CDec(txtRefundBiayaProvisi.Text)
    '            .RefundProvisiAmount = CDbl(txtRefundBiayaProvisiN.Text)
    '            .AlokasiInsentifBiayaLain = CDec(txtRefundLainN.Text)
    '            .AlokasiInsentifBiayaLainPercent = CDbl(txtRefundLain.Text)
    '            .SubsidiBungaDealer = CDbl(txtSubsidiBungaDealer.Text)
    '            .SubsidiAngsuran = CDbl(txtSubsidiAngsuran.Text)

    '            .RateIRR = RateIRR
    '            .ExcludeAmount = ExcludeAmount


    '            .AdminFee = AdminFee
    '            .ProvisionFee = ProvisionFee
    '            .SubsidiBunga = SubsidiBunga
    '            .IncomePremi = IncomePremi
    '            .OtherFee = OtherFee
    '            .IncentiveDealer = IncentiveDealer
    '            .InsuranceRefund = InsuranceRefund
    '            .ProvisionRefund = ProvisionRefund
    '            .OtherRefund = OtherRefund

    '            .SubsidiRisk = CDbl(txtSubsidiRisk.Text)
    '            .SubsidiRiskATPM = CDbl(txtSubsidiRiskATPM.Text)
    '            .RefundCreditProtectionPercent = CDec(txtRefundPremiCP.Text)
    '            .RefundCreditProtectionAmount = CDbl(txtRefundPremiCPN.Text)

    '        End With

    '        oFinancialData = m_controller.SaveFinancialDataTab2(oFinancialData)

    '    Catch ex As Exception
    '        ShowMessage(lblMessage, "error - " & ex.Message, True)
    '        Return False
    '    End Try
    '    Return True

    'End Function



End Class