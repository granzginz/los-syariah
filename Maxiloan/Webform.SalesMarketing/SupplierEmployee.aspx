﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SupplierEmployee.aspx.vb"
    Inherits="Maxiloan.Webform.SalesMarketing.SupplierEmployee" %>

<%@ Register TagPrefix="uc1" TagName="UcBankAccount" Src="../Webform.UserController/UcBankAccount.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UCMasterTransaksi" Src="../Webform.UserController/UCMasterTransaksi.ascx" %>
<%@ Register TagPrefix="uc2" TagName="ucNumberFormat" Src="../Webform.UserController/ucNumberFormat.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>SupplierEmployee</title>
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />   
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                DAFTAR KARYAWAN SUPPLIER
            </h3>
        </div>
    </div>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgPaging" runat="server" Width="100%" AllowSorting="True" DataKeyField="SupplierEmployeeID"
                        AutoGenerateColumns="False" OnSortCommand="Sorting" BorderStyle="None" BorderWidth="0"
                        CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="EDIT">
                                <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" ImageUrl="../Images/iconedit.gif"
                                        CommandName="Edit"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DELETE">
                                <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemTemplate>

                                    <asp:ImageButton ID="imgDelete" runat="server" CausesValidation="False" ImageUrl="../Images/icondelete.gif"
                                        CommandName="Delete"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="ID" Visible="false">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkSupplierEmployeeID" runat="server" Text='<%# DataBinder.eval(Container,"DataItem.SupplierEmployeeID") %>'
                                        CommandName="View">
                                    </asp:LinkButton>
                                    <asp:Label ID="lblBankBranchId" runat="server" Visible="False" Text='<%# DataBinder.eval(Container,"DataItem.SupplierEmployeeBankBranchId") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="NAMA" SortExpression="SupplierEmployeeName">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lblSupplierEmployeeName" CausesValidation="False" CommandName="View"
                                        runat="server" Text='<%# DataBinder.eval(Container,"DataItem.SupplierEmployeeName") %>'>
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="SupplierEmployeePosition" SortExpression="SupplierEmployeePosition"
                                HeaderText="JABATAN"></asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="SupplierEmployeeBankID"></asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="SupplierEmployeeBankBranch"></asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="SupplierEmployeeAccountNo"></asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="SupplierEmployeeAccountName"></asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="IsActive"></asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="TransID"></asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="NilaiAlokasi"></asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="PersenAlokasi"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="MASTER TRANSAKSI" Visible="false" >
                                <ItemTemplate>
                                    <asp:LinkButton ID="lblEmployeeMasterTransaksi" runat="server" CommandName="EmployeeMasterTransaksi"
                                        CausesValidation="False">Master Transaksi</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Master Transaksi" Visible="false" SortExpression="TransID">
                                <ItemTemplate>
                                    <asp:Label ID="lblTransID" CausesValidation="False" runat="server" Text='<%# DataBinder.eval(Container,"DataItem.TransID") %>'></asp:Label>
                                    <asp:Label ID="lblTransIDheader" Visible="false" CausesValidation="False" runat="server"
                                        Text='<%# DataBinder.eval(Container,"DataItem.TransID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Persentase" Visible="false" SortExpression="PersenAlokasi"
                                HeaderStyle-CssClass="th_right" ItemStyle-CssClass="item_grid_right">
                                <ItemTemplate>
                                    <asp:Label ID="lblPersenAlokasi" runat="server" Text='<%# DataBinder.eval(Container,"DataItem.PersenAlokasi") %>'></asp:Label>
                                    <asp:Label ID="lblPersenAlokasiheader" Visible="false" runat="server" Text='<%# DataBinder.eval(Container,"DataItem.PersenAlokasi") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Nilai" Visible="false" SortExpression=" NilaiAlokasi"
                                HeaderStyle-CssClass="th_right" ItemStyle-CssClass="item_grid_right">
                                <ItemTemplate>
                                    <asp:Label ID="lblNilaiAlokasi" runat="server" Text='<%# formatnumber(DataBinder.eval(Container,"DataItem.NilaiAlokasi"),0) %>'></asp:Label>
                                    <asp:Label ID="lblNilaiAlokasiheader" Visible="false" runat="server" Text='<%# DataBinder.eval(Container,"DataItem.NilaiAlokasi") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="SupplierID" Visible="false">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkSupplierID" runat="server" Text='<%# DataBinder.eval(Container,"DataItem.SupplierID") %>'>
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="SupplierEmployeeAccountName" Visible="false">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkSupplierEmployeeAccountName" runat="server" Text='<%# DataBinder.eval(Container,"DataItem.SupplierEmployeeAccountName") %>'>
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="SupplierEmployeePositionID" Visible="false">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkSupplierEmployeePositionID" runat="server" Text='<%# DataBinder.eval(Container,"DataItem.SupplierEmployeePositionID") %>'>
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="NPWP" SortExpression="NPWP" Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblNPWP" runat="server" Text='<%# DataBinder.eval(Container,"DataItem.NPWP") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>

                            <asp:TemplateColumn Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblIDNumber" runat="server" Text='<%# DataBinder.eval(Container,"DataItem.IDNumber") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="ALAMAT NPWP">
                                <ItemTemplate>
                                    <asp:Label ID="lblAlamatNPWP" runat="server" Visible="False" Text='<%# DataBinder.eval(Container,"DataItem.AlamatNPWP") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                    <div class="button_gridnavigation">
                        <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../Images/grid_navbutton01.png"
                            CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../Images/grid_navbutton02.png"
                            CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../Images/grid_navbutton03.png"
                            CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../Images/grid_navbutton04.png"
                            CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                        <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                            EnableViewState="False"></asp:Button>
                        <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" MinimumValue="1"
                            ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                            ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                    <div class="label_gridnavigation">
                        <asp:Label ID="lblPage" runat="server"></asp:Label>of
                        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                        <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                    </div>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonAdd" runat="server" Text="Add" CssClass="small button blue"
                CausesValidation="False"></asp:Button>
           
            <asp:Button ID="ButtonBack" runat="server" CausesValidation="False" Text="Back" CssClass="small button gray">
            </asp:Button>
        </div>
        <div class="form_box_title">
            <div class="form_single">
                <h4>
                    CARI KARYAWAN SUPPLIER
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Nama Karyawan</label>
                <asp:HyperLink ID="lblName" runat="server" NavigateUrl=""></asp:HyperLink>
                <asp:LinkButton ID="lnkID" runat="server" CausesValidation="False" Visible="False"></asp:LinkButton>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cari Berdasarkan</label>
                <asp:DropDownList ID="cboSearch" runat="server">
                    <asp:ListItem Value="SupplierEmployeeName">Nama Karyawan</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Jabatan</label>
                <asp:DropDownList ID="cboPosition" runat="server"></asp:DropDownList>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonSearch" runat="server" CausesValidation="False" Text="Search"
                CssClass="small button blue"></asp:Button>
            <asp:Button ID="ButtonReset" runat="server" CausesValidation="False" Text="Reset"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlAddEdit" runat="server">
        <div class="form_box_header">
            <div class="form_single">
                <h4>
                    KARYAWAN SUPPLIER -
                    <asp:Label ID="lblTitle" runat="server"></asp:Label>
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    Nama Karyawan
                </label>
                <asp:TextBox ID="txtEmpName" runat="server" MaxLength="50"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" ErrorMessage="Harap isi Nama Karyawan"
                    CssClass="validator_general" ControlToValidate="txtEmpName" Display="Dynamic"></asp:RequiredFieldValidator>
                <asp:Label ID="lblEmployeeID" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Jabatan
                </label>
                <asp:DropDownList ID="cboEmpPosition" runat="server">              
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Status</label>
                <asp:CheckBox ID="chkActive" runat="server" Text="Active"></asp:CheckBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    No KTP
                </label>
                <asp:TextBox ID="txtNoKTP" runat="server" MaxLength="50"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtNoKTP"
                    ErrorMessage="Harap isi dengan Angka" ValidationExpression="\d*" CssClass="validator_general"></asp:RegularExpressionValidator>
                 <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtNoKTP"
                ErrorMessage="Harap isi KTP" Display="Dynamic" CssClass ="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <%--edit npwp, ktp, telepon by ario--%>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    No NPWP
                </label>
                <asp:TextBox ID="txtNoNPWP" runat="server" MaxLength="50"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidatornpwp" runat="server"
                    ControlToValidate="txtNoNPWP" ErrorMessage="Harap isi dengan Angka" ValidationExpression="\d*"
                    CssClass="validator_general"></asp:RegularExpressionValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtNoNPWP"
                ErrorMessage="Harap isi NPWP" Display="Dynamic" CssClass ="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Alamat NPWP
                </label>
                <asp:TextBox ID="txtAlamatNPWP" runat="server" TextMode="MultiLine" CssClass="multiline_textbox"></asp:TextBox>
            </div>
        </div>
        <%--<div class="form_box">
                <div class="form_single">
                    <label class="label_general">
                       Status Penjualan</label>
                    <asp:RadioButtonList ID="rboStatus" runat="server" class="opt_single" RepeatDirection="Horizontal">
                        <asp:ListItem Value="N" Selected="True">Normal</asp:ListItem>
                        <asp:ListItem Value="U">Warning</asp:ListItem>
                        <asp:ListItem Value="M">Bad</asp:ListItem>
                    </asp:RadioButtonList>
                </div>
            </div>--%>
             <div class="form_box">
                    <div class="form_single">
                        <label class="label_general">
                            Status Penjualan</label>
                        <asp:RadioButtonList  ID="rboBadStatus" runat="server" class="opt_single"
                            RepeatDirection="Horizontal">
                            <asp:ListItem Value="N" Selected="True">Normal</asp:ListItem>
                            <asp:ListItem Value="W">Warning</asp:ListItem>
                            <asp:ListItem Value="B">Bad</asp:ListItem>
                        </asp:RadioButtonList>
                        <asp:TextBox ID="txtBadStatusNormal" Enabled="false" Visible="false" runat="server"
                            MaxLength="20" Columns="25" Text="Normal"></asp:TextBox>
                    </div>
                </div>
        <div class="form_box_header">
            <div class="form_single">
                <h4>
                    Rekening Bank
                </h4>
            </div>
        </div>
        <div class="form_box_uc">
            <uc1:ucbankaccount id="UcBankAccount" runat="server"></uc1:ucbankaccount>
        </div>
        <div class="form_box_hide">
            <div class="form_single">
                <h4>
                    Transaksi
                </h4>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonSave" runat="server" Text="Save" CausesValidation="true" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="ButtonCancel" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlView" runat="server">
        <div class="form_box_header">
            <div class="form_single">
                <h4>
                    VIEW - KARYAWAN SUPPLIER
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Nama Karyawan</label>
                <asp:Label ID="lblEmpName" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Jabatan</label>
                <asp:Label ID="lblPosition" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Status</label>
                <asp:Label ID="lblActive" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <h4>
                    Rekening Bank
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Nama Bank</label>
                <asp:Label ID="lblBankName" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Nama Cabang</label>
                <asp:Label ID="lblBranch" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    No Rekening</label>
                <asp:Label ID="lblANo" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Nama Rekening</label>
                <asp:Label ID="lblAName" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonClose" runat="server" Text="Close" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
