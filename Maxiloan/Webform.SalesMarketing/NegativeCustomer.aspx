﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="NegativeCustomer.aspx.vb"
    Inherits="Maxiloan.Webform.SalesMarketing.NegativeCustomer" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ucCompanyAddress" Src="../Webform.UserController/ucCompanyAddress.ascx" %>
<%@ Register Src="../Webform.UserController/ucDateCE.ascx" TagName="ucDateCE" TagPrefix="uc2" %>
<%@ Register Src="../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %>  
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>NegativeCustomer</title>
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <link href="../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;
        function OpenWinNegativeCust(pID) {
            window.open('NegativeCustomerView.aspx?NegativeCustomerID=' + pID + '', 'UserLookup', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }
          
         $(document).ready(function () {
             $('#Btn_Upload').click(function () {
                 if ($('#files').val() == "") {  return false; }
                 return true; 
              });
         });
    </script>
    
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
   
            <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>
                        NEGATIVE CUSTOMER
                    </h3>
                </div>
            </div>
            <asp:Panel ID="pnlList" runat="server">
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtgList" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                                DataKeyField="CustomerName" BorderStyle="None" BorderWidth="0" OnSortCommand="SortGrid"
                                CssClass="grid_general">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="EDIT">
                                        <ItemStyle CssClass="command_col"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../Images/iconedit.gif"
                                                CommandName="Edit"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="NegativeCustomerID" Visible="False">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </asp:BoundColumn>
                                    <asp:TemplateColumn SortExpression="CUSTOMERNAME" HeaderText="NAMA">
                                        <ItemTemplate>
                                            <a href="javascript:OpenWinNegativeCust('<%#Container.DataItem("NegativeCustomerID")%>');">
                                                <asp:Label ID="lblNamedtg" runat="server" Text='<%#Container.Dataitem("Customername")%>'>
                                                </asp:Label></a>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="LEGALADDRESS" SortExpression="LEGALADDRESS" HeaderText="ALAMAT KTP">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="Source" SortExpression="Source" HeaderText="SUMBER DATA">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="BADTYPEDesc" SortExpression="BADTYPEDesc" HeaderText="JENIS">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="BADTYPE" SortExpression="BADTYPE" HeaderText="TYPE" Visible="false">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="ISACTIVE" SortExpression="ISACTIVE" HeaderText="ACTIVE">
                                    </asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="IDType" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblIDType" runat="server" Text='<%# container.dataitem("IDType") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                               <uc2:ucGridNav id="GridNavigator" runat="server"/>
                         <%--   <div class="button_gridnavigation">
                                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../Images/grid_navbutton01.png"
                                    CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../Images/grid_navbutton02.png"
                                    CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../Images/grid_navbutton03.png"
                                    CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../Images/grid_navbutton04.png"
                                    CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                                    EnableViewState="False"></asp:Button>
                                <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" MinimumValue="1"
                                    ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"></asp:RangeValidator>
                                <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                                    ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                            </div>
                            <div class="label_gridnavigation">
                                <asp:Label ID="lblPage" runat="server"></asp:Label>of
                                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                            </div>--%>
                        </div>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonAdd" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue">
                    </asp:Button>
                     <asp:Button ID="Button_Search" runat="server" CausesValidation="False" Text="Upload" CssClass="small button blue" >
                    </asp:Button>
                </div>
                <div class="form_box_title">
                    <div class="form_single">
                        <h4>
                            CARI KONSUMEN BLACKLIST
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Cari Berdasarkan</label>
                        <asp:DropDownList ID="cboSearch" runat="server">
                            <asp:ListItem Value="CustomerName">Nama</asp:ListItem>
                            <asp:ListItem Value="LegalAddress">Alamat</asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonSearch" runat="server" CausesValidation="False" CssClass="small button blue"
                        Text="Search"></asp:Button>
                    <asp:Button ID="ButtonReset" runat="server" CausesValidation="False" Text="Reset"
                        CssClass="small button gray"></asp:Button>
                </div>
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlLoadCSV">
                    <div class="form_box">
                        <div class="form_single">
                            <label>
                                Load CSV Files
                            </label>
                            <asp:FileUpload runat="server" ID="files" ViewStateMode="Enabled" />
                        </div>
                    </div>
                    <div class="form_button">
                        <asp:Button ID="Btn_Upload" runat="server" CssClass="small button blue" Text="Upload"
                            CausesValidation="False"></asp:Button>&nbsp;
                    </div>
                </asp:Panel>
            <asp:Panel ID="pnlAdd" runat="server">
                <div class="form_box_header">
                    <div class="form_single">
                        <h4>
                            NAMA KONSUMEN
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Nama
                        </label>
                        <asp:TextBox ID="txtNameAdd" runat="server" CssClass="medium_text"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                            ControlToValidate="txtNameAdd" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Jenis ID Dokumen</label>
                        <asp:DropDownList ID="cboIDTypeAdd" runat="server">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Nomor ID Dokumen</label>
                        <asp:TextBox ID="txtIDNumberAdd" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Tempat / Tanggal Lahir</label>
                        <asp:TextBox ID="txtBirthPlaceAdd" runat="server"></asp:TextBox>&nbsp;/&nbsp;&nbsp;
                        <%--<asp:TextBox ID="txtBirthDate" runat="server"></asp:TextBox>
                        <asp:CalendarExtender ID="txtBirthDate_CalendarExtender" runat="server" Enabled="True"
                            TargetControlID="txtBirthDate" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                            ControlToValidate="txtBirthDate" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                        <uc2:ucDateCE ID="txtBirthDate" runat="server" />
                    </div>
                </div>
             
                <div class="form_box_uc">
                     <uc1:uccompanyaddress id="ucAddress" runat="server"></uc1:uccompanyaddress>
                </div>
             
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            No HandPhone</label>
                        <asp:TextBox ID="txtMobilePhoneAdd" runat="server"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="Regularexpressionvalidator9" runat="server" ControlToValidate="txtMobilePhoneAdd"
                            ErrorMessage="Format No HP Salah!" ValidationExpression="[0-9]{0,15}" CssClass="validator_general"
                            Display="Dynamic">
                        </asp:RegularExpressionValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            e-mail</label>
                        <asp:TextBox ID="txtEmailAdd" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <h5>
                            DATA NEGATIVE
                        </h5>
                    </div>
                </div>
                <asp:Panel ID="pnlBadAdd" runat="server">
                    <div class="form_box">
                        <div class="form_single">
                            <label>
                                Bad Type</label>
                            <asp:DropDownList ID="cboBadTypeAdd" runat="server">
                                <asp:ListItem Value="B">Bad</asp:ListItem>
                                <asp:ListItem Value="W">Warning</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label>
                                Sumber Data</label>
                            <asp:DropDownList ID="cboDataSourceAdd" runat="server">
                            </asp:DropDownList>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlBadEdt" runat="server">
                    <div class="form_box">
                        <div class="form_left">
                            <label>
                                Bad Type</label>
                            <asp:DropDownList ID="cboBadTypeEdt" runat="server">
                                <asp:ListItem Value="B">Bad</asp:ListItem>
                                <asp:ListItem Value="W">Warning</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="form_right">
                            <label>
                                SKT</label>
                            <asp:Label ID="lblSKT" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_left">
                            <label>
                                Sumber Data</label>
                            <asp:DropDownList ID="cboDataSourceEdt" runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form_right">
                            <label>
                                Reject Sebelum Survey</label>
                            <asp:Label ID="lblBefore" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_left">
                            <label class="label_general">
                                Masih Aktif</label>
                            <asp:RadioButtonList ID="rdoActive" runat="server" RepeatDirection="Horizontal" CssClass="opt_single">
                                <asp:ListItem Value="1">Yes</asp:ListItem>
                                <asp:ListItem Value="0">No</asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                        <div class="form_right">
                            <label>
                                Reject Setelah Survey</label>
                            <asp:Label ID="lblAfter" runat="server"></asp:Label>
                        </div>
                    </div>
                </asp:Panel>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_general">
                            Catatan</label>
                        <asp:TextBox ID="txtNotes" runat="server" CssClass="multiline_textbox multiline_new" TextMode="MultiLine"></asp:TextBox>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <h5>
                            Approval History</h5>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtgHistory" runat="server" Width="100%" CellPadding="3" AutoGenerateColumns="False"
                                BorderWidth="0" CssClass="grid_general">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <FooterStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:BoundColumn HeaderText="NO">
                                        <HeaderStyle Width="5%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="UserApproval" HeaderText="NAMA">
                                        <HeaderStyle Width="20%"></HeaderStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="ApprovalDate" HeaderText="TANGGAL">
                                        <HeaderStyle Width="10%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="WOA" HeaderText="JENIS">
                                        <HeaderStyle Width="10%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="Duration" HeaderText="LAMA">
                                        <HeaderStyle Width="10%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="SecurityCode" HeaderText="SECURITY CODE">
                                        <HeaderStyle Width="15%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="ApprovalNote" HeaderText="CATATAN">
                                        <HeaderStyle Width="30%"></HeaderStyle>
                                    </asp:BoundColumn>
                                </Columns>
                            </asp:DataGrid>
                        </div>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonSave" runat="server" CausesValidation="true" Text="Save" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="ButtonCancel" runat="server" CausesValidation="false" Text="Cancel"
                        CssClass="small button gray"></asp:Button>
                </div>
            </asp:Panel>
    </form>
</body>
</html>
