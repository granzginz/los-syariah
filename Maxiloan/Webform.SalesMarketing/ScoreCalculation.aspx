﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ScoreCalculation.aspx.vb" Inherits="Maxiloan.Webform.SalesMarketing.ScoreCalculation" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
  <%@ Register Src="../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %>  
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Prospect Scoring</title>
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <script src="../js/jquery-1.9.1.min.js"></script>
    <script src="../js/jquery-ui-1.10.3.custom.min.js"></script>

    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';
    </script>


    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
</head>
<script type="text/javascript">

    $(document).ready(function () {
        $("#dtgPaging_chkHeader").click(function () {
            $("#<%=dtgPaging.ClientID%> input[id*='ItemCheckBox']:checkbox").attr('checked', $(this).prop('checked'));
        });

        $('#btnProses').click(function () {
            if ($('input:checkbox[id^="dtgPaging_ItemCheckBox_"]:checked').length <= 0) {
                alert("Silahkan pilih data terlebih dahulu");
                return false;
            }
            return true;
        });
    });
    
    </script>

<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();" />
    
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    CETAK SCORE CALCULATION</h3>
            </div>
        </div>
        
    <asp:Panel runat="server" ID="pnlList">


     <div class="form_title">
            <div class="form_single">
                <h4> CARI APLIKASI</h4>
            </div>
    </div>
    <div class="form_box">
    <div class="form_single">
        <label>Cari Berdasarkan </label>
        <asp:DropDownList ID="cboSearch" runat="server">
            <asp:ListItem Value="Name">Name</asp:ListItem>
            <asp:ListItem Value="ProspectAppID">DATA ID</asp:ListItem>
        </asp:DropDownList>
        <asp:TextBox ID="txtSearch" runat="server" />
    </div></div>
    <div class="form_box">
            <div class="form_single">
                <label>Status Cetak</label>
                <asp:DropDownList ID="cboPrinted" runat="server">
                    <asp:ListItem Value="No">No</asp:ListItem>
                    <asp:ListItem Value="Yes">Yes</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
    
    <div class="form_button">
        <asp:Button ID="btnSearch" runat="server" CssClass="small button blue" Text="Find" CausesValidation="False" />
        <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="small button gray" CausesValidation="False" />
    </div>

     
     <div class="form_box_header">
            <div class="form_single">
                <asp:DataGrid ID="dtgPaging" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                    OnSortCommand="Sorting" BorderWidth="0px" CellPadding="3" CellSpacing="1" CssClass="grid_general">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <Columns>
                       <asp:TemplateColumn HeaderText="PILIH">
                        <HeaderTemplate> <asp:CheckBox ID="chkHeader" runat="server" /> </HeaderTemplate>
                            <ItemTemplate>  
                                <asp:CheckBox ID="ItemCheckBox" runat="server" />
                              </ItemTemplate>
                         </asp:TemplateColumn>
                         <asp:BoundColumn DataField= "ProspectAppID" HeaderText="DATA ID" />
                         <asp:BoundColumn DataField="Name" HeaderText="NAMA" />
                         <asp:BoundColumn DataField="CustomerType" HeaderText="TIPE" />
                         <asp:BoundColumn DataField="ProspectAppDate" HeaderText="TANGGAL"  DataFormatString="{0:dd/MM/yyyy}" />
                         <asp:BoundColumn DataField="Kendaraan" HeaderText="ASSET YANG DIBIAYAI" /> 
                    </Columns>
                </asp:DataGrid> 
                 <uc2:ucGridNav id="GridNavigator" runat="server"/>  
            </div>

            <div class="form_button">
               <asp:Button ID="btnProses" runat="server" Text="Print"  CssClass="small button green" /> 
                <asp:Button ID="btnCancelProses" runat="server" Text="Cancel" CssClass="small button gray"  CausesValidation="False" />
            </div>

        </div>
        </asp:Panel>

         <div class="form_box_hide">
            <CR:CrystalReportViewer ID="crViewer" runat="server" AutoDataBind="true" />
        </div>
    </form>
</body>
</html>

