﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ProspectDemografi.aspx.vb"
    Inherits="Maxiloan.Webform.SalesMarketing.ProspectDemografi" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../webform.UserController/ucProspectTab.ascx" TagName="ucProspectTab" TagPrefix="uc1" %>
<%@ Register Src="../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat" TagPrefix="uc1" %>
<%@ Register Src="../webform.UserController/ucDateCE.ascx" TagName="ucDateCE" TagPrefix="uc1" %>
<%@ Register Src="../webform.UserController/ucMonthCombo.ascx" TagName="ucMonthCombo" TagPrefix="uc2" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Prospect Maintenance Demografi</title>
    <link rel="Stylesheet" type="text/css" href="../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../Include/Buttons.css" />
    
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <link href="../include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../js/jquery-1.9.1.min.js"></script>
    <script src="../js/jquery-ui-1.10.3.custom.min.js"></script>
    <script language="javascript" type="text/javascript">
        function NamaIndustriBuClick(e, h) {
            //var cboselect = $("option:selected", $("#cboIndrustriHeader")).val();
            var cboselect = $('#cboIndrustriHeader').val();
            if (cboselect == 'Select One') return;
            var url = e + cboselect
            OpenJLookup(url, 'Daftar Industri', h); return false;
            // OpenJLookup('<%= ResolveClientUrl("~/webform.Utility/jLookup/Industri.aspx?kode=" & hdfKodeIndustriBU.ClientID & "&nama=" & txtNamaIndustriBU.ClientID) %>', 'Daftar Industri', '<%= jlookupContent.ClientID %>'); return false;
        }
        function NamaIndustriBuClickPasangan(e, h) {
            //var cboselect = $("option:selected", $("#cboIndrustriHeader")).val();
            var cboselect = $('#cboIndrustriHeaderPasangan').val();
            if (cboselect == 'Select One') return;
            var url = e + cboselect
            OpenJLookup(url, 'Daftar Industri', h); return false;
            // OpenJLookup('<%= ResolveClientUrl("~/webform.Utility/jLookup/Industri.aspx?kode=" & hdfKodeIndustriBU.ClientID & "&nama=" & txtNamaIndustriBU.ClientID) %>', 'Daftar Industri', '<%= jlookupContent.ClientID %>'); return false;
        }
        function number_format(number, decimals, dec_point, thousands_sep) {

            number = (number + '')
            .replace(/[^0-9+\-Ee.]/g, '');
            var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function (n, prec) {
                var k = Math.pow(10, prec);
                return '' + (Math.round(n * k) / k)
                .toFixed(prec);
            };
            // Fix for IE parseFloat(0.55).toFixed(0) = 0;
            s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
            .split('.');
            if (s[0].length > 3) {
                s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
            }
            if ((s[1] || '')
            .length < prec) {
                s[1] = s[1] || '';
                s[1] += new Array(prec - s[1].length + 1)
              .join('0');
            }
            return s.join(dec);
        }
        function recalculateAngs() {

            var kotor = $('#ucPenghasilanKotor_txtNumber').val();
            var bersih = $('#ucPenghasilanBersih_txtNumber').val();

            var xtotal = parseInt(kotor.replace(/\s*,\s*/g, '')) -
                         parseInt(bersih.replace(/\s*,\s*/g, '')) ;

            $('#lblBiayaHidup').html(number_format(xtotal));
        }
    </script>
</head>
<body>
    <asp:UpdateProgress ID="upg1" runat="server" AssociatedUpdatePanelID="upnl1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <div runat="server" id="jlookupContent" />
    <asp:UpdatePanel runat="server" ID="upnl1" UpdateMode="Conditional" ChildrenAsTriggers="false">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
                onclick="hideMessage();"></asp:Label>
            <uc1:ucProspectTab id="ucProspectTab1" runat="server" />
            <div class="form_title">
                <div class="form_single">
                    <h3>
                        DATA DEMOGRAFI</h3>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="label_general">
                           Jenis Kelamin
                        </label>
                        <asp:RadioButtonList ID="rdoJenisKelamin" runat="server" RepeatDirection="Horizontal" 
                                CssClass="opt_single">
                                <asp:ListItem Value="M" Selected="True">Laki-laki</asp:ListItem>
                                <asp:ListItem Value="F">Perempuan</asp:ListItem>
                            </asp:RadioButtonList>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label class="label_req">
                        Status Perkawinan</label>
                    <asp:DropDownList ID="cboPMarital" onchange=""
                        runat="server">
                    </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" Display="Dynamic"
                            InitialValue="Select One" ControlToValidate="cboPMarital" ErrorMessage="Harap pilih Status Perkawinan"
                            CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="label_req">
                           Jumlah Tanggungan
                        </label>
                        <uc1:ucnumberformat runat="server" id="ucJumlahTanggungan" TextCssClass="numberAlign reguler_text"></uc1:ucnumberformat> 
                      <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" Display="Dynamic"
                            InitialValue="0" ControlToValidate="ucJumlahTanggungan$txtNumber" ErrorMessage="Jumlah Tanggungan Harus >= 0"
                            CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="label_req">
                            Pendidikan</label>
                        <asp:DropDownList ID="cboPEducation" runat="server">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Display="Dynamic"
                            InitialValue="Select One" ControlToValidate="cboPEducation" ErrorMessage="Harap pilih Pendidikan"
                            CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="label_req">
                                    Status Rumah</label>
                                <asp:DropDownList ID="cboPHomeStatus" runat="server" >
                                </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="Dynamic"
                            InitialValue="Select One" ControlToValidate="cboPHomeStatus" ErrorMessage="Harap pilih Status Rumah"
                            CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="label_req">
                            Tinggal sejak Tahun</label>
                        <uc2:ucmonthcombo id="ucTinggalSejakBulan" runat="server" />
                        <asp:TextBox ID="txtPStaySince" runat="server" MaxLength="4" Columns="7" onkeypress="return numbersonly2(event)"></asp:TextBox>
                        <asp:RangeValidator ID="RangeValidator1" runat="server" Display="Dynamic" ControlToValidate="txtPStaySince" CssClass="validator_general"
                            ErrorMessage="RangeValidator" Type="Integer" MinimumValue="0">Tahun Harus lebih Kecil atau sama dengan hari ini</asp:RangeValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator10" runat="server"
                            Display="Dynamic" ControlToValidate="txtPStaySince" ErrorMessage="Harap isi dengan 4 Angka"
                            ValidationExpression="\d{4}" CssClass="validator_general"></asp:RegularExpressionValidator>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="label_req">
                                        Mulai Kerja
                                    </label>
                                    <uc2:ucmonthcombo id="ucKaryawanSejakCustomer" runat="server" />
                                    <asp:TextBox ID="txtEmploymentYear" runat="server" MaxLength="4" Columns="7" onkeypress="return numbersonly2(event)" ></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="Harap isi Menjadi Karyawan sejak Tahun"
                                        ControlToValidate="txtEmploymentYear" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                                    <asp:RangeValidator ID="RangeValidator2" runat="server" ErrorMessage="RangeValidator" CssClass="validator_general"
                                        ControlToValidate="txtEmploymentYear" Display="Dynamic" MinimumValue="0" Type="Integer">Tahun Harus lebih Kecil dari hari ini</asp:RangeValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ErrorMessage="Harap isi dengan 4 Angka"
                                        ControlToValidate="txtEmploymentYear" Display="Dynamic" ValidationExpression="\d{4}"
                                        CssClass="validator_general"></asp:RegularExpressionValidator>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="label_req">
                           Penghasilan Kotor Per Bulan
                        </label>
                        <uc1:ucnumberformat runat="server" id="ucPenghasilanKotor" TextCssClass="numberAlign reguler_text" onclientchange="recalculateAngs()" ></uc1:ucnumberformat>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" Display="Dynamic"
                            InitialValue="0" ControlToValidate="ucPenghasilanKotor$txtNumber" ErrorMessage="Penghasilan Kotor Per Bulan Harus > 0"
                            CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="label_req">
                           Penghasilan Bersih Per Bulan
                        </label>
                        <uc1:ucnumberformat runat="server" id="ucPenghasilanBersih" TextCssClass="numberAlign reguler_text" onclientchange="recalculateAngs()" ></uc1:ucnumberformat>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" Display="Dynamic"
                            InitialValue="0" ControlToValidate="ucPenghasilanBersih$txtNumber" ErrorMessage="Penghasilan Bersih Per Bulan Harus > 0"
                            CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="">
                           Biaya Hidup
                        </label>
                        <asp:Label runat="server" ID="lblBiayaHidup"  CssClass="numberAlign regular_text"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label class="label_req">
                        Pekerjaan</label>
                    <asp:DropDownList ID="cboJobType" runat="server">
                    </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Display="Dynamic"
                            InitialValue="Select One" ControlToValidate="cboJobType" ErrorMessage="Harap pilih Pekerjaan"
                            CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label class="label_req">Occupation</label>
                    <asp:DropDownList ID="cboOccupation" runat="server" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" Display="Dynamic"
                            InitialValue="Select One" ControlToValidate="cboOccupation" ErrorMessage="Harap pilih Occupation"
                            CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">                            
                    <label class="label_req">Bidang Usaha</label>
                    <asp:DropDownList ID="cboIndrustriHeader" runat="server" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic"
                            InitialValue="Select One" ControlToValidate="cboIndrustriHeader" ErrorMessage="Harap pilih Bidang Usaha"
                            CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label class="label_req">Bidang Usaha Detail</label> 
                    <asp:HiddenField runat="server" ID="hdfKodeIndustriBU" /> 
                    <asp:TextBox runat="server" ID="txtNamaIndustriBU" Enabled="false" CssClass="medium_text" text="-"/>
                    <%--<button class="small buttongo blue"  onclick ="OpenJLookup('<%= ResolveClientUrl("~/webform.Utility/jLookup/Industri.aspx?kode=" & hdfKodeIndustriBU.ClientID & "&nama=" & txtNamaIndustriBU.ClientID) & "&header=" &   cboIndrustriHeader.ClientID  %>','Daftar Industri','<%= jlookupContent.ClientID %>');return false;">...</button>--%>                                                                            
                    <button class="small buttongo blue"  onclick ="OpenJLookup('<%= ResolveClientUrl("~/webform.Utility/jLookup/Industri.aspx?kode=" & hdfKodeIndustriBU.ClientID & "&nama=" & txtNamaIndustriBU.ClientID) %>','Daftar Industri','<%= jlookupContent.ClientID %>');return false;">...</button>
                    <asp:RequiredFieldValidator ID="rfvtxtKodeIndustriBU" runat="server" ErrorMessage="*" Display="Dynamic"  CssClass="validator_general" ControlToValidate="txtNamaIndustriBU" />
                </div>
            </div>
            
            <div class="form_box">
                <div class="form_single">
                    <label class="label_req">
                        Natural Of Business</label>
                    <asp:DropDownList ID="cboNaturalOfBusiness" runat="server" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" Display="Dynamic"
                            InitialValue="Select One" ControlToValidate="cboNaturalOfBusiness" ErrorMessage="Harap pilih Natural Of Business"
                            CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label class="">
                        Pekerjaan Pasangan</label>
                    <asp:DropDownList ID="cboJobTypePasangan" runat="server">
                    </asp:DropDownList>
                </div>
            </div>
             <div class="form_box">
                <div class="form_single">
                    <label class="label">Occupation Pasangan</label>
                    <asp:DropDownList ID="cboOccupationSpouse" runat="server" />
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">                            
                    <label class="">Bidang Usaha Pasangan</label>
                    <asp:DropDownList ID="cboIndrustriHeaderPasangan" runat="server" />
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label class="">Bidang Usaha Detail Pasangan</label> 
                    <asp:HiddenField runat="server" ID="hdfKodeIndustriBUPasangan" /> 
                    <asp:TextBox runat="server" ID="txtNamaIndustriBUPasangan" Enabled="false" CssClass="medium_text" text="-"/>
                    <%--<button class="small buttongo blue"  onclick ="OpenJLookup('<%= ResolveClientUrl("~/webform.Utility/jLookup/Industri.aspx?kode=" & hdfKodeIndustriBU.ClientID & "&nama=" & txtNamaIndustriBU.ClientID) & "&header=" &   cboIndrustriHeader.ClientID  %>','Daftar Industri','<%= jlookupContent.ClientID %>');return false;">...</button>                                                                            --%>
                    <button class="small buttongo blue"  onclick ="OpenJLookup('<%= ResolveClientUrl("~/webform.Utility/jLookup/Industri.aspx?kode=" & hdfKodeIndustriBU.ClientID & "&nama=" & txtNamaIndustriBU.ClientID) %>','Daftar Industri','<%= jlookupContent.ClientID %>');return false;">...</button>
                </div>
            </div>   
            <div class="form_box">
                <div class="form_single">
                    <label class="">
                        Natural Of Business Pasangan
                    </label>
                    <asp:DropDownList ID="cboNaturalOfBusinessSpouse" runat="server" />
                </div>
            </div>                 
            <div class="form_button">
                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue"
                    CausesValidation="True"></asp:Button>
                <asp:Button ID="btnCancel" runat="server" Text="Back" CssClass="small button gray"
                    CausesValidation="False"></asp:Button>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
    </form>
</body>
</html>
