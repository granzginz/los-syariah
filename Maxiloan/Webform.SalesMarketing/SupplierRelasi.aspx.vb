﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.cbse
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region
Public Class SupplierRelasi
    Inherits Maxiloan.Webform.WebBased
    Private m_controller As New SupplierController
    Dim oCustom As New Parameter.Supplier
    Private Property supervisorID() As String
        Get
            Return CType(ViewState("supervisorID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("supervisorID") = Value
        End Set
    End Property

    
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Dim tmp As New DataTable
            oCustom.SupplierID = Request("id")
            oCustom.strConnection = GetConnectionString()
            tmp = m_controller.GetSupplierEmployeeSPV(oCustom).ListData
            cboSupervisor.DataValueField = "SupplierEmployeeID"
            cboSupervisor.DataTextField = "SupplierEmployeeName"
            cboSupervisor.DataSource = tmp.DefaultView
            cboSupervisor.DataBind()
            cboSupervisor.Items.Insert(0, "Select One")
            cboSupervisor.Items(0).Value = "Select One"

            Me.supervisorID = ""
            BindData()
        End If
    End Sub
    Public Sub BindData()
        Dim dtEntity As DataTable
        Dim oCustomClass As New Parameter.Supplier
        With oCustomClass
            .SupervisorID = Me.supervisorID
            .SupplierID = Request("id")
            .strConnection = GetConnectionString()
        End With
        oCustomClass = m_controller.GetSupplierEmployeeSales(oCustomClass)
        If Not oCustomClass Is Nothing Then
            dtEntity = oCustomClass.ListData
        End If
        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()
    End Sub

    Protected Sub ButtonBackAdd_Click(sender As Object, e As EventArgs) Handles ButtonBackAdd.Click
        Response.Redirect(Request("apps") & ".aspx?currentPage=" & Request("currentPage") & "")
    End Sub

    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim chk As New CheckBox
            Dim lblsalas As Label
            Dim lblexisting As Label
            Dim lblSupplierEmployeeName As New HyperLink

            chk = CType(e.Item.FindControl("chkSales"), CheckBox)
            lblsalas = CType(e.Item.FindControl("lblsales"), Label)
            lblexisting = CType(e.Item.FindControl("lblexisting"), Label)

            If lblsalas.Text >= 1 Then
                chk.Checked = True
            Else
                chk.Checked = False
                If lblexisting.Text = 0 And Me.supervisorID <> "" Then
                    chk.Enabled = True
                Else
                    chk.Enabled = False
                End If
            End If

           

        End If
    End Sub

    Protected Sub cboSupervisor_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboSupervisor.SelectedIndexChanged
        Me.supervisorID = cboSupervisor.SelectedValue.ToString
        BindData()
    End Sub

    Protected Sub ButtonSaveAdd_Click(sender As Object, e As EventArgs) Handles ButtonSaveAdd.Click
        Dim m_controller As New SupplierController
        Dim oRelasi As New DataTable
        Dim chk As New CheckBox
        Dim oRow As DataRow
        Dim lblSupplierEmployeeID As Label

        If cboSupervisor.SelectedValue.ToString = "Select One" Then
            ShowMessage(lblMessage, "Supervisor Belum dipilih!", True)
            Exit Sub
        End If

        oRelasi.Columns.Add("SupplierEmpployeeID", GetType(String))

        For intLoop = 0 To dtgPaging.Items.Count - 1
            chk = CType(dtgPaging.Items(intLoop).Cells(0).FindControl("chkSales"), CheckBox)
            lblSupplierEmployeeID = CType(dtgPaging.Items(intLoop).Cells(0).FindControl("lblEmployeeID"), Label)
            If chk.Checked Then
                oRow = oRelasi.NewRow
                oRow("SupplierEmpployeeID") = lblSupplierEmployeeID.Text.ToString
                oRelasi.Rows.Add(oRow)
            End If
        Next
        'If oRelasi.Rows.Count = 0 Then
        '    ShowMessage(lblMessage, "Harap pilih minimum 1 cabang .....", True)
        '    Exit Sub
        'End If



        oCustom.strConnection = GetConnectionString()
        oCustom.SupplierID = Request("id")
        oCustom.SupervisorID = cboSupervisor.SelectedItem.Value.ToString.Trim

        m_controller.SaveSupplierEmployeeRelasi(oCustom, oRelasi)
        ShowMessage(lblMessage, "Successfully", False)

        BindData()

    End Sub
End Class