﻿Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
Imports System.IO
Imports System.Collections.Generic

Public Class ProspectAsset
    Inherits Maxiloan.Webform.WebBased


#Region "Controls"
    Private oController As New ProspectController
    Private m_controller As New AssetDataController
    'Protected WithEvents ucOTR As ucNumberFormat
    'Protected WithEvents ucUangMuka As ucNumberFormat
    'Protected WithEvents ucUangMukaTradeIn As ucNumberFormat
    'Protected WithEvents ucAngsuranBulanan As ucNumberFormat
    'Protected WithEvents ucTenor As ucNumberFormat
    Protected WithEvents ucProspectTab1 As ucProspectTab
    Protected WithEvents ucLookupProductOffering1 As UcLookUpPdctOffering
    Private time As String
#End Region

#Region "Properties"
    Property ProspectAppID() As String
        Get
            Return ViewState("ProspectAppID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ProspectAppID") = Value
        End Set
    End Property
    Property PageMode() As String
        Get
            Return ViewState("PageMode").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("PageMode") = Value
        End Set
    End Property
    Property ActivityDateStart() As DateTime
        Get
            Return ViewState("ActivityDateStart").ToString
        End Get
        Set(ByVal Value As DateTime)
            ViewState("ActivityDateStart") = Value
        End Set
    End Property
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Visible = False

        If SessionInvalid() Then Exit Sub

        If Not Page.IsPostBack Then
            initObjects()
            Me.PageMode = Request("page")
            If Request("ActivityDateStart") = Nothing Then
                time = Now.Hour.ToString("00") & ":" & Now.Minute.ToString("00") & ":" & Now.Second.ToString("00")
                Me.ActivityDateStart = Me.BusinessDate + " " + time
            Else
                Me.ActivityDateStart = Request("ActivityDateStart")
            End If

            If Me.PageMode = "Edit" Then
                Dim oCustomclass As New Parameter.Prospect
                Me.ProspectAppID = Request("id")
                With oCustomclass
                    .strConnection = GetConnectionString()
                    .ProspectAppID = Me.ProspectAppID
                End With
                BindEdit()

                ucProspectTab1.ProspectAppID = Me.ProspectAppID
                ucProspectTab1.setLink()
            Else
                Me.ProspectAppID = ""
            End If
        End If
    End Sub


#Region "Init Objects"
    Protected Sub initObjects()
        ucProspectTab1.selectedTab("Asset")
        FillCbo(cboUsage, "tblAssetUsage")
        RangeValidator3.MaximumValue = CStr(Year(Me.BusinessDate) + 1)
        If cboKondisiAsset.SelectedValue = "U" Then
            RFNoPolisi.Enabled = True
            RFNoSertifikat.Enabled = True
        Else
            RFNoPolisi.Enabled = False
            RFNoSertifikat.Enabled = False
        End If
        'ucLookupProductOffering1.BranchID = Me.sesBranchId.Replace("'", "")
        'ucOTR.RequiredFieldValidatorEnable = True
        'ucAngsuranBulanan.RequiredFieldValidatorEnable = True
        'ucTenor.RequiredFieldValidatorEnable = True
        'ucOTR.RangeValidatorMinimumValue = "1"
        'ucAngsuranBulanan.RangeValidatorMinimumValue = "1"
        'ucTenor.RangeValidatorMinimumValue = "1"
    End Sub
    Sub FillCbo(ByVal cboName As DropDownList, ByVal Table As String)
        Dim oAssetData As New Parameter.AssetData
        Dim oData As New DataTable
        oAssetData.strConnection = GetConnectionString()
        oAssetData.Table = Table
        oAssetData = m_controller.GetCbo(oAssetData)
        oData = oAssetData.ListData
        cboName.DataSource = oData
        cboName.DataTextField = "Description"
        cboName.DataValueField = "ID"
        cboName.DataBind()
        cboName.Items.Insert(0, "Select One")
        cboName.Items(0).Value = "Select One"
    End Sub
#End Region

#Region "BindEdit"
    Sub BindEdit()
        Dim oApplication As New Parameter.Prospect
        Dim oData As New DataTable
        Dim oRow As DataRow

        oApplication.strConnection = GetConnectionString()
        oApplication.BranchId = Replace(Me.sesBranchId, "'", "").ToString
        oApplication.ProspectAppID = Me.ProspectAppID
        oApplication = oController.GetViewProspect(oApplication)

        If Not oApplication Is Nothing Then
            oData = oApplication.listdata
        End If

        If oData.Rows.Count > 0 Then
            oRow = oData.Rows(0)
            'hdfAssetCode.Value = oRow("AssetCode").ToString
            'txtAssetName.Text = oRow("Description").ToString
            hdfAssettypeCode.Value = oRow("AssetType").ToString
            'txtAssetType.Text = oRow("AssetTypeDescription").ToString
            lblSupplierName.Text = oRow("SupplierName").ToString
            lblAssetType.Text = oRow("AssetTypeDescription").ToString
            hdfAssetBrandCode.Value = oRow("AssetBrand").ToString
            'txtAssetBrand.Text = oRow("AssetBrandDescription").ToString
            lblAssetBrand.Text = oRow("AssetBrandDescription").ToString
            ' lblKategoriAsset.Text = oRow("CategoryID").ToString
            cboKondisiAsset.SelectedIndex = cboKondisiAsset.Items.IndexOf(cboKondisiAsset.Items.FindByValue(oRow("UsedNew").ToString))
            txtYear.Text = oRow("ManufacturingYear").ToString
            cboUsage.SelectedIndex = cboUsage.Items.IndexOf(cboUsage.Items.FindByValue(oRow("AssetUsage").ToString))
            txtNoPolisi.Text = oRow("LicensePlate").ToString
            'ucOTR.Text = FormatNumber(oRow("OTRPrice"), 0)
            'ucUangMuka.Text = FormatNumber(oRow("DPAmount"), 0)
            'ucUangMukaTradeIn.Text = FormatNumber(oRow("DPAmountTradeIn"), 0)
            lblOTR.Text = FormatNumber(oRow("OTRPrice"), 0)
            lblUangMuka.Text = FormatNumber(oRow("DPAmount"), 0)
            lblUangMukaTradeIn.Text = FormatNumber(oRow("DPAmountTradeIn"), 0)
            lblTotalUangMuka.Text = FormatNumber(CDec(oRow("DPAmount")) + CDec(oRow("DPAmountTradeIn")), 0)
            lblPokokHutang.Text = FormatNumber(oRow("NTF"), 0)
            'ucAngsuranBulanan.Text = FormatNumber(oRow("InstallmentAmount"), 0)
            'ucTenor.Text = oRow("Tenor").ToString
            lblAngsuranBulanan.Text = FormatNumber(oRow("InstallmentAmount"), 0)
            lblTenor.Text = oRow("Tenor").ToString
            lblusiaakhirkontrak.Text = ""
            cboWayPymt.SelectedIndex = cboWayPymt.Items.IndexOf(cboWayPymt.Items.FindByValue(oRow("WayOfPayment").ToString))
            tanggalLahir.Value = ConvertDate(oRow("BirthDate"))
            lblusiaakhirkontrak.Text = getAge(ConvertDate(oRow("BirthDate")), CInt(oRow("Tenor").ToString))
            cboPackage.SelectedIndex = cboPackage.Items.IndexOf(cboPackage.Items.FindByValue(oRow("Package").ToString))
            'ucLookupProductOffering1.ProductID = oRow("ProductID").ToString.Trim
            'ucLookupProductOffering1.ProductOfferingID = oRow("ProductOfferingID").ToString.Trim
            'ucLookupProductOffering1.ProductOfferingDescription = oRow("ProductDescription").ToString.Trim
            'mesti diganti nih 
            cboKondisiAsset.Attributes.Add("onclick", "cboKondisiAssetChange();")
            cboAsset.Attributes.Add("onclick", "cboAssetChange();")
            'cboAsset.SelectedIndex = cboAsset.Items.IndexOf(cboAsset.Items.FindByValue(oRow("Asset").ToString))
            'txtNoSertifikat.Text = oRow("NoSertifikat").ToString
        End If

    End Sub

    Private Function getAge(ByVal dt As String, ByVal m As Integer) As String
        Dim rtn As String = ""
        Dim dob As DateTime
        dob = New DateTime(dt.Substring(0, 4), dt.Substring(6, 2), dt.Substring(4, 2))
        Dim tday As TimeSpan = DateTime.Now.AddMonths(m).Subtract(dob)
        Dim years As Integer, months As Integer, days As Integer
        months = 12 * (DateTime.Now.AddMonths(m).Year - dob.Year) + (DateTime.Now.AddMonths(m).Month - dob.Month)

        If DateTime.Now.AddMonths(m).Day < dob.Day Then
            months -= 1
            days = DateTime.DaysInMonth(dob.Year, dob.Month) - dob.Day + DateTime.Now.AddMonths(m).Day
        Else
            days = DateTime.Now.AddMonths(m).Day - dob.Day
        End If
        years = Math.Floor(months / 12)
        months -= years * 12

        rtn = years & " tahun, " & months & " bulan and " & days & " hari"
        Return rtn
    End Function
#End Region

#Region "Save"
    Private Sub imbSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If Me.Page.IsValid Then
                lblMessage.Visible = False

                Dim oApplication As New Parameter.Prospect

                oApplication.strConnection = GetConnectionString()
                oApplication.BusinessDate = Me.BusinessDate
                oApplication.BranchId = Replace(Me.sesBranchId, "'", "").ToString
                oApplication.ProspectAppID = Me.ProspectAppID
                'oApplication.AssetCode = hdfAssetCode.Value
                oApplication.AssetType = hdfAssettypeCode.Value
                oApplication.AssetBrand = hdfAssetBrandCode.Value
                oApplication.UsedNew = cboKondisiAsset.SelectedValue
                oApplication.ManufacturingYear = IIf(IsNumeric(txtYear.Text), CInt(txtYear.Text), "0")
                oApplication.AssetUsage = cboUsage.SelectedValue
                oApplication.LicensePlate = txtNoPolisi.Text
                'oApplication.OTRPrice = CDec(ucOTR.Text)
                'oApplication.DPAmount = CDec(ucUangMuka.Text)
                'oApplication.DPAmountTradeIn = CDec(ucUangMukaTradeIn.Text)
                oApplication.OTRPrice = CDec(lblOTR.Text)
                oApplication.DPAmount = CDec(lblUangMuka.Text)
                oApplication.DPAmountTradeIn = CDec(lblUangMukaTradeIn.Text)
                oApplication.NTF = oApplication.OTRPrice - (oApplication.DPAmount + oApplication.DPAmountTradeIn)
                'oApplication.InstallmentAmount = CDec(ucAngsuranBulanan.Text)
                'oApplication.Tenor = CDec(ucTenor.Text)
                oApplication.InstallmentAmount = CDec(lblAngsuranBulanan.Text)
                oApplication.Tenor = CDec(lblTenor.Text)
                oApplication.WayOfPayment = cboWayPymt.SelectedValue
                'oApplication.ProductID = ucLookupProductOffering1.ProductID
                'oApplication.ProductOfferingID = ucLookupProductOffering1.ProductOfferingID
                'mesti diganti nih
                oApplication.Mode = Me.PageMode
                oApplication.LoginId = Me.Loginid
                oApplication.Package = cboPackage.SelectedValue
                'oApplication.ProductID = ucLookupProductOffering1.ProductID
                'oApplication.ProductOfferingID = ucLookupProductOffering1.ProductOfferingID
                oApplication.Sertifikat = txtNoSertifikat.Text
                oApplication.Asset = cboAsset.SelectedValue

                Dim ErrorMessage As String = ""
                Dim oReturn As New Parameter.Prospect

                oReturn = oController.ProspectSaveAsset(oApplication)

                If oReturn.Err <> "" Then
                    ShowMessage(lblMessage, ErrorMessage, True)
                Else
                    ShowMessage(lblMessage, "Data saved!", False)
                    'ucProspectTab1.ProspectAppID = Me.ProspectAppID
                    'ucProspectTab1.setLink()
                    'Me.PageMode = "Edit"
                    Response.Redirect("ProspectDemografi.aspx?id=" & oReturn.ProspectAppID.Trim & "&page=Edit&ActivityDateStart=" & Me.ActivityDateStart & "")
                End If
            Else
                ShowMessage(lblMessage, "Data Sudah Ada", True)
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub



#End Region

    Protected Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("ProspectApplication.aspx")
    End Sub


    Protected Function cboKondisiAssetChange() As String
        Return "cboKondisiAssetChange();"
    End Function

    Protected Function cboAssetChange() As String
        Return "cboAssetChange();"
    End Function

End Class