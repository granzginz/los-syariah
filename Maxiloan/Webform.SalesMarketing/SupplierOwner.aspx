﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SupplierOwner.aspx.vb"
    Inherits="Maxiloan.Webform.SalesMarketing.SupplierOwner" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcCompanyAdress" Src="../Webform.UserController/ucCompanyAddress.ascx" %>
<%@ Register src="../Webform.UserController/ucDateCE.ascx" tagname="ucDateCE" tagprefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>SupplierOwner</title>
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <link href="../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;

        function OpenWin(SupplierID, pStyle) {
            var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            window.open('http://<%#Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.SalesMarketing/Viewsupplier.aspx?style=' + pStyle + '&SupplierID=' + SupplierID, 'UserLookup', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');        
        }

        function OpenWinOwnerID(SupplierID, SupplierOwnerID, pStyle) {
            var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            window.open('http://<%#Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.SalesMarketing/ViewSupplierOwnerID.aspx?style=' + pStyle + '&SupplierID=' + SupplierID + '&OwnerID=' + SupplierOwnerID, 'UserLookup', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');           
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">    
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
    <ContentTemplate>
        <asp:Label ID="lblMessage" runat="server" Visible ="false" ></asp:Label>                                     
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">                   
                <h3>
                    DAFTAR PEMILIK SUPPLIER
                </h3>
            </div>
        </div>        
        <asp:Panel ID="pnlList" runat="server">        
        <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgPaging" runat="server" Width="100%" DataKeyField="SupplierOwnerID"
                            BorderStyle="None" BorderWidth="0" OnSortCommand="Sorting" AutoGenerateColumns="False"
                            AllowSorting="True" CssClass="grid_general">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <Columns>
                                <asp:TemplateColumn HeaderText="EDIT">
                                    <ItemStyle CssClass="command_col"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" ImageUrl="../Images/iconedit.gif"
                                            CommandName="Edit"></asp:ImageButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="DELETE">
                                    <ItemStyle CssClass="command_col"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgDelete" runat="server" CausesValidation="False" ImageUrl="../Images/icondelete.gif"
                                            CommandName="Delete"></asp:ImageButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="SupplierOwnerID" HeaderText="ID" Visible="False">                                    
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkOwnerID" runat="server" CommandName="OwnerID" CausesValidation="False"
                                            Text='<%# DataBinder.Eval(Container, "DataItem.SupplierOwnerID") %>'>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="SupplierOwnerName" HeaderText="NAMA">                                    
                                    <ItemTemplate>
                                        <asp:HyperLink ID="lblSupplierOwnerName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SupplierOwnerName") %>'>
                                        </asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn DataField="Address" SortExpression="Address" HeaderText="ALAMAT">                                    
                                </asp:BoundColumn>
                            </Columns>
                        </asp:DataGrid>                  
                <div class="button_gridnavigation">
	                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../Images/grid_navbutton01.png"
		                CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../Images/grid_navbutton02.png"
		                CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../Images/grid_navbutton03.png"
		                CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../Images/grid_navbutton04.png"
		                CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
	                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
		                EnableViewState="False"></asp:Button>
	                <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage"
		                MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer"
		                CssClass="validator_general"></asp:RangeValidator>
	                <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
		                ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>
                <div class="label_gridnavigation">
	                <asp:Label ID="lblPage" runat="server"></asp:Label>of
	                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
	                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                </div>
            </div>                
        </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonAdd" runat="server"  Text="Add" CssClass ="small button blue" CausesValidation="False">
            </asp:Button>                                                              
            <asp:Button ID="ButtonBack" runat="server" CausesValidation="False" Text="Back" CssClass ="small button gray">
            </asp:Button>
        </div>
        <div class="form_box_title">
            <div class="form_single">              
                <h4>
                    CARI PEMILIK SUPPLIER
                </h4>
            </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Nama Supplier</label>
                <asp:HyperLink ID="lblName" runat="server" NavigateUrl=""></asp:HyperLink>
	        </div>
        </div>  
        <div class="form_box">
	        <div class="form_single">
                <label>Cari Berdasarkan</label>
                <asp:DropDownList ID="cboSearch" runat="server">
                    <asp:ListItem Value="SupplierOwnerName">Nama Pemilik</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearch" runat="server" MaxLength="50"></asp:TextBox>
	        </div>
        </div>  
        <div class="form_button">
            <asp:Button ID="ButtonSearch" runat="server" CausesValidation="False"  Text="Search" CssClass ="small button blue">
            </asp:Button>
            <asp:Button ID="ButtonReset" runat="server" CausesValidation="False"  Text="Reset" CssClass ="small button gray">
            </asp:Button>
        </div>                  
        </asp:Panel>
        <asp:Panel ID="pnlAddEdit" runat="server">
        <div class="form_box_header">
            <div class="form_single">              
                <h4>
                    PEMILIK SUPPLIER-<asp:Label ID="lblTitle" runat="server"></asp:Label>
                </h4>
            </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label class ="label_req">
				Nama</label>
                <asp:TextBox ID="txtName" runat="server"  MaxLength="50" Columns="55"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtName" CssClass="validator_general"
                    ErrorMessage="Harap isi Nama" Display="Dynamic"></asp:RequiredFieldValidator>
	        </div>
        </div>  
         <div class="form_box">
	        <div class="form_single">
                <label>Jabatan</label>
                <asp:TextBox ID="txtTitle" runat="server"  MaxLength="50" Columns="55"></asp:TextBox>
	        </div>
        </div>
         <div class="form_box">
	        <div class="form_single">
                 <label class ="label_req">
				Tempat / Tanggal Lahir</label>
                <asp:TextBox ID="txtBirthPlace" runat="server"  MaxLength="50"
                    Columns="30"></asp:TextBox>
                                                  
                <%--
                <asp:TextBox ID="txtuscBirthDate" runat="server"></asp:TextBox>                   
                <asp:CalendarExtender ID="txtuscBirthDate_CalendarExtender" runat="server" 
                    Enabled="True" TargetControlID="txtuscBirthDate" Format ="dd/MM/yyyy"></asp:CalendarExtender>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*" CssClass="validator_general"
                ControlToValidate="txtuscBirthDate"></asp:RequiredFieldValidator>--%>
	             <uc2:ucDateCE ID="txtuscBirthDate" runat="server" />
	        </div>
        </div>
            <%--edit npwp, ktp, telepon by ario--%>
         <div class="form_box">
	        <div class="form_single">
                <label class="label_req">NPWP</label>
                <asp:TextBox ID="txtNPWP" runat="server"  MaxLength="20" Columns="25"></asp:TextBox>
                <asp:TextBox ID="TextBox1" runat="server" MaxLength="30" Columns="35"></asp:TextBox>
                <asp:RegularExpressionValidator ID="Regularexpressionvalidator4" runat="server" Display="Dynamic"
                ValidationExpression="[0-9]{0,10}" ErrorMessage="Harap isi Nomor NPWP dengan Angka"
                ControlToValidate="txtNPWP"  CssClass ="validator_general"></asp:RegularExpressionValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtNPWP"
                ErrorMessage="Harap isi Nomor NPWP" Display="Dynamic" CssClass ="validator_general"></asp:RequiredFieldValidator>
	        </div>
        </div>
         <div class="form_box">
	        <div class="form_single">
                <label>ID Jenis Dokumen</label>
                <asp:DropDownList ID="cboIDType" runat="server">
                </asp:DropDownList>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>No ID Dokumen</label>
                <asp:TextBox ID="txtIDNumber" runat="server"  MaxLength="20" Columns="15"></asp:TextBox>
	        </div>
        </div> 
        <div class="form_box_header">
            <div class="form_single">              
                <h4>
                    Alamat
                </h4>
            </div>
        </div>
        <div class="form_box_uc">
                <uc1:UcCompanyAdress id="ucAddress" runat="server"></uc1:UcCompanyAdress>	        
        </div> 
        <div class="form_box">
	        <div class="form_single">
                <label>HandPhone</label>
                <asp:TextBox ID="txtMobile" runat="server"  MaxLength="20" Columns="25"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtMobile" CssClass="validator_general"
                    ErrorMessage="No HandPhone salah" ValidationExpression="\d*"></asp:RegularExpressionValidator>
	        </div>
        </div> 
        <div class="form_box">
	        <div class="form_single">
                <label>e-mail</label>
                <asp:TextBox ID="txtEmail" runat="server"  MaxLength="30" Columns="35"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEmail" CssClass="validator_general"
                    ErrorMessage="E-mail Salah" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
	        </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonOK" runat="server" CausesValidation="False" Text="Ok" CssClass ="small button blue">
            </asp:Button>
            <asp:Button ID="ButtonSave" runat="server" Text="Save" CssClass ="small button blue">
            </asp:Button>
            <asp:Button ID="ButtonCancel" runat="server" CausesValidation="False"  Text="Cancel" CssClass ="small button gray">
            </asp:Button>
        </div>           
        </asp:Panel>    
    </ContentTemplate>
    </asp:UpdatePanel>                
    </form>
</body>
</html>
