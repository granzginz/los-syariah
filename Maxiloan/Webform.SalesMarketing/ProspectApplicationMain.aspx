﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ProspectApplicationMain.aspx.vb"
    Inherits="Maxiloan.Webform.SalesMarketing.ProspectApplicationMain" %>

<%@ Register TagPrefix="cc1" Namespace="System.Web.UI.WebControls" Assembly="System.Web" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../webform.UserController/ucProspectTab.ascx" TagName="ucProspectTab" TagPrefix="uc1" %>
<%@ Register Src="../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat" TagPrefix="uc1" %>
<%@ Register Src="../webform.UserController/ucDateCE.ascx" TagName="ucDateCE" TagPrefix="uc1" %>
<%@ Register TagPrefix="uc1" TagName="ucAddress" Src="../Webform.UserController/ucCompanyAddress.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Prospect Maintenance</title>
    <link rel="Stylesheet" type="text/css" href="../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../Include/Buttons.css" />
    
       <!-- Global stylesheets -->
    <link href="../lib/limitless/assets/css/fonts.css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css" />
    <link href="../lib/limitless/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css" />
    <link href="../lib/limitless/assets/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="../lib/limitless/assets/css/core.css" rel="stylesheet" type="text/css" />
    <link href="../lib/limitless/assets/css/components.css" rel="stylesheet" type="text/css" />
    <link href="../lib/limitless/assets/css/colors.css" rel="stylesheet" type="text/css" />
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <%--<script type="text/javascript" src="lib/limitless/assets/js/plugins/loaders/pace.min.js"></script>--%>
    <script type="text/javascript" src="../lib/limitless/assets/js/core/libraries/jquery.min.js"></script>
    <script type="text/javascript" src="../lib/limitless/assets/js/core/libraries/bootstrap.min.js"></script>
    <script type="text/javascript" src="../lib/limitless/assets/js/plugins/loaders/blockui.min.js"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="../lib/limitless/assets/js/plugins/forms/styling/uniform.min.js"></script>

    <script type="text/javascript" src="../lib/limitless/assets/js/plugins/notifications/pnotify.min.js"></script>
    <script type="text/javascript" src="../lib/limitless/assets/js/pages/components_notifications_pnotify.js"></script>

    <!-- /theme JS files -->

    <script src="../Maxiloan.js" type="text/javascript"></script>
    <link href="../include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <%--<script src="../js/jquery-1.9.1.min.js"></script>--%>
    <script src="../js/jquery-ui-1.10.3.custom.min.js"></script>
     <script language="javascript" type="text/javascript">
         //modify by nofi tgl 30jan2018 =>hasil testing pak agus rdoReferensiChange()
         $(document).ready(function () {
             rdoReferensiChange();
         });
         function rdoReferensiChange() {
             if (document.forms[0].rdoReferensi_1.checked) {
                 var theButton = document.getElementById('<%=txtPhoneReferensi.ClientID%>');
                 theButton.disabled = true;
             }
             else {
                 var theButton = document.getElementById('<%=txtPhoneReferensi.ClientID%>');
                 theButton.disabled = false;
             }
         }
    </script>
</head>
<body>
    <asp:UpdateProgress ID="upg1" runat="server" AssociatedUpdatePanelID="upnl1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <div runat="server" id="jlookupContent" />
    <asp:UpdatePanel runat="server" ID="upnl1" UpdateMode="Conditional" ChildrenAsTriggers="false">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
                onclick="hideMessage();"></asp:Label>
            <uc1:ucProspectTab id="ucProspectTab1" runat="server" />
            <div class="form_title">
                <div class="form_single">
                    <h3>
                        DATA APLIKASI</h3>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="label_req">Tanggal Aplikasi</label>
<%--                        <asp:TextBox ID="txttanggalAplikasi" runat="server" CssClass="small_text" ></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txttanggalAplikasi"
                            Format="dd/MM/yyyy">
                        </asp:CalendarExtender>--%>
                        <asp:Label ID="lbltanggalAplikasi" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="label_req">
                           Nama
                        </label>
<%--                        <asp:TextBox ID="txtNama" runat="server" CssClass="medium_text"  MaxLength="50"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" display="Dynamic"
                            ControlToValidate="txtNama" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>--%>
                            <asp:Label ID="lblNama" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                         <label class="label_req">
                            Tempat / Tanggal Lahir</label>
<%--                        <asp:TextBox ID="txtTempatLahir" runat="server" MaxLength="20" CssClass="medium_text"></asp:TextBox> / 
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" display="Dynamic"
                            ControlToValidate="txtTempatLahir" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
                        <asp:TextBox ID="txtTanggalLahir" runat="server" CssClass="small_text"></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtTanggalLahir"
                            Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator3" runat="server" display="Dynamic"
                            ControlToValidate="txtTanggalLahir" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>--%>
                            <asp:Label ID="lblTempatLahir" runat="server"></asp:Label> /
                            <asp:Label ID="lblTanggalLahir" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box_title">
                <div class="form_single">
                    <h4>
                       ADDRESS</h4>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="label_req">Alamat</label>
                        <asp:Label ID="lblAlamat" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="label">RT/RW</label>
                        <asp:Label ID="lblRT" runat="server"></asp:Label>
                        <asp:Label ID="lblMiring" runat="server">/</asp:Label>
                        <asp:Label ID="LblRW" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="label">Kelurahan</label>
                        <asp:Label ID="lblKelurahan" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="label">Kecamatan</label>
                        <asp:Label ID="lblKecamatan" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="label">Kota</label>
                        <asp:Label ID="lblCity" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="label">Kode Pos</label>
                        <asp:Label ID="lblZipCode" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="label">Telepon 1</label>
                        <asp:Label ID="lblAreaPhone1" runat="server"></asp:Label>
                        <asp:Label ID="lblStrip1" runat="server">-</asp:Label>
                        <asp:Label ID="lblPhone1" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="label">Telepon 2</label>
                        <asp:Label ID="lblAreaPhone2" runat="server"></asp:Label>
                        <asp:Label ID="lblStrip2" runat="server">-</asp:Label>
                        <asp:Label ID="lblPhone2" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="label">Fax</label>
                        <asp:Label ID="lblAreaFax" runat="server"></asp:Label>
                        <asp:Label ID="lblStripFax" runat="server">-</asp:Label>
                        <asp:Label ID="lblFax" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
<%--            <div class="form_box">
                    <uc1:ucAddress id="UCMailingAddress" runat="server"></uc1:ucAddress>
            </div>--%>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="label_req">
                           Mobile Phone
                        </label>
                        <%--<asp:TextBox ID="txtMobilePhone" runat="server" CssClass="medium_text"  MaxLength="20" onkeypress="return numbersonly2(event)"></asp:TextBox>--%>
                        <asp:Label ID="lblMobilePhone" runat="server"></asp:Label>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtMobilePhone" ErrorMessage="Harap isi Mobile Phone" Display="Dynamic" CssClass ="validator_general" />--%>
                    </div>
                </div>
            </div>
            <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                           Nama Ibu Kandung
                        </label>
                        <asp:Label ID="lblMotherName" runat="server"></asp:Label>
                    </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label>No Telepon Kantor </label>
                        <asp:TextBox ID="TxtAreaPhoneKantor" MaxLength="4" runat="server" Width="35px" ></asp:TextBox>-
                        <asp:TextBox ID="TxtPhoneKantor" MaxLength="10" runat="server"  Width="75px"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="Regularexpressionvalidator8" runat="server" Display="Dynamic" ValidationExpression="[0-9]{0,4}" ErrorMessage="Harap isi Area Telepon dengan Angka" ControlToValidate="TxtAreaPhoneKantor" CssClass="validator_general" />
                        <asp:RegularExpressionValidator ID="Regularexpressionvalidator9" runat="server" Display="Dynamic" ValidationExpression="[0-9]{0,10}" ErrorMessage="Harap isi Telepon  dengan Angka" ControlToValidate="TxtPhoneKantor" CssClass="validator_general" />
                    </div>
                </div>
            </div>

              <div class="form_box_hide">
                <div>
                    <div class="form_single">
                        <label>No Telepon Penjamin </label>
                        <asp:TextBox ID="TxtAreaPhoneGuarantor" runat="server" Width="35px" ></asp:TextBox>-
                        <asp:TextBox ID="TxtPhoneGuarantor" runat="server"  Width="75px"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="Regularexpressionvalidator1" runat="server" Display="Dynamic" ValidationExpression="[0-9]{0,4}" ErrorMessage="Harap isi Area Telepon dengan Angka" ControlToValidate="TxtAreaPhoneGuarantor" CssClass="validator_general" />
                        <asp:RegularExpressionValidator ID="Regularexpressionvalidator2" runat="server" Display="Dynamic" ValidationExpression="[0-9]{0,10}" ErrorMessage="Harap isi Telepon  dengan Angka" ControlToValidate="TxtPhoneGuarantor" CssClass="validator_general" />
                    </div>
                </div>
            </div>

            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="">
                           Jarak Survey
                        </label>
                        <%--<uc1:ucnumberformat runat="server" id="ucJarakSurvey"></uc1:ucnumberformat>--%>
                        <asp:Label ID="lblJarakSurvey" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="">
                           GPS Latitude
                        </label>
                        <asp:Label ID="lblGpsLatitude" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="">
                           GPS Longitude
                        </label>
                        <asp:Label ID="lblGpsLongtitude" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="">
                           Jenis Dokumen
                        </label>
                        <asp:DropDownList ID="cboIDTypeP" runat="server" CssClass="select" Enabled= "false">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Harap pilih Jenis Identitas"
                            ControlToValidate="cboIDTypeP" Display="Dynamic" InitialValue="Select One" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="label_req">
                           Nomor Dokumen 
                        </label>
<%--                        <asp:TextBox ID="txtIDNumberP" runat="server" MaxLength="25" CssClass="medium_text"
                            onkeypress="return numbersonly2(event)"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegEx1" ControlToValidate="txtIDNumberP" runat="server"
                            ErrorMessage="Masukkan angka saja" ValidationExpression="\d+" CssClass="validator_general"></asp:RegularExpressionValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Harap isi No Identitas"
                            ControlToValidate="txtIDNumberP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                            <asp:Label ID="lblIDNumberP" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="label_general">
                           Referensi
                        </label>
                        <asp:RadioButtonList ID="rdoReferensi" onclick="<%#rdoReferensiChange()%>" runat="server" RepeatDirection="Horizontal" CssClass="opt_single"  >
                                <asp:ListItem Value="1" Selected="True">Ya</asp:ListItem>
                                <asp:ListItem Value="0">Tidak</asp:ListItem>
                            </asp:RadioButtonList> 
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="">
                           Keterangan Referal
                        </label>
                        <asp:TextBox ID="txtKeteranganReferal" runat="server" TextMode="MultiLine" CssClass="multiline_textbox"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="">
                           No Telp Referensi/Penjamin
                        </label>
                        <asp:TextBox ID="txtPhoneReferensi" runat="server" CssClass="medium_text"  MaxLength="12" onkeypress="return numbersonly2(event)"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="label_general">
                           Status Nasabah
                        </label>
                        <asp:RadioButtonList ID="rdostatusnasabah" runat="server" RepeatDirection="Horizontal"
                                CssClass="opt_single">
                                <asp:ListItem Value="0" Selected="True">Baru</asp:ListItem>
                                <asp:ListItem Value="1">Existing</asp:ListItem>
                            </asp:RadioButtonList>
                    </div>
                </div>
            </div>
            <asp:Panel ID="PanelInfo" runat="server">
                <div class="form_box_title">
                    <div class="form_single">
                        <h4>
                            Similar Customer Data</h4>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ws">
                            <asp:DataGrid id="dtgList1" runat="server" Width="100%" AutoGenerateColumns="False" BorderWidth="0"
									cellpadding="3" cellspacing="1" DataKeyField="Name" CssClass="grid_general">
									<HeaderStyle CssClass="th"></HeaderStyle>
                                    <ItemStyle CssClass="item_grid" />
									<Columns>
										<asp:TemplateColumn HeaderText="NO">
											<ItemStyle HorizontalAlign="CENTER"></ItemStyle>
											<ItemTemplate>
												<asp:Label ID="lblNo1" Runat="server"></asp:Label>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="NAME">
											<ItemStyle HorizontalAlign="left"></ItemStyle>
											<ItemTemplate>
												<asp:linkbutton id="lnkCustName" runat="server" CausesValidation=false text='<%# DataBinder.eval(Container,"DataItem.Name") %>'>
												</asp:linkbutton>
												<asp:Label ID="lblCust" Runat=server Visible=False text='<%# DataBinder.eval(Container,"DataItem.CustomerID") %>'>
												</asp:Label>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:BoundColumn DataField="IDType" HeaderText="ID TYPE"></asp:BoundColumn>
										<asp:BoundColumn DataField="IDNumber" HeaderText="ID NUMBER"></asp:BoundColumn>
										<asp:BoundColumn DataField="BirthDate" ItemStyle-HorizontalAlign="Center" HeaderText="BIRTH DATE"
											DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
										<asp:BoundColumn DataField="Type" ItemStyle-HorizontalAlign="left" HeaderText="TYPE"></asp:BoundColumn>
									</Columns>
								</asp:DataGrid>
                        </div>
                    </div>
                </div>
                <div class="form_box_title">
                    <div class="form_single">
                        <h4>
                            Negative List</h4>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ws">
                            <asp:DataGrid id="dtgList2" runat="server" Width="100%" AutoGenerateColumns="False" BorderWidth="0"
									cellpadding="3" cellspacing="1" DataKeyField="Name" CssClass="grid_general">
									<HeaderStyle CssClass="th"></HeaderStyle>
                                    <ItemStyle CssClass="item_grid" />
									<Columns>
										<asp:TemplateColumn HeaderText="NO">
											<ItemStyle HorizontalAlign="CENTER"></ItemStyle>
											<ItemTemplate>
												<asp:Label ID="lblNo2" Runat="server"></asp:Label>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="NAME">
											<ItemStyle HorizontalAlign="left"></ItemStyle>
											<ItemTemplate>
												<asp:linkbutton id="lnkCustName2" runat="server" CausesValidation=false text='<%# DataBinder.eval(Container,"DataItem.Name") %>'>
												</asp:linkbutton>
												<asp:Label ID="lblCust2" Runat=server Visible=False text='<%# DataBinder.eval(Container,"DataItem.CustomerID") %>'>
												</asp:Label>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:BoundColumn DataField="IDType" HeaderText="ID TYPE"></asp:BoundColumn>
										<asp:BoundColumn DataField="IDNumber" HeaderText="ID NUMBER"></asp:BoundColumn>
										<asp:BoundColumn ItemStyle-HorizontalAlign="Center" DataField="BirthDate" HeaderText="BIRTH DATE"
											DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
										<asp:BoundColumn DataField="Type" HeaderText="TYPE"></asp:BoundColumn>
									</Columns>
								</asp:DataGrid>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <div class="form_button">
                <asp:Button ID="btnNext" runat="server" Text="Next" CssClass="small button green"
                    CausesValidation="True"></asp:Button>
                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue"
                    CausesValidation="True"></asp:Button>
                <asp:Button ID="btnCancel" runat="server" Text="Back" CssClass="small button gray"
                    CausesValidation="False"></asp:Button>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnNext" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
    </form>
</body>
</html>
