﻿#Region "Imports"
Imports Maxiloan.cbse
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports System.IO
#End Region

Public Class Viewsupplier
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private m_controller As New SupplierController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region
#Region "Property"
    Property SupplierID() As String
        Get
            Return viewstate("SupplierID").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("SupplierID") = Value
        End Set
    End Property
    Property WhereCond2() As String
        Get
            Return viewstate("WhereCond2").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("WhereCond2") = Value
        End Set
    End Property
    Property WhereCond() As String
        Get
            Return viewstate("WhereCond").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("WhereCond") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(viewstate("Sort"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Sort") = Value
        End Set
    End Property
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Me.SessionInvalid Then
            Dim strHTTPServer As String
            Dim strHTTPApp As String
            Dim strNameServer As String
            strHTTPServer = Request.ServerVariables("PATH_INFO")
            strNameServer = Request.ServerVariables("SERVER_NAME")
            strHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
            Response.Redirect("http://" & strNameServer & "/" & strHTTPApp & "/NotAuthorized.html")
        End If

        If Not Page.IsPostBack Then
            Me.SupplierID = Request("SupplierID").ToString
            Me.Sort = "Name ASC"
            Me.WhereCond = "ALL"
            Bindgrid()
            'Supplier Signature dihilangkan
            'Bind("")
        End If        
    End Sub
    Sub Bindgrid()
        Dim oSupplier As New Parameter.Supplier
        Dim oData As New DataTable
        Dim oDataAttribute As New DataTable
        Dim oDataAssetDoc As New DataTable
        oSupplier.strConnection = GetConnectionString
        oSupplier.SupplierID = Me.SupplierID
        oSupplier = m_controller.SupplierEdit(oSupplier)
        If Not oSupplier Is Nothing Then
            oData = oSupplier.ListData
        End If
        If oData.Rows.Count > 0 Then
            SetValueToLabel(oData)
        End If
    End Sub


    'Supplier Signature dihilangkan
    'Sub Bind(ByVal cmdWhere As String)
    '    Dim dtEntity As DataTable
    '    Me.WhereCond2 = "SupplierID = '" & Me.SupplierID & "'"
    '    Dim oCustomClass As New Parameter.Supplier
    '    oCustomClass.PageSize = pageSize
    '    oCustomClass.WhereCond = Me.WhereCond
    '    oCustomClass.SortBy = Me.Sort
    '    oCustomClass.WhereCond2 = Me.WhereCond2
    '    oCustomClass.CurrentPage = currentPage
    '    oCustomClass.strConnection = GetConnectionString
    '    oCustomClass = m_controller.GetSupplierSignature(oCustomClass)
    '    If Not oCustomClass Is Nothing Then
    '        dtEntity = oCustomClass.ListData
    '        recordCount = oCustomClass.TotalRecords
    '    Else
    '        recordCount = 0
    '    End If
    '    dtgPaging.DataSource = dtEntity.DefaultView
    '    dtgPaging.CurrentPageIndex = 0
    '    dtgPaging.DataBind()
    'End Sub
    'Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
    '    If InStr(Me.Sort, "DESC") > 0 Then
    '        Me.SortBy = e.SortExpression
    '    Else
    '        Me.Sort = e.SortExpression + " DESC"
    '    End If
    '    Me.WhereCond = "ALL"
    '    Bind(Me.WhereCond)
    'End Sub

    Sub SetValueToLabel(ByVal oData As DataTable)
        Dim oRow As DataRow
        oRow = oData.Rows(0)
        
        lblName.Text = oRow.Item("SupplierName").ToString.Trim
        lblNPWP.Text = oRow.Item("NPWP").ToString.Trim
        lblTDP.Text = oRow.Item("TDP").ToString.Trim
        lblSIUP.Text = oRow.Item("SIUP").ToString.Trim
        lblAddress.Text = oRow.Item("SupplierAddress").ToString.Trim
        lblRT.Text = oRow.Item("SupplierRT").ToString.Trim
        lblRW.Text = oRow.Item("SupplierRW").ToString.Trim
        lblKelurahan.Text = oRow.Item("SupplierKelurahan").ToString.Trim
        lblKecamatan.Text = oRow.Item("SupplierKecamatan").ToString.Trim
        lblCity.Text = oRow.Item("SupplierCity").ToString.Trim
        lblZip.Text = oRow.Item("SupplierZipCode").ToString.Trim
        lblAPhone1.Text = oRow.Item("SupplierAreaPhone1").ToString.Trim
        lblPhone1.Text = oRow.Item("SupplierPhone1").ToString.Trim
        lblAPhone2.Text = oRow.Item("SupplierAreaPhone2").ToString.Trim
        lblPhone2.Text = oRow.Item("SupplierPhone2").ToString.Trim
        lblAFax.Text = oRow.Item("SupplierAreaFax").ToString.Trim
        lblFax.Text = oRow.Item("SupplierFax").ToString.Trim
        lblCPName.Text = oRow.Item("ContactPersonName").ToString.Trim
        lblCPJobTitle.Text = oRow.Item("ContactPersonJobTitle").ToString.Trim
        lblCPEmail.Text = oRow.Item("ContactPersonEmail").ToString.Trim
        lblCPMobile.Text = oRow.Item("ContactPersonHP").ToString.Trim
        If oRow.Item("SupplierStartDate").ToString.Trim <> "" Then
            lblStartDate.Text = Format(oRow.Item("SupplierStartDate"), "dd/MM/yyyy")
        End If
        If oRow.Item("SupplierBadStatus").ToString.Trim = "N" Then
            lblBadStatus.Text = "Normal"
        ElseIf oRow.Item("SupplierBadStatus").ToString.Trim = "B" Then
            lblBadStatus.Text = "Bad"
        ElseIf oRow.Item("SupplierBadStatus").ToString.Trim = "W" Then
            lblBadStatus.Text = "Warning"
        End If
        If oRow.Item("SupplierLevelStatus").ToString.Trim = "U" Then
            lblLevel.Text = "Utama"
        ElseIf oRow.Item("SupplierLevelStatus").ToString.Trim = "A" Then
            lblLevel.Text = "Aktif"
        ElseIf oRow.Item("SupplierLevelStatus").ToString.Trim = "B" Then
            lblLevel.Text = "Biasa"
        ElseIf oRow.Item("SupplierLevelStatus").ToString.Trim = "P" Then
            lblLevel.Text = "Pasif"
        End If
        lblSell.Text = IIf(oRow.Item("IsAutomotive").ToString.Trim = "True", "Yes", "No").ToString
        lblBPKB.Text = IIf(oRow.Item("IsUrusBPKB").ToString.Trim = "True", "Yes", "No").ToString
        If oRow.Item("SupplierCategory").ToString.Trim = "MD" Then
            lblCategory.Text = "Main Dealer"
        ElseIf oRow.Item("SupplierCategory").ToString.Trim = "AD" Then
            lblCategory.Text = "Authorized Dealer"
        ElseIf oRow.Item("SupplierCategory").ToString.Trim = "SD" Then
            lblCategory.Text = "Sub Dealer"
        ElseIf oRow.Item("SupplierCategory").ToString.Trim = "SR" Then
            lblCategory.Text = "Show Room"
        ElseIf oRow.Item("SupplierCategory").ToString.Trim = "OT" Then
            lblCategory.Text = "Others"
        ElseIf oRow.Item("SupplierCategory").ToString.Trim = "PK" Then
            lblCategory.Text = "PK"
        End If


        If oRow.Item("SupplierAssetStatus").ToString.Trim = "N" Then
            lblAsset.Text = "New"
        ElseIf oRow.Item("SupplierAssetStatus").ToString.Trim = "U" Then
            lblAsset.Text = "Used"
        ElseIf oRow.Item("SupplierAssetStatus").ToString.Trim = "M" Then
            lblAsset.Text = "Mixed"
        End If

        lblPKSDate.Text = Format(oRow.Item("PKSDate"), "dd/MM/yyyy")
        lblPKSNo.Text = oRow.Item("NoPKS").ToString.Trim
    End Sub

    'Protected Sub imbClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbClose.Click
    '    imbClose.Attributes.Add("onclick", "Close();")
    'End Sub
End Class