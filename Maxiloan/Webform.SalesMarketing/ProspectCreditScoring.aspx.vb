﻿Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
Imports Maxiloan.Webform.LoanOrg
Imports Maxiloan.Parameter

Public Class ProspectCreditScoring
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents GridNavigator As ucGridNav

    Protected controller As InstallRcv3rdController
    Protected currentPage As Int32 = 1
    Protected pageSize As Int16 = 20
    Protected currentPageNumber As Int16 = 1
    Protected totalPages As Double = 1
    Protected recordCount As Int64 = 1
    Private time As String
    Private Const SCHEME_ID As String = "SCO"

#Region "Property"
    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property


    Private Property ProspekID() As String
        Get
            Return CType(ViewState("ProspekID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ProspekID") = Value
        End Set
    End Property
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("prosCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("prosCmdWhere") = Value
        End Set
    End Property

    Property ActivityDateStart() As DateTime
        Get
            Return ViewState("ActivityDateStart").ToString
        End Get
        Set(ByVal Value As DateTime)
            ViewState("ActivityDateStart") = Value
        End Set
    End Property

    Property ActivityDateEnd() As DateTime
        Get
            Return ViewState("ActivityDateEnd").ToString
        End Get
        Set(ByVal Value As DateTime)
            ViewState("ActivityDateEnd") = Value
        End Set
    End Property

    Property ProspectAppID() As String
        Get
            Return ViewState("ProspectAppID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ProspectAppID") = Value
        End Set
    End Property

    Property lblName() As String
        Get
            Return ViewState("lblName").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("lblName") = Value
        End Set
    End Property

    Private Property CREDITSCORINGCALCULATE As Parameter.CreditScoring_calculate
        Get
            Return CType(ViewState("CREDITSCORINGCALCULATE"), Parameter.CreditScoring_calculate)
        End Get
        Set(ByVal Value As Parameter.CreditScoring_calculate)
            ViewState("CREDITSCORINGCALCULATE") = Value
        End Set
    End Property
    Private Property myDataTable() As DataTable
        Get
            Return CType(ViewState("myDataTable"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            ViewState("myDataTable") = Value
        End Set
    End Property
    Private Property CreditScoreComponentID() As String
        Get
            Return CType(ViewState("CreditScoreComponentID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CreditScoreComponentID") = Value
        End Set
    End Property

    Property ProductID() As String
        Get
            Return ViewState("ProductID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ProductID") = Value
        End Set
    End Property

    Property ProductOfferingID() As String
        Get
            Return ViewState("ProductOfferingID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ProductOfferingID") = Value
        End Set
    End Property
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If

        AddHandler GridNavigator.PageChanged, AddressOf PageNavigation
        If Not Page.IsPostBack Then


            'If IsSingleBranch() And CInt(Replace(Me.sesBranchId, "'", "")) >= 1 And Me.IsHoBranch = False Then

            Me.FormID = "PRPCRDSCR"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            'CmdWhere = String.Format(" Prospect.IsScored=0 and Prospect.IsProceeded=1 and Prospect.BranchID ='{0}'", sesBranchId.Replace("'", "").Trim)
            CmdWhere = String.Format(" Prospect.IsScored=0 and Prospect.IsProceeded=1 and Prospect.BranchID ='{0}' and Prospect.ProspectStep='SVR' ", sesBranchId.Replace("'", "").Trim)
            Sort = "ProspectAppId ASC"
            BindGrid()
            'Else
            '    Dim strHTTPServer As String
            '    Dim StrHTTPApp As String
            '    Dim strNameServer As String
            '    strHTTPServer = Request.ServerVariables("PATH_INFO")
            '    strNameServer = Request.ServerVariables("SERVER_NAME")
            '    StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
            '    Response.Redirect("http://" & strNameServer & "/" & StrHTTPApp & "/error_notauthorized.aspx")
            'End If
            time = Now.Hour.ToString("00") & ":" & Now.Minute.ToString("00") & ":" & Now.Second.ToString("00")
            Me.ActivityDateStart = Me.BusinessDate + " " + time
            pnlScoring.Visible = False
            pnlProceed.Visible = False
        End If
    End Sub
    Protected Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        currentPage = e.CurrentPage
        BindGrid(True)
        GridNavigator.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
    End Sub
    Sub BindGrid(Optional isFrNav As Boolean = False)

        Dim oController As New ProspectController

        Dim oCustomClass As New Parameter.Prospect

        oCustomClass.PageSize = pageSize
        oCustomClass.WhereCond = CmdWhere
        oCustomClass.SortBy = Me.Sort
        oCustomClass.CurrentPage = currentPage
        oCustomClass.strConnection = GetConnectionString()
        oCustomClass = oController.GetProspect(oCustomClass)
        recordCount = oCustomClass.TotalRecords
        dtgPaging.DataSource = oCustomClass.listdata

        Try
            dtgPaging.DataBind()
        Catch
            dtgPaging.CurrentPageIndex = 0
            dtgPaging.DataBind()
        End Try


        If (isFrNav = False) Then
            GridNavigator.Initialize(recordCount, pageSize)
        End If
    End Sub

    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        Sort = String.Format("{0} {1}", e.SortExpression, IIf(InStr(Sort, "DESC") > 0, "", "DESC"))
        BindGrid()
    End Sub


    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        cboSearch.SelectedIndex = 0
        txtSearch.Text = ""
        'CmdWhere = String.Format("Prospect.IsScored=0 and Prospect.IsProceeded=1 and Prospect.BranchID ='{0}'", sesBranchId.Replace("'", "").Trim)
        CmdWhere = String.Format("Prospect.IsScored=0 and Prospect.IsProceeded=1 and Prospect.BranchID ='{0}' and Prospect.ProspectStep='SVR' ", sesBranchId.Replace("'", "").Trim)
        BindGrid()
        pnlScoring.Visible = False
        pnlProceed.Visible = False
        pnlList.Visible = True
        dtgPaging.Visible = True
    End Sub

    Private Sub imbSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        'CmdWhere = String.Format(" Prospect.IsScored=0 and Prospect.IsProceeded=1 and Prospect.BranchID ='{0}'", sesBranchId.Replace("'", "").Trim)
        CmdWhere = String.Format(" Prospect.IsScored=0 and Prospect.IsProceeded=1 and Prospect.BranchID ='{0}' and Prospect.ProspectStep='SVR' ", sesBranchId.Replace("'", "").Trim)
        If txtSearch.Text.Trim <> "" Then
            'CmdWhere = String.Format(" Prospect.IsScored=0 and isProceeded=1 and {0} LIKE '%{1}%' and  Prospect.BranchID ='{2}'", cboSearch.SelectedItem.Value, txtSearch.Text.Replace("%", ""), sesBranchId.Replace("'", "").Trim)
            CmdWhere = String.Format(" Prospect.IsScored=0 and isProceeded=1 and {0} LIKE '%{1}%' and  Prospect.BranchID ='{2}' and Prospect.ProspectStep='SVR' ", cboSearch.SelectedItem.Value, txtSearch.Text.Replace("%", ""), sesBranchId.Replace("'", "").Trim)
        End If

        BindGrid()
    End Sub

    'Private Sub dtgEntity_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
    '    If e.Item.ItemIndex >= 0 Then
    '        Dim id = e.Item.Cells(1).Text.Trim()
    '        Me.ProspectAppID = id
    '        If CheckFeature(Me.Loginid, Me.FormID, "Edit", Me.AppId) Then
    '            If SessionInvalid() Then
    '                Exit Sub
    '            End If

    '        End If


    '        Try
    '            Dim oController As New ProspectController
    '            Dim result = oController.DoProspectCreaditScoring(GetConnectionString, sesBranchId.Replace("'", "").Trim, id, Loginid, BusinessDate)
    '            ProspectLog()
    '            If (result.Trim.Contains("SCORE")) Then
    '                imbReset_Click(Nothing, Nothing)
    '                ShowMessage(lblMessage, String.Format("Score Prospect id {0} telah berhasil dengan {1}", id, result.Trim), False)

    '            ElseIf (result.Trim.Contains("ASSESSMENT")) Then
    '                ProspekID = id
    '                lblProspekId.Text = id
    '                lblScoreResult.Text = result.Split(",")(1)

    '                Dim script = "window.onload = function() { showDialog(); };"
    '                ClientScript.RegisterStartupScript(Me.GetType(), "showDialog", script, True)

    '            Else
    '                ShowMessage(lblMessage, result, True)
    '            End If

    '        Catch ex As Exception
    '            ShowMessage(lblMessage, ex.Message, True)
    '        End Try
    '    End If
    'End Sub

    Private Sub dtgEntity_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        If e.Item.ItemIndex >= 0 Then
            Dim id = e.Item.Cells(1).Text.Trim()
            Me.ProspectAppID = id
            If e.CommandName = "Scoring" Then
                pnlList.Visible = False
                dtgPaging.Visible = False
                pnlScoring.Visible = True

                btnSave.Visible = False


                lblResult.Text = ""
                pnlView.Visible = False

                Dim lblProspectAppID As New Label
                lblProspectAppID = CType(e.Item.FindControl("lblProspectAppID"), Label)

                Dim lblCustomerName1 As New Label
                lblCustomerName1 = CType(e.Item.FindControl("lblName"), Label)

                Dim lblProductID As New Label
                lblProductID = CType(e.Item.FindControl("lblProductID"), Label)

                Dim lblProductOfferingID As New Label
                lblProductOfferingID = CType(e.Item.FindControl("lblProductOfferingID"), Label)

                Me.ProspectAppID = lblProspectAppID.Text
                Me.lblName = lblCustomerName1.Text

                Me.ProductID = lblProductID.Text
                Me.ProductOfferingID = lblProductOfferingID.Text

                lblNoProspect.Text = Me.ProspectAppID
                lblCustomerName.Text = Me.lblName
            End If

        End If
    End Sub

    Private Sub btnGoToSurvey_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGoToSurvey.Click
        Dim oController As New ProspectController
        Try
            Dim result = oController.DoProspectDecision(GetConnectionString, ProspekID, 1)
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
        imbReset_Click(Nothing, Nothing)
    End Sub
    Private Sub btnReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReject.Click
        Dim oController As New ProspectController
        Try
            Dim result = oController.DoProspectDecision(GetConnectionString, ProspekID, 0)
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
        imbReset_Click(Nothing, Nothing)
    End Sub
    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click

        imbReset_Click(Nothing, Nothing)
    End Sub
    Sub ProspectLog()
        Dim oController As New ProspectController
        time = Now.Hour.ToString("00") & ":" & Now.Minute.ToString("00") & ":" & Now.Second.ToString("00")
        Me.ActivityDateEnd = Me.BusinessDate + " " + time

        Dim oApplication As New Parameter.Prospect
        oApplication.strConnection = GetConnectionString()
        oApplication.BranchId = Replace(Me.sesBranchId, "'", "").ToString
        oApplication.ProspectAppID = Me.ProspectAppID
        oApplication.ActivityType = "SCO"
        oApplication.ActivityDateStart = Me.ActivityDateStart
        oApplication.ActivityDateEnd = Me.ActivityDateEnd
        oApplication.ActivityUser = Me.Loginid
        oApplication.ActivitySeqNo = 7

        Dim ErrorMessage As String = ""
        Dim oReturn As New Parameter.Prospect

        oReturn = oController.ProspectLogSave(oApplication)

        If oReturn.Err <> "" Then
            ShowMessage(lblMessage, ErrorMessage, True)
            Exit Sub
        End If
    End Sub


#Region "imgCreditScoring_click"
    Private Sub imgCreditScoring_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCreditScoring.Click
        'lblResult.Text = CalculateCreditScoring(Me.BranchID, Me.ApplicationID)

        Dim scoringData As New Parameter.CreditScoring_calculate
        Dim lblProspectAppID As New Label
        With scoringData
            .strConnection = GetConnectionString()
            .BranchId = sesBranchId
            .ApplicationId = Me.ProspectAppID
            .ProductID = Me.ProductID
            .ProductOfferingID = Me.ProductOfferingID
        End With
        'scoringData = CreditScoringMdl.CalculateCreditScoringSM(scoringData)
        CREDITSCORINGCALCULATE = scoringData
        'set result

        'scoringData.DT = _scoreResults.ToDataTable
        'scoringData.CreditScore = _scoreResults.TotalScore
        'scoringData.CreditScoreResult = _scoreResults.TotalScore

        myDataTable = scoringData.ScoreResults.ToDataTable
        CreditScoreComponentID = scoringData.CreditScoreComponentID
        lblGrade.Text = scoringData.ScoreResults.CuttOff
        lblResult.Text = scoringData.ScoreResults.TotalScore
        lblDecision.Text = scoringData.ScoreResults.FinalDecision

        If lblResult.Text <> "" Then

            btnSave.Visible = (scoringData.ScoreResults.FinalDecision.Trim = "AUTO APPROVE") Or (scoringData.ScoreResults.FinalDecision.Trim = "MANUAL APPROVE")
            btnReject2.Visible = (scoringData.ScoreResults.FinalDecision.Trim = "AUTO REJECT") Or (scoringData.ScoreResults.FinalDecision.Trim = "MANUAL APPROVE")
            btnProceed.Visible = (scoringData.ScoreResults.FinalDecision.Trim = "AUTO REJECT") Or (scoringData.ScoreResults.FinalDecision.Trim = "MANUAL APPROVE")

            pnlList.Visible = False
            pnlScoring.Visible = True
            pnlView.Visible = True

            dtgView.DataSource = Me.myDataTable.Select("ComponentValue ='CRDITSCORE'")
            dtgView.CurrentPageIndex = 0

            dtgView.DataBind()

        End If

    End Sub
#End Region

    Private Sub btnReject2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReject2.Click
        Try
            Dim oController As New ProspectController
            Dim result = oController.DoProspectCreaditScoring(GetConnectionString, sesBranchId.Replace("'", "").Trim, Me.ProspectAppID, Loginid, BusinessDate)
            ProspectLog()
            If (result.Trim.Contains("SCORE")) Then
                imbReset_Click(Nothing, Nothing)
                ShowMessage(lblMessage, String.Format("Score Prospect id {0} telah berhasil dengan {1}", Me.ProspectAppID, result.Trim), False)

            ElseIf (result.Trim.Contains("ASSESSMENT")) Then
                ProspekID = ID
                lblProspekId.Text = ID
                lblScoreResult.Text = result.Split(",")(1)

                Dim script = "window.onload = function() { showDialog(); };"
                ClientScript.RegisterStartupScript(Me.GetType(), "showDialog", script, True)

            Else
                ShowMessage(lblMessage, result, True)
            End If

        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
        imbReset_Click(Nothing, Nothing)
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim oController As New ProspectController
            Dim result = oController.DoProspectCreaditScoring(GetConnectionString, sesBranchId.Replace("'", "").Trim, Me.ProspectAppID, Loginid, BusinessDate)
            ProspectLog()
            If (result.Trim.Contains("SCORE")) Then
                imbReset_Click(Nothing, Nothing)
                ShowMessage(lblMessage, String.Format("Score Prospect id {0} telah berhasil dengan {1}", Me.ProspectAppID, result.Trim), False)

            ElseIf (result.Trim.Contains("ASSESSMENT")) Then
                ProspekID = ID
                lblProspekId.Text = ID
                lblScoreResult.Text = result.Split(",")(1)

                Dim script = "window.onload = function() { showDialog(); };"
                ClientScript.RegisterStartupScript(Me.GetType(), "showDialog", script, True)

            Else
                ShowMessage(lblMessage, result, True)
            End If

        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
        imbReset_Click(Nothing, Nothing)
    End Sub
    Private Sub btnProceed_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProceed.Click
        pnlList.Visible = False
        dtgPaging.Visible = False
        pnlScoring.Visible = True
        pnlView.Visible = True
        pnlProceed.Visible = True

        Dim oData As New DataTable
        oData = Get_UserApproval("SCO", Me.sesBranchId.Replace("'", ""), CDec(1))
        cboApprovedBy.DataSource = oData.DefaultView
        cboApprovedBy.DataTextField = "Name"
        cboApprovedBy.DataValueField = "ID"
        cboApprovedBy.DataBind()
        cboApprovedBy.Items.Insert(0, "Select One")
        cboApprovedBy.Items(0).Value = "0"
    End Sub
    Private Sub SaveProceed_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles SaveProceed.Click

        Try
            Dim oCustomClass As New Parameter.Prospect With {
               .strConnection = GetConnectionString().Trim,
               .ProspectAppID = Me.ProspectAppID
           }
            Dim oEntitiesApproval As New Parameter.Approval
            With oEntitiesApproval
                .BranchId = Me.sesBranchId.Trim
                .SchemeID = SCHEME_ID
                .RequestDate = Me.BusinessDate
                .ApprovalNote = txtNoteApprove.Text.Trim
                .ApprovalValue = 1
                .UserRequest = Me.Loginid
                .UserApproval = cboApprovedBy.SelectedValue
                .AprovalType = Approval.ETransactionType.AmortisasiBiaya_Approval
                .Argumentasi = txtNoteApprove.Text.Trim
                .TransactionNo = Me.ProspectAppID
            End With

            Dim oController As New ProspectController
            oCustomClass.Approval = oEntitiesApproval
            Dim resultapp = oController.GetApprovalprospect(oCustomClass, "R")


            Dim result = oController.DoProspectCreaditScoringProceed(GetConnectionString, sesBranchId.Replace("'", "").Trim, Me.ProspectAppID, Loginid, BusinessDate, 1)
            ProspectLog()
            If (result.Trim.Contains("SCORE")) Then
                imbReset_Click(Nothing, Nothing)
                ShowMessage(lblMessage, String.Format("Score Prospect id {0} telah berhasil dengan {1}", Me.ProspectAppID, result.Trim), False)

            ElseIf (result.Trim.Contains("ASSESSMENT")) Then
                ProspekID = ID
                lblProspekId.Text = ID
                lblScoreResult.Text = result.Split(",")(1)

                Dim script = "window.onload = function() { showDialog(); };"
                ClientScript.RegisterStartupScript(Me.GetType(), "showDialog", script, True)

            Else
                ShowMessage(lblMessage, result, True)
            End If

        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
        imbReset_Click(Nothing, Nothing)
    End Sub
End Class