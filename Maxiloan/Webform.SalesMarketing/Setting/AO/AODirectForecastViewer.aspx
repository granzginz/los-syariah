<%@ Page Language="vb" AutoEventWireup="false" Codebehind="AODirectForecastViewer.aspx.vb" Inherits="Maxiloan.Webform.SalesMarketing.AODirectForecastViewer"%>
<%@ Register TagPrefix="cr" Namespace="CrystalDecisions.Web" Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>AODirectForecastViewer</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="95%" align="center" border="0">
				<TR>
					<TD>
						<asp:ImageButton id="imbBack" runat="server" ImageUrl="../images/ButtonBack.gif"></asp:ImageButton></TD>
				</TR>
				<tr>
					<td>
						<CR:CrystalReportViewer id="crvAOForecast" runat="server"></CR:CrystalReportViewer>
					</td>
				</tr>
			</TABLE>
		</form>
	</body>
</HTML>
