<%@ Page Language="vb" AutoEventWireup="false" Codebehind="AOSupervisorSalesBudget.aspx.vb" Inherits="Maxiloan.Webform.SalesMarketing.AOSupervisorSalesBudget"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>AOSupervisorSalesBudget</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="../Include/Marketing.css" type="text/css" rel="stylesheet">
		<script language="javascript">
			var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
			var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
			var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';			
		</script>
		<SCRIPT src="../Maxiloan.js"></SCRIPT>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<asp:label id="LblErrMessages" runat="server" ForeColor="Red"></asp:label><br>
			<asp:panel id="PnlGrid" runat="server" Width="100%" Height="216px">
				<TABLE id="Table4" border="0" cellSpacing="0" cellPadding="0" width="95%" align="center">
					<TR class="trtopi">
						<TD class="tdtopi" align="center">List Of Marketing Head Budget</TD>
					</TR>
				</TABLE>
				<TABLE id="Table6" border="0" cellSpacing="0" cellPadding="0" width="95%" align="center">
					<TR align="center">
						<TD>
							<asp:DataGrid id="dtgAO" runat="server" Width="100%" CssClass="tablegrid" HorizontalAlign="Center"
								cellspacing="1" cellpadding="3" BorderWidth="0px" OnSortCommand="Sorting" AutoGenerateColumns="False"
								AllowSorting="True">
								<AlternatingItemStyle CssClass="tdgenap"></AlternatingItemStyle>
								<ItemStyle HorizontalAlign="Center" CssClass="tdganjil"></ItemStyle>
								<HeaderStyle HorizontalAlign="Center" CssClass="tdjudul"></HeaderStyle>
								<Columns>
									<asp:TemplateColumn Visible="False" HeaderText="BRANCH ID">
										<HeaderStyle HorizontalAlign="Center" Width="18%"></HeaderStyle>
										<ItemStyle HorizontalAlign="Left" Width="18%"></ItemStyle>
										<ItemTemplate>
											<ASP:label id="lblBranchID" runat="server" text='<%#Container.DataItem("BranchID")%>'>
											</ASP:label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn SortExpression="Year" HeaderText="YEAR">
										<HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
										<ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
										<ItemTemplate>
											<ASP:HYPERLINK id="lnkYear" runat="server" text='<%#Container.DataItem("Year")%>'>
											</ASP:HYPERLINK>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn SortExpression="Month" HeaderText="MONTH">
										<HeaderStyle HorizontalAlign="Center" Width="18%"></HeaderStyle>
										<ItemStyle HorizontalAlign="Left" Width="18%"></ItemStyle>
										<ItemTemplate>
											<ASP:label id="lblMonthName" runat="server" text='<%#Container.DataItem("Month")%>'>
											</ASP:label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn SortExpression="AssetStatus" HeaderText="ASSET STATUS">
										<HeaderStyle HorizontalAlign="Center" Width="18%"></HeaderStyle>
										<ItemStyle HorizontalAlign="Left" Width="18%"></ItemStyle>
										<ItemTemplate>
											<ASP:LABEL id="lblAOLevel" runat="server" text='<%#Container.DataItem("AssetStatus")%>'>
											</ASP:LABEL>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn SortExpression="Unit" HeaderText="UNIT">
										<HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
										<ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
										<ItemTemplate>
											<ASP:LABEL id="lblUnit" runat="server" text='<%#Container.DataItem("Unit")%>'>
											</ASP:LABEL>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn SortExpression="Amount" HeaderText="AMOUNT">
										<HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
										<ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
										<ItemTemplate>
											<ASP:LABEL id="lblAmount" runat="server" text='<%#formatnumber(Container.DataItem("Amount"),2)%>'>
											</ASP:LABEL>
										</ItemTemplate>
									</asp:TemplateColumn>
								</Columns>
							</asp:DataGrid></TD>
					</TR>
				</TABLE>
				<TABLE class="nav_tbl" cellSpacing="0" cellPadding="0" align="center">
					<TR>
						<TD class="nav_tbl_td1">
							<TABLE class="nav_command_tbl" cellSpacing="0" cellPadding="0">
								<TR>
									<TD>
										<asp:ImageButton id="imbPrint" runat="server" ImageUrl="../Images/ButtonPrint.gif" Enabled="true"></asp:ImageButton></TD>
									<TD>
										<asp:ImageButton id="imbBack" runat="server" ImageUrl="../images/buttonback.gif" CausesValidation="False"></asp:ImageButton></TD>
								</TR>
							</TABLE>
						</TD>
						<TD class="nav_tbl_td2">
							<TABLE class="nav_page_tbl" cellSpacing="0" cellPadding="0">
								<TR>
									<TD>
										<asp:imagebutton id="imbFirstPage" runat="server" ImageUrl="../Images/butkiri1.gif" CausesValidation="False"
											CommandName="First" OnCommand="NavigationLink_Click"></asp:imagebutton><FONT face="Verdana"></FONT></TD>
									<TD>
										<asp:imagebutton id="imbPrevPage" runat="server" ImageUrl="../Images/butkiri.gif" CausesValidation="False"
											CommandName="Prev" OnCommand="NavigationLink_Click"></asp:imagebutton><FONT face="Verdana"></FONT></TD>
									<TD>
										<asp:imagebutton id="imbNextPage" runat="server" ImageUrl="../Images/butkanan.gif" CausesValidation="False"
											CommandName="Next" OnCommand="NavigationLink_Click"></asp:imagebutton><FONT face="Verdana"></FONT></TD>
									<TD>
										<asp:imagebutton id="imbLastPage" runat="server" ImageUrl="../Images/butkanan1.gif" CausesValidation="False"
											CommandName="Last" OnCommand="NavigationLink_Click"></asp:imagebutton><FONT face="Verdana"></FONT></TD>
									<TD>Page&nbsp;
										<asp:TextBox id="txtGoPage" runat="server" Width="34px" CssClass="InpType">1</asp:TextBox></TD>
									<TD>
										<asp:imagebutton id="imbGoPage" runat="server" ImageUrl="../Images/butgo.gif" EnableViewState="False"></asp:imagebutton></TD>
									<ASP:RANGEVALIDATOR id="rgvGo" runat="server" controltovalidate="txtGopage" minimumvalue="1" errormessage="Page No. is not valid"
										maximumvalue="999999999" type="Integer" forecolor="#993300" font-name="Verdana" font-size="11px"></ASP:RANGEVALIDATOR>
									<ASP:REQUIREDFIELDVALIDATOR id="rfvGo" runat="server" controltovalidate="txtGopage" errormessage="Page No. is not valid"
										forecolor="#993300" font-name="Verdana" font-size="11px" Display="Dynamic"></ASP:REQUIREDFIELDVALIDATOR></TR>
							</TABLE>
						</TD>
					</TR>
					<TR>
						<TD class="nav_totpage" colSpan="2">
							<asp:label id="lblPage" runat="server"></asp:label>&nbsp;of
							<asp:label id="lblTotPage" runat="server"></asp:label>, Total&nbsp;
							<asp:label id="lblrecord" runat="server"></asp:label>&nbsp;record(s)
						</TD>
					</TR>
				</TABLE>
			</asp:panel>
			<TABLE cellSpacing="0" cellPadding="0" width="95%" align="center" border="0">
				<TR class="trtopi">
					<TD class="tdtopi" align="center">
						Find Marketing Head
					</TD>
				</TR>
			</TABLE>
			<table class="tablegrid" cellSpacing="1" cellPadding="2" width="95%" align="center" border="0">
				<tr class="tdganjil">
					<td class="tdgenap" width="20%">MKTHD &nbsp;ID</td>
					<td class="tdganjil" width="30%" colSpan="5"><asp:hyperlink id="linkAOID" Runat="server"></asp:hyperlink></td>
				</tr>
				<tr class="tdganjil">
					<td class="tdgenap" width="20%">MKTHD Name</td>
					<td class="tdganjil" width="50%" colSpan="5"><asp:label id="lblName" Runat="server"></asp:label></td>
				</tr>
				<tr class="tdganjil">
					<td class="tdgenap" width="10%">Year <FONT color="#ff6666">(yyyy)</FONT></td>
					<td class="tdganjil" width="20%"><asp:textbox id="txtYear" runat="server" CssClass="inptype"></asp:textbox><asp:rangevalidator id="rvYear" runat="server" Display="Dynamic" ErrorMessage="Year Must Be In Numeric"
							ControlToValidate="txtYear" MinimumValue="1900" Type="Integer" MaximumValue="2200">Year Must Be In Numeric</asp:rangevalidator></td>
					<td class="tdgenap" width="10%">Month</td>
					<td class="tdganjil" width="20%"><asp:dropdownlist id="cboMonth" runat="server">
							<asp:ListItem Value="0">All</asp:ListItem>
							<asp:ListItem Value="1">January</asp:ListItem>
							<asp:ListItem Value="2">February</asp:ListItem>
							<asp:ListItem Value="3">March</asp:ListItem>
							<asp:ListItem Value="4">April</asp:ListItem>
							<asp:ListItem Value="5">May</asp:ListItem>
							<asp:ListItem Value="6">June</asp:ListItem>
							<asp:ListItem Value="7">July</asp:ListItem>
							<asp:ListItem Value="8">August</asp:ListItem>
							<asp:ListItem Value="9">September</asp:ListItem>
							<asp:ListItem Value="10">October</asp:ListItem>
							<asp:ListItem Value="11">November</asp:ListItem>
							<asp:ListItem Value="12">December</asp:ListItem>
						</asp:dropdownlist></td>
					<td class="tdgenap" width="10%">Asset Status</td>
					<td class="tdganjil" width="20%"><asp:dropdownlist id="cboAssetStatus" runat="server">
							<asp:ListItem Value="All">All</asp:ListItem>
							<asp:ListItem Value="N">New</asp:ListItem>
							<asp:ListItem Value="U">Used</asp:ListItem>
						</asp:dropdownlist></td>
				</tr>
			</table>
			<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="95%" align="center" border="0">
				<TR>
					<TD width="50%" height="30">
						<asp:imagebutton id="imbSearch" Runat="server" ImageUrl="../Images/ButtonSearch.gif" CausesValidation="True"></asp:imagebutton>&nbsp;
						<asp:imagebutton id="imbReset" Runat="server" ImageUrl="../Images/ButtonReset.gif" CausesValidation="False"></asp:imagebutton></TD>
					<TD align="right" width="50%"></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
