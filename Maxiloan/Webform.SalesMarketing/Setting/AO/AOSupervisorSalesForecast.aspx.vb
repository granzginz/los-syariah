
#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.General
Imports Maxiloan.Controller
#End Region
Public Class AOSupervisorSalesForecast
    Inherits Maxiloan.Webform.WebBased
#Region "Constanta"
    Private m_AOForecast As New AOSupervisorSalesController
    Private oAOForecast As New Parameter.AOSupervisorSales
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Protected WithEvents txtGoPage As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblrecordRec As System.Web.UI.WebControls.Label
    Private currentPage As Integer = 1
#End Region
#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblRecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If

    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        bindgrid(Me.Year, Me.Month, Me.AssetStatus, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbGoPage.Click

        If txtPage.Text = "" Then
            txtPage.Text = "0"
        Else
            If IsNumeric(txtPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                    currentPage = CType(txtPage.Text, Int32)
                    bindgrid(Me.Year, Me.Month, Me.AssetStatus, Me.SortBy)
                End If
            End If
        End If


    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        bindgrid(Me.Year, Me.Month, Me.AssetStatus, Me.SortBy)
    End Sub
#End Region
#Region "properties"
    Public Property Name() As String
        Get
            Return CType(viewstate("Name"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Name") = Value
        End Set
    End Property
    Public Property EmployeeID() As String
        Get
            Return CType(viewstate("EmployeeID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("EmployeeID") = Value
        End Set
    End Property
    Public Property AssetStatus() As String
        Get
            Return CType(viewstate("AssetStatus"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AssetStatus") = Value
        End Set
    End Property
    Public Property Year() As Integer
        Get
            Return CType(viewstate("Year"), Integer)
        End Get
        Set(ByVal Value As Integer)
            viewstate("Year") = Value
        End Set
    End Property

    Public Property Month() As Integer
        Get
            Return CType(viewstate("Month"), Integer)
        End Get
        Set(ByVal Value As Integer)
            viewstate("Month") = Value
        End Set
    End Property



#End Region
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents LblErrMessages As System.Web.UI.WebControls.Label
    Protected WithEvents linkAOID As System.Web.UI.WebControls.HyperLink
    Protected WithEvents lblName As System.Web.UI.WebControls.Label
    Protected WithEvents txtYear As System.Web.UI.WebControls.TextBox
    Protected WithEvents rvYear As System.Web.UI.WebControls.RangeValidator
    Protected WithEvents cboMonth As System.Web.UI.WebControls.DropDownList
    Protected WithEvents cboAssetStatus As System.Web.UI.WebControls.DropDownList
    Protected WithEvents imbSearch As System.Web.UI.WebControls.ImageButton
    Protected WithEvents imbReset As System.Web.UI.WebControls.ImageButton
    Protected WithEvents dtgAO As System.Web.UI.WebControls.DataGrid
    Protected WithEvents imbFirstPage As System.Web.UI.WebControls.ImageButton
    Protected WithEvents imbPrevPage As System.Web.UI.WebControls.ImageButton
    Protected WithEvents imbNextPage As System.Web.UI.WebControls.ImageButton
    Protected WithEvents imbLastPage As System.Web.UI.WebControls.ImageButton
    Protected WithEvents txtPage As System.Web.UI.WebControls.TextBox
    Protected WithEvents imbGoPage As System.Web.UI.WebControls.ImageButton
    Protected WithEvents rgvGo As System.Web.UI.WebControls.RangeValidator
    Protected WithEvents rfvGo As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents lblPage As System.Web.UI.WebControls.Label
    Protected WithEvents lblTotPage As System.Web.UI.WebControls.Label
    Protected WithEvents lblrecord As System.Web.UI.WebControls.Label
    Protected WithEvents imbBack As System.Web.UI.WebControls.ImageButton
    Protected WithEvents imbPrint As System.Web.UI.WebControls.ImageButton
    Protected WithEvents PnlGrid As System.Web.UI.WebControls.Panel

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Me.FormID = "MktAOSpvForecast"
        If Not Page.IsPostBack Then
            Me.BranchID = Request.QueryString("BranchID")
            Me.EmployeeID = Request.QueryString("EmployeeID")
            Me.Name = Request.QueryString("Name")

        End If


        If CheckForm(Me.Loginid, Me.FormID, "Eloan") Then
            InitialDefaultPanel()
            linkAOID.Text = Me.EmployeeID.Trim
            linkAOID.NavigateUrl = "javascript:OpenAO('Marketing', '" & Me.BranchID.Trim & "','" & Me.EmployeeID.Trim & "')"
            lblName.Text = Me.Name.Trim
        Else
            Dim strHTTPServer As String
            Dim strHTTPApp As String
            Dim strNameServer As String
            strHTTPServer = Request.ServerVariables("PATH_INFO")
            strNameServer = Request.ServerVariables("SERVER_NAME")
            strHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
            Response.Redirect("http://" & strNameServer & "/" & strHTTPApp & "/NotAuthorized.html")
        End If
    End Sub
#Region " Sub and Functions "
    Private Sub InitialDefaultPanel()
        PnlGrid.Visible = False
        LblErrMessages.Text = ""

    End Sub
    'sub ini untuk bind data hasil search ke datagrid
    Private Sub bindgrid(ByVal year As Integer, ByVal month As Integer, ByVal assetstatus As String, ByVal sortby As String)
        With oAOForecast
            .strConnection = GetConnectionString()
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = sortby
            .BranchId = Me.BranchID
            .EmployeeID = Me.EmployeeID
            .Year = Me.Year
            .Month = month
            .AssetStatus = assetstatus
        End With
        Try
            oAOForecast = m_AOForecast.AOSupervisorForecastListing(oAOForecast)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        With oAOForecast
            lblRecord.Text = CType(.TotalRecord, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecord
        End With
        dtgAO.DataSource = oAOForecast.ListData.DefaultView
        dtgAO.DataBind()
        PagingFooter()
        PnlGrid.Visible = True
    End Sub


#End Region

#Region " Event Handlers "
    Private Sub imbReset_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbReset.Click
        txtYear.Text = ""
        cboMonth.ClearSelection()
        cboAssetStatus.ClearSelection()
        PnlGrid.Visible = False
    End Sub
    Private Sub imbBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbBack.Click
        Response.Redirect("AOSupervisorSales.aspx")
    End Sub


    Private Sub imbSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbSearch.Click
        Me.SortBy = ""
        If txtYear.Text.Trim <> "" Then
            Me.Year = CInt(txtYear.Text.Trim)
        Else
            Me.Year = 0
        End If

        Me.AssetStatus = cboAssetStatus.SelectedItem.Value.Trim
        Me.Month = CInt(cboMonth.SelectedItem.Value.Trim)
        bindgrid(Me.Year, Me.Month, Me.AssetStatus, Me.SortBy)
    End Sub
    Private Sub dtgAO_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgAO.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim lnkYear As HyperLink
            lnkYear = CType(e.Item.FindControl("lnkYear"), HyperLink)
            Dim lblBranchID As Label
            Dim Month As Integer
            Dim Year As Integer
            lblBranchID = CType(e.Item.FindControl("lblBranchID"), Label)

            lnkYear.NavigateUrl = "AOSupervisorSalesForecastView.aspx?BranchID=" & lblBranchID.Text.Trim & "&EmployeeID=" & Me.EmployeeID.Trim & "&Name=" & Me.Name.Trim & "&Year=" & lnkYear.Text.Trim
        End If
    End Sub

    Private Sub imbPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbPrint.Click
        Dim cookie As HttpCookie = Request.Cookies("AOSupervisorForecast")
        If Not cookie Is Nothing Then

            cookie.Values("BranchID") = Me.BranchID.Trim
            cookie.Values("EmployeeID") = Me.EmployeeID
            cookie.Values("Name") = Me.Name
            If txtYear.Text <> "" Then
                cookie.Values("Year") = txtYear.Text.Trim
            Else
                cookie.Values("Year") = "0"
            End If
            cookie.Values("Month") = cboMonth.SelectedItem.Value.Trim
            cookie.Values("AssetStatus") = cboAssetStatus.SelectedItem.Value.Trim
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie("AOSupervisorForecast")

            cookieNew.Values.Add("BranchID", Me.BranchID)
            cookieNew.Values.Add("EmployeeID", Me.EmployeeID)
            cookieNew.Values.Add("Name", Me.Name)
            If txtYear.Text <> "" Then
                cookieNew.Values.Add("Year", txtYear.Text.Trim)
            Else
                cookieNew.Values.Add("Year", "0")
            End If

            cookieNew.Values.Add("Month", cboMonth.SelectedItem.Value.Trim)
            cookieNew.Values.Add("AssetStatus", cboAssetStatus.SelectedItem.Value.Trim)

            Response.AppendCookie(cookieNew)
        End If
        Response.Redirect("AOSupervisorSalesForecastViewer.aspx")
    End Sub
#End Region
End Class
