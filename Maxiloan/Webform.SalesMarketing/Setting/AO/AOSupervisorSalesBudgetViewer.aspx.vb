
#Region "Imports"
Imports Maxiloan.Exceptions
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Controller
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class AOSupervisorSalesBudgetViewer
    Inherits Maxiloan.Webform.WebBased

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents imbBack As System.Web.UI.WebControls.ImageButton


    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
#Region "Constanta"
    Private m_AOBudget As New AOSupervisorSalesController
    'Protected WithEvents crvAOBudget As CrystalDecisions.Web.CrystalReportViewer


    Private oAOBudget As New Parameter.AOSupervisorSales
#End Region
#Region "properties"
    Public Property Name() As String
        Get
            Return CType(viewstate("Name"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Name") = Value
        End Set
    End Property
    Public Property EmployeeID() As String
        Get
            Return CType(viewstate("EmployeeID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("EmployeeID") = Value
        End Set
    End Property
    Public Property AssetStatus() As String
        Get
            Return CType(viewstate("AssetStatus"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AssetStatus") = Value
        End Set
    End Property
    Public Property Year() As Integer
        Get
            Return CType(viewstate("Year"), Integer)
        End Get
        Set(ByVal Value As Integer)
            viewstate("Year") = Value
        End Set
    End Property

    Public Property Month() As Integer
        Get
            Return CType(viewstate("Month"), Integer)
        End Get
        Set(ByVal Value As Integer)
            viewstate("Month") = Value
        End Set
    End Property
    Public Property MonthName() As String
        Get
            Return CType(viewstate("MonthName"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("MonthName") = Value
        End Set
    End Property


#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here

        BindReport()
    End Sub
    Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies("AOSupervisorSalesBudget")
        Me.BranchID = cookie.Values("BranchID")
        Me.EmployeeID = cookie.Values("EmployeeID")
        Me.Name = cookie.Values("Name")
        Me.Year = CInt(cookie.Values("Year"))
        Me.Month = CInt(cookie.Values("Month"))
        Me.AssetStatus = cookie.Values("AssetStatus")
    End Sub
    Sub BindReport()
        'GetCookies()
        'Dim dstPrintAOBudgetForm As New DataTable
        'Dim PrintingAOBudgetForm As New RptAOSupervisorBudget
        'With oAOBudget
        '    .strConnection = GetConnectionString
        '    .BranchId = Me.BranchID
        '    .EmployeeID = Me.EmployeeID
        '    .Year = Me.Year
        '    .Month = Me.Month
        '    .AssetStatus = Me.AssetStatus
        'End With
        'oAOBudget = m_AOBudget.AOSupervisorBudgetPrint(oAOBudget)
        'dstPrintAOBudgetForm = oAOBudget.ListData
        'Dim numCopies As Int16 = 1
        'Dim Collate As Boolean = False
        'Dim startpage As Int16 = 1
        'PrintingAOBudgetForm.SetDataSource(dstPrintAOBudgetForm)
        'crvAOBudget.ReportSource = PrintingAOBudgetForm
        'crvAOBudget.Visible = True
        'crvAOBudget.HasToggleGroupTreeButton = False
        'crvAOBudget.DisplayGroupTree = False

        'Dim discrete As ParameterDiscreteValue
        'Dim ParamFields As ParameterFields
        'Dim ParamField As ParameterFieldDefinition
        'Dim CurrentValue As ParameterValues

        'ParamFields = New ParameterFields
        'ParamField = PrintingAOBudgetForm.DataDefinition.ParameterFields("LoginId")
        'discrete = New ParameterDiscreteValue
        'discrete.Value = Me.Loginid
        'CurrentValue = New ParameterValues
        'CurrentValue = ParamField.DefaultValues
        'CurrentValue.Add(discrete)
        'ParamField.ApplyCurrentValues(CurrentValue)

        'ParamFields = New ParameterFields
        'ParamField = PrintingAOBudgetForm.DataDefinition.ParameterFields("Branchid")
        'discrete = New ParameterDiscreteValue
        'discrete.Value = Me.BranchID
        'CurrentValue = New ParameterValues
        'CurrentValue = ParamField.DefaultValues
        'CurrentValue.Add(discrete)
        'ParamField.ApplyCurrentValues(CurrentValue)

        'ParamFields = New ParameterFields
        'ParamField = PrintingAOBudgetForm.DataDefinition.ParameterFields("AOID")
        'discrete = New ParameterDiscreteValue
        'discrete.Value = Me.EmployeeID
        'CurrentValue = New ParameterValues
        'CurrentValue = ParamField.DefaultValues
        'CurrentValue.Add(discrete)
        'ParamField.ApplyCurrentValues(CurrentValue)






    End Sub

    Private Sub imbBack_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbBack.Click
        Response.Redirect("AOSupervisorSalesBudget.aspx?BranchID=" & Me.BranchID.Trim & "&EmployeeID=" & Me.EmployeeID.Trim & "&Name=" & Me.Name.Trim)
    End Sub

End Class
