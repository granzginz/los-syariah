<%@ Page Language="vb" AutoEventWireup="false" Codebehind="AOIndirectSales.aspx.vb" Inherits="Maxiloan.Webform.SalesMarketing.AOIndirectSales"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>AOIndirectSales</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../Include/Marketing.css" type="text/css" rel="stylesheet">
		<script language="javascript">
			var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
			var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
			var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';			
		</script>
		<SCRIPT src="../Maxiloan.js"></SCRIPT>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<asp:label id="LblErrMessages" runat="server" ForeColor="Red"></asp:label><br>
			<asp:panel id="PnlGrid" runat="server" Height="216px" Width="100%">
				<TABLE id="Table4" border="0" cellSpacing="0" cellPadding="0" width="95%" align="center">
					<TR class="trtopi">
						<TD class="tdtopi" align="center">List Of CMO Indirect Sales</TD>
					</TR>
				</TABLE>
				<TABLE id="Table6" border="0" cellSpacing="0" cellPadding="0" width="95%" align="center">
					<TR align="center">
						<TD>
							<asp:DataGrid id="dtgAO" runat="server" Width="100%" CssClass="tablegrid" AllowSorting="True"
								AutoGenerateColumns="False" OnSortCommand="Sorting" BorderWidth="0px" cellpadding="3" cellspacing="1"
								HorizontalAlign="Center">
								<AlternatingItemStyle CssClass="tdgenap"></AlternatingItemStyle>
								<ItemStyle HorizontalAlign="Center" CssClass="tdganjil"></ItemStyle>
								<HeaderStyle HorizontalAlign="Center" CssClass="tdjudul"></HeaderStyle>
								<Columns>
									<asp:TemplateColumn HeaderText="BRANCH ID" ItemStyle-Width="20%" Visible="False">
										<HeaderStyle HorizontalAlign="Center" Width="18%"></HeaderStyle>
										<ItemStyle HorizontalAlign="left" Width="18%"></ItemStyle>
										<ItemTemplate>
											<ASP:label id="lblBranchID" runat="server" text='<%#Container.DataItem("BranchID")%>'>
											</ASP:label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn SortExpression="AOID" HeaderText="CMO ID" ItemStyle-Width="20%">
										<HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
										<ItemStyle HorizontalAlign="left" Width="12%"></ItemStyle>
										<ItemTemplate>
											<ASP:HYPERLINK id="lnkAOID" runat="server" text='<%#Container.DataItem("AOID")%>'>
											</ASP:HYPERLINK>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn SortExpression="AOName" HeaderText="CMO NAME" ItemStyle-Width="20%">
										<HeaderStyle HorizontalAlign="Center" Width="18%"></HeaderStyle>
										<ItemStyle HorizontalAlign="Left" Width="18%"></ItemStyle>
										<ItemTemplate>
											<ASP:label id="lblAOName" runat="server" text='<%#Container.DataItem("AOName")%>'>
											</ASP:label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn SortExpression="AOLevel" HeaderText="CMO LEVEL" ItemStyle-Width="20%">
										<HeaderStyle HorizontalAlign="Center" Width="18%"></HeaderStyle>
										<ItemStyle HorizontalAlign="Left" Width="18%"></ItemStyle>
										<ItemTemplate>
											<ASP:LABEL id="lblAOLevel" runat="server" text='<%#Container.DataItem("AOLevel")%>'>
											</ASP:LABEL>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="BUDGET">
										<HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
										<ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
										<ItemTemplate>
											<ASP:HYPERLINK id="lnkBudget" runat="server" text="Budget"></ASP:HYPERLINK>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="FORECAST">
										<HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
										<ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
										<ItemTemplate>
											<ASP:HYPERLINK id="lnkForecast" runat="server" text="Forecast"></ASP:HYPERLINK>
										</ItemTemplate>
									</asp:TemplateColumn>
								</Columns>
							</asp:DataGrid></TD>
					</TR>
				</TABLE>
				<TABLE class="nav_tbl" cellSpacing="0" cellPadding="0" align="center">
					<TR>
						<TD class="nav_tbl_td1">
							<TABLE class="nav_command_tbl" cellSpacing="0" cellPadding="0">
								<TR>
								</TR>
							</TABLE>
						</TD>
						<TD class="nav_tbl_td2">
							<TABLE class="nav_page_tbl" cellSpacing="0" cellPadding="0">
								<TR>
									<TD>
										<asp:imagebutton id="imbFirstPage" runat="server" ImageUrl="../Images/butkiri1.gif" CausesValidation="False"
											OnCommand="NavigationLink_Click" CommandName="First"></asp:imagebutton><FONT face="Verdana"></FONT></TD>
									<TD>
										<asp:imagebutton id="imbPrevPage" runat="server" ImageUrl="../Images/butkiri.gif" CausesValidation="False"
											OnCommand="NavigationLink_Click" CommandName="Prev"></asp:imagebutton><FONT face="Verdana"></FONT></TD>
									<TD>
										<asp:imagebutton id="imbNextPage" runat="server" ImageUrl="../Images/butkanan.gif" CausesValidation="False"
											OnCommand="NavigationLink_Click" CommandName="Next"></asp:imagebutton><FONT face="Verdana"></FONT></TD>
									<TD>
										<asp:imagebutton id="imbLastPage" runat="server" ImageUrl="../Images/butkanan1.gif" CausesValidation="False"
											OnCommand="NavigationLink_Click" CommandName="Last"></asp:imagebutton><FONT face="Verdana"></FONT></TD>
									<TD>Page&nbsp;
										<asp:TextBox id="txtPage" runat="server" Width="34px" CssClass="InpType">1</asp:TextBox></TD>
									<TD>
										<asp:imagebutton id="imbGoPage" runat="server" ImageUrl="../Images/butgo.gif" EnableViewState="False"></asp:imagebutton></TD>
									<ASP:RANGEVALIDATOR id="rgvGo" runat="server" font-size="11px" font-name="Verdana" forecolor="#993300"
										type="Integer" maximumvalue="999999999" errormessage="Page No. is not valid" minimumvalue="1" controltovalidate="txtpage"></ASP:RANGEVALIDATOR>
									<ASP:REQUIREDFIELDVALIDATOR id="rfvGo" runat="server" font-size="11px" font-name="Verdana" forecolor="#993300"
										errormessage="Page No. is not valid" controltovalidate="txtpage" Display="Dynamic"></ASP:REQUIREDFIELDVALIDATOR></TR>
							</TABLE>
						</TD>
					</TR>
					<TR>
						<TD class="nav_totpage" colSpan="2">
							<asp:label id="lblPage" runat="server"></asp:label>&nbsp;of
							<asp:label id="lblTotPage" runat="server"></asp:label>, Total&nbsp;
							<asp:label id="lblrecord" runat="server"></asp:label>&nbsp;record(s)
						</TD>
					</TR>
				</TABLE>
			</asp:panel>
			<TABLE cellSpacing="0" cellPadding="0" width="95%" align="center" border="0">
				<TR class="trtopi">
					<TD class="tdtopi" align="center">Find CMO Indirect Sales</TD>
				</TR>
			</TABLE>
			<table class="tablegrid" cellSpacing="1" cellPadding="2" width="95%" align="center" border="0">
				<tr class="tdganjil">
					<td class="tdgenap" width="20%">Branch&nbsp;<FONT color="red">*)</FONT></td>
					<td class="tdganjil" width="30%"><asp:dropdownlist id="cboBranch" runat="server"></asp:dropdownlist><asp:requiredfieldvalidator id="rfvcbobranchid" runat="server" ControlToValidate="cboBranch" ErrorMessage="Please Select Branch"
							Display="Dynamic" InitialValue="0"></asp:requiredfieldvalidator></td>
				</tr>
				<tr class="tdganjil">
					<td class="tdgenap" width="20%">Find By</td>
					<td class="tdganjil" width="30%"><asp:dropdownlist id="cboSearchBy" runat="server">
							<asp:ListItem Value="AOID">CMO ID</asp:ListItem>
							<asp:ListItem Value="AOName">CMO Name</asp:ListItem>
						</asp:dropdownlist><asp:textbox id="txtSearchBy" runat="server" CssClass="inptype"></asp:textbox></td>
				</tr>
				<tr class="tdganjil">
					<td class="tdgenap" width="20%">CMO Level</td>
					<td class="tdganjil" width="30%"><asp:dropdownlist id="cboAOLevel" runat="server">
							<asp:ListItem Value="All">All</asp:ListItem>
							<asp:ListItem Value="T">Trainee</asp:ListItem>
							<asp:ListItem Value="J">Junior</asp:ListItem>
							<asp:ListItem Value="S">Senior</asp:ListItem>
							<asp:ListItem Value="E">Executive</asp:ListItem>
						</asp:dropdownlist></td>
				</tr>
			</table>
			<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="95%" align="center" border="0">
				<TR>
					<TD width="50%" height="30"><asp:imagebutton id="imbSearch" Runat="server" ImageUrl="../Images/ButtonSearch.gif" CausesValidation="True"></asp:imagebutton>&nbsp;
						<asp:imagebutton id="imbReset" Runat="server" ImageUrl="../Images/ButtonReset.gif" CausesValidation="False"></asp:imagebutton></TD>
					<TD align="right" width="50%"></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
