
#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.General
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
#End Region
Partial Class AODirectSalesBudgetAdd
    Inherits Maxiloan.Webform.WebBased


#Region "Constanta"
    Private m_AOBudget As New AODirectSalesController
    Private oAOBudget As New Parameter.AODirectSales
#End Region
#Region "properties"
    Public Property Name() As String
        Get
            Return CType(viewstate("Name"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Name") = Value
        End Set
    End Property
    Public Property EmployeeID() As String
        Get
            Return CType(viewstate("EmployeeID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("EmployeeID") = Value
        End Set
    End Property
    Public Property AssetStatus() As String
        Get
            Return CType(viewstate("AssetStatus"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AssetStatus") = Value
        End Set
    End Property
    Public Property Year() As Integer
        Get
            Return CType(viewstate("Year"), Integer)
        End Get
        Set(ByVal Value As Integer)
            viewstate("Year") = Value
        End Set
    End Property

    Public Property Month() As Integer
        Get
            Return CType(viewstate("Month"), Integer)
        End Get
        Set(ByVal Value As Integer)
            viewstate("Month") = Value
        End Set
    End Property



#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Me.FormID = "MktAODirect"
        If Not Page.IsPostBack Then
            Me.BranchID = Request.QueryString("BranchID")
            Me.EmployeeID = Request.QueryString("EmployeeID")
            Me.Name = Request.QueryString("Name")
            txtNewUnit1.Text = "0"
            txtNewUnit2.Text = "0"
            txtNewUnit3.Text = "0"
            txtNewUnit4.Text = "0"
            txtNewUnit5.Text = "0"
            txtNewUnit6.Text = "0"
            txtNewUnit7.Text = "0"
            txtNewUnit8.Text = "0"
            txtNewUnit9.Text = "0"
            txtNewUnit10.Text = "0"
            txtNewUnit11.Text = "0"
            txtNewUnit12.Text = "0"
            txtAssetUnit1.Text = "0"
            txtAssetUnit2.Text = "0"
            txtAssetUnit3.Text = "0"
            txtAssetUnit4.Text = "0"
            txtAssetUnit5.Text = "0"
            txtAssetUnit6.Text = "0"
            txtAssetUnit7.Text = "0"
            txtAssetUnit8.Text = "0"
            txtAssetUnit9.Text = "0"
            txtAssetUnit10.Text = "0"
            txtAssetUnit11.Text = "0"
            txtAssetUnit12.Text = "0"
            txtNewAmount1.Text = "0"
            txtNewAmount2.Text = "0"
            txtNewAmount3.Text = "0"
            txtNewAmount4.Text = "0"
            txtNewAmount5.Text = "0"
            txtNewAmount6.Text = "0"
            txtNewAmount7.Text = "0"
            txtNewAmount8.Text = "0"
            txtNewAmount9.Text = "0"
            txtNewAmount10.Text = "0"
            txtNewAmount11.Text = "0"
            txtNewAmount12.Text = "0"
            txtAssetAmount1.Text = "0"
            txtAssetAmount2.Text = "0"
            txtAssetAmount3.Text = "0"
            txtAssetAmount4.Text = "0"
            txtAssetAmount5.Text = "0"
            txtAssetAmount6.Text = "0"
            txtAssetAmount7.Text = "0"
            txtAssetAmount8.Text = "0"
            txtAssetAmount9.Text = "0"
            txtAssetAmount10.Text = "0"
            txtAssetAmount11.Text = "0"
            txtAssetAmount12.Text = "0"
        End If


        If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then

            lnkAOID.Text = Me.EmployeeID.Trim
            lnkAOID.NavigateUrl = "javascript:OpenAO('Marketing', '" & Me.BranchID.Trim & "','" & Me.EmployeeID.Trim & "')"
            lblAOName.Text = Me.Name.Trim
        Else
            Dim strHTTPServer As String
            Dim strHTTPApp As String
            Dim strNameServer As String
            strHTTPServer = Request.ServerVariables("PATH_INFO")
            strNameServer = Request.ServerVariables("SERVER_NAME")
            strHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
            Response.Redirect("http://" & strNameServer & "/" & strHTTPApp & "/NotAuthorized.html")
        End If
    End Sub

#Region " Event Handlers "
    Private Sub imbSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbSave.Click
        If CInt(txtYear.Text) <= Me.BusinessDate.Year Then
            lblMessage.Text = ""
            lblMessage.Text = "Year should be bigger than BusinessDate "
            Exit Sub
        End If
        lblMessage.Text = ""
        Dim dttAOBudget As New DataTable
        Dim objrow As DataRow
        Dim i As Integer
        Dim j As Integer = 1
        dttAOBudget.Columns.Add("BranchID", GetType(String))
        dttAOBudget.Columns.Add("AOID", GetType(String))
        dttAOBudget.Columns.Add("BudgetYear", GetType(Integer))

        dttAOBudget.Columns.Add("BudgetMonth", GetType(Integer))
        dttAOBudget.Columns.Add("AssetUsedNew", GetType(String))
        dttAOBudget.Columns.Add("Unit", GetType(Integer))
        dttAOBudget.Columns.Add("Amount", GetType(Decimal))

        'inserting data into datatable
        For i = 0 To 11
            objrow = dttAOBudget.NewRow
            objrow("BranchID") = Me.BranchID
            objrow("AOID") = Me.EmployeeID
            objrow("BudgetYear") = CInt(txtYear.Text)
            objrow("BudgetMonth") = j
            objrow("AssetUsedNew") = "N"
            If j = 1 Then
                objrow("Unit") = CInt(txtNewUnit1.Text)
                objrow("Amount") = CDec(txtNewAmount1.Text)
            End If
            If j = 2 Then
                objrow("Unit") = CInt(txtNewUnit2.Text)
                objrow("Amount") = CDec(txtNewAmount2.Text)
            End If
            If j = 3 Then
                objrow("Unit") = CInt(txtNewUnit3.Text)
                objrow("Amount") = CDec(txtNewAmount3.Text)
            End If
            If j = 4 Then
                objrow("Unit") = CInt(txtNewUnit4.Text)
                objrow("Amount") = CDec(txtNewAmount4.Text)
            End If
            If j = 5 Then
                objrow("Unit") = CInt(txtNewUnit5.Text)
                objrow("Amount") = CDec(txtNewAmount5.Text)
            End If
            If j = 6 Then
                objrow("Unit") = CInt(txtNewUnit6.Text)
                objrow("Amount") = CDec(txtNewAmount6.Text)
            End If
            If j = 7 Then
                objrow("Unit") = CInt(txtNewUnit7.Text)
                objrow("Amount") = CDec(txtNewAmount7.Text)
            End If
            If j = 8 Then
                objrow("Unit") = CInt(txtNewUnit8.Text)
                objrow("Amount") = CDec(txtNewAmount8.Text)
            End If
            If j = 9 Then
                objrow("Unit") = CInt(txtNewUnit9.Text)
                objrow("Amount") = CDec(txtNewAmount9.Text)
            End If
            If j = 10 Then
                objrow("Unit") = CInt(txtNewUnit10.Text)
                objrow("Amount") = CDec(txtNewAmount10.Text)
            End If
            If j = 11 Then
                objrow("Unit") = CInt(txtNewUnit11.Text)
                objrow("Amount") = CDec(txtNewAmount11.Text)
            End If
            If j = 12 Then
                objrow("Unit") = CInt(txtNewUnit12.Text)
                objrow("Amount") = CDec(txtNewAmount12.Text)
            End If
            j = j + 1
            dttAOBudget.Rows.Add(objrow)
        Next
        j = 1
        For i = 0 To 11
            objrow = dttAOBudget.NewRow
            objrow("BranchID") = Me.BranchID
            objrow("AOID") = Me.EmployeeID
            objrow("BudgetYear") = CInt(txtYear.Text)
            objrow("BudgetMonth") = j
            objrow("AssetUsedNew") = "U"
            If j = 1 Then
                objrow("Unit") = CInt(txtAssetUnit1.Text)
                objrow("Amount") = CDec(txtAssetAmount1.Text)
            End If
            If j = 2 Then
                objrow("Unit") = CInt(txtAssetUnit2.Text)
                objrow("Amount") = CDec(txtAssetAmount2.Text)
            End If
            If j = 3 Then
                objrow("Unit") = CInt(txtAssetUnit3.Text)
                objrow("Amount") = CDec(txtAssetAmount3.Text)
            End If
            If j = 4 Then
                objrow("Unit") = CInt(txtAssetUnit4.Text)
                objrow("Amount") = CDec(txtAssetAmount4.Text)
            End If
            If j = 5 Then
                objrow("Unit") = CInt(txtAssetUnit5.Text)
                objrow("Amount") = CDec(txtAssetAmount5.Text)
            End If
            If j = 6 Then
                objrow("Unit") = CInt(txtAssetUnit6.Text)
                objrow("Amount") = CDec(txtAssetAmount6.Text)
            End If
            If j = 7 Then
                objrow("Unit") = CInt(txtAssetUnit7.Text)
                objrow("Amount") = CDec(txtAssetAmount7.Text)
            End If
            If j = 8 Then
                objrow("Unit") = CInt(txtAssetUnit8.Text)
                objrow("Amount") = CDec(txtAssetAmount8.Text)
            End If
            If j = 9 Then
                objrow("Unit") = CInt(txtAssetUnit9.Text)
                objrow("Amount") = CDec(txtAssetAmount9.Text)
            End If
            If j = 10 Then
                objrow("Unit") = CInt(txtAssetUnit10.Text)
                objrow("Amount") = CDec(txtAssetAmount10.Text)
            End If
            If j = 11 Then
                objrow("Unit") = CInt(txtAssetUnit11.Text)
                objrow("Amount") = CDec(txtAssetAmount11.Text)
            End If
            If j = 12 Then
                objrow("Unit") = CInt(txtAssetUnit12.Text)
                objrow("Amount") = CDec(txtAssetAmount12.Text)
            End If
            j = j + 1
            dttAOBudget.Rows.Add(objrow)
        Next
        oAOBudget.strConnection = GetConnectionString()
        oAOBudget.ListData = dttAOBudget
        Try
            m_AOBudget.AOBudgetAdd(oAOBudget)
            Response.Redirect("AODirectSalesBudget.aspx?BranchID=" & Me.BranchID.Trim & "&EmployeeID=" & Me.EmployeeID.Trim & "&Name=" & Me.Name.Trim)
        Catch ex As Exception

            lblMessage.Text = ""
            lblMessage.Text = "Data Has Already Exists"
            lblmessage.visible = True
        End Try

    End Sub
    Private Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbCancel.Click
        Response.Redirect("AODirectSalesBudget.aspx?BranchID=" & Me.BranchID.Trim & "&EmployeeID=" & Me.EmployeeID.Trim & "&Name=" & Me.Name.Trim)
    End Sub
#End Region



End Class
