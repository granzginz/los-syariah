
#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.General
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
#End Region
Partial Class AODirectSalesBudget
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private m_AOBudget As New AODirectSalesController
    Private oAOBudget As New Parameter.AODirectSales
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private currentPage As Integer = 1
#End Region
#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblRecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If

    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        bindgrid(Me.Year, Me.Month, Me.AssetStatus, Me.SortBy)
    End Sub

    Private Sub imgbtnPageNumb_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click

        If txtPage.Text = "" Then
            txtPage.Text = "0"
        Else
            If IsNumeric(txtPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                    currentPage = CType(txtPage.Text, Int32)
                    bindgrid(Me.Year, Me.Month, Me.AssetStatus, Me.SortBy)
                End If
            End If
        End If


    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        bindgrid(Me.Year, Me.Month, Me.AssetStatus, Me.SortBy)
    End Sub
#End Region
#Region "properties"
    Public Property Name() As String
        Get
            Return CType(viewstate("Name"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Name") = Value
        End Set
    End Property
    Public Property EmployeeID() As String
        Get
            Return CType(viewstate("EmployeeID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("EmployeeID") = Value
        End Set
    End Property
    Public Property AssetStatus() As String
        Get
            Return CType(viewstate("AssetStatus"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AssetStatus") = Value
        End Set
    End Property
    Public Property Year() As Integer
        Get
            Return CType(viewstate("Year"), Integer)
        End Get
        Set(ByVal Value As Integer)
            viewstate("Year") = Value
        End Set
    End Property

    Public Property Month() As Integer
        Get
            Return CType(viewstate("Month"), Integer)
        End Get
        Set(ByVal Value As Integer)
            viewstate("Month") = Value
        End Set
    End Property



#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Me.FormID = "MktAODirectBudget"
        If Not Page.IsPostBack Then
            Me.BranchID = Request.QueryString("BranchID")
            Me.EmployeeID = Request.QueryString("EmployeeID")
            Me.Name = Request.QueryString("Name")
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                cboYear.Items.Clear()
                For i As Integer = CInt(Date.Now.Year) - 3 To CInt(Date.Now.Year) + 3
                    cboYear.Items.Add(i.ToString)
                Next
                cboYear.SelectedValue = CInt(Date.Now.Year)
                InitialDefaultPanel()
                linkAOID.Text = Me.EmployeeID.Trim

                linkAOID.NavigateUrl = "javascript:OpenAO('Marketing', '" & Me.BranchID.Trim & "','" & Me.EmployeeID.Trim & "')"
                lblName.Text = Me.Name.Trim
            Else
                Dim strHTTPServer As String
                Dim strHTTPApp As String
                Dim strNameServer As String
                strHTTPServer = Request.ServerVariables("PATH_INFO")
                strNameServer = Request.ServerVariables("SERVER_NAME")
                strHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                Response.Redirect("http://" & strNameServer & "/" & strHTTPApp & "/NotAuthorized.html")
            End If

        End If


      
    End Sub
#Region " Sub and Functions "
    Private Sub InitialDefaultPanel()
        PnlGrid.Visible = False
        LblErrMessages.Text = ""

    End Sub
    'sub ini untuk bind data hasil search ke datagrid
    Private Sub bindgrid(ByVal year As Integer, ByVal month As Integer, ByVal assetstatus As String, ByVal sortby As String)
        With oAOBudget
            .strConnection = GetConnectionString()
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = sortby
            .BranchId = Me.BranchID
            .EmployeeID = Me.EmployeeID
            .Year = Me.Year
            .Month = month
            .AssetStatus = assetstatus
        End With
        Try
            oAOBudget = m_AOBudget.AOBudgetListing(oAOBudget)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        With oAOBudget
            lblRecord.Text = CType(.TotalRecord, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecord
        End With
        dtgAO.DataSource = oAOBudget.ListData.DefaultView
        dtgAO.DataBind()
        PagingFooter()
        PnlGrid.Visible = True
    End Sub

    Protected Function GetStatus(ByVal status As String) As String
        If status = "N" Then
            Return "New"
        End If
        If status = "U" Then
            Return "Used"
        End If
    End Function
    Protected Function GetMonthName(ByVal month As Integer) As String
        Select Case month
            Case 1
                Return "January"
            Case 2
                Return "February"
            Case 3
                Return "March"
            Case 4
                Return "April"
            Case 5
                Return "May"
            Case 6
                Return "June"
            Case 7
                Return "July"
            Case 8
                Return "August"
            Case 9
                Return "September"
            Case 10
                Return "October"
            Case 11
                Return "November"
            Case 12
                Return "December"
        End Select


    End Function

#End Region

#Region " Event Handlers "
  

   
    Private Sub dtgAO_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgAO.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim lnkYear As HyperLink
            lnkYear = CType(e.Item.FindControl("lnkYear"), HyperLink)
            Dim lblBranchID As Label
            Dim imbEdit As ImageButton
            imbEdit = CType(e.Item.FindControl("imbEdit"), ImageButton)

            If CInt(lnkYear.Text.Trim) <= Me.BusinessDate.Year Then
                imbEdit.Visible = False
            End If

            lblBranchID = CType(e.Item.FindControl("lblBranchID"), Label)

            lnkYear.NavigateUrl = "AODirectSalesBudgetView.aspx?BranchID=" & lblBranchID.Text.Trim & "&EmployeeID=" & Me.EmployeeID.Trim & "&Name=" & Me.Name.Trim & "&Year=" & lnkYear.Text.Trim
        End If
    End Sub
    Protected Sub imbButtonSearch_Click(sender As Object, e As EventArgs) Handles imbButtonSearch.Click
        Me.SortBy = ""
        If cboYear.SelectedValue <> "" Then
            Me.Year = CInt(cboYear.SelectedValue)
        Else
            Me.Year = 0
        End If

        Me.AssetStatus = cboAssetStatus.SelectedItem.Value.Trim
        Me.Month = CInt(cboMonth.SelectedItem.Value.Trim)
        bindgrid(Me.Year, Me.Month, Me.AssetStatus, Me.SortBy)
    End Sub

    Protected Sub imbButtonReset_Click(sender As Object, e As EventArgs) Handles imbButtonReset.Click
        cboYear.SelectedValue = CInt(Date.Now.Year)
        cboMonth.ClearSelection()
        cboAssetStatus.ClearSelection()
        PnlGrid.Visible = False
    End Sub
#End Region



    Private Sub dtgAO_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgAO.ItemCommand
        Select Case e.CommandName
            Case "Edit"
                Dim lblemployeeid As Label
                Dim lblbranchid As Label
                Dim lblname As Label
                Dim lblmonth As Label
                Dim lblmonthname As Label
                Dim lblAsset As Label
                Dim lnkYear As HyperLink
                lnkYear = CType(e.Item.FindControl("lnkYear"), HyperLink)
                lblAsset = CType(e.Item.FindControl("lblAOLevel"), Label)
                lblemployeeid = CType(e.Item.FindControl("lblAOID"), Label)
                lblbranchid = CType(e.Item.FindControl("lblBranchID"), Label)
                lblname = CType(e.Item.FindControl("lblAOName"), Label)
                lblmonth = CType(e.Item.FindControl("lblMonth"), Label)
                lblmonthname = CType(e.Item.FindControl("lblMonthName"), Label)
                Response.Redirect("AODirectSalesBudgetAdd.aspx?EmployeeID=" & lblemployeeid.Text.Trim & "&Asset=" & lblAsset.Text.Trim & "&Name=" & lblname.Text.Trim & "&BranchId=" & lblbranchid.Text.Trim & "&Year=" & lnkYear.Text.Trim & "&Month=" & lblmonth.Text.Trim & "&MonthName=" & lblmonthname.Text.Trim & "&EventStatus=" & "Edit")
        End Select
    End Sub

    Protected Sub ButtonAdd_Click(sender As Object, e As EventArgs) Handles ButtonAdd.Click
        Response.Redirect("AODirectSalesBudgetAdd.aspx?BranchID=" & Me.BranchID.Trim & "&EmployeeID=" & Me.EmployeeID.Trim & "&Name=" & Me.Name.Trim)
    End Sub

   

    Protected Sub ButtonBack_Click(sender As Object, e As EventArgs) Handles ButtonBack.Click
        Response.Redirect("AODirectSales.aspx")
    End Sub
End Class
