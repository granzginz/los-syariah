<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AODirectSalesBudget.aspx.vb"
    Inherits="Maxiloan.Webform.SalesMarketing.AODirectSalesBudget" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>AODirectSales</title>
    <link rel="Stylesheet" href="../../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../../Include/Buttons.css" type="text/css" />
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
    <%--<script language="javascript" type="text/javascript">
        var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%#Request.servervariables("SERVER_NAME")%>/';	
    </script>--%>
</head>
<body>
    <form id="Form1" runat="server">
    <asp:Label ID="LblErrMessages" runat="server" ForeColor="Red"></asp:Label>
    <asp:Panel ID="PnlGrid" runat="server" Width="100%" Height="216px">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    DAFTAR CMO BUDGET
                </h3>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgAO" runat="server" Width="100%" CssClass="grid_general" CellPadding="3"
                        CellSpacing="1" BorderStyle="None" BorderWidth="0" OnSortCommand="Sorting" AutoGenerateColumns="False"
                        AllowSorting="True">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="EDIT">
                                <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbEdit" CommandName="Edit" runat="server" ImageUrl="../../../images/iconEdit.gif">
                                    </asp:ImageButton></a>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="BRANCH ID" ItemStyle-Width="20%" Visible="False">
                                <HeaderStyle HorizontalAlign="Center" Width="18%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="left" Width="18%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblBranchID" runat="server" Text='<%#Container.DataItem("BranchID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="AO ID" ItemStyle-Width="20%" Visible="False">
                                <HeaderStyle HorizontalAlign="Center" Width="18%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="left" Width="18%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblAOID" runat="server" Text='<%#Container.DataItem("AOID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="AO Name" ItemStyle-Width="20%" Visible="False">
                                <HeaderStyle HorizontalAlign="Center" Width="18%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="left" Width="18%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblAOName" runat="server" Text='<%#Container.DataItem("Name")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Year" HeaderText="YEAR" ItemStyle-Width="20%">
                                <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="left" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkYear" runat="server" Text='<%#Container.DataItem("Year")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Month" HeaderText="MONTH" ItemStyle-Width="20%">
                                <HeaderStyle HorizontalAlign="Center" Width="18%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="18%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblMonthName" runat="server" Text='<%#GetMonthName(Container.DataItem("Month"))%>'>
                                    </asp:Label>
                                    <asp:Label ID="lblMonth" runat="server" Visible="false" Text='<%#Container.DataItem("Month")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AssetStatus" HeaderText="ASSET STATUS" ItemStyle-Width="20%">
                                <HeaderStyle HorizontalAlign="Center" Width="18%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="18%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblAOLevel" runat="server" Text='<%#Container.DataItem("AssetStatus")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Unit" HeaderText="UNIT" ItemStyle-Width="20%">
                                <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblUnit" runat="server" Text='<%#Container.DataItem("Unit")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Amount" HeaderText="AMOUNT" ItemStyle-Width="20%">
                                <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblAmount" runat="server" Text='<%#formatnumber(Container.DataItem("Amount"),2)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                    <div class="button_gridnavigation">
                        <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../../Images/grid_navbutton01.png"
                            CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../../Images/grid_navbutton02.png"
                            CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../../Images/grid_navbutton03.png"
                            CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../../Images/grid_navbutton04.png"
                            CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                        <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                            EnableViewState="False"></asp:Button>
                        <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" MinimumValue="1"
                            ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                            ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                    <div class="label_gridnavigation">
                        <asp:Label ID="lblPage" runat="server"></asp:Label>of
                        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                        <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                    </div>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonAdd" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="ButtonBack" runat="server" CausesValidation="False" Text="Back" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    <div class="form_box_title">
        <div class="form_single">
            <h4>
                CARI CMO DIRECT SALES
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single" runat="server" id="divBranch">
            <label>
                AO ID</label>
            <asp:HyperLink ID="linkAOID" runat="server"></asp:HyperLink>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                AO Name</label>
            <asp:Label ID="lblName" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Year</label>
            <asp:DropDownList ID="cboYear" runat="server">
            </asp:DropDownList>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Month</label>
            <asp:DropDownList ID="cboMonth" runat="server">
                <asp:ListItem Value="0">All</asp:ListItem>
                <asp:ListItem Value="1">January</asp:ListItem>
                <asp:ListItem Value="2">February</asp:ListItem>
                <asp:ListItem Value="3">March</asp:ListItem>
                <asp:ListItem Value="4">April</asp:ListItem>
                <asp:ListItem Value="5">May</asp:ListItem>
                <asp:ListItem Value="6">June</asp:ListItem>
                <asp:ListItem Value="7">July</asp:ListItem>
                <asp:ListItem Value="8">August</asp:ListItem>
                <asp:ListItem Value="9">September</asp:ListItem>
                <asp:ListItem Value="10">October</asp:ListItem>
                <asp:ListItem Value="11">November</asp:ListItem>
                <asp:ListItem Value="12">December</asp:ListItem>
            </asp:DropDownList>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Asset Status</label>
            <asp:DropDownList ID="cboAssetStatus" runat="server">
                <asp:ListItem Value="All">All</asp:ListItem>
                <asp:ListItem Value="N">New</asp:ListItem>
                <asp:ListItem Value="U">Used</asp:ListItem>
            </asp:DropDownList>
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="imbButtonSearch" runat="server" CssClass="small button blue" Text="Search">
        </asp:Button>
        <asp:Button ID="imbButtonReset" runat="server" CausesValidation="False" Text="Reset"
            CssClass="small button gray"></asp:Button>
    </div>
    </form>
</body>
</html>
