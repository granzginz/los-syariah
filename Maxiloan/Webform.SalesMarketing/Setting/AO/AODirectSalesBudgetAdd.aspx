﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AODirectSalesBudgetAdd.aspx.vb"
    Inherits="Maxiloan.Webform.SalesMarketing.AODirectSalesBudgetAdd1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../../../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat"
    TagPrefix="uc4" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="Stylesheet" href="../../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../../Include/Buttons.css" type="text/css" />
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            $('.number').each(function () { $(this).attr('type', 'number'); });
        });
        function minmax(value, min, max) {
//            $('.number').each(function () { $(this).attr('type', 'number'); });
            if (parseInt(value) < 1900 || isNaN(value))
                return 1900;
            else if (parseInt(value) > 4000)
                return 4000;
            else return value;
        }

//        var _textTypes = /^(text|password|hidden|search|tel|url|email|number|range|color|datetime|date|month|week|time|datetime-local)$/i;
//Error: Sys.WebForms.PageRequestManagerServerErrorException: Conversion from string "" to type 'Integer' is not valid.
//function Sys$WebForms$PageRequestManager$_onFormSubmit(evt) {

//    if (_textTypes.test(type) ||
//       (((type === 'checkbox') || (type === 'radio')) && element.checked)) {
//        formBody.append(encodeURIComponent(name));
//        formBody.append('=');
//        formBody.append(encodeURIComponent(element.value));
//        formBody.append('&');

//    }
        //}
this._textTypes = /^(text|password|hidden|search|tel|url|email|number|range|color|datetime|date|month|week|time|datetime-local)$/i;

function Sys$WebForms$PageRequestManager$_onFormSubmit(evt) {
    var i, l, continueSubmit = true,
            isCrossPost = this._isCrossPost;
    this._isCrossPost = false;
    if (this._onsubmit) {
        continueSubmit = this._onsubmit();
    }
    if (continueSubmit) {
        for (i = 0, l = this._onSubmitStatements.length; i < l; i++) {
            if (!this._onSubmitStatements[i]()) {
                continueSubmit = false;
                break;
            }
        }
    }
    if (!continueSubmit) {
        if (evt) {
            evt.preventDefault();
        }
        return;
    }
    var form = this._form;
    if (isCrossPost) {
        return;
    }
    if (this._activeDefaultButton && !this._activeDefaultButtonClicked) {
        this._onFormElementActive(this._activeDefaultButton, 0, 0);
    }
    if (!this._postBackSettings || !this._postBackSettings.async) {
        return;
    }
    var formBody = new Sys.StringBuilder(),
            count = form.elements.length,
            panelID = this._createPanelID(null, this._postBackSettings);
    formBody.append(panelID);
    for (i = 0; i < count; i++) {
        var element = form.elements[i];
        var name = element.name;
        if (typeof (name) === "undefined" || (name === null) || (name.length === 0) || (name === this._scriptManagerID)) {
            continue;
        }
        var tagName = element.tagName.toUpperCase();
        if (tagName === 'INPUT') {
            var type = element.type;
            if (this._textTypes.test(type)
                    || ((type === 'checkbox' || type === 'radio') && element.checked)) {
                formBody.append(encodeURIComponent(name));
                formBody.append('=');
                formBody.append(encodeURIComponent(element.value));
                formBody.append('&');
            }
        }
        else if (tagName === 'SELECT') {
            var optionCount = element.options.length;
            for (var j = 0; j < optionCount; j++) {
                var option = element.options[j];
                if (option.selected) {
                    formBody.append(encodeURIComponent(name));
                    formBody.append('=');
                    formBody.append(encodeURIComponent(option.value));
                    formBody.append('&');
                }
            }
        }
        else if (tagName === 'TEXTAREA') {
            formBody.append(encodeURIComponent(name));
            formBody.append('=');
            formBody.append(encodeURIComponent(element.value));
            formBody.append('&');
        }
    }
    formBody.append("__ASYNCPOST=true&");
    if (this._additionalInput) {
        formBody.append(this._additionalInput);
        this._additionalInput = null;
    }
}
    </script>
</head>
<body>
    <asp:UpdateProgress ID="upg1" runat="server" AssociatedUpdatePanelID="updatePanel1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
            <br />
              <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
                onclick="hideMessage();"></asp:Label>
           
            <div class="form_box_header">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h4>
                        AO DIRECT SALES BUDGET
                    </h4>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        CMO ID</label>
                    <asp:HyperLink ID="lnkAOID" runat="server"></asp:HyperLink>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        CMO Name</label>
                    <asp:Label ID="lblAOName" runat="server"></asp:Label>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Year</label>
                    <%-- <asp:TextBox ID="txtYear" runat="server" min="1900" max="4000" onkeyup="this.value = minmax(this.value, 1900, 4000)" CssClass="number" Style="width: 50px; border: 1px solid rgba(0, 0, 0, 0.2);
                margin: 2px; outline: medium none; padding: 3px; transition: all 0.25s ease-in-out 0s;
                display: inline-block; text-align: right !important;"></asp:TextBox>--%>
                  
                           <asp:DropDownList ID="cboYear" runat="server" AutoPostBack="True">
                        </asp:DropDownList>
                    <%-- <uc4:ucnumberformat id="txtNewAssetUnit" runat="server" TextCssClass="numberAlign smaller_text" />--%>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <asp:DataGrid ID="grdAODirectSalesBudget" runat="server" Width="100%" DataKeyField="ID"
                        AutoGenerateColumns="False" AllowSorting="false" CssClass="grid_general" BorderStyle="None"
                        BorderWidth="0" ShowFooter="true">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <FooterStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="NO">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblNomor" Text='<%# Container.ItemIndex + 1 %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="Bulan" HeaderText="Month"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="AssetNew" Visible="false">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblAssetNew" Text='<%# databinder.eval(container,"DataItem.AssetNew")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="New Asset Unit">
                                <ItemTemplate>
                                    <uc4:ucnumberformat id="txtNewAssetUnit" runat="server" textcssclass="numberAlign smaller_text"
                                        text='<%# databinder.eval(container,"DataItem.NewAssetUnit","{0:##0}")%>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="New Asset Amount">
                                <ItemTemplate>
                                    <uc4:ucnumberformat id="txtNewAssetAmount" runat="server" textcssclass="numberAlign small"
                                        text='<%# databinder.eval(container,"DataItem.NewAssetAmount","{0:##0}")%>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="AssetUsed" Visible="false">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblAssetUsed" Text='<%# databinder.eval(container,"DataItem.AssetUsed")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Used Asset Unit">
                                <ItemTemplate>
                                    <uc4:ucnumberformat id="txtUsedAssetUnit" runat="server" textcssclass="numberAlign smaller_text"
                                        text='<%# databinder.eval(container,"DataItem.UsedAssetUnit","{0:##0}")%>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Used Asset Amount">
                                <ItemTemplate>
                                    <uc4:ucnumberformat id="txtUsedAssetAmount" runat="server" textcssclass="numberAlign small"
                                        text='<%# databinder.eval(container,"DataItem.UsedAssetAmount","{0:##0}")%>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonSaveEdit" runat="server" Text="Save" CssClass="small button blue"
                        CausesValidation="True"></asp:Button>
                    <asp:Button ID="ButtonCancelEdit" runat="server" CausesValidation="False" Text="Back"
                        CssClass="small button gray"></asp:Button>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
