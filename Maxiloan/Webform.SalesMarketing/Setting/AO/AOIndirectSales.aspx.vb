
#Region "Imports"

Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.webform.UserController
Imports Maxiloan.Controller
Imports Maxiloan.General
Imports Maxiloan.General.CommonCookiesHelper

#End Region
Public Class AOIndirectSales
    Inherits Maxiloan.Webform.WebBased

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents LblErrMessages As System.Web.UI.WebControls.Label

    Protected WithEvents dtgAO As System.Web.UI.WebControls.DataGrid
    Protected WithEvents imbFirstPage As System.Web.UI.WebControls.ImageButton
    Protected WithEvents imbPrevPage As System.Web.UI.WebControls.ImageButton
    Protected WithEvents imbNextPage As System.Web.UI.WebControls.ImageButton
    Protected WithEvents imbLastPage As System.Web.UI.WebControls.ImageButton
    Protected WithEvents imbGoPage As System.Web.UI.WebControls.ImageButton
    Protected WithEvents rgvGo As System.Web.UI.WebControls.RangeValidator
    Protected WithEvents rfvGo As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents lblPage As System.Web.UI.WebControls.Label
    Protected WithEvents lblTotPage As System.Web.UI.WebControls.Label
    Protected WithEvents PnlGrid As System.Web.UI.WebControls.Panel
    Protected WithEvents cboBranch As System.Web.UI.WebControls.DropDownList
    Protected WithEvents rfvcbobranchid As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents cboSearchBy As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtSearchBy As System.Web.UI.WebControls.TextBox
    Protected WithEvents cboAOLevel As System.Web.UI.WebControls.DropDownList
    Protected WithEvents imbSearch As System.Web.UI.WebControls.ImageButton
    Protected WithEvents imbReset As System.Web.UI.WebControls.ImageButton
    Protected WithEvents txtPage As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblrecord As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
#Region "Constanta"
    Dim m_AODirectSales As New AOIndirectSalesController
    Private oAODirectSales As New Parameter.AOInDirectSales
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1


    Private currentPage As Integer = 1
#End Region
#Region "properties"




    Public Property Criteria() As String
        Get
            Return CType(viewstate("Criteria"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Criteria") = Value
        End Set
    End Property



#End Region
#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblRecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If

    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        Bindgrid(Me.SearchBy, Me.Criteria, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbGoPage.Click

        If txtpage.Text = "" Then
            txtpage.Text = "0"
        Else
            If IsNumeric(txtpage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And CType(txtpage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                    currentPage = CType(txtpage.Text, Int32)
                    Bindgrid(Me.SearchBy, Me.Criteria, Me.SortBy)
                End If
            End If
        End If


    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        Bindgrid(Me.SearchBy, Me.Criteria, Me.SortBy)
    End Sub
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Me.FormID = "MktAOInDirectSales"

        If Not Page.IsPostBack Then
            fillcbo()
        End If

        If CheckForm(Me.Loginid, Me.FormID, "Eloan") Then
            InitialDefaultPanel()

        Else
            Dim strHTTPServer As String
            Dim strHTTPApp As String
            Dim strNameServer As String
            strHTTPServer = Request.ServerVariables("PATH_INFO")
            strNameServer = Request.ServerVariables("SERVER_NAME")
            strHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
            Response.Redirect("http://" & strNameServer & "/" & strHTTPApp & "/NotAuthorized.html")
        End If
    End Sub
#Region "Sub and Functions"
    Private Sub InitialDefaultPanel()
        PnlGrid.Visible = False


        LblErrMessages.Text = ""

    End Sub
    'description : sub ini untuk mengisi data ke combo box branch
    Sub fillcbo()
        Dim m_controller As New DataUserControlController
        With cboBranch
            If Me.IsHoBranch Then
                .DataSource = m_controller.GetBranchAll(GetConnectionString)
                .DataValueField = "ID"
                .DataTextField = "Name"
                .DataBind()
                .Items.Insert(0, "Select One")
                .Items(0).Value = "0"
                .Items.Insert(1, "ALL")
                .Items(0).Value = "ALL"
            Else
                .DataSource = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
                .DataValueField = "ID"
                .DataTextField = "Name"
                .DataBind()
                .Items.Insert(0, "Select One")
                .Items(0).Value = "0"
                Dim strBranch() As String
                strBranch = Split(Me.sesBranchId, ",")
                If UBound(strBranch) > 0 Then
                    .Items.Insert(1, "ALL")
                    .Items(1).Value = "ALL"
                End If
            End If
        End With

    End Sub


    'description : sub ini untuk bind data ao ke datagrid

    Sub Bindgrid(ByVal cmdWhere As String, ByVal criteria As String, ByVal cmSort As String)
        Dim dtvEntity As New DataView
        Dim dtsEntity As New DataTable

        With oAODirectSales
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = cmSort
            .Criteria = criteria

        End With

        oAODirectSales = m_AODirectSales.AOIndirectListing(oAODirectSales)

        With oAODirectSales
            lblRecord.Text = CType(.TotalRecord, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecord
        End With

        dtsEntity = oAODirectSales.ListData
        dtsEntity.TableName = "Table"

        dtvEntity = dtsEntity.DefaultView

        dtgAO.DataSource = dtvEntity

        If Not dtvEntity Is Nothing Then
            dtgAO.DataBind()
        End If

        PagingFooter()
        PnlGrid.Visible = True

    End Sub





#End Region
#Region "Event Handlers"
    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        cboBranch.ClearSelection()
        cboSearchBy.ClearSelection()
        cboAOLevel.ClearSelection()
        txtSearchBy.Text = ""
    End Sub
    Private Sub imbsearch_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbSearch.Click

        Me.SearchBy = ""
        Me.SortBy = ""
        LblErrMessages.Text = ""
        If cboBranch.SelectedValue <> "" And cboBranch.SelectedValue <> "ALL" Then
            Me.SearchBy = "BranchID='" & cboBranch.SelectedValue & "'"
        End If
        If cboSearchBy.SelectedIndex <> -1 And txtSearchBy.Text <> "" Then
            If Me.SearchBy = "" Then
                Me.SearchBy = cboSearchBy.SelectedItem.Value.Trim & "='" & txtSearchBy.Text.Trim & "'"
            Else
                Me.SearchBy = Me.SearchBy & " and " & cboSearchBy.SelectedItem.Value.Trim & "='" & txtSearchBy.Text.Trim & "'"
            End If
        End If
        If cboAOLevel.SelectedItem.Value.Trim = "All" Then
            Me.Criteria = "All"
        ElseIf cboAOLevel.SelectedItem.Value.Trim = "T" Then
            Me.Criteria = "T"
        ElseIf cboAOLevel.SelectedItem.Value.Trim = "E" Then
            Me.Criteria = "E"
        ElseIf cboAOLevel.SelectedItem.Value.Trim = "S" Then
            Me.Criteria = "S"
        Else
            Me.Criteria = "J"
        End If
        Bindgrid(Me.SearchBy, Me.Criteria, Me.SortBy)
    End Sub
    Private Sub dtgAO_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgAO.ItemDataBound
        Dim lnkAOID As HyperLink
        Dim lblBranchid As Label
        Dim lnkBudget As HyperLink
        Dim lnkForecast As HyperLink
        Dim lblName As Label
        If e.Item.ItemIndex >= 0 Then
            lblName = CType(e.Item.FindControl("lblAOName"), Label)
            lblBranchid = CType(e.Item.FindControl("lblBranchID"), Label)
            lnkAOID = CType(e.Item.FindControl("lnkAOID"), HyperLink)
            lnkBudget = CType(e.Item.FindControl("lnkBudget"), HyperLink)
            lnkForecast = CType(e.Item.FindControl("lnkForecast"), HyperLink)
            lnkAOID.NavigateUrl = "javascript:OpenAO('Marketing', '" & lblBranchid.Text.Trim & "','" & lnkAOID.Text.Trim & "')"
            lnkBudget.NavigateUrl = "AOIndirectSalesBudget.aspx?BranchID=" & lblBranchid.Text.Trim & "&EmployeeID=" & lnkAOID.Text.Trim & "&Name=" & lblName.Text.Trim
            lnkForecast.NavigateUrl = "AOIndirectSalesForecast.aspx?BranchID=" & lblBranchid.Text.Trim & "&EmployeeID=" & lnkAOID.Text.Trim & "&Name=" & lblName.Text.Trim
        End If
    End Sub
#End Region

End Class
