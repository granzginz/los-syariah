<%@ Page Language="vb" AutoEventWireup="false" Codebehind="AODirectSalesForecastView.aspx.vb" Inherits="Maxiloan.Webform.SalesMarketing.AODirectSalesForecastView"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>AODirectSalesForecastView</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../Include/Marketing.css" type="text/css" rel="stylesheet">
		<script language="javascript">
			var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
			var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
			var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';			
		</script>
		<SCRIPT src="../Maxiloan.js"></SCRIPT>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<asp:label id="lblMessage" runat="server"></asp:label>
			<table cellSpacing="1" cellPadding="2" width="95%" align="center" border="0">
				<tr class="trtopiungu2">
					<td class="tdtopi" align="center">CMO Direct Sales Forecast - VIEW</td>
				</tr>
			</table>
			<table class="tablegrid" cellSpacing="1" cellPadding="2" width="95%" align="center" border="0">
				<tr>
					<td class="tdgenap" width="20%">CMO ID</td>
					<td class="tdganjil" width="30%" colSpan="5"><asp:hyperlink id="lnkAOID" runat="server"></asp:hyperlink></td>
				</tr>
				<TR>
					<TD class="tdgenap" width="20%">CMO Name</TD>
					<TD class="tdganjil" width="30%" colSpan="5"><asp:label id="lblAOName" runat="server"></asp:label></TD>
				</TR>
				<TR>
					<TD class="tdgenap" width="20%">Year</TD>
					<TD class="tdganjil" width="30%" colSpan="5"><asp:label id="lblYear" runat="server"></asp:label></TD>
				</TR>
			</table>
			<TABLE class="tablegrid" cellSpacing="1" cellPadding="0" width="95%" align="center" border="0">
				<TR>
					<TD class="tdjudul" align="left">FORECAST DATA</TD>
				</TR>
			</TABLE>
			<TABLE class="tablegrid" cellSpacing="0" cellPadding="0" width="95%" align="center" border="0">
				<TR class="tdjudul">
					<TD align="center" width="20%">Month</TD>
					<TD align="center" width="40%">New Asset</TD>
					<TD align="center" width="40%">Used Asset</TD>
				</TR>
			</TABLE>
			<TABLE class="tablegrid" id="Tabl6" cellSpacing="0" cellPadding="0" width="95%" align="center"
				border="0">
				<TR>
					<TD><asp:datagrid id="DtgForecastAdd" runat="server" Visible="True" width="100%" allowpaging="True"
							allowsorting="True" autogeneratecolumns="False" bordercolor="#CCCCCC" borderstyle="None" borderwidth="1px"
							backcolor="White" cellpadding="0" PageSize="12" ShowHeader="False">
							<SelectedItemStyle CssClass="tdgenap"></SelectedItemStyle>
							<AlternatingItemStyle CssClass="tdgenap"></AlternatingItemStyle>
							<ItemStyle Height="25px" CssClass="tdganjil"></ItemStyle>
							<Columns>
								<asp:TemplateColumn>
									<HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
									<ItemStyle HorizontalAlign="left" Width="20%"></ItemStyle>
									<ItemTemplate>
										<ASP:LABEL id="lblMonth" runat="server">
											<%#container.dataitem("Monthname")%>
										</ASP:LABEL>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn>
									<HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
									<ItemStyle HorizontalAlign="left" Width="40%"></ItemStyle>
									<ItemTemplate>
										Unit
										<asp:label id="lblNewUnit" runat="server" Width="10%">
											<%#container.dataitem("Unit")%>
										</asp:label>
										Amount
										<asp:label id="LblNewAmount" runat="server" Width="30%">
											<%#formatnumber(container.dataitem("Amount"),2)%>
										</asp:label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn>
									<HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
									<ItemStyle HorizontalAlign="left" Width="40%"></ItemStyle>
									<ItemTemplate>
										Unit
										<asp:label id="lblAssetUnit" runat="server" Width="10%">
											<%#container.dataitem("Unit2")%>
										</asp:label>
										Amount
										<asp:label id="lblAssetAmount" runat="server" Width="30%">
											<%#formatnumber(container.dataitem("Amount2"),2)%>
										</asp:label>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
							<PagerStyle Visible="False" HorizontalAlign="Left" ForeColor="#000066" BackColor="White" Mode="NumericPages"></PagerStyle>
						</asp:datagrid></TD>
				</TR>
			</TABLE>
			<table cellSpacing="0" cellPadding="0" width="95%" align="center" border="0">
				<tr>
					<td width="50%" height="30">
						<asp:imagebutton id="imbOK" runat="server" ImageUrl="../images/buttonOK.gif"></asp:imagebutton>
					</td>
					<td align="right" width="50%"></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
