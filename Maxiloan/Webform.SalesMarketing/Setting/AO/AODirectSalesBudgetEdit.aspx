<%@ Page Language="vb" AutoEventWireup="false" Codebehind="AODirectSalesBudgetEdit.aspx.vb" Inherits="Maxiloan.Webform.SalesMarketing.AODirectSalesBudgetEdit"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>AODirectSalesBudgetEdit</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../Include/Marketing.css" type="text/css" rel="stylesheet">
		<script language="javascript">
			var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
			var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
			var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';			
		</script>
		<SCRIPT src="../Maxiloan.js"></SCRIPT>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<asp:label id="lblMessage" runat="server"></asp:label>
			<table cellSpacing="0" cellPadding="0" width="95%" border="0">
				<tr class="trtopiungu2">					
					<td class="tdtopi" align="center">CMO Direct Sales Budget - Edit</td>					
				</tr>
			</table>
			<table class="tablegrid" cellSpacing="1" cellPadding="2" width="95%" border="0">
				<tr>
					<td class="tdgenap" width="20%">CMO ID</td>
					<td class="tdganjil" width="30%" colSpan="5"><asp:hyperlink id="lnkAOID" runat="server"></asp:hyperlink></td>
				</tr>
				<TR>
					<TD class="tdgenap" width="20%">CMO Name</TD>
					<TD class="tdganjil" width="30%" colSpan="5"><asp:label id="lblAOName" runat="server"></asp:label></TD>
				</TR>
				<TR>
					<TD class="tdgenap" width="20%">Year
					</TD>
					<TD class="tdganjil" width="30%" colSpan="5"><asp:label id="lblYear" runat="server"></asp:label></TD>
				</TR>
				<TR>
					<TD class="tdgenap" width="20%">Month
					</TD>
					<TD class="tdganjil" width="30%" colSpan="5"><asp:label id="lblMonth" runat="server"></asp:label></TD>
				</TR>
				<TR>
					<TD class="tdgenap" width="20%">Asset Status
					</TD>
					<TD class="tdganjil" width="30%" colSpan="5"><asp:label id="lblAsset" runat="server"></asp:label></TD>
				</TR>
				<TR>
					<TD class="tdgenap" width="20%">Unit</TD>
					<TD class="tdganjil" width="30%" colSpan="5"><asp:textbox id="txtUnit" runat="server" CssClass="inptype"></asp:textbox><asp:requiredfieldvalidator id="rfvUnit" runat="server" ControlToValidate="txtUnit" Display="Dynamic" ErrorMessage="Must Be Filled">Must Be Filled</asp:requiredfieldvalidator><asp:regularexpressionvalidator id="regAmountReceive" runat="server" Display="Dynamic" enabled="True" validationexpression="^\d{1,}$|^\d{1,}[.{1}]\d{1,}$"
							visible="True" errormessage="You have to fill numeric value" controltovalidate="txtUnit"></asp:regularexpressionvalidator></TD>
				</TR>
				<TR>
					<TD class="tdgenap" width="20%">Amount</TD>
					<TD class="tdganjil" width="30%" colSpan="5"><asp:textbox id="txtAmount" runat="server" CssClass="inptype"></asp:textbox><asp:requiredfieldvalidator id="rfvAmount" runat="server" ControlToValidate="txtAmount" Display="Dynamic" ErrorMessage="Must Be Filled">Must Be Filled</asp:requiredfieldvalidator><asp:regularexpressionvalidator id="Regularexpressionvalidator1" runat="server" Display="Dynamic" enabled="True"
							validationexpression="^\d{1,}$|^\d{1,}[.{1}]\d{1,}$" visible="True" errormessage="You have to fill numeric value" controltovalidate="txtAmount"></asp:regularexpressionvalidator></TD>
				</TR>
			</table>
			<table cellSpacing="0" cellPadding="0" width="95%" border="0">
				<tr>
					<td width="50%" height="30">
						<asp:imagebutton id="imbSave" runat="server" CausesValidation="True" ImageUrl="../images/buttonSave.gif"></asp:imagebutton>&nbsp;
						<asp:imagebutton id="imbCancel" runat="server" CausesValidation="False" ImageUrl="../images/buttonCancel.gif"></asp:imagebutton></td>
					<td align="right" width="50%"></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
