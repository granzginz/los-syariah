<%@ Page Language="vb" AutoEventWireup="false" Codebehind="AODirectSalesBudgetAdd_.aspx.vb" Inherits="Maxiloan.Webform.SalesMarketing.AODirectSalesBudgetAdd"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>AODirectSalesBudgetAdd</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../Include/Marketing.css" type="text/css" rel="stylesheet">
		<script language="javascript">
			var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
			var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
			var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';			
		</script>
		<SCRIPT src="../Maxiloan.js"></SCRIPT>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<asp:label id="lblMessage" runat="server" ForeColor="Red"></asp:label>
			<table cellSpacing="0" cellPadding="0" width="95%" border="0">
				<tr class="trtopiungu2">					
					<td class="tdtopi" align="center">CMO Direct Sales Budget - ADD</td>					
				</tr>
			</table>
			<table class="tablegrid" cellSpacing="1" cellPadding="2" width="95%" border="0">
				<tr>
					<td class="tdgenap" width="20%">CMO ID</td>
					<td class="tdganjil" width="30%" colSpan="5"><asp:hyperlink id="lnkAOID" runat="server"></asp:hyperlink></td>
				</tr>
				<TR>
					<TD class="tdgenap" width="20%">CMO Name</TD>
					<TD class="tdganjil" width="30%" colSpan="5"><asp:label id="lblAOName" runat="server"></asp:label></TD>
				</TR>
				<TR>
					<TD class="tdgenap" width="20%">Year <FONT color="#ff6666">(yyyy)</FONT></TD>
					<TD class="tdganjil" width="30%" colSpan="5"><asp:textbox id="txtYear" runat="server" CssClass="inptype"></asp:textbox><asp:rangevalidator id="revYear" runat="server" Display="Dynamic" Type="Integer" MinimumValue="1900"
							MaximumValue="3100" ControlToValidate="txtYear" ErrorMessage="Year Must Be In Numeric">Year Must Be In Numeric</asp:rangevalidator><asp:requiredfieldvalidator id="rfvYear" runat="server" Display="Dynamic" ControlToValidate="txtYear" ErrorMessage="Must Be Filled">Must Be Filled</asp:requiredfieldvalidator></TD>
				</TR>
			</table>
			<table cellSpacing="0" cellPadding="0" width="95%" border="0">
				<tr class="trtopiungu2">					
					<td class="tdtopi" align="left">Budget Data</td>					
				</tr>
			</table>
			<table class="tablegrid" cellSpacing="1" cellPadding="2" width="95%" border="0">
				<tr>
					<td class="tdjudul" align="center" width="20%">Month</td>
					<td class="tdjudul" align="center" width="40%">New Asset</td>
					<td class="tdjudul" align="center" width="40%">Used Asset</td>
				</tr>
			</table>
			<table class="tablegrid" cellSpacing="1" cellPadding="2" width="95%" border="0">
				<tr>
					<td class="tdgenap" width="20%">January</td>
					<td class="tdganjil" width="40%">Unit
						<asp:textbox id="txtNewUnit1" runat="server" CssClass="inptype" Width="30%"></asp:textbox>Amount<asp:textbox id="txtNewAmount1" runat="server" CssClass="inptype"></asp:textbox>
						<asp:requiredfieldvalidator id="Requiredfieldvalidator24" runat="server" Display="Dynamic" ControlToValidate="txtNewAmount1"
							ErrorMessage="Must Be Filled">Must Be Filled</asp:requiredfieldvalidator>
						<asp:requiredfieldvalidator id="rfv1" runat="server" Display="Dynamic" ControlToValidate="txtNewUnit1" ErrorMessage="Must Be Filled">Must Be Filled</asp:requiredfieldvalidator>
						<asp:rangevalidator id="revAmount1" runat="server" Display="Dynamic" Type="Integer" MinimumValue="0"
							MaximumValue="9999" ControlToValidate="txtNewUnit1" ErrorMessage="Unit Must Be In Numeric Value">Unit Must Be In Numeric Value</asp:rangevalidator>&nbsp;<asp:regularexpressionvalidator id="Regularexpressionvalidator1" runat="server" controltovalidate="txtNewAmount1"
							errormessage="You have to fill numeric value" Display="Dynamic" visible="True" validationexpression="^\d{1,}$|^\d{1,}[.{1}]\d{1,}$" enabled="True"></asp:regularexpressionvalidator></td>
					<td class="tdganjil" width="40%">Unit
						<asp:textbox id="txtAssetUnit1" runat="server" CssClass="inptype" Width="30%"></asp:textbox>Amount<asp:textbox id="txtAssetAmount1" runat="server" CssClass="inptype"></asp:textbox>
						<asp:requiredfieldvalidator id="Requiredfieldvalidator25" runat="server" Display="Dynamic" ControlToValidate="txtAssetUnit1"
							ErrorMessage="Must Be Filled">Must Be Filled</asp:requiredfieldvalidator>
						<asp:requiredfieldvalidator id="Requiredfieldvalidator1" runat="server" ErrorMessage="Must Be Filled" ControlToValidate="txtAssetAmount1"
							Display="Dynamic">Must Be Filled</asp:requiredfieldvalidator>
						<asp:rangevalidator id="Rangevalidator26" runat="server" Display="Dynamic" Type="Integer" MinimumValue="0"
							MaximumValue="9999" ControlToValidate="txtAssetUnit1" ErrorMessage="Unit Must Be In Numeric Value">Unit Must Be In Numeric Value</asp:rangevalidator>&nbsp;<asp:regularexpressionvalidator id="Regularexpressionvalidator3" runat="server" controltovalidate="txtAssetAmount1"
							errormessage="You have to fill numeric value" Display="Dynamic" visible="True" validationexpression="^\d{1,}$|^\d{1,}[.{1}]\d{1,}$" enabled="True"></asp:regularexpressionvalidator></td>
				</tr>
				<tr>
					<td class="tdgenap" width="20%">February</td>
					<td class="tdganjil" width="40%">Unit
						<asp:textbox id="txtNewUnit2" runat="server" CssClass="inptype" Width="30%"></asp:textbox>Amount<asp:textbox id="txtNewAmount2" runat="server" CssClass="inptype"></asp:textbox>
						<asp:requiredfieldvalidator id="Requiredfieldvalidator26" runat="server" Display="Dynamic" ControlToValidate="txtNewUnit2"
							ErrorMessage="Must Be Filled">Must Be Filled</asp:requiredfieldvalidator>
						<asp:requiredfieldvalidator id="Requiredfieldvalidator2" runat="server" ErrorMessage="Must Be Filled" ControlToValidate="txtNewAmount2"
							Display="Dynamic">Must Be Filled</asp:requiredfieldvalidator>
						<asp:rangevalidator id="Rangevalidator2" runat="server" Display="Dynamic" Type="Integer" MinimumValue="0"
							MaximumValue="9999" ControlToValidate="txtNewUnit2" ErrorMessage="Unit Must Be In Numeric Value">Unit Must Be In Numeric Value</asp:rangevalidator>&nbsp;<asp:regularexpressionvalidator id="Regularexpressionvalidator2" runat="server" controltovalidate="txtNewAmount2"
							errormessage="You have to fill numeric value" Display="Dynamic" visible="True" validationexpression="^\d{1,}$|^\d{1,}[.{1}]\d{1,}$" enabled="True"></asp:regularexpressionvalidator></td>
					<td class="tdganjil" width="40%">Unit
						<asp:textbox id="txtAssetUnit2" runat="server" CssClass="inptype" Width="30%"></asp:textbox>Amount<asp:textbox id="txtAssetAmount2" runat="server" CssClass="inptype"></asp:textbox>
						<asp:requiredfieldvalidator id="Requiredfieldvalidator27" runat="server" ErrorMessage="Must Be Filled" ControlToValidate="txtAssetUnit2"
							Display="Dynamic">Must Be Filled</asp:requiredfieldvalidator>
						<asp:requiredfieldvalidator id="Requiredfieldvalidator3" runat="server" ErrorMessage="Must Be Filled" ControlToValidate="txtAssetAmount2"
							Display="Dynamic">Must Be Filled</asp:requiredfieldvalidator>
						<asp:rangevalidator id="Rangevalidator28" runat="server" Display="Dynamic" Type="Integer" MinimumValue="0"
							MaximumValue="9999" ControlToValidate="txtAssetUnit2" ErrorMessage="Unit Must Be In Numeric Value">Unit Must Be In Numeric Value</asp:rangevalidator>&nbsp;<asp:regularexpressionvalidator id="Regularexpressionvalidator4" runat="server" controltovalidate="txtAssetAmount2"
							errormessage="You have to fill numeric value" Display="Dynamic" visible="True" validationexpression="^\d{1,}$|^\d{1,}[.{1}]\d{1,}$" enabled="True"></asp:regularexpressionvalidator></td>
				</tr>
				<tr>
					<td class="tdgenap" width="20%">March</td>
					<td class="tdganjil" width="40%">Unit
						<asp:textbox id="txtNewUnit3" runat="server" CssClass="inptype" Width="30%"></asp:textbox>Amount<asp:textbox id="txtNewAmount3" runat="server" CssClass="inptype"></asp:textbox>
						<asp:requiredfieldvalidator id="Requiredfieldvalidator28" runat="server" ErrorMessage="Must Be Filled" ControlToValidate="txtNewUnit3"
							Display="Dynamic">Must Be Filled</asp:requiredfieldvalidator>
						<asp:requiredfieldvalidator id="Requiredfieldvalidator4" runat="server" ErrorMessage="Must Be Filled" ControlToValidate="txtNewAmount3"
							Display="Dynamic">Must Be Filled</asp:requiredfieldvalidator>
						<asp:rangevalidator id="Rangevalidator4" runat="server" Display="Dynamic" Type="Integer" MinimumValue="0"
							MaximumValue="9999" ControlToValidate="txtNewUnit3" ErrorMessage="Unit Must Be In Numeric Value">Unit Must Be In Numeric Value</asp:rangevalidator>&nbsp;<asp:regularexpressionvalidator id="Regularexpressionvalidator5" runat="server" controltovalidate="txtNewAmount3"
							errormessage="You have to fill numeric value" Display="Dynamic" visible="True" validationexpression="^\d{1,}$|^\d{1,}[.{1}]\d{1,}$" enabled="True"></asp:regularexpressionvalidator></td>
					<td class="tdganjil" width="40%">Unit
						<asp:textbox id="txtAssetUnit3" runat="server" CssClass="inptype" Width="30%"></asp:textbox>Amount<asp:textbox id="txtAssetAmount3" runat="server" CssClass="inptype"></asp:textbox>
						<asp:requiredfieldvalidator id="Requiredfieldvalidator29" runat="server" ErrorMessage="Must Be Filled" ControlToValidate="txtAssetUnit3"
							Display="Dynamic">Must Be Filled</asp:requiredfieldvalidator>
						<asp:requiredfieldvalidator id="Requiredfieldvalidator5" runat="server" ErrorMessage="Must Be Filled" ControlToValidate="txtAssetAmount3"
							Display="Dynamic">Must Be Filled</asp:requiredfieldvalidator>
						<asp:rangevalidator id="Rangevalidator30" runat="server" Display="Dynamic" Type="Integer" MinimumValue="0"
							MaximumValue="9999" ControlToValidate="txtAssetUnit3" ErrorMessage="Unit Must Be In Numeric Value">Unit Must Be In Numeric Value</asp:rangevalidator>&nbsp;<asp:regularexpressionvalidator id="Regularexpressionvalidator6" runat="server" controltovalidate="txtAssetAmount3"
							errormessage="You have to fill numeric value" Display="Dynamic" visible="True" validationexpression="^\d{1,}$|^\d{1,}[.{1}]\d{1,}$" enabled="True"></asp:regularexpressionvalidator></td>
				</tr>
				<tr>
					<td class="tdgenap" width="20%">April</td>
					<td class="tdganjil" width="40%">Unit
						<asp:textbox id="txtNewUnit4" runat="server" CssClass="inptype" Width="30%"></asp:textbox>Amount<asp:textbox id="txtNewAmount4" runat="server" CssClass="inptype"></asp:textbox>
						<asp:requiredfieldvalidator id="Requiredfieldvalidator30" runat="server" ErrorMessage="Must Be Filled" ControlToValidate="txtNewUnit4"
							Display="Dynamic">Must Be Filled</asp:requiredfieldvalidator>
						<asp:requiredfieldvalidator id="Requiredfieldvalidator6" runat="server" ErrorMessage="Must Be Filled" ControlToValidate="txtNewAmount4"
							Display="Dynamic">Must Be Filled</asp:requiredfieldvalidator>
						<asp:rangevalidator id="Rangevalidator6" runat="server" Display="Dynamic" Type="Integer" MinimumValue="0"
							MaximumValue="9999" ControlToValidate="txtNewUnit4" ErrorMessage="Unit Must Be In Numeric Value">Unit Must Be In Numeric Value</asp:rangevalidator>&nbsp;<asp:regularexpressionvalidator id="Regularexpressionvalidator7" runat="server" controltovalidate="txtNewAmount4"
							errormessage="You have to fill numeric value" Display="Dynamic" visible="True" validationexpression="^\d{1,}$|^\d{1,}[.{1}]\d{1,}$" enabled="True"></asp:regularexpressionvalidator></td>
					<td class="tdganjil" width="40%">Unit
						<asp:textbox id="txtAssetUnit4" runat="server" CssClass="inptype" Width="30%"></asp:textbox>Amount<asp:textbox id="txtAssetAmount4" runat="server" CssClass="inptype"></asp:textbox>
						<asp:requiredfieldvalidator id="Requiredfieldvalidator31" runat="server" ErrorMessage="Must Be Filled" ControlToValidate="txtAssetUnit4"
							Display="Dynamic">Must Be Filled</asp:requiredfieldvalidator>
						<asp:requiredfieldvalidator id="Requiredfieldvalidator7" runat="server" ErrorMessage="Must Be Filled" ControlToValidate="txtAssetAmount4"
							Display="Dynamic">Must Be Filled</asp:requiredfieldvalidator>
						<asp:rangevalidator id="Rangevalidator32" runat="server" Display="Dynamic" Type="Integer" MinimumValue="0"
							MaximumValue="9999" ControlToValidate="txtAssetUnit4" ErrorMessage="Unit Must Be In Numeric Value">Unit Must Be In Numeric Value</asp:rangevalidator>&nbsp;<asp:regularexpressionvalidator id="Regularexpressionvalidator8" runat="server" controltovalidate="txtAssetAmount4"
							errormessage="You have to fill numeric value" Display="Dynamic" visible="True" validationexpression="^\d{1,}$|^\d{1,}[.{1}]\d{1,}$" enabled="True"></asp:regularexpressionvalidator></td>
				</tr>
				<tr>
					<td class="tdgenap" width="20%">May</td>
					<td class="tdganjil" width="40%">Unit
						<asp:textbox id="txtNewUnit5" runat="server" CssClass="inptype" Width="30%"></asp:textbox>Amount<asp:textbox id="txtNewAmount5" runat="server" CssClass="inptype"></asp:textbox>
						<asp:requiredfieldvalidator id="Requiredfieldvalidator32" runat="server" ErrorMessage="Must Be Filled" ControlToValidate="txtNewUnit5"
							Display="Dynamic">Must Be Filled</asp:requiredfieldvalidator>
						<asp:requiredfieldvalidator id="Requiredfieldvalidator8" runat="server" ErrorMessage="Must Be Filled" ControlToValidate="txtNewAmount5"
							Display="Dynamic">Must Be Filled</asp:requiredfieldvalidator>
						<asp:rangevalidator id="Rangevalidator8" runat="server" Display="Dynamic" Type="Integer" MinimumValue="0"
							MaximumValue="9999" ControlToValidate="txtNewUnit5" ErrorMessage="Unit Must Be In Numeric Value">Unit Must Be In Numeric Value</asp:rangevalidator>&nbsp;<asp:regularexpressionvalidator id="Regularexpressionvalidator9" runat="server" controltovalidate="txtNewAmount5"
							errormessage="You have to fill numeric value" Display="Dynamic" visible="True" validationexpression="^\d{1,}$|^\d{1,}[.{1}]\d{1,}$" enabled="True"></asp:regularexpressionvalidator></td>
					<td class="tdganjil" width="40%">Unit
						<asp:textbox id="txtAssetUnit5" runat="server" CssClass="inptype" Width="30%"></asp:textbox>Amount<asp:textbox id="txtAssetAmount5" runat="server" CssClass="inptype"></asp:textbox>
						<asp:requiredfieldvalidator id="Requiredfieldvalidator33" runat="server" ErrorMessage="Must Be Filled" ControlToValidate="txtAssetUnit5"
							Display="Dynamic">Must Be Filled</asp:requiredfieldvalidator>
						<asp:requiredfieldvalidator id="Requiredfieldvalidator9" runat="server" ErrorMessage="Must Be Filled" ControlToValidate="txtAssetAmount5"
							Display="Dynamic">Must Be Filled</asp:requiredfieldvalidator>
						<asp:rangevalidator id="Rangevalidator34" runat="server" Display="Dynamic" Type="Integer" MinimumValue="0"
							MaximumValue="9999" ControlToValidate="txtAssetUnit5" ErrorMessage="Unit Must Be In Numeric Value">Unit Must Be In Numeric Value</asp:rangevalidator>&nbsp;<asp:regularexpressionvalidator id="Regularexpressionvalidator10" runat="server" controltovalidate="txtAssetAmount5"
							errormessage="You have to fill numeric value" Display="Dynamic" visible="True" validationexpression="^\d{1,}$|^\d{1,}[.{1}]\d{1,}$" enabled="True"></asp:regularexpressionvalidator></td>
				</tr>
				<tr>
					<td class="tdgenap" width="20%">June</td>
					<td class="tdganjil" width="40%">Unit
						<asp:textbox id="txtNewUnit6" runat="server" CssClass="inptype" Width="30%"></asp:textbox>Amount<asp:textbox id="txtNewAmount6" runat="server" CssClass="inptype"></asp:textbox>
						<asp:requiredfieldvalidator id="Requiredfieldvalidator34" runat="server" ErrorMessage="Must Be Filled" ControlToValidate="txtNewUnit6"
							Display="Dynamic">Must Be Filled</asp:requiredfieldvalidator>
						<asp:requiredfieldvalidator id="Requiredfieldvalidator10" runat="server" ErrorMessage="Must Be Filled" ControlToValidate="txtNewAmount6"
							Display="Dynamic">Must Be Filled</asp:requiredfieldvalidator>
						<asp:rangevalidator id="Rangevalidator10" runat="server" Display="Dynamic" Type="Integer" MinimumValue="0"
							MaximumValue="9999" ControlToValidate="txtNewUnit6" ErrorMessage="Unit Must Be In Numeric Value">Unit Must Be In Numeric Value</asp:rangevalidator>&nbsp;<asp:regularexpressionvalidator id="Regularexpressionvalidator11" runat="server" controltovalidate="txtNewAmount6"
							errormessage="You have to fill numeric value" Display="Dynamic" visible="True" validationexpression="^\d{1,}$|^\d{1,}[.{1}]\d{1,}$" enabled="True"></asp:regularexpressionvalidator></td>
					<td class="tdganjil" width="40%">Unit
						<asp:textbox id="txtAssetUnit6" runat="server" CssClass="inptype" Width="30%"></asp:textbox>Amount<asp:textbox id="txtAssetAmount6" runat="server" CssClass="inptype"></asp:textbox>
						<asp:requiredfieldvalidator id="Requiredfieldvalidator35" runat="server" ErrorMessage="Must Be Filled" ControlToValidate="txtAssetUnit6"
							Display="Dynamic">Must Be Filled</asp:requiredfieldvalidator>
						<asp:requiredfieldvalidator id="Requiredfieldvalidator11" runat="server" ErrorMessage="Must Be Filled" ControlToValidate="txtAssetAmount6"
							Display="Dynamic">Must Be Filled</asp:requiredfieldvalidator>
						<asp:rangevalidator id="Rangevalidator36" runat="server" Display="Dynamic" Type="Integer" MinimumValue="0"
							MaximumValue="9999" ControlToValidate="txtAssetUnit6" ErrorMessage="Unit Must Be In Numeric Value">Unit Must Be In Numeric Value</asp:rangevalidator>&nbsp;<asp:regularexpressionvalidator id="Regularexpressionvalidator12" runat="server" controltovalidate="txtAssetAmount6"
							errormessage="You have to fill numeric value" Display="Dynamic" visible="True" validationexpression="^\d{1,}$|^\d{1,}[.{1}]\d{1,}$" enabled="True"></asp:regularexpressionvalidator></td>
				</tr>
				<tr>
					<td class="tdgenap" width="20%">July</td>
					<td class="tdganjil" width="40%">Unit
						<asp:textbox id="txtNewUnit7" runat="server" CssClass="inptype" Width="30%"></asp:textbox>Amount<asp:textbox id="txtNewAmount7" runat="server" CssClass="inptype"></asp:textbox>
						<asp:requiredfieldvalidator id="Requiredfieldvalidator36" runat="server" ErrorMessage="Must Be Filled" ControlToValidate="txtNewUnit7"
							Display="Dynamic">Must Be Filled</asp:requiredfieldvalidator>
						<asp:requiredfieldvalidator id="Requiredfieldvalidator12" runat="server" ErrorMessage="Must Be Filled" ControlToValidate="txtNewAmount7"
							Display="Dynamic">Must Be Filled</asp:requiredfieldvalidator>
						<asp:rangevalidator id="Rangevalidator12" runat="server" Display="Dynamic" Type="Integer" MinimumValue="0"
							MaximumValue="9999" ControlToValidate="txtNewUnit7" ErrorMessage="Unit Must Be In Numeric Value">Unit Must Be In Numeric Value</asp:rangevalidator>&nbsp;<asp:regularexpressionvalidator id="Regularexpressionvalidator13" runat="server" controltovalidate="txtNewAmount7"
							errormessage="You have to fill numeric value" Display="Dynamic" visible="True" validationexpression="^\d{1,}$|^\d{1,}[.{1}]\d{1,}$" enabled="True"></asp:regularexpressionvalidator></td>
					<td class="tdganjil" width="40%">Unit
						<asp:textbox id="txtAssetUnit7" runat="server" CssClass="inptype" Width="30%"></asp:textbox>Amount<asp:textbox id="txtAssetAmount7" runat="server" CssClass="inptype"></asp:textbox>
						<asp:requiredfieldvalidator id="Requiredfieldvalidator37" runat="server" ErrorMessage="Must Be Filled" ControlToValidate="txtAssetUnit7"
							Display="Dynamic">Must Be Filled</asp:requiredfieldvalidator>
						<asp:requiredfieldvalidator id="Requiredfieldvalidator13" runat="server" ErrorMessage="Must Be Filled" ControlToValidate="txtAssetAmount7"
							Display="Dynamic">Must Be Filled</asp:requiredfieldvalidator>
						<asp:rangevalidator id="Rangevalidator14" runat="server" Display="Dynamic" Type="Integer" MinimumValue="0"
							MaximumValue="9999" ControlToValidate="txtAssetUnit7" ErrorMessage="Unit Must Be In Numeric Value">Unit Must Be In Numeric Value</asp:rangevalidator>&nbsp;<asp:regularexpressionvalidator id="Regularexpressionvalidator14" runat="server" controltovalidate="txtAssetAmount7"
							errormessage="You have to fill numeric value" Display="Dynamic" visible="True" validationexpression="^\d{1,}$|^\d{1,}[.{1}]\d{1,}$" enabled="True"></asp:regularexpressionvalidator></td>
				</tr>
				<tr>
					<td class="tdgenap" width="20%">August</td>
					<td class="tdganjil" width="40%">Unit
						<asp:textbox id="txtNewUnit8" runat="server" CssClass="inptype" Width="30%"></asp:textbox>Amount<asp:textbox id="txtNewAmount8" runat="server" CssClass="inptype"></asp:textbox>
						<asp:requiredfieldvalidator id="Requiredfieldvalidator38" runat="server" ErrorMessage="Must Be Filled" ControlToValidate="txtNewUnit8"
							Display="Dynamic">Must Be Filled</asp:requiredfieldvalidator>
						<asp:requiredfieldvalidator id="Requiredfieldvalidator14" runat="server" ErrorMessage="Must Be Filled" ControlToValidate="txtNewAmount8"
							Display="Dynamic">Must Be Filled</asp:requiredfieldvalidator>
						<asp:rangevalidator id="Rangevalidator16" runat="server" Display="Dynamic" Type="Integer" MinimumValue="0"
							MaximumValue="9999" ControlToValidate="txtNewUnit8" ErrorMessage="Unit Must Be In Numeric Value">Unit Must Be In Numeric Value</asp:rangevalidator>&nbsp;<asp:regularexpressionvalidator id="Regularexpressionvalidator15" runat="server" controltovalidate="txtNewAmount8"
							errormessage="You have to fill numeric value" Display="Dynamic" visible="True" validationexpression="^\d{1,}$|^\d{1,}[.{1}]\d{1,}$" enabled="True"></asp:regularexpressionvalidator></td>
					<td class="tdganjil" width="40%">Unit
						<asp:textbox id="txtAssetUnit8" runat="server" CssClass="inptype" Width="30%"></asp:textbox>Amount<asp:textbox id="txtAssetAmount8" runat="server" CssClass="inptype"></asp:textbox>
						<asp:requiredfieldvalidator id="Requiredfieldvalidator39" runat="server" ErrorMessage="Must Be Filled" ControlToValidate="txtAssetUnit8"
							Display="Dynamic">Must Be Filled</asp:requiredfieldvalidator>
						<asp:requiredfieldvalidator id="Requiredfieldvalidator15" runat="server" ErrorMessage="Must Be Filled" ControlToValidate="txtAssetAmount8"
							Display="Dynamic">Must Be Filled</asp:requiredfieldvalidator>
						<asp:rangevalidator id="Rangevalidator38" runat="server" Display="Dynamic" Type="Integer" MinimumValue="0"
							MaximumValue="9999" ControlToValidate="txtAssetUnit8" ErrorMessage="Unit Must Be In Numeric Value">Unit Must Be In Numeric Value</asp:rangevalidator>&nbsp;<asp:regularexpressionvalidator id="Regularexpressionvalidator16" runat="server" controltovalidate="txtAssetAmount8"
							errormessage="You have to fill numeric value" Display="Dynamic" visible="True" validationexpression="^\d{1,}$|^\d{1,}[.{1}]\d{1,}$" enabled="True"></asp:regularexpressionvalidator></td>
				</tr>
				<tr>
					<td class="tdgenap" width="20%">September</td>
					<td class="tdganjil" width="40%">Unit
						<asp:textbox id="txtNewUnit9" runat="server" CssClass="inptype" Width="30%"></asp:textbox>Amount<asp:textbox id="txtNewAmount9" runat="server" CssClass="inptype"></asp:textbox>
						<asp:requiredfieldvalidator id="Requiredfieldvalidator40" runat="server" ErrorMessage="Must Be Filled" ControlToValidate="txtNewUnit9"
							Display="Dynamic">Must Be Filled</asp:requiredfieldvalidator>
						<asp:requiredfieldvalidator id="Requiredfieldvalidator16" runat="server" ErrorMessage="Must Be Filled" ControlToValidate="txtNewAmount9"
							Display="Dynamic">Must Be Filled</asp:requiredfieldvalidator>
						<asp:rangevalidator id="Rangevalidator18" runat="server" Display="Dynamic" Type="Integer" MinimumValue="0"
							MaximumValue="9999" ControlToValidate="txtNewUnit9" ErrorMessage="Unit Must Be In Numeric Value">Unit Must Be In Numeric Value</asp:rangevalidator>&nbsp;<asp:regularexpressionvalidator id="Regularexpressionvalidator17" runat="server" controltovalidate="txtNewAmount9"
							errormessage="You have to fill numeric value" Display="Dynamic" visible="True" validationexpression="^\d{1,}$|^\d{1,}[.{1}]\d{1,}$" enabled="True"></asp:regularexpressionvalidator></td>
					<td class="tdganjil" width="40%">Unit
						<asp:textbox id="txtAssetUnit9" runat="server" CssClass="inptype" Width="30%"></asp:textbox>Amount<asp:textbox id="txtAssetAmount9" runat="server" CssClass="inptype"></asp:textbox>
						<asp:requiredfieldvalidator id="Requiredfieldvalidator41" runat="server" ErrorMessage="Must Be Filled" ControlToValidate="txtAssetUnit9"
							Display="Dynamic">Must Be Filled</asp:requiredfieldvalidator>
						<asp:requiredfieldvalidator id="Requiredfieldvalidator17" runat="server" ErrorMessage="Must Be Filled" ControlToValidate="txtAssetAmount9"
							Display="Dynamic">Must Be Filled</asp:requiredfieldvalidator>
						<asp:rangevalidator id="Rangevalidator40" runat="server" Display="Dynamic" Type="Integer" MinimumValue="0"
							MaximumValue="9999" ControlToValidate="txtAssetUnit9" ErrorMessage="Unit Must Be In Numeric Value">Unit Must Be In Numeric Value</asp:rangevalidator>&nbsp;<asp:regularexpressionvalidator id="Regularexpressionvalidator18" runat="server" controltovalidate="txtAssetAmount9"
							errormessage="You have to fill numeric value" Display="Dynamic" visible="True" validationexpression="^\d{1,}$|^\d{1,}[.{1}]\d{1,}$" enabled="True"></asp:regularexpressionvalidator></td>
				</tr>
				<tr>
					<td class="tdgenap" width="20%">October</td>
					<td class="tdganjil" width="40%">Unit
						<asp:textbox id="txtNewUnit10" runat="server" CssClass="inptype" Width="30%"></asp:textbox>Amount<asp:textbox id="txtNewAmount10" runat="server" CssClass="inptype"></asp:textbox>
						<asp:requiredfieldvalidator id="Requiredfieldvalidator42" runat="server" ErrorMessage="Must Be Filled" ControlToValidate="txtNewUnit10"
							Display="Dynamic">Must Be Filled</asp:requiredfieldvalidator>
						<asp:requiredfieldvalidator id="Requiredfieldvalidator18" runat="server" ErrorMessage="Must Be Filled" ControlToValidate="txtNewAmount10"
							Display="Dynamic">Must Be Filled</asp:requiredfieldvalidator>
						<asp:rangevalidator id="Rangevalidator20" runat="server" Display="Dynamic" Type="Integer" MinimumValue="0"
							MaximumValue="9999" ControlToValidate="txtNewUnit10" ErrorMessage="Unit Must Be In Numeric Value">Unit Must Be In Numeric Value</asp:rangevalidator>&nbsp;<asp:regularexpressionvalidator id="Regularexpressionvalidator19" runat="server" controltovalidate="txtNewAmount10"
							errormessage="You have to fill numeric value" Display="Dynamic" visible="True" validationexpression="^\d{1,}$|^\d{1,}[.{1}]\d{1,}$" enabled="True"></asp:regularexpressionvalidator></td>
					<td class="tdganjil" width="40%">Unit
						<asp:textbox id="txtAssetUnit10" runat="server" CssClass="inptype" Width="30%"></asp:textbox>Amount<asp:textbox id="txtAssetAmount10" runat="server" CssClass="inptype"></asp:textbox>
						<asp:requiredfieldvalidator id="Requiredfieldvalidator43" runat="server" ErrorMessage="Must Be Filled" ControlToValidate="txtAssetUnit10"
							Display="Dynamic">Must Be Filled</asp:requiredfieldvalidator>
						<asp:requiredfieldvalidator id="Requiredfieldvalidator19" runat="server" ErrorMessage="Must Be Filled" ControlToValidate="txtAssetAmount10"
							Display="Dynamic">Must Be Filled</asp:requiredfieldvalidator>
						<asp:rangevalidator id="Rangevalidator42" runat="server" Display="Dynamic" Type="Integer" MinimumValue="0"
							MaximumValue="9999" ControlToValidate="txtAssetUnit10" ErrorMessage="Unit Must Be In Numeric Value">Unit Must Be In Numeric Value</asp:rangevalidator>&nbsp;<asp:regularexpressionvalidator id="Regularexpressionvalidator20" runat="server" controltovalidate="txtAssetAmount10"
							errormessage="You have to fill numeric value" Display="Dynamic" visible="True" validationexpression="^\d{1,}$|^\d{1,}[.{1}]\d{1,}$" enabled="True"></asp:regularexpressionvalidator></td>
				</tr>
				<tr>
					<td class="tdgenap" width="20%">November</td>
					<td class="tdganjil" width="40%">Unit
						<asp:textbox id="txtNewUnit11" runat="server" CssClass="inptype" Width="30%"></asp:textbox>Amount<asp:textbox id="txtNewAmount11" runat="server" CssClass="inptype"></asp:textbox>
						<asp:requiredfieldvalidator id="Requiredfieldvalidator44" runat="server" ErrorMessage="Must Be Filled" ControlToValidate="txtNewUnit11"
							Display="Dynamic">Must Be Filled</asp:requiredfieldvalidator>
						<asp:requiredfieldvalidator id="Requiredfieldvalidator20" runat="server" ErrorMessage="Must Be Filled" ControlToValidate="txtNewAmount11"
							Display="Dynamic">Must Be Filled</asp:requiredfieldvalidator>
						<asp:rangevalidator id="Rangevalidator22" runat="server" Display="Dynamic" Type="Integer" MinimumValue="0"
							MaximumValue="9999" ControlToValidate="txtNewUnit11" ErrorMessage="Unit Must Be In Numeric Value">Unit Must Be In Numeric Value</asp:rangevalidator>&nbsp;<asp:regularexpressionvalidator id="Regularexpressionvalidator21" runat="server" controltovalidate="txtNewAmount11"
							errormessage="You have to fill numeric value" Display="Dynamic" visible="True" validationexpression="^\d{1,}$|^\d{1,}[.{1}]\d{1,}$" enabled="True"></asp:regularexpressionvalidator></td>
					<td class="tdganjil" width="40%">Unit
						<asp:textbox id="txtAssetUnit11" runat="server" CssClass="inptype" Width="30%"></asp:textbox>Amount<asp:textbox id="txtAssetAmount11" runat="server" CssClass="inptype"></asp:textbox>
						<asp:requiredfieldvalidator id="Requiredfieldvalidator45" runat="server" ErrorMessage="Must Be Filled" ControlToValidate="txtAssetUnit11"
							Display="Dynamic">Must Be Filled</asp:requiredfieldvalidator>
						<asp:requiredfieldvalidator id="Requiredfieldvalidator21" runat="server" ErrorMessage="Must Be Filled" ControlToValidate="txtAssetAmount11"
							Display="Dynamic">Must Be Filled</asp:requiredfieldvalidator>
						<asp:rangevalidator id="Rangevalidator44" runat="server" Display="Dynamic" Type="Integer" MinimumValue="0"
							MaximumValue="9999" ControlToValidate="txtAssetUnit11" ErrorMessage="Unit Must Be In Numeric Value">Unit Must Be In Numeric Value</asp:rangevalidator>&nbsp;<asp:regularexpressionvalidator id="Regularexpressionvalidator22" runat="server" controltovalidate="txtAssetAmount11"
							errormessage="You have to fill numeric value" Display="Dynamic" visible="True" validationexpression="^\d{1,}$|^\d{1,}[.{1}]\d{1,}$" enabled="True"></asp:regularexpressionvalidator></td>
				</tr>
				<tr>
					<td class="tdgenap" width="20%">December</td>
					<td class="tdganjil" width="40%">Unit
						<asp:textbox id="txtNewUnit12" runat="server" CssClass="inptype" Width="30%"></asp:textbox>Amount<asp:textbox id="txtNewAmount12" runat="server" CssClass="inptype"></asp:textbox>
						<asp:requiredfieldvalidator id="Requiredfieldvalidator46" runat="server" ErrorMessage="Must Be Filled" ControlToValidate="txtNewUnit12"
							Display="Dynamic">Must Be Filled</asp:requiredfieldvalidator>
						<asp:requiredfieldvalidator id="Requiredfieldvalidator22" runat="server" ErrorMessage="Must Be Filled" ControlToValidate="txtNewAmount12"
							Display="Dynamic">Must Be Filled</asp:requiredfieldvalidator>
						<asp:rangevalidator id="Rangevalidator24" runat="server" Display="Dynamic" Type="Integer" MinimumValue="0"
							MaximumValue="9999" ControlToValidate="txtNewUnit12" ErrorMessage="Unit Must Be In Numeric Value">Unit Must Be In Numeric Value</asp:rangevalidator>&nbsp;<asp:regularexpressionvalidator id="Regularexpressionvalidator23" runat="server" controltovalidate="txtNewAmount12"
							errormessage="You have to fill numeric value" Display="Dynamic" visible="True" validationexpression="^\d{1,}$|^\d{1,}[.{1}]\d{1,}$" enabled="True"></asp:regularexpressionvalidator></td>
					<td class="tdganjil" width="40%">Unit
						<asp:textbox id="txtAssetUnit12" runat="server" CssClass="inptype" Width="30%"></asp:textbox>Amount<asp:textbox id="txtAssetAmount12" runat="server" CssClass="inptype"></asp:textbox>
						<asp:requiredfieldvalidator id="Requiredfieldvalidator47" runat="server" ErrorMessage="Must Be Filled" ControlToValidate="txtAssetUnit12"
							Display="Dynamic">Must Be Filled</asp:requiredfieldvalidator>
						<asp:requiredfieldvalidator id="Requiredfieldvalidator23" runat="server" ErrorMessage="Must Be Filled" ControlToValidate="txtAssetAmount12"
							Display="Dynamic">Must Be Filled</asp:requiredfieldvalidator>
						<asp:rangevalidator id="Rangevalidator46" runat="server" Display="Dynamic" Type="Integer" MinimumValue="0"
							MaximumValue="9999" ControlToValidate="txtAssetUnit12" ErrorMessage="Unit Must Be In Numeric Value">Unit Must Be In Numeric Value</asp:rangevalidator>&nbsp;<asp:regularexpressionvalidator id="Regularexpressionvalidator24" runat="server" controltovalidate="txtAssetAmount12"
							errormessage="You have to fill numeric value" Display="Dynamic" visible="True" validationexpression="^\d{1,}$|^\d{1,}[.{1}]\d{1,}$" enabled="True"></asp:regularexpressionvalidator></td>
				</tr>
			</table>
			<table cellSpacing="0" cellPadding="0" width="95%" border="0">
				<tr>
					<td width="50%" height="30">
						<asp:imagebutton id="imbSave" runat="server" ImageUrl="../images/buttonSave.gif" CausesValidation="true"></asp:imagebutton>&nbsp;
						<asp:imagebutton id="imbCancel" runat="server" ImageUrl="../images/buttonCancel.gif" CausesValidation="False"></asp:imagebutton></td>
					<td align="right" width="50%"></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
