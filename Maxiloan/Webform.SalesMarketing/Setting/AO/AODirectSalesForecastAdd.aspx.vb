
#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.General
Imports Maxiloan.webform.UserController
Imports Maxiloan.Controller
#End Region
Public Class AODirectSalesForecastAdd
    Inherits Maxiloan.Webform.WebBased

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    Protected WithEvents lnkAOID As System.Web.UI.WebControls.HyperLink
    Protected WithEvents lblAOName As System.Web.UI.WebControls.Label
    Protected WithEvents txtYear As System.Web.UI.WebControls.TextBox
    Protected WithEvents revYear As System.Web.UI.WebControls.RangeValidator
    Protected WithEvents rfvYear As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents txtNewUnit1 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtNewAmount1 As System.Web.UI.WebControls.TextBox
    Protected WithEvents revAmount1 As System.Web.UI.WebControls.RangeValidator
    Protected WithEvents Regularexpressionvalidator1 As System.Web.UI.WebControls.RegularExpressionValidator
    Protected WithEvents txtAssetUnit1 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtAssetAmount1 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Rangevalidator26 As System.Web.UI.WebControls.RangeValidator
    Protected WithEvents Regularexpressionvalidator3 As System.Web.UI.WebControls.RegularExpressionValidator
    Protected WithEvents txtNewUnit2 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtNewAmount2 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Rangevalidator2 As System.Web.UI.WebControls.RangeValidator
    Protected WithEvents Regularexpressionvalidator2 As System.Web.UI.WebControls.RegularExpressionValidator
    Protected WithEvents txtAssetUnit2 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtAssetAmount2 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Rangevalidator28 As System.Web.UI.WebControls.RangeValidator
    Protected WithEvents Regularexpressionvalidator4 As System.Web.UI.WebControls.RegularExpressionValidator
    Protected WithEvents txtNewUnit3 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtNewAmount3 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Rangevalidator4 As System.Web.UI.WebControls.RangeValidator
    Protected WithEvents Regularexpressionvalidator5 As System.Web.UI.WebControls.RegularExpressionValidator
    Protected WithEvents txtAssetUnit3 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtAssetAmount3 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Rangevalidator30 As System.Web.UI.WebControls.RangeValidator
    Protected WithEvents Regularexpressionvalidator6 As System.Web.UI.WebControls.RegularExpressionValidator
    Protected WithEvents txtNewUnit4 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtNewAmount4 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Rangevalidator6 As System.Web.UI.WebControls.RangeValidator
    Protected WithEvents Regularexpressionvalidator7 As System.Web.UI.WebControls.RegularExpressionValidator
    Protected WithEvents txtAssetUnit4 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtAssetAmount4 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Rangevalidator32 As System.Web.UI.WebControls.RangeValidator
    Protected WithEvents Regularexpressionvalidator8 As System.Web.UI.WebControls.RegularExpressionValidator
    Protected WithEvents txtNewUnit5 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtNewAmount5 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Rangevalidator8 As System.Web.UI.WebControls.RangeValidator
    Protected WithEvents Regularexpressionvalidator9 As System.Web.UI.WebControls.RegularExpressionValidator
    Protected WithEvents txtAssetUnit5 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtAssetAmount5 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Rangevalidator34 As System.Web.UI.WebControls.RangeValidator
    Protected WithEvents Regularexpressionvalidator10 As System.Web.UI.WebControls.RegularExpressionValidator
    Protected WithEvents txtNewUnit6 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtNewAmount6 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Rangevalidator10 As System.Web.UI.WebControls.RangeValidator
    Protected WithEvents Regularexpressionvalidator11 As System.Web.UI.WebControls.RegularExpressionValidator
    Protected WithEvents txtAssetUnit6 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtAssetAmount6 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Rangevalidator36 As System.Web.UI.WebControls.RangeValidator
    Protected WithEvents Regularexpressionvalidator12 As System.Web.UI.WebControls.RegularExpressionValidator
    Protected WithEvents txtNewUnit7 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtNewAmount7 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Rangevalidator12 As System.Web.UI.WebControls.RangeValidator
    Protected WithEvents Regularexpressionvalidator13 As System.Web.UI.WebControls.RegularExpressionValidator
    Protected WithEvents txtAssetUnit7 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtAssetAmount7 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Rangevalidator14 As System.Web.UI.WebControls.RangeValidator
    Protected WithEvents Regularexpressionvalidator14 As System.Web.UI.WebControls.RegularExpressionValidator
    Protected WithEvents txtNewUnit8 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtNewAmount8 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Rangevalidator16 As System.Web.UI.WebControls.RangeValidator
    Protected WithEvents Regularexpressionvalidator15 As System.Web.UI.WebControls.RegularExpressionValidator
    Protected WithEvents txtAssetUnit8 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtAssetAmount8 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Rangevalidator38 As System.Web.UI.WebControls.RangeValidator
    Protected WithEvents Regularexpressionvalidator16 As System.Web.UI.WebControls.RegularExpressionValidator
    Protected WithEvents txtNewUnit9 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtNewAmount9 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Rangevalidator18 As System.Web.UI.WebControls.RangeValidator
    Protected WithEvents Regularexpressionvalidator17 As System.Web.UI.WebControls.RegularExpressionValidator
    Protected WithEvents txtAssetUnit9 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtAssetAmount9 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Rangevalidator40 As System.Web.UI.WebControls.RangeValidator
    Protected WithEvents Regularexpressionvalidator18 As System.Web.UI.WebControls.RegularExpressionValidator
    Protected WithEvents txtNewUnit10 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtNewAmount10 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Rangevalidator20 As System.Web.UI.WebControls.RangeValidator
    Protected WithEvents Regularexpressionvalidator19 As System.Web.UI.WebControls.RegularExpressionValidator
    Protected WithEvents txtAssetUnit10 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtAssetAmount10 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Rangevalidator42 As System.Web.UI.WebControls.RangeValidator
    Protected WithEvents Regularexpressionvalidator20 As System.Web.UI.WebControls.RegularExpressionValidator
    Protected WithEvents txtNewUnit11 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtNewAmount11 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Rangevalidator22 As System.Web.UI.WebControls.RangeValidator
    Protected WithEvents Regularexpressionvalidator21 As System.Web.UI.WebControls.RegularExpressionValidator
    Protected WithEvents txtAssetUnit11 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtAssetAmount11 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Rangevalidator44 As System.Web.UI.WebControls.RangeValidator
    Protected WithEvents Regularexpressionvalidator22 As System.Web.UI.WebControls.RegularExpressionValidator
    Protected WithEvents txtNewUnit12 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtNewAmount12 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Rangevalidator24 As System.Web.UI.WebControls.RangeValidator
    Protected WithEvents Regularexpressionvalidator23 As System.Web.UI.WebControls.RegularExpressionValidator
    Protected WithEvents txtAssetUnit12 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtAssetAmount12 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Rangevalidator46 As System.Web.UI.WebControls.RangeValidator
    Protected WithEvents Regularexpressionvalidator24 As System.Web.UI.WebControls.RegularExpressionValidator
    Protected WithEvents imbSave As System.Web.UI.WebControls.ImageButton
    Protected WithEvents imbCancel As System.Web.UI.WebControls.ImageButton
    Protected WithEvents Requiredfieldvalidator1 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents Requiredfieldvalidator2 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents Requiredfieldvalidator3 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents Requiredfieldvalidator4 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents Requiredfieldvalidator5 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents Requiredfieldvalidator6 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents Requiredfieldvalidator7 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents Requiredfieldvalidator8 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents Requiredfieldvalidator9 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents Requiredfieldvalidator10 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents Requiredfieldvalidator11 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents Requiredfieldvalidator12 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents Requiredfieldvalidator13 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents Requiredfieldvalidator14 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents Requiredfieldvalidator15 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents Requiredfieldvalidator16 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents Requiredfieldvalidator17 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents Requiredfieldvalidator18 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents Requiredfieldvalidator19 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents Requiredfieldvalidator20 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents Requiredfieldvalidator21 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents Requiredfieldvalidator22 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents Requiredfieldvalidator23 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents Requiredfieldvalidator24 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents Requiredfieldvalidator25 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents Requiredfieldvalidator26 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents Requiredfieldvalidator27 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents Requiredfieldvalidator28 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents Requiredfieldvalidator29 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents Requiredfieldvalidator30 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents Requiredfieldvalidator31 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents Requiredfieldvalidator32 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents Requiredfieldvalidator33 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents Requiredfieldvalidator34 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents Requiredfieldvalidator35 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents Requiredfieldvalidator36 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents Requiredfieldvalidator37 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents Requiredfieldvalidator38 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents Requiredfieldvalidator39 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents Requiredfieldvalidator40 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents Requiredfieldvalidator41 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents Requiredfieldvalidator42 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents Requiredfieldvalidator43 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents Requiredfieldvalidator44 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents Requiredfieldvalidator45 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents Requiredfieldvalidator46 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents Requiredfieldvalidator47 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents Requiredfieldvalidator48 As System.Web.UI.WebControls.RequiredFieldValidator

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
#Region "Constanta"
    Private m_AOForecast As New AODirectSalesController
    Private oAOForecast As New Parameter.AODirectSales
    Private counter As Integer
#End Region
#Region "properties"
    Public Property Name() As String
        Get
            Return CType(viewstate("Name"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Name") = Value
        End Set
    End Property
    Public Property EmployeeID() As String
        Get
            Return CType(viewstate("EmployeeID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("EmployeeID") = Value
        End Set
    End Property
    Public Property AssetStatus() As String
        Get
            Return CType(viewstate("AssetStatus"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AssetStatus") = Value
        End Set
    End Property
    Public Property Year() As Integer
        Get
            Return CType(viewstate("Year"), Integer)
        End Get
        Set(ByVal Value As Integer)
            viewstate("Year") = Value
        End Set
    End Property

    Public Property Month() As Integer
        Get
            Return CType(viewstate("Month"), Integer)
        End Get
        Set(ByVal Value As Integer)
            viewstate("Month") = Value
        End Set
    End Property



#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        'Put user code to initialize the page here
        Me.FormID = "MktAODirect"
        If Not Page.IsPostBack Then
            Me.BranchID = Request.QueryString("BranchID")
            Me.EmployeeID = Request.QueryString("EmployeeID")
            Me.Name = Request.QueryString("Name")
            txtNewUnit1.Text = "0"
            txtNewUnit2.Text = "0"
            txtNewUnit3.Text = "0"
            txtNewUnit4.Text = "0"
            txtNewUnit5.Text = "0"
            txtNewUnit6.Text = "0"
            txtNewUnit7.Text = "0"
            txtNewUnit8.Text = "0"
            txtNewUnit9.Text = "0"
            txtNewUnit10.Text = "0"
            txtNewUnit11.Text = "0"
            txtNewUnit12.Text = "0"
            txtAssetUnit1.Text = "0"
            txtAssetUnit2.Text = "0"
            txtAssetUnit3.Text = "0"
            txtAssetUnit4.Text = "0"
            txtAssetUnit5.Text = "0"
            txtAssetUnit6.Text = "0"
            txtAssetUnit7.Text = "0"
            txtAssetUnit8.Text = "0"
            txtAssetUnit9.Text = "0"
            txtAssetUnit10.Text = "0"
            txtAssetUnit11.Text = "0"
            txtAssetUnit12.Text = "0"
            txtNewAmount1.Text = "0"
            txtNewAmount2.Text = "0"
            txtNewAmount3.Text = "0"
            txtNewAmount4.Text = "0"
            txtNewAmount5.Text = "0"
            txtNewAmount6.Text = "0"
            txtNewAmount7.Text = "0"
            txtNewAmount8.Text = "0"
            txtNewAmount9.Text = "0"
            txtNewAmount10.Text = "0"
            txtNewAmount11.Text = "0"
            txtNewAmount12.Text = "0"
            txtAssetAmount1.Text = "0"
            txtAssetAmount2.Text = "0"
            txtAssetAmount3.Text = "0"
            txtAssetAmount4.Text = "0"
            txtAssetAmount5.Text = "0"
            txtAssetAmount6.Text = "0"
            txtAssetAmount7.Text = "0"
            txtAssetAmount8.Text = "0"
            txtAssetAmount9.Text = "0"
            txtAssetAmount10.Text = "0"
            txtAssetAmount11.Text = "0"
            txtAssetAmount12.Text = "0"



           


        End If


        If CheckForm(Me.Loginid, Me.FormID, "Eloan") Then

            lnkAOID.Text = Me.EmployeeID.Trim
            lnkAOID.NavigateUrl = "javascript:OpenAO('Marketing', '" & Me.BranchID.Trim & "','" & Me.EmployeeID.Trim & "')"
            lblAOName.Text = Me.Name.Trim
        Else
            Dim strHTTPServer As String
            Dim strHTTPApp As String
            Dim strNameServer As String
            strHTTPServer = Request.ServerVariables("PATH_INFO")
            strNameServer = Request.ServerVariables("SERVER_NAME")
            strHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
            Response.Redirect("http://" & strNameServer & "/" & strHTTPApp & "/NotAuthorized.html")
        End If
    End Sub
#Region " Event Handlers "
    Private Sub imbSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbSave.Click
        If CInt(txtYear.Text) < Me.BusinessDate.Year Then
            lblMessage.Text = ""
            lblMessage.Text = "Year should be bigger than BusinessDate "
            Exit Sub
        End If
      
        lblMessage.Text = ""
        Dim dttAOForecast As New DataTable
        Dim objrow As DataRow
        Dim i As Integer
        Dim j As Integer = 1
        dttAOForecast.Columns.Add("BranchID", GetType(String))
        dttAOForecast.Columns.Add("AOID", GetType(String))
        dttAOForecast.Columns.Add("ForecastYear", GetType(Integer))

        dttAOForecast.Columns.Add("ForecastMonth", GetType(Integer))
        dttAOForecast.Columns.Add("AssetUsedNew", GetType(String))
        dttAOForecast.Columns.Add("Unit", GetType(Integer))
        dttAOForecast.Columns.Add("Amount", GetType(Decimal))

        'inserting data into datatable

        If CInt(txtYear.Text) > Me.BusinessDate.Year Then
            For j = 1 To 12
                objrow = dttAOForecast.NewRow
                objrow("BranchID") = Me.BranchID
                objrow("AOID") = Me.EmployeeID
                objrow("ForecastYear") = CInt(txtYear.Text)
                objrow("ForecastMonth") = j
                objrow("AssetUsedNew") = "N"
                If j = 1 Then
                    objrow("Unit") = CInt(txtNewUnit1.Text)
                    objrow("Amount") = CDec(txtNewAmount1.Text)
                End If
                If j = 2 Then
                    objrow("Unit") = CInt(txtNewUnit2.Text)
                    objrow("Amount") = CDec(txtNewAmount2.Text)
                End If
                If j = 3 Then
                    objrow("Unit") = CInt(txtNewUnit3.Text)
                    objrow("Amount") = CDec(txtNewAmount3.Text)
                End If
                If j = 4 Then
                    objrow("Unit") = CInt(txtNewUnit4.Text)
                    objrow("Amount") = CDec(txtNewAmount4.Text)
                End If
                If j = 5 Then
                    objrow("Unit") = CInt(txtNewUnit5.Text)
                    objrow("Amount") = CDec(txtNewAmount5.Text)
                End If
                If j = 6 Then
                    objrow("Unit") = CInt(txtNewUnit6.Text)
                    objrow("Amount") = CDec(txtNewAmount6.Text)
                End If
                If j = 7 Then
                    objrow("Unit") = CInt(txtNewUnit7.Text)
                    objrow("Amount") = CDec(txtNewAmount7.Text)
                End If
                If j = 8 Then
                    objrow("Unit") = CInt(txtNewUnit8.Text)
                    objrow("Amount") = CDec(txtNewAmount8.Text)
                End If
                If j = 9 Then
                    objrow("Unit") = CInt(txtNewUnit9.Text)
                    objrow("Amount") = CDec(txtNewAmount9.Text)
                End If
                If j = 10 Then
                    objrow("Unit") = CInt(txtNewUnit10.Text)
                    objrow("Amount") = CDec(txtNewAmount10.Text)
                End If
                If j = 11 Then
                    objrow("Unit") = CInt(txtNewUnit11.Text)
                    objrow("Amount") = CDec(txtNewAmount11.Text)
                End If
                If j = 12 Then
                    objrow("Unit") = CInt(txtNewUnit12.Text)
                    objrow("Amount") = CDec(txtNewAmount12.Text)
                End If
                dttAOForecast.Rows.Add(objrow)
            Next
            j = 1
            For j = 1 To 12
                objrow = dttAOForecast.NewRow
                objrow("BranchID") = Me.BranchID
                objrow("AOID") = Me.EmployeeID
                objrow("ForecastYear") = CInt(txtYear.Text)
                objrow("ForecastMonth") = j
                objrow("AssetUsedNew") = "U"
                If j = 1 Then
                    objrow("Unit") = CInt(txtAssetUnit1.Text)
                    objrow("Amount") = CDec(txtAssetAmount1.Text)
                End If
                If j = 2 Then
                    objrow("Unit") = CInt(txtAssetUnit2.Text)
                    objrow("Amount") = CDec(txtAssetAmount2.Text)
                End If
                If j = 3 Then
                    objrow("Unit") = CInt(txtAssetUnit3.Text)
                    objrow("Amount") = CDec(txtAssetAmount3.Text)
                End If
                If j = 4 Then
                    objrow("Unit") = CInt(txtAssetUnit4.Text)
                    objrow("Amount") = CDec(txtAssetAmount4.Text)
                End If
                If j = 5 Then
                    objrow("Unit") = CInt(txtAssetUnit5.Text)
                    objrow("Amount") = CDec(txtAssetAmount5.Text)
                End If
                If j = 6 Then
                    objrow("Unit") = CInt(txtAssetUnit6.Text)
                    objrow("Amount") = CDec(txtAssetAmount6.Text)
                End If
                If j = 7 Then
                    objrow("Unit") = CInt(txtAssetUnit7.Text)
                    objrow("Amount") = CDec(txtAssetAmount7.Text)
                End If
                If j = 8 Then
                    objrow("Unit") = CInt(txtAssetUnit8.Text)
                    objrow("Amount") = CDec(txtAssetAmount8.Text)
                End If
                If j = 9 Then
                    objrow("Unit") = CInt(txtAssetUnit9.Text)
                    objrow("Amount") = CDec(txtAssetAmount9.Text)
                End If
                If j = 10 Then
                    objrow("Unit") = CInt(txtAssetUnit10.Text)
                    objrow("Amount") = CDec(txtAssetAmount10.Text)
                End If
                If j = 11 Then
                    objrow("Unit") = CInt(txtAssetUnit11.Text)
                    objrow("Amount") = CDec(txtAssetAmount11.Text)
                End If
                If j = 12 Then
                    objrow("Unit") = CInt(txtAssetUnit12.Text)
                    objrow("Amount") = CDec(txtAssetAmount12.Text)
                End If
                dttAOForecast.Rows.Add(objrow)
            Next


        Else
            For j = Me.BusinessDate.Month To 12
                If j > Me.BusinessDate.Month Then
                    objrow = dttAOForecast.NewRow
                    objrow("BranchID") = Me.BranchID
                    objrow("AOID") = Me.EmployeeID
                    objrow("ForecastYear") = CInt(txtYear.Text)
                    objrow("ForecastMonth") = j
                    objrow("AssetUsedNew") = "N"
                    If j = 1 Then
                        objrow("Unit") = CInt(txtNewUnit1.Text)
                        objrow("Amount") = CDec(txtNewAmount1.Text)
                    End If
                    If j = 2 Then
                        objrow("Unit") = CInt(txtNewUnit2.Text)
                        objrow("Amount") = CDec(txtNewAmount2.Text)
                    End If
                    If j = 3 Then
                        objrow("Unit") = CInt(txtNewUnit3.Text)
                        objrow("Amount") = CDec(txtNewAmount3.Text)
                    End If
                    If j = 4 Then
                        objrow("Unit") = CInt(txtNewUnit4.Text)
                        objrow("Amount") = CDec(txtNewAmount4.Text)
                    End If
                    If j = 5 Then
                        objrow("Unit") = CInt(txtNewUnit5.Text)
                        objrow("Amount") = CDec(txtNewAmount5.Text)
                    End If
                    If j = 6 Then
                        objrow("Unit") = CInt(txtNewUnit6.Text)
                        objrow("Amount") = CDec(txtNewAmount6.Text)
                    End If
                    If j = 7 Then
                        objrow("Unit") = CInt(txtNewUnit7.Text)
                        objrow("Amount") = CDec(txtNewAmount7.Text)
                    End If
                    If j = 8 Then
                        objrow("Unit") = CInt(txtNewUnit8.Text)
                        objrow("Amount") = CDec(txtNewAmount8.Text)
                    End If
                    If j = 9 Then
                        objrow("Unit") = CInt(txtNewUnit9.Text)
                        objrow("Amount") = CDec(txtNewAmount9.Text)
                    End If
                    If j = 10 Then
                        objrow("Unit") = CInt(txtNewUnit10.Text)
                        objrow("Amount") = CDec(txtNewAmount10.Text)
                    End If
                    If j = 11 Then
                        objrow("Unit") = CInt(txtNewUnit11.Text)
                        objrow("Amount") = CDec(txtNewAmount11.Text)
                    End If
                    If j = 12 Then
                        objrow("Unit") = CInt(txtNewUnit12.Text)
                        objrow("Amount") = CDec(txtNewAmount12.Text)
                    End If
                    dttAOForecast.Rows.Add(objrow)
                End If
            Next

            For j = Me.BusinessDate.Month To 12
                If j > Me.BusinessDate.Month Then
                    objrow = dttAOForecast.NewRow
                    objrow("BranchID") = Me.BranchID
                    objrow("AOID") = Me.EmployeeID
                    objrow("ForecastYear") = CInt(txtYear.Text)
                    objrow("ForecastMonth") = j
                    objrow("AssetUsedNew") = "U"
                    If j = 1 Then
                        objrow("Unit") = CInt(txtAssetUnit1.Text)
                        objrow("Amount") = CDec(txtAssetAmount1.Text)
                    End If
                    If j = 2 Then
                        objrow("Unit") = CInt(txtAssetUnit2.Text)
                        objrow("Amount") = CDec(txtAssetAmount2.Text)
                    End If
                    If j = 3 Then
                        objrow("Unit") = CInt(txtAssetUnit3.Text)
                        objrow("Amount") = CDec(txtAssetAmount3.Text)
                    End If
                    If j = 4 Then
                        objrow("Unit") = CInt(txtAssetUnit4.Text)
                        objrow("Amount") = CDec(txtAssetAmount4.Text)
                    End If
                    If j = 5 Then
                        objrow("Unit") = CInt(txtAssetUnit5.Text)
                        objrow("Amount") = CDec(txtAssetAmount5.Text)
                    End If
                    If j = 6 Then
                        objrow("Unit") = CInt(txtAssetUnit6.Text)
                        objrow("Amount") = CDec(txtAssetAmount6.Text)
                    End If
                    If j = 7 Then
                        objrow("Unit") = CInt(txtAssetUnit7.Text)
                        objrow("Amount") = CDec(txtAssetAmount7.Text)
                    End If
                    If j = 8 Then
                        objrow("Unit") = CInt(txtAssetUnit8.Text)
                        objrow("Amount") = CDec(txtAssetAmount8.Text)
                    End If
                    If j = 9 Then
                        objrow("Unit") = CInt(txtAssetUnit9.Text)
                        objrow("Amount") = CDec(txtAssetAmount9.Text)
                    End If
                    If j = 10 Then
                        objrow("Unit") = CInt(txtAssetUnit10.Text)
                        objrow("Amount") = CDec(txtAssetAmount10.Text)
                    End If
                    If j = 11 Then
                        objrow("Unit") = CInt(txtAssetUnit11.Text)
                        objrow("Amount") = CDec(txtAssetAmount11.Text)
                    End If
                    If j = 12 Then
                        objrow("Unit") = CInt(txtAssetUnit12.Text)
                        objrow("Amount") = CDec(txtAssetAmount12.Text)
                    End If
                    dttAOForecast.Rows.Add(objrow)
                End If
            Next
        End If







        oAOForecast.strConnection = GetConnectionString()
        oAOForecast.ListData = dttAOForecast
        Try
            m_AOForecast.AOForecastAdd(oAOForecast)
            Response.Redirect("AODirectSalesForecast.aspx?BranchID=" & Me.BranchID.Trim & "&EmployeeID=" & Me.EmployeeID.Trim & "&Name=" & Me.Name.Trim)
        Catch ex As Exception
            lblMessage.Text = ""
            lblMessage.Text = "Data Has Already Exists"
            lblMessage.Visible = True
        End Try

    End Sub
    Private Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbCancel.Click
        Response.Redirect("AODirectSalesForecast.aspx?BranchID=" & Me.BranchID.Trim & "&EmployeeID=" & Me.EmployeeID.Trim & "&Name=" & Me.Name.Trim)
    End Sub
#End Region
End Class
