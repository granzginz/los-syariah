﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.General
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
Imports System.Reflection

#End Region
Public Class AODirectSalesBudgetAdd1
    Inherits Maxiloan.Webform.WebBased
#Region "Constanta"
    Private m_AOBudget As New AODirectSalesController
    Private oAOBudget As New Parameter.AODirectSales
    Protected WithEvents txtNewAssetUnit As ucNumberFormat
    Protected WithEvents txtNewAssetAmount As ucNumberFormat
    Protected WithEvents txtUsedAssetUnit As ucNumberFormat
    Protected WithEvents txtUsedAssetAmount As ucNumberFormat


#End Region

    'Public NotOverridable Overrides Sub ProcessRequest(context As HttpContext)
    '    MyBase.ProcessRequest(context)
    '    If CypherContainsAjax(context.Request.QueryString("d")) Then
    '        context.Response.Write(OnFormSubmit)
    '    End If
    'End Sub

    'Private Function CypherContainsAjax(cypher As String) As Boolean
    '    Dim text = DecryptString(cypher)
    '    If text Is Nothing Then
    '        Return True
    '    End If
    '    Then Add it everywhere. What else could I do? :D
    '    Return text.Contains("MicrosoftAjaxWebForms")
    'End Function

    'Private Function DecryptString(cypher As String) As String
    '    If PageDecryptString Is Nothing Then
    '        Return Nothing
    '    End If
    '    Return DirectCast(PageDecryptString.Invoke(Nothing, New Object() {cypher}), String)
    'End Function
    'Private Shared PageDecryptString As MethodInfo
    'Shared Sub New()
    '    PageDecryptString = GetType(Page).GetMethod("DecryptString", System.Reflection.BindingFlags.[Static] Or System.Reflection.BindingFlags.NonPublic)
    'End Sub


#Region "properties"
    Public Property Name() As String
        Get
            Return CType(ViewState("Name"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Name") = Value
        End Set
    End Property
    Public Property EmployeeID() As String
        Get
            Return CType(ViewState("EmployeeID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("EmployeeID") = Value
        End Set
    End Property
    Public Property AssetStatus() As String
        Get
            Return CType(ViewState("AssetStatus"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("AssetStatus") = Value
        End Set
    End Property
    Public Property Year() As Integer
        Get
            Return CType(ViewState("Year"), Integer)
        End Get
        Set(ByVal Value As Integer)
            ViewState("Year") = Value
        End Set
    End Property

    Public Property Month() As Integer
        Get
            Return CType(ViewState("Month"), Integer)
        End Get
        Set(ByVal Value As Integer)
            ViewState("Month") = Value
        End Set
    End Property
    Public Property EventStatus() As String
        Get
            Return CType(ViewState("EventStatus"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("EventStatus") = Value
        End Set
    End Property
    Public Property ParamAODirectSales As IList(Of Parameter.AODirectSales)
        Get
            Return CType(ViewState("AODirectSales"), IList(Of Parameter.AODirectSales))
        End Get
        Set(value As IList(Of Parameter.AODirectSales))
            ViewState("AODirectSales") = value
        End Set
    End Property

#End Region



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.FormID = "MktAODirect"

        If SessionInvalid() Then
            Exit Sub
        End If
        If Not Page.IsPostBack Then


            Me.BranchID = Request.QueryString("BranchID")
            Me.EmployeeID = Request.QueryString("EmployeeID")
            Me.Name = Request.QueryString("Name")
            Me.Month = Request.QueryString("Month")
            Me.Year = IIf(Request.QueryString("Year") = "", Date.Now.Year, Request.QueryString("Year"))
            Me.EventStatus = Request.QueryString("EventStatus")

            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                cboYear.Items.Clear()
                For i As Integer = CInt(Date.Now.Year) - 3 To CInt(Date.Now.Year) + 3
                    cboYear.Items.Add(i.ToString)
                Next
                lnkAOID.Text = Me.EmployeeID.Trim
                lnkAOID.NavigateUrl = "javascript:OpenAO('Marketing', '" & Me.BranchID.Trim & "','" & Me.EmployeeID.Trim & "')"
                lblAOName.Text = Me.Name.Trim
                cboYear.SelectedValue = CInt(Me.Year)
                Dim strFilter1 As String = " and BranchID = '" & Me.BranchID.Trim & "' and AOID ='" & Me.EmployeeID & "' and BudgetYear =" & CInt(Me.Year) & ""
                Dim strFilter2 As String = ""
              
                If Me.EventStatus = "Edit" Then
                    cboYear.Enabled = False
                    strFilter2 = " where ID = ' " & Me.Month & "'"
                Else
                    cboYear.Enabled = True
                    strFilter2 = " where ID not in (SELECT ab.BudgetMonth FROM dbo.AOBudget AS ab WHERE ab.BranchID='" & Me.BranchID.Trim & "' AND ab.AOID='" & Me.EmployeeID & "' AND BudgetYear =" & CInt(Me.Year) & ")"

                End If
                grdAODirectSalesBudget.DataSource = ViewAODirectSales(strFilter1, strFilter2).DefaultView
                grdAODirectSalesBudget.DataBind()
                'Dim strHTTPServer As String
                'Dim strHTTPApp As String
                'Dim strNameServer As String
                'strHTTPServer = Request.ServerVariables("PATH_INFO")
                'strNameServer = Request.ServerVariables("SERVER_NAME")
                'strHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                'Response.Redirect("http://" & strNameServer & "/" & strHTTPApp & "/NotAuthorized.html")
            End If
        End If


    End Sub
    Public Function ViewAODirectSales(ByVal Where1 As String, ByVal Where2 As String) As DataTable
        Dim whr1 As String = Where1
        Dim whr2 As String = Where2
        Dim oEntityAODirectSales As New Parameter.AODirectSales
        oEntityAODirectSales.strConnection = GetConnectionString()
        oEntityAODirectSales.WhereCond = whr1
        oEntityAODirectSales.WhereCond2 = Where2
        Dim dtBranchRefund As DataTable
        dtBranchRefund = m_AOBudget.AOBudgetList(oEntityAODirectSales).ListData
        Return dtBranchRefund
    End Function

    Protected Sub ButtonSaveEdit_Click(sender As Object, e As EventArgs) Handles ButtonSaveEdit.Click
        Try
            
        Dim oAODirectSales As New Parameter.AODirectSales
        oAODirectSales.BranchId = Me.BranchID
        oAODirectSales.EmployeeID = Me.EmployeeID.Trim()
        oAODirectSales.Year = CInt(cboYear.SelectedValue)
        oAODirectSales.strConnection = GetConnectionString()

        For n As Integer = 0 To grdAODirectSalesBudget.DataKeys.Count - 1
            oAODirectSales.Month = grdAODirectSalesBudget.DataKeys(n).ToString()
            Dim NewAssetUnit As ucNumberFormat = CType(grdAODirectSalesBudget.Items(n).FindControl("txtNewAssetUnit"), ucNumberFormat)
            Dim NewAssetAmount As ucNumberFormat = CType(grdAODirectSalesBudget.Items(n).FindControl("txtNewAssetAmount"), ucNumberFormat)
            Dim UsedAssetUnit As ucNumberFormat = CType(grdAODirectSalesBudget.Items(n).FindControl("txtUsedAssetUnit"), ucNumberFormat)
            Dim UsedAssetAmount As ucNumberFormat = CType(grdAODirectSalesBudget.Items(n).FindControl("txtUsedAssetAmount"), ucNumberFormat)
            oAODirectSales.NewAssetUnit = NewAssetUnit.Text
            oAODirectSales.NewAssetAmount = NewAssetAmount.Text
            oAODirectSales.UsedAssetUnit = UsedAssetUnit.Text
            oAODirectSales.UsedAssetAmount = UsedAssetAmount.Text

                m_AOBudget.AOBudgetSaveEdit(oAODirectSales)
               
            Next
            Response.Redirect("AODirectSalesBudget.aspx?BranchID=" & Me.BranchID.Trim & "&EmployeeID=" & Me.EmployeeID.Trim & "&Name=" & Me.Name.Trim)
            ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_SUCCESS, False)
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
            Exit Sub

        End Try
    End Sub

    Protected Sub ButtonCancelEdit_Click(sender As Object, e As EventArgs) Handles ButtonCancelEdit.Click
        Response.Redirect("AODirectSalesBudget.aspx?BranchID=" & Me.BranchID.Trim & "&EmployeeID=" & Me.EmployeeID.Trim & "&Name=" & Me.Name.Trim)
    End Sub

   
    Protected Sub cboYear_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboYear.SelectedIndexChanged
        Dim strFilter1 As String = " and BranchID = '" & Me.BranchID.Trim & "' and AOID ='" & Me.EmployeeID & "' and BudgetYear =" & CInt(cboYear.SelectedValue) & ""
        Dim strFilter2 As String = " where ID not in (SELECT ab.BudgetMonth FROM dbo.AOBudget AS ab WHERE ab.BranchID='" & Me.BranchID.Trim & "' AND ab.AOID='" & Me.EmployeeID & "' AND BudgetYear =" & CInt(cboYear.SelectedValue) & ")"
        grdAODirectSalesBudget.DataSource = ViewAODirectSales(strFilter1, strFilter2).DefaultView
        grdAODirectSalesBudget.DataBind()
    End Sub
End Class