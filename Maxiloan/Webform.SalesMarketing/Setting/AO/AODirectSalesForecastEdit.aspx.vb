
#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.General
Imports Maxiloan.webform.UserController
Imports Maxiloan.Controller
#End Region
Public Class AODirectSalesForecastEdit
    Inherits Maxiloan.Webform.WebBased
#Region "Constanta"
    Private m_AOForecast As New AODirectSalesController
   
    Private oAOForecast As New Parameter.AODirectSales
#End Region
#Region "properties"
    Public Property Name() As String
        Get
            Return CType(viewstate("Name"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Name") = Value
        End Set
    End Property
    Public Property EmployeeID() As String
        Get
            Return CType(viewstate("EmployeeID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("EmployeeID") = Value
        End Set
    End Property
    Public Property AssetStatus() As String
        Get
            Return CType(viewstate("AssetStatus"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AssetStatus") = Value
        End Set
    End Property
    Public Property Year() As Integer
        Get
            Return CType(viewstate("Year"), Integer)
        End Get
        Set(ByVal Value As Integer)
            viewstate("Year") = Value
        End Set
    End Property

    Public Property Month() As Integer
        Get
            Return CType(viewstate("Month"), Integer)
        End Get
        Set(ByVal Value As Integer)
            viewstate("Month") = Value
        End Set
    End Property

    Public Property MonthName() As String
        Get
            Return CType(viewstate("MonthName"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("MonthName") = Value
        End Set
    End Property

#End Region
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    Protected WithEvents lnkAOID As System.Web.UI.WebControls.HyperLink
    Protected WithEvents lblAOName As System.Web.UI.WebControls.Label
    Protected WithEvents lblYear As System.Web.UI.WebControls.Label
    Protected WithEvents lblMonth As System.Web.UI.WebControls.Label
    Protected WithEvents lblAsset As System.Web.UI.WebControls.Label
    Protected WithEvents txtUnit As System.Web.UI.WebControls.TextBox
    Protected WithEvents rfvUnit As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents regAmountReceive As System.Web.UI.WebControls.RegularExpressionValidator
    Protected WithEvents txtAmount As System.Web.UI.WebControls.TextBox
    Protected WithEvents rfvAmount As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents Regularexpressionvalidator1 As System.Web.UI.WebControls.RegularExpressionValidator
    Protected WithEvents imbSave As System.Web.UI.WebControls.ImageButton
    Protected WithEvents imbCancel As System.Web.UI.WebControls.ImageButton

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Me.FormID = "MktAODirect"
        If Not Page.IsPostBack Then
            Me.BranchID = Request.QueryString("BranchID")
            Me.EmployeeID = Request.QueryString("EmployeeID")
            Me.Name = Request.QueryString("Name")
            Me.AssetStatus = Request.QueryString("Asset")
            Me.MonthName = Request.QueryString("MonthName")
            Me.Month = CInt(Request.QueryString("Month"))
            Me.Year = CInt(Request.QueryString("Year"))
        End If


        If CheckForm(Me.Loginid, Me.FormID, "Eloan") Then

            lnkAOID.Text = Me.EmployeeID.Trim
            lnkAOID.NavigateUrl = "javascript:OpenAO('Marketing', '" & Me.BranchID.Trim & "','" & Me.EmployeeID.Trim & "')"
            lblAOName.Text = Me.Name.Trim
            lblAsset.Text = Me.AssetStatus
            lblYear.Text = CStr(Me.Year)
            lblMonth.Text = Me.MonthName
        Else
            Dim strHTTPServer As String
            Dim strHTTPApp As String
            Dim strNameServer As String
            strHTTPServer = Request.ServerVariables("PATH_INFO")
            strNameServer = Request.ServerVariables("SERVER_NAME")
            strHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
            Response.Redirect("http://" & strNameServer & "/" & strHTTPApp & "/NotAuthorized.html")
        End If
    End Sub
#Region " Event Handlers "
    Private Sub imbSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbSave.Click
        Dim status As String

        status = Me.AssetStatus.Trim
        If status = "New" Then
            status = "N"
        Else
            status = "U"
        End If
        With oAOForecast
            .strConnection = GetConnectionString()
            .BranchId = Me.BranchID
            .EmployeeID = Me.EmployeeID
            .Year = Me.Year
            .Month = Me.Month
            .AssetStatus = status
            .Unit = CInt(txtUnit.Text)
            .Amount = CDec(txtAmount.Text)
        End With

        Try
            m_AOForecast.AOForecastEdit(oAOForecast)
            Response.Redirect("AODirectSalesForecast.aspx?BranchID=" & Me.BranchID.Trim & "&EmployeeID=" & Me.EmployeeID.Trim & "&Name=" & Me.Name.Trim)
        Catch ex As Exception
            lblMessage.Text = ""
            lblMessage.Text = ex.Message
        End Try

    End Sub
    Private Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbCancel.Click
        Response.Redirect("AODirectSalesForecast.aspx?BranchID=" & Me.BranchID.Trim & "&EmployeeID=" & Me.EmployeeID.Trim & "&Name=" & Me.Name.Trim)
    End Sub
#End Region




End Class
