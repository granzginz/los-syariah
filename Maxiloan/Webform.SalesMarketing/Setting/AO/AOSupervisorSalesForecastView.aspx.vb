#Region "Revision History"

'$Header: 
'-----------------------------------------------------------
'$Log:
'
'
'-----------------------------------------------------------

#End Region
#Region "Imports"
Imports Maxiloan
Imports Maxiloan.Exceptions
Imports Maxiloan.General
Imports Maxiloan.webform.UserController
Imports Maxiloan.Controller
#End Region
Public Class AOSupervisorSalesForecastView
    Inherits Maxiloan.Webform.WebBased
#Region "Constanta"
    Private m_AOForecast As New AOSupervisorSalesController
    Protected WithEvents DtgBudgetAdd As System.Web.UI.WebControls.DataGrid
    Private oAOForecast As New Parameter.AOSupervisorSales
#End Region
#Region "properties"
    Public Property Name() As String
        Get
            Return CType(viewstate("Name"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Name") = Value
        End Set
    End Property
    Public Property EmployeeID() As String
        Get
            Return CType(viewstate("EmployeeID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("EmployeeID") = Value
        End Set
    End Property
    Public Property AssetStatus() As String
        Get
            Return CType(viewstate("AssetStatus"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AssetStatus") = Value
        End Set
    End Property
    Public Property Year() As Integer
        Get
            Return CType(viewstate("Year"), Integer)
        End Get
        Set(ByVal Value As Integer)
            viewstate("Year") = Value
        End Set
    End Property

    Public Property Month() As Integer
        Get
            Return CType(viewstate("Month"), Integer)
        End Get
        Set(ByVal Value As Integer)
            viewstate("Month") = Value
        End Set
    End Property
    Public Property MonthName() As String
        Get
            Return CType(viewstate("MonthName"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("MonthName") = Value
        End Set
    End Property


#End Region
#Region " Navigation "



    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        viewdata()
    End Sub
#End Region

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    Protected WithEvents lnkAOID As System.Web.UI.WebControls.HyperLink
    Protected WithEvents lblAOName As System.Web.UI.WebControls.Label
    Protected WithEvents lblYear As System.Web.UI.WebControls.Label
    Protected WithEvents DtgForecastAdd As System.Web.UI.WebControls.DataGrid
    Protected WithEvents imbOK As System.Web.UI.WebControls.ImageButton

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Me.FormID = "MktAOSpvSales"
        If Not Page.IsPostBack Then
            Me.BranchID = Request.QueryString("BranchID")
            Me.EmployeeID = Request.QueryString("EmployeeID")
            Me.Name = Request.QueryString("Name")
            Me.Year = CInt(Request.QueryString("Year"))
        End If


        If CheckForm(Me.Loginid, Me.FormID, "Eloan") Then

            lnkAOID.Text = Me.EmployeeID.Trim
            lnkAOID.NavigateUrl = "javascript:OpenAO('Marketing', '" & Me.BranchID.Trim & "','" & Me.EmployeeID.Trim & "')"
            lblAOName.Text = Me.Name.Trim
            lblYear.Text = CStr(Me.Year)
            viewdata()
        Else
            Dim strHTTPServer As String
            Dim strHTTPApp As String
            Dim strNameServer As String
            strHTTPServer = Request.ServerVariables("PATH_INFO")
            strNameServer = Request.ServerVariables("SERVER_NAME")
            strHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
            Response.Redirect("http://" & strNameServer & "/" & strHTTPApp & "/NotAuthorized.html")
        End If
    End Sub
#Region " Sub and Function "
    Private Sub viewdata()
        With oAOForecast
            .strConnection = GetConnectionString()
            .BranchId = Me.BranchID
            .EmployeeID = Me.EmployeeID
            .Year = Me.Year
        End With
        Try
            oAOForecast = m_AOForecast.AOSupervisorForecastView(oAOForecast)
            DtgForecastAdd.DataSource = oAOForecast.ListData.DefaultView
            DtgForecastAdd.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try



    End Sub

#End Region

    Private Sub imbOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOK.Click
        Response.Redirect("AOSupervisorSalesForecast.aspx?BranchID=" & Me.BranchID.Trim & "&EmployeeID=" & Me.EmployeeID.Trim & "&Name=" & Me.Name.Trim)
    End Sub
End Class
