<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AODirectSales.aspx.vb"
    Inherits="Maxiloan.Webform.SalesMarketing.AODirectSales" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>AODirectSales</title>
    <link rel="Stylesheet" href="../../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../../Include/Buttons.css" type="text/css" />
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
   
</head>
<body>
    <form id="Form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
            <asp:Label ID="LblErrMessages" runat="server" ForeColor="Red"></asp:Label><br />
            <asp:Panel ID="PnlGrid" runat="server" Width="100%">
                <div class="form_title">
                    <div class="title_strip">
                    </div>
                    <div class="form_single">
                        <h3>
                            DAFTAR CMO
                        </h3>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtgAO" runat="server" Width="100%" CssClass="grid_general" BorderStyle="None"
                                BorderWidth="0" OnSortCommand="Sorting" AutoGenerateColumns="False" AllowSorting="True">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="BRANCH ID" ItemStyle-Width="20%" Visible="False">
                                        <HeaderStyle HorizontalAlign="Center" Width="18%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="left" Width="18%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblBranchID" runat="server" Text='<%#Container.DataItem("BranchID")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="AOID" HeaderText="CMO ID" ItemStyle-Width="20%">
                                        <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="left" Width="12%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="lnkAOID" runat="server" Text='<%#Container.DataItem("AOID")%>'>
                                            </asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="AOName" HeaderText="CMO NAME" ItemStyle-Width="20%">
                                        <HeaderStyle HorizontalAlign="Center" Width="18%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="18%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblAOName" runat="server" Text='<%#Container.DataItem("AOName")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="AOLevel" HeaderText="CMO LEVEL" ItemStyle-Width="20%">
                                        <HeaderStyle HorizontalAlign="Center" Width="18%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="18%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblAOLevel" runat="server" Text='<%#Container.DataItem("AOLevel")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="BUDGET">
                                        <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="lnkBudget" runat="server" Text="Budget"></asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="FORECAST" Visible="false">
                                        <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="lnkForecast" runat="server" Text="Forecast"></asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                            <div class="button_gridnavigation">
                                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../../Images/grid_navbutton01.png"
                                    CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../../Images/grid_navbutton02.png"
                                    CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../../Images/grid_navbutton03.png"
                                    CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../../Images/grid_navbutton04.png"
                                    CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                                    EnableViewState="False"></asp:Button>
                                <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" MinimumValue="1"
                                    ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"></asp:RangeValidator>
                                <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                                    ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                            </div>
                            <div class="label_gridnavigation">
                                <asp:Label ID="lblPage" runat="server"></asp:Label>of
                                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <div class="form_title">
            
                <div class="form_single">
                    <h3>
                        CARI CMO
                    </h3>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single" runat="server" id="divBranch">
                    <label>
                        Branch</label>
                    <asp:DropDownList ID="cboBranch" runat="server">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="rfvBranch" runat="server" ErrorMessage="Harap pilih Branch"
                        ControlToValidate="cboBranch" InitialValue="Select One" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        CMO Level</label>
                    <asp:DropDownList ID="cboAOLevel" runat="server">
                        <asp:ListItem Value="All">All</asp:ListItem>
                        <asp:ListItem Value="T">Trainee</asp:ListItem>
                        <asp:ListItem Value="J">Junior</asp:ListItem>
                        <asp:ListItem Value="S">Senior</asp:ListItem>
                        <asp:ListItem Value="E">Executive</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Cari Berdasarkan</label>
                    <asp:DropDownList ID="cboSearchBy" runat="server">
                        <asp:ListItem Value="AOID">CMO ID</asp:ListItem>
                        <asp:ListItem Value="AOName">CMO Name</asp:ListItem>
                    </asp:DropDownList>
                    <asp:TextBox ID="txtSearchBy" runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="imbButtonSearch" runat="server" CssClass="small button blue" Text="Search">
                </asp:Button>
                <asp:Button ID="imbButtonReset" runat="server" CausesValidation="False" Text="Reset"
                    CssClass="small button gray"></asp:Button>
            </div>
            <%-- <table id="Table1" cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
        <tr>
            <td width="50%" height="30">
                <asp:ImageButton ID="imbSearch" CausesValidation="True" ImageUrl="../../../Images/ButtonSearch.gif"
                    runat="server"></asp:ImageButton>&nbsp;
                <asp:ImageButton ID="imbReset" CausesValidation="False" ImageUrl="../../../Images/ButtonReset.gif"
                    runat="server"></asp:ImageButton>
            </td>
            <td align="right" width="50%">
            </td>
        </tr>
    </table>--%>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
