﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
#End Region

Public Class CreditScoring_EditContent
    Inherits Maxiloan.Webform.WebBased

    Private m_Grade As New CreditScoreGradeMasterController
    Private m_controller As New CreditScoringMainController
    Private recordCount As Int64 = 1
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1

    Dim Temp_TotalWeight_CompanyCustomer As Double = 0
    Dim Temp_TotalWeight_CompanyAsset As Double = 0
    Dim Temp_TotalWeight_CompanyFinancial As Double = 0
    Dim Temp_TotalWeight_PersonalCustomer As Double = 0
    Dim Temp_TotalWeight_PersonalAsset As Double = 0
    Dim Temp_TotalWeight_PersonalFinancial As Double = 0
#Region "Property"
    Private Property AddEdit() As String
        Get
            Return CType(viewstate("AddEdit"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AddEdit") = Value
        End Set
    End Property

    Private Property ContentSeqNo() As Integer
        Get
            Return CType(viewstate("ContentSeqNo"), Integer)
        End Get
        Set(ByVal Value As Integer)
            viewstate("ContentSeqNo") = Value
        End Set
    End Property

    Private Property CreditScoreSchemeID() As String
        Get
            Return CType(viewstate("CreditScoreSchemeID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CreditScoreSchemeID") = Value
        End Set
    End Property

    Private Property Total_PersonalCustomer() As String
        Get
            Return CType(viewstate("Total_PersonalCustomer"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Total_PersonalCustomer") = Value
        End Set
    End Property

    Private Property Total_PersonalAsset() As String
        Get
            Return CType(viewstate("Total_PersonalAsset"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Total_PersonalAsset") = Value
        End Set
    End Property

    Private Property Total_PersonalFinancial() As String
        Get
            Return CType(viewstate("Total_PersonalFinancial"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Total_PersonalFinancial") = Value
        End Set
    End Property

    Private Property Total_CompanyCustomer() As String
        Get
            Return CType(viewstate("Total_CompanyCustomer"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Total_CompanyCustomer") = Value
        End Set
    End Property

    Private Property Total_CompanyAsset() As String
        Get
            Return CType(viewstate("Total_CompanyAsset"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Total_CompanyAsset") = Value
        End Set
    End Property

    Private Property Total_CompanyFinancial() As String
        Get
            Return CType(viewstate("Total_CompanyFinancial"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Total_CompanyFinancial") = Value
        End Set
    End Property

    Private Property CreditScoreComponentID() As String
        Get
            Return CType(viewstate("CreditScoreComponentID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CreditScoreComponentID") = Value
        End Set
    End Property
#End Region

#Region "InitialDefaultPanel"
    Private Sub InitialDefaultPanel()        
        'pnlCompanyAsset.Visible = True
        'pnlCompanyCustomer.Visible = True
        'pnlCompanyFinancial.Visible = True
        pnlPersonalAsset.Visible = True
        pnlPersonalCustomer.Visible = True
        pnlPersonalFinancial.Visible = True
        pnlEditContent_Range.Visible = False
        pnlEditContent_Table.Visible = False
    End Sub
#End Region

#Region "Page_Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        InitialDefaultPanel()
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.FormID = "CreditScoringCard"

            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If SessionInvalid() Then
                    Exit Sub
                End If
                Me.SearchBy = ""
                Me.SortBy = ""
            End If

            'txtPage.Text = "1"
            txtPage1.Text = "1"
            Me.CreditScoreSchemeID = Request.QueryString("CreditScoreSchemeID")

            Me.SortBy = "c.ContentSeqNo ASC"

            'BindGridEntity_CompanyAsset()
            'BindGridEntity_CompanyCustomer()
            'BindGridEntity_CompanyFinancial()
            BindGridEntity_PersonalAsset()
            BindGridEntity_PersonalCustomer()
            BindGridEntity_PersonalFinancial()
            BindGrade()
            GrandTotal()
            PanelSkemaApproval.Visible = False
        End If
    End Sub
#End Region

#Region "GrandTotal"
    Private Sub GrandTotal()
        lblGrandTotal_Company.Text = CStr(CDbl(Me.Total_CompanyAsset) + CDbl(Me.Total_CompanyCustomer) + CDbl(Me.Total_CompanyFinancial)) + " %"
        lblGrandTotal_Personal.Text = CStr(CDbl(Me.Total_PersonalAsset) + CDbl(Me.Total_PersonalCustomer) + CDbl(Me.Total_PersonalFinancial)) + " %"
    End Sub
#End Region

#Region "BindGridEntity"
    Sub BindGridEntity_PersonalCustomer()
        Dim dtEntity As DataTable
        Dim oCredit As New Parameter.CreditScoringMain

        With oCredit
            .WhereCond = "c.ScoringType='P' and c.GroupComponent='C'"
            .CreditScoreShemeId = Me.CreditScoreSchemeID
            .strConnection = GetConnectionString
            .SPName = "spCreditScoringCardView"
        End With
        oCredit = m_controller.CreditScoringMainView(oCredit)

        'Nilai description cuman perlu ditaruh di salah satu bindgridentity.
        lblSchemeID.Text = Me.CreditScoreSchemeID
        lblDescription.Text = oCredit.Description
        lblApprovalScore_Company.Text = CStr(oCredit.CompanyApprovedScore).Trim + " %"
        lblRejectScore_Company.Text = CStr(oCredit.CompanyRejectScore).Trim + " %"
        lblApprovalScore_Personal.Text = CStr(oCredit.PersonalApprovedScore).Trim + " %"
        lblRejectScore_Personal.Text = CStr(oCredit.PersonalRejectScore).Trim + " %"

        If Not oCredit Is Nothing Then
            dtEntity = oCredit.ListData
            recordCount = oCredit.TotalRecords
        Else
            recordCount = 0
        End If

        dtgPersonalCustomer.DataSource = dtEntity.DefaultView
        dtgPersonalCustomer.CurrentPageIndex = 0
        dtgPersonalCustomer.DataBind()

    End Sub

    Sub BindGridEntity_PersonalFinancial()
        Dim dtEntity As DataTable
        Dim oCredit As New Parameter.CreditScoringMain

        With oCredit
            .WhereCond = "c.ScoringType='P' and c.GroupComponent='F'"
            .CreditScoreShemeId = Me.CreditScoreSchemeID
            .strConnection = GetConnectionString
            .SPName = "spCreditScoringCardView"
        End With
        oCredit = m_controller.CreditScoringMainView(oCredit)

        If Not oCredit Is Nothing Then
            dtEntity = oCredit.ListData
            recordCount = oCredit.TotalRecords
        Else
            recordCount = 0
        End If

        dtgPersonalFinancial.DataSource = dtEntity.DefaultView
        dtgPersonalFinancial.CurrentPageIndex = 0
        dtgPersonalFinancial.DataBind()

    End Sub

    Sub BindGridEntity_PersonalAsset()
        Dim dtEntity As DataTable
        Dim oCredit As New Parameter.CreditScoringMain

        With oCredit
            .WhereCond = "c.ScoringType='P' and c.GroupComponent='A'"
            .CreditScoreShemeId = Me.CreditScoreSchemeID
            .strConnection = GetConnectionString
            .SPName = "spCreditScoringCardView"
        End With
        oCredit = m_controller.CreditScoringMainView(oCredit)

        If Not oCredit Is Nothing Then
            dtEntity = oCredit.ListData
            recordCount = oCredit.TotalRecords
        Else
            recordCount = 0
        End If

        dtgPersonalAsset.DataSource = dtEntity.DefaultView
        dtgPersonalAsset.CurrentPageIndex = 0
        dtgPersonalAsset.DataBind()

    End Sub

    Sub BindGridEntity_CompanyCustomer()
        Dim dtEntity As DataTable
        Dim oCredit As New Parameter.CreditScoringMain

        With oCredit
            .WhereCond = "c.ScoringType='C' and c.GroupComponent='C'"
            .CreditScoreShemeId = Me.CreditScoreSchemeID
            .strConnection = GetConnectionString
            .SPName = "spCreditScoringCardView"
        End With
        oCredit = m_controller.CreditScoringMainView(oCredit)

        If Not oCredit Is Nothing Then
            dtEntity = oCredit.ListData
            recordCount = oCredit.TotalRecords
        Else
            recordCount = 0
        End If

        dtgCompanyCustomer.DataSource = dtEntity.DefaultView
        dtgCompanyCustomer.CurrentPageIndex = 0
        dtgCompanyCustomer.DataBind()

    End Sub

    Sub BindGridEntity_CompanyFinancial()
        Dim dtEntity As DataTable
        Dim oCredit As New Parameter.CreditScoringMain

        With oCredit
            .WhereCond = "c.ScoringType='C' and c.GroupComponent='F'"
            .CreditScoreShemeId = Me.CreditScoreSchemeID
            .strConnection = GetConnectionString
            .SPName = "spCreditScoringCardView"
        End With
        oCredit = m_controller.CreditScoringMainView(oCredit)

        If Not oCredit Is Nothing Then
            dtEntity = oCredit.ListData
            recordCount = oCredit.TotalRecords
        Else
            recordCount = 0
        End If

        dtgCompanyFinancial.DataSource = dtEntity.DefaultView
        dtgCompanyFinancial.CurrentPageIndex = 0
        dtgCompanyFinancial.DataBind()

    End Sub

    Sub BindGridEntity_CompanyAsset()
        Dim dtEntity As DataTable
        Dim oCredit As New Parameter.CreditScoringMain

        With oCredit
            .WhereCond = "c.ScoringType='C' and c.GroupComponent='A'"
            .CreditScoreShemeId = Me.CreditScoreSchemeID
            .strConnection = GetConnectionString
            .SPName = "spCreditScoringCardView"
        End With
        oCredit = m_controller.CreditScoringMainView(oCredit)

        If Not oCredit Is Nothing Then
            dtEntity = oCredit.ListData
            recordCount = oCredit.TotalRecords
        Else
            recordCount = 0
        End If

        dtgCompanyAsset.DataSource = dtEntity.DefaultView
        dtgCompanyAsset.CurrentPageIndex = 0
        dtgCompanyAsset.DataBind()
    End Sub
#End Region

#Region "ItemDataBound"
    Private Sub dtgCompanyAsset_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgCompanyAsset.ItemDataBound
        Dim WeightCompanyAsset As New Label
        Dim TotalWeightCompanyAsset As New Label

        If e.Item.ItemIndex >= 0 Then
            WeightCompanyAsset = CType(e.Item.FindControl("lblWeight_CompanyAsset"), Label)
            Temp_TotalWeight_CompanyAsset += CDbl(WeightCompanyAsset.Text.Trim)
        End If

        If e.Item.ItemType = ListItemType.Footer Then
            TotalWeightCompanyAsset = CType(e.Item.FindControl("lblTotal_CompanyAsset"), Label)
            TotalWeightCompanyAsset.Text = Temp_TotalWeight_CompanyAsset.ToString
        End If
        Me.Total_CompanyAsset = TotalWeightCompanyAsset.Text
        TotalWeightCompanyAsset.Text = Me.Total_CompanyAsset + " %"
    End Sub

    Private Sub dtgCompanyCustomer_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgCompanyCustomer.ItemDataBound
        Dim WeightCompanyCustomer As New Label
        Dim TotalWeightCompanyCustomer As New Label

        If e.Item.ItemIndex >= 0 Then
            WeightCompanyCustomer = CType(e.Item.FindControl("lblWeight_CompanyCustomer"), Label)
            Temp_TotalWeight_CompanyCustomer += CDbl(WeightCompanyCustomer.Text.Trim)
        End If

        If e.Item.ItemType = ListItemType.Footer Then
            TotalWeightCompanyCustomer = CType(e.Item.FindControl("lblTotal_CompanyCustomer"), Label)
            TotalWeightCompanyCustomer.Text = Temp_TotalWeight_CompanyCustomer.ToString
        End If

        Me.Total_CompanyCustomer = TotalWeightCompanyCustomer.Text
        TotalWeightCompanyCustomer.Text = Me.Total_CompanyCustomer + " %"
    End Sub

    Private Sub dtgCompanyFinancial_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgCompanyFinancial.ItemDataBound
        Dim WeightCompanyFinancial As New Label
        Dim TotalWeightCompanyFinancial As New Label

        If e.Item.ItemIndex >= 0 Then
            WeightCompanyFinancial = CType(e.Item.FindControl("lblWeight_CompanyFinancial"), Label)
            Temp_TotalWeight_CompanyFinancial += CDbl(WeightCompanyFinancial.Text.Trim)
        End If

        If e.Item.ItemType = ListItemType.Footer Then
            TotalWeightCompanyFinancial = CType(e.Item.FindControl("lblTotal_CompanyFinancial"), Label)
            TotalWeightCompanyFinancial.Text = Temp_TotalWeight_CompanyFinancial.ToString
        End If

        Me.Total_CompanyFinancial = TotalWeightCompanyFinancial.Text
        TotalWeightCompanyFinancial.Text = Me.Total_CompanyFinancial + " %"
    End Sub

    Private Sub dtgPersonalAsset_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPersonalAsset.ItemDataBound
        Dim WeightPersonalAsset As New Label
        Dim TotalWeightPersonalAsset As New Label

        If e.Item.ItemIndex >= 0 Then
            WeightPersonalAsset = CType(e.Item.FindControl("lblWeight_PersonalAsset"), Label)
            Temp_TotalWeight_PersonalAsset += CDbl(WeightPersonalAsset.Text.Trim)
        End If

        If e.Item.ItemType = ListItemType.Footer Then
            TotalWeightPersonalAsset = CType(e.Item.FindControl("lblTotal_PersonalAsset"), Label)
            TotalWeightPersonalAsset.Text = Temp_TotalWeight_PersonalAsset.ToString
        End If

        Me.Total_PersonalAsset = TotalWeightPersonalAsset.Text
        TotalWeightPersonalAsset.Text = Me.Total_PersonalAsset + " %"
    End Sub

    Private Sub dtgPersonalCustomer_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPersonalCustomer.ItemDataBound
        Dim WeightPersonalCustomer As New Label
        Dim TotalWeightPersonalCustomer As New Label

        If e.Item.ItemIndex >= 0 Then
            WeightPersonalCustomer = CType(e.Item.FindControl("lblWeight_PersonalCustomer"), Label)
            Temp_TotalWeight_PersonalCustomer += CDbl(WeightPersonalCustomer.Text.Trim)
        End If

        If e.Item.ItemType = ListItemType.Footer Then
            TotalWeightPersonalCustomer = CType(e.Item.FindControl("lblTotal_PersonalCustomer"), Label)
            TotalWeightPersonalCustomer.Text = Temp_TotalWeight_PersonalCustomer.ToString
        End If

        Me.Total_PersonalCustomer = TotalWeightPersonalCustomer.Text
        TotalWeightPersonalCustomer.Text = Me.Total_PersonalCustomer + " %"
    End Sub

    Private Sub dtgPersonalFinancial_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPersonalFinancial.ItemDataBound
        Dim WeightPersonalFinancial As New Label
        Dim TotalWeightPersonalFinancial As New Label


        If e.Item.ItemIndex >= 0 Then
            WeightPersonalFinancial = CType(e.Item.FindControl("lblWeight_PersonalFinancial"), Label)
            Temp_TotalWeight_PersonalFinancial += CDbl(WeightPersonalFinancial.Text.Trim)
        End If

        If e.Item.ItemType = ListItemType.Footer Then
            TotalWeightPersonalFinancial = CType(e.Item.FindControl("lblTotal_PersonalFinancial"), Label)
            TotalWeightPersonalFinancial.Text = Temp_TotalWeight_PersonalFinancial.ToString
        End If

        Me.Total_PersonalFinancial = TotalWeightPersonalFinancial.Text
        TotalWeightPersonalFinancial.Text = Me.Total_PersonalFinancial + " %"
    End Sub
#End Region

#Region "dtgCompanyAsset_ItemCommand"
    Private Sub dtgCompanyAsset_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgCompanyAsset.ItemCommand
        Dim oCredit As New Parameter.CreditScoringMain
        If e.CommandName = "EditContent" Then
            If CheckFeature(Me.Loginid, Me.FormID, "EDIT", Me.AppId) Then
                Dim lblCalculationType As New Label
                lblCalculationType = CType(e.Item.FindControl("lblCalculationType_CompanyAsset"), Label)
                Dim strCalculationType As String = lblCalculationType.Text

                If strCalculationType = "R" Then
                    pnlEditContent_Range.Visible = True
                    pnlEditContent_Range_AddEdit.Visible = True
                    pnlEditContent_Range_Datagrid.Visible = True
                    pnlEditContent_Range_List.Visible = True

                    pnlEditContent_Table.Visible = False
                    pnlEditContent_Table_AddEdit.Visible = False
                    pnlEditContent_Tabel_List.Visible = False
                    pnlEditContent_Table_Datagrid.Visible = False

                    Dim lblCreditScoreComponentID As New Label
                    lblCreditScoreComponentID = CType(e.Item.FindControl("lblCreditScoreComponentID_CompanyAsset"), Label)
                    Dim strCreditScoreComponentID As String = lblCreditScoreComponentID.Text
                    Me.CreditScoreComponentID = strCreditScoreComponentID

                    Dim lblCreditScoreComponentID_CompanyAsset As New Label
                    lblCreditScoreComponentID_CompanyAsset = CType(e.Item.FindControl("lblCreditScoreComponentID_CompanyAsset"), Label)
                    Dim strCreditScoreComponentID_CompanyAsset As String = lblCreditScoreComponentID_CompanyAsset.Text

                    hplSchemeID.Text = lblSchemeID.Text
                    Me.CreditScoreSchemeID = lblSchemeID.Text
                    hplSchemeID.NavigateUrl = LinkTo(Me.CreditScoreSchemeID)
                    lblDescription_Range.Text = lblDescription.Text
                    lblComponent_Range.Text = strCreditScoreComponentID_CompanyAsset

                    BindGridEntity_EditContent_Range()
                ElseIf strCalculationType = "T" Then
                    pnlEditContent_Range.Visible = False
                    pnlEditContent_Range_AddEdit.Visible = False
                    pnlEditContent_Range_Datagrid.Visible = False
                    pnlEditContent_Range_List.Visible = False

                    pnlEditContent_Table.Visible = True
                    pnlEditContent_Table_AddEdit.Visible = False
                    pnlEditContent_Tabel_List.Visible = True
                    pnlEditContent_Table_Datagrid.Visible = True

                    Dim lblCreditScoreComponentID As New Label
                    lblCreditScoreComponentID = CType(e.Item.FindControl("lblCreditScoreComponentID_CompanyAsset"), Label)
                    Dim strCreditScoreComponentID As String = lblCreditScoreComponentID.Text
                    Me.CreditScoreComponentID = strCreditScoreComponentID

                    Dim lblCreditScoreComponentID_CompanyAsset As New Label
                    lblCreditScoreComponentID_CompanyAsset = CType(e.Item.FindControl("lblCreditScoreComponentID_CompanyAsset"), Label)
                    Dim strCreditScoreComponentID_CompanyAsset As String = lblCreditScoreComponentID_CompanyAsset.Text

                    hplSchemeID_Table.Text = lblSchemeID.Text
                    Me.CreditScoreSchemeID = lblSchemeID.Text
                    hplSchemeID_Table.NavigateUrl = LinkTo_Table(Me.CreditScoreSchemeID)
                    lblDescription_Table.Text = lblDescription.Text
                    lblComponent_Table.Text = strCreditScoreComponentID_CompanyAsset

                    BindGridEntity_EditContent_Table()
                End If

                pnlView.Visible = False
                pnlCompanyAsset.Visible = False
                pnlCompanyCustomer.Visible = False
                pnlCompanyFinancial.Visible = False
                pnlPersonalAsset.Visible = False
                pnlPersonalCustomer.Visible = False
                pnlPersonalFinancial.Visible = False

            End If
        End If
    End Sub
#End Region

#Region "dtgCompanyCustomer_ItemCommand"
    Private Sub dtgCompanyCustomer_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgCompanyCustomer.ItemCommand
        Dim oCredit As New Parameter.CreditScoringMain
        If e.CommandName = "EditContent" Then
            If CheckFeature(Me.Loginid, Me.FormID, "EDIT", Me.AppId) Then
                Dim lblCalculationType As New Label
                lblCalculationType = CType(e.Item.FindControl("lblCalculationType_CompanyCustomer"), Label)
                Dim strCalculationType As String = lblCalculationType.Text

                If strCalculationType = "R" Then
                    pnlEditContent_Range.Visible = True
                    pnlEditContent_Range_AddEdit.Visible = False
                    pnlEditContent_Range_Datagrid.Visible = True
                    pnlEditContent_Range_List.Visible = True

                    pnlEditContent_Table.Visible = False
                    pnlEditContent_Table_AddEdit.Visible = False
                    pnlEditContent_Tabel_List.Visible = False
                    pnlEditContent_Table_Datagrid.Visible = False

                    Dim lblCreditScoreComponentID As New Label
                    lblCreditScoreComponentID = CType(e.Item.FindControl("lblCreditScoreComponentID_CompanyCustomer"), Label)
                    Dim strCreditScoreComponentID As String = lblCreditScoreComponentID.Text
                    Me.CreditScoreComponentID = strCreditScoreComponentID

                    Dim lblCreditScoreComponentID_CompanyCustomer As New Label
                    lblCreditScoreComponentID_CompanyCustomer = CType(e.Item.FindControl("lblCreditScoreComponentID_CompanyCustomer"), Label)
                    Dim strCreditScoreComponentID_CompanyCustomer As String = lblCreditScoreComponentID_CompanyCustomer.Text

                    hplSchemeID.Text = lblSchemeID.Text
                    Me.CreditScoreSchemeID = lblSchemeID.Text
                    hplSchemeID.NavigateUrl = LinkTo(Me.CreditScoreSchemeID)
                    lblDescription_Range.Text = lblDescription.Text
                    lblComponent_Range.Text = strCreditScoreComponentID_CompanyCustomer

                    BindGridEntity_EditContent_Range()
                ElseIf strCalculationType = "T" Then
                    pnlEditContent_Range.Visible = False
                    pnlEditContent_Range_AddEdit.Visible = False
                    pnlEditContent_Range_Datagrid.Visible = False
                    pnlEditContent_Range_List.Visible = False

                    pnlEditContent_Table.Visible = True
                    pnlEditContent_Table_AddEdit.Visible = False
                    pnlEditContent_Tabel_List.Visible = True
                    pnlEditContent_Table_Datagrid.Visible = True

                    Dim lblCreditScoreComponentID As New Label
                    lblCreditScoreComponentID = CType(e.Item.FindControl("lblCreditScoreComponentID_CompanyCustomer"), Label)
                    Dim strCreditScoreComponentID As String = lblCreditScoreComponentID.Text
                    Me.CreditScoreComponentID = strCreditScoreComponentID

                    Dim lblCreditScoreComponentID_CompanyCustomer As New Label
                    lblCreditScoreComponentID_CompanyCustomer = CType(e.Item.FindControl("lblCreditScoreComponentID_CompanyCustomer"), Label)
                    Dim strCreditScoreComponentID_CompanyCustomer As String = lblCreditScoreComponentID_CompanyCustomer.Text

                    hplSchemeID_Table.Text = lblSchemeID.Text
                    Me.CreditScoreSchemeID = lblSchemeID.Text
                    hplSchemeID_Table.NavigateUrl = LinkTo(Me.CreditScoreSchemeID)
                    lblDescription_Table.Text = lblDescription.Text
                    lblComponent_Table.Text = strCreditScoreComponentID_CompanyCustomer

                    BindGridEntity_EditContent_Table()
                End If

                pnlView.Visible = False
                pnlCompanyAsset.Visible = False
                pnlCompanyCustomer.Visible = False
                pnlCompanyFinancial.Visible = False
                pnlPersonalAsset.Visible = False
                pnlPersonalCustomer.Visible = False
                pnlPersonalFinancial.Visible = False

            End If
        End If
    End Sub
#End Region

#Region "dtgCompanyFinancial_ItemCommand"
    Private Sub dtgCompanyFinancial_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgCompanyFinancial.ItemCommand
        Dim oCredit As New Parameter.CreditScoringMain
        If e.CommandName = "EditContent" Then
            If CheckFeature(Me.Loginid, Me.FormID, "EDIT", Me.AppId) Then
                Dim lblCalculationType As New Label
                lblCalculationType = CType(e.Item.FindControl("lblCalculationType_CompanyFinancial"), Label)
                Dim strCalculationType As String = lblCalculationType.Text


                If strCalculationType = "R" Then
                    pnlEditContent_Range.Visible = True
                    pnlEditContent_Range_AddEdit.Visible = False
                    pnlEditContent_Range_Datagrid.Visible = True
                    pnlEditContent_Range_List.Visible = True

                    pnlEditContent_Table.Visible = False
                    pnlEditContent_Table_AddEdit.Visible = False
                    pnlEditContent_Tabel_List.Visible = False
                    pnlEditContent_Table_Datagrid.Visible = False

                    Dim lblCreditScoreComponentID As New Label
                    lblCreditScoreComponentID = CType(e.Item.FindControl("lblCreditScoreComponentID_CompanyFinancial"), Label)
                    Dim strCreditScoreComponentID As String = lblCreditScoreComponentID.Text
                    Me.CreditScoreComponentID = strCreditScoreComponentID

                    Dim lblCreditScoreComponentID_CompanyFinancial As New Label
                    lblCreditScoreComponentID_CompanyFinancial = CType(e.Item.FindControl("lblCreditScoreComponentID_CompanyFinancial"), Label)
                    Dim strCreditScoreComponentID_CompanyFinancial As String = lblCreditScoreComponentID_CompanyFinancial.Text

                    hplSchemeID.Text = lblSchemeID.Text
                    Me.CreditScoreSchemeID = lblSchemeID.Text
                    hplSchemeID.NavigateUrl = LinkTo(Me.CreditScoreSchemeID)
                    lblDescription_Range.Text = lblDescription.Text
                    lblComponent_Range.Text = strCreditScoreComponentID_CompanyFinancial

                    BindGridEntity_EditContent_Range()
                ElseIf strCalculationType = "T" Then
                    pnlEditContent_Range.Visible = False
                    pnlEditContent_Range_AddEdit.Visible = False
                    pnlEditContent_Range_Datagrid.Visible = False
                    pnlEditContent_Range_List.Visible = False

                    pnlEditContent_Table.Visible = True
                    pnlEditContent_Table_AddEdit.Visible = False
                    pnlEditContent_Tabel_List.Visible = True
                    pnlEditContent_Table_Datagrid.Visible = True

                    Dim lblCreditScoreComponentID As New Label
                    lblCreditScoreComponentID = CType(e.Item.FindControl("lblCreditScoreComponentID_CompanyFinancial"), Label)
                    Dim strCreditScoreComponentID As String = lblCreditScoreComponentID.Text
                    Me.CreditScoreComponentID = strCreditScoreComponentID

                    Dim lblCreditScoreComponentID_CompanyFinancial As New Label
                    lblCreditScoreComponentID_CompanyFinancial = CType(e.Item.FindControl("lblCreditScoreComponentID_CompanyFinancial"), Label)
                    Dim strCreditScoreComponentID_CompanyFinancial As String = lblCreditScoreComponentID_CompanyFinancial.Text

                    hplSchemeID_Table.Text = lblSchemeID.Text
                    Me.CreditScoreSchemeID = lblSchemeID.Text
                    hplSchemeID_Table.NavigateUrl = LinkTo(Me.CreditScoreSchemeID)
                    lblDescription_Table.Text = lblDescription.Text
                    lblComponent_Table.Text = strCreditScoreComponentID_CompanyFinancial

                    BindGridEntity_EditContent_Table()
                End If

                pnlView.Visible = False
                pnlCompanyAsset.Visible = False
                pnlCompanyCustomer.Visible = False
                pnlCompanyFinancial.Visible = False
                pnlPersonalAsset.Visible = False
                pnlPersonalCustomer.Visible = False
                pnlPersonalFinancial.Visible = False


            End If
        End If
    End Sub
#End Region

#Region "dtgPersonalAsset_ItemCommand"
    Private Sub dtgPersonalAsset_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPersonalAsset.ItemCommand
        Dim oCredit As New Parameter.CreditScoringMain
        If e.CommandName = "EditContent" Then
            If CheckFeature(Me.Loginid, Me.FormID, "EDIT", Me.AppId) Then
                Dim lblCalculationType As New Label
                lblCalculationType = CType(e.Item.FindControl("lblCalculationType_PersonalAsset"), Label)
                Dim strCalculationType As String = lblCalculationType.Text

                If strCalculationType = "R" Then
                    pnlEditContent_Range.Visible = True
                    pnlEditContent_Range_AddEdit.Visible = False
                    pnlEditContent_Range_Datagrid.Visible = True
                    pnlEditContent_Range_List.Visible = True

                    pnlEditContent_Table.Visible = False
                    pnlEditContent_Table_AddEdit.Visible = False
                    pnlEditContent_Tabel_List.Visible = False
                    pnlEditContent_Table_Datagrid.Visible = False

                    Dim lblCreditScoreComponentID As New Label
                    lblCreditScoreComponentID = CType(e.Item.FindControl("lblCreditScoreComponentID_PersonalAsset"), Label)
                    Dim strCreditScoreComponentID As String = lblCreditScoreComponentID.Text
                    Me.CreditScoreComponentID = strCreditScoreComponentID

                    Dim lblCreditScoreComponentID_PersonalAsset As New Label
                    lblCreditScoreComponentID_PersonalAsset = CType(e.Item.FindControl("lblCreditScoreComponentID_PersonalAsset"), Label)
                    Dim strCreditScoreComponentID_PersonalAsset As String = lblCreditScoreComponentID_PersonalAsset.Text

                    hplSchemeID.Text = lblSchemeID.Text
                    Me.CreditScoreSchemeID = lblSchemeID.Text
                    hplSchemeID.NavigateUrl = LinkTo(Me.CreditScoreSchemeID)
                    lblDescription_Range.Text = lblDescription.Text
                    lblComponent_Range.Text = strCreditScoreComponentID_PersonalAsset

                    BindGridEntity_EditContent_Range()
                ElseIf strCalculationType = "T" Then
                    pnlEditContent_Range.Visible = False
                    pnlEditContent_Range_AddEdit.Visible = False
                    pnlEditContent_Range_Datagrid.Visible = False
                    pnlEditContent_Range_List.Visible = False

                    pnlEditContent_Table.Visible = True
                    pnlEditContent_Table_AddEdit.Visible = False
                    pnlEditContent_Tabel_List.Visible = True
                    pnlEditContent_Table_Datagrid.Visible = True

                    Dim lblCreditScoreComponentID As New Label
                    lblCreditScoreComponentID = CType(e.Item.FindControl("lblCreditScoreComponentID_PersonalAsset"), Label)
                    Dim strCreditScoreComponentID As String = lblCreditScoreComponentID.Text
                    Me.CreditScoreComponentID = strCreditScoreComponentID

                    Dim lblCreditScoreComponentID_PersonalAsset As New Label
                    lblCreditScoreComponentID_PersonalAsset = CType(e.Item.FindControl("lblCreditScoreComponentID_PersonalAsset"), Label)
                    Dim strCreditScoreComponentID_PersonalAsset As String = lblCreditScoreComponentID_PersonalAsset.Text

                    hplSchemeID_Table.Text = lblSchemeID.Text
                    Me.CreditScoreSchemeID = lblSchemeID.Text
                    hplSchemeID_Table.NavigateUrl = LinkTo(Me.CreditScoreSchemeID)
                    lblDescription_Table.Text = lblDescription.Text
                    lblComponent_Table.Text = strCreditScoreComponentID_PersonalAsset

                    BindGridEntity_EditContent_Table()
                End If

                pnlView.Visible = False
                pnlCompanyAsset.Visible = False
                pnlCompanyCustomer.Visible = False
                pnlCompanyFinancial.Visible = False
                pnlPersonalAsset.Visible = False
                pnlPersonalCustomer.Visible = False
                pnlPersonalFinancial.Visible = False

            End If
        End If
    End Sub
#End Region

#Region "dtgPersonalCustomer_ItemCommand"
    Private Sub dtgPersonalCustomer_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPersonalCustomer.ItemCommand
        Dim oCredit As New Parameter.CreditScoringMain
        If e.CommandName = "EditContent" Then
            If CheckFeature(Me.Loginid, Me.FormID, "EDIT", Me.AppId) Then
                Dim lblCalculationType As New Label
                lblCalculationType = CType(e.Item.FindControl("lblCalculationType_PersonalCustomer"), Label)
                Dim strCalculationType As String = lblCalculationType.Text

                If strCalculationType = "R" Then
                    pnlEditContent_Range.Visible = True
                    pnlEditContent_Range_AddEdit.Visible = False
                    pnlEditContent_Range_Datagrid.Visible = True
                    pnlEditContent_Range_List.Visible = True

                    pnlEditContent_Table.Visible = False
                    pnlEditContent_Table_AddEdit.Visible = False
                    pnlEditContent_Tabel_List.Visible = False
                    pnlEditContent_Table_Datagrid.Visible = False

                    Dim lblCreditScoreComponentID As New Label
                    lblCreditScoreComponentID = CType(e.Item.FindControl("lblCreditScoreComponentID_PersonalCustomer"), Label)
                    Dim strCreditScoreComponentID As String = lblCreditScoreComponentID.Text
                    Me.CreditScoreComponentID = strCreditScoreComponentID

                    Dim lblCreditScoreComponentID_PersonalCustomer As New Label
                    lblCreditScoreComponentID_PersonalCustomer = CType(e.Item.FindControl("lblCreditScoreComponentID_PersonalCustomer"), Label)
                    Dim strCreditScoreComponentID_PersonalCustomer As String = lblCreditScoreComponentID_PersonalCustomer.Text

                    hplSchemeID.Text = lblSchemeID.Text
                    Me.CreditScoreSchemeID = lblSchemeID.Text
                    hplSchemeID.NavigateUrl = LinkTo(Me.CreditScoreSchemeID)
                    lblDescription_Range.Text = lblDescription.Text
                    lblComponent_Range.Text = strCreditScoreComponentID_PersonalCustomer

                    BindGridEntity_EditContent_Range()
                ElseIf strCalculationType = "T" Then
                    pnlEditContent_Range.Visible = False
                    pnlEditContent_Range_AddEdit.Visible = False
                    pnlEditContent_Range_Datagrid.Visible = False
                    pnlEditContent_Range_List.Visible = False

                    pnlEditContent_Table.Visible = True
                    pnlEditContent_Table_AddEdit.Visible = False
                    pnlEditContent_Tabel_List.Visible = True
                    pnlEditContent_Table_Datagrid.Visible = True

                    Dim lblCreditScoreComponentID As New Label
                    lblCreditScoreComponentID = CType(e.Item.FindControl("lblCreditScoreComponentID_PersonalCustomer"), Label)
                    Dim strCreditScoreComponentID As String = lblCreditScoreComponentID.Text
                    Me.CreditScoreComponentID = strCreditScoreComponentID

                    Dim lblCreditScoreComponentID_PersonalCustomer As New Label
                    lblCreditScoreComponentID_PersonalCustomer = CType(e.Item.FindControl("lblCreditScoreComponentID_PersonalCustomer"), Label)
                    Dim strCreditScoreComponentID_PersonalCustomer As String = lblCreditScoreComponentID_PersonalCustomer.Text

                    hplSchemeID_Table.Text = lblSchemeID.Text
                    Me.CreditScoreSchemeID = lblSchemeID.Text
                    hplSchemeID_Table.NavigateUrl = LinkTo(Me.CreditScoreSchemeID)
                    lblDescription_Table.Text = lblDescription.Text
                    lblComponent_Table.Text = strCreditScoreComponentID_PersonalCustomer
                    BindGridEntity_EditContent_Table()
                End If

                pnlView.Visible = False
                pnlCompanyAsset.Visible = False
                pnlCompanyCustomer.Visible = False
                pnlCompanyFinancial.Visible = False
                pnlPersonalAsset.Visible = False
                pnlPersonalCustomer.Visible = False
                pnlPersonalFinancial.Visible = False


            End If
        End If
    End Sub
#End Region

#Region "dtgPersonalFinancial_ItemCommand"
    Private Sub dtgPersonalFinancial_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPersonalFinancial.ItemCommand
        Dim oCredit As New Parameter.CreditScoringMain
        If e.CommandName = "EditContent" Then
            If CheckFeature(Me.Loginid, Me.FormID, "EDIT", Me.AppId) Then
                Dim lblCalculationType As New Label
                lblCalculationType = CType(e.Item.FindControl("lblCalculationType_PersonalFinancial"), Label)
                Dim strCalculationType As String = lblCalculationType.Text


                If strCalculationType = "R" Then
                    pnlEditContent_Range.Visible = True
                    pnlEditContent_Range_AddEdit.Visible = True
                    pnlEditContent_Range_Datagrid.Visible = True
                    pnlEditContent_Range_List.Visible = True

                    pnlEditContent_Table.Visible = False
                    pnlEditContent_Table_AddEdit.Visible = False
                    pnlEditContent_Tabel_List.Visible = False
                    pnlEditContent_Table_Datagrid.Visible = False

                    Dim lblCreditScoreComponentID As New Label
                    lblCreditScoreComponentID = CType(e.Item.FindControl("lblCreditScoreComponentID_PersonalFinancial"), Label)
                    Dim strCreditScoreComponentID As String = lblCreditScoreComponentID.Text
                    Me.CreditScoreComponentID = strCreditScoreComponentID

                    Dim lblCreditScoreComponentID_PersonalFinancial As New Label
                    lblCreditScoreComponentID_PersonalFinancial = CType(e.Item.FindControl("lblCreditScoreComponentID_PersonalFinancial"), Label)
                    Dim strCreditScoreComponentID_PersonalFinancial As String = lblCreditScoreComponentID_PersonalFinancial.Text

                    hplSchemeID.Text = lblSchemeID.Text
                    Me.CreditScoreSchemeID = lblSchemeID.Text
                    hplSchemeID.NavigateUrl = LinkTo(Me.CreditScoreSchemeID)
                    lblDescription_Range.Text = lblDescription.Text
                    lblComponent_Range.Text = strCreditScoreComponentID_PersonalFinancial


                    BindGridEntity_EditContent_Range()
                ElseIf strCalculationType = "T" Then
                    pnlEditContent_Range.Visible = False
                    pnlEditContent_Range_AddEdit.Visible = False
                    pnlEditContent_Range_Datagrid.Visible = False
                    pnlEditContent_Range_List.Visible = False

                    pnlEditContent_Table.Visible = True
                    pnlEditContent_Table_AddEdit.Visible = False
                    pnlEditContent_Tabel_List.Visible = True
                    pnlEditContent_Table_Datagrid.Visible = True

                    Dim lblCreditScoreComponentID As New Label
                    lblCreditScoreComponentID = CType(e.Item.FindControl("lblCreditScoreComponentID_PersonalFinancial"), Label)
                    Dim strCreditScoreComponentID As String = lblCreditScoreComponentID.Text
                    Me.CreditScoreComponentID = strCreditScoreComponentID

                    Dim lblCreditScoreComponentID_PersonalFinancial As New Label
                    lblCreditScoreComponentID_PersonalFinancial = CType(e.Item.FindControl("lblCreditScoreComponentID_PersonalFinancial"), Label)
                    Dim strCreditScoreComponentID_PersonalFinancial As String = lblCreditScoreComponentID_PersonalFinancial.Text

                    hplSchemeID_Table.Text = lblSchemeID.Text
                    Me.CreditScoreSchemeID = lblSchemeID.Text
                    hplSchemeID_Table.NavigateUrl = LinkTo(Me.CreditScoreSchemeID)
                    lblDescription_Table.Text = lblDescription.Text
                    lblComponent_Table.Text = strCreditScoreComponentID_PersonalFinancial

                    BindGridEntity_EditContent_Table()
                End If

                pnlView.Visible = False
                pnlCompanyAsset.Visible = False
                pnlCompanyCustomer.Visible = False
                pnlCompanyFinancial.Visible = False
                pnlPersonalAsset.Visible = False
                pnlPersonalCustomer.Visible = False
                pnlPersonalFinancial.Visible = False

            End If
        End If
    End Sub
#End Region

#Region "imbOK_Click"
    Private Sub imbOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonOK.Click
        Response.Redirect("CreditScoring_Main.aspx")
    End Sub
#End Region

#Region "LinkTo"
    Function LinkTo(ByVal strCreditScoreSchemeID As String) As String
        Return "javascript:OpenWinSchemeView('" & strCreditScoreSchemeID & "')"
    End Function
#End Region

#Region "LinkTo_Table"
    Function LinkTo_Table(ByVal strCreditScoreSchemeID As String) As String
        Return "javascript:OpenWinSchemeView('" & strCreditScoreSchemeID & "')"
    End Function
#End Region

#Region "dtgEditContent_Range_ItemDataBound"
    Private Sub dtgEditContent_Range_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgEditContent_Range.ItemDataBound
        Dim imbDelete As ImageButton
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imgDeleteRange"), ImageButton)
            imbDelete.Attributes.Add("Onclick", "return DeleteConfirm()")
        End If
    End Sub
#End Region

#Region "dtgEditContent_Range_ItemCommand"
    Private Sub dtgEditContent_Range_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgEditContent_Range.ItemCommand
        Dim oCredit As New Parameter.CreditScoringMain
        If e.CommandName = "EditRange" Then
            Dim labelContentSeqNo As New Label
            labelContentSeqNo = CType(e.Item.FindControl("lblContentSeqNo"), Label)
            Me.ContentSeqNo = CInt(labelContentSeqNo.Text)

            If CheckFeature(Me.Loginid, Me.FormID, "EDIT", Me.AppId) Then
                pnlView.Visible = False
                pnlCompanyAsset.Visible = False
                pnlCompanyCustomer.Visible = False
                pnlCompanyFinancial.Visible = False
                pnlPersonalAsset.Visible = False
                pnlPersonalCustomer.Visible = False
                pnlPersonalFinancial.Visible = False
                pnlEditContent_Range.Visible = True
                pnlEditContent_Range_List.Visible = True
                pnlEditContent_Range_Datagrid.Visible = False
                pnlEditContent_Range_AddEdit.Visible = True

                Me.AddEdit = "EDIT"
                lblAddEdit.Text = Me.AddEdit

                oCredit.CreditScoreShemeId = hplSchemeID.Text
                oCredit.CreditScoreComponentId = lblComponent_Range.Text
                oCredit.ContentSeqNo = Me.ContentSeqNo
                oCredit.strConnection = GetConnectionString
                oCredit.SPName = "spCreditScoringEditContent_RangeEdit"
                oCredit = m_controller.EditContentEdit(oCredit)

                txtValueFrom.Text = CStr(oCredit.ValueFrom)
                txtValueTo.Text = CStr(oCredit.ValueTo)
                txtScoreValue.Text = CStr(oCredit.ScoreValue)
                'cboScoreStatus.SelectedItem.Value = oCredit.ScoreStatus
                cboScoreStatus.SelectedIndex = cboScoreStatus.Items.IndexOf(cboScoreStatus.Items.FindByValue(oCredit.ScoreStatus))
            End If
        ElseIf e.CommandName = "DeleteRange" Then
            If CheckFeature(Me.Loginid, Me.FormID, "DEL", Me.AppId) Then
                Dim labelContentSeqNo As New Label
                labelContentSeqNo = CType(e.Item.FindControl("lblContentSeqNo"), Label)
                Me.ContentSeqNo = CInt(labelContentSeqNo.Text)

                oCredit.CreditScoreShemeId = hplSchemeID.Text
                oCredit.CreditScoreComponentId = lblComponent_Range.Text.Trim
                oCredit.ContentSeqNo = Me.ContentSeqNo
                oCredit.strConnection = GetConnectionString
                oCredit.SPName = "spCreditScoringEditContent_RangeDelete"
                Dim ResultOutput As String
                ResultOutput = m_controller.EditContentDelete(oCredit)
                If ResultOutput = "OK" Then
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, False)
                Else
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_FAILED, True)
                End If
                BindGridEntity_EditContent_Range()
            End If
        End If
    End Sub
#End Region

#Region "dtgEditContent_Table_ItemCommand"
    Private Sub dtgEditContent_Table_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgEditContent_Table.ItemCommand
        If e.CommandName = "EditTable" Then
            Dim oCredit As New Parameter.CreditScoringMain
            Dim labelContentSeqNo As New Label
            labelContentSeqNo = CType(e.Item.FindControl("lblContentSeqNo_Table"), Label)
            Me.ContentSeqNo = CInt(labelContentSeqNo.Text)

            If CheckFeature(Me.Loginid, Me.FormID, "EDIT", Me.AppId) Then
                pnlView.Visible = False
                pnlCompanyAsset.Visible = False
                pnlCompanyCustomer.Visible = False
                pnlCompanyFinancial.Visible = False
                pnlPersonalAsset.Visible = False
                pnlPersonalCustomer.Visible = False
                pnlPersonalFinancial.Visible = False

                pnlEditContent_Table.Visible = True
                pnlEditContent_Table_AddEdit.Visible = True
                pnlEditContent_Tabel_List.Visible = True
                pnlEditContent_Table_Datagrid.Visible = False

                Me.AddEdit = "EDIT"
                lblAddEdit_Table.Text = Me.AddEdit

                oCredit.CreditScoreShemeId = hplSchemeID_Table.Text
                oCredit.CreditScoreComponentId = lblComponent_Table.Text
                oCredit.ContentSeqNo = Me.ContentSeqNo
                oCredit.strConnection = GetConnectionString
                oCredit.SPName = "spCreditScoringEditContent_TableEdit"
                oCredit = m_controller.EditContentEdit_Table(oCredit)

                lblValueContent.Text = oCredit.ValueContent
                lblValueDescription.Text = oCredit.ValueDescription

                txtScoreValue_Table.Text = CStr(oCredit.ScoreValue)
                'cboScoreStatus.SelectedItem.Value = oCredit.ScoreStatus
                cboScoreStatus_Table.SelectedIndex = cboScoreStatus_Table.Items.IndexOf(cboScoreStatus_Table.Items.FindByValue(oCredit.ScoreStatus))
            End If
        End If
    End Sub
#End Region

#Region "BindGridEntity_EditContent_Range"
    Sub BindGridEntity_EditContent_Range()
        Dim dtEntity As DataTable
        Dim oCredit As New Parameter.CreditScoringMain

        oCredit.SortBy = Me.SortBy
        oCredit.PageSize = pageSize
        oCredit.CurrentPage = currentPage
        oCredit.WhereCond = "a.CreditScoreSchemeID='" + lblSchemeID.Text + "' and b.CreditScoreComponentID='" + Me.CreditScoreComponentID + "'"
        oCredit.strConnection = GetConnectionString
        oCredit.SPName = "spCreditScoringEditContent_RangePaging"
        oCredit = m_controller.GetEditContentPaging(oCredit)

        If Not oCredit Is Nothing Then
            dtEntity = oCredit.ListData
            recordCount = oCredit.TotalRecords
        Else
            recordCount = 0
        End If

        dtgEditContent_Range.DataSource = dtEntity.DefaultView
        dtgEditContent_Range.CurrentPageIndex = 0
        dtgEditContent_Range.DataBind()
        'PagingFooter()

        pnlView.Visible = False
        pnlCompanyAsset.Visible = False
        pnlCompanyCustomer.Visible = False
        pnlCompanyFinancial.Visible = False
        pnlPersonalAsset.Visible = False
        pnlPersonalCustomer.Visible = False
        pnlPersonalFinancial.Visible = False
        pnlEditContent_Range.Visible = True
        pnlEditContent_Range_List.Visible = True
        pnlEditContent_Range_Datagrid.Visible = True
        pnlEditContent_Range_AddEdit.Visible = False
    End Sub
#End Region

#Region "BindGridEntity_EditContent_Table"
    Sub BindGridEntity_EditContent_Table()
        Dim dtEntity As DataTable
        Dim oCredit As New Parameter.CreditScoringMain

        oCredit.SortBy = Me.SortBy
        oCredit.PageSize = pageSize
        oCredit.CurrentPage = currentPage
        oCredit.CreditScoreShemeId = hplSchemeID_Table.Text
        oCredit.CreditScoreComponentId = lblComponent_Table.Text
        oCredit.WhereCond = "a.CreditScoreSchemeID='" + lblSchemeID.Text + "' and b.CreditScoreComponentID='" + Me.CreditScoreComponentID + "'"

        oCredit.strConnection = GetConnectionString
        oCredit.SPName = "spCreditScoringEditContent_TablePaging"
        oCredit = m_controller.GetEditContentPaging_Table(oCredit)

        If Not oCredit Is Nothing Then
            dtEntity = oCredit.ListData
            recordCount = oCredit.TotalRecords
        Else
            recordCount = 0
        End If

        dtgEditContent_Table.DataSource = dtEntity.DefaultView
        dtgEditContent_Table.CurrentPageIndex = 0
        dtgEditContent_Table.DataBind()
        PagingFooter1()

        pnlView.Visible = False
        pnlCompanyAsset.Visible = False
        pnlCompanyCustomer.Visible = False
        pnlCompanyFinancial.Visible = False
        pnlPersonalAsset.Visible = False
        pnlPersonalCustomer.Visible = False
        pnlPersonalFinancial.Visible = False
        pnlEditContent_Table.Visible = True
        pnlEditContent_Tabel_List.Visible = True
        pnlEditContent_Table_Datagrid.Visible = True
        pnlEditContent_Table_AddEdit.Visible = False
    End Sub
#End Region

#Region " Sorting "
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgEditContent_Range.SortCommand
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        BindGridEntity_EditContent_Range()
    End Sub
#End Region

#Region " Sorting1 "
    Public Sub Sorting1(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgEditContent_Table.SortCommand
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        BindGridEntity_EditContent_Table()
    End Sub
#End Region

    '#Region " Navigation "
    '    Private Sub PagingFooter()
    '        lblPage.Text = currentPage.ToString()
    '        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
    '        If totalPages = 0 Then
    '            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
    '            lblTotPage.Text = "1"
    '            rgvGo.MaximumValue = "1"
    '        Else
    '            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
    '            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
    '        End If
    '        lblrecord.Text = recordCount.ToString

    '        If currentPage = 1 Then
    '            imbPrevPage.Enabled = False
    '            imbFirstPage.Enabled = False
    '            If totalPages > 1 Then
    '                imbNextPage.Enabled = True
    '                imbLastPage.Enabled = True
    '            Else
    '                imbPrevPage.Enabled = False
    '                imbNextPage.Enabled = False
    '                imbLastPage.Enabled = False
    '                imbFirstPage.Enabled = False
    '            End If
    '        Else
    '            imbPrevPage.Enabled = True
    '            imbFirstPage.Enabled = True
    '            If currentPage = totalPages Then
    '                imbNextPage.Enabled = False
    '                imbLastPage.Enabled = False
    '            Else
    '                imbLastPage.Enabled = True
    '                imbNextPage.Enabled = True
    '            End If
    '        End If
    '    End Sub

    '    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
    '        Select Case e.CommandName
    '            Case "First" : currentPage = 1
    '            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
    '            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
    '            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
    '        End Select
    '        BindGridEntity_EditContent_Range()
    '    End Sub

    '    Private Sub imgbtnPageNumb_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click
    '        If IsNumeric(txtPage.Text) Then
    '            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
    '                currentPage = CType(txtPage.Text, Int32)
    '                BindGridEntity_EditContent_Range()
    '            Else
    '                pnlEditContent_Range.Visible = True
    '                pnlEditContent_Range_AddEdit.Visible = False
    '                pnlEditContent_Range_Datagrid.Visible = True
    '                pnlEditContent_Range_List.Visible = True

    '                pnlEditContent_Table.Visible = False
    '                pnlEditContent_Table_AddEdit.Visible = False
    '                pnlEditContent_Tabel_List.Visible = False
    '                pnlEditContent_Table_Datagrid.Visible = False

    '                pnlView.Visible = False
    '                pnlCompanyAsset.Visible = False
    '                pnlCompanyCustomer.Visible = False
    '                pnlCompanyFinancial.Visible = False
    '                pnlPersonalAsset.Visible = False
    '                pnlPersonalCustomer.Visible = False
    '                pnlPersonalFinancial.Visible = False

    '            End If
    '        Else
    '            txtPage.Text = "1"
    '            pnlEditContent_Range.Visible = True
    '            pnlEditContent_Range_AddEdit.Visible = False
    '            pnlEditContent_Range_Datagrid.Visible = True
    '            pnlEditContent_Range_List.Visible = True

    '            pnlEditContent_Table.Visible = False
    '            pnlEditContent_Table_AddEdit.Visible = False
    '            pnlEditContent_Tabel_List.Visible = False
    '            pnlEditContent_Table_Datagrid.Visible = False

    '            pnlView.Visible = False
    '            pnlCompanyAsset.Visible = False
    '            pnlCompanyCustomer.Visible = False
    '            pnlCompanyFinancial.Visible = False
    '            pnlPersonalAsset.Visible = False
    '            pnlPersonalCustomer.Visible = False
    '            pnlPersonalFinancial.Visible = False

    '        End If
    '    End Sub

    '#End Region

#Region " Navigation1"
    Private Sub PagingFooter1()
        lblPage1.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage1.Text = "1"
            rgvGo1.MaximumValue = "1"
        Else
            lblTotPage1.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo1.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord1.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage1.Enabled = False
            imbFirstPage1.Enabled = False
            If totalPages > 1 Then
                imbNextPage1.Enabled = True
                imbLastPage1.Enabled = True
            Else
                imbPrevPage1.Enabled = False
                imbNextPage1.Enabled = False
                imbLastPage1.Enabled = False
                imbFirstPage1.Enabled = False
            End If
        Else
            imbPrevPage1.Enabled = True
            imbFirstPage1.Enabled = True
            If currentPage = totalPages Then
                imbNextPage1.Enabled = False
                imbLastPage1.Enabled = False
            Else
                imbLastPage1.Enabled = True
                imbNextPage1.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink1_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage1.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage1.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage1.Text) - 1
        End Select
        BindGridEntity_EditContent_Table()
    End Sub

    Private Sub btnPageNumb1_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPageNumb1.Click
        If IsNumeric(txtPage1.Text) Then
            If CType(lblTotPage1.Text, Integer) > 1 And CType(txtPage1.Text, Integer) <= CType(lblTotPage1.Text, Integer) Then
                currentPage = CType(txtPage1.Text, Int32)
                BindGridEntity_EditContent_Table()
            Else
                BindGridEntity_EditContent_Table()
                pnlEditContent_Range_AddEdit.Visible = False
                pnlEditContent_Range_Datagrid.Visible = False
                pnlEditContent_Range_List.Visible = False

                pnlEditContent_Table.Visible = True
                pnlEditContent_Table_AddEdit.Visible = False
                pnlEditContent_Tabel_List.Visible = True
                pnlEditContent_Table_Datagrid.Visible = True

                pnlView.Visible = False
                pnlCompanyAsset.Visible = False
                pnlCompanyCustomer.Visible = False
                pnlCompanyFinancial.Visible = False
                pnlPersonalAsset.Visible = False
                pnlPersonalCustomer.Visible = False
                pnlPersonalFinancial.Visible = False

            End If
        Else
            txtPage1.Text = "1"
            BindGridEntity_EditContent_Table()
            pnlEditContent_Range_AddEdit.Visible = False
            pnlEditContent_Range_Datagrid.Visible = False
            pnlEditContent_Range_List.Visible = False

            pnlEditContent_Table.Visible = True
            pnlEditContent_Table_AddEdit.Visible = False
            pnlEditContent_Tabel_List.Visible = True
            pnlEditContent_Table_Datagrid.Visible = True

            pnlView.Visible = False
            pnlCompanyAsset.Visible = False
            pnlCompanyCustomer.Visible = False
            pnlCompanyFinancial.Visible = False
            pnlPersonalAsset.Visible = False
            pnlPersonalCustomer.Visible = False
            pnlPersonalFinancial.Visible = False

        End If
    End Sub

#End Region

#Region "imgSave_Click"
    Private Sub imgSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSave.Click

        If Not IsNumeric(txtValueFrom.Text.Trim) Then            
            ShowMessage(lblMessage, "Nilai FROM Salah", True)
            pnlView.Visible = False
            pnlCompanyAsset.Visible = False
            pnlCompanyCustomer.Visible = False
            pnlCompanyFinancial.Visible = False
            pnlPersonalAsset.Visible = False
            pnlPersonalCustomer.Visible = False
            pnlPersonalFinancial.Visible = False
            pnlEditContent_Range.Visible = True
            pnlEditContent_Range_List.Visible = True
            pnlEditContent_Range_Datagrid.Visible = False
            pnlEditContent_Range_AddEdit.Visible = True
            Exit Sub
        End If

        If Not IsNumeric(txtValueTo.Text.Trim) Then            
            ShowMessage(lblMessage, "Nilai TO Salah", True)
            pnlView.Visible = False
            pnlCompanyAsset.Visible = False
            pnlCompanyCustomer.Visible = False
            pnlCompanyFinancial.Visible = False
            pnlPersonalAsset.Visible = False
            pnlPersonalCustomer.Visible = False
            pnlPersonalFinancial.Visible = False
            pnlEditContent_Range.Visible = True
            pnlEditContent_Range_List.Visible = True
            pnlEditContent_Range_Datagrid.Visible = False
            pnlEditContent_Range_AddEdit.Visible = True
            Exit Sub
        End If

        If Not IsNumeric(txtScoreValue.Text.Trim) Then            
            ShowMessage(lblMessage, "Score Salah", True)
            pnlView.Visible = False
            pnlCompanyAsset.Visible = False
            pnlCompanyCustomer.Visible = False
            pnlCompanyFinancial.Visible = False
            pnlPersonalAsset.Visible = False
            pnlPersonalCustomer.Visible = False
            pnlPersonalFinancial.Visible = False
            pnlEditContent_Range.Visible = True
            pnlEditContent_Range_List.Visible = True
            pnlEditContent_Range_Datagrid.Visible = False
            pnlEditContent_Range_AddEdit.Visible = True
            Exit Sub
        End If

        Dim oCredit As New Parameter.CreditScoringMain
        Dim ErrMessage As String = ""
        oCredit.CreditScoreShemeId = hplSchemeID.Text
        oCredit.CreditScoreComponentId = lblComponent_Range.Text
        oCredit.ValueFrom = CDbl(txtValueFrom.Text.Trim)
        oCredit.ValueTo = CDbl(txtValueTo.Text.Trim)
        oCredit.ScoreValue = CInt(txtScoreValue.Text.Trim)
        oCredit.ScoreStatus = cboScoreStatus.SelectedItem.Value
        oCredit.ContentSeqNo = Me.ContentSeqNo

        'cmbPurpose.SelectedIndex = cmbPurpose.Items.IndexOf(cmbPurpose.Items.FindByValue(Me.BankPurpose))
        oCredit.strConnection = GetConnectionString()

        If Me.AddEdit = "EDIT" Then
            oCredit.SPName = "spCreditScoringEditContent_RangeUpdate"
            m_controller.EditContentSaveEdit(oCredit)
            'txtPage.Text = ""
            BindGridEntity_EditContent_Range()
            pnlView.Visible = False
            pnlCompanyAsset.Visible = False
            pnlCompanyCustomer.Visible = False
            pnlCompanyFinancial.Visible = False
            pnlPersonalAsset.Visible = False
            pnlPersonalCustomer.Visible = False
            pnlPersonalFinancial.Visible = False
            pnlEditContent_Range.Visible = True
            pnlEditContent_Range_List.Visible = True
            pnlEditContent_Range_Datagrid.Visible = True
            pnlEditContent_Range_AddEdit.Visible = False            
            ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
        ElseIf Me.AddEdit = "ADD" Then
            oCredit.SPName = "spCreditScoringEditContent_RangeAdd"
            ErrMessage = m_controller.EditContentSaveAdd(oCredit)
            If ErrMessage <> "" Then                
                ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_FAILED, True)
                pnlEditContent_Range_AddEdit.Visible = True
                pnlView.Visible = False
                pnlCompanyAsset.Visible = False
                pnlCompanyCustomer.Visible = False
                pnlCompanyFinancial.Visible = False
                pnlPersonalAsset.Visible = False
                pnlPersonalCustomer.Visible = False
                pnlPersonalFinancial.Visible = False
                pnlEditContent_Range.Visible = False
                pnlEditContent_Range_List.Visible = True
                pnlEditContent_Range_Datagrid.Visible = False
                Exit Sub
            Else                
                ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_SUCCESS, False)
                BindGridEntity_EditContent_Range()
                pnlView.Visible = False
                pnlCompanyAsset.Visible = False
                pnlCompanyCustomer.Visible = False
                pnlCompanyFinancial.Visible = False
                pnlPersonalAsset.Visible = False
                pnlPersonalCustomer.Visible = False
                pnlPersonalFinancial.Visible = False
                pnlEditContent_Range.Visible = True
                pnlEditContent_Range_List.Visible = True
                pnlEditContent_Range_Datagrid.Visible = True
                pnlEditContent_Range_AddEdit.Visible = False
            End If
        End If
        'txtPage.Text = "1"
    End Sub
#End Region

#Region "imgSave_Table_Click"
    Private Sub imgSave_Table_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSave_Table.Click

        If Not IsNumeric(txtScoreValue_Table.Text.Trim) Then            
            ShowMessage(lblMessage, "Score Salah", True)
            pnlView.Visible = False
            pnlCompanyAsset.Visible = False
            pnlCompanyCustomer.Visible = False
            pnlCompanyFinancial.Visible = False
            pnlPersonalAsset.Visible = False
            pnlPersonalCustomer.Visible = False
            pnlPersonalFinancial.Visible = False
            pnlEditContent_Table.Visible = True
            pnlEditContent_Tabel_List.Visible = True
            pnlEditContent_Table_Datagrid.Visible = False
            pnlEditContent_Table_AddEdit.Visible = True
            Exit Sub
        End If

        Dim oCredit As New Parameter.CreditScoringMain
        Dim ErrMessage As String = ""
        oCredit.CreditScoreShemeId = hplSchemeID_Table.Text
        oCredit.CreditScoreComponentId = lblComponent_Table.Text
        oCredit.ContentSeqNo = Me.ContentSeqNo
        oCredit.ValueContent = lblValueContent.Text
        oCredit.ValueDescription = lblValueDescription.Text
        oCredit.ScoreValue = CInt(txtScoreValue_Table.Text.Trim)
        oCredit.ScoreStatus = cboScoreStatus_Table.SelectedItem.Value

        'cmbPurpose.SelectedIndex = cmbPurpose.Items.IndexOf(cmbPurpose.Items.FindByValue(Me.BankPurpose))
        oCredit.strConnection = GetConnectionString()

        If Me.AddEdit = "EDIT" Then
            oCredit.SPName = "spCreditScoringEditContent_TableUpdate"
            m_controller.EditContentSaveEdit_Table(oCredit)
            txtPage1.Text = ""
            BindGridEntity_EditContent_Table()
            pnlView.Visible = False
            pnlCompanyAsset.Visible = False
            pnlCompanyCustomer.Visible = False
            pnlCompanyFinancial.Visible = False
            pnlPersonalAsset.Visible = False
            pnlPersonalCustomer.Visible = False
            pnlPersonalFinancial.Visible = False
            pnlEditContent_Table.Visible = True
            pnlEditContent_Tabel_List.Visible = True
            pnlEditContent_Table_Datagrid.Visible = True
            pnlEditContent_Table_AddEdit.Visible = False            
            ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
        End If
        txtPage1.Text = "1"
    End Sub
#End Region

#Region "imbBack"
    Private Sub imbBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        pnlView.Visible = True
        'pnlCompanyAsset.Visible = True
        'pnlCompanyCustomer.Visible = True
        'pnlCompanyFinancial.Visible = True
        pnlPersonalAsset.Visible = True
        pnlPersonalCustomer.Visible = True
        pnlPersonalFinancial.Visible = True
        pnlEditContent_Range.Visible = False
        pnlEditContent_Range_List.Visible = False
        pnlEditContent_Range_Datagrid.Visible = False
        pnlEditContent_Range_AddEdit.Visible = False
    End Sub
#End Region

#Region "imbAdd"
    Private Sub imbAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles imbAdd.Click
        Dim oCredit As New Parameter.CreditScoringMain
        If CheckFeature(Me.Loginid, Me.FormID, "ADD", Me.AppId) Then
            pnlView.Visible = False
            pnlCompanyAsset.Visible = False
            pnlCompanyCustomer.Visible = False
            pnlCompanyFinancial.Visible = False
            pnlPersonalAsset.Visible = False
            pnlPersonalCustomer.Visible = False
            pnlPersonalFinancial.Visible = False
            pnlEditContent_Range.Visible = True
            pnlEditContent_Range_List.Visible = True
            pnlEditContent_Range_Datagrid.Visible = False
            pnlEditContent_Range_AddEdit.Visible = True

            Me.AddEdit = "ADD"
            lblAddEdit.Text = Me.AddEdit

            oCredit.CreditScoreShemeId = hplSchemeID.Text
            oCredit.CreditScoreComponentId = lblComponent_Range.Text
            oCredit.strConnection = GetConnectionString()
            oCredit.SPName = "spCreditScoringEditContent_RangeEdit"
            oCredit = m_controller.EditContentEdit(oCredit)

            txtValueFrom.Text = "0"
            txtValueTo.Text = "0"
            txtScoreValue.Text = "0"
        End If
    End Sub
#End Region

#Region "imbCancel"
    Private Sub imgCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        pnlView.Visible = False
        pnlCompanyAsset.Visible = False
        pnlCompanyCustomer.Visible = False
        pnlCompanyFinancial.Visible = False
        pnlPersonalAsset.Visible = False
        pnlPersonalCustomer.Visible = False
        pnlPersonalFinancial.Visible = False
        pnlEditContent_Range.Visible = True
        pnlEditContent_Range_List.Visible = True
        pnlEditContent_Range_Datagrid.Visible = True
        pnlEditContent_Range_AddEdit.Visible = False
        'txtPage.Text = "1"
    End Sub
#End Region

#Region "imbCancel_Table"
    Private Sub imgCancel_Table_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCancel_Table.Click
        pnlView.Visible = False
        pnlCompanyAsset.Visible = False
        pnlCompanyCustomer.Visible = False
        pnlCompanyFinancial.Visible = False
        pnlPersonalAsset.Visible = False
        pnlPersonalCustomer.Visible = False
        pnlPersonalFinancial.Visible = False
        pnlEditContent_Table.Visible = True
        pnlEditContent_Tabel_List.Visible = True
        pnlEditContent_Table_Datagrid.Visible = True
        pnlEditContent_Table_AddEdit.Visible = False
        txtPage1.Text = "1"
    End Sub
#End Region

#Region "imgBack_Table"
    Private Sub imgBack_Table_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        pnlView.Visible = True
        'pnlCompanyAsset.Visible = True
        'pnlCompanyCustomer.Visible = True
        'pnlCompanyFinancial.Visible = True
        pnlPersonalAsset.Visible = True
        pnlPersonalCustomer.Visible = True
        pnlPersonalFinancial.Visible = True
        pnlEditContent_Table.Visible = False
        pnlEditContent_Tabel_List.Visible = False
        pnlEditContent_Table_Datagrid.Visible = False
        pnlEditContent_Table_AddEdit.Visible = False
    End Sub
#End Region

#Region "Bind Grade"
    Sub BindGrade()
        Dim GradeList As New List(Of Parameter.CreditScoreGradeMaster)
        Dim oGrade As New Parameter.CreditScoreGradeMaster

        oGrade.CreditScoreSchemeID = Me.CreditScoreSchemeId
        oGrade.strConnection = GetConnectionString()
        GradeList = m_Grade.GetCreditScoreGradeMasterList(oGrade)

        If GradeList.Count > 0 Then
            For i As Integer = 0 To GradeList.Count - 1
                oGrade = New Parameter.CreditScoreGradeMaster
                oGrade = GradeList(i)

                If oGrade.GradeID = 1 Then
                    If oGrade.CustType.ToUpper = "P" Then
                        txtGradeAFromPersonal.Text = oGrade.ScoreFrom.ToString
                        txtGradeAToPersonal.Text = oGrade.ScoreTo.ToString
                    ElseIf oGrade.CustType.ToUpper = "C" Then
                        txtGradeAFromCompany.Text = oGrade.ScoreFrom.ToString
                        txtGradeAToCompany.Text = oGrade.ScoreTo.ToString
                    End If
                ElseIf oGrade.GradeID = 2 Then
                    If oGrade.CustType.ToUpper = "P" Then
                        txtGradeBFromPersonal.Text = oGrade.ScoreFrom.ToString
                        txtGradeBToPersonal.Text = oGrade.ScoreTo.ToString
                    ElseIf oGrade.CustType.ToUpper = "C" Then
                        txtGradeBFromCompany.Text = oGrade.ScoreFrom.ToString
                        txtGradeBToCompany.Text = oGrade.ScoreTo.ToString
                    End If
                ElseIf oGrade.GradeID = 3 Then
                    If oGrade.CustType.ToUpper = "P" Then
                        txtGradeCFromPersonal.Text = oGrade.ScoreFrom.ToString
                        txtGradeCToPersonal.Text = oGrade.ScoreTo.ToString
                    ElseIf oGrade.CustType.ToUpper = "C" Then
                        txtGradeCFromCompany.Text = oGrade.ScoreFrom.ToString
                        txtGradeCToCompany.Text = oGrade.ScoreTo.ToString
                    End If
                ElseIf oGrade.GradeID = 4 Then
                    If oGrade.CustType.ToUpper = "P" Then
                        txtGradeCcFromPersonal.Text = oGrade.ScoreFrom.ToString
                        txtGradeCcToPersonal.Text = oGrade.ScoreTo.ToString
                    ElseIf oGrade.CustType.ToUpper = "C" Then
                        txtGradeCcFromCompany.Text = oGrade.ScoreFrom.ToString
                        txtGradeCcToCompany.Text = oGrade.ScoreTo.ToString
                    End If
                ElseIf oGrade.GradeID = 5 Then
                    If oGrade.CustType.ToUpper = "P" Then
                        txtGradeDFromPersonal.Text = oGrade.ScoreFrom.ToString
                        txtGradeDToPersonal.Text = oGrade.ScoreTo.ToString
                    ElseIf oGrade.CustType.ToUpper = "C" Then
                        txtGradeDFromCompany.Text = oGrade.ScoreFrom.ToString
                        txtGradeDToCompany.Text = oGrade.ScoreTo.ToString
                    End If
                End If
            Next
        Else
            txtGradeAFromPersonal.Text = ""
            txtGradeAToPersonal.Text = ""
            txtGradeAFromCompany.Text = ""
            txtGradeAToCompany.Text = ""
            txtGradeBFromPersonal.Text = ""
            txtGradeBToPersonal.Text = ""
            txtGradeBFromCompany.Text = ""
            txtGradeBToCompany.Text = ""
            txtGradeCFromPersonal.Text = ""
            txtGradeCToPersonal.Text = ""
            txtGradeCFromCompany.Text = ""
            txtGradeCToCompany.Text = ""
            txtGradeCcFromPersonal.Text = ""
            txtGradeCcToPersonal.Text = ""
            txtGradeCcFromCompany.Text = ""
            txtGradeCcToCompany.Text = ""
            txtGradeDFromPersonal.Text = ""
            txtGradeDToPersonal.Text = ""
            txtGradeDFromCompany.Text = ""
            txtGradeDToCompany.Text = ""
        End If


    End Sub
#End Region

    Protected Sub ButtonBack_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonBack.Click
        Response.Redirect("CreditScoring_EditContent.aspx?CreditScoreSchemeID=" & Me.CreditScoreSchemeID.ToString)
    End Sub
    Protected Sub ButtonBack_table_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonBack_table.Click
        Response.Redirect("CreditScoring_EditContent.aspx?CreditScoreSchemeID=" & Me.CreditScoreSchemeID.ToString)
    End Sub
End Class