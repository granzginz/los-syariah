﻿Imports Maxiloan.Parameter
Imports Maxiloan.Controller


Public Class CRRejectPolicy_EditContent
    Inherits Maxiloan.Webform.WebBased
    Private m_controller As New CreditScoringMainController

    Private recordCount As Int64 = 1
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1

    Private Property ContentSeqNo() As Integer
        Get
            Return CType(viewstate("ContentSeqNo"), Integer)
        End Get
        Set(ByVal Value As Integer)
            viewstate("ContentSeqNo") = Value
        End Set
    End Property

    Private Property ItemEditId() As String
        Get
            Return CType(ViewState("ItemEditId"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ItemEditId") = Value
        End Set
    End Property


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.FormID = "RCJPOLEDTC"
            If SessionInvalid() Then
                Exit Sub
            End If
            BindGrid()
        End If
    End Sub
    Private dtEntity As DataTable

    Sub BindGrid()

        Dim oCredit As New Parameter.CreditScoringMain

        With oCredit
            .WhereCond = ""
            .CreditScoreShemeId = ""
            .strConnection = GetConnectionString()
            .SPName = "spCreditRejectPolicyCardView"
        End With
        oCredit = m_controller.CreditScoringMainView(oCredit)

        If Not oCredit Is Nothing Then
            dtEntity = oCredit.ListData
            recordCount = oCredit.TotalRecords
        Else
            recordCount = 0
        End If

        dtgRejectPolicy.DataSource = dtEntity.DefaultView
        dtgRejectPolicy.CurrentPageIndex = 0
        dtgRejectPolicy.DataBind()
        pnlView.Visible = True
        pnlEditContent_Range_Datagrid.Visible = False
        pnlEditContent_Table_Datagrid.Visible = False
        pnlEditContent_Range_AddEdit.Visible = False
        pnlEditContent_Table_AddEdit.Visible = False
    End Sub


    Private Sub dtgCompanyCustomer_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgRejectPolicy.ItemCommand
        Dim oCredit As New Parameter.CreditScoringMain
        If e.CommandName = "EditContent" Then
            Select Case CType(e.Item.FindControl("lblCalculationType"), Label).Text
                Case "R"
                    BindGridEntity_EditContent_Range(CType(e.Item.FindControl("lblDataItemID"), Label).Text.Trim)
                Case "T"
                    BindGridEntity_EditContent_Table(CType(e.Item.FindControl("lblDataItemID"), Label).Text.Trim)
            End Select
        End If
    End Sub
    Sub BindGridEntity_EditContent_Range(itemId As String)

        Dim oCredit As New Parameter.CreditScoringMain

        ItemEditId = itemId
        oCredit.SortBy = ""
        oCredit.PageSize = 1
        oCredit.CurrentPage = 1
        oCredit.WhereCond = itemId
        oCredit.strConnection = GetConnectionString()
        oCredit.SPName = "spCreditScoreRejectPolicyEdit_RangePaging"
        oCredit = m_controller.GetEditContentPaging(oCredit)

        If Not oCredit Is Nothing Then
            dtEntity = oCredit.ListData
            recordCount = oCredit.TotalRecords
        Else
            recordCount = 0
        End If

        dtgEditContent_Range.DataSource = dtEntity.DefaultView
        dtgEditContent_Range.CurrentPageIndex = 0
        dtgEditContent_Range.DataBind()
        pnlView.Visible = False
        pnlEditContent_Range_Datagrid.Visible = True
        pnlEditContent_Range_AddEdit.Visible = False
    End Sub


#Region "dtgEditContent_Range_ItemDataBound"
    Private Sub dtgEditContent_Range_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgEditContent_Range.ItemDataBound
        Dim imbDelete As ImageButton
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imgDeleteRange"), ImageButton)
            imbDelete.Attributes.Add("Onclick", "return DeleteConfirm()")
        End If
    End Sub
#End Region

#Region "dtgEditContent_Range_ItemCommand"
    Private Sub dtgEditContent_Range_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgEditContent_Range.ItemCommand
        Dim oCredit As New Parameter.CreditScoringMain
        If e.CommandName = "EditRange" Then
            Dim labelContentSeqNo As New Label

            Me.ContentSeqNo = CInt(CType(e.Item.FindControl("lblContentSeqNo"), Label).Text)

            pnlView.Visible = False
            pnlEditContent_Range_Datagrid.Visible = False
            pnlEditContent_Range_AddEdit.Visible = True

            lblAddEdit.Text = "EDIT"

            oCredit.CreditScoreShemeId = ""
            oCredit.CreditScoreComponentId = ""
            oCredit.ContentSeqNo = Me.ContentSeqNo
            oCredit.strConnection = GetConnectionString()
            oCredit.SPName = "spCreditScoreRejectPolicyEditContent_RangeEdit"
            oCredit = m_controller.EditContentEdit(oCredit)

            txtValueFrom.Text = CStr(oCredit.ValueFrom)
            txtValueTo.Text = CStr(oCredit.ValueTo)
            txtScoreValue.Text = CStr(oCredit.ScoreValue)

            cboScoreStatus.SelectedIndex = cboScoreStatus.Items.IndexOf(cboScoreStatus.Items.FindByValue(oCredit.ScoreStatus))

        ElseIf e.CommandName = "DeleteRange" Then

            oCredit.CreditScoreShemeId = ""
            oCredit.CreditScoreComponentId = ""
            oCredit.ContentSeqNo = CInt(CType(e.Item.FindControl("lblContentSeqNo"), Label).Text)
            oCredit.strConnection = GetConnectionString()
            oCredit.SPName = "spCreditScoreRejectPolicyEditContent_RangeDelete"
            Dim ResultOutput As String
            ResultOutput = m_controller.EditContentDelete(oCredit)

            If ResultOutput = "OK" Then
                ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, False)
            Else
                ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_FAILED, True)
            End If
            BindGridEntity_EditContent_Range(ItemEditId)

        End If
    End Sub
#End Region


    Private Sub imgSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSave.Click

        If Not IsNumeric(txtValueFrom.Text.Trim) Then
            ShowMessage(lblMessage, "Nilai FROM Salah", True)
            Exit Sub
        End If

        If Not IsNumeric(txtValueTo.Text.Trim) Then
            ShowMessage(lblMessage, "Nilai TO Salah", True)
            Exit Sub
        End If

        If Not IsNumeric(txtScoreValue.Text.Trim) Then
            ShowMessage(lblMessage, "Score Salah", True)
            Exit Sub
        End If

        Dim oCredit As New Parameter.CreditScoringMain
        Dim ErrMessage As String = ""
        oCredit.CreditScoreShemeId = ""
        oCredit.CreditScoreComponentId = ItemEditId
        oCredit.ValueFrom = CDbl(txtValueFrom.Text.Trim)
        oCredit.ValueTo = CDbl(txtValueTo.Text.Trim)
        oCredit.ScoreValue = CInt(txtScoreValue.Text.Trim)
        oCredit.ScoreStatus = cboScoreStatus.SelectedItem.Value
        oCredit.ContentSeqNo = Me.ContentSeqNo

        'cmbPurpose.SelectedIndex = cmbPurpose.Items.IndexOf(cmbPurpose.Items.FindByValue(Me.BankPurpose))
        oCredit.strConnection = GetConnectionString()

        If lblAddEdit.Text = "EDIT" Then

            Try
                oCredit.SPName = "spCreditScoreRejectPolicyEditContent_RangeUpdate"
                m_controller.EditContentSaveEdit(oCredit)
                BindGridEntity_EditContent_Range(ItemEditId)
                ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
                BindGridEntity_EditContent_Range(ItemEditId)
            Catch ex As Exception
                ShowMessage(lblMessage, ex.Message, True)
            End Try
        ElseIf lblAddEdit.Text = "ADD" Then
            oCredit.SPName = "spCreditScoreRejectPolicyEditContent_RangeAdd"
            ErrMessage = m_controller.EditContentSaveAdd(oCredit)
            If ErrMessage <> "" Then
                ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_FAILED, True)
                pnlEditContent_Range_AddEdit.Visible = True
                Exit Sub
            Else
                ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_SUCCESS, False)
                BindGridEntity_EditContent_Range(ItemEditId)
            End If
        End If

    End Sub

    Private Sub imgCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        BindGridEntity_EditContent_Range(ItemEditId)
    End Sub

    Sub BindGridEntity_EditContent_Table(itemId As String)
        Dim dtEntity As DataTable
        Dim oCredit As New Parameter.CreditScoringMain
        ItemEditId = itemId
        oCredit.SortBy = ""
        oCredit.PageSize = 1
        oCredit.CurrentPage = 1
        oCredit.CreditScoreShemeId = ""
        oCredit.CreditScoreComponentId = ""
        oCredit.WhereCond = itemId

        oCredit.strConnection = GetConnectionString()
        oCredit.SPName = "spCreditScoreRejectPolicyEdit_TablePaging"
        oCredit = m_controller.GetEditContentPaging_Table(oCredit)

        If Not oCredit Is Nothing Then
            dtEntity = oCredit.ListData
            recordCount = oCredit.TotalRecords
        Else
            recordCount = 0
        End If

        dtgEditContent_Table.DataSource = dtEntity.DefaultView
        dtgEditContent_Table.CurrentPageIndex = 0
        dtgEditContent_Table.DataBind()

        pnlView.Visible = False

        pnlEditContent_Table_Datagrid.Visible = True
        pnlEditContent_Range_AddEdit.Visible = False
        pnlEditContent_Table_AddEdit.Visible = False
    End Sub


    Private Sub dtgEditContent_Table_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgEditContent_Table.ItemCommand
        If e.CommandName = "EditTable" Then
            Dim oCredit As New Parameter.CreditScoringMain
            Me.ContentSeqNo = CInt(CType(e.Item.FindControl("lblContentSeqNo_Table"), Label).Text)


            pnlView.Visible = False
            pnlEditContent_Table_Datagrid.Visible = False
            pnlEditContent_Table_AddEdit.Visible = True

            lblAddEdit_Table.Text = "EDIT"

            oCredit.CreditScoreShemeId = ""
            oCredit.CreditScoreComponentId = ItemEditId
            oCredit.ContentSeqNo = ContentSeqNo
            oCredit.strConnection = GetConnectionString()
            oCredit.SPName = "spCreditScoreRejectPolicyEditContent_TableEdit"
            oCredit = m_controller.EditContentEdit_Table(oCredit)

            lblValueContent.Text = oCredit.ValueContent
            lblValueDescription.Text = oCredit.ValueDescription

            txtScoreValue_Table.Text = CStr(oCredit.ScoreValue)
            cboScoreStatus_Table.SelectedIndex = cboScoreStatus_Table.Items.IndexOf(cboScoreStatus_Table.Items.FindByValue(oCredit.ScoreStatus))

        End If
    End Sub


    Private Sub imgSave_Table_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSave_Table.Click

        If Not IsNumeric(txtScoreValue_Table.Text.Trim) Then
            ShowMessage(lblMessage, "Score Salah", True)
            Exit Sub
        End If

        Dim oCredit As New Parameter.CreditScoringMain
        Dim ErrMessage As String = ""
        oCredit.CreditScoreShemeId = ""
        oCredit.CreditScoreComponentId = ""
        oCredit.ContentSeqNo = Me.ContentSeqNo
        oCredit.ValueContent = lblValueContent.Text
        oCredit.ValueDescription = lblValueDescription.Text
        oCredit.ScoreValue = CInt(txtScoreValue_Table.Text.Trim)
        oCredit.ScoreStatus = cboScoreStatus_Table.SelectedItem.Value

        oCredit.strConnection = GetConnectionString()

        If lblAddEdit_Table.Text = "EDIT" Then
            oCredit.SPName = "spCreditScoreRejectPolicyEditContent_TableUpdate"

            Try
                m_controller.EditContentSaveEdit_Table(oCredit)

                BindGridEntity_EditContent_Table(ItemEditId)
                pnlView.Visible = False

                pnlEditContent_Table_Datagrid.Visible = True
                pnlEditContent_Table_AddEdit.Visible = False
                ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
            Catch ex As Exception
                ShowMessage(lblMessage, ex.Message, True)
            End Try
        End If

    End Sub
    Private Sub imgCancel_Table_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCancel_Table.Click
        BindGridEntity_EditContent_Table(ItemEditId)
    End Sub
    Protected Sub ButtonBack_table_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonBack_table.Click
        Response.Redirect("CrRejectPolicy_EditContent.aspx")
    End Sub

    Protected Sub ButtonBack_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonBack.Click
        Response.Redirect("CrRejectPolicy_EditContent.aspx")
    End Sub
End Class