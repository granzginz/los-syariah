﻿#Region "Imports"
Imports System.DBNull
Imports Maxiloan.cbse
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports System.Linq
#End Region

Public Class CreditScoring_Main
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private m_controller As New CreditScoringMainController
    Private m_Grade As New CreditScoreGradeMasterController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Integer = 1

    Dim Temp_TotalWeight_CompanyCustomer As Double = 0
    Dim Temp_TotalWeight_CompanyAsset As Double = 0
    Dim Temp_TotalWeight_CompanyFinancial As Double = 0
    Dim Temp_TotalWeight_PersonalCustomer As Double = 0
    Dim Temp_TotalWeight_PersonalAsset As Double = 0
    Dim Temp_TotalWeight_PersonalFinancial As Double = 0
#End Region

#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(viewstate("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property AddEdit() As String
        Get
            Return CType(viewstate("vwsAddEdt"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("vwsAddEdt") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(viewstate("Sort"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Sort") = Value
        End Set
    End Property

    Private Property CreditScoreSchemeId() As String
        Get
            Return CType(viewstate("CreditScoreSchemeId"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CreditScoreSchemeId") = Value
        End Set
    End Property

    Private Property Description() As String
        Get
            Return CType(viewstate("Description"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Description") = Value
        End Set
    End Property

    Private Property Total_PersonalCustomer() As String
        Get
            Return CType(viewstate("Total_PersonalCustomer"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Total_PersonalCustomer") = Value
        End Set
    End Property

    Private Property Total_PersonalAsset() As String
        Get
            Return CType(viewstate("Total_PersonalAsset"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Total_PersonalAsset") = Value
        End Set
    End Property

    Private Property Total_PersonalFinancial() As String
        Get
            Return CType(viewstate("Total_PersonalFinancial"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Total_PersonalFinancial") = Value
        End Set
    End Property

    Private Property Total_CompanyCustomer() As String
        Get
            Return CType(viewstate("Total_CompanyCustomer"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Total_CompanyCustomer") = Value
        End Set
    End Property

    Private Property Total_CompanyAsset() As String
        Get
            Return CType(viewstate("Total_CompanyAsset"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Total_CompanyAsset") = Value
        End Set
    End Property

    Private Property Total_CompanyFinancial() As String
        Get
            Return CType(viewstate("Total_CompanyFinancial"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Total_CompanyFinancial") = Value
        End Set
    End Property

    Private Property TotalRow() As Integer
        Get
            Return CType(viewstate("TotalRow"), Integer)
        End Get
        Set(ByVal Value As Integer)
            viewstate("TotalRow") = Value
        End Set
    End Property

    Private Property TotalRow_PersonalCustomer() As Integer
        Get
            Return CType(viewstate("TotalRow_PersonalCustomer"), Integer)
        End Get
        Set(ByVal Value As Integer)
            viewstate("TotalRow_PersonalCustomer") = Value
        End Set
    End Property

    Private Property TotalRow_PersonalAsset() As Integer
        Get
            Return CType(viewstate("TotalRow_PersonalAsset"), Integer)
        End Get
        Set(ByVal Value As Integer)
            viewstate("TotalRow_PersonalAsset") = Value
        End Set
    End Property

    Private Property TotalRow_PersonalFinancial() As Integer
        Get
            Return CType(viewstate("TotalRow_PersonalFinancial"), Integer)
        End Get
        Set(ByVal Value As Integer)
            viewstate("TotalRow_PersonalFinancial") = Value
        End Set
    End Property

    Private Property TotalRow_CompanyCustomer() As Integer
        Get
            Return CType(viewstate("TotalRow_CompanyCustomer"), Integer)
        End Get
        Set(ByVal Value As Integer)
            viewstate("TotalRow_CompanyCustomer") = Value
        End Set
    End Property

    Private Property TotalRow_CompanyAsset() As Integer
        Get
            Return CType(viewstate("TotalRow_CompanyAsset"), Integer)
        End Get
        Set(ByVal Value As Integer)
            viewstate("TotalRow_CompanyAsset") = Value
        End Set
    End Property

    Private Property TotalRow_CompanyFinancial() As Integer
        Get
            Return CType(viewstate("TotalRow_CompanyFinancial"), Integer)
        End Get
        Set(ByVal Value As Integer)
            viewstate("TotalRow_CompanyFinancial") = Value
        End Set
    End Property
#End Region

#Region "Page_Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If
        InitialDefaultPanel()
        If Not Me.IsPostBack Then
            txtSearch.Text = ""
            Me.FormID = "CreditScoringCard"

            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If SessionInvalid() Then
                    Exit Sub
                End If
                Me.SearchBy = ""
                Me.SortBy = ""
            End If

            txtPage.Text = "1"
            txtSearch.Text = ""
            Me.Sort = "CreditScoreSchemeID ASC"
            Me.CmdWhere = "ALL"

            pnlAddEdit.Visible = False
            pnlList.Visible = True
            BindGridEntity_List(Me.CmdWhere)
        End If
    End Sub
#End Region

#Region "LinkTo"
    Function LinkTo(ByVal strCreditScoreSchemeID As String) As String
        Return "javascript:OpenWinSchemeView('" & strCreditScoreSchemeID & "')"
    End Function
#End Region

#Region "BindGridEntity"
    Sub BindGridEntity_List(ByVal cmdWhere As String)
        Dim dtEntity As DataTable
        Dim oCredit As New Parameter.CreditScoringMain

        With oCredit
            .WhereCond = Me.CmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = Me.Sort
            .strConnection = GetConnectionString
            .SPName = "spCreditScoringCardPaging"
        End With
        oCredit = m_controller.GetCreditScoringMain(oCredit)

        If Not oCredit Is Nothing Then
            dtEntity = oCredit.ListData
            recordCount = oCredit.TotalRecords
        Else
            recordCount = 0
        End If
        Me.TotalRow = dtEntity.Rows.Count

       

        dtgCreditScoring_Main.DataSource = dtEntity.DefaultView
        dtgCreditScoring_Main.CurrentPageIndex = 0
        dtgCreditScoring_Main.DataBind()
        PagingFooter()

    End Sub


    Sub BindGridEntity_PersonalCustomer(ByVal cmdWhere As String)
        Dim dtEntity As DataTable
        Dim oCredit As New Parameter.CreditScoringMain

        If Me.AddEdit = "EDIT" Then
            With oCredit
                .WhereCond = "c.ScoringType='P' and c.GroupComponent='C'"
                .CreditScoreShemeId = Me.CreditScoreSchemeId
                .CurrentPage = currentPage
                .PageSize = pageSize
                .SortBy = Me.Sort
                .strConnection = GetConnectionString
                .SPName = "spCreditScoringCardEdit"
            End With
            oCredit = m_controller.CreditScoringMainEdit(oCredit)

            lblSchemeID.Text = Me.CreditScoreSchemeId
            lblSchemeID.Visible = True
            txtSchemeID.Visible = False
            txtDescription.Text = Me.Description
            txtApprovalScore_Company.Text = CStr(oCredit.CompanyApprovedScore).Trim
            txtRejectScore_Company.Text = CStr(oCredit.CompanyRejectScore).Trim
            txtApprovalScore_Personal.Text = CStr(oCredit.PersonalApprovedScore).Trim
            txtRejectScore_Personal.Text = CStr(oCredit.PersonalRejectScore).Trim
            'cut
            txtCutOffBefore.Text = CStr(oCredit.CutOffBefore).Trim

        ElseIf Me.AddEdit = "ADD" Then
            With oCredit
                .WhereCond = "c.ScoringType='P' and c.GroupComponent='C'"
                .strConnection = GetConnectionString
                .SPName = "spCreditScoringCardAdd"
            End With
            oCredit = m_controller.CreditScoringMainAdd(oCredit)
        End If


        If Not oCredit Is Nothing Then
            dtEntity = oCredit.ListData
            recordCount = oCredit.TotalRecords
            Me.TotalRow_PersonalCustomer = recordCount
        Else
            recordCount = 0
        End If
        Me.TotalRow = dtEntity.Rows.Count


        dtgPersonalCustomer.DataSource = dtEntity.DefaultView
        dtgPersonalCustomer.CurrentPageIndex = 0
        dtgPersonalCustomer.DataBind()

    End Sub

    Sub BindGridEntity_PersonalFinancial(ByVal cmdWhere As String)
        Dim dtEntity As DataTable
        Dim oCredit As New Parameter.CreditScoringMain

        If Me.AddEdit = "EDIT" Then
            With oCredit
                .WhereCond = "c.ScoringType='P' and c.GroupComponent='F'"
                .CreditScoreShemeId = Me.CreditScoreSchemeId
                .CurrentPage = currentPage
                .PageSize = pageSize
                .SortBy = Me.Sort
                .strConnection = GetConnectionString
                .SPName = "spCreditScoringCardEdit"
            End With
            oCredit = m_controller.CreditScoringMainEdit(oCredit)
        ElseIf Me.AddEdit = "ADD" Then
            With oCredit
                .WhereCond = "c.ScoringType='P' and c.GroupComponent='F'"
                .strConnection = GetConnectionString
                .SPName = "spCreditScoringCardAdd"
            End With
            oCredit = m_controller.CreditScoringMainAdd(oCredit)

            lblSchemeID.Text = Me.CreditScoreSchemeId
            lblSchemeID.Visible = True
            txtSchemeID.Visible = False
            txtDescription.Text = Me.Description
            txtApprovalScore_Company.Text = CStr(oCredit.CompanyApprovedScore).Trim
            txtRejectScore_Company.Text = CStr(oCredit.CompanyRejectScore).Trim
            txtApprovalScore_Personal.Text = CStr(oCredit.PersonalApprovedScore).Trim
            txtRejectScore_Personal.Text = CStr(oCredit.PersonalRejectScore).Trim
            'cut
            txtCutOffBefore.Text = CStr(oCredit.CutOffBefore).Trim

        End If

        If Not oCredit Is Nothing Then
            dtEntity = oCredit.ListData
            recordCount = oCredit.TotalRecords
            Me.TotalRow_PersonalFinancial = recordCount
        Else
            recordCount = 0
        End If
        Me.TotalRow = dtEntity.Rows.Count


        dtgPersonalFinancial.DataSource = dtEntity.DefaultView
        dtgPersonalFinancial.CurrentPageIndex = 0
        dtgPersonalFinancial.DataBind()

    End Sub

    Sub BindGridEntity_PersonalAsset(ByVal cmdWhere As String)
        Dim dtEntity As DataTable
        Dim oCredit As New Parameter.CreditScoringMain

        If Me.AddEdit = "EDIT" Then
            With oCredit
                .WhereCond = "c.ScoringType='P' and c.GroupComponent='A'"
                .CreditScoreShemeId = Me.CreditScoreSchemeId
                .CurrentPage = currentPage
                .PageSize = pageSize
                .SortBy = Me.Sort
                .strConnection = GetConnectionString
                .SPName = "spCreditScoringCardEdit"
            End With
            oCredit = m_controller.CreditScoringMainEdit(oCredit)

            lblSchemeID.Text = Me.CreditScoreSchemeId
            lblSchemeID.Visible = True
            txtSchemeID.Visible = False
            txtDescription.Text = Me.Description
            txtApprovalScore_Company.Text = CStr(oCredit.CompanyApprovedScore).Trim
            txtRejectScore_Company.Text = CStr(oCredit.CompanyRejectScore).Trim
            txtApprovalScore_Personal.Text = CStr(oCredit.PersonalApprovedScore).Trim
            txtRejectScore_Personal.Text = CStr(oCredit.PersonalRejectScore).Trim
            'cut
            txtCutOffBefore.Text = CStr(oCredit.CutOffBefore).Trim


        ElseIf Me.AddEdit = "ADD" Then
            With oCredit
                .WhereCond = "c.ScoringType='P' and c.GroupComponent='A'"
                .strConnection = GetConnectionString
                .SPName = "spCreditScoringCardAdd"
            End With
            oCredit = m_controller.CreditScoringMainAdd(oCredit)
        End If

        If Not oCredit Is Nothing Then
            dtEntity = oCredit.ListData
            recordCount = oCredit.TotalRecords
            Me.TotalRow_PersonalAsset = recordCount
        Else
            recordCount = 0
        End If
        Me.TotalRow = dtEntity.Rows.Count

        dtgPersonalAsset.DataSource = dtEntity.DefaultView
        dtgPersonalAsset.CurrentPageIndex = 0
        dtgPersonalAsset.DataBind()

    End Sub

    Sub BindGridEntity_CompanyCustomer(ByVal cmdWhere As String)
        Dim dtEntity As DataTable
        Dim oCredit As New Parameter.CreditScoringMain

        If Me.AddEdit = "EDIT" Then
            With oCredit
                .WhereCond = "c.ScoringType='C' and c.GroupComponent='C'"
                .CreditScoreShemeId = Me.CreditScoreSchemeId
                .CurrentPage = currentPage
                .PageSize = pageSize
                .SortBy = Me.Sort
                .strConnection = GetConnectionString
                .SPName = "spCreditScoringCardEdit"
            End With
            oCredit = m_controller.CreditScoringMainEdit(oCredit)
        ElseIf Me.AddEdit = "ADD" Then
            With oCredit
                .WhereCond = "c.ScoringType='C' and c.GroupComponent='C'"
                .strConnection = GetConnectionString
                .SPName = "spCreditScoringCardAdd"
            End With
            oCredit = m_controller.CreditScoringMainAdd(oCredit)
        End If

        If Not oCredit Is Nothing Then
            dtEntity = oCredit.ListData
            recordCount = oCredit.TotalRecords
            Me.TotalRow_CompanyCustomer = recordCount
        Else
            recordCount = 0
        End If
        Me.TotalRow = dtEntity.Rows.Count

        dtgCompanyCustomer.DataSource = dtEntity.DefaultView
        dtgCompanyCustomer.CurrentPageIndex = 0
        dtgCompanyCustomer.DataBind()

    End Sub

    Sub BindGridEntity_CompanyFinancial(ByVal cmdWhere As String)
        Dim dtEntity As DataTable
        Dim oCredit As New Parameter.CreditScoringMain

        If Me.AddEdit = "EDIT" Then
            With oCredit
                .WhereCond = "c.ScoringType='C' and c.GroupComponent='F'"
                .CreditScoreShemeId = Me.CreditScoreSchemeId
                .CurrentPage = currentPage
                .PageSize = pageSize
                .SortBy = Me.Sort
                .strConnection = GetConnectionString
                .SPName = "spCreditScoringCardEdit"
            End With
            oCredit = m_controller.CreditScoringMainEdit(oCredit)
        ElseIf Me.AddEdit = "ADD" Then
            With oCredit
                .WhereCond = "c.ScoringType='C' and c.GroupComponent='F'"
                .strConnection = GetConnectionString
                .SPName = "spCreditScoringCardAdd"
            End With
            oCredit = m_controller.CreditScoringMainAdd(oCredit)
        End If

        If Not oCredit Is Nothing Then
            dtEntity = oCredit.ListData
            recordCount = oCredit.TotalRecords
            Me.TotalRow_CompanyFinancial = recordCount
        Else
            recordCount = 0
        End If
        Me.TotalRow = dtEntity.Rows.Count


        dtgCompanyFinancial.DataSource = dtEntity.DefaultView
        dtgCompanyFinancial.CurrentPageIndex = 0
        dtgCompanyFinancial.DataBind()

    End Sub

    Sub BindGridEntity_CompanyAsset(ByVal cmdWhere As String)
        Dim dtEntity As DataTable
        Dim oCredit As New Parameter.CreditScoringMain

        If Me.AddEdit = "EDIT" Then
            With oCredit
                .WhereCond = "c.ScoringType='C' and c.GroupComponent='A'"
                .CreditScoreShemeId = Me.CreditScoreSchemeId
                .CurrentPage = currentPage
                .PageSize = pageSize
                .SortBy = Me.Sort
                .strConnection = GetConnectionString
                .SPName = "spCreditScoringCardEdit"
            End With
            oCredit = m_controller.CreditScoringMainEdit(oCredit)

            lblSchemeID.Text = Me.CreditScoreSchemeId
            lblSchemeID.Visible = True
            txtSchemeID.Visible = False
            txtDescription.Text = Me.Description
            txtApprovalScore_Company.Text = CStr(oCredit.CompanyApprovedScore).Trim
            txtRejectScore_Company.Text = CStr(oCredit.CompanyRejectScore).Trim
            txtApprovalScore_Personal.Text = CStr(oCredit.PersonalApprovedScore).Trim
            txtRejectScore_Personal.Text = CStr(oCredit.PersonalRejectScore).Trim

        ElseIf Me.AddEdit = "ADD" Then
            With oCredit
                .WhereCond = "c.ScoringType='C' and c.GroupComponent='A'"
                .strConnection = GetConnectionString
                .SPName = "spCreditScoringCardAdd"
            End With
            oCredit = m_controller.CreditScoringMainAdd(oCredit)
        End If


        If Not oCredit Is Nothing Then
            dtEntity = oCredit.ListData
            recordCount = oCredit.TotalRecords
            Me.TotalRow_CompanyAsset = recordCount
        Else
            recordCount = 0
        End If
        Me.TotalRow = dtEntity.Rows.Count


        dtgCompanyAsset.DataSource = dtEntity.DefaultView
        dtgCompanyAsset.CurrentPageIndex = 0
        dtgCompanyAsset.DataBind()
    End Sub
#End Region

#Region "dtgCreditScoring_Main_ItemCommand"
    Private Sub dtgCreditScoring_Main_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgCreditScoring_Main.ItemCommand
        ButtonSave.Visible = True
        ButtonCancel.Visible = True

        Dim oCredit As New Parameter.CreditScoringMain
        If e.CommandName = "Edit" Then
            If CheckFeature(Me.Loginid, Me.FormID, "EDIT", Me.AppId) Then
                Me.AddEdit = "EDIT"
                pnlAddEdit.Visible = True
                'pnlCompanyAsset.Visible = True
                pnlCompanyAsset.Visible = True
                pnlCompanyCustomer.Visible = False
                pnlCompanyFinancial.Visible = False
                pnlPersonalAsset.Visible = True
                pnlPersonalCustomer.Visible = True
                pnlPersonalFinancial.Visible = True
                pnlList.Visible = False
                lblRequiredSchemeID.Visible = False

                ButtonSave.Visible = True
                ButtonCancel.Visible = True

                lblTitleAddEdit.Text = Me.AddEdit

                Dim label_SchemeId As New label
                Dim strSchemeId As String
                label_SchemeId = CType(e.Item.FindControl("lblSchemeId_Main"), label)
                strSchemeId = label_SchemeId.Text.Trim

                Dim label_Description As New label
                Dim strDescription As String
                label_Description = CType(e.Item.FindControl("lblDescription"), label)
                strDescription = label_Description.Text.Trim

                Me.CreditScoreSchemeId = strSchemeId
                Me.Description = strDescription

                'BindGridEntity_CompanyAsset(Me.CmdWhere)
                'BindGridEntity_CompanyCustomer(Me.CmdWhere)
                'BindGridEntity_CompanyFinancial(Me.CmdWhere)
                BindGridEntity_PersonalAsset(Me.CmdWhere)
                BindGridEntity_PersonalCustomer(Me.CmdWhere)
                BindGridEntity_PersonalFinancial(Me.CmdWhere)
                GrandTotal()
                BindGrade()
            End If
        ElseIf e.CommandName = "EditContent" Then
            If CheckFeature(Me.Loginid, Me.FormID, "EDIT", Me.AppId) Then
                Response.Redirect("CreditScoring_EditContent.aspx?CreditScoreSchemeID=" & dtgCreditScoring_Main.DataKeys.Item(e.Item.ItemIndex).ToString)
            End If
        ElseIf e.CommandName = "Delete" Then
            If CheckFeature(Me.Loginid, Me.FormID, "DEL", Me.AppId) Then
                ButtonSave.Visible = True
                ButtonCancel.Visible = True

                Dim customClass As New Parameter.CreditScoringMain
                With customClass
                    .CreditScoreShemeId = dtgCreditScoring_Main.DataKeys.Item(e.Item.ItemIndex).ToString
                    .strConnection = GetConnectionString
                    .SPName = "spCreditScoringCardDelete"
                End With
                Dim ResultOutput As String
                ResultOutput = m_controller.CreditScoringMainDelete(customClass)
                If ResultOutput = "OK" Then                    
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, False)
                Else                    
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_FAILED, True)
                End If
                Me.CmdWhere = "ALL"
                txtSearch.Text = ""
                SaveGrade("Delete")
                BindGridEntity_List(Me.CmdWhere)
            End If
        End If
    End Sub
#End Region

#Region "Save"
    Private Sub ButtonSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
        Dim customClass As New Parameter.CreditScoringMain
        Dim ErrMessage As String = ""

        If Page.IsValid And ValidasiGrade() Then
            Dim TotalRecord_PersonalCustomer As Integer = dtgPersonalCustomer.Items.Count
            Dim TotalRecord_PersonalAsset As Integer = dtgPersonalAsset.Items.Count
            Dim TotalRecord_PersonalFinancial As Integer = dtgPersonalFinancial.Items.Count
            Dim TotalRecord_CompanyCustomer As Integer = dtgCompanyCustomer.Items.Count
            Dim TotalRecord_CompanyAsset As Integer = dtgCompanyAsset.Items.Count
            Dim TotalRecord_CompanyFinancial As Integer = dtgCompanyFinancial.Items.Count

            'check kalo weight di personalcustomer kosong
            Dim i_PersonalCustomer As Integer
            For i_PersonalCustomer = 0 To TotalRecord_PersonalCustomer - 1
                Dim chkPersonalCustomer As New CheckBox
                chkPersonalCustomer = CType(dtgPersonalCustomer.Items.Item(i_PersonalCustomer).FindControl("chkComponent_PersonalCustomer"), CheckBox)

                Dim txtWeightPersonalCustomer As New TextBox
                txtWeightPersonalCustomer = CType(dtgPersonalCustomer.Items.Item(i_PersonalCustomer).FindControl("txtWeight_PersonalCustomer"), TextBox)

                Me.CreditScoreSchemeId = lblSchemeID.Text.Trim

                If (chkPersonalCustomer.Checked = True) And (txtWeightPersonalCustomer.Text.Trim = "0") Then                    
                    ShowMessage(lblMessage, "Harap isi bobot untuk komponen yg dipilih dalam Personal Customer!", True)
                    'txtWeightPersonalCustomer.ReadOnly = False
                    txtWeightPersonalCustomer.Enabled = True

                    pnlAddEdit.Visible = True
                    'pnlCompanyAsset.Visible = True
                    'pnlCompanyCustomer.Visible = False
                    'pnlCompanyFinancial.Visible = True
                    pnlCompanyAsset.Visible = True
                    pnlCompanyCustomer.Visible = False
                    pnlCompanyFinancial.Visible = True

                    pnlPersonalAsset.Visible = True
                    pnlPersonalCustomer.Visible = True
                    pnlPersonalFinancial.Visible = True
                    pnlList.Visible = False
                    Exit Sub
                End If
            Next

            'check kalo weight di personalasset kosong
            Dim i_PersonalAsset As Integer
            For i_PersonalAsset = 0 To TotalRecord_PersonalAsset - 1
                Dim chkPersonalAsset As New CheckBox
                chkPersonalAsset = CType(dtgPersonalAsset.Items.Item(i_PersonalAsset).FindControl("chkComponent_PersonalAsset"), CheckBox)

                Dim txtWeightPersonalAsset As New TextBox
                txtWeightPersonalAsset = CType(dtgPersonalAsset.Items.Item(i_PersonalAsset).FindControl("txtWeight_PersonalAsset"), TextBox)

                If (chkPersonalAsset.Checked = True) And (txtWeightPersonalAsset.Text.Trim = "0") Then                    
                    ShowMessage(lblMessage, "Harap isi bobot untuk komponen yg dipilih dalam Personal Asset!", True)
                    'txtWeightPersonalAsset.ReadOnly = False
                    txtWeightPersonalAsset.Enabled = True

                    pnlAddEdit.Visible = True
                    'pnlCompanyAsset.Visible = True
                    'pnlCompanyCustomer.Visible = False
                    'pnlCompanyFinancial.Visible = True
                    pnlCompanyAsset.Visible = True
                    pnlCompanyCustomer.Visible = False
                    pnlCompanyFinancial.Visible = True
                    pnlPersonalAsset.Visible = True
                    pnlPersonalAsset.Visible = True
                    pnlPersonalFinancial.Visible = True
                    pnlList.Visible = False
                    Exit Sub
                End If
            Next

            'check kalo weight di PersonalFinancial kosong
            Dim i_PersonalFinancial As Integer
            For i_PersonalFinancial = 0 To TotalRecord_PersonalFinancial - 1
                Dim chkPersonalFinancial As New CheckBox
                chkPersonalFinancial = CType(dtgPersonalFinancial.Items.Item(i_PersonalFinancial).FindControl("chkComponent_PersonalFinancial"), CheckBox)

                Dim txtWeightPersonalFinancial As New TextBox
                txtWeightPersonalFinancial = CType(dtgPersonalFinancial.Items.Item(i_PersonalFinancial).FindControl("txtWeight_PersonalFinancial"), TextBox)

                If (chkPersonalFinancial.Checked = True) And (txtWeightPersonalFinancial.Text.Trim = "0") Then                    
                    ShowMessage(lblMessage, "Harap isi bobot untuk komponen yg dipilih dalam Personal Financial", True)
                    'txtWeightPersonalFinancial.ReadOnly = False
                    txtWeightPersonalFinancial.Enabled = True

                    pnlAddEdit.Visible = True
                    'pnlCompanyAsset.Visible = True
                    'pnlCompanyCustomer.Visible = False
                    'pnlCompanyFinancial.Visible = True
                    pnlCompanyAsset.Visible = True
                    pnlCompanyCustomer.Visible = False
                    pnlCompanyFinancial.Visible = True

                    pnlPersonalFinancial.Visible = True
                    pnlPersonalFinancial.Visible = True
                    pnlPersonalFinancial.Visible = True
                    pnlList.Visible = False
                    Exit Sub
                End If
            Next

            'check kalo weight di CompanyCustomer kosong
            Dim i_CompanyCustomer As Integer
            For i_CompanyCustomer = 0 To TotalRecord_CompanyCustomer - 1
                Dim chkCompanyCustomer As New CheckBox
                chkCompanyCustomer = CType(dtgCompanyCustomer.Items.Item(i_CompanyCustomer).FindControl("chkComponent_CompanyCustomer"), CheckBox)

                Dim txtWeightCompanyCustomer As New TextBox
                txtWeightCompanyCustomer = CType(dtgCompanyCustomer.Items.Item(i_CompanyCustomer).FindControl("txtWeight_CompanyCustomer"), TextBox)

                If (chkCompanyCustomer.Checked = True) And (txtWeightCompanyCustomer.Text.Trim = "0") Then                    
                    ShowMessage(lblMessage, "Harap isi bobot untuk komponen yg dipilih dalam corporate Customer", True)
                    'txtWeightCompanyCustomer.ReadOnly = False
                    txtWeightCompanyCustomer.Enabled = True

                    pnlAddEdit.Visible = True
                    'pnlCompanyAsset.Visible = True
                    'pnlCompanyCustomer.Visible = False
                    'pnlCompanyFinancial.Visible = True
                    pnlCompanyAsset.Visible = True
                    pnlCompanyCustomer.Visible = False
                    pnlCompanyFinancial.Visible = True

                    pnlCompanyCustomer.Visible = False
                    pnlCompanyCustomer.Visible = False
                    pnlPersonalFinancial.Visible = True
                    pnlList.Visible = False
                    Exit Sub
                End If
            Next

            'check kalo weight di CompanyAsset kosong
            Dim i_CompanyAsset As Integer
            For i_CompanyAsset = 0 To TotalRecord_CompanyAsset - 1
                Dim chkCompanyAsset As New CheckBox
                chkCompanyAsset = CType(dtgCompanyAsset.Items.Item(i_CompanyAsset).FindControl("chkComponent_CompanyAsset"), CheckBox)

                Dim txtWeightCompanyAsset As New TextBox
                txtWeightCompanyAsset = CType(dtgCompanyAsset.Items.Item(i_CompanyAsset).FindControl("txtWeight_CompanyAsset"), TextBox)

                If (chkCompanyAsset.Checked = True) And (txtWeightCompanyAsset.Text.Trim = "0") Then                    
                    ShowMessage(lblMessage, "Harap isi bobot untuk komponen yg dipilih dalam Corporate Asset", True)
                    'txtWeightCompanyAsset.ReadOnly = False
                    txtWeightCompanyAsset.Enabled = True

                    pnlAddEdit.Visible = True
                    'pnlCompanyAsset.Visible = True
                    'pnlCompanyCustomer.Visible = False
                    'pnlCompanyFinancial.Visible = False
                    pnlCompanyAsset.Visible = True
                    pnlCompanyCustomer.Visible = False
                    pnlCompanyFinancial.Visible = False

                    pnlCompanyAsset.Visible = False
                    pnlCompanyAsset.Visible = False
                    pnlPersonalFinancial.Visible = True
                    pnlList.Visible = False
                    Exit Sub
                End If
            Next

            'check kalo weight di CompanyFinancial kosong
            Dim i_CompanyFinancial As Integer
            For i_CompanyFinancial = 0 To TotalRecord_CompanyFinancial - 1
                Dim chkCompanyFinancial As New CheckBox
                chkCompanyFinancial = CType(dtgCompanyFinancial.Items.Item(i_CompanyFinancial).FindControl("chkComponent_CompanyFinancial"), CheckBox)

                Dim txtWeightCompanyFinancial As New TextBox
                txtWeightCompanyFinancial = CType(dtgCompanyFinancial.Items.Item(i_CompanyFinancial).FindControl("txtWeight_CompanyFinancial"), TextBox)

                If (chkCompanyFinancial.Checked = True) And (txtWeightCompanyFinancial.Text.Trim = "0") Then                    
                    ShowMessage(lblMessage, "Harap pilih Komponen yg dipilih dalam Corporate Financial", True)
                    'txtWeightCompanyFinancial.ReadOnly = False
                    txtWeightCompanyFinancial.Enabled = True

                    pnlAddEdit.Visible = True
                    'pnlCompanyAsset.Visible = False
                    'pnlCompanyCustomer.Visible = False
                    'pnlCompanyFinancial.Visible = False
                    pnlCompanyAsset.Visible = False
                    pnlCompanyCustomer.Visible = False
                    pnlCompanyFinancial.Visible = False

                    pnlCompanyFinancial.Visible = False
                    pnlCompanyFinancial.Visible = False
                    pnlPersonalFinancial.Visible = True
                    pnlList.Visible = False
                    Exit Sub
                End If
            Next

            Dim strTotal_PersonalCustomer As String = txtTotal_PersonalCustomer.Text.Trim
            If Not IsNumeric(strTotal_PersonalCustomer) Or strTotal_PersonalCustomer = "NaN" Then
                ShowMessage(lblMessage, "Harap isi dengan angka untuk bobot Personal Customer", True)
                ReCheck_Company()
                ReCheck_Personal()

                pnlAddEdit.Visible = True
                'pnlCompanyAsset.Visible = False
                'pnlCompanyCustomer.Visible = False
                'pnlCompanyFinancial.Visible = False
                pnlCompanyAsset.Visible = False
                pnlCompanyCustomer.Visible = False
                pnlCompanyFinancial.Visible = False

                pnlPersonalAsset.Visible = True
                pnlPersonalCustomer.Visible = True
                pnlPersonalFinancial.Visible = True
                pnlList.Visible = False
                Exit Sub
            End If

            Dim strTotal_PersonalAsset As String = txtTotal_PersonalAsset.Text.Trim
            If Not IsNumeric(strTotal_PersonalAsset) Or strTotal_PersonalAsset = "NaN" Then
                ShowMessage(lblMessage, "Harap isi dengan angka untuk bobot Personal Asset", True)
                ReCheck_Company()
                ReCheck_Personal()

                pnlAddEdit.Visible = True
                'pnlCompanyAsset.Visible = False
                'pnlCompanyCustomer.Visible = False
                'pnlCompanyFinancial.Visible = False
                pnlCompanyAsset.Visible = False
                pnlCompanyCustomer.Visible = False
                pnlCompanyFinancial.Visible = False

                pnlPersonalAsset.Visible = True
                pnlPersonalCustomer.Visible = True
                pnlPersonalFinancial.Visible = True
                pnlList.Visible = False
                Exit Sub
            End If

            Dim strTotal_PersonalFinancial As String = txtTotal_PersonalFinancial.Text.Trim
            If Not IsNumeric(strTotal_PersonalFinancial) Or strTotal_PersonalFinancial = "NaN" Then
                ShowMessage(lblMessage, "Harap isi dengan angka untuk bobot Personal Financial", True)
                ReCheck_Company()
                ReCheck_Personal()

                pnlAddEdit.Visible = True
                'pnlCompanyAsset.Visible = False
                'pnlCompanyCustomer.Visible = False
                'pnlCompanyFinancial.Visible = False
                pnlCompanyAsset.Visible = False
                pnlCompanyCustomer.Visible = False
                pnlCompanyFinancial.Visible = False

                pnlPersonalAsset.Visible = True
                pnlPersonalCustomer.Visible = True
                pnlPersonalFinancial.Visible = True
                pnlList.Visible = False
                Exit Sub
            End If

            'Dim strTotal_CompanyCustomer As String = txtTotal_CompanyCustomer.Text.Trim
            'If Not IsNumeric(strTotal_CompanyCustomer) Or strTotal_CompanyCustomer = "NaN" Then
            '    ShowMessage(lblMessage, "Harap isi dengan angka untuk bobot Corporate Customer", True)
            '    ReCheck_Company()
            '    ReCheck_Personal()

            '    pnlAddEdit.Visible = True
            '    pnlCompanyAsset.Visible = False
            '    pnlCompanyCustomer.Visible = False
            '    pnlCompanyFinancial.Visible = False
            '    pnlPersonalAsset.Visible = True
            '    pnlPersonalCustomer.Visible = True
            '    pnlPersonalFinancial.Visible = True
            '    pnlList.Visible = False
            '    Exit Sub
            'End If

            'Dim strTotal_CompanyAsset As String = txtTotal_CompanyAsset.Text.Trim
            'If Not IsNumeric(strTotal_CompanyAsset) Or strTotal_CompanyAsset = "NaN" Then
            '    ShowMessage(lblMessage, "Harap isi dengan angka untuk bobot Corporate Asset", True)
            '    ReCheck_Company()
            '    ReCheck_Personal()

            '    pnlAddEdit.Visible = True
            '    pnlCompanyAsset.Visible = False
            '    pnlCompanyCustomer.Visible = False
            '    pnlCompanyFinancial.Visible = False
            '    pnlPersonalAsset.Visible = True
            '    pnlPersonalCustomer.Visible = True
            '    pnlPersonalFinancial.Visible = True
            '    pnlList.Visible = False
            '    Exit Sub
            'End If

            'Dim strTotal_CompanyFinancial As String = txtTotal_CompanyFinancial.Text.Trim
            'If Not IsNumeric(strTotal_CompanyFinancial) Or strTotal_CompanyFinancial = "NaN" Then
            '    ShowMessage(lblMessage, "Harap isi dengan angka untuk bobot Corporate Financial", True)
            '    ReCheck_Company()
            '    ReCheck_Personal()

            '    pnlAddEdit.Visible = True
            '    pnlCompanyAsset.Visible = False
            '    pnlCompanyCustomer.Visible = False
            '    pnlCompanyFinancial.Visible = False
            '    pnlPersonalAsset.Visible = True
            '    pnlPersonalCustomer.Visible = True
            '    pnlPersonalFinancial.Visible = True
            '    pnlList.Visible = False
            '    Exit Sub
            'End If

            Dim strTotal_Personal As String
            strTotal_Personal = CStr(CInt(strTotal_PersonalAsset) + CInt(strTotal_PersonalCustomer) + CInt(strTotal_PersonalFinancial))

            'Dim strTotal_Company As String
            'strTotal_Company = CStr(CInt(strTotal_CompanyAsset) + CInt(strTotal_CompanyCustomer) + CInt(strTotal_CompanyFinancial))

            'If strTotal_Personal <> "100" Then
            '    ShowMessage(lblMessage, "Total Bobot untuk Personal Customer harus 100%", True)
            '    ReCheck_Company()
            '    ReCheck_Personal()

            '    pnlAddEdit.Visible = True
            '    pnlCompanyAsset.Visible = False
            '    pnlCompanyCustomer.Visible = False
            '    pnlCompanyFinancial.Visible = False
            '    pnlPersonalAsset.Visible = True
            '    pnlPersonalCustomer.Visible = True
            '    pnlPersonalFinancial.Visible = True
            '    pnlList.Visible = False
            '    Exit Sub
            'End If

            'If strTotal_Company <> "100" Then
            '    ShowMessage(lblMessage, "Total Bobot untuk Corporate Customer harus 100%", True)
            '    ReCheck_Company()
            '    ReCheck_Personal()

            '    pnlAddEdit.Visible = True
            '    pnlCompanyAsset.Visible = False
            '    pnlCompanyCustomer.Visible = False
            '    pnlCompanyFinancial.Visible = False
            '    pnlPersonalAsset.Visible = True
            '    pnlPersonalCustomer.Visible = True
            '    pnlPersonalFinancial.Visible = True
            '    pnlList.Visible = False
            '    Exit Sub
            'End If


            ' Bagian pengecekan untuk ApprovalScore yang tidak numeric dan yang melebihi 100
            If Not IsNumeric(txtApprovalScore_Company.Text.Trim) Then
                ShowMessage(lblMessage, "Score Approval untuk Corporate harus angka", True)
                ReCheck_Company()
                ReCheck_Personal()

                pnlAddEdit.Visible = True
                'pnlCompanyAsset.Visible = True
                'pnlCompanyCustomer.Visible = True
                'pnlCompanyFinancial.Visible = True
                pnlCompanyAsset.Visible = True
                pnlCompanyCustomer.Visible = True
                pnlCompanyFinancial.Visible = True

                pnlPersonalAsset.Visible = True
                pnlPersonalCustomer.Visible = True
                pnlPersonalFinancial.Visible = True
                pnlList.Visible = False
                Exit Sub
            ElseIf (CDbl(txtApprovalScore_Company.Text.Trim) > 100) Then
                ShowMessage(lblMessage, "Nilai Maximum untuk Score Approval Corporate adalah 100%", True)
                ReCheck_Company()
                ReCheck_Personal()

                pnlAddEdit.Visible = True
                'pnlCompanyAsset.Visible = True
                'pnlCompanyCustomer.Visible = True
                'pnlCompanyFinancial.Visible = True
                pnlCompanyAsset.Visible = True
                pnlCompanyCustomer.Visible = True
                pnlCompanyFinancial.Visible = True

                pnlPersonalAsset.Visible = True
                pnlPersonalCustomer.Visible = True
                pnlPersonalFinancial.Visible = True
                pnlList.Visible = False
                Exit Sub
            End If
            If Not IsNumeric(txtCutOffBefore.Text.Trim) Then
                ShowMessage(lblMessage, "Cut Off Score Before Survey harus angka", True)
                ReCheck_Company()
                ReCheck_Personal()

                pnlAddEdit.Visible = True
                'pnlCompanyAsset.Visible = True
                'pnlCompanyCustomer.Visible = True
                'pnlCompanyFinancial.Visible = True
                pnlCompanyAsset.Visible = True
                pnlCompanyCustomer.Visible = True
                pnlCompanyFinancial.Visible = True

                pnlPersonalAsset.Visible = True
                pnlPersonalCustomer.Visible = True
                pnlPersonalFinancial.Visible = True
                pnlList.Visible = False
                Exit Sub
            End If
            If Not IsNumeric(txtRejectScore_Company.Text.Trim) Then
                ShowMessage(lblMessage, "Score Reject untuk Corporate harus angka", True)
                ReCheck_Company()
                ReCheck_Personal()

                pnlAddEdit.Visible = True
                'pnlCompanyAsset.Visible = True
                'pnlCompanyCustomer.Visible = True
                'pnlCompanyFinancial.Visible = True
                pnlCompanyAsset.Visible = True
                pnlCompanyCustomer.Visible = True
                pnlCompanyFinancial.Visible = True

                pnlPersonalAsset.Visible = True
                pnlPersonalCustomer.Visible = True
                pnlPersonalFinancial.Visible = True
                pnlList.Visible = False
                Exit Sub
            ElseIf (CDbl(txtRejectScore_Company.Text.Trim) > 100) Then
                ShowMessage(lblMessage, "Nilai Maximum untuk Score Reject Corporate adalah 100%", True)
                ReCheck_Company()
                ReCheck_Personal()

                pnlAddEdit.Visible = True
                'pnlCompanyAsset.Visible = True
                'pnlCompanyCustomer.Visible = True
                'pnlCompanyFinancial.Visible = True
                pnlCompanyAsset.Visible = True
                pnlCompanyCustomer.Visible = True
                pnlCompanyFinancial.Visible = True

                pnlPersonalAsset.Visible = True
                pnlPersonalCustomer.Visible = True
                pnlPersonalFinancial.Visible = True
                pnlList.Visible = False
                Exit Sub
            End If

            If Not IsNumeric(txtApprovalScore_Personal.Text.Trim) Then
                Me.CmdWhere = "ALL"
                'BindGridEntity_CompanyAsset(Me.CmdWhere)
                'BindGridEntity_CompanyCustomer(Me.CmdWhere)
                'BindGridEntity_CompanyFinancial(Me.CmdWhere)
                BindGridEntity_PersonalAsset(Me.CmdWhere)
                BindGridEntity_PersonalCustomer(Me.CmdWhere)
                BindGridEntity_PersonalFinancial(Me.CmdWhere)

                ShowMessage(lblMessage, "Score Approval untuk Personal Customer harus angka", True)
                ReCheck_Company()
                ReCheck_Personal()

                pnlAddEdit.Visible = True
                pnlCompanyAsset.Visible = False
                pnlCompanyCustomer.Visible = False
                pnlCompanyFinancial.Visible = False
                pnlPersonalAsset.Visible = True
                pnlPersonalCustomer.Visible = True
                pnlPersonalFinancial.Visible = True
                pnlList.Visible = False
                Exit Sub
            ElseIf (CDbl(txtApprovalScore_Personal.Text.Trim) > 100) Then
                Me.CmdWhere = "ALL"
                'BindGridEntity_CompanyAsset(Me.CmdWhere)
                'BindGridEntity_CompanyCustomer(Me.CmdWhere)
                'BindGridEntity_CompanyFinancial(Me.CmdWhere)
                BindGridEntity_PersonalAsset(Me.CmdWhere)
                BindGridEntity_PersonalCustomer(Me.CmdWhere)
                BindGridEntity_PersonalFinancial(Me.CmdWhere)

                ShowMessage(lblMessage, "Nilai Maximum untuk Score Approval Personal Customer adalah 100%", True)
                ReCheck_Company()
                ReCheck_Personal()

                pnlAddEdit.Visible = True
                pnlCompanyAsset.Visible = False
                pnlCompanyCustomer.Visible = False
                pnlCompanyFinancial.Visible = False
                pnlPersonalAsset.Visible = True
                pnlPersonalCustomer.Visible = True
                pnlPersonalFinancial.Visible = True
                pnlList.Visible = False
                Exit Sub
            End If

            If Not IsNumeric(txtRejectScore_Personal.Text.Trim) Then
                Me.CmdWhere = "ALL"
                'BindGridEntity_CompanyAsset(Me.CmdWhere)
                'BindGridEntity_CompanyCustomer(Me.CmdWhere)
                'BindGridEntity_CompanyFinancial(Me.CmdWhere)
                BindGridEntity_PersonalAsset(Me.CmdWhere)
                BindGridEntity_PersonalCustomer(Me.CmdWhere)
                BindGridEntity_PersonalFinancial(Me.CmdWhere)

                ShowMessage(lblMessage, "Score Reject untuk Personal Customer harus angka", True)
                ReCheck_Company()
                ReCheck_Personal()

                pnlAddEdit.Visible = True
                'pnlCompanyAsset.Visible = True
                'pnlCompanyCustomer.Visible = True
                'pnlCompanyFinancial.Visible = True
                pnlCompanyAsset.Visible = True
                pnlCompanyCustomer.Visible = True
                pnlCompanyFinancial.Visible = True

                pnlPersonalAsset.Visible = True
                pnlPersonalCustomer.Visible = True
                pnlPersonalFinancial.Visible = True
                pnlList.Visible = False
                Exit Sub
            ElseIf (CDbl(txtRejectScore_Personal.Text.Trim) > 100) Then
                Me.CmdWhere = "ALL"
                'BindGridEntity_CompanyAsset(Me.CmdWhere)
                'BindGridEntity_CompanyCustomer(Me.CmdWhere)
                'BindGridEntity_CompanyFinancial(Me.CmdWhere)
                BindGridEntity_PersonalAsset(Me.CmdWhere)
                BindGridEntity_PersonalCustomer(Me.CmdWhere)
                BindGridEntity_PersonalFinancial(Me.CmdWhere)

                ShowMessage(lblMessage, "Nilai Maximum untuk Score Reject Personal Customer adalah 100%", True)
                ReCheck_Company()
                ReCheck_Personal()

                pnlAddEdit.Visible = True
                'pnlCompanyAsset.Visible = True
                'pnlCompanyCustomer.Visible = True
                'pnlCompanyFinancial.Visible = True
                pnlCompanyAsset.Visible = True
                pnlCompanyCustomer.Visible = True
                pnlCompanyFinancial.Visible = True

                pnlPersonalAsset.Visible = True
                pnlPersonalCustomer.Visible = True
                pnlPersonalFinancial.Visible = True
                pnlList.Visible = False
                Exit Sub
            End If


            'Personal Customer
            Dim oTable_PersonalCustomer As New DataTable

            oTable_PersonalCustomer.Columns.Add("CreditScoreSchemeID", GetType(String))
            oTable_PersonalCustomer.Columns.Add("CreditScoreComponentID", GetType(String))
            oTable_PersonalCustomer.Columns.Add("Weight", GetType(String))
            oTable_PersonalCustomer.Columns.Add("IsChecked", GetType(Boolean))
            oTable_PersonalCustomer.Columns.Add("PersonalApprovedScore", GetType(Double))
            oTable_PersonalCustomer.Columns.Add("PersonalRejectScore", GetType(Double))
            oTable_PersonalCustomer.Columns.Add("CompanyApprovedScore", GetType(Double))
            oTable_PersonalCustomer.Columns.Add("CompanyRejectScore", GetType(Double))
            oTable_PersonalCustomer.Columns.Add("CustOffScore", GetType(Double))

            Dim counter_PersonalCustomer As Integer
            Dim lObjDataRow_PersonalCustomer As DataRow
            For counter_PersonalCustomer = 0 To TotalRecord_PersonalCustomer - 1
                Dim chkPersonalCustomer As New CheckBox
                chkPersonalCustomer = CType(dtgPersonalCustomer.Items.Item(counter_PersonalCustomer).FindControl("chkComponent_PersonalCustomer"), CheckBox)

                Dim txtWeightPersonalCustomer As New TextBox
                txtWeightPersonalCustomer = CType(dtgPersonalCustomer.Items.Item(counter_PersonalCustomer).FindControl("txtWeight_PersonalCustomer"), TextBox)

                Dim lblCreditScoreComponentID As New Label
                lblCreditScoreComponentID = CType(dtgPersonalCustomer.Items.Item(counter_PersonalCustomer).FindControl("lblCSC_PersonalCustomer"), Label)

                lObjDataRow_PersonalCustomer = oTable_PersonalCustomer.NewRow()
                lObjDataRow_PersonalCustomer("CreditScoreSchemeID") = lblSchemeID.Text.Trim
                lObjDataRow_PersonalCustomer("CreditScoreComponentID") = lblCreditScoreComponentID.Text.Trim

                If txtWeightPersonalCustomer.Text.Trim = "" Then
                    lObjDataRow_PersonalCustomer("Weight") = "0"
                Else
                    lObjDataRow_PersonalCustomer("Weight") = txtWeightPersonalCustomer.Text.Trim
                End If

                lObjDataRow_PersonalCustomer("IsChecked") = chkPersonalCustomer.Checked
                lObjDataRow_PersonalCustomer("PersonalApprovedScore") = CDbl(txtApprovalScore_Personal.Text.Trim)
                lObjDataRow_PersonalCustomer("PersonalRejectScore") = CDbl(txtRejectScore_Personal.Text.Trim)
                lObjDataRow_PersonalCustomer("CompanyApprovedScore") = CDbl(txtApprovalScore_Company.Text.Trim)
                lObjDataRow_PersonalCustomer("CompanyRejectScore") = CDbl(txtRejectScore_Company.Text.Trim)
                lObjDataRow_PersonalCustomer("CustOffScore") = CDbl(txtCutOffBefore.Text.Trim)

                oTable_PersonalCustomer.Rows.Add(lObjDataRow_PersonalCustomer)
            Next

            'Personal Asset
            Dim oTable_PersonalAsset As New DataTable

            oTable_PersonalAsset.Columns.Add("CreditScoreSchemeID", GetType(String))
            oTable_PersonalAsset.Columns.Add("CreditScoreComponentID", GetType(String))
            oTable_PersonalAsset.Columns.Add("Weight", GetType(String))
            oTable_PersonalAsset.Columns.Add("IsChecked", GetType(Boolean))
            oTable_PersonalAsset.Columns.Add("PersonalApprovedScore", GetType(Double))
            oTable_PersonalAsset.Columns.Add("PersonalRejectScore", GetType(Double))
            oTable_PersonalAsset.Columns.Add("CompanyApprovedScore", GetType(Double))
            oTable_PersonalAsset.Columns.Add("CompanyRejectScore", GetType(Double))
            oTable_PersonalAsset.Columns.Add("CustOffScore", GetType(Double))



            Dim counter_PersonalAsset As Integer
            Dim lObjDataRow_PersonalAsset As DataRow
            For counter_PersonalAsset = 0 To TotalRecord_PersonalAsset - 1
                Dim chkPersonalAsset As New CheckBox
                chkPersonalAsset = CType(dtgPersonalAsset.Items.Item(counter_PersonalAsset).FindControl("chkComponent_PersonalAsset"), CheckBox)

                Dim txtWeightPersonalAsset As New TextBox
                txtWeightPersonalAsset = CType(dtgPersonalAsset.Items.Item(counter_PersonalAsset).FindControl("txtWeight_PersonalAsset"), TextBox)

                Dim lblCreditScoreComponentID As New Label
                lblCreditScoreComponentID = CType(dtgPersonalAsset.Items.Item(counter_PersonalAsset).FindControl("lblCSC_PersonalAsset"), Label)


                lObjDataRow_PersonalAsset = oTable_PersonalAsset.NewRow()
                lObjDataRow_PersonalAsset("CreditScoreSchemeID") = lblSchemeID.Text.Trim
                lObjDataRow_PersonalAsset("CreditScoreComponentID") = lblCreditScoreComponentID.Text.Trim

                If txtWeightPersonalAsset.Text.Trim = "" Then
                    lObjDataRow_PersonalAsset("Weight") = "0"
                Else
                    lObjDataRow_PersonalAsset("Weight") = txtWeightPersonalAsset.Text.Trim
                End If

                lObjDataRow_PersonalAsset("IsChecked") = chkPersonalAsset.Checked
                lObjDataRow_PersonalAsset("PersonalApprovedScore") = CDbl(txtApprovalScore_Personal.Text.Trim)
                lObjDataRow_PersonalAsset("PersonalRejectScore") = CDbl(txtRejectScore_Personal.Text.Trim)
                lObjDataRow_PersonalAsset("CompanyApprovedScore") = CDbl(txtApprovalScore_Company.Text.Trim)
                lObjDataRow_PersonalAsset("CompanyRejectScore") = CDbl(txtRejectScore_Company.Text.Trim)
                lObjDataRow_PersonalAsset("CustOffScore") = CDbl(txtCutOffBefore.Text.Trim)

                oTable_PersonalAsset.Rows.Add(lObjDataRow_PersonalAsset)
            Next

            'Personal Financial
            Dim oTable_PersonalFinancial As New DataTable

            oTable_PersonalFinancial.Columns.Add("CreditScoreSchemeID", GetType(String))
            oTable_PersonalFinancial.Columns.Add("CreditScoreComponentID", GetType(String))
            oTable_PersonalFinancial.Columns.Add("Weight", GetType(String))
            oTable_PersonalFinancial.Columns.Add("IsChecked", GetType(Boolean))
            oTable_PersonalFinancial.Columns.Add("PersonalApprovedScore", GetType(Double))
            oTable_PersonalFinancial.Columns.Add("PersonalRejectScore", GetType(Double))
            oTable_PersonalFinancial.Columns.Add("CompanyApprovedScore", GetType(Double))
            oTable_PersonalFinancial.Columns.Add("CompanyRejectScore", GetType(Double))

            oTable_PersonalFinancial.Columns.Add("CustOffScore", GetType(Double))

            Dim counter_PersonalFinancial As Integer
            Dim lObjDataRow_PersonalFinancial As DataRow
            For counter_PersonalFinancial = 0 To TotalRecord_PersonalFinancial - 1
                Dim chkPersonalFinancial As New CheckBox
                chkPersonalFinancial = CType(dtgPersonalFinancial.Items.Item(counter_PersonalFinancial).FindControl("chkComponent_PersonalFinancial"), CheckBox)

                Dim txtWeightPersonalFinancial As New TextBox
                txtWeightPersonalFinancial = CType(dtgPersonalFinancial.Items.Item(counter_PersonalFinancial).FindControl("txtWeight_PersonalFinancial"), TextBox)

                Dim lblCreditScoreComponentID As New Label
                lblCreditScoreComponentID = CType(dtgPersonalFinancial.Items.Item(counter_PersonalFinancial).FindControl("lblCSC_PersonalFinancial"), Label)


                lObjDataRow_PersonalFinancial = oTable_PersonalFinancial.NewRow()
                lObjDataRow_PersonalFinancial("CreditScoreSchemeID") = lblSchemeID.Text.Trim
                lObjDataRow_PersonalFinancial("CreditScoreComponentID") = lblCreditScoreComponentID.Text.Trim

                If txtWeightPersonalFinancial.Text.Trim = "" Then
                    lObjDataRow_PersonalFinancial("Weight") = "0"
                Else
                    lObjDataRow_PersonalFinancial("Weight") = txtWeightPersonalFinancial.Text.Trim
                End If

                lObjDataRow_PersonalFinancial("IsChecked") = chkPersonalFinancial.Checked
                lObjDataRow_PersonalFinancial("PersonalApprovedScore") = CDbl(txtApprovalScore_Personal.Text.Trim)
                lObjDataRow_PersonalFinancial("PersonalRejectScore") = CDbl(txtRejectScore_Personal.Text.Trim)
                lObjDataRow_PersonalFinancial("CompanyApprovedScore") = CDbl(txtApprovalScore_Company.Text.Trim)
                lObjDataRow_PersonalFinancial("CompanyRejectScore") = CDbl(txtRejectScore_Company.Text.Trim)
                lObjDataRow_PersonalFinancial("CustOffScore") = CDbl(txtCutOffBefore.Text.Trim)

                oTable_PersonalFinancial.Rows.Add(lObjDataRow_PersonalFinancial)
            Next


            'CompanyCustomer
            Dim oTable_CompanyCustomer As New DataTable

            oTable_CompanyCustomer.Columns.Add("CreditScoreSchemeID", GetType(String))
            oTable_CompanyCustomer.Columns.Add("CreditScoreComponentID", GetType(String))
            oTable_CompanyCustomer.Columns.Add("Weight", GetType(String))
            oTable_CompanyCustomer.Columns.Add("IsChecked", GetType(Boolean))
            oTable_CompanyCustomer.Columns.Add("PersonalApprovedScore", GetType(Double))
            oTable_CompanyCustomer.Columns.Add("PersonalRejectScore", GetType(Double))
            oTable_CompanyCustomer.Columns.Add("CompanyApprovedScore", GetType(Double))
            oTable_CompanyCustomer.Columns.Add("CompanyRejectScore", GetType(Double))

            oTable_CompanyCustomer.Columns.Add("CustOffScore", GetType(Double))

            Dim counter_CompanyCustomer As Integer
            Dim lObjDataRow_CompanyCustomer As DataRow
            For counter_CompanyCustomer = 0 To TotalRecord_CompanyCustomer - 1
                Dim chkCompanyCustomer As New CheckBox
                chkCompanyCustomer = CType(dtgCompanyCustomer.Items.Item(counter_CompanyCustomer).FindControl("chkComponent_CompanyCustomer"), CheckBox)

                Dim txtWeightCompanyCustomer As New TextBox
                txtWeightCompanyCustomer = CType(dtgCompanyCustomer.Items.Item(counter_CompanyCustomer).FindControl("txtWeight_CompanyCustomer"), TextBox)

                Dim lblCreditScoreComponentID As New Label
                lblCreditScoreComponentID = CType(dtgCompanyCustomer.Items.Item(counter_CompanyCustomer).FindControl("lblCSC_CompanyCustomer"), Label)

                lObjDataRow_CompanyCustomer = oTable_CompanyCustomer.NewRow()
                lObjDataRow_CompanyCustomer("CreditScoreSchemeID") = lblSchemeID.Text.Trim
                lObjDataRow_CompanyCustomer("CreditScoreComponentID") = lblCreditScoreComponentID.Text.Trim

                If txtWeightCompanyCustomer.Text.Trim = "" Then
                    lObjDataRow_CompanyCustomer("Weight") = "0"
                Else
                    lObjDataRow_CompanyCustomer("Weight") = txtWeightCompanyCustomer.Text.Trim
                End If

                lObjDataRow_CompanyCustomer("IsChecked") = chkCompanyCustomer.Checked
                lObjDataRow_CompanyCustomer("PersonalApprovedScore") = CDbl(txtApprovalScore_Personal.Text.Trim)
                lObjDataRow_CompanyCustomer("PersonalRejectScore") = CDbl(txtRejectScore_Personal.Text.Trim)
                lObjDataRow_CompanyCustomer("CompanyApprovedScore") = CDbl(txtApprovalScore_Company.Text.Trim)
                lObjDataRow_CompanyCustomer("CompanyRejectScore") = CDbl(txtRejectScore_Company.Text.Trim)


                oTable_CompanyCustomer.Rows.Add(lObjDataRow_CompanyCustomer)
            Next

            'CompanyAsset
            Dim oTable_CompanyAsset As New DataTable

            oTable_CompanyAsset.Columns.Add("CreditScoreSchemeID", GetType(String))
            oTable_CompanyAsset.Columns.Add("CreditScoreComponentID", GetType(String))
            oTable_CompanyAsset.Columns.Add("Weight", GetType(String))
            oTable_CompanyAsset.Columns.Add("IsChecked", GetType(Boolean))
            oTable_CompanyAsset.Columns.Add("PersonalApprovedScore", GetType(Double))
            oTable_CompanyAsset.Columns.Add("PersonalRejectScore", GetType(Double))
            oTable_CompanyAsset.Columns.Add("CompanyApprovedScore", GetType(Double))
            oTable_CompanyAsset.Columns.Add("CompanyRejectScore", GetType(Double))

            Dim counter_CompanyAsset As Integer
            Dim lObjDataRow_CompanyAsset As DataRow
            For counter_CompanyAsset = 0 To TotalRecord_CompanyAsset - 1
                Dim chkCompanyAsset As New CheckBox
                chkCompanyAsset = CType(dtgCompanyAsset.Items.Item(counter_CompanyAsset).FindControl("chkComponent_CompanyAsset"), CheckBox)

                Dim txtWeightCompanyAsset As New TextBox
                txtWeightCompanyAsset = CType(dtgCompanyAsset.Items.Item(counter_CompanyAsset).FindControl("txtWeight_CompanyAsset"), TextBox)

                Dim lblCreditScoreComponentID As New Label
                lblCreditScoreComponentID = CType(dtgCompanyAsset.Items.Item(counter_CompanyAsset).FindControl("lblCSC_CompanyAsset"), Label)


                lObjDataRow_CompanyAsset = oTable_CompanyAsset.NewRow()
                lObjDataRow_CompanyAsset("CreditScoreSchemeID") = lblSchemeID.Text.Trim
                lObjDataRow_CompanyAsset("CreditScoreComponentID") = lblCreditScoreComponentID.Text.Trim

                If txtWeightCompanyAsset.Text.Trim = "" Then
                    lObjDataRow_CompanyAsset("Weight") = "0"
                Else
                    lObjDataRow_CompanyAsset("Weight") = txtWeightCompanyAsset.Text.Trim
                End If

                lObjDataRow_CompanyAsset("IsChecked") = chkCompanyAsset.Checked
                lObjDataRow_CompanyAsset("PersonalApprovedScore") = CDbl(txtApprovalScore_Personal.Text.Trim)
                lObjDataRow_CompanyAsset("PersonalRejectScore") = CDbl(txtRejectScore_Personal.Text.Trim)
                lObjDataRow_CompanyAsset("CompanyApprovedScore") = CDbl(txtApprovalScore_Company.Text.Trim)
                lObjDataRow_CompanyAsset("CompanyRejectScore") = CDbl(txtRejectScore_Company.Text.Trim)

                oTable_CompanyAsset.Rows.Add(lObjDataRow_CompanyAsset)
            Next


            'CompanyFinancial
            Dim oTable_CompanyFinancial As New DataTable

            oTable_CompanyFinancial.Columns.Add("CreditScoreSchemeID", GetType(String))
            oTable_CompanyFinancial.Columns.Add("CreditScoreComponentID", GetType(String))
            oTable_CompanyFinancial.Columns.Add("Weight", GetType(String))
            oTable_CompanyFinancial.Columns.Add("IsChecked", GetType(Boolean))
            oTable_CompanyFinancial.Columns.Add("PersonalApprovedScore", GetType(Double))
            oTable_CompanyFinancial.Columns.Add("PersonalRejectScore", GetType(Double))
            oTable_CompanyFinancial.Columns.Add("CompanyApprovedScore", GetType(Double))
            oTable_CompanyFinancial.Columns.Add("CompanyRejectScore", GetType(Double))

            Dim counter_CompanyFinancial As Integer
            Dim lObjDataRow_CompanyFinancial As DataRow
            For counter_CompanyFinancial = 0 To TotalRecord_CompanyFinancial - 1
                Dim chkCompanyFinancial As New CheckBox
                chkCompanyFinancial = CType(dtgCompanyFinancial.Items.Item(counter_CompanyFinancial).FindControl("chkComponent_CompanyFinancial"), CheckBox)

                Dim txtWeightCompanyFinancial As New TextBox
                txtWeightCompanyFinancial = CType(dtgCompanyFinancial.Items.Item(counter_CompanyFinancial).FindControl("txtWeight_CompanyFinancial"), TextBox)

                Dim lblCreditScoreComponentID As New Label
                lblCreditScoreComponentID = CType(dtgCompanyFinancial.Items.Item(counter_CompanyFinancial).FindControl("lblCSC_CompanyFinancial"), Label)


                lObjDataRow_CompanyFinancial = oTable_CompanyFinancial.NewRow()
                lObjDataRow_CompanyFinancial("CreditScoreSchemeID") = lblSchemeID.Text.Trim
                lObjDataRow_CompanyFinancial("CreditScoreComponentID") = lblCreditScoreComponentID.Text.Trim

                If txtWeightCompanyFinancial.Text.Trim = "" Then
                    lObjDataRow_CompanyFinancial("Weight") = "0"
                Else
                    lObjDataRow_CompanyFinancial("Weight") = txtWeightCompanyFinancial.Text.Trim
                End If

                lObjDataRow_CompanyFinancial("IsChecked") = chkCompanyFinancial.Checked
                lObjDataRow_CompanyFinancial("PersonalApprovedScore") = CDbl(txtApprovalScore_Personal.Text.Trim)
                lObjDataRow_CompanyFinancial("PersonalRejectScore") = CDbl(txtRejectScore_Personal.Text.Trim)
                lObjDataRow_CompanyFinancial("CompanyApprovedScore") = CDbl(txtApprovalScore_Company.Text.Trim)
                lObjDataRow_CompanyFinancial("CompanyRejectScore") = CDbl(txtRejectScore_Company.Text.Trim)

                oTable_CompanyFinancial.Rows.Add(lObjDataRow_CompanyFinancial)
            Next


            If Me.AddEdit = "ADD" Then
                With customClass
                    .CreditScoreShemeId = txtSchemeID.Text.Trim
                    .Description = txtDescription.Text
                    .PersonalApprovedScore = CDbl(txtApprovalScore_Personal.Text.Trim)
                    .PersonalRejectScore = CDbl(txtRejectScore_Personal.Text.Trim)
                    .CompanyApprovedScore = CDbl(txtApprovalScore_Company.Text.Trim)
                    .CompanyRejectScore = CDbl(txtRejectScore_Company.Text.Trim)
                    .strConnection = GetConnectionString()
                End With
            ElseIf Me.AddEdit = "EDIT" Then
                With customClass
                    .CreditScoreShemeId = lblSchemeID.Text.Trim
                    .Description = txtDescription.Text
                    .PersonalApprovedScore = CDbl(txtApprovalScore_Personal.Text.Trim)
                    .PersonalRejectScore = CDbl(txtRejectScore_Personal.Text.Trim)
                    .CompanyApprovedScore = CDbl(txtApprovalScore_Company.Text.Trim)
                    .CompanyRejectScore = CDbl(txtRejectScore_Company.Text.Trim)
                    .strConnection = GetConnectionString()
                End With
            End If

            If Me.AddEdit = "ADD" Then
                txtSearch.Text = ""
                Dim customerClass As New Parameter.CreditScoringMain
                With customClass
                    .dtCompanyAsset = oTable_CompanyAsset
                    .dtCompanyCustomer = oTable_CompanyCustomer
                    .dtCompanyFinancial = oTable_CompanyFinancial
                    .dtPersonalAsset = oTable_PersonalAsset
                    .dtPersonalCustomer = oTable_PersonalCustomer
                    .dtPersonalFinancial = oTable_PersonalFinancial
                    .Description = txtDescription.Text
                    .CutOffBefore = txtCutOffBefore.Text
                    .strConnection = GetConnectionString()
                    .SPName = "spCreditScoringCardSaveAdd"
                End With
                Try
                    m_controller.CreditScoringMainSaveAdd(customClass)
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_SUCCESS, False)
                    pnlAddEdit.Visible = False
                    pnlList.Visible = True
                    Me.CmdWhere = "ALL"
                    SaveGrade("Add")
                    BindGridEntity_List(Me.CmdWhere)
                Catch exp As Exception
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_FAILED, True)
                    pnlAddEdit.Visible = True
                    pnlList.Visible = False
                    'pnlCompanyAsset.Visible = True
                    'pnlCompanyCustomer.Visible = True
                    'pnlCompanyFinancial.Visible = True
                    pnlCompanyAsset.Visible = True
                    pnlCompanyCustomer.Visible = True
                    pnlCompanyFinancial.Visible = True

                    pnlPersonalAsset.Visible = True
                    pnlPersonalCustomer.Visible = True
                    pnlPersonalFinancial.Visible = True
                End Try
            ElseIf Me.AddEdit = "EDIT" Then
                txtSearch.Text = ""
                Dim customerClass As New Parameter.CreditScoringMain
                With customClass
                    .dtCompanyAsset = oTable_CompanyAsset
                    .dtCompanyCustomer = oTable_CompanyCustomer
                    .dtCompanyFinancial = oTable_CompanyFinancial
                    .dtPersonalAsset = oTable_PersonalAsset
                    .dtPersonalCustomer = oTable_PersonalCustomer
                    .dtPersonalFinancial = oTable_PersonalFinancial
                    .Description = txtDescription.Text
                    .strConnection = GetConnectionString()
                    .SPName = "spCreditScoringCardSaveEdit"
                End With

                m_controller.CreditScoringMainSaveEdit(customClass)
                ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
                pnlAddEdit.Visible = False
                pnlList.Visible = True
                Me.CmdWhere = "ALL"
                SaveGrade("Edit")
                BindGridEntity_List(Me.CmdWhere)
            End If
        Else
            ShowMessage(lblMessage, "Range Grade ada yang salah", True)
        End If
        txtPage.Text = "1"
    End Sub
    Sub SaveGrade(ByVal status As String)
        Dim PopulateGradeList As New List(Of Parameter.CreditScoreGradeMaster)
        Dim GetGradeList As New List(Of Parameter.CreditScoreGradeMaster)
        Dim oGrade As New Parameter.CreditScoreGradeMaster
        PopulateGrade(PopulateGradeList)
        oGrade.CreditScoreSchemeID = Me.CreditScoreSchemeId
        oGrade.strConnection = GetConnectionString()
        GetGradeList = m_Grade.GetCreditScoreGradeMasterList(oGrade)

        If GetGradeList.Count = 0 Then
            For i As Integer = 0 To PopulateGradeList.Count - 1
                oGrade = New Parameter.CreditScoreGradeMaster
                oGrade = PopulateGradeList(i)
                oGrade.CreditScoreSchemeID = Me.CreditScoreSchemeId
                oGrade.strConnection = GetConnectionString()
                m_Grade.CreditScoreGradeMasterSaveAdd(oGrade)
            Next
        ElseIf GetGradeList.Count > 1 Then
            For i As Integer = 0 To PopulateGradeList.Count - 1
                oGrade = New Parameter.CreditScoreGradeMaster
                oGrade = PopulateGradeList(i)
                oGrade.CreditScoreSchemeID = Me.CreditScoreSchemeId
                oGrade.strConnection = GetConnectionString()
                m_Grade.CreditScoreGradeMasterSaveEdit(oGrade)
            Next
        Else
            If status = "Delete" Then
                oGrade = New Parameter.CreditScoreGradeMaster
                oGrade.CreditScoreSchemeID = Me.CreditScoreSchemeId
                oGrade.strConnection = GetConnectionString()
                m_Grade.CreditScoreGradeMasterDelete(oGrade)
            End If
        End If
    End Sub
#End Region

#Region "Add"
    Private Sub imbAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonAdd.Click
        If CheckFeature(Me.Loginid, Me.FormID, "ADD", Me.AppId) Then
            pnlList.Visible = False
            pnlAddEdit.Visible = True

            pnlTotal_Company.Visible = True
            pnlTotal_Personal.Visible = True

            pnlCompanyAsset.Visible = True
            pnlCompanyCustomer.Visible = True
            pnlCompanyFinancial.Visible = True

            pnlPersonalAsset.Visible = True
            pnlPersonalCustomer.Visible = True
            pnlPersonalFinancial.Visible = True
            lblRequiredSchemeID.Visible = True

            Me.AddEdit = "ADD"
            If Me.CmdWhere <> "ALL" Then
                Me.CmdWhere = "ALL"
            End If

            'BindGridEntity_CompanyAsset(Me.CmdWhere)
            'BindGridEntity_CompanyCustomer(Me.CmdWhere)
            'BindGridEntity_CompanyFinancial(Me.CmdWhere)
            BindGridEntity_PersonalAsset(Me.CmdWhere)
            BindGridEntity_PersonalCustomer(Me.CmdWhere)
            BindGridEntity_PersonalFinancial(Me.CmdWhere)
            txtApprovalScore_Company.Text = "1"
            txtApprovalScore_Personal.Text = "1"
            txtRejectScore_Company.Text = "1"
            txtRejectScore_Personal.Text = "1"
            txtCutOffBefore.Text = "1"
            lblGrandTotal_Company.Text = "0 %"
            lblGrandTotal_Personal.Text = "0 %"

            ButtonCancel.Visible = True
            ButtonSave.Visible = True

            lblTitleAddEdit.Text = Me.AddEdit

            txtSchemeID.Text = ""
            txtSchemeID.Visible = True
            txtDescription.Text = ""
            txtDescription.Visible = True
            lblSchemeID.Visible = False
        End If
    End Sub
#End Region



#Region "SendCookies"
    Sub SendCookies()
        Dim cookie As HttpCookie = Request.Cookies("CreditScoringMain")

        If txtSearch.Text.Trim <> "" Then
            Me.CmdWhere = "a." + cboSearch.SelectedItem.Value + " = '" + txtSearch.Text.Trim + "'"
        Else
            Me.CmdWhere = "ALL"
        End If

        If Not cookie Is Nothing Then
            cookie.Values("PageFrom") = "CreditScoringMain"
            cookie.Values("CmdWhere") = Me.CmdWhere
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie("CreditScoringMain")
            cookieNew.Values.Add("PageFrom", "CreditScoringMain")
            cookieNew.Values.Add("CmdWhere", Me.CmdWhere)
            Response.AppendCookie(cookieNew)
        End If
    End Sub
#End Region

#Region "Reset"
    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        cboSearch.SelectedIndex = 0
        txtSearch.Text = ""
        Me.CmdWhere = "ALL"
        BindGridEntity_List(Me.CmdWhere)
        txtPage.Text = "1"
    End Sub
#End Region

#Region "imbSearch"
    Private Sub imbSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonSearch.Click
        If txtSearch.Text.Trim <> "" Then
            Me.CmdWhere = cboSearch.SelectedItem.Value + " LIKE '%" + txtSearch.Text.Trim + "%'"
        Else
            Me.CmdWhere = "ALL"
        End If
        BindGridEntity_List(Me.CmdWhere)
    End Sub
#End Region

#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidakditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridEntity_List(ViewState("vwsCmdWhere").ToString)
    End Sub

    Private Sub imgbtnPageNumb_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int32)
                BindGridEntity_List(ViewState("vwsCmdWhere").ToString)
            End If
        End If
    End Sub

#End Region

#Region "InitialDefaultPanel"
    Private Sub InitialDefaultPanel()
        pnlList.Visible = True
        pnlAddEdit.Visible = False
    End Sub
#End Region

#Region "ButtonCancel"
    Private Sub ButtonCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        InitialDefaultPanel()
        txtSearch.Text = ""
        Me.CmdWhere = "ALL"
        BindGridEntity_List(Me.CmdWhere)
        txtPage.Text = "1"
    End Sub
#End Region

#Region " Sorting "
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgCreditScoring_Main.SortCommand
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        BindGridEntity_List(Me.CmdWhere)
    End Sub
#End Region

#Region "dtg_ItemDataBound"
    Private Sub dtgCreditScoring_Main_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgCreditScoring_Main.ItemDataBound
        Dim imbDelete As ImageButton
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imbDelete"), ImageButton)
            imbDelete.Attributes.Add("Onclick", "return DeleteConfirm()")
        End If
    End Sub

    Private Sub dtgCompanyAsset_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgCompanyAsset.ItemDataBound
        Dim WeightCompanyAsset As New TextBox
        Dim TotalWeightCompanyAsset As New TextBox

        If e.Item.ItemIndex >= 0 Then
            WeightCompanyAsset = CType(e.Item.FindControl("txtWeight_CompanyAsset"), TextBox)
            Temp_TotalWeight_CompanyAsset += CDbl(WeightCompanyAsset.Text.Trim)
        End If

        txtTotal_CompanyAsset.Text = CStr(Temp_TotalWeight_CompanyAsset)
        Me.Total_CompanyAsset = txtTotal_CompanyAsset.Text.Trim
        'txtTotal_CompanyAsset.Text = Me.Total_CompanyAsset + " %"

        Dim chk_CompanyAsset As CheckBox
        Dim txtbox_Weight_CompanyAsset As TextBox

        If e.Item.ItemIndex >= 0 Then
            chk_CompanyAsset = CType(e.Item.FindControl("chkComponent_CompanyAsset"), CheckBox)
            txtbox_Weight_CompanyAsset = CType(e.Item.FindControl("txtWeight_CompanyAsset"), TextBox)

            chk_CompanyAsset.Attributes.Add("OnClick", _
            "return EnableTextBoxCompanyAsset('" & Me.TotalRow + 1 & "', " & _
            "'" & chk_CompanyAsset.ClientID & "','" & txtbox_Weight_CompanyAsset.ClientID & "');")

            txtbox_Weight_CompanyAsset.Attributes.Add("OnBlur", _
            "return TotalCompanyAsset('" & Me.TotalRow + 1 & "'," & _
            "'" & txtbox_Weight_CompanyAsset.ClientID & "','" & txtTotal_CompanyAsset.ClientID & "');")
        End If

    End Sub

    Private Sub dtgCompanyCustomer_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgCompanyCustomer.ItemDataBound
        Dim WeightCompanyCustomer As New TextBox
        Dim TotalWeightCompanyCustomer As New TextBox

        If e.Item.ItemIndex >= 0 Then
            WeightCompanyCustomer = CType(e.Item.FindControl("txtWeight_CompanyCustomer"), TextBox)
            Temp_TotalWeight_CompanyCustomer += CDbl(WeightCompanyCustomer.Text.Trim)
        End If

        txtTotal_CompanyCustomer.Text = CStr(Temp_TotalWeight_CompanyCustomer)
        Me.Total_CompanyCustomer = txtTotal_CompanyCustomer.Text.Trim
        'txtTotal_CompanyCustomer.Text = Me.Total_CompanyCustomer + " %"

        Dim chk_CompanyCustomer As CheckBox
        Dim txtbox_Weight_CompanyCustomer As TextBox

        If e.Item.ItemIndex >= 0 Then
            chk_CompanyCustomer = CType(e.Item.FindControl("chkComponent_CompanyCustomer"), CheckBox)
            txtbox_Weight_CompanyCustomer = CType(e.Item.FindControl("txtWeight_CompanyCustomer"), TextBox)

            chk_CompanyCustomer.Attributes.Add("OnClick", _
            "return EnableTextBoxCompanyCustomer('" & Me.TotalRow + 1 & "', " & _
            "'" & chk_CompanyCustomer.ClientID & "','" & txtbox_Weight_CompanyCustomer.ClientID & "');")

            txtbox_Weight_CompanyCustomer.Attributes.Add("OnBlur", _
            "return TotalCompanyCustomer('" & Me.TotalRow + 1 & "'," & _
            "'" & txtbox_Weight_CompanyCustomer.ClientID & "','" & txtTotal_CompanyCustomer.ClientID & "');")
        End If
    End Sub

    Private Sub dtgCompanyFinancial_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgCompanyFinancial.ItemDataBound
        Dim WeightCompanyFinancial As New TextBox
        Dim TotalWeightCompanyFinancial As New TextBox

        If e.Item.ItemIndex >= 0 Then
            WeightCompanyFinancial = CType(e.Item.FindControl("txtWeight_CompanyFinancial"), TextBox)
            Temp_TotalWeight_CompanyFinancial += CDbl(WeightCompanyFinancial.Text.Trim)
        End If

        txtTotal_CompanyFinancial.Text = CStr(Temp_TotalWeight_CompanyFinancial)
        Me.Total_CompanyFinancial = txtTotal_CompanyFinancial.Text.Trim
        'txtTotal_CompanyFinancial.Text = Me.Total_CompanyFinancial + " %"

        Dim chk_CompanyFinancial As CheckBox
        Dim txtbox_Weight_CompanyFinancial As TextBox

        If e.Item.ItemIndex >= 0 Then
            chk_CompanyFinancial = CType(e.Item.FindControl("chkComponent_CompanyFinancial"), CheckBox)
            txtbox_Weight_CompanyFinancial = CType(e.Item.FindControl("txtWeight_CompanyFinancial"), TextBox)

            chk_CompanyFinancial.Attributes.Add("OnClick", _
            "return EnableTextBoxCompanyFinancial('" & Me.TotalRow + 1 & "', " & _
            "'" & chk_CompanyFinancial.ClientID & "','" & txtbox_Weight_CompanyFinancial.ClientID & "');")

            txtbox_Weight_CompanyFinancial.Attributes.Add("OnBlur", _
            "return TotalCompanyFinancial('" & Me.TotalRow + 1 & "'," & _
            "'" & txtbox_Weight_CompanyFinancial.ClientID & "','" & txtTotal_CompanyFinancial.ClientID & "');")
        End If
    End Sub

    Private Sub dtgPersonalAsset_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPersonalAsset.ItemDataBound
        Dim WeightPersonalAsset As New TextBox
        Dim TotalWeightPersonalAsset As New TextBox

        If e.Item.ItemIndex >= 0 Then
            WeightPersonalAsset = CType(e.Item.FindControl("txtWeight_PersonalAsset"), TextBox)
            Temp_TotalWeight_PersonalAsset += CDbl(WeightPersonalAsset.Text.Trim)
        End If

        txtTotal_PersonalAsset.Text = CStr(Temp_TotalWeight_PersonalAsset)
        Me.Total_PersonalAsset = txtTotal_PersonalAsset.Text.Trim
        'txtTotal_PersonalAsset.Text = Me.Total_PersonalAsset + " %"

        Dim chk_PersonalAsset As CheckBox
        Dim txtbox_Weight_PersonalAsset As TextBox

        If e.Item.ItemIndex >= 0 Then
            chk_PersonalAsset = CType(e.Item.FindControl("chkComponent_PersonalAsset"), CheckBox)
            txtbox_Weight_PersonalAsset = CType(e.Item.FindControl("txtWeight_PersonalAsset"), TextBox)

            chk_PersonalAsset.Attributes.Add("OnClick", _
            "return EnableTextBoxPersonalAsset('" & Me.TotalRow + 1 & "', " & _
            "'" & chk_PersonalAsset.ClientID & "','" & txtbox_Weight_PersonalAsset.ClientID & "');")

            txtbox_Weight_PersonalAsset.Attributes.Add("OnBlur", _
            "return TotalPersonalAsset('" & Me.TotalRow + 1 & "'," & _
            "'" & txtbox_Weight_PersonalAsset.ClientID & "','" & txtTotal_PersonalAsset.ClientID & "');")


        End If
    End Sub

    Private Sub dtgPersonalCustomer_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPersonalCustomer.ItemDataBound
        Dim WeightPersonalCustomer As New TextBox
        Dim TotalWeightPersonalCustomer As New TextBox

        If e.Item.ItemIndex >= 0 Then
            WeightPersonalCustomer = CType(e.Item.FindControl("txtWeight_PersonalCustomer"), TextBox)
            Temp_TotalWeight_PersonalCustomer += CDbl(WeightPersonalCustomer.Text.Trim)
        End If

        txtTotal_PersonalCustomer.Text = CStr(Temp_TotalWeight_PersonalCustomer)
        Me.Total_PersonalCustomer = txtTotal_PersonalCustomer.Text.Trim
        'txtTotal_PersonalCustomer.Text = Me.Total_PersonalCustomer + " %"

        Dim chk_PersonalCustomer As CheckBox
        Dim txtbox_Weight_PersonalCustomer As TextBox

        If e.Item.ItemIndex >= 0 Then
            chk_PersonalCustomer = CType(e.Item.FindControl("chkComponent_PersonalCustomer"), CheckBox)
            txtbox_Weight_PersonalCustomer = CType(e.Item.FindControl("txtWeight_PersonalCustomer"), TextBox)

            chk_PersonalCustomer.Attributes.Add("OnClick", _
            "return EnableTextBoxPersonalCustomer('" & Me.TotalRow + 1 & "', " & _
            "'" & chk_PersonalCustomer.ClientID & "','" & txtbox_Weight_PersonalCustomer.ClientID & "');")

            txtbox_Weight_PersonalCustomer.Attributes.Add("OnBlur", _
            "return TotalPersonalCustomer('" & Me.TotalRow + 1 & "'," & _
            "'" & txtbox_Weight_PersonalCustomer.ClientID & "','" & txtTotal_PersonalCustomer.ClientID & "');")


        End If
    End Sub

    Private Sub dtgPersonalFinancial_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPersonalFinancial.ItemDataBound
        Dim WeightPersonalFinancial As New TextBox
        Dim TotalWeightPersonalFinancial As New TextBox

        If e.Item.ItemIndex >= 0 Then
            WeightPersonalFinancial = CType(e.Item.FindControl("txtWeight_PersonalFinancial"), TextBox)
            Temp_TotalWeight_PersonalFinancial += CDbl(WeightPersonalFinancial.Text.Trim)
        End If

        txtTotal_PersonalFinancial.Text = CStr(Temp_TotalWeight_PersonalFinancial)
        Me.Total_PersonalFinancial = txtTotal_PersonalFinancial.Text.Trim
        'txtTotal_PersonalFinancial.Text = Me.Total_PersonalFinancial + " %"

        Dim chk_PersonalFinancial As CheckBox
        Dim txtbox_Weight_PersonalFinancial As TextBox

        If e.Item.ItemIndex >= 0 Then
            chk_PersonalFinancial = CType(e.Item.FindControl("chkComponent_PersonalFinancial"), CheckBox)
            txtbox_Weight_PersonalFinancial = CType(e.Item.FindControl("txtWeight_PersonalFinancial"), TextBox)

            chk_PersonalFinancial.Attributes.Add("OnClick", _
            "return EnableTextBoxPersonalFinancial('" & Me.TotalRow + 1 & "', " & _
            "'" & chk_PersonalFinancial.ClientID & "','" & txtbox_Weight_PersonalFinancial.ClientID & "');")

            txtbox_Weight_PersonalFinancial.Attributes.Add("OnBlur", _
            "return TotalPersonalFinancial('" & Me.TotalRow + 1 & "'," & _
            "'" & txtbox_Weight_PersonalFinancial.ClientID & "','" & txtTotal_PersonalFinancial.ClientID & "');")

        End If
    End Sub
#End Region

#Region "GrandTotal"
    Private Sub GrandTotal()
        lblGrandTotal_Company.Text = CStr(CDbl(Me.Total_CompanyAsset) + CDbl(Me.Total_CompanyCustomer) + CDbl(Me.Total_CompanyFinancial)) + " %"
        lblGrandTotal_Personal.Text = CStr(CDbl(Me.Total_PersonalAsset) + CDbl(Me.Total_PersonalCustomer) + CDbl(Me.Total_PersonalFinancial)) + " %"
    End Sub
#End Region

#Region "ReCheck"
    Sub ReCheck_Personal()
        Dim counter_PersonalCustomer As Integer
        Dim chk_PersonalCustomer As New CheckBox
        Dim txtbox_Weight_PersonalCustomer As New TextBox

        For counter_PersonalCustomer = 0 To (Me.TotalRow_PersonalCustomer - 1)
            chk_PersonalCustomer = CType(dtgPersonalCustomer.Items.Item(counter_PersonalCustomer).FindControl("chkComponent_PersonalCustomer"), CheckBox)
            txtbox_Weight_PersonalCustomer = CType(dtgPersonalCustomer.Items.Item(counter_PersonalCustomer).FindControl("txtWeight_PersonalCustomer"), TextBox)
            If chk_PersonalCustomer.Checked = True Then
                txtbox_Weight_PersonalCustomer.Enabled = True
                'txtbox_Weight_PersonalCustomer.ReadOnly = False
            Else
                txtbox_Weight_PersonalCustomer.Enabled = False
                'txtbox_Weight_PersonalCustomer.ReadOnly = True
                txtbox_Weight_PersonalCustomer.Text = "0"
            End If
        Next

        Dim counter_PersonalAsset As Integer
        Dim chk_PersonalAsset As New CheckBox
        Dim txtbox_Weight_PersonalAsset As New TextBox

        For counter_PersonalAsset = 0 To (Me.TotalRow_PersonalAsset - 1)
            chk_PersonalAsset = CType(dtgPersonalAsset.Items.Item(counter_PersonalAsset).FindControl("chkComponent_PersonalAsset"), CheckBox)
            txtbox_Weight_PersonalAsset = CType(dtgPersonalAsset.Items.Item(counter_PersonalAsset).FindControl("txtWeight_PersonalAsset"), TextBox)
            If chk_PersonalAsset.Checked = True Then
                txtbox_Weight_PersonalAsset.Enabled = True
                'txtbox_Weight_PersonalAsset.ReadOnly = False
            Else
                txtbox_Weight_PersonalAsset.Enabled = False
                'txtbox_Weight_PersonalAsset.ReadOnly = True
                txtbox_Weight_PersonalAsset.Text = "0"
            End If
        Next

        Dim counter_PersonalFinancial As Integer
        Dim chk_PersonalFinancial As New CheckBox
        Dim txtbox_Weight_PersonalFinancial As New TextBox

        For counter_PersonalFinancial = 0 To (Me.TotalRow_PersonalFinancial - 1)
            chk_PersonalFinancial = CType(dtgPersonalFinancial.Items.Item(counter_PersonalFinancial).FindControl("chkComponent_PersonalFinancial"), CheckBox)
            txtbox_Weight_PersonalFinancial = CType(dtgPersonalFinancial.Items.Item(counter_PersonalFinancial).FindControl("txtWeight_PersonalFinancial"), TextBox)
            If chk_PersonalFinancial.Checked = True Then
                txtbox_Weight_PersonalFinancial.Enabled = True
                'txtbox_Weight_PersonalFinancial.ReadOnly = False
            Else
                txtbox_Weight_PersonalFinancial.Enabled = False
                'txtbox_Weight_PersonalFinancial.ReadOnly = True
                txtbox_Weight_PersonalFinancial.Text = "0"
            End If
        Next
    End Sub


    Sub ReCheck_Company()
        Dim counter_CompanyCustomer As Integer
        Dim chk_CompanyCustomer As New CheckBox
        Dim txtbox_Weight_CompanyCustomer As New TextBox

        For counter_CompanyCustomer = 0 To (Me.TotalRow_CompanyCustomer - 1)
            chk_CompanyCustomer = CType(dtgCompanyCustomer.Items.Item(counter_CompanyCustomer).FindControl("chkComponent_CompanyCustomer"), CheckBox)
            txtbox_Weight_CompanyCustomer = CType(dtgCompanyCustomer.Items.Item(counter_CompanyCustomer).FindControl("txtWeight_CompanyCustomer"), TextBox)
            If chk_CompanyCustomer.Checked = True Then
                txtbox_Weight_CompanyCustomer.Enabled = True
                'txtbox_Weight_CompanyCustomer.ReadOnly = False
            Else
                txtbox_Weight_CompanyCustomer.Enabled = False
                'txtbox_Weight_CompanyCustomer.ReadOnly = True
                txtbox_Weight_CompanyCustomer.Text = "0"
            End If
        Next

        Dim counter_CompanyAsset As Integer
        Dim chk_CompanyAsset As New CheckBox
        Dim txtbox_Weight_CompanyAsset As New TextBox

        For counter_CompanyAsset = 0 To (Me.TotalRow_CompanyAsset - 1)
            chk_CompanyAsset = CType(dtgCompanyAsset.Items.Item(counter_CompanyAsset).FindControl("chkComponent_CompanyAsset"), CheckBox)
            txtbox_Weight_CompanyAsset = CType(dtgCompanyAsset.Items.Item(counter_CompanyAsset).FindControl("txtWeight_CompanyAsset"), TextBox)
            If chk_CompanyAsset.Checked = True Then
                txtbox_Weight_CompanyAsset.Enabled = True
                'txtbox_Weight_CompanyAsset.ReadOnly = False
            Else
                txtbox_Weight_CompanyAsset.Enabled = False
                'txtbox_Weight_CompanyAsset.ReadOnly = True
                txtbox_Weight_CompanyAsset.Text = "0"
            End If
        Next

        Dim counter_CompanyFinancial As Integer
        Dim chk_CompanyFinancial As New CheckBox
        Dim txtbox_Weight_CompanyFinancial As New TextBox

        For counter_CompanyFinancial = 0 To (Me.TotalRow_CompanyFinancial - 1)
            chk_CompanyFinancial = CType(dtgCompanyFinancial.Items.Item(counter_CompanyFinancial).FindControl("chkComponent_CompanyFinancial"), CheckBox)
            txtbox_Weight_CompanyFinancial = CType(dtgCompanyFinancial.Items.Item(counter_CompanyFinancial).FindControl("txtWeight_CompanyFinancial"), TextBox)
            If chk_CompanyFinancial.Checked = True Then
                txtbox_Weight_CompanyFinancial.Enabled = True
                'txtbox_Weight_CompanyFinancial.ReadOnly = False
            Else
                txtbox_Weight_CompanyFinancial.Enabled = False
                'txtbox_Weight_CompanyFinancial.ReadOnly = True
                txtbox_Weight_CompanyFinancial.Text = "0"
            End If
        Next
    End Sub
#End Region

#Region "Populate Grade"
    Sub PopulateGrade(ByVal GradeList As List(Of Parameter.CreditScoreGradeMaster))
        Dim oCustomClass As New Parameter.CreditScoreGradeMaster

        'Personal
        oCustomClass = New Parameter.CreditScoreGradeMaster
        oCustomClass.CreditScoreSchemeID = Me.CreditScoreSchemeId
        oCustomClass.CustType = "P"
        oCustomClass.GradeID = 1
        oCustomClass.GradeDescription = "Grade A"
        oCustomClass.ScoreFrom = CInt(txtGradeAFromPersonal.Text)
        oCustomClass.ScoreTo = CInt(txtGradeAToPersonal.Text)
        GradeList.Add(oCustomClass)

        oCustomClass = New Parameter.CreditScoreGradeMaster
        oCustomClass.CreditScoreSchemeID = Me.CreditScoreSchemeId
        oCustomClass.CustType = "P"
        oCustomClass.GradeID = 2
        oCustomClass.GradeDescription = "Grade B"
        oCustomClass.ScoreFrom = CInt(txtGradeBFromPersonal.Text)
        oCustomClass.ScoreTo = CInt(txtGradeBToPersonal.Text)
        GradeList.Add(oCustomClass)

        oCustomClass = New Parameter.CreditScoreGradeMaster
        oCustomClass.CreditScoreSchemeID = Me.CreditScoreSchemeId
        oCustomClass.CustType = "P"
        oCustomClass.GradeID = 3
        oCustomClass.GradeDescription = "Grade C"
        oCustomClass.ScoreFrom = CInt(txtGradeCFromPersonal.Text)
        oCustomClass.ScoreTo = CInt(txtGradeCToPersonal.Text)
        GradeList.Add(oCustomClass)

        oCustomClass = New Parameter.CreditScoreGradeMaster
        oCustomClass.CreditScoreSchemeID = Me.CreditScoreSchemeId
        oCustomClass.CustType = "P"
        oCustomClass.GradeID = 4
        oCustomClass.GradeDescription = "Grade c"
        oCustomClass.ScoreFrom = CInt(txtGradeCcFromPersonal.Text)
        oCustomClass.ScoreTo = CInt(txtGradeCcToPersonal.Text)
        GradeList.Add(oCustomClass)

        oCustomClass = New Parameter.CreditScoreGradeMaster
        oCustomClass.CreditScoreSchemeID = Me.CreditScoreSchemeId
        oCustomClass.CustType = "P"
        oCustomClass.GradeID = 5
        oCustomClass.GradeDescription = "Grade D"
        oCustomClass.ScoreFrom = CInt(txtGradeDFromPersonal.Text)
        oCustomClass.ScoreTo = CInt(txtGradeDToPersonal.Text)
        GradeList.Add(oCustomClass)

        'Company
        oCustomClass = New Parameter.CreditScoreGradeMaster
        oCustomClass.CreditScoreSchemeID = Me.CreditScoreSchemeId
        oCustomClass.CustType = "C"
        oCustomClass.GradeID = 1
        oCustomClass.GradeDescription = "Grade A"
        oCustomClass.ScoreFrom = CInt(txtGradeAFromCompany.Text)
        oCustomClass.ScoreTo = CInt(txtGradeAToCompany.Text)
        GradeList.Add(oCustomClass)

        oCustomClass = New Parameter.CreditScoreGradeMaster
        oCustomClass.CreditScoreSchemeID = Me.CreditScoreSchemeId
        oCustomClass.CustType = "C"
        oCustomClass.GradeID = 2
        oCustomClass.GradeDescription = "Grade B"
        oCustomClass.ScoreFrom = CInt(txtGradeBFromCompany.Text)
        oCustomClass.ScoreTo = CInt(txtGradeBToCompany.Text)
        GradeList.Add(oCustomClass)

        oCustomClass = New Parameter.CreditScoreGradeMaster
        oCustomClass.CreditScoreSchemeID = Me.CreditScoreSchemeId
        oCustomClass.CustType = "C"
        oCustomClass.GradeID = 3
        oCustomClass.GradeDescription = "Grade C"
        oCustomClass.ScoreFrom = CInt(txtGradeCFromCompany.Text)
        oCustomClass.ScoreTo = CInt(txtGradeCToCompany.Text)
        GradeList.Add(oCustomClass)

        oCustomClass = New Parameter.CreditScoreGradeMaster
        oCustomClass.CreditScoreSchemeID = Me.CreditScoreSchemeId
        oCustomClass.CustType = "C"
        oCustomClass.GradeID = 4
        oCustomClass.GradeDescription = "Grade c"
        oCustomClass.ScoreFrom = CInt(txtGradeCcFromCompany.Text)
        oCustomClass.ScoreTo = CInt(txtGradeCcToCompany.Text)
        GradeList.Add(oCustomClass)

        oCustomClass = New Parameter.CreditScoreGradeMaster
        oCustomClass.CreditScoreSchemeID = Me.CreditScoreSchemeId
        oCustomClass.CustType = "C"
        oCustomClass.GradeID = 5
        oCustomClass.GradeDescription = "Grade D"
        oCustomClass.ScoreFrom = CInt(txtGradeDFromCompany.Text)
        oCustomClass.ScoreTo = CInt(txtGradeDToCompany.Text)
        GradeList.Add(oCustomClass)
    End Sub
#End Region

#Region "Bind Grade"
    Sub BindGrade()
        Dim GradeList As New List(Of Parameter.CreditScoreGradeMaster)
        Dim oGrade As New Parameter.CreditScoreGradeMaster

        oGrade.CreditScoreSchemeID = Me.CreditScoreSchemeId
        oGrade.strConnection = GetConnectionString()
        GradeList = m_Grade.GetCreditScoreGradeMasterList(oGrade)

        If GradeList.Count > 0 Then
            For i As Integer = 0 To GradeList.Count - 1
                oGrade = New Parameter.CreditScoreGradeMaster
                oGrade = GradeList(i)

                If oGrade.GradeID = 1 Then
                    If oGrade.CustType.ToUpper = "P" Then
                        txtGradeAFromPersonal.Text = oGrade.ScoreFrom.ToString
                        txtGradeAToPersonal.Text = oGrade.ScoreTo.ToString
                    ElseIf oGrade.CustType.ToUpper = "C" Then
                        txtGradeAFromCompany.Text = oGrade.ScoreFrom.ToString
                        txtGradeAToCompany.Text = oGrade.ScoreTo.ToString
                    End If
                ElseIf oGrade.GradeID = 2 Then
                    If oGrade.CustType.ToUpper = "P" Then
                        txtGradeBFromPersonal.Text = oGrade.ScoreFrom.ToString
                        txtGradeBToPersonal.Text = oGrade.ScoreTo.ToString
                    ElseIf oGrade.CustType.ToUpper = "C" Then
                        txtGradeBFromCompany.Text = oGrade.ScoreFrom.ToString
                        txtGradeBToCompany.Text = oGrade.ScoreTo.ToString
                    End If
                ElseIf oGrade.GradeID = 3 Then
                    If oGrade.CustType.ToUpper = "P" Then
                        txtGradeCFromPersonal.Text = oGrade.ScoreFrom.ToString
                        txtGradeCToPersonal.Text = oGrade.ScoreTo.ToString
                    ElseIf oGrade.CustType.ToUpper = "C" Then
                        txtGradeCFromCompany.Text = oGrade.ScoreFrom.ToString
                        txtGradeCToCompany.Text = oGrade.ScoreTo.ToString
                    End If
                ElseIf oGrade.GradeID = 4 Then
                    If oGrade.CustType.ToUpper = "P" Then
                        txtGradeCcFromPersonal.Text = oGrade.ScoreFrom.ToString
                        txtGradeCcToPersonal.Text = oGrade.ScoreTo.ToString
                    ElseIf oGrade.CustType.ToUpper = "C" Then
                        txtGradeCcFromCompany.Text = oGrade.ScoreFrom.ToString
                        txtGradeCcToCompany.Text = oGrade.ScoreTo.ToString
                    End If
                ElseIf oGrade.GradeID = 5 Then
                    If oGrade.CustType.ToUpper = "P" Then
                        txtGradeDFromPersonal.Text = oGrade.ScoreFrom.ToString
                        txtGradeDToPersonal.Text = oGrade.ScoreTo.ToString
                    ElseIf oGrade.CustType.ToUpper = "C" Then
                        txtGradeDFromCompany.Text = oGrade.ScoreFrom.ToString
                        txtGradeDToCompany.Text = oGrade.ScoreTo.ToString
                    End If
                End If
            Next
        Else
            txtGradeAFromPersonal.Text = ""
            txtGradeAToPersonal.Text = ""
            txtGradeAFromCompany.Text = ""
            txtGradeAToCompany.Text = ""
            txtGradeBFromPersonal.Text = ""
            txtGradeBToPersonal.Text = ""
            txtGradeBFromCompany.Text = ""
            txtGradeBToCompany.Text = ""
            txtGradeCFromPersonal.Text = ""
            txtGradeCToPersonal.Text = ""
            txtGradeCFromCompany.Text = ""
            txtGradeCToCompany.Text = ""
            txtGradeCcFromPersonal.Text = ""
            txtGradeCcToPersonal.Text = ""
            txtGradeCcFromCompany.Text = ""
            txtGradeCcToCompany.Text = ""
            txtGradeDFromPersonal.Text = ""
            txtGradeDToPersonal.Text = ""
            txtGradeDFromCompany.Text = ""
            txtGradeDToCompany.Text = ""
        End If
        

    End Sub
#End Region

#Region "Validasi Grade"
    Public Function ValidasiGrade() As Boolean
        Dim result As Boolean = True
        Dim GradeListTemp As New List(Of Parameter.CreditScoreGradeMaster)
        Dim oGrade As New Parameter.CreditScoreGradeMaster

        Dim Urutan1 As Integer = 0
        Dim Urutan2 As Integer = 0
        Dim Urutan3 As Integer = 0
        Dim Urutan4 As Integer = 0
        Dim Urutan5 As Integer = 0
        Dim Urutan6 As Integer = 0
        Dim Urutan7 As Integer = 0
        Dim Urutan8 As Integer = 0


        PopulateGrade(GradeListTemp)
        Dim GradeListPersonal = From G In GradeListTemp _
                                Where G.CustType.ToUpper = "P"
        Dim GradeListCompany = From G In GradeListTemp _
                                Where G.CustType.ToUpper = "C"

        'Grade Personal
        If GradeListPersonal.Count > 0 Then
            'Validasi Range
            For i As Integer = 0 To GradeListPersonal.Count - 1
                oGrade = New Parameter.CreditScoreGradeMaster
                oGrade = GradeListPersonal(i)
                If Not oGrade.ScoreFrom < oGrade.ScoreTo Then
                    Return result = False
                End If
            Next

            'Validasi Urutan
            For i As Integer = 0 To GradeListPersonal.Count - 1
                oGrade = New Parameter.CreditScoreGradeMaster
                oGrade = GradeListPersonal(i)
                If oGrade.GradeID = 1 Then
                    Urutan1 = CInt(oGrade.ScoreFrom)
                ElseIf oGrade.GradeID = 2 Then
                    Urutan2 = CInt(oGrade.ScoreTo)
                    Urutan3 = CInt(oGrade.ScoreFrom)
                ElseIf oGrade.GradeID = 3 Then
                    Urutan4 = CInt(oGrade.ScoreTo)
                    Urutan5 = CInt(oGrade.ScoreFrom)
                ElseIf oGrade.GradeID = 4 Then
                    Urutan6 = CInt(oGrade.ScoreTo)
                    Urutan7 = CInt(oGrade.ScoreFrom)
                ElseIf oGrade.GradeID = 5 Then
                    Urutan8 = CInt(oGrade.ScoreTo)
                End If
            Next

            If Not Urutan1 > Urutan2 Then
                Return result = False
            End If
            If Not Urutan2 > Urutan3 Then
                Return result = False
            End If
            If Not Urutan3 > Urutan4 Then
                Return result = False
            End If
            If Not Urutan4 > Urutan5 Then
                Return result = False
            End If
            If Not Urutan5 > Urutan6 Then
                Return result = False
            End If
            If Not Urutan6 > Urutan7 Then
                Return result = False
            End If
            If Not Urutan7 > Urutan8 Then
                Return result = False
            End If
            
        Else
            result = False
        End If

        'Grade Company
        If GradeListCompany.Count > 0 Then
            'Validasi Range
            For i As Integer = 0 To GradeListCompany.Count - 1
                oGrade = New Parameter.CreditScoreGradeMaster
                oGrade = GradeListCompany(i)
                If Not oGrade.ScoreFrom < oGrade.ScoreTo Then
                    Return result = False
                End If
            Next

            'Validasi Urutan
            For i As Integer = 0 To GradeListCompany.Count - 1
                oGrade = New Parameter.CreditScoreGradeMaster
                oGrade = GradeListCompany(i)
                If oGrade.GradeID = 1 Then
                    Urutan1 = CInt(oGrade.ScoreFrom)
                ElseIf oGrade.GradeID = 2 Then
                    Urutan2 = CInt(oGrade.ScoreTo)
                    Urutan3 = CInt(oGrade.ScoreFrom)
                ElseIf oGrade.GradeID = 3 Then
                    Urutan4 = CInt(oGrade.ScoreTo)
                    Urutan5 = CInt(oGrade.ScoreFrom)
                ElseIf oGrade.GradeID = 4 Then
                    Urutan6 = CInt(oGrade.ScoreTo)
                    Urutan7 = CInt(oGrade.ScoreFrom)
                ElseIf oGrade.GradeID = 5 Then
                    Urutan8 = CInt(oGrade.ScoreTo)
                End If
            Next

            If Not Urutan1 > Urutan2 Then
                Return result = False
            End If
            If Not Urutan2 > Urutan3 Then
                Return result = False
            End If
            If Not Urutan3 > Urutan4 Then
                Return result = False
            End If
            If Not Urutan4 > Urutan5 Then
                Return result = False
            End If
            If Not Urutan5 > Urutan6 Then
                Return result = False
            End If
            If Not Urutan6 > Urutan7 Then
                Return result = False
            End If
            If Not Urutan7 > Urutan8 Then
                Return result = False
            End If            

        Else
            result = False
        End If
        Return result
    End Function
#End Region
End Class