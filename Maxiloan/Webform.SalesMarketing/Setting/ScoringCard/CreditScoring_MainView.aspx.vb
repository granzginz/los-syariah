﻿Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller

Public Class CreditScoring_MainView
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private m_controller As New CreditScoringMainController
    Private recordCount As Int64 = 1
    Dim Temp_TotalWeight_CompanyCustomer As Double = 0
    Dim Temp_TotalWeight_CompanyAsset As Double = 0
    Dim Temp_TotalWeight_CompanyFinancial As Double = 0
    Dim Temp_TotalWeight_PersonalCustomer As Double = 0
    Dim Temp_TotalWeight_PersonalAsset As Double = 0
    Dim Temp_TotalWeight_PersonalFinancial As Double = 0
#End Region

#Region "Property"
    Private Property CreditScoreSchemeID() As String
        Get
            Return CType(ViewState("CreditScoreSchemeID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CreditScoreSchemeID") = Value
        End Set
    End Property

    Private Property Total_PersonalCustomer() As String
        Get
            Return CType(ViewState("Total_PersonalCustomer"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Total_PersonalCustomer") = Value
        End Set
    End Property

    Private Property Total_PersonalAsset() As String
        Get
            Return CType(ViewState("Total_PersonalAsset"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Total_PersonalAsset") = Value
        End Set
    End Property

    Private Property Total_PersonalFinancial() As String
        Get
            Return CType(ViewState("Total_PersonalFinancial"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Total_PersonalFinancial") = Value
        End Set
    End Property

    Private Property Total_CompanyCustomer() As String
        Get
            Return CType(ViewState("Total_CompanyCustomer"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Total_CompanyCustomer") = Value
        End Set
    End Property

    Private Property Total_CompanyAsset() As String
        Get
            Return CType(ViewState("Total_CompanyAsset"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Total_CompanyAsset") = Value
        End Set
    End Property

    Private Property Total_CompanyFinancial() As String
        Get
            Return CType(ViewState("Total_CompanyFinancial"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Total_CompanyFinancial") = Value
        End Set
    End Property
#End Region

#Region "Page_Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.CreditScoreSchemeID = Request.QueryString("CreditScoreSchemeID")

            'BindGridEntity_CompanyAsset()
            'BindGridEntity_CompanyCustomer()
            'BindGridEntity_CompanyFinancial()
            BindGridEntity_PersonalAsset()
            BindGridEntity_PersonalCustomer()
            BindGridEntity_PersonalFinancial()

            GrandTotal()
        End If        
    End Sub
#End Region

#Region "GrandTotal"
    Private Sub GrandTotal()
        lblGrandTotal_Company.Text = CStr(CDbl(Me.Total_CompanyAsset) + CDbl(Me.Total_CompanyCustomer) + CDbl(Me.Total_CompanyFinancial)) + " %"
        lblGrandTotal_Personal.Text = CStr(CDbl(Me.Total_PersonalAsset) + CDbl(Me.Total_PersonalCustomer) + CDbl(Me.Total_PersonalFinancial)) + " %"
    End Sub
#End Region

#Region "BindGridEntity"
    Sub BindGridEntity_PersonalCustomer()
        Dim dtEntity As DataTable
        Dim oCredit As New Parameter.CreditScoringMain

        With oCredit
            .WhereCond = "c.ScoringType='P' and c.GroupComponent='C'"
            .CreditScoreShemeId = Me.CreditScoreSchemeID
            .strConnection = GetConnectionString()
            .SPName = "spCreditScoringCardView"
        End With
        oCredit = m_controller.CreditScoringMainView(oCredit)

        'Nilai description cuman perlu ditaruh di salah satu bindgridentity.
        lblSchemeID.Text = Me.CreditScoreSchemeID
        lblDescription.Text = oCredit.Description
        lblApprovalScore_Company.Text = CStr(oCredit.CompanyApprovedScore).Trim + " %"
        lblRejectScore_Company.Text = CStr(oCredit.CompanyRejectScore).Trim + " %"
        lblApprovalScore_Personal.Text = CStr(oCredit.PersonalApprovedScore).Trim + " %"
        lblRejectScore_Personal.Text = CStr(oCredit.PersonalRejectScore).Trim + " %"

        If Not oCredit Is Nothing Then
            dtEntity = oCredit.ListData
            recordCount = oCredit.TotalRecords
        Else
            recordCount = 0
        End If

        dtgPersonalCustomer.DataSource = dtEntity.DefaultView
        dtgPersonalCustomer.CurrentPageIndex = 0
        dtgPersonalCustomer.DataBind()

    End Sub

    Sub BindGridEntity_PersonalFinancial()
        Dim dtEntity As DataTable
        Dim oCredit As New Parameter.CreditScoringMain

        With oCredit
            .WhereCond = "c.ScoringType='P' and c.GroupComponent='F'"
            .CreditScoreShemeId = Me.CreditScoreSchemeID
            .strConnection = GetConnectionString()
            .SPName = "spCreditScoringCardView"
        End With
        oCredit = m_controller.CreditScoringMainView(oCredit)

        If Not oCredit Is Nothing Then
            dtEntity = oCredit.ListData
            recordCount = oCredit.TotalRecords
        Else
            recordCount = 0
        End If

        dtgPersonalFinancial.DataSource = dtEntity.DefaultView
        dtgPersonalFinancial.CurrentPageIndex = 0
        dtgPersonalFinancial.DataBind()

    End Sub

    Sub BindGridEntity_PersonalAsset()
        Dim dtEntity As DataTable
        Dim oCredit As New Parameter.CreditScoringMain

        With oCredit
            .WhereCond = "c.ScoringType='P' and c.GroupComponent='A'"
            .CreditScoreShemeId = Me.CreditScoreSchemeID
            .strConnection = GetConnectionString()
            .SPName = "spCreditScoringCardView"
        End With
        oCredit = m_controller.CreditScoringMainView(oCredit)

        If Not oCredit Is Nothing Then
            dtEntity = oCredit.ListData
            recordCount = oCredit.TotalRecords
        Else
            recordCount = 0
        End If

        dtgPersonalAsset.DataSource = dtEntity.DefaultView
        dtgPersonalAsset.CurrentPageIndex = 0
        dtgPersonalAsset.DataBind()

    End Sub

    Sub BindGridEntity_CompanyCustomer()
        Dim dtEntity As DataTable
        Dim oCredit As New Parameter.CreditScoringMain

        With oCredit
            .WhereCond = "c.ScoringType='C' and c.GroupComponent='C'"
            .CreditScoreShemeId = Me.CreditScoreSchemeID
            .strConnection = GetConnectionString()
            .SPName = "spCreditScoringCardView"
        End With
        oCredit = m_controller.CreditScoringMainView(oCredit)

        If Not oCredit Is Nothing Then
            dtEntity = oCredit.ListData
            recordCount = oCredit.TotalRecords
        Else
            recordCount = 0
        End If

        dtgCompanyCustomer.DataSource = dtEntity.DefaultView
        dtgCompanyCustomer.CurrentPageIndex = 0
        dtgCompanyCustomer.DataBind()

    End Sub

    Sub BindGridEntity_CompanyFinancial()
        Dim dtEntity As DataTable = Nothing
        Dim oCredit As New Parameter.CreditScoringMain

        With oCredit
            .WhereCond = "c.ScoringType='C' and c.GroupComponent='F'"
            .CreditScoreShemeId = Me.CreditScoreSchemeID
            .strConnection = GetConnectionString()
            .SPName = "spCreditScoringCardView"
        End With
        oCredit = m_controller.CreditScoringMainView(oCredit)

        If Not oCredit Is Nothing Then
            dtEntity = oCredit.ListData
            recordCount = oCredit.TotalRecords
        Else
            recordCount = 0
        End If

        dtgCompanyFinancial.DataSource = dtEntity.DefaultView
        dtgCompanyFinancial.CurrentPageIndex = 0
        dtgCompanyFinancial.DataBind()

    End Sub

    Sub BindGridEntity_CompanyAsset()
        Dim dtEntity As DataTable = Nothing
        Dim oCredit As New Parameter.CreditScoringMain

        With oCredit
            .WhereCond = "c.ScoringType='C' and c.GroupComponent='A'"
            .CreditScoreShemeId = Me.CreditScoreSchemeID
            .strConnection = GetConnectionString()
            .SPName = "spCreditScoringCardView"
        End With
        oCredit = m_controller.CreditScoringMainView(oCredit)

        If Not oCredit Is Nothing Then
            dtEntity = oCredit.ListData
            recordCount = oCredit.TotalRecords
        Else
            recordCount = 0
        End If

        dtgCompanyAsset.DataSource = dtEntity.DefaultView
        dtgCompanyAsset.CurrentPageIndex = 0
        dtgCompanyAsset.DataBind()
    End Sub
#End Region

#Region "ItemDataBound"
    Private Sub dtgCompanyAsset_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgCompanyAsset.ItemDataBound
        Dim WeightCompanyAsset As New Label
        Dim TotalWeightCompanyAsset As New Label

        If e.Item.ItemIndex >= 0 Then
            WeightCompanyAsset = CType(e.Item.FindControl("lblWeight_CompanyAsset"), Label)
            Temp_TotalWeight_CompanyAsset += CDbl(WeightCompanyAsset.Text.Trim)
        End If

        If e.Item.ItemType = ListItemType.Footer Then
            TotalWeightCompanyAsset = CType(e.Item.FindControl("lblTotal_CompanyAsset"), Label)
            TotalWeightCompanyAsset.Text = Temp_TotalWeight_CompanyAsset.ToString
        End If
        Me.Total_CompanyAsset = TotalWeightCompanyAsset.Text
        TotalWeightCompanyAsset.Text = Me.Total_CompanyAsset + " %"
    End Sub

    Private Sub dtgCompanyCustomer_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgCompanyCustomer.ItemDataBound
        Dim WeightCompanyCustomer As New Label
        Dim TotalWeightCompanyCustomer As New Label

        If e.Item.ItemIndex >= 0 Then
            WeightCompanyCustomer = CType(e.Item.FindControl("lblWeight_CompanyCustomer"), Label)
            Temp_TotalWeight_CompanyCustomer += CDbl(WeightCompanyCustomer.Text.Trim)
        End If

        If e.Item.ItemType = ListItemType.Footer Then
            TotalWeightCompanyCustomer = CType(e.Item.FindControl("lblTotal_CompanyCustomer"), Label)
            TotalWeightCompanyCustomer.Text = Temp_TotalWeight_CompanyCustomer.ToString
        End If

        Me.Total_CompanyCustomer = TotalWeightCompanyCustomer.Text
        TotalWeightCompanyCustomer.Text = Me.Total_CompanyCustomer + " %"
    End Sub

    Private Sub dtgCompanyFinancial_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgCompanyFinancial.ItemDataBound
        Dim WeightCompanyFinancial As New Label
        Dim TotalWeightCompanyFinancial As New Label

        If e.Item.ItemIndex >= 0 Then
            WeightCompanyFinancial = CType(e.Item.FindControl("lblWeight_CompanyFinancial"), Label)
            Temp_TotalWeight_CompanyFinancial += CDbl(WeightCompanyFinancial.Text.Trim)
        End If

        If e.Item.ItemType = ListItemType.Footer Then
            TotalWeightCompanyFinancial = CType(e.Item.FindControl("lblTotal_CompanyFinancial"), Label)
            TotalWeightCompanyFinancial.Text = Temp_TotalWeight_CompanyFinancial.ToString
        End If

        Me.Total_CompanyFinancial = TotalWeightCompanyFinancial.Text
        TotalWeightCompanyFinancial.Text = Me.Total_CompanyFinancial + " %"
    End Sub

    Private Sub dtgPersonalAsset_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPersonalAsset.ItemDataBound
        Dim WeightPersonalAsset As New Label
        Dim TotalWeightPersonalAsset As New Label

        If e.Item.ItemIndex >= 0 Then
            WeightPersonalAsset = CType(e.Item.FindControl("lblWeight_PersonalAsset"), Label)
            Temp_TotalWeight_PersonalAsset += CDbl(WeightPersonalAsset.Text.Trim)
        End If

        If e.Item.ItemType = ListItemType.Footer Then
            TotalWeightPersonalAsset = CType(e.Item.FindControl("lblTotal_PersonalAsset"), Label)
            TotalWeightPersonalAsset.Text = Temp_TotalWeight_PersonalAsset.ToString
        End If

        Me.Total_PersonalAsset = TotalWeightPersonalAsset.Text
        TotalWeightPersonalAsset.Text = Me.Total_PersonalAsset + " %"
    End Sub

    Private Sub dtgPersonalCustomer_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPersonalCustomer.ItemDataBound
        Dim WeightPersonalCustomer As New Label
        Dim TotalWeightPersonalCustomer As New Label

        If e.Item.ItemIndex >= 0 Then
            WeightPersonalCustomer = CType(e.Item.FindControl("lblWeight_PersonalCustomer"), Label)
            Temp_TotalWeight_PersonalCustomer += CDbl(WeightPersonalCustomer.Text.Trim)
        End If

        If e.Item.ItemType = ListItemType.Footer Then
            TotalWeightPersonalCustomer = CType(e.Item.FindControl("lblTotal_PersonalCustomer"), Label)
            TotalWeightPersonalCustomer.Text = Temp_TotalWeight_PersonalCustomer.ToString
        End If

        Me.Total_PersonalCustomer = TotalWeightPersonalCustomer.Text
        TotalWeightPersonalCustomer.Text = Me.Total_PersonalCustomer + " %"
    End Sub

    Private Sub dtgPersonalFinancial_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPersonalFinancial.ItemDataBound
        Dim WeightPersonalFinancial As New Label
        Dim TotalWeightPersonalFinancial As New Label


        If e.Item.ItemIndex >= 0 Then
            WeightPersonalFinancial = CType(e.Item.FindControl("lblWeight_PersonalFinancial"), Label)
            Temp_TotalWeight_PersonalFinancial += CDbl(WeightPersonalFinancial.Text.Trim)
        End If

        If e.Item.ItemType = ListItemType.Footer Then
            TotalWeightPersonalFinancial = CType(e.Item.FindControl("lblTotal_PersonalFinancial"), Label)
            TotalWeightPersonalFinancial.Text = Temp_TotalWeight_PersonalFinancial.ToString
        End If

        Me.Total_PersonalFinancial = TotalWeightPersonalFinancial.Text
        TotalWeightPersonalFinancial.Text = Me.Total_PersonalFinancial + " %"
    End Sub
#End Region


End Class