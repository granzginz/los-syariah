﻿Public Class RejectPolicy_Main
    Inherits Maxiloan.Webform.WebBased

    Protected Property Skema() As String
        Get
            Return CType(ViewState("Skema"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Skema") = Value
        End Set
    End Property
    Protected Property NamaSkema() As String
        Get
            Return CType(ViewState("NamaSkema"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("NamaSkema") = Value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Visible = False
        Me.FormID = "REJECTPOLICYMAIN"
        If SessionInvalid() Then
            Exit Sub
        End If
    End Sub
    Private Sub imbSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonEdit.Click
        Me.NamaSkema = Label1.Text
        Me.Skema = lblSchemeID.Text
        Response.Redirect("RejectPolicy_EditContent.aspx?NamaSkema=" + Me.NamaSkema + "&Skema=" + Me.Skema)
    End Sub
End Class