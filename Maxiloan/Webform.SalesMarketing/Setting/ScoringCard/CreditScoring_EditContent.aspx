﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CreditScoring_EditContent.aspx.vb"
    Inherits="Maxiloan.Webform.SalesMarketing.CreditScoring_EditContent" %>

<%@ Register Src="../../../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %> 
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>CreditScoring_EditContent</title>
    <script language="JavaScript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;

        function fClose() {
            window.close();
            return false;
        }

        function DeleteConfirm() {
            if (confirm("Apakah yakin mau hapus data ini ?")) {
                return true;
            }
            else {
                return false;
            }
        }


        function OpenWinSchemeView(pCreditScoreSchemeID) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.SalesMarketing/Setting/ScoringCard/CreditScoring_MainView.aspx?CreditScoreSchemeID=' + pCreditScoreSchemeID, 'CreditScoringView', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=1');
        }			
    </script>
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../../Include/Buttons.css" type="text/css" />
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script> 
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                SCORE COMPONEN CONTENT PEMBIAYAAN
            </h3>
        </div>
    </div>
    <asp:Panel ID="pnlView" runat="server">
        <div class="form_box_header">
            <div class="form_single">
                <h4>
                    EDIT - SCORE COMPONEN PEMBIAYAAN
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    <p>
                        ID Skema</p>
                </label>
                <asp:Label ID="lblSchemeID" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Nama Skema</label>
                <asp:Label ID="lblDescription" runat="server"></asp:Label>
            </div>
        </div>
        <asp:Panel ID="pnlPersonalCustomer" runat="server">
            <div class="form_box_title">
                <div class="form_single">
                    <h4>
                        PERSONAL CUSTOMER
                    </h4>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <h4>
                        CUSTOMER
                    </h4>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ns">
                        <asp:DataGrid ID="dtgPersonalCustomer" runat="server" CssClass="grid_general" BorderStyle="None"
                            BorderWidth="0" Width="100%" AllowSorting="True" AutoGenerateColumns="False">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <Columns>
                                <asp:TemplateColumn HeaderText="Edit Content">
                                    <ItemStyle CssClass="command_col"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" ImageUrl="../../../Images/iconedit.gif"
                                            CommandName="EditContent"></asp:ImageButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Komponen">
                                    <ItemTemplate>
                                        <asp:Label ID="lblComponent_PersonalCustomer" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Component")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label runat="server" ID="lblTotal" Text="Total"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Bobot (%)">
                                    <ItemTemplate>
                                        <asp:Label ID="lblWeight_PersonalCustomer" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Weight")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblTotal_PersonalCustomer" runat="server"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="CalculationType" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCalculationType_PersonalCustomer" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CalculationType")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="PembiayaanScoreComponentID" Visible="False">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCreditScoreComponentID_PersonalCustomer" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CreditScoreComponentID")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                    </div>
                </div>
            </div>
            <asp:Panel ID="pnlPersonalFinancial" runat="server">
                <div class="form_box_header">
                    <div class="form_single">
                        <h4>
                            FINANCIAL
                        </h4>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtgPersonalFinancial" runat="server" CssClass="grid_general" BorderStyle="None"
                                BorderWidth="0" Width="100%" AllowSorting="True" AutoGenerateColumns="False">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="Edit Content">
                                        <ItemStyle CssClass="command_col"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="Imagebutton1" runat="server" CausesValidation="False" ImageUrl="../../../Images/iconedit.gif"
                                                CommandName="EditContent"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Komponen">
                                        <ItemTemplate>
                                            <asp:Label ID="lblComponent_PersonalFinancial" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Component")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label runat="server" ID="label" Text="Total"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Bobot (%)">
                                        <ItemTemplate>
                                            <asp:Label ID="lblWeight_PersonalFinancial" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Weight")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotal_PersonalFinancial" runat="server"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="CalculationType" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCalculationType_PersonalFinancial" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CalculationType")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PembiayaanScoreComponentID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCreditScoreComponentID_PersonalFinancial" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CreditScoreComponentID")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                        </div>
                    </div>
                </div>
                <asp:Panel ID="pnlPersonalAsset" runat="server">
                    <div class="form_box_header">
                        <div class="form_single">
                            <h4>
                                ASSET
                            </h4>
                        </div>
                    </div>
                    <div class="form_box_header">
                        <div class="form_single">
                            <div class="grid_wrapper_ns">
                                <asp:DataGrid ID="dtgPersonalAsset" runat="server" CssClass="grid_general" BorderStyle="None"
                                    BorderWidth="0" Width="100%" AllowSorting="True" AutoGenerateColumns="False">
                                    <HeaderStyle CssClass="th" />
                                    <ItemStyle CssClass="item_grid" />
                                    <Columns>
                                        <asp:TemplateColumn HeaderText="Edit Content">
                                            <ItemStyle CssClass="command_col"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:ImageButton ID="Imagebutton2" runat="server" CausesValidation="False" ImageUrl="../../../images/iconedit.gif"
                                                    CommandName="EditContent"></asp:ImageButton>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Komponen">
                                            <ItemTemplate>
                                                <asp:Label ID="lblComponent_PersonalAsset" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Component")%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label runat="server" ID="Label2" Text="Total"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Bobot (%)">
                                            <ItemTemplate>
                                                <asp:Label ID="lblWeight_PersonalAsset" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Weight")%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotal_PersonalAsset" runat="server"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="CalculationType" Visible="False">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCalculationType_PersonalAsset" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CalculationType")%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="PembiayaanScoreComponentID" Visible="False">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCreditScoreComponentID_PersonalAsset" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CreditScoreComponentID")%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
            </asp:Panel>
            <div class="form_box">
                <div class="form_single">
                    <label class="label_req">
                        Grade A</label>
                    <asp:TextBox Enabled="false" ID="txtGradeAFromPersonal" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ErrorMessage="*"
                        CssClass="validator_general" ControlToValidate="txtGradeAFromPersonal"></asp:RequiredFieldValidator>
                    S/D
                    <asp:TextBox Enabled="false" ID="txtGradeAToPersonal" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" ErrorMessage="*"
                        CssClass="validator_general" ControlToValidate="txtGradeAToPersonal"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label class="label_req">
                        Grade B</label>
                    <asp:TextBox Enabled="false" ID="txtGradeBFromPersonal" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator18" runat="server" ErrorMessage="*"
                        CssClass="validator_general" ControlToValidate="txtGradeBFromPersonal"></asp:RequiredFieldValidator>
                    S/D
                    <asp:TextBox Enabled="false" ID="txtGradeBToPersonal" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator19" runat="server" ErrorMessage="*"
                        CssClass="validator_general" ControlToValidate="txtGradeBToPersonal"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label class="label_req">
                        Grade C</label>
                    <asp:TextBox Enabled="false" ID="txtGradeCFromPersonal" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator20" runat="server" ErrorMessage="*"
                        CssClass="validator_general" ControlToValidate="txtGradeCFromPersonal"></asp:RequiredFieldValidator>
                    S/D
                    <asp:TextBox Enabled="false" ID="txtGradeCToPersonal" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator21" runat="server" ErrorMessage="*"
                        CssClass="validator_general" ControlToValidate="txtGradeCToPersonal"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label class="label_req">
                        Grade c</label>
                    <asp:TextBox Enabled="false" ID="txtGradeCcFromPersonal" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator22" runat="server" ErrorMessage="*"
                        CssClass="validator_general" ControlToValidate="txtGradeCcFromPersonal"></asp:RequiredFieldValidator>
                    S/D
                    <asp:TextBox Enabled="false" ID="txtGradeCcToPersonal" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator23" runat="server" ErrorMessage="*"
                        CssClass="validator_general" ControlToValidate="txtGradeCcToPersonal"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label class="label_req">
                        Grade D</label>
                    <asp:TextBox Enabled="false" ID="txtGradeDFromPersonal" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator24" runat="server" ErrorMessage="*"
                        CssClass="validator_general" ControlToValidate="txtGradeDFromPersonal"></asp:RequiredFieldValidator>
                    S/D
                    <asp:TextBox Enabled="false" ID="txtGradeDToPersonal" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator25" runat="server" ErrorMessage="*"
                        CssClass="validator_general" ControlToValidate="txtGradeDToPersonal"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <h4>
                        Score Approval :
                        <asp:Label ID="lblApprovalScore_Personal" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PersonalApprovedScore")%>'>
                        </asp:Label>
                        Score Reject :
                        <asp:Label ID="lblRejectScore_Personal" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PersonalRejectScore")%>'>
                        </asp:Label>
                        Total :
                        <asp:Label ID="lblGrandTotal_Personal" runat="server"></asp:Label>
                    </h4>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlCompanyCustomer" runat="server" Visible="false">
            <div class="form_box_title">
                <div class="form_single">
                    <h4>
                        CORPORATE CUSTOMER
                    </h4>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <h4>
                        DATA CORPORATE
                    </h4>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ns">
                        <asp:DataGrid ID="dtgCompanyCustomer" runat="server" CssClass="grid_general" BorderStyle="None"
                            BorderWidth="0" Width="100%" AllowSorting="True" AutoGenerateColumns="False">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <Columns>
                                <asp:TemplateColumn HeaderText="Edit Content">
                                    <ItemStyle CssClass="command_col"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="Imagebutton3" runat="server" CausesValidation="False" ImageUrl="../../../images/iconedit.gif"
                                            CommandName="EditContent"></asp:ImageButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Komponen">
                                    <ItemTemplate>
                                        <asp:Label ID="lblComponent_CompanyCustomer" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Component")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label runat="server" ID="Label3" Text="Total"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Bobot (%)">
                                    <ItemTemplate>
                                        <asp:Label ID="lblWeight_CompanyCustomer" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Weight")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblTotal_CompanyCustomer" runat="server"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="CalculationType" Visible="False">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCalculationType_CompanyCustomer" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CalculationType")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="PembiayaanScoreComponentID" Visible="False">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCreditScoreComponentID_CompanyCustomer" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CreditScoreComponentID")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                    </div>
                </div>
            </div>
            <asp:Panel ID="pnlCompanyFinancial" runat="server">
                <div class="form_box_header">
                    <div class="form_single">
                        <h4>
                            FINANCIAL
                        </h4>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtgCompanyFinancial" runat="server" CssClass="grid_general" BorderStyle="None"
                                BorderWidth="0" Width="100%" AllowSorting="True" AutoGenerateColumns="False">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="Edit Content">
                                        <ItemStyle CssClass="command_col"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="Imagebutton4" runat="server" CausesValidation="False" ImageUrl="../../../images/iconedit.gif"
                                                CommandName="EditContent"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Komponen">
                                        <ItemTemplate>
                                            <asp:Label ID="lblComponent_CompanyFinancial" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Component")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label runat="server" ID="Label4" Text="Total"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Bobot (%)">
                                        <ItemTemplate>
                                            <asp:Label ID="lblWeight_CompanyFinancial" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Weight")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotal_CompanyFinancial" runat="server"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="CalculationType" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCalculationType_CompanyFinancial" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CalculationType")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PembiayaanScoreComponentID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCreditScoreComponentID_CompanyFinancial" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CreditScoreComponentID")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                        </div>
                    </div>
                </div>
                <asp:Panel ID="pnlCompanyAsset" runat="server">
                    <div class="form_box_header">
                        <div class="form_single">
                            <h4>
                                ASSET
                            </h4>
                        </div>
                    </div>
                    <div class="form_box_header">
                        <div class="form_single">
                            <div class="grid_wrapper_ns">
                                <asp:DataGrid ID="dtgCompanyAsset" runat="server" CssClass="grid_general" BorderStyle="None"
                                    BorderWidth="0" Width="100%" AllowSorting="True" AutoGenerateColumns="False">
                                    <HeaderStyle CssClass="th" />
                                    <ItemStyle CssClass="item_grid" />
                                    <Columns>
                                        <asp:TemplateColumn HeaderText="Edit Content">
                                            <ItemStyle CssClass="command_col"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:ImageButton ID="Imagebutton5" runat="server" CausesValidation="False" ImageUrl="../../../images/iconedit.gif"
                                                    CommandName="EditContent"></asp:ImageButton>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Komponen">
                                            <ItemTemplate>
                                                <asp:Label ID="lblComponent_CompanyAsset" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Component")%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                            <FooterStyle HorizontalAlign="Left"></FooterStyle>
                                            <FooterTemplate>
                                                <asp:Label runat="server" ID="Label5" Text="Total"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Bobot (%)">
                                            <ItemTemplate>
                                                <asp:Label ID="lblWeight_CompanyAsset" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Weight")%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotal_CompanyAsset" runat="server"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="CalculationType" Visible="False">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCalculationType_CompanyAsset" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CalculationType")%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="PembiayaanScoreComponentID" Visible="False">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCreditScoreComponentID_CompanyAsset" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CreditScoreComponentID")%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
            </asp:Panel>
            <div class="form_box">
                <div class="form_single">
                    <label class="label_req">
                        Grade A</label>
                    <asp:TextBox Enabled="false" ID="txtGradeAFromCompany" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*"
                        CssClass="validator_general" ControlToValidate="txtGradeAFromCompany"></asp:RequiredFieldValidator>
                    S/D
                    <asp:TextBox Enabled="false" ID="txtGradeAToCompany" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="*"
                        CssClass="validator_general" ControlToValidate="txtGradeAToCompany"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label class="label_req">
                        Grade B</label>
                    <asp:TextBox Enabled="false" ID="txtGradeBFromCompany" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="*"
                        CssClass="validator_general" ControlToValidate="txtGradeBFromCompany"></asp:RequiredFieldValidator>
                    S/D
                    <asp:TextBox Enabled="false" ID="txtGradeBToCompany" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="*"
                        CssClass="validator_general" ControlToValidate="txtGradeBToCompany"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label class="label_req">
                        Grade C</label>
                    <asp:TextBox Enabled="false" ID="txtGradeCFromCompany" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="*"
                        CssClass="validator_general" ControlToValidate="txtGradeCFromCompany"></asp:RequiredFieldValidator>
                    S/D
                    <asp:TextBox Enabled="false" ID="txtGradeCToCompany" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="*"
                        CssClass="validator_general" ControlToValidate="txtGradeCToCompany"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label class="label_req">
                        Grade c</label>
                    <asp:TextBox Enabled="false" ID="txtGradeCcFromCompany" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage="*"
                        CssClass="validator_general" ControlToValidate="txtGradeCcFromCompany"></asp:RequiredFieldValidator>
                    S/D
                    <asp:TextBox Enabled="false" ID="txtGradeCcToCompany" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ErrorMessage="*"
                        CssClass="validator_general" ControlToValidate="txtGradeCcToCompany"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label class="label_req">
                        Grade D</label>
                    <asp:TextBox Enabled="false" ID="txtGradeDFromCompany" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ErrorMessage="*"
                        CssClass="validator_general" ControlToValidate="txtGradeDFromCompany"></asp:RequiredFieldValidator>
                    S/D
                    <asp:TextBox Enabled="false" ID="txtGradeDToCompany" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ErrorMessage="*"
                        CssClass="validator_general" ControlToValidate="txtGradeDToCompany"></asp:RequiredFieldValidator>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="PanelSkemaApproval" runat="server" Visible="false">
        <div class="form_box_header">
            <div class="form_single">
                <h4>
                    Score Approval :
                    <asp:Label ID="lblApprovalScore_Company" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CompanyApprovedScore")%>'>
                    </asp:Label>
                    Score Reject :
                    <asp:Label ID="lblRejectScore_Company" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CompanyRejectScore")%>'>
                    </asp:Label>
                    Total :
                    <asp:Label ID="lblGrandTotal_Company" runat="server"></asp:Label>
                </h4>
            </div>
        </div>
        </asp:Panel>
        <div class="form_button">
            <asp:Button ID="ButtonOK" runat="server" Text="Ok" CssClass="small button blue" CausesValidation="false">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlEditContent_Range" runat="server">
        <asp:Panel ID="pnlEditContent_Range_List" runat="server">
            <div class="form_box">
                <div class="form_single">
                    <label>
                        <p>
                            ID Skema</p>
                    </label>
                    <asp:HyperLink ID="hplSchemeID" runat="server"></asp:HyperLink>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Nama Skema</label>
                    <asp:Label ID="lblDescription_Range" runat="server"></asp:Label>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Komponen</label>
                    <asp:Label ID="lblComponent_Range" runat="server"></asp:Label>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlEditContent_Range_Datagrid" runat="server">
            <div class="form_box_header">
                <div class="form_single">
                    <h4>
                        DAFTAR SCORE COMPONEN CONTENT PEMBIAYAAN
                    </h4>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ns">
                        <asp:DataGrid ID="dtgEditContent_Range" runat="server" Width="100%" AllowSorting="True"
                            AutoGenerateColumns="False" CssClass="grid_general" BorderStyle="None" BorderWidth="0"
                            OnSortCommand="Sorting">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <Columns>
                                <asp:TemplateColumn HeaderText="EDIT" ItemStyle-Width="10%">
                                    <ItemStyle CssClass="command_col"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgEditRange" runat="server" CausesValidation="False" ImageUrl="../../../images/iconedit.gif"
                                            CommandName="EditRange"></asp:ImageButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="DELETE" ItemStyle-Width="10%">
                                    <ItemStyle CssClass="command_col"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgDeleteRange" runat="server" CausesValidation="False" ImageUrl="../../../images/icondelete.gif"
                                            CommandName="DeleteRange"></asp:ImageButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="Content" HeaderText="CONTENT">
                                    <ItemTemplate>
                                        <asp:Label ID="lblContent" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Content")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="ScoreValue" HeaderText="SCORE">
                                    <ItemTemplate>
                                        <asp:Label ID="lblScore" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ScoreValue")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="ScoreStatus" HeaderText="STATUS SCORE">
                                    <ItemTemplate>
                                        <asp:Label ID="lblScoreStatus" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ScoreStatus")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn Visible="false" HeaderText="ContentSeqNo">
                                    <ItemTemplate>
                                        <asp:Label ID="lblContentSeqNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ContentSeqNo")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                        <%--<div class="button_gridnavigation">
                            <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../../Images/grid_navbutton01.png"
                                CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../../Images/grid_navbutton02.png"
                                CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../../Images/grid_navbutton03.png"
                                CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../../Images/grid_navbutton04.png"
                                CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                            </asp:ImageButton>
                            <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                            <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                                EnableViewState="False"></asp:Button>
                            <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" MinimumValue="1"
                                ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"></asp:RangeValidator>
                            <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                                ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        </div>
                            <div class="label_gridnavigation">
                            <asp:Label ID="lblPage" runat="server" ></asp:Label>of
                            <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                            <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                        </div>--%>
                        <uc2:ucGridNav id="GridNavigator" runat="server"/>
                    </div>
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="imbAdd" runat="server" CausesValidation="False" Text="Addaaaa" CssClass="small button blue">
                </asp:Button>
                <asp:Button ID="ButtonBack" runat="server" Text="Back" CssClass="small button blue"
                    CausesValidation="False"></asp:Button>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlEditContent_Range_AddEdit" runat="server">
            <div class="form_box_header">
                <div class="form_single">
                    <h4>
                        <asp:Label ID="lblAddEdit" runat="server"></asp:Label>
                    </h4>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label class="label_req">
                        <p>
                            Content</p>
                    </label>
                    <asp:TextBox ID="txtValueFrom" runat="server" MaxLength="15"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator4" runat="server" Display="Dynamic"
                        CssClass="validator_general" ErrorMessage="" ControlToValidate="txtValueFrom"></asp:RequiredFieldValidator>to
                    <asp:TextBox ID="txtValueTo" runat="server" MaxLength="15"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" Display="Dynamic"
                        CssClass="validator_general" ErrorMessage="" ControlToValidate="txtValueTo"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="Rangevalidator1" runat="server" ErrorMessage="Nilai Salah"
                        MinimumValue="0" Type="double" ControlToValidate="txtValueFrom" MaximumValue="999999999999999"
                        CssClass="validator_general" Display="Dynamic"></asp:RangeValidator>
                    <asp:RangeValidator ID="Rangevalidator2" runat="server" ErrorMessage="Nilai Salah"
                        MinimumValue="0" Type="double" ControlToValidate="txtValueTo" MaximumValue="999999999999999"
                        CssClass="validator_general" Display="Dynamic"></asp:RangeValidator>
                    <asp:CompareValidator ID="cmv" runat="server" ErrorMessage="Nilai DARI harus lebih kecil dari Nilai KE"
                        CssClass="validator_general" ControlToValidate="txtValueFrom" Display="Dynamic" Type="Integer"
                        ControlToCompare="txtValueTo" Operator="LessThan"></asp:CompareValidator>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label class="label_req">
                        Score</label>
                    <asp:TextBox ID="txtScoreValue" runat="server" MaxLength="3"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" Display="Dynamic"
                        CssClass="validator_general" ErrorMessage="" ControlToValidate="txtScoreValue"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="Rangevalidator3" runat="server" ErrorMessage="Score harus antara 0 s/d 100"
                        CssClass="validator_general" MinimumValue="0" Type="integer" ControlToValidate="txtScoreValue"
                        MaximumValue="100" Display="Dynamic"></asp:RangeValidator>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Status Score</label>
                    <asp:DropDownList ID="cboScoreStatus" runat="server" Width="88px">
                        <asp:ListItem Value="N">Normal</asp:ListItem>
                        <asp:ListItem Value="W">Warning</asp:ListItem>
                        <asp:ListItem Value="F">Fatal</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="ButtonSave" runat="server" Text="Save" CssClass="small button blue">
                </asp:Button>
                <asp:Button ID="ButtonCancel" runat="server" Text="Cancel" CssClass="small button gray"
                    CausesValidation="False"></asp:Button>
            </div>
        </asp:Panel>
    </asp:Panel>
    <asp:Panel ID="pnlEditContent_Table" runat="server">
        <asp:Panel ID="pnlEditContent_Tabel_List" runat="server">
            <div class="form_box_header">
                <div class="form_single">
                    <h4>
                        SCORE COMPONEN CONTENT PEMBIAYAAN
                    </h4>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        <p>
                            ID Skema</p>
                    </label>
                    <asp:HyperLink ID="hplSchemeID_Table" runat="server"></asp:HyperLink>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Nama Skema</label>
                    <asp:Label ID="lblDescription_Table" runat="server"></asp:Label>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Komponen</label>
                    <asp:Label ID="lblComponent_Table" runat="server"></asp:Label>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlEditContent_Table_Datagrid" runat="server">
            <div class="form_box_header">
                <div class="form_single">
                    <h4>
                        DAFTAR SCORE COMPONENT CONTENT PEMBIAYAAN
                    </h4>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ns">
                        <asp:DataGrid ID="dtgEditContent_Table" runat="server" CCssClass="grid_general" BorderStyle="None"
                            BorderWidth="0" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                            OnSortCommand="Sorting1">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <Columns>
                                <asp:TemplateColumn HeaderText="EDIT">
                                    <ItemStyle CssClass="command_col"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgEdit_Table" runat="server" CausesValidation="False" ImageUrl="../../../images/iconedit.gif"
                                            CommandName="EditTable"></asp:ImageButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="ValueContent" HeaderText="CONTENT">
                                    <ItemTemplate>
                                        <asp:Label ID="lblValueContent_Table" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ValueContent")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="ValueDescription" HeaderText="KETERANGAN">
                                    <ItemTemplate>
                                        <asp:Label ID="lblValueDescription_Table" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ValueDescription")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="ScoreValue" HeaderText="SCORE">
                                    <ItemTemplate>
                                        <asp:Label ID="lblScoreValue_Table" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ScoreValue")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="ScoreStatus" HeaderText="STATUS SCORE">
                                    <ItemTemplate>
                                        <asp:Label ID="lblScoreStatus_Table" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ScoreStatus")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn Visible="false" HeaderText="ContentSeqNo">
                                    <ItemTemplate>
                                        <asp:Label ID="lblContentSeqNo_Table" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ContentSeqNo")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                        <div class="button_gridnavigation">
                            <asp:ImageButton ID="imbFirstPage1" runat="server" ImageUrl="../../../Images/grid_navbutton01.png"
                                CommandName="First" OnCommand="NavigationLink1_Click" CausesValidation="False">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbPrevPage1" runat="server" ImageUrl="../../../Images/grid_navbutton02.png"
                                CommandName="Prev" OnCommand="NavigationLink1_Click" CausesValidation="False">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbNextPage1" runat="server" ImageUrl="../../../Images/grid_navbutton03.png"
                                CommandName="Next" OnCommand="NavigationLink1_Click" CausesValidation="False">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbLastPage1" runat="server" ImageUrl="../../../Images/grid_navbutton04.png"
                                CommandName="Last" OnCommand="NavigationLink1_Click" CausesValidation="False">
                            </asp:ImageButton>
                            Page<asp:TextBox ID="txtPage1" runat="server" Width="34px">1</asp:TextBox>
                            <%--<asp:ImageButton ID="imgbtnPageNumb1" runat="server" ImageUrl="../../../Images/butgo.gif"
                                EnableViewState="False"></asp:ImageButton>--%>
                            <asp:Button ID="btnPageNumb1" runat="server" Text="Go" CssClass="small buttongo blue" EnableViewState="False"/>
                            <asp:RangeValidator ID="rgvGo1" runat="server" ControlToValidate="txtPage1" MinimumValue="1"
                                ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"></asp:RangeValidator>
                            <asp:RequiredFieldValidator ID="rfvGo1" runat="server" ControlToValidate="txtPage1"
                                ErrorMessage="No Halaman Salah" CssClass="validator_general" Display="Dynamic"></asp:RequiredFieldValidator>
                        </div>
                        <div class="label_gridnavigation">
                            <asp:Label ID="lblPage1" runat="server"></asp:Label>of
                            <asp:Label ID="lblTotPage1" runat="server"></asp:Label>, Total
                            <asp:Label ID="lblrecord1" runat="server"></asp:Label>record(s)
                        </div>
                    </div>
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="ButtonBack_table" runat="server" Text="Back" CssClass="small button gray"
                    CausesValidation="False"></asp:Button>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlEditContent_Table_AddEdit" runat="server">
            <div class="form_box_header">
                <div class="form_single">
                    <h4>
                        <asp:Label ID="lblAddEdit_Table" runat="server"></asp:Label>
                    </h4>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        <p>
                            Content</p>
                    </label>
                    <asp:Label ID="lblValueContent" runat="server"></asp:Label>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        <p>
                            Keterangan</p>
                    </label>
                    <asp:Label ID="lblValueDescription" runat="server"></asp:Label>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label class="label_req">
                        Score</label>
                    <asp:TextBox ID="txtScoreValue_Table" runat="server" MaxLength="3"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator6" runat="server" Display="Dynamic"
                        CssClass="validator_general" ErrorMessage="" ControlToValidate="txtScoreValue_Table"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="Rangevalidator7" runat="server" ErrorMessage="Nilai Score Salah"
                        CssClass="validator_general" MinimumValue="0" Type="integer" ControlToValidate="txtScoreValue_Table"
                        MaximumValue="100" Display="Dynamic"></asp:RangeValidator>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Status Score</label>
                    <asp:DropDownList ID="cboScoreStatus_Table" runat="server" Width="88px">
                        <asp:ListItem Value="N">Normal</asp:ListItem>
                        <asp:ListItem Value="W">Warning</asp:ListItem>
                        <asp:ListItem Value="F">Fatal</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="ButtonSave_Table" runat="server" Text="Save" CssClass="small button blue">
                </asp:Button>
                <asp:Button ID="ButtonCancel_Table" runat="server" Text="Cancel" CssClass="small button gray"
                    CausesValidation="False"></asp:Button>
            </div>
        </asp:Panel>
    </asp:Panel>
    </form>
</body>
</html>
