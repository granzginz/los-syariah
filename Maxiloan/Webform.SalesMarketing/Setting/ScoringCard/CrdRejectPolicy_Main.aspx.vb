﻿Public Class CrdRejectPolicy_Main
    Inherits Maxiloan.Webform.WebBased

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Visible = False
        Me.FormID = "CRREJECTPOLICYMAIN"
        If SessionInvalid() Then
            Exit Sub
        End If
    End Sub
    Private Sub imbSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonEdit.Click
        Response.Redirect("CRRejectPolicy_EditContent.aspx")
    End Sub

End Class