﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="MarketingScoring_MainView.aspx.vb"
    Inherits="Maxiloan.Webform.SalesMarketing.MarketingScoring_MainView" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>MarketingScoring_MainView</title>
    <script language="JavaScript" type="text/javascript">
        function fClose() {
            window.close();
            return false;
        }				
    </script>
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../../Include/General.css" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:Panel ID="pnlView" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">                   
                <h3>
                    VIEW SCORING CARD MARKETING
                </h3>
            </div>
        </div>        
        <div class="form_box">
	        <div class="form_single">
                <label><p>Skema</p></label>
                <asp:Label ID="lblSchemeID" runat="server"></asp:Label>
	        </div>
        </div> 
        <div class="form_box">
	        <div class="form_single">
                <label>Nama Skema</label>
                <asp:Label ID="lblDescription" runat="server"></asp:Label>
	        </div>
        </div>                        
        <asp:Panel ID="pnlPersonalCustomer" runat="server">
        <div class="form_box_header">
            <div class="form_single">              
                <h4>
                    PERSONAL CUSTOMER
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">              
                <h5>
                    CUSTOMER
                </h5>
            </div>
        </div> 
        <div class="form_box_header">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgPersonalCustomer" runat="server" CssClass="grid_general" BorderStyle="None" BorderWidth="0"
                            AutoGenerateColumns="False" AllowSorting="True" Width="100%" >
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <Columns>
                                <asp:TemplateColumn HeaderText="Komponen">
                                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Left" Width="30%" CssClass="tdgenap"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblComponent_PersonalCustomer" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Component")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                    <FooterStyle HorizontalAlign="Left" Width="30%" CssClass="tdgenap"></FooterStyle>
                                    <FooterTemplate>
                                        <asp:Label runat="server" ID="lblTotal" Text="Total"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Bobot (%)">
                                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Left" Width="60%" CssClass="tdganjil"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblWeight_PersonalCustomer" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Weight")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                    <FooterStyle HorizontalAlign="Left" Width="60%" CssClass="tdganjil"></FooterStyle>
                                    <FooterTemplate>
                                        <asp:Label ID="lblTotal_PersonalCustomer" runat="server"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
	        </div>
        </div>                            
        <asp:Panel ID="pnlPersonalFinancial" runat="server">
        <div class="form_box_header">
            <div class="form_single">              
                <h5>    
                    FINANCIAL
                </h5>
            </div>
        </div> 
        <div class="form_box_header">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgPersonalFinancial" runat="server" CssClass="grid_general" BorderStyle="None" BorderWidth="0"
                                AutoGenerateColumns="False" AllowSorting="True" Width="100%">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="Komponen">
                                        <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="30%" CssClass="tdgenap"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblComponent_PersonalFinancial" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Component")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <FooterStyle HorizontalAlign="Left" Width="30%" CssClass="tdgenap"></FooterStyle>
                                        <FooterTemplate>
                                            <asp:Label runat="server" ID="label" Text="Total"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Bobot (%)">
                                        <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="60%" CssClass="tdganjil"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblWeight_PersonalFinancial" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Weight")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <FooterStyle HorizontalAlign="Left" Width="60%" CssClass="tdganjil"></FooterStyle>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotal_PersonalFinancial" runat="server"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
	        </div>
        </div>                                     
        <asp:Panel ID="pnlPersonalAsset" runat="server">
        <div class="form_box_header">
            <div class="form_single">              
                <h5>    
                    ASSET
                </h5>
            </div>
        </div> 
        <div class="form_box_header">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgPersonalAsset" runat="server" CssClass="grid_general" BorderStyle="None" BorderWidth="0"
                                    AutoGenerateColumns="False" AllowSorting="True" Width="100%">
                                    <HeaderStyle CssClass="th" />
                                    <ItemStyle CssClass="item_grid" />
                                    <Columns>
                                        <asp:TemplateColumn HeaderText="Komponen">
                                            <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Left" Width="30%" CssClass="tdgenap"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblComponent_PersonalAsset" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Component")%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                            <FooterStyle HorizontalAlign="Left" Width="30%" CssClass="tdgenap"></FooterStyle>
                                            <FooterTemplate>
                                                <asp:Label runat="server" ID="Label2" Text="Total"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Bobot (%)">
                                            <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Left" Width="60%" CssClass="tdganjil"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblWeight_PersonalAsset" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Weight")%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                            <FooterStyle HorizontalAlign="Left" Width="60%" CssClass="tdganjil"></FooterStyle>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotal_PersonalAsset" runat="server"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
	        </div>
        </div>                                
        <asp:Panel ID="pnlTotal_Personal" runat="server">
        <div class="form_box">
	        <div class="form_single">
                Score Approval&nbsp;:&nbsp;
                <asp:Label ID="lblApprovalScore_Personal" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PersonalApprovedScore")%>'>
                </asp:Label>
                Score Reject&nbsp;:&nbsp;
                <asp:Label ID="lblRejectScore_Personal" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PersonalRejectScore")%>'>
                </asp:Label>
                Total&nbsp;:&nbsp;
                <asp:Label ID="lblGrandTotal_Personal" runat="server"></asp:Label>
	        </div>
        </div>                                
        </asp:Panel>
        </asp:Panel>
        </asp:Panel>
        </asp:Panel>        
        <asp:Panel ID="pnlCompanyCustomer" runat="server">
        <div class="form_box_header">
            <div class="form_single">              
                <h4>
                    CORPORATE CUSTOMER
                </h4>   
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">              
                <h5>
                    CORPORATE
                </h5>
            </div>
        </div> 
        <div class="form_box_header">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgCompanyCustomer" runat="server" CssClass="grid_general" BorderStyle="None" BorderWidth="0"
                            AutoGenerateColumns="False" AllowSorting="True" Width="100%">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <Columns>
                                <asp:TemplateColumn HeaderText="Komponen">
                                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Left" Width="30%" CssClass="tdgenap"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblComponent_CompanyCustomer" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Component")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                    <FooterStyle HorizontalAlign="Left" Width="30%" CssClass="tdgenap"></FooterStyle>
                                    <FooterTemplate>
                                        <asp:Label runat="server" ID="Label3" Text="Total"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Bobot (%)">
                                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Left" Width="60%" CssClass="tdganjil"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblWeight_CompanyCustomer" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Weight")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                    <FooterStyle HorizontalAlign="Left" Width="60%" CssClass="tdganjil"></FooterStyle>
                                    <FooterTemplate>
                                        <asp:Label ID="lblTotal_CompanyCustomer" runat="server"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
	        </div>
        </div>                               
        <asp:Panel ID="pnlCompanyFinancial" runat="server">
        <div class="form_box_header">
            <div class="form_single">              
                <h5>
                    Financial
                </h5>
            </div>
        </div> 
        <div class="form_box_header">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgCompanyFinancial" runat="server" CssClass="grid_general" BorderStyle="None" BorderWidth="0"
                                AutoGenerateColumns="False" AllowSorting="True" Width="100%">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="Komponen">
                                        <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="30%" CssClass="tdgenap"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblComponent_CompanyFinancial" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Component")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <FooterStyle HorizontalAlign="Left" Width="30%" CssClass="tdgenap"></FooterStyle>
                                        <FooterTemplate>
                                            <asp:Label runat="server" ID="Label4" Text="Total"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Bobot (%)">
                                        <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="60%" CssClass="tdganjil"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblWeight_CompanyFinancial" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Weight")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <FooterStyle HorizontalAlign="Left" Width="60%" CssClass="tdganjil"></FooterStyle>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotal_CompanyFinancial" runat="server"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
	        </div>
        </div>      
        <asp:Panel ID="pnlCompanyAsset" runat="server">
        <div class="form_box_header">
            <div class="form_single">              
                <h5>
                    ASSET
                </h5>
            </div>
        </div> 
        <div class="form_box_header">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgCompanyAsset" runat="server" CssClass="grid_general" BorderStyle="None" BorderWidth="0"
                                    AutoGenerateColumns="False" AllowSorting="True" Width="100%">
                                    <HeaderStyle CssClass="th" />
                                    <ItemStyle CssClass="item_grid" />
                                    <Columns>
                                        <asp:TemplateColumn HeaderText="Komponen">
                                            <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Left" Width="30%" CssClass="tdgenap"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblComponent_CompanyAsset" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Component")%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                            <FooterStyle HorizontalAlign="Left" Width="30%" CssClass="tdgenap"></FooterStyle>
                                            <FooterTemplate>
                                                <asp:Label runat="server" ID="Label5" Text="Total"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Bobot (%)">
                                            <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Left" Width="60%" CssClass="tdganjil"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblWeight_CompanyAsset" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Weight")%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                            <FooterStyle HorizontalAlign="Left" Width="60%" CssClass="tdganjil"></FooterStyle>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotal_CompanyAsset" runat="server"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
	        </div>
        </div>                                   
        </asp:Panel>
        </asp:Panel>
        </asp:Panel>
        <asp:Panel ID="pnlTotal_Company" runat="server">
        <div class="form_box">
	        <div class="form_single">
                Score Approval&nbsp;:&nbsp;
                <asp:Label ID="lblApprovalScore_Company" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CompanyApprovedScore")%>'>
                </asp:Label>
                Score Reject&nbsp;:&nbsp;
                <asp:Label ID="lblRejectScore_Company" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CompanyRejectScore")%>'>
                </asp:Label>
                Total&nbsp;:&nbsp;
                <asp:Label ID="lblGrandTotal_Company" runat="server"></asp:Label>
	        </div>
        </div>                       
        </asp:Panel>
        <div class="form_button">            
            <asp:Button ID="ButtonClose" OnClientClick="fClose();" runat="server" CausesValidation="false" Text="Close" CssClass ="small button gray">
            </asp:Button>     
        </div>     
    </asp:Panel>
    </form>
</body>
</html>
