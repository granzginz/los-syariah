﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="MarketingScoring_EditContent.aspx.vb"
    Inherits="Maxiloan.Webform.SalesMarketing.MarketingScoring_EditContent" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>MarketingScoring_EditContent</title>
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../../Include/General.css" type="text/css" />
    <script language="JavaScript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;

        function fClose() {
            window.close();
            return false;
        }

        function DeleteConfirm() {
            if (confirm("Apakah yakin mau hapus data ini ?")) {
                return true;
            }
            else {
                return false;
            }
        }


        function OpenWinSchemeView(pCreditScoreSchemeID) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.SalesMarketing/Setting/ScoringCard/MarketingScoring_MainView.aspx?CreditScoreSchemeID=' + pCreditScoreSchemeID, 'CreditScoringView', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=1');
        }			
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                EDIT - SCORE COMPONENT MARKETING
            </h3>
        </div>
    </div>
    <asp:Panel ID="pnlView" runat="server">
        <div class="form_box">
            <div class="form_single">
                <p>
                    <label>
                        ID Skema</label></p>
                <asp:Label ID="lblSchemeID" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Nama Skema</label>
                <asp:Label ID="lblDescription" runat="server"></asp:Label>
            </div>
        </div>
        <asp:Panel ID="pnlPersonalCustomer" runat="server">
            <div class="form_box_header">
                <div class="form_single">
                    <h4>
                        PERSONAL COMPUTER
                    </h4>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <h5>
                        CUSTOMER
                    </h5>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ns">
                        <asp:DataGrid ID="dtgPersonalCustomer" runat="server" AutoGenerateColumns="False"
                            AllowSorting="True" Width="100%" CssClass="grid_general" BorderStyle="None" BorderWidth="0">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <Columns>
                                <asp:TemplateColumn HeaderText="Edit Content">
                                    <ItemStyle CssClass="command_col"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" ImageUrl="../../../images/iconedit.gif"
                                            CommandName="EditContent"></asp:ImageButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Komponen">
                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblComponent_PersonalCustomer" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Component")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label runat="server" ID="lblTotal" Text="Total"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Bobot (%)">
                                    <ItemTemplate>
                                        <asp:Label ID="lblWeight_PersonalCustomer" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Weight")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblTotal_PersonalCustomer" runat="server"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="CalculationType" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCalculationType_PersonalCustomer" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CalculationType")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="PembiayaanScoreComponentID" Visible="False">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCreditScoreComponentID_PersonalCustomer" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CreditScoreComponentID")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                    </div>
                </div>
            </div>
            <asp:Panel ID="pnlPersonalFinancial" runat="server">
                <div class="form_box_header">
                    <div class="form_single">
                        <h5>
                            FINANCIAL
                        </h5>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtgPersonalFinancial" runat="server" AutoGenerateColumns="False"
                                AllowSorting="True" Width="100%" CssClass="grid_general" BorderStyle="None" BorderWidth="0">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="Edit Content">
                                        <ItemStyle CssClass="command_col"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="Imagebutton1" runat="server" CausesValidation="False" ImageUrl="../../../images/iconedit.gif"
                                                CommandName="EditContent"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Komponen">
                                        <ItemTemplate>
                                            <asp:Label ID="lblComponent_PersonalFinancial" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Component")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label runat="server" ID="label" Text="Total"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Bobot (%)">
                                        <ItemTemplate>
                                            <asp:Label ID="lblWeight_PersonalFinancial" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Weight")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotal_PersonalFinancial" runat="server"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="CalculationType" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCalculationType_PersonalFinancial" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CalculationType")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PembiayaanScoreComponentID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCreditScoreComponentID_PersonalFinancial" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CreditScoreComponentID")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                        </div>
                    </div>
                </div>
                <asp:Panel ID="pnlPersonalAsset" runat="server">
                    <div class="form_box_header">
                        <div class="form_single">
                            <h5>
                                ASSET
                            </h5>
                        </div>
                    </div>
                    <div class="form_box_header">
                        <div class="form_single">
                            <div class="grid_wrapper_ns">
                                <asp:DataGrid ID="dtgPersonalAsset" runat="server" AutoGenerateColumns="False" AllowSorting="True"
                                    Width="100%" CssClass="grid_general" BorderStyle="None" BorderWidth="0">
                                    <HeaderStyle CssClass="th" />
                                    <ItemStyle CssClass="item_grid" />
                                    <Columns>
                                        <asp:TemplateColumn HeaderText="Edit Content">
                                            <ItemStyle CssClass="command_col"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:ImageButton ID="Imagebutton2" runat="server" CausesValidation="False" ImageUrl="../../../images/iconedit.gif"
                                                    CommandName="EditContent"></asp:ImageButton>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Komponen">
                                            <ItemTemplate>
                                                <asp:Label ID="lblComponent_PersonalAsset" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Component")%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label runat="server" ID="Label2" Text="Total"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Bobot (%)">
                                            <ItemTemplate>
                                                <asp:Label ID="lblWeight_PersonalAsset" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Weight")%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotal_PersonalAsset" runat="server"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="CalculationType" Visible="False">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCalculationType_PersonalAsset" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CalculationType")%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="PembiayaanScoreComponentID" Visible="False">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCreditScoreComponentID_PersonalAsset" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CreditScoreComponentID")%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </div>
                        </div>
                    </div>
                    <asp:Panel ID="pnlTotal_Personal" runat="server">
                        <div class="form_box">
                            <div class="form_single">
                                Score Approval&nbsp;:&nbsp;
                                <asp:Label ID="lblApprovalScore_Personal" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PersonalApprovedScore")%>'>
                                </asp:Label>
                                Score Reject&nbsp;:&nbsp;
                                <asp:Label ID="lblRejectScore_Personal" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PersonalRejectScore")%>'>
                                </asp:Label>
                                Total&nbsp;:&nbsp;
                                <asp:Label ID="lblGrandTotal_Personal" runat="server"></asp:Label>
                            </div>
                        </div>
                    </asp:Panel>
                </asp:Panel>
            </asp:Panel>
        </asp:Panel>
        <asp:Panel ID="pnlCompanyCustomer" runat="server" Visible="false">
            <div class="form_box_header">
                <div class="form_single">
                    <h4>
                        CORPORATE CUSTOMER
                    </h4>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <h5>
                        CORPORATE
                    </h5>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ns">
                        <asp:DataGrid ID="dtgCompanyCustomer" runat="server" AutoGenerateColumns="False"
                            AllowSorting="True" Width="100%" CssClass="grid_general" BorderStyle="None" BorderWidth="0">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <Columns>
                                <asp:TemplateColumn HeaderText="Edit Content">
                                    <ItemStyle CssClass="command_col"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="Imagebutton3" runat="server" CausesValidation="False" ImageUrl="../../../images/iconedit.gif"
                                            CommandName="EditContent"></asp:ImageButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Komponen">
                                    <ItemTemplate>
                                        <asp:Label ID="lblComponent_CompanyCustomer" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Component")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label runat="server" ID="Label3" Text="Total"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Bobot (%)">
                                    <ItemTemplate>
                                        <asp:Label ID="lblWeight_CompanyCustomer" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Weight")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblTotal_CompanyCustomer" runat="server"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="CalculationType" Visible="False">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCalculationType_CompanyCustomer" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CalculationType")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="PembiayaanScoreComponentID" Visible="False">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCreditScoreComponentID_CompanyCustomer" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CreditScoreComponentID")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                    </div>
                </div>
            </div>
            <asp:Panel ID="pnlCompanyFinancial" runat="server">
                <div class="form_box_header">
                    <div class="form_single">
                        <h5>
                            FINANCIAL
                        </h5>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtgCompanyFinancial" runat="server" AutoGenerateColumns="False"
                                AllowSorting="True" Width="100%" CssClass="grid_general" BorderStyle="None" BorderWidth="0">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="Edit Content">
                                        <ItemStyle CssClass="command_col"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="Imagebutton4" runat="server" CausesValidation="False" ImageUrl="../../../images/iconedit.gif"
                                                CommandName="EditContent"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Komponen">
                                        <ItemTemplate>
                                            <asp:Label ID="lblComponent_CompanyFinancial" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Component")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label runat="server" ID="Label4" Text="Total"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Bobot (%)">
                                        <ItemTemplate>
                                            <asp:Label ID="lblWeight_CompanyFinancial" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Weight")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotal_CompanyFinancial" runat="server"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="CalculationType" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCalculationType_CompanyFinancial" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CalculationType")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PembiayaanScoreComponentID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCreditScoreComponentID_CompanyFinancial" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CreditScoreComponentID")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                        </div>
                    </div>
                </div>
                <asp:Panel ID="pnlCompanyAsset" runat="server">
                    <div class="form_box_header">
                        <div class="form_single">
                            <h5>
                                ASSET
                            </h5>
                        </div>
                    </div>
                    <div class="form_box_header">
                        <div class="form_single">
                            <div class="grid_wrapper_ns">
                                <asp:DataGrid ID="dtgCompanyAsset" runat="server" AutoGenerateColumns="False" AllowSorting="True"
                                    Width="100%" CssClass="grid_general" BorderStyle="None" BorderWidth="0">
                                    <HeaderStyle CssClass="th" />
                                    <ItemStyle CssClass="item_grid" />
                                    <Columns>
                                        <asp:TemplateColumn HeaderText="Edit Content">
                                            <ItemStyle CssClass="command_col"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:ImageButton ID="Imagebutton5" runat="server" CausesValidation="False" ImageUrl="../../../images/iconedit.gif"
                                                    CommandName="EditContent"></asp:ImageButton>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Komponen">
                                            <ItemTemplate>
                                                <asp:Label ID="lblComponent_CompanyAsset" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Component")%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label runat="server" ID="Label5" Text="Total"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Bobot (%)">
                                            <ItemTemplate>
                                                <asp:Label ID="lblWeight_CompanyAsset" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Weight")%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotal_CompanyAsset" runat="server"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="CalculationType" Visible="False">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCalculationType_CompanyAsset" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CalculationType")%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="PembiayaanScoreComponentID" Visible="False">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCreditScoreComponentID_CompanyAsset" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CreditScoreComponentID")%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
            </asp:Panel>
        </asp:Panel>
        <asp:Panel ID="pnlTotal_Company" runat="server" Visible="false">
            <div class="form_box">
                <div class="form_single">
                    Score Approval&nbsp;:&nbsp;
                    <asp:Label ID="lblApprovalScore_Company" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CompanyApprovedScore")%>'>
                    </asp:Label>
                    Score Reject&nbsp;:&nbsp;
                    <asp:Label ID="lblRejectScore_Company" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CompanyRejectScore")%>'>
                    </asp:Label>
                    Total&nbsp;:&nbsp;
                    <asp:Label ID="lblGrandTotal_Company" runat="server"></asp:Label>
                </div>
            </div>
        </asp:Panel>
        <div class="form_button">
            <asp:Button ID="ButtonOK" runat="server" CausesValidation="false" Text="Ok" CssClass="small button blue">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlEditContent_Range" runat="server">
        <asp:Panel ID="pnlEditContent_Range_List" runat="server">
            <div class="form_box_header">
                <div class="form_single">
                    <h3>
                        SCORE COMPONENT CONTENT MARKETING
                    </h3>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <p>
                        <label>
                            ID Skema</label></p>
                    <asp:HyperLink ID="hplSchemeID" runat="server"></asp:HyperLink>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Nama Skema</label>
                    <asp:Label ID="lblDescription_Range" runat="server"></asp:Label>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Komponen</label>
                    <asp:Label ID="lblComponent_Range" runat="server"></asp:Label>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlEditContent_Range_Datagrid" runat="server">
            <div class="form_box_header">
                <div class="form_single">
                    <h4>
                        DAFTAR SCORE COMPONENT CONTENT MARKETING
                    </h4>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ns">
                        <asp:DataGrid ID="dtgEditContent_Range" runat="server" CssClass="grid_general" BorderStyle="None"
                            BorderWidth="0" AutoGenerateColumns="False" AllowSorting="True" Width="100%"
                            OnSortCommand="Sorting">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <Columns>
                                <asp:TemplateColumn HeaderText="EDIT">
                                    <ItemStyle CssClass="command_col"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgEditRange" runat="server" CausesValidation="False" ImageUrl="../../../images/iconedit.gif"
                                            CommandName="EditRange"></asp:ImageButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="DELETE">
                                    <ItemStyle CssClass="command_col"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgDeleteRange" runat="server" CausesValidation="False" ImageUrl="../../../images/icondelete.gif"
                                            CommandName="DeleteRange"></asp:ImageButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="Content" HeaderText="CONTENT">
                                    <ItemTemplate>
                                        <asp:Label ID="lblContent" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Content")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="ScoreValue" HeaderText="SCORE">
                                    <ItemTemplate>
                                        <asp:Label ID="lblScore" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ScoreValue")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="ScoreStatus" HeaderText="STATUS SCORE">
                                    <ItemTemplate>
                                        <asp:Label ID="lblScoreStatus" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ScoreStatus")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn Visible="false" HeaderText="ContentSeqNo">
                                    <ItemTemplate>
                                        <asp:Label ID="lblContentSeqNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ContentSeqNo")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                         <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../../Images/grid_navbutton01.png"
                        CausesValidation="False" CommandName="First" OnCommand="NavigationLink_Click">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../../Images/grid_navbutton02.png"
                        CausesValidation="False" CommandName="Prev" OnCommand="NavigationLink_Click">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../../Images/grid_navbutton03.png"
                        CausesValidation="False" CommandName="Next" OnCommand="NavigationLink_Click">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../../Images/grid_navbutton04.png"
                        CausesValidation="False" CommandName="Last" OnCommand="NavigationLink_Click">
                    </asp:ImageButton>
                    Page
                    <asp:TextBox ID="txtPage" runat="server" CssClass="small_text">1</asp:TextBox>
                    <asp:Button ID="btnGoPage" runat="server" CssClass="small buttongo blue" Text="Go"
                        EnableViewState="False" />
                    <asp:RangeValidator ID="Rangevalidator4" runat="server" Type="Integer" MaximumValue="999999999"
                        ErrorMessage="No Halaman Salah" MinimumValue="1" ControlToValidate="txtPage"
                        CssClass="validator_general"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo" runat="server" ErrorMessage="No Halaman Salah"
                        ControlToValidate="txtPage" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="rgvGo" runat="server" ErrorMessage="No Halaman Salah" MinimumValue="1"
                        Type="integer" ControlToValidate="txtPage" CssClass="validator_general"></asp:RangeValidator>
                </div>
                <div class="label_gridnavigation">
                    <asp:Label ID="lblPage" runat="server"></asp:Label>of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                    <asp:Label ID="lblTotRec" runat="server"></asp:Label>
                    record(s)
                </div>
                    </div>
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="ButtonAdd" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue">
                </asp:Button>
                <asp:Button ID="ButtonBack" runat="server" CausesValidation="False" Text="Back" CssClass="small button gray">
                </asp:Button>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlEditContent_Range_AddEdit" runat="server">
            <div class="form_box_header">
                <div class="form_single">
                    <h4>
                        SCORE COMPONENT CONTENT MARKETING -&nbsp;
                        <asp:Label ID="lblAddEdit" runat="server"></asp:Label>
                    </h4>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <p>
                        <label class="label_req">
                            Content</label></p>
                    <asp:TextBox ID="txtValueFrom" runat="server" MaxLength="15"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator4" runat="server" ControlToValidate="txtValueFrom"
                        CssClass="validator_general" ErrorMessage="" Display="Dynamic"></asp:RequiredFieldValidator>&nbsp;to
                    <asp:TextBox ID="txtValueTo" runat="server" MaxLength="15"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" ControlToValidate="txtValueTo"
                        CssClass="validator_general" ErrorMessage="" Display="Dynamic"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="Rangevalidator1" runat="server" ControlToValidate="txtValueFrom"
                        CssClass="validator_general" Type="double" MinimumValue="0" ErrorMessage="Nilai DARI Salah"
                        MaximumValue="999999999999999" Display="Dynamic"></asp:RangeValidator>
                    <asp:RangeValidator ID="Rangevalidator2" runat="server" ControlToValidate="txtValueTo"
                        Type="double" MinimumValue="0" ErrorMessage="Nilai KE Salah" MaximumValue="999999999999999"
                        CssClass="validator_general" Display="Dynamic"></asp:RangeValidator>
                    <asp:CompareValidator ID="cmv" runat="server" ControlToValidate="txtValueFrom" ErrorMessage="Nilai DARI harus lebih kecil dari nilai KE"
                        CssClass="validator_general" Display="Dynamic" Operator="LessThan"   type="Integer" CultureInvariantValues="True"  ControlToCompare="txtValueTo"></asp:CompareValidator>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label class="label_req">
                        Score</label>
                    <asp:TextBox ID="txtScoreValue" runat="server" MaxLength="3"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" ControlToValidate="txtScoreValue"
                        CssClass="validator_general" ErrorMessage="" Display="Dynamic"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="Rangevalidator3" runat="server" ControlToValidate="txtScoreValue"
                        CssClass="validator_general" Type="integer" MinimumValue="0" ErrorMessage="Score harus antara 0 s/d 100"
                        MaximumValue="100" Display="Dynamic"></asp:RangeValidator>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Status Score</label>
                    <asp:DropDownList ID="cboScoreStatus" runat="server" Width="88px">
                        <asp:ListItem Value="N">Normal</asp:ListItem>
                        <asp:ListItem Value="W">Warning</asp:ListItem>
                        <asp:ListItem Value="F">Fatal</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="ButtonSave" runat="server" CausesValidation="true" Text="Save" CssClass="small button blue">
                </asp:Button>
                <asp:Button ID="ButtonCancel" runat="server" CausesValidation="false" Text="Cancel"
                    CssClass="small button gray"></asp:Button>
            </div>
        </asp:Panel>
    </asp:Panel>
    <asp:Panel ID="pnlEditContent_Table" runat="server">
        <asp:Panel ID="pnlEditContent_Tabel_List" runat="server">
            <div class="form_box_header">
                <div class="form_single">
                    <h3>
                        SCORE COMPONENT CONTENT MARKETING
                    </h3>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <p>
                        <label>
                            ID Skema</label></p>
                    <asp:HyperLink ID="hplSchemeID_Table" runat="server"></asp:HyperLink>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Nama Skema</label>
                    <asp:Label ID="lblDescription_Table" runat="server"></asp:Label>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Komponen</label>
                    <asp:Label ID="lblComponent_Table" runat="server"></asp:Label>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlEditContent_Table_Datagrid" runat="server">
            <div class="form_box_header">
                <div class="form_single">
                    <h4>
                        DAFTAR SCORE COMPONENT CONTENT MARKETING
                    </h4>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ns">
                        <asp:DataGrid ID="dtgEditContent_Table" runat="server" CssClass="grid_general" BorderStyle="None"
                            BorderWidth="0" AutoGenerateColumns="False" AllowSorting="True" Width="100%"
                            OnSortCommand="Sorting1">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <Columns>
                                <asp:TemplateColumn HeaderText="EDIT">
                                    <ItemStyle CssClass="command_col"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgEdit_Table" runat="server" CausesValidation="False" ImageUrl="../../../images/iconedit.gif"
                                            CommandName="EditTable"></asp:ImageButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="ValueContent" HeaderText="CONTENT">
                                    <ItemTemplate>
                                        <asp:Label ID="lblValueContent_Table" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ValueContent")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="ValueDescription" HeaderText="DESCRIPTION">
                                    <ItemTemplate>
                                        <asp:Label ID="lblValueDescription_Table" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ValueDescription")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="ScoreValue" HeaderText="SCORE">
                                    <ItemTemplate>
                                        <asp:Label ID="lblScoreValue_Table" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ScoreValue")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="ScoreStatus" HeaderText="STATUS SCORE">
                                    <ItemTemplate>
                                        <asp:Label ID="lblScoreStatus_Table" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ScoreStatus")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn Visible="false" HeaderText="ContentSeqNo">
                                    <ItemTemplate>
                                        <asp:Label ID="lblContentSeqNo_Table" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ContentSeqNo")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                        <%--<div class="button_gridnavigation">
                            <asp:ImageButton ID="imbFirstPage1" runat="server" ImageUrl="../../../Images/grid_navbutton01.png"
                                CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbPrevPage1" runat="server" ImageUrl="../../../Images/grid_navbutton02.png"
                                CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbNextPage1" runat="server" ImageUrl="../../../Images/grid_navbutton03.png"
                                CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbLastPage1" runat="server" ImageUrl="../../../Images/grid_navbutton04.png"
                                CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                            </asp:ImageButton>
                            <asp:TextBox ID="txtPage1" runat="server">1</asp:TextBox>
                            <asp:Button ID="btnPageNumb1" runat="server" Text="Go" CssClass="small buttongo blue"
                                EnableViewState="False"></asp:Button>
                            <asp:RangeValidator ID="rgvGo1" runat="server" ControlToValidate="txtPage1" MinimumValue="1"
                                ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"></asp:RangeValidator>
                            <asp:RequiredFieldValidator ID="rfvGo1" runat="server" ControlToValidate="txtPage1"
                                ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        </div>
                        <div class="label_gridnavigation">
                            <asp:Label ID="lblPage1" runat="server"></asp:Label>of
                            <asp:Label ID="lblTotPage1" runat="server"></asp:Label>, Total
                            <asp:Label ID="lblrecord1" runat="server"></asp:Label>record(s)
                        </div>--%>

                        <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPage1" runat="server" ImageUrl="../../../Images/grid_navbutton01.png"
                        CausesValidation="False" CommandName="First" OnCommand="NavigationLink1_Click">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage1" runat="server" ImageUrl="../../../Images/grid_navbutton02.png"
                        CausesValidation="False" CommandName="Prev" OnCommand="NavigationLink1_Click">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage1" runat="server" ImageUrl="../../../Images/grid_navbutton03.png"
                        CausesValidation="False" CommandName="Next" OnCommand="NavigationLink1_Click">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage1" runat="server" ImageUrl="../../../Images/grid_navbutton04.png"
                        CausesValidation="False" CommandName="Last" OnCommand="NavigationLink1_Click">
                    </asp:ImageButton>
                    Page
                    <asp:TextBox ID="txtPage1" runat="server" CssClass="small_text">1</asp:TextBox>
                    <asp:Button ID="btnPageNumb1" runat="server" CssClass="small buttongo blue" Text="Go"
                        EnableViewState="False" />
                   <asp:RangeValidator ID="Rangevalidator5" runat="server" Type="Integer" MaximumValue="999999999"
                        ErrorMessage="No Halaman Salah" MinimumValue="1" ControlToValidate="txtPage1"
                        CssClass="validator_general"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo1" runat="server" ErrorMessage="No Halaman Salah"
                        ControlToValidate="txtPage1" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="rgvGo1" runat="server" ErrorMessage="No Halaman Salah" MinimumValue="1"
                        Type="integer" ControlToValidate="txtPage1" CssClass="validator_general"></asp:RangeValidator>
                </div>
                <div class="label_gridnavigation">
                    <asp:Label ID="lblPage1" runat="server"></asp:Label>of
                    <asp:Label ID="lblTotPage1" runat="server"></asp:Label>, Total
                    <asp:Label ID="lblTotRec1" runat="server"></asp:Label>
                    record(s)
                </div>
                    </div>
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="ButtonBack_Table" runat="server" CausesValidation="false" Text="Back"
                    CssClass="small button gray"></asp:Button>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlEditContent_Table_AddEdit" runat="server">
            <div class="form_box_header">
                <div class="form_single">
                    <h4>
                        SCORE COMPONENT CONTENT MARKETING -&nbsp;
                        <asp:Label ID="lblAddEdit_Table" runat="server"></asp:Label>
                    </h4>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <p>
                        <label>
                            Content</label></p>
                    <asp:Label ID="lblValueContent" runat="server"></asp:Label>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <p>
                        <label>
                            Description</label></p>
                    <asp:Label ID="lblValueDescription" runat="server"></asp:Label>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label class="label_req">
                        Score</label>
                    <asp:TextBox ID="txtScoreValue_Table" runat="server" MaxLength="3"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator6" runat="server" ControlToValidate="txtScoreValue_Table"
                        CssClass="validator_general" ErrorMessage="" Display="Dynamic"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="Rangevalidator7" runat="server" ControlToValidate="txtScoreValue_Table"
                        CssClass="validator_general" Type="integer" MinimumValue="0" ErrorMessage="Score Salah"
                        MaximumValue="100" Display="Dynamic"></asp:RangeValidator>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Status Score</label>
                    <asp:DropDownList ID="cboScoreStatus_Table" runat="server" Width="88px">
                        <asp:ListItem Value="N">Normal</asp:ListItem>
                        <asp:ListItem Value="W">Warning</asp:ListItem>
                        <asp:ListItem Value="F">Fatal</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="ButtonSave_Table" runat="server" CausesValidation="true" Text="Save"
                    CssClass="small button blue"></asp:Button>
                <asp:Button ID="ButtonCancel_Table" runat="server" CausesValidation="false" Text="Cancel"
                    CssClass="small button gray"></asp:Button>
            </div>
        </asp:Panel>
    </asp:Panel>
    </form>
</body>
</html>
