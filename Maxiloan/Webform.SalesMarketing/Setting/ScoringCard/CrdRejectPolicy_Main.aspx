﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CrdRejectPolicy_Main.aspx.vb" Inherits="Maxiloan.Webform.SalesMarketing.CrdRejectPolicy_Main" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   <title>Credit RejectPolicy_Main</title>
     <script src="../../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../../Include/Buttons.css" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:UpdateProgress ID="upg1" runat="server" AssociatedUpdatePanelID="updatePanel1"><ProgressTemplate><div class="progress_bg"><label class="progress_img" /></div></ProgressTemplate></asp:UpdateProgress>
    <form id="form2" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" />

    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false"  ToolTip="Click to close" onclick="hideMessage();" />

            <div class="form_title">
                <div class="title_strip"> </div>
                <div class="form_single">
                    <h3>DAFTAR PEMBIAYAAN SCORING POLICY</h3>
                </div>
            </div>

            <div class="form_box">
                <div class="form_single">
                    <label  >SKEMA </label> <asp:Label ID="lblSchemeID" runat="server">CMMOBIL</asp:Label>
                </div>
            </div>

            <div class="form_box">
                <div class="form_single">
                    <label  >NAMA SKEMA </label> <asp:Label ID="Label1" runat="server">PEMBIAYAAN SCORE REJECT POLICY</asp:Label>
                </div>
            </div>


             <div class="form_button">
                <asp:Button ID="ButtonEdit" runat="server" CausesValidation="False" Text="EDIT CONTENT" CssClass="small button blue" />
             </div>


            </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>

