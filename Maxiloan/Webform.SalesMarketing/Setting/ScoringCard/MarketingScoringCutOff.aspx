﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="MarketingScoringCutOff.aspx.vb" Inherits="Maxiloan.Webform.SalesMarketing.MarketingScoringCutOff" %>

<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../../../Webform.UserController/UcSearchBy.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>BankMaster</title>
    <link rel="Stylesheet" type="text/css" href="../../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../../Include/Buttons.css" />
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../../include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../../js/jquery-1.9.1.min.js"></script>
    <script src="../../../js/jquery-ui-1.10.3.custom.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    &nbsp;
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlDatagrid" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h4>
                    SETUP CUT OFF SCORING CABANG
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgCutOffScore" runat="server" OnSortCommand="SortGrid" CssClass="grid_general"
                        DataKeyField="BranchID" AutoGenerateColumns="False" AllowSorting="True" AllowPaging="True">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <FooterStyle  ></FooterStyle>
                        <Columns>
                            <asp:TemplateColumn HeaderText="EDIT">
                                <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbEdit" runat="server" CausesValidation="False" ImageUrl="../../../Images/IconEdit.gif"
                                        CommandName="EDIT"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DELETE">
                                <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbDelete" runat="server" CausesValidation="False" ImageUrl="../../../Images/IconDelete.gif"
                                        CommandName="DELETE"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="ScoreSchemeID" HeaderText="Score Scheme ID">
                                <ItemTemplate>
                                    <asp:Label ID="lblScoreSchemeID" runat="server" Text='<%#Container.DataItem("ScoreSchemeID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="BranchID" HeaderText="ID CABANG">
                                <ItemTemplate>
                                    <asp:Label ID="lblBranchID" runat="server" Text='<%#Container.DataItem("BranchID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="BranchFullName" HeaderText="NAMA CABANG">
                                <ItemTemplate>
                                    <asp:Label ID="lblBranchName" runat="server" Text='<%#Container.DataItem("BranchFullName")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Package" HeaderText="PAKET">
                                <ItemTemplate>
                                    <%--<asp:Label ID="lblPackage" runat="server" Text='<%#Iif(container.dataitem("Package")="N", "NON SUBSIDI",Iif(container.dataitem("Package")="P", "SUBSIDI"))%>'>--%>
                                    <asp:Label ID="lblPackage" runat="server" Text='<%#ProcessMyDataItem(Eval("Package"))%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="CutOffScoreNewCustomer" HeaderText="CUT OFF SCORE New Customer">
                                <ItemTemplate>
                                    <asp:Label ID="lblCutOffScoreNewCustomer" runat="server" Text='<%#Container.DataItem("CutOffScoreNewCustomer")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="CutOffScoreExistCustomer" HeaderText="CUT OFF SCORE Existing Customer">
                                <ItemTemplate>
                                    <asp:Label ID="lblCutOffScoreExistCustomer" runat="server" Text='<%#Container.DataItem("CutOffScoreExistCustomer")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                        <PagerStyle Visible="False" HorizontalAlign="Left"  
                            Mode="NumericPages"></PagerStyle>
                    </asp:DataGrid>
                    <div class="button_gridnavigation">
                        <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../../Images/grid_navbutton01.png"
                            CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="First">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../../Images/grid_navbutton02.png"
                            CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Prev">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../../Images/grid_navbutton03.png"
                            CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Next">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../../Images/grid_navbutton04.png"
                            CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Last">
                        </asp:ImageButton>
                        Page&nbsp;
                        <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                        <asp:Button ID="imgbtnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                            EnableViewState="False"></asp:Button>
                        <asp:RangeValidator ID="rgvGo" runat="server" ErrorMessage="No Halaman Salah" ControlToValidate="txtPage"
                            Type="Integer" MaximumValue="999999999" MinimumValue="1" CssClass="validator_general"
                            Display="Dynamic"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator4" runat="server" Display="Dynamic"
                            ErrorMessage="No Halaman Salah" ControlToValidate="txtPage" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                    <div class="label_gridnavigation">
                        <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                        <asp:Label ID="lblrecord" runat="server"></asp:Label>&nbsp;record(s)
                    </div>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="imbAdd" runat="server" Text="Add" CssClass="small button blue"
                CausesValidation="False"></asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    CABANG
                </h4>
            </div>
        </div>
        <div class="form_box_uc">
            <uc1:ucsearchby id="oSearchBy" runat="server"></uc1:ucsearchby>
        </div>
        <div class="form_button">
            <asp:Button ID="imgsearch" runat="server" Text="Search" CssClass="small button blue">
            </asp:Button>
            &nbsp;
            <asp:Button ID="imbReset" runat="server" Text="Reset" CssClass="small button gray"
                CausesValidation="False"></asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlAdd" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    <asp:Label ID="lblTitle" runat="server"></asp:Label>
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    Score Scheme ID
                </label>
                <asp:TextBox ID="txtScoreSchemeID" runat="server" MaxLength="7"> </asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" ErrorMessage="Harap isi Score Scheme ID"
                    InitialValue="" ControlToValidate="txtScoreSchemeID" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
            <div class="form_box">
                <div class="form_single">
                        <div style="float:left">
                        <label class="label_req">
                            ID Cabang</label>                     
                        <asp:HiddenField runat="server" ID="hdfBranchId" />                                       
                        <asp:HiddenField runat="server" ID="hdfBranchName" />     
                        <asp:HiddenField runat="server" ID="hdfAssetTypeID" />     

                            <asp:TextBox runat="server" ID="txtBranchName" Enabled="false" CssClass="medium_text"></asp:TextBox>
                        </div>
                        <div style="margin-top:2px">
                            <asp:Panel ID="pnlLookupBranchID" runat="server">
                            <button class="small buttongo blue"                             
                            onclick ="OpenJLookup('<%= ResolveClientUrl("~/webform.Utility/jLookup/LookupBranch.aspx?kode=" & hdfBranchId.ClientID & "&nama=" & txtBranchName.ClientID & "&AssetTypeID=" & hdfAssetTypeID.Value)%>','Daftar Supplier','<%= pnlAdd.ClientID%>');return false;">...</button>                                                                            
                            </asp:Panel>
                        </div> 
                 </div>
            </div>
<%--        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    Nama Cabang
                </label>
                <asp:TextBox ID="txtBranchName" runat="server" Width="475px"  MaxLength="50" enabled=true> </asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator3" runat="server" ErrorMessage="Harap isi Nama Cabang"
                    ControlToValidate="txtBranchName" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>--%>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    Paket
                </label>
                <asp:DropDownList ID="cboPackage" runat="server">
                    <asp:ListItem Text="SUBSIDI" Value="S" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="NON SUBSIDI" Value="N" ></asp:ListItem>
                    <asp:ListItem Text="PICK UP" Value="P" ></asp:ListItem>
                    <asp:ListItem Text="AGRICULTURE" Value="A" ></asp:ListItem>
                    <asp:ListItem Text="OTHER (PASSENGER)" Value="O" ></asp:ListItem>        
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" Display="Dynamic"
                            InitialValue="" ControlToValidate="cboPackage" ErrorMessage="Harap pilih Paket"
                            CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    CutOffScore - NewCustomer
                </label>
                <asp:TextBox ID="txtCutOffScoreNewCustomer" runat="server"  MaxLength="7" ></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator5" runat="server" ErrorMessage="Harap Isi CutOffScore - NewCustomer"
                    ControlToValidate="txtCutOffScoreNewCustomer" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    CutOffScore - ExistCustomer
                </label>
                <asp:TextBox ID="txtCutOffScoreExistCustomer" runat="server"  MaxLength="7" ></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator6" runat="server" ErrorMessage="Harap Isi CutOffScore - ExistCustomer"
                    ControlToValidate="txtCutOffScoreExistCustomer" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSaveAdd" runat="server" Text="Save" CssClass="small button blue"
                CausesValidation="True"></asp:Button>
            <asp:Button ID="btnCancelAdd" runat="server" Text="Cancel" CssClass="small button gray"
                CausesValidation="False"></asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
