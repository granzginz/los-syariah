﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CreditScoring_Main.aspx.vb"
    Inherits="Maxiloan.Webform.SalesMarketing.CreditScoring_Main" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>CreditScoring_Main</title>
    <script language="JavaScript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;
        // PersonalCustomer
        function EnableTextBoxPersonalCustomer(pItemCount, pChkBoxID, pTextBoxID) {
            var oTextBox = eval('document.forms[0].' + pTextBoxID);
            var oCheckBox = eval('document.forms[0].' + pChkBoxID);
            if (oCheckBox.checked == true) {
                oTextBox.disabled = false;
            }
            if (oCheckBox.checked == false) {
                oTextBox.disabled = true;
                document.forms[0].txtTotal_PersonalCustomer.value = document.forms[0].txtTotal_PersonalCustomer.value - oTextBox.value;
                oTextBox.value = 0;
            }
        }
        function TotalPersonalCustomer(pItemCount, pTextBoxID, pMyTotalID) {
            var myID;
            var total = 0;
            for (var i = 1; i < pItemCount; i++) {
                myID = 'dtgPersonalCustomer__ctl' + (i + 1) + '_txtWeight_PersonalCustomer';
                if (eval('document.forms[0].' + myID).disabled == false) {
                    total = parseFloat(total) + parseFloat(eval('document.forms[0].' + myID).value);
                }
            }
            document.forms[0].txtTotal_PersonalCustomer.value = total;

        }

        // PersonalAsset
        function EnableTextBoxPersonalAsset(pItemCount, pChkBoxID, pTextBoxID) {
            var oTextBox = eval('document.forms[0].' + pTextBoxID);
            var oCheckBox = eval('document.forms[0].' + pChkBoxID);
            if (oCheckBox.checked == true) {
                oTextBox.disabled = false;
            }
            if (oCheckBox.checked == false) {
                oTextBox.disabled = true;
                document.forms[0].txtTotal_PersonalAsset.value = document.forms[0].txtTotal_PersonalAsset.value - oTextBox.value;
                oTextBox.value = 0;
            }
        }
        function TotalPersonalAsset(pItemCount, pTextBoxID, pMyTotalID) {
            var myID;
            var total = 0;
            for (var i = 1; i < pItemCount; i++) {
                myID = 'dtgPersonalAsset__ctl' + (i + 1) + '_txtWeight_PersonalAsset';
                if (eval('document.forms[0].' + myID).disabled == false) {
                    total = parseFloat(total) + parseFloat(eval('document.forms[0].' + myID).value);
                }
            }
            document.forms[0].txtTotal_PersonalAsset.value = total;


        }

        // PersonalFinancial
        function EnableTextBoxPersonalFinancial(pItemCount, pChkBoxID, pTextBoxID) {
            var oTextBox = eval('document.forms[0].' + pTextBoxID);
            var oCheckBox = eval('document.forms[0].' + pChkBoxID);
            if (oCheckBox.checked == true) {
                oTextBox.disabled = false;
            }
            if (oCheckBox.checked == false) {
                oTextBox.disabled = true;
                document.forms[0].txtTotal_PersonalFinancial.value = document.forms[0].txtTotal_PersonalFinancial.value - oTextBox.value;
                oTextBox.value = 0;
            }
        }
        function TotalPersonalFinancial(pItemCount, pTextBoxID, pMyTotalID) {
            var myID;
            var total = 0;
            for (var i = 1; i < pItemCount; i++) {
                myID = 'dtgPersonalFinancial__ctl' + (i + 1) + '_txtWeight_PersonalFinancial';
                if (eval('document.forms[0].' + myID).disabled == false) {
                    total = parseFloat(total) + parseFloat(eval('document.forms[0].' + myID).value);
                }
            }
            document.forms[0].txtTotal_PersonalFinancial.value = total;


        }

        // CompanyCustomer
        function EnableTextBoxCompanyCustomer(pItemCount, pChkBoxID, pTextBoxID) {
            var oTextBox = eval('document.forms[0].' + pTextBoxID);
            var oCheckBox = eval('document.forms[0].' + pChkBoxID);
            if (oCheckBox.checked == true) {
                oTextBox.disabled = false;
            }
            if (oCheckBox.checked == false) {
                oTextBox.disabled = true;
                document.forms[0].txtTotal_CompanyCustomer.value = document.forms[0].txtTotal_CompanyCustomer.value - oTextBox.value;
                oTextBox.value = 0;
            }
        }
        function TotalCompanyCustomer(pItemCount, pTextBoxID, pMyTotalID) {
            var myID;
            var total = 0;
            for (var i = 1; i < pItemCount; i++) {
                myID = 'dtgCompanyCustomer__ctl' + (i + 1) + '_txtWeight_CompanyCustomer';
                if (eval('document.forms[0].' + myID).disabled == false) {
                    total = parseFloat(total) + parseFloat(eval('document.forms[0].' + myID).value);
                }
            }
            document.forms[0].txtTotal_CompanyCustomer.value = total;
        }


        // CompanyAsset
        function EnableTextBoxCompanyAsset(pItemCount, pChkBoxID, pTextBoxID) {
            var oTextBox = eval('document.forms[0].' + pTextBoxID);
            var oCheckBox = eval('document.forms[0].' + pChkBoxID);
            if (oCheckBox.checked == true) {
                oTextBox.disabled = false;
            }
            if (oCheckBox.checked == false) {
                oTextBox.disabled = true;
                document.forms[0].txtTotal_CompanyAsset.value = document.forms[0].txtTotal_CompanyAsset.value - oTextBox.value;
                oTextBox.value = 0;
            }
        }
        function TotalCompanyAsset(pItemCount, pTextBoxID, pMyTotalID) {
            var myID;
            var total = 0;
            for (var i = 1; i < pItemCount; i++) {
                myID = 'dtgCompanyAsset__ctl' + (i + 1) + '_txtWeight_CompanyAsset';
                if (eval('document.forms[0].' + myID).disabled == false) {
                    total = parseFloat(total) + parseFloat(eval('document.forms[0].' + myID).value);
                }
            }
            document.forms[0].txtTotal_CompanyAsset.value = total;
        }


        // CompanyFinancial
        function EnableTextBoxCompanyFinancial(pItemCount, pChkBoxID, pTextBoxID) {
            var oTextBox = eval('document.forms[0].' + pTextBoxID);
            var oCheckBox = eval('document.forms[0].' + pChkBoxID);
            if (oCheckBox.checked == true) {
                oTextBox.disabled = false;
            }
            if (oCheckBox.checked == false) {
                oTextBox.disabled = true;
                document.forms[0].txtTotal_CompanyFinancial.value = document.forms[0].txtTotal_CompanyFinancial.value - oTextBox.value;
                oTextBox.value = 0;
            }
        }
        function TotalCompanyFinancial(pItemCount, pTextBoxID, pMyTotalID) {
            var myID;
            var total = 0;
            for (var i = 1; i < pItemCount; i++) {
                myID = 'dtgCompanyFinancial__ctl' + (i + 1) + '_txtWeight_CompanyFinancial';
                if (eval('document.forms[0].' + myID).disabled == false) {
                    total = parseFloat(total) + parseFloat(eval('document.forms[0].' + myID).value);
                }
            }
            document.forms[0].txtTotal_CompanyFinancial.value = total;
        }




        // -----------------------
        function OpenWinSchemeView(pCreditScoreSchemeID) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.SalesMarketing/Setting/ScoringCard/CreditScoring_MainView.aspx?CreditScoreSchemeID=' + pCreditScoreSchemeID, 'CreditScoringView', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=1');
        }

        function fBack() {
            history.back(-1);
            return false;
        }
        function DeleteConfirm() {
            if (confirm("Apakah yakin mau hapus data ini ?")) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../../Include/Buttons.css" type="text/css" />
</head>
<body>
    <asp:UpdateProgress ID="upg1" runat="server" AssociatedUpdatePanelID="updatePanel1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false"  ToolTip="Click to close"
                onclick="hideMessage();"></asp:Label>
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>
                        DAFTAR SCORING CARD PEMBIAYAAN
                    </h3>
                </div>
            </div>
            <asp:Panel ID="pnlList" runat="server">
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtgCreditScoring_Main" runat="server" Width="100%" AllowSorting="True"
                                AutoGenerateColumns="False" OnSortCommand="Sorting" BorderStyle="None" BorderWidth="0"
                                DataKeyField="CreditScoreSchemeID" CssClass="grid_general">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="EDIT">
                                        <ItemStyle CssClass="command_col"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbEdit" runat="server" CausesValidation="False" ImageUrl="../../../images/iconedit.gif"
                                                CommandName="Edit"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="EDIT CONTENT">
                                        <ItemStyle CssClass="command_col"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbEditContent" runat="server" CausesValidation="False" ImageUrl="../../../images/iconedit.gif"
                                                CommandName="EditContent"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="DELETE">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbDelete" runat="server" CausesValidation="False" ImageUrl="../../../images/iconDelete.gif"
                                                CommandName="Delete"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="SKEMA" SortExpression="CreditScoreSchemeID">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hynSchemeID" runat="server" Text='<%#container.dataitem("CreditScoreSchemeID")%>'
                                                NavigateUrl='<%# LinkTo(container.dataITem("CreditScoreSchemeID")) %>'>
                                            </asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="NAMA SKEMA" Visible="true" SortExpression="Description">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDescription" runat="server" Text='<%#container.dataitem("Description")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="SCHEME" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSchemeID_Main" runat="server" Text='<%#container.dataitem("CreditScoreSchemeID")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                            <div class="button_gridnavigation">
                                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../../Images/grid_navbutton01.png"
                                    CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../../Images/grid_navbutton02.png"
                                    CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../../Images/grid_navbutton03.png"
                                    CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../../Images/grid_navbutton04.png"
                                    CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                                    EnableViewState="False"></asp:Button>
                                <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" MinimumValue="1"
                                    ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"></asp:RangeValidator>
                                <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                                    ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                            </div>
                            <div class="label_gridnavigation">
                                <asp:Label ID="lblPage" runat="server"></asp:Label>of
                                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonAdd" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue">
                    </asp:Button>
                    
                </div>
                <div class="form_box_title">
                    <div class="form_single">
                        <h4>
                            CARI SCORING CARD PEMBIAYAAN
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Cari Berdasarkan</label>
                        <asp:DropDownList ID="cboSearch" runat="server">
                            <asp:ListItem Value="CreditScoreSchemeID">ID Skema</asp:ListItem>
                            <asp:ListItem Value="Description">Nama Skema</asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtSearch" runat="server" CssClass="medium_text"></asp:TextBox>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonSearch" runat="server" CausesValidation="False" Text="Search"
                        CssClass="small button blue"></asp:Button>
                    <asp:Button ID="ButtonReset" runat="server" CausesValidation="False" Text="Reset"
                        CssClass="small button gray"></asp:Button>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlAddEdit" runat="server">
                <div class="form_box_header">
                    <div class="form_single">
                        <h4>
                            <asp:Label ID="lblTitleAddEdit" runat="server"></asp:Label>
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            <p>
                                ID Skema<font color="red"></font></p>
                        </label>
                        <asp:Label ID="lblSchemeID" runat="server"></asp:Label>
                        <asp:TextBox ID="txtSchemeID" runat="server" Visible="False" Columns="13" MaxLength="10"></asp:TextBox>
                        <asp:Label ID="lblRequiredSchemeID" runat="server"></asp:Label>
                        <asp:RequiredFieldValidator ID="label" runat="server" ErrorMessage="Harap isi ID Skema"
                            CssClass="validator_general" ControlToValidate="txtSchemeID" Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Nama Skema<font color="red"></font></label>
                        <asp:TextBox ID="txtDescription" runat="server" Width="480px" MaxLength="100" Columns="53"></asp:TextBox>
                        <asp:Label ID="Label2" runat="server"></asp:Label>
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" ErrorMessage="Harap isi Nama Skema"
                            CssClass="validator_general" ControlToValidate="txtDescription" Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <asp:Panel ID="pnlPersonalCustomer" runat="server">
                    <div class="form_box_title">
                        <div class="form_single">
                            <h4>
                                PERSONAL CUSTOMER
                            </h4>
                        </div>
                    </div>
                    <div class="form_box_header">
                        <div class="form_single">
                            <h4>
                                CUSTOMER
                            </h4>
                        </div>
                    </div>
                    <div class="form_box_header">
                        <div class="form_single">
                            <div class="grid_wrapper_ns">
                                <asp:DataGrid ID="dtgPersonalCustomer" runat="server" CssClass="grid_general" BorderStyle="None"
                                    BorderWidth="0" AllowSorting="True" AutoGenerateColumns="False" Width="100%">
                                    <HeaderStyle CssClass="th" />
                                    <ItemStyle CssClass="item_grid" />
                                    <Columns>
                                        <asp:TemplateColumn HeaderText="Komponen">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkComponent_PersonalCustomer" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Component")%>'
                                                    Checked='<%#iif(Container.Dataitem("IsChecked")="1", True, False)%>'></asp:CheckBox>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Bobot (%)">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtWeight_PersonalCustomer" Enabled='<%#iif(Container.Dataitem("IsChecked")="1", True, False)%>'
                                                    runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Weight")%>'>
                                                </asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="PembiayaanScoreComponentID" Visible="true">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCSC_PersonalCustomer" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CreditScoreComponentID")%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </div>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label>
                                Total</label>
                            <asp:TextBox ID="txtTotal_PersonalCustomer" runat="server" ReadOnly="True"></asp:TextBox>
                        </div>
                    </div>
                    <asp:Panel ID="pnlPersonalFinancial" runat="server">
                        <div class="form_box_header">
                            <div class="form_single">
                                <h4>
                                    FINANCIAL
                                </h4>
                            </div>
                        </div>
                        <div class="form_box_header">
                            <div class="form_single">
                                <div class="grid_wrapper_ns">
                                    <asp:DataGrid ID="dtgPersonalFinancial" runat="server" CssClass="grid_general" BorderStyle="None"
                                        BorderWidth="0" AllowSorting="True" AutoGenerateColumns="False" Width="100%">
                                        <HeaderStyle CssClass="th" />
                                        <ItemStyle CssClass="item_grid" />
                                        <Columns>
                                            <asp:TemplateColumn HeaderText="Komponen">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkComponent_PersonalFinancial" Checked='<%#iif(Container.Dataitem("IsChecked")="1", True, False)%>'
                                                        runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Component")%>'>
                                                    </asp:CheckBox>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Bobot (%)">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtWeight_PersonalFinancial" runat="server" Enabled='<%#iif(Container.Dataitem("IsChecked")="1", True, False)%>'
                                                        Text='<%# DataBinder.Eval(Container, "DataItem.Weight")%>'>
                                                    </asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="PembiayaanScoreComponentID" Visible="False">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCSC_PersonalFinancial" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CreditScoreComponentID")%>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                        </Columns>
                                    </asp:DataGrid>
                                </div>
                            </div>
                        </div>
                        <div class="form_box">
                            <div class="form_single">
                                <label>
                                    Total</label>
                                <asp:TextBox ID="txtTotal_PersonalFinancial" runat="server" ReadOnly="True"></asp:TextBox>
                            </div>
                        </div>
                        <asp:Panel ID="pnlPersonalAsset" runat="server">
                            <div class="form_box_header">
                                <div class="form_single">
                                    <h4>
                                        ASSET
                                    </h4>
                                </div>
                            </div>
                            <div class="form_box_header">
                                <div class="form_single">
                                    <div class="grid_wrapper_ns">
                                        <asp:DataGrid ID="dtgPersonalAsset" runat="server" CssClass="grid_general" BorderStyle="None"
                                            BorderWidth="0" AllowSorting="True" AutoGenerateColumns="False" Width="100%">
                                            <HeaderStyle CssClass="th" />
                                            <ItemStyle CssClass="item_grid" />
                                            <Columns>
                                                <asp:TemplateColumn HeaderText="Komponen">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkComponent_PersonalAsset" Checked='<%#iif(Container.Dataitem("IsChecked")="1", True, False)%>'
                                                            runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Component")%>'>
                                                        </asp:CheckBox>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Bobot (%)">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtWeight_PersonalAsset" runat="server" Enabled='<%#iif(Container.Dataitem("IsChecked")="1", True, False)%>'
                                                            Text='<%# DataBinder.Eval(Container, "DataItem.Weight")%>'>
                                                        </asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="PembiayaanScoreComponentID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCSC_PersonalAsset" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CreditScoreComponentID")%>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                            </Columns>
                                        </asp:DataGrid>
                                    </div>
                                </div>
                            </div>
                            <div class="form_box">
                                <div class="form_single">
                                    <label>
                                        Total</label>
                                    <asp:TextBox ID="txtTotal_PersonalAsset" runat="server" ReadOnly="True"></asp:TextBox>
                                </div>
                            </div>
                        </asp:Panel>
                    </asp:Panel>
                    <asp:Panel ID="pnlTotal_Personal" runat="server" Visible="True">
                        <div class="form_box">
                            <div class="form_single">
                                Score Approval :<font color="red"></font>
                                <asp:TextBox ID="txtApprovalScore_Personal" runat="server" MaxLength="3"></asp:TextBox>
                                <asp:Label ID="Label3" runat="server"></asp:Label>
                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator3" runat="server" ErrorMessage="Harap isi Score Approval Personal"
                                    CssClass="validator_general" ControlToValidate="txtApprovalScore_Personal" Display="Dynamic"></asp:RequiredFieldValidator>
                                % Score Reject :<font color="red"></font>
                                <asp:TextBox ID="txtRejectScore_Personal" runat="server" MaxLength="3"></asp:TextBox>
                                <asp:Label ID="Label4" runat="server"></asp:Label>
                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator4" runat="server" ErrorMessage="Harap isi Score Reject Personal"
                                    CssClass="validator_general" ControlToValidate="txtRejectScore_Personal" Display="Dynamic"></asp:RequiredFieldValidator>
                                % Total :
                                <asp:Label ID="lblGrandTotal_Personal" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="form_box">
                            <div class="form_single">
                                <asp:RangeValidator ID="Rangevalidator1" runat="server" ErrorMessage="Minimum Score Approval : 1, Maximum Score Approval : 100"
                                    MaximumValue="100" MinimumValue="1" Type="Double" ControlToValidate="txtApprovalScore_Personal"
                                    CssClass="validator_general" Display="Dynamic" Font-Name="Verdana" Font-Size="8"></asp:RangeValidator>
                                <asp:RangeValidator ID="Rangevalidator2" runat="server" ErrorMessage="Minimum Score Reject : 1, Maximum Score Reject : 100"
                                    CssClass="validator_general" MaximumValue="100" MinimumValue="1" Type="Double"
                                    ControlToValidate="txtRejectScore_Personal" Display="Dynamic" Font-Name="Verdana"
                                    Font-Size="8"></asp:RangeValidator>
                                <asp:CompareValidator ID="cmp_Personal" runat="server" ErrorMessage="Score Approval harus lebih besar dari Score Reject"
                                    CssClass="validator_general" Type="Double" ControlToValidate="txtApprovalScore_Personal"
                                    Display="Dynamic" Font-Name="Verdana" Font-Size="8" ControlToCompare="txtRejectScore_Personal"
                                    Operator="GreaterThan"></asp:CompareValidator>
                                <asp:CompareValidator ID="Comparevalidator2" runat="server" ErrorMessage="" Type="Double"
                                    CssClass="validator_general" ControlToValidate="txtRejectScore_Personal" Display="Dynamic"
                                    Font-Name="Verdana" Font-Size="8" ControlToCompare="txtApprovalScore_Personal"
                                    Operator="LessThan">
                                </asp:CompareValidator>
                            </div>
                        </div>
                    </asp:Panel>
                    <div class="form_box">
                        <div class="form_single">
                            <label class="label_req">
                                Grade A</label>
                            <asp:TextBox ID="txtGradeAFromPersonal" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ErrorMessage="*"
                                CssClass="validator_general" ControlToValidate="txtGradeAFromPersonal"></asp:RequiredFieldValidator>
                            S/D
                            <asp:TextBox ID="txtGradeAToPersonal" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" ErrorMessage="*"
                                CssClass="validator_general" ControlToValidate="txtGradeAToPersonal"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label class="label_req">
                                Grade B</label>
                            <asp:TextBox ID="txtGradeBFromPersonal" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator18" runat="server" ErrorMessage="*"
                                CssClass="validator_general" ControlToValidate="txtGradeBFromPersonal"></asp:RequiredFieldValidator>
                            S/D
                            <asp:TextBox ID="txtGradeBToPersonal" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator19" runat="server" ErrorMessage="*"
                                CssClass="validator_general" ControlToValidate="txtGradeBToPersonal"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label class="label_req">
                                Grade C</label>
                            <asp:TextBox ID="txtGradeCFromPersonal" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator20" runat="server" ErrorMessage="*"
                                CssClass="validator_general" ControlToValidate="txtGradeCFromPersonal"></asp:RequiredFieldValidator>
                            S/D
                            <asp:TextBox ID="txtGradeCToPersonal" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator21" runat="server" ErrorMessage="*"
                                CssClass="validator_general" ControlToValidate="txtGradeCToPersonal"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label class="label_req">
                                Grade c</label>
                            <asp:TextBox ID="txtGradeCcFromPersonal" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator22" runat="server" ErrorMessage="*"
                                CssClass="validator_general" ControlToValidate="txtGradeCcFromPersonal"></asp:RequiredFieldValidator>
                            S/D
                            <asp:TextBox ID="txtGradeCcToPersonal" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator23" runat="server" ErrorMessage="*"
                                CssClass="validator_general" ControlToValidate="txtGradeCcToPersonal"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label class="label_req">
                                Grade D</label>
                            <asp:TextBox ID="txtGradeDFromPersonal" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator24" runat="server" ErrorMessage="*"
                                CssClass="validator_general" ControlToValidate="txtGradeDFromPersonal"></asp:RequiredFieldValidator>
                            S/D
                            <asp:TextBox ID="txtGradeDToPersonal" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator25" runat="server" ErrorMessage="*"
                                CssClass="validator_general" ControlToValidate="txtGradeDToPersonal"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlCompanyCustomer" runat="server">
                    <div class="form_box_title">
                        <div class="form_single">
                            <h4>
                                CORPORATE CUSTOMER
                            </h4>
                        </div>
                    </div>
                    <div class="form_box_header">
                        <div class="form_single">
                            <h4>
                                DATA CORPORATE
                            </h4>
                        </div>
                    </div>
                    <div class="form_box_header">
                        <div class="form_single">
                            <div class="grid_wrapper_ns">
                                <asp:DataGrid ID="dtgCompanyCustomer" runat="server" CssClass="grid_general" BorderStyle="None"
                                    BorderWidth="0" AllowSorting="True" AutoGenerateColumns="false" Width="100%">
                                    <HeaderStyle CssClass="th" />
                                    <ItemStyle CssClass="item_grid" />
                                    <Columns>
                                        <asp:TemplateColumn HeaderText="Komponen">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkComponent_CompanyCustomer" Checked='<%#iif(Container.Dataitem("IsChecked")="1", True, False)%>'
                                                    runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Component")%>'>
                                                </asp:CheckBox>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Bobot (%)">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtWeight_CompanyCustomer" runat="server" Enabled='<%#iif(Container.Dataitem("IsChecked")="1", True, False)%>'
                                                    Text='<%# DataBinder.Eval(Container, "DataItem.Weight")%>'>
                                                </asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="PembiayaanScoreComponentID" Visible="False">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCSC_CompanyCustomer" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CreditScoreComponentID")%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </div>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label>
                                Total</label>
                            <asp:TextBox ID="txtTotal_CompanyCustomer" runat="server" ReadOnly="True"></asp:TextBox>
                        </div>
                    </div>
                    <asp:Panel ID="pnlCompanyFinancial" runat="server">
                        <div class="form_box_header">
                            <div class="form_single">
                                <h4>
                                    FINANCIAL
                                </h4>
                            </div>
                        </div>
                        <div class="form_box_header">
                            <div class="form_single">
                                <div class="grid_wrapper_ns">
                                    <asp:DataGrid ID="dtgCompanyFinancial" runat="server" CCssClass="grid_general" BorderStyle="None"
                                        BorderWidth="0" AllowSorting="True" AutoGenerateColumns="False" Width="100%">
                                        <HeaderStyle CssClass="th" />
                                        <ItemStyle CssClass="item_grid" />
                                        <Columns>
                                            <asp:TemplateColumn HeaderText="Komponen">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkComponent_CompanyFinancial" Checked='<%#iif(Container.Dataitem("IsChecked")="1", True, False)%>'
                                                        runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Component")%>'>
                                                    </asp:CheckBox>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Bobot (%)">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtWeight_CompanyFinancial" runat="server" Enabled='<%#iif(Container.Dataitem("IsChecked")="1", True, False)%>'
                                                        Text='<%# DataBinder.Eval(Container, "DataItem.Weight")%>'>
                                                    </asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="PembiayaanScoreComponentID" Visible="False">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCSC_CompanyFinancial" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CreditScoreComponentID")%>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                        </Columns>
                                    </asp:DataGrid>
                                </div>
                            </div>
                        </div>
                        <div class="form_box">
                            <div class="form_single">
                                <label>
                                    Total</label>
                                <asp:TextBox ID="txtTotal_CompanyFinancial" runat="server" ReadOnly="True"></asp:TextBox>
                            </div>
                        </div>
                        <asp:Panel ID="pnlCompanyAsset" runat="server">
                            <div class="form_box_header">
                                <div class="form_single">
                                    <h4>
                                        ASSET
                                    </h4>
                                </div>
                            </div>
                            <div class="form_box_header">
                                <div class="form_single">
                                    <div class="grid_wrapper_ns">
                                        <asp:DataGrid ID="dtgCompanyAsset" runat="server" CssClass="grid_general" BorderStyle="None"
                                            BorderWidth="0" AllowSorting="True" AutoGenerateColumns="False" Width="100%">
                                            <HeaderStyle CssClass="th" />
                                            <ItemStyle CssClass="item_grid" />
                                            <Columns>
                                                <asp:TemplateColumn HeaderText="Komponen">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkComponent_CompanyAsset" Checked='<%#iif(Container.Dataitem("IsChecked")="1", True, False)%>'
                                                            runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Component")%>'>
                                                        </asp:CheckBox>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Bobot (%)">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtWeight_CompanyAsset" Enabled='<%#iif(Container.Dataitem("IsChecked")="1", True, False)%>'
                                                            runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Weight")%>'>
                                                        </asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="PembiayaanScoreComponentID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCSC_CompanyAsset" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CreditScoreComponentID")%>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                            </Columns>
                                        </asp:DataGrid>
                                    </div>
                                </div>
                            </div>
                            <div class="form_box">
                                <div class="form_single">
                                    <label>
                                        Total</label>
                                    <asp:TextBox ID="txtTotal_CompanyAsset" runat="server" ReadOnly="True"></asp:TextBox>
                                </div>
                            </div>
                        </asp:Panel>
                    </asp:Panel>
                </asp:Panel>
                <asp:Panel ID="pnlTotal_Company" runat="server" Visible="True">
                    <div class="form_box">
                        <div class="form_single">
                            Score Approval :<font color="red"></font>
                            <asp:TextBox ID="txtApprovalScore_Company" runat="server" MaxLength="3"></asp:TextBox>
                            <asp:Label ID="Label5" runat="server"></asp:Label>
                            <asp:RequiredFieldValidator ID="Requiredfieldvalidator5" runat="server" ErrorMessage="Harap isi Score Approval Corporate"
                                CssClass="validator_general" ControlToValidate="txtApprovalScore_Company" Display="Dynamic"></asp:RequiredFieldValidator>
                            % Score Reject :<font color="red"></font>
                            <asp:TextBox ID="txtRejectScore_Company" runat="server" MaxLength="3"></asp:TextBox>
                            <asp:Label ID="Label6" runat="server"></asp:Label>
                            <asp:RequiredFieldValidator ID="Requiredfieldvalidator6" runat="server" ErrorMessage="Harap isi Score Reject Corporate"
                                CssClass="validator_general" ControlToValidate="txtRejectScore_Company" Display="Dynamic"></asp:RequiredFieldValidator>
                            % Total :
                            <asp:Label ID="lblGrandTotal_Company" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <asp:RangeValidator ID="Rangevalidator3" runat="server" ErrorMessage="Minimum score Approval : 1, Maximum Score Approval : 100"
                                CssClass="validator_general" MaximumValue="100" MinimumValue="1" Type="Double"
                                ControlToValidate="txtApprovalScore_Company" Display="Dynamic" Font-Name="Verdana"
                                Font-Size="8"></asp:RangeValidator>
                            <asp:RangeValidator ID="Rangevalidator4" runat="server" ErrorMessage="Minimum score Reject : 1, Maximum Score Reject : 100"
                                CssClass="validator_general" MaximumValue="100" MinimumValue="1" Type="Double"
                                ControlToValidate="txtRejectScore_Company" Display="Dynamic" Font-Name="Verdana"
                                Font-Size="8"></asp:RangeValidator>
                            <asp:CompareValidator ID="Comparevalidator1" runat="server" ErrorMessage="Score Approval harus lebih besar dari Score Reject"
                                CssClass="validator_general" Type="Double" ControlToValidate="txtApprovalScore_Company"
                                Display="Dynamic" Font-Name="Verdana" Font-Size="8" ControlToCompare="txtRejectScore_Company"
                                Operator="GreaterThan"></asp:CompareValidator>
                            <asp:CompareValidator ID="Comparevalidator3" runat="server" ErrorMessage="" Type="Double"
                                CssClass="validator_general" ControlToValidate="txtRejectScore_Company" Display="Dynamic"
                                Font-Name="Verdana" Font-Size="8" ControlToCompare="txtApprovalScore_Company"
                                Operator="LessThan"></asp:CompareValidator>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label>
                                Grade A</label>
                            <asp:TextBox ID="txtGradeAFromCompany" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                                CssClass="validator_general" ControlToValidate="txtGradeAFromCompany"></asp:RequiredFieldValidator>
                            S/D
                            <asp:TextBox ID="txtGradeAToCompany" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="*"
                                CssClass="validator_general" ControlToValidate="txtGradeAToCompany"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label>
                                Grade B</label>
                            <asp:TextBox ID="txtGradeBFromCompany" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="*"
                                CssClass="validator_general" ControlToValidate="txtGradeBFromCompany"></asp:RequiredFieldValidator>
                            S/D
                            <asp:TextBox ID="txtGradeBToCompany" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="*"
                                CssClass="validator_general" ControlToValidate="txtGradeBToCompany"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label>
                                Grade C</label>
                            <asp:TextBox ID="txtGradeCFromCompany" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="*"
                                CssClass="validator_general" ControlToValidate="txtGradeCFromCompany"></asp:RequiredFieldValidator>
                            S/D
                            <asp:TextBox ID="txtGradeCToCompany" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="*"
                                CssClass="validator_general" ControlToValidate="txtGradeCToCompany"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label>
                                Grade c</label>
                            <asp:TextBox ID="txtGradeCcFromCompany" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage="*"
                                CssClass="validator_general" ControlToValidate="txtGradeCcFromCompany"></asp:RequiredFieldValidator>
                            S/D
                            <asp:TextBox ID="txtGradeCcToCompany" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ErrorMessage="*"
                                CssClass="validator_general" ControlToValidate="txtGradeCcToCompany"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label>
                                Grade D</label>
                            <asp:TextBox ID="txtGradeDFromCompany" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ErrorMessage="*"
                                CssClass="validator_general" ControlToValidate="txtGradeDFromCompany"></asp:RequiredFieldValidator>
                            S/D
                            <asp:TextBox ID="txtGradeDToCompany" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ErrorMessage="*"
                                CssClass="validator_general" ControlToValidate="txtGradeDToCompany"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                </asp:Panel>
                 <asp:Panel ID="PnlCutOff" runat="server">
                        <div class="form_box">
	                        <div class="form_single">
                            Cut Off Decision Value for After Survey :&nbsp;<font color="red"></font>
                                <asp:TextBox ID="txtCutOffBefore" runat="server"  MaxLength="3"></asp:TextBox>
           
                            </div>
                            <%--  <div class="form_single">
                            Cut Off Decision Value for After Survey:&nbsp;&nbsp;<font color="red"></font>
                                <asp:TextBox ID="txtCutOffAfter" runat="server"  MaxLength="3"></asp:TextBox>
           
                            </div>  --%>
                        </div> 
                        </asp:Panel>
                <div class="form_button">
                    <asp:Button ID="ButtonSave" runat="server" CausesValidation="true" Text="Save" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="ButtonCancel" runat="server" CausesValidation="false" Text="Cancel"
                        CssClass="small button gray"></asp:Button>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
