﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CRRejectPolicy_EditContent.aspx.vb" Inherits="Maxiloan.Webform.SalesMarketing.CRRejectPolicy_EditContent" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>RejectPolicy_EditContent</title>
        <script src="../../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../../Include/Buttons.css" type="text/css" />
</head>

<script language="JavaScript" type="text/javascript">
    var x = screen.width;
    var y = screen.height - 100;

    function fClose() {
        window.close();
        return false;
    }

    function DeleteConfirm() {
        if (confirm("Apakah yakin mau hapus data ini ?")) {
            return true;
        }
        else {
            return false;
        }
    }
    
    </script>


<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false"  ToolTip="Click to close" onclick="hideMessage();" />
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                SCORE POLICY COMPONEN CONTENT 
            </h3>
        </div>
    </div>
    
      <asp:Panel ID="pnlView" runat="server">

        <div class="form_box_header">
            <div class="form_single">
                <h4>EDIT - CORE REJECT POLICY
                </h4>
            </div>
        </div>
         
        <div class="form_box">
            <div class="form_single">
                <label>Nama</label>
                <asp:Label ID="lblDescription" runat="server">SCORE REJECT POLICY</asp:Label>
            </div>
        </div>


         <div class="form_box_header">
                <div class="form_single"> <h4> CUSTOMER </h4> </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ns">
                        <asp:DataGrid ID="dtgRejectPolicy" runat="server" CssClass="grid_general" BorderStyle="None" BorderWidth="0" Width="100%" AllowSorting="True" AutoGenerateColumns="False">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <Columns>
                                <asp:TemplateColumn HeaderText="Edit Content">
                                    <ItemStyle CssClass="command_col"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" ImageUrl="../../../Images/iconedit.gif" CommandName="EditContent"></asp:ImageButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Komponen">
                                    <ItemTemplate>
                                        <asp:Label ID="lblComponent" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ItemDescription")%>'>
                                        </asp:Label>
                                    </ItemTemplate> 
                                      </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="CalculationType" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCalculationType" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CalculationType")%>' />
                                        <asp:Label ID="lblDataItemID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Item")%>' />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                 
                            </Columns>
                        </asp:DataGrid>
                    </div>
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="ButtonOK" runat="server" Text="Ok" CssClass="small button blue" CausesValidation="false" />
        </div>
    </asp:Panel>


       <asp:Panel ID="pnlEditContent_Range_Datagrid" runat="server">
            <div class="form_box_header">
                <div class="form_single">
                    <h4>
                        DAFTAR  COMPONEN CONTENT 
                    </h4>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ns">
                        <asp:DataGrid ID="dtgEditContent_Range" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False" CssClass="grid_general" BorderStyle="None" BorderWidth="0"  >
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <Columns>
                                <asp:TemplateColumn HeaderText="EDIT" ItemStyle-Width="10%">
                                    <ItemStyle CssClass="command_col"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgEditRange" runat="server" CausesValidation="False" ImageUrl="../../../images/iconedit.gif" CommandName="EditRange" />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="DELETE" ItemStyle-Width="10%">
                                    <ItemStyle CssClass="command_col"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgDeleteRange" runat="server" CausesValidation="False" ImageUrl="../../../images/icondelete.gif" CommandName="DeleteRange" />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="Content" HeaderText="CONTENT">
                                    <ItemTemplate>
                                        <asp:Label ID="lblContent" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Content")%>' />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="ScoreValue" HeaderText="SCORE">
                                    <ItemTemplate>
                                        <asp:Label ID="lblScore" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ScoreValue")%>' />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="ScoreStatus" HeaderText="STATUS SCORE">
                                    <ItemTemplate>
                                        <asp:Label ID="lblScoreStatus" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ScoreStatus")%>' />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn Visible="false" HeaderText="ContentSeqNo">
                                    <ItemTemplate>
                                        <asp:Label ID="lblContentSeqNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.id")%>' />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                       
                    </div>
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="ButtonAdd" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue" />
                <asp:Button ID="ButtonBack" runat="server" Text="Back" CssClass="small button blue" CausesValidation="False" />
            </div>
        </asp:Panel>

       <asp:Panel ID="pnlEditContent_Range_AddEdit" runat="server">
            <div class="form_box_header">
                <div class="form_single">
                    <h4> <asp:Label ID="lblAddEdit" runat="server"></asp:Label> </h4>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label class="label_req">  Content  </label>
                    <asp:TextBox ID="txtValueFrom" runat="server" MaxLength="15"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator4" runat="server" Display="Dynamic" CssClass="validator_general" ErrorMessage="" ControlToValidate="txtValueFrom"></asp:RequiredFieldValidator>to
                    <asp:TextBox ID="txtValueTo" runat="server" MaxLength="15"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" Display="Dynamic" CssClass="validator_general" ErrorMessage="" ControlToValidate="txtValueTo" />
                    <asp:RangeValidator ID="Rangevalidator1" runat="server" ErrorMessage="Nilai Salah" MinimumValue="0" Type="double" ControlToValidate="txtValueFrom" MaximumValue="999999999999999" CssClass="validator_general" Display="Dynamic" />
                    <asp:RangeValidator ID="Rangevalidator2" runat="server" ErrorMessage="Nilai Salah" MinimumValue="0" Type="double" ControlToValidate="txtValueTo" MaximumValue="999999999999999" CssClass="validator_general" Display="Dynamic" />
                    <asp:CompareValidator ID="cmv" runat="server" ErrorMessage="Nilai DARI harus lebih kecil dari Nilai KE" CssClass="validator_general" ControlToValidate="txtValueFrom" Display="Dynamic" ControlToCompare="txtValueTo" Operator="LessThan" />
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label class="label_req"> Score</label>
                    <asp:TextBox ID="txtScoreValue" runat="server" MaxLength="3"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" Display="Dynamic"  CssClass="validator_general" ErrorMessage="" ControlToValidate="txtScoreValue" />
                    <asp:RangeValidator ID="Rangevalidator3" runat="server" ErrorMessage="Score harus antara 0 s/d 100" CssClass="validator_general" MinimumValue="0" Type="integer" ControlToValidate="txtScoreValue" MaximumValue="100" Display="Dynamic" />
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>Status Score</label>
                    <asp:DropDownList ID="cboScoreStatus" runat="server" Width="88px">
                        <asp:ListItem Value="N">Normal</asp:ListItem>
                        <asp:ListItem Value="W">Warning</asp:ListItem>
                        <asp:ListItem Value="R">Reject</asp:ListItem>
                        <asp:ListItem Value="F">Fatal</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="ButtonSave" runat="server" Text="Save" CssClass="small button blue" />
                <asp:Button ID="ButtonCancel" runat="server" Text="Cancel" CssClass="small button gray" CausesValidation="False" />
            </div>
        </asp:Panel>


      <asp:Panel ID="pnlEditContent_Table_Datagrid" runat="server">
            <div class="form_box_header">
                <div class="form_single">
                    <h4>
                        DAFTAR SCORE COMPONENT CONTENT KREDIT
                    </h4>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ns">
                        <asp:DataGrid ID="dtgEditContent_Table" runat="server" CCssClass="grid_general" BorderStyle="None"
                            BorderWidth="0" Width="100%" AllowSorting="True" AutoGenerateColumns="False" >
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <Columns>
                                <asp:TemplateColumn HeaderText="EDIT">
                                    <ItemStyle CssClass="command_col"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgEdit_Table" runat="server" CausesValidation="False" ImageUrl="../../../images/iconedit.gif"
                                            CommandName="EditTable"></asp:ImageButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="ValueContent" HeaderText="CONTENT">
                                    <ItemTemplate>
                                        <asp:Label ID="lblValueContent_Table" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ValueContent")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="ValueDescription" HeaderText="KETERANGAN">
                                    <ItemTemplate>
                                        <asp:Label ID="lblValueDescription_Table" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ValueDescription")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="ScoreValue" HeaderText="SCORE">
                                    <ItemTemplate>
                                        <asp:Label ID="lblScoreValue_Table" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ScoreValue")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="ScoreStatus" HeaderText="STATUS SCORE">
                                    <ItemTemplate>
                                        <asp:Label ID="lblScoreStatus_Table" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ScoreStatus")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn Visible="false" HeaderText="ContentSeqNo">
                                    <ItemTemplate>
                                        <asp:Label ID="lblContentSeqNo_Table" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Id")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                         
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="ButtonBack_table" runat="server" Text="Back" CssClass="small button gray" CausesValidation="False" />
            </div>
        </asp:Panel>


      <asp:Panel ID="pnlEditContent_Table_AddEdit" runat="server">
            <div class="form_box_header">
                <div class="form_single">
                    <h4> <asp:Label ID="lblAddEdit_Table" runat="server"></asp:Label> </h4>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label> Content  </label>
                    <asp:Label ID="lblValueContent" runat="server"></asp:Label>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>Keterangan </label>
                    <asp:Label ID="lblValueDescription" runat="server"></asp:Label>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label class="label_req">Score</label>
                    <asp:TextBox ID="txtScoreValue_Table" runat="server" MaxLength="3"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator6" runat="server" Display="Dynamic" CssClass="validator_general" ErrorMessage="" ControlToValidate="txtScoreValue_Table" />
                    <asp:RangeValidator ID="Rangevalidator7" runat="server" ErrorMessage="Nilai Score Salah" CssClass="validator_general" MinimumValue="0" Type="integer" ControlToValidate="txtScoreValue_Table" MaximumValue="100" Display="Dynamic" />
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>Status Score</label>
                    <asp:DropDownList ID="cboScoreStatus_Table" runat="server" Width="88px">
                        <asp:ListItem Value="N">Normal</asp:ListItem>
                        <asp:ListItem Value="W">Warning</asp:ListItem>
                        <asp:ListItem Value="R">Reject</asp:ListItem>
                        <asp:ListItem Value="F">Fatal</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="ButtonSave_Table" runat="server" Text="Save" CssClass="small button blue" />
                <asp:Button ID="ButtonCancel_Table" runat="server" Text="Cancel" CssClass="small button gray" CausesValidation="False" />
            </div>
        </asp:Panel>
    </form>
</body>
</html>
