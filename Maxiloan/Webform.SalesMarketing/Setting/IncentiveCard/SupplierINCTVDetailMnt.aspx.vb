﻿
#Region "Imports"
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.cbse
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class SupplierINCTVDetailMnt
    Inherits Maxiloan.Webform.WebBased

#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property CardID() As Integer
        Get
            Return CType(ViewState("CardID"), Integer)
        End Get
        Set(ByVal Value As Integer)
            ViewState("CardID") = Value
        End Set
    End Property
    Private Property isEdit() As Boolean
        Get
            Return CType(ViewState("isEdit"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            ViewState("isEdit") = Value
        End Set
    End Property
    Private Property IntervalUnit() As Integer
        Get
            Return CType(ViewState("IntervalUnit"), Integer)
        End Get
        Set(ByVal Value As Integer)
            ViewState("IntervalUnit") = Value
        End Set
    End Property
    Private Property IntervalAF() As Integer
        Get
            Return CType(ViewState("IntervalAF"), Integer)
        End Get
        Set(ByVal Value As Integer)
            ViewState("IntervalAF") = Value
        End Set
    End Property
    Private Property dtsEntity() As DataTable
        Get
            Return CType(ViewState("dtsEntity"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            ViewState("dtsEntity") = Value
        End Set
    End Property
    Private Property StatusScheme() As String
        Get
            Return CType(ViewState("StatusScheme"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("StatusScheme") = Value
        End Set
    End Property
#End Region
#Region "PrivateConst"
    Private oCustomClass As New Parameter.IncentiveCard
    Private oController As New IncentiveCardController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        'Put user code to initialize the page here
        If SessionInvalid() Then
            Exit Sub
        End If
        Me.FormID = "SUPPINCTVMNT"
        If Not IsPostBack Then
            pnlEditDetail.Visible = True
            PnlEditUnit.Visible = False
            PnlEditAF.Visible = False
            Me.CmdWhere = ""
            Me.SortBy = ""
            If Request.QueryString("CardID") <> "" Then
                Me.CardID = CInt(Request.QueryString("CardID"))
            Else
                Me.CardID = CInt(0)
            End If
        End If
        BindData()
    End Sub
#Region "Button Click"
    Private Sub ImbCancelAF_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ImbCancelAF.Click
        Response.Redirect("SupplierINCTVDetailMnt.aspx?CardID=" & Me.CardID & "")
    End Sub
    Private Sub imbCancelCategory_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imbCancelCategory.Click
        Response.Redirect("SupplierINCTVDetailMnt.aspx?CardID=" & Me.CardID & "")
    End Sub
    Private Sub ImbCancelEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ImbCancelEdit.Click
        Response.Redirect("SupplierIncentiveMnt.aspx")
    End Sub
    Private Sub imbOKCategory_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imbOKCategory.Click
        Dim cmdWhere As String
        cmdWhere = ""
        If cboCategory.SelectedItem.Value <> "0" Then
            cmdWhere = cmdWhere + "" & cboCategory.SelectedItem.Value.Trim & ""
        Else            
            ShowMessage(lblMessage, "Please Select Category First", True)
            Exit Sub
        End If
        CheckIfEditORAddDetail()
        BindGridUnit(cmdWhere)
    End Sub
    Private Sub ImbOKEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ImbOKEdit.Click
        Dim cmdWhere As String
        pnlEditDetail.Visible = True

        If cboStatusScheme.SelectedItem.Value = "Unit" Then
            Me.StatusScheme = "U"
            PnlEditUnit.Visible = True
            PnlEditAF.Visible = False
            dtgPagingUnit.Visible = False
            ImbSaveCategory.Visible = False
            imbCancelCategory.Visible = False
            BindComboCategory()
        ElseIf cboStatusScheme.SelectedItem.Value = "AF" Then
            Me.StatusScheme = "A"
            PnlEditUnit.Visible = False
            PnlEditAF.Visible = True
            cmdWhere = "SupplierIncentiveCardH.SupplierIncentiveCardID = '" & Me.CardID & "'"
            CheckIfEditORAddDetail()
            BindGridAF(cmdWhere)
        End If
    End Sub
    Private Sub ImbSaveCategory_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ImbSaveCategory.Click
        Dim dtAdd As New DataTable
        Dim dtRow As DataRow
        Dim txtUnitMIN As TextBox
        Dim TxtIncentiveUnit As TextBox
        Dim txtUnitMax As TextBox
        Dim i As Integer
        If Me.dtsEntity.Rows.Count > 0 Then
            dtAdd.Columns.Add("UnitMin", System.Type.GetType("System.String"))
            dtAdd.Columns.Add("UnitMax", System.Type.GetType("System.String"))
            dtAdd.Columns.Add("IncentiveUnit", System.Type.GetType("System.String"))
            For i = 0 To Me.dtsEntity.Rows.Count - 1
                txtUnitMIN = CType(dtgPagingUnit.Items(i).Cells(0).FindControl("txtUnitMIN"), TextBox)
                txtUnitMax = CType(dtgPagingUnit.Items(i).Cells(1).FindControl("txtUnitMax"), TextBox)
                TxtIncentiveUnit = CType(dtgPagingUnit.Items(i).Cells(2).FindControl("TxtIncentiveUnit"), TextBox)
                If Not ((txtUnitMIN.Text = "0" And txtUnitMax.Text = "0" And TxtIncentiveUnit.Text = "0") Or (txtUnitMIN.Text = "" And txtUnitMax.Text = "" And TxtIncentiveUnit.Text = "")) Then
                    dtRow = dtAdd.NewRow()
                    dtRow("UnitMin") = txtUnitMIN.Text
                    dtRow("UnitMax") = txtUnitMax.Text
                    dtRow("IncentiveUnit") = TxtIncentiveUnit.Text
                    dtAdd.Rows.Add(dtRow)
                End If
            Next
            If dtAdd.Rows.Count > 0 Then
                With oCustomClass
                    .strConnection = GetConnectionString()
                    .CardID = Me.CardID
                    .ListData = dtAdd
                    .CategoryID = cboCategory.SelectedItem.Value.Trim
                End With
                If Me.isEdit = True Then
                    Try
                        oController.SupplierINCTVDetailUnitEdit(oCustomClass)                        
                        ShowMessage(lblMessage, "Record is updated successfully ", True)
                        pnlEditDetail.Visible = True
                        PnlEditAF.Visible = False
                        PnlEditUnit.Visible = False
                    Catch ex As Exception                        
                        ShowMessage(lblMessage, "Error: " & ex.Message & "<br>" & ex.Source, True)
                        pnlEditDetail.Visible = True
                        PnlEditAF.Visible = False
                        PnlEditUnit.Visible = False
                        Exit Sub
                    End Try
                ElseIf Me.isEdit = False Then
                    Try
                        oController.SupplierINCTVDetailUnitAdd(oCustomClass)
                        ShowMessage(lblMessage, "Record is added successfully ", True)
                        pnlEditDetail.Visible = True
                        PnlEditAF.Visible = False
                        PnlEditUnit.Visible = False
                    Catch ex As Exception                        
                        ShowMessage(lblMessage, "Error: " & ex.Message & "<br>" & ex.Source, True)
                        pnlEditDetail.Visible = True
                        PnlEditAF.Visible = False
                        PnlEditUnit.Visible = False
                        Exit Sub
                    End Try
                End If
            End If

        End If
    End Sub
    Private Sub ImbSaveAF_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ImbSaveAF.Click
        Dim dtAdd As New DataTable
        Dim dtRow As DataRow
        Dim txtBaseAFUnitMIN As TextBox
        Dim txtBaseAFUNitMax As TextBox
        Dim txtRateIncentiveAF As TextBox
        Dim i As Integer
        If Me.dtsEntity.Rows.Count > 0 Then
            dtAdd.Columns.Add("UnitMin", System.Type.GetType("System.String"))
            dtAdd.Columns.Add("UnitMax", System.Type.GetType("System.String"))
            dtAdd.Columns.Add("RateIncentiveAF", System.Type.GetType("System.String"))
            For i = 0 To Me.dtsEntity.Rows.Count - 1
                txtBaseAFUnitMIN = CType(DtgPagingAF.Items(i).Cells(0).FindControl("txtBaseAFUnitMIN"), TextBox)
                txtBaseAFUNitMax = CType(DtgPagingAF.Items(i).Cells(1).FindControl("txtBaseAFUNitMax"), TextBox)
                txtRateIncentiveAF = CType(DtgPagingAF.Items(i).Cells(2).FindControl("txtRateIncentiveAF"), TextBox)
                If Not ((txtBaseAFUnitMIN.Text = "0" And txtBaseAFUNitMax.Text = "0" And txtRateIncentiveAF.Text = "0") Or (txtBaseAFUnitMIN.Text = "" And txtBaseAFUNitMax.Text = "" And txtRateIncentiveAF.Text = "")) Then
                    dtRow = dtAdd.NewRow()
                    dtRow("UnitMin") = txtBaseAFUnitMIN.Text.Trim
                    dtRow("UnitMax") = txtBaseAFUNitMax.Text.Trim
                    dtRow("RateIncentiveAF") = txtRateIncentiveAF.Text.Trim
                    dtAdd.Rows.Add(dtRow)
                End If
            Next
            With oCustomClass
                .strConnection = GetConnectionString()
                .CardID = Me.CardID
                .ListData = dtAdd
            End With
            If Me.isEdit = True Then
                Try
                    oController.SupplierINCTVDetailAFEdit(oCustomClass)                    
                    ShowMessage(lblMessage, "Record is added successfully ", True)
                    pnlEditDetail.Visible = True
                    PnlEditAF.Visible = False
                    PnlEditUnit.Visible = False
                Catch ex As Exception                    
                    ShowMessage(lblMessage, "Error: " & ex.Message & "<br>" & ex.Source, True)
                    pnlEditDetail.Visible = True
                    PnlEditAF.Visible = False
                    PnlEditUnit.Visible = False
                    Exit Sub
                End Try
            ElseIf Me.isEdit = False Then
                Try
                    oController.SupplierINCTVDetailAFAdd(oCustomClass)                    
                    ShowMessage(lblMessage, "Record is added successfully ", True)
                    pnlEditDetail.Visible = True
                    PnlEditAF.Visible = False
                    PnlEditUnit.Visible = False
                Catch ex As Exception                    
                    ShowMessage(lblMessage, "Error: " & ex.Message & "<br>" & ex.Source, True)
                    pnlEditDetail.Visible = True
                    PnlEditAF.Visible = False
                    PnlEditUnit.Visible = False
                    Exit Sub
                End Try
            End If
        End If
    End Sub
#End Region
#Region "PrivateSub"
    Private Sub CheckIfEditORAddDetail()
        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(GetConnectionString)
        Dim CategoryID As String
        If Me.StatusScheme = "A" Then
            CategoryID = ""
        ElseIf Me.StatusScheme = "U" Then
            CategoryID = cboCategory.SelectedItem.Value.Trim
        End If
        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.CommandText = "spSupplierINCTVCheckEditOrAddNewDetail"
            objCommand.Parameters.Add("@cardID", SqlDbType.Int).Value = Me.CardID
            objCommand.Parameters.Add("@BaseTarget", SqlDbType.Char, 1).Value = Me.StatusScheme
            objCommand.Parameters.Add("@CategoryID", SqlDbType.VarChar, 50).Value = CategoryID.Trim
            objCommand.Parameters.Add("@IsDataExists", SqlDbType.Bit).Direction = ParameterDirection.Output
            objCommand.Connection = objConnection
            objCommand.ExecuteNonQuery()
            Me.isEdit = CBool(IIf(IsDBNull(objCommand.Parameters("@IsDataExists").Value), "", objCommand.Parameters("@IsDataExists").Value))
        Catch ex As Exception
            Response.Write(ex.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
            objCommand.Dispose()
        End Try
    End Sub
    Private Sub BindData()
        Dim dtlist As DataTable
        Dim dtvEntity As DataView
        Try
            Dim strConnection As String = GetConnectionString()
            With oCustomClass
                .strConnection = GetConnectionString()
                .CardID = Me.CardID
                .SPName = "spSupplierINCTVViewByID"
            End With
            oCustomClass = oController.GetViewByID(oCustomClass)
            dtlist = oCustomClass.ListData
            If dtlist.Rows.Count > 0 Then
                lblCardName.Text = CStr(dtlist.Rows(0).Item("CardName")).Trim
                lblValidFrom.Text = CDate(dtlist.Rows(0).Item("ValidFrom")).ToString("dd/MM/yyyy")
                LblValidUntil.Text = CDate(dtlist.Rows(0).Item("ValidUntil")).ToString("dd/MM/yyyy")
                lblMinimumAFIncentiveUnit.Text = FormatNumber(dtlist.Rows(0).Item("MinAFIncentiveUnit"), 0).Trim
                LBLIntervalUnit.Text = CStr(dtlist.Rows(0).Item("IntervalUnit")).Trim
                lblIntervalAF.Text = CStr(dtlist.Rows(0).Item("IntervalAF")).Trim
                LBLRefundPremi.Text = FormatNumber(dtlist.Rows(0).Item("RefundPremi"), 2).Trim
                Me.IntervalUnit = CInt(dtlist.Rows(0).Item("IntervalUnit"))
                Me.IntervalAF = CInt(dtlist.Rows(0).Item("IntervalAF"))
            End If
        Catch ex As Exception            
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub
    Private Sub BindComboCategory()
        Dim dtlist As DataTable
        Dim dtvEntity As DataView
        Try
            Dim strConnection As String = GetConnectionString()
            With oCustomClass
                .strConnection = GetConnectionString()
                .CardID = Me.CardID
                .SPName = "spSupplierINCTVGetComboCategory"
            End With
            oCustomClass = oController.GetViewByID(oCustomClass)
            dtlist = oCustomClass.ListData

            With cboCategory
                .DataSource = dtlist
                .DataValueField = "CategoryID"
                .DataTextField = "Description"
                .DataBind()
                .Items.Insert(0, "Select One")
                .Items(0).Value = "0"
                .Items.Insert(1, "ALL")
                .Items(0).Value = "ALL"
            End With
        Catch ex As Exception            
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub
#End Region
#Region "BindGridEntity"
    Sub BindGridUnit(ByVal cmdWhere As String)
        Dim dtvEntity As DataView
        'Dim dtsEntity As DataTable
        Dim cSupplierIncentive As New GeneralPagingController
        Dim oSupplierIncentive As New Parameter.GeneralPaging
        Dim IntLoopGrid As Integer
        Dim txtUnitMIN As TextBox
        Dim TxtIncentiveUnit As TextBox
        Dim txtUnitMax As TextBox

        With oSupplierIncentive
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = Me.CardID
            .PageSize = pageSize
            .SortBy = ""
            .SpName = "spSupplierINCTVDetailUnitPerCategory"
        End With

        oSupplierIncentive = cSupplierIncentive.GetGeneralPaging(oSupplierIncentive)

        Me.dtsEntity = oSupplierIncentive.ListData
        dtvEntity = Me.dtsEntity.DefaultView
        'dtvEntity.Sort = Me.SortBy

        'If dtvEntity.Count <= 1 Then
        '    For IntLoopGrid = 0 To Me.IntervalUnit - 1
        '        txtUnitMIN = CType(dtgPagingUnit.Items(IntLoopGrid).Cells(0).FindControl("txtUnitMIN"), TextBox)
        '        txtUnitMax = CType(dtgPagingUnit.Items(IntLoopGrid).Cells(1).FindControl("txtUnitMax"), TextBox)
        '        TxtIncentiveUnit = CType(dtgPagingUnit.Items(IntLoopGrid).Cells(2).FindControl("TxtIncentiveUnit"), TextBox)
        '        txtUnitMIN.Text = "0"
        '        txtUnitMax.Text = "0"
        '        TxtIncentiveUnit.Text = "0"
        '    Next
        'End If

        dtgPagingUnit.DataSource = dtvEntity
        Try
            dtgPagingUnit.DataBind()
            ImbSaveCategory.Visible = True
            imbCancelCategory.Visible = True
            dtgPagingUnit.Visible = True
        Catch
            dtgPagingUnit.DataBind()
        End Try

    End Sub
    Sub BindGridAF(ByVal cmdWhere As String)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim cSupplierIncentive As New GeneralPagingController
        Dim oSupplierIncentive As New Parameter.GeneralPaging
        Dim IntLoopGrid As Integer
        Dim txtBaseAFUnitMIN As TextBox
        Dim txtBaseAFUNitMax As TextBox
        Dim txtRateIncentiveAF As TextBox

        With oSupplierIncentive
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = Me.CardID
            .PageSize = pageSize
            .SortBy = ""
            .SpName = "spSupplierINCTVDetailAF"
        End With

        oSupplierIncentive = cSupplierIncentive.GetGeneralPaging(oSupplierIncentive)

        Me.dtsEntity = oSupplierIncentive.ListData
        dtvEntity = Me.dtsEntity.DefaultView
        'dtvEntity.Sort = Me.SortBy


        'If dtvEntity.Count <= 1 Then
        '    For IntLoopGrid = 0 To Me.IntervalAF - 1
        '        txtBaseAFUnitMIN = CType(DtgPagingAF.Items(IntLoopGrid).Cells(0).FindControl("txtBaseAFUnitMIN"), TextBox)
        '        txtBaseAFUNitMax = CType(DtgPagingAF.Items(IntLoopGrid).Cells(1).FindControl("txtBaseAFUNitMax"), TextBox)
        '        txtRateIncentiveAF = CType(DtgPagingAF.Items(IntLoopGrid).Cells(2).FindControl("txtRateIncentiveAF"), TextBox)
        '        txtBaseAFUnitMIN.Text = "0"
        '        txtBaseAFUNitMax.Text = "0"
        '        txtRateIncentiveAF.Text = "0"
        '    Next
        'End If

        DtgPagingAF.DataSource = dtvEntity
        Try
            DtgPagingAF.DataBind()
            ImbSaveAF.Visible = True
            ImbCancelAF.Visible = True
        Catch
            DtgPagingAF.DataBind()
        End Try

    End Sub
#End Region
End Class