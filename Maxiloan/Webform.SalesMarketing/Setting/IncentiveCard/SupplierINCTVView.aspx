﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SupplierINCTVView.aspx.vb" Inherits="Maxiloan.Webform.SalesMarketing.SupplierINCTVView" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>View of Supplier Incentive Card</title>
    <script type="text/javascript" language="JavaScript">
        function fClose() {
            window.close();
            return false;
        }			
	</script>
    <script type="text/javascript" src="../../../Maxiloan.js"></script>
    <link rel="Stylesheet" href="../../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../../Include/General.css" type="text/css" />
</head>
<body>
    <form id="Form1" runat="server">
        <asp:Label ID="lblMessage" runat="server" Visible ="false" ></asp:Label>
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">                            
                <h3>
                    View of Supplier Incentive Card
                </h3>
            </div>
        </div> 		
		<asp:panel id="pnlList" runat="server">
        <div class="form_box">
	        <div class="form_single">
                <label>Card Name</label>
                <asp:Label id="txtCardName" runat="server"></asp:Label>
	        </div>
        </div> 
        <div class="form_box">
	        <div class="form_single">
                <label>Valid Date</label>
                <asp:Label id="lblValidFrom" Runat="Server"></asp:Label>&nbsp;TO&nbsp;
				<asp:Label id="LblValidUntil" Runat="Server"></asp:Label>
	        </div>
        </div> 
        <div class="form_box">
	        <div class="form_single">
                <label>Minimum AF Incentive Unit</label>
                <asp:Label id="lblMinimumAFIncentiveUnit" runat="server" ></asp:Label>
	        </div>
        </div>  
        <div class="form_box">
	        <div class="form_single">
                <label>#of Interval for Unit</label>
                <asp:Label id="LBLIntervalUnit" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label># of Interval for AF</label>
                <asp:Label id="lblIntervalAF" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>% Refund Premium</label>
                <asp:Label id="LBLRefundPremi" runat="server"></asp:Label>%
	        </div>
        </div>  
        <div class="form_box_header">
            <div class="form_single">              
                <h4>
                    Unit Base
                </h4>
            </div>
        </div>
        <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:datagrid id="dtgPagingUnit" runat="server" width="100%" allowsorting="False"
								autogeneratecolumns="False" CssClass="grid_general" BorderStyle="None" BorderWidth="0">
								<HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
								<columns>
									<asp:templatecolumn headertext="CATEGORY" sortexpression="CategoryID">										
										<itemtemplate>
											<asp:Label ID="lblCategory" Runat="server" text='<%#container.dataitem("Description")%>'>
											</asp:Label>
										</itemtemplate>
									</asp:templatecolumn>
									<asp:templatecolumn headertext="UNIT MIN" sortexpression="UnitMin">										
										<itemtemplate>
											<asp:label ID="lblUnitMIN" Runat="server" text='<%#FormatNumber(container.dataitem("UnitMin"),0)%>'>
											</asp:label>
										</itemtemplate>
									</asp:templatecolumn>
									<asp:templatecolumn headertext="UNIT MAX" sortexpression="UnitMax">										
										<itemtemplate>
											<asp:label ID="LblUnitMax" Runat="server" text='<%#FormatNumber(container.dataitem("UnitMax"),0)%>'>
											</asp:label>
										</itemtemplate>
									</asp:templatecolumn>
									<asp:templatecolumn headertext="Incentive Unit" sortexpression="IncentiveUnit">										
										<itemtemplate>
											<asp:label ID="LblIncentiveUnit" Runat="server" text='<%#FormatNumber(container.dataitem("IncentiveUnit"),0)%>'>
											</asp:label>
										</itemtemplate>
									</asp:templatecolumn>
								</columns>
							</asp:datagrid>
	        </div>
        </div>     
        </div>
        <div class="form_box_header">
            <div class="form_single">              
                <h4>
                    Amount Base
                </h4>
            </div>
        </div>
        <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:datagrid id="DtgPagingAF" runat="server" width="100%" allowsorting="False"
								autogeneratecolumns="False" CssClass="grid_general" BorderStyle="None" BorderWidth="0">
								<HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
								<columns>
									<asp:templatecolumn headertext="AF MIN" sortexpression="UnitMin">										
										<itemtemplate>
											<asp:Label ID="LblBaseAFUnitMIN" Runat="server" text='<%#FormatNumber(container.dataitem("UnitMin"),0)%>'>
											</asp:Label>
										</itemtemplate>
									</asp:templatecolumn>
									<asp:templatecolumn headertext="AF MAX" sortexpression="BaseAFUNitMax">										
										<itemtemplate>
											<asp:label ID="LblBaseAFUNitMax" Runat="server" text='<%#formatNumber(container.dataitem("UnitMax"),0)%>'>
											</asp:label>
										</itemtemplate>
									</asp:templatecolumn>
									<asp:templatecolumn headertext="% Incentive" sortexpression="RateIncentiveAF">										
										<itemtemplate>
											<asp:label ID="LblRateIncentiveAF" Runat="server" text='<%#container.dataitem("RateIncentiveAF")%>'>
											</asp:label>
										</itemtemplate>
									</asp:templatecolumn>
								</columns>
							</asp:datagrid>
	        </div>
        </div>     
        </div>     
        <div class="form_box">
	        <div class="form_single">

	        </div>
        </div>              				
		<div class="form_button">            
            <asp:Button ID="imbClose" runat="server" CausesValidation="false" Text="Close" CssClass ="small button gray">
            </asp:Button>     
        </div>
		</asp:panel>
	</form>
</body>
</html>
