﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SupplierIncentiveMnt.aspx.vb"
    Inherits="Maxiloan.Webform.SalesMarketing.SupplierIncentiveMnt" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Supplier Incentive Card</title>
    <script type="text/javascript" language="JavaScript">
        var x = screen.width;
        var y = screen.height - 100;

        function fClose() {
            window.close();
            return false;
        }
        function fConfirm() {
            if (confirm("Are you sure want to delete this record ?")) {
                return true;
            }
            else {
                return false;
            }
        }

        var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%#Request.servervariables("SERVER_NAME")%>/';			
    
        function OpenSupplierINCTVView(pStyle, pCardID) {
            var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            window.open('http://<%#Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.SalesMarketing/Setting/IncentiveCard/SupplierINCTVVIEW.aspx?Style=' + pStyle + '&CardID=' + pCardID, 'Incentive', 'left=0, top=0, width=' + screen.width + x + ', height=' + y + ', menubar=0, scrollbars=1');
        }							
    </script>
    <script type="text/javascript" src="../../../Maxiloan.js"></script>
    <link rel="Stylesheet" href="../../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../../Include/Buttons.css" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
    <ContentTemplate>
        <asp:Label ID="lblMessage" runat="server" Visible ="false" ></asp:Label>                 
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">                   
                <h3>
                    List of Supplier Incentive Card
                </h3>
            </div>
        </div>         
        <asp:Panel ID="pnlList" runat="server">        
        <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgPaging" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="Sorting" BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="EDIT DETAIL">
                                <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImbEditDetail" runat="server" ImageUrl="../../../Images/iconDetail.gif"
                                        CommandName="EditDetail" CausesValidation="False"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="EDIT">
                                <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImbEdit" runat="server" ImageUrl="../../../Images/iconedit.gif"
                                        CommandName="Edit" CausesValidation="False"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DELETE">                                
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImbDelete" runat="server" ImageUrl="../../../Images/icondelete.gif"
                                        CommandName="Delete" CausesValidation="False"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="SupplierIncentiveCardID" SortExpression="SupplierIncentiveCardID"
                                Visible="False">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblSupplierIncentiveCardID" runat="server" Text='<%#container.dataitem("SupplierIncentiveCardID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Card Name" SortExpression="CardName">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyCardName" runat="server" Text='<%#container.dataitem("CardName")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Valid From" SortExpression="ValidFrom">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblValidFrom" runat="server" Text='<%#container.dataitem("ValidFrom")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="ValidUntil" SortExpression="ValidUntil">                                
                                <ItemTemplate>
                                    <asp:Label ID="LblValidUntil" runat="server" Text='<%#container.dataitem("ValidUntil")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>                        
                <div class="button_gridnavigation">
	                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../../Images/grid_navbutton01.png"
		                CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../../Images/grid_navbutton02.png"
		                CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../../Images/grid_navbutton03.png"
		                CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../../Images/grid_navbutton04.png"
		                CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
	                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
		                EnableViewState="False"></asp:Button>
	                <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage"
		                MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer"
		                CssClass="validator_general"></asp:RangeValidator>
	                <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
		                ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>
                <div class="label_gridnavigation">
	                <asp:Label ID="lblPage" runat="server"></asp:Label>of
	                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
	                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                </div>
            </div>
        </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonAdd" runat="server" CausesValidation="False"  Text="Add" CssClass ="small button blue">
            </asp:Button>
            <asp:Button ID="ButtonPrint" runat="server" Text="Print" CssClass ="small button blue"
            Enabled="true"></asp:Button>
        </div>
        <div class="form_box_title">
            <div class="form_single">              
                <h4>
                    Supplier Incentive Card
                </h4>
            </div>
        </div> 
        <div class="form_box">
	        <div class="form_single">
                <label>Find By</label>
                <asp:DropDownList ID="cboSearch" runat="server">
                    <asp:ListItem Value="CardName">Card Name</asp:ListItem>
                </asp:DropDownList>                
                <asp:TextBox ID="txtSearch" runat="server" ></asp:TextBox>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label class ="label_req">
				Valid Date</label>
                <asp:TextBox ID="txtSDate" runat="server"></asp:TextBox>                   
                <asp:CalendarExtender ID="txtSDate_CalendarExtender" runat="server" 
                    Enabled="True" TargetControlID="txtSDate" Format ="dd/MM/yyyy"></asp:CalendarExtender>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*" CssClass="validator_general"
                ControlToValidate="txtSDate"></asp:RequiredFieldValidator>
                &nbsp; To &nbsp;&nbsp;
                <asp:TextBox ID="txtEDate" runat="server"></asp:TextBox>                   
                <asp:CalendarExtender ID="txtEDate_CalendarExtender" runat="server" 
                    Enabled="True" TargetControlID="txtEDate" Format ="dd/MM/yyyy"></asp:CalendarExtender>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*" CssClass="validator_general"
                ControlToValidate="txtEDate"></asp:RequiredFieldValidator>
	        </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonSearch" runat="server" CausesValidation="False"  Text="Search" CssClass ="small button blue">
            </asp:Button>&nbsp;
            <asp:Button ID="ButtonReset" runat="server" CausesValidation="False"  Text="Reset" CssClass ="small button gray">
            </asp:Button>
        </div>        
        </asp:Panel>
        <asp:Panel ID="pnlAddEdit" runat="server" Visible="False">        
        
        <div class="form_box_header">
            <div class="form_single">              
                <h4>
                    Supplier Incentive Card tes -
                    <asp:Label ID="lblAddEdit" runat="server"></asp:Label>
                </h4>
            </div>
        </div       
        ><div class="form_box">
        </div>                                                      
        <div class="form_box">
	        <div class="form_single">
                 <label class ="label_req">
				 Card Name</label>
                <asp:TextBox ID="txtCardName" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rgvtxtCardName" runat="server" ControlToValidate="txtCardName" CssClass="validator_general"
                    ErrorMessage="Please fill in Card Name" Display="Dynamic"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                 <label class ="label_req">
				 Value Date</label>
                <asp:TextBox ID="txtoValidFrom" runat="server"></asp:TextBox>                   
                <asp:CalendarExtender ID="txtoValidFrom_CalendarExtender" runat="server" 
                    Enabled="True" TargetControlID="txtoValidFrom" Format ="dd/MM/yyyy"></asp:CalendarExtender>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*" CssClass="validator_general"
                ControlToValidate="txtoValidFrom"></asp:RequiredFieldValidator>
                &nbsp; To &nbsp;&nbsp;
                <asp:TextBox ID="txtoValidUntil" runat="server"></asp:TextBox>                   
                <asp:CalendarExtender ID="txtoValidUntilCalendarExtender" runat="server" 
                    Enabled="True" TargetControlID="txtoValidUntil" Format ="dd/MM/yyyy"></asp:CalendarExtender>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="*" CssClass="validator_general"
                ControlToValidate="txtoValidUntil"></asp:RequiredFieldValidator>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label class ="label_req">
				Minimum AF Incentive Unit</label>
                <asp:TextBox ID="TxtMinimumAFIncentiveUnit" runat="server" ></asp:TextBox>
                <asp:RangeValidator ID="RVTxtMinimumAFIncentiveUnit" runat="server" ControlToValidate="TxtMinimumAFIncentiveUnit" CssClass="validator_general"
                    Type="Double" MinimumValue="0" ErrorMessage="Fill In Numeric Value" Display="Dynamic"
                    MaximumValue="999999999999999"></asp:RangeValidator>
                <asp:RequiredFieldValidator ID="RFVTxtMinimumAFIncentiveUnit" runat="server" ControlToValidate="TxtMinimumAFIncentiveUnit" CssClass="validator_general"
                    ErrorMessage="Please fill in Minimum AF Incentive Unit"></asp:RequiredFieldValidator>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                 <label class ="label_req"># of Interval for Unit</label>
                <asp:TextBox ID="TxtIntervalUnit" runat="server" ></asp:TextBox>
                <asp:RangeValidator ID="RVTxtIntervalUnit" runat="server" ControlToValidate="TxtIntervalUnit" CssClass="validator_general"
                    Type="Double" MinimumValue="0" ErrorMessage="Fill In Numeric Value" Display="Dynamic"
                    MaximumValue="99"></asp:RangeValidator>
                <asp:RequiredFieldValidator ID="RfvTxtIntervalUnit" runat="server" ControlToValidate="TxtIntervalUnit" CssClass="validator_general"
                    ErrorMessage="Please fill in Interval Unit"></asp:RequiredFieldValidator>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                 <label class ="label_req"># of Interval for AF</label>
                <asp:TextBox ID="TxtIntervalAF" runat="server" ></asp:TextBox>
                <asp:RangeValidator ID="RVTxtIntervalAF" runat="server" ControlToValidate="TxtIntervalAF" CssClass="validator_general"
                    Type="Double" MinimumValue="0" ErrorMessage="Fill In Numeric Value" Display="Dynamic"
                    MaximumValue="99"></asp:RangeValidator>
                <asp:RequiredFieldValidator ID="RfvTxtIntervalAF" runat="server" ControlToValidate="TxtIntervalAF" CssClass="validator_general"
                    ErrorMessage="Please fill in Interval AF"></asp:RequiredFieldValidator>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>% Refund Premium</label>
                <asp:TextBox ID="TxtRefundPremi" runat="server" ></asp:TextBox>%
                <asp:RangeValidator ID="rgvTxtRefundPremi" runat="server" ControlToValidate="TxtRefundPremi" CssClass="validator_general"
                    Type="Double" MinimumValue="0" ErrorMessage="Refund Premi is not valid!" MaximumValue="100"
                    Enabled="True"></asp:RangeValidator>
	        </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonSAVE" runat="server" CausesValidation="False"  Text="Save" CssClass ="small button blue">
            </asp:Button>&nbsp;
            <asp:Button ID="ButtonCancel" runat="server" CausesValidation="False"  Text="Cancel" CssClass ="small button gray">
            </asp:Button>
        </div>       
    </asp:Panel>
    </ContentTemplate>
    </asp:UpdatePanel>    
    </form>
</body>
</html>
