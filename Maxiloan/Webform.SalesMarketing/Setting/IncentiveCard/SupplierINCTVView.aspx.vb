﻿#Region "Imports"
Imports Maxiloan.cbse
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region


Public Class SupplierINCTVView
    Inherits Maxiloan.Webform.WebBased

#Region "Property"
    Private Property CardID() As Integer
        Get
            Return CType(ViewState("CardID"), Integer)
        End Get
        Set(ByVal Value As Integer)
            ViewState("CardID") = Value
        End Set
    End Property
#End Region
#Region "PrivateConst"
    Private oCustomClass As New Parameter.IncentiveCard
    Private oController As New IncentiveCardController
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Dim Cmdwhere As String
        If Not Me.IsPostBack Then
            If SessionInvalid() Then
                Exit Sub
            End If
            If Request.QueryString("CardID") <> "" Then Me.CardID = CInt(Request.QueryString("CardID"))
            View()
            Cmdwhere = "SupplierIncentiveCardD.SupplierIncentiveCardID = " & Me.CardID & ""
            BindGridUnit(Cmdwhere)
            BindGridAF(Cmdwhere)
            imbClose.Attributes.Add("Onclick", "fClose();")
        End If
    End Sub
    Private Sub View()
        Dim dtlist As DataTable
        Try
            Dim strConnection As String = GetConnectionString()
            With oCustomClass
                .strConnection = GetConnectionString()
                .CardID = Me.CardID
                .SPName = "spSupplierINCTVViewByID"
            End With
            oCustomClass = oController.GetViewByID(oCustomClass)
            dtlist = oCustomClass.ListData
            If dtlist.Rows.Count > 0 Then
                txtCardName.Text = CStr(dtlist.Rows(0).Item("CardName")).Trim
                lblValidFrom.Text = CDate(dtlist.Rows(0).Item("ValidFrom")).ToString("dd/MM/yyyy")
                LblValidUntil.Text = CDate(dtlist.Rows(0).Item("ValidUntil")).ToString("dd/MM/yyyy")
                lblMinimumAFIncentiveUnit.Text = FormatNumber(dtlist.Rows(0).Item("MinAFIncentiveUnit"), 0).Trim
                LBLIntervalUnit.Text = CStr(dtlist.Rows(0).Item("IntervalUnit")).Trim
                lblIntervalAF.Text = CStr(dtlist.Rows(0).Item("IntervalAF")).Trim
                LBLRefundPremi.Text = FormatNumber(dtlist.Rows(0).Item("RefundPremi"), 2).Trim
            End If
        Catch ex As Exception            
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub
#Region "BindGridEntity"
    Sub BindGridUnit(ByVal cmdWhere As String)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim cSupplierIncentive As New GeneralPagingController
        Dim oSupplierIncentive As New Parameter.GeneralPaging

        With oSupplierIncentive
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = 1
            .PageSize = 10
            .SortBy = ""
            .SpName = "spSupplierINCTVViewDetailUnitPerCategory"
        End With

        oSupplierIncentive = cSupplierIncentive.GetGeneralPaging(oSupplierIncentive)

        dtsEntity = oSupplierIncentive.ListData
        dtvEntity = dtsEntity.DefaultView

        dtgPagingUnit.DataSource = dtvEntity
        Try
            dtgPagingUnit.DataBind()
        Catch
            dtgPagingUnit.DataBind()
        End Try

    End Sub
    Sub BindGridAF(ByVal cmdWhere As String)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim cSupplierIncentive As New GeneralPagingController
        Dim oSupplierIncentive As New Parameter.GeneralPaging

        With oSupplierIncentive
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = 1
            .PageSize = 10
            .SortBy = ""
            .SpName = "spSupplierINCTVViewDetailAF"
        End With

        oSupplierIncentive = cSupplierIncentive.GetGeneralPaging(oSupplierIncentive)

        dtsEntity = oSupplierIncentive.ListData
        dtvEntity = dtsEntity.DefaultView

        DtgPagingAF.DataSource = dtvEntity
        Try
            DtgPagingAF.DataBind()
        Catch
            DtgPagingAF.DataBind()
        End Try

    End Sub
#End Region
End Class