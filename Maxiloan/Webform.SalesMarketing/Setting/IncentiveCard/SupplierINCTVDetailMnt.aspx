﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SupplierINCTVDetailMnt.aspx.vb" Inherits="Maxiloan.Webform.SalesMarketing.SupplierINCTVDetailMnt" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>SupplierINCTVDetailMnt</title>
    <script type="text/javascript" language="JavaScript">
            var x = screen.width;
            var y = screen.height - 100;
            function fClose() {
                window.close();
                return false;
            }			
		
		    var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
		    var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
		    var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';			
		
		    function OpenSupplierINCTVView(pStyle, pCardID) {
		        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
		        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
		        window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.SalesMarketing/Setting/IncentiveCard/SupplierINCTVVIEW.aspx?Style=' + pStyle + '&CardID=' + pCardID, 'Incentive', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=1');
		    }							
	</script>
	<script type="text/javascript" src="../../../Maxiloan.js"></script>
    <link rel="Stylesheet" href="../../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../../Include/General.css" type="text/css" />
</head>
<body>
    <form id="Form1" runat="server">
        <asp:Label ID="lblMessage" runat="server" Visible ="false" ></asp:Label>  
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">                          
                <h3>
                    Supplier Incentive Card Detail
                </h3>
            </div>
        </div> 		
		<asp:panel id="pnlEditDetail" runat="server">
        <div class="form_box">
	        <div class="form_single">
                <label>Card Name</label>
                <asp:Label id="lblCardName" runat="server"></asp:Label>
	        </div>
        </div> 		
        <div class="form_box">
	        <div class="form_single">
                <label>Value Date</label>
                <asp:Label id="lblValidFrom" Runat="Server"></asp:Label>&nbsp;TO&nbsp;
				<asp:Label id="LblValidUntil" Runat="Server"></asp:Label>
	        </div>
        </div> 
        <div class="form_box">
	        <div class="form_single">
                <label>Minimum AF Incentive Unit</label>
                <asp:Label id="lblMinimumAFIncentiveUnit" runat="server"></asp:Label>
	        </div>
        </div> 
        <div class="form_box">
	        <div class="form_single">
                <label># of Interval for Unit</label>
                <asp:Label id="LBLIntervalUnit" runat="server"></asp:Label>
	        </div>
        </div> 
        <div class="form_box">
	        <div class="form_single">
                <label># of Interval for AF</label>
                <asp:Label id="lblIntervalAF" runat="server"></asp:Label>
	        </div>
        </div> 
        <div class="form_box">
	        <div class="form_single">
                <label>% Refund Premium</label>
                <asp:Label id="LBLRefundPremi" runat="server"></asp:Label>%
	        </div>
        </div> 
        <div class="form_box">
	        <div class="form_single">
                <label>Status Scheme</label>
                <asp:DropDownList id="cboStatusScheme" Runat="server">
					<asp:ListItem Value="Unit" Selected="True">Unit</asp:ListItem>
					<asp:ListItem Value="AF">Amount Funding</asp:ListItem>
				</asp:DropDownList>
	        </div>
        </div> 
        <div class="form_button">
            <asp:Button id="ImbOKEdit" Runat="server" Text="Ok" CssClass ="small button blue"></asp:Button>
			<asp:Button id="ImbCancelEdit" Runat="server" Text="Cancel" CssClass ="small button gray"></asp:Button>
        </div>        				
		</asp:panel>
		<asp:panel id="PnlEditUnit" runat="server">
        <div class="form_box_header">
            <div class="form_single">              
                <h4>
                    Unit Base
                </h4>
            </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Category</label>
                <asp:DropDownList id="cboCategory" Runat="server"></asp:DropDownList>
                <asp:Button id="imbOKCategory" Runat="server" Text="Ok" CssClass ="small button blue"></asp:Button>
	        </div>
        </div>
        <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:datagrid id="dtgPagingUnit" runat="server" width="100%" CssClass="grid_general" BorderStyle="None" BorderWidth="0"
								autogeneratecolumns="False" allowsorting="False">								
								<HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
								<columns>
									<asp:templatecolumn headertext="UNIT MIN" sortexpression="UNITMIN">										
										<itemtemplate>
											<asp:TextBox ID="txtUnitMIN" Runat="server" text='<%#container.dataitem("UNITMIN")%>'>
											</asp:TextBox>
										</itemtemplate>
									</asp:templatecolumn>
									<asp:templatecolumn headertext="UNIT MAX" sortexpression="UNITMAX">										
										<itemtemplate>
											<asp:TextBox ID="txtUnitMax" Runat="server" text='<%#container.dataitem("UnitMax")%>'>
											</asp:TextBox>
										</itemtemplate>
									</asp:templatecolumn>
									<asp:templatecolumn headertext="Incentive Unit" sortexpression="IncentiveUnit">										
										<itemtemplate>
											<asp:textBox ID="TxtIncentiveUnit" Runat="server" text='<%#container.dataitem("IncentiveUnit")%>'>
											</asp:textBox>
										</itemtemplate>
									</asp:templatecolumn>
								</columns>
							</asp:datagrid>
	        </div>
        </div> 	
        </div> 	
        <div class="form_button">
            <asp:Button id="ImbSaveCategory" Runat="server" Text="Save" CssClass ="small button blue"></asp:Button>
		    <asp:Button id="imbCancelCategory" Runat="server" Text="Cancel" CssClass ="small button gray"></asp:Button>
        </div>        								
		</asp:panel>
		<asp:panel id="PnlEditAF" runat="server">
        <div class="form_box_header">
            <div class="form_single">              
                <h4>    
                    Amount Finance Base
                </h4>
            </div>
        </div>
        <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:datagrid id="DtgPagingAF" runat="server" width="100%" CssClass="grid_general" BorderStyle="None" BorderWidth="0"
								autogeneratecolumns="False" allowsorting="False">
								<HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
								<columns>
									<asp:templatecolumn headertext="AF MIN" sortexpression="BaseAFUnitMIN">										
										<itemtemplate>
											<asp:TextBox ID="txtBaseAFUnitMIN" Runat="server" text='<%#container.dataitem("UnitMin")%>'>
											</asp:TextBox>
										</itemtemplate>
									</asp:templatecolumn>
									<asp:templatecolumn headertext="AF MAX" sortexpression="BaseAFUNitMax">										
										<itemtemplate>
											<asp:TextBox ID="txtBaseAFUNitMax" Runat="server" text='<%#container.dataitem("UnitMax")%>'>
											</asp:TextBox>
										</itemtemplate>
									</asp:templatecolumn>
									<asp:templatecolumn headertext="% Incentive" sortexpression="RateIncentiveAF">										
										<itemtemplate>
											<asp:TextBox ID="txtRateIncentiveAF" Runat="server" text='<%#formatnumber(container.dataitem("RateIncentiveAF"),8)%>'>
											</asp:TextBox>
										</itemtemplate>
									</asp:templatecolumn>
								</columns>
							</asp:datagrid>
	        </div>
        </div> 	
        </div> 
        <div class="form_button">
            <asp:Button id="ImbSaveAF" Runat="server" Text="Save" CssClass ="small button blue"></asp:Button>
			<asp:Button id="ImbCancelAF" Runat="server" Text="Cancel" CssClass ="small button gray"></asp:Button>
        </div>
	    </asp:panel>
	</form>        
</body>
</html>
