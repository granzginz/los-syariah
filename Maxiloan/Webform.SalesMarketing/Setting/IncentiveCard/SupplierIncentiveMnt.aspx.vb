﻿
#Region "Imports"
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.cbse
Imports Maxiloan.Exceptions
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
#End Region

Public Class SupplierIncentiveMnt
    Inherits Maxiloan.Webform.WebBased

    
    Protected WithEvents oValidUntil As ValidDate

#Region "Property"
    Private Property IsIntervalChanged() As Boolean
        Get
            Return CType(viewstate("IsIntervalChanged"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            viewstate("IsIntervalChanged") = Value
        End Set
    End Property

    Private Property CmdWhere() As String
        Get
            Return CType(viewstate("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property ActionAddEdit() As String
        Get
            Return CType(viewstate("ActionAddEdit"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ActionAddEdit") = Value
        End Set
    End Property
    Private Property CardID() As Integer
        Get
            Return CType(viewstate("CardID"), Integer)
        End Get
        Set(ByVal Value As Integer)
            viewstate("CardID") = Value
        End Set
    End Property
    Private Property IntervalUnit() As Integer
        Get
            Return CType(viewstate("IntervalUnit"), Integer)
        End Get
        Set(ByVal Value As Integer)
            viewstate("IntervalUnit") = Value
        End Set
    End Property
    Private Property IntervalAF() As Integer
        Get
            Return CType(viewstate("IntervalAF"), Integer)
        End Get
        Set(ByVal Value As Integer)
            viewstate("IntervalAF") = Value
        End Set
    End Property
    Private Property isDataExists() As Boolean
        Get
            Return CType(viewstate("isDataExists"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            viewstate("isDataExists") = Value
        End Set
    End Property
#End Region
#Region "PrivateConst"
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private oCustomClass As New Parameter.IncentiveCard
    Private oController As New IncentiveCardController
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        'Put user code to initialize the page here
        If SessionInvalid() Then
            Exit Sub
        End If
        Me.FormID = "SUPPINCTVMNT"
        If Not IsPostBack Then
            pnlList.Visible = True
            pnlAddEdit.Visible = False
            Me.CmdWhere = ""
            Me.SortBy = ""
            Me.IsIntervalChanged = False
            BindGridEntity(Me.CmdWhere)
        End If
    End Sub
#Region "ButtonClick"
    Private Sub imbSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonSearch.Click
        Me.CmdWhere = ""
        Me.SortBy = ""
        If txtSearch.Text.Trim <> "" Then
            If Right(txtSearch.Text.Trim, 1) = "%" Then
                Me.CmdWhere = Me.CmdWhere + "SupplierIncentiveCardH.Cardname like '" & txtSearch.Text.Trim & "' and "
            Else
                Me.CmdWhere = Me.CmdWhere + "SupplierIncentiveCardH.Cardname = '" & txtSearch.Text.Trim & "' and "
            End If
        End If
        If txtSDate.Text <> "" Then
            CmdWhere = CmdWhere + "SupplierIncentiveCardH.ValidFrom >= '" & ConvertDate2(txtSDate.Text.Trim).ToString("yyyyMMdd") & "' And "
        End If
        If txtEDate.Text <> "" Then
            CmdWhere = CmdWhere + "SupplierIncentiveCardH.ValidUntil <= '" & ConvertDate2(txtEDate.Text.Trim).ToString("yyyyMMdd") & "' And "
        End If
        If Me.CmdWhere.Trim <> "" Then
            Me.CmdWhere = Left(Me.CmdWhere, Len(Me.CmdWhere.Trim) - 4)
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub ImbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        Response.Redirect("SupplierIncentiveMnt.Aspx")
    End Sub
    Private Sub imbReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        Response.Redirect("SupplierIncentiveMnt.Aspx")
    End Sub
    Private Sub imbAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonAdd.Click
        Me.ActionAddEdit = "ADD"
        lblAddEdit.Text = "ADD"
        pnlList.Visible = False
        pnlAddEdit.Visible = True
        TxtIntervalAF.Text = "0"
        TxtIntervalUnit.Text = "0"
        TxtMinimumAFIncentiveUnit.Text = "0"
        TxtRefundPremi.Text = "0"
    End Sub
    Private Sub ImbSAVE_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonSAVE.Click
        Select Case Me.ActionAddEdit
            Case "ADD"
                Add()
                BindGridEntity(Me.CmdWhere)
                pnlList.Visible = True
            Case "EDIT"
                edit()
                If Me.IsIntervalChanged = True Then
                    pnlList.Visible = False
                    pnlAddEdit.Visible = True
                    Me.ActionAddEdit = "EDIT"
                    lblAddEdit.Text = "EDIT"
                    'editView()
                ElseIf Me.IsIntervalChanged = False Then
                    BindGridEntity(Me.CmdWhere)
                    pnlList.Visible = True
                End If

        End Select
    End Sub
#End Region
#Region "BindGridEntity"
    Sub BindGridEntity(ByVal cmdWhere As String)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim cSupplierIncentive As New GeneralPagingController
        Dim oSupplierIncentive As New Parameter.GeneralPaging

        With oSupplierIncentive
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = Me.SortBy
            .SpName = "spSupplierINCTVPaging"
        End With

        oSupplierIncentive = cSupplierIncentive.GetGeneralPaging(oSupplierIncentive)

        With oSupplierIncentive
            lblrecord.Text = CType(.TotalRecords, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecords
        End With

        dtsEntity = oSupplierIncentive.ListData
        dtvEntity = dtsEntity.DefaultView
        dtvEntity.Sort = Me.SortBy

        If dtvEntity.Count <= 0 Then
            ButtonPrint.Enabled = False            
            ShowMessage(lblMessage, "Data not found...", True)
        Else
            ButtonPrint.Enabled = True
        End If

        dtgPaging.DataSource = dtvEntity
        Try
            dtgPaging.DataBind()
        Catch
            dtgPaging.CurrentPageIndex = 0
            dtgPaging.DataBind()
        End Try
        PagingFooter()
    End Sub
#End Region
#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then            
            ShowMessage(lblMessage, "Data not found .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int32)
                BindGridEntity(Me.CmdWhere)
            End If
        End If
    End Sub
    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If

        BindGridEntity(Me.CmdWhere)
        pnlList.Visible = True
        pnlAddEdit.Visible = False
    End Sub
#End Region

    Private Sub dtgPaging_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        Dim lblcardId As Label        
        Select Case e.CommandName
            Case "EditDetail"
                If checkFeature(Me.Loginid, Me.FormID, "Edit", Me.AppId) Then
                    If sessioninvalid() Then
                        Exit Sub
                    End If
                    lblcardId = CType(dtgPaging.Items(e.Item.ItemIndex).FindControl("lblSupplierIncentiveCardID"), Label)
                    Me.CardID = CInt(lblcardId.Text)
                    Response.Redirect("SupplierINCTVDetailMnt.aspx?CardID=" & Me.CardID & "")
                End If
            Case "Edit"
                If checkFeature(Me.Loginid, Me.FormID, "Edit", Me.AppId) Then
                    If sessioninvalid() Then
                        Exit Sub
                    End If
                    lblcardId = CType(dtgPaging.Items(e.Item.ItemIndex).FindControl("lblSupplierIncentiveCardID"), Label)
                    Me.CardID = CInt(lblcardId.Text)
                    pnlList.Visible = False
                    pnlAddEdit.Visible = True
                    Me.ActionAddEdit = "EDIT"
                    lblAddEdit.Text = "EDIT"
                    editView()
                End If
            Case "Delete"
                If checkFeature(Me.Loginid, Me.FormID, "Del", Me.AppId) Then
                    If sessioninvalid() Then
                        Exit Sub
                    End If
                    Me.CmdWhere = ""
                    Me.SortBy = ""
                    lblcardId = CType(dtgPaging.Items(e.Item.ItemIndex).FindControl("lblSupplierIncentiveCardID"), Label)
                    Me.CardID = CInt(lblcardId.Text.Trim)
                    CheckIsDataExistsForSelectedCardID(Me.CardID)
                    If Me.isDataExists = True Then                        
                        ShowMessage(lblMessage, "Incentive CardID has been Used for Supplier Branch IncentiveCard Setting, Therefore process Delete Can't be done", True)
                        Exit Sub
                    Else
                        With oCustomClass
                            .strConnection = getConnectionString
                            .CardID = Me.CardID
                        End With

                        Try
                            oController.SupplierINCTVHeaderDelete(oCustomClass)                            
                            ShowMessage(lblMessage, "Record is deleted successfully ", True)

                        Catch ex As Exception                            
                            ShowMessage(lblMessage, ex.Message, True)

                        Finally
                            BindGridEntity(Me.CmdWhere)
                        End Try
                    End If
                End If
        End Select
    End Sub
#Region "EditView"
    Private Sub editView()
        Dim dtlist As DataTable
        Try
            Dim strConnection As String = getConnectionString
            With oCustomClass
                .strConnection = getConnectionString()
                .CardID = Me.CardID
                .spName = "spSupplierINCTVViewByID"
            End With
            oCustomClass = oController.GetViewByID(oCustomClass)
            dtlist = oCustomClass.ListData
            If dtlist.Rows.Count > 0 Then
                txtCardName.Text = CStr(dtlist.Rows(0).Item("CardName")).Trim
                txtoValidFrom.text = CDate(dtlist.Rows(0).Item("ValidFrom")).ToString("dd/MM/yyyy")
                txtoValidUntil.text = CDate(dtlist.Rows(0).Item("ValidUntil")).ToString("dd/MM/yyyy")
                TxtMinimumAFIncentiveUnit.Text = CStr(dtlist.Rows(0).Item("MinAFIncentiveUnit")).Trim
                TxtIntervalUnit.Text = CStr(dtlist.Rows(0).Item("IntervalUnit")).Trim
                Me.IntervalUnit = CInt(TxtIntervalUnit.Text)
                TxtIntervalAF.Text = CStr(dtlist.Rows(0).Item("IntervalAF")).Trim
                Me.IntervalAF = CInt(TxtIntervalAF.Text)
                TxtRefundPremi.Text = CStr(dtlist.Rows(0).Item("RefundPremi")).Trim
            End If
        Catch ex As Exception            
            ShowMessage(lblMessage, ex.Message, True)
        End Try

    End Sub

#End Region
#Region "ADD"
    Private Sub Add()
        Try
            With oCustomClass
                .strConnection = GetConnectionString
                .CardName = txtCardName.Text.Trim
                .ValidFrom = ConvertDate2(txtoValidFrom.text)
                .ValidUntil = ConvertDate2(txtoValidUntil.text)
                .MinAFIncentiveUnit = CDec(TxtMinimumAFIncentiveUnit.Text)
                .IntervalUnit = CInt(TxtIntervalUnit.Text)
                .IntervalAF = CInt(TxtIntervalAF.Text)
                .RefundPremi = CDec(TxtRefundPremi.Text)
            End With
            oController.SupplierINCTVHeaderAdd(oCustomClass)            
            ShowMessage(lblMessage, "Record is added successfully ", True)
            pnlList.Visible = True
            pnlAddEdit.Visible = False
            BindGridEntity("")
        Catch ex As Exception            
            ShowMessage(lblMessage, "Error: " & ex.Message & "<br>" & ex.Source, True)
            pnlList.Visible = True
            pnlAddEdit.Visible = False
            BindGridEntity(Me.CmdWhere)
            Exit Sub
        End Try
    End Sub

#End Region
#Region "EDIT"
    Private Sub edit()
        If Me.IsIntervalChanged = False Then
            If Me.IntervalUnit <> CInt(TxtIntervalUnit.Text) Or Me.IntervalAF <> CInt(TxtIntervalAF.Text) Then                
                ShowMessage(lblMessage, "You are willing to change Interval For Unit OR Amount Finance, This will cause Changes in Number of Interval for Unit Base or AF Base, Continue to change ? ", True)
                Me.IsIntervalChanged = True
                Me.IntervalUnit = CInt(TxtIntervalUnit.Text)
                Me.IntervalAF = CInt(TxtIntervalAF.Text)
                Exit Sub
            End If
        ElseIf Me.IsIntervalChanged = True Then
            Me.IsIntervalChanged = False
        End If


        With oCustomClass
            .strConnection = GetConnectionString
            .CardName = txtCardName.Text.Trim
            .ValidFrom = ConvertDate2(txtoValidFrom.text)
            .ValidUntil = ConvertDate2(txtoValidUntil.text)
            .MinAFIncentiveUnit = CDec(TxtMinimumAFIncentiveUnit.Text)
            .IntervalUnit = CInt(TxtIntervalUnit.Text)
            .IntervalAF = CInt(TxtIntervalAF.Text)
            .RefundPremi = CDec(TxtRefundPremi.Text)
            .CardID = Me.CardID
        End With
        Try
            oController.SupplierINCTVHeaderEdit(oCustomClass)            
            ShowMessage(lblMessage, "Record is updated successfully ", True)
            pnlList.Visible = True
            pnlAddEdit.Visible = False
            BindGridEntity("")
        Catch ex As Exception            
            ShowMessage(lblMessage, ex.Message, True)
            pnlList.Visible = True
            pnlAddEdit.Visible = False
            BindGridEntity(Me.CmdWhere)
            Exit Sub
        End Try
    End Sub

#End Region
#Region "CheckIsDataExistsForSelectedCardID"
    Public Sub CheckIsDataExistsForSelectedCardID(ByVal CardID As Integer)
        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(getConnectionString)
        Me.isDataExists = False
        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.CommandText = "spSupplierINCTVCheckIsDataExistsForSelectedCardID"
            objCommand.Parameters.Add("@CardID", SqlDbType.Int).Value = CardID
            objCommand.Parameters.Add("@isDataExists", SqlDbType.Bit).Direction = ParameterDirection.Output
            objCommand.Connection = objConnection
            objCommand.ExecuteNonQuery()
            Me.isDataExists = CBool(IIf(IsDBNull(objCommand.Parameters("@isDataExists").Value), "", objCommand.Parameters("@isDataExists").Value))
        Catch ex As Exception
            Response.Write(ex.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
            objCommand.Dispose()
        End Try
    End Sub
#End Region

    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        Dim imbDelete As ImageButton
        Dim HyCardName As HyperLink
        Dim lblSupplierIncentiveCardID As Label
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("ImbDelete"), ImageButton)
            imbDelete.Attributes.Add("onclick", "return fConfirm()")
            HyCardName = CType(e.Item.FindControl("hyCardName"), HyperLink)
            lblSupplierIncentiveCardID = CType(e.Item.FindControl("lblSupplierIncentiveCardID"), Label)
            HyCardName.NavigateUrl = "javascript:OpenSupplierINCTVView('" & "Marketing" & "','" & lblSupplierIncentiveCardID.Text.Trim & "')"
        End If
    End Sub


    Private Sub imbPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonPrint.Click

    End Sub
End Class