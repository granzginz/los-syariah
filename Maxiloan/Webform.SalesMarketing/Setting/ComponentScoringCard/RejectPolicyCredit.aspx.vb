﻿Imports System.DBNull 
Imports Maxiloan.Parameter
Imports Maxiloan.Controller


Public Class RejectPolicyCredit
    Inherits Maxiloan.Webform.WebBased
    Private m_controller As New CreditController
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If

        Me.FormID = "RejectPolicyCredit"

        If Not Me.IsPostBack Then 
            If SessionInvalid() Then
                Exit Sub
            End If
            pnlAddEdit.Visible = False
            Me.SearchBy = ""
            Me.SortBy = ""
            BindGridEntity()
        End If
    End Sub



    Sub BindGridEntity()
        Dim dtEntity As DataTable
        Dim oCredit As New Parameter.Credit

        With oCredit
            .WhereCond = ""
            .CurrentPage = 1
            .PageSize = 1
            .SortBy = ""
            .SPName = "spScoreRejectPolicyCreditPaging"
            .strConnection = GetConnectionString()
        End With
        oCredit = m_controller.GetCredit(oCredit)

        If Not oCredit Is Nothing Then
            dtEntity = oCredit.ListData
        End If


        dtgRejectPolicy.DataSource = dtEntity.DefaultView
        dtgRejectPolicy.CurrentPageIndex = 0
        dtgRejectPolicy.DataBind()

    End Sub



    Private Sub dtgRejectPolicy_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgRejectPolicy.ItemCommand
        buttonSave.Visible = True
        buttonCancel.Visible = True
        buttonBack.Visible = False

        Dim oCredit As New Parameter.Credit
        If e.CommandName = "Edit" Then
          
                pnlAddEdit.Visible = True
                lblRequiredScoreComponentID.Visible = False
 
                pnlPersonalCustomer.Visible = False 
                buttonCancel.Visible = True
                buttonSave.Visible = True

                lblTitleAddEdit.Text = "EDIT"
                oCredit.Id = dtgRejectPolicy.DataKeys.Item(e.Item.ItemIndex).ToString
                oCredit.strConnection = GetConnectionString()
            oCredit.SPName = "spspScoreRejectPolicyView"
                oCredit = m_controller.CreditEdit(oCredit)

            'lblScoreComponentID.Visible = True
            lblScoreComponentID.Visible = False
                lblDescription.Visible = False

            'lblScoringType.Visible = False
                ' lblGroupComponent.Visible = False
                lblCalculationType.Visible = False
                lblSqlCommand.Visible = False
            'lblDataSource.Visible = False

            'txtScoreComponentID.Visible = False
            txtScoreComponentID.Visible = True
                txtDescription.Visible = True

                txtSqlCommand.Visible = True
            'txtDataSource.Visible = True
                cboCalculationType.Visible = True 
            'cboScoringType.Visible = True


            'lblScoreComponentID.Text = dtgRejectPolicy.DataKeys.Item(e.Item.ItemIndex).ToString
            txtScoreComponentID.Text = dtgRejectPolicy.DataKeys.Item(e.Item.ItemIndex).ToString
                txtDescription.Text = oCredit.Description

                txtSqlCommand.Text = oCredit.SQLCmd
            'txtDataSource.Text = oCredit.DataSource

            'If oCredit.ScoringType = "P" Then
            '    cboScoringType.SelectedIndex = cboScoringType.Items.IndexOf(cboScoringType.Items.FindByValue("P"))
            'ElseIf oCredit.ScoringType = "C" Then
            '    cboScoringType.SelectedIndex = cboScoringType.Items.IndexOf(cboScoringType.Items.FindByValue("C"))
            'End If

                'If oCredit.GroupComponent = "C" Then
                '    cboGroupComponent.SelectedIndex = cboGroupComponent.Items.IndexOf(cboGroupComponent.Items.FindByValue("C"))
                'ElseIf oCredit.GroupComponent = "F" Then
                '    cboGroupComponent.SelectedIndex = cboGroupComponent.Items.IndexOf(cboGroupComponent.Items.FindByValue("F"))
                'ElseIf oCredit.GroupComponent = "A" Then
                '    cboGroupComponent.SelectedIndex = cboGroupComponent.Items.IndexOf(cboGroupComponent.Items.FindByValue("A"))
                'End If

                If oCredit.CalculationType = "R" Then
                    cboCalculationType.SelectedIndex = cboCalculationType.Items.IndexOf(cboCalculationType.Items.FindByValue("R"))
                'txtDataSource.Enabled = False
                ElseIf oCredit.CalculationType = "T" Then
                    cboCalculationType.SelectedIndex = cboCalculationType.Items.IndexOf(cboCalculationType.Items.FindByValue("T"))
                'txtDataSource.Enabled = True
                End If

        ElseIf e.CommandName = "View" Then

            buttonSave.Visible = False
            buttonCancel.Visible = False
            buttonBack.Visible = True 
            BindDetail(dtgRejectPolicy.DataKeys.Item(e.Item.ItemIndex).ToString)

        ElseIf e.CommandName = "Delete" Then

            Dim customClass As New Parameter.Credit
            With customClass
                .Id = dtgRejectPolicy.DataKeys.Item(e.Item.ItemIndex).ToString
                .strConnection = GetConnectionString()
                .SPName = "spScoreRejectPolicyDelete"
            End With

            Dim ResultOutput As String
            ResultOutput = m_controller.CreditDelete(customClass)
            If ResultOutput = "OK" Then
                ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, False)
            Else
                ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_FAILED, True)
            End If

            txtSearch.Text = ""
            BindGridEntity()

        End If
    End Sub
    Sub BindDetail(ByVal ID As String)
        Dim oCredit As New Parameter.Credit

        pnlAddEdit.Visible = True
        lblTitleAddEdit.Text = "VIEW"

        oCredit.Id = ID
        oCredit.strConnection = GetConnectionString
        oCredit.SPName = "spspScoreRejectPolicyView"
        oCredit = m_controller.CreditEdit(oCredit)
         
        pnlPersonalCustomer.Visible = False 
        lblScoreComponentID.Visible = True
        lblDescription.Visible = True
        ' lblSequenceComponent.Visible = True
        'lblScoringType.Visible = True

        lblCalculationType.Visible = True
        lblSqlCommand.Visible = True
        'lblDataSource.Visible = True

        txtScoreComponentID.Visible = False
        '  txtDescription.Visible = False
        ' txtSequenceComponent.Visible = False
        txtSqlCommand.Visible = False
        'txtDataSource.Visible = False
        cboCalculationType.Visible = False

        'cboScoringType.Visible = False

        lblScoreComponentID.Text = ID
        lblDescription.Text = oCredit.Description
        ' lblSequenceComponent.Text = CStr(oCredit.SeqComponent)

        'If oCredit.ScoringType = "P" Then
        '    lblScoringType.Text = "Personal"
        'ElseIf oCredit.ScoringType = "C" Then
        '    lblScoringType.Text = "Company"
        'End If
         

        If oCredit.CalculationType = "R" Then
            lblCalculationType.Text = "Range"
        ElseIf oCredit.CalculationType = "T" Then
            lblCalculationType.Text = "Table"
        End If

        lblSqlCommand.Text = oCredit.SQLCmd
        'lblDataSource.Text = oCredit.DataSource
    End Sub



    Private Sub ButtonSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonSave.Click
        If Page.IsValid Then
            

            Dim customClass As New Parameter.Credit
            Dim ErrMessage As String = ""

            If lblTitleAddEdit.Text = "ADD" Then
                With customClass
                    .Id = txtScoreComponentID.Text.Trim
                    .Description = txtDescription.Text
                    .ScoringType = ""
                    .GroupComponent = ""
                    .SeqComponent = 0
                    .CalculationType = cboCalculationType.SelectedItem.Value
                    .SQLCmd = txtSqlCommand.Text
                    .DataSource = ""
                    .strConnection = GetConnectionString()
                    .SPName = "spCreditSaveAdd"
                End With

                ErrMessage = m_controller.CreditSaveAdd(customClass)
                If ErrMessage <> "" Then
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_FAILED, True)
                    pnlAddEdit.Visible = True 
                    Exit Sub
                Else
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_SUCCESS, True)
                    ButtonBack_Click(Nothing, Nothing)
                End If
            ElseIf lblTitleAddEdit.Text = "EDIT" Then
                With customClass
                    '.Id = lblScoreComponentID.Text
                    .Id = txtScoreComponentID.Text.Trim
                    .Description = txtDescription.Text
                    .ScoringType = ""
                    .GroupComponent = ""
                    .SeqComponent = 0
                    .CalculationType = cboCalculationType.SelectedItem.Value
                    .SQLCmd = txtSqlCommand.Text
                    .DataSource = ""
                    .strConnection = GetConnectionString()
                    .SPName = "spScoreRejectPolicySaveEdit"
                End With

                m_controller.CreditSaveEdit(customClass)
                ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, True)
                ButtonBack_Click(Nothing, Nothing)
            End If
            txtSearch.Text = ""
        End If
    End Sub
     

    Private Sub ButtonCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonCancel.Click
        ButtonBack_Click(Nothing, Nothing)
    End Sub
    Private Sub ButtonBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonBack.Click
        Response.Redirect("RejectPolicyCredit.aspx")
    End Sub
End Class