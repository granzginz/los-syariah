﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CrRejectPolicyCredit.aspx.vb" Inherits="Maxiloan.Webform.SalesMarketing.CrRejectPolicyCredit" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Credit Reject Policy</title>
    <script language="JavaScript" type="text/javascript">
        function DataSource() {
            if (document.forms[0].cboCalculationType.value == 'T') {
                document.forms[0].txtDataSource.disabled = false;
            }
            else {
                document.forms[0].txtDataSource.disabled = true;
                document.forms[0].txtDataSource.value = '';
            }
        }

        function fBack() {
            history.back(-1);
            return false;
        }
        function fClose() {
            window.close();
            return false;
        }
        function DeleteConfirm() {
            if (confirm("Apakah Yakin mau hapus data ini ?")) {
                return true;
            }
            else {
                return false;
            }
        }	
    </script>
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../../Include/Buttons.css" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" />
    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3> PEMBIAYAAN SCORE POLICY </h3>
                </div>
            </div>
    

      <asp:Panel ID="pnlPersonalCustomer" runat="server">
                <div class="form_box_header">
                    <div class="form_single">
                        <h4> DAFTAR </h4>
                    </div>
                </div>

                  <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtgRejectPolicy" runat="server" Width="100%" DataKeyField="CreditScoreComponentID" CssClass="grid_general" BorderStyle="None" BorderWidth="0" AutoGenerateColumns="False" AllowSorting="True" Visible="True">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="Edit">
                                        <ItemStyle CssClass="command_col"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" ImageUrl="../../../Images/iconedit.gif" CommandName="Edit" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Delete">
                                        <ItemStyle CssClass="command_col"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbDelete_dtgPersonalCustomer" runat="server" CausesValidation="False" ImageUrl="../../../Images/iconDelete.gif" CommandName="Delete" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="ID Komponen Scoring">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkScor_PersonalCustomer" runat="server" CommandName="View" Text='<%# container.dataitem("CreditScoreComponentID")%>' CommandArgument='<%# container.dataitem("CreditScoreComponentID")%>' CausesValidation="false" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="Description" HeaderText="Keterangan" />
                                </Columns>
                            </asp:DataGrid>
                        </div>
                    </div>
                </div>



                 <div class="form_button">
                    <asp:Button ID="buttonAdd" runat="server" Text="Add" CssClass="small button blue" CausesValidation="False" />
                    <asp:Button ID="buttonPrint" runat="server" Text="Print" CssClass="small button blue"   CausesValidation="False" />
                </div>

                <div class="form_box_title">
                    <div class="form_single">
                        <h4>CARI KOMPONEN PEMBIAYAAN SCORING CARD</h4>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <h5><label>Cari Berdasarkan</label></h5>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <asp:DropDownList ID="cboSearch" runat="server">
                            <asp:ListItem Value="CreditScoreComponentID">ID Komponen Scoring</asp:ListItem>
                            <asp:ListItem Value="Description">Keterangan</asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtSearch" runat="server" Width="248px"></asp:TextBox>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="buttonSearch" runat="server" Text="Search" CssClass="small button blue" CausesValidation="False" />
                    <asp:Button ID="buttonReset" runat="server" Text="Reset" CssClass="small button gray" CausesValidation="False" />
                </div>
            </asp:Panel>



            
            <asp:Panel ID="pnlAddEdit" runat="server">
                <div class="form_box_header">
                    <div class="form_single">
                        <h4>KOMPONEN PEMBIAYAAN SCORING CARD - <asp:Label ID="lblTitleAddEdit" runat="server" /> </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req"> ID Komponen Scoring <font color="red"></font>
                        </label>
                        <asp:TextBox ID="txtDescription" runat="server" MaxLength="100" Columns="103" />
                        <asp:Label ID="Label2" runat="server" />
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" Display="Dynamic" ErrorMessage="Harap isi Keterangan" CssClass="validator_general" ControlToValidate="txtDescription" />
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req"> ID<font color="red"></font></label>
                        <asp:Label ID="lblScoreComponentID" runat="server" />
                        <asp:TextBox ID="txtScoreComponentID" runat="server" MaxLength="10" Columns="13" />
                        <asp:Label ID="lblRequiredScoreComponentID" runat="server" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic" ErrorMessage="Harap isi ID Komponen Score" CssClass="validator_general" ControlToValidate="txtScoreComponentID" />
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>Keterangan<font color="red"></font></label>
                        <asp:Label ID="lblDescription" runat="server" />
                    </div>
                </div>
             
                <div class="form_box">
                    <div class="form_single">
                        <label>Jenis Calculation</label>
                        <asp:Label ID="lblCalculationType" runat="server"></asp:Label>
                        <asp:DropDownList ID="cboCalculationType" runat="server" onchange="DataSource()">
                            <asp:ListItem Value="R">Range</asp:ListItem>
                            <asp:ListItem Value="T">Table</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">SQL Command</Label><font color="red"  >
                        <asp:Label ID="lblSqlCommand" runat="server"></asp:Label></font>
                        <asp:TextBox ID="txtSqlCommand" runat="server" Width="552px" Columns="28" TextMode="MultiLine" />
                        <asp:Label ID="Label4" runat="server" />
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator7" runat="server" Display="Dynamic" ErrorMessage="Harap isi SQL Command" CssClass="validator_general" ControlToValidate="txtSqlCommand" />
                    </div>
                </div>
               
                <div class="form_button">
                    <asp:Button ID="buttonSave" runat="server" Text="Save" CssClass="small button blue" CausesValidation="true" />
                    <asp:Button ID="buttonCancel" runat="server" Text="Cancel" CssClass="small button gray" CausesValidation="false" />
                    <asp:Button ID="buttonBack" runat="server" Text="Back" CssClass="small button gray" CausesValidation="false" />
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
