﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="MarketingCredit.aspx.vb"
    Inherits="Maxiloan.Webform.SalesMarketing.MarketingCredit" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>MarketingCredit</title>
    <script language="JavaScript" type="text/javascript">
        function DataSource() {
            if (document.forms[0].cboCalculationType.value == 'T') {
                document.forms[0].txtDataSource.disabled = false;
            }
            else {
                document.forms[0].txtDataSource.disabled = true;
                document.forms[0].txtDataSource.value = '';
            }
        }

        function fBack() {
            history.back(-1);
            return false;
        }
        function fClose() {
            window.close();
            return false;
        }
        function DeleteConfirm() {
            if (confirm("Apakah Yakin mau hapus data ini?")) {
                return true;
            }
            else {
                return false;
            }
        }	
    </script>
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../../Include/Buttons.css" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible ="false" ></asp:Label>         
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">               
            <h3>                
                MARKETING PEMBIAYAAN
            </h3>
        </div>
    </div>    
    <asp:Panel ID="pnlPersonalCustomer" runat="server" >
    <div class="form_box_header">
        <div class="form_single">              
            <h4>
                PERSONAL CUSTOMER
            </h4>
        </div>
    </div> 
     </asp:Panel>
    <asp:Panel ID="PnlCustomer" runat="server" >
    <div class="form_box_header">
        <div class="form_single">              
            <h5>
                CUSTOMER
            </h5>
        </div>
    </div>        
    <div class="form_box_header">
    <div class="form_single">
        <div class="grid_wrapper_ns">
            <asp:DataGrid ID="dtgPersonalCustomer" runat="server" Width="100%" DataKeyField="CreditScoreComponentID"
                        CssClass="grid_general" BorderStyle="None" BorderWidth="0" AutoGenerateColumns="False"
                        AllowSorting="True" Visible="True">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="Edit">
                                <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" ImageUrl="../../../Images/iconedit.gif"
                                        CommandName="Edit"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Delete">
                                <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbDelete_dtgPersonalCustomer" runat="server" CausesValidation="False"
                                        ImageUrl="../../../Images/iconDelete.gif" CommandName="Delete"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="ID Komponen Scoring">                                
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkScor_PersonalCustomer" runat="server" CommandName="View" Text='<%# container.dataitem("CreditScoreComponentID")%>'
                                        CommandArgument='<%# container.dataitem("CreditScoreComponentID")%>' CausesValidation="false">
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="Description" HeaderText="Keterangan">                                
                            </asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
	    </div>
    </div> 
    </div> 
    </asp:Panel>           
    <asp:Panel ID="pnlPersonalFinancial" runat="server">
    <div class="form_box_header">
        <div class="form_single">              
            <h5>
                FINANCIAL
            </h5>
        </div>
    </div> 
    <div class="form_box_header">
    <div class="form_single">
        <div class="grid_wrapper_ns">
            <asp:DataGrid ID="dtgPersonalFinancial" runat="server" Width="100%" DataKeyField="CreditScoreComponentID"
                            CssClass="grid_general" BorderStyle="None" BorderWidth="0" AutoGenerateColumns="False" AllowSorting="True" Visible="True" >
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <Columns>
                                <asp:TemplateColumn HeaderText="Edit">
                                    <ItemStyle CssClass="command_col"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imageEdit" runat="server" CausesValidation="False" ImageUrl="../../../Images/iconedit.gif"
                                            CommandName="Edit"></asp:ImageButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Delete">
                                    <ItemStyle CssClass="command_col"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imbDelete_dtgPersonalFinancial" runat="server" CausesValidation="False"
                                            ImageUrl="../../../Images/iconDelete.gif" CommandName="Delete"></asp:ImageButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="ID Komponen Scoring">                                    
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkScor_PersonalFinancial" runat="server" CommandName="View"
                                            Text='<%# container.dataitem("CreditScoreComponentID")%>' CommandArgument='<%# container.dataitem("CreditScoreComponentID")%>'
                                            CausesValidation="false">
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn DataField="Description" HeaderText="Keterangan">                                    
                                </asp:BoundColumn>
                            </Columns>
            </asp:DataGrid>
	    </div>
    </div>    
    </div>  
    </asp:Panel>       
    <asp:Panel ID="pnlPersonalAsset" runat="server">
    <div class="form_box_header">
        <div class="form_single">              
            <h5>
                ASSET
            </h5>
        </div>
    </div> 
    <div class="form_box_header">
    <div class="form_single">
        <div class="grid_wrapper_ns">
            <asp:DataGrid ID="dtgPersonalAsset" runat="server" Width="100%" DataKeyField="CreditScoreComponentID"
                                CssClass="grid_general" BorderStyle="None" BorderWidth="0" AutoGenerateColumns="False"
                                AllowSorting="True" Visible="True">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="Edit">
                                        <ItemStyle CssClass="command_col"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="Imagebutton1" runat="server" CausesValidation="False" ImageUrl="../../../Images/iconedit.gif"
                                                CommandName="Edit"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Delete">
                                        <ItemStyle CssClass="command_col"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbDelete_dtgPersonalAsset" runat="server" CausesValidation="False"
                                                ImageUrl="../../../Images/iconDelete.gif" CommandName="Delete"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="ID Komponen Scoring">                                        
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkScor_PersonalAsset" runat="server" CommandName="View" Text='<%# container.dataitem("CreditScoreComponentID")%>'
                                                CommandArgument='<%# container.dataitem("CreditScoreComponentID")%>' CausesValidation="false">
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="Description" HeaderText="Keterangan">                                        
                                    </asp:BoundColumn>
                                </Columns>
                            </asp:DataGrid>
	    </div>
    </div>  
    </div>                                  
    </asp:Panel>
   
    <asp:Panel ID="pnlCompanyCustomer" runat="server" Visible="false">
    <div class="form_box_header">
        <div class="form_single">              
            <h4>
                CORPORATE CUSTOMER
            </h4>
        </div>
    </div> 
   
    <asp:Panel ID="PnlCompany" runat="server">
    <div class="form_box_header">
        <div class="form_single">              
            <h5>
                CORPORATE DATA
            </h5>
        </div>
    </div>
    <div class="form_box_header">
    <div class="form_single">
        <div class="grid_wrapper_ns">
            <asp:DataGrid ID="dtgCompanyCustomer" runat="server" Width="100%" DataKeyField="CreditScoreComponentID"
                            CssClass="grid_general" BorderStyle="None" BorderWidth="0" AutoGenerateColumns="False"
                            AllowSorting="True" Visible="True" >
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <Columns>
                                <asp:TemplateColumn HeaderText="Edit">
                                    <ItemStyle CssClass="command_col"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="Imagebutton7" runat="server" CausesValidation="False" ImageUrl="../../../Images/iconedit.gif"
                                            CommandName="Edit"></asp:ImageButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Delete">
                                    <ItemStyle CssClass="command_col"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imbDelete_dtgCompanyCustomer" runat="server" CausesValidation="False"
                                            ImageUrl="../../../Images/iconDelete.gif" CommandName="Delete"></asp:ImageButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="ID Komponen Scoring">                                   
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkScor_CompanyCustomer" runat="server" CommandName="View" Text='<%# container.dataitem("CreditScoreComponentID")%>'
                                            CommandArgument='<%# container.dataitem("CreditScoreComponentID")%>' CausesValidation="false">
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn DataField="Description" HeaderText="Keterangan">                                    
                                </asp:BoundColumn>
                            </Columns>
                        </asp:DataGrid>
	    </div>
    </div> 
    </div>  
    </asp:Panel>                                  
    <asp:Panel ID="pnlCompanyFinancial" runat="server">
    <div class="form_box_header">
        <div class="form_single">              
            <h5>
                FINANCIAL
            </h5>
        </div>
    </div> 
    <div class="form_box_header">
    <div class="form_single">
        <div class="grid_wrapper_ns">
            <asp:DataGrid ID="dtgCompanyFinancial" runat="server" Width="100%" DataKeyField="CreditScoreComponentID"
                                CssClass="grid_general" BorderStyle="None" BorderWidth="0" AutoGenerateColumns="False"
                                AllowSorting="True" Visible="True">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="Edit">
                                        <ItemStyle CssClass="command_col"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="Imagebutton9" runat="server" CausesValidation="False" ImageUrl="../../../Images/iconedit.gif"
                                                CommandName="Edit"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Delete">
                                        <ItemStyle CssClass="command_col"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbDelete_dtgCompanyFinancial" runat="server" CausesValidation="False"
                                                ImageUrl="../../../Images/iconDelete.gif" CommandName="Delete"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="ID Komponen Scoring">                                        
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkScor_CompanyFinancial" runat="server" CommandName="View" Text='<%# container.dataitem("CreditScoreComponentID")%>'
                                                CommandArgument='<%# container.dataitem("CreditScoreComponentID")%>' CausesValidation="false">
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="Description" HeaderText="Keterangan">                                        
                                    </asp:BoundColumn>
                                </Columns>
                            </asp:DataGrid>
	    </div>
    </div>                 
    </div>    
     </asp:Panel>             
    <asp:Panel ID="pnlCompanyAsset" runat="server">
    <div class="form_box_header">
        <div class="form_single">              
            <h5>
                ASSET
            </h5>
        </div>
    </div> 
    <div class="form_box_header">
    <div class="form_single">
        <div class="grid_wrapper_ns">
            <asp:DataGrid ID="dtgCompanyAsset" runat="server" Width="100%" DataKeyField="CreditScoreComponentID"
                                    CssClass="grid_general" BorderStyle="None" BorderWidth="0" AutoGenerateColumns="False"
                                    AllowSorting="True" Visible="True" >
                                    <HeaderStyle CssClass="th" />
                                    <ItemStyle CssClass="item_grid" />
                                    <Columns>
                                        <asp:TemplateColumn HeaderText="Edit">
                                            <ItemStyle CssClass="command_col"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:ImageButton ID="Imagebutton5" runat="server" CausesValidation="False" ImageUrl="../../../Images/iconedit.gif"
                                                    CommandName="Edit"></asp:ImageButton>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Delete">
                                            <ItemStyle CssClass="command_col"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imbDelete_dtgCompanyAsset" runat="server" CausesValidation="False"
                                                    ImageUrl="../../../Images/iconDelete.gif" CommandName="Delete"></asp:ImageButton>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="ID Komponen Scoring">                                            
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkScor_CompanyAsset" runat="server" CommandName="View" Text='<%# container.dataitem("CreditScoreComponentID")%>'
                                                    CommandArgument='<%# container.dataitem("CreditScoreComponentID")%>' CausesValidation="false">
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:BoundColumn DataField="Description" HeaderText="Keterangan">                                            
                                        </asp:BoundColumn>
                                    </Columns>
                                </asp:DataGrid>
	    </div>
    </div>                  
    </div>                       
    </asp:Panel>
</asp:Panel>  
    <div class="form_button">
        <asp:Button ID="ButtonAdd" runat="server" CausesValidation="False"  Text="Add" CssClass ="small button blue">
        </asp:Button>&nbsp;
        <asp:Button ID="ButtonPrint" runat="server" CausesValidation="False"  Text="Print" Visible="false"  CssClass ="small button blue">
        </asp:Button>
    </div>   
    <div class="form_box_title">
        <div class="form_single">              
            <h4>
                CARI KOMPONEN MARKETING SCORING CARD
            </h4>
        </div>
    </div>          
    <div class="form_box">
	    <div class="form_single">
            <label>Cari Berdasarkan</label>
            <asp:DropDownList ID="cboSearch" runat="server">
                <asp:ListItem Value="ScoreComponentID">ID Komponen Score</asp:ListItem>
                <asp:ListItem Value="Description">Keterangan</asp:ListItem>
            </asp:DropDownList>
            &nbsp;
            <asp:TextBox ID="txtSearch" runat="server"  Width="248px"></asp:TextBox>
	    </div>
    </div> 
    <div class="form_button">
        <asp:Button ID="ButtonSearch" runat="server"  Text="Search" CssClass ="small button blue"
            CausesValidation="False"></asp:Button>&nbsp;
        <asp:Button ID="ButtonReset" runat="server"  Text="Reset" CssClass ="small button gray"
            CausesValidation="False"></asp:Button>
    </div>   
           
    
    <asp:Panel ID="pnlAddEdit" runat="server">     
    <div class="form_box_header">
        <div class="form_single">              
            <h4>
                KOMPONEN MARKETING SCORING CARD&nbsp;&nbsp;-
                    <asp:Label ID="lblTitleAddEdit" runat="server"></asp:Label>
            </h4>
        </div>
    </div> 
    <div class="form_box">
	    <div class="form_single">
             <label class ="label_req">
			 ID Komponen Score<font color="red"></font></label>
            <asp:Label ID="lblScoreComponentID" runat="server"></asp:Label>
            <asp:TextBox ID="txtScoreComponentID" runat="server"  MaxLength="10"
                Columns="13"></asp:TextBox>
            <asp:Label ID="lblRequiredScoreComponentID" runat="server"></asp:Label>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic" CssClass="validator_general"
                ErrorMessage="Harap isi ID Komponen Score" ControlToValidate="txtScoreComponentID"></asp:RequiredFieldValidator>
	    </div>
    </div>  
    <div class="form_box">
	    <div class="form_single">
             <label class ="label_req">
			 Keterangan<font color="red"></font></label>
            <asp:Label ID="lblDescription" runat="server"></asp:Label>
            <asp:TextBox ID="txtDescription" runat="server" Width="560px" 
                MaxLength="100" Columns="103"></asp:TextBox>
            <asp:Label ID="Label2" runat="server"></asp:Label>
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" Display="Dynamic" CssClass="validator_general"
                ErrorMessage="Harap isi Keterangan" ControlToValidate="txtDescription"></asp:RequiredFieldValidator>
	    </div>
    </div> 
    <div class="form_box">
	    <div class="form_single">
            <label>Jenis Scoring</label>
            <asp:Label ID="lblScoringType" runat="server"></asp:Label>
            <asp:DropDownList ID="cboScoringType" runat="server">
                <asp:ListItem Value="P">Personal</asp:ListItem>
                <%--<asp:ListItem Value="C">Corporate</asp:ListItem>--%>
            </asp:DropDownList>
	    </div>
    </div> 
    <div class="form_box">
	    <div class="form_single">
            <label>Group Komponen</label>
            <asp:Label ID="lblGroupComponent" runat="server"></asp:Label>
            <asp:DropDownList ID="cboGroupComponent" runat="server">
                <asp:ListItem Value="C">Customer</asp:ListItem>
                <asp:ListItem Value="F">Financial</asp:ListItem>
                <asp:ListItem Value="A">Asset</asp:ListItem>
            </asp:DropDownList>
	    </div>
    </div> 
    <div class="form_box">
	    <div class="form_single">
           <label class ="label_req">
		   Urutan Komponen<font color="red"></font></label>
            <asp:Label ID="lblSequenceComponent" runat="server"></asp:Label>
            <asp:TextBox ID="txtSequenceComponent" runat="server"  MaxLength="3"
                Columns="13"></asp:TextBox>
            <asp:Label ID="Label3" runat="server"></asp:Label>
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator3" runat="server" Display="Dynamic" CssClass="validator_general"
                ErrorMessage="Harap isi Urutan Komponen" ControlToValidate="txtSequenceComponent"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="rgvSequenceComponent" runat="server" Display="Dynamic" ErrorMessage="Urutan Komponen Salah" CssClass="validator_general"
                ControlToValidate="txtSequenceComponent" Type="Integer" MaximumValue="999" MinimumValue="1"></asp:RangeValidator>
	    </div>
    </div> 
    <div class="form_box">
	    <div class="form_single">
            <label>Jenis Calculation</label>
            <asp:Label ID="lblCalculationType" runat="server"></asp:Label>
            <asp:DropDownList ID="cboCalculationType" runat="server" onchange="DataSource()">
                <asp:ListItem Value="R">Range</asp:ListItem>
                <asp:ListItem Value="T">Table</asp:ListItem>
            </asp:DropDownList>
	    </div>
    </div> 
    <div class="form_box">
	    <div class="form_single">
            <label class ="label_req">
			SQL Command<font color="red"></font></label>
            <asp:Label ID="lblSqlCommand" runat="server"></asp:Label>
            <asp:TextBox ID="txtSqlCommand" runat="server"  Width="552px" MaxLength="255"
                Columns="28" TextMode="MultiLine"></asp:TextBox>
            <asp:Label ID="Label4" runat="server"></asp:Label>
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator7" runat="server" Display="Dynamic" CssClass="validator_general"
                ErrorMessage="Harap isi SQL Command" ControlToValidate="txtSqlCommand"></asp:RequiredFieldValidator>
	    </div>
    </div> 
    <div class="form_box">
	    <div class="form_single">
            <label>Sumber Data</label>
            <asp:Label ID="lblDataSource" runat="server"></asp:Label>
            <asp:TextBox ID="txtDataSource" runat="server"  Width="552px" MaxLength="255"
                Columns="28" TextMode="MultiLine"></asp:TextBox>            
	    </div>
    </div> 
    <div class="form_button">
        <asp:Button ID="ButtonSave" runat="server" CausesValidation="true" Text="Save" CssClass ="small button blue">
        </asp:Button>&nbsp;
        <asp:Button ID="ButtonCancel" runat="server" CausesValidation="false" Text="Cancel" CssClass ="small button gray">
        </asp:Button>&nbsp;
        <asp:Button ID="ButtonBack" runat="server" CausesValidation="false"  Text="Back" CssClass ="small button gray">
        </asp:Button>
    </div>               
    </asp:Panel>
    </form>
</body>
</html>
