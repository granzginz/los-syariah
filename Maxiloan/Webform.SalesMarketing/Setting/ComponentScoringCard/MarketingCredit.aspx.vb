﻿#Region "Imports"
Imports System.DBNull
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
#End Region

Public Class MarketingCredit
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private m_controller As New CreditController
    Private currentPage As Integer = 1
    Private pageSize As Int16
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region

#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(viewstate("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property AddEdit() As String
        Get
            Return CType(viewstate("vwsAddEdt"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("vwsAddEdt") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(viewstate("Sort"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Sort") = Value
        End Set
    End Property

#End Region

#Region "Page_Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        If sessioninvalid() Then
            Exit Sub
        End If
        InitialDefaultPanel()
        Me.FormID = "MarCreditScore"

        If Not Me.IsPostBack Then
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If sessioninvalid() Then
                    Exit Sub
                End If
                Me.SearchBy = ""
                Me.SortBy = ""
            End If

            txtSearch.Text = ""
            Me.Sort = "SeqComponent ASC"
            Me.CmdWhere = "ALL"

            pnlCompanyAsset.Visible = True
            pnlCompanyCustomer.Visible = False
            pnlCompanyFinancial.Visible = True
            pnlPersonalAsset.Visible = True
            pnlPersonalCustomer.Visible = True
            pnlPersonalFinancial.Visible = True
            PnlCustomer.Visible = True
            PnlCompany.Visible = False


            BindGridEntity_CompanyCustomer(Me.CmdWhere)
            BindGridEntity_CompanyAsset(Me.CmdWhere)
            BindGridEntity_CompanyFinancial(Me.CmdWhere)
            BindGridEntity_PersonalCustomer(Me.CmdWhere)
            BindGridEntity_PersonalAsset(Me.CmdWhere)
            BindGridEntity_PersonalFinancial(Me.CmdWhere)
        End If
        'check_totalrecord_print()
    End Sub
#End Region

#Region "BindGridEntity"
    Sub BindGridEntity_PersonalCustomer(ByVal cmdWhere As String)
        Dim dtEntity As DataTable
        Dim oCredit As New Parameter.Credit

        With oCredit
            If cmdWhere <> "ALL" Then
                .WhereCond = "ScoringType='P' and GroupComponent='C' and " & cmdWhere
            Else
                .WhereCond = "ScoringType='P' and GroupComponent='C'"
            End If
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = Me.Sort
            .strConnection = GetConnectionString
            .SPName = "spMarketingCreditPaging"
        End With
        oCredit = m_controller.GetCredit(oCredit)

        If Not oCredit Is Nothing Then
            dtEntity = oCredit.ListData
            recordCount = oCredit.TotalRecords
        Else
            recordCount = 0
        End If

        dtgPersonalCustomer.DataSource = dtEntity.DefaultView
        dtgPersonalCustomer.CurrentPageIndex = 0
        dtgPersonalCustomer.DataBind()

    End Sub

    Sub BindGridEntity_PersonalFinancial(ByVal cmdWhere As String)
        Dim dtEntity As DataTable
        Dim oCredit As New Parameter.Credit

        With oCredit
            If cmdWhere <> "ALL" Then
                .WhereCond = "ScoringType='P' and GroupComponent='F' and " & cmdWhere
            Else
                .WhereCond = "ScoringType='P' and GroupComponent='F'"
            End If
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = Me.Sort
            .strConnection = GetConnectionString
            .SPName = "spMarketingCreditPaging"
        End With
        oCredit = m_controller.GetCredit(oCredit)

        If Not oCredit Is Nothing Then
            dtEntity = oCredit.ListData
            recordCount = oCredit.TotalRecords
        Else
            recordCount = 0
        End If

        dtgPersonalFinancial.DataSource = dtEntity.DefaultView
        dtgPersonalFinancial.CurrentPageIndex = 0
        dtgPersonalFinancial.DataBind()

    End Sub

    Sub BindGridEntity_PersonalAsset(ByVal cmdWhere As String)
        Dim dtEntity As DataTable
        Dim oCredit As New Parameter.Credit

        With oCredit
            If cmdWhere <> "ALL" Then
                .WhereCond = "ScoringType='P' and GroupComponent='A' and " & cmdWhere
            Else
                .WhereCond = "ScoringType='P' and GroupComponent='A'"
            End If
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = Me.Sort
            .strConnection = GetConnectionString
            .SPName = "spMarketingCreditPaging"
        End With
        oCredit = m_controller.GetCredit(oCredit)

        If Not oCredit Is Nothing Then
            dtEntity = oCredit.ListData
            recordCount = oCredit.TotalRecords
        Else
            recordCount = 0
        End If

        dtgPersonalAsset.DataSource = dtEntity.DefaultView
        dtgPersonalAsset.CurrentPageIndex = 0
        dtgPersonalAsset.DataBind()

    End Sub

    Sub BindGridEntity_CompanyCustomer(ByVal cmdWhere As String)
        Dim dtEntity As DataTable
        Dim oCredit As New Parameter.Credit

        With oCredit
            If cmdWhere <> "ALL" Then
                .WhereCond = "ScoringType='C' and GroupComponent='C' and " & cmdWhere
            Else
                .WhereCond = "ScoringType='C' and GroupComponent='C'"
            End If
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = Me.Sort
            .strConnection = GetConnectionString
            .SPName = "spMarketingCreditPaging"
        End With
        oCredit = m_controller.GetCredit(oCredit)

        If Not oCredit Is Nothing Then
            dtEntity = oCredit.ListData
            recordCount = oCredit.TotalRecords
        Else
            recordCount = 0
        End If


        dtgCompanyCustomer.DataSource = dtEntity.DefaultView
        dtgCompanyCustomer.CurrentPageIndex = 0
        dtgCompanyCustomer.DataBind()

    End Sub

    Sub BindGridEntity_CompanyFinancial(ByVal cmdWhere As String)
        Dim dtEntity As DataTable
        Dim oCredit As New Parameter.Credit

        With oCredit
            If cmdWhere <> "ALL" Then
                .WhereCond = "ScoringType='C' and GroupComponent='F' and " & cmdWhere
            Else
                .WhereCond = "ScoringType='C' and GroupComponent='F'"
            End If
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = Me.Sort
            .strConnection = GetConnectionString
            .SPName = "spMarketingCreditPaging"
        End With
        oCredit = m_controller.GetCredit(oCredit)

        If Not oCredit Is Nothing Then
            dtEntity = oCredit.ListData
            recordCount = oCredit.TotalRecords
        Else
            recordCount = 0
        End If

        dtgCompanyFinancial.DataSource = dtEntity.DefaultView
        dtgCompanyFinancial.CurrentPageIndex = 0
        dtgCompanyFinancial.DataBind()

    End Sub

    Sub BindGridEntity_CompanyAsset(ByVal cmdWhere As String)
        Dim dtEntity As DataTable
        Dim oCredit As New Parameter.Credit

        With oCredit
            If cmdWhere <> "ALL" Then
                .WhereCond = "ScoringType='C' and GroupComponent='A' and " & cmdWhere
            Else
                .WhereCond = "ScoringType='C' and GroupComponent='A'"
            End If
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = Me.Sort
            .strConnection = GetConnectionString
            .SPName = "spMarketingCreditPaging"
        End With
        oCredit = m_controller.GetCredit(oCredit)

        If Not oCredit Is Nothing Then
            dtEntity = oCredit.ListData
            recordCount = oCredit.TotalRecords
        Else
            recordCount = 0
        End If

        dtgCompanyAsset.DataSource = dtEntity.DefaultView
        dtgCompanyAsset.CurrentPageIndex = 0
        dtgCompanyAsset.DataBind()
    End Sub
#End Region

#Region "BindGridEntitySearch"
    Sub BindGridEntity_PersonalCustomerSeacrh(ByVal cmdWhere As String)
        Dim dtEntity As DataTable
        Dim oCredit As New Parameter.Credit

        With oCredit
            If cmdWhere <> "ALL" Then
                .WhereCond = "ScoringType='P' and GroupComponent='C' and " & cmdWhere
            Else
                .WhereCond = "ScoringType='P' and GroupComponent='C'"
            End If
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = Me.Sort
            .strConnection = GetConnectionString()
            .SPName = "spMarketingCreditPaging"
        End With
        oCredit = m_controller.GetCredit(oCredit)

        If Not oCredit Is Nothing Then
            dtEntity = oCredit.ListData
            recordCount = oCredit.TotalRecords
        Else
            recordCount = 0
        End If

        dtgPersonalCustomer.DataSource = dtEntity.DefaultView
        dtgPersonalCustomer.CurrentPageIndex = 0
        dtgPersonalCustomer.DataBind()

        'PnlCustomer.Visible = True
        'pnlPersonalAsset.Visible = False
        'pnlPersonalFinancial.Visible = False
    End Sub

    Sub BindGridEntity_PersonalFinancialSearch(ByVal cmdWhere As String)
        Dim dtEntity As DataTable
        Dim oCredit As New Parameter.Credit

        With oCredit
            If cmdWhere <> "ALL" Then
                .WhereCond = "ScoringType='P' and GroupComponent='F' and " & cmdWhere
            Else
                .WhereCond = "ScoringType='P' and GroupComponent='F'"
            End If
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = Me.Sort
            .strConnection = GetConnectionString()
            .SPName = "spMarketingCreditPaging"
        End With
        oCredit = m_controller.GetCredit(oCredit)

        If Not oCredit Is Nothing Then
            dtEntity = oCredit.ListData
            recordCount = oCredit.TotalRecords
        Else
            recordCount = 0
        End If

        dtgPersonalFinancial.DataSource = dtEntity.DefaultView
        dtgPersonalFinancial.CurrentPageIndex = 0
        dtgPersonalFinancial.DataBind()
        'PnlCustomer.Visible = False
        'pnlPersonalAsset.Visible = False
        'pnlPersonalFinancial.Visible = True
    End Sub

    Sub BindGridEntity_PersonalAssetSearch(ByVal cmdWhere As String)
        Dim dtEntity As DataTable
        Dim oCredit As New Parameter.Credit

        With oCredit
            If cmdWhere <> "ALL" Then
                .WhereCond = "ScoringType='P' and GroupComponent='A' and " & cmdWhere
            Else
                .WhereCond = "ScoringType='P' and GroupComponent='A'"
            End If
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = Me.Sort
            .strConnection = GetConnectionString()
            .SPName = "spMarketingCreditPaging"
        End With
        oCredit = m_controller.GetCredit(oCredit)

        If Not oCredit Is Nothing Then
            dtEntity = oCredit.ListData
            recordCount = oCredit.TotalRecords
        Else
            recordCount = 0
        End If

        dtgPersonalAsset.DataSource = dtEntity.DefaultView
        dtgPersonalAsset.CurrentPageIndex = 0
        dtgPersonalAsset.DataBind()
        'PnlCustomer.Visible = False
        'pnlPersonalAsset.Visible = True
        'pnlPersonalFinancial.Visible = False
    End Sub
#End Region

#Region "dtgCompanyAsset_ItemCommand"
    Private Sub dtgCompanyAsset_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgCompanyAsset.ItemCommand
        ButtonSave.Visible = True
        ButtonCancel.Visible = True
        ButtonBack.Visible = False

        Dim oCredit As New Parameter.Credit
        If e.CommandName = "Edit" Then
            If CheckFeature(Me.Loginid, Me.FormID, "EDIT", Me.AppId) Then
                Me.AddEdit = "EDIT"
                pnlAddEdit.Visible = True
                ButtonSave.Visible = True
                ButtonCancel.Visible = True
                ButtonBack.Visible = False
                lblRequiredScoreComponentID.Visible = False

                'semua panel harus divisible=false
                pnlCompanyAsset.Visible = False
                pnlCompanyCustomer.Visible = False
                pnlCompanyFinancial.Visible = False
                pnlPersonalAsset.Visible = False
                pnlPersonalCustomer.Visible = False
                pnlPersonalFinancial.Visible = False
                ButtonCancel.Visible = True
                ButtonSave.Visible = True

                lblTitleAddEdit.Text = Me.AddEdit
                oCredit.Id = dtgCompanyAsset.DataKeys.Item(e.Item.ItemIndex).ToString
                oCredit.strConnection = GetConnectionString
                oCredit.SPName = "spMarketingCreditEdit"
                oCredit = m_controller.CreditEdit(oCredit)

                lblScoreComponentID.Visible = True
                lblDescription.Visible = False
                lblSequenceComponent.Visible = False
                lblScoringType.Visible = False
                lblGroupComponent.Visible = False
                lblCalculationType.Visible = False
                lblSqlCommand.Visible = False
                lblDataSource.Visible = False

                txtScoreComponentID.Visible = False
                txtDescription.Visible = True
                txtSequenceComponent.Visible = True
                txtSqlCommand.Visible = True
                txtDataSource.Visible = True
                cboCalculationType.Visible = True
                cboGroupComponent.Visible = True
                cboScoringType.Visible = True


                lblScoreComponentID.Text = dtgCompanyAsset.DataKeys.Item(e.Item.ItemIndex).ToString
                txtDescription.Text = oCredit.Description
                txtSequenceComponent.Text = CStr(oCredit.SeqComponent)
                txtSqlCommand.Text = oCredit.SQLCmd
                txtDataSource.Text = oCredit.DataSource

                If oCredit.ScoringType = "P" Then
                    cboScoringType.SelectedIndex = cboScoringType.Items.IndexOf(cboScoringType.Items.FindByValue("P"))
                ElseIf oCredit.ScoringType = "C" Then
                    cboScoringType.SelectedIndex = cboScoringType.Items.IndexOf(cboScoringType.Items.FindByValue("C"))
                End If

                If oCredit.GroupComponent = "C" Then
                    cboGroupComponent.SelectedIndex = cboGroupComponent.Items.IndexOf(cboGroupComponent.Items.FindByValue("C"))
                ElseIf oCredit.GroupComponent = "F" Then
                    cboGroupComponent.SelectedIndex = cboGroupComponent.Items.IndexOf(cboGroupComponent.Items.FindByValue("F"))
                ElseIf oCredit.GroupComponent = "A" Then
                    cboGroupComponent.SelectedIndex = cboGroupComponent.Items.IndexOf(cboGroupComponent.Items.FindByValue("A"))
                End If

                If oCredit.CalculationType = "R" Then
                    cboCalculationType.SelectedIndex = cboCalculationType.Items.IndexOf(cboCalculationType.Items.FindByValue("R"))
                    txtDataSource.Enabled = False
                ElseIf oCredit.CalculationType = "T" Then
                    cboCalculationType.SelectedIndex = cboCalculationType.Items.IndexOf(cboCalculationType.Items.FindByValue("T"))
                    txtDataSource.Enabled = True
                End If
            End If
        ElseIf e.CommandName = "View" Then
            If CheckFeature(Me.Loginid, Me.FormID, "VIEW", Me.AppId) Then
                ButtonSave.Visible = False
                ButtonCancel.Visible = False
                ButtonBack.Visible = True
                Dim lnkID As LinkButton = CType(e.Item.FindControl("lnkScor_CompanyAsset"), LinkButton)
                Dim strID As String = lnkID.Text.Trim
                BindDetail(strID)
            End If
        ElseIf e.CommandName = "Delete" Then
            If CheckFeature(Me.Loginid, Me.FormID, "DEL", Me.AppId) Then
                ButtonSave.Visible = True
                ButtonCancel.Visible = True
                ButtonBack.Visible = False
                Dim customClass As New Parameter.Credit
                With customClass
                    .Id = dtgCompanyAsset.DataKeys.Item(e.Item.ItemIndex).ToString
                    .strConnection = GetConnectionString
                    .SPName = "spMarketingCreditDelete"
                End With

                Dim ResultOutput As String
                ResultOutput = m_controller.CreditDelete(customClass)
                If ResultOutput = "OK" Then                    
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, False)
                Else                    
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_FAILED, True)
                End If
                Me.CmdWhere = "ALL"
                txtSearch.Text = ""
                BindGridEntity_CompanyAsset(Me.CmdWhere)
            End If
        End If
    End Sub
#End Region

#Region "dtgCompanyCustomer_ItemCommand"
    Private Sub dtgCompanyCustomer_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgCompanyCustomer.ItemCommand
        ButtonSave.Visible = True
        ButtonCancel.Visible = True
        ButtonBack.Visible = False

        Dim oCredit As New Parameter.Credit
        If e.CommandName = "Edit" Then
            If CheckFeature(Me.Loginid, Me.FormID, "EDIT", Me.AppId) Then
                Me.AddEdit = "EDIT"
                pnlAddEdit.Visible = True
                lblRequiredScoreComponentID.Visible = False

                'semua panel harus divisible=false
                pnlCompanyAsset.Visible = False
                pnlCompanyCustomer.Visible = False
                pnlCompanyFinancial.Visible = False
                pnlPersonalAsset.Visible = False
                pnlPersonalCustomer.Visible = False
                pnlPersonalFinancial.Visible = False
                ButtonCancel.Visible = True
                ButtonSave.Visible = True

                lblTitleAddEdit.Text = Me.AddEdit
                oCredit.Id = dtgCompanyCustomer.DataKeys.Item(e.Item.ItemIndex).ToString
                oCredit.strConnection = GetConnectionString
                oCredit.SPName = "spMarketingCreditEdit"
                oCredit = m_controller.CreditEdit(oCredit)

                lblScoreComponentID.Visible = True
                lblDescription.Visible = False
                lblSequenceComponent.Visible = False
                lblScoringType.Visible = False
                lblGroupComponent.Visible = False
                lblCalculationType.Visible = False
                lblSqlCommand.Visible = False
                lblDataSource.Visible = False

                txtScoreComponentID.Visible = False
                txtDescription.Visible = True
                txtSequenceComponent.Visible = True
                txtSqlCommand.Visible = True
                txtDataSource.Visible = True
                cboCalculationType.Visible = True
                cboGroupComponent.Visible = True
                cboScoringType.Visible = True


                lblScoreComponentID.Text = dtgCompanyCustomer.DataKeys.Item(e.Item.ItemIndex).ToString
                txtDescription.Text = oCredit.Description
                txtSequenceComponent.Text = CStr(oCredit.SeqComponent)
                txtSqlCommand.Text = oCredit.SQLCmd
                txtDataSource.Text = oCredit.DataSource

                If oCredit.ScoringType = "P" Then
                    cboScoringType.SelectedIndex = cboScoringType.Items.IndexOf(cboScoringType.Items.FindByValue("P"))
                ElseIf oCredit.ScoringType = "C" Then
                    cboScoringType.SelectedIndex = cboScoringType.Items.IndexOf(cboScoringType.Items.FindByValue("C"))
                End If

                If oCredit.GroupComponent = "C" Then
                    cboGroupComponent.SelectedIndex = cboGroupComponent.Items.IndexOf(cboGroupComponent.Items.FindByValue("C"))
                ElseIf oCredit.GroupComponent = "F" Then
                    cboGroupComponent.SelectedIndex = cboGroupComponent.Items.IndexOf(cboGroupComponent.Items.FindByValue("F"))
                ElseIf oCredit.GroupComponent = "A" Then
                    cboGroupComponent.SelectedIndex = cboGroupComponent.Items.IndexOf(cboGroupComponent.Items.FindByValue("A"))
                End If

                If oCredit.CalculationType = "R" Then
                    cboCalculationType.SelectedIndex = cboCalculationType.Items.IndexOf(cboCalculationType.Items.FindByValue("R"))
                    txtDataSource.Enabled = False
                ElseIf oCredit.CalculationType = "T" Then
                    cboCalculationType.SelectedIndex = cboCalculationType.Items.IndexOf(cboCalculationType.Items.FindByValue("T"))
                    txtDataSource.Enabled = True
                End If
            End If
        ElseIf e.CommandName = "View" Then
            If CheckFeature(Me.Loginid, Me.FormID, "VIEW", Me.AppId) Then
                ButtonSave.Visible = False
                ButtonCancel.Visible = False
                ButtonBack.Visible = True
                Dim lnkID As LinkButton = CType(e.Item.FindControl("lnkScor_CompanyCustomer"), LinkButton)
                Dim strID As String = lnkID.Text.Trim
                BindDetail(strID)
            End If
        ElseIf e.CommandName = "Delete" Then
            If CheckFeature(Me.Loginid, Me.FormID, "DEL", Me.AppId) Then
                Dim customClass As New Parameter.Credit
                With customClass
                    .Id = dtgCompanyCustomer.DataKeys.Item(e.Item.ItemIndex).ToString
                    .strConnection = GetConnectionString
                    .SPName = "spMarketingCreditDelete"
                End With

                Dim ResultOutput As String
                ResultOutput = m_controller.CreditDelete(customClass)
                If ResultOutput = "OK" Then                    
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, False)
                Else
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_FAILED, True)
                End If
                Me.CmdWhere = "ALL"
                txtSearch.Text = ""
                BindGridEntity_CompanyCustomer(Me.CmdWhere)
            End If
        End If
    End Sub
#End Region

#Region "dtgCompanyFinancial_ItemCommand"
    Private Sub dtgCompanyFinancial_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgCompanyFinancial.ItemCommand
        ButtonSave.Visible = True
        ButtonCancel.Visible = True
        ButtonBack.Visible = False

        Dim oCredit As New Parameter.Credit
        If e.CommandName = "Edit" Then
            If CheckFeature(Me.Loginid, Me.FormID, "EDIT", Me.AppId) Then
                Me.AddEdit = "EDIT"
                pnlAddEdit.Visible = True
                ButtonSave.Visible = True
                ButtonCancel.Visible = True
                ButtonBack.Visible = False
                lblRequiredScoreComponentID.Visible = False

                'semua panel harus divisible=false
                pnlCompanyAsset.Visible = False
                pnlCompanyCustomer.Visible = False
                pnlCompanyFinancial.Visible = False
                pnlPersonalAsset.Visible = False
                pnlPersonalCustomer.Visible = False
                pnlPersonalFinancial.Visible = False
                ButtonCancel.Visible = True
                ButtonSave.Visible = True

                lblTitleAddEdit.Text = Me.AddEdit
                oCredit.Id = dtgCompanyFinancial.DataKeys.Item(e.Item.ItemIndex).ToString
                oCredit.strConnection = GetConnectionString
                oCredit.SPName = "spMarketingCreditEdit"
                oCredit = m_controller.CreditEdit(oCredit)

                lblScoreComponentID.Visible = True
                lblDescription.Visible = False
                lblSequenceComponent.Visible = False
                lblScoringType.Visible = False
                lblGroupComponent.Visible = False
                lblCalculationType.Visible = False
                lblSqlCommand.Visible = False
                lblDataSource.Visible = False

                txtScoreComponentID.Visible = False
                txtDescription.Visible = True
                txtSequenceComponent.Visible = True
                txtSqlCommand.Visible = True
                txtDataSource.Visible = True
                cboCalculationType.Visible = True
                cboGroupComponent.Visible = True
                cboScoringType.Visible = True


                lblScoreComponentID.Text = dtgCompanyFinancial.DataKeys.Item(e.Item.ItemIndex).ToString
                txtDescription.Text = oCredit.Description
                txtSequenceComponent.Text = CStr(oCredit.SeqComponent)
                txtSqlCommand.Text = oCredit.SQLCmd
                txtDataSource.Text = oCredit.DataSource

                If oCredit.ScoringType = "P" Then
                    cboScoringType.SelectedIndex = cboScoringType.Items.IndexOf(cboScoringType.Items.FindByValue("P"))
                ElseIf oCredit.ScoringType = "C" Then
                    cboScoringType.SelectedIndex = cboScoringType.Items.IndexOf(cboScoringType.Items.FindByValue("C"))
                End If

                If oCredit.GroupComponent = "C" Then
                    cboGroupComponent.SelectedIndex = cboGroupComponent.Items.IndexOf(cboGroupComponent.Items.FindByValue("C"))
                ElseIf oCredit.GroupComponent = "F" Then
                    cboGroupComponent.SelectedIndex = cboGroupComponent.Items.IndexOf(cboGroupComponent.Items.FindByValue("F"))
                ElseIf oCredit.GroupComponent = "A" Then
                    cboGroupComponent.SelectedIndex = cboGroupComponent.Items.IndexOf(cboGroupComponent.Items.FindByValue("A"))
                End If

                If oCredit.CalculationType = "R" Then
                    cboCalculationType.SelectedIndex = cboCalculationType.Items.IndexOf(cboCalculationType.Items.FindByValue("R"))
                    txtDataSource.Enabled = False
                ElseIf oCredit.CalculationType = "T" Then
                    cboCalculationType.SelectedIndex = cboCalculationType.Items.IndexOf(cboCalculationType.Items.FindByValue("T"))
                    txtDataSource.Enabled = True
                End If
            End If
        ElseIf e.CommandName = "View" Then
            If CheckFeature(Me.Loginid, Me.FormID, "VIEW", Me.AppId) Then
                ButtonSave.Visible = False
                ButtonCancel.Visible = False
                ButtonBack.Visible = True
                Dim lnkID As LinkButton = CType(e.Item.FindControl("lnkScor_CompanyFinancial"), LinkButton)
                Dim strID As String = lnkID.Text.Trim
                BindDetail(strID)
            End If
        ElseIf e.CommandName = "Delete" Then
            If CheckFeature(Me.Loginid, Me.FormID, "DEL", Me.AppId) Then
                ButtonSave.Visible = True
                ButtonCancel.Visible = True
                ButtonBack.Visible = False
                Dim customClass As New Parameter.Credit
                With customClass
                    .Id = dtgCompanyFinancial.DataKeys.Item(e.Item.ItemIndex).ToString
                    .strConnection = GetConnectionString
                    .SPName = "spMarketingCreditDelete"
                End With

                Dim ResultOutput As String
                ResultOutput = m_controller.CreditDelete(customClass)
                If ResultOutput = "OK" Then                    
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, False)
                Else                    
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_FAILED, True)
                End If
                Me.CmdWhere = "ALL"
                txtSearch.Text = ""
                BindGridEntity_CompanyFinancial(Me.CmdWhere)
            End If
        End If
    End Sub
#End Region

#Region "dtgPersonalAsset_ItemCommand"
    Private Sub dtgPersonalAsset_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPersonalAsset.ItemCommand

        ButtonSave.Visible = True
        ButtonCancel.Visible = True
        ButtonBack.Visible = False

        Dim oCredit As New Parameter.Credit
        If e.CommandName = "Edit" Then
            If CheckFeature(Me.Loginid, Me.FormID, "EDIT", Me.AppId) Then
                Me.AddEdit = "EDIT"
                pnlAddEdit.Visible = True
                ButtonSave.Visible = True
                ButtonCancel.Visible = True
                ButtonBack.Visible = False
                lblRequiredScoreComponentID.Visible = False

                'semua panel harus divisible=false
                pnlCompanyAsset.Visible = False
                pnlCompanyCustomer.Visible = False
                pnlCompanyFinancial.Visible = False
                pnlPersonalAsset.Visible = False
                pnlPersonalCustomer.Visible = False
                pnlPersonalFinancial.Visible = False
                ButtonCancel.Visible = True
                ButtonSave.Visible = True

                lblTitleAddEdit.Text = Me.AddEdit
                oCredit.Id = dtgPersonalAsset.DataKeys.Item(e.Item.ItemIndex).ToString
                oCredit.strConnection = GetConnectionString
                oCredit.SPName = "spMarketingCreditEdit"
                oCredit = m_controller.CreditEdit(oCredit)

                lblScoreComponentID.Visible = True
                lblDescription.Visible = False
                lblSequenceComponent.Visible = False
                lblScoringType.Visible = False
                lblGroupComponent.Visible = False
                lblCalculationType.Visible = False
                lblSqlCommand.Visible = False
                lblDataSource.Visible = False

                txtScoreComponentID.Visible = False
                txtDescription.Visible = True
                txtSequenceComponent.Visible = True
                txtSqlCommand.Visible = True
                txtDataSource.Visible = True
                cboCalculationType.Visible = True
                cboGroupComponent.Visible = True
                cboScoringType.Visible = True


                lblScoreComponentID.Text = dtgPersonalAsset.DataKeys.Item(e.Item.ItemIndex).ToString
                txtDescription.Text = oCredit.Description
                txtSequenceComponent.Text = CStr(oCredit.SeqComponent)
                txtSqlCommand.Text = oCredit.SQLCmd
                txtDataSource.Text = oCredit.DataSource

                If oCredit.ScoringType = "P" Then
                    cboScoringType.SelectedIndex = cboScoringType.Items.IndexOf(cboScoringType.Items.FindByValue("P"))
                ElseIf oCredit.ScoringType = "C" Then
                    cboScoringType.SelectedIndex = cboScoringType.Items.IndexOf(cboScoringType.Items.FindByValue("C"))
                End If

                If oCredit.GroupComponent = "C" Then
                    cboGroupComponent.SelectedIndex = cboGroupComponent.Items.IndexOf(cboGroupComponent.Items.FindByValue("C"))
                ElseIf oCredit.GroupComponent = "F" Then
                    cboGroupComponent.SelectedIndex = cboGroupComponent.Items.IndexOf(cboGroupComponent.Items.FindByValue("F"))
                ElseIf oCredit.GroupComponent = "A" Then
                    cboGroupComponent.SelectedIndex = cboGroupComponent.Items.IndexOf(cboGroupComponent.Items.FindByValue("A"))
                End If

                If oCredit.CalculationType = "R" Then
                    cboCalculationType.SelectedIndex = cboCalculationType.Items.IndexOf(cboCalculationType.Items.FindByValue("R"))
                    txtDataSource.Enabled = False
                ElseIf oCredit.CalculationType = "T" Then
                    cboCalculationType.SelectedIndex = cboCalculationType.Items.IndexOf(cboCalculationType.Items.FindByValue("T"))
                    txtDataSource.Enabled = True
                End If
            End If
        ElseIf e.CommandName = "View" Then
            If CheckFeature(Me.Loginid, Me.FormID, "VIEW", Me.AppId) Then
                ButtonSave.Visible = False
                ButtonCancel.Visible = False
                ButtonBack.Visible = True
                Dim lnkID As LinkButton = CType(e.Item.FindControl("lnkScor_PersonalAsset"), LinkButton)
                Dim strID As String = lnkID.Text.Trim
                BindDetail(strID)
            End If
        ElseIf e.CommandName = "Delete" Then
            If CheckFeature(Me.Loginid, Me.FormID, "DEL", Me.AppId) Then
                ButtonSave.Visible = True
                ButtonCancel.Visible = True
                ButtonBack.Visible = False
                Dim customClass As New Parameter.Credit
                With customClass
                    .Id = dtgPersonalAsset.DataKeys.Item(e.Item.ItemIndex).ToString
                    .strConnection = GetConnectionString
                    .SPName = "spMarketingCreditDelete"
                End With

                Dim ResultOutput As String
                ResultOutput = m_controller.CreditDelete(customClass)
                If ResultOutput = "OK" Then                    
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, False)
                Else                    
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_FAILED, True)
                End If
                Me.CmdWhere = "ALL"
                txtSearch.Text = ""
                BindGridEntity_PersonalAsset(Me.CmdWhere)
            End If
        End If
    End Sub
#End Region

#Region "dtgPersonalCustomer_ItemCommand"
    Private Sub dtgPersonalCustomer_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPersonalCustomer.ItemCommand
        Dim oCredit As New Parameter.Credit
        If e.CommandName = "Edit" Then
            If CheckFeature(Me.Loginid, Me.FormID, "EDIT", Me.AppId) Then
                Me.AddEdit = "EDIT"
                pnlAddEdit.Visible = True

                ButtonCancel.Visible = True
                ButtonSave.Visible = True
                ButtonBack.Visible = False
                lblRequiredScoreComponentID.Visible = False

                'semua panel harus divisible=false
                pnlCompanyAsset.Visible = False
                pnlCompanyCustomer.Visible = False
                pnlCompanyFinancial.Visible = False
                pnlPersonalAsset.Visible = False
                pnlPersonalCustomer.Visible = False
                pnlPersonalFinancial.Visible = False
                ButtonCancel.Visible = True
                ButtonSave.Visible = True

                lblTitleAddEdit.Text = Me.AddEdit
                oCredit.Id = dtgPersonalCustomer.DataKeys.Item(e.Item.ItemIndex).ToString
                oCredit.strConnection = GetConnectionString
                oCredit.SPName = "spMarketingCreditEdit"
                oCredit = m_controller.CreditEdit(oCredit)

                lblScoreComponentID.Visible = True
                lblDescription.Visible = False
                lblSequenceComponent.Visible = False
                lblScoringType.Visible = False
                lblGroupComponent.Visible = False
                lblCalculationType.Visible = False
                lblSqlCommand.Visible = False
                lblDataSource.Visible = False

                txtScoreComponentID.Visible = False
                txtDescription.Visible = True
                txtSequenceComponent.Visible = True
                txtSqlCommand.Visible = True
                txtDataSource.Visible = True
                cboCalculationType.Visible = True
                cboGroupComponent.Visible = True
                cboScoringType.Visible = True


                lblScoreComponentID.Text = dtgPersonalCustomer.DataKeys.Item(e.Item.ItemIndex).ToString
                txtDescription.Text = oCredit.Description
                txtSequenceComponent.Text = CStr(oCredit.SeqComponent)
                txtSqlCommand.Text = oCredit.SQLCmd
                txtDataSource.Text = oCredit.DataSource

                If oCredit.ScoringType = "P" Then
                    cboScoringType.SelectedIndex = cboScoringType.Items.IndexOf(cboScoringType.Items.FindByValue("P"))
                ElseIf oCredit.ScoringType = "C" Then
                    cboScoringType.SelectedIndex = cboScoringType.Items.IndexOf(cboScoringType.Items.FindByValue("C"))
                End If

                If oCredit.GroupComponent = "C" Then
                    cboGroupComponent.SelectedIndex = cboGroupComponent.Items.IndexOf(cboGroupComponent.Items.FindByValue("C"))
                ElseIf oCredit.GroupComponent = "F" Then
                    cboGroupComponent.SelectedIndex = cboGroupComponent.Items.IndexOf(cboGroupComponent.Items.FindByValue("F"))
                ElseIf oCredit.GroupComponent = "A" Then
                    cboGroupComponent.SelectedIndex = cboGroupComponent.Items.IndexOf(cboGroupComponent.Items.FindByValue("A"))
                End If

                If oCredit.CalculationType = "R" Then
                    cboCalculationType.SelectedIndex = cboCalculationType.Items.IndexOf(cboCalculationType.Items.FindByValue("R"))
                    txtDataSource.Enabled = False
                ElseIf oCredit.CalculationType = "T" Then
                    cboCalculationType.SelectedIndex = cboCalculationType.Items.IndexOf(cboCalculationType.Items.FindByValue("T"))
                    txtDataSource.Enabled = True
                End If
            End If
        ElseIf e.CommandName = "View" Then
            If CheckFeature(Me.Loginid, Me.FormID, "VIEW", Me.AppId) Then
                ButtonSave.Visible = False
                ButtonCancel.Visible = False
                ButtonBack.Visible = True

                Dim lnkID As LinkButton = CType(e.Item.FindControl("lnkScor_PersonalCustomer"), LinkButton)
                Dim strID As String = lnkID.Text.Trim
                BindDetail(strID)
            End If
        ElseIf e.CommandName = "Delete" Then
            If CheckFeature(Me.Loginid, Me.FormID, "DEL", Me.AppId) Then
                ButtonCancel.Visible = True
                ButtonSave.Visible = True
                ButtonBack.Visible = False

                Dim customClass As New Parameter.Credit
                With customClass
                    .Id = dtgPersonalCustomer.DataKeys.Item(e.Item.ItemIndex).ToString
                    .strConnection = GetConnectionString
                    .SPName = "spMarketingCreditDelete"
                End With

                Dim ResultOutput As String
                ResultOutput = m_controller.CreditDelete(customClass)
                If ResultOutput = "OK" Then                    
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, False)
                Else
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_FAILED, True)
                End If
                Me.CmdWhere = "ALL"
                txtSearch.Text = ""
                BindGridEntity_PersonalCustomer(Me.CmdWhere)
            End If
        End If
    End Sub
#End Region

#Region "dtgPersonalFinancial_ItemCommand"
    Private Sub dtgPersonalFinancial_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPersonalFinancial.ItemCommand
        Dim oCredit As New Parameter.Credit
        If e.CommandName = "Edit" Then
            If CheckFeature(Me.Loginid, Me.FormID, "EDIT", Me.AppId) Then
                Me.AddEdit = "EDIT"
                pnlAddEdit.Visible = True
                lblRequiredScoreComponentID.Visible = False

                'semua panel harus divisible=false
                pnlCompanyAsset.Visible = False
                pnlCompanyCustomer.Visible = False
                pnlCompanyFinancial.Visible = False
                pnlPersonalAsset.Visible = False
                pnlPersonalCustomer.Visible = False
                pnlPersonalFinancial.Visible = False
                ButtonCancel.Visible = True
                ButtonSave.Visible = True

                lblTitleAddEdit.Text = Me.AddEdit
                oCredit.Id = dtgPersonalFinancial.DataKeys.Item(e.Item.ItemIndex).ToString
                oCredit.strConnection = GetConnectionString
                oCredit.SPName = "spMarketingCreditEdit"
                oCredit = m_controller.CreditEdit(oCredit)

                lblScoreComponentID.Visible = True
                lblDescription.Visible = False
                lblSequenceComponent.Visible = False
                lblScoringType.Visible = False
                lblGroupComponent.Visible = False
                lblCalculationType.Visible = False
                lblSqlCommand.Visible = False
                lblDataSource.Visible = False

                txtScoreComponentID.Visible = False
                txtDescription.Visible = True
                txtSequenceComponent.Visible = True
                txtSqlCommand.Visible = True
                txtDataSource.Visible = True
                cboCalculationType.Visible = True
                cboGroupComponent.Visible = True
                cboScoringType.Visible = True


                lblScoreComponentID.Text = dtgPersonalFinancial.DataKeys.Item(e.Item.ItemIndex).ToString
                txtDescription.Text = oCredit.Description
                txtSequenceComponent.Text = CStr(oCredit.SeqComponent)
                txtSqlCommand.Text = oCredit.SQLCmd
                txtDataSource.Text = oCredit.DataSource

                If oCredit.ScoringType = "P" Then
                    cboScoringType.SelectedIndex = cboScoringType.Items.IndexOf(cboScoringType.Items.FindByValue("P"))
                ElseIf oCredit.ScoringType = "C" Then
                    cboScoringType.SelectedIndex = cboScoringType.Items.IndexOf(cboScoringType.Items.FindByValue("C"))
                End If

                If oCredit.GroupComponent = "C" Then
                    cboGroupComponent.SelectedIndex = cboGroupComponent.Items.IndexOf(cboGroupComponent.Items.FindByValue("C"))
                ElseIf oCredit.GroupComponent = "F" Then
                    cboGroupComponent.SelectedIndex = cboGroupComponent.Items.IndexOf(cboGroupComponent.Items.FindByValue("F"))
                ElseIf oCredit.GroupComponent = "A" Then
                    cboGroupComponent.SelectedIndex = cboGroupComponent.Items.IndexOf(cboGroupComponent.Items.FindByValue("A"))
                End If

                If oCredit.CalculationType = "R" Then
                    cboCalculationType.SelectedIndex = cboCalculationType.Items.IndexOf(cboCalculationType.Items.FindByValue("R"))
                    txtDataSource.Enabled = False
                ElseIf oCredit.CalculationType = "T" Then
                    cboCalculationType.SelectedIndex = cboCalculationType.Items.IndexOf(cboCalculationType.Items.FindByValue("T"))
                    txtDataSource.Enabled = True
                End If
            End If
        ElseIf e.CommandName = "View" Then
            If CheckFeature(Me.Loginid, Me.FormID, "VIEW", Me.AppId) Then
                ButtonSave.Visible = False
                ButtonCancel.Visible = False
                ButtonBack.Visible = True

                Dim lnkID As LinkButton = CType(e.Item.FindControl("lnkScor_PersonalFinancial"), LinkButton)
                Dim strID As String = lnkID.Text.Trim
                BindDetail(strID)
            End If
        ElseIf e.CommandName = "Delete" Then
            If CheckFeature(Me.Loginid, Me.FormID, "DEL", Me.AppId) Then
                ButtonCancel.Visible = True
                ButtonSave.Visible = True
                ButtonBack.Visible = False

                Dim customClass As New Parameter.Credit
                With customClass
                    .Id = dtgPersonalFinancial.DataKeys.Item(e.Item.ItemIndex).ToString
                    .strConnection = GetConnectionString
                    .SPName = "spMarketingCreditDelete"
                End With

                Dim ResultOutput As String
                ResultOutput = m_controller.CreditDelete(customClass)
                If ResultOutput = "OK" Then                    
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, False)
                Else                    
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_FAILED, True)
                End If
                Me.CmdWhere = "ALL"
                txtSearch.Text = ""
                BindGridEntity_PersonalFinancial(Me.CmdWhere)
            End If
        End If
    End Sub
#End Region

#Region "BindDetail"
    Sub BindDetail(ByVal ID As String)
        Dim oCredit As New Parameter.Credit
        Me.AddEdit = "VIEW"
        pnlAddEdit.Visible = True
        lblTitleAddEdit.Text = Me.AddEdit

        oCredit.Id = ID
        oCredit.strConnection = GetConnectionString
        oCredit.SPName = "spMarketingCreditEdit"
        oCredit = m_controller.CreditEdit(oCredit)

        'semua panel harus divisible=false
        pnlCompanyAsset.Visible = False
        pnlCompanyCustomer.Visible = False
        pnlCompanyFinancial.Visible = False
        pnlPersonalAsset.Visible = False
        pnlPersonalCustomer.Visible = False
        pnlPersonalFinancial.Visible = False

        lblScoreComponentID.Visible = True
        lblDescription.Visible = True
        lblSequenceComponent.Visible = True
        lblScoringType.Visible = True
        lblGroupComponent.Visible = True
        lblCalculationType.Visible = True
        lblSqlCommand.Visible = True
        lblDataSource.Visible = True

        txtScoreComponentID.Visible = False
        txtDescription.Visible = False
        txtSequenceComponent.Visible = False
        txtSqlCommand.Visible = False
        txtDataSource.Visible = False
        cboCalculationType.Visible = False
        cboGroupComponent.Visible = False
        cboScoringType.Visible = False

        lblScoreComponentID.Text = ID
        lblDescription.Text = oCredit.Description
        lblSequenceComponent.Text = CStr(oCredit.SeqComponent)

        If oCredit.ScoringType = "P" Then
            lblScoringType.Text = "Personal"
        ElseIf oCredit.ScoringType = "C" Then
            lblScoringType.Text = "Company"
        End If

        If oCredit.GroupComponent = "C" Then
            lblGroupComponent.Text = "Customer"
        ElseIf oCredit.GroupComponent = "F" Then
            lblGroupComponent.Text = "Financial"
        ElseIf oCredit.GroupComponent = "A" Then
            lblGroupComponent.Text = "Asset"
        End If

        If oCredit.CalculationType = "R" Then
            lblCalculationType.Text = "Range"
        ElseIf oCredit.CalculationType = "T" Then
            lblCalculationType.Text = "Table"
        End If

        lblSqlCommand.Text = oCredit.SQLCmd
        lblDataSource.Text = oCredit.DataSource
    End Sub
#End Region

#Region "Save"
    Private Sub ButtonSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
        If Page.IsValid Then
            ButtonPrint.Enabled = True
            If cboCalculationType.SelectedItem.Value = "T" Then
                If txtDataSource.Text.Trim = "" Then                    
                    ShowMessage(lblMessage, "*", True)
                    txtDataSource.Enabled = True
                    pnlAddEdit.Visible = True
                    pnlCompanyAsset.Visible = False
                    pnlCompanyCustomer.Visible = False
                    pnlCompanyFinancial.Visible = False
                    pnlPersonalAsset.Visible = False
                    pnlPersonalCustomer.Visible = False
                    pnlPersonalFinancial.Visible = False
                    Exit Sub
                End If
            End If

            Dim customClass As New Parameter.Credit
            Dim ErrMessage As String = ""

            If Me.AddEdit = "ADD" Then
                With customClass
                    .Id = txtScoreComponentID.Text.Trim
                    .Description = txtDescription.Text
                    .ScoringType = cboScoringType.SelectedItem.Value
                    .GroupComponent = cboGroupComponent.SelectedItem.Value
                    .SeqComponent = CInt(txtSequenceComponent.Text)
                    .CalculationType = cboCalculationType.SelectedItem.Value
                    .SQLCmd = txtSqlCommand.Text
                    .DataSource = txtDataSource.Text
                    .strConnection = GetConnectionString()
                    .SPName = "spMarketingCreditSaveAdd"
                End With

                ErrMessage = m_controller.CreditSaveAdd(customClass)
                If ErrMessage <> "" Then                    
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_FAILED, True)
                    pnlAddEdit.Visible = True
                    pnlCompanyAsset.Visible = False
                    pnlCompanyCustomer.Visible = False
                    pnlCompanyFinancial.Visible = False
                    pnlPersonalAsset.Visible = False
                    pnlPersonalCustomer.Visible = False
                    pnlPersonalFinancial.Visible = False
                    Exit Sub
                Else                    
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_SUCCESS, False)
                    BindGridEntity_CompanyAsset(Me.CmdWhere)
                    Me.CmdWhere = "ALL"
                    BindGridEntity_CompanyAsset(Me.CmdWhere)
                    BindGridEntity_CompanyCustomer(Me.CmdWhere)
                    BindGridEntity_CompanyFinancial(Me.CmdWhere)
                    BindGridEntity_PersonalAsset(Me.CmdWhere)
                    BindGridEntity_PersonalCustomer(Me.CmdWhere)
                    BindGridEntity_PersonalFinancial(Me.CmdWhere)
                End If
            ElseIf Me.AddEdit = "EDIT" Then
                With customClass
                    .Id = lblScoreComponentID.Text
                    .Description = txtDescription.Text
                    .ScoringType = cboScoringType.SelectedItem.Value
                    .GroupComponent = cboGroupComponent.SelectedItem.Value
                    .SeqComponent = CInt(txtSequenceComponent.Text)
                    .CalculationType = cboCalculationType.SelectedItem.Value
                    .SQLCmd = txtSqlCommand.Text
                    .DataSource = txtDataSource.Text
                    .strConnection = GetConnectionString()
                    .SPName = "spMarketingCreditSaveEdit"
                End With

                m_controller.CreditSaveEdit(customClass)                
                ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
                Me.CmdWhere = "ALL"
                BindGridEntity_CompanyAsset(Me.CmdWhere)
                BindGridEntity_CompanyCustomer(Me.CmdWhere)
                BindGridEntity_CompanyFinancial(Me.CmdWhere)
                BindGridEntity_PersonalAsset(Me.CmdWhere)
                BindGridEntity_PersonalCustomer(Me.CmdWhere)
                BindGridEntity_PersonalFinancial(Me.CmdWhere)
            End If
            txtSearch.Text = ""
        End If
    End Sub
#End Region

#Region "Add"
    Private Sub imbAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonAdd.Click
        If CheckFeature(Me.Loginid, Me.FormID, "ADD", Me.AppId) Then
            pnlAddEdit.Visible = True
            pnlCompanyAsset.Visible = False
            pnlCompanyCustomer.Visible = False
            pnlCompanyFinancial.Visible = False
            pnlPersonalAsset.Visible = False
            pnlPersonalCustomer.Visible = False
            pnlPersonalFinancial.Visible = False
            lblRequiredScoreComponentID.Visible = True

            ButtonCancel.Visible = True
            ButtonSave.Visible = True
            ButtonBack.Visible = False

            Me.AddEdit = "ADD"
            lblTitleAddEdit.Text = Me.AddEdit

            lblScoreComponentID.Visible = False
            lblDescription.Visible = False
            lblSequenceComponent.Visible = False
            lblScoringType.Visible = False
            lblGroupComponent.Visible = False
            lblCalculationType.Visible = False
            lblSqlCommand.Visible = False
            lblDataSource.Visible = False

            txtScoreComponentID.Visible = True
            txtDescription.Visible = True
            txtSequenceComponent.Visible = True
            txtSqlCommand.Visible = True
            txtDataSource.Visible = True
            cboCalculationType.Visible = True
            cboGroupComponent.Visible = True
            cboScoringType.Visible = True

            txtScoreComponentID.Text = ""
            txtDescription.Text = ""
            txtSequenceComponent.Text = ""
            txtSqlCommand.Text = ""
            txtDataSource.Text = ""
            cboCalculationType.SelectedIndex = 0
            cboGroupComponent.SelectedIndex = 0
            cboScoringType.SelectedIndex = 0

            If cboCalculationType.SelectedIndex = 1 Then
                txtDataSource.Enabled = True
            ElseIf cboCalculationType.SelectedIndex = 0 Then
                txtDataSource.Enabled = False
            End If
        End If
    End Sub
#End Region

#Region "print"
    Private Sub ButtonPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonPrint.Click
        If CheckFeature(Me.Loginid, Me.FormID, "PRINT", Me.AppId) Then
            SendCookies()
            Response.Redirect("Report/MarketingCreditView.aspx")
        End If
    End Sub
#End Region

#Region "SendCookies"
    Sub SendCookies()
        Dim cookie As HttpCookie = Request.Cookies("MarketingCredit")
        If Not cookie Is Nothing Then
            cookie.Values("SearchBy") = cboSearch.SelectedItem.Value
            cookie.Values("SearchText") = txtSearch.Text
            cookie.Values("PageFrom") = "Credit"
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie("MarketingCredit")
            cookieNew.Values.Add("SearchBy", cboSearch.SelectedItem.Value)
            cookieNew.Values.Add("SearchText", txtSearch.Text)
            cookieNew.Values.Add("PageFrom", "Credit")
            Response.AppendCookie(cookieNew)
        End If
    End Sub
#End Region

#Region "Reset"
    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        cboSearch.SelectedIndex = 0
        txtSearch.Text = ""
        Me.CmdWhere = "ALL"
        BindGridEntity_CompanyAsset(Me.CmdWhere)
        BindGridEntity_CompanyCustomer(Me.CmdWhere)
        BindGridEntity_CompanyFinancial(Me.CmdWhere)
        BindGridEntity_PersonalAsset(Me.CmdWhere)
        BindGridEntity_PersonalCustomer(Me.CmdWhere)
        BindGridEntity_PersonalFinancial(Me.CmdWhere)

        pnlCompanyAsset.Visible = True
        pnlCompanyCustomer.Visible = False
        pnlCompanyFinancial.Visible = True
        pnlPersonalAsset.Visible = True
        pnlPersonalCustomer.Visible = True
        pnlPersonalFinancial.Visible = True
        PnlCustomer.Visible = True
        PnlCompany.Visible = True
        ButtonPrint.Enabled = True
    End Sub
#End Region

#Region "imbSearch"
    Private Sub imbSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonSearch.Click
        If txtSearch.Text.Trim <> "" Then
            Me.CmdWhere = cboSearch.SelectedItem.Value + " LIKE '%" + txtSearch.Text.Trim + "%'"
        Else
            Me.CmdWhere = "ALL"
        End If
        BindGridEntity_PersonalAssetSearch(Me.CmdWhere)
        BindGridEntity_PersonalCustomerSeacrh(Me.CmdWhere)
        BindGridEntity_PersonalFinancialSearch(Me.CmdWhere)
        pnlCompanyCustomer.Visible = False
        check_totalrecord_print()
    End Sub
#End Region

#Region "Check_totalrecord"
    Sub check_totalrecord_message()
        If (dtgCompanyAsset.Items.Count = 0 And dtgCompanyCustomer.Items.Count = 0 And dtgCompanyFinancial.Items.Count = 0 And dtgPersonalAsset.Items.Count = 0 And dtgPersonalCustomer.Items.Count = 0 And dtgPersonalFinancial.Items.Count = 0) Then            
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            Exit Sub
        End If
    End Sub

    Sub check_totalrecord_print()
        If (dtgCompanyAsset.Items.Count = 0 And dtgCompanyCustomer.Items.Count = 0 And dtgCompanyFinancial.Items.Count = 0 And dtgPersonalAsset.Items.Count = 0 And dtgPersonalCustomer.Items.Count = 0 And dtgPersonalFinancial.Items.Count = 0) Then            
            ShowMessage(lblMessage, "Data tidak ditemukan.....", True)
            ButtonPrint.Enabled = False
            Exit Sub
        End If
    End Sub
#End Region

#Region "InitialDefaultPanel"
    Private Sub InitialDefaultPanel()
        pnlAddEdit.Visible = False
        pnlCompanyAsset.Visible = False
        pnlCompanyCustomer.Visible = False
        pnlCompanyFinancial.Visible = False
        pnlPersonalAsset.Visible = True
        pnlPersonalCustomer.Visible = True
        pnlPersonalFinancial.Visible = True        

    End Sub
#End Region

#Region "ButtonCancel"
    Private Sub ButtonCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        InitialDefaultPanel()
        txtSearch.Text = ""
        Me.CmdWhere = "ALL"
        'BindGridEntity_CompanyAsset(Me.CmdWhere)
        'BindGridEntity_CompanyCustomer(Me.CmdWhere)
        'BindGridEntity_CompanyFinancial(Me.CmdWhere)
        BindGridEntity_PersonalAsset(Me.CmdWhere)
        BindGridEntity_PersonalCustomer(Me.CmdWhere)
        BindGridEntity_PersonalFinancial(Me.CmdWhere)
        ButtonPrint.Enabled = True
    End Sub
#End Region

#Region "ButtonBack"
    Private Sub ButtonBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonBack.Click
        InitialDefaultPanel()
    End Sub
#End Region

#Region "dtg_ItemDataBound"
    Private Sub dtgCompanyAsset_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgCompanyAsset.ItemDataBound
        Dim imbDelete As ImageButton
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imbDelete_dtgCompanyAsset"), ImageButton)
            imbDelete.Attributes.Add("Onclick", "return DeleteConfirm()")
        End If
    End Sub

    Private Sub dtgCompanyCustomer_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgCompanyCustomer.ItemDataBound
        Dim imbDelete As ImageButton
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imbDelete_dtgCompanyCustomer"), ImageButton)
            imbDelete.Attributes.Add("Onclick", "return DeleteConfirm()")
        End If
    End Sub

    Private Sub dtgCompanyFinancial_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgCompanyFinancial.ItemDataBound
        Dim imbDelete As ImageButton
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imbDelete_dtgCompanyFinancial"), ImageButton)
            imbDelete.Attributes.Add("Onclick", "return DeleteConfirm()")
        End If
    End Sub

    Private Sub dtgPersonalAsset_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPersonalAsset.ItemDataBound
        Dim imbDelete As ImageButton
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imbDelete_dtgPersonalAsset"), ImageButton)
            imbDelete.Attributes.Add("Onclick", "return DeleteConfirm()")
        End If
    End Sub

    Private Sub dtgPersonalCustomer_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPersonalCustomer.ItemDataBound
        Dim imbDelete As ImageButton
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imbDelete_dtgPersonalCustomer"), ImageButton)
            imbDelete.Attributes.Add("Onclick", "return DeleteConfirm()")
        End If
    End Sub


    Private Sub dtgPersonalFinancial_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPersonalFinancial.ItemDataBound
        Dim imbDelete As ImageButton
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imbDelete_dtgPersonalFinancial"), ImageButton)
            imbDelete.Attributes.Add("Onclick", "return DeleteConfirm()")
        End If
    End Sub
#End Region

End Class