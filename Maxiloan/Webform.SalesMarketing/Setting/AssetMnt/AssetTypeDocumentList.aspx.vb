﻿#Region "Imports"
Imports Maxiloan.cbse
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
#End Region


Public Class AssetTypeDocumentList
    Inherits Maxiloan.Webform.WebBased

    Private m_controller As New AssetTypeDocumentListController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property DocumentListID() As String
        Get
            Return CType(ViewState("DocumentListID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("DocumentListID") = Value
        End Set
    End Property
    Private Property AddEdit() As String
        Get
            Return CType(ViewState("vwsAddEdt"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsAddEdt") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property
    Private Property AssetTypeID() As String
        Get
            Return CType(ViewState("AssetTypeID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("AssetTypeID") = Value
        End Set
    End Property
    Private Property DescriptionAssetType() As String
        Get
            Return CType(ViewState("DescriptionAssetType"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("DescriptionAssetType") = Value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If
        InitialDefaultPanel()
        Me.FormID = "AssetType"

        If Not Me.IsPostBack Then
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If SessionInvalid() Then
                    Exit Sub
                End If
                Me.SearchBy = ""
                Me.SortBy = ""
            End If
            txtPage.Text = "1"
            Me.Sort = "AssetDocID ASC"
            Me.AssetTypeID = Request("id")
            Me.DescriptionAssetType = Request("Description")
            Me.DocumentListID = Request("DocumentListID")

            'If Request("cmd") = "dtl" Then
            '    BindDetail(Me.AssetTypeID, Me.DocumentListID)
            'End If
            hplAssetID.NavigateUrl = LinkTo(Me.AssetTypeID)

            If Request("cond") <> "" Then
                Me.CmdWhere = "AssetTypeID= '" & Me.AssetTypeID & "' " + Request("cond")
            Else
                Me.CmdWhere = "AssetTypeID= '" & Me.AssetTypeID & "'"

            End If
            BindGridEntity(Me.CmdWhere)
            lblJenisAssetID.Text = Me.AssetTypeID
            'ButtonBack.Attributes.Add("OnClick", "return fBack()")
            'ButtonCancel.Attributes.Add("OnClick", "return fBack()")
        End If
    End Sub

    Function LinkTo(ByVal strAssetTypeID As String) As String
        Return "javascript:OpenWinAssetTypeView('" & strAssetTypeID & "')"
    End Function

    Private Sub InitialDefaultPanel()
        pnlList.Visible = True
        pnlAddEdit.Visible = False
        pnlHeader.Visible = True
    End Sub

    Sub BindGridEntity(ByVal cmdWhere As String)
        Dim dtEntity As DataTable
        Dim oAssetTypeDocumentList As New Parameter.AssetTypeDocumentList

        With oAssetTypeDocumentList
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = Me.Sort
            .strConnection = GetConnectionString()
        End With
        oAssetTypeDocumentList = m_controller.GetAssetTypeDocumentList(oAssetTypeDocumentList)

        If Not oAssetTypeDocumentList Is Nothing Then
            dtEntity = oAssetTypeDocumentList.ListData
            recordCount = oAssetTypeDocumentList.TotalRecords
            Me.DocumentListID = oAssetTypeDocumentList.DocumentListID
        Else
            recordCount = 0
        End If
       
        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()

        hplAssetID.Text = Me.AssetTypeID
        lblDescription.Text = Me.DescriptionAssetType

        PagingFooter()
    End Sub
    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        Dim imbDelete As ImageButton
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imbDelete"), ImageButton)
            imbDelete.Attributes.Add("Onclick", "return DeleteConfirm()")
        End If
    End Sub
    Sub BindDetail(ByVal ID As String, ByVal DocumentListID As String)
        Dim oAssetTypeDocumentList As New Parameter.AssetTypeDocumentList
        Me.AddEdit = "VIEW"
        pnlAddEdit.Visible = True
        pnlList.Visible = False
        pnlHeader.Visible = True
        ButtonBack.Visible = True
        ButtonCancel.Visible = False
        ButtonSave.Visible = False

        lblTitleAddEdit.Text = Me.AddEdit
        With oAssetTypeDocumentList
            .Id = ID
            .DocumentListID = DocumentListID
            .strConnection = GetConnectionString()
        End With
        oAssetTypeDocumentList = m_controller.GetAssetTypeDocumentListEdit(oAssetTypeDocumentList)

        lblDocumentListID.Visible = True
        txtDocumentListID.Visible = False
        lblName.Visible = True
        txtName.Visible = False
        lblValue.Visible = True
        rboValue.Visible = False
        lblMainDoc.Visible = True
        rboMainDoc.Visible = False
        lblUntukCustomer.Visible = True
        rboUntukCustomer.Visible = False
        lblNewAsset.Visible = True
        rboNewAsset.Visible = False
        lblUsedAsset.Visible = True
        rboUsedAsset.Visible = False

        lblDocumentListID.Text = DocumentListID.Trim
        lblName.Text = oAssetTypeDocumentList.Name.Trim
        lblValue.Text = IIf(oAssetTypeDocumentList.Value.Trim = "0", "No", "Yes").ToString
        lblMainDoc.Text = IIf(oAssetTypeDocumentList.MainDoc.Trim = "0", "Additional", "Main").ToString
        lblNewAsset.Text = IIf(oAssetTypeDocumentList.NewAsset.Trim = "0", "No", "Yes").ToString
        lblUsedAsset.Text = IIf(oAssetTypeDocumentList.UsedAsset.Trim = "0", "No", "Yes").ToString
        lblUntukCustomer.Text = rboUntukCustomer.Items.IndexOf(rboUntukCustomer.Items.FindByValue(oAssetTypeDocumentList.CustomerType.ToUpper.Trim)).ToString
    End Sub
#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan.....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int32)
                BindGridEntity(Me.CmdWhere)
            End If
        End If
    End Sub
#End Region

    Private Sub dtgEntity_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        Dim oAssetTypeDocumentList As New Parameter.AssetTypeDocumentList
        If e.CommandName = "Edit" Then
            If CheckFeature(Me.Loginid, Me.FormID, "EDIT", Me.AppId) Then
                Me.AddEdit = "EDIT"
                pnlAddEdit.Visible = True
                pnlList.Visible = False
                pnlHeader.Visible = False
                ButtonBack.Visible = False
                ButtonCancel.Visible = True
                ButtonSave.Visible = True
                lblRequiredDocListID.Visible = False

                lblDocumentListID.Visible = True
                txtDocumentListID.Visible = False
                lblName.Visible = False
                txtName.Visible = True
                lblValue.Visible = False
                rboValue.Visible = True
                lblMainDoc.Visible = False
                rboMainDoc.Visible = True
                lblUntukCustomer.Visible = False
                rboUntukCustomer.Visible = True
                lblNewAsset.Visible = False
                rboNewAsset.Visible = True
                lblUsedAsset.Visible = False
                rboUsedAsset.Visible = True

                lblTitleAddEdit.Text = Me.AddEdit
                With oAssetTypeDocumentList
                    .strConnection = GetConnectionString()
                    .Id = Me.AssetTypeID
                    .DocumentListID = dtgPaging.DataKeys.Item(e.Item.ItemIndex).ToString
                End With
                oAssetTypeDocumentList = m_controller.GetAssetTypeDocumentListEdit(oAssetTypeDocumentList)
                txtDocumentListID.Text = dtgPaging.DataKeys.Item(e.Item.ItemIndex).ToString
                lblDocumentListID.Text = dtgPaging.DataKeys.Item(e.Item.ItemIndex).ToString
                txtName.Text = oAssetTypeDocumentList.Name.Trim
                rboValue.SelectedIndex = rboValue.Items.IndexOf(rboValue.Items.FindByValue(oAssetTypeDocumentList.Value))
                rboMainDoc.SelectedIndex = rboMainDoc.Items.IndexOf(rboMainDoc.Items.FindByValue(oAssetTypeDocumentList.MainDoc))
                rboNewAsset.SelectedIndex = rboNewAsset.Items.IndexOf(rboNewAsset.Items.FindByValue(oAssetTypeDocumentList.NewAsset))
                rboUsedAsset.SelectedIndex = rboUsedAsset.Items.IndexOf(rboUsedAsset.Items.FindByValue(oAssetTypeDocumentList.UsedAsset))
                rboUntukCustomer.SelectedIndex = rboUntukCustomer.Items.IndexOf(rboUntukCustomer.Items.FindByValue(oAssetTypeDocumentList.CustomerType))
                rboOrigination.SelectedIndex = rboOrigination.Items.IndexOf(rboOrigination.Items.FindByValue(oAssetTypeDocumentList.Origination))
                If oAssetTypeDocumentList.IsNoRequired.Trim <> "" Then
                    If CBool(oAssetTypeDocumentList.IsNoRequired) Then
                        rboNoDokumen.SelectedIndex = rboNoDokumen.Items.IndexOf(rboNoDokumen.Items.FindByValue("1"))
                    Else
                        rboNoDokumen.SelectedIndex = rboNoDokumen.Items.IndexOf(rboNoDokumen.Items.FindByValue("0"))
                    End If
                Else
                    rboNoDokumen.SelectedIndex = rboNoDokumen.Items.IndexOf(rboNoDokumen.Items.FindByValue("0"))
                End If
            End If
        ElseIf e.CommandName = "Delete" Then
            If CheckFeature(Me.Loginid, Me.FormID, "DEL", Me.AppId) Then
                Dim customClass As New Parameter.AssetTypeDocumentList
                With customClass
                    .Id = Me.AssetTypeID
                    .DocumentListID = dtgPaging.DataKeys.Item(e.Item.ItemIndex).ToString
                    .strConnection = GetConnectionString()
                End With
                Dim ResultOutput As String

                ResultOutput = m_controller.AssetTypeDocumentListDelete(customClass)
                If ResultOutput = "OK" Then
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, False)
                Else
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_FAILED, True)
                End If
                Me.CmdWhere = "AssetTypeID= '" & Me.AssetTypeID & "'"
                txtSearch.Text = ""
                BindGridEntity(Me.CmdWhere)
            End If
        ElseIf e.CommandName = "View" Then
            If CheckFeature(Me.Loginid, Me.FormID, "VIEW", Me.AppId) Then
                Dim lnkAssetDocID As LinkButton = CType(e.Item.FindControl("lnkAssetDocID"), LinkButton)
                Dim strAssetDocID As String = lnkAssetDocID.Text.Trim
                BindDetail(hplAssetID.Text, strAssetDocID)
                Me.DescriptionAssetType = lblDescription.Text
                lblRequiredDocListID.Visible = False
                lblRequiredDocName.Visible = False
            End If
        End If
    End Sub

    Private Sub ButtonSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
        If Page.IsValid Then
            Dim customClass As New Parameter.AssetTypeDocumentList
            Dim ErrMessage As String = ""
            With customClass
                .Id = hplAssetID.Text
                .Name = txtName.Text
                .DocumentListID = txtDocumentListID.Text
                .Value = rboValue.SelectedValue
                .MainDoc = rboMainDoc.SelectedValue
                .CustomerType = rboUntukCustomer.SelectedValue
                .NewAsset = rboNewAsset.SelectedValue
                .UsedAsset = rboUsedAsset.SelectedValue
                .Origination = rboOrigination.SelectedValue
                .IsNoRequired = rboNoDokumen.SelectedValue
                .strConnection = GetConnectionString()
            End With

            If Me.AddEdit = "ADD" Then
                ErrMessage = m_controller.AssetTypeDocumentListSaveAdd(customClass)
                If ErrMessage <> "" Then
                    If ErrMessage = "Double" Then
                        ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_FAILED, True)
                        pnlAddEdit.Visible = True
                        pnlList.Visible = False
                        pnlHeader.Visible = False
                        Exit Sub
                    Else
                        ShowMessage(lblMessage, "Setiap Jenis Asset tidak boleh memiliki multiple main dokumen", True)
                        pnlAddEdit.Visible = True
                        pnlList.Visible = False
                        pnlHeader.Visible = True
                    End If
                Else
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_SUCCESS, False)
                    txtSearch.Text = ""
                    Me.CmdWhere = "AssetTypeID = '" & Me.AssetTypeID.Trim & "'"
                    BindGridEntity(Me.CmdWhere)
                End If
            ElseIf Me.AddEdit = "EDIT" Then
                ErrMessage = m_controller.AssetTypeDocumentListSaveEdit(customClass)
                If ErrMessage = "MainDoc" Then
                    ShowMessage(lblMessage, "Setiap Jenis Asset tidak boleh memiliki multiple main dokumen", True)
                    pnlAddEdit.Visible = True
                    pnlList.Visible = False
                    pnlHeader.Visible = True
                Else
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
                    txtSearch.Text = ""
                    Me.CmdWhere = "AssetTypeID = '" & Me.AssetTypeID.Trim & "'"
                    BindGridEntity(Me.CmdWhere)
                End If
            End If

            txtPage.Text = "1"
        End If
    End Sub

    Private Sub imbAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonAdd.Click
        If CheckFeature(Me.Loginid, Me.FormID, "ADD", Me.AppId) Then
            pnlAddEdit.Visible = True
            pnlList.Visible = False
            ButtonBack.Visible = False
            ButtonCancel.Visible = True
            ButtonSave.Visible = True
            lblRequiredDocListID.Visible = True
            pnlHeader.Visible = False
            Me.AddEdit = "ADD"
            lblTitleAddEdit.Text = Me.AddEdit

            lblDocumentListID.Visible = False
            txtDocumentListID.Visible = True
            lblName.Visible = False
            txtName.Visible = True
            lblValue.Visible = False
            rboValue.Visible = True
            lblMainDoc.Visible = False
            rboMainDoc.Visible = True
            lblUntukCustomer.Visible = False
            rboUntukCustomer.Visible = True
            lblNewAsset.Visible = False
            rboNewAsset.Visible = True
            lblUsedAsset.Visible = False
            rboUsedAsset.Visible = True

            txtDocumentListID.Text = ""
            txtName.Text = ""
            rboValue.SelectedIndex = 0
            rboMainDoc.SelectedIndex = 0
            rboNewAsset.SelectedIndex = 0
            rboUsedAsset.SelectedIndex = 0
            rboOrigination.SelectedIndex = 0
            rboNoDokumen.SelectedIndex = 1
        End If
    End Sub

    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub

    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        cboSearch.SelectedIndex = 0
        txtSearch.Text = ""
        txtPage.Text = "1"
        Me.CmdWhere = "AssetTypeID= '" & Me.AssetTypeID & "'"
        BindGridEntity(Me.CmdWhere)
    End Sub

    Private Sub imbSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonSearch.Click
        If txtSearch.Text.Trim <> "" Then
            Me.CmdWhere = "AssetTypeID = '" & Me.AssetTypeID.Trim & "' and " + cboSearch.SelectedItem.Value + " = '" + txtSearch.Text.Trim + "'"
        Else
            Me.CmdWhere = "AssetTypeID = '" & Me.AssetTypeID.Trim & "'"
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub

    Private Sub ButtonBackPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonBackPage.Click
        Response.Redirect("AssetType.aspx")
    End Sub

   
    Sub SendCookies()
        Dim cookie As HttpCookie = Request.Cookies("AssetType")
        If Not cookie Is Nothing Then
            cookie.Values("AssetTypeID") = Me.AssetTypeID
            cookie.Values("Description") = Me.DescriptionAssetType
            cookie.Values("PageFrom") = "DocumentList"
            cookie.Values("SearchBy") = cboSearch.SelectedItem.Value
            cookie.Values("SearchText") = txtSearch.Text
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie("AssetType")
            cookieNew.Values.Add("AssetTypeID", Me.AssetTypeID)
            cookieNew.Values.Add("Description", Me.DescriptionAssetType)
            cookieNew.Values.Add("PageFrom", "DocumentList")
            cookieNew.Values.Add("SearchBy", cboSearch.SelectedItem.Value)
            cookieNew.Values.Add("SearchText", txtSearch.Text)
            Response.AppendCookie(cookieNew)
        End If
    End Sub

    Private Sub ButtonCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        InitialDefaultPanel()
        pnlHeader.Visible = False
        txtSearch.Text = ""
        txtPage.Text = "1"
        Me.CmdWhere = "AssetTypeID= '" & Me.AssetTypeID & "'"
        BindGridEntity(Me.CmdWhere)
    End Sub

    Private Sub ButtonBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonBack.Click
        InitialDefaultPanel()
        lblDescription.Text = Me.DescriptionAssetType
    End Sub

End Class