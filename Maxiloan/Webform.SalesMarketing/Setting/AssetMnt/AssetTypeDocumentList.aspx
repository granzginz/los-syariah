﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AssetTypeDocumentList.aspx.vb"
    Inherits="Maxiloan.Webform.SalesMarketing.AssetTypeDocumentList" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>AssetTypeDocumentList</title>
    <script language="JavaScript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;
        function OpenWinAssetTypeView(pAssetTypeID) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.SalesMarketing/Setting/AssetMnt/AssetTypeView.aspx?AssetTypeID=' + pAssetTypeID, 'AssetType', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=1');
        }

        function fBack() {
            history.back(-1);
            return false;
        }
        function DeleteConfirm() {
            if (confirm("Apakah yakin mau hapus data ini?")) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../../Include/General.css" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                DAFTAR DOKUMEN ASSET
            </h3>
        </div>
    </div>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgPaging" runat="server" Width="100%" DataKeyField="AssetDocID"
                        BorderStyle="None" BorderWidth="0" OnSortCommand="Sorting" AutoGenerateColumns="False"
                        AllowSorting="True" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="EDIT">
                                <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" ImageUrl="../../../Images/iconedit.gif"
                                        CommandName="Edit"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DELETE">
                                <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbDelete" runat="server" CausesValidation="False" ImageUrl="../../../Images/iconDelete.gif"
                                        CommandName="Delete"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AssetDocID" HeaderText="ID DOKUMEN">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkAssetDocID" runat="server" CommandName="View" Text='<%# container.dataitem("AssetDocID")%>'
                                        CommandArgument='<%# container.dataitem("AssetDocID")%>' CausesValidation="false">
                                    </asp:LinkButton>
                                    <asp:TextBox ID="txtOrigination" Visible="false" runat="server" Text='<%# DataBinder.eval(Container,"DataItem.Origination")%>'></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="AssetDocName" SortExpression="AssetDocName" HeaderText="NAMA DOKUMEN">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="IsValueNeeded" SortExpression="IsValueNeeded" HeaderText="NILAI DIPERLUKAN">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="MandatoryForNewAsset" SortExpression="MandatoryForNewAsset"
                                HeaderText="MAND. ASSET BARU"></asp:BoundColumn>
                            <asp:BoundColumn DataField="MandatoryForUsedAsset" SortExpression="MandatoryForUsedAsset"
                                HeaderText="MAND. ASSET BEKAS"></asp:BoundColumn>
                            <asp:BoundColumn DataField="IsMainDoc" SortExpression="IsMainDoc" HeaderText="JENIS DOKUMEN">
                            </asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <div class="button_gridnavigation">
                        <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../../Images/grid_navbutton01.png"
                            CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../../Images/grid_navbutton02.png"
                            CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../../Images/grid_navbutton03.png"
                            CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../../Images/grid_navbutton04.png"
                            CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                        <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                            EnableViewState="False"></asp:Button>
                        <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" MinimumValue="1"
                            ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                            ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                    <div class="label_gridnavigation">
                        <asp:Label ID="lblPage" runat="server"></asp:Label>of
                        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                        <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                    </div>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonAdd" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue">
            </asp:Button>
          
            <asp:Button ID="ButtonBackPage" runat="server" CausesValidation="False" Text="Back"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlHeader" runat="server">
        <div class="form_box_title">
            <div class="form_single">
                <h4>
                    CARI DOKUMEN ASSET
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    ID Jenis Asset</label>
                <asp:HyperLink ID="hplAssetID" runat="server" Text='<%#request("id")%>'>
                </asp:HyperLink>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Keterangan</label>
                <asp:Label ID="lblDescription" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cari Berdasarkan</label>
                <asp:DropDownList ID="cboSearch" runat="server">
                    <asp:ListItem Value="AssetDocID">ID Dokumen</asp:ListItem>
                    <asp:ListItem Value="AssetDocName">Nama Dokumen</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearch" runat="server" MaxLength="100"></asp:TextBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonSearch" runat="server" CausesValidation="False" CssClass="small button blue"
                Text="Search"></asp:Button>
            <asp:Button ID="ButtonReset" runat="server" CausesValidation="False" Text="Reset"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlAddEdit" runat="server">
        <div class="form_box_header">
            <div class="form_single">
                <h4>
                    DOKUMEN ASSET -&nbsp;
                    <asp:Label ID="lblTitleAddEdit" runat="server"></asp:Label>
                </h4>
            </div>
        </div>
           <div class="form_box">
            <div class="form_single">
                <label>
                    ID Jenis Asset
                </label>
                <asp:Label ID="lblJenisAssetID" runat="server"></asp:Label>
            </div>
        </div>        
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    ID Dokumen
                </label>
                <asp:Label ID="lblDocumentListID" runat="server"></asp:Label>
                <asp:TextBox ID="txtDocumentListID" runat="server" MaxLength="10" Columns="13"></asp:TextBox>
                <asp:Label ID="lblRequiredDocListID" runat="server"></asp:Label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please fill in Document ID"
                    CssClass="validator_general" ControlToValidate="txtDocumentListID"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    Nama Dokumen
                </label>
                <asp:Label ID="lblName" runat="server"></asp:Label>
                <asp:TextBox ID="txtName" runat="server" Width="416px" MaxLength="50" Columns="53"></asp:TextBox>
                <asp:Label ID="lblRequiredDocName" runat="server"></asp:Label>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" ErrorMessage="Please fill in Document Name"
                    CssClass="validator_general" ControlToValidate="txtName"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_general">
                    Nilai diperlukan</label>
                <asp:Label ID="lblValue" runat="server"></asp:Label>
                <asp:RadioButtonList ID="rboValue" runat="server" class="opt_single" RepeatDirection="Horizontal">
                    <asp:ListItem Value="1" Selected="True">Yes</asp:ListItem>
                    <asp:ListItem Value="0">No</asp:ListItem>
                </asp:RadioButtonList>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_general">
                    Mandatory untuk Asset Baru</label>
                <asp:Label ID="lblNewAsset" runat="server"></asp:Label>
                <asp:RadioButtonList ID="rboNewAsset" runat="server" class="opt_single" RepeatDirection="Horizontal">
                    <asp:ListItem Value="1" Selected="True">Yes</asp:ListItem>
                    <asp:ListItem Value="0">No</asp:ListItem>
                </asp:RadioButtonList>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_general">
                    Mandatory untuk Asset Bekas</label>
                <asp:Label ID="lblUsedAsset" runat="server"></asp:Label>
                <asp:RadioButtonList ID="rboUsedAsset" runat="server" class="opt_single" RepeatDirection="Horizontal">
                    <asp:ListItem Value="1" Selected="True">Yes</asp:ListItem>
                    <asp:ListItem Value="0">No</asp:ListItem>
                </asp:RadioButtonList>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_general">
                    Jenis Dokumen</label>
                <asp:Label ID="lblMainDoc" runat="server"></asp:Label>
                <asp:RadioButtonList ID="rboMainDoc" runat="server" class="opt_single" RepeatDirection="Horizontal">
                    <asp:ListItem Value="1" Selected="True">Main</asp:ListItem>
                    <asp:ListItem Value="0">Additional</asp:ListItem>
                </asp:RadioButtonList>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_general">
                    Untuk Customer</label>
                <asp:Label ID="lblUntukCustomer" runat="server"></asp:Label>
                <asp:RadioButtonList ID="rboUntukCustomer" runat="server" class="opt_single" RepeatDirection="Horizontal">
                    <asp:ListItem Value="P">Personal</asp:ListItem>
                    <asp:ListItem Value="C">Corporate</asp:ListItem>
                    <asp:ListItem Value="A" Selected="True">All</asp:ListItem>
                </asp:RadioButtonList>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_general">
                    Origination
                </label>
                <asp:RadioButtonList ID="rboOrigination" runat="server" class="opt_single" RepeatDirection="Horizontal">
                    <asp:ListItem Value="CBU"> Complete Build Up </asp:ListItem>
                    <asp:ListItem Value="CKD"> Complete Knock Down </asp:ListItem>
                </asp:RadioButtonList>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_general">
                    No Dokumen diperlukan</label>
                <asp:Label ID="lblNoDokumen" runat="server"></asp:Label>
                <asp:RadioButtonList ID="rboNoDokumen" runat="server" class="opt_single" RepeatDirection="Horizontal">
                    <asp:ListItem Value="1">Yes</asp:ListItem>
                    <asp:ListItem Value="0" Selected="True">No</asp:ListItem>
                </asp:RadioButtonList>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonSave" runat="server" CausesValidation="true" Text="Save" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="ButtonCancel" runat="server" CausesValidation="false" Text="Cancel"
                CssClass="small button gray"></asp:Button>
            <asp:Button ID="ButtonBack" runat="server" CausesValidation="false" Text="Back" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
