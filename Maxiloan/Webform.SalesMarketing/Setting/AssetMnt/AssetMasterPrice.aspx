﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AssetMasterPrice.aspx.vb"
    Inherits="Maxiloan.Webform.SalesMarketing.AssetMasterPrice" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcLookupAsset" Src="../../../Webform.UserController/UcLookupAsset.ascx" %>
<%@ Register Src="../../../Webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat"
    TagPrefix="uc2" %>
<%@ Register Src="../../../Webform.UserController/ucYear.ascx" TagName="ucYear" TagPrefix="uc3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>CARI HARGA JUAL MOBIL BEKAS</title>
    <link rel="Stylesheet" href="../../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../../Include/Buttons.css" type="text/css" />
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
    <script language="JavaScript" type="text/javascript">
        function fBack() {
            history.back(-1);
            return false; 
        }
        function DeleteConfirm() {
            if (confirm("Apakah yakin mau hapus data ini ?")) {
                return true;
            }
            else {
                return false;
            }
        }
        function tes()
        { 
            var attachmentName = document.all.<%=FileUploadAssetCode.ClientID%>
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                CARI HARGA JUAL MOBIL BEKAS
            </h3>
        </div>
    </div>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgPaging" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="Sorting" DataKeyField="AssetCode" BorderStyle="None" BorderWidth="0"
                        CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="EDIT">
                                <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" ImageUrl="../../../Images/iconedit.gif"
                                        CommandName="Edit"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DELETE">
                                <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbDelete" runat="server" CausesValidation="False" ImageUrl="../../../Images/iconDelete.gif"
                                        CommandName="Delete"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="ASSETCODE" SortExpression="PICID">
                                <ItemTemplate>
                                    <a href='AssetMaster.aspx?cmd=dtlAssetMasterPrice&AssetCode=<%# DataBinder.eval(Container,"DataItem.AssetCode") 
                                    %>&desc=<%# DataBinder.eval(Container,"DataItem.AssetDescription") %>'>
                                        <asp:Label ID="lblAssetCode" runat="server" Text='<%# DataBinder.eval(Container,"DataItem.AssetCode") %>'>
                                        </asp:Label></a> </a>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="AssetDescription" SortExpression="AssetDescription" HeaderText="NAMA ASSET">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="ManufacturingYear" SortExpression="ManufacturingYear"
                                HeaderText="TAHUN">
                                <HeaderStyle CssClass="th_right" />
                                <ItemStyle CssClass="item_grid_right" />
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="Price" SortExpression="Price" HeaderText="HARGA OTR"
                                DataFormatString="{0:N0}">
                                <HeaderStyle CssClass="th_right" />
                                <ItemStyle CssClass="item_grid_right" />
                            </asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <div class="button_gridnavigation">
                        <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../../Images/grid_navbutton01.png"
                            CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../../Images/grid_navbutton02.png"
                            CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../../Images/grid_navbutton03.png"
                            CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../../Images/grid_navbutton04.png"
                            CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                        <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                            EnableViewState="False"></asp:Button>
                        <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" MinimumValue="1"
                            ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                            ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                    <div class="label_gridnavigation">
                        <asp:Label ID="lblPage" runat="server"></asp:Label>of
                        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                        <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                    </div>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonAdd" runat="server" Text="Add" CssClass="small button blue"
                CausesValidation="False"></asp:Button>
            <asp:Button ID="ButtonDownload" runat="server" Text="Download" CssClass="small button blue"
                CausesValidation="False"></asp:Button>
            <asp:Button ID="ButtonUpload" runat="server" Text="Upload" CssClass="small button blue"
                CausesValidation="False"></asp:Button>
          
        </div>
        <div class="form_box_title">
            <div class="form_single">
                <h4>
                    CARI ASSET MASTER PRICE
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <asp:DropDownList Visible="false" ID="cboAssetType" runat="server">
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Tahun Produksi</label>
                <asp:TextBox ID="txtCariTahunProduksi" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cari Berdasarkan</label>
                <asp:DropDownList ID="cboSearch" runat="server">
                    <asp:ListItem Value="AssetMasterPrice.AssetCode">ASSET CODE</asp:ListItem>
                    <asp:ListItem Value="AssetMaster.Description">NAMA ASSET</asp:ListItem>
                    <asp:ListItem Value="Price">HARGA OTR</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearch" runat="server" Width="20%" MaxLength="100"></asp:TextBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonSearch" runat="server" CausesValidation="False" Text="Search"
                CssClass="small button blue"></asp:Button>
            <asp:Button ID="ButtonReset" runat="server" CausesValidation="False" Text="Reset"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlAddEdit" runat="server">
        <div class="form_box_header">
            <div class="form_single">
                <h4>
                    <asp:Label ID="lblTitleAddEdit" runat="server"></asp:Label>
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Nama Cabang</label>
                <asp:Label ID="lblCabang" runat="server"></asp:Label>
                <asp:TextBox ID="txtCabang" runat="server" Enabled="false"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <label>
                            Asset Code</label>
                        <asp:TextBox runat="server" Enabled="false" ID="txtAssetName" 
                            CssClass="long_text"></asp:TextBox>
                        <asp:Button ID="btnLookupAsset" Text="…" runat="server" CausesValidation="False"
                            CssClass="small buttongo blue" />
                        <uc1:uclookupasset id="UcLookupAsset" runat="server" oncatselected="CatSelectedAsset" />
                        </uc1:UcLookupAsset>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Tahun Produksi</label>
                <asp:Label ID="lblTahunProduksi" runat="server"></asp:Label>
                <%--<asp:TextBox ID="txtTahunProduksi" runat="server"></asp:TextBox>--%>
                <uc3:ucYear ID="txtTahunProduksi" runat="server" />
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Harga OTR</label>
                <asp:Label ID="lblHargaOTR" runat="server"></asp:Label>
                <%--<asp:TextBox ID="txtHargaOTR" runat="server"></asp:TextBox>--%>
                <uc2:ucNumberFormat ID="txtHargaOTR" runat="server" />
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonSave" runat="server" CausesValidation="true" Text="Save" CssClass="small button blue">
            </asp:Button>&nbsp;
            <asp:Button ID="ButtonCancel" runat="server" CausesValidation="true" Text="Cancel"
                CssClass="small button gray"></asp:Button>&nbsp;
            <asp:Button ID="ButtonBack" runat="server" CausesValidation="true" Text="Back" CssClass="small button gray">
            </asp:Button>&nbsp;
            <asp:Button ID="ButtonClose" runat="server" CausesValidation="False" Text="Close"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="PnlDownload" runat="server">
        <div class="form_box_title">
            <div class="form_single">
                <h4>
                    DOWNLOAD DATA MASTER PRICE
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Nama Asset</label>
                <asp:DropDownList runat="server" ID="cboAssetCode">
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonDownloadAsset" runat="server" CausesValidation="true" Text="Download"
                CssClass="small button blue"></asp:Button>
            <asp:Button ID="ButtonDownloadCancel" runat="server" CausesValidation="true" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlUpload">
        <div class="form_box_title">
            <div class="form_single">
                <h4>
                    UPLOAD DATA MASTER PRICE
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    File Asset</label>
                <asp:FileUpload ID="FileUploadAssetCode" runat="server" />
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonUploadAsset" runat="server" CausesValidation="true" OnClientClick="tes();"
                Text="Upload" CssClass="small button blue"></asp:Button>
            <asp:Button ID="ButtonUploadCancel" runat="server" CausesValidation="true" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    <input id="hdnEditAssetCode" type="hidden" name="hdnEditAssetCode" runat="server" />
    <input id="hdnManufacturingYear" type="hidden" name="hdnManufacturingYear" runat="server" />
    </form>
</body>
</html>
