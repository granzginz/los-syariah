﻿#Region "Imports"
Imports Maxiloan.cbse
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
#End Region

Public Class AssetTypeOrigination
    Inherits Maxiloan.Webform.WebBased

    Private m_controller As New AssetTypeOriginationController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(viewstate("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property OriginationID() As String
        Get
            Return CType(viewstate("OriginationID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("OriginationID") = Value
        End Set
    End Property
    Private Property AddEdit() As String
        Get
            Return CType(viewstate("vwsAddEdt"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("vwsAddEdt") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(viewstate("Sort"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Sort") = Value
        End Set
    End Property
    Private Property AssetTypeID() As String
        Get
            Return CType(viewstate("AssetTypeID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AssetTypeID") = Value
        End Set
    End Property
    Private Property DescriptionAssetType() As String
        Get
            Return CType(viewstate("DescriptionAssetType"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("DescriptionAssetType") = Value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        If sessioninvalid() Then
            Exit Sub
        End If
        InitialDefaultPanel()
        Me.FormID = "AssetType"

        If Not Me.IsPostBack Then

            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If sessioninvalid() Then
                    Exit Sub
                End If
                Me.SearchBy = ""
                Me.SortBy = ""
            End If
            txtPage.Text = "1"
            Me.Sort = "OriginationID ASC"
            Me.AssetTypeID = Request("id")
            Me.DescriptionAssetType = Request("Description")
            Me.OriginationID = Request("OriginationID")

            'If Request("cmd") = "dtl" Then
            '    BindDetail(Me.AssetTypeID, Me.OriginationID)
            'End If
            hplAssetID.NavigateUrl = LinkTo(Me.AssetTypeID)

            If Request("cond") <> "" Then
                Me.CmdWhere = "AssetTypeID= '" & Me.AssetTypeID & "' " + Request("cond")
            Else
                Me.CmdWhere = "AssetTypeID= '" & Me.AssetTypeID & "'"

            End If
            BindGridEntity(Me.CmdWhere)
            lblJenisAssetID.Text = Me.AssetTypeID
            'ButtonBackPage.Attributes.Add("OnClick", "return fBack()")
            'ButtonCancel.Attributes.Add("OnClick", "return fBack()")
        End If
    End Sub

    Function LinkTo(ByVal strAssetTypeID As String) As String
        Return "javascript:OpenWinAssetTypeView('" & strAssetTypeID & "')"
    End Function

    Private Sub InitialDefaultPanel()
        pnlList.Visible = True
        pnlAddEdit.Visible = False
        pnlHeader.Visible = True
    End Sub

    Sub BindGridEntity(ByVal cmdWhere As String)
        Dim dtEntity As DataTable
        Dim oAssetTypeOrigination As New Parameter.AssetTypeOrigination

        With oAssetTypeOrigination
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = Me.Sort
            .strConnection = GetConnectionString
        End With
        oAssetTypeOrigination = m_controller.GetAssetTypeOrigination(oAssetTypeOrigination)

        If Not oAssetTypeOrigination Is Nothing Then
            dtEntity = oAssetTypeOrigination.ListData
            recordCount = oAssetTypeOrigination.TotalRecords
            Me.OriginationID = oAssetTypeOrigination.OriginationID
        Else
            recordCount = 0
        End If
     
        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()

        hplAssetID.Text = Me.AssetTypeID
        lblDescription.Text = Me.DescriptionAssetType
        PagingFooter()
    End Sub
    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        Dim imbDelete As ImageButton
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imbDelete"), ImageButton)
            imbDelete.Attributes.Add("Onclick", "return DeleteConfirm()")
        End If
    End Sub
    Sub BindDetail(ByVal ID As String, ByVal OriginationID As String)
        Dim oAssetTypeOrigination As New Parameter.AssetTypeOrigination
        Me.AddEdit = "VIEW"
        pnlAddEdit.Visible = True
        pnlList.Visible = False
        pnlHeader.Visible = True
        ButtonBackPage.Visible = True
        ButtonCancel.Visible = False
        ButtonSave.Visible = False

        lblTitleAddEdit.Text = Me.AddEdit
        oAssetTypeOrigination.Id = ID
        oAssetTypeOrigination.OriginationID = OriginationID
        oAssetTypeOrigination.strConnection = GetConnectionString
        oAssetTypeOrigination = m_controller.GetAssetTypeOriginationEdit(oAssetTypeOrigination)

        lblOriginationID.Visible = True
        txtOriginationID.Visible = False
        lblDescriptionAddEdit.Visible = True
        txtDescriptionAddEdit.Visible = False

        lblOriginationID.Text = OriginationID.Trim
        lblDescriptionAddEdit.Text = oAssetTypeOrigination.Description.Trim
    End Sub
#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int32)
                BindGridEntity(Me.CmdWhere)
            End If
        End If
    End Sub
#End Region

    Private Sub dtgEntity_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        Dim oAssetType As New Parameter.AssetTypeOrigination
        If e.CommandName = "Edit" Then
            If CheckFeature(Me.Loginid, Me.FormID, "EDIT", Me.AppId) Then
                Me.AddEdit = "EDIT"
                pnlAddEdit.Visible = True
                pnlList.Visible = False
                pnlHeader.Visible = False
                'ButtonBackPage.Visible = False
                ButtonCancel.Visible = True
                ButtonSave.Visible = True
                lblRequiredOriginationID.Visible = False

                lblOriginationID.Visible = True
                txtOriginationID.Visible = False
                lblDescriptionAddEdit.Visible = False
                txtDescriptionAddEdit.Visible = True

                lblTitleAddEdit.Text = Me.AddEdit
                oAssetType.Id = Me.AssetTypeID
                oAssetType.OriginationID = dtgPaging.DataKeys.Item(e.Item.ItemIndex).ToString
                oAssetType.strConnection = GetConnectionString
                oAssetType = m_controller.GetAssetTypeOriginationEdit(oAssetType)
                txtOriginationID.Text = dtgPaging.DataKeys.Item(e.Item.ItemIndex).ToString
                lblOriginationID.Text = dtgPaging.DataKeys.Item(e.Item.ItemIndex).ToString
                txtDescriptionAddEdit.Text = oAssetType.Description.Trim
            End If
        ElseIf e.CommandName = "Delete" Then
            If CheckFeature(Me.Loginid, Me.FormID, "DEL", Me.AppId) Then
                Dim customClass As New Parameter.AssetTypeOrigination
                With customClass
                    .Id = Me.AssetTypeID
                    .OriginationID = dtgPaging.DataKeys.Item(e.Item.ItemIndex).ToString
                    .strConnection = GetConnectionString
                End With
                Dim ResultOutput As String
                ResultOutput = m_controller.AssetTypeOriginationDelete(customClass)
                If ResultOutput = "OK" Then
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, False)
                Else
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_FAILED, True)
                End If
                Me.CmdWhere = "AssetTypeID= '" & Me.AssetTypeID & "'"
                txtSearch.Text = ""
                BindGridEntity(Me.CmdWhere)
            End If
        ElseIf e.CommandName = "View" Then
            If CheckFeature(Me.Loginid, Me.FormID, "VIEW", Me.AppId) Then
                Dim lnkOriginationID As LinkButton = CType(e.Item.FindControl("lnkOriginationID"), LinkButton)
                Dim strOriginationID As String = lnkOriginationID.Text.Trim
                BindDetail(hplAssetID.Text, strOriginationID)
                Me.DescriptionAssetType = lblDescription.Text
                lblRequiredOriginationID.Visible = False
            End If
        End If
    End Sub

    Private Sub ButtonSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
        If Page.IsValid Then
            Dim customClass As New Parameter.AssetTypeOrigination
            Dim ErrMessage As String = ""
            With customClass
                .Id = hplAssetID.Text
                .Description = txtDescriptionAddEdit.Text
                .OriginationID = txtOriginationID.Text
                .strConnection = GetConnectionString()
            End With

            If Me.AddEdit = "ADD" Then
                ErrMessage = m_controller.AssetTypeOriginationSaveAdd(customClass)
                If ErrMessage <> "" Then
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_FAILED, True)
                    pnlAddEdit.Visible = True
                    pnlList.Visible = False
                    pnlHeader.Visible = False
                    Exit Sub
                Else
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_SUCCESS, False)
                    txtSearch.Text = ""
                    Me.CmdWhere = "AssetTypeID = '" & Me.AssetTypeID.Trim & "'"
                    BindGridEntity(Me.CmdWhere)
                End If
            ElseIf Me.AddEdit = "EDIT" Then
                m_controller.AssetTypeOriginationSaveEdit(customClass)
                ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
                txtSearch.Text = ""
                Me.CmdWhere = "AssetTypeID = '" & Me.AssetTypeID.Trim & "'"
                BindGridEntity(Me.CmdWhere)
            End If
            txtPage.Text = "1"
        End If
    End Sub

    Private Sub imbAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonAdd.Click
        If CheckFeature(Me.Loginid, Me.FormID, "ADD", Me.AppId) Then
            pnlAddEdit.Visible = True
            pnlList.Visible = False
            pnlHeader.Visible = False
            'ButtonBackPage.Visible = False
            ButtonCancel.Visible = True
            ButtonSave.Visible = True
            lblRequiredOriginationID.Visible = True

            Me.AddEdit = "ADD"
            lblTitleAddEdit.Text = Me.AddEdit

            lblOriginationID.Visible = False
            txtOriginationID.Visible = True
            lblDescriptionAddEdit.Visible = False
            txtDescriptionAddEdit.Visible = True

            txtOriginationID.Text = ""
            txtDescriptionAddEdit.Text = ""
        End If
    End Sub

    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub

    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        cboSearch.SelectedIndex = 0
        txtSearch.Text = ""
        txtPage.Text = "1"
        Me.CmdWhere = "AssetTypeID= '" & Me.AssetTypeID & "'"
        BindGridEntity(Me.CmdWhere)
    End Sub

    Private Sub imbSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonSearch.Click
        If txtSearch.Text.Trim <> "" Then
            Me.CmdWhere = "AssetTypeID = '" & Me.AssetTypeID.Trim & "' and " + cboSearch.SelectedItem.Value + " = '" + txtSearch.Text.Trim + "'"
        Else
            Me.CmdWhere = "AssetTypeID = '" & Me.AssetTypeID.Trim & "'"
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub

    Private Sub ButtonBackPagePage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonBackPage.Click
        Response.Redirect("AssetType.aspx")
    End Sub

  
    Sub SendCookies()
        Dim cookie As HttpCookie = Request.Cookies("AssetType")
        If Not cookie Is Nothing Then
            cookie.Values("AssetTypeID") = Me.AssetTypeID
            cookie.Values("Description") = Me.DescriptionAssetType
            cookie.Values("PageFrom") = "Origination"
            cookie.Values("SearchBy") = cboSearch.SelectedItem.Value
            cookie.Values("SearchText") = txtSearch.Text
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie("AssetType")
            cookieNew.Values.Add("AssetTypeID", Me.AssetTypeID)
            cookieNew.Values.Add("Description", Me.DescriptionAssetType)
            cookieNew.Values.Add("PageFrom", "Origination")
            cookieNew.Values.Add("SearchBy", cboSearch.SelectedItem.Value)
            cookieNew.Values.Add("SearchText", txtSearch.Text)
            Response.AppendCookie(cookieNew)
        End If
    End Sub

    Private Sub ButtonCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        InitialDefaultPanel()
        txtSearch.Text = ""
        txtPage.Text = "1"
        Me.CmdWhere = "AssetTypeID= '" & Me.AssetTypeID & "'"
        BindGridEntity(Me.CmdWhere)
    End Sub


    Private Sub ButtonBackPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonBackPage.Click
        InitialDefaultPanel()
        lblDescription.Text = Me.DescriptionAssetType
    End Sub

End Class