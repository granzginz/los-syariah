﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AssetMaster.aspx.vb" Inherits="Maxiloan.Webform.SalesMarketing.AssetMaster" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>AssetMaster</title>
    <link rel="Stylesheet" href="../../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../../Include/Buttons.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        function fConfirm() {
            if (window.confirm("Apakah yakin mau hapus data ini ?"))
                return true;
            else
                return false;
        }
        function fback() {
            history.go(-1);
            return false;
        }
    </script>
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <asp:UpdateProgress ID="upg1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false" onclick="hideMessage();"></asp:Label>
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>
                        CARI MASTER ASSET
                    </h3>
                </div>
            </div>
            <asp:Panel ID="pnlList" runat="server">
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Cari Berdasarkan</label>
                        <asp:DropDownList ID="cboSearchBy" runat="server">
                            <asp:ListItem Value="Description" Selected="True">Nama Asset</asp:ListItem>
                            <asp:ListItem Value="AssetCode">KodeAsset</asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtSearchBy" runat="server" MaxLength="50"></asp:TextBox>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Jenis Asset
                        </label>
                        <asp:DropDownList ID="cboFilterBy" runat="server">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="cboFilterBy"
                            CssClass="validator_general" ErrorMessage="Harap pilih Jenis Asset" Display="Dynamic"
                            InitialValue="0"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonSearch" runat="server" Text="Search" CssClass="small button blue">
                    </asp:Button>&nbsp;
                    <asp:Button ID="ButtonReset" runat="server" Text="Reset" CssClass="small button gray"
                        CausesValidation="False"></asp:Button>
                </div>
                <asp:Panel ID="pnlGrid" runat="server" Visible="False">
                    <div class="form_box_title">
                        <div class="form_single">
                            <h4>
                                DAFTAR MASTER ASSET
                            </h4>
                        </div>
                    </div>
                    <div class="form_box_header">
                        <div class="form_single">
                            <div class="grid_wrapper_ns">
                                <asp:DataGrid ID="dtgAssetType" runat="server" Width="100%" BorderStyle="None" BorderWidth="0"
                                    AutoGenerateColumns="False" OnSortCommand="doSorting" DataKeyField="AssetCode"
                                    AllowSorting="True" CssClass="grid_general">
                                    <HeaderStyle CssClass="th" />
                                    <ItemStyle CssClass="item_grid" />
                                    <Columns>
                                        <asp:TemplateColumn HeaderText="EDIT">
                                            <ItemStyle CssClass="command_col"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:ImageButton ID="lnkEdit" runat="server" CommandName="edit" ImageUrl="../../../Images/IconEdit.gif"
                                                    CausesValidation="false"></asp:ImageButton>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="DELETE">
                                            <ItemStyle CssClass="command_col"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:ImageButton ID="lnkDelete" runat="server" CommandName="delete" ImageUrl="../../../Images/IconDelete.gif"
                                                    CausesValidation="false"></asp:ImageButton>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="ADD">
                                            <ItemStyle CssClass="command_col"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkAddDetail" runat="server" CommandName="add_detail" Text="Detail"
                                                    Enabled='<%# isEnabled(DataBinder.eval(Container,"DataItem.AssetLevel"),DataBinder.eval(Container,"DataItem.JumlahLevel")) %>'
                                                    CausesValidation="false">
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn SortExpression="assetcode" HeaderText="KODE ASSET">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkAssetCode" runat="server" CommandName="view" Text='<%# DataBinder.eval(Container,"DataItem.AssetCode")%>'
                                                    CausesValidation="false">
                                                </asp:LinkButton>
                                                <asp:TextBox ID="txtOrigination" Visible="false" runat="server" Text='<%# DataBinder.eval(Container,"DataItem.Origination")%>'></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:BoundColumn DataField="Description" SortExpression="Description" HeaderText="NAMA ASSET">
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="DescLevel" SortExpression="DescLevel" HeaderText="LEVEL ASSET">
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="AssetLevel" Visible="False"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="JumlahLevel" Visible="False"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="Origination" Visible="False"></asp:BoundColumn>
                                    </Columns>
                                </asp:DataGrid>
                                <div class="button_gridnavigation">
                                    <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../../Images/grid_navbutton01.png"
                                        CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                                    </asp:ImageButton>
                                    <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../../Images/grid_navbutton02.png"
                                        CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                                    </asp:ImageButton>
                                    <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../../Images/grid_navbutton03.png"
                                        CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                                    </asp:ImageButton>
                                    <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../../Images/grid_navbutton04.png"
                                        CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                                    </asp:ImageButton>
                                    <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                                    <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                                        EnableViewState="False"></asp:Button>
                                    <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" MinimumValue="1"
                                        ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"></asp:RangeValidator>
                                    <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                                        ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                                </div>
                                <div class="label_gridnavigation">
                                    <asp:Label ID="lblPage" runat="server"></asp:Label>of
                                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                                    <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form_button">
                        <asp:Button ID="ButtonAddNew" runat="server" Text="Add" CssClass="small button blue"
                            CausesValidation="False"></asp:Button>&nbsp;
                       
                    </div>
                </asp:Panel>
            </asp:Panel>
            <asp:Panel ID="pnlHeader" runat="server">
                <div class="form_box_header">
                    <div class="form_single">
                        <h4>
                            <asp:Label ID="lblEntryFormHeader" runat="server"></asp:Label>
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Jenis Asset</label>
                        <asp:Label ID="lblJenisAset" runat="server"></asp:Label>
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlView" Visible="false" runat="server">
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Kode Asset</label>
                        <asp:Label ID="lblViewAssetCode" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Nama Asset</label>
                        <asp:Label ID="lblViewDescription" runat="server"></asp:Label>
                    </div>
                </div>
                <asp:Panel ID="pnlViewAdd" runat="server" Visible="False">
                    <div class="form_box">
                        <div class="form_single">
                            <label>
                                Kategori Asset</label>
                            <asp:Label ID="lblViewAssetCategory" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="form_box" id="divLblResaleValue" runat="server">
                        <div class="form_single">
                            <label>
                                Nilai Jual Kembali (%)</label>
                            <asp:Label ID="lblResaleValue" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="form_box" id="divLblSilinder" runat="server">
                        <div class="form_single">
                            <label>
                                Isi Silinder(cc)</label>
                            <asp:Label ID="lblSilinder" runat="server"></asp:Label>
                        </div>
                    </div>
                     <div class="form_box" id="divLblTransmissionType" runat="server">
                        <div class="form_single">
                            <label>
                                Transmisi</label>
                            <asp:Label ID="lblTransmisi" runat="server"></asp:Label>
                        </div>
                    </div>
                      <div class="form_box" id="divLblFuelType" runat="server">
                        <div class="form_single">
                            <label>
                                Bahan Bakar</label>
                            <asp:Label ID="lblBahanBakar" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label>
                                Negara Pembuat</label>
                            <asp:Label ID="lblNegara" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="form_box" id="divLblJmlTempDuduk" runat="server">
                        <div class="form_single">
                            <label>
                                Jumlah tempat duduk</label>
                            <asp:Label ID="lblJmlDuduk" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="form_box" id="divLblCheckKaroseri" runat="server">
                        <div class="form_single">
                            <label>
                                Karoseri</label>
                            <asp:CheckBox ID="checkLblKaroseri" runat="server" />
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label class="label_general">
                                Origination
                            </label>
                            <asp:RadioButtonList ID="rbolblOrigination" runat="server" class="opt_single" RepeatDirection="Horizontal">
                                <asp:ListItem Value="CBU"> Complete Build Up </asp:ListItem>
                                <asp:ListItem Value="CKD" Selected="True"> Complete Knock Down </asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                    </div>
                </asp:Panel>
                <div class="form_button">
                    <asp:Button ID="ButtonClose" runat="server" Text="Close" CssClass="small button gray"
                        CausesValidation="false"></asp:Button>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlLevel12" Width="100%" runat="server">
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Kode Asset</label>
                        <asp:Label ID="lblAssetCodeLevel12" runat="server" Visible="false"></asp:Label>
                        <asp:TextBox ID="txtAssetCodeLevel12" runat="server" MaxLength="20" Visible="False"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvAssetCodeLevel12" runat="server" ErrorMessage="Harap isi Kode Asset"
                            CssClass="validator_general" ControlToValidate="txtAssetCodeLevel12" Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Nama Asset</label>
                        <asp:Label ID="lblDescriptionLevel12" runat="server"></asp:Label>
                        <%--<asp:TextBox ID="txtDescriptionLevel12" runat="server" CssClass="medium_text" MaxLength="50"></asp:TextBox>--%>
                        <asp:TextBox ID="txtDescriptionLevel12" runat="server" CssClass="medium_text" MaxLength="100"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvDescriptionLevel12" runat="server" ErrorMessage="Harap isi Nama Asset"
                            CssClass="validator_general" ControlToValidate="txtDescriptionLevel12" Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_general">
                            Origination
                        </label>
                        <asp:RadioButtonList ID="rboOriginationEdit" runat="server" class="opt_single" RepeatDirection="Horizontal">
                            <asp:ListItem Value="CBU"> Complete Build Up </asp:ListItem>
                            <asp:ListItem Value="CKD" Selected="True"> Complete Knock Down </asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonSaveLevel12" runat="server" Text="Save" CssClass="small button blue">
                    </asp:Button>&nbsp;&nbsp;
                    <asp:Button ID="ButtonCancelLevel12" runat="server" Text="Cancel" CssClass="small button gray"
                        CausesValidation="false"></asp:Button>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlLevel3" Width="100%" runat="server">
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Kode Asset</label>
                        <asp:Label ID="lblAssetCodeLevel3" runat="server" Visible="false"></asp:Label>
                        <asp:TextBox ID="txtAssetCodeLevel3" runat="server" MaxLength="20" Visible="False"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvAssetCodeLevel3" runat="server" Visible="True"
                            ErrorMessage="Harap isi Kode Asset" ControlToValidate="txtAssetCodeLevel3" CssClass="validator_general"
                            Display="Dynamic" Enabled="True"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Nama Asset</label>
                        <asp:Label ID="lblDescriptionLevel3" runat="server"></asp:Label>
                        <%--<asp:TextBox ID="txtDescriptionLevel3" runat="server" MaxLength="50"></asp:TextBox>--%>
                        <asp:TextBox ID="txtDescriptionLevel3" runat="server" MaxLength="100"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvDescriptionLevel3" runat="server" Visible="True"
                            ErrorMessage="Harap isi Nama Asset" ControlToValidate="txtDescriptionLevel3"
                            CssClass="validator_general" Display="Dynamic" Enabled="True"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Kategori Asset</label>
                        <asp:DropDownList ID="cboAssetCategoryLevel3" runat="server">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="rfvCategory3" runat="server" Visible="True" ErrorMessage="Harap pilih kategori Asset"
                            CssClass="validator_general" ControlToValidate="cboAssetCategoryLevel3" Display="Dynamic"
                            Enabled="True"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box" id="divResaleValue" runat="server"> 
                    <div class="form_single">
                        <label>
                            Nilai Jual Kembali (%)</label>
                        <asp:TextBox ID="txtResaleValue" runat="server" MaxLength="5"></asp:TextBox>
                        <asp:RangeValidator ID="rgValAmount" runat="server" ControlToValidate="txtResaleValue"
                            MinimumValue="0" MaximumValue="100" Type="Double" ErrorMessage="Maximum Nilai Jual kembali 100%"
                            Display="Dynamic" Visible="True" CssClass="validator_general"></asp:RangeValidator>
                        <asp:RegularExpressionValidator ID="rvfAmount" runat="server" ControlToValidate="txtResaleValue"
                            ErrorMessage="Harap isi Nilai Jual kembali dengan angka" Display="Dynamic" Visible="True"
                            CssClass="validator_general" ValidationExpression="^\d{1,}$|^\d{1,}[.{1}]\d{1,}$"
                            Enabled="True"></asp:RegularExpressionValidator>
                    </div>
                </div>
                <div class="form_box" id="divSilinder" runat="server">
                    <div class="form_single">
                        <label>
                            Isi Silinder(cc)</label>
                        <asp:TextBox ID="txtSilinder" runat="server" MaxLength="5"></asp:TextBox>
                    </div>
                </div>
                <div class="form_box" id="divTransmissionType" runat="server">
                    <div class="form_single">
                        <label>
                            Transmisi</label>
                        <asp:DropDownList ID="cboTransmissionType" runat="server">
                            <asp:ListItem Value="-" Selected="True">-</asp:ListItem>
                            <asp:ListItem Value="M">MANUAL</asp:ListItem>
                            <asp:ListItem Value="A">AUTOMATIC</asp:ListItem>
                        </asp:DropDownList>
                         
                    </div>
                </div>

                   <div class="form_box" id="divFuelType" runat="server">
                    <div class="form_single">
                        <label>
                            Bahan Bakar</label>
                        <asp:DropDownList ID="cboFuelType" runat="server">
                            <asp:ListItem Value="-" Selected="True">-</asp:ListItem>
                            <asp:ListItem Value="G">GASOLINE</asp:ListItem>
                            <asp:ListItem Value="S">SOLAR</asp:ListItem>
                        </asp:DropDownList>
                         
                    </div>
                </div>

                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Negara Pembuat</label>
                        <asp:DropDownList ID="cboNegara" runat="server">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form_box" id="divJmlTempDuduk" runat="server">
                    <div class="form_single">
                        <label>
                            Jumlah tempat duduk</label>
                        <asp:TextBox ID="txtJmlTempDuduk" runat="server" MaxLength="5"></asp:TextBox>
                    </div>
                </div>
                <div class="form_box" id="divCheckKaroseri" runat="server">
                    <div class="form_single">
                        <label>
                            Karoseri</label>
                        <asp:CheckBox ID="CheckKaroseri" runat="server" />
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_general">
                            Origination
                        </label>
                        <asp:RadioButtonList ID="rboOrigination" runat="server" class="opt_single" RepeatDirection="Horizontal">
                            <asp:ListItem Value="CBU"> Complete Build Up </asp:ListItem>
                            <asp:ListItem Value="CKD" Selected="True"> Complete Knock Down </asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonSaveLevel3" runat="server" Text="Save" CssClass="small button blue">
                    </asp:Button>&nbsp;
                    <asp:Button ID="ButtonCancelLevel3" runat="server" Text="Cancel" CssClass="small button gray"
                        CausesValidation="false"></asp:Button>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
