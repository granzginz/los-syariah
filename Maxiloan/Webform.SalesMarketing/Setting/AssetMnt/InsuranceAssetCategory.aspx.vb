﻿#Region " Imports "
Imports System.Data.SqlClient
Imports Maxiloan.cbse
Imports Maxiloan.Controller
#End Region


Public Class InsuranceAssetCategory
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents imbDelete As ImageButton
#Region " Private Const "
    Private m_controller As New InsuranceAssetCategoryController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region

#Region " Properties "
    Private Property Sort_By() As String
        Get
            Return CType(viewstate("Sort"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Sort") = Value
        End Set
    End Property
    Private Property AddEdit() As String
        Get
            Return CType(viewstate("AddEdit"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AddEdit") = Value
        End Set
    End Property

    Private Property CmdWhere() As String
        Get
            Return CType(viewstate("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("vwsCmdWhere") = Value.ToString
        End Set
    End Property
    Private Property InsRateCategoryID() As String
        Get
            Return CType(viewstate("InsRateCategoryID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("InsRateCategoryID") = Value.ToString
        End Set
    End Property
    Private Property Description() As String
        Get
            Return CType(viewstate("Description"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Description") = Value.ToString
        End Set
    End Property
    Private Property AssetType() As String
        Get
            Return CType(viewstate("AssetType"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AssetType") = Value.ToString
        End Set
    End Property
#End Region

#Region " PageLoad "
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If
        InitialDefaultPanel()
        Me.FormID = "INSRATECATEGORY"

        If Not Me.IsPostBack Then
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If SessionInvalid() Then
                    Exit Sub
                End If
                Me.SearchBy = ""
                Me.SortBy = ""
            End If
            txtPage.Text = "1"
            Me.Sort_By = "InsRateCategoryID ASC "
            If Request("cond") <> "" Then
                Me.CmdWhere = Request("cond")
            Else
                Me.CmdWhere = "ALL"
            End If
            BindGridEntity(Me.CmdWhere)
        End If
    End Sub
#End Region
#Region "Bind Asset Type"
    Private Sub doFilterBinding()
        Dim objcommand As New SqlCommand
        Dim objconnection As New SqlConnection(Getconnectionstring)
        Dim objread As SqlDataReader
        Try
            If objconnection.State = ConnectionState.Closed Then objconnection.Open()
            objcommand.CommandType = CommandType.StoredProcedure
            objcommand.CommandText = "spGetAssetType"
            objcommand.Connection = objconnection
            objread = objcommand.ExecuteReader

            With cboAssetType
                .DataSource = objread
                .DataTextField = "description"
                .DataValueField = "assettypeid"
                .DataBind()
                .Items.Insert(0, "Select One")
                .Items(0).Value = "0"
                .SelectedIndex = 0
            End With
            objread.Close()
        Catch exp As Exception            
            ShowMessage(lblMessage, exp.Message, True)
        Finally
            objcommand.Dispose()
            If objconnection.State = ConnectionState.Open Then objconnection.Close()
            objconnection.Dispose()
        End Try
    End Sub
#End Region
#Region " BindGridEntity "
    Sub BindGridEntity(ByVal cmdWhere As String)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim oInsuranceAssetCategory As New Parameter.InsuranceAssetCategory

        oInsuranceAssetCategory.WhereCond = Me.CmdWhere
        oInsuranceAssetCategory.CurrentPage = currentPage
        oInsuranceAssetCategory.SortBy = Me.Sort_By
        oInsuranceAssetCategory.strConnection = GetConnectionString
        oInsuranceAssetCategory.PageSize = pageSize
        oInsuranceAssetCategory = m_controller.GetInsuranceAssetCategory(oInsuranceAssetCategory)

        Try
            dtsEntity = oInsuranceAssetCategory.ListData
        Catch ex As Exception
        End Try

        dtvEntity = dtsEntity.DefaultView
        lblrecord.Text = CStr(oInsuranceAssetCategory.TotalRecords)
        recordCount = oInsuranceAssetCategory.TotalRecords

        'cek totalrecord untuk meng-enable/ disable tombol print
        

        dtgInsAssetCategory.DataSource = dtvEntity
        Try
            dtgInsAssetCategory.DataBind()
            dtgInsAssetCategory.CurrentPageIndex = 0
            dtgInsAssetCategory.DataBind()
        Catch e As Exception
        End Try

        PagingFooter()
    End Sub
#End Region

#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then            
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridEntity(ViewState("vwsCmdWhere").ToString)
    End Sub

    Private Sub imgbtnPageNumb_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int32)
                BindGridEntity(ViewState("vwsCmdWhere").ToString)
            End If
        End If
    End Sub

#End Region

#Region " dtgEntity_ItemCommand "
    Private Sub dtgEntity_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgInsAssetCategory.ItemCommand
        If e.CommandName = "Delete" Then
            If CheckFeature(Me.Loginid, Me.FormID, "DEL", Me.AppId) Then
                Dim imbDelete As ImageButton
                txtDescription.Text = ""
                pnlAddEdit.Visible = False

                Me.InsRateCategoryID = e.Item.Cells(2).Text.Trim
                Me.Description = e.Item.Cells(3).Text.Trim

                LInsAssetCategoryID.Visible = False
                LDescription.Visible = True

                Dim oInsuranceAssetCategory As New Parameter.InsuranceAssetCategory
                With oInsuranceAssetCategory
                    .strConnection = GetConnectionString
                    .InsRateCategoryID = Me.InsRateCategoryID
                    .Description = Me.Description
                End With

                Dim ResultOutput As String
                ResultOutput = m_controller.InsuranceAssetCategoryDelete(oInsuranceAssetCategory)
                If ResultOutput = "OK" Then                    
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, False)
                Else                    
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_FAILED, True)
                End If
                Me.CmdWhere = "ALL"
                txtSearch.Text = ""
                BindGridEntity(Me.CmdWhere)
            End If
        ElseIf e.CommandName = "Edit" Then
            If CheckFeature(Me.Loginid, Me.FormID, "EDIT", Me.AppId) Then
                Me.AddEdit = "EDIT"
                lblTitleAddEdit.Text = Me.AddEdit
                doFilterBinding()
                pnlAddEdit.Visible = True
                pnlList.Visible = False

                Dim oInsuranceAssetCategory As New Parameter.InsuranceAssetCategory
                Me.InsRateCategoryID = e.Item.Cells(2).Text.Trim
                Me.Description = e.Item.Cells(3).Text.Trim
                Me.AssetType = e.Item.Cells(4).Text.Trim



                lblTitleAddEdit.Text = Me.AddEdit

                oInsuranceAssetCategory.InsRateCategoryID = Me.InsRateCategoryID
                oInsuranceAssetCategory.strConnection = GetConnectionString

                lblInsAssetCategoryID.Visible = True
                txtInsAssetCategoryID.Visible = False
                lblInsAssetCategoryID.Text = Me.InsRateCategoryID
                txtInsAssetCategoryID.Text = Me.InsRateCategoryID
                txtDescription.Text = Me.Description
                cboAssetType.SelectedIndex = cboAssetType.Items.IndexOf(cboAssetType.Items.FindByText(Me.AssetType))
            End If
        End If
    End Sub
#End Region

#Region " imbSave_Click "
    Private Sub imbSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
        If Page.IsValid Then
            Dim oInsuranceAssetCategory As New Parameter.InsuranceAssetCategory
            Me.InsRateCategoryID = txtInsAssetCategoryID.Text.Trim
            Me.Description = txtDescription.Text

            With oInsuranceAssetCategory
                .InsRateCategoryID = Me.InsRateCategoryID
                .Description = Me.Description
                .AssetType = cboAssetType.SelectedItem.Value.Trim
                .strConnection = GetConnectionString()
            End With

            Dim ResultOutput As String
            If Me.AddEdit = "EDIT" Then
                ResultOutput = m_controller.InsuranceAssetCategoryUpdate(oInsuranceAssetCategory)
                'Return Value dari Update sebenarnya tidak dibutuhkan,
                'karena selalu boleh edit ,dan save dengan nilai yang sama
                'If ResultOutput = "OK" Then                
                ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)                
                'viewstate("vwsCmdWhere") = "ALL"
                BindGridEntity(Me.CmdWhere)
                'End If
            ElseIf Me.AddEdit = "ADD" Then
                ResultOutput = m_controller.InsuranceAssetCategoryAdd(oInsuranceAssetCategory)
                If ResultOutput = "OK" Then                    
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_SUCCESS, False)
                    ViewState("vwsCmdWhere") = "ALL"
                    BindGridEntity(ViewState("vwsCmdWhere").ToString)
                ElseIf ResultOutput = "KO" Then                    
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_FAILED, True)
                    pnlAddEdit.Visible = True
                    pnlList.Visible = False
                    Exit Sub
                End If
                BindGridEntity(Me.CmdWhere)
            End If
            txtSearch.Text = ""
        End If
    End Sub
#End Region

#Region " imbAdd_Click "
    Private Sub imbAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonAdd.Click
        If CheckFeature(Me.Loginid, Me.FormID, "ADD", Me.AppId) Then
            Me.AddEdit = "ADD"
            lblTitleAddEdit.Text = Me.AddEdit
            doFilterBinding()
            txtInsAssetCategoryID.Text = ""
            txtInsAssetCategoryID.Visible = True
            lblInsAssetCategoryID.Visible = False
            txtDescription.Text = ""
            LInsAssetCategoryID.Visible = True
            LDescription.Visible = True

            pnlAddEdit.Visible = True
            pnlList.Visible = False

            'pnlSearch.Visible = False
        End If
    End Sub
#End Region

#Region " InitialDefaultPanel "
    Private Sub InitialDefaultPanel()
        pnlAddEdit.Visible = False
        pnlList.Visible = True                
    End Sub
#End Region

#Region " Sorting "
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgInsAssetCategory.SortCommand
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.Sort_By = e.SortExpression
        Else
            Me.Sort_By = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub
#End Region

#Region " imbBack "
    Private Sub imbBack_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        InitialDefaultPanel()
    End Sub
#End Region

#Region " imbReset "
    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        cboSearch.SelectedIndex = 0
        txtSearch.Text = ""
        txtPage.Text = "1"
        Me.CmdWhere = "ALL"
        BindGridEntity(Me.CmdWhere)
    End Sub
#End Region

#Region " imbSearch "
    Private Sub imbSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonSearch.Click
        If txtSearch.Text.Trim <> "" Then
            Me.CmdWhere = cboSearch.SelectedItem.Value + " LIKE '%" + txtSearch.Text + "%'"
        Else
            Me.CmdWhere = "ALL"
        End If
        txtPage.Text = ""
        BindGridEntity(Me.CmdWhere)
    End Sub
#End Region

#Region " SendCookies "
    Sub SendCookies()
        Dim cookie As HttpCookie = Request.Cookies("InsuranceAssetCategory")
        If Not cookie Is Nothing Then
            cookie.Values("SearchBy") = cboSearch.SelectedItem.Value
            cookie.Values("SearchText") = txtSearch.Text
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie("InsuranceAssetCategory")
            cookieNew.Values.Add("SearchBy", cboSearch.SelectedItem.Value)
            cookieNew.Values.Add("SearchText", txtSearch.Text)
            Response.AppendCookie(cookieNew)
        End If
    End Sub
#End Region

#Region " dtgEntity_ItemDataBound "
    Private Sub dtgEntity_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgInsAssetCategory.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imbDelete"), ImageButton)
            imbDelete.Attributes.Add("Onclick", "return DeleteConfirm()")
        End If
    End Sub
#End Region

#Region " imbCancel "
    Private Sub imbCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        pnlAddEdit.Visible = False
        pnlList.Visible = True
        txtPage.Text = "1"
        Me.CmdWhere = "ALL"
        txtSearch.Text = ""
        BindGridEntity(Me.CmdWhere)
    End Sub
#End Region



End Class