﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AssetType.aspx.vb" Inherits="Maxiloan.Webform.SalesMarketing.AssetType" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>AssetType</title>
    <script language="JavaScript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;

        function OpenWinAssetTypeView(pAssetTypeID) {
            var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
            window.open('http://<%#Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.SalesMarketing/Setting/AssetMnt/AssetTypeView.aspx?AssetTypeID=' + pAssetTypeID, 'AssetType', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=1');
        }

        function fBack() {
            history.back(-1);
            return false;
        }
        function fClose() {
            window.close();
            return false;
        }
        function DeleteConfirm() {
            if (confirm("Apakah yakin mau hapus data ini?")) {
                return true;
            }
            else {
                return false;
            }
        }	
    </script>
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../../Include/Buttons.css" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
    <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible ="false" ></asp:Label>                 
            <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">                   
                <h3>
                    DAFTAR JENIS ASSET
                </h3>
            </div>
        </div>         
        <asp:Panel ID="pnlList" runat="server">
        <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgPaging" runat="server" Width="100%" CssClass="grid_general" DataKeyField="AssetTypeID"
                        BorderStyle="None" BorderWidth="0" OnSortCommand="Sorting" AutoGenerateColumns="False"
                        AllowSorting="True">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="EDIT">
                                <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" ImageUrl="../../../Images/iconedit.gif"
                                        CommandName="Edit"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DELETE">
                                <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbDelete" runat="server" CausesValidation="False" ImageUrl="../../../Images/iconDelete.gif"
                                        CommandName="Delete"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AssetTypeID" HeaderText="ID">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="hynAssetTypeID" runat="server" Text='<%#container.dataitem("AssetTypeID")%>'
                                        NavigateUrl='<%# LinkTo(container.dataITem("AssetTypeID")) %>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="Description" SortExpression="Description" HeaderText="KETERANGAN">
                            </asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="SKEMA">                                
                                <ItemTemplate>
                                    <a href='AssetTypeScheme.aspx?id=<%# DataBinder.eval(Container,"DataItem.AssetTypeID") %>&Description=<%# DataBinder.eval(Container,"DataItem.Description") %>'>
                                        <asp:Label ID="lblScheme" Text='SKEMA' runat="server"></asp:Label>
                                    </a>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="KATEGORI">                                
                                <ItemTemplate>
                                    <a href='AssetTypeCategory.aspx?id=<%# DataBinder.eval(Container,"DataItem.AssetTypeID") %>&Description=<%# DataBinder.eval(Container,"DataItem.Description") %>'>
                                        <asp:Label ID="lblCategory" Text='KATEGORI' runat="server"></asp:Label>
                                    </a>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="MANUFACTURE">                                
                                <ItemTemplate>
                                    <a href='AssetTypeOrigination.aspx?id=<%# DataBinder.eval(Container,"DataItem.AssetTypeID") %>&Description=<%# DataBinder.eval(Container,"DataItem.Description") %>'>
                                        <asp:Label ID="lblOrigination" Text='MANUFACTURE' runat="server"></asp:Label>
                                    </a>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="ATRIBUT">                                
                                <ItemTemplate>
                                    <a href='AssetTypeAttribute.aspx?id=<%# DataBinder.eval(Container,"DataItem.AssetTypeID") %>&Description=<%# DataBinder.eval(Container,"DataItem.Description") %>'>
                                        <asp:Label ID="lblAttribute" Text='ATRIBUT' runat="server"></asp:Label>
                                    </a>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DAFTAR DOKUMEN">                                
                                <ItemTemplate>
                                    <a href='AssetTypeDocumentList.aspx?id=<%# DataBinder.eval(Container,"DataItem.AssetTypeID") %>&Description=<%# DataBinder.eval(Container,"DataItem.Description") %>'>
                                        <asp:Label ID="lblDocumentList" Text='DAFTAR DOKUMEN' runat="server"></asp:Label>
                                    </a>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>                                
                <div class="button_gridnavigation">
	                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../../Images/grid_navbutton01.png"
		                CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../../Images/grid_navbutton02.png"
		                CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../../Images/grid_navbutton03.png"
		                CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../../Images/grid_navbutton04.png"
		                CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
	                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
		                EnableViewState="False"></asp:Button>
	                <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage"
		                MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer"
		                CssClass="validator_general"></asp:RangeValidator>
	                <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
		                ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>
                <div class="label_gridnavigation">
	                <asp:Label ID="lblPage" runat="server"></asp:Label>of
	                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
	                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                </div>
            </div>                       
        </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonAdd" runat="server" CausesValidation="False" Text="Add" CssClass ="small button blue">
            </asp:Button>
         
        </div>
        <div class="form_box_title">
            <div class="form_single">              
                <h4>
                    CARI JENIS ASSET
                </h4>
            </div>
        </div> 
        <div class="form_box">
	        <div class="form_single">
                <label>Cari Berdasarkan</label>
                <asp:DropDownList ID="cboSearch" runat="server">
                    <asp:ListItem Value="AssetTypeID">ID Jenis Asset</asp:ListItem>
                    <asp:ListItem Value="Description">Keterangan</asp:ListItem>
                </asp:DropDownList>                
                <asp:TextBox ID="txtSearch" runat="server" ></asp:TextBox>
	        </div>
        </div>         
        <div class="form_button">
            <asp:Button ID="ButtonSearch" runat="server" CausesValidation="False"  Text="Search" CssClass ="small button blue">
            </asp:Button>&nbsp;
            <asp:Button ID="ButtonReset" runat="server" CausesValidation="False"  Text="Reset" CssClass ="small button gray">
            </asp:Button>
        </div>        
        </asp:Panel>
        <asp:Panel ID="pnlAddEdit" runat="server">        
        <div class="form_box_header">
            <div class="form_single">              
                <h4>
                    JENIS ASSET&nbsp;-
                    <asp:Label ID="lblTitleAddEdit" runat="server"></asp:Label>
                </h4>
            </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                 <label class ="label_req">
				 ID JENIS ASSET <font color="red"></font></label>
                <asp:Label ID="lblAssetID" runat="server"></asp:Label>
                <asp:TextBox ID="txtAssetID" runat="server"  Columns="13" MaxLength="10"></asp:TextBox>
                <asp:Label ID="lblReqAssetTypeID" runat="server"></asp:Label>
                <asp:RequiredFieldValidator ID="lblRequiredAssetID" runat="server" Display="Dynamic" CssClass="validator_general"
                    ErrorMessage="Harap isi ID Jenis Asset" ControlToValidate="txtAssetID"></asp:RequiredFieldValidator>
	        </div>
        </div> 
        <div class="form_box">
	        <div class="form_single">
                 <label class ="label_req">
				Keterangan <font color="red"></font></label>
                <asp:Label ID="lblDescription" runat="server"></asp:Label>
                <asp:TextBox ID="txtDescription" runat="server" Width="344px" 
                    Columns="103" MaxLength="100"></asp:TextBox>
                <asp:Label ID="lblReqDesc" runat="server"></asp:Label>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" Display="Dynamic" CssClass="validator_general"
                    ErrorMessage="Harap isi Keterangan" ControlToValidate="txtDescription"></asp:RequiredFieldValidator>                
	        </div>
        </div> 
        <div class="form_box">
	        <div class="form_single">
                <label class ="label_req">
				Label Serial No 1<font color="red"></font></label>
                <asp:Label ID="lblSerial1" runat="server"></asp:Label>
                <asp:TextBox ID="txtSerial1" runat="server"  Columns="28" MaxLength="25" onkeypress="return OnlyHurufAndNumber(event);"></asp:TextBox>
                <asp:Label ID="lblReqSerial1" runat="server"></asp:Label>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator3" runat="server" Display="Dynamic" CssClass="validator_general"
                    ErrorMessage="Harap isi Label Serial No 1" ControlToValidate="txtSerial1"></asp:RequiredFieldValidator>
	        </div>
        </div>         
        <div class="form_box">
	        <div class="form_single">
                 <label class ="label_req">
				 Label Serial No 2<font color="red"></font></label>                
                <asp:Label ID="lblSerial2" runat="server"></asp:Label>
                <asp:TextBox ID="txtSerial2" runat="server"  Columns="28" MaxLength="25" onkeypress="return OnlyHurufAndNumber(event);"></asp:TextBox>
                <asp:Label ID="lblReqSerial2" runat="server"></asp:Label>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator4" runat="server" Display="Dynamic" CssClass="validator_general"
                    ErrorMessage="Harap isi Label Serial No 2" ControlToValidate="txtSerial2"></asp:RequiredFieldValidator>
	        </div>
        </div>         
        <div class="form_button">
            <asp:Button ID="ButtonSave" runat="server"  Text="Save" CssClass ="small button blue"
                CausesValidation="true"></asp:Button>&nbsp;
            <asp:Button ID="ButtonCancel" runat="server"  Text="Cancel" CssClass ="small button gray"
                CausesValidation="false"></asp:Button>
        </div>         
    </asp:Panel>
    </ContentTemplate>
    </asp:UpdatePanel>    
    </form>
</body>
</html>
