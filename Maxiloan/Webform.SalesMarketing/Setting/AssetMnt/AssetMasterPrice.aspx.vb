﻿Imports System.Linq
Imports System.Text.RegularExpressions
#Region "Imports"
Imports Maxiloan.Controller
Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.Webform.UserController
Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data.OleDb
Imports System.Data.Odbc
Imports System.Text
Imports System.Data.SqlClient
Imports System.IO
Imports System.Web.UI.WebControls.FileUpload
Imports Microsoft.Office.Interop

#End Region

Public Class AssetMasterPrice
    Inherits Maxiloan.Webform.WebBased


    Protected WithEvents UcLookupAsset As UcLookupAsset
    Protected WithEvents txtHargaOTR As ucNumberFormat    
    Protected WithEvents txtTahunProduksi As ucYear


    Private m_controller As New AssetMasterPriceController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property AddEdit() As String
        Get
            Return CType(ViewState("vwsAddEdt"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsAddEdt") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property
    Private Property AssetCode() As String
        Get
            Return CType(ViewState("AssetCode"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("AssetCode") = Value
        End Set
    End Property
    Private Property AssetDescription() As String
        Get
            Return CType(ViewState("AssetDescription"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("AssetDescription") = Value
        End Set
    End Property
    Private Property AssetTypeID() As String
        Get
            Return CType(ViewState("AssetTypeID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("AssetTypeID") = Value
        End Set
    End Property    
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If        
        If Not Me.IsPostBack Then
            Me.FormID = "AssetMasterPrice"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppID) Then
                txtPage.Text = "1"
                AssetTypeID = Request("AssetTypeID")
                Me.Sort = "AssetCode ASC"
                Me.CmdWhere = "BranchID = '" & Replace(Me.sesBranchId, "'", "") & "'"
                BindGridEntity(Me.CmdWhere)
                BindCombo()
                pnlAddEdit.Visible = False
                pnlList.Visible = True
                PnlDownload.Visible = False
                pnlUpload.Visible = False

                'txtHargaOTR.RequiredFieldValidatorEnable = True


            End If
        End If

    End Sub
    Private Sub InitialDefaultPanel()
        pnlList.Visible = True
        pnlAddEdit.Visible = False
    End Sub
    Sub BindGridEntity(ByVal cmdWhere As String)
        Dim dtEntity As System.Data.DataTable = Nothing
        Dim oAssetMasterPrice As New Parameter.AssetMasterPrice
        InitialDefaultPanel()
        oAssetMasterPrice.strConnection = GetConnectionString()
        oAssetMasterPrice.WhereCond = cmdWhere
        oAssetMasterPrice.CurrentPage = currentPage
        oAssetMasterPrice.PageSize = pageSize
        oAssetMasterPrice.SortBy = Me.Sort
        oAssetMasterPrice = m_controller.GetAssetMasterPrice(oAssetMasterPrice)

        If Not oAssetMasterPrice Is Nothing Then
            dtEntity = oAssetMasterPrice.Listdata
            recordCount = oAssetMasterPrice.Totalrecords
        Else
            recordCount = 0
        End If
       
        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()
        PagingFooter()
    End Sub
    Sub BindCombo()
        Dim oAssetMasterPrice As New Parameter.AssetMasterPrice
        Dim tblAssetType As New System.Data.DataTable
        oAssetMasterPrice.strConnection = GetConnectionString()
        tblAssetType = m_controller.GetAssetTypeCombo(oAssetMasterPrice)
        cboAssetType.DataTextField = "description"
        cboAssetType.DataValueField = "assettypeid"
        cboAssetType.DataSource = tblAssetType
        cboAssetType.DataBind()
    End Sub
    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        Dim imbDelete As ImageButton
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imbDelete"), ImageButton)
            imbDelete.Attributes.Add("Onclick", "return DeleteConfirm()")

        End If
    End Sub
#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub imgbtnPageNumb_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int32)
                BindGridEntity(Me.CmdWhere)
            End If
        End If
    End Sub
#End Region
    Private Sub dtgEntity_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        Dim err As String
        Dim oAssetMasterPrice As New Parameter.AssetMasterPrice
        If e.CommandName = "Edit" Then
            If CheckFeature(Me.Loginid, Me.FormID, "Edit", Me.AppID) Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            Me.AddEdit = "EDIT"
            pnlAddEdit.Visible = True
            pnlList.Visible = False
            PnlDownload.Visible = False
            pnlUpload.Visible = False
            ButtonBack.Visible = False
            ButtonCancel.Visible = True
            ButtonSave.Visible = True
            ButtonClose.Visible = False
            ButtonCancel.CausesValidation = False

            lblTitleAddEdit.Text = Me.AddEdit
            oAssetMasterPrice.AssetCode = dtgPaging.DataKeys.Item(e.Item.ItemIndex).ToString
            oAssetMasterPrice.BranchId = Me.sesBranchId.Replace("'", "").Trim
            oAssetMasterPrice.ManufacturingYear = dtgPaging.Items(e.Item.ItemIndex).Cells.Item(4).Text
            oAssetMasterPrice.strConnection = GetConnectionString()
            oAssetMasterPrice = m_controller.GetAssetMasterPriceList(oAssetMasterPrice)

            txtCabang.Text = Me.BranchName
            txtAssetName.Text = oAssetMasterPrice.AssetDescription
            hdnEditAssetCode.Value = oAssetMasterPrice.AssetCode
            txtTahunProduksi.Text = oAssetMasterPrice.ManufacturingYear
            hdnManufacturingYear.Value = oAssetMasterPrice.ManufacturingYear
            txtHargaOTR.Text = FormatNumber(oAssetMasterPrice.Price, 0)


        ElseIf e.CommandName = "Delete" Then
            If CheckFeature(Me.Loginid, Me.FormID, "Del", Me.AppID) Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            Dim customClass As New Parameter.AssetMasterPrice
            With customClass
                .AssetCode = dtgPaging.DataKeys.Item(e.Item.ItemIndex).ToString
                .BranchId = Me.sesBranchId.Replace("'", "").Trim
                .ManufacturingYear = dtgPaging.Items(e.Item.ItemIndex).Cells.Item(4).Text
                .strConnection = GetConnectionString()
            End With
            err = m_controller.AssetMasterPriceDelete(customClass)
            If err <> "" Then
                ShowMessage(lblMessage, err, True)
            Else
                ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, False)
            End If
            BindGridEntity(Me.CmdWhere)
            txtPage.Text = "1"
        End If
    End Sub
    Private Sub imbSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
        Dim customClass As New Parameter.AssetMasterPrice
        Dim ErrMessage As String = ""


        With customClass            
            .ManufacturingYear = hdnManufacturingYear.Value
            .EditAssetCode = Me.AssetCode
            .EditManufacturingYear = txtTahunProduksi.Text.Trim
            .Price = txtHargaOTR.Text.Trim
            .BranchId = Me.sesBranchId.Replace("'", "").Trim
            .LoginId = Me.Loginid
            .BusinessDate = Me.BusinessDate
            .strConnection = GetConnectionString()
        End With



        If Me.AddEdit = "ADD" Then
            ErrMessage = m_controller.AssetMasterPriceSaveAdd(customClass)
            If ErrMessage <> "" Then
                ShowMessage(lblMessage, ErrMessage, True)
                Exit Sub
            Else
                ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_SUCCESS, False)
                BindGridEntity(Me.CmdWhere)
            End If
        ElseIf Me.AddEdit = "EDIT" Then
            customClass.AssetCode = hdnEditAssetCode.Value.Trim
            customClass.EditAssetCode = IIf(IsNothing(Me.AssetCode), hdnEditAssetCode.Value.Trim, Me.AssetCode)
            customClass.BranchId = Me.sesBranchId.Replace("'", "").Trim
            customClass.ManufacturingYear = hdnManufacturingYear.Value.Trim
            m_controller.AssetMasterPriceSaveEdit(customClass)
            ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
            BindGridEntity(Me.CmdWhere)
        End If
    End Sub
    Private Sub imbAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonAdd.Click
        If CheckFeature(Me.Loginid, Me.FormID, "Add", Me.AppID) Then
            If SessionInvalid() Then
                Exit Sub
            End If
        End If
        pnlAddEdit.Visible = True
        pnlList.Visible = False
        PnlDownload.Visible = False
        pnlUpload.Visible = False
        ButtonBack.Visible = False
        ButtonCancel.Visible = True
        ButtonSave.Visible = True
        ButtonClose.Visible = False
        ButtonCancel.CausesValidation = False
        Me.AddEdit = "ADD"
        lblTitleAddEdit.Text = Me.AddEdit
        txtCabang.Text = Me.BranchName
        txtAssetName.Text = Nothing
        txtTahunProduksi.Text = Nothing
        txtHargaOTR.Text = "0"
    End Sub
    Private Sub ButtonUpload_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonUpload.Click
        pnlAddEdit.Visible = False
        pnlList.Visible = False
        PnlDownload.Visible = False
        pnlUpload.Visible = True
    End Sub
    Private Sub ButtonDownload_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonDownload.Click
        Dim oAssetMasterPrice As New Parameter.AssetMasterPrice
        Dim tblAssetCode As New System.Data.DataTable
        oAssetMasterPrice.strConnection = GetConnectionString()
        tblAssetCode = m_controller.GetAssetCodeCombo(oAssetMasterPrice)
        cboAssetCode.DataTextField = "Description"
        cboAssetCode.DataValueField = "AssetCode"
        cboAssetCode.DataSource = tblAssetCode
        cboAssetCode.DataBind()
        cboAssetCode.Items.Insert(0, "Select One")
        cboAssetCode.Items(0).Value = ""

        pnlAddEdit.Visible = False
        pnlList.Visible = False
        PnlDownload.Visible = True
        pnlUpload.Visible = False
    End Sub
    Private Sub ButtonDownloadAsset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonDownloadAsset.Click
        Dim oAssetMasterPrice As New Parameter.AssetMasterPrice
        Dim data As New System.Data.DataTable
        Dim strFileName As String


        If cboAssetCode.SelectedValue.Trim <> "" Then
            oAssetMasterPrice.strConnection = GetConnectionString()
            oAssetMasterPrice.AssetCode = "%" + cboAssetCode.SelectedValue.Trim + "%"
            oAssetMasterPrice.BranchId = sesBranchId.Replace("'", "").Trim
            oAssetMasterPrice = m_controller.GetAllAssetCode(oAssetMasterPrice)
            data = oAssetMasterPrice.Listdata

            Try

                strFileName = Request.ServerVariables("APPL_PHYSICAL_PATH") & "xml\"
                strFileName += "AssetCode" + sesBranchId.Replace("'", "") + "" + Me.UserID.Trim + ".xls"

                If data.Rows.Count > 0 Then
                    'File.Delete(strFileName)
                    'Dim _excel As New Excel.Application
                    'Dim wBook As Excel.Workbook
                    'Dim wSheet As Excel.Worksheet

                    'wBook = _excel.Workbooks.Add()
                    'wSheet = wBook.ActiveSheet()

                    'Dim missing As Object = Type.Missing
                    'Dim dt As System.Data.DataTable = data
                    'Dim dc As System.Data.DataColumn
                    'Dim dr As System.Data.DataRow
                    'Dim colIndex As Integer = 0
                    'Dim rowIndex As Integer = 0

                    'For Each dc In dt.Columns
                    '    colIndex = colIndex + 1
                    '    wSheet.Cells(1, colIndex) = dc.ColumnName
                    'Next

                    'For Each dr In dt.Rows
                    '    rowIndex = rowIndex + 1
                    '    colIndex = 0
                    '    For Each dc In dt.Columns
                    '        colIndex = colIndex + 1
                    '        wSheet.Cells(rowIndex + 1, colIndex) = dr(dc.ColumnName)
                    '    Next
                    'Next
                    'wSheet.Columns.AutoFit()
                    'wBook.SaveCopyAs(strFileName)

                    'ReleaseObject(wSheet)
                    'wBook.Close(False)
                    'ReleaseObject(wBook)
                    '_excel.Quit()
                    'ReleaseObject(_excel)
                    'GC.Collect()

                    'Response.ContentType = "application/vnd.ms-excel"
                    'Response.AppendHeader("Content-Disposition", "attachment; filename=AssetCode" + sesBranchId.Replace("'", "") + "" + Me.UserID.Trim + ".xls")                    
                    'Response.TransmitFile(strFileName)
                    'Response.End()


                    Dim objReport As AssetMasterPriceDownload = New AssetMasterPriceDownload
                    Dim oData As New DataSet
                    Dim strHTTPServer As String
                    Dim StrHTTPApp As String
                    Dim strNameServer As String
                    Dim strFileLocation As String

                    oData.Tables.Add(data.Copy)
                    objReport.SetDataSource(oData)
                    Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions

                    objReport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
                    objReport.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.ExcelRecord
                    DiskOpts.DiskFileName = strFileName
                    objReport.ExportOptions.DestinationOptions = DiskOpts
                    objReport.Export()
                    objReport.Close()

                    strHTTPServer = Request.ServerVariables("PATH_INFO")
                    strNameServer = Request.ServerVariables("SERVER_NAME")
                    StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                    strFileLocation = "../../../XML/" & "AssetCode" + sesBranchId.Replace("'", "") + "" + Me.UserID.Trim + ".xls"

                    Response.Write("<script language = javascript>" & vbCrLf _
                    & "var x = screen.width; " & vbCrLf _
                    & "var y = screen.heigth; " & vbCrLf _
                    & "window.open('" & strFileLocation & "','Installment', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes') " & vbCrLf _
                    & "</script>")

                Else
                    ShowMessage(lblMessage, "Data download is empty", True)
                End If
            Catch ex As Exception
                ShowMessage(lblMessage, ex.Message, True)
            End Try

        Else
            ShowMessage(lblMessage, "Selected Combo", True)
        End If

    End Sub

    Private Sub ReleaseObject(ByVal o As Object)
        Try
            While (System.Runtime.InteropServices.Marshal.ReleaseComObject(o) > 0)
            End While
        Catch
        Finally
            o = Nothing
        End Try
    End Sub

    Private Sub ButtonDownloadCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonDownloadCancel.Click
        Dim strFileName As String = Context.Request.MapPath("~\\xml\\") + "AssetCode" + sesBranchId.Replace("'", "") + "" + Me.UserID.Trim + ".xls"
        If File.Exists(strFileName) Then
            File.Delete(strFileName)
        End If
        Response.Redirect("AssetMasterPrice.aspx")
    End Sub
    Private Sub ButtonUploadAsset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonUploadAsset.Click
        Dim customClass As New Parameter.AssetMasterPrice
        Dim listAssetMasterPrice As New List(Of Parameter.AssetMasterPrice)
        Dim strFileName As String = Context.Request.MapPath("~\\xml\\") + "AssetCode" + sesBranchId.Replace("'", "") + "" + Me.UserID.Trim + ".xls"
        Dim sb As New StringBuilder

        If FileUploadAssetCode.HasFile Then
            If File.Exists(strFileName) Then
                File.Delete(strFileName)
            End If
            Try
                FileUploadAssetCode.SaveAs(strFileName)
                Dim reader = New StreamReader(FileUploadAssetCode.FileContent)
                listAssetMasterPrice = UploadAssetCode(reader)


                If listAssetMasterPrice.Count > 0 Then

                    For index = 0 To listAssetMasterPrice.Count - 1
                        customClass = New Parameter.AssetMasterPrice
                        Dim oAssetMasterPrice As New Parameter.AssetMasterPrice

                        oAssetMasterPrice.AssetCode = listAssetMasterPrice(index).AssetCode.Trim
                        oAssetMasterPrice.BranchId = listAssetMasterPrice(index).BranchId.Trim
                        oAssetMasterPrice.ManufacturingYear = listAssetMasterPrice(index).ManufacturingYear.Trim
                        oAssetMasterPrice.strConnection = GetConnectionString()
                        oAssetMasterPrice = m_controller.GetAssetMasterPriceList(oAssetMasterPrice)

                        If oAssetMasterPrice.AssetCode <> Nothing Or oAssetMasterPrice.AssetCode <> "" Then
                            With customClass
                                .AssetCode = oAssetMasterPrice.AssetCode.Trim
                                .ManufacturingYear = oAssetMasterPrice.ManufacturingYear.Trim
                                .EditAssetCode = listAssetMasterPrice(index).AssetCode.Trim
                                .EditManufacturingYear = listAssetMasterPrice(index).ManufacturingYear.Trim
                                .Price = listAssetMasterPrice(index).Price
                                .BranchId = oAssetMasterPrice.BranchId.Trim
                                .LoginId = Me.Loginid
                                .BusinessDate = Me.BusinessDate
                                .strConnection = GetConnectionString()
                            End With

                            m_controller.AssetMasterPriceSaveEdit(customClass)
                        Else
                            oAssetMasterPrice = New Parameter.AssetMasterPrice
                            oAssetMasterPrice.AssetCode = listAssetMasterPrice(index).AssetCode.Trim
                            oAssetMasterPrice.strConnection = GetConnectionString()
                            oAssetMasterPrice = m_controller.GetAssetMasterByAssetCode(oAssetMasterPrice)

                            If oAssetMasterPrice.Listdata.Rows.Count > 0 Then
                                With customClass
                                    .EditAssetCode = listAssetMasterPrice(index).AssetCode.Trim
                                    .EditManufacturingYear = listAssetMasterPrice(index).ManufacturingYear.Trim
                                    .Price = listAssetMasterPrice(index).Price
                                    .BranchId = Me.sesBranchId.Replace("'", "").Trim
                                    .LoginId = Me.Loginid
                                    .BusinessDate = Me.BusinessDate
                                    .strConnection = GetConnectionString()
                                End With

                                m_controller.AssetMasterPriceSaveAdd(customClass)
                                sb.Append(" (AddNew " + listAssetMasterPrice(index).AssetCode.Trim + "_" + listAssetMasterPrice(index).ManufacturingYear.Trim + ") ")
                            Else
                                sb.Append(" (Not Valid " + listAssetMasterPrice(index).AssetCode + ") ")
                            End If

                        End If
                    Next

                    ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS + " " + sb.ToString + " ", False)
                Else
                    ShowMessage(lblMessage, "Data Upload kosong", True)
                End If
            Catch ex As Exception
                ShowMessage(lblMessage, ex.Message, True)
            End Try
        Else
            ShowMessage(lblMessage, "File belum ada", True)
        End If

    End Sub
    Private Sub ButtonUploadCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonUploadCancel.Click
        Dim strFileName As String = Context.Request.MapPath("~\\xml\\") + "AssetCode" + sesBranchId.Replace("'", "") + "" + Me.UserID.Trim + ".xls"
        If File.Exists(strFileName) Then
            File.Delete(strFileName)
        End If
        Response.Redirect("AssetMasterPrice.aspx")
    End Sub
    Function GenerateHeaderData() As StringBuilder
        Dim tabDelimiter As String = "	"
        Dim oneRowData As New StringBuilder()
        oneRowData.Append("BranchID")
        oneRowData.Append(tabDelimiter)
        oneRowData.Append("AssetCode")
        oneRowData.Append(tabDelimiter)
        oneRowData.Append("AssetDescription")
        oneRowData.Append(tabDelimiter)
        oneRowData.Append("ManufacturingYear")
        oneRowData.Append(tabDelimiter)
        oneRowData.Append("Price")
        oneRowData.Append(tabDelimiter)

        Return oneRowData
    End Function
    Function GenerateStringBuilder(ByVal row As DataRow) As StringBuilder
        Dim tabDelimiter As String = "	"
        Dim oneRowData As New StringBuilder()
        oneRowData.Append(row.Item("BranchID").ToString.Trim)
        oneRowData.Append(tabDelimiter)
        oneRowData.Append(row.Item("AssetCode").ToString.Trim)
        oneRowData.Append(tabDelimiter)
        oneRowData.Append(row.Item("AssetDescription").ToString.Trim)
        oneRowData.Append(tabDelimiter)
        oneRowData.Append(row.Item("ManufacturingYear").ToString.Trim)
        oneRowData.Append(tabDelimiter)
        oneRowData.Append(row.Item("Price").ToString.Trim)
        oneRowData.Append(tabDelimiter)

        Return oneRowData
    End Function

    Function UploadAssetCode(reader As StreamReader) As List(Of Parameter.AssetMasterPrice)
        Dim listAssetMasterPrice As New List(Of Parameter.AssetMasterPrice)
        Try
            While Not reader.EndOfStream

                Dim originalLine = reader.ReadLine()
                Dim delim = ";"
                Dim oriSplit = Split(originalLine, delim)

                If (oriSplit.Length <> 4) Then
                    Throw New Exception("CSV File Format Tidak Sesuai")
                End If

                If (oriSplit(0).ToString().ToUpper = "BRANCHID") Then Continue While

                Dim assetMasterPrice = New Parameter.AssetMasterPrice
                With assetMasterPrice
                    .BranchId = oriSplit(0).ToString().Trim
                    .AssetCode = oriSplit(1).ToString().Trim
                    .AssetDescription = oriSplit(2).ToString().Trim
                    .ManufacturingYear = CInt(oriSplit(3))
                    .Price = CDec(oriSplit(4))
                End With

                listAssetMasterPrice.Add(assetMasterPrice)
         
            End While

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

        Return listAssetMasterPrice

    End Function
    Function UploadAssetCode(ByVal strFileName As String) As List(Of Parameter.AssetMasterPrice)
        Dim _connectionstring As New System.Data.OleDb.OleDbConnection
        Dim DS As New System.Data.DataSet
        Dim MyCommand As New System.Data.OleDb.OleDbDataAdapter
        Dim listAssetMasterPrice As New List(Of Parameter.AssetMasterPrice)
        Dim strcon As String

        Dim _con_Odbc As New System.Data.Odbc.OdbcConnection
        Dim _com_Odbc As New System.Data.Odbc.OdbcDataAdapter

        Try

        
        strcon = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" & strFileName & ";Extended Properties=""Excel 8.0;HDR=YES;IMEX=1;"""

        _connectionstring = New System.Data.OleDb.OleDbConnection(strcon)
        _connectionstring.Open()

        MyCommand = New System.Data.OleDb.OleDbDataAdapter( _
                    "select * from [Sheet1$]", _connectionstring)

        DS = New System.Data.DataSet()
        MyCommand.Fill(DS)


        If DS.Tables.Count > 0 Then
            Dim data As New DataTable
            data = DS.Tables(0)
            For index = 0 To data.Rows.Count - 1
                Dim assetMasterPrice As New Parameter.AssetMasterPrice
                assetMasterPrice.BranchId = CStr(data.Rows(index).Item(0))
                assetMasterPrice.AssetCode = CStr(data.Rows(index).Item(1))
                assetMasterPrice.AssetDescription = CStr(data.Rows(index).Item(2))
                assetMasterPrice.ManufacturingYear = CInt(data.Rows(index).Item(3))
                assetMasterPrice.Price = CDec(data.Rows(index).Item(4))

                listAssetMasterPrice.Add(assetMasterPrice)
            Next

        End If
        _connectionstring.Close()

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

        Return listAssetMasterPrice
    End Function
  
    Sub SendCookies()
        Dim cookie As HttpCookie = Request.Cookies("AssetMasterPrice")
        If Not cookie Is Nothing Then
            cookie.Values("where") = Me.CmdWhere
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie("AssetMasterPrice")
            cookieNew.Values.Add("where", Me.CmdWhere)
            Response.AppendCookie(cookieNew)
        End If
    End Sub
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        cboSearch.SelectedIndex = 0
        txtSearch.Text = ""
        txtCariTahunProduksi.Text = ""
        Me.CmdWhere = "BranchID = '" & Replace(Me.sesBranchId, "'", "") & "'"
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub imbSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonSearch.Click
        Me.CmdWhere = "BranchID = '" & Replace(Me.sesBranchId, "'", "") & "'"

        If txtCariTahunProduksi.Text <> "" Then
            Dim tmpCThnProd As String = txtCariTahunProduksi.Text.Replace("%", "")
            Me.CmdWhere += "AND ManufacturingYear LIKE '%" + tmpCThnProd + "%'"
        End If

        If txtSearch.Text.Trim <> "" And Me.CmdWhere <> "" Then
            Dim tmpSearch As String = txtSearch.Text.Replace("%", "").Trim
            Me.CmdWhere += " AND " + cboSearch.SelectedItem.Value + " LIKE '%" + tmpSearch + "%'"
        ElseIf txtSearch.Text.Trim <> "" And Me.CmdWhere = "" Then
            Dim tmpSearch As String = txtSearch.Text.Replace("%", "").Trim
            Me.CmdWhere += " " + cboSearch.SelectedItem.Value + " LIKE '%" + tmpSearch + "%'"
        End If

        If Me.CmdWhere = "" Then
            Me.CmdWhere = "BranchID = '" & Replace(Me.sesBranchId, "'", "") & "'"
        End If

        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub imbBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonBack.Click
        Response.Redirect("AssetMasterPrice.aspx")
    End Sub
    Private Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        Response.Redirect("AssetMasterPrice.aspx")
    End Sub
    Protected Sub btnLookupAsset_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnLookupAsset.Click
        UcLookupAsset.AssetTypeID = "MOBIL"
        UcLookupAsset.Sort = "AssetCode ASC"
        UcLookupAsset.ApplicationId = Request.QueryString("applicationid")
        UcLookupAsset.CmdWhere = ""              
        UcLookupAsset.Popup()
    End Sub
    Public Sub CatSelectedAsset(ByVal CatAssetCode As String, ByVal CatDescription As String, ByVal isKaroseri As Boolean)
        AssetCode = CatAssetCode
        AssetDescription = CatDescription
        txtAssetName.Text = CatDescription        
    End Sub
End Class