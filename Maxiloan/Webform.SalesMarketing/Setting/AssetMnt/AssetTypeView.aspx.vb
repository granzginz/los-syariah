﻿#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
#End Region

Public Class AssetTypeView
    Inherits Maxiloan.Webform.WebBased

    Private m_controller As New AssetTypeController

    Private Property AssetTypeId() As String
        Get
            Return CType(viewstate("AssetTypeID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AssetTypeID") = Value
        End Set
    End Property


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.AssetTypeId = Request.QueryString("AssetTypeID")
            Bind()
        End If        
    End Sub

    Sub Bind()
        Dim oAssetType As New Parameter.AssetType

        oAssetType.Id = Me.AssetTypeId
        oAssetType.strConnection = GetConnectionString

        oAssetType = m_controller.GetAssetTypeEdit(oAssetType)

        lblAssetID.Visible = True
        lblAssetID.Text = Me.AssetTypeId
        lblDescription.Text = oAssetType.Description
        lblSerial1.Text = oAssetType.Serial1
        lblSerial2.Text = oAssetType.Serial2
    End Sub
End Class