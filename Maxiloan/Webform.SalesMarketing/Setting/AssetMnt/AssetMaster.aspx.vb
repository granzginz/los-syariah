﻿#Region "Imports"
Imports System.Data.SqlClient
Imports System.Web.Security
Imports Maxiloan.General.CommonCookiesHelper
Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.Controller
#End Region

Public Class AssetMaster
    Inherits Maxiloan.Webform.WebBased
#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property Mode() As String
        Get
            Return (CType(ViewState("Mode"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("Mode") = Value
        End Set
    End Property

    Private Property AssetParent() As String
        Get
            Return (CType(ViewState("AssetParent"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("AssetParent") = Value
        End Set
    End Property

    Private Property MaxLevel() As String
        Get
            Return (CType(ViewState("MaxLevel"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("MaxLevel") = Value
        End Set
    End Property

    Private Property Criteria() As String
        Get
            Return (CType(ViewState("Criteria"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("Criteria") = Value
        End Set
    End Property

    Private Property AssetCode() As String
        Get
            Return (CType(ViewState("assetcode"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("assetcode") = Value
        End Set
    End Property

    Private Property AssetLevel() As Integer
        Get
            Return (CType(ViewState("AssetLevel"), Integer))
        End Get
        Set(ByVal Value As Integer)
            ViewState("AssetLevel") = Value
        End Set
    End Property
    Private Property AssetLeveltest() As Integer
        Get
            Return (CType(ViewState("AssetLeveltest"), Integer))
        End Get
        Set(ByVal Value As Integer)
            ViewState("AssetLeveltest") = Value
        End Set
    End Property
    Private Property JumlahLevel() As Integer
        Get
            Return CType(ViewState("jumlahlevel"), Integer)
        End Get
        Set(ByVal Value As Integer)
            ViewState("jumlahlevel") = Value
        End Set
    End Property
#End Region
#Region "Constanta"
    Private oController As New AssetTypeController
    Private m_controller As New AssetMasterPriceController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region
#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If
        If lblMessage.Text = "Data tidak ditemukan ....." Then
            lblMessage.Visible = False
        End If
        If Not IsPostBack Then
            Me.JumlahLevel = 0
            dtgAssetType.PageSize = 10
            doFilterBinding()
            InitialPanel()
            BindNegara()
            Me.SortBy = " assetcode asc"
            If Request("cmd") = "dtlAssetMasterPrice" Then
                BindViewAsset(Request("AssetCode").Trim, 0, 0, "")
            End If
        End If
    End Sub

    Private Sub InitialPanel()
        pnlList.Visible = True
        pnlGrid.Visible = False
        pnlViewAdd.Visible = False
        pnlLevel12.Visible = False
        pnlLevel3.Visible = False
        rfvAssetCodeLevel3.Enabled = False
        rfvDescriptionLevel3.Enabled = False
        'rfvCarsLevel3.Enabled = False
        rfvAssetCodeLevel12.Enabled = False
        rfvDescriptionLevel12.Enabled = False
        rfvCategory3.Enabled = False
        pnlHeader.Visible = False
        txtPage.Text = "1"
    End Sub

    Private Sub imbSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSearch.Click
        Me.CmdWhere = "Assettypeid = '" + cboFilterBy.SelectedValue.Trim + "' and " + cboSearchBy.SelectedValue + " LIKE '%" + txtSearchBy.Text.Trim + "%'"
        doFilter()
        lblJenisAset.Text = cboFilterBy.SelectedItem.Text
    End Sub
    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        txtSearchBy.Text = ""
        txtPage.Text = "1"
        doFilter()
        doBinding()
        cboFilterBy.SelectedIndex = 0
        pnlGrid.Visible = False
    End Sub
#Region "Bind Combo Negara"
    Private Sub BindNegara()
        Dim oControllerNegara As New NegaraController
        Dim oNegara As New Parameter.Negara
        Dim tblNegara As New DataTable
        oNegara.strConnection = GetConnectionString()
        tblNegara = oControllerNegara.GetNegaraCombo(oNegara)
        cboNegara.DataTextField = "Description"
        cboNegara.DataValueField = "Description"
        cboNegara.DataSource = tblNegara
        cboNegara.DataBind()
    End Sub
#End Region
#Region "Bind Asset Type"
    Private Sub doFilterBinding()
        Dim objcommand As New SqlCommand
        Dim objconnection As New SqlConnection(GetConnectionString)
        Dim objread As SqlDataReader
        Try
            If objconnection.State = ConnectionState.Closed Then objconnection.Open()
            objcommand.CommandType = CommandType.StoredProcedure
            objcommand.CommandText = "spGetAssetType"
            objcommand.Connection = objconnection
            objread = objcommand.ExecuteReader

            With cboFilterBy
                .DataSource = objread
                .DataTextField = "description"
                .DataValueField = "assettypeid"
                .DataBind()
                .Items.Insert(0, "Select One")
                .Items(0).Value = "0"
                .SelectedIndex = 0
            End With
            objread.Close()

            'doFilter()
        Catch exp As Exception
            ShowMessage(lblMessage, exp.Message, True)
        Finally
            objcommand.Dispose()
            If objconnection.State = ConnectionState.Open Then objconnection.Close()
            objconnection.Dispose()
        End Try
    End Sub
#End Region

#Region "Bind Asset Category"
    Private Sub doComboBinding()
        Dim objcommand As New SqlCommand
        Dim objconnection As New SqlConnection(GetConnectionString)
        Dim objread As SqlDataReader
        Try
            If objconnection.State = ConnectionState.Closed Then objconnection.Open()
            objcommand.CommandType = CommandType.StoredProcedure
            objcommand.CommandText = "spGetAssetCategory"
            If cboFilterBy.Items.Count > 0 Then
                objcommand.Parameters.Add("@AssetTypeID", SqlDbType.VarChar, 50).Value = cboFilterBy.SelectedItem.Value.Trim.Replace("'", "''")
            Else
                objcommand.Parameters.Add("@AssetTypeID", SqlDbType.VarChar, 50).Value = ""
            End If
            objcommand.Connection = objconnection
            objread = objcommand.ExecuteReader
            With cboAssetCategoryLevel3
                .DataSource = objread
                .DataTextField = "description"
                .DataValueField = "categoryid"
                .DataBind()
            End With
            objread.Close()
        Catch exp As Exception
            ShowMessage(lblMessage, exp.Message, True)
        Finally
            objcommand.Dispose()
            If objconnection.State = ConnectionState.Open Then objconnection.Close()
            objconnection.Dispose()
        End Try
    End Sub
#End Region

#Region "Bind Data Grid"
    Private Sub doBinding()
        Dim dtEntity As DataTable = Nothing
        Dim oAssetMasterPrice As New Parameter.AssetMasterPrice
        oAssetMasterPrice.strConnection = GetConnectionString()
        oAssetMasterPrice.WhereCond = Me.CmdWhere
        oAssetMasterPrice.CurrentPage = currentPage
        oAssetMasterPrice.PageSize = pageSize
        oAssetMasterPrice.SortBy = "AssetCode asc"
        oAssetMasterPrice = m_controller.GetAssetMaster(oAssetMasterPrice)

        If Not oAssetMasterPrice Is Nothing Then
            dtEntity = oAssetMasterPrice.Listdata
            recordCount = oAssetMasterPrice.Totalrecords
        Else
            recordCount = 0
        End If
        
        dtgAssetType.DataSource = dtEntity.DefaultView
        dtgAssetType.CurrentPageIndex = 0
        dtgAssetType.DataBind()
        PagingFooter()
    End Sub
#End Region

#End Region
#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        doBinding()
    End Sub
    Private Sub imgbtnPageNumb_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int32)
                doBinding()
            End If
        End If
    End Sub
#End Region
#Region "Data Grid Command"
    Private Sub dtgAssetType_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgAssetType.ItemCommand
        Dim lnkAssetCode As LinkButton
        Dim AssetLevel As Integer
        Dim JumlahLevel As Integer
        Dim assetDescription As String
        Dim Origination As TextBox
        Select Case e.CommandName
            Case "view"
                AssetLevel = CInt(dtgAssetType.Items(e.Item.ItemIndex).Cells(6).Text)
                JumlahLevel = CInt(dtgAssetType.Items(e.Item.ItemIndex).Cells(7).Text)
                lnkAssetCode = CType(dtgAssetType.Items(e.Item.ItemIndex).FindControl("lnkAssetCode"), LinkButton)
                Origination = CType(dtgAssetType.Items(e.Item.ItemIndex).FindControl("txtOrigination"), TextBox)
                BindViewAsset(lnkAssetCode.Text.Trim, AssetLevel, JumlahLevel, Origination.Text.Trim)
            Case "add_detail"
                AssetLevel = CInt(dtgAssetType.Items(e.Item.ItemIndex).Cells(6).Text)
                JumlahLevel = CInt(dtgAssetType.Items(e.Item.ItemIndex).Cells(7).Text)
                assetDescription = CStr(dtgAssetType.Items(e.Item.ItemIndex).Cells(4).Text)
                lnkAssetCode = CType(dtgAssetType.Items(e.Item.ItemIndex).FindControl("lnkAssetCode"), LinkButton)
                Origination = CType(dtgAssetType.Items(e.Item.ItemIndex).FindControl("txtOrigination"), TextBox)
                AddDetailAssetMaster(lnkAssetCode.Text.Trim, assetDescription, AssetLevel, JumlahLevel, Origination.Text.Trim)
            Case "edit"
                AssetLevel = CInt(dtgAssetType.Items(e.Item.ItemIndex).Cells(6).Text)
                JumlahLevel = CInt(dtgAssetType.Items(e.Item.ItemIndex).Cells(7).Text)
                lnkAssetCode = CType(dtgAssetType.Items(e.Item.ItemIndex).FindControl("lnkAssetCode"), LinkButton)
                assetDescription = CStr(dtgAssetType.Items(e.Item.ItemIndex).Cells(4).Text)
                Origination = CType(dtgAssetType.Items(e.Item.ItemIndex).FindControl("txtOrigination"), TextBox)
                EditAssetMaster(lnkAssetCode.Text.Trim, assetDescription, AssetLevel, JumlahLevel, Origination.Text.Trim)

            Case "delete"
                AssetLevel = CInt(dtgAssetType.Items(e.Item.ItemIndex).Cells(6).Text)
                JumlahLevel = CInt(dtgAssetType.Items(e.Item.ItemIndex).Cells(7).Text)
                lnkAssetCode = CType(dtgAssetType.Items(e.Item.ItemIndex).FindControl("lnkAssetCode"), LinkButton)
                DeleteDetail(lnkAssetCode.Text.Trim)
                txtSearchBy.Text = ""
                doFilter()
                doBinding()
        End Select
    End Sub
    Public Sub doSorting(ByVal sender As Object, ByVal e As DataGridSortCommandEventArgs)
        If CInt(InStr(Me.SortBy, "DESC")) > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression & " DESC"
        End If
        doBinding()
    End Sub
#End Region
#Region "Bind View Asset"
    Private Sub BindViewAsset(ByVal AssetCode As String, ByVal AssetLevel As Integer, ByVal JumlahLevel As Integer, ByVal Origination As String)
        Dim objcommand As New SqlCommand
        Dim objconnection As New SqlConnection(GetConnectionString)
        Dim objread As SqlDataReader
        Try
            If objconnection.State = ConnectionState.Closed Then objconnection.Open()
            objcommand.Connection = objconnection
            objcommand.CommandType = CommandType.StoredProcedure
            objcommand.CommandText = "spAssetMasterView"
            objcommand.Parameters.Add("@AssetCode", SqlDbType.VarChar, 50).Value = AssetCode
            objread = objcommand.ExecuteReader
            If objread.Read Then
                Call AssetTypeLabel()
                pnlGrid.Visible = False
                pnlList.Visible = False
                pnlView.Visible = True
                pnlHeader.Visible = True
                lblEntryFormHeader.Text = "View Record"
                lblViewAssetCode.Text = CStr(objread("assetcode")).Trim
                lblViewDescription.Text = CStr(objread("Description")).Trim
                lblNegara.Text = CStr(objread("ManufacturerCountry")).Trim
                lblSilinder.Text = CStr(objread("CylinderCapacity")).Trim
                lblTransmisi.Text = CStr(objread("Transmission")).Trim
                lblBahanBakar.Text = CStr(objread("Fuel")).Trim
                lblJmlDuduk.Text = CStr(objread("SeatCapacity")).Trim
                checkLblKaroseri.Checked = CBool(objread("Karoseri"))
                checkLblKaroseri.Enabled = False
                rbolblOrigination.SelectedItem.Value = CStr(objread("Origination")).Trim
                rbolblOrigination.Enabled = False
                ''added temporary by teddy for testing assetLevel
                'Me.AssetLeveltest = CInt(objread("assetlevel"))
                If CInt(objread("assetlevel")) > 1 And AssetLevel = JumlahLevel Then
                    pnlViewAdd.Visible = True
                    If IsDBNull(objread("categoryid")) Then
                        lblViewAssetCategory.Text = "-"
                    Else
                        lblViewAssetCategory.Text = CStr(objread("desccategory")).Trim
                        lblResaleValue.Text = FormatNumber(objread("resalevalue"), 2)
                    End If
                Else
                    pnlViewAdd.Visible = False
                End If
                Me.AssetCode = CStr(objread("assetcode")).Trim
                Me.Mode = "view"
            Else
                ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            End If
            objread.Close()


        Catch exp As Exception
            ShowMessage(lblMessage, exp.Message, True)
        Finally
            If objconnection.State = ConnectionState.Closed Then objconnection.Open()
            objconnection.Dispose()
        End Try
    End Sub
#End Region
#Region "Sub Delete Detail"
    Private Sub DeleteDetail(ByVal AssetCode As String)
        If CheckBeforeDelete(AssetCode) = True Then
            ShowMessage(lblMessage, "Asset telah digunakan, tidak dapat dihapus", True)
            Exit Sub
        End If
        Dim objcommand As New SqlCommand
        Dim objconnection As New SqlConnection(GetConnectionString)
        Dim objTransaction As SqlTransaction = Nothing
        Try
            If objconnection.State = ConnectionState.Closed Then objconnection.Open()
            objTransaction = objconnection.BeginTransaction

            objcommand.CommandType = CommandType.StoredProcedure
            objcommand.Connection = objconnection
            objcommand.Transaction = objTransaction
            objcommand.CommandText = "spAssetMasterDelete"
            objcommand.Parameters.Add("@AssetCode", SqlDbType.VarChar, 50).Value = AssetCode
            objcommand.ExecuteNonQuery()
            objTransaction.Commit()
            ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, False)
        Catch exp As Exception
            objTransaction.Rollback()
            ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_FAILED, True)
        Finally
            If objconnection.State = ConnectionState.Open Then objconnection.Close()
            objconnection.Dispose()
            objcommand.Dispose()
        End Try
    End Sub
    Private Function CheckBeforeDelete(ByVal assetcode As String) As Boolean
        Dim rtn As Boolean = False
        Dim objcommand As New SqlCommand
        Dim objconnection As New SqlConnection(GetConnectionString)
        If objconnection.State = ConnectionState.Closed Then objconnection.Open()
        Dim retValue As Object

        objcommand.CommandType = CommandType.StoredProcedure
        objcommand.Connection = objconnection
        objcommand.CommandText = "spCheckAssetMasterDelete"
        objcommand.Parameters.Add("@AssetCode", SqlDbType.VarChar, 50).Value = assetcode
        retValue = objcommand.ExecuteScalar()
        If CInt(retValue) > 0 Then
            rtn = True
        End If
        Return rtn
    End Function

#End Region
#Region "Edit"
    Private Sub EditAssetMaster(ByVal AssetCode As String, ByVal AssetDescription As String, ByVal AssetLevel As Integer, ByVal JumlahLevel As Integer, _
                                ByVal Origination As String)
        pnlGrid.Visible = False
        pnlView.Visible = False
        Dim objcommand As New SqlCommand
        Dim objconnection As New SqlConnection(GetConnectionString)
        Dim objread As SqlDataReader
        pnlGrid.Visible = False
        pnlList.Visible = False
        pnlView.Visible = False

        Try
            If objconnection.State = ConnectionState.Closed Then objconnection.Open()
            objcommand.Connection = objconnection
            objcommand.CommandType = CommandType.StoredProcedure
            objcommand.CommandText = "spAssetMasterView"
            objcommand.Parameters.Add("@AssetCode", SqlDbType.VarChar, 50).Value = AssetCode
            objread = objcommand.ExecuteReader
            If objread.Read Then
                pnlHeader.Visible = True
                lblEntryFormHeader.Text = "ASSET MASTER - EDIT"

                Me.AssetLevel = CInt(objread("assetlevel"))
                'If Me.AssetLevel > 1 And AssetLevel = JumlahLevel Then
                If AssetLevel = JumlahLevel Then
                    pnlLevel3.Visible = True
                    pnlLevel12.Visible = False
                    rfvAssetCodeLevel3.Enabled = False
                    rfvDescriptionLevel3.Enabled = True
                    'rfvCarsLevel3.Enabled = True
                    rfvCategory3.Enabled = True
                    rfvAssetCodeLevel12.Enabled = False
                    rfvDescriptionLevel12.Enabled = False
                    txtAssetCodeLevel3.Visible = False
                    lblAssetCodeLevel3.Visible = True
                    lblDescriptionLevel3.Visible = True
                    Me.Mode = "edit_level3"
                    lblAssetCodeLevel3.Text = AssetCode
                    Dim ArrSplit() As String
                    ArrSplit = Split(AssetDescription, "-")

                    If UBound(ArrSplit) = 0 Then
                        lblDescriptionLevel3.Text = ""
                        txtDescriptionLevel3.Text = AssetDescription.Trim
                    Else
                        Dim m As Int32
                        For m = 0 To UBound(ArrSplit) - 1
                            lblDescriptionLevel3.Text += CStr(ArrSplit(m)) + "-"
                        Next
                        txtDescriptionLevel3.Text = CStr(ArrSplit(UBound(ArrSplit)))
                    End If
                    If Not IsDBNull(objread("categoryid")) Then
                        cboAssetCategoryLevel3.SelectedIndex = cboAssetCategoryLevel3.Items.IndexOf(cboAssetCategoryLevel3.Items.FindByText(CStr(objread("desccategory")).Trim))
                    End If
                    txtResaleValue.Text = CStr(objread("resalevalue")).Trim
                    cboNegara.SelectedItem.Text = CStr(objread("ManufacturerCountry")).Trim
                    txtSilinder.Text = CStr(objread("CylinderCapacity")).Trim
                    txtJmlTempDuduk.Text = CStr(objread("SeatCapacity")).Trim
                    CheckKaroseri.Checked = CBool(objread("Karoseri"))
                    cboTransmissionType.SelectedValue = CStr(objread("TransmissionType")).Trim
                    cboFuelType.SelectedValue = CStr(objread("FuelType")).Trim
                    If objread("Origination").ToString.Trim = "" Then
                        rboOrigination.SelectedIndex = rboOrigination.Items.IndexOf(rboOrigination.Items.FindByValue("CKD"))
                    Else
                        rboOrigination.SelectedIndex = rboOrigination.Items.IndexOf(rboOrigination.Items.FindByValue(objread("Origination").ToString.Trim))
                    End If

                Else
                    pnlLevel3.Visible = False
                    pnlLevel12.Visible = True
                    rfvAssetCodeLevel3.Enabled = False
                    rfvDescriptionLevel3.Enabled = False
                    'rfvCarsLevel3.Enabled = False
                    rfvCategory3.Enabled = False
                    rfvAssetCodeLevel12.Enabled = False 'True

                    rfvDescriptionLevel12.Enabled = True
                    txtAssetCodeLevel12.Visible = False
                    lblAssetCodeLevel12.Visible = True
                    Me.Mode = "edit_level1"
                    lblAssetCodeLevel12.Text = AssetCode
                    lblDescriptionLevel12.Visible = True
                    Dim ArrSplit() As String
                    ArrSplit = Split(AssetDescription.Trim, "-")
                    If UBound(ArrSplit) = 0 Then
                        lblDescriptionLevel12.Text = ""
                        txtDescriptionLevel12.Text = AssetDescription.Trim
                    Else
                        Dim m As Int32
                        For m = 0 To CInt(UBound(ArrSplit) - 1)
                            lblDescriptionLevel12.Text += CStr(ArrSplit(m)) + "-"
                        Next
                        txtDescriptionLevel12.Text = CStr(ArrSplit(UBound(ArrSplit)))
                        txtDescriptionLevel12.Visible = True
                    End If
                End If
                Me.AssetCode = AssetCode
            Else
                ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            End If
        Catch exp As Exception
            ShowMessage(lblMessage, exp.Message, True)
        Finally
            If objconnection.State = ConnectionState.Closed Then objconnection.Open()
            objconnection.Dispose()
        End Try
    End Sub
#End Region
#Region "AddDetail Asset Master"
    Private Sub AddDetailAssetMaster(ByVal AssetCode As String, ByVal AssetDescription As String, ByVal AssetLevel As Integer, ByVal JumlahLevel As Integer, _
                                      ByVal Origination As String)
        pnlGrid.Visible = False
        pnlView.Visible = False

        Dim objcommand As New SqlCommand
        Dim objconnection As New SqlConnection(GetConnectionString)
        Dim objread As SqlDataReader
        Try
            pnlGrid.Visible = False
            pnlList.Visible = False
            If objconnection.State = ConnectionState.Closed Then objconnection.Open()
            objcommand.Connection = objconnection
            objcommand.CommandType = CommandType.StoredProcedure
            objcommand.CommandText = "spAssetMasterView"
            objcommand.Parameters.Add("@AssetCode", SqlDbType.VarChar, 50).Value = AssetCode
            objread = objcommand.ExecuteReader
            pnlHeader.Visible = True
            lblEntryFormHeader.Text = "ASSET MASTER - ADD"
            If objread.Read Then
                Me.AssetParent = AssetCode
                If AssetLevel + 1 < JumlahLevel Then
                    pnlLevel12.Visible = True
                    pnlLevel3.Visible = False
                    rfvAssetCodeLevel3.Enabled = False
                    rfvDescriptionLevel3.Enabled = False
                    'rfvCarsLevel3.Enabled = False
                    rfvCategory3.Enabled = False
                    rfvAssetCodeLevel12.Enabled = True
                    rfvDescriptionLevel12.Enabled = True
                    txtAssetCodeLevel3.Visible = False
                    lblAssetCodeLevel3.Visible = True
                    Me.Mode = "new_level1"
                    Me.AssetLevel = CInt(objread("assetlevel")) + 1
                    lblDescriptionLevel12.Text = AssetDescription & "-"
                    lblDescriptionLevel12.Visible = True
                    txtDescriptionLevel12.Text = ""
                    txtAssetCodeLevel12.Visible = True
                    lblAssetCodeLevel12.Text = AssetCode & "."
                    lblAssetCodeLevel12.Visible = True
                    txtAssetCodeLevel12.Text = ""

                Else
                    Me.Mode = "new_level3"
                    Me.AssetLevel = CInt(objread("AssetLevel"))
                    ''Added By teddy
                    Me.JumlahLevel = JumlahLevel
                    pnlLevel12.Visible = False
                    pnlLevel3.Visible = True
                    rfvAssetCodeLevel3.Enabled = True
                    rfvDescriptionLevel3.Enabled = True
                    'rfvCarsLevel3.Enabled = True
                    rfvCategory3.Enabled = True
                    rfvAssetCodeLevel12.Enabled = False
                    rfvDescriptionLevel12.Enabled = False

                    lblAssetCodeLevel12.Visible = False
                    txtAssetCodeLevel12.Visible = False

                    lblDescriptionLevel3.Text = AssetDescription & "-"
                    lblDescriptionLevel3.Visible = True
                    txtDescriptionLevel3.Text = ""
                    lblAssetCodeLevel3.Text = AssetCode & "."
                    lblAssetCodeLevel3.Visible = True
                    txtAssetCodeLevel3.Text = ""
                    txtAssetCodeLevel3.Visible = True
                    txtResaleValue.Text = "0.00"

                    Call AssetTypeTxt()
                 
                End If
            End If
        Catch exp As Exception
            ShowMessage(lblMessage, exp.Message, True)
        Finally
            If objconnection.State = ConnectionState.Closed Then objconnection.Open()
            objconnection.Dispose()
        End Try
    End Sub
#End Region
    Private Sub AssetTypeTxt()
        Dim strAssetTypeID As String = cboFilterBy.SelectedItem.Value.Trim
        If strAssetTypeID = "HE" Then
            divResaleValue.Visible = False
            divSilinder.Visible = False
            divTransmissionType.Visible = False
            divFuelType.Visible = False
            divJmlTempDuduk.Visible = False
            divCheckKaroseri.Visible = False
        Else
            divResaleValue.Visible = True
            divSilinder.Visible = True
            divTransmissionType.Visible = True
            divFuelType.Visible = True
            divJmlTempDuduk.Visible = True
            divCheckKaroseri.Visible = True
        End If
    End Sub

    Private Sub AssetTypeLabel()
        Dim strAssetTypeID As String = cboFilterBy.SelectedItem.Value.Trim
        If strAssetTypeID = "HE" Then
            divLblResaleValue.Visible = False
            divLblSilinder.Visible = False
            divLblTransmissionType.Visible = False
            divLblFuelType.Visible = False
            divLblJmlTempDuduk.Visible = False
            divLblCheckKaroseri.Visible = False
        Else
            divLblResaleValue.Visible = True
            divLblSilinder.Visible = True
            divLblTransmissionType.Visible = True
            divLblFuelType.Visible = True
            divLblJmlTempDuduk.Visible = True
            divLblCheckKaroseri.Visible = True
        End If
    End Sub
       
#Region "getTransMessage"
    Private Function getTransMessage() As String
        Select Case Trim(Me.Mode)
            Case "del" : Return "Data Transaksi sudah dihapus"
            Case "new" : Return "Data Transaksi sudah disimpan"
            Case "edit" : Return "Data Transaksi sudah diupdate"
            Case Else : Return ""
        End Select
    End Function
#End Region
#Region "Hide Process"
    Private Sub doClear()
        lblAssetCodeLevel12.Text = ""
        lblAssetCodeLevel3.Text = ""
        lblDescriptionLevel12.Text = ""
        lblDescriptionLevel3.Text = ""
        txtAssetCodeLevel12.Text = ""
        txtAssetCodeLevel3.Text = ""
        txtDescriptionLevel12.Text = ""
        txtDescriptionLevel12.Text = ""
        'cboAssetCategoryLevel3.SelectedIndex = 0
    End Sub

    Private Sub doHide()
        doClear()
        Me.Mode = ""
        pnlLevel12.Visible = False
        pnlLevel3.Visible = False
        pnlHeader.Visible = False
        pnlView.Visible = False
        pnlViewAdd.Visible = False
        rfvAssetCodeLevel3.Enabled = False
        rfvDescriptionLevel3.Enabled = False
        'rfvCarsLevel3.Enabled = False
        rfvCategory3.Enabled = False
        rfvAssetCodeLevel12.Enabled = False
        rfvDescriptionLevel12.Enabled = False
        txtAssetCodeLevel12.Visible = False
        lblAssetCodeLevel12.Visible = False
        lblDescriptionLevel12.Text = ""
    End Sub
#End Region
#Region "DAta Grid Command"
    Private Sub dtgAssetType_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgAssetType.ItemDataBound
        e.Item.Cells(1).Attributes.Add("onclick", "return fConfirm()")
        If (e.Item.Cells(6).Text = "3") Then
            e.Item.Cells(2).Text = ""
        End If
    End Sub
#End Region
#Region "Filter"
    Private Sub doFilter()
        doHide()
        doBinding()
        doComboBinding()
        dtgAssetType.Visible = True
        pnlGrid.Visible = True

    End Sub
#End Region
#Region "Full Asset"
    Private Function getFullAsset(ByVal strAssetCode As String, ByVal strFlag As String) As String
        Dim objConnAsset As New SqlConnection(GetConnectionString)
        Dim strResult As String

        Dim objCommand As New SqlCommand("sp_GetAssetTypeDescription", objConnAsset)
        objCommand.CommandType = CommandType.StoredProcedure

        Dim objParam As SqlParameter
        '= objCommand.Parameters.Add("Return_Value", SqlDbType.VarChar, 1000)
        'objParam.Direction() = ParameterDirection.ReturnValue

        objParam = objCommand.Parameters.Add("@assetcode", SqlDbType.VarChar, 20)
        objParam.Value = strAssetCode.Trim
        objParam = objCommand.Parameters.Add("@flag", SqlDbType.VarChar, 10)
        objParam.Value = LCase(strFlag.Trim)
        objParam = objCommand.Parameters.Add("@result", SqlDbType.VarChar, 1000)
        objParam.Direction = ParameterDirection.Output
        objConnAsset.Open()

        objCommand.ExecuteNonQuery()
        objConnAsset = Nothing
        strResult = CStr(objCommand.Parameters("@result").Value)
        Return strResult
    End Function
#End Region
#Region "Is Enabled"
    Public Function isEnabled(ByVal intAssetLevel As Int16, ByVal intJumlahAsset As Int16) As Boolean
        If intAssetLevel = intJumlahAsset Then
            Return False
        Else
            Return True
        End If
    End Function
#End Region
#Region "Add New"
    Private Sub ImgAddNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonAddNew.Click
        'If CheckFeature(Me.Loginid, Me.FormID, "Add", Me.AppId) Then
        pnlGrid.Visible = False
        pnlList.Visible = False
        pnlHeader.Visible = True




        Dim strAssetTypeID As String = cboFilterBy.SelectedItem.Value.Trim
        Dim MaxLevel As Integer


        MaxLevel = oController.GetMaxLevel(strAssetTypeID, GetConnectionString)
        Me.MaxLevel = CStr(MaxLevel)

        If MaxLevel = 1 Then
            Me.Mode = "new_level3"
            pnlLevel12.Visible = False
            pnlLevel3.Visible = True
            txtAssetCodeLevel3.Visible = True
            lblAssetCodeLevel3.Visible = False



        Else
            Me.Mode = "new_level1"
            pnlLevel12.Visible = True
            pnlLevel3.Visible = False
        End If




        lblEntryFormHeader.Text = "ASSET MASTER - ADD"
        doComboBinding()
        doClear()

        Me.AssetParent = ""
        rfvAssetCodeLevel3.Enabled = False
        rfvDescriptionLevel3.Enabled = False
        'rfvCarsLevel3.Enabled = False
        rfvCategory3.Enabled = False
        rfvAssetCodeLevel12.Enabled = True
        rfvDescriptionLevel12.Enabled = True
        txtAssetCodeLevel12.Visible = True
        lblAssetCodeLevel12.Visible = False
        lblDescriptionLevel12.Text = ""
        lblDescriptionLevel12.Visible = False
        Me.AssetLevel = 1
        'Endif
    End Sub
#End Region
#Region "btnClose"
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonClose.Click
        If Request("cmd") = "dtlAssetMasterPrice" Then
            Response.Redirect("AssetMasterPrice.aspx")
        Else
            doHide()
            lblEntryFormHeader.Text = ""
            pnlList.Visible = True
            pnlGrid.Visible = True
            pnlLevel12.Visible = False
            txtSearchBy.Text = ""
            txtPage.Text = "1"
            doFilter()
            doBinding()
        End If
    End Sub
#End Region
#Region "Asset Level 12"
    Private Sub btnSaveLevel12_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSaveLevel12.Click
        Dim objcommand As New SqlCommand
        Dim objconnection As New SqlConnection(GetConnectionString)
        Dim objTransaction As SqlTransaction = Nothing
        Dim strAssetCode As String
        Dim strAssetDescription As String
        Dim IsFinal As Boolean
        Dim Origination As String
        Try
            If objconnection.State = ConnectionState.Closed Then objconnection.Open()
            objTransaction = objconnection.BeginTransaction
            If cboFilterBy.Items.Count <= 0 Then
                Exit Sub
            End If

            If CInt(Me.JumlahLevel) = CInt(Me.AssetLevel) Then
                IsFinal = True
            Else
                IsFinal = False
            End If
            'If (lblDescriptionLevel12.Text.Trim + txtDescriptionLevel12.Text.Trim).Length > 50 Then
            '    ShowMessage(lblMessage, "Nama Asset harus maksimum 50 Character", True)
            '    Exit Sub
            'End If
            If (lblDescriptionLevel12.Text.Trim + txtDescriptionLevel12.Text.Trim).Length > 100 Then
                ShowMessage(lblMessage, "Nama Asset harus maksimum 100 Character", True)
                Exit Sub
            End If
            If (lblAssetCodeLevel12.Text.Trim + txtAssetCodeLevel12.Text.Trim).Length > 20 Then
                ShowMessage(lblMessage, "Kode Asset maksimum 20 character", True)
                Exit Sub
            End If

            objcommand.Connection = objconnection
            objcommand.Transaction = objTransaction

            Select Case Me.Mode
                Case "new_level1"
                    If lblAssetCodeLevel12.Text = "" Then
                        strAssetCode = txtAssetCodeLevel12.Text.Trim.Replace(".", "")
                    Else
                        strAssetCode = lblAssetCodeLevel12.Text.Trim & txtAssetCodeLevel12.Text.Trim.Replace(".", "")
                    End If
                    If lblDescriptionLevel12.Text = "" Then
                        strAssetDescription = txtDescriptionLevel12.Text.Trim.Replace("-", "")
                    Else
                        strAssetDescription = lblDescriptionLevel12.Text.Trim & txtDescriptionLevel12.Text.Trim.Replace("-", "")
                    End If
                    objcommand.CommandText = "spAssetMaster12Add"
                    objcommand.CommandType = CommandType.StoredProcedure
                    objcommand.Parameters.Add("@AssetCode", SqlDbType.VarChar, 50).Value = strAssetCode
                    If Me.AssetParent <> "" Then
                        objcommand.Parameters.Add("@AssetParent", SqlDbType.VarChar, 50).Value = Me.AssetParent
                    End If
                    objcommand.Parameters.Add("@AssetTypeID", SqlDbType.VarChar, 10).Value = cboFilterBy.SelectedItem.Value.Trim
                    objcommand.Parameters.Add("@AssetLevel", SqlDbType.Int).Value = Me.AssetLevel
                    objcommand.Parameters.Add("@description", SqlDbType.VarChar, 50).Value = strAssetDescription
                    objcommand.Parameters.Add("@IsFinal", SqlDbType.Bit).Value = IsFinal
                    If IsNothing(rboOrigination.SelectedItem) Then
                        objcommand.Parameters.Add("@Origination", SqlDbType.Char).Value = ""
                    Else
                        objcommand.Parameters.Add("@Origination", SqlDbType.Char).Value = rboOrigination.SelectedItem.Value.Trim
                    End If
                Case "edit_level1"
                    objcommand.CommandText = "spAssetMaster12Update"
                    objcommand.CommandType = CommandType.StoredProcedure
                    objcommand.Parameters.Add("@AssetCode", SqlDbType.VarChar, 50).Value = Me.AssetCode.Trim
                    'objcommand.Parameters.Add("@description", SqlDbType.VarChar, 50).Value = lblDescriptionLevel12.Text.Trim & txtDescriptionLevel12.Text.Trim.Replace("-", "")
                    objcommand.Parameters.Add("@description", SqlDbType.VarChar, 100).Value = lblDescriptionLevel12.Text.Trim & txtDescriptionLevel12.Text.Trim.Replace("-", "")
                    If IsNothing(rboOrigination.SelectedItem) Then
                        objcommand.Parameters.Add("@Origination", SqlDbType.Char).Value = ""
                    Else
                        objcommand.Parameters.Add("@Origination", SqlDbType.Char).Value = rboOrigination.SelectedItem.Value.Trim
                    End If
            End Select
            objcommand.ExecuteNonQuery()
            objTransaction.Commit()
            pnlGrid.Visible = True
            pnlList.Visible = True
            pnlHeader.Visible = False
            pnlLevel12.Visible = False
            pnlView.Visible = False
            pnlLevel3.Visible = False
            txtSearchBy.Text = ""
            doFilter()
            doBinding()
            If Me.Mode = "new_level1" Then
                ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_SUCCESS, False)
            Else
                ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
            End If
        Catch exp As Exception
            objTransaction.Rollback()
            If Me.Mode = "new_level1" Then
                ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_FAILED, True)
            Else
                ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_FAILED, True)
            End If
        Finally
            If objconnection.State = ConnectionState.Open Then objconnection.Close()
            objconnection.Dispose()
            objcommand.Dispose()
        End Try
    End Sub

    Private Sub btnCancelLevel12_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCancelLevel12.Click
        doHide()
        txtSearchBy.Text = ""
        txtPage.Text = "1"
        lblEntryFormHeader.Text = ""
        pnlList.Visible = True
        pnlGrid.Visible = True
        doFilter()
        doBinding()

    End Sub
#End Region
#Region "Asset Level 3"
    Private Sub btnSaveLevel3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSaveLevel3.Click
        Dim objcommand As New SqlCommand
        Dim objconnection As New SqlConnection(GetConnectionString)
        Dim objTransaction As SqlTransaction = Nothing
        Try
            If objconnection.State = ConnectionState.Closed Then objconnection.Open()
            objTransaction = objconnection.BeginTransaction
            If cboFilterBy.Items.Count <= 0 Then
                Exit Sub
            End If

            'If (lblDescriptionLevel3.Text.Trim + txtDescriptionLevel3.Text.Trim).Length > 50 Then
            '    ShowMessage(lblMessage, "Nama Asset Maksimum 50 character", True)
            '    Exit Sub
            'End If
            If (lblDescriptionLevel3.Text.Trim + txtDescriptionLevel3.Text.Trim).Length > 100 Then
                ShowMessage(lblMessage, "Nama Asset Maksimum 100 character", True)
                Exit Sub
            End If
            objcommand.CommandType = CommandType.StoredProcedure
            objcommand.Connection = objconnection
            objcommand.Transaction = objTransaction
            Select Case Me.Mode
                Case "new_level3"
                    If (lblAssetCodeLevel3.Text.Trim + txtAssetCodeLevel3.Text.Trim).Length > 20 Then
                        ShowMessage(lblMessage, "Kode Asset Maksimum 20 character", True)
                        Exit Sub
                    End If
                    objcommand.CommandText = "spAssetMaster3Add"
                    objcommand.Parameters.Add("@AssetCode", SqlDbType.VarChar, 50).Value = lblAssetCodeLevel3.Text.Trim & txtAssetCodeLevel3.Text.Replace(".", "")
                    If Me.AssetParent <> "" Then
                        objcommand.Parameters.Add("@AssetParent", SqlDbType.VarChar, 50).Value = Me.AssetParent
                    End If
                    'objcommand.Parameters.Add("@description", SqlDbType.VarChar, 50).Value = lblDescriptionLevel3.Text.Trim & txtDescriptionLevel3.Text.Trim.Replace("-", "")
                    objcommand.Parameters.Add("@description", SqlDbType.VarChar, 100).Value = lblDescriptionLevel3.Text.Trim & txtDescriptionLevel3.Text.Trim.Replace("-", "")
                    objcommand.Parameters.Add("@CategoryID", SqlDbType.VarChar, 10).Value = cboAssetCategoryLevel3.SelectedItem.Value.Trim
                    objcommand.Parameters.Add("@AssetLevel", SqlDbType.Int).Value = Me.JumlahLevel
                    'Marked by teddy
                    'objcommand.Parameters.Add("@AssetLevel", SqlDbType.Int).Value = Me.AssetLeveltest
                    'Changed into by teddy
                    objcommand.Parameters.Add("@ResaleValue", SqlDbType.Decimal).Value = CDbl(txtResaleValue.Text.Trim)
                    objcommand.Parameters.Add("@AssetTypeID", SqlDbType.VarChar, 10).Value = cboFilterBy.SelectedItem.Value.Trim
                    objcommand.Parameters.Add("@IsFinal", SqlDbType.Bit).Value = 1
                    objcommand.Parameters.Add("@TransmissionType", SqlDbType.Char, 1).Value = cboTransmissionType.SelectedItem.Value.Trim
                    objcommand.Parameters.Add("@FuelType", SqlDbType.Char, 1).Value = cboFuelType.SelectedItem.Value.Trim
                    objcommand.Parameters.Add("@ManufacturerCountry", SqlDbType.VarChar, 50).Value = cboNegara.SelectedItem.Text
                    objcommand.Parameters.Add("@CC", SqlDbType.Int).Value = Convert.ToInt32(IIf(txtSilinder.Text.Trim = "", 0, txtSilinder.Text).ToString).ToString
                    objcommand.Parameters.Add("@SeatCapacity", SqlDbType.SmallInt).Value = Convert.ToInt32(IIf(txtJmlTempDuduk.Text.Trim = "", 0, txtJmlTempDuduk.Text).ToString).ToString
                    objcommand.Parameters.Add("@karoseri", SqlDbType.Bit).Value = CBool(CheckKaroseri.Checked)
                    If IsNothing(rboOrigination.SelectedItem) Then
                        objcommand.Parameters.Add("@Origination", SqlDbType.Char).Value = ""
                    Else
                        objcommand.Parameters.Add("@Origination", SqlDbType.Char).Value = rboOrigination.SelectedItem.Value.Trim
                    End If


                Case "edit_level3"
                    objcommand.CommandText = "spAssetMaster3Update"
                    objcommand.Parameters.Add("@AssetCode", SqlDbType.VarChar, 50).Value = Me.AssetCode
                    objcommand.Parameters.Add("@description", SqlDbType.VarChar, 50).Value = lblDescriptionLevel3.Text.Trim & txtDescriptionLevel3.Text.Replace("-", "")
                    objcommand.Parameters.Add("@CategoryID", SqlDbType.VarChar, 10).Value = cboAssetCategoryLevel3.SelectedItem.Value.Trim
                    objcommand.Parameters.Add("@AssetLevel", SqlDbType.Int).Value = CInt(Me.AssetLevel)
                    objcommand.Parameters.Add("@ResaleValue", SqlDbType.Int).Value = CDbl(txtResaleValue.Text.Trim)
                    objcommand.Parameters.Add("@AssetTypeID", SqlDbType.VarChar, 10).Value = cboFilterBy.SelectedItem.Value.Trim
                    objcommand.Parameters.Add("@IsFinal", SqlDbType.Bit).Value = 1
                    objcommand.Parameters.Add("@TransmissionType", SqlDbType.Char, 1).Value = cboTransmissionType.SelectedItem.Value.Trim
                    objcommand.Parameters.Add("@FuelType", SqlDbType.Char, 1).Value = cboFuelType.SelectedItem.Value.Trim
                    objcommand.Parameters.Add("@ManufacturerCountry", SqlDbType.VarChar, 50).Value = cboNegara.SelectedItem.Text
                    objcommand.Parameters.Add("@CC", SqlDbType.Int).Value = Convert.ToInt32(txtSilinder.Text)
                    objcommand.Parameters.Add("@SeatCapacity", SqlDbType.SmallInt).Value = Convert.ToInt32(txtJmlTempDuduk.Text)
                    objcommand.Parameters.Add("@karoseri", SqlDbType.Bit).Value = CBool(CheckKaroseri.Checked)
                    If IsNothing(rboOrigination.SelectedItem) Then
                        objcommand.Parameters.Add("@Origination", SqlDbType.Char).Value = ""
                    Else
                        objcommand.Parameters.Add("@Origination", SqlDbType.Char).Value = rboOrigination.SelectedItem.Value.Trim
                    End If
            End Select
            objcommand.ExecuteNonQuery()
            objTransaction.Commit()
            If Me.Mode = "new_level3" Then
                ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_SUCCESS, False)
            Else
                ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
            End If
            pnlGrid.Visible = True
            pnlList.Visible = True
            pnlHeader.Visible = False
            pnlLevel12.Visible = False
            pnlView.Visible = False
            pnlLevel3.Visible = False
            txtSearchBy.Text = ""
            doFilter()
            doBinding()
        Catch exp As Exception
            objTransaction.Rollback()
            If Me.Mode = "new_level3" Then
                ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_SUCCESS, False)
            Else
                ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_FAILED, True)
            End If
            ShowMessage(lblMessage, exp.Message, True)
        Finally
            If objconnection.State = ConnectionState.Open Then objconnection.Close()
            objconnection.Dispose()
            objcommand.Dispose()
        End Try
    End Sub

    Private Sub btnCancelLevel3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCancelLevel3.Click
        doHide()
        txtSearchBy.Text = ""
        pnlGrid.Visible = True
        pnlList.Visible = True
        lblEntryFormHeader.Text = ""
        txtPage.Text = "1"
        doFilter()
        doBinding()
    End Sub
#End Region


End Class