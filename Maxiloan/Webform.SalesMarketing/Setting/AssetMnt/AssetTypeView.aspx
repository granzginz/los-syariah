﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AssetTypeView.aspx.vb"
    Inherits="Maxiloan.Webform.SalesMarketing.AssetTypeView" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>AssetTypeView.aspx</title>
    <script language="JavaScript" type="text/javascript">
        function fClose() {
            window.close();
            return false;
        }				
    </script>
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../../Include/Buttons.css" type="text/css" />
<link rel="Stylesheet" href="../../../Include/General.css" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">                     
                <h3>
                    VIEW - JENIS ASSET
                </h3>
            </div>
        </div> 
        <asp:Panel ID="pnlAddEdit" runat="server">
        <div class="form_box">
	        <div class="form_single">
                <label>ID Jenis Asset</label>
                <asp:Label ID="lblAssetID" runat="server"></asp:Label>
	        </div>
        </div> 
        <div class="form_box">
	        <div class="form_single">
                <label>Nama Jenis Asset</label>
                <asp:Label ID="lblDescription" runat="server"></asp:Label>
	        </div>
        </div> 
        <div class="form_box">
	        <div class="form_single">
                <label>Label No Serial 1</label>
                <asp:Label ID="lblSerial1" runat="server"></asp:Label>
	        </div>
        </div> 
        <div class="form_box">
	        <div class="form_single">
                <label>Label No Serial 2</label>
                <asp:Label ID="lblSerial2" runat="server"></asp:Label>
	        </div>
        </div>                
        <div class="form_button">
            <asp:Button ID="ButtonClose" OnClientClick="fClose();" runat="server" text="Close"  CssClass ="small button gray"
                        CausesValidation="false"></asp:Button>
        </div>        
    </asp:Panel>
    </form>
</body>
</html>
