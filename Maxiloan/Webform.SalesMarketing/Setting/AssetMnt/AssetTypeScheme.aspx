﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AssetTypeScheme.aspx.vb"
    Inherits="Maxiloan.Webform.SalesMarketing.AssetTypeScheme" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>AssetTypeScheme</title>
    <link rel="Stylesheet" href="../../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../../Include/General.css" type="text/css" />
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;
        function OpenWinAssetTypeView(pAssetTypeID) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.SalesMarketing/Setting/AssetMnt/AssetTypeView.aspx?AssetTypeID=' + pAssetTypeID, 'AssetType', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=1');
        }

        function fback() {
            history.back(-1);
            return false;
        }	
    </script>
    <script language="JavaScript" type="text/javascript">
        function fBack() {
            history.back(-1);
            return false;
        }
        function DeleteConfirm() {
            if (confirm("Apakah yakin mau hapus data ini ?")) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:Label ID="lblMessage" runat="server" Visible ="false" ></asp:Label>                
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">                   
                <h3>
                    DAFTAR SKEMA ASSET
                </h3>
            </div>
        </div>                
        <asp:Panel ID="pnlList" runat="server">
        <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgPaging" runat="server" Width="100%" CssClass="grid_general" DataKeyField="AssetLevel"
                        BorderStyle="None" BorderWidth="0" OnSortCommand="Sorting" AutoGenerateColumns="False"
                        AllowSorting="True">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="EDIT">    
                                <ItemStyle CssClass="command_col"></ItemStyle>                            
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" ImageUrl="../../../Images/iconedit.gif"
                                        CommandName="Edit"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DELETE">
                                <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbDelete" runat="server" CausesValidation="False" ImageUrl="../../../Images/iconDelete.gif"
                                        CommandName="Delete"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="AssetLevel" SortExpression="AssetLevel" HeaderText="ID SKEMA">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="Description" SortExpression="Description" HeaderText="KETERANGAN">
                            </asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>               
                <div class="button_gridnavigation">
	                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../../Images/grid_navbutton01.png"
		                CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../../Images/grid_navbutton02.png"
		                CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../../Images/grid_navbutton03.png"
		                CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../../Images/grid_navbutton04.png"
		                CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
	                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
		                EnableViewState="False"></asp:Button>
	                <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage"
		                MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer"
		                CssClass="validator_general"></asp:RangeValidator>
	                <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
		                ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>
                <div class="label_gridnavigation">
	                <asp:Label ID="lblPage" runat="server"></asp:Label>of
	                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
	                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                </div>                    
            </div>         
        </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonAdd" runat="server" CausesValidation="False" Text="Add" CssClass ="small button blue">
            </asp:Button>                            
                                       
            <asp:Button ID="ButtonBackPage" runat="server" CausesValidation="False" Text="Back" CssClass ="small button gray">
            </asp:Button>
        </div>                        
        </asp:Panel>        
        <asp:Panel ID="pnlHeader" runat="server">
        <div class="form_box_header">
            <div class="form_single">              
                <h4>
                    SKEMA ASSET
                </h4>
            </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>ID Jenis Asset</label>
                <asp:HyperLink ID="hplAssetID" runat="server" Text='<%#request("id")%>'>
                </asp:HyperLink>
	        </div>
        </div> 
        <div class="form_box">
	        <div class="form_single">
                <label>Keterangan</label>
                <asp:Label ID="lblDescription" runat="server"></asp:Label>
	        </div>
        </div> 
        <div class="form_box">
	        <div class="form_single">
                <label>Cari Berdasarkan</label>
                <asp:DropDownList ID="cboSearch" runat="server">
                    <asp:ListItem Value="AssetLevel">Level Asset</asp:ListItem>
                    <asp:ListItem Value="Description">Keterangan</asp:ListItem>
                </asp:DropDownList>                    
                <asp:TextBox ID="txtSearch" runat="server" ></asp:TextBox>
	        </div>
        </div> 
        <div class="form_button">
            <asp:Button ID="ButtonSearch" runat="server" CausesValidation="False" CssClass ="small button blue" Text="Search">
            </asp:Button>
            <asp:Button ID="ButtonReset" runat="server" CausesValidation="False" Text="Reset" CssClass ="small button gray">
            </asp:Button>
        </div> 
        </asp:Panel>
        <asp:Panel ID="pnlAddEdit" runat="server">        
        <div class="form_box_header">
            <div class="form_single">              
                <h4>
                    SKEMA ASSET -
                    <asp:Label ID="lblTitleAddEdit" runat="server"></asp:Label>
                </h4>
            </div>
        </div>
           <div class="form_box">
            <div class="form_single">
                <label>
                    ID Jenis Asset
                </label>
                <asp:Label ID="lblJenisAssetID" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>ID Skema Asset</label>
                <asp:Label ID="lblAssetLevel" runat="server"></asp:Label>
	        </div>
        </div> 
        <div class="form_box">
	        <div class="form_single">
               <label class ="label_req">
			   Keterangan </label>
                <asp:Label ID="lblDescriptionAddEdit" runat="server"></asp:Label>
                <asp:TextBox ID="txtDescriptionAddEdit" runat="server" Width="464px" 
                    MaxLength="50"></asp:TextBox>
                <asp:Label ID="label" runat="server"></asp:Label>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" ControlToValidate="txtDescriptionAddEdit" CssClass="validator_general"
                    ErrorMessage="Harap isi Keterangan"></asp:RequiredFieldValidator>
	        </div>
        </div> 
        <div class="form_button">
            <asp:Button ID="ButtonSave" runat="server" CausesValidation="true" Text="Save" CssClass ="small button blue">
            </asp:Button>
            <asp:Button ID="ButtonCancel" runat="server" CausesValidation="false" Text="Cancel" CssClass ="small button gray">
            </asp:Button>
            <asp:Button ID="ButtonBack" runat="server" CausesValidation="false" Text="Back" CssClass ="small button gray">
            </asp:Button>
        </div>                               
    </asp:Panel>
    </form>
</body>
</html>
