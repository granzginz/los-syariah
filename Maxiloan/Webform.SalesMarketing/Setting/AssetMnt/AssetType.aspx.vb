﻿#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
#End Region

Public Class AssetType
    Inherits Maxiloan.Webform.WebBased

    Private m_controller As New AssetTypeController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private Property CmdWhere() As String
        Get
            Return CType(viewstate("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property AddEdit() As String
        Get
            Return CType(viewstate("vwsAddEdt"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("vwsAddEdt") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(viewstate("Sort"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Sort") = Value
        End Set
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If
        InitialDefaultPanel()
        Me.FormID = "AssetType"
        If Not Me.IsPostBack Then
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If SessionInvalid() Then
                    Exit Sub
                End If
                Me.SearchBy = ""
                Me.SortBy = ""
            End If
            txtPage.Text = "1"
            Me.Sort = "AssetTypeID ASC"
            If Request("cond") <> "" Then
                Me.CmdWhere = Request("cond")
            Else
                Me.CmdWhere = "ALL"

            End If
            BindGridEntity(Me.CmdWhere)
            If Request("cmd") = "dtl" Then
                BindDetail(Request("id"))
            End If
            'imbCancel.Attributes.Add("OnClick", "return fBack()")
        End If
    End Sub

    Sub BindGridEntity(ByVal cmdWhere As String)
        Dim dtEntity As DataTable
        Dim oAssetType As New Parameter.AssetType

        With oAssetType
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = Me.Sort
            .strConnection = GetConnectionString
        End With
        oAssetType = m_controller.GetAssetType(oAssetType)

        If Not oAssetType Is Nothing Then
            dtEntity = oAssetType.ListData
            recordCount = oAssetType.TotalRecords
        Else
            recordCount = 0
        End If
       
        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()
        PagingFooter()
    End Sub
    'Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
    '    Dim hynAssetTypeID As HyperLink
    '    If e.Item.ItemIndex >= 0 Then
    '        hynAssetTypeID = CType(e.Item.FindControl("hynAssetTypeID"), HyperLink)
    '        'hynAssetTypeID.Attributes.Add("Onclick", "javascript:OpenWinAssetTypeView('" & hynAssetTypeID.Text & "')")
    '        hynAssetTypeID.NavigateUrl = "javascript:OpenWinAssetTypeView('" & hynAssetTypeID.Text & "')"
    '    End If
    'End Sub

    Function LinkTo(ByVal strAssetTypeID As String) As String
        Return "javascript:OpenWinAssetTypeView('" & strAssetTypeID & "')"
    End Function

    'Function LinkTo() As String
    '    Return "javascript:OpenWinAssetTypeView()"
    'End Function



    Sub BindDetail(ByVal ID As String)
        Dim oAssetType As New Parameter.AssetType
        Me.AddEdit = "VIEW"
        pnlAddEdit.Visible = True
        pnlList.Visible = False
        If Request("cmd2") = "close" Then
            'imbBack.Visible = False
            'imbClose.Visible = True
        Else
            'imbBack.Visible = True
            'imbClose.Visible = False
        End If
        ButtonCancel.Visible = False
        ButtonSave.Visible = False

        lblTitleAddEdit.Text = Me.AddEdit
        oAssetType.Id = ID
        oAssetType.strConnection = GetConnectionString
        oAssetType = m_controller.GetAssetTypeEdit(oAssetType)

        lblAssetID.Visible = True
        txtAssetID.Visible = False
        lblDescription.Visible = True
        txtDescription.Visible = False
        txtSerial2.Visible = False
        lblSerial2.Visible = True
        txtSerial1.Visible = False
        lblSerial1.Visible = True

        lblAssetID.Text = ID
        lblDescription.Text = oAssetType.Description.Trim
        lblSerial1.Text = oAssetType.Serial1.Trim
        lblSerial2.Text = oAssetType.Serial2.Trim
    End Sub



#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then                        
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub imgbtnPageNumb_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int32)
                BindGridEntity(Me.CmdWhere)
            End If
        End If
    End Sub
#End Region

    Private Sub dtgEntity_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        Dim oAssetType As New Parameter.AssetType
        If e.CommandName = "Edit" Then
            If CheckFeature(Me.Loginid, Me.FormID, "EDIT", Me.AppId) Then
                Me.AddEdit = "EDIT"
                pnlAddEdit.Visible = True
                pnlList.Visible = False
                'imbBack.Visible = False
                ButtonCancel.Visible = True
                ButtonSave.Visible = True
                'imbClose.Visible = False            
                lblReqAssetTypeID.Visible = False
                lblReqDesc.Visible = True
                lblReqSerial1.Visible = True
                lblReqSerial2.Visible = True


                lblTitleAddEdit.Text = Me.AddEdit
                oAssetType.Id = dtgPaging.DataKeys.Item(e.Item.ItemIndex).ToString
                oAssetType.strConnection = GetConnectionString
                oAssetType = m_controller.GetAssetTypeEdit(oAssetType)
                lblAssetID.Visible = True
                txtAssetID.Visible = False
                lblDescription.Visible = False
                txtDescription.Visible = True
                txtSerial2.Visible = True
                lblSerial2.Visible = False
                txtSerial1.Visible = True
                lblSerial1.Visible = False
                lblAssetID.Text = dtgPaging.DataKeys.Item(e.Item.ItemIndex).ToString
                txtAssetID.Text = dtgPaging.DataKeys.Item(e.Item.ItemIndex).ToString
                txtDescription.Text = oAssetType.Description.Trim
                txtSerial1.Text = oAssetType.Serial1.Trim
                txtSerial2.Text = oAssetType.Serial2.Trim
            End If
        ElseIf e.CommandName = "Delete" Then
            If CheckFeature(Me.Loginid, Me.FormID, "DEL", Me.AppId) Then
                Dim customClass As New Parameter.AssetType
                With customClass
                    .Id = dtgPaging.DataKeys.Item(e.Item.ItemIndex).ToString
                    .strConnection = GetConnectionString
                End With

                Dim ResultOutput As String
                ResultOutput = m_controller.AssetTypeDelete(customClass)
                If ResultOutput = "OK" Then                    
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, false)
                Else                    
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_FAILED, True)
                End If
                Me.CmdWhere = "ALL"
                txtSearch.Text = ""
                BindGridEntity(Me.CmdWhere)
            End If
        End If
    End Sub

    Private Sub imbSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
        If Page.IsValid Then
            Dim customClass As New Parameter.AssetType
            Dim ErrMessage As String = ""
            If txtSerial1.Text.Trim = txtSerial2.Text.Trim Then                
                ShowMessage(lblMessage, "Label No Serial 1 harus berbeda dengan label No serial 2...", True)
                Exit Sub
            End If
            With customClass
                .Id = txtAssetID.Text
                .Description = txtDescription.Text
                .Serial1 = txtSerial1.Text
                .Serial2 = txtSerial2.Text
                .strConnection = GetConnectionString()
            End With

            If Me.AddEdit = "ADD" Then
                ErrMessage = m_controller.AssetTypeAdd(customClass)
                If ErrMessage <> "" Then                    
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_FAILED, True)
                    pnlAddEdit.Visible = True
                    pnlList.Visible = False
                    Exit Sub
                Else                    
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_SUCCESS, False)
                    txtSearch.Text = ""
                    Me.CmdWhere = "ALL"
                    BindGridEntity(Me.CmdWhere)
                End If
            ElseIf Me.AddEdit = "EDIT" Then
                m_controller.AssetTypeUpdate(customClass)                
                ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, True)
                txtSearch.Text = ""
                Me.CmdWhere = "ALL"
                BindGridEntity(Me.CmdWhere)
            End If
            txtPage.Text = "1"
        End If
    End Sub

    Private Sub imbAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonAdd.Click
        If CheckFeature(Me.Loginid, Me.FormID, "ADD", Me.AppId) Then
            pnlAddEdit.Visible = True
            pnlList.Visible = False
            'imbBack.Visible = False
            ButtonCancel.Visible = True
            ButtonSave.Visible = True
            'imbClose.Visible = False            
            lblReqAssetTypeID.Visible = True
            lblReqDesc.Visible = True
            lblReqSerial1.Visible = True
            lblReqSerial2.Visible = True


            Me.AddEdit = "ADD"
            lblTitleAddEdit.Text = Me.AddEdit

            lblAssetID.Visible = False
            txtAssetID.Visible = True
            lblDescription.Visible = False
            txtDescription.Visible = True
            txtSerial2.Visible = True
            lblSerial2.Visible = False
            txtSerial1.Visible = True
            lblSerial1.Visible = False
            txtAssetID.Text = ""
            txtDescription.Text = ""
            txtSerial1.Text = ""
            txtSerial2.Text = ""
        End If
    End Sub



    Sub SendCookies()
        Dim cookie As HttpCookie = Request.Cookies("AssetType")
        If Not cookie Is Nothing Then
            cookie.Values("SearchBy") = cboSearch.SelectedItem.Value
            cookie.Values("SearchText") = txtSearch.Text
            cookie.Values("PageFrom") = "AssetType"
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie("AssetType")
            cookieNew.Values.Add("SearchBy", cboSearch.SelectedItem.Value)
            cookieNew.Values.Add("SearchText", txtSearch.Text)
            cookieNew.Values.Add("PageFrom", "AssetType")
            Response.AppendCookie(cookieNew)
        End If
        'If Not cookie Is Nothing Then
        '    cookie.Values("AssetTypeID") = ""
        '    cookie.Values("Description") = ""
        '    cookie.Values("Page") = "AssetType"
        '    Response.AppendCookie(cookie)
        'Else
        '    Dim cookieNew As New HttpCookie("AssetType")
        '    cookieNew.Values.Add("AssetTypeID", "")
        '    cookieNew.Values.Add("Description", "")
        '    cookieNew.Values.Add("Page", "AssetType")
        '    Response.AppendCookie(cookieNew)
        'End If
    End Sub

    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub

    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        cboSearch.SelectedIndex = 0
        txtSearch.Text = ""
        txtPage.Text = "1"
        Me.CmdWhere = "ALL"
        BindGridEntity(Me.CmdWhere)
    End Sub

    Private Sub imbSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonSearch.Click
        If txtSearch.Text.Trim <> "" Then
            Me.CmdWhere = cboSearch.SelectedItem.Value + " = '" + txtSearch.Text.Trim + "'"
        Else
            Me.CmdWhere = "ALL"
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub

    Private Sub InitialDefaultPanel()
        pnlAddEdit.Visible = False
        pnlList.Visible = True        
    End Sub

    Private Sub imbCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        InitialDefaultPanel()
        txtSearch.Text = ""
        txtPage.Text = "1"
        Me.CmdWhere = "ALL"
        BindGridEntity(Me.CmdWhere)
    End Sub

    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        Dim imbDelete As ImageButton
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imbDelete"), ImageButton)
            imbDelete.Attributes.Add("Onclick", "return DeleteConfirm()")
        End If
    End Sub

End Class