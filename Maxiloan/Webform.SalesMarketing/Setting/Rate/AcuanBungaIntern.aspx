﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AcuanBungaIntern.aspx.vb" Inherits="Maxiloan.Webform.SalesMarketing.AcuanBungaIntern" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Acuan Bunga Internal</title>
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../../Include/Buttons.css" type="text/css" />
    <script type="text/javascript">

        function ChangeRateType(tipe) {
            document.getElementById('txtRateType').value = tipe
        }
        function ChangeUsedNew(tipe) {
            document.getElementById('txtUsedNew').value = tipe
        }
        function OpenUsage(u) {
            myWindow = window.open(u, '_blank', 'width=600,height=700, directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no,resizable=no')
            myWindow.focus()
        }
        function hideMessage() {
            document.getElementById('lblMessage').style.display = 'none';
        }	
    </script>

</head>
<body>

    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
         <ContentTemplate>
           <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage()"></asp:Label>
           <div class="form_title">
               <div class="title_strip">
               </div>
               <div class="form_single">
                   <h3>
                       DAFTAR ACUAN MARGIN INTERNAL
                   </h3>
               </div>
           </div>
           <asp:Panel ID="pnlList" runat="server" Width="100%">
               <div class="form_box_header">
                   <div class="form_single">
                       <div class="grid_wrapper_ns">
                           <asp:DataGrid ID="dtgPaging" runat="server" Width="100%" CssClass="grid_general"
                               DataKeyField="AcuanID" BorderStyle="None" BorderWidth="0" OnSortCommand="Sorting"
                               AutoGenerateColumns="False" AllowSorting="True">
                               <HeaderStyle CssClass="th" HorizontalAlign="Center" VerticalAlign="Middle" />
                               <ItemStyle CssClass="item_grid" HorizontalAlign="Center" />
                               <Columns>
                                   <asp:TemplateColumn HeaderText="EDIT">
                                       <ItemStyle CssClass="command_col"></ItemStyle>
                                       <ItemTemplate>
                                           <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" ImageUrl="../../../Images/iconedit.gif"
                                               CommandName="Edit"></asp:ImageButton>
                                       </ItemTemplate>
                                   </asp:TemplateColumn>
                                   <asp:TemplateColumn HeaderText="DELETE">
                                       <ItemStyle CssClass="command_col"></ItemStyle>
                                       <ItemTemplate>
                                           <asp:ImageButton ID="imgDelete" runat="server" CausesValidation="False" ImageUrl="../../../Images/icondelete.gif"
                                               CommandName="Delete"></asp:ImageButton>
                                       </ItemTemplate>
                                   </asp:TemplateColumn>
                                  <asp:TemplateColumn Visible="true" SortExpression="AcuanID" HeaderText="ID">
                                        <ItemTemplate>
                                            <asp:Label ID="AcuanID" runat="server" Text='<%# DataBinder.eval(Container,"DataItem.AcuanID") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                   <asp:TemplateColumn Visible="True" SortExpression="Description" HeaderText="NAMA">
                                    <%--<ItemStyle CssClass="grid_wrapper_ws" />--%>
                                        <ItemTemplate>
                                            <asp:Label ID="Description" runat="server" Text='<%# DataBinder.eval(Container,"DataItem.Description") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn Visible="True" DataField="JenisKreditText" SortExpression="JenisKreditText" HeaderText="JENIS PEMBIAYAAN">
                                   </asp:BoundColumn>
                                    <asp:TemplateColumn Visible="false" SortExpression="JenisKredit" HeaderText="JENIS PEMBIAYAAN">
                                        <ItemTemplate>
                                            <asp:Label ID="JenisKredit" runat="server" Text='<%# DataBinder.eval(Container,"DataItem.JenisKredit") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn Visible="True" DataField="AssetCategory" SortExpression="AssetCategory" HeaderText="ASSET CATEGORY">
                                   </asp:BoundColumn>
                                    <asp:TemplateColumn Visible="false" SortExpression="InsRateCategoryID" HeaderText="ASSET CATEGORY">
                                        <ItemTemplate>
                                            <asp:Label ID="InsRateCategoryID" runat="server" Text='<%# DataBinder.eval(Container,"DataItem.InsRateCategoryID") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                   <asp:BoundColumn Visible="True" DataField="UsedNewDesc" SortExpression="UsedNew" HeaderText="USED/NEW">
                                   </asp:BoundColumn>
                                   <asp:TemplateColumn Visible="false" SortExpression="UsedNew" HeaderText="Used/New">
                                        <ItemTemplate>
                                            <asp:Label ID="UsedNew" runat="server" Text='<%# DataBinder.eval(Container,"DataItem.UsedNew") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                   <asp:BoundColumn Visible="True" DataField="ManfcYear" SortExpression="ManfcYear" HeaderText="MANUFACTURING YEAR">
                                   </asp:BoundColumn>
                                   <asp:TemplateColumn Visible="false" SortExpression="ManfcYearFrom" HeaderText="ManfcYearFrom">
                                        <ItemTemplate>
                                            <asp:Label ID="ManfcYearFrom" runat="server" Text='<%# DataBinder.eval(Container,"DataItem.ManfcYearFrom") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn Visible="false" SortExpression="ManfcYearTo" HeaderText="ManfcYearTo">
                                        <ItemTemplate>
                                            <asp:Label ID="ManfcYearTo" runat="server" Text='<%# DataBinder.eval(Container,"DataItem.ManfcYearTo") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                   <asp:BoundColumn Visible="True" DataField="RateTypeDesc" SortExpression="RateTypeDesc" HeaderText="RATE TYPE">
                                   </asp:BoundColumn>
                                   <asp:TemplateColumn Visible="false" SortExpression="RateType" HeaderText="Rate Type">
                                        <ItemTemplate>
                                            <asp:Label ID="RateType" runat="server" Text='<%# DataBinder.eval(Container,"DataItem.RateType") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                   <asp:TemplateColumn Visible="True" SortExpression="Tenor" HeaderText="TENOR(THN)">
                                        <ItemTemplate>
                                            <asp:Label ID="Tenor" runat="server" Text='<%# DataBinder.eval(Container,"DataItem.Tenor") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn Visible="True" SortExpression="Rate" HeaderText="Rate(%)">
                                        <ItemTemplate>
                                            <asp:Label ID="Rate" runat="server" Text='<%# DataBinder.eval(Container,"DataItem.Rate") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                   <asp:TemplateColumn HeaderText="ASSET USAGE" Visible="false">
                                        <ItemTemplate>
                                              <asp:HyperLink ID="AssetUsage" runat="server">ASSET USAGE</asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="ASSET CATEGORY"  Visible="false">
                                        <ItemTemplate>
                                              <asp:HyperLink ID="AssetCategory" runat="server">ASSET CATEGORY</asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="ASSET LIST" >
                                        <ItemTemplate>
                                              <asp:HyperLink ID="AssetList" runat="server">ASSET LIST</asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                   
                               </Columns>
                           </asp:DataGrid>
                           <div class="button_gridnavigation">
                               <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../../Images/grid_navbutton01.png"
                                   CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                               </asp:ImageButton>
                               <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../../Images/grid_navbutton02.png"
                                   CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                               </asp:ImageButton>
                               <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../../Images/grid_navbutton03.png"
                                   CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                               </asp:ImageButton>
                               <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../../Images/grid_navbutton04.png"
                                   CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                               </asp:ImageButton>
                               <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                               <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                                   EnableViewState="False"></asp:Button>
                               <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" MinimumValue="1"
                                   ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"></asp:RangeValidator>
                               <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                                   ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                           </div>
                           <div class="label_gridnavigation">
                               <asp:Label ID="lblPage" runat="server"></asp:Label>of
                               <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                               <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                           </div>
                       </div>
                   </div>
               </div>
               <div class="form_button">
                   <asp:Button ID="imbButtonAdd" runat="server" CausesValidation="False" Text="Add"
                       CssClass="small button blue"></asp:Button>
                   <asp:Button ID="imbButtonPrint" runat="server" Text="Print" CssClass="small button blue">
                   </asp:Button>
               </div>
               <div class="form_box_title">
                   <div class="form_single">
                       <h5>
                           CARI MASTER ACUAN MARGIN INTERNAL
                       </h5>
                   </div>
               </div>
               <div class="form_box">
                   <div class="form_single">
                       <label>
                           Cari Berdasarkan</label>
                       <asp:DropDownList ID="cboSearch" runat="server">
                           <asp:ListItem Value="ID">ID</asp:ListItem>
                           <asp:ListItem Value="Description">Nama</asp:ListItem>
                       </asp:DropDownList>
                       <asp:TextBox ID="txtSearch" runat="server" Width="20%"></asp:TextBox>
                   </div>
               </div>
               <div class="form_box">
                    <div class="form_single">
                        <label>Tenor</label>
                       <asp:DropDownList runat="server" ID="cboTenorSearch"> 
                            <asp:ListItem Value="">Select One</asp:ListItem>
                            <asp:ListItem Value="1">1</asp:ListItem>
                            <asp:ListItem Value="2">2</asp:ListItem>
                            <asp:ListItem Value="3">3</asp:ListItem>
                            <asp:ListItem Value="4">4</asp:ListItem>
                            <asp:ListItem Value="5">5</asp:ListItem>
                       </asp:DropDownList>
                    </div>
               </div>
               <div class="form_box">
                    <div class="form_single">
                        <label>Jenis Pembiayaan</label>
                       <asp:DropDownList runat="server" ID="cboJenisKreditSearch"> 
                            <asp:ListItem Value="">Select One</asp:ListItem>
                            <asp:ListItem Value="AFI">AFI</asp:ListItem>
                            <asp:ListItem Value="NK">NASMOCO</asp:ListItem>
                       </asp:DropDownList>
                    </div>
               </div>
               <div class="form_box">
                    <div class="form_single">
                        <label>Asset Kategori</label>
                       <asp:DropDownList runat="server" ID="cboAssetKategoriSearch"> 
                       </asp:DropDownList>
                    </div>
               </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>Used / New</label>
                       <asp:DropDownList runat="server" ID="cboUsedNewSearch"> 
                            <asp:ListItem Value="">Select One</asp:ListItem>
                            <asp:ListItem Value="U">Used</asp:ListItem>
                            <asp:ListItem Value="N">New</asp:ListItem>
                       </asp:DropDownList>
                    </div>
               </div>
               <div class="form_box">
                    <div class="form_single">
                        <label>Rate Type</label>
                       <asp:DropDownList runat="server" ID="cboRateTypeSearch"> 
                            <asp:ListItem Value="">Select One</asp:ListItem>
                            <asp:ListItem Value="L">Low</asp:ListItem>
                            <asp:ListItem Value="M">Medium</asp:ListItem>
                       </asp:DropDownList>
                    </div>
               </div>
               <div class="form_button">
                   <asp:Button ID="imbButtonSearch" runat="server" CausesValidation="False" CssClass="small button blue"
                       Text="Search"></asp:Button>
                   <asp:Button ID="imbButtonReset" runat="server" CausesValidation="False" Text="Reset"
                       CssClass="small button gray"></asp:Button>
               </div>
           </asp:Panel>
           <asp:Panel ID="pnlAddEdit" runat="server" Visible="False">
                <div class="form_box_title">
                    <div class="form_single">
                        <h4>
                            ACUAN MARGIN INTERNAL -
                            <asp:Label ID="lblAddEdit" runat="server"></asp:Label>
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            ID Acuan&nbsp;</label>
                        <asp:TextBox ID="txtAcuanID" runat="server"  MaxLength="10"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rgvAcuanID" runat="server" ControlToValidate="txtAcuanID"
                            CssClass="validator_general" ErrorMessage="Harap isi dengan ID Acuan"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label> Nama&nbsp;</label>
                        <asp:TextBox ID="txtNama" runat="server" Width="250px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rgvNama" runat="server" ControlToValidate="txtNama"
                            CssClass="validator_general" ErrorMessage="Harap isi dengan Nama Description"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label> Jenis Pembiayaan&nbsp;</label>
                        <asp:DropDownList ID="cboJenisKredit" runat="server" style="width:160px">
                            <asp:ListItem Value="AFI" Selected="True">AFI</asp:ListItem>
                            <asp:ListItem Value="NK">NASMOCO PEMBIAYAAN</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label> Kategori Asset&nbsp;</label>
                        <asp:DropDownList ID="cboKategoriAsset" runat="server" style="width:160px">
                        </asp:DropDownList>
                         <asp:RequiredFieldValidator ID="rgvKategoriAsset" runat="server" ControlToValidate="cboKategoriAsset"
                            CssClass="validator_general" ErrorMessage="Pilih dulu kategory asset"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>Asset Used / New&nbsp;</label>
                        <asp:RadioButton ID="rdusednewU" runat="server" GroupName="usednew" onClick="ChangeUsedNew('U')" />Used
                        <asp:RadioButton ID="rdusednewN" runat="server" GroupName="usednew" onClick="ChangeUsedNew('N')" />New
                        <asp:textbox ID="txtUsedNew" runat="server" style="display:none"></asp:textbox>
                        <asp:RequiredFieldValidator ID="rvqusednew" runat="server" ControlToValidate="txtUsedNew"
                            CssClass="validator_general" ErrorMessage="Harap pilih Used / New"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label> Manufacturing Year&nbsp;</label>
                        <asp:TextBox ID="txtManufacturingYearFrom" runat="server" size="10" onkeypress="return numbersonly2(event)" placeholder="YYYY"></asp:TextBox> to
                        <asp:TextBox ID="txtManufacturingYearTo" runat="server" size="10" onkeypress="return numbersonly2(event)" placeholder="YYYY"></asp:TextBox>
                        <asp:Label ID="lblManufaturing" runat="server" Visible="false"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label> Tenor&nbsp;</label>
                        <asp:DropDownList ID="cboTenor" runat="server" style="width:55px">
                            <asp:ListItem Value="1" Selected="True">1</asp:ListItem>
                            <asp:ListItem Value="2">2</asp:ListItem>
                            <asp:ListItem Value="3">3</asp:ListItem>
                            <asp:ListItem Value="4">4</asp:ListItem>
                            <asp:ListItem Value="5">5</asp:ListItem>
                        </asp:DropDownList>
                         Thn
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label> Rate Type&nbsp;</label>
                        <asp:RadioButton ID="rdRateTypeM" runat="server" GroupName="RateType" onClick="ChangeRateType('M')" />Medium
                        <asp:RadioButton ID="rdRateTypeL" runat="server" GroupName="RateType" onClick="ChangeRateType('L')" />Low
                        <asp:textbox ID="txtRateType" runat="server" style="display:none"></asp:textbox>
                        <asp:RequiredFieldValidator ID="rqvRateType" runat="server" ControlToValidate="txtRateType"
                            CssClass="validator_general" ErrorMessage="Harap pilih Rate Type"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>Rate &nbsp;</label>
                        <asp:TextBox ID="txtRate" runat="server" size="5" onkeypress="return decimalonly(event)"></asp:TextBox> %
                        <asp:RequiredFieldValidator ID="rgvRate" runat="server" ControlToValidate="txtRate"
                            CssClass="validator_general" ErrorMessage="Harap isi dengan Rate"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator Display = "Dynamic" ControlToValidate = "txtRate" CssClass="validator_general" ID="rgxDecimal" 
                        ValidationExpression = "^[0-9]{1,11}(?:\.[0-9]{1,3})?$" runat="server" ErrorMessage="Penulisan angka desimal salah."></asp:RegularExpressionValidator>

                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonSave" runat="server" Text="Save" CssClass="small button blue"
                        CausesValidation="true"></asp:Button>&nbsp;
                    <asp:Button ID="ButtonCancel" runat="server" Text="Cancel" CssClass="small button gray"
                        CausesValidation="false"></asp:Button>&nbsp;
                </div>
           </asp:Panel>
         </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>