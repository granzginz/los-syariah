﻿#Region "Imports"
Imports Maxiloan.cbse
Imports Maxiloan.Exceptions
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
#End Region

Public Class AcuanAssetList

    Inherits Maxiloan.Webform.WebBased

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Dim m_controller As New AcuanBungaInternController
    Dim PAcuan As New Parameter.PAcuanBungaIntern
    Dim ManYearFrom As String = ""
    Dim ManYearTo As String = ""

    Private Property AcuanID() As String
        Get
            Return CType(ViewState("AcuanID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("AcuanID") = Value
        End Set
    End Property
    Private Property CurrentPageAcuan() As Integer
        Get
            Return CType(ViewState("CurrentPageAcuan"), Integer)
        End Get
        Set(ByVal Value As Integer)
            ViewState("CurrentPageAcuan") = Value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim dtTmp As New DataTable

        If Not Me.IsPostBack Then
            If SessionInvalid() Then
                Exit Sub
            End If

            Me.AcuanID = Request.QueryString("AcuanID")
            Me.CurrentPageAcuan = Request.QueryString("currentPage")

            With PAcuan
                .strConnection = GetConnectionString()
                .WhereCond = " Acuanid = '" & Me.AcuanID & "'"
                .CurrentPage = currentPage
                .PageSize = pageSize
                .SortBy = SortBy
            End With

            PAcuan = m_controller.GetListing(PAcuan)
            If Not PAcuan Is Nothing Then
                dtTmp = PAcuan.ListData

                lblAcuanID.Text = dtTmp.Rows(0).Item("AcuanID").ToString.Trim
                lblNama.Text = dtTmp.Rows(0).Item("Description").ToString.Trim

                If dtTmp.Rows(0).Item("UsedNew").ToString.Trim = "U" Then
                    rdusednewU.Checked = True
                ElseIf dtTmp.Rows(0).Item("UsedNew").ToString.Trim = "N" Then
                    rdusednewN.Checked = True
                End If
                If dtTmp.Rows(0).Item("RateType").ToString.Trim = "L" Then
                    rdRateTypeL.Checked = True
                ElseIf dtTmp.Rows(0).Item("RateType").ToString.Trim = "M" Then
                    rdRateTypeM.Checked = True
                End If

                rdRateTypeL.Enabled = False
                rdRateTypeM.Enabled = False
                rdusednewN.Enabled = False
                rdusednewU.Enabled = False

                If dtTmp.Rows(0).Item("ManfcYearFrom").ToString.Trim = 0 Then
                    txtManufacturingYearFrom.Text = ""
                    Me.ManYearFrom = ""
                Else
                    txtManufacturingYearFrom.Text = dtTmp.Rows(0).Item("ManfcYearFrom").ToString.Trim
                    Me.ManYearFrom = dtTmp.Rows(0).Item("ManfcYearFrom").ToString.Trim
                End If

                If dtTmp.Rows(0).Item("ManfcYearTo").ToString.Trim = 0 Then
                    txtManufacturingYearTo.Text = ""
                    Me.ManYearTo = ""
                Else
                    txtManufacturingYearTo.Text = dtTmp.Rows(0).Item("ManfcYearTo").ToString.Trim
                    Me.ManYearTo = dtTmp.Rows(0).Item("ManfcYearTo").ToString.Trim
                End If

                txtJenisKredit.Text = dtTmp.Rows(0).Item("JenisKreditText").ToString.Trim
                txtKategoriAsset.Text = dtTmp.Rows(0).Item("AssetCategory").ToString.Trim

                txtRate.Text = dtTmp.Rows(0).Item("Rate").ToString.Trim

                cboTenor.SelectedIndex = cboTenor.Items.IndexOf(cboTenor.Items.FindByValue(dtTmp.Rows(0).Item("Tenor").ToString.Trim))
                cboTenor.Enabled = False

            End If

            LoadUsage()
        End If
    End Sub
    Private Sub LoadUsage()
        Dim DtUsage As New DataTable

        PAcuan.strConnection = GetConnectionString()
        PAcuan.AcuanID = Me.AcuanID

        DtUsage = m_controller.GetListingUCL(PAcuan, "L")
        dtgPaging.DataSource = DtUsage.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()
    End Sub

    Protected Sub ButtonSave_Click(sender As Object, e As EventArgs) Handles ButtonSave.Click
        Dim ChkUsage As CheckBox
        Dim ArrID As New ArrayList
        Dim err As String
        Dim idusage As String

        For i As Integer = 0 To dtgPaging.Items.Count - 1
            ChkUsage = CType(dtgPaging.Items(i).FindControl("chkidusage"), CheckBox)
            idusage = CType(dtgPaging.Items(i).FindControl("ID"), Label).Text
            If ChkUsage.Checked = True Then
                ArrID.Add(idusage & "|" & CType(dtgPaging.Items(i).FindControl("txtManfcYearFrom"), TextBox).Text & "|" & CType(dtgPaging.Items(i).FindControl("txtManfcYearTo"), TextBox).Text)
            End If
        Next

        'If ArrID.Count <= 0 Then
        '    ShowMessage(lblMessage, "Warning: Belum ada Asset List yang di pilih", True)
        'Else
        With PAcuan
            .AcuanID = Me.AcuanID
            .strConnection = GetConnectionString()
            .ArrUsage = ArrID
        End With

        err = m_controller.UpdateAssetAcuan(PAcuan, "L")
        ShowMessage(lblMessage, "Pesan: Update Acuan Bunga terhadap master berhasil", False)

        'End If
    End Sub

    Protected Sub ButtonCancel_Click(sender As Object, e As EventArgs) Handles ButtonCancel.Click
        'ScriptManager.RegisterClientScriptBlock(Page, GetType(Page), "close", "window.close();", True)
        Response.Redirect("AcuanBungaIntern.aspx?currentPage=" & Me.CurrentPageAcuan)
    End Sub
    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        Dim ChkID As New CheckBox
        Dim lblStatus As String
        

        If e.Item.ItemIndex >= 0 Then
            ChkID = CType(e.Item.FindControl("chkidusage"), CheckBox)
            lblStatus = CType(e.Item.FindControl("status"), Label).Text
            CType(e.Item.FindControl("txtManfcYearFrom"), TextBox).Text = ManYearFrom
            CType(e.Item.FindControl("txtManfcYearTo"), TextBox).Text = ManYearTo

            If CType(e.Item.FindControl("ManfcYearFrom"), Label).Text <> "" Then
                CType(e.Item.FindControl("txtManfcYearFrom"), TextBox).Text = CType(e.Item.FindControl("ManfcYearFrom"), Label).Text
            End If

            If CType(e.Item.FindControl("ManfcYearTo"), Label).Text <> "" Then
                CType(e.Item.FindControl("txtManfcYearTo"), TextBox).Text = CType(e.Item.FindControl("ManfcYearTo"), Label).Text
            Else

            End If

            If lblStatus = "1" Then
                ChkID.Checked = True
            End If
        End If

    End Sub

End Class