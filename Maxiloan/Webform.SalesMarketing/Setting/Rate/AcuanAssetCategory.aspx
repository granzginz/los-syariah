﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AcuanAssetCategory.aspx.vb" Inherits="Maxiloan.Webform.SalesMarketing.AcuanAssetCategory" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Acuan Bunga Internal</title>
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../../Include/Buttons.css" type="text/css" />
    <script type='text/javascript'>
        function CloseWindow() {
            window.close()
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
   <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
         <ContentTemplate>
           <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
           <div class="form_title">
               <div class="title_strip">
               </div>
               <div class="form_single">
                   <h3>
                       ACUAN MARGIN ASSET USAGE
                   </h3>
               </div>
           </div>
           <div class="form_box">
                    <div class="form_single">
                        <label>
                            ID Acuan&nbsp;</label>
                        <asp:TextBox ID="txtAcuanID" runat="server"  MaxLength="10" readonly></asp:TextBox>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label> Nama&nbsp;</label>
                        <asp:TextBox ID="txtNama" runat="server" Width="250px" readonly></asp:TextBox>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label> Used / New&nbsp;</label>
                        <asp:RadioButton ID="rdusednewU" runat="server" GroupName="usednew" onClick="ChangeUsedNew('U')" readonly />Used
                        <asp:RadioButton ID="rdusednewN" runat="server" GroupName="usednew" onClick="ChangeUsedNew('N')" readonly />New
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label> Manufacturing Year&nbsp;</label>
                        <asp:TextBox ID="txtManufacturingYearFrom" runat="server" size="10" placeholder="YYYY"  readonly></asp:TextBox> to
                        <asp:TextBox ID="txtManufacturingYearTo" runat="server" size="10" placeholder="YYYY"  readonly></asp:TextBox>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label> Tenor (Thn)&nbsp;</label>
                        <asp:DropDownList ID="cboTenor" runat="server" style="width:55px"  readonly>
                            <asp:ListItem Value="1" Selected="True">1</asp:ListItem>
                            <asp:ListItem Value="2">2</asp:ListItem>
                            <asp:ListItem Value="3">3</asp:ListItem>
                            <asp:ListItem Value="4">4</asp:ListItem>
                            <asp:ListItem Value="5">5</asp:ListItem>
                        </asp:DropDownList>

                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label> Rate Type&nbsp;</label>
                        <asp:RadioButton ID="rdRateTypeM" runat="server" GroupName="RateType" onClick="ChangeRateType('M')"  enable="false" />Medium
                        <asp:RadioButton ID="rdRateTypeL" runat="server" GroupName="RateType" onClick="ChangeRateType('L')" enable="false" />Low
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>Rate&nbsp;</label>
                        <asp:TextBox ID="txtRate" runat="server" size="5" readonly></asp:TextBox>  (%)
                    </div>
                </div>
                <div class="form_box_header">
                   <div class="form_single">
                       <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgPaging" runat="server" Width="50%" CssClass="grid_general"
                               DataKeyField="ID" BorderStyle="None" BorderWidth="0"
                               AutoGenerateColumns="False" AllowSorting="True">
                               <HeaderStyle CssClass="th" />
                               <ItemStyle CssClass="item_grid" />
                               <Columns>
                                    <asp:TemplateColumn>
                                        <ItemStyle CssClass="command_col"></ItemStyle>
                                        <ItemTemplate>
                                           <asp:CheckBox ID="chkidusage" runat="server" />
                                        </ItemTemplate>
                                   </asp:TemplateColumn>
                                   <asp:TemplateColumn Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="status" runat="server" Text='<%# DataBinder.eval(Container,"DataItem.status") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="ID">
                                        <ItemTemplate>
                                           <asp:Label ID="ID" runat="server"  Text='<%# DataBinder.eval(Container,"DataItem.id") %>'></asp:Label>
                                        </ItemTemplate>
                                   </asp:TemplateColumn>
                                   <asp:TemplateColumn HeaderText="Nama">
                                        <ItemTemplate>
                                           <asp:Label ID="Description" runat="server"  Text='<%# DataBinder.eval(Container,"DataItem.Description") %>'></asp:Label>
                                        </ItemTemplate>
                                   </asp:TemplateColumn>
                               </Columns>
                 </asp:DataGrid>
                        </div>
                        <div class="form_button">
                            <asp:Button ID="ButtonSave" runat="server" Text="Save" CssClass="small button blue"
                                CausesValidation="true"></asp:Button>&nbsp;
                            <asp:Button ID="ButtonCancel" runat="server" Text="Cancel" CssClass="small button gray"
                                CausesValidation="false"></asp:Button>&nbsp;
                        </div>
                    </div>
                 </div>
         </ContentTemplate>
     </asp:UpdatePanel>
    </form>
</body>
</html>