﻿#Region "Imports"
Imports Maxiloan.cbse
Imports Maxiloan.Exceptions
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
#End Region

Public Class AcuanAssetUsage

    Inherits Maxiloan.Webform.WebBased

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1


    Dim m_controller As New AcuanBungaInternController
    Dim PAcuan As New Parameter.PAcuanBungaIntern
    Private Property AcuanID() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim dtTmp As New DataTable

        If Not Me.IsPostBack Then
            If SessionInvalid() Then
                Exit Sub
            End If

            Me.AcuanID = Request.QueryString("AcuanID")
            LoadUsage()
            With PAcuan
                .strConnection = GetConnectionString()
                .WhereCond = " Acuanid = '" & Me.AcuanID & "'"
                .CurrentPage = currentPage
                .PageSize = pageSize
                .SortBy = SortBy
            End With

            PAcuan = m_controller.GetListing(PAcuan)
            If Not PAcuan Is Nothing Then
                dtTmp = PAcuan.ListData
                txtAcuanID.Text = dtTmp.Rows(0).Item("AcuanID").ToString.Trim
                txtNama.Text = dtTmp.Rows(0).Item("Description").ToString.Trim

                If dtTmp.Rows(0).Item("UsedNew").ToString.Trim = "U" Then
                    rdusednewU.Checked = True
                ElseIf dtTmp.Rows(0).Item("UsedNew").ToString.Trim = "N" Then
                    rdusednewN.Checked = True
                End If
                If dtTmp.Rows(0).Item("RateType").ToString.Trim = "L" Then
                    rdRateTypeL.Checked = True
                ElseIf dtTmp.Rows(0).Item("RateType").ToString.Trim = "M" Then
                    rdRateTypeM.Checked = True
                End If

                rdRateTypeL.Enabled = False
                rdRateTypeM.Enabled = False
                rdusednewN.Enabled = False
                rdusednewU.Enabled = False

                If dtTmp.Rows(0).Item("ManfcYearFrom").ToString.Trim = 0 Then
                    txtManufacturingYearFrom.Text = ""
                Else
                    txtManufacturingYearFrom.Text = dtTmp.Rows(0).Item("ManfcYearFrom").ToString.Trim
                End If

                If dtTmp.Rows(0).Item("ManfcYearTo").ToString.Trim = 0 Then
                    txtManufacturingYearTo.Text = ""
                Else
                    txtManufacturingYearTo.Text = dtTmp.Rows(0).Item("ManfcYearTo").ToString.Trim
                End If


                txtRate.Text = dtTmp.Rows(0).Item("Rate").ToString.Trim

                cboTenor.SelectedIndex = cboTenor.Items.IndexOf(cboTenor.Items.FindByValue(dtTmp.Rows(0).Item("Tenor").ToString.Trim))
                cboTenor.Enabled = False

            End If


        End If
    End Sub
    Private Sub LoadUsage()
        Dim DtUsage As New DataTable

        PAcuan.strConnection = GetConnectionString()
        PAcuan.AcuanID = Me.AcuanID

        DtUsage = m_controller.GetListingUCL(PAcuan, "U")
        dtgPaging.DataSource = DtUsage.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()
    End Sub

    Protected Sub ButtonSave_Click(sender As Object, e As EventArgs) Handles ButtonSave.Click
        Dim ChkUsage As CheckBox
        Dim ArrID As New ArrayList
        Dim err As String
        Dim idusage As String

        For i As Integer = 0 To dtgPaging.Items.Count - 1
            ChkUsage = CType(dtgPaging.Items(i).FindControl("chkidusage"), CheckBox)
            idusage = CType(dtgPaging.Items(i).FindControl("ID"), Label).Text
            If ChkUsage.Checked = True Then
                ArrID.Add(idusage)
            End If
        Next

        If ArrID.Count <= 0 Then
            ShowMessage(lblMessage, "Warning: Belum ada Asset Usage yang di pilih", True)
        Else
            With PAcuan
                .AcuanID = Me.AcuanID
                .strConnection = GetConnectionString()
                .ArrUsage = ArrID
            End With

            err = m_controller.UpdateAssetAcuan(PAcuan, "U")

            ScriptManager.RegisterClientScriptBlock(Page, GetType(Page), "close", "window.close();", True)
        End If
    End Sub

    Protected Sub ButtonCancel_Click(sender As Object, e As EventArgs) Handles ButtonCancel.Click
        ScriptManager.RegisterClientScriptBlock(Page, GetType(Page), "close", "window.close();", True)
    End Sub
    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        Dim ChkID As New CheckBox
        Dim lblStatus As String

        If e.Item.ItemIndex >= 0 Then
            ChkID = CType(e.Item.FindControl("chkidusage"), CheckBox)
            lblStatus = CType(e.Item.FindControl("status"), Label).Text

            If lblStatus = "1" Then
                ChkID.Checked = True
            End If
        End If

    End Sub
End Class