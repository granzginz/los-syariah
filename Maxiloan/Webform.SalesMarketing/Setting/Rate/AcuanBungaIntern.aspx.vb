﻿#Region "Imports"
Imports Maxiloan.cbse
Imports Maxiloan.Exceptions
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
#End Region


Public Class AcuanBungaIntern
    Inherits Maxiloan.Webform.WebBased



#Region "Constanta"
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Dim m_acuan As New AcuanBungaInternController
    Dim oRow As DataRow
#End Region
#Region "Property"
    Private Property AddEdit() As String
        Get
            Return CType(ViewState("AddEdit"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("AddEdit") = Value
        End Set
    End Property
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property
#End Region
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        Me.FormID = "RATEINTERN"

        If Not Me.IsPostBack Then

            If Me.IsHoBranch = False Then
                ShowMessage(lblMessage, "Hanya kantor pusat", True)
                pnlList.Visible = False
                Exit Sub
            End If

            If CheckForm(Me.Loginid, "RATEINTERN", Me.AppId) Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If

            If Not Request.QueryString("currentPage") Is Nothing Then
                Me.currentPage = Request.QueryString("currentPage")
                txtPage.Text = Request.QueryString("currentPage")
            End If

            BindGridEntity(Me.CmdWhere)

            GenerateCombo(cboKategoriAsset, "spShadowInsuranceRateCategory", "Description", "InsRateCategoryID")
            GenerateCombo(cboAssetKategoriSearch, "spShadowInsuranceRateCategory", "Description", "InsRateCategoryID")
        End If
    End Sub
    Public Sub BindGridEntity(ByVal where As String)
        Dim TAcuan As New DataTable
        Dim oAcuan As New Parameter.PAcuanBungaIntern
        Dim m_controller As New Controller.AcuanBungaInternController

        With oAcuan
            .strConnection = GetConnectionString()
            .WhereCond = where
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oAcuan = m_controller.GetListing(oAcuan)

        If Not TAcuan Is Nothing Then
            TAcuan = oAcuan.ListData
            recordCount = oAcuan.RecordCount
        Else
            recordCount = 0
        End If

        dtgPaging.DataSource = TAcuan.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()


        PagingFooter()
    End Sub
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub
#Region "Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub imgbtnPageNumb_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int32)
                BindGridEntity(Me.CmdWhere)
            End If
        End If
    End Sub
#End Region
    Private Sub imbAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles imbButtonAdd.Click
        If CheckFeature(Me.Loginid, Me.FormID, "ADD", Me.AppId) Then
            pnlAddEdit.Visible = True
            pnlList.Visible = False
            Me.AddEdit = "ADD"

            lblAddEdit.Text = Me.AddEdit
            lblMessage.Visible = False
        End If
    End Sub
    Private Sub imbButtonSave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
        Dim pAcuanBunga As New Parameter.PAcuanBungaIntern
        Dim result As String
        Dim Tdooble As String = ""

        pAcuanBunga.AcuanID = txtAcuanID.Text.Trim
        pAcuanBunga.Description = txtNama.Text.Trim
        pAcuanBunga.UsedNew = txtUsedNew.Text.Trim
        pAcuanBunga.ManfcYearFrom = txtManufacturingYearFrom.Text
        pAcuanBunga.ManfcYearTo = txtManufacturingYearTo.Text
        pAcuanBunga.RateType = txtRateType.Text
        pAcuanBunga.Tenor = cboTenor.SelectedValue
        pAcuanBunga.Rate = txtRate.Text
        pAcuanBunga.AddEdit = Me.AddEdit
        pAcuanBunga.JenisKredit = cboJenisKredit.SelectedValue.Trim
        pAcuanBunga.KategoriAsset = cboKategoriAsset.SelectedValue.Trim

        pAcuanBunga.strConnection = GetConnectionString()

        result = m_acuan.SaveUpdate(pAcuanBunga)
        If result = "Success" Then
            ShowMessage(lblMessage, Me.AddEdit & " data berhasil", False)
            InitAwal()
            BindGridEntity(Me.CmdWhere)
        ElseIf result = "Double" Then
            If pAcuanBunga.UsedNew = "N" Then
                Tdooble = "New "
            Else
                Tdooble = "Used "
            End If

            If pAcuanBunga.RateType = "M" Then
                Tdooble = Tdooble & "Medium "
            Else
                Tdooble = Tdooble & "Low "
            End If

            Tdooble = Tdooble & " Dengan tenor " & cboTenor.SelectedValue.ToString.Trim & " tahun"

            ShowMessage(lblMessage, "Gagal Tambah data: Acuan bunga untuk jenis '" & Tdooble & "' sudah ada", True)

        ElseIf result = "Unik" Then
            ShowMessage(lblMessage, "Acuan ID harus unik, ID " & pAcuanBunga.AcuanID & " Sudah digunakan", True)
        End If

    End Sub
    Private Sub imbCancel_click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        InitAwal()
    End Sub
    Sub InitAwal()
        pnlAddEdit.Visible = False
        pnlList.Visible = True
        txtAcuanID.Text = ""
        txtAcuanID.Enabled = True
        txtManufacturingYearFrom.Text = ""
        txtManufacturingYearTo.Text = ""
        txtRate.Text = ""
        txtRate.Enabled = True
        txtNama.Text = ""
        txtRateType.Text = ""
        txtRateType.Enabled = True
        txtUsedNew.Text = ""
        txtUsedNew.Enabled = True
        rdRateTypeL.Checked = False
        rdRateTypeL.Enabled = True
        rdRateTypeM.Checked = False
        rdRateTypeM.Enabled = True
        rdusednewN.Checked = False
        rdusednewN.Enabled = True
        rdusednewU.Checked = False
        rdusednewU.Enabled = True


        cboJenisKredit.Enabled = True
        cboKategoriAsset.Enabled = True
        cboTenor.Enabled = True

        cboKategoriAsset.SelectedIndex = 0

        
    End Sub
    Private Sub GenerateCombo(ByVal cbogenerate As System.Web.UI.WebControls.DropDownList, ByVal spName As String, ByVal TextField As String, ByVal ValueField As String)
        Dim PGeneral As New Parameter.GeneralPaging
        Dim CGeneral As New GeneralPagingController
        Dim dtTemp As New DataTable


        With PGeneral
            .SpName = spName
            .strConnection = GetConnectionString()
            .CurrentPage = 1
            .PageSize = 100
            .WhereCond = ""
            .SortBy = ""
        End With

        dtTemp = CGeneral.GetGeneralPaging(PGeneral).ListData

        With cbogenerate
            .DataSource = dtTemp
            .DataTextField = TextField
            .DataValueField = ValueField
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = ""
            .SelectedIndex = 0
        End With

    End Sub
    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        Dim LinkUsage As New HyperLink
        Dim LinkCategory As New HyperLink
        Dim LinkList As New HyperLink
        Dim AcuanID As String

        If SessionInvalid() Then
            Exit Sub
        End If

        If e.Item.ItemIndex >= 0 Then
            AcuanID = CType(e.Item.FindControl("AcuanID"), Label).Text
            LinkUsage = CType(e.Item.FindControl("AssetUsage"), HyperLink)
            LinkCategory = CType(e.Item.FindControl("AssetCategory"), HyperLink)
            LinkList = CType(e.Item.FindControl("AssetList"), HyperLink)

            'LinkUsage.NavigateUrl = "javascript:OpenUsage('AcuanAssetUsage.aspx?AcuanID=" & AcuanID.Trim & "')"
            'LinkCategory.NavigateUrl = "javascript:OpenUsage('AcuanAssetCategory.aspx?AcuanID=" & AcuanID & "')"
            LinkList.NavigateUrl = "AcuanAssetList.aspx?AcuanID=" & AcuanID & "&currentPage=" & currentPage
        End If

    End Sub
    Private Sub dtgPaging_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        Dim PAcuan As New Parameter.PAcuanBungaIntern


        If e.CommandName = "Edit" Then
            If CheckFeature(Me.Loginid, "RATEINTERN", "Edit", Me.AppId) Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If

            Me.AddEdit = "Edit"
            txtAcuanID.Text = CType(e.Item.FindControl("AcuanID"), Label).Text
            txtAcuanID.Enabled = False

            txtNama.Text = CType(e.Item.FindControl("Description"), Label).Text
            If CType(e.Item.FindControl("UsedNew"), Label).Text = "U" Then
                rdusednewU.Checked = True
            ElseIf CType(e.Item.FindControl("UsedNew"), Label).Text = "N" Then
                rdusednewN.Checked = True
            End If
            txtUsedNew.Text = CType(e.Item.FindControl("UsedNew"), Label).Text
            If CType(e.Item.FindControl("RateType"), Label).Text = "M" Then
                rdRateTypeM.Checked = True
            ElseIf CType(e.Item.FindControl("RateType"), Label).Text = "L" Then
                rdRateTypeL.Checked = True
            End If
            txtRateType.Text = CType(e.Item.FindControl("RateType"), Label).Text

            If CType(e.Item.FindControl("ManfcYearFrom"), Label).Text <> "0" Then
                txtManufacturingYearFrom.Text = CType(e.Item.FindControl("ManfcYearFrom"), Label).Text
            Else
                txtManufacturingYearFrom.Text = ""
            End If

            If CType(e.Item.FindControl("ManfcYearTo"), Label).Text <> "0" Then
                txtManufacturingYearTo.Text = CType(e.Item.FindControl("ManfcYearTo"), Label).Text
            Else
                txtManufacturingYearTo.Text = ""
            End If

            cboJenisKredit.SelectedIndex = cboJenisKredit.Items.IndexOf(cboJenisKredit.Items.FindByValue(CType(e.Item.FindControl("JenisKredit"), Label).Text))
            cboKategoriAsset.SelectedIndex = cboKategoriAsset.Items.IndexOf(cboKategoriAsset.Items.FindByValue(CType(e.Item.FindControl("InsRateCategoryID"), Label).Text))
            cboTenor.SelectedIndex = cboTenor.Items.IndexOf(cboTenor.Items.FindByValue(CType(e.Item.FindControl("Tenor"), Label).Text))
            txtRate.Text = CType(e.Item.FindControl("Rate"), Label).Text


            pnlAddEdit.Visible = True
            pnlList.Visible = False

            lblAddEdit.Text = Me.AddEdit
        ElseIf e.CommandName = "Delete" Then
            PAcuan.strConnection = GetConnectionString()
            PAcuan.AcuanID = CType(e.Item.FindControl("AcuanID"), Label).Text
            m_acuan.Delete(PAcuan)

            ShowMessage(lblMessage, "Data acuan sudah terhapus", True)
            BindGridEntity(Me.CmdWhere)
        End If
    End Sub


    Protected Sub imbButtonSearch_Click(sender As Object, e As EventArgs) Handles imbButtonSearch.Click
        lblMessage.Visible = False
        Me.CmdWhere = Nothing

        If Not txtSearch.Text.Trim = "" Then
            Me.CmdWhere = cboSearch.SelectedValue & " like '%" & txtSearch.Text.Trim & "%'"
        End If

        If Not cboTenorSearch.SelectedValue = "" Then
            If Not IsNothing(Me.CmdWhere) Then
                Me.CmdWhere = Me.CmdWhere & " and "
            End If
            Me.CmdWhere = CmdWhere & " tenor = '" & cboTenorSearch.SelectedValue & "'"
        End If

        If Not cboJenisKreditSearch.SelectedValue = "" Then
            If Not IsNothing(Me.CmdWhere) Then
                Me.CmdWhere = Me.CmdWhere & " and "
            End If
            Me.CmdWhere = CmdWhere & " JenisKredit = '" & cboJenisKreditSearch.SelectedValue & "'"
        End If

        If Not cboAssetKategoriSearch.SelectedValue = "" Then
            If Not IsNothing(Me.CmdWhere) Then
                Me.CmdWhere = Me.CmdWhere & " and "
            End If
            Me.CmdWhere = CmdWhere & " AcuanBungaIntern.InsRateCategoryID = '" & cboAssetKategoriSearch.SelectedValue & "'"
        End If

        If Not cboUsedNewSearch.SelectedValue = "" Then
            If Not IsNothing(Me.CmdWhere) Then
                Me.CmdWhere = Me.CmdWhere & " and "
            End If
            Me.CmdWhere = CmdWhere & " UsedNew = '" & cboUsedNewSearch.SelectedValue & "'"
        End If

        If Not cboRateTypeSearch.SelectedValue = "" Then
            If Not IsNothing(Me.CmdWhere) Then
                Me.CmdWhere = Me.CmdWhere & " and "
            End If
            Me.CmdWhere = CmdWhere & " RateType = '" & cboRateTypeSearch.SelectedValue & "'"
        End If


        BindGridEntity(Me.CmdWhere)
    End Sub
End Class