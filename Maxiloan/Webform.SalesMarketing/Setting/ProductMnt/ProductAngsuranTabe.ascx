﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ProductAngsuranTabe.ascx.vb" Inherits="Maxiloan.Webform.SalesMarketing.ProductAngsuranTabe" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<div class="form_box_title">
    <div class="form_single"> <h4> ANGSURAN </h4> </div>
</div>
<%--<div class="form_box">
    <div class="form_single">
        <label class="label_general">Pengaturan Pembayaran</label>
        <asp:label  ID="lblPengaturanPembayaran" runat="server" />
         <asp:RadioButtonList ID="rboPengaturanPembayaran" runat="server" AutoPostBack="true" CssClass="opt_single_product" RepeatDirection="Horizontal"> 
             <asp:ListItem Value="PR" Selected="True">Prioritas</asp:ListItem> 
             <asp:ListItem Value="ML">Manual</asp:ListItem> 
         </asp:RadioButtonList>
    </div>
</div>--%>
<div class="form_box">
    <div class="form_single">
        <label class="label_general">Pengaturan Pembayaran</label>
        <asp:label  ID="lblPengaturanPembayaran" runat="server" />
         <div id="RadioDiv">
         <asp:RadioButtonList ID="rboPengaturanPembayaran" runat="server" CssClass="opt_single_product" RepeatDirection="Horizontal"> <asp:ListItem Value="PR" Selected="True">Prioritas</asp:ListItem> <asp:ListItem Value="ML">Manual</asp:ListItem> </asp:RadioButtonList>
         </div>
    </div>
</div>
<div class="form_box" id="TR_Prioritas" runat="server">
    <div class="form_single">
        <label class="label_general">Prioritas Alokasi Pembayaran</label>
        <asp:label  ID="lblPriotasPembayaran" runat="server" />
         <asp:DropDownList ID="cboPrioritasPembayaran" runat="server" CssClass="opt_single_product"> 
            <asp:ListItem Value="PIL">Pokok-Margin-Ta'Widh</asp:ListItem>
            <asp:ListItem Value="PLI">Pokok-Ta'Widh-Margin</asp:ListItem> 
            <asp:ListItem Value="IPL">Margin-Pokok-Ta'Widh</asp:ListItem> 
            <asp:ListItem Value="ILP">Margin-Ta'Widh-Pokok</asp:ListItem> 
            <asp:ListItem Value="LPI">Ta'Widh-Pokok-Margin</asp:ListItem> 
            <asp:ListItem Value="LIP">Ta'Widh-Margin-Pokok</asp:ListItem> 
         </asp:DropDownList>
    </div>
</div>
<div class="form_box">
    <div class="form_single">
        <label> Jumlah Toleransi terhadap Angsuran</label>
        <asp:label ID="lblInsToleranceAmount" runat="server" />
          <asp:TextBox ID="txtInsToleranceAmount" runat="server"  MaxLength="15" onblur="extractNumber(this,2,true);blockInvalid(this);this.value=numberFormat(this.value);this.value=blankToZero(this.value);"
    onkeyup="extractNumber(this,2,true);" onkeypress="return blockNonNumbers(this, event, true, true);"
    onfocus="this.value=resetNumber(this.value);" CssClass="numberAlign" OnChange="javascript:void(0)" autocomplete="off">0</asp:TextBox>
          <asp:DropDownList ID="cboInsToleranceAmount" runat="server" /> 
          <asp:RequiredFieldValidator ID="Requiredfieldvalidator29" runat="server" ControlToValidate="txtInsToleranceAmount" CssClass="validator_general" ErrorMessage="Jumlah Toleransi terhadap Angsuran salah" Display="Dynamic"></asp:RequiredFieldValidator>
          <asp:RangeValidator ID="Rangevalidator23" runat="server" ControlToValidate="txtInsToleranceAmount" Type="Currency" MinimumValue="0" ErrorMessage="Jumlah Toleransi terhadap Angsuran Salah" CssClass="validator_general" Display="Dynamic" MaximumValue="999999999999"></asp:RangeValidator>
    </div>
</div>
<div class="form_box">
    <div class="form_single">
        <label> Prosentase Ta'Widh Keterlambatan (Angsuran)</label>
        <asp:label ID= "lblLCP_Installment" runat="server"   />
         <asp:TextBox ID="txtLCP_Installment" runat="server"  MaxLength="9">0</asp:TextBox><asp:label ID= "Label1" runat="server"   >‰ / day</asp:label>
         <asp:DropDownList ID="cboLCP_Installment" runat="server" /> 
         <asp:RequiredFieldValidator ID="Requiredfieldvalidator10" runat="server" ControlToValidate="txtLCP_Installment"  CssClass="validator_general" ErrorMessage="Prosentase Ta'Widh Keterlambatan Angsuran salah" Display="Dynamic"></asp:RequiredFieldValidator>
         <asp:RangeValidator ID="Rangevalidator4" runat="server" ControlToValidate="txtLCP_Installment" CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Prosentase Ta'Widh Keterlambatan Angsuran salah" Display="Dynamic" MaximumValue="100"></asp:RangeValidator>
    </div>
</div>
    <div class="form_box">
    <div class="form_single">
        <label class=      "label_general"> Dasar Perhitungan</label>
        <asp:label  ID="lblPenaltyBasedOn" runat="server" />
         <asp:RadioButtonList ID="rboPenaltyBasedOn" runat="server" CssClass="opt_single_product" RepeatDirection="Horizontal"> <asp:ListItem Value="PB" Selected="True">Pokok + Margin</asp:ListItem> <asp:ListItem Value="PO">Pokok</asp:ListItem> </asp:RadioButtonList>
         <asp:DropDownList ID="cboPenaltyBasedOn" runat="server"/> 
    </div>
</div>
<div class="form_box">
    <div class="form_single">
        <label>Tanggal Efektif</label>
        <asp:label ID="lblPenaltyRateEffectiveDate" runat="server" />
        <asp:TextBox ID="txtPenaltyRateEffectiveDate" runat="server"></asp:TextBox>      
        <asp:CalendarExtender ID="txtPenaltyRateEffectiveDate_CalendarExtender1" runat="server" Enabled="True" TargetControlID="txtPenaltyRateEffectiveDate" Format ="dd/MM/yyyy"></asp:CalendarExtender>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator61" runat="server" ErrorMessage="*" CssClass="validator_general" ControlToValidate="txtPenaltyRateEffectiveDate"></asp:RequiredFieldValidator>
         
    </div>
</div>
<div class="form_box">
    <div class="form_single">
        <label> Prosentase Ta'Widh Keterlambatan Sebelumnya</label>
        <asp:label ID="lblPenaltyRatePrevious" runat="server" />
        <asp:TextBox ID="txtPenaltyRatePrevious" runat="server"  MaxLength="9">0</asp:TextBox><asp:label ID= "Label2" runat="server"   >‰ / day</asp:label> 
    </div>
</div>
    <div class="form_box">
    <div class="form_single">
        <label> Biaya Tagih</label>
        <asp:label ID="lblBilingCharges" runat="server"  />
        <asp:TextBox ID="txtBilingCharges" runat="server"  MaxLength="15">0</asp:TextBox>
        <asp:DropDownList ID="cboBilingCharges" runat="server" /> 
        <asp:RequiredFieldValidator ID="Requiredfieldvalidator56" runat="server" ControlToValidate="txtBilingCharges" CssClass="validator_general" ErrorMessage="Biaya Tagih Salah" Display="Dynamic"></asp:RequiredFieldValidator>
        <asp:RangeValidator ID="Rangevalidator50" runat="server" ControlToValidate="txtBilingCharges" CssClass="validator_general" Type="double" MinimumValue="0" ErrorMessage="Biaya Tagih Salah" Display="Dynamic" MaximumValue="999999999999"></asp:RangeValidator>
    </div>
</div>
<div class="form_box_title_nobg">
    <div class="form_single">
        <label> Pinalty</label>
        <asp:label  ID="lblPPR" runat="server"  MaxLength="9" />
          <asp:TextBox ID="txtPPR" runat="server"  MaxLength="9">0</asp:TextBox><asp:label ID= "Label3" runat="server"   >%</asp:label>
          <asp:DropDownList ID="cboPPR" runat="server" /> 
          <asp:RequiredFieldValidator ID="Requiredfieldvalidator39" runat="server" ControlToValidate="txtPPR" CssClass="validator_general" ErrorMessage="Ta'Widh Pelunasan dipercepat Salah" Display="Dynamic"></asp:RequiredFieldValidator>
          <asp:RangeValidator ID="Rangevalidator33" runat="server" ControlToValidate="txtPPR" Type="double" MinimumValue="0" ErrorMessage="Ta'Widh Pelunasan dipercepat Salah" CssClass="validator_general" Display="Dynamic" MaximumValue="100"></asp:RangeValidator>
    </div>
</div>
<div class="form_box">
    <div class="form_single">
        <label>Tanggal Efektif</label>
        <asp:label ID="lblPrepaymentPenaltyEffectiveDate" runat="server" />
        <asp:TextBox ID="txtPrepaymentPenaltyEffectiveDate" runat="server"></asp:TextBox>
         
        <asp:CalendarExtender ID="txtPrepaymentPenaltyEffectiveDate_CalendarExtender" runat="server"  Enabled="True" TargetControlID="txtPrepaymentPenaltyEffectiveDate" Format ="dd/MM/yyyy"></asp:CalendarExtender>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator59" runat="server" ErrorMessage="*" CssClass="validator_general" ControlToValidate="txtPrepaymentPenaltyEffectiveDate"></asp:RequiredFieldValidator>
         
    </div>
</div>
<div class="form_box">
    <div class="form_single">
        <label> Ta'Widh Pelunasan Sebelumnya</label>
        <asp:label ID="lblPrepaymentPenaltyPrevious" runat="server"  />
        <asp:TextBox ID="txtPrepaymentPenaltyPrevious" runat="server"  MaxLength="9">0</asp:TextBox><asp:label ID="Label4" runat="server"  >‰ / day</asp:label>
    </div>
</div>
<div class="form_box_title_nobg">
    <div class="form_single">
        <label> Admin Pelunasan</label>
        <asp:label ID="lblPrepaymentPenaltyFixed" runat="server" />
          <asp:TextBox ID="txtPrepaymentPenaltyFixed" runat="server" />
          <asp:DropDownList ID="cboPrepaymentPenaltyFixedBehaviour" runat="server"/> 
          <asp:RequiredFieldValidator ID="Requiredfieldvalidator58" runat="server" ControlToValidate="txtPrepaymentPenaltyFixed" CssClass="validator_general" ErrorMessage="Ta'Widh Tetap Pelunasan dipercepat tidak boleh kosong" Display="Dynamic"></asp:RequiredFieldValidator>
        </div>
</div>
 
<div class="form_box">
    <div class="form_single">
        <label>Tanggal Efektif</label>
        <asp:label ID="lblPrepaymentPenaltyFixedEffectiveDate" runat="server"></asp:label>
        <asp:TextBox ID="txtPrepaymentPenaltyFixedEffectiveDate" runat="server"></asp:TextBox>                    
        <asp:CalendarExtender ID="txtPrepaymentPenaltyFixedEffectiveDate_CalendarExtender1" runat="server"  Enabled="True" TargetControlID="txtPrepaymentPenaltyFixedEffectiveDate" Format ="dd/MM/yyyy"></asp:CalendarExtender> <asp:RequiredFieldValidator ID="RequiredFieldValidator60" runat="server" ErrorMessage="*" CssClass="validator_general" ControlToValidate="txtPrepaymentPenaltyFixedEffectiveDate"></asp:RequiredFieldValidator> 
        </div>
</div>
<div class="form_box">
	    <div class="form_single">
            <label>Ta'Widh Pelunasan Tetap Sebelumnya</label>
            <asp:Label ID="lblPrepaymentPenaltyFixedPrevious" runat="server"></asp:Label>
            <asp:TextBox ID="txtPrepaymentPenaltyFixedPrevious" runat="server"  MaxLength="9">0</asp:TextBox>
	    </div>
    </div>
                            