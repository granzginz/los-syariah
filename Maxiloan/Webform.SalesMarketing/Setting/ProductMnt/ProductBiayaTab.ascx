﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ProductBiayaTab.ascx.vb" Inherits="Maxiloan.Webform.SalesMarketing.ProductBiayaTab" %>
 
    <div class="form_box_title">
        <div class="form_single"> <h4> BIAYA </h4> </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label> Biaya Pembatalan</label>
            <asp:Label ID="lblCancellationFee" runat="server"></asp:Label> 
             <asp:TextBox ID="txtCancellationFee" runat="server"  MaxLength="10"
             onblur="extractNumber(this,2,true);blockInvalid(this);this.value=numberFormat(this.value);this.value=blankToZero(this.value);"
    onkeyup="extractNumber(this,2,true);" onkeypress="return blockNonNumbers(this, event, true, true);"
    onfocus="this.value=resetNumber(this.value);" CssClass="numberAlign" OnChange="javascript:void(0)" autocomplete="off">0</asp:TextBox>
             <asp:DropDownList ID="cboCancellationFee" runat="server" /> 
             <asp:RequiredFieldValidator ID="Requiredfieldvalidator12" runat="server" ControlToValidate="txtCancellationFee" CssClass="validator_general" ErrorMessage="Biaya Pembatalan Salah" Display="Dynamic"></asp:RequiredFieldValidator>
             <asp:RangeValidator ID="Rangevalidator6" runat="server" ControlToValidate="txtCancellationFee" CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Biaya Pembatalan Salah" Display="Dynamic" MaximumValue="9999999999"></asp:RangeValidator>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label> Biaya Administrasi</label>
            <asp:Label ID="lblAdminFee" runat="server"  ></asp:Label>
             <asp:TextBox ID="txtAdminFee" runat="server"  MaxLength="10" onblur="extractNumber(this,2,true);blockInvalid(this);this.value=numberFormat(this.value);this.value=blankToZero(this.value);"
    onkeyup="extractNumber(this,2,true);" onkeypress="return blockNonNumbers(this, event, true, true);"
    onfocus="this.value=resetNumber(this.value);" CssClass="numberAlign" OnChange="javascript:void(0)" autocomplete="off">0</asp:TextBox>
             <asp:DropDownList ID="cboAdminFee" runat="server" /> 
             <asp:RequiredFieldValidator ID="Requiredfieldvalidator13" runat="server" ControlToValidate="txtAdminFee" CssClass="validator_general" ErrorMessage="Biaya Administrasi Salah" Display="Dynamic"></asp:RequiredFieldValidator>
             <asp:RangeValidator ID="Rangevalidator7" runat="server" ControlToValidate="txtAdminFee" CssClass="validator_general" Type="Currency" MinimumValue="0" ErrorMessage="Biaya Administrasi Salah" Display="Dynamic" MaximumValue="9999999999"></asp:RangeValidator>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label> Biaya Fidusia</label>
            <asp:Label ID="lblFiduciaFee" runat="server"  ></asp:Label>
             <asp:TextBox ID="txtFiduciaFee" runat="server"  MaxLength="10" onblur="extractNumber(this,2,true);blockInvalid(this);this.value=numberFormat(this.value);this.value=blankToZero(this.value);"
    onkeyup="extractNumber(this,2,true);" onkeypress="return blockNonNumbers(this, event, true, true);"
    onfocus="this.value=resetNumber(this.value);" CssClass="numberAlign" OnChange="javascript:void(0)" autocomplete="off">0</asp:TextBox>
             <asp:DropDownList ID="cboFiduciaFee" runat="server" /> 
             <asp:RequiredFieldValidator ID="Requiredfieldvalidator14" runat="server" ControlToValidate="txtFiduciaFee" CssClass="validator_general" ErrorMessage="Biaya Fidusia Salah" Display="Dynamic"></asp:RequiredFieldValidator> 
             <asp:RangeValidator ID="Rangevalidator8" runat="server" ControlToValidate="txtFiduciaFee" CssClass="validator_general" Type="Currency" MinimumValue="0" ErrorMessage="Biaya Fidusia Salah" Display="Dynamic" MaximumValue="9999999999"></asp:RangeValidator>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label> Biaya Provisi</label>
            <asp:label  ID="lblProvisionFee" runat="server"  ></asp:label> 
            <asp:TextBox ID="txtProvisionFee" runat="server"  MaxLength="10" onblur="extractNumber(this,2,true);blockInvalid(this);this.value=numberFormat(this.value);this.value=blankToZero(this.value);"
    onkeyup="extractNumber(this,2,true);" onkeypress="return blockNonNumbers(this, event, true, true);"
    onfocus="this.value=resetNumber(this.value);" CssClass="numberAlign" OnChange="javascript:void(0)" autocomplete="off">0</asp:TextBox>
            <asp:DropDownList ID="cboProvisionFee" runat="server" /> 
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator15" runat="server" ControlToValidate="txtProvisionFee" CssClass="validator_general" ErrorMessage="Biaya Provisi Salah" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="Rangevalidator9" runat="server" ControlToValidate="txtProvisionFee" CssClass="validator_general" Type="Currency" MinimumValue="0" ErrorMessage="Biaya Provisi Salah" Display="Dynamic" MaximumValue="9999999999"></asp:RangeValidator>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Biaya Notaris</label>
            <asp:label  ID="lblNotaryFee" runat="server"  ></asp:label>
              <asp:TextBox ID="txtNotaryFee" runat="server"  MaxLength="10" onblur="extractNumber(this,2,true);blockInvalid(this);this.value=numberFormat(this.value);this.value=blankToZero(this.value);"
    onkeyup="extractNumber(this,2,true);" onkeypress="return blockNonNumbers(this, event, true, true);"
    onfocus="this.value=resetNumber(this.value);" CssClass="numberAlign" OnChange="javascript:void(0)" autocomplete="off">0</asp:TextBox>
              <asp:DropDownList ID="cboNotaryFee" runat="server" /> 
              <asp:RequiredFieldValidator ID="Requiredfieldvalidator16" runat="server" ControlToValidate="txtNotaryFee" CssClass="validator_general" ErrorMessage="Biaya Notaris Salah" Display="Dynamic"></asp:RequiredFieldValidator>
              <asp:RangeValidator ID="Rangevalidator10" runat="server" ControlToValidate="txtNotaryFee" CssClass="validator_general" Type="Currency" MinimumValue="0" ErrorMessage="Biaya Notaris Salah" MaximumValue="9999999999"></asp:RangeValidator>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label> Biaya Survey</label>
            <asp:label ID="lblSurveyFee" runat="server"  ></asp:label>
             <asp:TextBox ID="txtSurveyFee" runat="server"  MaxLength="10" onblur="extractNumber(this,2,true);blockInvalid(this);this.value=numberFormat(this.value);this.value=blankToZero(this.value);"
    onkeyup="extractNumber(this,2,true);" onkeypress="return blockNonNumbers(this, event, true, true);"
    onfocus="this.value=resetNumber(this.value);" CssClass="numberAlign" OnChange="javascript:void(0)" autocomplete="off">0</asp:TextBox>
             <asp:DropDownList ID="cboSurveyFee" runat="server" /> 
             <asp:RequiredFieldValidator ID="Requiredfieldvalidator17" runat="server" ControlToValidate="txtSurveyFee" CssClass="validator_general" ErrorMessage="Biaya Survey salah" Display="Dynamic"></asp:RequiredFieldValidator>
             <asp:RangeValidator ID="Rangevalidator11" runat="server" ControlToValidate="txtSurveyFee" CssClass="validator_general" Type="Currency" MinimumValue="0" ErrorMessage="Biaya Survey salah" Display="Dynamic" MaximumValue="9999999999"></asp:RangeValidator>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>  Biaya Kunjungan</label>
            <asp:label ID="lblVisitFee" runat="server"  ></asp:label>
             <asp:TextBox ID="txtVisitFee" runat="server"  MaxLength="10" onblur="extractNumber(this,2,true);blockInvalid(this);this.value=numberFormat(this.value);this.value=blankToZero(this.value);"
    onkeyup="extractNumber(this,2,true);" onkeypress="return blockNonNumbers(this, event, true, true);"
    onfocus="this.value=resetNumber(this.value);" CssClass="numberAlign" OnChange="javascript:void(0)" autocomplete="off">0</asp:TextBox>
             <asp:DropDownList ID="cboVisitFee" runat="server" /> 
             <asp:RequiredFieldValidator ID="Requiredfieldvalidator18" runat="server" ControlToValidate="txtVisitFee" CssClass="validator_general" ErrorMessage="Biaya Kunjungan Salah" Display="Dynamic"></asp:RequiredFieldValidator>
             <asp:RangeValidator ID="Rangevalidator12" runat="server" ControlToValidate="txtVisitFee" Type="Currency" MinimumValue="0" ErrorMessage="Biaya Kunjungan Salah" Display="Dynamic" CssClass="validator_general" MaximumValue="9999999999"></asp:RangeValidator>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label> Biaya Rescheduling</label>
            <asp:label ID="lblReschedulingFee" runat="server"></asp:label>
              <asp:TextBox ID="txtReschedulingFee" runat="server"  MaxLength="10" onblur="extractNumber(this,2,true);blockInvalid(this);this.value=numberFormat(this.value);this.value=blankToZero(this.value);"
    onkeyup="extractNumber(this,2,true);" onkeypress="return blockNonNumbers(this, event, true, true);"
    onfocus="this.value=resetNumber(this.value);" CssClass="numberAlign" OnChange="javascript:void(0)" autocomplete="off">0</asp:TextBox>
              <asp:DropDownList ID="cboReschedulingFee" runat="server" /> 
              <asp:RequiredFieldValidator ID="Requiredfieldvalidator19" runat="server" ControlToValidate="txtReschedulingFee" CssClass="validator_general" ErrorMessage="Biaya Rescheduling Salah" Display="Dynamic"></asp:RequiredFieldValidator>
              <asp:RangeValidator ID="Rangevalidator13" runat="server" ControlToValidate="txtReschedulingFee" Type="Currency" MinimumValue="0" ErrorMessage="Biaya Rescheduling Salah" CssClass="validator_general" Display="Dynamic" MaximumValue="9999999999"></asp:RangeValidator>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label> Biaya Over Kontrak</label>
            <asp:label ID="lblATFee" runat="server"  ></asp:label> 
              <asp:TextBox ID="txtATFee" runat="server"  MaxLength="10" onblur="extractNumber(this,2,true);blockInvalid(this);this.value=numberFormat(this.value);this.value=blankToZero(this.value);"
    onkeyup="extractNumber(this,2,true);" onkeypress="return blockNonNumbers(this, event, true, true);"
    onfocus="this.value=resetNumber(this.value);" CssClass="numberAlign" OnChange="javascript:void(0)" autocomplete="off">0</asp:TextBox>
              <asp:DropDownList ID="cboATFee" runat="server" /> 
              <asp:RequiredFieldValidator ID="Requiredfieldvalidator20" runat="server" ControlToValidate="txtATFee" CssClass="validator_general" ErrorMessage="Biaya Over Kontrak salah" Display="Dynamic"></asp:RequiredFieldValidator>
              <asp:RangeValidator ID="Rangevalidator14" runat="server" ControlToValidate="txtATFee" CssClass="validator_general" Type="Currency" MinimumValue="0" ErrorMessage="Biaya Over Kontrak salah" MaximumValue="9999999999"></asp:RangeValidator>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label> Biaya Ganti Tgl jatuh Tempo</label>
            <asp:label ID="lblCDDRFee" runat="server"  ></asp:label>
              <asp:TextBox ID="txtCDDRFee" runat="server"  MaxLength="10" onblur="extractNumber(this,2,true);blockInvalid(this);this.value=numberFormat(this.value);this.value=blankToZero(this.value);"
    onkeyup="extractNumber(this,2,true);" onkeypress="return blockNonNumbers(this, event, true, true);"
    onfocus="this.value=resetNumber(this.value);" CssClass="numberAlign" OnChange="javascript:void(0)" autocomplete="off">0</asp:TextBox>
              <asp:DropDownList ID="cboCDDRFee" runat="server" /> 
              <asp:RequiredFieldValidator ID="Requiredfieldvalidator21" runat="server" ControlToValidate="txtCDDRFee" CssClass="validator_general" ErrorMessage="Biaya Ganti Tgl jatuh Tempo Salah" Display="Dynamic"></asp:RequiredFieldValidator>
              <asp:RangeValidator ID="Rangevalidator15" runat="server" ControlToValidate="txtCDDRFee" CssClass="validator_general" Type="Currency" MinimumValue="0" ErrorMessage="Biaya Ganti Tgl jatuh Tempo Salah" Display="Dynamic" MaximumValue="9999999999"></asp:RangeValidator>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label> Biaya Penggantian Asset</label>
            <asp:label ID="lblAssetReplacementFee" runat="server"  ></asp:label>
             <asp:TextBox ID="txtAssetReplacementFee" runat="server"  MaxLength="10" onblur="extractNumber(this,2,true);blockInvalid(this);this.value=numberFormat(this.value);this.value=blankToZero(this.value);"
    onkeyup="extractNumber(this,2,true);" onkeypress="return blockNonNumbers(this, event, true, true);"
    onfocus="this.value=resetNumber(this.value);" CssClass="numberAlign" OnChange="javascript:void(0)" autocomplete="off">0</asp:TextBox>
             <asp:DropDownList ID="cboAssetReplacementFee" runat="server" /> 
             <asp:RequiredFieldValidator ID="Requiredfieldvalidator22" runat="server" ControlToValidate="txtAssetReplacementFee" CssClass="validator_general" ErrorMessage="Biaya Penggantian Asset salah" Display="Dynamic"></asp:RequiredFieldValidator>
             <asp:RangeValidator ID="Rangevalidator16" runat="server" ControlToValidate="txtAssetReplacementFee" CssClass="validator_general" Type="Currency" MinimumValue="0" ErrorMessage="Biaya Penggantian Asset salah" Display="Dynamic" MaximumValue="9999999999"></asp:RangeValidator>
    </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label> Biaya Penarikan</label>
            <asp:label ID="lblRepossesFee" runat="server"  ></asp:label>
            <asp:TextBox ID="txtRepossesFee" runat="server"  MaxLength="10" onblur="extractNumber(this,2,true);blockInvalid(this);this.value=numberFormat(this.value);this.value=blankToZero(this.value);"
    onkeyup="extractNumber(this,2,true);" onkeypress="return blockNonNumbers(this, event, true, true);"
    onfocus="this.value=resetNumber(this.value);" CssClass="numberAlign" OnChange="javascript:void(0)" autocomplete="off">0</asp:TextBox>
            <asp:DropDownList ID="cboRepossesFee" runat="server" /> 
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator23" runat="server" ControlToValidate="txtRepossesFee" CssClass="validator_general" ErrorMessage="Biaya Penarikan Salah" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="Rangevalidator17" runat="server" ControlToValidate="txtRepossesFee" Type="Currency" MinimumValue="0" ErrorMessage="Biaya Penarikan Salah" Display="Dynamic" CssClass="validator_general" MaximumValue="9999999999"></asp:RangeValidator>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label> Biaya Legalisasi Dokumen</label>
            <asp:label ID="lblLDFee" runat="server"   ></asp:label>
            <asp:TextBox ID="txtLDFee" runat="server"  MaxLength="10" onblur="extractNumber(this,2,true);blockInvalid(this);this.value=numberFormat(this.value);this.value=blankToZero(this.value);"
    onkeyup="extractNumber(this,2,true);" onkeypress="return blockNonNumbers(this, event, true, true);"
    onfocus="this.value=resetNumber(this.value);" CssClass="numberAlign" OnChange="javascript:void(0)" autocomplete="off">0</asp:TextBox>
            <asp:DropDownList ID="cboLDFee" runat="server" /> 
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator24" runat="server" ControlToValidate="txtLDFee" CssClass="validator_general" ErrorMessage="Biaya Legalisasi Dokumen Salah" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="Rangevalidator18" runat="server" ControlToValidate="txtLDFee" Type="Currency" MinimumValue="0" ErrorMessage="Biaya Legalisasi Dokumen Salah" CssClass="validator_general" MaximumValue="9999999999"></asp:RangeValidator>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label> Biaya Penolakan PDC</label>
            <asp:label ID="lblPDCBounceFee" runat="server"  ></asp:label>
             <asp:TextBox ID="txtPDCBounceFee" runat="server"  MaxLength="10" onblur="extractNumber(this,2,true);blockInvalid(this);this.value=numberFormat(this.value);this.value=blankToZero(this.value);"
    onkeyup="extractNumber(this,2,true);" onkeypress="return blockNonNumbers(this, event, true, true);"
    onfocus="this.value=resetNumber(this.value);" CssClass="numberAlign" OnChange="javascript:void(0)" autocomplete="off">0</asp:TextBox>
             <asp:DropDownList ID="cboPDCBounceFee" runat="server" /> 
             <asp:RequiredFieldValidator ID="Requiredfieldvalidator25" runat="server" ControlToValidate="txtPDCBounceFee" CssClass="validator_general" ErrorMessage="Biaya Penolakan PDC salah" Display="Dynamic"></asp:RequiredFieldValidator>
             <asp:RangeValidator ID="Rangevalidator19" runat="server" ControlToValidate="txtPDCBounceFee" Type="Currency" MinimumValue="0" ErrorMessage="Biaya Penolakan PDC salah" Display="Dynamic" CssClass="validator_general" MaximumValue="9999999999"></asp:RangeValidator>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label> Biaya Asuransi Jiwa</label>
            <asp:label ID="lblInsuranceAdminFee"  runat="server"  ></asp:label>
            <asp:TextBox ID="txtInsuranceAdminFee" runat="server"  MaxLength="10" onblur="extractNumber(this,2,true);blockInvalid(this);this.value=numberFormat(this.value);this.value=blankToZero(this.value);"
    onkeyup="extractNumber(this,2,true);" onkeypress="return blockNonNumbers(this, event, true, true);"
    onfocus="this.value=resetNumber(this.value);" CssClass="numberAlign" OnChange="javascript:void(0)" autocomplete="off">0</asp:TextBox>
            <asp:DropDownList ID="cboInsuranceAdminFee" runat="server" /> 
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator26" runat="server" ControlToValidate="txtInsuranceAdminFee" CssClass="validator_general" ErrorMessage="Biaya Admin Asuransi salah" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="Rangevalidator20" runat="server" ControlToValidate="txtInsuranceAdminFee" CssClass="validator_general" Type="Currency" MinimumValue="0" ErrorMessage="Biaya Admin Asuransi Salah" Display="Dynamic" MaximumValue="9999999999"></asp:RangeValidator>
         </div> 
        </div>
 
    <div class="form_box">
        <div class="form_single">
            <label> Biaya Asuransi Asset</label>
            <asp:label ID="lblISDFee" runat="server"  ></asp:label>
             <asp:TextBox ID="txtISDFee" runat="server"  MaxLength="10" onblur="extractNumber(this,2,true);blockInvalid(this);this.value=numberFormat(this.value);this.value=blankToZero(this.value);"
    onkeyup="extractNumber(this,2,true);" onkeypress="return blockNonNumbers(this, event, true, true);"
    onfocus="this.value=resetNumber(this.value);" CssClass="numberAlign" OnChange="javascript:void(0)" autocomplete="off">0</asp:TextBox>
             <asp:DropDownList ID="cboISDFee" runat="server" /> 
             <asp:RequiredFieldValidator ID="Requiredfieldvalidator27" runat="server" ControlToValidate="txtISDFee" CssClass="validator_general" ErrorMessage="Biaya Materai Asuransi salah" Display="Dynamic"></asp:RequiredFieldValidator>
             <asp:RangeValidator ID="Rangevalidator21" runat="server" ControlToValidate="txtISDFee" Type="Currency" MinimumValue="0" ErrorMessage="Biaya Materai Asuransi salah" CssClass="validator_general" Display="Dynamic" MaximumValue="9999999999"></asp:RangeValidator>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label> Biaya Asuransi Collateral Tambahan</label>
            <asp:label ID="lblBiayaPolis" runat="server"  ></asp:label>
            <asp:TextBox ID="txtBiayaPolis" runat="server"  MaxLength="10" onblur="extractNumber(this,2,true);blockInvalid(this);this.value=numberFormat(this.value);this.value=blankToZero(this.value);"
    onkeyup="extractNumber(this,2,true);" onkeypress="return blockNonNumbers(this, event, true, true);"
    onfocus="this.value=resetNumber(this.value);" CssClass="numberAlign" OnChange="javascript:void(0)" autocomplete="off">0</asp:TextBox>
            <asp:DropDownList ID="cboBiayaPolis" runat="server" /> 
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator64" runat="server" ControlToValidate="txtBiayaPolis" CssClass="validator_general" ErrorMessage="Biaya Polis salah" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="Rangevalidator54" runat="server" ControlToValidate="txtBiayaPolis" Type="Currency" MinimumValue="0" ErrorMessage="Biaya Polis salah" CssClass="validator_general" Display="Dynamic" MaximumValue="9999999999"></asp:RangeValidator>
        </div>
    </div>

