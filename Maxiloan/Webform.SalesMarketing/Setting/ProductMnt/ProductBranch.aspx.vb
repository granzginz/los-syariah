﻿#Region "Imports"
Imports Maxiloan.cbse
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
#End Region

Public Class ProductBranch
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private m_controller As New ProductController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region

#Region "Property"
    Private Property IsAuthorized() As Boolean
        Get
            Return CType(viewstate("IsAuthorized"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            viewstate("IsAuthorized") = Value
        End Set
    End Property

    Private Property CmdWhere() As String
        Get
            Return CType(viewstate("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property ProductID() As String
        Get
            Return CType(viewstate("ProductID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ProductID") = Value
        End Set
    End Property

    Private Property Description() As String
        Get
            Return CType(viewstate("Description"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Description") = Value
        End Set
    End Property

    Private Property IsActive() As Boolean
        Get
            Return CType(viewstate("IsActive"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            viewstate("IsActive") = Value
        End Set
    End Property

    Private Property AddEdit() As String
        Get
            Return CType(viewstate("vwsAddEdt"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("vwsAddEdt") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(viewstate("Sort"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Sort") = Value
        End Set
    End Property

    Private Property SPBehaviour() As String
        Get
            Return CType(viewstate("SPBehaviour"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("SPBehaviour") = Value
        End Set
    End Property

    Private Property SP1Behaviour() As String
        Get
            Return CType(viewstate("SP1Behaviour"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("SP1Behaviour") = Value
        End Set
    End Property

    Private Property SP2Behaviour() As String
        Get
            Return CType(viewstate("SP2Behaviour"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("SP2Behaviour") = Value
        End Set
    End Property

    Private Property SPAutomatic() As String
        Get
            Return CType(viewstate("SPAutomatic"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("SPAutomatic") = Value
        End Set
    End Property

    Private Property SP1Automatic() As String
        Get
            Return CType(viewstate("SP1Automatic"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("SP1Automatic") = Value
        End Set
    End Property

    Private Property SP2Automatic() As String
        Get
            Return CType(viewstate("SP2Automatic"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("SP2Automatic") = Value
        End Set
    End Property

    Private Property LengthSP() As Integer
        Get
            Return CType(viewstate("LengthSP"), Integer)
        End Get
        Set(ByVal Value As Integer)
            viewstate("LengthSP") = Value
        End Set
    End Property

    Private Property LengthSP1() As Integer
        Get
            Return CType(viewstate("LengthSP1"), Integer)
        End Get
        Set(ByVal Value As Integer)
            viewstate("LengthSP1") = Value
        End Set
    End Property

    Private Property LengthSP2() As Integer
        Get
            Return CType(viewstate("LengthSP2"), Integer)
        End Get
        Set(ByVal Value As Integer)
            viewstate("LengthSP2") = Value
        End Set
    End Property
#End Region

#Region "Page_Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If
        InitialDefaultPanel()

        Me.FormID = "ProductBranch"
        If Not Me.IsPostBack Then
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If SessionInvalid() Then
                    Exit Sub
                End If
                Me.SearchBy = ""
                Me.SortBy = ""
            End If
            txtPage.Text = "1"
            txtSearchBy.Text = ""
            Me.Sort = "ProductID ASC"

            Me.CmdWhere = "a.BranchID='" & Me.sesBranchId.Replace("'", "") & "'"
            Me.BranchID = Me.sesBranchId.Replace("'", "")


            If IsSingleBranch() And CInt(Replace(Me.sesBranchId, "'", "")) >= 1 And Me.IsHoBranch = False Then
                BindGridEntity(Me.CmdWhere)
            Else
                NotAuthorized()
            End If
        End If
    End Sub
#End Region
    Sub NotAuthorized()
        Dim strHTTPServer As String
        Dim StrHTTPApp As String
        Dim strNameServer As String
        strHTTPServer = Request.ServerVariables("PATH_INFO")
        strNameServer = Request.ServerVariables("SERVER_NAME")
        StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
        Response.Redirect("http://" & strNameServer & "/" & StrHTTPApp & "/error_notauthorized.aspx")
    End Sub
#Region "LinkTo"
    Function LinkTo(ByVal strProductID As String) As String
        Return "javascript:OpenWinProductView('Marketing','" & strProductID & "')"
    End Function
#End Region

#Region "LinkToProductBranch"
    Function LinkToProductBranchHO(ByVal strProductID As String, ByVal strBranchID As String) As String
        Return "javascript:OpenWinProductBranchHOView('" & strProductID & "','" & strBranchID & "')"
    End Function
#End Region

#Region "InitialDefaultPanel"
    Private Sub InitialDefaultPanel()
        pnlList.Visible = True
        pnlEdit.Visible = False        
    End Sub
#End Region

#Region "BindGridEntity"
    Sub BindGridEntity(ByVal cmdWhere As String)
        Dim dtEntity As DataTable
        Dim oProduct As New Parameter.Product

        If Me.IsActive = False Then
            ButtonPrint.Enabled = False
        Else
            ButtonPrint.Enabled = True
        End If

        With oProduct
            .WhereCond = Me.CmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = Me.Sort
            .strConnection = GetConnectionString
        End With
        oProduct = m_controller.ProductBranchPaging(oProduct)

        If Not oProduct Is Nothing Then
            dtEntity = oProduct.ListData
            recordCount = oProduct.TotalRecords
        Else
            recordCount = 0
        End If
        If recordCount = 0 Then
            ButtonPrint.Enabled = False
        Else
            ButtonPrint.Enabled = True
        End If
        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0

        Dim oCustomClass As New Parameter.Common
        Dim oController As New AuthorizationController

        With oCustomClass
            .strConnection = GetConnectionString()
            .LoginId = Me.Loginid
            .FeatureID = "VIEW"
            .FormID = Me.FormID
            .AppID = Me.AppId
        End With
        Me.IsAuthorized = oController.CheckFeature(oCustomClass)

        dtgPaging.DataBind()

        PagingFooter()
    End Sub
#End Region

#Region "dtgPaging_ItemDataBound"
    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        Dim imbEdit As ImageButton
        Dim hynProductID As HyperLink
        If e.Item.ItemIndex >= 0 Then
            imbEdit = CType(e.Item.FindControl("imbEdit"), ImageButton)
            hynProductID = CType(e.Item.FindControl("hynProductID"), HyperLink)
            hynProductID.NavigateUrl = LinkToProductBranchHO(dtgPaging.DataKeys.Item(e.Item.ItemIndex).ToString, Me.BranchID)

            If Not Me.IsAuthorized Then
                Dim strHTTPServer As String
                Dim StrHTTPApp As String
                Dim strNameServer As String

                strHTTPServer = Request.ServerVariables("PATH_INFO")
                StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                strNameServer = Request.ServerVariables("SERVER_NAME")

                hynProductID = CType(e.Item.FindControl("hynProductID"), HyperLink)
                hynProductID.NavigateUrl = "http://" & strNameServer & "/" & StrHTTPApp & "/NotAuthorized.html"
            End If
        End If
    End Sub
#End Region

#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then            
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)            
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub imgbtnPageNumb_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int32)
                BindGridEntity(Me.CmdWhere)
            End If
        End If
    End Sub
#End Region

#Region "Behaviour_Value"
    Function Behaviour_Value(ByVal Behaviour As String) As String
        If Behaviour = "D" Then
            Return "Default"
        ElseIf Behaviour = "L" Then
            Return "Locked"
        ElseIf Behaviour = "N" Then
            Return "Minimum"
        ElseIf Behaviour = "X" Then
            Return "Maximum"
        End If
    End Function
#End Region

#Region "dtgEntity_ItemCommand"
    Private Sub dtgEntity_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        Dim oProduct As New Parameter.Product
        If e.CommandName = "EDIT" Then
            If CheckFeature(Me.Loginid, Me.FormID, "EDIT", Me.AppId) Then
                Me.AddEdit = "EDIT"
                pnlEdit.Visible = True
                pnlList.Visible = False

                oProduct.ProductId = dtgPaging.DataKeys.Item(e.Item.ItemIndex).ToString
                Me.ProductID = oProduct.ProductId
                oProduct.BranchId = Me.BranchID
                oProduct.ProductId = Me.ProductID
                oProduct.strConnection = GetConnectionString
                oProduct = m_controller.ProductBranchEdit(oProduct)

                E_lblBranchID.Text = oProduct.BranchFullName
                E_lblProductID.Text = Me.ProductID
                E_lblDescription.Text = oProduct.Description
                E_lblAssetType.Text = oProduct.AssetType_desc
                E_lblScoreSchemeID.Text = oProduct.ScoreSchemeMaster_desc
                E_lblCreditScoreSchemeID.Text = oProduct.CreditScoreSchemeMaster_desc
                E_lblJournalSchemeID.Text = oProduct.JournalScheme_desc
                E_lblApprovalSchemeID.Text = oProduct.ApprovalTypeScheme_desc
                 
                E_lblAssetUsedNew.Text = IIf(oProduct.AssetUsedNew = "U", "Used", "New")

                lblKegiatanUsaha.Text = oProduct.KegiatanUsahaDesc
                lblJenisPembiayaan.Text = oProduct.JenisPembiayaanDesc
                txtUmurKendaraanFrom.Text = oProduct.UmurKendaraanFrom.ToString
                txtUmurKendaraanTo.Text = oProduct.UmurKendaraanTo.ToString


                SkemaTab.Product = oProduct
                SkemaTab.UCMode = "EDIT"
                SkemaTab.InitUCEditMode()
                 
                BiayaTab.Product = oProduct
                BiayaTab.UCMode = "EDIT"
                BiayaTab.InitUCEditMode()
                 
                AngsuranTab.Product = oProduct
                AngsuranTab.UCMode = "EDIT"
                AngsuranTab.InitUCEditMode()

                CollactionTab.Product = oProduct
                CollactionTab.UCMode = "EDIT"
                CollactionTab.InitUCEditMode()

                FinaceTab.Product = oProduct
                FinaceTab.UCMode = "EDIT"
                FinaceTab.InitUCEditMode()

                'SkemaTab.BehaviourLockSetting()

            'If oProduct.FinanceType = "CF" Then
            '    E_lblFinanceType.Text = "Consumer Finance"
            'ElseIf oProduct.AssetUsedNew = "L" Then
            '    E_lblFinanceType.Text = "Leasing"
            'End If

            'Effective Rate'
                'E_txtEffectiveRate.Text = CStr(FormatNumber(oProduct.EffectiveRate, 6))
                'Hide_EffectiveRate.Value = CStr(FormatNumber(oProduct.EffectiveRate_HO, 6))
                'Hide_EffectiveRateBeh.Value = oProduct.EffectiveRateBehaviour_HO
                'E_cboEffectiveRateBehaviour.SelectedIndex = E_cboEffectiveRateBehaviour.Items.IndexOf(E_cboEffectiveRateBehaviour.Items.FindByValue(oProduct.EffectiveRateBehaviour))
                'If oProduct.EffectiveRateBehaviour_HO = "D" Then
                '    E_txtEffectiveRate.Enabled = True
                '    E_cboEffectiveRateBehaviour.Enabled = True
                '    rgv_EffectiveRate.MinimumValue = "0"
                '    rgv_EffectiveRate.MaximumValue = "100"
                'ElseIf oProduct.EffectiveRateBehaviour_HO = "L" Then
                '    E_txtEffectiveRate.Enabled = False
                '    E_cboEffectiveRateBehaviour.Enabled = False
                'ElseIf oProduct.EffectiveRateBehaviour_HO = "N" Then
                '    E_txtEffectiveRate.Enabled = True
                '    E_cboEffectiveRateBehaviour.Enabled = True
                '    E_cboEffectiveRateBehaviour.Items.Remove(E_cboEffectiveRateBehaviour.Items.FindByValue("X"))
                '    rgv_EffectiveRate.MinimumValue = Hide_EffectiveRate.Value
                '    rgv_EffectiveRate.MaximumValue = "100"
                '    rgv_EffectiveRate.ErrorMessage = "Harap is Suku Bunga Effective antara  " & rgv_EffectiveRate.MinimumValue & " dan 100 "
                'ElseIf oProduct.EffectiveRateBehaviour_HO = "X" Then
                '    E_txtEffectiveRate.Enabled = True
                '    E_cboEffectiveRateBehaviour.Enabled = True
                '    E_cboEffectiveRateBehaviour.Items.Remove(E_cboEffectiveRateBehaviour.Items.FindByValue("N"))
                '    rgv_EffectiveRate.MinimumValue = "0"
                '    rgv_EffectiveRate.MaximumValue = Hide_EffectiveRate.Value
                '    rgv_EffectiveRate.ErrorMessage = "Harap is Suku Bunga Effective antara 1 dan " & rgv_EffectiveRate.MaximumValue
                'End If
                'lblEffectiveRate_Branch.Text = CStr(FormatNumber(oProduct.EffectiveRate_HO, 6)) & " % " & Behaviour_Value(oProduct.EffectiveRateBehaviour_HO)


                ''GrossYieldRate'
                'E_txtGrossYieldRate.Text = CStr(FormatNumber(oProduct.GrossYieldRate, 6))
                'Hide_GrossYieldRate.Value = CStr(FormatNumber(oProduct.GrossYieldRate_HO, 6))
                'Hide_GrossYieldRateBeh.Value = oProduct.GrossYieldRateBehaviour_HO
                'E_cboGrossYieldRateBehaviour.SelectedIndex = E_cboGrossYieldRateBehaviour.Items.IndexOf(E_cboGrossYieldRateBehaviour.Items.FindByValue(oProduct.GrossYieldRateBehaviour))
                'If oProduct.GrossYieldRateBehaviour_HO = "D" Then
                '    E_txtGrossYieldRate.Enabled = True
                '    E_cboGrossYieldRateBehaviour.Enabled = True
                '    rgv_GrossYield.MinimumValue = "0"
                '    rgv_GrossYield.MaximumValue = "100"
                'ElseIf oProduct.GrossYieldRateBehaviour_HO = "L" Then
                '    E_txtGrossYieldRate.Enabled = False
                '    E_cboGrossYieldRateBehaviour.Enabled = False
                'ElseIf oProduct.GrossYieldRateBehaviour_HO = "N" Then
                '    E_txtGrossYieldRate.Enabled = True
                '    E_cboGrossYieldRateBehaviour.Enabled = True
                '    'E_cboGrossYieldRateBehaviour.Items.Remove(E_cboGrossYieldRateBehaviour.Items.FindByValue("X"))
                '    rgv_GrossYield.MinimumValue = Hide_GrossYieldRate.Value
                '    rgv_GrossYield.MaximumValue = "100"
                '    rgv_GrossYield.ErrorMessage = "Harap isi Gross Yield Rate antara  " & rgv_GrossYield.MinimumValue & " dan 100 "
                'ElseIf oProduct.GrossYieldRateBehaviour_HO = "X" Then
                '    E_txtGrossYieldRate.Enabled = True
                '    E_cboGrossYieldRateBehaviour.Enabled = True
                '    'E_cboGrossYieldRateBehaviour.Items.Remove(E_cboGrossYieldRateBehaviour.Items.FindByValue("N"))
                '    rgv_GrossYield.MinimumValue = "0"
                '    rgv_GrossYield.MaximumValue = Hide_EffectiveRate.Value
                '    rgv_GrossYield.ErrorMessage = "Harap isi Gross Yield Rate antara 1 dan " & rgv_GrossYield.MaximumValue
                'End If
                'E_cboGrossYieldRateBehaviour.Items.Remove(E_cboGrossYieldRateBehaviour.Items.FindByValue("X"))
                'E_cboGrossYieldRateBehaviour.Items.Remove(E_cboGrossYieldRateBehaviour.Items.FindByValue("D"))
                'E_cboGrossYieldRateBehaviour.Items.Remove(E_cboGrossYieldRateBehaviour.Items.FindByValue("L"))
                'lblGrossYieldRate_Branch.Text = CStr(FormatNumber(oProduct.GrossYieldRate_HO, 6)) & " % " & Behaviour_Value(oProduct.GrossYieldRateBehaviour_HO)

                'rgv_MaximumTenor.Enabled = True
                'rgv_MinimumTenor.Enabled = True
                ''Minimum Tenor
                'E_txtMinimumTenor.Text = CStr(oProduct.MinimumTenor)
                'Hide_MinimumTenor.Value = CStr(oProduct.MinimumTenor_HO)
                'lblMinimumTenor_Branch.Text = CStr(oProduct.MinimumTenor_HO) & " months"
                'If CInt(E_txtMinimumTenor.Text.Trim) < CInt(Hide_MinimumTenor.Value) Then
                '    rgv_MinimumTenor.MinimumValue = Hide_MinimumTenor.Value
                '    rgv_MinimumTenor.MaximumValue = "999"
                '    rgv_MinimumTenor.ErrorMessage = "Harap isi Jangka Waktu Minimum antara  " & rgv_MinimumTenor.MinimumValue & " dan 999"
                'Else
                '    rgv_MinimumTenor.MinimumValue = "1"
                '    rgv_MinimumTenor.MaximumValue = "999"
                '    rgv_MinimumTenor.ErrorMessage = "Jangka Waktu Minimum Salah"
                'End If

                ''Maximum Tenor
                'E_txtMaximumTenor.Text = CStr(oProduct.MaximumTenor)
                'Hide_MaximumTenor.Value = CStr(oProduct.MaximumTenor_HO)
                'lblMaximumTenor_Branch.Text = CStr(oProduct.MaximumTenor_HO) & " months"
                'If CInt(E_txtMaximumTenor.Text.Trim) > CInt(Hide_MaximumTenor.Value) Then
                '    rgv_MaximumTenor.MinimumValue = "1" ' Hide_MaximumTenor.Value
                '    rgv_MaximumTenor.MaximumValue = Hide_MaximumTenor.Value
                '    rgv_MaximumTenor.ErrorMessage = "Harap isi Jangka Waktu Maximum antara 0 dan " & rgv_MaximumTenor.MaximumValue
                'Else
                '    rgv_MaximumTenor.MinimumValue = "1"
                '    rgv_MaximumTenor.MaximumValue = "999"
                '    rgv_MaximumTenor.ErrorMessage = "Jangka Waktu Maximum Salah"
                'End If

                'If (CInt(E_txtMinimumTenor.Text.Trim) > CInt(E_txtMaximumTenor.Text.Trim)) Then
                '    If (E_txtMinimumTenor.Text.Trim = "1" And E_txtMaximumTenor.Text.Trim = "1") Then
                '        'Emang sengaja dikosongin
                '    ElseIf E_txtMaximumTenor.Text.Trim = "1" Then
                '        rgv_MinimumTenor.MinimumValue = "1"
                '        rgv_MinimumTenor.MaximumValue = E_txtMaximumTenor.Text.Trim
                '        rgv_MinimumTenor.ErrorMessage = "Jangka Waktu Minimum harus lebih kecil dari Jangka Waktu Maximum"

                '        rgv_MaximumTenor.MinimumValue = Hide_MaximumTenor.Value
                '        rgv_MaximumTenor.MaximumValue = "999"
                '        rgv_MaximumTenor.ErrorMessage = "Jangka Waktu Maximum harus lebih besar dari jangka waktu Minimum"
                '    Else
                '        rgv_MinimumTenor.MinimumValue = "1"
                '        rgv_MinimumTenor.MaximumValue = CStr(CInt(E_txtMaximumTenor.Text.Trim) - 1)
                '        rgv_MinimumTenor.ErrorMessage = "Jangka Waktu Minimum harus lebih kecil dari Jangka Waktu Maximum"

                '        rgv_MaximumTenor.MinimumValue = Hide_MaximumTenor.Value
                '        rgv_MaximumTenor.MaximumValue = "999"
                '        rgv_MaximumTenor.ErrorMessage = "Jangka Waktu Maximum harus lebih besar dari Jangka Waktu Minimum"
                '    End If
                'End If

            'Late Charges Percentage (Installment)
                'E_txtPenaltyPercentage.Text = CStr(FormatNumber(oProduct.PenaltyPercentage, 6))
                'Hide_PenaltyPercentage.Value = CStr(FormatNumber(oProduct.PenaltyPercentage_HO, 2))
                'Hide_PenaltyPercentageBeh.Value = oProduct.PenaltyPercentageBehaviour_HO
                'E_cboPenaltyPercentageBehaviour.SelectedIndex = E_cboPenaltyPercentageBehaviour.Items.IndexOf(E_cboPenaltyPercentageBehaviour.Items.FindByValue(oProduct.PenaltyPercentageBehaviour))

                ''=====

                'lblPenaltyRateEffectiveDate_Branch.Text = oProduct.PenaltyRateEffectiveDate_HO.ToString("dd/MM/yyyy")
                'If oProduct.PenaltyBasedOn_HO = "PB" Then
                '    lblPenaltyBasedOn_Branch.Text = "Pokok + Bunga"
                'ElseIf oProduct.PenaltyBasedOn_HO = "PO" Then
                '    lblPenaltyBasedOn_Branch.Text = "Pokok"
                'End If

                'If oProduct.PenaltyBasedOnBehaviour_HO = "D" Then
                '    lblPenaltyBasedOnBehaviour_Branch.Text = "Default"
                'ElseIf oProduct.PenaltyBasedOnBehaviour_HO = "L" Then
                '    lblPenaltyBasedOnBehaviour_Branch.Text = "Locked"
                'ElseIf oProduct.PenaltyBasedOnBehaviour_HO = "N" Then
                '    lblPenaltyBasedOnBehaviour_Branch.Text = "Minimum"
                'ElseIf oProduct.PenaltyBasedOnBehaviour_HO = "X" Then
                '    lblPenaltyBasedOnBehaviour_Branch.Text = "Maximum"
                'End If


                'If oProduct.PrioritasPembayaran_HO = "DE" Then
                '    lblPrioritasPembayaran_Branch.Text = "Denda"
                'ElseIf oProduct.PrioritasPembayaran_HO = "BE" Then
                '    lblPrioritasPembayaran_Branch.Text = "Bebas"
                'End If

                'lblPrepaymentPenaltyEffectiveDate_Branch.Text = oProduct.PrepaymentPenaltyEffectiveDate_HO.ToString("dd/MM/yyyy")
                'lblPrepaymentPenaltyPrevious_Branch.Text = CStr(FormatNumber(oProduct.PrepaymentPenaltyPrevious_HO, 10))


                'lblPrepaymentPenaltyFixed_Branch.Text = CStr(FormatNumber(oProduct.PrepaymentPenaltyFixed_HO, 2))

                'If oProduct.PrepaymentPenaltyFixedBehaviour_HO = "D" Then
                '    lblPrepaymentPenaltyFixedBehaviour_Branch.Text = "Default"
                'ElseIf oProduct.PrepaymentPenaltyFixedBehaviour_HO = "L" Then
                '    lblPrepaymentPenaltyFixedBehaviour_Branch.Text = "Locked"
                'ElseIf oProduct.PrepaymentPenaltyFixedBehaviour_HO = "N" Then
                '    lblPrepaymentPenaltyFixedBehaviour_Branch.Text = "Minimum"
                'ElseIf oProduct.PrepaymentPenaltyFixedBehaviour_HO = "X" Then
                '    lblPrepaymentPenaltyFixedBehaviour_Branch.Text = "Maximum"
                'End If


                'lblPrepaymentPenaltyFixedEffectiveDate_Branch.Text = oProduct.PrepaymentPenaltyFixedEffectiveDate_HO.ToString("dd/MM/yyyy")
                'lblPrepaymentPenaltyFixedPrevious_Branch.Text = CStr(FormatNumber(oProduct.PrepaymentPenaltyFixedPrevious_HO, 2))

                'lblInsuranceDiscPercentage_Branch.Text = CStr(FormatNumber(oProduct.InsuranceDiscPercentage_HO, 10))

                'If oProduct.InsuranceDiscPercentageBehaviour_HO = "D" Then
                '    lblInsuranceDiscPercentageBehaviour_Branch.Text = "Default"
                'ElseIf oProduct.InsuranceDiscPercentageBehaviour_HO = "L" Then
                '    lblInsuranceDiscPercentageBehaviour_Branch.Text = "Locked"
                'ElseIf oProduct.InsuranceDiscPercentageBehaviour_HO = "N" Then
                '    lblInsuranceDiscPercentageBehaviour_Branch.Text = "Minimum"
                'ElseIf oProduct.InsuranceDiscPercentageBehaviour_HO = "X" Then
                '    lblInsuranceDiscPercentageBehaviour_Branch.Text = "Maximum"
                'End If

                'lblIncomeInsRatioPercentage_Branch.Text = CStr(FormatNumber(oProduct.IncomeInsRatioPercentage_HO, 10))

                'If oProduct.IncomeInsRatioPercentageBehaviour_HO = "D" Then
                '    lblIncomeInsRatioPercentageBehaviour_Branch.Text = "Default"
                'ElseIf oProduct.IncomeInsRatioPercentageBehaviour_HO = "L" Then
                '    lblIncomeInsRatioPercentageBehaviour_Branch.Text = "Locked"
                'ElseIf oProduct.IncomeInsRatioPercentageBehaviour_HO = "N" Then
                '    lblIncomeInsRatioPercentageBehaviour_Branch.Text = "Minimum"
                'ElseIf oProduct.IncomeInsRatioPercentageBehaviour_HO = "X" Then
                '    lblIncomeInsRatioPercentageBehaviour_Branch.Text = "Maximum"
                'End If
                ''============

                'txtPrepaymentPenaltyFixed.Text = CStr(FormatNumber(oProduct.PrepaymentPenaltyFixed, 2))
                'cboPrepaymentPenaltyFixedBehaviour.SelectedIndex = cboPrepaymentPenaltyFixedBehaviour.Items.IndexOf(cboPrepaymentPenaltyFixedBehaviour.Items.FindByValue(oProduct.PrepaymentPenaltyFixedBehaviour))
                'cboPenaltyBasedOn.SelectedIndex = cboPenaltyBasedOn.Items.IndexOf(cboPenaltyBasedOn.Items.FindByValue(oProduct.PenaltyBasedOnBehaviour))
                'rboPrioritasPembayaran.SelectedIndex = rboPrioritasPembayaran.Items.IndexOf(rboPrioritasPembayaran.Items.FindByValue(oProduct.PrioritasPembayaran))
                'rboPenaltyBasedOn.SelectedIndex = rboPenaltyBasedOn.Items.IndexOf(rboPenaltyBasedOn.Items.FindByValue(oProduct.PenaltyBasedOn))

                'If oProduct.PrepaymentPenaltyFixedBehaviour = "D" Then
                '    txtPrepaymentPenaltyFixed.Enabled = True
                '    cboPrepaymentPenaltyFixedBehaviour.Enabled = True
                'ElseIf oProduct.PrepaymentPenaltyFixedBehaviour = "L" Then
                '    txtPrepaymentPenaltyFixed.Enabled = False
                '    cboPrepaymentPenaltyFixedBehaviour.Enabled = False
                'ElseIf oProduct.PrepaymentPenaltyFixedBehaviour = "N" Then
                '    txtPrepaymentPenaltyFixed.Enabled = True
                '    cboPrepaymentPenaltyFixedBehaviour.Enabled = True
                '    cboPrepaymentPenaltyFixedBehaviour.Items.Remove(cboPrepaymentPenaltyFixedBehaviour.Items.FindByValue("X"))
                'ElseIf oProduct.PrepaymentPenaltyFixedBehaviour = "X" Then
                '    txtPrepaymentPenaltyFixed.Enabled = True
                '    cboPrepaymentPenaltyFixedBehaviour.Enabled = True
                '    cboPrepaymentPenaltyFixedBehaviour.Items.Remove(cboPrepaymentPenaltyFixedBehaviour.Items.FindByValue("N"))
                'End If

                'If oProduct.PenaltyBasedOnBehaviour = "D" Then
                '    rboPenaltyBasedOn.Enabled = True
                '    cboPenaltyBasedOn.Enabled = True
                'ElseIf oProduct.PenaltyBasedOnBehaviour = "L" Then
                '    rboPenaltyBasedOn.Enabled = False
                '    cboPenaltyBasedOn.Enabled = False
                'ElseIf oProduct.PenaltyBasedOnBehaviour = "N" Then
                '    rboPenaltyBasedOn.Enabled = True
                '    cboPenaltyBasedOn.Enabled = True
                '    cboPenaltyBasedOn.Items.Remove(cboPenaltyBasedOn.Items.FindByValue("X"))
                'ElseIf oProduct.PenaltyBasedOnBehaviour = "X" Then
                '    rboPenaltyBasedOn.Enabled = True
                '    cboPenaltyBasedOn.Enabled = True
                '    cboPenaltyBasedOn.Items.Remove(cboPenaltyBasedOn.Items.FindByValue("N"))
                'End If

                'txtPenaltyRatePrevious.Text = CStr(FormatNumber(oProduct.PenaltyRatePrevious, 10))
                'txtPenaltyRateEffectiveDate.Text = oProduct.PenaltyRateEffectiveDate.ToString("dd/MM/yyyy")
                'txtPrepaymentPenaltyPrevious.Text = CStr(FormatNumber(oProduct.PrepaymentPenaltyPrevious, 10))
                'txtPrepaymentPenaltyEffectiveDate.Text = oProduct.PrepaymentPenaltyEffectiveDate.ToString("dd/MM/yyyy")
                'txtPrepaymentPenaltyFixedPrevious.Text = CStr(FormatNumber(oProduct.PrepaymentPenaltyFixedPrevious, 2))
                'txtPrepaymentPenaltyFixedEffectiveDate.Text = oProduct.PrepaymentPenaltyFixedEffectiveDate.ToString("dd/MM/yyyy")

                'txtInsuranceDiscPercentage.Text = CStr(FormatNumber(oProduct.InsuranceDiscPercentage, 10))
                'cboInsuranceDiscPercentageBehaviour.SelectedIndex = cboInsuranceDiscPercentageBehaviour.Items.IndexOf(cboInsuranceDiscPercentageBehaviour.Items.FindByValue(oProduct.InsuranceDiscPercentageBehaviour))


                'If oProduct.InsuranceDiscPercentageBehaviour = "D" Then
                '    txtInsuranceDiscPercentage.Enabled = True
                '    cboInsuranceDiscPercentageBehaviour.Enabled = True
                'ElseIf oProduct.InsuranceDiscPercentageBehaviour = "L" Then
                '    txtInsuranceDiscPercentage.Enabled = False
                '    cboInsuranceDiscPercentageBehaviour.Enabled = False
                'ElseIf oProduct.InsuranceDiscPercentageBehaviour = "N" Then
                '    txtInsuranceDiscPercentage.Enabled = True
                '    cboInsuranceDiscPercentageBehaviour.Enabled = True
                '    cboInsuranceDiscPercentageBehaviour.Items.Remove(cboInsuranceDiscPercentageBehaviour.Items.FindByValue("X"))
                'ElseIf oProduct.InsuranceDiscPercentageBehaviour = "X" Then
                '    txtInsuranceDiscPercentage.Enabled = True
                '    cboInsuranceDiscPercentageBehaviour.Enabled = True
                '    cboInsuranceDiscPercentageBehaviour.Items.Remove(cboInsuranceDiscPercentageBehaviour.Items.FindByValue("N"))
                'End If

                'txtIncomeInsRatioPercentage.Text = CStr(FormatNumber(oProduct.IncomeInsRatioPercentage, 10))
                'cboIncomeInsRatioPercentageBehaviour.SelectedIndex = cboIncomeInsRatioPercentageBehaviour.Items.IndexOf(cboIncomeInsRatioPercentageBehaviour.Items.FindByValue(oProduct.IncomeInsRatioPercentageBehaviour))

                'If oProduct.IncomeInsRatioPercentageBehaviour = "D" Then
                '    txtIncomeInsRatioPercentage.Enabled = True
                '    cboIncomeInsRatioPercentageBehaviour.Enabled = True
                'ElseIf oProduct.IncomeInsRatioPercentageBehaviour = "L" Then
                '    txtIncomeInsRatioPercentage.Enabled = False
                '    cboIncomeInsRatioPercentageBehaviour.Enabled = False
                'ElseIf oProduct.IncomeInsRatioPercentageBehaviour = "N" Then
                '    txtIncomeInsRatioPercentage.Enabled = True
                '    cboIncomeInsRatioPercentageBehaviour.Enabled = True
                '    cboIncomeInsRatioPercentageBehaviour.Items.Remove(cboIncomeInsRatioPercentageBehaviour.Items.FindByValue("X"))
                'ElseIf oProduct.IncomeInsRatioPercentageBehaviour = "X" Then
                '    txtIncomeInsRatioPercentage.Enabled = True
                '    cboIncomeInsRatioPercentageBehaviour.Enabled = True
                '    cboIncomeInsRatioPercentageBehaviour.Items.Remove(cboIncomeInsRatioPercentageBehaviour.Items.FindByValue("N"))
                'End If

       
            '-----

                'cboIsSPAutomaticBehaviour.SelectedIndex = cboIsSPAutomaticBehaviour.Items.IndexOf(cboIsSPAutomaticBehaviour.Items.FindByValue(oProduct.IsSPAutomaticBehaviour))
                'cboIsSP1AutomaticBehaviour.SelectedIndex = cboIsSP1AutomaticBehaviour.Items.IndexOf(cboIsSP1AutomaticBehaviour.Items.FindByValue(oProduct.IsSP1AutomaticBehaviour))
                'cboIsSP2AutomaticBehaviour.SelectedIndex = cboIsSP2AutomaticBehaviour.Items.IndexOf(cboIsSP2AutomaticBehaviour.Items.FindByValue(oProduct.IsSP2AutomaticBehaviour))

                'If oProduct.PenaltyPercentageBehaviour_HO = "D" Then
                '    E_txtPenaltyPercentage.Enabled = True
                '    E_cboPenaltyPercentageBehaviour.Enabled = True
                '    rgvPenaltyPercentage.MinimumValue = "0"
                '    rgvPenaltyPercentage.MaximumValue = "100"
                'ElseIf oProduct.PenaltyPercentageBehaviour_HO = "L" Then
                '    E_txtPenaltyPercentage.Enabled = False
                '    E_cboPenaltyPercentageBehaviour.Enabled = False
                'ElseIf oProduct.PenaltyPercentageBehaviour_HO = "N" Then
                '    E_txtPenaltyPercentage.Enabled = True
                '    E_cboPenaltyPercentageBehaviour.Enabled = True
                '    E_cboPenaltyPercentageBehaviour.Items.Remove(E_cboPenaltyPercentageBehaviour.Items.FindByValue("X"))
                '    rgvPenaltyPercentage.MinimumValue = Hide_PenaltyPercentage.Value
                '    rgvPenaltyPercentage.MaximumValue = "100"
                '    rgvPenaltyPercentage.ErrorMessage = "Harap isi prosentase Denda Keterlambatan Angsuran antara  " & rgvPenaltyPercentage.MinimumValue & " dan 100 "

                'ElseIf oProduct.PenaltyPercentageBehaviour_HO = "X" Then
                '    E_txtPenaltyPercentage.Enabled = True
                '    E_cboPenaltyPercentageBehaviour.Enabled = True
                '    E_cboPenaltyPercentageBehaviour.Items.Remove(E_cboPenaltyPercentageBehaviour.Items.FindByValue("N"))
                '    rgvPenaltyPercentage.MinimumValue = "1"
                '    rgvPenaltyPercentage.MaximumValue = Hide_PenaltyPercentage.Value
                '    rgvPenaltyPercentage.ErrorMessage = "Harap isi prosentase Denda Keterlambatan Angsuran antara 0 dan  " & rgvPenaltyPercentage.MaximumValue
                'End If
                'lblPenaltyPercentage_Branch.Text = CStr(FormatNumber(oProduct.PenaltyPercentage_HO, 2)) & " ‰ / day " & Behaviour_Value(oProduct.PenaltyPercentageBehaviour_HO)

            'Insurance Late Charges Percentage (Insurance)
                'E_txtInsurancePenaltyPercentage.Text = CStr(FormatNumber(oProduct.InsurancePenaltyPercentage, 6))
                'Hide_InsurancePenaltyPercentage.Value = CStr(oProduct.InsurancePenaltyPercentage_HO)
                'Hide_InsurancePenaltyPercentageBeh.Value = oProduct.InsurancePenaltyPercentageBehaviour_HO
                'E_cboInsurancePenaltyPercentageBehaviour.SelectedIndex = E_cboInsurancePenaltyPercentageBehaviour.Items.IndexOf(E_cboInsurancePenaltyPercentageBehaviour.Items.FindByValue(oProduct.InsurancePenaltyPercentageBehaviour))
                'If oProduct.InsurancePenaltyPercentageBehaviour_HO = "D" Then
                '    E_txtInsurancePenaltyPercentage.Enabled = True
                '    E_cboInsurancePenaltyPercentageBehaviour.Enabled = True
                '    rgv_InsurancePenaltyPercentage.MinimumValue = "0"
                '    rgv_InsurancePenaltyPercentage.MaximumValue = "100"
                'ElseIf oProduct.InsurancePenaltyPercentageBehaviour_HO = "L" Then
                '    E_txtInsurancePenaltyPercentage.Enabled = False
                '    E_cboInsurancePenaltyPercentageBehaviour.Enabled = False
                'ElseIf oProduct.InsurancePenaltyPercentageBehaviour_HO = "N" Then
                '    E_txtInsurancePenaltyPercentage.Enabled = True
                '    E_cboInsurancePenaltyPercentageBehaviour.Enabled = True
                '    E_cboInsurancePenaltyPercentageBehaviour.Items.Remove(E_cboInsurancePenaltyPercentageBehaviour.Items.FindByValue("X"))
                '    rgv_InsurancePenaltyPercentage.MinimumValue = Hide_InsurancePenaltyPercentage.Value
                '    rgv_InsurancePenaltyPercentage.MaximumValue = "100"
                '    rgv_InsurancePenaltyPercentage.ErrorMessage = "Harap isi prosentase Denda Keterlambatan Asuransi antara " & rgv_InsurancePenaltyPercentage.MinimumValue & " and 100 "

                'ElseIf oProduct.InsurancePenaltyPercentageBehaviour_HO = "X" Then
                '    E_txtInsurancePenaltyPercentage.Enabled = True
                '    E_cboInsurancePenaltyPercentageBehaviour.Enabled = True
                '    E_cboInsurancePenaltyPercentageBehaviour.Items.Remove(E_cboInsurancePenaltyPercentageBehaviour.Items.FindByValue("N"))
                '    rgv_InsurancePenaltyPercentage.MinimumValue = "0"
                '    rgv_InsurancePenaltyPercentage.MaximumValue = Hide_InsurancePenaltyPercentage.Value
                '    rgv_InsurancePenaltyPercentage.ErrorMessage = "Harap isi prosentase Denda Keterlambatan Asuransi antara 0 dan " & rgv_InsurancePenaltyPercentage.MaximumValue

                'End If
                'lblInsurancePenaltyPercentage_Branch.Text = CStr(oProduct.InsurancePenaltyPercentage_HO) & " ‰ / day " & Behaviour_Value(oProduct.InsurancePenaltyPercentageBehaviour_HO)

            'Cancellation Fee
                'E_txtCancellationFee.Text = CStr(oProduct.CancellationFee)
                'Hide_CancellationFee.Value = CStr(oProduct.CancellationFee_HO)
                'Hide_CancellationFeeBeh.Value = oProduct.CancellationFeeBehaviour_HO
                'E_cboCancellationFeeBehaviour.SelectedIndex = E_cboCancellationFeeBehaviour.Items.IndexOf(E_cboCancellationFeeBehaviour.Items.FindByValue(oProduct.CancellationFeeBehaviour))
                'If oProduct.CancellationFeeBehaviour_HO = "D" Then
                '    E_txtCancellationFee.Enabled = True
                '    E_cboCancellationFeeBehaviour.Enabled = True
                '    rgv_CancellationFee.MinimumValue = "0"
                '    rgv_CancellationFee.MaximumValue = "9999999999"
                'ElseIf oProduct.CancellationFeeBehaviour_HO = "L" Then
                '    E_txtCancellationFee.Enabled = False
                '    E_cboCancellationFeeBehaviour.Enabled = False
                'ElseIf oProduct.CancellationFeeBehaviour_HO = "N" Then
                '    E_txtCancellationFee.Enabled = True
                '    E_cboCancellationFeeBehaviour.Enabled = True
                '    E_cboCancellationFeeBehaviour.Items.Remove(E_cboCancellationFeeBehaviour.Items.FindByValue("X"))
                '    rgv_CancellationFee.MinimumValue = Hide_CancellationFee.Value
                '    rgv_CancellationFee.MaximumValue = "9999999999"
                '    rgv_CancellationFee.ErrorMessage = "Harap isi Biaya Pembatalan antara " & rgv_CancellationFee.MinimumValue & " dan 9999999999 "
                'ElseIf oProduct.CancellationFeeBehaviour_HO = "X" Then
                '    E_txtCancellationFee.Enabled = True
                '    E_cboCancellationFeeBehaviour.Enabled = True
                '    E_cboCancellationFeeBehaviour.Items.Remove(E_cboCancellationFeeBehaviour.Items.FindByValue("N"))
                '    rgv_CancellationFee.MinimumValue = "0"
                '    rgv_CancellationFee.MaximumValue = Hide_CancellationFee.Value
                '    rgv_CancellationFee.ErrorMessage = "Harap isi Biaya Pembatalan antara 0 dan " & rgv_CancellationFee.MinimumValue

                'End If
                'lblCancellationFee_Branch.Text = CStr(oProduct.CancellationFee_HO) & " " & Behaviour_Value(oProduct.CancellationFeeBehaviour_HO)

                ''Admin Fee
                'E_txtAdminFee.Text = CStr(oProduct.AdminFee)
                'Hide_AdminFee.Value = CStr(oProduct.AdminFee_HO)
                'Hide_AdminFeeBeh.Value = oProduct.AdminFeeBehaviour_HO
                'E_cboAdminFeeBehaviour.SelectedIndex = E_cboAdminFeeBehaviour.Items.IndexOf(E_cboAdminFeeBehaviour.Items.FindByValue(oProduct.AdminFeeBehaviour))
                'If oProduct.AdminFeeBehaviour_HO = "D" Then
                '    E_txtAdminFee.Enabled = True
                '    E_cboAdminFeeBehaviour.Enabled = True
                '    rgv_AdminFee.MinimumValue = "0"
                '    rgv_AdminFee.MaximumValue = "9999999999"
                'ElseIf oProduct.AdminFeeBehaviour_HO = "L" Then
                '    E_txtAdminFee.Enabled = False
                '    E_cboAdminFeeBehaviour.Enabled = False
                'ElseIf oProduct.AdminFeeBehaviour_HO = "N" Then
                '    E_txtAdminFee.Enabled = True
                '    E_cboAdminFeeBehaviour.Enabled = True
                '    E_cboAdminFeeBehaviour.Items.Remove(E_cboAdminFeeBehaviour.Items.FindByValue("X"))
                '    rgv_AdminFee.MinimumValue = Hide_AdminFee.Value
                '    rgv_AdminFee.MaximumValue = "9999999999"
                '    rgv_AdminFee.ErrorMessage = "Harap isi Biaya Administrasi antara " & rgv_AdminFee.MinimumValue & " dan 9999999999 "

                'ElseIf oProduct.AdminFeeBehaviour_HO = "X" Then
                '    E_txtAdminFee.Enabled = True
                '    E_cboAdminFeeBehaviour.Enabled = True
                '    E_cboAdminFeeBehaviour.Items.Remove(E_cboAdminFeeBehaviour.Items.FindByValue("N"))
                '    rgv_AdminFee.MinimumValue = "0"
                '    rgv_AdminFee.MaximumValue = Hide_AdminFee.Value
                '    rgv_AdminFee.ErrorMessage = "Harap isi Biaya Administrasi antara 0 dan " & rgv_AdminFee.MinimumValue
                'End If
                'lblAdminFee_Branch.Text = CStr(oProduct.AdminFee_HO) & " " & Behaviour_Value(oProduct.AdminFeeBehaviour_HO)

                ''Fiducia Fee
                'E_txtFiduciaFee.Text = CStr(oProduct.FiduciaFee)
                'Hide_FiduciaFee.Value = CStr(oProduct.FiduciaFee_HO)
                'Hide_FiduciaFeeBeh.Value = oProduct.FiduciaFeeBehaviour_HO
                'E_cboFiduciaFeeBehaviour.SelectedIndex = E_cboFiduciaFeeBehaviour.Items.IndexOf(E_cboFiduciaFeeBehaviour.Items.FindByValue(oProduct.FiduciaFeeBehaviour))
                'If oProduct.FiduciaFeeBehaviour_HO = "D" Then
                '    E_txtFiduciaFee.Enabled = True
                '    E_cboFiduciaFeeBehaviour.Enabled = True
                '    rgv_FiduciaFee.MinimumValue = "0"
                '    rgv_FiduciaFee.MaximumValue = "9999999999"
                'ElseIf oProduct.FiduciaFeeBehaviour_HO = "L" Then
                '    E_txtFiduciaFee.Enabled = False
                '    E_cboFiduciaFeeBehaviour.Enabled = False
                'ElseIf oProduct.FiduciaFeeBehaviour_HO = "N" Then
                '    E_txtFiduciaFee.Enabled = True
                '    E_cboFiduciaFeeBehaviour.Enabled = True
                '    E_cboFiduciaFeeBehaviour.Items.Remove(E_cboFiduciaFeeBehaviour.Items.FindByValue("X"))
                '    rgv_FiduciaFee.MinimumValue = Hide_FiduciaFee.Value
                '    rgv_FiduciaFee.MaximumValue = "9999999999"
                '    rgv_FiduciaFee.ErrorMessage = "Harap isi Biaya Fidusia antara " & rgv_FiduciaFee.MinimumValue & " dan 9999999999 "
                'ElseIf oProduct.FiduciaFeeBehaviour_HO = "X" Then
                '    E_txtFiduciaFee.Enabled = True
                '    E_cboFiduciaFeeBehaviour.Enabled = True
                '    E_cboFiduciaFeeBehaviour.Items.Remove(E_cboFiduciaFeeBehaviour.Items.FindByValue("N"))
                '    rgv_FiduciaFee.MinimumValue = "0"
                '    rgv_FiduciaFee.MaximumValue = Hide_FiduciaFee.Value
                '    rgv_FiduciaFee.ErrorMessage = "Harap isi Biaya Fidusia antara 0 dan " & rgv_FiduciaFee.MinimumValue
                'End If
                'lblFiduciaFee_Branch.Text = CStr(oProduct.FiduciaFee_HO) & " " & Behaviour_Value(oProduct.FiduciaFeeBehaviour_HO)


            'Provision Fee
                'E_txtProvisionFee.Text = CStr(oProduct.ProvisionFeeHO)
                'Hide_ProvisionFee.Value = CStr(oProduct.ProvisionFeeHO)
                'Hide_ProvisionFeeBeh.Value = oProduct.ProvisionFeeBehaviour_HO
                'E_cboProvisionFeeBehaviour.SelectedIndex = E_cboProvisionFeeBehaviour.Items.IndexOf(E_cboProvisionFeeBehaviour.Items.FindByValue(oProduct.ProvisionFeeBehaviour))
                'If oProduct.ProvisionFeeBehaviour_HO = "D" Then
                '    E_txtProvisionFee.Enabled = True
                '    E_cboProvisionFeeBehaviour.Enabled = True
                '    rgv_ProvisionFee.MinimumValue = "0"
                '    rgv_ProvisionFee.MaximumValue = "9999999999"
                'ElseIf oProduct.ProvisionFeeBehaviour_HO = "L" Then
                '    E_txtProvisionFee.Enabled = False
                '    E_cboProvisionFeeBehaviour.Enabled = False
                'ElseIf oProduct.ProvisionFeeBehaviour_HO = "N" Then
                '    E_txtProvisionFee.Enabled = True
                '    E_cboProvisionFeeBehaviour.Enabled = True
                '    E_cboProvisionFeeBehaviour.Items.Remove(E_cboProvisionFeeBehaviour.Items.FindByValue("X"))
                '    rgv_ProvisionFee.MinimumValue = Hide_ProvisionFee.Value
                '    rgv_ProvisionFee.MaximumValue = "9999999999"
                '    rgv_ProvisionFee.ErrorMessage = "Harap isi Biaya Provisi antara " & rgv_ProvisionFee.MinimumValue & " dan 9999999999 "
                'ElseIf oProduct.ProvisionFeeBehaviour_HO = "X" Then
                '    E_txtProvisionFee.Enabled = True
                '    E_cboProvisionFeeBehaviour.Enabled = True
                '    E_cboProvisionFeeBehaviour.Items.Remove(E_cboProvisionFeeBehaviour.Items.FindByValue("N"))
                '    rgv_ProvisionFee.MinimumValue = "0"
                '    rgv_ProvisionFee.MaximumValue = Hide_ProvisionFee.Value
                '    rgv_ProvisionFee.ErrorMessage = "Harap isi Biaya Provisi antara 0 dan " & rgv_ProvisionFee.MinimumValue
                'End If
                'lblProvisionFee_Branch.Text = CStr(oProduct.ProvisionFeeHO) & " " & Behaviour_Value(oProduct.ProvisionFeeBehaviour_HO)

                ''Notary Fee
                'E_txtNotaryFee.Text = CStr(oProduct.NotaryFee)
                'Hide_NotaryFee.Value = CStr(oProduct.NotaryFee_HO)
                'Hide_NotaryFeeBeh.Value = oProduct.NotaryFeeBehaviour_HO
                'E_cboNotaryFeeBehaviour.SelectedIndex = E_cboNotaryFeeBehaviour.Items.IndexOf(E_cboNotaryFeeBehaviour.Items.FindByValue(oProduct.NotaryFeeBehaviour))
                'If oProduct.NotaryFeeBehaviour_HO = "D" Then
                '    E_txtNotaryFee.Enabled = True
                '    E_cboNotaryFeeBehaviour.Enabled = True
                '    rgv_NotaryFee.MinimumValue = "0"
                '    rgv_NotaryFee.MaximumValue = "9999999999"
                'ElseIf oProduct.NotaryFeeBehaviour_HO = "L" Then
                '    E_txtNotaryFee.Enabled = False
                '    E_cboNotaryFeeBehaviour.Enabled = False
                'ElseIf oProduct.NotaryFeeBehaviour_HO = "N" Then
                '    E_txtNotaryFee.Enabled = True
                '    E_cboNotaryFeeBehaviour.Enabled = True
                '    E_cboNotaryFeeBehaviour.Items.Remove(E_cboNotaryFeeBehaviour.Items.FindByValue("X"))
                '    rgv_NotaryFee.MinimumValue = Hide_NotaryFee.Value
                '    rgv_NotaryFee.MaximumValue = "9999999999"
                '    rgv_NotaryFee.ErrorMessage = "Harap isi Biaya Notaris antara " & rgv_NotaryFee.MinimumValue & " dan 9999999999 "
                'ElseIf oProduct.NotaryFeeBehaviour_HO = "X" Then
                '    E_txtNotaryFee.Enabled = True
                '    E_cboNotaryFeeBehaviour.Enabled = True
                '    E_cboNotaryFeeBehaviour.Items.Remove(E_cboNotaryFeeBehaviour.Items.FindByValue("N"))
                '    rgv_NotaryFee.MinimumValue = "0"
                '    rgv_NotaryFee.MaximumValue = Hide_NotaryFee.Value
                '    rgv_NotaryFee.ErrorMessage = "Harap isi Biaya Notaris antara 0 dan " & rgv_NotaryFee.MinimumValue
                'End If
                'lblNotaryFee_Branch.Text = CStr(oProduct.NotaryFee_HO) & " " & Behaviour_Value(oProduct.NotaryFeeBehaviour_HO)


                ''Survey Fee
                'E_txtSurveyFee.Text = CStr(oProduct.SurveyFee)
                'Hide_SurveyFee.Value = CStr(oProduct.SurveyFee_HO)
                'Hide_SurveyFeeBeh.Value = oProduct.SurveyFeeBehaviour_HO
                'E_cboSurveyFeeBehaviour.SelectedIndex = E_cboSurveyFeeBehaviour.Items.IndexOf(E_cboSurveyFeeBehaviour.Items.FindByValue(oProduct.SurveyFeeBehaviour))
                'If oProduct.SurveyFeeBehaviour_HO = "D" Then
                '    E_txtSurveyFee.Enabled = True
                '    E_cboSurveyFeeBehaviour.Enabled = True
                '    rgv_SurveyFee.MinimumValue = "0"
                '    rgv_SurveyFee.MaximumValue = "9999999999"
                'ElseIf oProduct.SurveyFeeBehaviour_HO = "L" Then
                '    E_txtSurveyFee.Enabled = False
                '    E_cboSurveyFeeBehaviour.Enabled = False
                'ElseIf oProduct.SurveyFeeBehaviour_HO = "N" Then
                '    E_txtSurveyFee.Enabled = True
                '    E_cboSurveyFeeBehaviour.Enabled = True
                '    E_cboSurveyFeeBehaviour.Items.Remove(E_cboSurveyFeeBehaviour.Items.FindByValue("X"))
                '    rgv_SurveyFee.MinimumValue = Hide_SurveyFee.Value
                '    rgv_SurveyFee.MaximumValue = "9999999999"
                '    rgv_SurveyFee.ErrorMessage = "Harap isi Biaya Survey antara " & rgv_SurveyFee.MinimumValue & " dan 9999999999 "
                'ElseIf oProduct.SurveyFeeBehaviour_HO = "X" Then
                '    E_txtSurveyFee.Enabled = True
                '    E_cboSurveyFeeBehaviour.Enabled = True
                '    E_cboSurveyFeeBehaviour.Items.Remove(E_cboSurveyFeeBehaviour.Items.FindByValue("N"))
                '    rgv_SurveyFee.MinimumValue = "0"
                '    rgv_SurveyFee.MaximumValue = Hide_SurveyFee.Value
                '    rgv_SurveyFee.ErrorMessage = "Harap isi Biaya Survey antara 0 dan " & rgv_SurveyFee.MinimumValue
                'End If
                'lblSurveyFee_Branch.Text = CStr(oProduct.SurveyFee_HO) & " " & Behaviour_Value(oProduct.SurveyFeeBehaviour_HO)


            'Visit Fee
                'E_txtVisitFee.Text = CStr(oProduct.VisitFee)
                'Hide_VisitFee.Value = CStr(oProduct.VisitFee_HO)
                'Hide_VisitFeeBeh.Value = oProduct.VisitFeeBehaviour_HO
                'E_cboVisitFeeBehaviour.SelectedIndex = E_cboVisitFeeBehaviour.Items.IndexOf(E_cboVisitFeeBehaviour.Items.FindByValue(oProduct.VisitFeeBehaviour))
                'If oProduct.VisitFeeBehaviour_HO = "D" Then
                '    E_txtVisitFee.Enabled = True
                '    E_cboVisitFeeBehaviour.Enabled = True
                '    rgv_VisitFee.MinimumValue = "0"
                '    rgv_VisitFee.MaximumValue = "9999999999"
                'ElseIf oProduct.VisitFeeBehaviour_HO = "L" Then
                '    E_txtVisitFee.Enabled = False
                '    E_cboVisitFeeBehaviour.Enabled = False
                'ElseIf oProduct.VisitFeeBehaviour_HO = "N" Then
                '    E_txtVisitFee.Enabled = True
                '    E_cboVisitFeeBehaviour.Enabled = True
                '    E_cboVisitFeeBehaviour.Items.Remove(E_cboVisitFeeBehaviour.Items.FindByValue("X"))
                '    rgv_VisitFee.MinimumValue = Hide_VisitFee.Value
                '    rgv_VisitFee.MaximumValue = "9999999999"
                '    rgv_VisitFee.ErrorMessage = "Harap isi Biaya Kunjungan antara " & rgv_VisitFee.MinimumValue & " dan 9999999999 "
                'ElseIf oProduct.VisitFeeBehaviour_HO = "X" Then
                '    E_txtVisitFee.Enabled = True
                '    E_cboVisitFeeBehaviour.Enabled = True
                '    E_cboVisitFeeBehaviour.Items.Remove(E_cboVisitFeeBehaviour.Items.FindByValue("N"))
                '    rgv_VisitFee.MinimumValue = "0"
                '    rgv_VisitFee.MaximumValue = Hide_VisitFee.Value
                '    rgv_VisitFee.ErrorMessage = "Harap isi Biaya Kunjungan antara 0 dan " & rgv_VisitFee.MinimumValue
                'End If
                'lblVisitFee_Branch.Text = CStr(oProduct.VisitFee_HO) & " " & Behaviour_Value(oProduct.VisitFeeBehaviour_HO)


                ''Rescheduling Fee
                'E_txtReschedulingFee.Text = CStr(oProduct.ReschedulingFee)
                'Hide_ReschedulingFee.Value = CStr(oProduct.ReschedulingFee_HO)
                'Hide_ReschedulingFeeBeh.Value = oProduct.ReschedulingFeeBehaviour_HO
                'E_cboReschedulingFeeBehaviour.SelectedIndex = E_cboReschedulingFeeBehaviour.Items.IndexOf(E_cboReschedulingFeeBehaviour.Items.FindByValue(oProduct.ReschedulingFeeBehaviour))
                'If oProduct.ReschedulingFeeBehaviour_HO = "D" Then
                '    E_txtReschedulingFee.Enabled = True
                '    E_cboReschedulingFeeBehaviour.Enabled = True
                '    rgv_ReschedulingFee.MinimumValue = "0"
                '    rgv_ReschedulingFee.MaximumValue = "9999999999"
                'ElseIf oProduct.ReschedulingFeeBehaviour_HO = "L" Then
                '    E_txtReschedulingFee.Enabled = False
                '    E_cboReschedulingFeeBehaviour.Enabled = False
                'ElseIf oProduct.ReschedulingFeeBehaviour_HO = "N" Then
                '    E_txtReschedulingFee.Enabled = True
                '    E_cboReschedulingFeeBehaviour.Enabled = True
                '    E_cboReschedulingFeeBehaviour.Items.Remove(E_cboReschedulingFeeBehaviour.Items.FindByValue("X"))
                '    rgv_ReschedulingFee.MinimumValue = Hide_ReschedulingFee.Value
                '    rgv_ReschedulingFee.MaximumValue = "9999999999"
                '    rgv_ReschedulingFee.ErrorMessage = "Harap isi Biaya Rescheduling antara " & rgv_ReschedulingFee.MinimumValue & " dan 9999999999 "
                'ElseIf oProduct.ReschedulingFeeBehaviour_HO = "X" Then
                '    E_txtReschedulingFee.Enabled = True
                '    E_cboReschedulingFeeBehaviour.Enabled = True
                '    E_cboReschedulingFeeBehaviour.Items.Remove(E_cboReschedulingFeeBehaviour.Items.FindByValue("N"))
                '    rgv_ReschedulingFee.MinimumValue = "0"
                '    rgv_ReschedulingFee.MaximumValue = Hide_ReschedulingFee.Value
                '    rgv_ReschedulingFee.ErrorMessage = "Harap isi Biaya Rescheduling antara 0 dan " & rgv_ReschedulingFee.MinimumValue
                'End If
                'lblReschedulingFee_Branch.Text = CStr(oProduct.ReschedulingFee_HO) & " " & Behaviour_Value(oProduct.ReschedulingFeeBehaviour_HO)

                ''AgreementTransfer Fee
                'E_txtAgreementTransferFee.Text = CStr(oProduct.AgreementTransferFee)
                'Hide_AgreementTransferFee.Value = CStr(oProduct.AgreementTransferFee_HO)
                'Hide_AgreementTransferFeeBeh.Value = oProduct.AgreementTransferFeeBehaviour_HO
                'E_cboAgreementTransferFeeBehaviour.SelectedIndex = E_cboAgreementTransferFeeBehaviour.Items.IndexOf(E_cboAgreementTransferFeeBehaviour.Items.FindByValue(oProduct.AgreementTransferFeeBehaviour))
                'If oProduct.AgreementTransferFeeBehaviour_HO = "D" Then
                '    E_txtAgreementTransferFee.Enabled = True
                '    E_cboAgreementTransferFeeBehaviour.Enabled = True
                '    rgv_AgreementTransferFee.MinimumValue = "0"
                '    rgv_AgreementTransferFee.MaximumValue = "9999999999"
                'ElseIf oProduct.AgreementTransferFeeBehaviour_HO = "L" Then
                '    E_txtAgreementTransferFee.Enabled = False
                '    E_cboAgreementTransferFeeBehaviour.Enabled = False
                'ElseIf oProduct.AgreementTransferFeeBehaviour_HO = "N" Then
                '    E_txtAgreementTransferFee.Enabled = True
                '    E_cboAgreementTransferFeeBehaviour.Enabled = True
                '    E_cboAgreementTransferFeeBehaviour.Items.Remove(E_cboAgreementTransferFeeBehaviour.Items.FindByValue("X"))
                '    rgv_AgreementTransferFee.MinimumValue = Hide_AgreementTransferFee.Value
                '    rgv_AgreementTransferFee.MaximumValue = "9999999999"
                '    rgv_AgreementTransferFee.ErrorMessage = "Harap isi Biaya OverKontrak antara " & rgv_AgreementTransferFee.MinimumValue & " dan 9999999999 "
                'ElseIf oProduct.AgreementTransferFeeBehaviour_HO = "X" Then
                '    E_txtAgreementTransferFee.Enabled = True
                '    E_cboAgreementTransferFeeBehaviour.Enabled = True
                '    E_cboAgreementTransferFeeBehaviour.Items.Remove(E_cboAgreementTransferFeeBehaviour.Items.FindByValue("N"))
                '    rgv_AgreementTransferFee.MinimumValue = "0"
                '    rgv_AgreementTransferFee.MaximumValue = Hide_AgreementTransferFee.Value
                '    rgv_AgreementTransferFee.ErrorMessage = "Harap isi Biaya OverKontrak antara 0 dan " & rgv_AgreementTransferFee.MinimumValue
                'End If
                'lblAgreementTransferFee_Branch.Text = CStr(oProduct.AgreementTransferFee_HO) & " " & Behaviour_Value(oProduct.AgreementTransferFeeBehaviour_HO)

            'ChangeDueDate Fee
                'E_txtChangeDueDateFee.Text = CStr(oProduct.ChangeDueDateFee)
                'Hide_ChangeDueDateFee.Value = CStr(oProduct.ChangeDueDateFee_HO)
                'Hide_ChangeDueDateFeeBeh.Value = oProduct.ChangeDueDateFeeBehaviour_HO
                'E_cboChangeDueDateFee.SelectedIndex = E_cboChangeDueDateFee.Items.IndexOf(E_cboChangeDueDateFee.Items.FindByValue(oProduct.ChangeDueDateFeeBehaviour))
                'If oProduct.ChangeDueDateFeeBehaviour_HO = "D" Then
                '    E_txtChangeDueDateFee.Enabled = True
                '    E_cboChangeDueDateFee.Enabled = True
                '    rgv_ChangeDueDateFee.MinimumValue = "0"
                '    rgv_ChangeDueDateFee.MaximumValue = "9999999999"
                'ElseIf oProduct.ChangeDueDateFeeBehaviour_HO = "L" Then
                '    E_txtChangeDueDateFee.Enabled = False
                '    E_cboChangeDueDateFee.Enabled = False
                'ElseIf oProduct.ChangeDueDateFeeBehaviour_HO = "N" Then
                '    E_txtChangeDueDateFee.Enabled = True
                '    E_cboChangeDueDateFee.Enabled = True
                '    E_cboChangeDueDateFee.Items.Remove(E_cboChangeDueDateFee.Items.FindByValue("X"))
                '    rgv_ChangeDueDateFee.MinimumValue = Hide_ChangeDueDateFee.Value
                '    rgv_ChangeDueDateFee.MaximumValue = "9999999999"
                '    rgv_ChangeDueDateFee.ErrorMessage = "Harap isi Biaya Ganti Tanggal jatuh Tempo antara " & rgv_ChangeDueDateFee.MinimumValue & " dan 9999999999 "
                'ElseIf oProduct.ChangeDueDateFeeBehaviour_HO = "X" Then
                '    E_txtChangeDueDateFee.Enabled = True
                '    E_cboChangeDueDateFee.Enabled = True
                '    E_cboChangeDueDateFee.Items.Remove(E_cboChangeDueDateFee.Items.FindByValue("N"))
                '    rgv_ChangeDueDateFee.MinimumValue = "0"
                '    rgv_ChangeDueDateFee.MaximumValue = Hide_ChangeDueDateFee.Value
                '    rgv_ChangeDueDateFee.ErrorMessage = "Harap isi Biaya Ganti Tanggal jatuh Tempo antara 0 dan " & rgv_ChangeDueDateFee.MinimumValue
                'End If
                'lblChangeDueDateFee_Branch.Text = CStr(oProduct.ChangeDueDateFee_HO) & " " & Behaviour_Value(oProduct.ChangeDueDateFeeBehaviour_HO)

                ''AssetReplacement Fee
                'E_txtAssetReplacementFee.Text = CStr(oProduct.AssetReplacementFee)
                'Hide_AssetReplacementFee.Value = CStr(oProduct.AssetReplacementFee_HO)
                'Hide_AssetReplacementFeeBeh.Value = oProduct.AssetReplacementFeeBehaviour_HO
                'E_cboAssetReplacementFeeBehaviour.SelectedIndex = E_cboAssetReplacementFeeBehaviour.Items.IndexOf(E_cboAssetReplacementFeeBehaviour.Items.FindByValue(oProduct.AssetReplacementFeeBehaviour))
                'If oProduct.AssetReplacementFeeBehaviour_HO = "D" Then
                '    E_txtAssetReplacementFee.Enabled = True
                '    E_cboAssetReplacementFeeBehaviour.Enabled = True
                '    rgv_AssetReplacementFee.MinimumValue = "0"
                '    rgv_AssetReplacementFee.MaximumValue = "9999999999"
                'ElseIf oProduct.AssetReplacementFeeBehaviour_HO = "L" Then
                '    E_txtAssetReplacementFee.Enabled = False
                '    E_cboAssetReplacementFeeBehaviour.Enabled = False
                'ElseIf oProduct.AssetReplacementFeeBehaviour_HO = "N" Then
                '    E_txtAssetReplacementFee.Enabled = True
                '    E_cboAssetReplacementFeeBehaviour.Enabled = True
                '    E_cboAssetReplacementFeeBehaviour.Items.Remove(E_cboAssetReplacementFeeBehaviour.Items.FindByValue("X"))
                '    rgv_AssetReplacementFee.MinimumValue = Hide_AssetReplacementFee.Value
                '    rgv_AssetReplacementFee.MaximumValue = "9999999999"
                '    rgv_AssetReplacementFee.ErrorMessage = "Harap isi Biaya Ganti Asset antara " & rgv_AssetReplacementFee.MinimumValue & " dan 9999999999 "
                'ElseIf oProduct.AssetReplacementFeeBehaviour_HO = "X" Then
                '    E_txtAssetReplacementFee.Enabled = True
                '    E_cboAssetReplacementFeeBehaviour.Enabled = True
                '    E_cboAssetReplacementFeeBehaviour.Items.Remove(E_cboAssetReplacementFeeBehaviour.Items.FindByValue("N"))
                '    rgv_AssetReplacementFee.MinimumValue = "0"
                '    rgv_AssetReplacementFee.MaximumValue = Hide_AssetReplacementFee.Value
                '    rgv_AssetReplacementFee.ErrorMessage = "Harap isi Biaya Ganti Asset antara 0 dan " & rgv_AssetReplacementFee.MinimumValue
                'End If
                'lblAssetReplacementFee_Branch.Text = CStr(oProduct.AssetReplacementFee_HO) & " " & Behaviour_Value(oProduct.AssetReplacementFeeBehaviour_HO)

                ''Reposses Fee
                'E_txtRepossesFee.Text = CStr(oProduct.RepossesFee)
                'Hide_RepossesFee.Value = CStr(oProduct.RepossesFee_HO)
                'Hide_RepossesFeeBeh.Value = oProduct.RepossesFeeBehaviour_HO
                'E_cboRepossesFeeBehaviour.SelectedIndex = E_cboRepossesFeeBehaviour.Items.IndexOf(E_cboRepossesFeeBehaviour.Items.FindByValue(oProduct.RepossesFeeBehaviour))
                'If oProduct.RepossesFeeBehaviour_HO = "D" Then
                '    E_txtRepossesFee.Enabled = True
                '    E_cboRepossesFeeBehaviour.Enabled = True
                '    rgv_RepossesFee.MinimumValue = "0"
                '    rgv_RepossesFee.MaximumValue = "9999999999"
                'ElseIf oProduct.RepossesFeeBehaviour_HO = "L" Then
                '    E_txtRepossesFee.Enabled = False
                '    E_cboRepossesFeeBehaviour.Enabled = False
                'ElseIf oProduct.RepossesFeeBehaviour_HO = "N" Then
                '    E_txtRepossesFee.Enabled = True
                '    E_cboRepossesFeeBehaviour.Enabled = True
                '    E_cboRepossesFeeBehaviour.Items.Remove(E_cboRepossesFeeBehaviour.Items.FindByValue("X"))
                '    rgv_RepossesFee.MinimumValue = Hide_RepossesFee.Value
                '    rgv_RepossesFee.MaximumValue = "9999999999"
                '    rgv_RepossesFee.ErrorMessage = "Harap isi Biaya Tarik Asset antara " & rgv_RepossesFee.MinimumValue & " dan 9999999999 "
                'ElseIf oProduct.RepossesFeeBehaviour_HO = "X" Then
                '    E_txtRepossesFee.Enabled = True
                '    E_cboRepossesFeeBehaviour.Enabled = True
                '    E_cboRepossesFeeBehaviour.Items.Remove(E_cboRepossesFeeBehaviour.Items.FindByValue("N"))
                '    rgv_RepossesFee.MinimumValue = "0"
                '    rgv_RepossesFee.MaximumValue = Hide_RepossesFee.Value
                '    rgv_RepossesFee.ErrorMessage = "Harap isi Biaya Tarik Asset antara 0 dan " & rgv_RepossesFee.MinimumValue
                'End If
                'lblRepossesFee_Branch.Text = CStr(oProduct.RepossesFee_HO) & " " & Behaviour_Value(oProduct.RepossesFeeBehaviour_HO)

                ''LegalisirDoc Fee
                'E_LegalizedDocumentFee.Text = CStr(oProduct.LegalisirDocFee)
                'Hide_LegalizedDocumentFee.Value = CStr(oProduct.LegalisirDocFee_HO)
                'Hide_LegalizedDocumentFeeBeh.Value = oProduct.LegalisirDocFeeBehaviour_HO
                'E_cboLegalizedDocumentFeeBehaviour.SelectedIndex = E_cboLegalizedDocumentFeeBehaviour.Items.IndexOf(E_cboLegalizedDocumentFeeBehaviour.Items.FindByValue(oProduct.LegalisirDocFeeBehaviour))
                'If oProduct.LegalisirDocFeeBehaviour_HO = "D" Then
                '    E_LegalizedDocumentFee.Enabled = True
                '    E_cboLegalizedDocumentFeeBehaviour.Enabled = True
                '    rgv_LegalizedDocumentFee.MinimumValue = "0"
                '    rgv_LegalizedDocumentFee.MaximumValue = "9999999999"
                'ElseIf oProduct.LegalisirDocFeeBehaviour_HO = "L" Then
                '    E_LegalizedDocumentFee.Enabled = False
                '    E_cboLegalizedDocumentFeeBehaviour.Enabled = False
                'ElseIf oProduct.LegalisirDocFeeBehaviour_HO = "N" Then
                '    E_cboLegalizedDocumentFeeBehaviour.Enabled = True
                '    E_LegalizedDocumentFee.Enabled = True
                '    E_cboLegalizedDocumentFeeBehaviour.Items.Remove(E_cboLegalizedDocumentFeeBehaviour.Items.FindByValue("X"))
                '    rgv_LegalizedDocumentFee.MinimumValue = Hide_LegalizedDocumentFee.Value
                '    rgv_LegalizedDocumentFee.MaximumValue = "9999999999"
                '    rgv_LegalizedDocumentFee.ErrorMessage = "Harap isi Biaya Legalisasi Dokumen antara " & rgv_LegalizedDocumentFee.MinimumValue & " dan 9999999999 "
                'ElseIf oProduct.LegalisirDocFeeBehaviour_HO = "X" Then
                '    E_cboLegalizedDocumentFeeBehaviour.Enabled = True
                '    E_LegalizedDocumentFee.Enabled = True
                '    E_cboLegalizedDocumentFeeBehaviour.Items.Remove(E_cboLegalizedDocumentFeeBehaviour.Items.FindByValue("N"))
                '    rgv_LegalizedDocumentFee.MinimumValue = "0"
                '    rgv_LegalizedDocumentFee.MaximumValue = Hide_LegalizedDocumentFee.Value
                '    rgv_LegalizedDocumentFee.ErrorMessage = "Harap isi Biaya Legalisasi Dokumen antara 0 dan " & rgv_LegalizedDocumentFee.MinimumValue
                'End If
                'lblLegalizedDocumentFee_Branch.Text = CStr(oProduct.LegalisirDocFee_HO) & " " & Behaviour_Value(oProduct.LegalisirDocFeeBehaviour_HO)

            'PDCBounce Fee
                'E_PDCBounceFee.Text = CStr(oProduct.PDCBounceFee)
                'Hide_PDCBounceFee.Value = CStr(oProduct.PDCBounceFee_HO)
                'Hide_PDCBounceFeeBeh.Value = oProduct.PDCBounceFeeBehaviour_HO
                'E_cboPDCBounceFeeBehaviour.SelectedIndex = E_cboPDCBounceFeeBehaviour.Items.IndexOf(E_cboPDCBounceFeeBehaviour.Items.FindByValue(oProduct.PDCBounceFeeBehaviour))
                'If oProduct.PDCBounceFeeBehaviour_HO = "D" Then
                '    E_PDCBounceFee.Enabled = True
                '    E_cboPDCBounceFeeBehaviour.Enabled = True
                '    rgv_PDCBounceFee.MinimumValue = "0"
                '    rgv_PDCBounceFee.MaximumValue = "9999999999"
                'ElseIf oProduct.PDCBounceFeeBehaviour_HO = "L" Then
                '    E_PDCBounceFee.Enabled = False
                '    E_cboPDCBounceFeeBehaviour.Enabled = False
                'ElseIf oProduct.PDCBounceFeeBehaviour_HO = "N" Then
                '    E_PDCBounceFee.Enabled = True
                '    E_cboPDCBounceFeeBehaviour.Enabled = True
                '    E_cboPDCBounceFeeBehaviour.Items.Remove(E_cboPDCBounceFeeBehaviour.Items.FindByValue("X"))
                '    rgv_PDCBounceFee.MinimumValue = Hide_PDCBounceFee.Value
                '    rgv_PDCBounceFee.MaximumValue = "9999999999"
                '    rgv_PDCBounceFee.ErrorMessage = "Harap isi Biaya Penolakan PDC antara " & rgv_PDCBounceFee.MinimumValue & " dan 9999999999 "
                'ElseIf oProduct.PDCBounceFeeBehaviour_HO = "X" Then
                '    E_PDCBounceFee.Enabled = True
                '    E_cboPDCBounceFeeBehaviour.Enabled = True
                '    E_cboPDCBounceFeeBehaviour.Items.Remove(E_cboPDCBounceFeeBehaviour.Items.FindByValue("N"))
                '    rgv_PDCBounceFee.MinimumValue = "0"
                '    rgv_PDCBounceFee.MaximumValue = Hide_PDCBounceFee.Value
                '    rgv_PDCBounceFee.ErrorMessage = "Harap isi Biaya Penolakan PDC antara 0 dan " & rgv_PDCBounceFee.MinimumValue
                'End If
                'lblPDCBounceFee_Branch.Text = CStr(oProduct.PDCBounceFee_HO) & " " & Behaviour_Value(oProduct.PDCBounceFeeBehaviour_HO)

                ''InsuranceAdmin Fee
                'E_txtInsuranceAdminFee.Text = CStr(oProduct.InsAdminFee)
                'Hide_InsuranceAdminFee.Value = CStr(oProduct.InsAdminFee_HO)
                'Hide_InstallmentToleranceAmountBeh.Value = oProduct.InsAdminFeeBehaviour_HO
                'E_cboInsuranceAdminFeeBehaviour.SelectedIndex = E_cboInsuranceAdminFeeBehaviour.Items.IndexOf(E_cboInsuranceAdminFeeBehaviour.Items.FindByValue(oProduct.InsAdminFeeBehaviour))
                'If oProduct.InsAdminFeeBehaviour_HO = "D" Then
                '    E_txtInsuranceAdminFee.Enabled = True
                '    E_cboInsuranceAdminFeeBehaviour.Enabled = True
                '    rgv_InsuranceAdminFee.MinimumValue = "0"
                '    rgv_InsuranceAdminFee.MaximumValue = "9999999999"
                'ElseIf oProduct.InsAdminFeeBehaviour_HO = "L" Then
                '    E_txtInsuranceAdminFee.Enabled = False
                '    E_cboInsuranceAdminFeeBehaviour.Enabled = False
                'ElseIf oProduct.InsAdminFeeBehaviour_HO = "N" Then
                '    E_txtInsuranceAdminFee.Enabled = True
                '    E_cboInsuranceAdminFeeBehaviour.Enabled = True
                '    E_cboInsuranceAdminFeeBehaviour.Items.Remove(E_cboInsuranceAdminFeeBehaviour.Items.FindByValue("X"))
                '    rgv_InsuranceAdminFee.MinimumValue = Hide_InsuranceAdminFee.Value
                '    rgv_InsuranceAdminFee.MaximumValue = "9999999999"
                '    rgv_InsuranceAdminFee.ErrorMessage = "Harap isi Biaya Admin Asuransi antara " & rgv_InsuranceAdminFee.MinimumValue & " dan 9999999999 "
                'ElseIf oProduct.InsAdminFeeBehaviour_HO = "X" Then
                '    E_txtInsuranceAdminFee.Enabled = True
                '    E_cboInsuranceAdminFeeBehaviour.Enabled = True
                '    E_cboInsuranceAdminFeeBehaviour.Items.Remove(E_cboInsuranceAdminFeeBehaviour.Items.FindByValue("N"))
                '    rgv_InsuranceAdminFee.MinimumValue = "0"
                '    rgv_InsuranceAdminFee.MaximumValue = Hide_InsuranceAdminFee.Value
                '    rgv_InsuranceAdminFee.ErrorMessage = "Harap isi Biaya Admin Asuransi antara 0 dan " & rgv_InsuranceAdminFee.MinimumValue
                'End If
                'lblInsuranceAdminFee_Branch.Text = CStr(oProduct.InsAdminFee_HO) & " " & Behaviour_Value(oProduct.InsAdminFeeBehaviour_HO)

                ''InsuranceStampDutyFee 
                'E_txtInsuranceStampDutyFee.Text = CStr(oProduct.InsStampDutyFee)
                'Hide_InsuranceStampDutyFee.Value = CStr(oProduct.InsStampDutyFee_HO)
                'Hide_InsuranceStampDutyFeeBeh.Value = oProduct.InsStampDutyFeeBehaviour_HO
                'E_cboInsuranceStampDutyFeeBehaviour.SelectedIndex = E_cboInsuranceStampDutyFeeBehaviour.Items.IndexOf(E_cboInsuranceStampDutyFeeBehaviour.Items.FindByValue(oProduct.InsStampDutyFeeBehaviour))
                'If oProduct.InsStampDutyFeeBehaviour_HO = "D" Then
                '    E_txtInsuranceStampDutyFee.Enabled = True
                '    E_cboInsuranceStampDutyFeeBehaviour.Enabled = True
                '    rgv_InsuranceStampDutyFee.MinimumValue = "0"
                '    rgv_InsuranceStampDutyFee.MaximumValue = "9999999999"
                'ElseIf oProduct.InsStampDutyFeeBehaviour_HO = "L" Then
                '    E_txtInsuranceStampDutyFee.Enabled = False
                '    E_cboInsuranceStampDutyFeeBehaviour.Enabled = False
                'ElseIf oProduct.InsStampDutyFeeBehaviour_HO = "N" Then
                '    E_txtInsuranceStampDutyFee.Enabled = True
                '    E_cboInsuranceStampDutyFeeBehaviour.Enabled = True
                '    E_cboInsuranceStampDutyFeeBehaviour.Items.Remove(E_cboInsuranceStampDutyFeeBehaviour.Items.FindByValue("X"))
                '    rgv_InsuranceStampDutyFee.MinimumValue = Hide_InsuranceStampDutyFee.Value
                '    rgv_InsuranceStampDutyFee.MaximumValue = "9999999999"
                '    rgv_InsuranceStampDutyFee.ErrorMessage = "Harap isi Biaya Materai Asuransi antara " & rgv_InsuranceStampDutyFee.MinimumValue & " dan 9999999999 "
                'ElseIf oProduct.InsStampDutyFeeBehaviour_HO = "X" Then
                '    E_txtInsuranceStampDutyFee.Enabled = True
                '    E_cboInsuranceStampDutyFeeBehaviour.Enabled = True
                '    E_cboInsuranceStampDutyFeeBehaviour.Items.Remove(E_cboInsuranceStampDutyFeeBehaviour.Items.FindByValue("N"))
                '    rgv_InsuranceStampDutyFee.MinimumValue = "0"
                '    rgv_InsuranceStampDutyFee.MaximumValue = Hide_InsuranceStampDutyFee.Value
                '    rgv_InsuranceStampDutyFee.ErrorMessage = "Harap isi Biaya Materai Asuransi antara 0 dan " & rgv_InsuranceStampDutyFee.MinimumValue
                'End If
                'lblInsuranceStampDutyFee_Branch.Text = CStr(oProduct.InsStampDutyFee_HO) & " " & Behaviour_Value(oProduct.InsStampDutyFeeBehaviour_HO)

            'BiayaPolis 
                'E_txtBiayaPolis.Text = CStr(oProduct.BiayaPolis)
                'Hide_BiayaPolis.Value = CStr(oProduct.BiayaPolis_HO)
                'Hide_BiayaPolisBeh.Value = oProduct.BiayaPolisBehaviour_HO
                'E_cboBiayaPolisBehaviour.SelectedIndex = E_cboBiayaPolisBehaviour.Items.IndexOf(E_cboBiayaPolisBehaviour.Items.FindByValue(oProduct.BiayaPolisBehaviour))
                'If oProduct.BiayaPolisBehaviour_HO = "D" Then
                '    E_txtBiayaPolis.Enabled = True
                '    E_cboBiayaPolisBehaviour.Enabled = True
                '    rgv_BiayaPolis.MinimumValue = "0"
                '    rgv_BiayaPolis.MaximumValue = "9999999999"
                'ElseIf oProduct.BiayaPolisBehaviour_HO = "L" Then
                '    E_txtBiayaPolis.Enabled = False
                '    E_cboBiayaPolisBehaviour.Enabled = False
                'ElseIf oProduct.BiayaPolisBehaviour_HO = "N" Then
                '    E_txtBiayaPolis.Enabled = True
                '    E_cboBiayaPolisBehaviour.Enabled = True
                '    E_cboBiayaPolisBehaviour.Items.Remove(E_cboBiayaPolisBehaviour.Items.FindByValue("X"))
                '    rgv_BiayaPolis.MinimumValue = Hide_BiayaPolis.Value
                '    rgv_BiayaPolis.MaximumValue = "9999999999"
                '    rgv_BiayaPolis.ErrorMessage = "Harap isi Biaya Polis antara " & rgv_BiayaPolis.MinimumValue & " dan 9999999999 "
                'ElseIf oProduct.BiayaPolisBehaviour_HO = "X" Then
                '    E_txtBiayaPolis.Enabled = True
                '    E_cboBiayaPolisBehaviour.Enabled = True
                '    E_cboBiayaPolisBehaviour.Items.Remove(E_cboBiayaPolisBehaviour.Items.FindByValue("N"))
                '    rgv_BiayaPolis.MinimumValue = "0"
                '    rgv_BiayaPolis.MaximumValue = Hide_BiayaPolis.Value
                '    rgv_BiayaPolis.ErrorMessage = "Harap isi Biaya Polis antara 0 dan " & rgv_BiayaPolis.MinimumValue
                'End If
                'lblBiayaPolis_Branch.Text = CStr(oProduct.BiayaPolis_HO) & " " & Behaviour_Value(oProduct.BiayaPolisBehaviour_HO)

                ''POExpiration Days
                'E_txtPOExpirationDays.Text = CStr(oProduct.DaysPOExpiration)
                'Hide_POExpirationDays.Value = CStr(oProduct.DaysPOExpiration_HO)
                'Hide_POExpirationDaysBeh.Value = oProduct.DaysPOExpirationBehaviour_HO
                'E_cboPOExpirationDaysBehaviour.SelectedIndex = E_cboPOExpirationDaysBehaviour.Items.IndexOf(E_cboPOExpirationDaysBehaviour.Items.FindByValue(oProduct.DaysPOExpirationBehaviour))
                'If oProduct.DaysPOExpirationBehaviour_HO = "D" Then
                '    E_txtPOExpirationDays.Enabled = True
                '    E_cboPOExpirationDaysBehaviour.Enabled = True
                '    rgv_POExpirationDays.MinimumValue = "0"
                '    rgv_POExpirationDays.MaximumValue = "9999"
                'ElseIf oProduct.DaysPOExpirationBehaviour_HO = "L" Then
                '    E_txtPOExpirationDays.Enabled = False
                '    E_cboPOExpirationDaysBehaviour.Enabled = False
                'ElseIf oProduct.DaysPOExpirationBehaviour_HO = "N" Then
                '    E_txtPOExpirationDays.Enabled = True
                '    E_cboPOExpirationDaysBehaviour.Enabled = True
                '    E_cboPOExpirationDaysBehaviour.Items.Remove(E_cboPOExpirationDaysBehaviour.Items.FindByValue("X"))
                '    rgv_POExpirationDays.MinimumValue = Hide_POExpirationDays.Value
                '    rgv_POExpirationDays.MaximumValue = "9999"
                '    rgv_POExpirationDays.ErrorMessage = "Harap isi Masa Berlaku PO antara " & rgv_POExpirationDays.MinimumValue & " dan 9999 "
                'ElseIf oProduct.DaysPOExpirationBehaviour_HO = "X" Then
                '    E_txtPOExpirationDays.Enabled = True
                '    E_cboPOExpirationDaysBehaviour.Enabled = True
                '    E_cboPOExpirationDaysBehaviour.Items.Remove(E_cboPOExpirationDaysBehaviour.Items.FindByValue("N"))
                '    rgv_POExpirationDays.MinimumValue = "0"
                '    rgv_POExpirationDays.MaximumValue = Hide_POExpirationDays.Value
                '    rgv_POExpirationDays.ErrorMessage = "Harap isi Masa Berlaku PO antara 0 dan " & rgv_POExpirationDays.MinimumValue
                'End If
                'lblPOExpirationDays_Branch.Text = CStr(oProduct.DaysPOExpiration_HO) & " days " & Behaviour_Value(oProduct.DaysPOExpirationBehaviour_HO)

                ''Installment Tolerance Amount
                'E_txtInstallmentToleranceAmount.Text = CStr(oProduct.InstallmentToleranceAmount)
                'Hide_InstallmentToleranceAmount.Value = CStr(oProduct.InstallmentToleranceAmount_HO)
                'Hide_InstallmentToleranceAmountBeh.Value = oProduct.InstallmentToleranceAmountBehaviour_HO
                'E_cboInstallmentToleranceAmountBehaviour.SelectedIndex = E_cboInstallmentToleranceAmountBehaviour.Items.IndexOf(E_cboInstallmentToleranceAmountBehaviour.Items.FindByValue(oProduct.InstallmentToleranceAmountBehaviour))
                'If oProduct.InstallmentToleranceAmountBehaviour_HO = "D" Then
                '    E_txtInstallmentToleranceAmount.Enabled = True
                '    E_cboInstallmentToleranceAmountBehaviour.Enabled = True
                '    rgv_InstallmentToleranceAmount.MinimumValue = "0"
                '    rgv_InstallmentToleranceAmount.MaximumValue = "999999999999"
                'ElseIf oProduct.InstallmentToleranceAmountBehaviour_HO = "L" Then
                '    E_txtInstallmentToleranceAmount.Enabled = False
                '    E_cboInstallmentToleranceAmountBehaviour.Enabled = False
                'ElseIf oProduct.InstallmentToleranceAmountBehaviour_HO = "N" Then
                '    E_txtInstallmentToleranceAmount.Enabled = True
                '    E_cboInstallmentToleranceAmountBehaviour.Enabled = True
                '    E_cboInstallmentToleranceAmountBehaviour.Items.Remove(E_cboInstallmentToleranceAmountBehaviour.Items.FindByValue("X"))
                '    rgv_InstallmentToleranceAmount.MinimumValue = Hide_InstallmentToleranceAmount.Value
                '    rgv_InstallmentToleranceAmount.MaximumValue = "999999999999"
                '    rgv_InstallmentToleranceAmount.ErrorMessage = "Harap isi Jumlah Toleransi terhadap Angsuran antara " & rgv_InstallmentToleranceAmount.MinimumValue & " dan 999999999999 "
                'ElseIf oProduct.InstallmentToleranceAmountBehaviour_HO = "X" Then
                '    E_txtInstallmentToleranceAmount.Enabled = True
                '    E_cboInstallmentToleranceAmountBehaviour.Enabled = True
                '    E_cboInstallmentToleranceAmountBehaviour.Items.Remove(E_cboInstallmentToleranceAmountBehaviour.Items.FindByValue("N"))
                '    rgv_InstallmentToleranceAmount.MinimumValue = "0"
                '    rgv_InstallmentToleranceAmount.MaximumValue = Hide_InstallmentToleranceAmount.Value
                '    rgv_InstallmentToleranceAmount.ErrorMessage = "Harap isi Jumlah Toleransi terhadap Angsuran antara 0 dan " & rgv_InstallmentToleranceAmount.MinimumValue
                'End If
                'lblInstallmentToleranceAmount_Branch.Text = CStr(oProduct.InstallmentToleranceAmount_HO) & " " & Behaviour_Value(oProduct.InstallmentToleranceAmountBehaviour_HO)

                ''DP Percentage Amount
                'E_txtDPPercentage.Text = CStr(oProduct.DPPercentage)
                'Hide_DPPercentage.Value = CStr(oProduct.DPPercentage_HO)
                'Hide_DPPercentageBeh.Value = oProduct.DPPercentageBehaviour_HO
                'E_cboDPPercentageBehaviour.SelectedIndex = E_cboDPPercentageBehaviour.Items.IndexOf(E_cboDPPercentageBehaviour.Items.FindByValue(oProduct.DPPercentageBehaviour))
                'If oProduct.DPPercentageBehaviour_HO = "D" Then
                '    E_txtDPPercentage.Enabled = True
                '    E_cboDPPercentageBehaviour.Enabled = True
                '    rgv_DPPercentage.MinimumValue = "0"
                '    rgv_DPPercentage.MaximumValue = "100"
                'ElseIf oProduct.DPPercentageBehaviour_HO = "L" Then
                '    E_txtDPPercentage.Enabled = False
                '    E_cboDPPercentageBehaviour.Enabled = False
                'ElseIf oProduct.DPPercentageBehaviour_HO = "N" Then
                '    E_txtDPPercentage.Enabled = True
                '    E_cboDPPercentageBehaviour.Enabled = True
                '    E_cboDPPercentageBehaviour.Items.Remove(E_cboDPPercentageBehaviour.Items.FindByValue("X"))
                '    rgv_DPPercentage.MinimumValue = Hide_DPPercentage.Value
                '    rgv_DPPercentage.MaximumValue = "100"
                '    rgv_DPPercentage.ErrorMessage = "Harap isi Prosentase Minimum DP antara " & rgv_DPPercentage.MinimumValue & " dan 100 "
                'ElseIf oProduct.DPPercentageBehaviour_HO = "X" Then
                '    E_txtDPPercentage.Enabled = True
                '    E_cboDPPercentageBehaviour.Enabled = True
                '    E_cboDPPercentageBehaviour.Items.Remove(E_cboDPPercentageBehaviour.Items.FindByValue("N"))
                '    rgv_DPPercentage.MinimumValue = "0"
                '    rgv_DPPercentage.MaximumValue = Hide_DPPercentage.Value
                '    rgv_DPPercentage.ErrorMessage = "Harap isi Prosentase Minimum DP antara 0 dan " & rgv_DPPercentage.MinimumValue
                'End If
                'lblDPPercentage_Branch.Text = CStr(oProduct.DPPercentage_HO) & "% " & Behaviour_Value(oProduct.DPPercentageBehaviour_HO)

            'Reject Minimum Income
                'E_txtRejectMinimumIncome.Text = CStr(oProduct.RejectMinimumIncome)
                'Hide_RejectMinimumIncome.Value = CStr(oProduct.RejectMinimumIncome_HO)
                'Hide_RejectMinimumIncomeBeh.Value = oProduct.RejectMinimumIncomeBehaviour_HO
                'E_cboRejectMinimumIncomeBehaviour.SelectedIndex = E_cboRejectMinimumIncomeBehaviour.Items.IndexOf(E_cboRejectMinimumIncomeBehaviour.Items.FindByValue(oProduct.RejectMinimumIncomeBehaviour))
                'If oProduct.RejectMinimumIncomeBehaviour_HO = "D" Then
                '    E_txtRejectMinimumIncome.Enabled = True
                '    E_cboRejectMinimumIncomeBehaviour.Enabled = True
                '    rgv_RejectMinimumIncome.MinimumValue = "0"
                '    rgv_RejectMinimumIncome.MaximumValue = "999999999999"
                'ElseIf oProduct.RejectMinimumIncomeBehaviour_HO = "L" Then
                '    E_txtRejectMinimumIncome.Enabled = False
                '    E_cboRejectMinimumIncomeBehaviour.Enabled = False
                'ElseIf oProduct.RejectMinimumIncomeBehaviour_HO = "N" Then
                '    E_txtRejectMinimumIncome.Enabled = True
                '    E_cboRejectMinimumIncomeBehaviour.Enabled = True
                '    E_cboRejectMinimumIncomeBehaviour.Items.Remove(E_cboRejectMinimumIncomeBehaviour.Items.FindByValue("X"))
                '    rgv_RejectMinimumIncome.MinimumValue = Hide_RejectMinimumIncome.Value
                '    rgv_RejectMinimumIncome.MaximumValue = "999999999999"
                '    rgv_RejectMinimumIncome.ErrorMessage = "Harap isi Pendapatan Bunga Minimum yg harus ditolak antara " & rgv_RejectMinimumIncome.MinimumValue & " dan 999999999999 "
                'ElseIf oProduct.RejectMinimumIncomeBehaviour_HO = "X" Then
                '    E_txtRejectMinimumIncome.Enabled = True
                '    E_cboRejectMinimumIncomeBehaviour.Enabled = True
                '    E_cboRejectMinimumIncomeBehaviour.Items.Remove(E_cboRejectMinimumIncomeBehaviour.Items.FindByValue("N"))
                '    rgv_RejectMinimumIncome.MinimumValue = "0"
                '    rgv_RejectMinimumIncome.MaximumValue = Hide_RejectMinimumIncome.Value
                '    rgv_RejectMinimumIncome.ErrorMessage = "Harap isi Pendapatan Bunga Minimum yg harus ditolak antara 0 dan " & rgv_RejectMinimumIncome.MinimumValue
                'End If
                'lblRejectMinimumIncome_Branch.Text = CStr(oProduct.RejectMinimumIncome_HO) & " " & Behaviour_Value(oProduct.RejectMinimumIncomeBehaviour_HO)

                ''Warning Minimum Income
                'E_txtWarningMinimumIncome.Text = CStr(oProduct.WarningMinimumIncome)
                'Hide_WarningMinimumIncome.Value = CStr(oProduct.WarningMinimumIncome_HO)
                'Hide_WarningMinimumIncomeBeh.Value = oProduct.WarningMinimumIncomeBehaviour_HO
                'E_cboWarningMinimumIncomeBehaviour.SelectedIndex = E_cboWarningMinimumIncomeBehaviour.Items.IndexOf(E_cboWarningMinimumIncomeBehaviour.Items.FindByValue(oProduct.WarningMinimumIncomeBehaviour))
                'If oProduct.WarningMinimumIncomeBehaviour_HO = "D" Then
                '    E_txtWarningMinimumIncome.Enabled = True
                '    E_cboWarningMinimumIncomeBehaviour.Enabled = True
                '    rgv_WarningMinimumIncome.MinimumValue = "0"
                '    rgv_WarningMinimumIncome.MaximumValue = "999999999999"
                'ElseIf oProduct.WarningMinimumIncomeBehaviour_HO = "L" Then
                '    E_txtWarningMinimumIncome.Enabled = False
                '    E_cboWarningMinimumIncomeBehaviour.Enabled = False
                'ElseIf oProduct.WarningMinimumIncomeBehaviour_HO = "N" Then
                '    E_txtWarningMinimumIncome.Enabled = True
                '    E_cboWarningMinimumIncomeBehaviour.Enabled = True
                '    E_cboWarningMinimumIncomeBehaviour.Items.Remove(E_cboWarningMinimumIncomeBehaviour.Items.FindByValue("X"))
                '    rgv_WarningMinimumIncome.MinimumValue = Hide_WarningMinimumIncome.Value
                '    rgv_WarningMinimumIncome.MaximumValue = "999999999999"
                '    rgv_WarningMinimumIncome.ErrorMessage = "Harap isi Pendapatan Bunga Minimum yg harus diingatkan antara " & rgv_WarningMinimumIncome.MinimumValue & " dan 999999999999 "
                'ElseIf oProduct.WarningMinimumIncomeBehaviour_HO = "X" Then
                '    E_txtWarningMinimumIncome.Enabled = True
                '    E_cboWarningMinimumIncomeBehaviour.Enabled = True
                '    E_cboWarningMinimumIncomeBehaviour.Items.Remove(E_cboWarningMinimumIncomeBehaviour.Items.FindByValue("N"))
                '    rgv_WarningMinimumIncome.MinimumValue = "0"
                '    rgv_WarningMinimumIncome.MaximumValue = Hide_WarningMinimumIncome.Value
                '    rgv_WarningMinimumIncome.ErrorMessage = "Harap isi Pendapatan Bunga Minimum yg harus diingatkan antara 0 dan " & rgv_WarningMinimumIncome.MinimumValue
                'End If
                'lblWarningMinimumIncome_Branch.Text = CStr(oProduct.WarningMinimumIncome_HO) & " " & Behaviour_Value(oProduct.WarningMinimumIncomeBehaviour_HO)





            'Dim strIsSPAutomatic As String
            'If oProduct.IsSPAutomatic_HO = True Then
            '    strIsSPAutomatic = "Yes"
            'Else
            '    strIsSPAutomatic = "No"
            'End If

            'Dim strIsSP1Automatic As String
            'If oProduct.IsSP1Automatic_HO = True Then
            '    strIsSP1Automatic = "Yes"
            'Else
            '    strIsSP1Automatic = "No"
            'End If

            'Dim strIsSP2Automatic As String
            'If oProduct.IsSP2Automatic_HO = True Then
            '    strIsSP2Automatic = "Yes"
            'Else
            '    strIsSP2Automatic = "No"
            'End If




            ' SPAutomatic
                'E_cboIsSPAutomatic.SelectedIndex = E_cboIsSPAutomatic.Items.IndexOf(E_cboIsSPAutomatic.Items.FindByValue(IIf(oProduct.IsSPAutomatic, "Yes", "No").ToString))
                'E_txtLengthSPProcess.Text = CStr(oProduct.LengthSPProcess)
                ''Hide_LengthSPProcess.Value = CStr(oProduct.LengthSPProcess_HO)

                'If oProduct.IsSPAutomaticBehaviour_HO = "D" Then
                '    E_cboIsSPAutomatic.Enabled = True
                '    E_txtLengthSPProcess.Enabled = True
                'ElseIf oProduct.IsSPAutomaticBehaviour_HO = "L" Then
                '    E_cboIsSPAutomatic.Enabled = False
                '    E_txtLengthSPProcess.Enabled = False
                'End If

                'lblLengthSPProcess_Branch.Text = IIf(oProduct.IsSPAutomatic_HO, "Yes", "No").ToString & " Length SP Process " & CStr(oProduct.LengthSPProcess_HO) & " days"





            ' SP1Automatic
                'E_cboIsSP1Automatic.SelectedIndex = E_cboIsSP1Automatic.Items.IndexOf(E_cboIsSP1Automatic.Items.FindByValue(IIf(oProduct.IsSP1Automatic, "Yes", "No").ToString))
                'E_txtLengthSP1Process.Text = CStr(oProduct.LengthSP1Process)
                ''Hide_LengthSP1Process.Value = CStr(oProduct.LengthSP1Process_HO)

                'If oProduct.IsSP1AutomaticBehaviour_HO = "D" Then
                '    E_cboIsSP1Automatic.Enabled = True
                '    E_txtLengthSP1Process.Enabled = True
                'ElseIf oProduct.IsSP1AutomaticBehaviour_HO = "L" Then
                '    E_cboIsSP1Automatic.Enabled = False
                '    E_txtLengthSP1Process.Enabled = False
                'End If
                'lblLengthSP1Process_Branch.Text = IIf(oProduct.IsSP2Automatic_HO, "Yes", "No").ToString & " Length SP 1 Process " & CStr(oProduct.LengthSP1Process_HO) & " days"






            ' SP2Automatic
                'E_cboIsSP2Automatic.SelectedIndex = E_cboIsSP2Automatic.Items.IndexOf(E_cboIsSP2Automatic.Items.FindByValue(IIf(oProduct.IsSP2Automatic, "Yes", "No").ToString))
                'E_txtLengthSP2Process.Text = CStr(oProduct.LengthSP2Process)
                ''Hide_LengthSP2Process.Value = CStr(oProduct.LengthSP2Process_HO)

                'If oProduct.IsSP2AutomaticBehaviour_HO = "D" Then
                '    E_cboIsSP2Automatic.Enabled = True
                '    E_txtLengthSP2Process.Enabled = True
                'ElseIf oProduct.IsSP2AutomaticBehaviour_HO = "L" Then
                '    E_cboIsSP2Automatic.Enabled = False
                '    E_txtLengthSP2Process.Enabled = False
                'End If
                'lblLengthSP2Process_Branch.Text = IIf(oProduct.IsSP2Automatic_HO, "Yes", "No").ToString & " Length SP 2 Process " & CStr(oProduct.LengthSP2Process_HO) & " days"





                'Me.SPBehaviour = oProduct.IsSPAutomaticBehaviour_HO
                'Me.SP1Behaviour = oProduct.IsSP1AutomaticBehaviour_HO
                'Me.SP2Behaviour = oProduct.IsSP2AutomaticBehaviour_HO

                'If oProduct.IsSPAutomatic_HO = True Then
                '    Me.SPAutomatic = "1"
                'Else
                '    Me.SPAutomatic = "0"
                'End If

                'If oProduct.IsSP1Automatic_HO = True Then
                '    Me.SP1Automatic = "1"
                'Else
                '    Me.SP1Automatic = "0"
                'End If

                'If oProduct.IsSP2Automatic_HO = True Then
                '    Me.SP2Automatic = "1"
                'Else
                '    Me.SP2Automatic = "0"
                'End If




                'Me.LengthSP = oProduct.LengthSPProcess_HO
                'Me.LengthSP1 = oProduct.LengthSP1Process_HO
                'Me.LengthSP2 = oProduct.LengthSP2Process_HO

            'Length Main Document Processed
                'E_txtLengthMainDocProcess.Text = CStr(oProduct.LengthMainDocProcess)
                'Hide_LengthMainDocProcess.Value = CStr(oProduct.LengthMainDocProcess_HO)
                'Hide_LengthMainDocProcessBeh.Value = oProduct.LengthMainDocProcessBehaviour_HO
                'E_cboLengthMainDocProcessBehaviour.SelectedIndex = E_cboLengthMainDocProcessBehaviour.Items.IndexOf(E_cboLengthMainDocProcessBehaviour.Items.FindByValue(oProduct.LengthMainDocProcessBehaviour))
                'If oProduct.LengthMainDocProcessBehaviour_HO = "D" Then
                '    E_txtLengthMainDocProcess.Enabled = True
                '    E_cboLengthMainDocProcessBehaviour.Enabled = True
                '    rgv_LengthMainDocProcess.MinimumValue = "0"
                '    rgv_LengthMainDocProcess.MaximumValue = "9999"
                'ElseIf oProduct.LengthMainDocProcessBehaviour_HO = "L" Then
                '    E_txtLengthMainDocProcess.Enabled = False
                '    E_cboLengthMainDocProcessBehaviour.Enabled = False
                'ElseIf oProduct.LengthMainDocProcessBehaviour_HO = "N" Then
                '    E_txtLengthMainDocProcess.Enabled = True
                '    E_cboLengthMainDocProcessBehaviour.Enabled = True
                '    E_cboLengthMainDocProcessBehaviour.Items.Remove(E_cboLengthMainDocProcessBehaviour.Items.FindByValue("X"))
                '    rgv_LengthMainDocProcess.MinimumValue = Hide_LengthMainDocProcess.Value
                '    rgv_LengthMainDocProcess.MaximumValue = "9999"
                '    rgv_LengthMainDocProcess.ErrorMessage = "Harap isi Jangka Waktu proses Dokumen Utama  antara " & rgv_LengthMainDocProcess.MinimumValue & " dan 9999 "
                'ElseIf oProduct.LengthMainDocProcessBehaviour_HO = "X" Then
                '    E_txtLengthMainDocProcess.Enabled = True
                '    E_cboLengthMainDocProcessBehaviour.Enabled = True
                '    E_cboLengthMainDocProcessBehaviour.Items.Remove(E_cboLengthMainDocProcessBehaviour.Items.FindByValue("N"))
                '    rgv_LengthMainDocProcess.MinimumValue = "0"
                '    rgv_LengthMainDocProcess.MaximumValue = Hide_LengthMainDocProcess.Value
                '    rgv_LengthMainDocProcess.ErrorMessage = "Harap isi Jangka Waktu proses Dokumen Utama  antara 0 dan " & rgv_LengthMainDocProcess.MaximumValue
                'End If
                'lblLengthMainDocProcess_Branch.Text = CStr(oProduct.LengthMainDocProcess_HO) & " days " & Behaviour_Value(oProduct.LengthMainDocProcessBehaviour_HO)

            'Length Main Document Taken
                'E_txtLengthMainDocTaken.Text = CStr(oProduct.LengthMainDocTaken)
                'Hide_LengthMainDocTaken.Value = CStr(oProduct.LengthMainDocTaken_HO)
                'Hide_LengthMainDocProcessBeh.Value = oProduct.LengthMainDocTakenBehaviour_HO
                'E_cboLengthMainDocTakenBehaviour.SelectedIndex = E_cboLengthMainDocTakenBehaviour.Items.IndexOf(E_cboLengthMainDocTakenBehaviour.Items.FindByValue(oProduct.LengthMainDocTakenBehaviour))
                'If oProduct.LengthMainDocTakenBehaviour_HO = "D" Then
                '    E_txtLengthMainDocTaken.Enabled = True
                '    E_cboLengthMainDocTakenBehaviour.Enabled = True
                '    rgv_LengthMainDocTaken.MinimumValue = "0"
                '    rgv_LengthMainDocTaken.MaximumValue = "9999"
                'ElseIf oProduct.LengthMainDocTakenBehaviour_HO = "L" Then
                '    E_txtLengthMainDocTaken.Enabled = False
                '    E_cboLengthMainDocTakenBehaviour.Enabled = False
                'ElseIf oProduct.LengthMainDocTakenBehaviour_HO = "N" Then
                '    E_txtLengthMainDocTaken.Enabled = True
                '    E_cboLengthMainDocTakenBehaviour.Enabled = True
                '    E_cboLengthMainDocTakenBehaviour.Items.Remove(E_cboLengthMainDocTakenBehaviour.Items.FindByValue("X"))
                '    rgv_LengthMainDocTaken.MinimumValue = Hide_LengthMainDocTaken.Value
                '    rgv_LengthMainDocTaken.MaximumValue = "9999"
                '    rgv_LengthMainDocTaken.ErrorMessage = "Harap isi Jangka Waktu Dokumen Utama diambil antara " & rgv_LengthMainDocTaken.MinimumValue & " dan 9999 "
                'ElseIf oProduct.LengthMainDocTakenBehaviour_HO = "X" Then
                '    E_txtLengthMainDocTaken.Enabled = True
                '    E_cboLengthMainDocTakenBehaviour.Enabled = True
                '    E_cboLengthMainDocTakenBehaviour.Items.Remove(E_cboLengthMainDocTakenBehaviour.Items.FindByValue("N"))
                '    rgv_LengthMainDocTaken.MinimumValue = "0"
                '    rgv_LengthMainDocTaken.MaximumValue = Hide_LengthMainDocTaken.Value
                '    rgv_LengthMainDocTaken.ErrorMessage = "Harap isi Jangka Waktu Dokumen Utama diambil antara 0 dan " & rgv_LengthMainDocTaken.MaximumValue
                'End If
                'lblLengthMainDocTaken_Branch.Text = CStr(oProduct.LengthMainDocTaken_HO) & " days " & Behaviour_Value(oProduct.LengthMainDocTakenBehaviour_HO)


            'Grace Period Late Charges
                'E_txtGracePeriodLateCharges.Text = CStr(oProduct.GracePeriodLateCharges)
                'Hide_GracePeriodLateCharges.Value = CStr(oProduct.GracePeriodLateCharges_HO)
                'Hide_GracePeriodLateChargesBeh.Value = oProduct.GracePeriodLateChargesBehaviour_HO
                'E_cboGracePeriodLateChargesBehaviour.SelectedIndex = E_cboGracePeriodLateChargesBehaviour.Items.IndexOf(E_cboGracePeriodLateChargesBehaviour.Items.FindByValue(oProduct.GracePeriodLateChargesBehaviour))
                'If oProduct.GracePeriodLateChargesBehaviour_HO = "D" Then
                '    E_txtGracePeriodLateCharges.Enabled = True
                '    E_cboGracePeriodLateChargesBehaviour.Enabled = True
                '    rgv_GracePeriodLateCharges.MinimumValue = "0"
                '    rgv_GracePeriodLateCharges.MaximumValue = "9999"
                'ElseIf oProduct.GracePeriodLateChargesBehaviour_HO = "L" Then
                '    E_txtGracePeriodLateCharges.Enabled = False
                '    E_cboGracePeriodLateChargesBehaviour.Enabled = False
                'ElseIf oProduct.GracePeriodLateChargesBehaviour_HO = "N" Then
                '    E_txtGracePeriodLateCharges.Enabled = True
                '    E_cboGracePeriodLateChargesBehaviour.Enabled = True
                '    E_cboGracePeriodLateChargesBehaviour.Items.Remove(E_cboGracePeriodLateChargesBehaviour.Items.FindByValue("X"))
                '    rgv_GracePeriodLateCharges.MinimumValue = Hide_GracePeriodLateCharges.Value
                '    rgv_GracePeriodLateCharges.MaximumValue = "9999"
                '    rgv_GracePeriodLateCharges.ErrorMessage = "Harap isi Grace Period Denda Keterlambatan antara " & rgv_GracePeriodLateCharges.MinimumValue & " dan 9999 "
                'ElseIf oProduct.GracePeriodLateChargesBehaviour_HO = "X" Then
                '    E_txtGracePeriodLateCharges.Enabled = True
                '    E_cboGracePeriodLateChargesBehaviour.Enabled = True
                '    E_cboGracePeriodLateChargesBehaviour.Items.Remove(E_cboGracePeriodLateChargesBehaviour.Items.FindByValue("N"))
                '    rgv_GracePeriodLateCharges.MinimumValue = "0"
                '    rgv_GracePeriodLateCharges.MaximumValue = Hide_GracePeriodLateCharges.Value
                '    rgv_GracePeriodLateCharges.ErrorMessage = "Harap isi Grace Period Denda Keterlambatan antara 0 dan " & rgv_GracePeriodLateCharges.MaximumValue
                'End If
                'lblGracePeriodLateCharges_Branch.Text = CStr(oProduct.GracePeriodLateCharges_HO) & " days " & Behaviour_Value(oProduct.GracePeriodLateChargesBehaviour_HO)

                ''Prepayment Penalty Rate
                'E_txtPrepaymentPenaltyRate.Text = CStr(FormatNumber(oProduct.PrepaymentPenaltyRate, 6))
                'Hide_PrepaymentPenaltyRate.Value = CStr(FormatNumber(oProduct.PrepaymentPenaltyRate_HO, 6))
                'Hide_PrepaymentPenaltyRateBeh.Value = oProduct.PrepaymentPenaltyRateBehaviour_HO
                'E_cboPrepaymentPenaltyRateBehaviour.SelectedIndex = E_cboPrepaymentPenaltyRateBehaviour.Items.IndexOf(E_cboPrepaymentPenaltyRateBehaviour.Items.FindByValue(oProduct.PrepaymentPenaltyRateBehaviour))
                'If oProduct.PrepaymentPenaltyRateBehaviour_HO = "D" Then
                '    E_txtPrepaymentPenaltyRate.Enabled = True
                '    E_cboPrepaymentPenaltyRateBehaviour.Enabled = True
                '    rgv_PrepaymentPenaltyRate.MinimumValue = "0"
                '    rgv_PrepaymentPenaltyRate.MaximumValue = "100"
                'ElseIf oProduct.PrepaymentPenaltyRateBehaviour_HO = "L" Then
                '    E_txtPrepaymentPenaltyRate.Enabled = False
                '    E_cboPrepaymentPenaltyRateBehaviour.Enabled = False
                'ElseIf oProduct.PrepaymentPenaltyRateBehaviour_HO = "N" Then
                '    E_txtPrepaymentPenaltyRate.Enabled = True
                '    E_cboPrepaymentPenaltyRateBehaviour.Enabled = True
                '    E_cboPrepaymentPenaltyRateBehaviour.Items.Remove(E_cboPrepaymentPenaltyRateBehaviour.Items.FindByValue("X"))
                '    rgv_PrepaymentPenaltyRate.MinimumValue = Hide_PrepaymentPenaltyRate.Value
                '    rgv_PrepaymentPenaltyRate.MaximumValue = "100"
                '    rgv_PrepaymentPenaltyRate.ErrorMessage = "Harap isi Denda Pelunasan dipercepat antara " & rgv_PrepaymentPenaltyRate.MinimumValue & " dan 100 "
                'ElseIf oProduct.PrepaymentPenaltyRateBehaviour_HO = "X" Then
                '    E_txtPrepaymentPenaltyRate.Enabled = True
                '    E_cboPrepaymentPenaltyRateBehaviour.Enabled = True
                '    E_cboPrepaymentPenaltyRateBehaviour.Items.Remove(E_cboPrepaymentPenaltyRateBehaviour.Items.FindByValue("N"))
                '    rgv_PrepaymentPenaltyRate.MinimumValue = "0"
                '    rgv_PrepaymentPenaltyRate.MaximumValue = Hide_PrepaymentPenaltyRate.Value
                '    rgv_PrepaymentPenaltyRate.ErrorMessage = "Harap isi Denda Pelunasan dipercepat antara 0 dan " & rgv_PrepaymentPenaltyRate.MaximumValue
                'End If
                'lblPrepaymentPenaltyRate_Branch.Text = CStr(FormatNumber(oProduct.PrepaymentPenaltyRate_HO, 6)) & " % " & Behaviour_Value(oProduct.PrepaymentPenaltyRateBehaviour_HO)

                ''Days to remind Installment
                'E_txtDeskCollPhoneRemind.Text = CStr(oProduct.DeskCollPhoneRemind)
                'Hide_DeskCollPhoneRemind.Value = CStr(oProduct.DeskCollPhoneRemind_HO)
                'Hide_DeskCollPhoneRemindBeh.Value = oProduct.DeskCollPhoneRemindBehaviour_HO
                'E_cboDeskCollPhoneRemindBehaviour.SelectedIndex = E_cboDeskCollPhoneRemindBehaviour.Items.IndexOf(E_cboDeskCollPhoneRemindBehaviour.Items.FindByValue(oProduct.DeskCollPhoneRemindBehaviour))
                'If oProduct.DeskCollPhoneRemindBehaviour_HO = "D" Then
                '    E_txtDeskCollPhoneRemind.Enabled = True
                '    E_cboDeskCollPhoneRemindBehaviour.Enabled = True
                '    rgv_DeskCollPhoneRemind.MinimumValue = "0"
                '    rgv_DeskCollPhoneRemind.MaximumValue = "9999"
                'ElseIf oProduct.DeskCollPhoneRemindBehaviour_HO = "L" Then
                '    E_txtDeskCollPhoneRemind.Enabled = False
                '    E_cboDeskCollPhoneRemindBehaviour.Enabled = False
                'ElseIf oProduct.DeskCollPhoneRemindBehaviour_HO = "N" Then
                '    E_txtDeskCollPhoneRemind.Enabled = True
                '    E_cboDeskCollPhoneRemindBehaviour.Enabled = True
                '    E_cboDeskCollPhoneRemindBehaviour.Items.Remove(E_cboDeskCollPhoneRemindBehaviour.Items.FindByValue("X"))
                '    rgv_DeskCollPhoneRemind.MinimumValue = Hide_DeskCollPhoneRemind.Value
                '    rgv_DeskCollPhoneRemind.MaximumValue = "9999"
                '    rgv_DeskCollPhoneRemind.ErrorMessage = "Harap isi Jumlah hari mengingatkan angsuran antara " & rgv_DeskCollPhoneRemind.MinimumValue & " dan 9999 "
                'ElseIf oProduct.DeskCollPhoneRemindBehaviour_HO = "X" Then
                '    E_txtDeskCollPhoneRemind.Enabled = True
                '    E_cboDeskCollPhoneRemindBehaviour.Enabled = True
                '    E_cboDeskCollPhoneRemindBehaviour.Items.Remove(E_cboDeskCollPhoneRemindBehaviour.Items.FindByValue("N"))
                '    rgv_DeskCollPhoneRemind.MinimumValue = "0"
                '    rgv_DeskCollPhoneRemind.MaximumValue = Hide_DeskCollPhoneRemind.Value
                '    rgv_DeskCollPhoneRemind.ErrorMessage = "Harap isi Jumlah hari mengingatkan angsuran antara 0 dan " & rgv_DeskCollPhoneRemind.MaximumValue
                'End If
                'lblDeskCollPhoneRemind_Branch.Text = CStr(oProduct.DeskCollPhoneRemind_HO) & " days " & Behaviour_Value(oProduct.DeskCollPhoneRemindBehaviour_HO)


            'Days overdue to call
                'E_txtDeskCollOD.Text = CStr(oProduct.DeskCollOD)
                'Hide_DeskCollOD.Value = CStr(oProduct.DeskCollOD_HO)
                'Hide_DeskCollODBeh.Value = oProduct.DeskCollODBehaviour_HO
                'E_cboDeskCollODBehaviour.SelectedIndex = E_cboDeskCollODBehaviour.Items.IndexOf(E_cboDeskCollODBehaviour.Items.FindByValue(oProduct.DeskCollODBehaviour))
                'If oProduct.DeskCollODBehaviour_HO = "D" Then
                '    E_txtDeskCollOD.Enabled = True
                '    E_cboDeskCollODBehaviour.Enabled = True
                '    rgv_DeskCollOD.MinimumValue = "0"
                '    rgv_DeskCollOD.MaximumValue = "9999"
                'ElseIf oProduct.DeskCollODBehaviour_HO = "L" Then
                '    E_txtDeskCollOD.Enabled = False
                '    E_cboDeskCollODBehaviour.Enabled = False
                'ElseIf oProduct.DeskCollODBehaviour_HO = "N" Then
                '    E_txtDeskCollOD.Enabled = True
                '    E_cboDeskCollODBehaviour.Enabled = True
                '    E_cboDeskCollODBehaviour.Items.Remove(E_cboDeskCollODBehaviour.Items.FindByValue("X"))
                '    rgv_DeskCollOD.MinimumValue = Hide_DeskCollOD.Value
                '    rgv_DeskCollOD.MaximumValue = "9999"
                '    rgv_DeskCollOD.ErrorMessage = "Harap isi Jumlah hari Overdue untuk Deskcoll antara " & rgv_DeskCollOD.MinimumValue & " dan 9999 "
                'ElseIf oProduct.DeskCollODBehaviour_HO = "X" Then
                '    E_txtDeskCollOD.Enabled = True
                '    E_cboDeskCollODBehaviour.Enabled = True
                '    E_cboDeskCollODBehaviour.Items.Remove(E_cboDeskCollODBehaviour.Items.FindByValue("N"))
                '    rgv_DeskCollOD.MinimumValue = "0"
                '    rgv_DeskCollOD.MaximumValue = Hide_DeskCollOD.Value
                '    rgv_DeskCollOD.ErrorMessage = "Harap isi Jumlah hari Overdue untuk Deskcoll antara 0 dan " & rgv_DeskCollOD.MaximumValue
                'End If
                'lblDeskCollOD_Branch.Text = CStr(oProduct.DeskCollOD_HO) & " days " & Behaviour_Value(oProduct.DeskCollODBehaviour_HO)


                ''Previous overdue to remind
                'E_txtPrevODToRemind.Text = CStr(oProduct.PrevODToRemind)
                'Hide_PrevODToRemind.Value = CStr(oProduct.PrevODToRemind_HO)
                'Hide_PrevODToRemindBeh.Value = oProduct.PrevODToRemindBehaviour_HO
                'E_cboPrevODToRemindBehaviour.SelectedIndex = E_cboPrevODToRemindBehaviour.Items.IndexOf(E_cboPrevODToRemindBehaviour.Items.FindByValue(oProduct.PrevODToRemindBehaviour))
                'If oProduct.PrevODToRemindBehaviour_HO = "D" Then
                '    E_txtPrevODToRemind.Enabled = True
                '    E_cboPrevODToRemindBehaviour.Enabled = True
                '    rgv_PrevODToRemind.MinimumValue = "0"
                '    rgv_PrevODToRemind.MaximumValue = "9999"
                'ElseIf oProduct.PrevODToRemindBehaviour_HO = "L" Then
                '    E_txtPrevODToRemind.Enabled = False
                '    E_cboPrevODToRemindBehaviour.Enabled = False
                'ElseIf oProduct.PrevODToRemindBehaviour_HO = "N" Then
                '    E_txtPrevODToRemind.Enabled = True
                '    E_cboPrevODToRemindBehaviour.Enabled = True
                '    E_cboPrevODToRemindBehaviour.Items.Remove(E_cboPrevODToRemindBehaviour.Items.FindByValue("X"))
                '    rgv_PrevODToRemind.MinimumValue = Hide_PrevODToRemind.Value
                '    rgv_PrevODToRemind.MaximumValue = "9999"
                '    rgv_PrevODToRemind.ErrorMessage = "Harap isi Overdue sebelumnya untuk mengingatkan antara " & rgv_PrevODToRemind.MinimumValue & " dan 9999 "
                'ElseIf oProduct.PrevODToRemindBehaviour_HO = "X" Then
                '    E_txtPrevODToRemind.Enabled = True
                '    E_cboPrevODToRemindBehaviour.Enabled = True
                '    E_cboPrevODToRemindBehaviour.Items.Remove(E_cboPrevODToRemindBehaviour.Items.FindByValue("N"))
                '    rgv_PrevODToRemind.MinimumValue = "0"
                '    rgv_PrevODToRemind.MaximumValue = Hide_PrevODToRemind.Value
                '    rgv_PrevODToRemind.ErrorMessage = "Harap isi Overdue sebelumnya untuk mengingatkan antara 0 dan " & rgv_PrevODToRemind.MaximumValue
                'End If
                'lblPrevODToRemind_Branch.Text = CStr(oProduct.PrevODToRemind_HO) & " days " & Behaviour_Value(oProduct.PrevODToRemindBehaviour_HO)

                ''PDC request to call
                'E_txtPDCDaysToRemind.Text = CStr(oProduct.PDCDayToRemind)
                'Hide_PDCDaysToRemind.Value = CStr(oProduct.PDCDayToRemind_HO)
                'Hide_PDCDaysToRemindBeh.Value = oProduct.PDCDayToRemindBehaviour_HO
                'E_cboPDCDaysToRemindBehaviour.SelectedIndex = E_cboPDCDaysToRemindBehaviour.Items.IndexOf(E_cboPDCDaysToRemindBehaviour.Items.FindByValue(oProduct.PDCDayToRemindBehaviour))
                'If oProduct.PDCDayToRemindBehaviour_HO = "D" Then
                '    E_txtPDCDaysToRemind.Enabled = True
                '    E_cboPDCDaysToRemindBehaviour.Enabled = True
                '    rgv_PDCDaysToRemind.MinimumValue = "0"
                '    rgv_PDCDaysToRemind.MaximumValue = "9999"
                'ElseIf oProduct.PDCDayToRemindBehaviour_HO = "L" Then
                '    E_txtPDCDaysToRemind.Enabled = False
                '    E_cboPDCDaysToRemindBehaviour.Enabled = False
                'ElseIf oProduct.PDCDayToRemindBehaviour_HO = "N" Then
                '    E_txtPDCDaysToRemind.Enabled = True
                '    E_cboPDCDaysToRemindBehaviour.Enabled = True
                '    E_cboPDCDaysToRemindBehaviour.Items.Remove(E_cboPDCDaysToRemindBehaviour.Items.FindByValue("X"))
                '    rgv_PDCDaysToRemind.MinimumValue = Hide_PDCDaysToRemind.Value
                '    rgv_PDCDaysToRemind.MaximumValue = "9999"
                '    rgv_PDCDaysToRemind.ErrorMessage = "Harap isi Permintaan PDC untuk telepon antara " & rgv_PDCDaysToRemind.MinimumValue & " dan 9999 "
                'ElseIf oProduct.PDCDayToRemindBehaviour_HO = "X" Then
                '    E_txtPDCDaysToRemind.Enabled = True
                '    E_cboPDCDaysToRemindBehaviour.Enabled = True
                '    E_cboPDCDaysToRemindBehaviour.Items.Remove(E_cboPDCDaysToRemindBehaviour.Items.FindByValue("N"))
                '    rgv_PDCDaysToRemind.MinimumValue = "0"
                '    rgv_PDCDaysToRemind.MaximumValue = Hide_PDCDaysToRemind.Value
                '    rgv_PDCDaysToRemind.ErrorMessage = "Harap isi Permintaan PDC untuk telepon antara 0 dan " & rgv_PDCDaysToRemind.MaximumValue
                'End If
                'lblPDCDaysToRemind_Branch.Text = CStr(oProduct.PDCDayToRemind_HO) & " Dayss " & Behaviour_Value(oProduct.PDCDayToRemindBehaviour_HO)

                ''Days to remind by SMS
                'E_txtDeskCollSMSRemind.Text = CStr(oProduct.DeskCollSMSRemind)
                'Hide_DeskCollSMSRemind.Value = CStr(oProduct.DeskCollSMSRemind_HO)
                'Hide_DeskCollSMSRemindBeh.Value = oProduct.DeskCollSMSRemindBehaviour_HO
                'E_cboDeskCollSMSRemindBehaviour.SelectedIndex = E_cboDeskCollSMSRemindBehaviour.Items.IndexOf(E_cboDeskCollSMSRemindBehaviour.Items.FindByValue(oProduct.DeskCollSMSRemindBehaviour))
                'If oProduct.DeskCollSMSRemindBehaviour_HO = "D" Then
                '    E_txtDeskCollSMSRemind.Enabled = True
                '    E_cboDeskCollSMSRemindBehaviour.Enabled = True
                '    rgv_DeskCollSMSRemind.MinimumValue = "0"
                '    rgv_DeskCollSMSRemind.MaximumValue = "9999"
                'ElseIf oProduct.DeskCollSMSRemindBehaviour_HO = "L" Then
                '    E_txtDeskCollSMSRemind.Enabled = False
                '    E_cboDeskCollSMSRemindBehaviour.Enabled = False
                'ElseIf oProduct.DeskCollSMSRemindBehaviour_HO = "N" Then
                '    E_txtDeskCollSMSRemind.Enabled = True
                '    E_cboDeskCollSMSRemindBehaviour.Enabled = True
                '    E_cboDeskCollSMSRemindBehaviour.Items.Remove(E_cboDeskCollSMSRemindBehaviour.Items.FindByValue("X"))
                '    rgv_DeskCollSMSRemind.MinimumValue = Hide_DeskCollSMSRemind.Value
                '    rgv_DeskCollSMSRemind.MaximumValue = "9999"
                '    rgv_DeskCollSMSRemind.ErrorMessage = "Harap isi Jumlah hari mengingatkan dgn SMS antara " & rgv_DeskCollSMSRemind.MinimumValue & " dan 9999 "
                'ElseIf oProduct.DeskCollSMSRemindBehaviour_HO = "X" Then
                '    E_txtDeskCollSMSRemind.Enabled = True
                '    E_cboDeskCollSMSRemindBehaviour.Enabled = True
                '    E_cboDeskCollSMSRemindBehaviour.Items.Remove(E_cboDeskCollSMSRemindBehaviour.Items.FindByValue("N"))
                '    rgv_DeskCollSMSRemind.MinimumValue = "0"
                '    rgv_DeskCollSMSRemind.MaximumValue = Hide_DeskCollSMSRemind.Value
                '    rgv_DeskCollSMSRemind.ErrorMessage = "Harap isi Jumlah hari mengingatkan dgn SMS antara 0 dan " & rgv_DeskCollSMSRemind.MaximumValue
                'End If
                'lblDeskCollSMSRemind_Branch.Text = CStr(oProduct.DeskCollSMSRemind_HO) & " days " & Behaviour_Value(oProduct.DeskCollSMSRemindBehaviour_HO)


                ''Days to generate DCR
                'E_txtDCR.Text = CStr(oProduct.DCR)
                'Hide_DCR.Value = CStr(oProduct.DCR_HO)
                'Hide_DCRBeh.Value = oProduct.DCRBehaviour_HO
                'E_cboDCRBehaviour.SelectedIndex = E_cboDCRBehaviour.Items.IndexOf(E_cboDCRBehaviour.Items.FindByValue(oProduct.DCRBehaviour))
                'If oProduct.DCRBehaviour_HO = "D" Then
                '    E_txtDCR.Enabled = True
                '    E_cboDCRBehaviour.Enabled = True
                '    rgv_DCR.MinimumValue = "0"
                '    rgv_DCR.MaximumValue = "9999"
                'ElseIf oProduct.DCRBehaviour_HO = "L" Then
                '    E_txtDCR.Enabled = False
                '    E_cboDCRBehaviour.Enabled = False
                'ElseIf oProduct.DCRBehaviour_HO = "N" Then
                '    E_txtDCR.Enabled = True
                '    E_cboDCRBehaviour.Enabled = True
                '    E_cboDCRBehaviour.Items.Remove(E_cboDCRBehaviour.Items.FindByValue("X"))
                '    rgv_DCR.MinimumValue = Hide_DCR.Value
                '    rgv_DCR.MaximumValue = "9999"
                '    rgv_DCR.ErrorMessage = "Harap isi Jumlah hari buat DCR antara " & rgv_DCR.MinimumValue & " dan 9999 "
                'ElseIf oProduct.DCRBehaviour_HO = "X" Then
                '    E_txtDCR.Enabled = True
                '    E_cboDCRBehaviour.Enabled = True
                '    E_cboDCRBehaviour.Items.Remove(E_cboDCRBehaviour.Items.FindByValue("N"))
                '    rgv_DCR.MinimumValue = "0"
                '    rgv_DCR.MaximumValue = Hide_DCR.Value
                '    rgv_DCR.ErrorMessage = "Harap isi Jumlah hari buat DCR antara 0 dan " & rgv_DCR.MaximumValue
                'End If
                'lblDCR_Branch.Text = CStr(oProduct.DCR_HO) & " days " & Behaviour_Value(oProduct.DCRBehaviour_HO)

                ''Days before due to generate Receipt Notes
                'E_txtDaysBeforeDueToRN.Text = CStr(oProduct.DaysBeforeDueToRN)
                'Hide_DaysBeforeDueToRN.Value = CStr(oProduct.DaysBeforeDueToRN_HO)
                'Hide_DaysBeforeDueToRNBeh.Value = oProduct.DaysBeforeDueToRNBehaviour_HO
                'E_cboDaysBeforeDueToRNBehaviour.SelectedIndex = E_cboDaysBeforeDueToRNBehaviour.Items.IndexOf(E_cboDaysBeforeDueToRNBehaviour.Items.FindByValue(oProduct.DaysBeforeDueToRNBehaviour))
                'If oProduct.DaysBeforeDueToRNBehaviour_HO = "D" Then
                '    E_txtDaysBeforeDueToRN.Enabled = True
                '    E_cboDaysBeforeDueToRNBehaviour.Enabled = True
                '    rgv_DaysBeforeDueToRN.MinimumValue = "0"
                '    rgv_DaysBeforeDueToRN.MaximumValue = "9999"
                'ElseIf oProduct.DaysBeforeDueToRNBehaviour_HO = "L" Then
                '    E_txtDaysBeforeDueToRN.Enabled = False
                '    E_cboDaysBeforeDueToRNBehaviour.Enabled = False
                'ElseIf oProduct.DaysBeforeDueToRNBehaviour_HO = "N" Then
                '    E_txtDaysBeforeDueToRN.Enabled = True
                '    E_cboDaysBeforeDueToRNBehaviour.Enabled = True
                '    E_cboDaysBeforeDueToRNBehaviour.Items.Remove(E_cboDaysBeforeDueToRNBehaviour.Items.FindByValue("X"))
                '    rgv_DaysBeforeDueToRN.MinimumValue = Hide_DaysBeforeDueToRN.Value
                '    rgv_DaysBeforeDueToRN.MaximumValue = "9999"
                '    rgv_DaysBeforeDueToRN.ErrorMessage = "Harap isi Jumlah hari sebelum JT buat Kwitansi antara " & rgv_DaysBeforeDueToRN.MinimumValue & " dan 9999 "
                'ElseIf oProduct.DaysBeforeDueToRNBehaviour_HO = "X" Then
                '    E_txtDaysBeforeDueToRN.Enabled = True
                '    E_cboDaysBeforeDueToRNBehaviour.Enabled = True
                '    E_cboDaysBeforeDueToRNBehaviour.Items.Remove(E_cboDaysBeforeDueToRNBehaviour.Items.FindByValue("N"))
                '    rgv_DaysBeforeDueToRN.MinimumValue = "0"
                '    rgv_DaysBeforeDueToRN.MaximumValue = Hide_DaysBeforeDueToRN.Value
                '    rgv_DaysBeforeDueToRN.ErrorMessage = "Harap isi Jumlah hari sebelum JT buat Kwitansi antara 0 dan " & rgv_DaysBeforeDueToRN.MaximumValue
                'End If
                'lblDaysBeforeDueToRN_Branch.Text = CStr(oProduct.DaysBeforeDueToRN_HO) & " days " & Behaviour_Value(oProduct.DaysBeforeDueToRNBehaviour_HO)

                ''Days to generate RAL
                'E_txtODToRAL.Text = CStr(oProduct.ODToRAL)
                'Hide_ODToRAL.Value = CStr(oProduct.ODToRAL_HO)
                'Hide_ODToRALBeh.Value = oProduct.ODToRALBehaviour_HO
                'E_cboODToRALBehaviour.SelectedIndex = E_cboODToRALBehaviour.Items.IndexOf(E_cboODToRALBehaviour.Items.FindByValue(oProduct.ODToRALBehaviour))
                'If oProduct.ODToRALBehaviour_HO = "D" Then
                '    E_txtODToRAL.Enabled = True
                '    E_cboODToRALBehaviour.Enabled = True
                '    rgv_ODToRAL.MinimumValue = "0"
                '    rgv_ODToRAL.MaximumValue = "9999"
                'ElseIf oProduct.ODToRALBehaviour_HO = "L" Then
                '    E_txtODToRAL.Enabled = False
                '    E_cboODToRALBehaviour.Enabled = False
                'ElseIf oProduct.ODToRALBehaviour_HO = "N" Then
                '    E_txtODToRAL.Enabled = True
                '    E_cboODToRALBehaviour.Enabled = True
                '    E_cboODToRALBehaviour.Items.Remove(E_cboODToRALBehaviour.Items.FindByValue("X"))
                '    rgv_ODToRAL.MinimumValue = Hide_ODToRAL.Value
                '    rgv_ODToRAL.MaximumValue = "9999"
                '    rgv_ODToRAL.ErrorMessage = "Harap isi Jumlah hari buat SKT antara " & rgv_ODToRAL.MinimumValue & " dan 9999 "
                'ElseIf oProduct.ODToRALBehaviour_HO = "X" Then
                '    E_txtODToRAL.Enabled = True
                '    E_cboODToRALBehaviour.Enabled = True
                '    E_cboODToRALBehaviour.Items.Remove(E_cboODToRALBehaviour.Items.FindByValue("N"))
                '    rgv_ODToRAL.MinimumValue = "0"
                '    rgv_ODToRAL.MaximumValue = Hide_ODToRAL.Value
                '    rgv_ODToRAL.ErrorMessage = "Harap isi Jumlah hari buat SKT antara 0 dan " & rgv_ODToRAL.MaximumValue
                'End If
                'lblODToRAL_Branch.Text = CStr(oProduct.ODToRAL_HO) & " days " & Behaviour_Value(oProduct.ODToRALBehaviour_HO)

                ''RAL Period
                'E_txtRALPeriod.Text = CStr(oProduct.RALPeriod)
                'Hide_RALPeriod.Value = CStr(oProduct.RALPeriod_HO)
                'Hide_RALPeriodBeh.Value = oProduct.RALPeriodBehaviour_HO
                'E_cboRALPeriodBehaviour.SelectedIndex = E_cboRALPeriodBehaviour.Items.IndexOf(E_cboRALPeriodBehaviour.Items.FindByValue(oProduct.RALPeriodBehaviour))
                'If oProduct.RALPeriodBehaviour_HO = "D" Then
                '    E_txtRALPeriod.Enabled = True
                '    E_cboRALPeriodBehaviour.Enabled = True
                '    rgv_RALPeriod.MinimumValue = "0"
                '    rgv_RALPeriod.MaximumValue = "9999"
                'ElseIf oProduct.RALPeriodBehaviour_HO = "L" Then
                '    E_txtRALPeriod.Enabled = False
                '    E_cboRALPeriodBehaviour.Enabled = False
                'ElseIf oProduct.RALPeriodBehaviour_HO = "N" Then
                '    E_txtRALPeriod.Enabled = True
                '    E_cboRALPeriodBehaviour.Enabled = True
                '    E_cboRALPeriodBehaviour.Items.Remove(E_cboRALPeriodBehaviour.Items.FindByValue("X"))
                '    rgv_RALPeriod.MinimumValue = Hide_RALPeriod.Value
                '    rgv_RALPeriod.MaximumValue = "9999"
                '    rgv_RALPeriod.ErrorMessage = "Harap isi Jangka Waktu SKT antara " & rgv_RALPeriod.MinimumValue & " dan 9999 "
                'ElseIf oProduct.RALPeriodBehaviour_HO = "X" Then
                '    E_txtRALPeriod.Enabled = True
                '    E_cboRALPeriodBehaviour.Enabled = True
                '    E_cboRALPeriodBehaviour.Items.Remove(E_cboRALPeriodBehaviour.Items.FindByValue("N"))
                '    rgv_RALPeriod.MinimumValue = "0"
                '    rgv_RALPeriod.MaximumValue = Hide_RALPeriod.Value
                '    rgv_RALPeriod.ErrorMessage = "Harap isi Jangka Waktu SKT antara 0 dan " & rgv_RALPeriod.MaximumValue
                'End If
                'lblRALPeriod_Branch.Text = CStr(oProduct.RALPeriod_HO) & " days " & Behaviour_Value(oProduct.RALPeriodBehaviour_HO)


            'Maximum days Receipt notes paid
                'E_txtMaxDaysRNPaid.Text = CStr(oProduct.MaxDaysRNPaid)
                'Hide_MaxDaysRNPaid.Value = CStr(oProduct.MaxDaysRNPaid_HO)
                'Hide_MaxDaysRNPaidBeh.Value = oProduct.MaxDaysRNPaidBehaviour_HO
                'E_cboMaxDaysRNPaidBehaviour.SelectedIndex = E_cboMaxDaysRNPaidBehaviour.Items.IndexOf(E_cboMaxDaysRNPaidBehaviour.Items.FindByValue(oProduct.MaxDaysRNPaidBehaviour))
                'If oProduct.MaxDaysRNPaidBehaviour_HO = "D" Then
                '    E_txtMaxDaysRNPaid.Enabled = True
                '    E_cboMaxDaysRNPaidBehaviour.Enabled = True
                '    rgv_MaxDaysRNPaid.MinimumValue = "0"
                '    rgv_MaxDaysRNPaid.MaximumValue = "9999"
                'ElseIf oProduct.MaxDaysRNPaidBehaviour_HO = "L" Then
                '    E_txtMaxDaysRNPaid.Enabled = False
                '    E_cboMaxDaysRNPaidBehaviour.Enabled = False
                'ElseIf oProduct.MaxDaysRNPaidBehaviour_HO = "N" Then
                '    E_txtMaxDaysRNPaid.Enabled = True
                '    E_cboMaxDaysRNPaidBehaviour.Enabled = True
                '    E_cboMaxDaysRNPaidBehaviour.Items.Remove(E_cboMaxDaysRNPaidBehaviour.Items.FindByValue("X"))
                '    rgv_MaxDaysRNPaid.MinimumValue = Hide_MaxDaysRNPaid.Value
                '    rgv_MaxDaysRNPaid.MaximumValue = "9999"
                '    rgv_MaxDaysRNPaid.ErrorMessage = "Harap isi Jumlah hari Maximum Kwitansi dibayar antara " & rgv_MaxDaysRNPaid.MinimumValue & " dan 9999 "
                'ElseIf oProduct.MaxDaysRNPaidBehaviour_HO = "X" Then
                '    E_txtMaxDaysRNPaid.Enabled = True
                '    E_cboMaxDaysRNPaidBehaviour.Enabled = True
                '    E_cboMaxDaysRNPaidBehaviour.Items.Remove(E_cboMaxDaysRNPaidBehaviour.Items.FindByValue("N"))
                '    rgv_MaxDaysRNPaid.MinimumValue = "0"
                '    rgv_MaxDaysRNPaid.MaximumValue = Hide_MaxDaysRNPaid.Value
                '    rgv_MaxDaysRNPaid.ErrorMessage = "Harap isi Jumlah hari Maximum Kwitansi dibayar antara 0 dan " & rgv_MaxDaysRNPaid.MaximumValue
                'End If
                'lblMaxDaysRNPaid_Branch.Text = CStr(oProduct.MaxDaysRNPaid_HO) & " days " & Behaviour_Value(oProduct.MaxDaysRNPaidBehaviour_HO)


                ''Maximum Promise To Pay days
                'E_txtMaxPTPYDays.Text = CStr(oProduct.MaxPTPYDays)
                'Hide_MaxPTPYDays.Value = CStr(oProduct.MaxPTPYDays_HO)
                'Hide_MaxPTPYDaysBeh.Value = oProduct.MaxPTPYDaysBehaviour_HO
                'E_cboMaxPTPYDaysBehaviour.SelectedIndex = E_cboMaxPTPYDaysBehaviour.Items.IndexOf(E_cboMaxPTPYDaysBehaviour.Items.FindByValue(oProduct.MaxPTPYDaysBehaviour))
                'If oProduct.MaxPTPYDaysBehaviour_HO = "D" Then
                '    E_txtMaxPTPYDays.Enabled = True
                '    E_cboMaxPTPYDaysBehaviour.Enabled = True
                '    rgv_MaxPTPYDays.MinimumValue = "0"
                '    rgv_MaxPTPYDays.MaximumValue = "9999"
                'ElseIf oProduct.MaxPTPYDaysBehaviour_HO = "L" Then
                '    E_txtMaxPTPYDays.Enabled = False
                '    E_cboMaxPTPYDaysBehaviour.Enabled = False
                'ElseIf oProduct.MaxPTPYDaysBehaviour_HO = "N" Then
                '    E_txtMaxPTPYDays.Enabled = True
                '    E_cboMaxPTPYDaysBehaviour.Enabled = True
                '    E_cboMaxPTPYDaysBehaviour.Items.Remove(E_cboMaxPTPYDaysBehaviour.Items.FindByValue("X"))
                '    rgv_MaxPTPYDays.MinimumValue = Hide_MaxPTPYDays.Value
                '    rgv_MaxPTPYDays.MaximumValue = "9999"
                '    rgv_MaxPTPYDays.ErrorMessage = "Harap isi Jumlah hari Maximum janji Bayar antara " & rgv_MaxPTPYDays.MinimumValue & " dan 9999 "
                'ElseIf oProduct.MaxPTPYDaysBehaviour_HO = "X" Then
                '    E_txtMaxPTPYDays.Enabled = True
                '    E_cboMaxPTPYDaysBehaviour.Enabled = True
                '    E_cboMaxPTPYDaysBehaviour.Items.Remove(E_cboMaxPTPYDaysBehaviour.Items.FindByValue("N"))
                '    rgv_MaxPTPYDays.MinimumValue = "0"
                '    rgv_MaxPTPYDays.MaximumValue = Hide_MaxPTPYDays.Value
                '    rgv_MaxPTPYDays.ErrorMessage = "Harap isi Jumlah hari Maximum janji Bayar antara 0 dan " & rgv_MaxPTPYDays.MaximumValue
                'End If
                'lblMaxPTPYDays_Branch.Text = CStr(oProduct.MaxPTPYDays_HO) & " days " & Behaviour_Value(oProduct.MaxPTPYDaysBehaviour_HO)


            'Promise To Pay To Bank
                'E_txtPTPYBank.Text = CStr(oProduct.PTPYBank)
                'Hide_PTPYBank.Value = CStr(oProduct.PTPYBank_HO)
                'Hide_PTPYBankBeh.Value = oProduct.PTPYBankBehaviour_HO
                'E_txtPTPYBankBehaviour.SelectedIndex = E_txtPTPYBankBehaviour.Items.IndexOf(E_txtPTPYBankBehaviour.Items.FindByValue(oProduct.PTPYBankBehaviour))
                'If oProduct.PTPYBankBehaviour_HO = "D" Then
                '    E_txtPTPYBank.Enabled = True
                '    E_txtPTPYBankBehaviour.Enabled = True
                '    rgv_PTPYBank.MinimumValue = "0"
                '    rgv_PTPYBank.MaximumValue = "9999"
                'ElseIf oProduct.PTPYBankBehaviour_HO = "L" Then
                '    E_txtPTPYBank.Enabled = False
                '    E_txtPTPYBankBehaviour.Enabled = False
                'ElseIf oProduct.PTPYBankBehaviour_HO = "N" Then
                '    E_txtPTPYBank.Enabled = True
                '    E_txtPTPYBankBehaviour.Enabled = True
                '    E_txtPTPYBankBehaviour.Items.Remove(E_txtPTPYBankBehaviour.Items.FindByValue("X"))
                '    rgv_PTPYBank.MinimumValue = Hide_GracePeriodLateCharges.Value
                '    rgv_PTPYBank.MaximumValue = "9999"
                '    rgv_PTPYBank.ErrorMessage = "Harap isi Janji Bayar ke Bank antara " & rgv_PTPYBank.MinimumValue & " dan 9999 "
                'ElseIf oProduct.PTPYBankBehaviour_HO = "X" Then
                '    E_txtPTPYBank.Enabled = True
                '    E_txtPTPYBankBehaviour.Enabled = True
                '    E_txtPTPYBankBehaviour.Items.Remove(E_txtPTPYBankBehaviour.Items.FindByValue("N"))
                '    rgv_PTPYBank.MinimumValue = "0"
                '    rgv_PTPYBank.MaximumValue = Hide_GracePeriodLateCharges.Value
                '    rgv_PTPYBank.ErrorMessage = "Harap isi Janji Bayar ke Bank antara 0 dan " & rgv_PTPYBank.MaximumValue
                'End If
                'lblPTPYBank_Branch.Text = CStr(oProduct.PTPYBank_HO) & " days " & Behaviour_Value(oProduct.PTPYBankBehaviour_HO)


            'Promise To Pay To Company
                'E_txtPTPYCompany.Text = CStr(oProduct.PTPYCompany)
                'Hide_PTPYCompany.Value = CStr(oProduct.PTPYCompany_HO)
                'Hide_PTPYCompanyBeh.Value = oProduct.PTPYCompanyBehaviour_HO
                'E_cboPTPYCompanyBehaviour.SelectedIndex = E_cboPTPYCompanyBehaviour.Items.IndexOf(E_cboPTPYCompanyBehaviour.Items.FindByValue(oProduct.PTPYCompanyBehaviour))
                'If oProduct.PTPYCompanyBehaviour_HO = "D" Then
                '    E_txtPTPYCompany.Enabled = True
                '    E_cboPTPYCompanyBehaviour.Enabled = True
                '    rgv_PTPYCompany.MinimumValue = "0"
                '    rgv_PTPYCompany.MaximumValue = "9999"
                'ElseIf oProduct.PTPYCompanyBehaviour_HO = "L" Then
                '    E_txtPTPYCompany.Enabled = False
                '    E_cboPTPYCompanyBehaviour.Enabled = False
                'ElseIf oProduct.PTPYCompanyBehaviour_HO = "N" Then
                '    E_txtPTPYCompany.Enabled = True
                '    E_cboPTPYCompanyBehaviour.Enabled = True
                '    E_cboPTPYCompanyBehaviour.Items.Remove(E_cboPTPYCompanyBehaviour.Items.FindByValue("X"))
                '    rgv_PTPYCompany.MinimumValue = Hide_PTPYCompany.Value
                '    rgv_PTPYCompany.MaximumValue = "9999"
                '    rgv_PTPYCompany.ErrorMessage = "Harap isi Janji bayar ke Perusahaan antara " & rgv_PTPYCompany.MinimumValue & " dan 9999 "
                'ElseIf oProduct.PTPYCompanyBehaviour_HO = "X" Then
                '    E_txtPTPYCompany.Enabled = True
                '    E_cboPTPYCompanyBehaviour.Enabled = True
                '    E_cboPTPYCompanyBehaviour.Items.Remove(E_cboPTPYCompanyBehaviour.Items.FindByValue("N"))
                '    rgv_PTPYCompany.MinimumValue = "0"
                '    rgv_PTPYCompany.MaximumValue = Hide_PTPYCompany.Value
                '    rgv_PTPYCompany.ErrorMessage = "Harap isi Janji bayar ke Perusahaan antara 0 dan " & rgv_PTPYCompany.MaximumValue
                'End If
                'lblPTPYCompany_Branch.Text = CStr(oProduct.PTPYCompany_HO) & " days " & Behaviour_Value(oProduct.PTPYCompanyBehaviour_HO)


            'Promise To Pay To Supplier
                'E_txtPTPYSupplier.Text = CStr(oProduct.PTPYSupplier)
                'Hide_PTPYSupplier.Value = CStr(oProduct.PTPYSupplier_HO)
                'Hide_PTPYSupplierBeh.Value = oProduct.PTPYSupplierBehaviour_HO
                'E_cboPTPYSupplierBehaviour.SelectedIndex = E_cboPTPYSupplierBehaviour.Items.IndexOf(E_cboPTPYSupplierBehaviour.Items.FindByValue(oProduct.PTPYSupplierBehaviour))
                'If oProduct.PTPYSupplierBehaviour_HO = "D" Then
                '    E_txtPTPYSupplier.Enabled = True
                '    E_cboPTPYSupplierBehaviour.Enabled = True
                '    rgv_PTPYSupplier.MinimumValue = "0"
                '    rgv_PTPYSupplier.MaximumValue = "9999"
                'ElseIf oProduct.PTPYSupplierBehaviour_HO = "L" Then
                '    E_txtPTPYSupplier.Enabled = False
                '    E_cboPTPYSupplierBehaviour.Enabled = False
                'ElseIf oProduct.PTPYSupplierBehaviour_HO = "N" Then
                '    E_txtPTPYSupplier.Enabled = True
                '    E_cboPTPYSupplierBehaviour.Enabled = True
                '    E_cboPTPYSupplierBehaviour.Items.Remove(E_cboPTPYSupplierBehaviour.Items.FindByValue("X"))
                '    rgv_PTPYSupplier.MinimumValue = Hide_PTPYSupplier.Value
                '    rgv_PTPYSupplier.MaximumValue = "9999"
                '    rgv_PTPYSupplier.ErrorMessage = "Harap isi Janji bayar ke Supplier antara " & rgv_PTPYSupplier.MinimumValue & " dan 9999 "
                'ElseIf oProduct.PTPYSupplierBehaviour_HO = "X" Then
                '    E_txtPTPYSupplier.Enabled = True
                '    E_cboPTPYSupplierBehaviour.Enabled = True
                '    E_cboPTPYSupplierBehaviour.Items.Remove(E_cboPTPYSupplierBehaviour.Items.FindByValue("N"))
                '    rgv_PTPYSupplier.MinimumValue = "0"
                '    rgv_PTPYSupplier.MaximumValue = Hide_PTPYSupplier.Value
                '    rgv_PTPYSupplier.ErrorMessage = "Harap isi Janji bayar ke Supplier antara 0 dan " & rgv_PTPYSupplier.MaximumValue
                'End If
                'lblPTPYSupplier_Branch.Text = CStr(oProduct.PTPYSupplier_HO) & " days " & Behaviour_Value(oProduct.PTPYSupplierBehaviour_HO)

                ''RAL Extension
                'E_txtRALExtension.Text = CStr(oProduct.RALExtension)
                'Hide_RALExtension.Value = CStr(oProduct.RALExtension_HO)
                'Hide_RALExtensionBeh.Value = oProduct.RALExtensionBehaviour_HO
                'E_cboRALExtensionBehaviour.SelectedIndex = E_cboRALExtensionBehaviour.Items.IndexOf(E_cboRALExtensionBehaviour.Items.FindByValue(oProduct.RALExtensionBehaviour))
                'If oProduct.RALExtensionBehaviour_HO = "D" Then
                '    E_txtRALExtension.Enabled = True
                '    E_cboRALExtensionBehaviour.Enabled = True
                '    rgv_RALExtension.MinimumValue = "0"
                '    rgv_RALExtension.MaximumValue = "9999"
                'ElseIf oProduct.RALExtensionBehaviour_HO = "L" Then
                '    E_txtRALExtension.Enabled = False
                '    E_cboRALExtensionBehaviour.Enabled = False
                'ElseIf oProduct.RALExtensionBehaviour_HO = "N" Then
                '    E_txtRALExtension.Enabled = True
                '    E_cboRALExtensionBehaviour.Enabled = True
                '    E_cboRALExtensionBehaviour.Items.Remove(E_cboRALExtensionBehaviour.Items.FindByValue("X"))
                '    rgv_RALExtension.MinimumValue = Hide_RALExtension.Value
                '    rgv_RALExtension.MaximumValue = "9999"
                '    rgv_RALExtension.ErrorMessage = "Harap isi Perpanjangan SKT antara " & rgv_RALExtension.MinimumValue & " dan 9999 "
                'ElseIf oProduct.RALExtensionBehaviour_HO = "X" Then
                '    E_txtRALExtension.Enabled = True
                '    E_cboRALExtensionBehaviour.Enabled = True
                '    E_cboRALExtensionBehaviour.Items.Remove(E_cboRALExtensionBehaviour.Items.FindByValue("N"))
                '    rgv_RALExtension.MinimumValue = "0"
                '    rgv_RALExtension.MaximumValue = Hide_RALExtension.Value
                '    rgv_RALExtension.ErrorMessage = "Harap isi Perpanjangan SKT antara 0 dan " & rgv_RALExtension.MaximumValue
                'End If
                'lblRALExtension_Branch.Text = CStr(oProduct.RALExtension_HO) & " days " & Behaviour_Value(oProduct.RALExtensionBehaviour_HO)


                ''Inventory Expected
                'E_txtInventoryExpected.Text = CStr(oProduct.InventoryExpected)
                'Hide_InventoryExpected.Value = CStr(oProduct.InventoryExpected_HO)
                'Hide_InventoryExpectedBeh.Value = oProduct.InventoryExpectedBehaviour_HO
                'E_cboInventoryExpectedBehaviour.SelectedIndex = E_cboInventoryExpectedBehaviour.Items.IndexOf(E_cboInventoryExpectedBehaviour.Items.FindByValue(oProduct.InventoryExpectedBehaviour))
                'If oProduct.InventoryExpectedBehaviour_HO = "D" Then
                '    E_txtInventoryExpected.Enabled = True
                '    E_cboInventoryExpectedBehaviour.Enabled = True
                '    rgv_InventoryExpected.MinimumValue = "0"
                '    rgv_InventoryExpected.MaximumValue = "9999"
                'ElseIf oProduct.InventoryExpectedBehaviour_HO = "L" Then
                '    E_txtInventoryExpected.Enabled = False
                '    E_cboInventoryExpectedBehaviour.Enabled = False
                'ElseIf oProduct.InventoryExpectedBehaviour_HO = "N" Then
                '    E_txtInventoryExpected.Enabled = True
                '    E_cboInventoryExpectedBehaviour.Enabled = True
                '    E_cboInventoryExpectedBehaviour.Items.Remove(E_cboInventoryExpectedBehaviour.Items.FindByValue("X"))
                '    rgv_InventoryExpected.MinimumValue = Hide_InventoryExpected.Value
                '    rgv_InventoryExpected.MaximumValue = "9999"
                '    rgv_InventoryExpected.ErrorMessage = "Harap isi Jangka Waktu jadi Inventory antara " & rgv_InventoryExpected.MinimumValue & " dan 9999 "
                'ElseIf oProduct.InventoryExpectedBehaviour_HO = "X" Then
                '    E_txtInventoryExpected.Enabled = True
                '    E_cboInventoryExpectedBehaviour.Enabled = True
                '    E_cboInventoryExpectedBehaviour.Items.Remove(E_cboInventoryExpectedBehaviour.Items.FindByValue("N"))
                '    rgv_InventoryExpected.MinimumValue = "0"
                '    rgv_InventoryExpected.MaximumValue = Hide_InventoryExpected.Value
                '    rgv_InventoryExpected.ErrorMessage = "Harap isi Jangka Waktu jadi Inventory antara 0 dan " & rgv_InventoryExpected.MaximumValue
                'End If
                'lblInventoryExpected_Branch.Text = CStr(oProduct.InventoryExpected_HO) & " days " & Behaviour_Value(oProduct.InventoryExpectedBehaviour_HO)

                ''Billing Charges
                'E_txtBillingCharges.Text = CStr(oProduct.BillingCharges)
                'Hide_BillingCharges.Value = CStr(oProduct.BillingCharges_HO)
                'Hide_BillingChargesBeh.Value = oProduct.BillingChargesBehaviour_HO
                'E_cboBillingChargesBehaviour.SelectedIndex = E_cboBillingChargesBehaviour.Items.IndexOf(E_cboBillingChargesBehaviour.Items.FindByValue(oProduct.BillingChargesBehaviour))
                'If oProduct.BillingChargesBehaviour_HO = "D" Then
                '    E_txtBillingCharges.Enabled = True
                '    E_cboBillingChargesBehaviour.Enabled = True
                '    rgv_BillingCharges.MinimumValue = "0"
                '    rgv_BillingCharges.MaximumValue = "999999999999"
                'ElseIf oProduct.BillingChargesBehaviour_HO = "L" Then
                '    E_txtBillingCharges.Enabled = False
                '    E_cboBillingChargesBehaviour.Enabled = False
                'ElseIf oProduct.BillingChargesBehaviour_HO = "N" Then
                '    E_txtBillingCharges.Enabled = True
                '    E_cboBillingChargesBehaviour.Enabled = True
                '    E_cboBillingChargesBehaviour.Items.Remove(E_cboBillingChargesBehaviour.Items.FindByValue("X"))
                '    rgv_BillingCharges.MinimumValue = Hide_BillingCharges.Value
                '    rgv_BillingCharges.MaximumValue = "999999999999"
                '    rgv_BillingCharges.ErrorMessage = "Harap isi Biaya Tagih antara " & rgv_BillingCharges.MinimumValue & " dan 999999999999 "
                'ElseIf oProduct.BillingChargesBehaviour_HO = "X" Then
                '    E_txtBillingCharges.Enabled = True
                '    E_cboBillingChargesBehaviour.Enabled = True
                '    E_cboBillingChargesBehaviour.Items.Remove(E_cboBillingChargesBehaviour.Items.FindByValue("N"))
                '    rgv_BillingCharges.MinimumValue = "0"
                '    rgv_BillingCharges.MaximumValue = Hide_BillingCharges.Value
                '    rgv_BillingCharges.ErrorMessage = "Harap isi Biaya Tagih antaran 0 dan " & rgv_BillingCharges.MaximumValue
                'End If
                'lblBillingCharges_Branch.Text = CStr(oProduct.BillingCharges_HO) & "  " & Behaviour_Value(oProduct.BillingChargesBehaviour_HO)


                ''Limit A/P Disbursement at Branch
                'E_txtLimitAPCash.Text = CStr(oProduct.LimitAPCash)
                'Hide_LimitAPCash.Value = CStr(oProduct.LimitAPCash_HO)
                'Hide_LimitAPCashBeh.Value = oProduct.LimitAPCashBehaviour_HO
                'E_cboLimitAPCashBehaviour.SelectedIndex = E_cboLimitAPCashBehaviour.Items.IndexOf(E_cboLimitAPCashBehaviour.Items.FindByValue(oProduct.LimitAPCashBehaviour))
                'If oProduct.LimitAPCashBehaviour_HO = "D" Then
                '    E_txtLimitAPCash.Enabled = True
                '    E_cboLimitAPCashBehaviour.Enabled = True
                '    rgv_LimitAPCash.MinimumValue = "0"
                '    rgv_LimitAPCash.MaximumValue = "999999999999"
                'ElseIf oProduct.LimitAPCashBehaviour_HO = "L" Then
                '    E_txtLimitAPCash.Enabled = False
                '    E_cboLimitAPCashBehaviour.Enabled = False
                'ElseIf oProduct.LimitAPCashBehaviour_HO = "N" Then
                '    E_txtLimitAPCash.Enabled = True
                '    E_cboLimitAPCashBehaviour.Enabled = True
                '    E_cboLimitAPCashBehaviour.Items.Remove(E_cboLimitAPCashBehaviour.Items.FindByValue("X"))
                '    rgv_LimitAPCash.MinimumValue = Hide_LimitAPCash.Value
                '    rgv_LimitAPCash.MaximumValue = "9999"
                '    rgv_LimitAPCash.ErrorMessage = "Harap isi Limit Pembayaran di Cabang antara " & rgv_LimitAPCash.MinimumValue & " dan 999999999999 "
                'ElseIf oProduct.LimitAPCashBehaviour_HO = "X" Then
                '    E_txtLimitAPCash.Enabled = True
                '    E_cboLimitAPCashBehaviour.Enabled = True
                '    E_cboLimitAPCashBehaviour.Items.Remove(E_cboLimitAPCashBehaviour.Items.FindByValue("N"))
                '    rgv_LimitAPCash.MinimumValue = "0"
                '    rgv_LimitAPCash.MaximumValue = Hide_LimitAPCash.Value
                '    rgv_LimitAPCash.ErrorMessage = "Harap isi Limit Pembayaran di Cabang antara 0 dan " & rgv_LimitAPCash.MaximumValue
                'End If
                'lblLimitAPCash_Branch.Text = CStr(oProduct.LimitAPCash_HO) & "  " & Behaviour_Value(oProduct.LimitAPCashBehaviour_HO)


                ''Is Recourse
                'Dim strIsRecourse As String
                'If oProduct.IsRecourse_HO = True Then
                '    strIsRecourse = "Yes"
                'Else
                '    strIsRecourse = "No"
                'End If
                'E_lblIsRecourse.Text = strIsRecourse
        End If
        End If
    End Sub
#End Region

#Region "imbSave"
    Private Sub imbSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
        If Page.IsValid Then
            Dim customClass As New Parameter.Product
            Dim ErrMessage As String = ""
            Dim oProduct As New Parameter.Product
            Dim BranchID As String = Me.sesBranchId
            BranchID = Replace(BranchID, "'", "")

            If Me.AddEdit = "EDIT" Then
                ' cek untuk SP
                If Me.SPBehaviour = "L" Then
                    'If (E_cboIsSPAutomatic.SelectedItem.Value.Trim <> Me.SPAutomatic) Then                        
                    '    ShowMessage(lblMessage, "SP Otomatis cabang harus sama seperti KP", True)
                    '    pnlList.Visible = False
                    '    pnlEdit.Visible = True
                    '    Exit Sub
                    'End If

                    'If (CInt(E_txtLengthSPProcess.Text.Trim) <> Me.LengthSP) Then                        
                    '    ShowMessage(lblMessage, "Jangka Waktu SP cabang harus sama seperti KP", True)
                    '    pnlList.Visible = False
                    '    pnlEdit.Visible = True
                    '    Exit Sub
                    'End If
                End If

                ' cek untuk SP1
                If Me.SP1Behaviour = "L" Then
                    'If (E_cboIsSP1Automatic.SelectedItem.Value.Trim <> Me.SP1Automatic) Then                        
                    '    ShowMessage(lblMessage, "SP1 Otomatis cabang harus sama seperti KP", True)
                    '    pnlList.Visible = False
                    '    pnlEdit.Visible = True
                    '    Exit Sub
                    'End If

                    'If (CInt(E_txtLengthSP1Process.Text.Trim) <> Me.LengthSP1) Then                        
                    '    ShowMessage(lblMessage, "Jangka Waktu SP1 cabang harus sama seperti KP", True)
                    '    pnlList.Visible = False
                    '    pnlEdit.Visible = True
                    '    Exit Sub
                    'End If
                End If

                ' cek untuk SP2
                If Me.SP2Behaviour = "L" Then
                    'If (E_cboIsSP2Automatic.SelectedItem.Value.Trim <> Me.SP2Automatic) Then                        
                    '    ShowMessage(lblMessage, "SP2 Otomatis cabang harus sama seperti KP", True)
                    '    pnlList.Visible = False
                    '    pnlEdit.Visible = True
                    '    Exit Sub
                    'End If

                    'If (CInt(E_txtLengthSP2Process.Text.Trim) <> Me.LengthSP2) Then                        
                    '    ShowMessage(lblMessage, "Jangka Waktu SP2 cabang harus sama seperti KP", True)
                    '    pnlList.Visible = False
                    '    pnlEdit.Visible = True
                    '    Exit Sub
                    'End If
                End If

                With oProduct
                    .ProductId = E_lblProductID.Text
                    .Branch_ID = Me.BranchID

                    '.EffectiveRate = CDec(E_txtEffectiveRate.Text.Trim)
                    '.EffectiveRateBehaviour = E_cboEffectiveRateBehaviour.SelectedItem.Value.Trim
                    '.GrossYieldRate = CDec(E_txtGrossYieldRate.Text.Trim)
                    '.GrossYieldRateBehaviour = E_cboGrossYieldRateBehaviour.SelectedItem.Value.Trim
                    '.MinimumTenor = CInt(E_txtMinimumTenor.Text.Trim)
                    '.MaximumTenor = CInt(E_txtMaximumTenor.Text.Trim)

                    '.PrepaymentPenaltyFixed = CDec(txtPrepaymentPenaltyFixed.Text.Trim)
                    '.PrepaymentPenaltyFixedBehaviour = cboPrepaymentPenaltyFixedBehaviour.SelectedItem.Value.Trim
                    '.PenaltyBasedOn = rboPenaltyBasedOn.SelectedItem.Value.Trim
                    '.PenaltyBasedOnBehaviour = cboPenaltyBasedOn.SelectedItem.Value.Trim

                    '.PenaltyRatePrevious = CDec(txtPenaltyRatePrevious.Text.Trim)
                    '.PenaltyRateEffectiveDate = Date.ParseExact(txtPenaltyRateEffectiveDate.Text.ToString, "dd/MM/yyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo)
                    '.PrepaymentPenaltyPrevious = CDec(txtPrepaymentPenaltyPrevious.Text.Trim)
                    '.PrepaymentPenaltyEffectiveDate = Date.ParseExact(txtPrepaymentPenaltyEffectiveDate.Text.ToString, "dd/MM/yyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo)
                    '.PrepaymentPenaltyFixedPrevious = CDec(txtPrepaymentPenaltyFixedPrevious.Text.Trim)
                    '.PrepaymentPenaltyFixedEffectiveDate = Date.ParseExact(txtPrepaymentPenaltyFixedEffectiveDate.Text.ToString, "dd/MM/yyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo)
                    ''.PrioritasPembayaran = rboPrioritasPembayaran.SelectedItem.Value.Trim

                    '.InsuranceDiscPercentage = CDec(txtInsuranceDiscPercentage.Text.Trim)
                    '.InsuranceDiscPercentageBehaviour = cboInsuranceDiscPercentageBehaviour.SelectedItem.Value.Trim
                    '.IncomeInsRatioPercentage = CDec(txtIncomeInsRatioPercentage.Text.Trim)
                    '.IncomeInsRatioPercentageBehaviour = cboIncomeInsRatioPercentageBehaviour.SelectedItem.Value.Trim

                    .UmurKendaraanFrom = CInt(txtUmurKendaraanFrom.Text.Trim)
                    .UmurKendaraanTo = CInt(txtUmurKendaraanTo.Text.Trim)
                    ''------
                    '.IsSPAutomaticBehaviour = cboIsSPAutomaticBehaviour.SelectedItem.Value.Trim
                    '.IsSP1AutomaticBehaviour = cboIsSP1AutomaticBehaviour.SelectedItem.Value.Trim
                    '.IsSP2AutomaticBehaviour = cboIsSP2AutomaticBehaviour.SelectedItem.Value.Trim
                    '.PenaltyPercentage = CDec(E_txtPenaltyPercentage.Text.Trim)
                    '.PenaltyPercentageBehaviour = E_cboPenaltyPercentageBehaviour.SelectedItem.Value.Trim
                    '.InsurancePenaltyPercentage = CDec(E_txtInsurancePenaltyPercentage.Text.Trim)
                    '.InsurancePenaltyPercentageBehaviour = E_cboInsurancePenaltyPercentageBehaviour.SelectedItem.Value.Trim
                    '.CancellationFee = CDec(E_txtCancellationFee.Text.Trim)
                    '.CancellationFeeBehaviour = E_cboCancellationFeeBehaviour.SelectedItem.Value.Trim
                    '.AdminFee = CDec(E_txtAdminFee.Text.Trim)
                    '.AdminFeeBehaviour = E_cboAdminFeeBehaviour.SelectedItem.Value.Trim
                    '.FiduciaFee = CDec(E_txtFiduciaFee.Text.Trim)
                    '.FiduciaFeeBehaviour = E_cboFiduciaFeeBehaviour.SelectedItem.Value.Trim
                    '.ProvisionFeeHO = CDec(E_txtProvisionFee.Text.Trim)
                    '.ProvisionFeeBehaviour = E_cboProvisionFeeBehaviour.SelectedItem.Value.Trim
                    '.NotaryFee = CDec(E_txtNotaryFee.Text.Trim)
                    '.NotaryFeeBehaviour = E_cboNotaryFeeBehaviour.SelectedItem.Value.Trim
                    '.SurveyFee = CDec(E_txtSurveyFee.Text.Trim)
                    '.SurveyFeeBehaviour = E_cboSurveyFeeBehaviour.SelectedItem.Value.Trim
                    '.VisitFee = CDec(E_txtVisitFee.Text.Trim)
                    '.VisitFeeBehaviour = E_cboVisitFeeBehaviour.SelectedItem.Value.Trim
                    '.ReschedulingFee = CDec(E_txtReschedulingFee.Text.Trim)
                    '.ReschedulingFeeBehaviour = E_cboReschedulingFeeBehaviour.SelectedItem.Value.Trim
                    '.AgreementTransferFee = CDec(E_txtAgreementTransferFee.Text.Trim)
                    '.AgreementTransferFeeBehaviour = E_cboAgreementTransferFeeBehaviour.SelectedItem.Value.Trim
                    '.ChangeDueDateFee = CDec(E_txtChangeDueDateFee.Text.Trim)
                    '.ChangeDueDateFeeBehaviour = E_cboChangeDueDateFee.SelectedItem.Value.Trim
                    '.AssetReplacementFee = CDec(E_txtAssetReplacementFee.Text.Trim)
                    '.AssetReplacementFeeBehaviour = E_cboAssetReplacementFeeBehaviour.SelectedItem.Value.Trim
                    '.RepossesFee = CDec(E_txtRepossesFee.Text.Trim)
                    '.RepossesFeeBehaviour = E_cboRepossesFeeBehaviour.SelectedItem.Value.Trim
                    '.LegalisirDocFee = CDec(E_LegalizedDocumentFee.Text.Trim)
                    '.LegalisirDocFeeBehaviour = E_cboLegalizedDocumentFeeBehaviour.SelectedItem.Value.Trim
                    '.PDCBounceFee = CDec(E_PDCBounceFee.Text.Trim)
                    '.PDCBounceFeeBehaviour = E_cboPDCBounceFeeBehaviour.SelectedItem.Value.Trim
                    '.InsAdminFee = CDec(E_txtInsuranceAdminFee.Text.Trim)
                    '.InsAdminFeeBehaviour = E_cboInsuranceAdminFeeBehaviour.SelectedItem.Value.Trim
                    '.InsStampDutyFee = CDec(E_txtInsuranceStampDutyFee.Text.Trim)
                    '.InsStampDutyFeeBehaviour = E_cboInsuranceStampDutyFeeBehaviour.SelectedItem.Value.Trim
                    '.BiayaPolis = CDec(E_txtBiayaPolis.Text.Trim)
                    '.BiayaPolisBehaviour = E_cboBiayaPolisBehaviour.SelectedItem.Value.Trim

                    '.DaysPOExpiration = CInt(E_txtPOExpirationDays.Text.Trim)
                    '.DaysPOExpirationBehaviour = E_cboPOExpirationDaysBehaviour.SelectedItem.Value.Trim
                    '.InstallmentToleranceAmount = CDec(E_txtInstallmentToleranceAmount.Text.Trim)
                    '.InstallmentToleranceAmountBehaviour = E_cboInstallmentToleranceAmountBehaviour.SelectedItem.Value.Trim
                    '.DPPercentage = CDec(E_txtDPPercentage.Text.Trim)
                    '.DPPercentageBehaviour = E_cboDPPercentageBehaviour.SelectedItem.Value.Trim
                    '.RejectMinimumIncome = CDec(E_txtRejectMinimumIncome.Text.Trim)
                    '.RejectMinimumIncomeBehaviour = E_cboRejectMinimumIncomeBehaviour.SelectedItem.Value.Trim
                    '.WarningMinimumIncome = CDec(E_txtWarningMinimumIncome.Text.Trim)
                    '.WarningMinimumIncomeBehaviour = E_cboWarningMinimumIncomeBehaviour.SelectedItem.Value.Trim

                    '.IsSPAutomatic = CBool(E_cboIsSPAutomatic.SelectedItem.Value.Trim)
                    '.LengthSPProcess = CInt(E_txtLengthSPProcess.Text.Trim)

                    '.IsSP1Automatic = CBool(E_cboIsSP1Automatic.SelectedItem.Value.Trim)
                    '.LengthSP1Process = CInt(E_txtLengthSP1Process.Text.Trim)

                    '.IsSP2Automatic = CBool(E_cboIsSP2Automatic.SelectedItem.Value.Trim)
                    '.LengthSP2Process = CInt(E_txtLengthSP2Process.Text.Trim)

                    '.LengthMainDocProcess = CInt(E_txtLengthMainDocProcess.Text.Trim)
                    '.LengthMainDocProcessBehaviour = E_cboLengthMainDocProcessBehaviour.SelectedItem.Value.Trim
                    '.LengthMainDocTaken = CInt(E_txtLengthMainDocTaken.Text.Trim)
                    '.LengthMainDocTakenBehaviour = E_cboLengthMainDocTakenBehaviour.SelectedItem.Value.Trim

                    '.GracePeriodLateCharges = CInt(E_txtGracePeriodLateCharges.Text.Trim)
                    '.GracePeriodLateChargesBehaviour = E_cboGracePeriodLateChargesBehaviour.SelectedItem.Value.Trim
                    '.PrepaymentPenaltyRate = CDec(E_txtPrepaymentPenaltyRate.Text.Trim)
                    '.PrepaymentPenaltyRateBehaviour = E_cboPrepaymentPenaltyRateBehaviour.SelectedItem.Value.Trim
                    '.DeskCollPhoneRemind = CInt(E_txtDeskCollPhoneRemind.Text.Trim)
                    '.DeskCollPhoneRemindBehaviour = E_cboDeskCollPhoneRemindBehaviour.SelectedItem.Value.Trim
                    '.DeskCollOD = CInt(E_txtDeskCollOD.Text.Trim)
                    '.DeskCollODBehaviour = E_cboDeskCollODBehaviour.SelectedItem.Value.Trim
                    '.PrevODToRemind = CInt(E_txtPrevODToRemind.Text.Trim)
                    '.PrevODToRemindBehaviour = E_cboPrevODToRemindBehaviour.SelectedItem.Value.Trim
                    '.PDCDayToRemind = CInt(E_txtPDCDaysToRemind.Text.Trim)
                    '.PDCDayToRemindBehaviour = E_cboPDCDaysToRemindBehaviour.SelectedItem.Value.Trim
                    '.DeskCollSMSRemind = CInt(E_txtDeskCollSMSRemind.Text.Trim)
                    '.DeskCollSMSRemindBehaviour = E_cboDeskCollSMSRemindBehaviour.SelectedItem.Value.Trim

                    '.DCR = CInt(E_txtDCR.Text.Trim)
                    '.DCRBehaviour = E_cboDCRBehaviour.SelectedItem.Value.Trim
                    '.DaysBeforeDueToRN = CInt(E_txtDaysBeforeDueToRN.Text.Trim)
                    '.DaysBeforeDueToRNBehaviour = E_cboDaysBeforeDueToRNBehaviour.SelectedItem.Value.Trim
                    '.ODToRAL = CInt(E_txtODToRAL.Text.Trim)
                    '.ODToRALBehaviour = E_cboODToRALBehaviour.SelectedItem.Value.Trim
                    '.RALPeriod = CInt(E_txtRALPeriod.Text.Trim)
                    '.RALPeriodBehaviour = E_cboRALPeriodBehaviour.SelectedItem.Value.Trim
                    '.MaxDaysRNPaid = CInt(E_txtMaxDaysRNPaid.Text.Trim)
                    '.MaxDaysRNPaidBehaviour = E_cboMaxDaysRNPaidBehaviour.SelectedItem.Value.Trim
                    '.MaxPTPYDays = CInt(E_txtMaxPTPYDays.Text.Trim)
                    '.MaxPTPYDaysBehaviour = E_cboMaxPTPYDaysBehaviour.SelectedItem.Value.Trim
                    '.PTPYBank = CInt(E_txtPTPYBank.Text.Trim)
                    '.PTPYBankBehaviour = E_txtPTPYBankBehaviour.SelectedItem.Value.Trim
                    '.PTPYCompany = CInt(E_txtPTPYCompany.Text.Trim)
                    '.PTPYCompanyBehaviour = E_cboPTPYCompanyBehaviour.SelectedItem.Value.Trim
                    '.PTPYSupplier = CInt(E_txtPTPYSupplier.Text.Trim)
                    '.PTPYSupplierBehaviour = E_cboPTPYSupplierBehaviour.SelectedItem.Value.Trim
                    '.RALExtension = CInt(E_txtRALExtension.Text.Trim)
                    '.RALExtensionBehaviour = E_cboRALExtensionBehaviour.SelectedItem.Value.Trim
                    '.InventoryExpected = CInt(E_txtInventoryExpected.Text.Trim)
                    '.InventoryExpectedBehaviour = E_cboInventoryExpectedBehaviour.SelectedItem.Value.Trim
                    '.BillingCharges = CDec(E_txtBillingCharges.Text.Trim)
                    '.BillingChargesBehaviour = E_cboBillingChargesBehaviour.SelectedItem.Value.Trim

                    '.LimitAPCash = CInt(E_txtLimitAPCash.Text.Trim)
                    '.LimitAPCashBehaviour = E_cboLimitAPCashBehaviour.SelectedItem.Value.Trim
                    .strConnection = GetConnectionString()
                End With


                SkemaTab.CollectResult(oProduct)
                BiayaTab.CollectResult(oProduct)
                AngsuranTab.CollectResult(oProduct)
                CollactionTab.CollectResult(oProduct)
                FinaceTab.CollectResult(oProduct)


                Try
                    m_controller.ProductBranchSaveEdit(oProduct)
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
                    pnlEdit.Visible = False
                    pnlList.Visible = True

                    Me.CmdWhere = "a.BranchID='" & Me.sesBranchId.Replace("'", "") & "'"
                    BindGridEntity(Me.CmdWhere)

                Catch exp As Exception
                    ShowMessage(lblMessage, exp.Message, True)
                End Try
            End If
            txtPage.Text = "1"
        End If
    End Sub
#End Region

#Region "Sorting"
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub
#End Region

#Region "imbReset"
    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        cboSearchBy.SelectedIndex = 0
        txtSearchBy.Text = ""
        txtPage.Text = "1"

        Me.CmdWhere = "a.BranchID='" & Me.sesBranchId.Replace("'", "") & "'"
        BindGridEntity(Me.CmdWhere)
    End Sub
#End Region

#Region "imbSearch"
    Private Sub imbSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonSearch.Click
        If txtSearchBy.Text.Trim = "" Then
            Me.CmdWhere = "a.BranchID='" & Me.sesBranchId.Replace("'", "") & "'"
        Else
            Me.CmdWhere = "a.BranchID= '" & Me.sesBranchId.Replace("'", "") & "' and a." & cboSearchBy.SelectedItem.Value & " = '" & txtSearchBy.Text.Trim & "'"
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub
#End Region

#Region "imbPrint"
    Private Sub imbPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonPrint.Click
        If CheckFeature(Me.Loginid, Me.FormID, "PRINT", Me.AppId) Then
            SendCookies()
            Response.Redirect("Report/ProductBranchRpt.aspx")
        End If
    End Sub
#End Region

#Region "SendCookies"
    Sub SendCookies()
        Dim cookie As HttpCookie = Request.Cookies("ProductBranch")
        If txtSearchBy.Text.Trim = "" Then
            Me.CmdWhere = "a.BranchID='" & Me.BranchID & "'"
        Else
            Me.CmdWhere = "a.BranchID= '" & Me.BranchID & "' and a." & cboSearchBy.SelectedItem.Value & " = '" & txtSearchBy.Text.Trim & "'"
        End If

        If Not cookie Is Nothing Then
            cookie.Values("PageFrom") = "ProductBranch"
            cookie.Values("cmdwhere") = Me.CmdWhere
            cookie.Values("BranchID") = Me.BranchID
            'cookie.Values("IsActive") = Me.IsActive.ToString
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie("ProductBranch")
            cookieNew.Values.Add("PageFrom", "ProductBranch")
            cookieNew.Values.Add("cmdwhere", Me.CmdWhere)
            cookieNew.Values.Add("BranchID", Me.BranchID)
            'cookieNew.Values.Add("IsActive", Me.IsActive.ToString)
            Response.AppendCookie(cookieNew)
        End If
    End Sub
#End Region
    

End Class