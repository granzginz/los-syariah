﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
#End Region

Public Class ProductBranchView
    Inherits Maxiloan.Webform.WebBased
    Private m_controller As New ProductController

#Region "Property"
    Private Property ProductID() As String
        Get
            Return CType(viewstate("ProductID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ProductID") = Value
        End Set
    End Property

    Private Property Branch_ID() As String
        Get
            Return CType(viewstate("Branch_ID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Branch_ID") = Value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not IsPostBack Then
            Me.ProductID = Request("ProductID")
            Me.Branch_ID = Request("BranchID")
            Bind()
        End If        
    End Sub

#Region "Behaviour_Value"
    Function Behaviour_Value(ByVal Behaviour As String) As String
        If Behaviour = "D" Then
            Return "Default"
        ElseIf Behaviour = "L" Then
            Return "Locked"
        ElseIf Behaviour = "N" Then
            Return "Minimum"
        ElseIf Behaviour = "X" Then
            Return "Maximum"
        End If
    End Function
#End Region

#Region "Bind"
    Sub Bind()
        Dim oProduct As New Parameter.Product

        oProduct.ProductId = Me.ProductID
        oProduct.BranchId = Me.BranchID
        oProduct.strConnection = GetConnectionString

        oProduct = m_controller.ProductBranchEdit(oProduct)


        UmumTab.Product = oProduct
        UmumTab.UCMode = "VIEW"
        UmumTab.UCProductName = "PRODUK CABANG"
        UmumTab.InitUCViewMode()


        SkemaTab.Product = oProduct
        SkemaTab.UCMode = "VIEW"
        SkemaTab.InitUCViewMode()

        'lblProductID.Text = Me.ProductID
        'lblDescription.Text = oProduct.Description
        'lblAssetType.Text = oProduct.AssetType_desc
        'lblScoreSchemeId.Text = oProduct.ScoreSchemeMaster_desc
        'lblCreditScoreSchemeId.Text = oProduct.CreditScoreSchemeMaster_desc
        'lblJournalSchemeId.Text = oProduct.JournalScheme_desc
        'lblApprovalSchemeId.Text = oProduct.ApprovalTypeScheme_desc

        'If oProduct.AssetUsedNew = "N" Then
        '    lblAssetUsedNew.Text = "New"
        'Else
        '    lblAssetUsedNew.Text = "Used"
        'End If

        'If oProduct.FinanceType = "CF" Then
        '    lblFinanceType.Text = "Consumer Finance"
        'Else
        '    lblFinanceType.Text = "Leasing"
        'End If
        'lblUmurKendaraanFrom.Text = oProduct.UmurKendaraanFrom.ToString
        'lblUmurKendaraanTo.Text = oProduct.UmurKendaraanTo.ToString
        'lblEffectiveRate.Text = FormatNumber(CStr(oProduct.EffectiveRate).Trim, 2)

        'Dim EffectiveRateBehaviour As String
        'If oProduct.EffectiveRateBehaviour = "D" Then
        '    EffectiveRateBehaviour = "Default"
        'ElseIf oProduct.EffectiveRateBehaviour = "L" Then
        '    EffectiveRateBehaviour = "Locked"
        'ElseIf oProduct.EffectiveRateBehaviour = "N" Then
        '    EffectiveRateBehaviour = "Minimum"
        'ElseIf oProduct.EffectiveRateBehaviour = "X" Then
        '    EffectiveRateBehaviour = "Maximum"
        'End If

        'lblEffectiveRateBehaviour.Text = EffectiveRateBehaviour
        'lblGrossYieldRate.Text = FormatNumber(CStr(oProduct.GrossYieldRate).Trim, 2)

        'Dim GrossYieldRateBehaviour As String
        'If oProduct.GrossYieldRateBehaviour = "N" Then
        '    GrossYieldRateBehaviour = "Minimum"
        'End If
        'lblGrossYieldRateBehaviour.Text = GrossYieldRateBehaviour
        'lblMinimumTenor.Text = CStr(oProduct.MinimumTenor)
        'lblMaximumTenor.Text = CStr(oProduct.MaximumTenor)

        If oProduct.PenaltyBasedOn = "PB" Then
            lblPenaltyBasedOn.Text = "Pokok + Bunga"
        ElseIf oProduct.PenaltyBasedOn = "PO" Then
            lblPenaltyBasedOn.Text = "Pokok"
        End If

        If oProduct.PenaltyBasedOnBehaviour = "D" Then
            lblPenaltyBasedOnBehaviour.Text = "Default"
        ElseIf oProduct.PenaltyBasedOnBehaviour = "L" Then
            lblPenaltyBasedOnBehaviour.Text = "Locked"
        ElseIf oProduct.PenaltyBasedOnBehaviour = "N" Then
            lblPenaltyBasedOnBehaviour.Text = "Minimum"
        ElseIf oProduct.PenaltyBasedOnBehaviour = "X" Then
            lblPenaltyBasedOnBehaviour.Text = "Maximum"
        End If

        lblPenaltyRatePrevious.Text = oProduct.PenaltyRatePrevious.ToString
        lblPenaltyRateEffectiveDate.Text = oProduct.PenaltyRateEffectiveDate.ToString("dd/MM/yyyy")


        lblPrepaymentPenaltyFixed.Text = oProduct.PrepaymentPenaltyFixed.ToString
        If oProduct.PrepaymentPenaltyFixedBehaviour = "D" Then
            lblPrepaymentPenaltyFixedBehaviour.Text = "Default"
        ElseIf oProduct.PrepaymentPenaltyFixedBehaviour = "L" Then
            lblPrepaymentPenaltyFixedBehaviour.Text = "Locked"
        ElseIf oProduct.PrepaymentPenaltyFixedBehaviour = "N" Then
            lblPrepaymentPenaltyFixedBehaviour.Text = "Minimum"
        ElseIf oProduct.PrepaymentPenaltyFixedBehaviour = "X" Then
            lblPrepaymentPenaltyFixedBehaviour.Text = "Maximum"
        End If

        lblPrepaymentPenaltyPrevious.Text = oProduct.PrepaymentPenaltyPrevious.ToString
        lblPrepaymentPenaltyEffectiveDate.Text = oProduct.PrepaymentPenaltyEffectiveDate.ToString("dd/MM/yyyy")
        lblPrepaymentPenaltyFixedEffectiveDate.Text = oProduct.PrepaymentPenaltyFixedEffectiveDate.ToString("dd/MM/yyyy")
        lblPrepaymentPenaltyFixedPrevious.Text = oProduct.PrepaymentPenaltyFixedPrevious.ToString

        If oProduct.PrioritasPembayaran = "DE" Then
            lblPrioritasPembayaran.Text = "Denda"
        ElseIf oProduct.PrioritasPembayaran = "BE" Then
            lblPrioritasPembayaran.Text = "Bebas"
        End If

        lblInsuranceDiscPercentage.Text = oProduct.InsuranceDiscPercentage.ToString
        If oProduct.InsuranceDiscPercentageBehaviour = "D" Then
            lblInsuranceDiscPercentageBehaviour.Text = "Default"
        ElseIf oProduct.InsuranceDiscPercentageBehaviour = "L" Then
            lblInsuranceDiscPercentageBehaviour.Text = "Locked"
        ElseIf oProduct.InsuranceDiscPercentageBehaviour = "N" Then
            lblInsuranceDiscPercentageBehaviour.Text = "Minimum"
        ElseIf oProduct.InsuranceDiscPercentageBehaviour = "X" Then
            lblInsuranceDiscPercentageBehaviour.Text = "Maximum"
        End If

        lblIncomeInsRatioPercentage.Text = oProduct.IncomeInsRatioPercentage.ToString
        If oProduct.PenaltyBasedOnBehaviour = "D" Then
            lblIncomeInsRatioPercentageBehaviour.Text = "Default"
        ElseIf oProduct.PenaltyBasedOnBehaviour = "L" Then
            lblIncomeInsRatioPercentageBehaviour.Text = "Locked"
        ElseIf oProduct.PenaltyBasedOnBehaviour = "N" Then
            lblIncomeInsRatioPercentageBehaviour.Text = "Minimum"
        ElseIf oProduct.PenaltyBasedOnBehaviour = "X" Then
            lblIncomeInsRatioPercentageBehaviour.Text = "Maximum"
        End If
        '---------------------------

       
        lblLCP_Installment.Text = FormatNumber(CStr(oProduct.PenaltyPercentage).Trim, 2)

        Dim PenaltyPercentageBehaviour As String
        If oProduct.PenaltyPercentageBehaviour = "D" Then
            PenaltyPercentageBehaviour = "Default"
        ElseIf oProduct.PenaltyPercentageBehaviour = "L" Then
            PenaltyPercentageBehaviour = "Locked"
        ElseIf oProduct.PenaltyPercentageBehaviour = "N" Then
            PenaltyPercentageBehaviour = "Minimum"
        ElseIf oProduct.PenaltyPercentageBehaviour = "X" Then
            PenaltyPercentageBehaviour = "Maximum"
        End If
        lblLCP_InstallmentBehaviour.Text = PenaltyPercentageBehaviour
        lblLCP_Insurance.Text = FormatNumber(CStr(oProduct.InsurancePenaltyPercentage).Trim, 2)

        Dim InsurancePenaltyPercentageBehaviour As String
        If oProduct.InsurancePenaltyPercentageBehaviour = "D" Then
            InsurancePenaltyPercentageBehaviour = "Default"
        ElseIf oProduct.InsurancePenaltyPercentageBehaviour = "L" Then
            InsurancePenaltyPercentageBehaviour = "Locked"
        ElseIf oProduct.InsurancePenaltyPercentageBehaviour = "N" Then
            InsurancePenaltyPercentageBehaviour = "Minimum"
        ElseIf oProduct.InsurancePenaltyPercentageBehaviour = "X" Then
            InsurancePenaltyPercentageBehaviour = "Maximum"
        End If
        lblLCP_InsuranceBehaviour.Text = InsurancePenaltyPercentageBehaviour
        lblCancellationFee.Text = FormatNumber(CStr(oProduct.CancellationFee).Trim, 2)

        Dim CancellationFeeBehaviour As String
        If oProduct.CancellationFeeBehaviour = "D" Then
            CancellationFeeBehaviour = "Default"
        ElseIf oProduct.CancellationFeeBehaviour = "L" Then
            CancellationFeeBehaviour = "Locked"
        ElseIf oProduct.CancellationFeeBehaviour = "N" Then
            CancellationFeeBehaviour = "Minimum"
        ElseIf oProduct.CancellationFeeBehaviour = "X" Then
            CancellationFeeBehaviour = "Maximum"
        End If
        lblCancellationFeeBehaviour.Text = CancellationFeeBehaviour
        lblAdminFee.Text = FormatNumber(CStr(oProduct.AdminFee).Trim, 2)

        Dim AdminFeeBehaviour As String
        If oProduct.AdminFeeBehaviour = "D" Then
            AdminFeeBehaviour = "Default"
        ElseIf oProduct.AdminFeeBehaviour = "L" Then
            AdminFeeBehaviour = "Locked"
        ElseIf oProduct.AdminFeeBehaviour = "N" Then
            AdminFeeBehaviour = "Minimum"
        ElseIf oProduct.AdminFeeBehaviour = "X" Then
            AdminFeeBehaviour = "Maximum"
        End If
        lblAdminFeeBehaviour.Text = AdminFeeBehaviour
        lblFiduciaFee.Text = FormatNumber(CStr(oProduct.FiduciaFee).Trim, 2)

        Dim FiduciaFeeBehaviour As String
        If oProduct.FiduciaFeeBehaviour = "D" Then
            FiduciaFeeBehaviour = "Default"
        ElseIf oProduct.FiduciaFeeBehaviour = "L" Then
            FiduciaFeeBehaviour = "Locked"
        ElseIf oProduct.FiduciaFeeBehaviour = "N" Then
            FiduciaFeeBehaviour = "Minimum"
        ElseIf oProduct.FiduciaFeeBehaviour = "X" Then
            FiduciaFeeBehaviour = "Maximum"
        End If
        lblFiduciaFeeBehaviour.Text = FiduciaFeeBehaviour
        lblProvisionFee.Text = FormatNumber(CStr(oProduct.ProvisionFee).Trim, 2)

        Dim ProvisionFeeBehaviour As String
        If oProduct.ProvisionFeeBehaviour = "D" Then
            ProvisionFeeBehaviour = "Default"
        ElseIf oProduct.ProvisionFeeBehaviour = "L" Then
            ProvisionFeeBehaviour = "Locked"
        ElseIf oProduct.ProvisionFeeBehaviour = "N" Then
            ProvisionFeeBehaviour = "Minimum"
        ElseIf oProduct.ProvisionFeeBehaviour = "X" Then
            ProvisionFeeBehaviour = "Maximum"
        End If
        lblProvisionFeeBehaviour.Text = ProvisionFeeBehaviour
        lblNotaryFee.Text = FormatNumber(CStr(oProduct.NotaryFee).Trim, 2)

        Dim NotaryFeeBehaviour As String
        If oProduct.NotaryFeeBehaviour = "D" Then
            NotaryFeeBehaviour = "Default"
        ElseIf oProduct.NotaryFeeBehaviour = "L" Then
            NotaryFeeBehaviour = "Locked"
        ElseIf oProduct.NotaryFeeBehaviour = "N" Then
            NotaryFeeBehaviour = "Minimum"
        ElseIf oProduct.NotaryFeeBehaviour = "X" Then
            NotaryFeeBehaviour = "Maximum"
        End If
        lblNotaryFeeBehaviour.Text = NotaryFeeBehaviour
        lblSurveyFee.Text = FormatNumber(CStr(oProduct.SurveyFee).Trim, 2)

        Dim SurveyFeeBehaviour As String
        If oProduct.SurveyFeeBehaviour = "D" Then
            SurveyFeeBehaviour = "Default"
        ElseIf oProduct.SurveyFeeBehaviour = "L" Then
            SurveyFeeBehaviour = "Locked"
        ElseIf oProduct.SurveyFeeBehaviour = "N" Then
            SurveyFeeBehaviour = "Minimum"
        ElseIf oProduct.SurveyFeeBehaviour = "X" Then
            SurveyFeeBehaviour = "Maximum"
        End If
        lblSurveyFeeBehaviour.Text = SurveyFeeBehaviour
        lblVisitFee.Text = FormatNumber(CStr(oProduct.VisitFee).Trim, 2)

        Dim VisitFeeBehaviour As String
        If oProduct.VisitFeeBehaviour = "D" Then
            VisitFeeBehaviour = "Default"
        ElseIf oProduct.VisitFeeBehaviour = "L" Then
            VisitFeeBehaviour = "Locked"
        ElseIf oProduct.VisitFeeBehaviour = "N" Then
            VisitFeeBehaviour = "Minimum"
        ElseIf oProduct.VisitFeeBehaviour = "X" Then
            VisitFeeBehaviour = "Maximum"
        End If
        lblVisitFeeBehaviour.Text = VisitFeeBehaviour
        lblReschedulingFee.Text = FormatNumber(CStr(oProduct.ReschedulingFee).Trim, 2)

        Dim ReschedulingFeeBehaviour As String
        If oProduct.ReschedulingFeeBehaviour = "D" Then
            ReschedulingFeeBehaviour = "Default"
        ElseIf oProduct.ReschedulingFeeBehaviour = "L" Then
            ReschedulingFeeBehaviour = "Locked"
        ElseIf oProduct.ReschedulingFeeBehaviour = "N" Then
            ReschedulingFeeBehaviour = "Minimum"
        ElseIf oProduct.ReschedulingFeeBehaviour = "X" Then
            ReschedulingFeeBehaviour = "Maximum"
        End If

        lblReschedulingFeeBehaviour.Text = ReschedulingFeeBehaviour
        lblATFee.Text = FormatNumber(CStr(oProduct.AgreementTransferFee).Trim, 2)

        Dim AgreementTransferFeeBehaviour As String
        If oProduct.AgreementTransferFeeBehaviour = "D" Then
            AgreementTransferFeeBehaviour = "Default"
        ElseIf oProduct.AgreementTransferFeeBehaviour = "L" Then
            AgreementTransferFeeBehaviour = "Locked"
        ElseIf oProduct.AgreementTransferFeeBehaviour = "N" Then
            AgreementTransferFeeBehaviour = "Minimum"
        ElseIf oProduct.AgreementTransferFeeBehaviour = "X" Then
            AgreementTransferFeeBehaviour = "Maximum"
        End If
        lblATFeeBehaviour.Text = AgreementTransferFeeBehaviour
        lblCDDRFee.Text = FormatNumber(CStr(oProduct.ChangeDueDateFeeBehaviour).Trim, 2)

        Dim ChangeDueDateFeeBehaviour As String
        If oProduct.ChangeDueDateFeeBehaviour = "D" Then
            ChangeDueDateFeeBehaviour = "Default"
        ElseIf oProduct.ChangeDueDateFeeBehaviour = "L" Then
            ChangeDueDateFeeBehaviour = "Locked"
        ElseIf oProduct.ChangeDueDateFeeBehaviour = "N" Then
            ChangeDueDateFeeBehaviour = "Minimum"
        ElseIf oProduct.ChangeDueDateFeeBehaviour = "X" Then
            ChangeDueDateFeeBehaviour = "Maximum"
        End If
        lblCDDRFeeBehaviour.Text = ChangeDueDateFeeBehaviour
        lblAssetReplacementFee.Text = FormatNumber(CStr(oProduct.AssetReplacementFee).Trim, 2)

        Dim AssetReplacementFeeBehaviour As String
        If oProduct.AssetReplacementFeeBehaviour = "D" Then
            AssetReplacementFeeBehaviour = "Default"
        ElseIf oProduct.AssetReplacementFeeBehaviour = "L" Then
            AssetReplacementFeeBehaviour = "Locked"
        ElseIf oProduct.AssetReplacementFeeBehaviour = "N" Then
            AssetReplacementFeeBehaviour = "Minimum"
        ElseIf oProduct.AssetReplacementFeeBehaviour = "X" Then
            AssetReplacementFeeBehaviour = "Maximum"
        End If
        lblAssetReplacementFeeBehaviour.Text = AssetReplacementFeeBehaviour
        lblRepossesFee.Text = FormatNumber(CStr(oProduct.RepossesFee).Trim, 2)

        Dim RepossesFeeBehaviour As String
        If oProduct.RepossesFeeBehaviour = "D" Then
            RepossesFeeBehaviour = "Default"
        ElseIf oProduct.RepossesFeeBehaviour = "L" Then
            RepossesFeeBehaviour = "Locked"
        ElseIf oProduct.RepossesFeeBehaviour = "N" Then
            RepossesFeeBehaviour = "Minimum"
        ElseIf oProduct.RepossesFeeBehaviour = "X" Then
            RepossesFeeBehaviour = "Maximum"
        End If
        lblRepossesFeeBehaviour.Text = RepossesFeeBehaviour
        lblLDFee.Text = FormatNumber(CStr(oProduct.LegalisirDocFee).Trim, 2)

        Dim LegalisirDocFeeBehaviour As String
        If oProduct.LegalisirDocFeeBehaviour = "D" Then
            LegalisirDocFeeBehaviour = "Default"
        ElseIf oProduct.LegalisirDocFeeBehaviour = "L" Then
            LegalisirDocFeeBehaviour = "Locked"
        ElseIf oProduct.LegalisirDocFeeBehaviour = "N" Then
            LegalisirDocFeeBehaviour = "Minimum"
        ElseIf oProduct.LegalisirDocFeeBehaviour = "X" Then
            LegalisirDocFeeBehaviour = "Maximum"
        End If
        lblLDFeeBehaviour.Text = LegalisirDocFeeBehaviour
        lblPDCBounceFee.Text = FormatNumber(CStr(oProduct.PDCBounceFee).Trim, 2)

        Dim PDCBounceFeeBehaviour As String
        If oProduct.PDCBounceFeeBehaviour = "D" Then
            PDCBounceFeeBehaviour = "Default"
        ElseIf oProduct.PDCBounceFeeBehaviour = "L" Then
            PDCBounceFeeBehaviour = "Locked"
        ElseIf oProduct.PDCBounceFeeBehaviour = "N" Then
            PDCBounceFeeBehaviour = "Minimum"
        ElseIf oProduct.PDCBounceFeeBehaviour = "X" Then
            PDCBounceFeeBehaviour = "Maximum"
        End If
        lblPDCBounceFeeBehaviour.Text = PDCBounceFeeBehaviour
        lblInsuranceAdminFee.Text = FormatNumber(CStr(oProduct.InsAdminFee).Trim, 2)

        Dim InsAdminFeeBehaviour As String
        If oProduct.InsAdminFeeBehaviour = "D" Then
            InsAdminFeeBehaviour = "Default"
        ElseIf oProduct.InsAdminFeeBehaviour = "L" Then
            InsAdminFeeBehaviour = "Locked"
        ElseIf oProduct.InsAdminFeeBehaviour = "N" Then
            InsAdminFeeBehaviour = "Minimum"
        ElseIf oProduct.InsAdminFeeBehaviour = "X" Then
            InsAdminFeeBehaviour = "Maximum"
        End If
        lblInsuranceAdminFeeBehaviour.Text = InsAdminFeeBehaviour

        lblISDFee.Text = FormatNumber(CStr(oProduct.InsStampDutyFee).Trim, 2)
        Dim InsStampDutyFeeBehaviour As String
        If oProduct.InsStampDutyFeeBehaviour = "D" Then
            InsStampDutyFeeBehaviour = "Default"
        ElseIf oProduct.InsStampDutyFeeBehaviour = "L" Then
            InsStampDutyFeeBehaviour = "Locked"
        ElseIf oProduct.InsStampDutyFeeBehaviour = "N" Then
            InsStampDutyFeeBehaviour = "Minimum"
        ElseIf oProduct.InsStampDutyFeeBehaviour = "X" Then
            InsStampDutyFeeBehaviour = "Maximum"
        End If
        lblISDFeeBehaviour.Text = InsStampDutyFeeBehaviour

        lblBiayaPolis.Text = FormatNumber(CStr(oProduct.BiayaPolis).Trim, 2)
        Dim BiayaPolisBehaviour As String
        If oProduct.BiayaPolisBehaviour = "D" Then
            BiayaPolisBehaviour = "Default"
        ElseIf oProduct.BiayaPolisBehaviour = "L" Then
            BiayaPolisBehaviour = "Locked"
        ElseIf oProduct.BiayaPolisBehaviour = "N" Then
            BiayaPolisBehaviour = "Minimum"
        ElseIf oProduct.BiayaPolisBehaviour = "X" Then
            BiayaPolisBehaviour = "Maximum"
        End If
        lblBiayaPolisBehaviour.Text = BiayaPolisBehaviour

        lblPOExpirationDays.Text = CStr(oProduct.DaysPOExpiration)

        Dim DaysPOExpirationBehaviour As String
        If oProduct.DaysPOExpirationBehaviour = "D" Then
            DaysPOExpirationBehaviour = "Default"
        ElseIf oProduct.DaysPOExpirationBehaviour = "L" Then
            DaysPOExpirationBehaviour = "Locked"
        ElseIf oProduct.DaysPOExpirationBehaviour = "N" Then
            DaysPOExpirationBehaviour = "Minimum"
        ElseIf oProduct.DaysPOExpirationBehaviour = "X" Then
            DaysPOExpirationBehaviour = "Maximum"
        End If

        lblPOExpirationDaysBehaviour.Text = DaysPOExpirationBehaviour
        lblInsToleranceAmount.Text = FormatNumber(CStr(oProduct.InstallmentToleranceAmount).Trim, 2)

        Dim InstallmentToleranceAmountBehaviour As String
        If oProduct.InstallmentToleranceAmountBehaviour = "D" Then
            InstallmentToleranceAmountBehaviour = "Default"
        ElseIf oProduct.InstallmentToleranceAmountBehaviour = "L" Then
            InstallmentToleranceAmountBehaviour = "Locked"
        ElseIf oProduct.InstallmentToleranceAmountBehaviour = "N" Then
            InstallmentToleranceAmountBehaviour = "Minimum"
        ElseIf oProduct.InstallmentToleranceAmountBehaviour = "X" Then
            InstallmentToleranceAmountBehaviour = "Maximum"
        End If
        lblInsToleranceAmountBehaviour.Text = InstallmentToleranceAmountBehaviour

        lblMinimumDPPerc.Text = FormatNumber(CStr(oProduct.DPPercentage).Trim, 2)

        Dim DPPercentageBehaviour As String
        If oProduct.DPPercentageBehaviour = "D" Then
            DPPercentageBehaviour = "Default"
        ElseIf oProduct.DPPercentageBehaviour = "L" Then
            DPPercentageBehaviour = "Locked"
        ElseIf oProduct.DPPercentageBehaviour = "N" Then
            DPPercentageBehaviour = "Minimum"
        ElseIf oProduct.DPPercentageBehaviour = "X" Then
            DPPercentageBehaviour = "Maximum"
        End If
        lblMinimumDPPercBehaviour.Text = DPPercentageBehaviour

        lblMIITBR.Text = FormatNumber(CStr(oProduct.RejectMinimumIncome).Trim, 2)

        'Dim RejectMinimumIncomeBehaviour As String
        'If oProduct.RejectMinimumIncomeBehaviour = "N" Then
        '    RejectMinimumIncomeBehaviour = "Minimum"
        'End If
        'lblMIITBRBehaviour.Text = RejectMinimumIncomeBehaviour

        'lblMIITBW.Text = FormatNumber(CStr(oProduct.WarningMinimumIncome).Trim, 2)


        Dim RejectMinimumIncomeBehaviour As String
        If oProduct.RejectMinimumIncomeBehaviour = "D" Then
            RejectMinimumIncomeBehaviour = "Default"
        ElseIf oProduct.RejectMinimumIncomeBehaviour = "L" Then
            RejectMinimumIncomeBehaviour = "Locked"
        ElseIf oProduct.RejectMinimumIncomeBehaviour = "N" Then
            RejectMinimumIncomeBehaviour = "Minimum"
        ElseIf oProduct.RejectMinimumIncomeBehaviour = "X" Then
            RejectMinimumIncomeBehaviour = "Maximum"
        End If
        lblMIITBRBehaviour.Text = RejectMinimumIncomeBehaviour

        lblMIITBW.Text = FormatNumber(CStr(oProduct.WarningMinimumIncome).Trim, 2)


        'Dim WarningMinimumIncomeBehaviour As String
        'If oProduct.WarningMinimumIncomeBehaviour = "N" Then
        '    WarningMinimumIncomeBehaviour = "Minimum"
        'End If
        'lblMIITBWBehaviour.Text = WarningMinimumIncomeBehaviour

        'If CStr(oProduct.IsSPAutomatic) = "True" Then
        '    lblIsSPAutomatic.Text = "Yes"
        'Else
        '    lblIsSPAutomatic.Text = "No"
        'End If


        Dim WarningMinimumIncomeBehaviour As String
        If oProduct.WarningMinimumIncomeBehaviour = "D" Then
            WarningMinimumIncomeBehaviour = "Default"
        ElseIf oProduct.WarningMinimumIncomeBehaviour = "L" Then
            WarningMinimumIncomeBehaviour = "Locked"
        ElseIf oProduct.WarningMinimumIncomeBehaviour = "N" Then
            WarningMinimumIncomeBehaviour = "Minimum"
        ElseIf oProduct.WarningMinimumIncomeBehaviour = "X" Then
            WarningMinimumIncomeBehaviour = "Maximum"
        End If
        lblMIITBWBehaviour.Text = WarningMinimumIncomeBehaviour

        If CStr(oProduct.IsSPAutomatic) = "True" Then
            lblIsSPAutomatic.Text = "Yes"
        Else
            lblIsSPAutomatic.Text = "No"
        End If

        Dim IsSPAutomaticBehaviour As String
        If oProduct.IsSPAutomaticBehaviour = "D" Then
            IsSPAutomaticBehaviour = "Default"
        ElseIf oProduct.IsSPAutomaticBehaviour = "L" Then
            IsSPAutomaticBehaviour = "Locked"
        ElseIf oProduct.IsSPAutomaticBehaviour = "N" Then
            IsSPAutomaticBehaviour = "Minimum"
        ElseIf oProduct.IsSPAutomaticBehaviour = "X" Then
            IsSPAutomaticBehaviour = "Maximum"
        End If
        lblIsSPAutomaticBehaviour.Text = IsSPAutomaticBehaviour
        lblLengthSPProcess.Text = CStr(oProduct.LengthSPProcess)

        If CStr(oProduct.IsSP1Automatic) = "True" Then
            lblIsSP1Automatic.Text = "Yes"
        Else
            lblIsSP1Automatic.Text = "No"
        End If


        Dim IsSP1AutomaticBehaviour As String
        If oProduct.IsSP1AutomaticBehaviour = "D" Then
            IsSP1AutomaticBehaviour = "Default"
        ElseIf oProduct.IsSP1AutomaticBehaviour = "L" Then
            IsSP1AutomaticBehaviour = "Locked"
        ElseIf oProduct.IsSP1AutomaticBehaviour = "N" Then
            IsSP1AutomaticBehaviour = "Minimum"
        ElseIf oProduct.IsSP1AutomaticBehaviour = "X" Then
            IsSP1AutomaticBehaviour = "Maximum"
        End If
        lblIsSP1AutomaticBehaviour.Text = IsSP1AutomaticBehaviour
        lblLengthSP1Process.Text = CStr(oProduct.LengthSP1Process)

        If CStr(oProduct.IsSP2Automatic) = "True" Then
            lblIsSP2Automatic.Text = "Yes"
        Else
            lblIsSP2Automatic.Text = "No"
        End If

        Dim IsSP2AutomaticBehaviour As String
        If oProduct.IsSP2AutomaticBehaviour = "D" Then
            IsSP2AutomaticBehaviour = "Default"
        ElseIf oProduct.IsSP2AutomaticBehaviour = "L" Then
            IsSP2AutomaticBehaviour = "Locked"
        ElseIf oProduct.IsSP2AutomaticBehaviour = "N" Then
            IsSP2AutomaticBehaviour = "Minimum"
        ElseIf oProduct.IsSP2AutomaticBehaviour = "X" Then
            IsSP2AutomaticBehaviour = "Maximum"
        End If
        lblIsSP2AutomaticBehaviour.Text = IsSP2AutomaticBehaviour
        lblLengthSP2Process.Text = CStr(oProduct.LengthSP2Process)

        lblLMDProcessed.Text = CStr(oProduct.LengthMainDocProcess)

        Dim LengthMainDocProcessBehaviour As String
        If oProduct.LengthMainDocProcessBehaviour = "D" Then
            LengthMainDocProcessBehaviour = "Default"
        ElseIf oProduct.LengthMainDocProcessBehaviour = "L" Then
            LengthMainDocProcessBehaviour = "Locked"
        ElseIf oProduct.LengthMainDocProcessBehaviour = "N" Then
            LengthMainDocProcessBehaviour = "Minimum"
        ElseIf oProduct.LengthMainDocProcessBehaviour = "X" Then
            LengthMainDocProcessBehaviour = "Maximum"
        End If
        lblLMDProcessedBehaviour.Text = LengthMainDocProcessBehaviour

        lblLMDTaken.Text = CStr(oProduct.LengthMainDocTaken)

        Dim LengthMainDocTakenBehaviour As String
        If oProduct.LengthMainDocTakenBehaviour = "D" Then
            LengthMainDocTakenBehaviour = "Default"
        ElseIf oProduct.LengthMainDocTakenBehaviour = "L" Then
            LengthMainDocTakenBehaviour = "Locked"
        ElseIf oProduct.LengthMainDocTakenBehaviour = "N" Then
            LengthMainDocTakenBehaviour = "Minimum"
        ElseIf oProduct.LengthMainDocTakenBehaviour = "X" Then
            LengthMainDocTakenBehaviour = "Maximum"
        End If
        lblLMDTakenBehaviour.Text = LengthMainDocTakenBehaviour

        lblGPLC.Text = CStr(oProduct.GracePeriodLateCharges)

        Dim GracePeriodLateChargesBehaviour As String
        If oProduct.GracePeriodLateChargesBehaviour = "D" Then
            GracePeriodLateChargesBehaviour = "Default"
        ElseIf oProduct.GracePeriodLateChargesBehaviour = "L" Then
            GracePeriodLateChargesBehaviour = "Locked"
        ElseIf oProduct.GracePeriodLateChargesBehaviour = "N" Then
            GracePeriodLateChargesBehaviour = "Minimum"
        ElseIf oProduct.GracePeriodLateChargesBehaviour = "X" Then
            GracePeriodLateChargesBehaviour = "Maximum"
        End If
        lblGPLCBehaviour.Text = GracePeriodLateChargesBehaviour

        lblPPR.Text = FormatNumber(CStr(oProduct.PrepaymentPenaltyRate).Trim, 2)

        Dim PrepaymentPenaltyRateBehaviour As String
        If oProduct.PrepaymentPenaltyRateBehaviour = "D" Then
            PrepaymentPenaltyRateBehaviour = "Default"
        ElseIf oProduct.PrepaymentPenaltyRateBehaviour = "L" Then
            PrepaymentPenaltyRateBehaviour = "Locked"
        ElseIf oProduct.PrepaymentPenaltyRateBehaviour = "N" Then
            PrepaymentPenaltyRateBehaviour = "Minimum"
        ElseIf oProduct.PrepaymentPenaltyRateBehaviour = "X" Then
            PrepaymentPenaltyRateBehaviour = "Maximum"
        End If
        lblPPRBehaviour.Text = PrepaymentPenaltyRateBehaviour

        lblDaysToRemind.Text = CStr(oProduct.DeskCollPhoneRemind)

        Dim DeskCollPhoneRemindBehaviour As String
        If oProduct.DeskCollPhoneRemindBehaviour = "D" Then
            DeskCollPhoneRemindBehaviour = "Default"
        ElseIf oProduct.DeskCollPhoneRemindBehaviour = "L" Then
            DeskCollPhoneRemindBehaviour = "Locked"
        ElseIf oProduct.DeskCollPhoneRemindBehaviour = "N" Then
            DeskCollPhoneRemindBehaviour = "Minimum"
        ElseIf oProduct.DeskCollPhoneRemindBehaviour = "X" Then
            DeskCollPhoneRemindBehaviour = "Maximum"
        End If
        lblDaysToRemindBehaviour.Text = DeskCollPhoneRemindBehaviour

        lblDaysOverduetocall.Text = CStr(oProduct.DeskCollOD)

        Dim DeskCollODBehaviour As String
        If oProduct.DeskCollODBehaviour = "D" Then
            DeskCollODBehaviour = "Default"
        ElseIf oProduct.DeskCollODBehaviour = "L" Then
            DeskCollODBehaviour = "Locked"
        ElseIf oProduct.DeskCollODBehaviour = "N" Then
            DeskCollODBehaviour = "Minimum"
        ElseIf oProduct.DeskCollODBehaviour = "X" Then
            DeskCollODBehaviour = "Maximum"
        End If
        lblDaysOverduetocallBehaviour.Text = DeskCollODBehaviour

        lblPreviousOverduetoremind.Text = CStr(oProduct.PrevODToRemind)

        Dim PrevODToRemindBehaviour As String
        If oProduct.PrevODToRemindBehaviour = "D" Then
            PrevODToRemindBehaviour = "Default"
        ElseIf oProduct.PrevODToRemindBehaviour = "L" Then
            PrevODToRemindBehaviour = "Locked"
        ElseIf oProduct.PrevODToRemindBehaviour = "N" Then
            PrevODToRemindBehaviour = "Minimum"
        ElseIf oProduct.PrevODToRemindBehaviour = "X" Then
            PrevODToRemindBehaviour = "Maximum"
        End If
        lblPreviousOverduetoremindBehaviour.Text = PrevODToRemindBehaviour

        lblPDCRequesttocall.Text = CStr(oProduct.PDCDayToRemind)

        Dim PDCDayToRemindBehaviour As String
        If oProduct.PDCDayToRemindBehaviour = "D" Then
            PDCDayToRemindBehaviour = "Default"
        ElseIf oProduct.PDCDayToRemindBehaviour = "L" Then
            PDCDayToRemindBehaviour = "Locked"
        ElseIf oProduct.PDCDayToRemindBehaviour = "N" Then
            PDCDayToRemindBehaviour = "Minimum"
        ElseIf oProduct.PDCDayToRemindBehaviour = "X" Then
            PDCDayToRemindBehaviour = "Maximum"
        End If
        lblPDCRequesttocallBehaviour.Text = PDCDayToRemindBehaviour

        lblDaysToRemind.Text = CStr(oProduct.DeskCollSMSRemind)

        Dim DeskCollSMSRemindBehaviour As String
        If oProduct.DeskCollSMSRemindBehaviour = "D" Then
            DeskCollSMSRemindBehaviour = "Default"
        ElseIf oProduct.DeskCollSMSRemindBehaviour = "L" Then
            DeskCollSMSRemindBehaviour = "Locked"
        ElseIf oProduct.DeskCollSMSRemindBehaviour = "N" Then
            DeskCollSMSRemindBehaviour = "Minimum"
        ElseIf oProduct.DeskCollSMSRemindBehaviour = "X" Then
            DeskCollSMSRemindBehaviour = "Maximum"
        End If
        lblDaysToRemindBehaviour.Text = DeskCollSMSRemindBehaviour

        lblDaysToGenerateDCR.Text = CStr(oProduct.DCR)

        Dim DCRBehaviour As String
        If oProduct.DCRBehaviour = "D" Then
            DCRBehaviour = "Default"
        ElseIf oProduct.DCRBehaviour = "L" Then
            DCRBehaviour = "Locked"
        ElseIf oProduct.DCRBehaviour = "N" Then
            DCRBehaviour = "Minimum"
        ElseIf oProduct.DCRBehaviour = "X" Then
            DCRBehaviour = "Maximum"
        End If
        lblDaysToGenerateDCRBehaviour.Text = DCRBehaviour

        lblDaysToGenerateRAL.Text = CStr(oProduct.ODToRAL)

        Dim ODToRALBehaviour As String
        If oProduct.ODToRALBehaviour = "D" Then
            ODToRALBehaviour = "Default"
        ElseIf oProduct.ODToRALBehaviour = "L" Then
            ODToRALBehaviour = "Locked"
        ElseIf oProduct.ODToRALBehaviour = "N" Then
            ODToRALBehaviour = "Minimum"
        ElseIf oProduct.ODToRALBehaviour = "X" Then
            ODToRALBehaviour = "Maximum"
        End If
        lblDaysToGenerateRALBehaviour.Text = ODToRALBehaviour

        lblDaysBeforeDuetoRN.Text = CStr(oProduct.DaysBeforeDueToRN)

        Dim DaysBeforeDueToRNBehaviour As String
        If oProduct.DaysBeforeDueToRNBehaviour = "D" Then
            DaysBeforeDueToRNBehaviour = "Default"
        ElseIf oProduct.DaysBeforeDueToRNBehaviour = "L" Then
            DaysBeforeDueToRNBehaviour = "Locked"
        ElseIf oProduct.DaysBeforeDueToRNBehaviour = "N" Then
            DaysBeforeDueToRNBehaviour = "Minimum"
        ElseIf oProduct.DaysBeforeDueToRNBehaviour = "X" Then
            DaysBeforeDueToRNBehaviour = "Maximum"
        End If
        lblDaysBeforeDuetoRNBehaviour.Text = DaysBeforeDueToRNBehaviour

        lblRALPeriod.Text = CStr(oProduct.RALPeriod)


        Dim RALPeriodBehaviour As String
        If oProduct.RALPeriodBehaviour = "D" Then
            RALPeriodBehaviour = "Default"
        ElseIf oProduct.RALPeriodBehaviour = "L" Then
            RALPeriodBehaviour = "Locked"
        ElseIf oProduct.RALPeriodBehaviour = "N" Then
            RALPeriodBehaviour = "Minimum"
        ElseIf oProduct.RALPeriodBehaviour = "X" Then
            RALPeriodBehaviour = "Maximum"
        End If
        lblRALPeriodBehaviour.Text = RALPeriodBehaviour

        lblMaxDaysRNPaid.Text = CStr(oProduct.MaxDaysRNPaid)

        Dim MaxDaysRNPaidBehaviour As String
        If oProduct.MaxDaysRNPaidBehaviour = "D" Then
            MaxDaysRNPaidBehaviour = "Default"
        ElseIf oProduct.MaxDaysRNPaidBehaviour = "L" Then
            MaxDaysRNPaidBehaviour = "Locked"
        ElseIf oProduct.MaxDaysRNPaidBehaviour = "N" Then
            MaxDaysRNPaidBehaviour = "Minimum"
        ElseIf oProduct.MaxDaysRNPaidBehaviour = "X" Then
            MaxDaysRNPaidBehaviour = "Maximum"
        End If
        lblMaxDaysRNPaidBehaviour.Text = MaxDaysRNPaidBehaviour

        lblMaxPTPYDays.Text = CStr(oProduct.MaxPTPYDays)

        Dim MaxPTPYDaysBehaviour As String
        If oProduct.MaxPTPYDaysBehaviour = "D" Then
            MaxPTPYDaysBehaviour = "Default"
        ElseIf oProduct.MaxPTPYDaysBehaviour = "L" Then
            MaxPTPYDaysBehaviour = "Locked"
        ElseIf oProduct.MaxPTPYDaysBehaviour = "N" Then
            MaxPTPYDaysBehaviour = "Minimum"
        ElseIf oProduct.MaxPTPYDaysBehaviour = "X" Then
            MaxPTPYDaysBehaviour = "Maximum"
        End If
        lblMaxPTPYDaysBehaviour.Text = MaxPTPYDaysBehaviour

        lblPTPYBank.Text = CStr(oProduct.PTPYBank)

        Dim PTPYBankBehaviour As String
        If oProduct.PTPYBankBehaviour = "D" Then
            PTPYBankBehaviour = "Default"
        ElseIf oProduct.PTPYBankBehaviour = "L" Then
            PTPYBankBehaviour = "Locked"
        ElseIf oProduct.PTPYBankBehaviour = "N" Then
            PTPYBankBehaviour = "Minimum"
        ElseIf oProduct.PTPYBankBehaviour = "X" Then
            PTPYBankBehaviour = "Maximum"
        End If
        lblPTPYBankBehaviour.Text = PTPYBankBehaviour

        lblPTPYCompany.Text = CStr(oProduct.PTPYCompany)

        Dim PTPYCompanyBehaviour As String
        If oProduct.PTPYCompanyBehaviour = "D" Then
            PTPYCompanyBehaviour = "Default"
        ElseIf oProduct.PTPYCompanyBehaviour = "L" Then
            PTPYCompanyBehaviour = "Locked"
        ElseIf oProduct.PTPYCompanyBehaviour = "N" Then
            PTPYCompanyBehaviour = "Minimum"
        ElseIf oProduct.PTPYCompanyBehaviour = "X" Then
            PTPYCompanyBehaviour = "Maximum"
        End If
        lblPTPYCompanyBehaviour.Text = PTPYCompanyBehaviour

        lblPTPYSupplier.Text = CStr(oProduct.PTPYSupplier)

        Dim PTPYSupplierBehaviour As String
        If oProduct.PTPYSupplierBehaviour = "D" Then
            PTPYSupplierBehaviour = "Default"
        ElseIf oProduct.PTPYSupplierBehaviour = "L" Then
            PTPYSupplierBehaviour = "Locked"
        ElseIf oProduct.PTPYSupplierBehaviour = "N" Then
            PTPYSupplierBehaviour = "Minimum"
        ElseIf oProduct.PTPYSupplierBehaviour = "X" Then
            PTPYSupplierBehaviour = "Maximum"
        End If
        lblPTPYSupplierBehaviour.Text = PTPYSupplierBehaviour

        lblRALExtension.Text = CStr(oProduct.RALExtension)

        Dim RALExtensionBehaviour As String
        If oProduct.RALExtensionBehaviour = "D" Then
            RALExtensionBehaviour = "Default"
        ElseIf oProduct.RALExtensionBehaviour = "L" Then
            RALExtensionBehaviour = "Locked"
        ElseIf oProduct.RALExtensionBehaviour = "N" Then
            RALExtensionBehaviour = "Minimum"
        ElseIf oProduct.RALExtensionBehaviour = "X" Then
            RALExtensionBehaviour = "Maximum"
        End If
        lblRALExtensionBehaviour.Text = RALExtensionBehaviour

        lblInventoryExpected.Text = CStr(oProduct.InventoryExpected)

        Dim InventoryExpectedBehaviour As String
        If oProduct.InventoryExpectedBehaviour = "D" Then
            InventoryExpectedBehaviour = "Default"
        ElseIf oProduct.InventoryExpectedBehaviour = "L" Then
            InventoryExpectedBehaviour = "Locked"
        ElseIf oProduct.InventoryExpectedBehaviour = "N" Then
            InventoryExpectedBehaviour = "Minimum"
        ElseIf oProduct.InventoryExpectedBehaviour = "X" Then
            InventoryExpectedBehaviour = "Maximum"
        End If
        lblInventoryExpectedBehaviour.Text = InventoryExpectedBehaviour

        lblBilingCharges.Text = FormatNumber(CStr(oProduct.BillingCharges).Trim, 2)

        Dim BillingChargesBehaviour As String
        If oProduct.BillingChargesBehaviour = "D" Then
            BillingChargesBehaviour = "Default"
        ElseIf oProduct.BillingChargesBehaviour = "L" Then
            BillingChargesBehaviour = "Locked"
        ElseIf oProduct.BillingChargesBehaviour = "N" Then
            BillingChargesBehaviour = "Minimum"
        ElseIf oProduct.BillingChargesBehaviour = "X" Then
            BillingChargesBehaviour = "Maximum"
        End If
        lblBilingChargesBehaviour.Text = BillingChargesBehaviour

        lblLimitAPCash.Text = FormatNumber(CStr(oProduct.LimitAPCash).Trim, 2)

        Dim LimitAPCashBehaviour As String
        If oProduct.LimitAPCashBehaviour = "D" Then
            LimitAPCashBehaviour = "Default"
        ElseIf oProduct.LimitAPCashBehaviour = "L" Then
            LimitAPCashBehaviour = "Locked"
        ElseIf oProduct.LimitAPCashBehaviour = "N" Then
            LimitAPCashBehaviour = "Minimum"
        ElseIf oProduct.LimitAPCashBehaviour = "X" Then
            LimitAPCashBehaviour = "Maximum"
        End If
        lblLimitAPCashBehaviour.Text = LimitAPCashBehaviour


        If CStr(oProduct.IsRecourse_HO) = "True" Then
            lblIsRecourse.Text = "Yes"
        Else
            lblIsRecourse.Text = "No"
        End If

        If CStr(oProduct.IsActive_HO) = "True" Then
            lblIsActive.Text = "Yes"
        Else
            lblIsActive.Text = "No"
        End If
    End Sub
#End Region
End Class