﻿Module BehaviorHelperModule
    Public Sub Behaviour_Lock(bhv As String, textBox As TextBox, ddl As DropDownList)
        Dim enable = bhv <> "L"
        textBox.Enabled = enable
        ddl.Enabled = enable

        If bhv = "N" Then
            ddl.Items.Remove(ddl.Items.FindByValue("X"))
        ElseIf bhv = "X" Then
            ddl.Items.Remove(ddl.Items.FindByValue("N"))
        End If
    End Sub
    Public Sub Behaviour_Lock(bhv As String, textBox As RadioButtonList, ddl As DropDownList)
        Dim enable = bhv <> "L"
        textBox.Enabled = enable
        ddl.Enabled = enable

        If bhv = "N" Then
            ddl.Items.Remove(ddl.Items.FindByValue("X"))
        ElseIf bhv = "X" Then
            ddl.Items.Remove(ddl.Items.FindByValue("N"))
        End If
    End Sub
End Module
