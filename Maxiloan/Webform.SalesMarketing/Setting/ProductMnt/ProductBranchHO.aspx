﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ProductBranchHO.aspx.vb"
    Inherits="Maxiloan.Webform.SalesMarketing.ProductBranchHO" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="productUmumHOTab.ascx" TagName="umum" TagPrefix="uc1" %>  
<%@ Register Src="productSkemaHOTab.ascx" TagName="skema" TagPrefix="uc2" %>   
<%@ Register Src="productBiayaHOTab.ascx" TagName="biaya" TagPrefix="uc3" %>  

<%@ Register Src="productAngsuranHOTab.ascx" TagName="angs" TagPrefix="uc4" %>  
  <%@ Register Src="productCollectionHOTab.ascx" TagName="coll" TagPrefix="uc5" %>  
<%@ Register Src="productFinanceHOTab.ascx" TagName="fin" TagPrefix="uc6" %>  

<%@ Register TagPrefix="cc1" Namespace="System.Web.UI.WebControls" Assembly="System.Web" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ProductBranchHO</title>
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
    <script src="../../../js/jquery-2.1.1.min.js" type="text/javascript"></script>
     <script src="../../../js/product.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../../Include/Buttons.css" type="text/css" />
    <script language="JavaScript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;

        function OpenWinProductView(pProductID, pStyle) {
            var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
            window.open('http://<%#Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.SalesMarketing/Setting/ProductMnt/ProductView.aspx?ProductID=' + pProductID + '&style=' + pStyle, 'Product', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=1');
        }

        function OpenWinProductBranchHOView(pProductID, pBranchID) {
            var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
            window.open('http://<%#Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.SalesMarketing/Setting/ProductMnt/ProductBranchHOView.aspx?ProductID=' + pProductID + '&BranchID=' + pBranchID, 'Product', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=1');
        }


        function fBack() {
            history.back(-1);
            return false;
        }
        function fClose() {
            window.close();
            return false;
        }
        function DeleteConfirm() {
            if (confirm("Apakah yakin mau hapus data ini ?")) {
                return true;
            }
            else {
                return false;
            }
        }

//        function click() {
//            if (event.button == 2) {
//                alert('You Are Not Authorize!');
//            }
//        }
//        document.onmousedown = click

        //EffectiveRate
//        function EffectiveRate() {
//            var oCombo = document.forms[0].Hide_EffectiveRateBeh;
//            var otxt = document.forms[0].E_txtEffectiveRate;
//            var otxt_GYR = document.forms[0].E_txtGrossYieldRate;
//            var ohide = document.forms[0].Hide_EffectiveRate;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi suku Margin effective antara ' + ohide.value + ' dan 100');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi suku Margin effective antara 1 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }

//            if (parseFloat(otxt.value) > parseFloat(otxt_GYR.value)) {
//                alert('Suku Margin Effective harus <= Gross Yield Rate');
//                otxt.value = ohide.value;
//            }
//        }
        //GrossYieldRate
//        function GrossYieldRate() {
//            var oCombo = document.forms[0].Hide_GrossYieldRateBeh;
//            var otxt = document.forms[0].E_txtGrossYieldRate;
//            var otxt_ER = document.forms[0].E_txtEffectiveRate;
//            var ohide = document.forms[0].Hide_GrossYieldRate;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Gross Yield Rate antara ' + ohide.value + ' dan 100');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Gross Yield Rate antara 1 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }

//            if (parseFloat(otxt.value) < parseFloat(otxt_ER.value)) {
//                alert('Suku Margin Effective harus <= Gross Yield Rate');
//                otxt.value = ohide.value;
//            }
//        }

        //Minimum Tenor
//        function MinimumTenor() {
//            var otxt = document.forms[0].E_txtMinimumTenor;
//            var otxt_max = document.forms[0].E_txtMaximumTenor;
//            var ohide = document.forms[0].Hide_MinimumTenor;

//            if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                alert('Harap isi jangka waktu minimum antara ' + ohide.value + ' dan 999');
//                otxt.value = ohide.value;
//            }

//            if (parseFloat(otxt.value) > parseFloat(otxt_max.value)) {
//                alert('Jangka Waktu Minimum harus <= Jangka waktu Maximum');
//                otxt.value = ohide.value;
//            }

//        }

//        //Maximum Tenor
//        function MaximumTenor() {
//            var otxt_min = document.forms[0].E_txtMinimumTenor;
//            var otxt = document.forms[0].E_txtMaximumTenor;
//            var ohide = document.forms[0].Hide_MaximumTenor;

//            if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                alert('Harap isi jangka waktu Maximum antara  0 dan ' + ohide.value);
//                otxt.value = ohide.value;
//            }

//            if (parseFloat(otxt.value) < parseFloat(otxt_min.value)) {
//                alert('Jangka Waktu Minimum harus <= Jangka waktu Maximum');
//                otxt.value = ohide.value;
//            }
//        }


//        // Late Charges Percentage (Installment)
//        function PenaltyPercentage() {
//            var oCombo = document.forms[0].Hide_PenaltyPercentageBeh;
//            var otxt = document.forms[0].E_txtPenaltyPercentage;
//            var ohide = document.forms[0].Hide_PenaltyPercentage;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Prosentase denda keterlambatan angsuran antara ' + ohide.value + ' dan 100');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Prosentase denda keterlambatan angsuran antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }


//        // Late Charges Percentage (Insurance)
//        function InsurancePenaltyPercentage() {
//            var oCombo = document.forms[0].Hide_InsurancePenaltyPercentageBeh;
//            var otxt = document.forms[0].E_txtInsurancePenaltyPercentage;
//            var ohide = document.forms[0].Hide_InsurancePenaltyPercentage;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Prosentase denda keterlambatan Asuransi antara ' + ohide.value + ' dan 100');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Prosentase denda keterlambatan Asuransi antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }


//        // Cancellation Fee
//        function CancellationFee() {
//            var oCombo = document.forms[0].Hide_CancellationFeeBeh;
//            var otxt = document.forms[0].E_txtCancellationFee;
//            var ohide = document.forms[0].Hide_CancellationFee;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Biaya Pembatalan antara ' + ohide.value + ' dan 9999999999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Biaya Pembatalan antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }


//        // Admin Fee
//        function AdminFee() {
//            var oCombo = document.forms[0].Hide_AdminFeeBeh;
//            var otxt = document.forms[0].E_txtAdminFee;
//            var ohide = document.forms[0].Hide_AdminFee;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Biaya Administrasi antara ' + ohide.value + ' dan 9999999999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Biaya Administrasi antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }


//        // Fiducia Fee
//        function FiduciaFee() {
//            var oCombo = document.forms[0].Hide_FiduciaFeeBeh;
//            var otxt = document.forms[0].E_txtFiduciaFee;
//            var ohide = document.forms[0].Hide_FiduciaFee;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Biaya Fidusia antara ' + ohide.value + ' dan 9999999999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Biaya Fidusia antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }


//        // Provision Fee
//        function ProvisionFee() {
//            var oCombo = document.forms[0].Hide_ProvisionFeeBeh;
//            var otxt = document.forms[0].E_txtProvisionFee;
//            var ohide = document.forms[0].Hide_ProvisionFee;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Biaya Provisi antara ' + ohide.value + ' dan 9999999999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Biaya Provisi antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }

//        // Notary Fee
//        function NotaryFee() {
//            var oCombo = document.forms[0].Hide_NotaryFeeBeh;
//            var otxt = document.forms[0].E_txtNotaryFee;
//            var ohide = document.forms[0].Hide_NotaryFee;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Biaya Notaris antara ' + ohide.value + ' dan 9999999999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Biaya Notaris antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }

//        // Survey Fee
//        function SurveyFee() {
//            var oCombo = document.forms[0].Hide_SurveyFeeBeh;
//            var otxt = document.forms[0].E_txtSurveyFee;
//            var ohide = document.forms[0].Hide_SurveyFee;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Biaya Survey antara ' + ohide.value + ' dan 9999999999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Biaya Survey antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }

//        // Visit Fee
//        function VisitFee() {
//            var oCombo = document.forms[0].Hide_VisitFeeBeh;
//            var otxt = document.forms[0].E_txtVisitFee;
//            var ohide = document.forms[0].Hide_VisitFee;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Biaya Kunjungan antara ' + ohide.value + ' dan 9999999999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Biaya Kunjungan antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }

//        // Rescheduling Fee
//        function ReschedulingFee() {
//            var oCombo = document.forms[0].Hide_ReschedulingFeeBeh;
//            var otxt = document.forms[0].E_txtReschedulingFee;
//            var ohide = document.forms[0].Hide_ReschedulingFee;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Biaya Rescheduling antara ' + ohide.value + ' dan 9999999999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Biaya Rescheduling antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }

//        // AgreementTransferFee
//        function AgreementTransferFee() {
//            var oCombo = document.forms[0].Hide_AgreementTransferFeeBeh;
//            var otxt = document.forms[0].E_txtAgreementTransferFee;
//            var ohide = document.forms[0].Hide_AgreementTransferFee;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Biaya OverKontrak antara ' + ohide.value + ' dan 9999999999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Biaya OverKontrak antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }

//        // ChangeDueDateFee
//        function ChangeDueDateFee() {
//            var oCombo = document.forms[0].Hide_ChangeDueDateFeeBeh;
//            var otxt = document.forms[0].E_txtChangeDueDateFee;
//            var ohide = document.forms[0].Hide_ChangeDueDateFee;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Biaya Ganti Tanggal jatuh Tempo antara ' + ohide.value + ' dan 9999999999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Biaya Ganti Tanggal jatuh Tempo antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }

//        // AssetReplacementFee
//        function AssetReplacementFee() {
//            var oCombo = document.forms[0].Hide_AssetReplacementFeeBeh;
//            var otxt = document.forms[0].E_txtAssetReplacementFee;
//            var ohide = document.forms[0].Hide_AssetReplacementFee;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Biaya Ganti Asset antara ' + ohide.value + ' dan 9999999999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Biaya Ganti Asset antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }

//        // RepossesFee
//        function RepossesFee() {
//            var oCombo = document.forms[0].Hide_RepossesFeeBeh;
//            var otxt = document.forms[0].E_txtRepossesFee;
//            var ohide = document.forms[0].Hide_RepossesFee;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Biaya Tarik Asset antara ' + ohide.value + ' dan 9999999999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Biaya Tarik Asset antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }

//        // LegalizedDocumentFee
//        function LegalizedDocumentFee() {
//            var oCombo = document.forms[0].Hide_LegalizedDocumentFeeBeh;
//            var otxt = document.forms[0].E_LegalizedDocumentFee;
//            var ohide = document.forms[0].Hide_LegalizedDocumentFee;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Biaya Legalisasi Dokumen antara ' + ohide.value + ' dan 9999999999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Biaya Legalisasi Dokumen antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }

//        // PDCBounceFee
//        function PDCBounceFee() {
//            var oCombo = document.forms[0].Hide_PDCBounceFeeBeh;
//            var otxt = document.forms[0].E_PDCBounceFee;
//            var ohide = document.forms[0].Hide_PDCBounceFee;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Biaya Penolakan PDC antara ' + ohide.value + ' dan 9999999999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Biaya Penolakan PDC antara 0 and ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }


//        // InsuranceAdminFee
//        function InsuranceAdminFee() {
//            var oCombo = document.forms[0].Hide_InsuranceAdminFeeBeh;
//            var otxt = document.forms[0].E_txtInsuranceAdminFee;
//            var ohide = document.forms[0].Hide_InsuranceAdminFee;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Biaya Admin Asuransi antara ' + ohide.value + ' dan 9999999999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Biaya Admin Asuransi antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }        

//        // InsuranceStampDutyFee
//        function InsuranceStampDutyFee() {
//            var oCombo = document.forms[0].Hide_InsuranceStampDutyFeeBeh;
//            var otxt = document.forms[0].E_txtInsuranceStampDutyFee;
//            var ohide = document.forms[0].Hide_InsuranceStampDutyFee;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Biaya Materai Asuransi antara ' + ohide.value + ' dan 9999999999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Biaya Materai Asuransi antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }

//        // BiayaPolis
//        function BiayaPolis() {
//            var oCombo = document.forms[0].Hide_BiayaPolisBeh;
//            var otxt = document.forms[0].E_txtBiayaPolis;
//            var ohide = document.forms[0].Hide_BiayaPolis;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Biaya Polis antara ' + ohide.value + ' dan 9999999999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Biaya Polis antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }

//        // POExpirationDays
//        function POExpirationDays() {
//            var oCombo = document.forms[0].Hide_POExpirationDaysBeh;
//            var otxt = document.forms[0].E_txtPOExpirationDays;
//            var ohide = document.forms[0].Hide_POExpirationDays;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Masa Berlaku PO antara ' + ohide.value + ' dan 9999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Masa Berlaku PO antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }


//        // InstallmentToleranceAmount
//        function InstallmentToleranceAmount() {
//            var oCombo = document.forms[0].Hide_InstallmentToleranceAmountBeh;
//            var otxt = document.forms[0].E_txtInstallmentToleranceAmount;
//            var ohide = document.forms[0].Hide_InstallmentToleranceAmount;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Jumlah Toleransi terhadap Angsuran antara ' + ohide.value + ' dan 999999999999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Jumlah Toleransi terhadap Angsuran antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }

//        // DPPercentage
//        function DPPercentage() {
//            var oCombo = document.forms[0].Hide_DPPercentageBeh;
//            var otxt = document.forms[0].E_txtDPPercentage;
//            var ohide = document.forms[0].Hide_DPPercentage;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Prosentase Minimum DP antara ' + ohide.value + ' dan 100');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Prosentase Minimum DP antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }

//        // RejectMinimumIncome
//        function RejectMinimumIncome() {
//            var oCombo = document.forms[0].Hide_RejectMinimumIncomeBeh;
//            var otxt = document.forms[0].E_txtRejectMinimumIncome;
//            var ohide = document.forms[0].Hide_RejectMinimumIncome;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Pendapatan Margin Minimum yg harus ditolak antara ' + ohide.value + ' dan 999999999999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Pendapatan Margin Minimum yg harus ditolak antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }

//        // WarningMinimumIncome
//        function WarningMinimumIncome() {
//            var oCombo = document.forms[0].Hide_WarningMinimumIncomeBeh;
//            var otxt = document.forms[0].E_txtWarningMinimumIncome;
//            var ohide = document.forms[0].Hide_WarningMinimumIncome;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Pendapatan Bunga Minimum yg harus diingatkann antara ' + ohide.value + ' dan 999999999999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Pendapatan Bunga Minimum yg harus diingatkann antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }


//        // Length Main Document Processed
//        function LengthMainDocProcess() {
//            var oCombo = document.forms[0].Hide_LengthMainDocProcessBeh;
//            var otxt = document.forms[0].E_txtLengthMainDocProcess;
//            var ohide = document.forms[0].Hide_LengthMainDocProcess;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Jangka Waktu proses Dokumen Utama  antara ' + ohide.value + ' dan 9999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Jangka Waktu proses Dokumen Utama  antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }

//        // Length Main Document Taken
//        function LengthMainDocTaken() {
//            var oCombo = document.forms[0].Hide_LengthMainDocTakenBeh;
//            var otxt = document.forms[0].E_txtLengthMainDocTaken;
//            var ohide = document.forms[0].Hide_LengthMainDocTaken;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Jangka Waktu Dokumen Utama diambil antara ' + ohide.value + ' dan 9999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Jangka Waktu Dokumen Utama diambil antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }

//        // Grace Period Late Charges
//        function GracePeriodLateCharges() {
//            var oCombo = document.forms[0].Hide_GracePeriodLateChargesBeh;
//            var otxt = document.forms[0].E_txtGracePeriodLateCharges;
//            var ohide = document.forms[0].Hide_GracePeriodLateCharges;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Grace Period Denda Keterlambatan antara ' + ohide.value + ' dan 9999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Grace Period Denda Keterlambatan antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }


//        // Prepayment Penalty Rate
//        function PrepaymentPenaltyRate() {
//            var oCombo = document.forms[0].Hide_PrepaymentPenaltyRateBeh;
//            var otxt = document.forms[0].E_txtPrepaymentPenaltyRate;
//            var ohide = document.forms[0].Hide_PrepaymentPenaltyRate;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Denda Pelunasan dipercepat antara ' + ohide.value + ' dan 100');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Denda Pelunasan dipercepat antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }

//        // Days to remind Installment
//        function DeskCollPhoneRemind() {
//            var oCombo = document.forms[0].Hide_DeskCollPhoneRemindBeh;
//            var otxt = document.forms[0].E_txtDeskCollPhoneRemind;
//            var ohide = document.forms[0].Hide_DeskCollPhoneRemind;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Jumlah hari mengingatkan angsuran antara  ' + ohide.value + ' dan 9999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Jumlah hari mengingatkan angsuran antara  0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }


//        // Days overdue to call
//        function DeskCollOD() {
//            var oCombo = document.forms[0].Hide_DeskCollODBeh;
//            var otxt = document.forms[0].E_txtDeskCollOD;
//            var ohide = document.forms[0].Hide_DeskCollOD;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Jumlah hari Overdue untuk Deskcoll antara ' + ohide.value + ' dan 9999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Jumlah hari Overdue untuk Deskcoll antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }


//        // Previous overdue to remind
//        function PrevODToRemind() {
//            var oCombo = document.forms[0].Hide_PrevODToRemindBeh;
//            var otxt = document.forms[0].E_txtPrevODToRemind;
//            var ohide = document.forms[0].Hide_PrevODToRemind;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Overdue sebelumnya untuk mengingatkan antara ' + ohide.value + ' dan 9999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Overdue sebelumnya untuk mengingatkan antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }

//        // PDC request to call
//        function PDCDaysToRemind() {
//            var oCombo = document.forms[0].Hide_PDCDaysToRemindBeh;
//            var otxt = document.forms[0].E_txtPDCDaysToRemind;
//            var ohide = document.forms[0].Hide_PDCDaysToRemind;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Permintaan PDC untuk telepon antara ' + ohide.value + ' dan 9999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Permintaan PDC untuk telepon antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }


//        // Days to remind by SMS
//        function DeskCollSMSRemind() {
//            var oCombo = document.forms[0].Hide_DeskCollSMSRemindBeh;
//            var otxt = document.forms[0].E_txtDeskCollSMSRemind;
//            var ohide = document.forms[0].Hide_DeskCollSMSRemind;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Jumlah hari mengingatkan dgn SMS antara ' + ohide.value + ' dan 9999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Jumlah hari mengingatkan dgn SMS antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }

//        // Days to generate DCR
//        function DCR() {
//            var oCombo = document.forms[0].Hide_DCRBeh;
//            var otxt = document.forms[0].E_txtDCR;
//            var ohide = document.forms[0].Hide_DCR;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Jumlah hari buat DCR antara ' + ohide.value + ' dan 9999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Jumlah hari buat DCR antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }

//        // Days before due to generate Receipt Notes
//        function DaysBeforeDueToRN() {
//            var oCombo = document.forms[0].Hide_DaysBeforeDueToRNBeh;
//            var otxt = document.forms[0].E_txtDaysBeforeDueToRN;
//            var ohide = document.forms[0].Hide_DaysBeforeDueToRN;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Jumlah hari sebelum JT buat Kwitansi antara ' + ohide.value + ' dan 9999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Jumlah hari sebelum JT buat Kwitansi antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }

//        // Days to generate RAL
//        function ODToRAL() {
//            var oCombo = document.forms[0].Hide_ODToRALBeh;
//            var otxt = document.forms[0].E_txtODToRAL;
//            var ohide = document.forms[0].Hide_ODToRAL;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Jumlah hari buat SKT antara ' + ohide.value + ' dan 9999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Jumlah hari buat SKT antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }

//        // RAL Period
//        function RALPeriod() {
//            var oCombo = document.forms[0].Hide_RALPeriodBeh;
//            var otxt = document.forms[0].E_txtRALPeriod;
//            var ohide = document.forms[0].Hide_RALPeriod;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Jangka Waktu SKT antara ' + ohide.value + ' dan 9999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Jangka Waktu SKT antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }

//        // Maximum days Receipt notes paid
//        function MaxDaysRNPaid() {
//            var oCombo = document.forms[0].Hide_MaxDaysRNPaidBeh;
//            var otxt = document.forms[0].E_txtMaxDaysRNPaid;
//            var ohide = document.forms[0].Hide_MaxDaysRNPaid;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Jumlah hari Maximum Kwitansi dibayar antara ' + ohide.value + ' dan 9999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Jumlah hari Maximum Kwitansi dibayar antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }

//        // Maximum Promise To Pay days
//        function MaxPTPYDays() {
//            var oCombo = document.forms[0].Hide_MaxPTPYDaysBeh;
//            var otxt = document.forms[0].E_txtMaxPTPYDays;
//            var ohide = document.forms[0].Hide_MaxPTPYDays;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Jumlah hari Maximum janji Bayar antara ' + ohide.value + ' dan 9999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Jumlah hari Maximum janji Bayar antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }


//        // Promise To Pay To Bank
//        function PTPYBank() {
//            var oCombo = document.forms[0].Hide_PTPYBankBeh;
//            var otxt = document.forms[0].E_txtPTPYBank;
//            var ohide = document.forms[0].Hide_PTPYBank;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Janji Bayar ke Bank antara ' + ohide.value + ' dan 9999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Janji Bayar ke Bank antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }


//        // Promise To Pay To Company
//        function PTPYCompany() {
//            var oCombo = document.forms[0].Hide_PTPYCompanyBeh;
//            var otxt = document.forms[0].E_txtPTPYCompany;
//            var ohide = document.forms[0].Hide_PTPYCompany;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Janji bayar ke Perusahaan antara ' + ohide.value + ' dan 9999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Janji bayar ke Perusahaan antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }

//        // Promise To Pay To Supplier
//        function PTPYSupplier() {
//            var oCombo = document.forms[0].Hide_PTPYSupplierBeh;
//            var otxt = document.forms[0].E_txtPTPYSupplier;
//            var ohide = document.forms[0].Hide_PTPYSupplier;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Janji bayar ke Supplier antara ' + ohide.value + ' dan 9999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Janji bayar ke Supplier antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }

//        // RAL Extension
//        function RALExtension() {
//            var oCombo = document.forms[0].Hide_RALExtensionBeh;
//            var otxt = document.forms[0].E_txtRALExtension;
//            var ohide = document.forms[0].Hide_RALExtension;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Perpanjangan SKT antara ' + ohide.value + ' dan 9999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Perpanjangan SKT antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }

//        // Inventory Expected
//        function InventoryExpected() {
//            var oCombo = document.forms[0].Hide_InventoryExpectedBeh;
//            var otxt = document.forms[0].E_txtInventoryExpected;
//            var ohide = document.forms[0].Hide_InventoryExpected;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Jangka Waktu jadi Inventory antara ' + ohide.value + ' dan 9999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Jangka Waktu jadi Inventory antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }


//        // Billing Charges
//        function BillingCharges() {
//            var oCombo = document.forms[0].Hide_BillingChargesBeh;
//            var otxt = document.forms[0].E_txtBillingCharges;
//            var ohide = document.forms[0].Hide_BillingCharges;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Biaya Tagih antara ' + ohide.value + ' dan 999999999999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Biaya Tagih antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }


//        // Limit A/P Disbursement at Branch
//        function LimitAPCash() {
//            var oCombo = document.forms[0].Hide_LimitAPCashBeh;
//            var otxt = document.forms[0].E_txtLimitAPCash;
//            var ohide = document.forms[0].Hide_LimitAPCash;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Limit Pembayaran di Cabang antara ' + ohide.value + ' dan 999999999999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Limit Pembayaran di Cabang antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }

        //=====================================	

//        function cboIsSpAutomaticChange() {
//            var oCombo = document.forms[0].E_cboIsSPAutomatic;
//            var otxt = document.forms[0].E_txtLengthSPProcess;

//            if (oCombo.options[oCombo.selectedIndex].value == "0") {
//                otxt.disabled = true;
//                otxt.value = "0";
//            }
//            else {
//                otxt.disabled = false;
//                otxt.value = "0";
//            }
//        }

//        function cboIsSp1AutomaticChange() {
//            var oCombo = document.forms[0].E_cboIsSP1Automatic;
//            var otxt = document.forms[0].E_txtLengthSP1Process;

//            if (oCombo.options[oCombo.selectedIndex].value == "0") {
//                otxt.disabled = true;
//                otxt.value = "0";
//            }
//            else {
//                otxt.disabled = false;
//                otxt.value = "0";
//            }
//        }

//        function cboIsSp2AutomaticChange() {
//            var oCombo = document.forms[0].E_cboIsSP2Automatic;
//            var otxt = document.forms[0].E_txtLengthSP2Process;

//            if (oCombo.options[oCombo.selectedIndex].value == "0") {
//                otxt.disabled = true;
//                otxt.value = "0";
//            }
//            else {
//                otxt.disabled = false;
//                otxt.value = "0";
//            }
//        }
				
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>
                        DAFTAR PRODUK CABANG
                    </h3>
                </div>
            </div>
            <asp:Panel ID="pnlList" runat="server">
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtgPaging" runat="server" Width="100%" CssClass="grid_general"
                                AllowSorting="True" AutoGenerateColumns="False" OnSortCommand="Sorting" BorderStyle="None"
                                BorderWidth="0" DataKeyField="BranchID">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="EDIT">
                                        <ItemStyle CssClass="command_col"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbEdit" runat="server" CausesValidation="False" ImageUrl="../../../Images/iconedit.gif"
                                                CommandName="EDIT"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="CABANG" SortExpression="BranchFullName">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hynBranchFullName" runat="server" Text='<%#container.dataitem("BranchFullName")%>'>
                                            </asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="AKTIF" SortExpression="IsActive">
                                        <ItemTemplate>
                                            <asp:Label ID="lblIsActive" runat="server" Text='<%#container.dataitem("IsActive")%>'>
                                            </asp:Label>
                                            <asp:Label ID="lblBrchID" runat="server" Visible="False" Text='<%#container.dataitem("BranchID")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                            <div class="button_gridnavigation">
                                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../../Images/grid_navbutton01.png"
                                    CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../../Images/grid_navbutton02.png"
                                    CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../../Images/grid_navbutton03.png"
                                    CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../../Images/grid_navbutton04.png"
                                    CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                                    EnableViewState="False"></asp:Button>
                                <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" MinimumValue="1"
                                    ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"></asp:RangeValidator>
                                <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                                    ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                            </div>
                            <div class="label_gridnavigation">
                                <asp:Label ID="lblPage" runat="server"></asp:Label>of
                                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonAdd" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue">
                    </asp:Button>
                   <%-- <asp:Button ID="ButtonPrint" runat="server" Enabled="true" Text="Print" CssClass="small button blue">
                    </asp:Button>--%>
                    <asp:Button ID="ButtonBack" runat="server" CausesValidation="False" Text="Back" CssClass="small button gray">
                    </asp:Button>
                </div>
                <div class="form_box_title">
                    <div class="form_single">
                        <h4>
                            CARI PRODUK CABANG
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>ID Produk</label>
                        <asp:HyperLink ID="hplProductID" runat="server" Text='<%#request("ProductID")%>'> </asp:HyperLink>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Nama Produk</label>
                        <asp:Label ID="lblDescription" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Cari Berdasarkan</label>
                        <asp:DropDownList ID="cboSearchBy" runat="server">
                            <asp:ListItem Value="BranchFullName">Cabang</asp:ListItem>
                            <asp:ListItem Value="IsActive">Status Aktif</asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtSearchBy" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonSearch" runat="server" CausesValidation="False" Text="Search"
                        CssClass="small button blue"></asp:Button>
                    <asp:Button ID="ButtonReset" runat="server" CausesValidation="False" Text="Reset"
                        CssClass="small button gray"></asp:Button>
                </div>
            </asp:Panel>



            <asp:Panel ID="pnlBranch" runat="server"> 
                <div class="form_box_header">
                    <div class="form_single">
                        <h4>
                            PRODUK
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>ID Produck</label>
                        <asp:HyperLink ID="hplB_ProductID" runat="server" Text='<%#request("ProductID")%>'>
                        </asp:HyperLink>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Nama Produk</label>
                        <asp:Label ID="B_lblDescription" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <h4>
                            PRODUK ASSIGN DI CABANG
                        </h4>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtgBranchList" runat="server" Width="100%" CssClass="grid_general"
                                BorderStyle="None" BorderWidth="0" AllowSorting="True" AutoGenerateColumns="False"
                                DataKeyField="BranchID">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="ASSIGN">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chk_Branch" runat="server" Checked="True"></asp:CheckBox>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="CABANG" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" Text='<%# DataBinder.Eval(Container, "DataItem.BranchFullName")%>'
                                                runat="server">
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="BRANCHID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblBranchID" Text='<%# DataBinder.Eval(Container, "DataItem.BranchID")%>'
                                                runat="server">
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                        </div>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonOK" runat="server" CausesValidation="False" Text="Ok" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="ButtonCancel" runat="server" CausesValidation="False" Text="Cancel"
                        CssClass="small button gray"></asp:Button>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlEdit" runat="server">


            <div class="form_single">
                 <asp:TabContainer runat="server" ID="Tabs" style="Height:auto;"  ActiveTabIndex="0"  Width="100%">
               
                     <asp:TabPanel runat="server" ID="pnlTabUmum" HeaderText="UMUM"   >
                        <ContentTemplate>
                        <div>
                         <uc1:umum id="UmumTab" runat="server"/>  
                          </div>
                        </ContentTemplate>
                    </asp:TabPanel>

                     <asp:TabPanel runat="server" ID="pnlTabSkema" HeaderText="SKEMA"  >
                        <ContentTemplate>
                        <div>
                       <uc2:skema id="SkemaTab" runat="server"/>  
                          </div>
                        </ContentTemplate>
                    </asp:TabPanel>

                     <asp:TabPanel runat="server" ID="pnlTabBiaya" HeaderText="BIAYA"  >
                        <ContentTemplate>
                        <uc3:biaya id="BiayaTab" runat="server"/>  
                         </ContentTemplate>
                    </asp:TabPanel>

                     <asp:TabPanel runat="server" ID="pnlTabAngsuran" HeaderText="ANGSURAN"  >
                        <ContentTemplate>
                        <uc4:angs id="AngsuranTab" runat="server"/>  
                        
                         </ContentTemplate>
                    </asp:TabPanel>

                    <asp:TabPanel runat="server" ID="pnlTabCollection" HeaderText="COLLECTION"  >
                        <ContentTemplate>
                          <uc5:coll id="CollactionTab" runat="server"/> 
                         </ContentTemplate>
                    </asp:TabPanel>

                       <asp:TabPanel runat="server" ID="pnlTabFinance" HeaderText="FINANCE"  >
                        <ContentTemplate>
                          <uc6:fin id="FinanceTab" runat="server"/>  
                          </ContentTemplate>
                    </asp:TabPanel>

                </asp:TabContainer>

                       <%-- <div class="form_box_title">
                            <div class="form_single">
                                <h4>
                                    EDIT - PRODUK CABANG
                                </h4>
                            </div>
                        </div>
                        <div class="form_box">
                            <div class="form_single">
                                <label>
                                    Cabang</label>
                                <asp:Label ID="E_lblBranchID" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="form_box">
                            <div class="form_single">
                                <label>
                                    ID Produk</label>
                                <asp:Label ID="E_lblProductID" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="form_box">
                            <div class="form_single">
                                <label>
                                    Nama Produk</label>
                                <asp:Label ID="E_lblDescription" runat="server"></asp:Label>
                            </div>
                        </div> 

                        <div class="form_box_title">  <div class="form_single"> <h4> UMUM </h4> </div> </div>

                         <div class="form_box">
                            <div class="form_single">
                                <label>
                                    Jenis Asset</label>
                                <asp:Label ID="E_lblAssetType" runat="server" Visible="True"></asp:Label>
                            </div>
                        </div>
                        <div class="form_box"  style="display:none">
                            <div class="form_single">
                                <label>
                                    Skema Score - Marketing</label>
                                <asp:Label ID="E_lblScoreSchemeID" runat="server" Visible="True"></asp:Label>
                            </div>
                        </div> 
                        <div class="form_box">
                            <div class="form_single">
                                <label>
                                    Kredit Scoring </label>
                                <asp:Label ID="E_lblCreditScoreSchemeID" runat="server" Visible="True"></asp:Label>
                            </div>
                        </div>
                        <div class="form_box">
                            <div class="form_single">
                                <label>
                                    Skema Jurnal</label>
                                <asp:Label ID="E_lblJournalSchemeID" runat="server" Visible="True"></asp:Label>
                            </div>
                        </div>
                        <div class="form_box">
                            <div class="form_single">
                                <label>
                                    Skema Approval</label>
                                <asp:Label ID="E_lblApprovalSchemeID" runat="server" Visible="True"></asp:Label>
                            </div>
                        </div>
                        <div class="form_box">
                            <div class="form_single">
                                <label>
                                    Kondisi Asset</label>
                                <asp:Label ID="E_lblAssetUsedNew" runat="server" Visible="True"></asp:Label>
                            </div>
                        </div>
                        <div class="form_box">
                            <div class="form_single">
                                <label>
                                    Umur Kendaraan</label>
                                <asp:TextBox ID="txtUmurKendaraanFrom" runat="server" MaxLength="9">0</asp:TextBox>&nbsp;&nbsp;To&nbsp;&nbsp;
                                <asp:TextBox ID="txtUmurKendaraanTo" runat="server" MaxLength="9">0</asp:TextBox>
                            </div>
                        </div>
                        <div class="form_box">
                            <div class="form_single">
                                <label>
                                    Kegiatan Usaha</label>
                                <asp:Label ID="E_lblKegiatanUsaha" runat="server" Visible="True"></asp:Label>
                            </div>
                        </div>

                         <div class="form_box">
                            <div class="form_single">
                                <label>
                                    Jenis Pembiayaan</label>
                                <asp:Label ID="E_lblJenisPembiayaan" runat="server" Visible="True"></asp:Label>
                            </div>
                        </div>
--%>
                       

                       <%--  <div class="form_box_hide">
                        <div class="form_box_header form_box_hide">
                            <div class="form_single">
                                <h5>
                                    Gross Yield Rate
                                </h5>
                            </div>
                        </div>

                       
                        <div class="form_box_hide">
                            <div class="form_single">
                                <label class="label_req">
                                    From HO</label>
                                <asp:TextBox onblur="GrossYieldRate();" ID="E_txtGrossYieldRate" runat="server" MaxLength="9">0</asp:TextBox>%
                                <asp:DropDownList ID="E_cboGrossYieldRateBehaviour" runat="server" Visible="false">
                                    <asp:ListItem Value="N">Minimum</asp:ListItem>
                                </asp:DropDownList>
                                <input id="Hide_GrossYieldRate" type="hidden" name="Hide_GrossYieldRate" runat="server" />
                                <input id="Hide_GrossYieldRateBeh" type="hidden" name="Hide_GrossYieldRateBeh" runat="server" />--%>
                               <%-- <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" ControlToValidate="E_txtGrossYieldRate"
                                    CssClass="validator_general" ErrorMessage="Gross Yield Rate Salah" Display="Dynamic"></asp:RequiredFieldValidator>
                                <asp:RangeValidator ID="rgv_GrossYield" runat="server" ControlToValidate="E_txtGrossYieldRate"
                                    CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Gross Yield Rate Salah"
                                    MaximumValue="100"></asp:RangeValidator>--%>
                          <%--  </div>
                        </div>
                        <div class="form_box_hide">
                            <div class="form_single">
                                <label>
                                    Branch</label>
                                <asp:Label ID="lblGrossYieldRate_Branch" runat="server"></asp:Label>
                            </div>
                        </div> 

                        </div>
                         <div class="form_box_title">
                                <div class="form_single"> <h4> SKEMA </h4> </div>
                            </div>
                
                        <div class="form_box_header">
                            <div class="form_single">
                                <h5>
                                    Jangka Waktu Maximum
                                </h5>
                            </div>
                        </div>
                        <div class="form_box">
                            <div class="form_single">
                                <label class="label_req">
                                    From HO</label>
                                <asp:TextBox onblur="MaximumTenor();" ID="E_txtMaximumTenor" runat="server" MaxLength="3">1</asp:TextBox>%
                                <input id="Hide_MaximumTenor" type="hidden" name="Hide_MaximumTenor" runat="server" />
                                <input id="Hide_MaximumTenorBeh" type="hidden" name="Hide_MaximumTenorBeh" runat="server" />
                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator3" runat="server" ControlToValidate="E_txtMaximumTenor"
                                    CssClass="validator_general" ErrorMessage="Jangka Waktu Maximum salah" Display="Dynamic"></asp:RequiredFieldValidator>
                                <asp:RangeValidator ID="Rangevalidator3" runat="server" ControlToValidate="E_txtMaximumTenor"
                                    CssClass="validator_general" Type="Double" MinimumValue="1" ErrorMessage="Jangka Waktu Maximum salah"
                                    MaximumValue="999"></asp:RangeValidator>
                            </div>
                        </div>
                        <div class="form_box">
                            <div class="form_single">
                                <label>
                                    Branch</label>
                                <asp:Label ID="lblMaximumTenor_Branch" runat="server"></asp:Label>
                            </div>
                        </div>

                        <div class="form_box_header">
                            <div class="form_single">
                                <h5>
                                    Jangka Waktu Minimum
                                </h5>
                            </div>
                        </div>
                        <div class="form_box">
                            <div class="form_single">
                                <label class="label_req">
                                    From HO</label>
                                <asp:TextBox onblur="MinimumTenor();" ID="E_txtMinimumTenor" runat="server" MaxLength="3">1</asp:TextBox>%
                                <input id="Hide_MinimumTenor" type="hidden" name="Hide_MinimumTenor" runat="server" />
                                <input id="Hide_MinimumTenorBeh" type="hidden" name="Hide_MinimumTenorBeh" runat="server" />
                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" ControlToValidate="E_txtMinimumTenor"
                                    CssClass="validator_general" ErrorMessage="Jangka Waktu Minimum salah" Display="Dynamic"></asp:RequiredFieldValidator>
                                <asp:RangeValidator ID="Rangevalidator2" runat="server" ControlToValidate="E_txtMinimumTenor"
                                    CssClass="validator_general" Type="Double" MinimumValue="1" ErrorMessage="Jangka Waktu Minimum salah"
                                    MaximumValue="999"></asp:RangeValidator>
                            </div>
                        </div>
                        <div class="form_box">
                            <div class="form_single">
                                <label>
                                    Branch</label>
                                <asp:Label ID="lblMinimumTenor_Branch" runat="server"></asp:Label>
                            </div>
                        </div>

                         <div class="form_box_header">
                            <div class="form_single">
                                <h5>
                                    Angsuran Pertama
                                </h5>
                            </div>
                        </div>
                          <div class="form_box">
                            <div class="form_single">
                                <label class="label_req">From HO</label>
                                <asp:DropDownList ID="E_cboAngsuranPertama" runat="server" >
                                            <asp:ListItem Value="ARR" Selected="True">ARREAR</asp:ListItem>
                                            <asp:ListItem Value="ADV">ADVANCE</asp:ListItem> 
                                 </asp:DropDownList>
                                 <input id="Hide_AngsuranPertama" type="hidden" name="Hide_AngsuranPertama" runat="server" />
                             </div>
                        </div>
                        <div class="form_box">
                            <div class="form_single">
                                <label>Branch</label>
                                <asp:Label ID="lblAngsuranPertama_Branch" runat="server"></asp:Label>
                            </div>
                        </div>


                        <div class="form_box_header">
                            <div class="form_single">
                                <h5>
                                    Suku Bunga Flat
                                </h5>
                            </div>
                        </div>


                        <div class="form_box">
                        <div class="form_single">
                        <label class="label_req"> From HO</label>
                           <asp:TextBox ID="E_txtFlatRate" runat="server"  MaxLength="9">0</asp:TextBox>%
                                        <asp:DropDownList ID="E_cboFlatRateBehaviour" runat="server" >
                                            <asp:ListItem Value="D" Selected="True">Default</asp:ListItem>
                                            <asp:ListItem Value="L">Locked</asp:ListItem>
                                            <asp:ListItem Value="N">Minimum</asp:ListItem>
                                            <asp:ListItem Value="X">Maximum</asp:ListItem>
                                        </asp:DropDownList>
                                        <input id="Hide_FlatRate" type="hidden" name="Hide_FlatRate" runat="server" />
                                        <input id="Hide_FlatRateBeh" type="hidden" name="Hide_FlatRateBeh" runat="server" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator67" runat="server" ControlToValidate="E_txtFlatRate"
                                            CssClass="validator_general" ErrorMessage="Suku bunga Flat salah" Enabled="True"
                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                        <asp:RangeValidator ID="RangeValidator55" runat="server" ControlToValidate="E_txtFlatRate"
                                            CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Seku bunga Flat salah"
                                            Enabled="True" MaximumValue="100"></asp:RangeValidator>
                                    </div>
                         </div>
                        </div>
                        <div class="form_box">
                            <div class="form_single">
                                <label>
                                    Branch</label>
                                <asp:Label ID="lblFlatRate_Branch" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="form_box_header">
                            <div class="form_single">
                                <h5>
                                    Suku Bunga Effective
                                </h5>
                            </div>
                        </div>
                        <div class="form_box">
                            <div class="form_single">
                                <label class="label_req">
                                    From HO</label>
                                <asp:TextBox onblur="EffectiveRate();" ID="E_txtEffectiveRate" runat="server" MaxLength="9">0</asp:TextBox>%
                                <asp:DropDownList ID="E_cboEffectiveRateBehaviour" runat="server">
                                    <asp:ListItem Value="D" Selected="True">Default</asp:ListItem>
                                    <asp:ListItem Value="L">Locked</asp:ListItem>
                                    <asp:ListItem Value="N">Minimum</asp:ListItem>
                                    <asp:ListItem Value="X">Maximum</asp:ListItem>
                                </asp:DropDownList>
                                <input id="Hide_EffectiveRate" type="hidden" name="Hide_EffectiveRate" runat="server" />
                                <input id="Hide_EffectiveRateBeh" type="hidden" name="Hide_EffectiveRateBeh" runat="server" />
                                <asp:RequiredFieldValidator ID="rfvEffectiveRate" runat="server" ControlToValidate="E_txtEffectiveRate"
                                    CssClass="validator_general" ErrorMessage="Suku bunga Effective salah" Display="Dynamic"></asp:RequiredFieldValidator>
                                <asp:RangeValidator ID="rgv_EffectiveRate" runat="server" ControlToValidate="E_txtEffectiveRate"
                                    CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Suku bunga Effective salah"
                                    MaximumValue="100"></asp:RangeValidator>
                            </div>
                        </div>
                        <div class="form_box">
                            <div class="form_single">
                                <label>
                                    Branch</label>
                                <asp:Label ID="lblEffectiveRate_Branch" runat="server"></asp:Label>
                            </div>
                        </div>

                
                        <div class="form_box_header">
                            <div class="form_single">
                                <h5>
                                    Opsi Uang Muka
                                </h5>
                            </div>
                        </div>
                         <div class="form_box">
                            <div class="form_single">
                                <label class="label_req"> From HO</label>
                                        <asp:DropDownList ID="E_cboOpsiUangMuka" runat="server" >
                                            <asp:ListItem Value="P" Selected="True">PROSENTASE</asp:ListItem>
                                            <asp:ListItem Value="A">ANGSURAN</asp:ListItem> 
                                        </asp:DropDownList>
                                          <input id="Hide_OpsiUangMuka" type="hidden" name="Hide_AOpsiUangMuka" runat="server" />
                                     </div>
                           </div>
                        <div class="form_box">
                            <div class="form_single">
                                <label> Branch</label>
                                <asp:Label ID="lblOpsiUangMuka_Branch" runat="server"></asp:Label>
                            </div>
                        </div>

                        <div class="form_box_header">
                            <div class="form_single">
                                <h5>
                                   Uang Muka (%)
                                </h5>
                            </div>
                        </div>
                        <div class="form_box">
                            <div class="form_single">
                                <label class="label_req">
                                    From HO</label>
                                <asp:TextBox onblur="DPPercentage();" ID="E_txtDPPercentage" runat="server" MaxLength="9">0</asp:TextBox>%
                                <asp:DropDownList ID="E_cboDPPercentageBehaviour" runat="server">
                                    <asp:ListItem Value="D" Selected="True">Default</asp:ListItem>
                                    <asp:ListItem Value="L">Locked</asp:ListItem>
                                    <asp:ListItem Value="N">Minimum</asp:ListItem>
                                    <asp:ListItem Value="X">Maximum</asp:ListItem>
                                </asp:DropDownList>
                                <input id="Hide_DPPercentage" type="hidden" name="Hide_DPPercentage" runat="server" />
                                <input id="Hide_DPPercentageBeh" type="hidden" name="Hide_DPPercentageBeh" runat="server" />
                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator24" runat="server" ControlToValidate="E_txtDPPercentage"
                                    CssClass="validator_general" ErrorMessage="Prosentase Minimum DP Salah" Display="Dynamic"></asp:RequiredFieldValidator>
                                <asp:RangeValidator ID="rgv_DPPercentage" runat="server" ControlToValidate="E_txtDPPercentage"
                                    CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Prosentase Minimum DP Salah"
                                    MaximumValue="100"></asp:RangeValidator>
                            </div>
                        </div>
                        <div class="form_box">
                            <div class="form_single">
                                <label>
                                    Branch</label>
                                <asp:Label ID="lblDPPercentage_Branch" runat="server"></asp:Label>
                            </div>
                        </div>
                
                         <div class="form_box_header">
                            <div class="form_single"> <h5> Uang Muka (Angsuran) </h5> </div>
                        </div>
                          <div class="form_box">
                            <div class="form_single">
                                <label>From HO</label>
                                        <asp:TextBox ID="txtUangMukaAngsuran" runat="server"  MaxLength="9">0</asp:TextBox>x 
                                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator68" runat="server" ControlToValidate="txtUangMukaAngsuran"
                                            CssClass="validator_general" ErrorMessage="Uang Muka (Angsuran) Salah" Display="Dynamic"></asp:RequiredFieldValidator>
                                        <asp:RangeValidator ID="Rangevalidator56" runat="server" ControlToValidate="txtUangMukaAngsuran"
                                            CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Uang Muka (Angsuran) Salah"
                                            Display="Dynamic"  MaximumValue="500"></asp:RangeValidator>
                                      <input id="Hide_UangMukaAngsuran" type="hidden" name="Hide_UangMukaAngsuran" runat="server" />
                             </div>
                        </div> 
                        <div class="form_box">
                            <div class="form_single">
                                <label>
                                    Branch</label>
                                <asp:Label ID="lblUangMukaAngsuran_Branch" runat="server"></asp:Label>
                            </div>
                        </div>


                         <div class="form_box_header">
                            <div class="form_single">
                                <h5>
                                    Masa Berlaku PO (dalam hari)
                                </h5>
                            </div>
                        </div>
                        <div class="form_box">
                            <div class="form_single">
                                <label class="label_req">
                                    From HO</label>
                                <asp:TextBox onblur="POExpirationDays();" ID="E_txtPOExpirationDays" runat="server"
                                    MaxLength="4">0</asp:TextBox>days
                                <asp:DropDownList ID="E_cboPOExpirationDaysBehaviour" runat="server">
                                    <asp:ListItem Value="D" Selected="True">Default</asp:ListItem>
                                    <asp:ListItem Value="L">Locked</asp:ListItem>
                                    <asp:ListItem Value="N">Minimum</asp:ListItem>
                                    <asp:ListItem Value="X">Maximum</asp:ListItem>
                                </asp:DropDownList>
                                <input id="Hide_POExpirationDays" type="hidden" name="Hide_POExpirationDays" runat="server" />
                                <input id="Hide_POExpirationDaysBeh" type="hidden" name="Hide_POExpirationDaysBeh"
                                    runat="server" />
                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator22" runat="server" ControlToValidate="E_txtPOExpirationDays"
                                    CssClass="validator_general" ErrorMessage="Masa Berlaku PO salah" Display="Dynamic"></asp:RequiredFieldValidator>
                                <asp:RangeValidator ID="rgv_POExpirationDays" runat="server" ControlToValidate="E_txtPOExpirationDays"
                                    CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Masa Berlaku PO salah"
                                    MaximumValue="9999"></asp:RangeValidator>
                            </div>
                        </div>
                        <div class="form_box">
                            <div class="form_single">
                                <label>
                                    Branch</label>
                                <asp:Label ID="lblPOExpirationDays_Branch" runat="server"></asp:Label>
                            </div>
                        </div>--%> 
                     
                     <%--   <div>

                        <div class="form_box_title">
                                <div class="form_single"> <h4> BIAYA </h4> </div>
                            </div>
                            <div class="form_box_header">
                                <div class="form_single">
                                    <h5>
                                        Biaya Pembatalan
                                    </h5>
                                </div>
                            </div>
                            <div class="form_box">
                                <div class="form_single">
                                    <label class="label_req">
                                        From HO</label>
                                    <asp:TextBox onblur="CancellationFee();" ID="E_txtCancellationFee" runat="server"
                                        MaxLength="10">0</asp:TextBox>
                                    <asp:DropDownList ID="E_cboCancellationFeeBehaviour" runat="server">
                                        <asp:ListItem Value="D" Selected="True">Default</asp:ListItem>
                                        <asp:ListItem Value="L">Locked</asp:ListItem>
                                        <asp:ListItem Value="N">Minimum</asp:ListItem>
                                        <asp:ListItem Value="X">Maximum</asp:ListItem>
                                    </asp:DropDownList>
                                    <input id="Hide_CancellationFee" type="hidden" name="Hide_CancellationFee" runat="server" />
                                    <input id="Hide_CancellationFeeBeh" type="hidden" name="Hide_CancellationFeeBeh"
                                        runat="server" />
                                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator6" runat="server" ControlToValidate="E_txtCancellationFee"
                                        CssClass="validator_general" ErrorMessage="Biaya Pembatalan Salah" Display="Dynamic"></asp:RequiredFieldValidator>
                                    <asp:RangeValidator ID="rgv_CancellationFee" runat="server" ControlToValidate="E_txtCancellationFee"
                                        CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Biaya Pembatalan Salah"
                                        MaximumValue="9999999999"></asp:RangeValidator>
                                </div>
                            </div>
                            <div class="form_box">
                                <div class="form_single">
                                    <label>
                                        Branch</label>
                                    <asp:Label ID="lblCancellationFee_Branch" runat="server"></asp:Label>
                                </div>
                            </div>

                            <div class="form_box_header">
                                <div class="form_single">
                                    <h5>
                                        Biaya Administrasi
                                    </h5>
                                </div>
                            </div>
                            <div class="form_box">
                                <div class="form_single">
                                    <label class="label_req">
                                        From HO</label>
                                    <asp:TextBox onblur="AdminFee();" ID="E_txtAdminFee" runat="server" MaxLength="10">0</asp:TextBox>
                                    <asp:DropDownList ID="E_cboAdminFeeBehaviour" runat="server">
                                        <asp:ListItem Value="D" Selected="True">Default</asp:ListItem>
                                        <asp:ListItem Value="L">Locked</asp:ListItem>
                                        <asp:ListItem Value="N">Minimum</asp:ListItem>
                                        <asp:ListItem Value="X">Maximum</asp:ListItem>
                                    </asp:DropDownList>
                                    <input id="Hide_AdminFee" type="hidden" name="Hide_AdminFee" runat="server" />
                                    <input id="Hide_AdminFeeBeh" type="hidden" name="Hide_AdminFeeBeh" runat="server" />
                                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator17" runat="server" ControlToValidate="E_txtAdminFee"
                                        CssClass="validator_general" ErrorMessage="Biaya Administrasi Salah" Display="Dynamic"></asp:RequiredFieldValidator>
                                    <asp:RangeValidator ID="rgv_AdminFee" runat="server" ControlToValidate="E_txtAdminFee"
                                        CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Biaya Administrasi Salah"
                                        MaximumValue="9999999999"></asp:RangeValidator>
                                </div>
                            </div>
                            <div class="form_box">
                                <div class="form_single">
                                    <label>
                                        Branch</label>
                                    <asp:Label ID="lblAdminFee_Branch" runat="server"></asp:Label>
                                </div>
                            </div>

                            <div class="form_box_header">
                                <div class="form_single">
                                    <h5>
                                        Biaya Fidusia
                                    </h5>
                                </div>
                            </div>
                            <div class="form_box">
                                <div class="form_single">
                                    <label class="label_req">
                                        From HO</label>
                                    <asp:TextBox onblur="FiduciaFee();" ID="E_txtFiduciaFee" runat="server" MaxLength="10">0</asp:TextBox>
                                    <asp:DropDownList ID="E_cboFiduciaFeeBehaviour" runat="server">
                                        <asp:ListItem Value="D" Selected="True">Default</asp:ListItem>
                                        <asp:ListItem Value="L">Locked</asp:ListItem>
                                        <asp:ListItem Value="N">Minimum</asp:ListItem>
                                        <asp:ListItem Value="X">Maximum</asp:ListItem>
                                    </asp:DropDownList>
                                    <input id="Hide_FiduciaFee" type="hidden" name="Hide_FiduciaFee" runat="server" />
                                    <input id="Hide_FiduciaFeeBeh" type="hidden" name="Hide_FiduciaFeeBeh" runat="server" />
                                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator80" runat="server" ControlToValidate="E_txtFiduciaFee"
                                        CssClass="validator_general" ErrorMessage="Biaya Fidusia Salah" Display="Dynamic"></asp:RequiredFieldValidator>
                                    <asp:RangeValidator ID="rgv_FiduciaFee" runat="server" ControlToValidate="E_txtFiduciaFee"
                                        CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Biaya Fidusia Salah"
                                        MaximumValue="9999999999"></asp:RangeValidator>
                                </div>
                            </div>
                            <div class="form_box">
                                <div class="form_single">
                                    <label>
                                        Branch</label>
                                    <asp:Label ID="lblFiduciaFee_Branch" runat="server"></asp:Label>
                                </div>
                            </div>

                            <div class="form_box_header">
                                <div class="form_single">
                                    <h5>
                                        Biaya Provisi
                                    </h5>
                                </div>
                            </div>
                            <div class="form_box">
                                <div class="form_single">
                                    <label class="label_req">
                                        From HO</label>
                                    <asp:TextBox onblur="ProvisionFee();" ID="E_txtProvisionFee" runat="server" MaxLength="10">0</asp:TextBox>
                                    <asp:DropDownList ID="E_cboProvisionFeeBehaviour" runat="server">
                                        <asp:ListItem Value="D" Selected="True">Default</asp:ListItem>
                                        <asp:ListItem Value="L">Locked</asp:ListItem>
                                        <asp:ListItem Value="N">Minimum</asp:ListItem>
                                        <asp:ListItem Value="X">Maximum</asp:ListItem>
                                    </asp:DropDownList>
                                    <input id="Hide_ProvisionFee" type="hidden" name="Hide_ProvisionFee" runat="server" />
                                    <input id="Hide_ProvisionFeeBeh" type="hidden" name="Hide_ProvisionFeeBeh" runat="server" />
                                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator90" runat="server" ControlToValidate="E_txtProvisionFee"
                                        CssClass="validator_general" ErrorMessage="Biaya Provisi Salah" Display="Dynamic"></asp:RequiredFieldValidator>
                                    <asp:RangeValidator ID="rgv_ProvisionFee" runat="server" ControlToValidate="E_txtProvisionFee"
                                        CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Biaya Provisi Salah"
                                        MaximumValue="9999999999"></asp:RangeValidator>
                                </div>
                            </div>
                            <div class="form_box">
                                <div class="form_single">
                                    <label>
                                        Branch</label>
                                    <asp:Label ID="lblProvisionFee_Branch" runat="server"></asp:Label>
                                </div>
                            </div>

                            <div class="form_box_header">
                                <div class="form_single">
                                    <h5>
                                        Biaya Notaris
                                    </h5>
                                </div>
                            </div>
                            <div class="form_box">
                                <div class="form_single">
                                    <label class="label_req">
                                        From HO</label>
                                    <asp:TextBox onblur="NotaryFee();" ID="E_txtNotaryFee" runat="server" MaxLength="10">0</asp:TextBox>
                                    <asp:DropDownList ID="E_cboNotaryFeeBehaviour" runat="server">
                                        <asp:ListItem Value="D" Selected="True">Default</asp:ListItem>
                                        <asp:ListItem Value="L">Locked</asp:ListItem>
                                        <asp:ListItem Value="N">Minimum</asp:ListItem>
                                        <asp:ListItem Value="X">Maximum</asp:ListItem>
                                    </asp:DropDownList>
                                    <input id="Hide_NotaryFee" type="hidden" name="Hide_NotaryFee" runat="server" />
                                    <input id="Hide_NotaryFeeBeh" type="hidden" name="Hide_NotaryFeeBeh" runat="server" />
                                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator10" runat="server" ControlToValidate="E_txtNotaryFee"
                                        CssClass="validator_general" ErrorMessage="Biaya Notaris Salah" Display="Dynamic"></asp:RequiredFieldValidator>
                                    <asp:RangeValidator ID="rgv_NotaryFee" runat="server" ControlToValidate="E_txtNotaryFee"
                                        CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Biaya Notaris Salah"
                                        MaximumValue="9999999999"></asp:RangeValidator>
                                </div>
                            </div>
                            <div class="form_box">
                                <div class="form_single">
                                    <label>
                                        Branch</label>
                                    <asp:Label ID="lblNotaryFee_Branch" runat="server"></asp:Label>
                                </div>
                            </div>

                            <div class="form_box_header">
                                <div class="form_single">
                                    <h5>
                                        Biaya Survey
                                    </h5>
                                </div>
                            </div>
                            <div class="form_box">
                                <div class="form_single">
                                    <label class="label_req">
                                        From HO</label>
                                    <asp:TextBox onblur="SurveyFee();" ID="E_txtSurveyFee" runat="server" MaxLength="10">0</asp:TextBox>
                                    <asp:DropDownList ID="E_cboSurveyFeeBehaviour" runat="server">
                                        <asp:ListItem Value="D" Selected="True">Default</asp:ListItem>
                                        <asp:ListItem Value="L">Locked</asp:ListItem>
                                        <asp:ListItem Value="N">Minimum</asp:ListItem>
                                        <asp:ListItem Value="X">Maximum</asp:ListItem>
                                    </asp:DropDownList>
                                    <input id="Hide_SurveyFee" type="hidden" name="Hide_SurveyFee" runat="server" />
                                    <input id="Hide_SurveyFeeBeh" type="hidden" name="Hide_SurveyFeeBeh" runat="server" />
                                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator11" runat="server" ControlToValidate="E_txtSurveyFee"
                                        CssClass="validator_general" ErrorMessage="Biaya Survey salah" Display="Dynamic"></asp:RequiredFieldValidator>
                                    <asp:RangeValidator ID="rgv_SurveyFee" runat="server" ControlToValidate="E_txtSurveyFee"
                                        CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Biaya Survey salah"
                                        MaximumValue="9999999999"></asp:RangeValidator>
                                </div>
                            </div>
                            <div class="form_box">
                                <div class="form_single">
                                    <label>
                                        Branch</label>
                                    <asp:Label ID="lblSurveyFee_Branch" runat="server"></asp:Label>
                                </div>
                            </div>

                            <div class="form_box_header">
                                <div class="form_single">
                                    <h5>
                                        Biaya Kunjungan
                                    </h5>
                                </div>
                            </div>
                            <div class="form_box">
                                <div class="form_single">
                                    <label class="label_req">
                                        From HO</label>
                                    <asp:TextBox onblur="VisitFee();" ID="E_txtVisitFee" runat="server" MaxLength="10">0</asp:TextBox>
                                    <asp:DropDownList ID="E_cboVisitFeeBehaviour" runat="server">
                                        <asp:ListItem Value="D" Selected="True">Default</asp:ListItem>
                                        <asp:ListItem Value="L">Locked</asp:ListItem>
                                        <asp:ListItem Value="N">Minimum</asp:ListItem>
                                        <asp:ListItem Value="X">Maximum</asp:ListItem>
                                    </asp:DropDownList>
                                    <input id="Hide_VisitFee" type="hidden" name="Hide_VisitFee" runat="server" />
                                    <input id="Hide_VisitFeeBeh" type="hidden" name="Hide_VisitFeeBeh" runat="server" />
                                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator12" runat="server" ControlToValidate="E_txtVisitFee"
                                        CssClass="validator_general" ErrorMessage="Biaya Kunjungan Salah" Display="Dynamic"></asp:RequiredFieldValidator>
                                    <asp:RangeValidator ID="rgv_VisitFee" runat="server" ControlToValidate="E_txtVisitFee"
                                        CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Biaya Kunjungan Salah"
                                        MaximumValue="9999999999"></asp:RangeValidator>
                                </div>
                            </div>
                            <div class="form_box">
                                <div class="form_single">
                                    <label>
                                        Branch</label>
                                    <asp:Label ID="lblVisitFee_Branch" runat="server"></asp:Label>
                                </div>
                            </div>

                            <div class="form_box_header">
                                <div class="form_single">
                                    <h5>
                                        Biaya Reschedulling
                                    </h5>
                                </div>
                            </div>
                            <div class="form_box">
                                <div class="form_single">
                                    <label class="label_req">
                                        From HO</label>
                                    <asp:TextBox onblur="ReschedulingFee();" ID="E_txtReschedulingFee" runat="server"
                                        MaxLength="10">0</asp:TextBox>
                                    <asp:DropDownList ID="E_cboReschedulingFeeBehaviour" runat="server">
                                        <asp:ListItem Value="D" Selected="True">Default</asp:ListItem>
                                        <asp:ListItem Value="L">Locked</asp:ListItem>
                                        <asp:ListItem Value="N">Minimum</asp:ListItem>
                                        <asp:ListItem Value="X">Maximum</asp:ListItem>
                                    </asp:DropDownList>
                                    <input id="Hide_ReschedulingFee" type="hidden" name="Hide_ReschedulingFee" runat="server" />
                                    <input id="Hide_ReschedulingFeeBeh" type="hidden" name="Hide_ReschedulingFeeBeh"
                                        runat="server" />
                                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator13" runat="server" ControlToValidate="E_txtReschedulingFee"
                                        CssClass="validator_general" ErrorMessage="Biaya Rescheduling Salah" Display="Dynamic"></asp:RequiredFieldValidator>
                                    <asp:RangeValidator ID="rgv_ReschedulingFee" runat="server" ControlToValidate="E_txtReschedulingFee"
                                        CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Biaya Rescheduling Salah"
                                        MaximumValue="9999999999"></asp:RangeValidator>
                                </div>
                            </div>
                            <div class="form_box">
                                <div class="form_single">
                                    <label>
                                        Branch</label>
                                    <asp:Label ID="lblReschedulingFee_Branch" runat="server"></asp:Label>
                                </div>
                            </div>

                            <div class="form_box_header">
                                <div class="form_single">
                                    <h5>
                                        Biaya Over Kontrak
                                    </h5>
                                </div>
                            </div>
                            <div class="form_box">
                                <div class="form_single">
                                    <label class="label_req">
                                        From HO</label>
                                    <asp:TextBox onblur="AgreementTransferFee();" ID="E_txtAgreementTransferFee" runat="server"
                                        MaxLength="10">0</asp:TextBox>
                                    <asp:DropDownList ID="E_cboAgreementTransferFeeBehaviour" runat="server">
                                        <asp:ListItem Value="D" Selected="True">Default</asp:ListItem>
                                        <asp:ListItem Value="L">Locked</asp:ListItem>
                                        <asp:ListItem Value="N">Minimum</asp:ListItem>
                                        <asp:ListItem Value="X">Maximum</asp:ListItem>
                                    </asp:DropDownList>
                                    <input id="Hide_AgreementTransferFee" type="hidden" name="Hide_AgreementTransferFee"
                                        runat="server" />
                                    <input id="Hide_AgreementTransferFeeBeh" type="hidden" name="Hide_AgreementTransferFeeBeh"
                                        runat="server" />
                                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator14" runat="server" ControlToValidate="E_txtAgreementTransferFee"
                                        CssClass="validator_general" ErrorMessage="Biaya Over Kontrak salah" Display="Dynamic"></asp:RequiredFieldValidator>
                                    <asp:RangeValidator ID="rgv_AgreementTransferFee" runat="server" ControlToValidate="E_txtAgreementTransferFee"
                                        CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Biaya Over Kontrak salah"
                                        MaximumValue="9999999999"></asp:RangeValidator>
                                </div>
                            </div>
                            <div class="form_box">
                                <div class="form_single">
                                    <label>
                                        Branch</label>
                                    <asp:Label ID="lblAgreementTransferFee_Branch" runat="server"></asp:Label>
                                </div>
                            </div>

                            <div class="form_box_header">
                                <div class="form_single">
                                    <h5>
                                        Biaya Ganti Tgl jatuh Tempo
                                    </h5>
                                </div>
                            </div>
                            <div class="form_box">
                                <div class="form_single">
                                    <label class="label_req">
                                        From HO</label>
                                    <asp:TextBox onblur="ChangeDueDateFee();" ID="E_txtChangeDueDateFee" runat="server"
                                        MaxLength="10">0</asp:TextBox>
                                    <asp:DropDownList ID="E_cboChangeDueDateFee" runat="server">
                                        <asp:ListItem Value="D" Selected="True">Default</asp:ListItem>
                                        <asp:ListItem Value="L">Locked</asp:ListItem>
                                        <asp:ListItem Value="N">Minimum</asp:ListItem>
                                        <asp:ListItem Value="X">Maximum</asp:ListItem>
                                    </asp:DropDownList>
                                    <input id="Hide_ChangeDueDateFee" type="hidden" name="Hide_ChangeDueDateFee" runat="server" />
                                    <input id="Hide_ChangeDueDateFeeBeh" type="hidden" name="Hide_ChangeDueDateFeeBeh"
                                        runat="server" />
                                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator15" runat="server" ControlToValidate="E_txtChangeDueDateFee"
                                        CssClass="validator_general" ErrorMessage="Biaya Ganti Tgl jatuh Tempo Salah"
                                        Display="Dynamic"></asp:RequiredFieldValidator>
                                    <asp:RangeValidator ID="rgv_ChangeDueDateFee" runat="server" ControlToValidate="E_txtChangeDueDateFee"
                                        CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Biaya Ganti Tgl jatuh Tempo Salah"
                                        MaximumValue="9999999999"></asp:RangeValidator>
                                </div>
                            </div>
                            <div class="form_box">
                                <div class="form_single">
                                    <label>
                                        Branch</label>
                                    <asp:Label ID="lblChangeDueDateFee_Branch" runat="server"></asp:Label>
                                </div>
                            </div>

                            <div class="form_box_header">
                                <div class="form_single">
                                    <h5>
                                        Biaya Penggantian Asset
                                    </h5>
                                </div>
                            </div>
                            <div class="form_box">
                                <div class="form_single">
                                    <label class="label_req">
                                        From HO</label>
                                    <asp:TextBox onblur="AssetReplacementFee();" ID="E_txtAssetReplacementFee" runat="server"
                                        MaxLength="10">0</asp:TextBox>
                                    <asp:DropDownList ID="E_cboAssetReplacementFeeBehaviour" runat="server">
                                        <asp:ListItem Value="D" Selected="True">Default</asp:ListItem>
                                        <asp:ListItem Value="L">Locked</asp:ListItem>
                                        <asp:ListItem Value="N">Minimum</asp:ListItem>
                                        <asp:ListItem Value="X">Maximum</asp:ListItem>
                                    </asp:DropDownList>
                                    <input id="Hide_AssetReplacementFee" type="hidden" name="Hide_AssetReplacementFee"
                                        runat="server" />
                                    <input id="Hide_AssetReplacementFeeBeh" type="hidden" name="Hide_AssetReplacementFeeBeh"
                                        runat="server" />
                                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator16" runat="server" ControlToValidate="E_txtAssetReplacementFee"
                                        CssClass="validator_general" ErrorMessage="Biaya Penggantian Asset salah" Display="Dynamic"></asp:RequiredFieldValidator>
                                    <asp:RangeValidator ID="rgv_AssetReplacementFee" runat="server" ControlToValidate="E_txtAssetReplacementFee"
                                        CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Biaya Penggantian Asset salah"
                                        MaximumValue="9999999999"></asp:RangeValidator>
                                </div>
                            </div>
                            <div class="form_box">
                                <div class="form_single">
                                    <label>
                                        Branch</label>
                                    <asp:Label ID="lblAssetReplacementFee_Branch" runat="server"></asp:Label>
                                </div>
                            </div>

                            <div class="form_box_header">
                                <div class="form_single">
                                    <h5>
                                        Biaya Penarikan
                                    </h5>
                                </div>
                            </div>
                            <div class="form_box">
                                <div class="form_single">
                                    <label class="label_req">
                                        From HO</label>
                                    <asp:TextBox onblur="RepossesFee();" ID="E_txtRepossesFee" runat="server" MaxLength="10">0</asp:TextBox>
                                    <asp:DropDownList ID="E_cboRepossesFeeBehaviour" runat="server">
                                        <asp:ListItem Value="D" Selected="True">Default</asp:ListItem>
                                        <asp:ListItem Value="L">Locked</asp:ListItem>
                                        <asp:ListItem Value="N">Minimum</asp:ListItem>
                                        <asp:ListItem Value="X">Maximum</asp:ListItem>
                                    </asp:DropDownList>
                                    <input id="Hide_RepossesFee" type="hidden" name="Hide_RepossesFee" runat="server" />
                                    <input id="Hide_RepossesFeeBeh" type="hidden" name="Hide_RepossesFeeBeh" runat="server" />
                                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator70" runat="server" ControlToValidate="E_txtRepossesFee"
                                        CssClass="validator_general" ErrorMessage="Biaya Penarikan Salah" Display="Dynamic"></asp:RequiredFieldValidator>
                                    <asp:RangeValidator ID="rgv_RepossesFee" runat="server" ControlToValidate="E_txtRepossesFee"
                                        CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Biaya Penarikan Salah"
                                        MaximumValue="9999999999"></asp:RangeValidator>
                                </div>
                            </div>
                            <div class="form_box">
                                <div class="form_single">
                                    <label>
                                        Branch</label>
                                    <asp:Label ID="lblRepossesFee_Branch" runat="server"></asp:Label>
                                </div>
                            </div>

                            <div class="form_box_header">
                                <div class="form_single">
                                    <h5>
                                        Biaya Legalisasi Dokumen
                                    </h5>
                                </div>
                            </div>
                            <div class="form_box">
                                <div class="form_single">
                                    <label class="label_req">
                                        From HO</label>
                                    <asp:TextBox onblur="LegalizedDocumentFee();" ID="E_LegalizedDocumentFee" runat="server"
                                        MaxLength="10">0</asp:TextBox>
                                    <asp:DropDownList ID="E_cboLegalizedDocumentFeeBehaviour" runat="server">
                                        <asp:ListItem Value="D" Selected="True">Default</asp:ListItem>
                                        <asp:ListItem Value="L">Locked</asp:ListItem>
                                        <asp:ListItem Value="N">Minimum</asp:ListItem>
                                        <asp:ListItem Value="X">Maximum</asp:ListItem>
                                    </asp:DropDownList>
                                    <input id="Hide_LegalizedDocumentFee" type="hidden" name="Hide_LegalizedDocumentFee"
                                        runat="server" />
                                    <input id="Hide_LegalizedDocumentFeeBeh" type="hidden" name="Hide_LegalizedDocumentFeeBeh"
                                        runat="server" />
                                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator18" runat="server" ControlToValidate="E_LegalizedDocumentFee"
                                        CssClass="validator_general" ErrorMessage="Biaya Legalisasi Dokumen Salah" Display="Dynamic"></asp:RequiredFieldValidator>
                                    <asp:RangeValidator ID="rgv_LegalizedDocumentFee" runat="server" ControlToValidate="E_LegalizedDocumentFee"
                                        CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Biaya Legalisasi Dokumen Salah"
                                        MaximumValue="9999999999"></asp:RangeValidator>
                                </div>
                            </div>
                            <div class="form_box">
                                <div class="form_single">
                                    <label>
                                        Branch</label>
                                    <asp:Label ID="lblLegalizedDocumentFee_Branch" runat="server"></asp:Label>
                                </div>
                            </div>

                            <div class="form_box_header">
                                <div class="form_single">
                                    <h5>
                                        Biaya Penolakan PDC
                                    </h5>
                                </div>
                            </div>
                            <div class="form_box">
                                <div class="form_single">
                                    <label class="label_req">
                                        From HO</label>
                                    <asp:TextBox onblur="PDCBounceFee();" ID="E_PDCBounceFee" runat="server" MaxLength="10">0</asp:TextBox>
                                    <asp:DropDownList ID="E_cboPDCBounceFeeBehaviour" runat="server">
                                        <asp:ListItem Value="D" Selected="True">Default</asp:ListItem>
                                        <asp:ListItem Value="L">Locked</asp:ListItem>
                                        <asp:ListItem Value="N">Minimum</asp:ListItem>
                                        <asp:ListItem Value="X">Maximum</asp:ListItem>
                                    </asp:DropDownList>
                                    <input id="Hide_PDCBounceFee" type="hidden" name="Hide_PDCBounceFee" runat="server" />
                                    <input id="Hide_PDCBounceFeeBeh" type="hidden" name="Hide_PDCBounceFeeBeh" runat="server" />
                                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator19" runat="server" ControlToValidate="E_PDCBounceFee"
                                        CssClass="validator_general" ErrorMessage="Biaya Penolakan PDC salah" Display="Dynamic"></asp:RequiredFieldValidator>
                                    <asp:RangeValidator ID="rgv_PDCBounceFee" runat="server" ControlToValidate="E_PDCBounceFee"
                                        CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Biaya Penolakan PDC salah"
                                        MaximumValue="9999999999"></asp:RangeValidator>
                                </div>
                            </div>
                            <div class="form_box">
                                <div class="form_single">
                                    <label>
                                        Branch</label>
                                    <asp:Label ID="lblPDCBounceFee_Branch" runat="server"></asp:Label>
                                </div>
                            </div>

                            <div class="form_box_header">
                                <div class="form_single">
                                    <h5>
                                        Biaya Admin Asuransi
                                    </h5>
                                </div>
                            </div>
                            <div class="form_box">
                                <div class="form_single">
                                    <label class="label_req">
                                        From HO</label>
                                    <asp:TextBox onblur="InsuranceAdminFee();" ID="E_txtInsuranceAdminFee" runat="server"
                                        MaxLength="10">0</asp:TextBox>
                                    <asp:DropDownList ID="E_cboInsuranceAdminFeeBehaviour" runat="server">
                                        <asp:ListItem Value="D" Selected="True">Default</asp:ListItem>
                                        <asp:ListItem Value="L">Locked</asp:ListItem>
                                        <asp:ListItem Value="N">Minimum</asp:ListItem>
                                        <asp:ListItem Value="X">Maximum</asp:ListItem>
                                    </asp:DropDownList>
                                    <input id="Hide_InsuranceAdminFee" type="hidden" name="Hide_InsuranceAdminFee" runat="server" />
                                    <input id="Hide_InsuranceAdminFeeBeh" type="hidden" name="Hide_InsuranceAdminFeeBeh"
                                        runat="server" />
                                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator20" runat="server" ControlToValidate="E_txtInsuranceAdminFee"
                                        CssClass="validator_general" ErrorMessage="Biaya Admin Asuransi salah" Display="Dynamic"></asp:RequiredFieldValidator>
                                    <asp:RangeValidator ID="rgv_InsuranceAdminFee" runat="server" ControlToValidate="E_txtInsuranceAdminFee"
                                        CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Biaya Admin Asuransi salah"
                                        MaximumValue="9999999999"></asp:RangeValidator>
                                </div>
                            </div>
                            <div class="form_box">
                                <div class="form_single">
                                    <label>
                                        Branch</label>
                                    <asp:Label ID="lblInsuranceAdminFee_Branch" runat="server"></asp:Label>
                                </div>
                            </div>

                            <div class="form_box_header">
                                <div class="form_single">
                                    <h5>
                                        Biaya Materai Asuransi
                                    </h5>
                                </div>
                            </div>
                            <div class="form_box">
                                <div class="form_single">
                                    <label class="label_req">
                                        From HO</label>
                                    <asp:TextBox onblur="InsuranceStampDutyFee();" ID="E_txtInsuranceStampDutyFee" runat="server"
                                        MaxLength="10">0</asp:TextBox>
                                    <asp:DropDownList ID="E_cboInsuranceStampDutyFeeBehaviour" runat="server">
                                        <asp:ListItem Value="D" Selected="True">Default</asp:ListItem>
                                        <asp:ListItem Value="L">Locked</asp:ListItem>
                                        <asp:ListItem Value="N">Minimum</asp:ListItem>
                                        <asp:ListItem Value="X">Maximum</asp:ListItem>
                                    </asp:DropDownList>
                                    <input id="Hide_InsuranceStampDutyFee" type="hidden" name="Hide_InsuranceStampDutyFee"
                                        runat="server" />
                                    <input id="Hide_InsuranceStampDutyFeeBeh" type="hidden" name="Hide_InsuranceStampDutyFeeBeh"
                                        runat="server" />
                                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator21" runat="server" ControlToValidate="E_txtInsuranceStampDutyFee"
                                        CssClass="validator_general" ErrorMessage="Biaya Materai Asuransi salah" Display="Dynamic"></asp:RequiredFieldValidator>
                                    <asp:RangeValidator ID="rgv_InsuranceStampDutyFee" runat="server" ControlToValidate="E_txtInsuranceStampDutyFee"
                                        CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Biaya Materai Asuransi salah"
                                        MaximumValue="9999999999"></asp:RangeValidator>
                                </div>
                            </div>
                            <div class="form_box">
                                <div class="form_single">
                                    <label>
                                        Branch</label>
                                    <asp:Label ID="lblInsuranceStampDutyFee_Branch" runat="server"></asp:Label>
                                </div>
                            </div>

                            <div class="form_box_header">
                                <div class="form_single">
                                    <h5>
                                        Biaya Polis
                                    </h5>
                                </div>
                            </div>
                            <div class="form_box">
                                <div class="form_single">
                                    <label class="label_req">
                                        From HO</label>
                                    <asp:TextBox onblur="BiayaPolis();" ID="E_txtBiayaPolis" runat="server"
                                        MaxLength="10">0</asp:TextBox>
                                    <asp:DropDownList ID="E_cboBiayaPolisBehaviour" runat="server">
                                        <asp:ListItem Value="D" Selected="True">Default</asp:ListItem>
                                        <asp:ListItem Value="L">Locked</asp:ListItem>
                                        <asp:ListItem Value="N">Minimum</asp:ListItem>
                                        <asp:ListItem Value="X">Maximum</asp:ListItem>
                                    </asp:DropDownList>
                                    <input id="Hide_BiayaPolis" type="hidden" name="Hide_BiayaPolis"
                                        runat="server" />
                                    <input id="Hide_BiayaPolisBeh" type="hidden" name="Hide_BiayaPolisBeh"
                                        runat="server" />
                                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator7" runat="server" ControlToValidate="E_txtBiayaPolis"
                                        CssClass="validator_general" ErrorMessage="Biaya Biaya Polis salah" Display="Dynamic"></asp:RequiredFieldValidator>
                                    <asp:RangeValidator ID="rgv_BiayaPolis" runat="server" ControlToValidate="E_txtBiayaPolis"
                                        CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Biaya Biaya Polis salah"
                                        MaximumValue="9999999999"></asp:RangeValidator>
                                </div>
                            </div>
                            <div class="form_box">
                                <div class="form_single">
                                    <label>
                                        Branch</label>
                                    <asp:Label ID="lblBiayaPolis_Branch" runat="server"></asp:Label>
                                </div>
                            </div>


                        
                        </div>--%>
                        
                   <%--     <div>
                           <div class="form_box_title">
                                <div class="form_single"> <h4> ANGSURAN </h4> </div>
                            </div>

                            <div class="form_box_header">
                            <div class="form_single">
                                <h5>
                                    Prioritas Alokasi Pembayaran</h5>
                            </div>
                        </div>
                        <div class="form_box">
                            <div class="form_single">
                                <label class="label_general">
                                    From HO</label>
                                <asp:RadioButtonList ID="rboPrioritasPembayaran" runat="server" CssClass="opt_single_product"
                                    RepeatDirection="Horizontal">
                                    <asp:ListItem Value="DE" Selected="True">Denda</asp:ListItem>
                                    <asp:ListItem Value="BE">Bebas</asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                        </div>
                        <div class="form_box">
                            <div class="form_single">
                                <label>
                                    Branch</label>
                                <asp:Label ID="lblPrioritasPembayaran_Branch" runat="server"></asp:Label>
                            </div>
                        </div>

                        <div class="form_box_header">
                            <div class="form_single">
                                <h5>
                                    Jumlah Toleransi terhadap Angsuran
                                </h5>
                            </div>
                        </div>
                        <div class="form_box">
                            <div class="form_single">
                                <label class="label_req">
                                    From HO</label>
                                <asp:TextBox onblur="InstallmentToleranceAmount();" ID="E_txtInstallmentToleranceAmount"
                                    runat="server" MaxLength="15">0</asp:TextBox>
                                <asp:DropDownList ID="E_cboInstallmentToleranceAmountBehaviour" runat="server">
                                    <asp:ListItem Value="D" Selected="True">Default</asp:ListItem>
                                    <asp:ListItem Value="L">Locked</asp:ListItem>
                                    <asp:ListItem Value="N">Minimum</asp:ListItem>
                                    <asp:ListItem Value="X">Maximum</asp:ListItem>
                                </asp:DropDownList>
                                <input id="Hide_InstallmentToleranceAmount" type="hidden" name="Hide_InstallmentToleranceAmount"
                                    runat="server" />
                                <input id="Hide_InstallmentToleranceAmountBeh" type="hidden" name="Hide_InstallmentToleranceAmountBeh"
                                    runat="server" />
                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator23" runat="server" ControlToValidate="E_txtInstallmentToleranceAmount"
                                    CssClass="validator_general" ErrorMessage="Jumlah Toleransi terhadap Angsuran salah"
                                    Display="Dynamic"></asp:RequiredFieldValidator>
                                <asp:RangeValidator ID="rgv_InstallmentToleranceAmount" runat="server" ControlToValidate="E_txtInstallmentToleranceAmount"
                                    Type="Double" MinimumValue="0" ErrorMessage="Jumlah Toleransi terhadap Angsuran salah"
                                    CssClass="validator_general" MaximumValue="999999999999"></asp:RangeValidator>
                            </div>
                        </div>
                        <div class="form_box">
                            <div class="form_single">
                                <label>
                                    Branch</label>
                                <asp:Label ID="lblInstallmentToleranceAmount_Branch" runat="server"></asp:Label>
                            </div>
                        </div>

                          <div class="form_box_header">
                            <div class="form_single">
                                <h5>
                                    Prosentase Denda Keterlambatan (Angsuran)
                                </h5>
                            </div>
                        </div>
                        <div class="form_box">
                            <div class="form_single">
                                <label class="label_req">
                                    From HO</label>
                                <asp:TextBox onblur="PenaltyPercentage();" ID="E_txtPenaltyPercentage" runat="server"
                                    MaxLength="9">0</asp:TextBox>‰ / day
                                <asp:DropDownList ID="E_cboPenaltyPercentageBehaviour" runat="server">
                                    <asp:ListItem Value="D" Selected="True">Default</asp:ListItem>
                                    <asp:ListItem Value="L">Locked</asp:ListItem>
                                    <asp:ListItem Value="N">Minimum</asp:ListItem>
                                    <asp:ListItem Value="X">Maximum</asp:ListItem>
                                </asp:DropDownList>
                                <input id="Hide_PenaltyPercentage" type="hidden" name="Hide_PenaltyPercentage" runat="server" />
                                <input id="Hide_PenaltyPercentageBeh" type="hidden" name="Hide_PenaltyPercentageBeh"
                                    runat="server" />
                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator4" runat="server" ControlToValidate="E_txtPenaltyPercentage"
                                    CssClass="validator_general" ErrorMessage="Prosentase Denda Keterlambatan Angsuran salah"
                                    Display="Dynamic"></asp:RequiredFieldValidator>
                                <asp:RangeValidator ID="rgvPenaltyPercentage" runat="server" ControlToValidate="E_txtPenaltyPercentage"
                                    CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Prosentase Denda Keterlambatan Angsuran salah"
                                    MaximumValue="100"></asp:RangeValidator>
                            </div>
                        </div>
                        <div class="form_box">
                            <div class="form_single">
                                <label>
                                    Branch</label>
                                <asp:Label ID="lblPenaltyPercentage_Branch" runat="server"></asp:Label>
                            </div>
                        </div>

                         <div class="form_box_header">
                            <div class="form_single"> <h5> Dasar Perhitungan</h5> </div>
                        </div>
                        <div class="form_box">
                            <div class="form_single">
                                <label class="label_general">
                                    From HO</label>
                                <asp:RadioButtonList ID="rboPenaltyBasedOn" runat="server" CssClass="opt_single_product"
                                    RepeatDirection="Horizontal">
                                    <asp:ListItem Value="PB" Selected="True">Pokok + Bunga</asp:ListItem>
                                    <asp:ListItem Value="PO">Pokok</asp:ListItem>
                                </asp:RadioButtonList>
                                <asp:DropDownList ID="cboPenaltyBasedOn" runat="server">
                                    <asp:ListItem Value="D" Selected="True">Default</asp:ListItem>
                                    <asp:ListItem Value="L">Locked</asp:ListItem>
                                    <asp:ListItem Value="N">Minimum</asp:ListItem>
                                    <asp:ListItem Value="X">Maximum</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="form_box">
                            <div class="form_single">
                                <label>
                                    Branch</label>
                                <asp:Label runat="server" ID="lblPenaltyBasedOn_Branch"></asp:Label>&nbsp;&nbsp;
                                <asp:Label runat="server" ID="lblPenaltyBasedOnBehaviour_Branch"></asp:Label>
                            </div>
                        </div>
                 
                        <div class="form_box_header">
                            <div class="form_single"> <h5> Tanggal Efektif</h5> </div>
                        </div>
                        <div class="form_box">
                            <div class="form_single">
                                <label class="label_req">
                                    From HO</label>
                                <asp:TextBox ID="txtPenaltyRateEffectiveDate" runat="server"></asp:TextBox>
                                <asp:CalendarExtender ID="txtPenaltyRateEffectiveDate_CalendarExtender1" runat="server"
                                    Enabled="True" TargetControlID="txtPenaltyRateEffectiveDate" Format="dd/MM/yyyy">
                                </asp:CalendarExtender>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator61" runat="server" ErrorMessage="*"
                                    CssClass="validator_general" ControlToValidate="txtPenaltyRateEffectiveDate"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="form_box">
                            <div class="form_single">
                                <label>
                                    Branch</label>
                                <asp:Label runat="server" ID="lblPenaltyRateEffectiveDate_Branch"></asp:Label>
                            </div>
                        </div>
                 
                        <div class="form_box_header">
                            <div class="form_single">
                                <h5>
                                    Prosentase Denda Keterlambatan Sebelumnya</h5>
                            </div>
                        </div>
                        <div class="form_box">
                            <div class="form_single">
                                <label>
                                    From HO</label>
                                <asp:TextBox ID="txtPenaltyRatePrevious" runat="server" MaxLength="9">0</asp:TextBox>‰
                                / day
                            </div>
                        </div>
                        <div class="form_box">
                            <div class="form_single">
                                <label>
                                    Branch</label>
                                <asp:Label runat="server" ID="lblPenaltyRatePrevious_Branch"></asp:Label>‰ / day
                            </div>
                        </div>

                        <div class="form_box_header">
                            <div class="form_single">
                                <h5>
                                    Biaya Tagih
                                </h5>
                            </div>
                        </div>
                        <div class="form_box">
                            <div class="form_single">
                                <label class="label_req">
                                    From HO</label>
                                <asp:TextBox onblur="BillingCharges();" ID="E_txtBillingCharges" runat="server" MaxLength="15">0</asp:TextBox>
                                <asp:DropDownList ID="E_cboBillingChargesBehaviour" runat="server">
                                    <asp:ListItem Value="D" Selected="True">Default</asp:ListItem>
                                    <asp:ListItem Value="L">Locked</asp:ListItem>
                                    <asp:ListItem Value="N">Minimum</asp:ListItem>
                                    <asp:ListItem Value="X">Maximum</asp:ListItem>
                                </asp:DropDownList>
                                <input id="Hide_BillingCharges" type="hidden" name="Hide_BillingCharges" runat="server" />
                                <input id="Hide_BillingChargesBeh" type="hidden" name="Hide_BillingChargesBeh" runat="server" />
                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator49" runat="server" ControlToValidate="E_txtBillingCharges"
                                    CssClass="validator_general" ErrorMessage="Biaya Tagih Salah" Display="Dynamic"></asp:RequiredFieldValidator>
                                <asp:RangeValidator ID="rgv_BillingCharges" runat="server" ControlToValidate="E_txtBillingCharges"
                                    CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Biaya Tagih Salah"
                                    MaximumValue="999999999999"></asp:RangeValidator>
                            </div>
                        </div>
                        <div class="form_box">
                            <div class="form_single">
                                <label>
                                    Branch</label>
                                <asp:Label ID="lblBillingCharges_Branch" runat="server"></asp:Label>
                            </div>
                        </div>

                         <div class="form_box_header">
                            <div class="form_single">
                                <h5>
                                    Denda Pelunasan dipercepat
                                </h5>
                            </div>
                        </div>
                        <div class="form_box">
                            <div class="form_single">
                                <label class="label_req">
                                    From HO</label>
                                <asp:TextBox onblur="PrepaymentPenaltyRate();" ID="E_txtPrepaymentPenaltyRate" runat="server"
                                    MaxLength="9">0</asp:TextBox>%
                                <asp:DropDownList ID="E_cboPrepaymentPenaltyRateBehaviour" runat="server">
                                    <asp:ListItem Value="D" Selected="True">Default</asp:ListItem>
                                    <asp:ListItem Value="L">Locked</asp:ListItem>
                                    <asp:ListItem Value="N">Minimum</asp:ListItem>
                                    <asp:ListItem Value="X">Maximum</asp:ListItem>
                                </asp:DropDownList>
                                <input id="Hide_PrepaymentPenaltyRate" type="hidden" name="Hide_PrepaymentPenaltyRate"
                                    runat="server" />
                                <input id="Hide_PrepaymentPenaltyRateBeh" type="hidden" name="Hide_PrepaymentPenaltyRateBeh"
                                    runat="server" />
                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator33" runat="server" ControlToValidate="E_txtPrepaymentPenaltyRate"
                                    CssClass="validator_general" ErrorMessage="Denda Pelunasan dipercepat Salah"
                                    Display="Dynamic"></asp:RequiredFieldValidator>
                                <asp:RangeValidator ID="rgv_PrepaymentPenaltyRate" runat="server" ControlToValidate="E_txtPrepaymentPenaltyRate"
                                    CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Denda Pelunasan dipercepat Salah"
                                    MaximumValue="100"></asp:RangeValidator>
                            </div>
                        </div>
                        <div class="form_box">
                            <div class="form_single">
                                <label>
                                    Branch</label>
                                <asp:Label ID="lblPrepaymentPenaltyRate_Branch" runat="server"></asp:Label>
                            </div>
                        </div>

                        <div class="form_box_header">
                            <div class="form_single">
                                <h5>
                                    Tanggal Efektif</h5>
                            </div>
                        </div>
                        <div class="form_box">
                            <div class="form_single">
                                <label class="label_req">
                                    From HO</label>
                                <asp:TextBox ID="txtPrepaymentPenaltyEffectiveDate" runat="server"></asp:TextBox>
                                <asp:CalendarExtender ID="txtPrepaymentPenaltyEffectiveDate_CalendarExtender" runat="server"
                                    Enabled="True" TargetControlID="txtPrepaymentPenaltyEffectiveDate" Format="dd/MM/yyyy">
                                </asp:CalendarExtender>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator59" runat="server" ErrorMessage="*"
                                    CssClass="validator_general" ControlToValidate="txtPrepaymentPenaltyEffectiveDate"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="form_box">
                            <div class="form_single">
                                <label>
                                    Branch</label>
                                <asp:Label runat="server" ID="lblPrepaymentPenaltyEffectiveDate_Branch"></asp:Label>
                            </div>
                        </div>

                        <div class="form_box_header">
                            <div class="form_single">
                                <h5>
                                    Denda Pelunasan Sebelumnya</h5>
                            </div>
                        </div>
                        <div class="form_box">
                            <div class="form_single">
                                <label>
                                    From HO</label>
                                <asp:TextBox ID="txtPrepaymentPenaltyPrevious" runat="server" MaxLength="9">0</asp:TextBox>‰
                                / day
                            </div>
                        </div>
                        <div class="form_box">
                            <div class="form_single">
                                <label>
                                    Branch</label>
                                <asp:Label ID="lblPrepaymentPenaltyPrevious_Branch" runat="server"></asp:Label>
                                / day
                            </div>
                        </div>

                        <div class="form_box_header">
                            <div class="form_single">
                                <h5>
                                    Denda Tetap Pelunasan dipercepat</h5>
                            </div>
                        </div>
                        <div class="form_box">
                            <div class="form_single">
                                <label class="label_req">
                                    From HO</label>
                                <asp:TextBox ID="txtPrepaymentPenaltyFixed" runat="server" />
                                <asp:DropDownList ID="cboPrepaymentPenaltyFixedBehaviour" runat="server">
                                    <asp:ListItem Value="D" Selected="True">Default</asp:ListItem>
                                    <asp:ListItem Value="L">Locked</asp:ListItem>
                                    <asp:ListItem Value="N">Minimum</asp:ListItem>
                                    <asp:ListItem Value="X">Maximum</asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator58" runat="server" ControlToValidate="txtPrepaymentPenaltyFixed"
                                    CssClass="validator_general" ErrorMessage="Denda Tetap Pelunasan dipercepat tidak boleh kosong"
                                    Display="Dynamic"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="form_box">
                            <div class="form_single">
                                <label>
                                    Branch</label>
                                <asp:Label ID="lblPrepaymentPenaltyFixed_Branch" runat="server"></asp:Label>&nbsp;&nbsp;
                                <asp:Label ID="lblPrepaymentPenaltyFixedBehaviour_Branch" runat="server"></asp:Label>
                            </div>
                        </div>

                        <div class="form_box_header">
                            <div class="form_single">
                                <h5>
                                    Tanggal Efektif</h5>
                            </div>
                        </div>
                        <div class="form_box">
                            <div class="form_single">
                                <label class="label_req">
                                    From HO</label>
                                <asp:TextBox ID="txtPrepaymentPenaltyFixedEffectiveDate" runat="server"></asp:TextBox>
                                <asp:CalendarExtender ID="txtPrepaymentPenaltyFixedEffectiveDate_CalendarExtender1"
                                    runat="server" Enabled="True" TargetControlID="txtPrepaymentPenaltyFixedEffectiveDate"
                                    Format="dd/MM/yyyy">
                                </asp:CalendarExtender>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator60" runat="server" ErrorMessage="*"
                                    CssClass="validator_general" ControlToValidate="txtPrepaymentPenaltyFixedEffectiveDate"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="form_box">
                            <div class="form_single">
                                <label>
                                    Branch</label>
                                <asp:Label ID="lblPrepaymentPenaltyFixedEffectiveDate_Branch" runat="server"></asp:Label>
                            </div>
                        </div>

                        <div class="form_box_header">
                            <div class="form_single">
                                <h5>
                                    Denda Tetap Pelunasan Sebelumnya</h5>
                            </div>
                        </div>
                        <div class="form_box">
                            <div class="form_single">
                                <label>
                                    From HO</label>
                                <asp:TextBox ID="txtPrepaymentPenaltyFixedPrevious" runat="server" MaxLength="9">0</asp:TextBox>
                            </div>
                        </div>
                        <div class="form_box">
                            <div class="form_single">
                                <label>
                                    Branch</label>
                                <asp:Label ID="lblPrepaymentPenaltyFixedPrevious_Branch" runat="server"></asp:Label>
                            </div>
                        </div>


                        </div>--%>
                        <%--
                        <div>
                      <div class="form_box_title">
                                <div class="form_single"> <h4> COLLECTION </h4> </div>
                            </div>
                                
                <div class="form_box_header">
                    <div class="form_single">
                        <h5>
                            Buat SP 1 Otomatis
                        </h5>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            From HO</label>
                        <asp:DropDownList ID="E_cboIsSPAutomatic" runat="server" onchange="cboIsSpAutomaticChange();">
                            <asp:ListItem Value="1" Selected="True">Yes</asp:ListItem>
                            <asp:ListItem Value="0">No</asp:ListItem>
                        </asp:DropDownList>
                        Length SP Process
                        <asp:TextBox ID="E_txtLengthSPProcess" runat="server" MaxLength="4">0</asp:TextBox>days
                        <input id="Hide_LengthSPProcess" type="hidden" name="Hide_LengthSPProcess" runat="server" />
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator27" runat="server" ControlToValidate="E_txtLengthSPProcess"
                            CssClass="validator_general" ErrorMessage="Jangka Waktu Proses SP Salah" Display="Dynamic"></asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="rgv_LengthSPProcess" runat="server" ControlToValidate="E_txtLengthSPProcess"
                            CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Jangka Waktu Proses SP Salah"
                            MaximumValue="9999"></asp:RangeValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Branch</label>
                        <asp:Label ID="lblLengthSPProcess_Branch" runat="server"></asp:Label>
                    </div>
                </div>

                <div class="form_box_header">
                    <div class="form_single">
                        <h5>
                            Buat SP 2 Otomatis
                        </h5>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            From HO</label>
                        <asp:DropDownList ID="E_cboIsSP1Automatic" runat="server" onchange="cboIsSp1AutomaticChange();">
                            <asp:ListItem Value="1" Selected="True">Yes</asp:ListItem>
                            <asp:ListItem Value="0">No</asp:ListItem>
                        </asp:DropDownList>
                        Length SP Process
                        <asp:TextBox ID="E_txtLengthSP1Process" runat="server" MaxLength="4">0</asp:TextBox>days
                        <input id="Hide_LengthSP1Process" type="hidden" name="Hide_LengthSP1Process" runat="server" />
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator28" runat="server" ControlToValidate="E_txtLengthSP1Process"
                            CssClass="validator_general" ErrorMessage="Jangka Waktu Proses SP1 Salah" Display="Dynamic"></asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="rgv_LengthSP1Process" runat="server" ControlToValidate="E_txtLengthSP1Process"
                            CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Jangka Waktu Proses SP1 Salah"
                            MaximumValue="9999"></asp:RangeValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Branch</label>
                        <asp:Label ID="lblLengthSP1Process_Branch" runat="server"></asp:Label>
                    </div>
                </div>

                <div class="form_box_header">
                    <div class="form_single">
                        <h5>
                            Buat SP 3 Otomatis
                        </h5>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            From HO</label>
                        <asp:DropDownList ID="E_cboIsSP2Automatic" runat="server" onchange="cboIsSp2AutomaticChange();">
                            <asp:ListItem Value="1" Selected="True">Yes</asp:ListItem>
                            <asp:ListItem Value="0">No</asp:ListItem>
                        </asp:DropDownList>
                        Length SP Process
                        <asp:TextBox ID="E_txtLengthSP2Process" runat="server" MaxLength="4">0</asp:TextBox>days
                        <input id="Hide_LengthSP2Process" type="hidden" name="Hide_LengthSP2Process" runat="server" />
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator29" runat="server" ControlToValidate="E_txtLengthSP2Process"
                            CssClass="validator_general" ErrorMessage="Jangka Waktu Proses SP2 Salah" Display="Dynamic"></asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="rgv_LengthSP2Process" runat="server" ControlToValidate="E_txtLengthSP2Process"
                            CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Jangka Waktu Proses SP2 Salah"
                            MaximumValue="9999"></asp:RangeValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Branch</label>
                        <asp:Label ID="lblLengthSP2Process_Branch" runat="server"></asp:Label>
                    </div>
                </div>

                <div class="form_box_header">
                    <div class="form_single">
                        <h5>
                            Jangka Waktu proses Dokumen Utama
                        </h5>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            From HO</label>
                        <asp:TextBox onblur="LengthMainDocProcess();" ID="E_txtLengthMainDocProcess" runat="server"
                            MaxLength="4">0</asp:TextBox>days
                        <asp:DropDownList ID="E_cboLengthMainDocProcessBehaviour" runat="server">
                            <asp:ListItem Value="D" Selected="True">Default</asp:ListItem>
                            <asp:ListItem Value="L">Locked</asp:ListItem>
                            <asp:ListItem Value="N">Minimum</asp:ListItem>
                            <asp:ListItem Value="X">Maximum</asp:ListItem>
                        </asp:DropDownList>
                        <input id="Hide_LengthMainDocProcess" type="hidden" name="Hide_LengthMainDocProcess"
                            runat="server" />
                        <input id="Hide_LengthMainDocProcessBeh" type="hidden" name="Hide_LengthMainDocProcessBeh"
                            runat="server" />
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator30" runat="server" ControlToValidate="E_txtLengthMainDocProcess"
                            CssClass="validator_general" ErrorMessage="Jangka Waktu proses Dokumen Utama salah"
                            Display="Dynamic"></asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="rgv_LengthMainDocProcess" runat="server" ControlToValidate="E_txtLengthMainDocProcess"
                            CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Jangka Waktu proses Dokumen Utama salah"
                            MaximumValue="9999"></asp:RangeValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Branch</label>
                        <asp:Label ID="lblLengthMainDocProcess_Branch" runat="server"></asp:Label>
                    </div>
                </div>

                <div class="form_box_header">
                    <div class="form_single">
                        <h5>
                            Jangka Waktu Dokumen Utama diambil
                        </h5>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            From HO</label>
                        <asp:TextBox onblur="LengthMainDocTaken();" ID="E_txtLengthMainDocTaken" runat="server"
                            MaxLength="4">0</asp:TextBox>days
                        <asp:DropDownList ID="E_cboLengthMainDocTakenBehaviour" runat="server">
                            <asp:ListItem Value="D" Selected="True">Default</asp:ListItem>
                            <asp:ListItem Value="L">Locked</asp:ListItem>
                            <asp:ListItem Value="N">Minimum</asp:ListItem>
                            <asp:ListItem Value="X">Maximum</asp:ListItem>
                        </asp:DropDownList>
                        <input id="Hide_LengthMainDocTaken" type="hidden" name="Hide_LengthMainDocTaken"
                            runat="server" />
                        <input id="Hide_LengthMainDocTakenBeh" type="hidden" name="Hide_LengthMainDocTakenBeh"
                            runat="server" />
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator31" runat="server" ControlToValidate="E_txtLengthMainDocTaken"
                            CssClass="validator_general" ErrorMessage="Jangka Waktu Dokumen Utama diambil salah"
                            Display="Dynamic"></asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="rgv_LengthMainDocTaken" runat="server" ControlToValidate="E_txtLengthMainDocTaken"
                            CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Jangka Waktu Dokumen Utama diambil salah"
                            MaximumValue="9999"></asp:RangeValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Branch</label>
                        <asp:Label ID="lblLengthMainDocTaken_Branch" runat="server"></asp:Label>
                    </div>
                </div>

                <div class="form_box_header">
                    <div class="form_single">
                        <h5>
                            Grace Period Denda Keterlambatan
                        </h5>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            From HO</label>
                        <asp:TextBox onblur="GracePeriodLateCharges();" ID="E_txtGracePeriodLateCharges"
                            runat="server" MaxLength="4">0</asp:TextBox>days
                        <asp:DropDownList ID="E_cboGracePeriodLateChargesBehaviour" runat="server">
                            <asp:ListItem Value="D" Selected="True">Default</asp:ListItem>
                            <asp:ListItem Value="L">Locked</asp:ListItem>
                            <asp:ListItem Value="N">Minimum</asp:ListItem>
                            <asp:ListItem Value="X">Maximum</asp:ListItem>
                        </asp:DropDownList>
                        <input id="Hide_GracePeriodLateCharges" type="hidden" name="Hide_GracePeriodLateCharges"
                            runat="server" />
                        <input id="Hide_GracePeriodLateChargesBeh" type="hidden" name="Hide_GracePeriodLateChargesBeh"
                            runat="server" />
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator32" runat="server" ControlToValidate="E_txtGracePeriodLateCharges"
                            CssClass="validator_general" ErrorMessage="Grace Period Denda Keterlambatan Salah"
                            Display="Dynamic"></asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="rgv_GracePeriodLateCharges" runat="server" ControlToValidate="E_txtGracePeriodLateCharges"
                            CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Grace Period Denda Keterlambatan Salah"
                            MaximumValue="9999"></asp:RangeValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Branch</label>
                        <asp:Label ID="lblGracePeriodLateCharges_Branch" runat="server"></asp:Label>
                    </div>
                </div>

                <div class="form_box_header">
                    <div class="form_single">
                        <h5>
                            Jumlah hari mengingatkan angsuran
                        </h5>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            From HO</label>
                        <asp:TextBox onblur="DeskCollPhoneRemind();" ID="E_txtDeskCollPhoneRemind" runat="server"
                            MaxLength="4">0</asp:TextBox>days
                        <asp:DropDownList ID="E_cboDeskCollPhoneRemindBehaviour" runat="server">
                            <asp:ListItem Value="D" Selected="True">Default</asp:ListItem>
                            <asp:ListItem Value="L">Locked</asp:ListItem>
                            <asp:ListItem Value="N">Minimum</asp:ListItem>
                            <asp:ListItem Value="X">Maximum</asp:ListItem>
                        </asp:DropDownList>
                        <input id="Hide_DeskCollPhoneRemind" type="hidden" name="Hide_DeskCollPhoneRemind"
                            runat="server" />
                        <input id="Hide_DeskCollPhoneRemindBeh" type="hidden" name="Hide_DeskCollPhoneRemindBeh"
                            runat="server" />
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator34" runat="server" ControlToValidate="E_txtDeskCollPhoneRemind"
                            CssClass="validator_general" ErrorMessage="Jumlah hari mengingatkan angsuran salah"
                            Display="Dynamic"></asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="rgv_DeskCollPhoneRemind" runat="server" ControlToValidate="E_txtDeskCollPhoneRemind"
                            CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Jumlah hari mengingatkan angsuran salah"
                            MaximumValue="9999"></asp:RangeValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Branch</label>
                        <asp:Label ID="lblDeskCollPhoneRemind_Branch" runat="server"></asp:Label>
                    </div>
                </div>

                <div class="form_box_header">
                    <div class="form_single">
                        <h5>
                            Jumlah hari Overdue untuk Deskcoll
                        </h5>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            From HO</label>
                        <asp:TextBox onblur="DeskCollOD();" ID="E_txtDeskCollOD" runat="server" MaxLength="4">0</asp:TextBox>days
                        <asp:DropDownList ID="E_cboDeskCollODBehaviour" runat="server">
                            <asp:ListItem Value="D" Selected="True">Default</asp:ListItem>
                            <asp:ListItem Value="L">Locked</asp:ListItem>
                            <asp:ListItem Value="N">Minimum</asp:ListItem>
                            <asp:ListItem Value="X">Maximum</asp:ListItem>
                        </asp:DropDownList>
                        <input id="Hide_DeskCollOD" type="hidden" name="Hide_DeskCollOD" runat="server" />
                        <input id="Hide_DeskCollODBeh" type="hidden" name="Hide_DeskCollODBeh" runat="server" />
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator101" runat="server" ControlToValidate="E_txtDeskCollOD"
                            CssClass="validator_general" ErrorMessage="Jumlah hari Overdue untuk DeskColl salah"
                            Display="Dynamic"></asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="rgv_DeskCollOD" runat="server" ControlToValidate="E_txtDeskCollOD"
                            Type="Double" MinimumValue="0" ErrorMessage="Jumlah hari Overdue untuk DeskColl salah"
                            CssClass="validator_general" MaximumValue="9999"></asp:RangeValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Branch</label>
                        <asp:Label ID="lblDeskCollOD_Branch" runat="server"></asp:Label>
                    </div>
                </div>

                <div class="form_box_header">
                    <div class="form_single">
                        <h5>
                            Overdue sebelumnya untuk mengingatkan
                        </h5>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            From HO</label>
                        <asp:TextBox onblur="PrevODToRemind();" ID="E_txtPrevODToRemind" runat="server" MaxLength="4">0</asp:TextBox>days
                        <asp:DropDownList ID="E_cboPrevODToRemindBehaviour" runat="server">
                            <asp:ListItem Value="D" Selected="True">Default</asp:ListItem>
                            <asp:ListItem Value="L">Locked</asp:ListItem>
                            <asp:ListItem Value="N">Minimum</asp:ListItem>
                            <asp:ListItem Value="X">Maximum</asp:ListItem>
                        </asp:DropDownList>
                        <input id="Hide_PrevODToRemind" type="hidden" name="Hide_PrevODToRemind" runat="server" />
                        <input id="Hide_PrevODToRemindBeh" type="hidden" name="Hide_PrevODToRemindBeh" runat="server" />
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator100" runat="server" ControlToValidate="E_txtPrevODToRemind"
                            CssClass="validator_general" ErrorMessage="Overdue sebelumnya untuk mengingatkan Salah"
                            Display="Dynamic"></asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="rgv_PrevODToRemind" runat="server" ControlToValidate="E_txtPrevODToRemind"
                            CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Overdue sebelumnya untuk mengingatkan Salah"
                            MaximumValue="9999"></asp:RangeValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Branch</label>
                        <asp:Label ID="lblPrevODToRemind_Branch" runat="server"></asp:Label>
                    </div>
                </div>

                <div class="form_box_header">
                    <div class="form_single">
                        <h5>
                            Permintaan PDC untuk telepon
                        </h5>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            From HO</label>
                        <asp:TextBox onblur="PDCDaysToRemind();" ID="E_txtPDCDaysToRemind" runat="server"
                            MaxLength="4">0</asp:TextBox>days
                        <asp:DropDownList ID="E_cboPDCDaysToRemindBehaviour" runat="server">
                            <asp:ListItem Value="D" Selected="True">Default</asp:ListItem>
                            <asp:ListItem Value="L">Locked</asp:ListItem>
                            <asp:ListItem Value="N">Minimum</asp:ListItem>
                            <asp:ListItem Value="X">Maximum</asp:ListItem>
                        </asp:DropDownList>
                        <input id="Hide_PDCDaysToRemind" type="hidden" name="Hide_PDCDaysToRemind" runat="server" />
                        <input id="Hide_PDCDaysToRemindBeh" type="hidden" name="Hide_PDCDaysToRemindBeh"
                            runat="server" />
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator36" runat="server" ControlToValidate="E_txtPDCDaysToRemind"
                            CssClass="validator_general" ErrorMessage="Permintaan PDC untuk telepon Salah"
                            Display="Dynamic"></asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="rgv_PDCDaysToRemind" runat="server" ControlToValidate="E_txtPDCDaysToRemind"
                            CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Permintaan PDC untuk telepon Salah"
                            MaximumValue="9999"></asp:RangeValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Branch</label>
                        <asp:Label ID="lblPDCDaysToRemind_Branch" runat="server"></asp:Label>
                    </div>
                </div>

                <div class="form_box_header">
                    <div class="form_single">
                        <h5>
                            Jumlah hari mengingatkan dgn SMS
                        </h5>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            From HO</label>
                        <asp:TextBox onblur="DeskCollSMSRemind();" ID="E_txtDeskCollSMSRemind" runat="server"
                            MaxLength="4">0</asp:TextBox>days
                        <asp:DropDownList ID="E_cboDeskCollSMSRemindBehaviour" runat="server">
                            <asp:ListItem Value="D" Selected="True">Default</asp:ListItem>
                            <asp:ListItem Value="L">Locked</asp:ListItem>
                            <asp:ListItem Value="N">Minimum</asp:ListItem>
                            <asp:ListItem Value="X">Maximum</asp:ListItem>
                        </asp:DropDownList>
                        <input id="Hide_DeskCollSMSRemind" type="hidden" name="Hide_DeskCollSMSRemind" runat="server" />
                        <input id="Hide_DeskCollSMSRemindBeh" type="hidden" name="Hide_DeskCollSMSRemindBeh"
                            runat="server" />
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator37" runat="server" ControlToValidate="E_txtDeskCollSMSRemind"
                            CssClass="validator_general" ErrorMessage="Jumlah hari mengingatkan dgn SMS salah"
                            Display="Dynamic"></asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="rgv_DeskCollSMSRemind" runat="server" ControlToValidate="E_txtDeskCollSMSRemind"
                            CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Jumlah hari mengingatkan dgn SMS salah"
                            MaximumValue="9999"></asp:RangeValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Branch</label>
                        <asp:Label ID="lblDeskCollSMSRemind_Branch" runat="server"></asp:Label>
                    </div>
                </div>

                <div class="form_box_header">
                    <div class="form_single">
                        <h5>
                            Jumlah hari buat DCR
                        </h5>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            From HO</label>
                        <asp:TextBox onblur="DCR();" ID="E_txtDCR" runat="server" MaxLength="4">0</asp:TextBox>days
                        <asp:DropDownList ID="E_cboDCRBehaviour" runat="server">
                            <asp:ListItem Value="D" Selected="True">Default</asp:ListItem>
                            <asp:ListItem Value="L">Locked</asp:ListItem>
                            <asp:ListItem Value="N">Minimum</asp:ListItem>
                            <asp:ListItem Value="X">Maximum</asp:ListItem>
                        </asp:DropDownList>
                        <input id="Hide_DCR" type="hidden" name="Hide_DCR" runat="server" />
                        <input id="Hide_DCRBeh" type="hidden" name="Hide_DCRBeh" runat="server" />
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator38" runat="server" ControlToValidate="E_txtDCR"
                            CssClass="validator_general" ErrorMessage="Jumlah hari buat DCR Salah" Display="Dynamic"></asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="rgv_DCR" runat="server" ControlToValidate="E_txtDCR" Type="Double"
                            CssClass="validator_general" MinimumValue="0" ErrorMessage="Jumlah hari buat DCR Salah"
                            MaximumValue="9999"></asp:RangeValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Branch</label>
                        <asp:Label ID="lblDCR_Branch" runat="server"></asp:Label>
                    </div>
                </div>

                <div class="form_box_header">
                    <div class="form_single">
                        <h5>
                            Jumlah hari sebelum JT buat Kwitansi
                        </h5>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            From HO</label>
                        <asp:TextBox onblur="DaysBeforeDueToRN();" ID="E_txtDaysBeforeDueToRN" runat="server"
                            MaxLength="4">0</asp:TextBox>days
                        <asp:DropDownList ID="E_cboDaysBeforeDueToRNBehaviour" runat="server">
                            <asp:ListItem Value="D" Selected="True">Default</asp:ListItem>
                            <asp:ListItem Value="L">Locked</asp:ListItem>
                            <asp:ListItem Value="N">Minimum</asp:ListItem>
                            <asp:ListItem Value="X">Maximum</asp:ListItem>
                        </asp:DropDownList>
                        <input id="Hide_DaysBeforeDueToRN" type="hidden" name="Hide_DaysBeforeDueToRN" runat="server" />
                        <input id="Hide_DaysBeforeDueToRNBeh" type="hidden" name="Hide_DaysBeforeDueToRNBeh"
                            runat="server" />
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator39" runat="server" ControlToValidate="E_txtDaysBeforeDueToRN"
                            CssClass="validator_general" ErrorMessage="Jumlah hari sebelum JT buat Kwitansi Salah"
                            Display="Dynamic"></asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="rgv_DaysBeforeDueToRN" runat="server" ControlToValidate="E_txtDaysBeforeDueToRN"
                            CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Jumlah hari sebelum JT buat Kwitansi Salah"
                            MaximumValue="9999"></asp:RangeValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Branch</label>
                        <asp:Label ID="lblDaysBeforeDueToRN_Branch" runat="server"></asp:Label>
                    </div>
                </div>

                <div class="form_box_header">
                    <div class="form_single">
                        <h5>
                            Jumlah hari buat SKT (Surat Kuasa Tarik)
                        </h5>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            From HO</label>
                        <asp:TextBox onblur="ODToRAL();" ID="E_txtODToRAL" runat="server" MaxLength="4">0</asp:TextBox>days
                        <asp:DropDownList ID="E_cboODToRALBehaviour" runat="server">
                            <asp:ListItem Value="D" Selected="True">Default</asp:ListItem>
                            <asp:ListItem Value="L">Locked</asp:ListItem>
                            <asp:ListItem Value="N">Minimum</asp:ListItem>
                            <asp:ListItem Value="X">Maximum</asp:ListItem>
                        </asp:DropDownList>
                        <input id="Hide_ODToRAL" type="hidden" name="Hide_ODToRAL" runat="server" />
                        <input id="Hide_ODToRALBeh" type="hidden" name="Hide_ODToRALBeh" runat="server" />
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator40" runat="server" ControlToValidate="E_txtODToRAL"
                            CssClass="validator_general" ErrorMessage="Jumlah hari buat SKT salah" Display="Dynamic"></asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="rgv_ODToRAL" runat="server" ControlToValidate="E_txtODToRAL"
                            CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Jumlah hari buat SKT salah"
                            MaximumValue="9999"></asp:RangeValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Branch</label>
                        <asp:Label ID="lblODToRAL_Branch" runat="server"></asp:Label>
                    </div>
                </div>

                <div class="form_box_header">
                    <div class="form_single">
                        <h5>
                            Jangka Waktu SKT
                        </h5>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            From HO</label>
                        <asp:TextBox onblur="RALPeriod();" ID="E_txtRALPeriod" runat="server" MaxLength="4">0</asp:TextBox>days
                        <asp:DropDownList ID="E_cboRALPeriodBehaviour" runat="server">
                            <asp:ListItem Value="D" Selected="True">Default</asp:ListItem>
                            <asp:ListItem Value="L">Locked</asp:ListItem>
                            <asp:ListItem Value="N">Minimum</asp:ListItem>
                            <asp:ListItem Value="X">Maximum</asp:ListItem>
                        </asp:DropDownList>
                        <input id="Hide_RALPeriod" type="hidden" name="Hide_RALPeriod" runat="server" />
                        <input id="Hide_RALPeriodBeh" type="hidden" name="Hide_RALPeriodBeh" runat="server" />
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator41" runat="server" ControlToValidate="E_txtRALPeriod"
                            CssClass="validator_general" ErrorMessage="Jangka Waktu SKT Salah" Display="Dynamic"></asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="rgv_RALPeriod" runat="server" ControlToValidate="E_txtRALPeriod"
                            CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Jangka Waktu SKT Salah"
                            MaximumValue="9999"></asp:RangeValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Branch</label>
                        <asp:Label ID="lblRALPeriod_Branch" runat="server"></asp:Label>
                    </div>
                </div>

                <div class="form_box_header">
                    <div class="form_single">
                        <h5>
                            Jumlah hari Maximum Kwitansi dibayar
                        </h5>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            From HO</label>
                        <asp:TextBox onblur="MaxDaysRNPaid();" ID="E_txtMaxDaysRNPaid" runat="server" MaxLength="4">0</asp:TextBox>days
                        <asp:DropDownList ID="E_cboMaxDaysRNPaidBehaviour" runat="server">
                            <asp:ListItem Value="D" Selected="True">Default</asp:ListItem>
                            <asp:ListItem Value="L">Locked</asp:ListItem>
                            <asp:ListItem Value="N">Minimum</asp:ListItem>
                            <asp:ListItem Value="X">Maximum</asp:ListItem>
                        </asp:DropDownList>
                        <input id="Hide_MaxDaysRNPaid" type="hidden" name="Hide_MaxDaysRNPaid" runat="server" />
                        <input id="Hide_MaxDaysRNPaidBeh" type="hidden" name="Hide_MaxDaysRNPaidBeh" runat="server" />
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator42" runat="server" ControlToValidate="E_txtMaxDaysRNPaid"
                            CssClass="validator_general" ErrorMessage="Jumlah hari Maximum Kwitansi dibayar Salah"
                            Display="Dynamic"></asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="rgv_MaxDaysRNPaid" runat="server" ControlToValidate="E_txtMaxDaysRNPaid"
                            CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Jumlah hari Maximum Kwitansi dibayar Salah"
                            MaximumValue="9999"></asp:RangeValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Branch</label>
                        <asp:Label ID="lblMaxDaysRNPaid_Branch" runat="server"></asp:Label>
                    </div>
                </div>

                <div class="form_box_header">
                    <div class="form_single">
                        <h5>
                            Jumlah hari Maximum janji Bayar
                        </h5>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            From HO</label>
                        <asp:TextBox onblur="MaxPTPYDays();" ID="E_txtMaxPTPYDays" runat="server" MaxLength="4">0</asp:TextBox>days
                        <asp:DropDownList ID="E_cboMaxPTPYDaysBehaviour" runat="server">
                            <asp:ListItem Value="D" Selected="True">Default</asp:ListItem>
                            <asp:ListItem Value="L">Locked</asp:ListItem>
                            <asp:ListItem Value="N">Minimum</asp:ListItem>
                            <asp:ListItem Value="X">Maximum</asp:ListItem>
                        </asp:DropDownList>
                        <input id="Hide_MaxPTPYDays" type="hidden" name="Hide_MaxPTPYDays" runat="server" />
                        <input id="Hide_MaxPTPYDaysBeh" type="hidden" name="Hide_MaxPTPYDaysBeh" runat="server" />
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator43" runat="server" ControlToValidate="E_txtMaxPTPYDays"
                            CssClass="validator_general" ErrorMessage="Jumlah hari Maximum janji Bayar Salah"
                            Display="Dynamic"></asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="rgv_MaxPTPYDays" runat="server" ControlToValidate="E_txtMaxPTPYDays"
                            CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Jumlah hari Maximum janji Bayar Salah"
                            MaximumValue="9999"></asp:RangeValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Branch</label>
                        <asp:Label ID="lblMaxPTPYDays_Branch" runat="server"></asp:Label>
                    </div>
                </div>

                <div class="form_box_header">
                    <div class="form_single">
                        <h5>
                            Janji Bayar ke Bank
                        </h5>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            From HO</label>
                        <asp:TextBox onblur="PTPYBank();" ID="E_txtPTPYBank" runat="server" MaxLength="4">0</asp:TextBox>days
                        <asp:DropDownList ID="E_txtPTPYBankBehaviour" runat="server">
                            <asp:ListItem Value="D" Selected="True">Default</asp:ListItem>
                            <asp:ListItem Value="L">Locked</asp:ListItem>
                            <asp:ListItem Value="N">Minimum</asp:ListItem>
                            <asp:ListItem Value="X">Maximum</asp:ListItem>
                        </asp:DropDownList>
                        <input id="Hide_PTPYBank" type="hidden" name="Hide_PTPYBank" runat="server" />
                        <input id="Hide_PTPYBankBeh" type="hidden" name="Hide_PTPYBankBeh" runat="server" />
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator44" runat="server" ControlToValidate="E_txtPTPYBank"
                            CssClass="validator_general" ErrorMessage="Janji Bayar ke Bank Salah" Display="Dynamic"></asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="rgv_PTPYBank" runat="server" ControlToValidate="E_txtPTPYBank"
                            CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Janji Bayar ke Bank Salah"
                            MaximumValue="9999"></asp:RangeValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Branch</label>
                        <asp:Label ID="lblPTPYBank_Branch" runat="server"></asp:Label>
                    </div>
                </div>

                <div class="form_box_header">
                    <div class="form_single">
                        <h5>
                            Janji bayar ke Perusahaan
                        </h5>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            From HO</label>
                        <asp:TextBox onblur="PTPYCompany();" ID="E_txtPTPYCompany" runat="server" MaxLength="4">0</asp:TextBox>days
                        <asp:DropDownList ID="E_cboPTPYCompanyBehaviour" runat="server">
                            <asp:ListItem Value="D" Selected="True">Default</asp:ListItem>
                            <asp:ListItem Value="L">Locked</asp:ListItem>
                            <asp:ListItem Value="N">Minimum</asp:ListItem>
                            <asp:ListItem Value="X">Maximum</asp:ListItem>
                        </asp:DropDownList>
                        <input id="Hide_PTPYCompany" type="hidden" name="Hide_PTPYCompany" runat="server" />
                        <input id="Hide_PTPYCompanyBeh" type="hidden" name="Hide_PTPYCompanyBeh" runat="server" />
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator45" runat="server" ControlToValidate="E_txtPTPYCompany"
                            CssClass="validator_general" ErrorMessage="Janji bayar ke Perusahaan Salah" Display="Dynamic"></asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="rgv_PTPYCompany" runat="server" ControlToValidate="E_txtPTPYCompany"
                            CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Janji bayar ke Perusahaan Salah"
                            MaximumValue="9999"></asp:RangeValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Branch</label>
                        <asp:Label ID="lblPTPYCompany_Branch" runat="server"></asp:Label>
                    </div>
                </div>

                <div class="form_box_header">
                    <div class="form_single">
                        <h5>
                            Janji bayar ke Supplier
                        </h5>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            From HO</label>
                        <asp:TextBox onblur="PTPYSupplier();" ID="E_txtPTPYSupplier" runat="server" MaxLength="4">0</asp:TextBox>days
                        <asp:DropDownList ID="E_cboPTPYSupplierBehaviour" runat="server">
                            <asp:ListItem Value="D" Selected="True">Default</asp:ListItem>
                            <asp:ListItem Value="L">Locked</asp:ListItem>
                            <asp:ListItem Value="N">Minimum</asp:ListItem>
                            <asp:ListItem Value="X">Maximum</asp:ListItem>
                        </asp:DropDownList>
                        <input id="Hide_PTPYSupplier" type="hidden" name="Hide_PTPYSupplier" runat="server" />
                        <input id="Hide_PTPYSupplierBeh" type="hidden" name="Hide_PTPYSupplierBeh" runat="server" />
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator46" runat="server" ControlToValidate="E_txtPTPYSupplier"
                            CssClass="validator_general" ErrorMessage="Janji bayar ke Supplier Salah" Display="Dynamic"></asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="rgv_PTPYSupplier" runat="server" ControlToValidate="E_txtPTPYSupplier"
                            CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Janji bayar ke Supplier Salah"
                            MaximumValue="9999"></asp:RangeValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Branch</label>
                        <asp:Label ID="lblPTPYSupplier_Branch" runat="server"></asp:Label>
                    </div>
                </div>

                <div class="form_box_header">
                    <div class="form_single">
                        <h5>
                            Perpanjangan SKT
                        </h5>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            From HO</label>
                        <asp:TextBox onblur="RALExtension();" ID="E_txtRALExtension" runat="server" MaxLength="4">0</asp:TextBox>days
                        <asp:DropDownList ID="E_cboRALExtensionBehaviour" runat="server">
                            <asp:ListItem Value="D" Selected="True">Default</asp:ListItem>
                            <asp:ListItem Value="L">Locked</asp:ListItem>
                            <asp:ListItem Value="N">Minimum</asp:ListItem>
                            <asp:ListItem Value="X">Maximum</asp:ListItem>
                        </asp:DropDownList>
                        <input id="Hide_RALExtension" type="hidden" name="Hide_RALExtension" runat="server" />
                        <input id="Hide_RALExtensionBeh" type="hidden" name="Hide_RALExtensionBeh" runat="server" />
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator47" runat="server" ControlToValidate="E_txtRALExtension"
                            CssClass="validator_general" ErrorMessage="Perpanjangan SKT Salah" Display="Dynamic"></asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="rgv_RALExtension" runat="server" ControlToValidate="E_txtRALExtension"
                            CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Perpanjangan SKT Salah"
                            MaximumValue="9999"></asp:RangeValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Branch</label>
                        <asp:Label ID="lblRALExtension_Branch" runat="server"></asp:Label>
                    </div>
                </div>

                <div class="form_box_header">
                    <div class="form_single">
                        <h5>
                            Jangka Waktu jadi Inventory
                        </h5>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single"> 
                            <label class="label_req">
                                From HO</label>
                            <asp:TextBox onblur="InventoryExpected();" ID="E_txtInventoryExpected" runat="server"
                                MaxLength="4">0</asp:TextBox>days
                            <asp:DropDownList ID="E_cboInventoryExpectedBehaviour" runat="server">
                                <asp:ListItem Value="D" Selected="True">Default</asp:ListItem>
                                <asp:ListItem Value="L">Locked</asp:ListItem>
                                <asp:ListItem Value="N">Minimum</asp:ListItem>
                                <asp:ListItem Value="X">Maximum</asp:ListItem>
                            </asp:DropDownList>
                            <input id="Hide_InventoryExpected" visible="false" type="hidden" name="Hide_InventoryExpected"
                                runat="server" />
                            <input id="Hide_InventoryExpectedBeh" visible="false" type="hidden" name="Hide_InventoryExpectedBeh"
                                runat="server" />
                            <asp:RequiredFieldValidator ID="Requiredfieldvalidator48" runat="server" ControlToValidate="E_txtInventoryExpected"
                                CssClass="validator_general" ErrorMessage="Jangka Waktu jadi Inventory Salah"
                                Display="Dynamic"></asp:RequiredFieldValidator>
                            <asp:RangeValidator ID="rgv_InventoryExpected" runat="server" ControlToValidate="E_txtInventoryExpected"
                                CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Jangka Waktu jadi Inventory Salah"
                                MaximumValue="9999"></asp:RangeValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Branch</label>
                        <asp:Label ID="lblInventoryExpected_Branch" runat="server"></asp:Label>
                    </div>
                </div>
                
                        </div>--%>
                        <%--
                        <div>

                                  <div class="form_box_title">
                                <div class="form_single"> <h4> FINANCE </h4> </div>
                            </div>
                    <div class="form_box_header">
                    <div class="form_single">
                        <h5>
                            Limit Pembayaran di Cabang
                        </h5>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            From HO</label>
                        <asp:TextBox onblur="LimitAPCash();" ID="E_txtLimitAPCash" runat="server" MaxLength="15">0</asp:TextBox>
                        <asp:DropDownList ID="E_cboLimitAPCashBehaviour" runat="server">
                            <asp:ListItem Value="D" Selected="True">Default</asp:ListItem>
                            <asp:ListItem Value="L">Locked</asp:ListItem>
                            <asp:ListItem Value="N">Minimum</asp:ListItem>
                            <asp:ListItem Value="X">Maximum</asp:ListItem>
                        </asp:DropDownList>
                        <input id="Hide_LimitAPCash" type="hidden" name="Hide_LimitAPCash" runat="server" />
                        <input id="Hide_LimitAPCashBeh" type="hidden" name="Hide_LimitAPCashBeh" runat="server" />
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator50" runat="server" ControlToValidate="E_txtLimitAPCash"
                            CssClass="validator_general" ErrorMessage="Limit Pembayaran di Cabang Salah"
                            Display="Dynamic"></asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="rgv_LimitAPCash" runat="server" ControlToValidate="E_txtLimitAPCash"
                            CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Limit Pembayaran di Cabang Salah"
                            MaximumValue="999999999999"></asp:RangeValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Branch</label>
                        <asp:Label ID="lblLimitAPCash_Branch" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Recourse</label>
                        <asp:Label ID="E_lblIsRecourse" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_general">
                            Aktif</label>
                        <asp:RadioButtonList ID="E_rboIsActive" runat="server" class="opt_single" RepeatDirection="Horizontal"
                            Height="4px">
                            <asp:ListItem Value="1" Selected="True">Yes</asp:ListItem>
                            <asp:ListItem Value="0">No</asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                </div>

                <div class="form_box_header">
                    <div class="form_single">
                        <h5>
                            Diskon Asuransi</h5>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            From HO</label>
                        <asp:TextBox ID="txtInsuranceDiscPercentage" runat="server" MaxLength="9">0</asp:TextBox>%
                        <asp:DropDownList ID="cboInsuranceDiscPercentageBehaviour" runat="server">
                            <asp:ListItem Value="D" Selected="True">Default</asp:ListItem>
                            <asp:ListItem Value="L">Locked</asp:ListItem>
                            <asp:ListItem Value="N">Minimum</asp:ListItem>
                            <asp:ListItem Value="X">Maximum</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator62" runat="server" ControlToValidate="txtInsuranceDiscPercentage"
                            CssClass="validator_general" ErrorMessage="Diskon Asuransi Salah" Display="Dynamic"></asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="Rangevalidator52" runat="server" ControlToValidate="txtInsuranceDiscPercentage"
                            Type="double" MinimumValue="0" ErrorMessage="Diskon Asuransi Salah" CssClass="validator_general"
                            Display="Dynamic" MaximumValue="100"></asp:RangeValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Branch</label>
                        <asp:Label ID="lblInsuranceDiscPercentage_Branch" runat="server"></asp:Label>%&nbsp;&nbsp;
                        <asp:Label ID="lblInsuranceDiscPercentageBehaviour_Branch" runat="server"></asp:Label>
                    </div>
                </div>


                        </div>--%>
                        

               
                <div class="form_box_hide">
               
                <div class="form_box_hide">
                    <div class="form_single">
                        <h5>
                            Prosentase Ta'widh keterlambatan (Asuransi)
                        </h5>
                    </div>
                </div>
                <div class="form_box_hide">
                    <div class="form_single">
                        <label class="label_req">
                            From HO</label>
                        <asp:TextBox onblur="InsurancePenaltyPercentage();" ID="E_txtInsurancePenaltyPercentage"
                            runat="server" MaxLength="9">0</asp:TextBox>
                        <asp:DropDownList ID="E_cboInsurancePenaltyPercentageBehaviour" runat="server">
                            <asp:ListItem Value="D" Selected="True">Default</asp:ListItem>
                            <asp:ListItem Value="L">Locked</asp:ListItem>
                            <asp:ListItem Value="N">Minimum</asp:ListItem>
                            <asp:ListItem Value="X">Maximum</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator5" runat="server" ControlToValidate="E_txtInsurancePenaltyPercentage"
                            CssClass="validator_general" ErrorMessage="Prosentase Ta'widh keterlambatan Asuransi salah"
                            Display="Dynamic"></asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="rgv_InsurancePenaltyPercentage" runat="server" ControlToValidate="E_txtInsurancePenaltyPercentage"
                            CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Prosentase Ta'widh keterlambatan Asuransi salah"
                            MaximumValue="100"></asp:RangeValidator>
                    </div>
                </div>
                <input id="Hide_InsurancePenaltyPercentage" type="hidden" name="Hide_InsurancePenaltyPercentage"
                    runat="server" />
                <input id="Hide_InsurancePenaltyPercentageBeh" type="hidden" name="Hide_InsurancePenaltyPercentageBeh"
                    runat="server" />
                <div class="form_box_hide">
                    <div class="form_single">
                        <label>
                            Branch</label>
                        <asp:Label ID="lblInsurancePenaltyPercentage_Branch" runat="server"></asp:Label>
                    </div>
                </div>

                <div class="form_box_header">
                    <div class="form_single">
                        <h5>
                            Pendapatan Margin Minimum yg harus ditolak
                        </h5>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            From HO</label>
                        <asp:TextBox onblur="RejectMinimumIncome();" ID="E_txtRejectMinimumIncome" runat="server"
                            MaxLength="15">0</asp:TextBox>
                        <asp:DropDownList ID="E_cboRejectMinimumIncomeBehaviour" runat="server">
                            <asp:ListItem Value="N" Selected="True">Minimum</asp:ListItem>
                        </asp:DropDownList>
                        <input id="Hide_RejectMinimumIncome" type="hidden" name="Hide_RejectMinimumIncome"
                            runat="server" />
                        <input id="Hide_RejectMinimumIncomeBeh" type="hidden" name="Hide_RejectMinimumIncomeBeh"
                            runat="server" />
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator25" runat="server" ControlToValidate="E_txtRejectMinimumIncome"
                            CssClass="validator_general" ErrorMessage="Pendapatan Margin Minimum yg harus ditolak Salah"
                            Display="Dynamic"></asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="rgv_RejectMinimumIncome" runat="server" ControlToValidate="E_txtRejectMinimumIncome"
                            CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Pendapatan Margin Minimum yg harus ditolak Salah"
                            MaximumValue="999999999999"></asp:RangeValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Branch</label>
                        <asp:Label ID="lblRejectMinimumIncome_Branch" runat="server"></asp:Label>
                    </div>
                </div>

                <div class="form_box_header">
                    <div class="form_single">
                        <h5>
                            Pendapatan Margin Minimum yg harus diingatkan
                        </h5>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            From HO</label>
                        <asp:TextBox onblur="WarningMinimumIncome();" ID="E_txtWarningMinimumIncome" runat="server"
                            MaxLength="15">0</asp:TextBox>
                        <asp:DropDownList ID="E_cboWarningMinimumIncomeBehaviour" runat="server">
                            <asp:ListItem Value="N" Selected="True">Minimum</asp:ListItem>
                        </asp:DropDownList>
                        <input id="Hide_WarningMinimumIncome" type="hidden" name="Hide_WarningMinimumIncome"
                            runat="server" />
                        <input id="Hide_WarningMinimumIncomeBeh" type="hidden" name="Hide_WarningMinimumIncomeBeh"
                            runat="server" />
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator26" runat="server" ControlToValidate="E_txtWarningMinimumIncome"
                            CssClass="validator_general" ErrorMessage="Pendapatan Margin Minimum yg harus diingatkan salah"
                            Display="Dynamic"></asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="rgv_WarningMinimumIncome" runat="server" ControlToValidate="E_txtWarningMinimumIncome"
                            CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Pendapatan Margin Minimum yg harus diingatkan salah"
                            MaximumValue="999999999999"></asp:RangeValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Branch</label>
                        <asp:Label ID="lblWarningMinimumIncome_Branch" runat="server"></asp:Label>
                    </div>
                </div>
              
             

                <div class="form_box_header">
                    <div class="form_single">
                        <h5>
                            Rasio Penghasilan terhadap Angsuran</h5>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            From HO</label>
                        <asp:TextBox ID="txtIncomeInsRatioPercentage" runat="server" MaxLength="9">0</asp:TextBox>%
                        <asp:DropDownList ID="cboIncomeInsRatioPercentageBehaviour" runat="server">
                            <asp:ListItem Value="D" Selected="True">Default</asp:ListItem>
                            <asp:ListItem Value="L">Locked</asp:ListItem>
                            <asp:ListItem Value="N">Minimum</asp:ListItem>
                            <asp:ListItem Value="X">Maximum</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator63" runat="server" ControlToValidate="txtIncomeInsRatioPercentage"
                            CssClass="validator_general" ErrorMessage="Rasio Penghasilan thd Angsuran Salah"
                            Display="Dynamic"></asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="Rangevalidator53" runat="server" ControlToValidate="txtIncomeInsRatioPercentage"
                            Type="double" MinimumValue="0" ErrorMessage="Rasio Penghasilan thd Angsuran Salah"
                            CssClass="validator_general" Display="Dynamic" MaximumValue="100"></asp:RangeValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Branch</label>
                        <asp:Label ID="lblIncomeInsRatioPercentage_Branch" runat="server"></asp:Label>%&nbsp;&nbsp;
                        <asp:Label ID="lblIncomeInsRatioPercentageBehaviour_Branch" runat="server"></asp:Label>
                    </div>
                </div>

                  </div>


                <div class="form_button">
                    <asp:Button ID="ButtonimbSave" runat="server" Text="Save" CssClass="small button blue"
                        CausesValidation="true"></asp:Button>
                    <asp:Button ID="ButtonimbCancel" runat="server" Text="Cancel" CssClass="small button gray"
                        CausesValidation="false"></asp:Button>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
