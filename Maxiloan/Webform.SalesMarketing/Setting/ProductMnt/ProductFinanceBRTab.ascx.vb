﻿Public Class ProductFinanceBRTab
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Public Property UCMode As String
        Get
            Return CType(ViewState("FINANCETABUCMODE"), String)
        End Get
        Set(value As String)
            ViewState("FINANCETABUCMODE") = value
        End Set
    End Property
    Public Property Product() As Parameter.Product
        Get
            Return CType(ViewState("ProductFinanceTabProduct"), Parameter.Product)
        End Get
        Set(ByVal Value As Parameter.Product)
            ViewState("ProductFinanceTabProduct") = Value
        End Set
    End Property
    Sub InitialControls()

        lblLimitAPCash.Visible = (UCMode = "VIEW")
        E_txtLimitAPCash.Visible = (UCMode <> "VIEW")
        E_cboLimitAPCashBehaviour.Visible = (UCMode <> "VIEW")


        lblIsActive.Visible = (UCMode = "VIEW")
        E_rboIsActive.Visible = (UCMode <> "VIEW")


        lblInsuranceDiscPercentage.Visible = (UCMode = "VIEW")
        txtInsuranceDiscPercentage.Visible = (UCMode <> "VIEW")
        cboInsuranceDiscPercentageBehaviour.Visible = (UCMode <> "VIEW")

        Label1.Visible = (UCMode <> "VIEW")

        If (UCMode = "ADD") Then
            initFieldsForAddMode()
        End If
        If (UCMode = "EDIT") Then
            BindFiedsForEdit()
        End If
    End Sub


    Sub initFieldsForAddMode()

    End Sub
    Public Sub BindFiedsForEdit()
        txtInsuranceDiscPercentage.Text = CStr(FormatNumber(Product.InsuranceDiscPercentage, 10))
        cboInsuranceDiscPercentageBehaviour.SelectedIndex = cboInsuranceDiscPercentageBehaviour.Items.IndexOf(cboInsuranceDiscPercentageBehaviour.Items.FindByValue(Product.InsuranceDiscPercentageBehaviour))
        Behaviour_Lock(Product.InsuranceDiscPercentageBehaviour, txtInsuranceDiscPercentage, cboInsuranceDiscPercentageBehaviour)

        'Limit A/P Disbursement at Branch
        E_txtLimitAPCash.Text = CStr(Product.LimitAPCash)
        Hide_LimitAPCash.Value = CStr(Product.LimitAPCash)
        Hide_LimitAPCashBeh.Value = Product.LimitAPCashBehaviour
        E_cboLimitAPCashBehaviour.SelectedIndex = E_cboLimitAPCashBehaviour.Items.IndexOf(E_cboLimitAPCashBehaviour.Items.FindByValue(Product.LimitAPCashBehaviour))
        Behaviour_Lock(Product.LimitAPCashBehaviour, E_txtLimitAPCash, E_cboLimitAPCashBehaviour)

        'Is Recourse 
        E_lblIsRecourse.Text = IIf(Product.IsRecourse = True, "Yes", "No")
        E_rboIsActive.SelectedIndex = E_rboIsActive.Items.IndexOf(E_rboIsActive.Items.FindByValue(IIf(Product.IsActive = True, "1", "0")))

        setBranchText()
    End Sub

    Sub setBranchText()

        lblLimitAPCash_Branch.Text = CStr(Product.LimitAPCash_HO) & "  " & Parameter.Product.Behaviour_Value(Product.LimitAPCashBehaviour_HO)
        E_lblIsRecourse.Text = IIf(Product.IsRecourse_HO = True, "Yes", "No")
        lblIsActive.Text = IIf(CStr(Product.IsActive) = "True", "Yes", "No")
        lblInsuranceDiscPercentage_Branch.Text = String.Format("{0} % {1}", CStr(FormatNumber(Product.InsuranceDiscPercentage_HO, 10)), Parameter.Product.Behaviour_Value(Product.InsuranceDiscPercentageBehaviour_HO))

    End Sub

    Public Sub InitUCViewMode()
        InitialControls()
        lblLimitAPCash.Text = String.Format("{0} {1}", FormatNumber(CStr(Product.LimitAPCash), 2), Parameter.Product.Behaviour_Value(Product.LimitAPCashBehaviour))
        E_lblIsRecourse.Text = IIf(Product.IsRecourse = True, "Yes", "No")
        lblIsActive.Text = IIf(CStr(Product.IsActive) = "True", "Yes", "No")
        lblInsuranceDiscPercentage.Text = String.Format("{0} % {1}", CStr(FormatNumber(Product.InsuranceDiscPercentage, 10)), Parameter.Product.Behaviour_Value(Product.InsuranceDiscPercentageBehaviour))

        setBranchText()
    End Sub


    Sub bindBehaviour(ddl As DropDownList)
        ddl.DataValueField = "Value"
        ddl.DataTextField = "Text"
        ddl.DataSource = Parameter.Product.BehaviourItems
        ddl.DataBind()
    End Sub


    Public Sub InitUCEditMode()
        bindBehaviour(E_cboLimitAPCashBehaviour)
        bindBehaviour(cboInsuranceDiscPercentageBehaviour)
        InitialControls()
    End Sub
    Public Sub CollectResult(oProduct As Parameter.Product)
        With oProduct 
            .InsuranceDiscPercentage = CDec(txtInsuranceDiscPercentage.Text.Trim)
            .InsuranceDiscPercentageBehaviour = cboInsuranceDiscPercentageBehaviour.SelectedItem.Value.Trim
            .LimitAPCash = CInt(E_txtLimitAPCash.Text.Trim)
            .LimitAPCashBehaviour = E_cboLimitAPCashBehaviour.SelectedItem.Value.Trim
            .IsActive = CBool(E_rboIsActive.SelectedItem.Value.Trim)
        End With
    End Sub
End Class