﻿#Region "Imports"
Imports Maxiloan.cbse
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
#End Region

Public Class ProductBranchHO
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private m_controller As New ProductController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region

#Region "Property"
    Private Property IsAuthorized() As Boolean
        Get
            Return CType(viewstate("IsAuthorized"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            viewstate("IsAuthorized") = Value
        End Set
    End Property

    Private Property CmdWhere() As String
        Get
            Return CType(viewstate("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property ProductID() As String
        Get
            Return CType(viewstate("ProductID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ProductID") = Value
        End Set
    End Property

    Private Property Description() As String
        Get
            Return CType(viewstate("Description"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Description") = Value
        End Set
    End Property

    Private Property IsActive() As Boolean
        Get
            Return CType(viewstate("IsActive"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            viewstate("IsActive") = Value
        End Set
    End Property

    Private Property AddEdit() As String
        Get
            Return CType(viewstate("vwsAddEdt"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("vwsAddEdt") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(viewstate("Sort"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Sort") = Value
        End Set
    End Property
#End Region

#Region "Page_Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Page.Header.DataBind()
        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If
        InitialDefaultPanel()

        Me.FormID = "ProductBranchHO"
        If Not Me.IsPostBack Then
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If SessionInvalid() Then
                    Exit Sub
                End If
                Me.SearchBy = ""
                Me.SortBy = ""
            End If
            txtPage.Text = "1"
            txtSearchBy.Text = ""
            Me.Sort = "a.BranchID ASC"
            Me.ProductID = Request("ProductID")
            hplProductID.NavigateUrl = LinkTo(Me.ProductID, "Marketing")
            hplB_ProductID.NavigateUrl = LinkTo(Me.ProductID, "Marketing")

            hplProductID.Text = Me.ProductID
            Me.Description = Request("Description")
            Me.IsActive = CBool(Request("IsActive"))
            Me.CmdWhere = "ProductID='" & Me.ProductID & "'"
            BindGridEntity(Me.CmdWhere)
            pnlBranch.Visible = False
            'E_cboGrossYieldRateBehaviour.Visible = False
        End If
    End Sub
#End Region

#Region "LinkTo"
    Function LinkTo(ByVal strProductID As String, ByVal strStyle As String) As String
        Return "javascript:OpenWinProductView('" & strProductID & "','" & strStyle & "')"
    End Function
#End Region
#Region "LinkToProductBranch"
    Function LinkToProductBranchHO(ByVal strProductID As String, ByVal strBranchID As String) As String
        Return "javascript:OpenWinProductBranchHOView('" & strProductID & "','" & strBranchID & "')"
    End Function
#End Region


#Region "InitialDefaultPanel"
    Private Sub InitialDefaultPanel()
        pnlList.Visible = True
        pnlEdit.Visible = False
        pnlBranch.Visible = False
    End Sub
#End Region

#Region "BindGridEntity"
    Sub BindGridEntity(ByVal cmdWhere As String)
        Dim dtEntity As DataTable
        Dim oProduct As New Parameter.Product

        If Me.IsActive = False Then
            ButtonAdd.Enabled = False
            'ButtonPrint.Enabled = False
        Else
            ButtonAdd.Enabled = True
            'ButtonPrint.Enabled = True
        End If

        With oProduct
            .WhereCond = Me.CmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = Me.Sort
            .strConnection = GetConnectionString
        End With
        oProduct = m_controller.ProductBranchHOPaging(oProduct)

        If Not oProduct Is Nothing Then
            dtEntity = oProduct.ListData
            recordCount = oProduct.TotalRecords
        Else
            recordCount = 0
        End If
        'If recordCount = 0 Then
        '    ButtonPrint.Enabled = False
        'Else
        '    ButtonPrint.Enabled = True
        'End If
        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0

        Dim oCustomClass As New Parameter.Common
        Dim oController As New AuthorizationController

        With oCustomClass
            .strConnection = GetConnectionString()
            .LoginId = Me.Loginid
            .FeatureID = "VIEW"
            .FormID = Me.FormID
            .AppID = Me.AppId
        End With
        Me.IsAuthorized = oController.CheckFeature(oCustomClass)

        dtgPaging.DataBind()

        hplProductID.Text = Me.ProductID
        lblDescription.Text = Me.Description

        PagingFooter()
    End Sub
#End Region

#Region "dtgPaging_ItemDataBound"
    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        Dim imbEdit As ImageButton
        Dim lblBrchID As Label
        Dim hynBranchFullName As HyperLink
        If e.Item.ItemIndex >= 0 Then
            imbEdit = CType(e.Item.FindControl("imbEdit"), ImageButton)

            lblBrchID = CType(e.Item.FindControl("lblBrchID"), Label)
            hynBranchFullName = CType(e.Item.FindControl("hynBranchFullName"), HyperLink)
            hynBranchFullName.NavigateUrl = LinkToProductBranchHO(Me.ProductID, lblBrchID.Text.Trim)

            If Me.IsActive = False Then
                ButtonAdd.Visible = False
                'ButtonPrint.Visible = False
                imbEdit.Visible = False
            Else
                ButtonAdd.Visible = True
                'ButtonPrint.Visible = True
                imbEdit.Visible = True
            End If

            If Not Me.IsAuthorized Then
                Dim strHTTPServer As String
                Dim StrHTTPApp As String
                Dim strNameServer As String

                strHTTPServer = Request.ServerVariables("PATH_INFO")
                StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                strNameServer = Request.ServerVariables("SERVER_NAME")

                hynBranchFullName.NavigateUrl = "http://" & strNameServer & "/" & StrHTTPApp & "/NotAuthorized.html"
            End If
        End If
    End Sub
#End Region

#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int32)
                BindGridEntity(Me.CmdWhere)
            End If
        End If
    End Sub
#End Region

#Region "Behaviour_Value"
    Function Behaviour_Value(ByVal Behaviour As String) As String
        If Behaviour = "D" Then
            Return "Default"
        ElseIf Behaviour = "L" Then
            Return "Locked"
        ElseIf Behaviour = "N" Then
            Return "Minimum"
        ElseIf Behaviour = "X" Then
            Return "Maximum"
        End If
        Return ""
    End Function
    Sub Behaviour_Lock(bhv As String, textBox As RadioButtonList, ddl As DropDownList)
        Dim enable = bhv <> "L"
        textBox.Enabled = enable
        'E_cboEffectiveRateBehaviour.Enabled = enable

        If bhv = "N" Then
            ddl.Items.Remove(ddl.Items.FindByValue("X"))
        ElseIf bhv = "X" Then
            ddl.Items.Remove(ddl.Items.FindByValue("N"))
        End If
    End Sub
    Sub Behaviour_Lock(bhv As String, textBox As TextBox, ddl As DropDownList)
        Dim enable = bhv <> "L"

        textBox.Enabled = enable
        'E_cboEffectiveRateBehaviour.Enabled = enable

        If bhv = "N" Then
            ddl.Items.Remove(ddl.Items.FindByValue("X"))
        ElseIf bhv = "X" Then
            ddl.Items.Remove(ddl.Items.FindByValue("N"))
        End If 
    End Sub

#End Region

#Region "dtgEntity_ItemCommand"
    Private Sub dtgEntity_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        Dim oProduct As New Parameter.Product
        If e.CommandName = "EDIT" Then
            If CheckFeature(Me.Loginid, Me.FormID, "EDIT", Me.AppId) Then
                Me.AddEdit = "EDIT"

                pnlBranch.Visible = False
                pnlEdit.Visible = True
                pnlList.Visible = False

                Me.BranchID = dtgPaging.DataKeys.Item(e.Item.ItemIndex).ToString

                oProduct.ProductId = Me.ProductID
                oProduct.Branch_ID = Me.BranchID
                oProduct.strConnection = GetConnectionString()
                oProduct = m_controller.ProductBranchHOEdit(oProduct)

                oProduct.ProductId = Me.ProductID
                oProduct.Branch_ID = Me.BranchID


                UmumTab.Product = oProduct
                UmumTab.UCMode = "EDIT"
                UmumTab.UCProductName = "PRODUK CABANG"
                UmumTab.InitUCEditMode()

                SkemaTab.Product = oProduct
                SkemaTab.UCMode = "EDIT"
                SkemaTab.InitUCEditMode()


                BiayaTab.Product = oProduct
                BiayaTab.UCMode = "EDIT"
                BiayaTab.InitUCEditMode()

                AngsuranTab.Product = oProduct
                AngsuranTab.UCMode = "EDIT"
                AngsuranTab.InitUCEditMode()

                CollactionTab.Product = oProduct
                CollactionTab.UCMode = "EDIT"
                CollactionTab.InitUCEditMode()

                FinanceTab.Product = oProduct
                FinanceTab.UCMode = "EDIT"
                FinanceTab.InitUCEditMode()

                'E_lblBranchID.Text = oProduct.BranchFullName
                'E_lblProductID.Text = Me.ProductID
                'E_lblDescription.Text = Me.Description
                'E_lblAssetType.Text = oProduct.AssetType_desc
                'E_lblScoreSchemeID.Text = oProduct.ScoreSchemeMaster_desc
                'E_lblCreditScoreSchemeID.Text = oProduct.CreditScoreSchemeMaster_desc
                'E_lblJournalSchemeID.Text = oProduct.JournalScheme_desc
                'E_lblApprovalSchemeID.Text = oProduct.ApprovalTypeScheme_desc

                'E_lblAssetUsedNew.Text = IIf(oProduct.AssetUsedNew = "U", "Used", "New")

                'E_lblKegiatanUsaha.Text = oProduct.KegiatanUsahaDesc
                'E_lblJenisPembiayaan.Text = oProduct.JenisPembiayaanDesc

                'Effective Rate'
                'E_txtEffectiveRate.Text = CStr(FormatNumber(oProduct.EffectiveRate_HO, 6))
                'Hide_EffectiveRate.Value = E_txtEffectiveRate.Text
                'Hide_EffectiveRateBeh.Value = oProduct.EffectiveRateBehaviour_HO
                'E_cboEffectiveRateBehaviour.SelectedIndex = E_cboEffectiveRateBehaviour.Items.IndexOf(E_cboEffectiveRateBehaviour.Items.FindByValue(oProduct.EffectiveRateBehaviour_HO))
                'Behaviour_Lock(oProduct.EffectiveRateBehaviour_HO, E_txtEffectiveRate, E_cboEffectiveRateBehaviour)


                'rgv_EffectiveRate.MinimumValue = "0"
                'rgv_EffectiveRate.MaximumValue = "100"

                'If oProduct.EffectiveRateBehaviour_HO = "N" Then
                '    'E_cboEffectiveRateBehaviour.Items.Remove(E_cboEffectiveRateBehaviour.Items.FindByValue("X"))
                '    rgv_EffectiveRate.MinimumValue = Hide_EffectiveRate.Value
                '    rgv_EffectiveRate.MaximumValue = "100"
                '    rgv_EffectiveRate.ErrorMessage = "Harap is Suku Bunga Effective antara " & rgv_EffectiveRate.MinimumValue & " dan 100 "
                'ElseIf oProduct.EffectiveRateBehaviour_HO = "X" Then

                '    rgv_EffectiveRate.MinimumValue = "0"
                '    rgv_EffectiveRate.MaximumValue = Hide_EffectiveRate.Value
                '    rgv_EffectiveRate.ErrorMessage = "Harap is Suku Bunga Effective antara 1 dan " & rgv_EffectiveRate.MaximumValue
                'End If
                'lblEffectiveRate_Branch.Text = CStr(FormatNumber(oProduct.EffectiveRate, 6)) & " % " & Behaviour_Value(oProduct.EffectiveRateBehaviour)


                'GrossYieldRate'
                'E_txtGrossYieldRate.Text = CStr(FormatNumber(oProduct.GrossYieldRate_HO, 6))
                'Hide_GrossYieldRate.Value = E_txtGrossYieldRate.Text
                'Hide_GrossYieldRateBeh.Value = oProduct.GrossYieldRateBehaviour_HO
                'E_cboGrossYieldRateBehaviour.SelectedIndex = E_cboGrossYieldRateBehaviour.Items.IndexOf(E_cboGrossYieldRateBehaviour.Items.FindByValue(oProduct.GrossYieldRateBehaviour_HO))


                'E_cboGrossYieldRateBehaviour.Items.Remove(E_cboGrossYieldRateBehaviour.Items.FindByValue("L"))
                'E_cboGrossYieldRateBehaviour.Items.Remove(E_cboGrossYieldRateBehaviour.Items.FindByValue("X"))
                'E_cboGrossYieldRateBehaviour.Items.Remove(E_cboGrossYieldRateBehaviour.Items.FindByValue("D"))


                'Behaviour_Lock(oProduct.GrossYieldRateBehaviour_HO, E_txtGrossYieldRate, E_cboGrossYieldRateBehaviour)
                'lblGrossYieldRate_Branch.Text = CStr(FormatNumber(oProduct.GrossYieldRate, 6)) & " % " & Behaviour_Value(oProduct.GrossYieldRateBehaviour)

                ''Minimum Tenor
                'E_txtMinimumTenor.Text = CStr(oProduct.MinimumTenor_HO)
                'Hide_MinimumTenor.Value = CStr(oProduct.MinimumTenor_HO)
                'lblMinimumTenor_Branch.Text = CStr(oProduct.MinimumTenor) & " months"

                ''Maximum Tenor
                'E_txtMaximumTenor.Text = CStr(oProduct.MaximumTenor_HO)
                'Hide_MaximumTenor.Value = CStr(oProduct.MaximumTenor_HO)
                'lblMaximumTenor_Branch.Text = CStr(oProduct.MaximumTenor) & " months"

                'Late Charges Percentage (Installment)
                'E_txtPenaltyPercentage.Text = CStr(FormatNumber(oProduct.PenaltyPercentage_HO, 6))
                'Hide_PenaltyPercentage.Value = E_txtPenaltyPercentage.Text
                'Hide_PenaltyPercentageBeh.Value = oProduct.PenaltyPercentageBehaviour_HO
                'E_cboPenaltyPercentageBehaviour.SelectedIndex = E_cboPenaltyPercentageBehaviour.Items.IndexOf(E_cboPenaltyPercentageBehaviour.Items.FindByValue(oProduct.PenaltyPercentageBehaviour_HO))
                'Behaviour_Lock(oProduct.PenaltyPercentageBehaviour_HO, E_txtPenaltyPercentage, E_cboPenaltyPercentageBehaviour)


                'lblPenaltyPercentage_Branch.Text = CStr(FormatNumber(oProduct.PenaltyPercentage, 2)) & " ‰ / day " & Behaviour_Value(oProduct.PenaltyPercentageBehaviour)

                'Insurance Late Charges Percentage (Insurance)
                E_txtInsurancePenaltyPercentage.Text = CStr(FormatNumber(oProduct.InsurancePenaltyPercentage_HO, 6))
                Hide_InsurancePenaltyPercentage.Value = E_txtInsurancePenaltyPercentage.Text
                Hide_InsurancePenaltyPercentageBeh.Value = oProduct.InsurancePenaltyPercentageBehaviour_HO
                E_cboInsurancePenaltyPercentageBehaviour.SelectedIndex = E_cboInsurancePenaltyPercentageBehaviour.Items.IndexOf(E_cboInsurancePenaltyPercentageBehaviour.Items.FindByValue(oProduct.InsurancePenaltyPercentageBehaviour_HO))
                Behaviour_Lock(oProduct.InsurancePenaltyPercentageBehaviour_HO, E_txtInsurancePenaltyPercentage, E_cboInsurancePenaltyPercentageBehaviour)
                
                lblInsurancePenaltyPercentage_Branch.Text = CStr(oProduct.InsurancePenaltyPercentage) & " ‰ / day " & Behaviour_Value(oProduct.InsurancePenaltyPercentageBehaviour)


                E_txtRejectMinimumIncome.Text = CStr(oProduct.RejectMinimumIncome_HO)
                Hide_RejectMinimumIncome.Value = CStr(oProduct.RejectMinimumIncome_HO)
                Hide_RejectMinimumIncomeBeh.Value = oProduct.RejectMinimumIncomeBehaviour_HO
                E_cboRejectMinimumIncomeBehaviour.SelectedIndex = E_cboRejectMinimumIncomeBehaviour.Items.IndexOf(E_cboRejectMinimumIncomeBehaviour.Items.FindByValue(oProduct.RejectMinimumIncomeBehaviour_HO))
                Behaviour_Lock(oProduct.RejectMinimumIncomeBehaviour_HO, E_txtRejectMinimumIncome, E_cboRejectMinimumIncomeBehaviour)

                lblRejectMinimumIncome_Branch.Text = CStr(oProduct.RejectMinimumIncome) & " " & Behaviour_Value(oProduct.RejectMinimumIncomeBehaviour)

                'Warning Minimum Income
                E_txtWarningMinimumIncome.Text = CStr(oProduct.WarningMinimumIncome_HO)
                Hide_WarningMinimumIncome.Value = CStr(oProduct.WarningMinimumIncome_HO)
                Hide_WarningMinimumIncomeBeh.Value = oProduct.WarningMinimumIncomeBehaviour_HO
                E_cboWarningMinimumIncomeBehaviour.SelectedIndex = E_cboWarningMinimumIncomeBehaviour.Items.IndexOf(E_cboWarningMinimumIncomeBehaviour.Items.FindByValue(oProduct.WarningMinimumIncomeBehaviour_HO))
                Behaviour_Lock(oProduct.WarningMinimumIncomeBehaviour_HO, E_txtWarningMinimumIncome, E_cboWarningMinimumIncomeBehaviour)

                lblWarningMinimumIncome_Branch.Text = CStr(oProduct.WarningMinimumIncome) & " " & Behaviour_Value(oProduct.WarningMinimumIncomeBehaviour)

                Dim strIsSPAutomatic_HO = IIf(oProduct.IsSPAutomatic_HO = True, "1", "0")
                Dim strIsSP1Automatic_HO = IIf(oProduct.IsSP1Automatic_HO = True, "1", "0")
                Dim strIsSP2Automatic_HO = IIf(oProduct.IsSP2Automatic_HO = True, "1", "0")



                lblIncomeInsRatioPercentage_Branch.Text = CStr(FormatNumber(oProduct.IncomeInsRatioPercentage, 10))
                lblIncomeInsRatioPercentageBehaviour_Branch.Text = Behaviour_Value(oProduct.IncomeInsRatioPercentageBehaviour)

                txtIncomeInsRatioPercentage.Text = CStr(FormatNumber(oProduct.IncomeInsRatioPercentage_HO, 10))
                cboIncomeInsRatioPercentageBehaviour.SelectedIndex = cboIncomeInsRatioPercentageBehaviour.Items.IndexOf(cboIncomeInsRatioPercentageBehaviour.Items.FindByValue(oProduct.IncomeInsRatioPercentageBehaviour_HO))
                Behaviour_Lock(oProduct.IncomeInsRatioPercentageBehaviour_HO, txtIncomeInsRatioPercentage, cboIncomeInsRatioPercentageBehaviour)


                ''Cancellation Fee
                'E_txtCancellationFee.Text = CStr(oProduct.CancellationFee_HO)
                'Hide_CancellationFee.Value = CStr(oProduct.CancellationFee_HO)
                'Hide_CancellationFeeBeh.Value = oProduct.CancellationFeeBehaviour_HO
                'E_cboCancellationFeeBehaviour.SelectedIndex = E_cboCancellationFeeBehaviour.Items.IndexOf(E_cboCancellationFeeBehaviour.Items.FindByValue(oProduct.CancellationFeeBehaviour_HO))
                'Behaviour_Lock(oProduct.CancellationFeeBehaviour_HO, E_txtCancellationFee, E_cboCancellationFeeBehaviour)

                'lblCancellationFee_Branch.Text = CStr(oProduct.CancellationFee) & " " & Behaviour_Value(oProduct.CancellationFeeBehaviour)

                ''Admin Fee
                'E_txtAdminFee.Text = CStr(oProduct.AdminFee_HO)
                'Hide_AdminFee.Value = CStr(oProduct.AdminFee_HO)
                'Hide_AdminFeeBeh.Value = oProduct.AdminFeeBehaviour_HO
                'E_cboAdminFeeBehaviour.SelectedIndex = E_cboAdminFeeBehaviour.Items.IndexOf(E_cboAdminFeeBehaviour.Items.FindByValue(oProduct.AdminFeeBehaviour_HO))

                'Behaviour_Lock(oProduct.AdminFeeBehaviour_HO, E_txtAdminFee, E_cboAdminFeeBehaviour)

                'lblAdminFee_Branch.Text = CStr(oProduct.AdminFee) & " " & Behaviour_Value(oProduct.AdminFeeBehaviour)

                ''Fiducia Fee
                'E_txtFiduciaFee.Text = CStr(oProduct.FiduciaFee_HO)
                'Hide_FiduciaFee.Value = CStr(oProduct.FiduciaFee_HO)
                'Hide_FiduciaFeeBeh.Value = oProduct.FiduciaFeeBehaviour_HO

                'E_cboFiduciaFeeBehaviour.SelectedIndex = E_cboFiduciaFeeBehaviour.Items.IndexOf(E_cboFiduciaFeeBehaviour.Items.FindByValue(oProduct.FiduciaFeeBehaviour_HO))
                'Behaviour_Lock(oProduct.FiduciaFeeBehaviour_HO, E_txtFiduciaFee, E_cboFiduciaFeeBehaviour)


                'lblFiduciaFee_Branch.Text = CStr(oProduct.FiduciaFee) & " " & Behaviour_Value(oProduct.FiduciaFeeBehaviour)


                ''Provision Fee
                'E_txtProvisionFee.Text = CStr(oProduct.ProvisionFeeHO)
                'Hide_ProvisionFee.Value = CStr(oProduct.ProvisionFeeHO)
                'Hide_ProvisionFeeBeh.Value = oProduct.ProvisionFeeBehaviour_HO
                'E_cboProvisionFeeBehaviour.SelectedIndex = E_cboProvisionFeeBehaviour.Items.IndexOf(E_cboProvisionFeeBehaviour.Items.FindByValue(oProduct.ProvisionFeeBehaviour_HO))
                'Behaviour_Lock(oProduct.ProvisionFeeBehaviour_HO, E_txtProvisionFee, E_cboProvisionFeeBehaviour)

                'lblProvisionFee_Branch.Text = CStr(oProduct.ProvisionFee) & " " & Behaviour_Value(oProduct.ProvisionFeeBehaviour)

                ''Notary Fee
                'E_txtNotaryFee.Text = CStr(oProduct.NotaryFee_HO)
                'Hide_NotaryFee.Value = CStr(oProduct.NotaryFee_HO)
                'Hide_NotaryFeeBeh.Value = oProduct.NotaryFeeBehaviour_HO
                'E_cboNotaryFeeBehaviour.SelectedIndex = E_cboNotaryFeeBehaviour.Items.IndexOf(E_cboNotaryFeeBehaviour.Items.FindByValue(oProduct.NotaryFeeBehaviour_HO))
                'Behaviour_Lock(oProduct.NotaryFeeBehaviour_HO, E_txtNotaryFee, E_cboNotaryFeeBehaviour)


                'lblNotaryFee_Branch.Text = CStr(oProduct.NotaryFee) & " " & Behaviour_Value(oProduct.NotaryFeeBehaviour)


                ''Survey Fee
                'E_txtSurveyFee.Text = CStr(oProduct.SurveyFee_HO)
                'Hide_SurveyFee.Value = CStr(oProduct.SurveyFee_HO)
                'Hide_SurveyFeeBeh.Value = oProduct.SurveyFeeBehaviour_HO

                'E_cboSurveyFeeBehaviour.SelectedIndex = E_cboSurveyFeeBehaviour.Items.IndexOf(E_cboSurveyFeeBehaviour.Items.FindByValue(oProduct.SurveyFeeBehaviour_HO))
                'Behaviour_Lock(oProduct.SurveyFeeBehaviour_HO, E_txtSurveyFee, E_cboSurveyFeeBehaviour)

                'lblSurveyFee_Branch.Text = CStr(oProduct.SurveyFee) & " " & Behaviour_Value(oProduct.SurveyFeeBehaviour)


                ''Visit Fee
                'E_txtVisitFee.Text = CStr(oProduct.VisitFee_HO)
                'Hide_VisitFee.Value = CStr(oProduct.VisitFee_HO)
                'Hide_VisitFeeBeh.Value = oProduct.VisitFeeBehaviour_HO
                'E_cboVisitFeeBehaviour.SelectedIndex = E_cboVisitFeeBehaviour.Items.IndexOf(E_cboVisitFeeBehaviour.Items.FindByValue(oProduct.VisitFeeBehaviour_HO))
                'Behaviour_Lock(oProduct.VisitFeeBehaviour_HO, E_txtVisitFee, E_cboVisitFeeBehaviour)

                'lblVisitFee_Branch.Text = CStr(oProduct.VisitFee) & " " & Behaviour_Value(oProduct.VisitFeeBehaviour)


                ''Rescheduling Fee
                'E_txtReschedulingFee.Text = CStr(oProduct.ReschedulingFee_HO)
                'Hide_ReschedulingFee.Value = CStr(oProduct.ReschedulingFee_HO)
                'Hide_ReschedulingFeeBeh.Value = oProduct.ReschedulingFeeBehaviour_HO

                'E_cboReschedulingFeeBehaviour.SelectedIndex = E_cboReschedulingFeeBehaviour.Items.IndexOf(E_cboReschedulingFeeBehaviour.Items.FindByValue(oProduct.ReschedulingFeeBehaviour_HO))
                'Behaviour_Lock(oProduct.ReschedulingFeeBehaviour_HO, E_txtReschedulingFee, E_cboReschedulingFeeBehaviour)

                'lblReschedulingFee_Branch.Text = CStr(oProduct.ReschedulingFee) & " " & Behaviour_Value(oProduct.ReschedulingFeeBehaviour)

                ''AgreementTransfer Fee
                'E_txtAgreementTransferFee.Text = CStr(oProduct.AgreementTransferFee_HO)
                'Hide_AgreementTransferFee.Value = CStr(oProduct.AgreementTransferFee_HO)
                'Hide_AgreementTransferFeeBeh.Value = oProduct.AgreementTransferFeeBehaviour_HO
                'E_cboAgreementTransferFeeBehaviour.SelectedIndex = E_cboAgreementTransferFeeBehaviour.Items.IndexOf(E_cboAgreementTransferFeeBehaviour.Items.FindByValue(oProduct.AgreementTransferFeeBehaviour_HO))
                'Behaviour_Lock(oProduct.AgreementTransferFeeBehaviour_HO, E_txtAgreementTransferFee, E_cboAgreementTransferFeeBehaviour)

                'lblAgreementTransferFee_Branch.Text = CStr(oProduct.AgreementTransferFee) & " " & Behaviour_Value(oProduct.AgreementTransferFeeBehaviour)

                ''ChangeDueDate Fee
                'E_txtChangeDueDateFee.Text = CStr(oProduct.ChangeDueDateFee_HO)
                'Hide_ChangeDueDateFee.Value = CStr(oProduct.ChangeDueDateFee_HO)
                'Hide_ChangeDueDateFeeBeh.Value = oProduct.ChangeDueDateFeeBehaviour_HO
                'E_cboChangeDueDateFee.SelectedIndex = E_cboChangeDueDateFee.Items.IndexOf(E_cboChangeDueDateFee.Items.FindByValue(oProduct.ChangeDueDateFeeBehaviour_HO))
                'Behaviour_Lock(oProduct.ChangeDueDateFeeBehaviour_HO, E_txtChangeDueDateFee, E_cboChangeDueDateFee)


                'lblChangeDueDateFee_Branch.Text = CStr(oProduct.ChangeDueDateFee) & " " & Behaviour_Value(oProduct.ChangeDueDateFeeBehaviour)

                ''AssetReplacement Fee
                'E_txtAssetReplacementFee.Text = CStr(oProduct.AssetReplacementFee_HO)
                'Hide_AssetReplacementFee.Value = CStr(oProduct.AssetReplacementFee_HO)
                'Hide_AssetReplacementFeeBeh.Value = oProduct.AssetReplacementFeeBehaviour_HO

                'E_cboAssetReplacementFeeBehaviour.SelectedIndex = E_cboAssetReplacementFeeBehaviour.Items.IndexOf(E_cboAssetReplacementFeeBehaviour.Items.FindByValue(oProduct.AssetReplacementFeeBehaviour_HO))
                'Behaviour_Lock(oProduct.AssetReplacementFeeBehaviour_HO, E_txtAssetReplacementFee, E_cboAssetReplacementFeeBehaviour)

                'lblAssetReplacementFee_Branch.Text = CStr(oProduct.AssetReplacementFee) & " " & Behaviour_Value(oProduct.AssetReplacementFeeBehaviour)

                ''Reposses Fee
                'E_txtRepossesFee.Text = CStr(oProduct.RepossesFee_HO)
                'Hide_RepossesFee.Value = CStr(oProduct.RepossesFee_HO)
                'Hide_RepossesFeeBeh.Value = oProduct.RepossesFeeBehaviour_HO

                'E_cboRepossesFeeBehaviour.SelectedIndex = E_cboRepossesFeeBehaviour.Items.IndexOf(E_cboRepossesFeeBehaviour.Items.FindByValue(oProduct.RepossesFeeBehaviour_HO))
                'Behaviour_Lock(oProduct.RepossesFeeBehaviour_HO, E_txtRepossesFee, E_cboRepossesFeeBehaviour)

                'lblRepossesFee_Branch.Text = CStr(oProduct.RepossesFee) & " " & Behaviour_Value(oProduct.RepossesFeeBehaviour)

                ''LegalisirDoc Fee
                'E_LegalizedDocumentFee.Text = CStr(oProduct.LegalisirDocFee_HO)
                'Hide_LegalizedDocumentFee.Value = CStr(oProduct.LegalisirDocFee_HO)
                'Hide_LegalizedDocumentFeeBeh.Value = oProduct.LegalisirDocFeeBehaviour_HO
                'E_cboLegalizedDocumentFeeBehaviour.SelectedIndex = E_cboLegalizedDocumentFeeBehaviour.Items.IndexOf(E_cboLegalizedDocumentFeeBehaviour.Items.FindByValue(oProduct.LegalisirDocFeeBehaviour_HO))
                'Behaviour_Lock(oProduct.LegalisirDocFeeBehaviour_HO, E_LegalizedDocumentFee, E_cboLegalizedDocumentFeeBehaviour)

                'lblLegalizedDocumentFee_Branch.Text = CStr(oProduct.LegalisirDocFee) & " " & Behaviour_Value(oProduct.LegalisirDocFeeBehaviour)

                ''PDCBounce Fee
                'E_PDCBounceFee.Text = CStr(oProduct.PDCBounceFee_HO)
                'Hide_PDCBounceFee.Value = CStr(oProduct.PDCBounceFee_HO)
                'Hide_PDCBounceFeeBeh.Value = oProduct.PDCBounceFeeBehaviour_HO
                'E_cboPDCBounceFeeBehaviour.SelectedIndex = E_cboPDCBounceFeeBehaviour.Items.IndexOf(E_cboPDCBounceFeeBehaviour.Items.FindByValue(oProduct.PDCBounceFeeBehaviour_HO))
                'Behaviour_Lock(oProduct.PDCBounceFeeBehaviour_HO, E_PDCBounceFee, E_cboPDCBounceFeeBehaviour)



                'lblPDCBounceFee_Branch.Text = CStr(oProduct.PDCBounceFee) & " " & Behaviour_Value(oProduct.PDCBounceFeeBehaviour)

                ''InsuranceAdmin Fee
                'E_txtInsuranceAdminFee.Text = CStr(oProduct.InsAdminFee_HO)
                'Hide_InsuranceAdminFee.Value = CStr(oProduct.InsAdminFee_HO)
                'Hide_InstallmentToleranceAmountBeh.Value = oProduct.InsAdminFeeBehaviour_HO
                'E_cboInsuranceAdminFeeBehaviour.SelectedIndex = E_cboInsuranceAdminFeeBehaviour.Items.IndexOf(E_cboInsuranceAdminFeeBehaviour.Items.FindByValue(oProduct.InsAdminFeeBehaviour_HO))
                'Behaviour_Lock(oProduct.InsAdminFeeBehaviour_HO, E_txtInsuranceAdminFee, E_cboInsuranceAdminFeeBehaviour)

                'lblInsuranceAdminFee_Branch.Text = CStr(oProduct.InsAdminFee) & " " & Behaviour_Value(oProduct.InsAdminFeeBehaviour)

                ''InsuranceStampDutyFee 
                'E_txtInsuranceStampDutyFee.Text = CStr(oProduct.InsStampDutyFee_HO)
                'Hide_InsuranceStampDutyFee.Value = CStr(oProduct.InsStampDutyFee_HO)
                'Hide_InsuranceStampDutyFeeBeh.Value = oProduct.InsStampDutyFeeBehaviour_HO
                'E_cboInsuranceStampDutyFeeBehaviour.SelectedIndex = E_cboInsuranceStampDutyFeeBehaviour.Items.IndexOf(E_cboInsuranceStampDutyFeeBehaviour.Items.FindByValue(oProduct.InsStampDutyFeeBehaviour_HO))
                'Behaviour_Lock(oProduct.InsStampDutyFeeBehaviour_HO, E_txtInsuranceStampDutyFee, E_cboInsuranceStampDutyFeeBehaviour)

                'lblInsuranceStampDutyFee_Branch.Text = CStr(oProduct.InsStampDutyFee) & " " & Behaviour_Value(oProduct.InsStampDutyFeeBehaviour)

                ''BiayaPolis 
                'E_txtBiayaPolis.Text = CStr(oProduct.InsStampDutyFee_HO)
                'Hide_BiayaPolis.Value = CStr(oProduct.InsStampDutyFee_HO)
                'Hide_BiayaPolisBeh.Value = oProduct.InsStampDutyFeeBehaviour_HO
                'E_cboBiayaPolisBehaviour.SelectedIndex = E_cboBiayaPolisBehaviour.Items.IndexOf(E_cboBiayaPolisBehaviour.Items.FindByValue(oProduct.InsStampDutyFeeBehaviour_HO))
                'Behaviour_Lock(oProduct.InsStampDutyFeeBehaviour_HO, E_txtBiayaPolis, E_cboBiayaPolisBehaviour)

                'lblBiayaPolis_Branch.Text = CStr(oProduct.InsStampDutyFee) & " " & Behaviour_Value(oProduct.InsStampDutyFeeBehaviour)



                ''Installment Tolerance Amount
                'E_txtInstallmentToleranceAmount.Text = CStr(oProduct.InstallmentToleranceAmount_HO)
                'Hide_InstallmentToleranceAmount.Value = CStr(oProduct.InstallmentToleranceAmount_HO)
                'Hide_InstallmentToleranceAmountBeh.Value = oProduct.InstallmentToleranceAmountBehaviour_HO
                'E_cboInstallmentToleranceAmountBehaviour.SelectedIndex = E_cboInstallmentToleranceAmountBehaviour.Items.IndexOf(E_cboInstallmentToleranceAmountBehaviour.Items.FindByValue(oProduct.InstallmentToleranceAmountBehaviour_HO))
                'Behaviour_Lock(oProduct.InstallmentToleranceAmountBehaviour_HO, E_txtInstallmentToleranceAmount, E_cboInstallmentToleranceAmountBehaviour)

                'lblInstallmentToleranceAmount_Branch.Text = CStr(oProduct.InstallmentToleranceAmount) & " " & Behaviour_Value(oProduct.InstallmentToleranceAmountBehaviour)
                'POExpiration Days
                'E_txtPOExpirationDays.Text = CStr(oProduct.DaysPOExpiration_HO)
                'Hide_POExpirationDays.Value = CStr(oProduct.DaysPOExpiration_HO)
                'Hide_POExpirationDaysBeh.Value = oProduct.DaysPOExpirationBehaviour_HO
                'E_cboPOExpirationDaysBehaviour.SelectedIndex = E_cboPOExpirationDaysBehaviour.Items.IndexOf(E_cboPOExpirationDaysBehaviour.Items.FindByValue(oProduct.DaysPOExpirationBehaviour_HO))
                'Behaviour_Lock(oProduct.DaysPOExpirationBehaviour_HO, E_txtPOExpirationDays, E_cboPOExpirationDaysBehaviour)

                'lblPOExpirationDays_Branch.Text = CStr(oProduct.DaysPOExpiration) & " days " & Behaviour_Value(oProduct.DaysPOExpirationBehaviour)
                'DP Percentage Amount
                'E_txtDPPercentage.Text = CStr(oProduct.DPPercentage_HO)
                'Hide_DPPercentage.Value = CStr(oProduct.DPPercentage_HO)
                'Hide_DPPercentageBeh.Value = oProduct.DPPercentageBehaviour_HO
                'E_cboDPPercentageBehaviour.SelectedIndex = E_cboDPPercentageBehaviour.Items.IndexOf(E_cboDPPercentageBehaviour.Items.FindByValue(oProduct.DPPercentageBehaviour_HO))
                'Behaviour_Lock(oProduct.DPPercentageBehaviour_HO, E_txtDPPercentage, E_cboDPPercentageBehaviour)


                'lblDPPercentage_Branch.Text = CStr(oProduct.DPPercentage) & " " & Behaviour_Value(oProduct.DPPercentageBehaviour)

                'Reject Minimum Income


                ' SPAutomatic
                'E_cboIsSPAutomatic.SelectedIndex = E_cboIsSPAutomatic.Items.IndexOf(E_cboIsSPAutomatic.Items.FindByValue(strIsSPAutomatic_HO))
                'E_txtLengthSPProcess.Text = CStr(oProduct.LengthSPProcess_HO)

                'Hide_LengthSPProcess.Value = CStr(oProduct.LengthSPProcess_HO)
                'Behaviour_Lock(oProduct.IsSPAutomatic_HO, E_txtLengthSPProcess, E_cboIsSPAutomatic)


                'lblLengthSPProcess_Branch.Text = IIf(oProduct.IsSPAutomatic = True, "Yes", "No") & " Length SP Process " & CStr(oProduct.LengthSPProcess) & " days"

                '' SP1Automatic
                'E_cboIsSP1Automatic.SelectedIndex = E_cboIsSP1Automatic.Items.IndexOf(E_cboIsSP1Automatic.Items.FindByValue(strIsSP1Automatic_HO))
                'E_txtLengthSP1Process.Text = CStr(oProduct.LengthSP1Process_HO)
                'Hide_LengthSP1Process.Value = CStr(oProduct.LengthSP1Process_HO)

                'Behaviour_Lock(oProduct.IsSP1Automatic_HO, E_txtLengthSP1Process, E_cboIsSP1Automatic)

                'lblLengthSP1Process_Branch.Text = IIf(oProduct.IsSP1Automatic = True, "Yes", "No") & " Length SP 1 Process " & CStr(oProduct.LengthSP1Process) & " days"

                '' SP2Automatic
                'E_cboIsSP2Automatic.SelectedIndex = E_cboIsSP2Automatic.Items.IndexOf(E_cboIsSP2Automatic.Items.FindByValue(strIsSP2Automatic_HO))
                'E_txtLengthSP2Process.Text = CStr(oProduct.LengthSP2Process_HO)
                'Hide_LengthSP2Process.Value = CStr(oProduct.LengthSP2Process_HO)

                'If oProduct.IsSP2Automatic_HO = True Then
                '    Behaviour_Lock(oProduct.IsSP2AutomaticBehaviour_HO, E_txtLengthSP2Process, E_cboIsSP2Automatic) 
                'Else
                '    E_cboIsSP2Automatic.Enabled = False
                '    E_txtLengthSP2Process.Enabled = False
                'End If
                'lblLengthSP2Process_Branch.Text = IIf(oProduct.IsSP2Automatic = True, "Yes", "No") & " Length SP 2 Process " & CStr(oProduct.LengthSP2Process) & " days"

                ''Length Main Document Processed
                'E_txtLengthMainDocProcess.Text = CStr(oProduct.LengthMainDocProcess_HO)
                'Hide_LengthMainDocProcess.Value = CStr(oProduct.LengthMainDocProcess_HO)
                'Hide_LengthMainDocProcessBeh.Value = oProduct.LengthMainDocProcessBehaviour_HO
                'E_cboLengthMainDocProcessBehaviour.SelectedIndex = E_cboLengthMainDocProcessBehaviour.Items.IndexOf(E_cboLengthMainDocProcessBehaviour.Items.FindByValue(oProduct.LengthMainDocProcessBehaviour_HO))
                'Behaviour_Lock(oProduct.LengthMainDocProcessBehaviour_HO, E_txtLengthMainDocProcess, E_cboLengthMainDocProcessBehaviour)

                'lblLengthMainDocProcess_Branch.Text = CStr(oProduct.LengthMainDocProcess) & " days " & Behaviour_Value(oProduct.LengthMainDocProcessBehaviour)

                ''Length Main Document Taken
                'E_txtLengthMainDocTaken.Text = CStr(oProduct.LengthMainDocTaken_HO)
                'Hide_LengthMainDocTaken.Value = CStr(oProduct.LengthMainDocTaken_HO)
                'Hide_LengthMainDocProcessBeh.Value = oProduct.LengthMainDocTakenBehaviour_HO
                'E_cboLengthMainDocTakenBehaviour.SelectedIndex = E_cboLengthMainDocTakenBehaviour.Items.IndexOf(E_cboLengthMainDocTakenBehaviour.Items.FindByValue(oProduct.LengthMainDocTakenBehaviour_HO))
                'Behaviour_Lock(oProduct.LengthMainDocTakenBehaviour_HO, E_txtLengthMainDocTaken, E_cboLengthMainDocTakenBehaviour)

                'lblLengthMainDocTaken_Branch.Text = CStr(oProduct.LengthMainDocTaken) & " days " & Behaviour_Value(oProduct.LengthMainDocTakenBehaviour)


                ''Grace Period Late Charges
                'E_txtGracePeriodLateCharges.Text = CStr(oProduct.GracePeriodLateCharges_HO)
                'Hide_GracePeriodLateCharges.Value = CStr(oProduct.GracePeriodLateCharges_HO)
                'Hide_GracePeriodLateChargesBeh.Value = oProduct.GracePeriodLateChargesBehaviour_HO
                'E_cboGracePeriodLateChargesBehaviour.SelectedIndex = E_cboGracePeriodLateChargesBehaviour.Items.IndexOf(E_cboGracePeriodLateChargesBehaviour.Items.FindByValue(oProduct.GracePeriodLateChargesBehaviour_HO))
                'Behaviour_Lock(oProduct.GracePeriodLateChargesBehaviour_HO, E_txtGracePeriodLateCharges, E_cboGracePeriodLateChargesBehaviour)

                'lblGracePeriodLateCharges_Branch.Text = CStr(oProduct.GracePeriodLateCharges) & " days " & Behaviour_Value(oProduct.GracePeriodLateChargesBehaviour)

                '===

                'lblPenaltyRateEffectiveDate_Branch.Text = oProduct.PenaltyRateEffectiveDate.ToString("dd/MM/yyyy")
                'If oProduct.PenaltyBasedOn = "PB" Then
                '    lblPenaltyBasedOn_Branch.Text = "Pokok + Bunga"
                'ElseIf oProduct.PenaltyBasedOn = "PO" Then
                '    lblPenaltyBasedOn_Branch.Text = "Pokok"
                'End If
                'lblPenaltyBasedOnBehaviour_Branch.Text = Behaviour_Value(oProduct.PenaltyBasedOnBehaviour)



                'lblPrioritasPembayaran_Branch.Text = IIf(oProduct.PrioritasPembayaran = "DE", "Denda", "Bebas")

                'lblPrepaymentPenaltyEffectiveDate_Branch.Text = oProduct.PrepaymentPenaltyEffectiveDate.ToString("dd/MM/yyyy")
                'lblPrepaymentPenaltyPrevious_Branch.Text = CStr(FormatNumber(oProduct.PrepaymentPenaltyPrevious, 10))


                'lblPrepaymentPenaltyFixed_Branch.Text = CStr(FormatNumber(oProduct.PrepaymentPenaltyFixed, 2))
                'lblPrepaymentPenaltyFixedBehaviour_Branch.Text = Behaviour_Value(oProduct.PrepaymentPenaltyFixedBehaviour)



                'lblPrepaymentPenaltyFixedEffectiveDate_Branch.Text = oProduct.PrepaymentPenaltyFixedEffectiveDate.ToString("dd/MM/yyyy")
                'lblPrepaymentPenaltyFixedPrevious_Branch.Text = CStr(FormatNumber(oProduct.PrepaymentPenaltyFixedPrevious, 2))



                '============

                'txtPrepaymentPenaltyFixed.Text = CStr(FormatNumber(oProduct.PrepaymentPenaltyFixed_HO, 2))
                'cboPrepaymentPenaltyFixedBehaviour.SelectedIndex = cboPrepaymentPenaltyFixedBehaviour.Items.IndexOf(cboPrepaymentPenaltyFixedBehaviour.Items.FindByValue(oProduct.PrepaymentPenaltyFixedBehaviour_HO))
                'cboPenaltyBasedOn.SelectedIndex = cboPenaltyBasedOn.Items.IndexOf(cboPenaltyBasedOn.Items.FindByValue(oProduct.PenaltyBasedOnBehaviour_HO))
                'rboPrioritasPembayaran.SelectedIndex = rboPrioritasPembayaran.Items.IndexOf(rboPrioritasPembayaran.Items.FindByValue(oProduct.PrioritasPembayaran_HO))
                'rboPenaltyBasedOn.SelectedIndex = rboPenaltyBasedOn.Items.IndexOf(rboPenaltyBasedOn.Items.FindByValue(oProduct.PenaltyBasedOn_HO))
                'Behaviour_Lock(oProduct.PrepaymentPenaltyFixedBehaviour_HO, txtPrepaymentPenaltyFixed, cboPrepaymentPenaltyFixedBehaviour)

                'Behaviour_Lock(oProduct.PenaltyBasedOnBehaviour_HO, rboPenaltyBasedOn, cboPrepaymentPenaltyFixedBehaviour)


                'txtPenaltyRatePrevious.Text = CStr(FormatNumber(oProduct.PenaltyRatePrevious_HO, 10))
                'txtPenaltyRateEffectiveDate.Text = oProduct.PenaltyRateEffectiveDate_HO.ToString("dd/MM/yyyy")
                'txtPrepaymentPenaltyPrevious.Text = CStr(FormatNumber(oProduct.PrepaymentPenaltyPrevious_HO, 10))
                'txtPrepaymentPenaltyEffectiveDate.Text = oProduct.PrepaymentPenaltyEffectiveDate_HO.ToString("dd/MM/yyyy")
                'txtPrepaymentPenaltyFixedPrevious.Text = CStr(FormatNumber(oProduct.PrepaymentPenaltyFixedPrevious_HO, 2))
                'txtPrepaymentPenaltyFixedEffectiveDate.Text = oProduct.PrepaymentPenaltyFixedEffectiveDate_HO.ToString("dd/MM/yyyy")





                'txtUmurKendaraanFrom.Text = oProduct.UmurKendaraanFrom_HO.ToString
                'txtUmurKendaraanTo.Text = oProduct.UmurKendaraanTo_HO.ToString
                '-----

                'Prepayment Penalty Rate
                'E_txtPrepaymentPenaltyRate.Text = CStr(FormatNumber(oProduct.PrepaymentPenaltyRate_HO, 6))
                'Hide_PrepaymentPenaltyRate.Value = E_txtPrepaymentPenaltyRate.Text
                'Hide_PrepaymentPenaltyRateBeh.Value = oProduct.PrepaymentPenaltyRateBehaviour_HO
                'E_cboPrepaymentPenaltyRateBehaviour.SelectedIndex = E_cboPrepaymentPenaltyRateBehaviour.Items.IndexOf(E_cboPrepaymentPenaltyRateBehaviour.Items.FindByValue(oProduct.PrepaymentPenaltyRateBehaviour_HO))
                'Behaviour_Lock(oProduct.PrepaymentPenaltyRateBehaviour_HO, E_txtPrepaymentPenaltyRate, E_cboPrepaymentPenaltyRateBehaviour)

                'lblPrepaymentPenaltyRate_Branch.Text = CStr(FormatNumber(oProduct.PrepaymentPenaltyRate, 6)) & " % " & Behaviour_Value(oProduct.PrepaymentPenaltyRateBehaviour)

                'Days to remind Installment
                'E_txtDeskCollPhoneRemind.Text = CStr(oProduct.DeskCollPhoneRemind_HO)
                'Hide_DeskCollPhoneRemind.Value = CStr(oProduct.DeskCollPhoneRemind_HO)
                'Hide_DeskCollPhoneRemindBeh.Value = oProduct.DeskCollPhoneRemindBehaviour_HO
                'E_cboDeskCollPhoneRemindBehaviour.SelectedIndex = E_cboDeskCollPhoneRemindBehaviour.Items.IndexOf(E_cboDeskCollPhoneRemindBehaviour.Items.FindByValue(oProduct.DeskCollPhoneRemindBehaviour_HO))
                'Behaviour_Lock(oProduct.DeskCollPhoneRemindBehaviour_HO, E_txtDeskCollPhoneRemind, E_cboDeskCollPhoneRemindBehaviour)

                'lblDeskCollPhoneRemind_Branch.Text = CStr(oProduct.DeskCollPhoneRemind) & " days " & Behaviour_Value(oProduct.DeskCollPhoneRemindBehaviour)


                ''Days overdue to call
                'E_txtDeskCollOD.Text = CStr(oProduct.DeskCollOD_HO)
                'Hide_DeskCollOD.Value = CStr(oProduct.DeskCollOD_HO)
                'Hide_DeskCollODBeh.Value = oProduct.DeskCollODBehaviour_HO
                'E_cboDeskCollODBehaviour.SelectedIndex = E_cboDeskCollODBehaviour.Items.IndexOf(E_cboDeskCollODBehaviour.Items.FindByValue(oProduct.DeskCollODBehaviour_HO))
                'Behaviour_Lock(oProduct.DeskCollODBehaviour_HO, E_txtDeskCollOD, E_cboDeskCollODBehaviour)

                'lblDeskCollOD_Branch.Text = CStr(oProduct.DeskCollOD) & " days " & Behaviour_Value(oProduct.DeskCollODBehaviour)


                ''Previous overdue to remind
                'E_txtPrevODToRemind.Text = CStr(oProduct.PrevODToRemind_HO)
                'Hide_PrevODToRemind.Value = CStr(oProduct.PrevODToRemind_HO)
                'Hide_PrevODToRemindBeh.Value = oProduct.PrevODToRemindBehaviour_HO
                'E_cboPrevODToRemindBehaviour.SelectedIndex = E_cboPrevODToRemindBehaviour.Items.IndexOf(E_cboPrevODToRemindBehaviour.Items.FindByValue(oProduct.PrevODToRemindBehaviour_HO))
                'Behaviour_Lock(oProduct.PrevODToRemindBehaviour_HO, E_txtPrevODToRemind, E_cboPrevODToRemindBehaviour)

                'lblPrevODToRemind_Branch.Text = CStr(oProduct.PrevODToRemind) & " days " & Behaviour_Value(oProduct.PrevODToRemindBehaviour)

                ''PDC request to call
                'E_txtPDCDaysToRemind.Text = CStr(oProduct.PDCDayToRemind_HO)
                'Hide_PDCDaysToRemind.Value = CStr(oProduct.PDCDayToRemind_HO)
                'Hide_PDCDaysToRemindBeh.Value = oProduct.PDCDayToRemindBehaviour_HO
                'E_cboPDCDaysToRemindBehaviour.SelectedIndex = E_cboPDCDaysToRemindBehaviour.Items.IndexOf(E_cboPDCDaysToRemindBehaviour.Items.FindByValue(oProduct.PDCDayToRemindBehaviour_HO))
                'Behaviour_Lock(oProduct.PDCDayToRemindBehaviour_HO, E_txtPDCDaysToRemind, E_cboPDCDaysToRemindBehaviour)

                'lblPDCDaysToRemind_Branch.Text = CStr(oProduct.PDCDayToRemind) & " Dayss " & Behaviour_Value(oProduct.PDCDayToRemindBehaviour)

                ''Days to remind by SMS
                'E_txtDeskCollSMSRemind.Text = CStr(oProduct.DeskCollSMSRemind_HO)
                'Hide_DeskCollSMSRemind.Value = CStr(oProduct.DeskCollSMSRemind_HO)
                'Hide_DeskCollSMSRemindBeh.Value = oProduct.DeskCollSMSRemindBehaviour_HO
                'E_cboDeskCollSMSRemindBehaviour.SelectedIndex = E_cboDeskCollSMSRemindBehaviour.Items.IndexOf(E_cboDeskCollSMSRemindBehaviour.Items.FindByValue(oProduct.DeskCollSMSRemindBehaviour_HO))
                'Behaviour_Lock(oProduct.DeskCollSMSRemindBehaviour_HO, E_txtDeskCollSMSRemind, E_cboDeskCollSMSRemindBehaviour)

                'lblDeskCollSMSRemind_Branch.Text = CStr(oProduct.DeskCollSMSRemind) & " days " & Behaviour_Value(oProduct.DeskCollSMSRemindBehaviour)


                ''Days to generate DCR
                'E_txtDCR.Text = CStr(oProduct.DCR_HO)
                'Hide_DCR.Value = CStr(oProduct.DCR_HO)
                'Hide_DCRBeh.Value = oProduct.DCRBehaviour_HO
                'E_cboDCRBehaviour.SelectedIndex = E_cboDCRBehaviour.Items.IndexOf(E_cboDCRBehaviour.Items.FindByValue(oProduct.DCRBehaviour_HO))

                'Behaviour_Lock(oProduct.DCRBehaviour_HO, E_txtDCR, E_cboDCRBehaviour)

                'lblDCR_Branch.Text = CStr(oProduct.DCR) & " days " & Behaviour_Value(oProduct.DCRBehaviour)

                ''Days before due to generate Receipt Notes
                'E_txtDaysBeforeDueToRN.Text = CStr(oProduct.DaysBeforeDueToRN_HO)
                'Hide_DaysBeforeDueToRN.Value = CStr(oProduct.DaysBeforeDueToRN_HO)
                'Hide_DaysBeforeDueToRNBeh.Value = oProduct.DaysBeforeDueToRNBehaviour_HO
                'E_cboDaysBeforeDueToRNBehaviour.SelectedIndex = E_cboDaysBeforeDueToRNBehaviour.Items.IndexOf(E_cboDaysBeforeDueToRNBehaviour.Items.FindByValue(oProduct.DaysBeforeDueToRNBehaviour_HO))
                'Behaviour_Lock(oProduct.DaysBeforeDueToRNBehaviour_HO, E_txtDaysBeforeDueToRN, E_cboDaysBeforeDueToRNBehaviour)

                'lblDaysBeforeDueToRN_Branch.Text = CStr(oProduct.DaysBeforeDueToRN) & " days " & Behaviour_Value(oProduct.DaysBeforeDueToRNBehaviour)

                ''Days to generate RAL
                'E_txtODToRAL.Text = CStr(oProduct.ODToRAL_HO)
                'Hide_ODToRAL.Value = CStr(oProduct.ODToRAL_HO)
                'Hide_ODToRALBeh.Value = oProduct.ODToRALBehaviour_HO
                'E_cboODToRALBehaviour.SelectedIndex = E_cboODToRALBehaviour.Items.IndexOf(E_cboODToRALBehaviour.Items.FindByValue(oProduct.ODToRALBehaviour_HO))
                'Behaviour_Lock(oProduct.ODToRALBehaviour_HO, E_txtODToRAL, E_cboODToRALBehaviour)

                'lblODToRAL_Branch.Text = CStr(oProduct.ODToRAL) & " days " & Behaviour_Value(oProduct.ODToRALBehaviour)

                ''RAL Period
                'E_txtRALPeriod.Text = CStr(oProduct.RALPeriod_HO)
                'Hide_RALPeriod.Value = CStr(oProduct.RALPeriod_HO)
                'Hide_RALPeriodBeh.Value = oProduct.RALPeriodBehaviour_HO
                'E_cboRALPeriodBehaviour.SelectedIndex = E_cboRALPeriodBehaviour.Items.IndexOf(E_cboRALPeriodBehaviour.Items.FindByValue(oProduct.RALPeriodBehaviour_HO))
                'Behaviour_Lock(oProduct.RALPeriodBehaviour_HO, E_txtRALPeriod, E_cboRALPeriodBehaviour)

                'lblRALPeriod_Branch.Text = CStr(oProduct.RALPeriod) & " days " & Behaviour_Value(oProduct.RALPeriodBehaviour)


                ''Maximum days Receipt notes paid
                'E_txtMaxDaysRNPaid.Text = CStr(oProduct.MaxDaysRNPaid_HO)
                'Hide_MaxDaysRNPaid.Value = CStr(oProduct.MaxDaysRNPaid_HO)
                'Hide_MaxDaysRNPaidBeh.Value = oProduct.MaxDaysRNPaidBehaviour_HO
                'E_cboMaxDaysRNPaidBehaviour.SelectedIndex = E_cboMaxDaysRNPaidBehaviour.Items.IndexOf(E_cboMaxDaysRNPaidBehaviour.Items.FindByValue(oProduct.MaxDaysRNPaidBehaviour_HO))
                'Behaviour_Lock(oProduct.MaxDaysRNPaidBehaviour_HO, E_txtMaxDaysRNPaid, E_cboMaxDaysRNPaidBehaviour)

                'lblMaxDaysRNPaid_Branch.Text = CStr(oProduct.MaxDaysRNPaid) & " days " & Behaviour_Value(oProduct.MaxDaysRNPaidBehaviour)


                ''Maximum Promise To Pay days
                'E_txtMaxPTPYDays.Text = CStr(oProduct.MaxPTPYDays_HO)
                'Hide_MaxPTPYDays.Value = CStr(oProduct.MaxPTPYDays_HO)
                'Hide_MaxPTPYDaysBeh.Value = oProduct.MaxPTPYDaysBehaviour_HO
                'E_cboMaxPTPYDaysBehaviour.SelectedIndex = E_cboMaxPTPYDaysBehaviour.Items.IndexOf(E_cboMaxPTPYDaysBehaviour.Items.FindByValue(oProduct.MaxPTPYDaysBehaviour_HO))
                'Behaviour_Lock(oProduct.MaxPTPYDaysBehaviour_HO, E_txtMaxPTPYDays, E_cboMaxPTPYDaysBehaviour)

                'lblMaxPTPYDays_Branch.Text = CStr(oProduct.MaxPTPYDays) & " days " & Behaviour_Value(oProduct.MaxPTPYDaysBehaviour)


                ''Promise To Pay To Bank
                'E_txtPTPYBank.Text = CStr(oProduct.PTPYBank_HO)
                'Hide_PTPYBank.Value = CStr(oProduct.PTPYBank_HO)
                'Hide_PTPYBankBeh.Value = oProduct.PTPYBankBehaviour_HO
                'E_txtPTPYBankBehaviour.SelectedIndex = E_txtPTPYBankBehaviour.Items.IndexOf(E_txtPTPYBankBehaviour.Items.FindByValue(oProduct.PTPYBankBehaviour_HO))
                'Behaviour_Lock(oProduct.PTPYBankBehaviour_HO, E_txtPTPYBank, E_txtPTPYBankBehaviour)

                'lblPTPYBank_Branch.Text = CStr(oProduct.PTPYBank) & " days " & Behaviour_Value(oProduct.PTPYBankBehaviour)


                ''Promise To Pay To Company
                'E_txtPTPYCompany.Text = CStr(oProduct.PTPYCompany_HO)
                'Hide_PTPYCompany.Value = CStr(oProduct.PTPYCompany_HO)
                'Hide_PTPYCompanyBeh.Value = oProduct.PTPYCompanyBehaviour_HO
                'E_cboPTPYCompanyBehaviour.SelectedIndex = E_cboPTPYCompanyBehaviour.Items.IndexOf(E_cboPTPYCompanyBehaviour.Items.FindByValue(oProduct.PTPYCompanyBehaviour_HO))
                'Behaviour_Lock(oProduct.PTPYCompanyBehaviour_HO, E_txtPTPYCompany, E_cboPTPYCompanyBehaviour)

                'lblPTPYCompany_Branch.Text = CStr(oProduct.PTPYCompany) & " days " & Behaviour_Value(oProduct.PTPYCompanyBehaviour)


                ''Promise To Pay To Supplier
                'E_txtPTPYSupplier.Text = CStr(oProduct.PTPYSupplier_HO)
                'Hide_PTPYSupplier.Value = CStr(oProduct.PTPYSupplier_HO)
                'Hide_PTPYSupplierBeh.Value = oProduct.PTPYSupplierBehaviour_HO
                'E_cboPTPYSupplierBehaviour.SelectedIndex = E_cboPTPYSupplierBehaviour.Items.IndexOf(E_cboPTPYSupplierBehaviour.Items.FindByValue(oProduct.PTPYSupplierBehaviour_HO))
                'Behaviour_Lock(oProduct.PTPYSupplierBehaviour_HO, E_txtPTPYSupplier, E_cboPTPYSupplierBehaviour)

                'lblPTPYSupplier_Branch.Text = CStr(oProduct.PTPYSupplier) & " days " & Behaviour_Value(oProduct.PTPYSupplierBehaviour)

                ''RAL Extension
                'E_txtRALExtension.Text = CStr(oProduct.RALExtension_HO)
                'Hide_RALExtension.Value = CStr(oProduct.RALExtension_HO)
                'Hide_RALExtensionBeh.Value = oProduct.RALExtensionBehaviour_HO
                'E_cboRALExtensionBehaviour.SelectedIndex = E_cboRALExtensionBehaviour.Items.IndexOf(E_cboRALExtensionBehaviour.Items.FindByValue(oProduct.RALExtensionBehaviour_HO))
                'Behaviour_Lock(oProduct.RALExtensionBehaviour_HO, E_txtRALExtension, E_cboRALExtensionBehaviour)

                'lblRALExtension_Branch.Text = CStr(oProduct.RALExtension) & " days " & Behaviour_Value(oProduct.RALExtensionBehaviour)


                ''Inventory Expected
                'E_txtInventoryExpected.Text = CStr(oProduct.InventoryExpected_HO)
                'Hide_InventoryExpected.Value = CStr(oProduct.InventoryExpected_HO)
                'Hide_InventoryExpectedBeh.Value = oProduct.InventoryExpectedBehaviour_HO
                'E_cboInventoryExpectedBehaviour.SelectedIndex = E_cboInventoryExpectedBehaviour.Items.IndexOf(E_cboInventoryExpectedBehaviour.Items.FindByValue(oProduct.InventoryExpectedBehaviour_HO))
                'Behaviour_Lock(oProduct.InventoryExpectedBehaviour_HO, E_txtInventoryExpected, E_cboInventoryExpectedBehaviour)

                'lblInventoryExpected_Branch.Text = CStr(oProduct.InventoryExpected) & " days " & Behaviour_Value(oProduct.InventoryExpectedBehaviour)

                'Billing Charges
                'E_txtBillingCharges.Text = CStr(oProduct.BillingCharges_HO)
                'Hide_BillingCharges.Value = CStr(oProduct.BillingCharges_HO)
                'Hide_BillingChargesBeh.Value = oProduct.BillingChargesBehaviour_HO
                'E_cboBillingChargesBehaviour.SelectedIndex = E_cboBillingChargesBehaviour.Items.IndexOf(E_cboBillingChargesBehaviour.Items.FindByValue(oProduct.BillingChargesBehaviour_HO))
                'Behaviour_Lock(oProduct.BillingChargesBehaviour_HO, E_txtBillingCharges, E_cboBillingChargesBehaviour)

                'lblBillingCharges_Branch.Text = CStr(oProduct.BillingCharges) & "  " & Behaviour_Value(oProduct.BillingChargesBehaviour)




                'lblInsuranceDiscPercentage_Branch.Text = CStr(FormatNumber(oProduct.InsuranceDiscPercentage, 10))
                'lblInsuranceDiscPercentageBehaviour_Branch.Text = Behaviour_Value(oProduct.InsuranceDiscPercentageBehaviour)
                'txtInsuranceDiscPercentage.Text = CStr(FormatNumber(oProduct.InsuranceDiscPercentage_HO, 10))
                'cboInsuranceDiscPercentageBehaviour.SelectedIndex = cboInsuranceDiscPercentageBehaviour.Items.IndexOf(cboInsuranceDiscPercentageBehaviour.Items.FindByValue(oProduct.InsuranceDiscPercentageBehaviour_HO))
                'Behaviour_Lock(oProduct.InsuranceDiscPercentageBehaviour_HO, txtInsuranceDiscPercentage, cboInsuranceDiscPercentageBehaviour)

                ''Limit A/P Disbursement at Branch
                'E_txtLimitAPCash.Text = CStr(oProduct.LimitAPCash_HO)
                'Hide_LimitAPCash.Value = CStr(oProduct.LimitAPCash_HO)
                'Hide_LimitAPCashBeh.Value = oProduct.LimitAPCashBehaviour_HO
                'E_cboLimitAPCashBehaviour.SelectedIndex = E_cboLimitAPCashBehaviour.Items.IndexOf(E_cboLimitAPCashBehaviour.Items.FindByValue(oProduct.LimitAPCashBehaviour_HO))
                'Behaviour_Lock(oProduct.LimitAPCashBehaviour_HO, E_txtLimitAPCash, E_cboLimitAPCashBehaviour) 
                'lblLimitAPCash_Branch.Text = CStr(oProduct.LimitAPCash) & "  " & Behaviour_Value(oProduct.LimitAPCashBehaviour)


                ''Is Recourse
                'Dim strIsRecourse As String
                'If oProduct.IsRecourse_HO = True Then
                '    strIsRecourse = "Yes"
                'Else
                '    strIsRecourse = "No"
                'End If
                'E_lblIsRecourse.Text = strIsRecourse

                'Dim strIsActive As String
                'If oProduct.IsActive_HO = True Then
                '    strIsActive = "1"
                'Else
                '    strIsActive = "0"
                'End If
                'E_rboIsActive.SelectedIndex = E_rboIsActive.Items.IndexOf(E_rboIsActive.Items.FindByValue(strIsActive))

                'Hide_AngsuranPertama.Value = oProduct.AngsuranPertama_HO
                'E_cboAngsuranPertama.SelectedIndex = E_cboAngsuranPertama.Items.IndexOf(E_cboAngsuranPertama.Items.FindByValue(oProduct.AngsuranPertama_HO))
                'lblAngsuranPertama_Branch.Text = IIf(oProduct.AngsuranPertama = "ARR", "ARREAR", "ADVANCE")

                'E_txtFlatRate.Text = CStr(FormatNumber(oProduct.SukuBungaFlat_HO, 6))
                'Hide_FlatRate.Value = E_txtFlatRate.Text
                'Hide_FlatRateBeh.Value = oProduct.SukuBungaFlatBehaviour_HO
                'E_cboFlatRateBehaviour.SelectedIndex = E_cboFlatRateBehaviour.Items.IndexOf(E_cboFlatRateBehaviour.Items.FindByValue(oProduct.SukuBungaFlatBehaviour_HO))
                'Behaviour_Lock(oProduct.SukuBungaFlatBehaviour_HO, E_txtFlatRate, E_cboFlatRateBehaviour)
                'lblFlatRate_Branch.Text = CStr(FormatNumber(oProduct.SukuBungaFlat, 6)) & " % " & Behaviour_Value(oProduct.SukuBungaFlatBehaviour)

                'Hide_OpsiUangMuka.Value = oProduct.OpsiUangMuka_HO
                'E_cboOpsiUangMuka.SelectedIndex = E_cboOpsiUangMuka.Items.IndexOf(E_cboOpsiUangMuka.Items.FindByValue(oProduct.OpsiUangMuka_HO))
                'lblOpsiUangMuka_Branch.Text = IIf(oProduct.OpsiUangMuka = "P", "PROSENTASE", "ANGSURAN")

                'txtUangMukaAngsuran.Text = CStr(oProduct.OpsiUangMukaAngsur)
                'Hide_UangMukaAngsuran.Value = CStr(oProduct.OpsiUangMukaAngsur)

                'lblUangMukaAngsuran_Branch.Text = CStr(oProduct.OpsiUangMukaAngsur) & " x"
            End If
        End If
    End Sub                
#End Region

#Region "imbSave"
    Private Sub imbSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonimbSave.Click
        If Page.IsValid Then
            Dim customClass As New Parameter.Product
            Dim ErrMessage As String = ""
            Dim oProduct As New Parameter.Product
            Dim BranchID As String = Me.sesBranchId
            BranchID = Replace(BranchID, "'", "")
             
            If Me.AddEdit = "EDIT" Then
                 
                UmumTab.CollectResult(oProduct)
                SkemaTab.CollectResult(oProduct)
                BiayaTab.CollectResult(oProduct) 
                AngsuranTab.CollectResult(oProduct)

                CollactionTab.CollectResult(oProduct)
                FinanceTab.CollectResult(oProduct)

                With oProduct 
                    .Branch_ID = Me.BranchID 
                    .InsurancePenaltyPercentage = CDec(E_txtInsurancePenaltyPercentage.Text.Trim)
                    .InsurancePenaltyPercentageBehaviour = E_cboInsurancePenaltyPercentageBehaviour.SelectedItem.Value.Trim
                    .IncomeInsRatioPercentage = CDec(txtIncomeInsRatioPercentage.Text.Trim)
                    .IncomeInsRatioPercentageBehaviour = cboIncomeInsRatioPercentageBehaviour.SelectedItem.Value.Trim
                    .strConnection = GetConnectionString()
                End With

                Try
                    m_controller.ProductBranchHOSaveEdit(oProduct)

                    ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
                    pnlEdit.Visible = False
                    pnlList.Visible = True

                    Me.CmdWhere = "a.ProductID='" & Me.ProductID & "'"
                    BindGridEntity(Me.CmdWhere)

                Catch exp As Exception
                End Try
            End If
            txtPage.Text = "1"
        End If
    End Sub
    '.ProductId = E_lblProductID.Text
    '.UmurKendaraanFrom = CInt(txtUmurKendaraanFrom.Text.Trim)
    '.UmurKendaraanTo = CInt(txtUmurKendaraanTo.Text.Trim)
    '.EffectiveRate = CDec(E_txtEffectiveRate.Text.Trim)
    '.EffectiveRateBehaviour = E_cboEffectiveRateBehaviour.SelectedItem.Value.Trim
    '.GrossYieldRate = CDec(E_txtGrossYieldRate.Text.Trim)
    '.GrossYieldRateBehaviour = E_cboGrossYieldRateBehaviour.SelectedItem.Value.Trim
    '.MinimumTenor = CInt(E_txtMinimumTenor.Text.Trim)
    '.MaximumTenor = CInt(E_txtMaximumTenor.Text.Trim)
    '.DaysPOExpiration = CInt(E_txtPOExpirationDays.Text.Trim)
    '.DaysPOExpirationBehaviour = E_cboPOExpirationDaysBehaviour.SelectedItem.Value.Trim
    '.DPPercentage = CDec(E_txtDPPercentage.Text.Trim)
    '.DPPercentageBehaviour = E_cboDPPercentageBehaviour.SelectedItem.Value.Trim

    '.AngsuranPertama = E_cboAngsuranPertama.SelectedItem.Value.Trim()
    '.SukuBungaFlat = CDec(E_txtFlatRate.Text)
    '.SukuBungaFlatBehaviour = E_cboFlatRateBehaviour.SelectedItem.Value.Trim()

    '.OpsiUangMuka = E_cboOpsiUangMuka.SelectedItem.Value.Trim()
    '.OpsiUangMukaAngsur = CInt(txtUangMukaAngsuran.Text)





    '.CancellationFee = CDec(E_txtCancellationFee.Text.Trim)
    '.CancellationFeeBehaviour = E_cboCancellationFeeBehaviour.SelectedItem.Value.Trim
    '.AdminFee = CDec(E_txtAdminFee.Text.Trim)
    '.AdminFeeBehaviour = E_cboAdminFeeBehaviour.SelectedItem.Value.Trim
    '.FiduciaFee = CDec(E_txtFiduciaFee.Text.Trim)
    '.FiduciaFeeBehaviour = E_cboFiduciaFeeBehaviour.SelectedItem.Value.Trim
    '.ProvisionFee = CDec(E_txtProvisionFee.Text.Trim)
    '.ProvisionFeeBehaviour = E_cboProvisionFeeBehaviour.SelectedItem.Value.Trim
    '.NotaryFee = CDec(E_txtNotaryFee.Text.Trim)
    '.NotaryFeeBehaviour = E_cboNotaryFeeBehaviour.SelectedItem.Value.Trim
    '.SurveyFee = CDec(E_txtSurveyFee.Text.Trim)
    '.SurveyFeeBehaviour = E_cboSurveyFeeBehaviour.SelectedItem.Value.Trim
    '.VisitFee = CDec(E_txtVisitFee.Text.Trim)
    '.VisitFeeBehaviour = E_cboVisitFeeBehaviour.SelectedItem.Value.Trim
    '.ReschedulingFee = CDec(E_txtReschedulingFee.Text.Trim)
    '.ReschedulingFeeBehaviour = E_cboReschedulingFeeBehaviour.SelectedItem.Value.Trim
    '.AgreementTransferFee = CDec(E_txtAgreementTransferFee.Text.Trim)
    '.AgreementTransferFeeBehaviour = E_cboAgreementTransferFeeBehaviour.SelectedItem.Value.Trim
    '.ChangeDueDateFee = CDec(E_txtChangeDueDateFee.Text.Trim)
    '.ChangeDueDateFeeBehaviour = E_cboChangeDueDateFee.SelectedItem.Value.Trim
    '.AssetReplacementFee = CDec(E_txtAssetReplacementFee.Text.Trim)
    '.AssetReplacementFeeBehaviour = E_cboAssetReplacementFeeBehaviour.SelectedItem.Value.Trim
    '.RepossesFee = CDec(E_txtRepossesFee.Text.Trim)
    '.RepossesFeeBehaviour = E_cboRepossesFeeBehaviour.SelectedItem.Value.Trim
    '.LegalisirDocFee = CDec(E_LegalizedDocumentFee.Text.Trim)
    '.LegalisirDocFeeBehaviour = E_cboLegalizedDocumentFeeBehaviour.SelectedItem.Value.Trim
    '.PDCBounceFee = CDec(E_PDCBounceFee.Text.Trim)
    '.PDCBounceFeeBehaviour = E_cboPDCBounceFeeBehaviour.SelectedItem.Value.Trim
    '.InsAdminFee = CDec(E_txtInsuranceAdminFee.Text.Trim)
    '.InsAdminFeeBehaviour = E_cboInsuranceAdminFeeBehaviour.SelectedItem.Value.Trim
    '.InsStampDutyFee = CDec(E_txtInsuranceStampDutyFee.Text.Trim)
    '.InsStampDutyFeeBehaviour = E_cboInsuranceStampDutyFeeBehaviour.SelectedItem.Value.Trim
    '.BiayaPolis = CDec(E_txtBiayaPolis.Text.Trim)
    '.BiayaPolisBehaviour = E_cboBiayaPolisBehaviour.SelectedItem.Value.Trim







    '.InsuranceDiscPercentage = CDec(txtInsuranceDiscPercentage.Text.Trim)
    '.InsuranceDiscPercentageBehaviour = cboInsuranceDiscPercentageBehaviour.SelectedItem.Value.Trim


    '.IsSPAutomatic = CBool(E_cboIsSPAutomatic.SelectedItem.Value.Trim)
    '.LengthSPProcess = CInt(E_txtLengthSPProcess.Text.Trim)

    '.IsSP1Automatic = CBool(E_cboIsSP1Automatic.SelectedItem.Value.Trim)
    '.LengthSP1Process = CInt(E_txtLengthSP1Process.Text.Trim)

    '.IsSP2Automatic = CBool(E_cboIsSP2Automatic.SelectedItem.Value.Trim)
    '.LengthSP2Process = CInt(E_txtLengthSP2Process.Text.Trim)

    '.LengthMainDocProcess = CInt(E_txtLengthMainDocProcess.Text.Trim)
    '.LengthMainDocProcessBehaviour = E_cboLengthMainDocProcessBehaviour.SelectedItem.Value.Trim
    '.LengthMainDocTaken = CInt(E_txtLengthMainDocTaken.Text.Trim)
    '.LengthMainDocTakenBehaviour = E_cboLengthMainDocTakenBehaviour.SelectedItem.Value.Trim
    '.GracePeriodLateCharges = CInt(E_txtGracePeriodLateCharges.Text.Trim)
    '.GracePeriodLateChargesBehaviour = E_cboGracePeriodLateChargesBehaviour.SelectedItem.Value.Trim


    '.PenaltyPercentage = CDec(E_txtPenaltyPercentage.Text.Trim)
    '.PenaltyPercentageBehaviour = E_cboPenaltyPercentageBehaviour.SelectedItem.Value.Trim
    '.InstallmentToleranceAmount = CDec(E_txtInstallmentToleranceAmount.Text.Trim)
    '.InstallmentToleranceAmountBehaviour = E_cboInstallmentToleranceAmountBehaviour.SelectedItem.Value.Trim

    '.PrepaymentPenaltyFixed = CDec(txtPrepaymentPenaltyFixed.Text.Trim)
    '.PrepaymentPenaltyFixedBehaviour = cboPrepaymentPenaltyFixedBehaviour.SelectedItem.Value.Trim
    '.PenaltyBasedOn = rboPenaltyBasedOn.SelectedItem.Value.Trim
    '.PenaltyBasedOnBehaviour = cboPenaltyBasedOn.SelectedItem.Value.Trim

    '.PenaltyRatePrevious = CDec(txtPenaltyRatePrevious.Text.Trim)
    '.PenaltyRateEffectiveDate = Date.ParseExact(txtPenaltyRateEffectiveDate.Text.ToString, "dd/MM/yyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo)
    '.PrepaymentPenaltyPrevious = CDec(txtPrepaymentPenaltyPrevious.Text.Trim)
    '.PrepaymentPenaltyEffectiveDate = Date.ParseExact(txtPrepaymentPenaltyEffectiveDate.Text.ToString, "dd/MM/yyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo)
    '.PrepaymentPenaltyFixedPrevious = CDec(txtPrepaymentPenaltyFixedPrevious.Text.Trim)
    '.PrepaymentPenaltyFixedEffectiveDate = Date.ParseExact(txtPrepaymentPenaltyFixedEffectiveDate.Text.ToString, "dd/MM/yyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo)
    '.PrioritasPembayaran = rboPrioritasPembayaran.SelectedItem.Value.Trim

    '.PrepaymentPenaltyRate = CDec(E_txtPrepaymentPenaltyRate.Text.Trim)
    '.PrepaymentPenaltyRateBehaviour = E_cboPrepaymentPenaltyRateBehaviour.SelectedItem.Value.Trim


    '.BillingCharges = CDec(E_txtBillingCharges.Text.Trim)
    '.BillingChargesBehaviour = E_cboBillingChargesBehaviour.SelectedItem.Value.Trim


    '.DeskCollPhoneRemind = CInt(E_txtDeskCollPhoneRemind.Text.Trim)
    '.DeskCollPhoneRemindBehaviour = E_cboDeskCollPhoneRemindBehaviour.SelectedItem.Value.Trim
    '.DeskCollOD = CInt(E_txtDeskCollOD.Text.Trim)
    '.DeskCollODBehaviour = E_cboDeskCollODBehaviour.SelectedItem.Value.Trim
    '.PrevODToRemind = CInt(E_txtPrevODToRemind.Text.Trim)
    '.PrevODToRemindBehaviour = E_cboPrevODToRemindBehaviour.SelectedItem.Value.Trim
    '.PDCDayToRemind = CInt(E_txtPDCDaysToRemind.Text.Trim)
    '.PDCDayToRemindBehaviour = E_cboPDCDaysToRemindBehaviour.SelectedItem.Value.Trim
    '.DeskCollSMSRemind = CInt(E_txtDeskCollSMSRemind.Text.Trim)
    '.DeskCollSMSRemindBehaviour = E_cboDeskCollSMSRemindBehaviour.SelectedItem.Value.Trim

    '.DCR = CInt(E_txtDCR.Text.Trim)
    '.DCRBehaviour = E_cboDCRBehaviour.SelectedItem.Value.Trim
    '.DaysBeforeDueToRN = CInt(E_txtDaysBeforeDueToRN.Text.Trim)
    '.DaysBeforeDueToRNBehaviour = E_cboDaysBeforeDueToRNBehaviour.SelectedItem.Value.Trim
    '.ODToRAL = CInt(E_txtODToRAL.Text.Trim)
    '.ODToRALBehaviour = E_cboODToRALBehaviour.SelectedItem.Value.Trim
    '.RALPeriod = CInt(E_txtRALPeriod.Text.Trim)
    '.RALPeriodBehaviour = E_cboRALPeriodBehaviour.SelectedItem.Value.Trim
    '.MaxDaysRNPaid = CInt(E_txtMaxDaysRNPaid.Text.Trim)
    '.MaxDaysRNPaidBehaviour = E_cboMaxDaysRNPaidBehaviour.SelectedItem.Value.Trim
    '.MaxPTPYDays = CInt(E_txtMaxPTPYDays.Text.Trim)
    '.MaxPTPYDaysBehaviour = E_cboMaxPTPYDaysBehaviour.SelectedItem.Value.Trim
    '.PTPYBank = CInt(E_txtPTPYBank.Text.Trim)
    '.PTPYBankBehaviour = E_txtPTPYBankBehaviour.SelectedItem.Value.Trim
    '.PTPYCompany = CInt(E_txtPTPYCompany.Text.Trim)
    '.PTPYCompanyBehaviour = E_cboPTPYCompanyBehaviour.SelectedItem.Value.Trim
    '.PTPYSupplier = CInt(E_txtPTPYSupplier.Text.Trim)
    '.PTPYSupplierBehaviour = E_cboPTPYSupplierBehaviour.SelectedItem.Value.Trim
    '.RALExtension = CInt(E_txtRALExtension.Text.Trim)
    '.RALExtensionBehaviour = E_cboRALExtensionBehaviour.SelectedItem.Value.Trim
    '.InventoryExpected = CInt(E_txtInventoryExpected.Text.Trim)
    '.InventoryExpectedBehaviour = E_cboInventoryExpectedBehaviour.SelectedItem.Value.Trim


    '.LimitAPCash = CInt(E_txtLimitAPCash.Text.Trim)
    '.LimitAPCashBehaviour = E_cboLimitAPCashBehaviour.SelectedItem.Value.Trim
    '.IsActive = CBool(E_rboIsActive.SelectedItem.Value.Trim)




#End Region

#Region "imbAdd"
    Private Sub imbAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonAdd.Click
        If CheckFeature(Me.Loginid, Me.FormID, "ADD", Me.AppId) Then
            pnlList.Visible = False
            pnlBranch.Visible = True
            Me.AddEdit = "ADD"
            hplB_ProductID.Text = hplProductID.Text
            B_lblDescription.Text = lblDescription.Text

            Dim oProduct As New Parameter.Product
            Dim dtEntity As DataTable

            With oProduct
                .ProductId = Me.ProductID
                .strConnection = GetConnectionString()
            End With
            oProduct = m_controller.GetBranch(oProduct)

            If Not oProduct Is Nothing Then
                dtEntity = oProduct.ListData
                recordCount = oProduct.TotalRecords
            Else
                recordCount = 0
            End If

            dtgBranchList.DataSource = dtEntity.DefaultView
            dtgBranchList.CurrentPageIndex = 0
            dtgBranchList.DataBind()

        End If
    End Sub
#End Region

#Region "Sorting"
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub
#End Region

#Region "imbReset"
    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        cboSearchBy.SelectedIndex = 0
        txtSearchBy.Text = ""
        txtPage.Text = "1"
        Me.CmdWhere = "a.ProductID='" & Me.ProductID & "'"
        BindGridEntity(Me.CmdWhere)
    End Sub
#End Region

#Region "imbSearch"
    Private Sub imbSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonSearch.Click
        If txtSearchBy.Text.Trim = "" Then
            Me.CmdWhere = "a.ProductID='" & Me.ProductID & "'"
        Else
            If cboSearchBy.SelectedItem.Value.Trim = "BranchFullName" Then
                Me.CmdWhere = "a.ProductID= '" & Me.ProductID & "' and g." & cboSearchBy.SelectedItem.Value & " = '" & txtSearchBy.Text.Trim & "'"
            Else
                Me.CmdWhere = "a.ProductID= '" & Me.ProductID & "' and a." & cboSearchBy.SelectedItem.Value & " = '" & txtSearchBy.Text.Trim & "'"
            End If
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub
#End Region

#Region "imbPrint"
    'Private Sub imbPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonPrint.Click
    '    If CheckFeature(Me.Loginid, Me.FormID, "PRINT", Me.AppId) Then
    '        SendCookies()
    '        Response.Redirect("Report/ProductBranchHORpt.aspx")
    '    End If
    'End Sub
#End Region

#Region "SendCookies"
    Sub SendCookies()
        If txtSearchBy.Text.Trim = "" Then
            Me.CmdWhere = "a.ProductID='" & Me.ProductID & "'"
        Else
            If cboSearchBy.SelectedItem.Value.Trim = "BranchFullName" Then
                Me.CmdWhere = "a.ProductID= '" & Me.ProductID & "' and g." & cboSearchBy.SelectedItem.Value & " = '" & txtSearchBy.Text.Trim & "'"
            Else
                Me.CmdWhere = "a.ProductID= '" & Me.ProductID & "' and a." & cboSearchBy.SelectedItem.Value & " = '" & txtSearchBy.Text.Trim & "'"
            End If
        End If

        Dim cookie As HttpCookie = Request.Cookies("ProductBranchHO")
        If Not cookie Is Nothing Then
            cookie.Values("PageFrom") = "ProductBranchHO"
            cookie.Values("cmdwhere") = Me.CmdWhere
            cookie.Values("Product_Description") = lblDescription.Text
            cookie.Values("ProductID") = Me.ProductID
            cookie.Values("IsActive") = CStr(Me.IsActive)
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie("ProductBranchHO")
            cookieNew.Values.Add("PageFrom", "ProductBranchHO")
            cookieNew.Values.Add("cmdwhere", Me.CmdWhere)
            cookieNew.Values.Add("Product_Description", lblDescription.Text)
            cookieNew.Values.Add("ProductID", Me.ProductID)
            cookieNew.Values.Add("IsActive", CStr(Me.IsActive))
            Response.AppendCookie(cookieNew)
        End If
    End Sub
#End Region

#Region "imbBack"
    Private Sub imbBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonBack.Click
        Response.Redirect("Product.aspx")
    End Sub
#End Region

#Region "imgOK"
    Private Sub B_imgOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonOK.Click
        Dim TotalRow_Branch As Integer
        Dim i As Integer
        Dim oProduct As New Parameter.Product
        TotalRow_Branch = dtgBranchList.Items.Count

        Dim lbl_BranchID As New Label
        Dim lbl_BranchName As New Label
        Dim chk_Branch As New CheckBox

        Dim strTotalBranchId As String = ""
        Dim strBranchId As String
        Dim counter_check As Integer = 0

        For i = 0 To TotalRow_Branch - 1
            chk_Branch = CType(dtgBranchList.Items(i).FindControl("chk_Branch"), CheckBox)
            If chk_Branch.Checked Then
                counter_check += 1
                lbl_BranchID = CType(dtgBranchList.Items(i).FindControl("lblBranchId"), Label)
                strBranchId = lbl_BranchID.Text.Trim

                strTotalBranchId = strTotalBranchId + strBranchId + ","
            End If
        Next

        If counter_check = 0 Then
            ShowMessage(lblMessage, "Harap assign minimal 1 Cabang", True)
            pnlBranch.Visible = True
            pnlList.Visible = False
            pnlEdit.Visible = False
            Exit Sub
        End If

        If strTotalBranchId <> "" Then
            strTotalBranchId = CStr(Left(strTotalBranchId, Len(strTotalBranchId) - 1))
        End If

        Try
            oProduct.ProductId = Me.ProductID
            oProduct.TotalBranch = strTotalBranchId
            oProduct.strConnection = GetConnectionString()
            m_controller.ProductBranchHOSaveAdd(oProduct)
            ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_SUCCESS, False)
            pnlEdit.Visible = False
            pnlBranch.Visible = False
            pnlList.Visible = True
            Me.CmdWhere = "ProductID = '" & Me.ProductID & "'"
            BindGridEntity(Me.CmdWhere)
        Catch exp As Exception
        End Try

        txtSearchBy.Text = ""
        txtPage.Text = "1"
    End Sub
#End Region

#Region "Cancel"
    Private Sub B_imgCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        pnlList.Visible = True
        pnlBranch.Visible = False
        Me.CmdWhere = "ProductID = '" & Me.ProductID & "'"
        BindGridEntity(Me.CmdWhere)
        txtSearchBy.Text = ""
        txtPage.Text = "1"
    End Sub
#End Region

    Protected Sub dtgPaging_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles dtgPaging.SelectedIndexChanged

    End Sub
End Class