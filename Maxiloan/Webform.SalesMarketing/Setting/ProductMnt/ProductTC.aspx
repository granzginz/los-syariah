﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ProductTC.aspx.vb" Inherits="Maxiloan.Webform.SalesMarketing.ProductTC" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ProductTC</title>
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../../Include/Buttons.css" type="text/css" />
    <script language="JavaScript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;
        function OpenWinProductView(pProductID, pStyle) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.SalesMarketing/Setting/ProductMnt/ProductView.aspx?ProductID=' + pProductID + '&style=' + pStyle, 'Product', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=1');            
        }

        function fBack() {
            history.back(-1);
            return false;
        }
        function fClose() {
            window.close();
            return false;
        }
        function DeleteConfirm() {
            if (confirm("Apakah yakin mau hapus data ini ?")) {
                return true;
            }
            else {
                return false;
            }
        }	
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible ="false" ></asp:Label>             
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">               
            <h3>
                DAFTAR PERSYARATAN KREDIT
            </h3>
        </div>
    </div> 
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgPaging" runat="server" Width="100%" CssClass="grid_general"	 AllowSorting="True"
                        AutoGenerateColumns="False" OnSortCommand="Sorting" BorderStyle="None" BorderWidth="0" DataKeyField="MasterTCID">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="EDIT">
                                <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbEdit" runat="server" CausesValidation="False" ImageUrl="../../../Images/iconedit.gif"
                                        CommandName="EDIT"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DELETE">
                                <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbDelete" runat="server" CausesValidation="False" ImageUrl="../../../Images/icondelete.gif"
                                        CommandName="DELETE"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="PERSYARATAN KREDIT" SortExpression="TCName">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblTerm_n_ConditionList" runat="server" Text='<%#container.dataitem("TCName")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="SEBELUM" SortExpression="PriorTo">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblPriorTo" runat="server" Text='<%#container.dataitem("PriorTo")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="MANDATORY" SortExpression="IsMandatory">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblIsMandatory" runat="server" Text='<%#container.dataitem("IsMandatory")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="MasterTCID" Visible="False">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblMasterTCID" runat="server" Text='<%#container.dataitem("MasterTCID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>                       
                <div class="button_gridnavigation">
	                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../../Images/grid_navbutton01.png"
		                CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../../Images/grid_navbutton02.png"
		                CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../../Images/grid_navbutton03.png"
		                CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../../Images/grid_navbutton04.png"
		                CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
	                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
		                EnableViewState="False"></asp:Button>
	                <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage"
		                MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer"
		                CssClass="validator_general"></asp:RangeValidator>
	                <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
		                ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>
                <div class="label_gridnavigation">
	                <asp:Label ID="lblPage" runat="server"></asp:Label>of
	                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
	                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                </div>
            </div>
        </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonAdd" runat="server"  Text="Add" CssClass ="small button blue"
                CausesValidation="False"></asp:Button>                            
            <asp:Button ID="ButtonPrint" runat="server"  Text="Print" CssClass ="small button blue"
                Enabled="true" STyle="display:none;"></asp:Button>                            
            <asp:Button ID="ButtonBack" runat="server" CausesValidation="False"  Text="Back" CssClass ="small button gray">
            </asp:Button>
        </div>                       
    </asp:Panel>
    <asp:Panel ID="PnlHeader" runat="server">
        <div class="form_box_header">
            <div class="form_single">              
                <h4>
                    PRODUK PERSYARATAN KREDIT
                </h4>
            </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>ID Produk</label>
                <asp:HyperLink ID="hplProductID" runat="server" Text='<%#request("id")%>'>
                </asp:HyperLink>
	        </div>
        </div> 
        <div class="form_box">
	        <div class="form_single">
                <label>Nama Produk</label>
                <asp:Label ID="lblDescription" runat="server"></asp:Label>
	        </div>  
        </div> 
        <div class="form_box">
	        <div class="form_single">
                <label>Sebelum</label>
                <asp:DropDownList ID="cboPriorTo" runat="server">
                    <asp:ListItem Value="ALL">ALL</asp:ListItem>
                    <asp:ListItem Value="APK">APK Baru</asp:ListItem>
                    <asp:ListItem Value="REQ">Approval</asp:ListItem>
                    <asp:ListItem Value="PO">Purchase Order</asp:ListItem>
                    <asp:ListItem Value="DO">Pra Pencairan</asp:ListItem>
                    <asp:ListItem Value="AKR">Aktivasi Pembiayaan</asp:ListItem>
                </asp:DropDownList>
	        </div>
        </div> 
        <div class="form_button">
            <asp:Button ID="ButtonSearch" runat="server"  Text="Search" CssClass ="small button blue"
                CausesValidation="False"></asp:Button>
            <asp:Button ID="ButtonReset" runat="server"  Text="Reset" CssClass ="small button gray"
                CausesValidation="False"></asp:Button>
        </div>                         
    </asp:Panel>
    <asp:Panel ID="pnlAddEdit" runat="server">
        <div class="form_box_header">
            <div class="form_single">              
                <h4>
                    PRODUK PERSYARATAN KREDIT -
                    <asp:Label ID="lblAddEdit" runat="server"></asp:Label>
                </h4>
            </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label class ="label_req">
				Syarat dan Kondisi </label>
                <asp:DropDownList ID="cboTerm_n_Condition" runat="server">
                </asp:DropDownList>
                <asp:Label ID="lblTerm_n_Condition" runat="server"></asp:Label>
                <asp:Label ID="lblSign" runat="server"></asp:Label>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" ControlToValidate="cboTerm_n_Condition" CssClass="validator_general"
                    ErrorMessage="Harap pilih syarat dan Kondisi" Display="Dynamic" InitialValue="SelectOne"></asp:RequiredFieldValidator>
	        </div>
        </div> 
        <div class="form_box">
	        <div class="form_single">
                <label>Sebelum</label>
                <asp:DropDownList ID="cboPriorToAddEdit" runat="server">
                    <asp:ListItem Value="APK">APK Baru</asp:ListItem>
                    <asp:ListItem Value="REQ">Approval</asp:ListItem>
                    <asp:ListItem Value="PO">Purchase Order</asp:ListItem>
                    <asp:ListItem Value="DO">Pra Pencairan</asp:ListItem>
                    <asp:ListItem Value="AKR">Aktivasi Pembiayaan</asp:ListItem>
                </asp:DropDownList>
	        </div>
        </div> 
        <div class="form_box">
	        <div class="form_single">
                <label class="label_general">Mandatory</label>
                <asp:RadioButtonList ID="rboMandatory" runat="server"  class="opt_single"  Height="16px"
                    RepeatDirection="Horizontal">
                    <asp:ListItem Value="1" Selected="True">Yes</asp:ListItem>
                    <asp:ListItem Value="0">No</asp:ListItem>
                </asp:RadioButtonList>
	        </div>
        </div> 
        <div class="form_button">
            <asp:Button ID="ButtonSave" runat="server"  Text="Save" CssClass ="small button blue"
                CausesValidation="True"></asp:Button>
            <asp:Button ID="ButtonCancel" runat="server"  Text="Cancel" CssClass ="small button gray"
                CausesValidation="False"></asp:Button>
        </div>                                 
    </asp:Panel>
    </form>
</body>
</html>
