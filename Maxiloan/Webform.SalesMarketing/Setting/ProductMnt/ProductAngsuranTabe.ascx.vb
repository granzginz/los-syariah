﻿Public Class ProductAngsuranTabe
    Inherits System.Web.UI.UserControl
    Public Property UCMode As String
        Get
            Return CType(ViewState("ANGSURANTABUCMODE"), String)
        End Get
        Set(value As String)
            ViewState("ANGSURANTABUCMODE") = value
        End Set
    End Property
    Public Property Product() As Parameter.Product
        Get
            Return CType(ViewState("ProductAngsuranTabeProduct"), Parameter.Product)
        End Get
        Set(ByVal Value As Parameter.Product)
            ViewState("ProductAngsuranTabeProduct") = Value
        End Set
    End Property
    Public Property PRIORITASPEMBAYARAN As DataTable
        Get
            Return CType(ViewState("PRIORITASPEMBAYARAN"), DataTable)
        End Get
        Set(value As DataTable)
            ViewState("PRIORITASPEMBAYARAN") = value
        End Set
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
     
    End Sub
    Public Sub InitUCEditMode()

        bindBehaviour(cboInsToleranceAmount)
        bindBehaviour(cboLCP_Installment)
        bindBehaviour(cboPenaltyBasedOn)
        bindBehaviour(cboBilingCharges)
        bindBehaviour(cboPPR)
        bindBehaviour(cboPrepaymentPenaltyFixedBehaviour)
        InitialControls()

        If (UCMode = "ADD") Then
            initFieldsForAddMode()
        End If
        If (UCMode = "EDIT") Then
            bindFiedsForEdit()
        End If
    End Sub
    Sub bindBehaviour(ddl As DropDownList)
        ddl.DataValueField = "Value"
        ddl.DataTextField = "Text"
        ddl.DataSource = Parameter.Product.BehaviourItems
        ddl.DataBind()
    End Sub
    Sub InitialControls()

        lblInsToleranceAmount.Visible = (UCMode = "VIEW")
        txtInsToleranceAmount.Visible = (UCMode <> "VIEW")
        cboInsToleranceAmount.Visible = (UCMode <> "VIEW")

        lblLCP_Installment.Visible = (UCMode = "VIEW")
        txtLCP_Installment.Visible = (UCMode <> "VIEW")
        cboLCP_Installment.Visible = (UCMode <> "VIEW")

        lblPenaltyBasedOn.Visible = (UCMode = "VIEW")
        rboPenaltyBasedOn.Visible = (UCMode <> "VIEW")
        cboPenaltyBasedOn.Visible = (UCMode <> "VIEW")


        lblPenaltyRateEffectiveDate.Visible = (UCMode = "VIEW")
        txtPenaltyRateEffectiveDate.Visible = (UCMode <> "VIEW")

        lblPenaltyRatePrevious.Visible = (UCMode = "VIEW")
        txtPenaltyRatePrevious.Visible = (UCMode <> "VIEW")

        lblBilingCharges.Visible = (UCMode = "VIEW")
        txtBilingCharges.Visible = (UCMode <> "VIEW")
        cboBilingCharges.Visible = (UCMode <> "VIEW")

        lblPPR.Visible = (UCMode = "VIEW")
        txtPPR.Visible = (UCMode <> "VIEW")
        cboPPR.Visible = (UCMode <> "VIEW")

        lblPrepaymentPenaltyEffectiveDate.Visible = (UCMode = "VIEW")
        txtPrepaymentPenaltyEffectiveDate.Visible = (UCMode <> "VIEW")

        lblPrepaymentPenaltyPrevious.Visible = (UCMode = "VIEW")
        txtPrepaymentPenaltyPrevious.Visible = (UCMode <> "VIEW")

        lblPrepaymentPenaltyFixed.Visible = (UCMode = "VIEW")
        txtPrepaymentPenaltyFixed.Visible = (UCMode <> "VIEW")
        cboPrepaymentPenaltyFixedBehaviour.Visible = (UCMode <> "VIEW")

        lblPrepaymentPenaltyFixedEffectiveDate.Visible = (UCMode = "VIEW")
        txtPrepaymentPenaltyFixedEffectiveDate.Visible = (UCMode <> "VIEW")

        lblPrepaymentPenaltyFixedPrevious.Visible = (UCMode = "VIEW")
        txtPrepaymentPenaltyFixedPrevious.Visible = (UCMode <> "VIEW")
        Label1.Visible = (UCMode <> "VIEW")
        Label2.Visible = (UCMode <> "VIEW")
        Label3.Visible = (UCMode <> "VIEW")
        Label4.Visible = (UCMode <> "VIEW")
    End Sub
    Sub initFieldsForAddMode()
        txtLCP_Installment.Text = "0"
        cboLCP_Installment.SelectedIndex = 0

        txtPrepaymentPenaltyFixed.Text = "0"
        cboPrepaymentPenaltyFixedBehaviour.SelectedIndex = 0
        rboPenaltyBasedOn.SelectedIndex = rboPenaltyBasedOn.Items.IndexOf(rboPenaltyBasedOn.Items.FindByValue("PB"))
        cboPenaltyBasedOn.SelectedIndex = cboPenaltyBasedOn.Items.IndexOf(cboPenaltyBasedOn.Items.FindByValue("D"))
        txtPenaltyRatePrevious.Text = "0"
        txtPenaltyRateEffectiveDate.Text = ""
        txtPrepaymentPenaltyPrevious.Text = "0"
        txtPrepaymentPenaltyEffectiveDate.Text = ""
        txtPrepaymentPenaltyFixedPrevious.Text = "0"
        txtPrepaymentPenaltyFixedEffectiveDate.Text = ""
         
        txtInsToleranceAmount.Text = "0"
        cboInsToleranceAmount.SelectedIndex = 0

        txtPPR.Text = "0"
        cboPPR.SelectedIndex = 0

        txtBilingCharges.Text = "0"
        cboBilingCharges.SelectedIndex = cboBilingCharges.Items.IndexOf(cboBilingCharges.Items.FindByValue("D"))

    End Sub
    Public Sub bindFiedsForEdit()


        txtInsToleranceAmount.Text = FormatNumber(Product.InstallmentToleranceAmount, 0)
        cboInsToleranceAmount.SelectedIndex = cboInsToleranceAmount.Items.IndexOf(cboInsToleranceAmount.Items.FindByValue(Product.InstallmentToleranceAmountBehaviour))

        txtLCP_Installment.Text = CStr(FormatNumber(Product.PenaltyPercentage, 6))
        cboLCP_Installment.SelectedIndex = cboLCP_Installment.Items.IndexOf(cboLCP_Installment.Items.FindByValue(Product.PenaltyPercentageBehaviour))


        rboPenaltyBasedOn.SelectedIndex = rboPenaltyBasedOn.Items.IndexOf(rboPenaltyBasedOn.Items.FindByValue(Product.PenaltyBasedOn))
        cboPenaltyBasedOn.SelectedIndex = cboPenaltyBasedOn.Items.IndexOf(cboPenaltyBasedOn.Items.FindByValue(Product.PenaltyBasedOnBehaviour))

        cboPrioritasPembayaran.SelectedIndex = cboPrioritasPembayaran.Items.IndexOf(cboPrioritasPembayaran.Items.FindByValue(Product.PrioritasPembayaran))

        txtPenaltyRateEffectiveDate.Text = Product.PenaltyRateEffectiveDate.ToString("dd/MM/yyyy")

        txtPenaltyRatePrevious.Text = CStr(FormatNumber(Product.PenaltyRatePrevious, 10))

        txtBilingCharges.Text = CStr(Product.BillingCharges)
        cboBilingCharges.SelectedIndex = cboBilingCharges.Items.IndexOf(cboBilingCharges.Items.FindByValue(Product.BillingChargesBehaviour))

        txtPPR.Text = CStr(FormatNumber(Product.PrepaymentPenaltyRate, 6))
        cboPPR.SelectedIndex = cboPPR.Items.IndexOf(cboPPR.Items.FindByValue(Product.PrepaymentPenaltyRateBehaviour))
        txtPrepaymentPenaltyEffectiveDate.Text = Product.PrepaymentPenaltyEffectiveDate.ToString("dd/MM/yyyy")
        txtPrepaymentPenaltyPrevious.Text = CStr(FormatNumber(Product.PrepaymentPenaltyPrevious, 10))

        txtPrepaymentPenaltyFixed.Text = CStr(FormatNumber(Product.PrepaymentPenaltyFixed, 2))
        cboPrepaymentPenaltyFixedBehaviour.SelectedIndex = cboPrepaymentPenaltyFixedBehaviour.Items.IndexOf(cboPrepaymentPenaltyFixedBehaviour.Items.FindByValue(Product.PrepaymentPenaltyFixedBehaviour))

        txtPrepaymentPenaltyFixedEffectiveDate.Text = Product.PrepaymentPenaltyFixedEffectiveDate.ToString("dd/MM/yyyy")

        txtPrepaymentPenaltyFixedPrevious.Text = CStr(FormatNumber(Product.PrepaymentPenaltyFixedPrevious, 2))

    End Sub
    Public Sub InitUCViewMode()

        InitialControls()

        'lblPrioritasPembayaran.Text = IIf(Product.PrioritasPembayaran = "DE", "Denda", "Bebas")
        'LblAlokasiPembayaran.Text = IIf(Product.PrioritasPembayaran = "PR", "Prioritas", "Manual")
        'lblPriotasPembayaran.Text = IIf(Product.PrioritasPembayaran = 
        'lblPengaturanPembayaran.Text = IIf(Product.PengaturanBiaya = "PR", "Prioritas", "Manual")
        lblInsToleranceAmount.Text = FormatNumber(CStr(Product.InstallmentToleranceAmount).Trim, 2) & " " & Parameter.Product.Behaviour_Value(Product.InstallmentToleranceAmountBehaviour)

        lblLCP_Installment.Text = String.Format("{0} ‰ / day {1}", CStr(FormatNumber(Product.PenaltyPercentage, 2)), Parameter.Product.Behaviour_Value(Product.PenaltyPercentageBehaviour))
        lblPenaltyBasedOn.Text = String.Format("{0} {1}", IIf(Product.PenaltyBasedOn_HO = "PB", "Pokok + Bunga", "Pokok"), Parameter.Product.Behaviour_Value(Product.PenaltyBasedOnBehaviour_HO))

        lblPenaltyRateEffectiveDate.Text = Product.PenaltyRateEffectiveDate.ToString("dd/MM/yyyy")
        lblPenaltyRatePrevious.Text = Product.PenaltyRatePrevious.ToString

        lblBilingCharges.Text = FormatNumber(CStr(Product.BillingCharges).Trim, 2) & "  " & Parameter.Product.Behaviour_Value(Product.BillingChargesBehaviour)
        lblPPR.Text = CStr(FormatNumber(Product.PrepaymentPenaltyRate, 6)) & " % " & Parameter.Product.Behaviour_Value(Product.PrepaymentPenaltyRateBehaviour)
        lblPrepaymentPenaltyEffectiveDate.Text = Product.PrepaymentPenaltyEffectiveDate.ToString("dd/MM/yyyy")
        lblPrepaymentPenaltyPrevious.Text = Product.PrepaymentPenaltyPrevious.ToString
        lblPrepaymentPenaltyFixed.Text = String.Format("{0} {1}", Product.PrepaymentPenaltyFixed.ToString, Parameter.Product.Behaviour_Value(Product.PrepaymentPenaltyFixedBehaviour))
        lblPrepaymentPenaltyFixedEffectiveDate.Text = Product.PrepaymentPenaltyFixedEffectiveDate.ToString("dd/MM/yyyy")
        lblPrepaymentPenaltyFixedPrevious.Text = Product.PrepaymentPenaltyFixedPrevious.ToString
    End Sub
    Public Sub CollectResult(oProduct As Parameter.Product)
        With oProduct

            .PrepaymentPenaltyFixed = CDec(txtPrepaymentPenaltyFixed.Text.Trim)
            .PrepaymentPenaltyFixedBehaviour = cboPrepaymentPenaltyFixedBehaviour.SelectedItem.Value.Trim
            .PenaltyBasedOn = rboPenaltyBasedOn.SelectedItem.Value.Trim
            .PengaturanBiaya = rboPengaturanPembayaran.SelectedItem.Value.Trim
            .PrioritasPembayaran = cboPrioritasPembayaran.SelectedValue.Trim
            .PenaltyBasedOnBehaviour = cboPenaltyBasedOn.SelectedItem.Value.Trim

            .PenaltyRatePrevious = CDec(txtPenaltyRatePrevious.Text.Trim)
            .PenaltyRateEffectiveDate = Date.ParseExact(txtPenaltyRateEffectiveDate.Text.ToString, "dd/MM/yyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo)
            .PrepaymentPenaltyPrevious = CDec(txtPrepaymentPenaltyPrevious.Text.Trim)
            .PrepaymentPenaltyEffectiveDate = Date.ParseExact(txtPrepaymentPenaltyEffectiveDate.Text.ToString, "dd/MM/yyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo)
            .PrepaymentPenaltyFixedPrevious = CDec(txtPrepaymentPenaltyFixedPrevious.Text.Trim)
            .PrepaymentPenaltyFixedEffectiveDate = Date.ParseExact(txtPrepaymentPenaltyFixedEffectiveDate.Text.ToString, "dd/MM/yyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo)
            .PengaturanBiaya = rboPengaturanPembayaran.SelectedValue.Trim
            .PenaltyPercentage = CDec(txtLCP_Installment.Text.Trim)
            .PenaltyPercentageBehaviour = cboLCP_Installment.SelectedItem.Value.Trim


            .InstallmentToleranceAmount = CDec(txtInsToleranceAmount.Text.Trim)
            .InstallmentToleranceAmountBehaviour = cboInsToleranceAmount.SelectedItem.Value.Trim
            .PrepaymentPenaltyRate = CDec(txtPPR.Text.Trim)
            .PrepaymentPenaltyRateBehaviour = cboPPR.SelectedItem.Value.Trim
            .BillingCharges = CDec(txtBilingCharges.Text.Trim)
            .BillingChargesBehaviour = cboBilingCharges.SelectedItem.Value.Trim


            .InsurancePenaltyPercentage = 0
            .InsurancePenaltyPercentageBehaviour = "D"
             

            .RejectMinimumIncome = 0
            .RejectMinimumIncomeBehaviour = "D"
            .WarningMinimumIncome = 0
            .WarningMinimumIncomeBehaviour = "D"
            
        End With
    End Sub
    Public Sub BehaviourLockSetting()
        BehaviorHelperModule.Behaviour_Lock(Product.InstallmentToleranceAmountBehaviour, txtInsToleranceAmount, cboInsToleranceAmount) 
        BehaviorHelperModule.Behaviour_Lock(Product.PenaltyPercentageBehaviour, txtLCP_Installment, cboLCP_Installment) 
        BehaviorHelperModule.Behaviour_Lock(Product.PenaltyBasedOnBehaviour, rboPenaltyBasedOn, cboPenaltyBasedOn) 
        BehaviorHelperModule.Behaviour_Lock(Product.BillingChargesBehaviour, txtBilingCharges, cboBilingCharges) 
        BehaviorHelperModule.Behaviour_Lock(Product.PrepaymentPenaltyRateBehaviour, txtPPR, cboPPR) 
        BehaviorHelperModule.Behaviour_Lock(Product.PrepaymentPenaltyFixedBehaviour, txtPrepaymentPenaltyFixed, cboPrepaymentPenaltyFixedBehaviour) 
    End Sub

    Public Sub changed(sender As Object, e As EventArgs) Handles rboPengaturanPembayaran.SelectedIndexChanged

        cboPrioritasPembayaran.Visible = (rboPengaturanPembayaran.SelectedIndex = 0)
        TR_Prioritas.Visible = (rboPengaturanPembayaran.SelectedIndex = 0)
    End Sub
End Class