﻿#Region "Imports"
Imports Maxiloan.cbse
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
#End Region

Public Class ProductTC
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private m_controller As New ProductController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region

#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(viewstate("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property ProductID() As String
        Get
            Return CType(viewstate("ProductID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ProductID") = Value
        End Set
    End Property

    Private Property Description() As String
        Get
            Return CType(viewstate("Description"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Description") = Value
        End Set
    End Property

    Private Property IsActive() As Boolean
        Get
            Return CType(viewstate("IsActive"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            viewstate("IsActive") = Value
        End Set
    End Property

    Private Property AddEdit() As String
        Get
            Return CType(viewstate("vwsAddEdt"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("vwsAddEdt") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(viewstate("Sort"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Sort") = Value
        End Set
    End Property
    Private Property MasterTCId() As String
        Get
            Return CType(viewstate("MasterTCId"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("MasterTCId") = Value
        End Set
    End Property
#End Region

#Region "FillCbo_Term_n_Condition"
    Sub FillCbo_Term_n_Condition()
        Dim oProduct As New Parameter.Product
        Dim dtEntity As DataTable

        oProduct.ProductId = Me.ProductID
        oProduct.strConnection = GetConnectionString()
        oProduct = m_controller.Get_Combo_Term_n_Condition(oProduct)
        dtEntity = oProduct.ListData
        cboTerm_n_Condition.DataSource = dtEntity
        cboTerm_n_Condition.DataValueField = "mastertcid"
        cboTerm_n_Condition.DataTextField = "tcname"
        cboTerm_n_Condition.DataBind()
        cboTerm_n_Condition.Items.Insert(0, "select one")
        cboTerm_n_Condition.Items(0).Value = "SelectOne"
    End Sub
#End Region

#Region "Page_Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        If sessioninvalid() Then
            Exit Sub
        End If
        InitialDefaultPanel()

        Me.FormID = "ProductTC"
        If Not Me.IsPostBack Then
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If sessioninvalid() Then
                    Exit Sub
                End If
                Me.SearchBy = ""
                Me.SortBy = ""
            End If
            txtPage.Text = "1"
            Me.Sort = "a.MasterTCID ASC"
            Me.ProductID = Request("ProductID")
            hplProductID.NavigateUrl = LinkTo(Me.ProductID, "Marketing")

            hplProductID.Text = Me.ProductID
            Me.Description = Request("Description")
            Me.IsActive = CBool(Request("IsActive"))
            Me.CmdWhere = "ProductID='" & Me.ProductID & "'"
            BindGridEntity(Me.CmdWhere)
        End If
    End Sub
#End Region

#Region "LinkTo"
    Function LinkTo(ByVal strProductID As String, ByVal strStyle As String) As String
        Return "javascript:OpenWinProductView('" & strProductID & "','" & strStyle & "')"
    End Function
#End Region

#Region "InitialDefaultPanel"
    Private Sub InitialDefaultPanel()
        pnlList.Visible = True
        pnlAddEdit.Visible = False        
        PnlHeader.Visible = True
    End Sub
#End Region

#Region "BindGridEntity"
    Sub BindGridEntity(ByVal cmdWhere As String)
        Dim dtEntity As DataTable
        Dim oProduct As New Parameter.Product

        With oProduct
            .WhereCond = Me.CmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = Me.Sort
            .strConnection = GetConnectionString
        End With
        oProduct = m_controller.ProductTCPaging(oProduct)

        If Not oProduct Is Nothing Then
            dtEntity = oProduct.ListData
            recordCount = oProduct.TotalRecords
        Else
            recordCount = 0
        End If
        If recordCount = 0 Then
            ButtonPrint.Enabled = False
        Else
            ButtonPrint.Enabled = True
        End If
        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0

        dtgPaging.DataBind()

        hplProductID.Text = Me.ProductID
        lblDescription.Text = Me.Description

        PagingFooter()
    End Sub
#End Region

#Region "dtgPaging_ItemDataBound"
    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        Dim imbDelete As ImageButton
        Dim imbEdit As ImageButton

        If e.Item.ItemIndex >= 0 Then
            'imbDelete = CType(e.Item.FindControl("imbDelete"), ImageButton)
            'imbDelete.Attributes.Add("Onclick", "return DeleteConfirm()")

            'imbEdit = CType(e.Item.FindControl("imbEdit"), ImageButton)

            'If Me.IsActive = False Then
            'imbAdd.Visible = False
            'imbPrint.Visible = False
            'imbEdit.Visible = False
            'imbDelete.Visible = False
            'Else
            'imbAdd.Visible = True
            'imbPrint.Visible = True
            'imbEdit.Visible = True
            'imbDelete.Visible = True
            'End If

        End If
    End Sub
#End Region

#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then            
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int32)
                BindGridEntity(Me.CmdWhere)
            End If
        End If
    End Sub
#End Region

#Region "dtgEntity_ItemCommand"
    Private Sub dtgEntity_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        Dim oProduct As New Parameter.Product
        If e.CommandName = "EDIT" Then
            If CheckFeature(Me.Loginid, Me.FormID, "EDIT", Me.AppId) Then
                Me.AddEdit = "EDIT"
                pnlAddEdit.Visible = True
                pnlList.Visible = False
                PnlHeader.Visible = False
                Dim lblMasterTCID As String

                lblAddEdit.Text = Me.AddEdit
                cboTerm_n_Condition.Visible = False
                lblSign.Visible = False
                lblTerm_n_Condition.Visible = True
                'lblTerm_n_Condition.Text = dtgPaging.DataKeys.Item(e.Item.ItemIndex).ToString
                'With oProduct
                '    .ProductId = Me.ProductID
                '    .MasterTCID = dtgPaging.DataKeys.Item(e.Item.ItemIndex).ToString
                '    .strConnection = GetConnectionString
                'End With
                oProduct.ProductId = Me.ProductID
                'oProduct.MasterTCID = dtgPaging.DataKeys.Item(e.Item.ItemIndex).ToString
                'Added by Teddy
                lblMasterTCID = CType(e.Item.FindControl("lblMasterTCID"), Label).Text
                Me.MasterTCId = lblMasterTCID.Trim
                oProduct.MasterTCID = lblMasterTCID.Trim
                oProduct.strConnection = GetConnectionString
                oProduct = m_controller.ProductTCEdit(oProduct)

                cboTerm_n_Condition.Visible = False
                lblTerm_n_Condition.Text = CType(e.Item.FindControl("lblTerm_n_ConditionList"), Label).Text
                lblTerm_n_Condition.Visible = True

                cboPriorToAddEdit.SelectedIndex = cboPriorToAddEdit.Items.IndexOf(cboPriorToAddEdit.Items.FindByValue(oProduct.PriorTo))
                If oProduct.IsMandatory = True Then
                    rboMandatory.SelectedIndex = rboMandatory.Items.IndexOf(rboMandatory.Items.FindByValue("1"))
                Else
                    rboMandatory.SelectedIndex = rboMandatory.Items.IndexOf(rboMandatory.Items.FindByValue("0"))
                End If
            End If
        ElseIf e.CommandName = "DELETE" Then
            If CheckFeature(Me.Loginid, Me.FormID, "DEL", Me.AppId) Then
                Dim customClass As New Parameter.Product
                Dim BranchID As String = Me.sesBranchId
                BranchID = Replace(BranchID, "'", "")

                With customClass
                    .ProductId = Me.ProductID
                    .MasterTCID = dtgPaging.DataKeys.Item(e.Item.ItemIndex).ToString
                    .BranchId = BranchID
                    .strConnection = GetConnectionString
                End With

                Dim ResultOutput As String
                ResultOutput = m_controller.ProductTCDelete(customClass)
                If ResultOutput = "OK" Then                    
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, False)
                Else                    
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_FAILED, True)
                End If

                Me.CmdWhere = "ProductID='" & Me.ProductID & "'"
                BindGridEntity(Me.CmdWhere)
            End If
        End If
    End Sub
#End Region

#Region "imbSave"
    Private Sub imbSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
        If Page.IsValid Then
            Dim customClass As New Parameter.Product
            Dim ErrMessage As String = ""

            Dim BranchID As String = Me.sesBranchId
            BranchID = Replace(BranchID, "'", "")

            If Me.AddEdit = "ADD" Then
                With customClass
                    .ProductId = Me.ProductID
                    .MasterTCID = cboTerm_n_Condition.SelectedItem.Value
                    .PriorTo = cboPriorToAddEdit.SelectedItem.Value
                    .IsMandatory = CBool(rboMandatory.SelectedItem.Value)
                    .BranchId = BranchID
                    .strConnection = GetConnectionString()
                End With

                ErrMessage = m_controller.ProductTCSaveAdd(customClass)
                If ErrMessage <> "" Then
                    ShowMessage(lblMessage, ErrMessage, True)
                    pnlAddEdit.Visible = True
                    pnlList.Visible = False
                    PnlHeader.Visible = False
                    Exit Sub
                Else
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_SUCCESS, False)                
                    Me.CmdWhere = "ProductID='" & Me.ProductID & "'"
                    BindGridEntity(Me.CmdWhere)
                End If
            ElseIf Me.AddEdit = "EDIT" Then
                With customClass
                    .ProductId = Me.ProductID
                    '.MasterTCID = cboTerm_n_Condition.SelectedItem.Value
                    .MasterTCID = Me.MasterTCId
                    .PriorTo = cboPriorToAddEdit.SelectedItem.Value
                    .IsMandatory = CBool(rboMandatory.SelectedItem.Value)
                    .BranchId = BranchID
                    .strConnection = GetConnectionString()
                End With

                m_controller.ProductTCSaveEdit(customClass)
                ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)                

                Me.CmdWhere = "ProductID='" & Me.ProductID & "'"
                BindGridEntity(Me.CmdWhere)
            End If
            txtPage.Text = "1"
        End If
    End Sub
#End Region

#Region "imbAdd"
    Private Sub imbAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonAdd.Click
        If CheckFeature(Me.Loginid, Me.FormID, "ADD", Me.AppId) Then
            pnlList.Visible = False
            PnlHeader.Visible = False
            pnlAddEdit.Visible = True
            lblSign.Visible = True

            Me.AddEdit = "ADD"
            lblAddEdit.Text = Me.AddEdit

            cboTerm_n_Condition.Visible = True
            lblTerm_n_Condition.Visible = False
            FillCbo_Term_n_Condition()
            cboPriorToAddEdit.SelectedIndex = 0
            rboMandatory.SelectedIndex = rboMandatory.Items.IndexOf(rboMandatory.Items.FindByValue("1"))
        End If
    End Sub
#End Region

#Region "Sorting"
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub
#End Region

#Region "imbReset"
    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        cboPriorTo.SelectedIndex = 0
        Me.CmdWhere = "ProductID='" & Me.ProductID & "'"
        BindGridEntity(Me.CmdWhere)
        txtPage.Text = "1"
    End Sub
#End Region

#Region "imbSearch"
    Private Sub imbSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonSearch.Click
        If cboPriorTo.SelectedIndex = 0 Then
            Me.CmdWhere = "ProductID='" & Me.ProductID & "'"
        Else
            Me.CmdWhere = "ProductID= '" & Me.ProductID & "' and PriorTo = '" & cboPriorTo.SelectedItem.Value.Trim & "'"
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub
#End Region

#Region "imbPrint"
    Private Sub imbPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonPrint.Click
        If CheckFeature(Me.Loginid, Me.FormID, "PRINT", Me.AppId) Then
            SendCookies()
            Response.Redirect("Report/ProductTCRpt.aspx")
        End If
    End Sub
#End Region

#Region "SendCookies"
    Sub SendCookies()
        If cboPriorTo.SelectedIndex = 0 Then
            Me.CmdWhere = "ProductID= '" & Me.ProductID & "'"
        Else
            Me.CmdWhere = "ProductID= '" & Me.ProductID & "' and PriorTo = '" & cboPriorTo.SelectedItem.Value.Trim & "'"
        End If


        Dim cookie As HttpCookie = Request.Cookies("ProductTC")
        If Not cookie Is Nothing Then
            cookie.Values("PageFrom") = "ProductTC"
            cookie.Values("CmdWhere") = Me.CmdWhere
            cookie.Values("ProductID") = Me.ProductID
            cookie.Values("Description") = Me.Description
            cookie.Values("PriorTo") = cboPriorTo.SelectedItem.Value.Trim
            cookie.Values("IsActive") = Me.IsActive.ToString
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie("ProductTC")
            cookieNew.Values.Add("PageFrom", "ProductTC")
            cookieNew.Values.Add("CmdWhere", Me.CmdWhere)
            cookieNew.Values.Add("ProductID", Me.ProductID)
            cookieNew.Values.Add("Description", Me.Description)
            cookieNew.Values.Add("PriorTo", cboPriorTo.SelectedItem.Value.Trim)
            cookieNew.Values.Add("IsActive", Me.IsActive.ToString)
            Response.AppendCookie(cookieNew)
        End If
    End Sub
#End Region

#Region "imbBack"
    Private Sub imbBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonBack.Click
        Response.Redirect("Product.aspx")
    End Sub
#End Region

#Region "imgCancel"
    Private Sub imgCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        pnlList.Visible = True
        PnlHeader.Visible = True
        pnlAddEdit.Visible = False
        txtPage.Text = "1"
        Me.CmdWhere = "ProductID='" & Me.ProductID & "'"
        BindGridEntity(Me.CmdWhere)
    End Sub
#End Region

End Class