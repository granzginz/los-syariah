﻿Public Class ProductCollectionTab
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub
    Public Property Product() As Parameter.Product
        Get
            Return CType(ViewState("ProductCollectionTabProduct"), Parameter.Product)
        End Get
        Set(ByVal Value As Parameter.Product)
            ViewState("ProductCollectionTabProduct") = Value
        End Set
    End Property
    Public Property UCMode As String
        Get
            Return CType(ViewState("COLLECTIONTABUCMODE"), String)
        End Get
        Set(value As String)
            ViewState("COLLECTIONTABUCMODE") = value
        End Set
    End Property
    Sub bindBehaviour(ddl As DropDownList)
        ddl.DataValueField = "Value"
        ddl.DataTextField = "Text"
        ddl.DataSource = Parameter.Product.BehaviourItems
        ddl.DataBind()
    End Sub
    Public Sub InitUCEditMode()


        bindBehaviour(cboLMDProcessed)
        bindBehaviour(cboLMDTaken)
        bindBehaviour(cboGPLC)
        bindBehaviour(cboDaystoremindInstallment)
        bindBehaviour(cboDaysOverduetocall)
        bindBehaviour(cboPreviousOverduetoremind)
        bindBehaviour(cboPDCRequesttocall)
        bindBehaviour(cboDaysToRemind)
        bindBehaviour(cboDaysToRemind2)
        bindBehaviour(cboDaysToRemind3)
        bindBehaviour(cboDaysToGenerateDCR)
        bindBehaviour(cboDaysBeforeDuetoRN)
        bindBehaviour(cboDaysToGenerateRAL)
        bindBehaviour(cboRALPeriod)
        bindBehaviour(cboMaxDaysRNPaid)
        bindBehaviour(cboMaxPTPYDays)


        bindBehaviour(cboPTPYBank)
        bindBehaviour(cboPTPYCompany)
        bindBehaviour(cboPTPYSupplier)
        ''meisan
        bindBehaviour(cboPerpanjanganSKT)
        bindBehaviour(cboRALExtension)
        bindBehaviour(cboInventoryExpected)

        InitialControls()
        If (UCMode = "ADD") Then
            initFieldsForAddMode()
        End If
        If (UCMode = "EDIT") Then
            bindFiedsForEdit()
        End If
    End Sub

    Sub InitialControls()

        lblIsSPAutomatic.Visible = (UCMode = "VIEW")
        cboIsSPAutomatic.Visible = (UCMode <> "VIEW")
        txtLengthSPProcess.Visible = (UCMode <> "VIEW")
        cboIsSPAutomaticBehaviour.Visible = (UCMode <> "VIEW")

        lblIsSP1Automatic.Visible = (UCMode = "VIEW")
        cboIsSP1Automatic.Visible = (UCMode <> "VIEW")
        txtLengthSP1Process.Visible = (UCMode <> "VIEW")
        cboIsSP1AutomaticBehaviour.Visible = (UCMode <> "VIEW")


        lblIsSP2Automatic.Visible = (UCMode = "VIEW")
        cboIsSP2Automatic.Visible = (UCMode <> "VIEW")
        txtLengthSP2Process.Visible = (UCMode <> "VIEW")
        cboIsSP2AutomaticBehaviour.Visible = (UCMode <> "VIEW")

        lblLMDProcessed.Visible = (UCMode = "VIEW")
        txtLMDProcessed.Visible = (UCMode <> "VIEW")
        cboLMDProcessed.Visible = (UCMode <> "VIEW")

        lblLMDTaken.Visible = (UCMode = "VIEW")
        txtLMDTaken.Visible = (UCMode <> "VIEW")
        cboLMDTaken.Visible = (UCMode <> "VIEW")

        lblGPLC.Visible = (UCMode = "VIEW")
        txtGPLC.Visible = (UCMode <> "VIEW")
        cboGPLC.Visible = (UCMode <> "VIEW")

        lblDaystoremindInstallment.Visible = (UCMode = "VIEW")
        txtDaystoremindInstallment.Visible = (UCMode <> "VIEW")
        cboDaystoremindInstallment.Visible = (UCMode <> "VIEW")

        lblDaysOverduetocall.Visible = (UCMode = "VIEW")
        txtDaysOverduetocall.Visible = (UCMode <> "VIEW")
        cboDaysOverduetocall.Visible = (UCMode <> "VIEW")

        lblPreviousOverduetoremind.Visible = (UCMode = "VIEW")
        txtPreviousOverduetoremind.Visible = (UCMode <> "VIEW")
        cboPreviousOverduetoremind.Visible = (UCMode <> "VIEW")
 
        lblPDCRequesttocall.Visible = (UCMode = "VIEW")
        txtPDCRequesttocall.Visible = (UCMode <> "VIEW")
        cboPDCRequesttocall.Visible = (UCMode <> "VIEW")

        ''ke 1
        lblDaysToRemind.Visible = (UCMode = "VIEW")
        txtDaysToRemind.Visible = (UCMode <> "VIEW")
        cboDaysToRemind.Visible = (UCMode <> "VIEW")

        ''ke 2

        lblDaysToRemind2.Visible = (UCMode = "VIEW")
        txtDaysToRemind2.Visible = (UCMode <> "VIEW")
        cboDaysToRemind2.Visible = (UCMode <> "VIEW")

        ''ke 3


        lblDaysToRemind3.Visible = (UCMode = "VIEW")
        txtDaysToRemind3.Visible = (UCMode <> "VIEW")
        cboDaysToRemind3.Visible = (UCMode <> "VIEW")
 
        lblDaysToGenerateDCR.Visible = (UCMode = "VIEW")
        txtDaysToGenerateDCR.Visible = (UCMode <> "VIEW")
        cboDaysToGenerateDCR.Visible = (UCMode <> "VIEW")


        lblDaysBeforeDuetoRN.Visible = (UCMode = "VIEW")
        txtDaysBeforeDuetoRN.Visible = (UCMode <> "VIEW")
        cboDaysBeforeDuetoRN.Visible = (UCMode <> "VIEW")
 
        lblDaysToGenerateRAL.Visible = (UCMode = "VIEW")
        txtDaysToGenerateRAL.Visible = (UCMode <> "VIEW")
        cboDaysToGenerateRAL.Visible = (UCMode <> "VIEW")


        lblRALPeriod.Visible = (UCMode = "VIEW")
        txtRALPeriod.Visible = (UCMode <> "VIEW")
        cboRALPeriod.Visible = (UCMode <> "VIEW")
 
        lblMaxDaysRNPaid.Visible = (UCMode = "VIEW")
        txtMaxDaysRNPaid.Visible = (UCMode <> "VIEW")
        cboMaxDaysRNPaid.Visible = (UCMode <> "VIEW")
 
        lblMaxPTPYDays.Visible = (UCMode = "VIEW")
        txtMaxPTPYDays.Visible = (UCMode <> "VIEW")
        cboMaxPTPYDays.Visible = (UCMode <> "VIEW")

        lblPTPYBank.Visible = (UCMode = "VIEW")
        txtPTPYBank.Visible = (UCMode <> "VIEW")
        cboPTPYBank.Visible = (UCMode <> "VIEW")


        lblPTPYCompany.Visible = (UCMode = "VIEW")
        txtPTPYCompany.Visible = (UCMode <> "VIEW")
        cboPTPYCompany.Visible = (UCMode <> "VIEW")

        lblPTPYSupplier.Visible = (UCMode = "VIEW")
        txtPTPYSupplier.Visible = (UCMode <> "VIEW")
        cboPTPYSupplier.Visible = (UCMode <> "VIEW")


        ''meisan
        lblPerpanjanganSKT.Visible = (UCMode = "VIEW")
        TxtPerpanjanganSKT.Visible = (UCMode <> "VIEW")
        cboPerpanjanganSKT.Visible = (UCMode <> "VIEW")

        lblRALExtension.Visible = (UCMode = "VIEW")
        txtRALExtension.Visible = (UCMode <> "VIEW")
        cboRALExtension.Visible = (UCMode <> "VIEW")
 
        lblInventoryExpected.Visible = (UCMode = "VIEW")
        txtInventoryExpected.Visible = (UCMode <> "VIEW")
        cboInventoryExpected.Visible = (UCMode <> "VIEW")

        Label1.Visible = (UCMode <> "VIEW")
        Label2.Visible = (UCMode <> "VIEW")
        Label3.Visible = (UCMode <> "VIEW")
        Label4.Visible = (UCMode <> "VIEW")
        Label5.Visible = (UCMode <> "VIEW")
        Label6.Visible = (UCMode <> "VIEW")
        Label7.Visible = (UCMode <> "VIEW")
        Label8.Visible = (UCMode <> "VIEW")
        Label9.Visible = (UCMode <> "VIEW")
        Label10.Visible = (UCMode <> "VIEW")
        Label11.Visible = (UCMode <> "VIEW")
        Label12.Visible = (UCMode <> "VIEW")
        Label13.Visible = (UCMode <> "VIEW")
        Label14.Visible = (UCMode <> "VIEW")
        Label15.Visible = (UCMode <> "VIEW")
        Label16.Visible = (UCMode <> "VIEW")
        Label17.Visible = (UCMode <> "VIEW")
        Label18.Visible = (UCMode <> "VIEW")
        Label19.Visible = (UCMode <> "VIEW")
        Label20.Visible = (UCMode <> "VIEW")
        Label21.Visible = (UCMode <> "VIEW")
        Label22.Visible = (UCMode <> "VIEW")
        Label23.Visible = (UCMode <> "VIEW")
        Label24.Visible = (UCMode <> "VIEW")
        Label25.Visible = (UCMode <> "VIEW")
    End Sub

    Sub initFieldsForAddMode()
         
        cboIsSPAutomatic.SelectedIndex = cboIsSPAutomatic.Items.IndexOf(cboIsSPAutomatic.Items.FindByValue("0"))
        cboIsSPAutomaticBehaviour.SelectedIndex = cboIsSPAutomaticBehaviour.Items.IndexOf(cboIsSPAutomaticBehaviour.Items.FindByValue("D"))
        txtLengthSPProcess.Text = "0"
        cboIsSPAutomatic.Enabled = True
        cboIsSP1Automatic.SelectedIndex = cboIsSP1Automatic.Items.IndexOf(cboIsSP1Automatic.Items.FindByValue("0"))
        cboIsSP1AutomaticBehaviour.SelectedIndex = cboIsSP1AutomaticBehaviour.Items.IndexOf(cboIsSP1AutomaticBehaviour.Items.FindByValue("D"))
        txtLengthSP1Process.Text = "0"
        cboIsSP1Automatic.Enabled = True
        cboIsSP2Automatic.SelectedIndex = cboIsSP2Automatic.Items.IndexOf(cboIsSP2Automatic.Items.FindByValue("0"))
        cboIsSP2AutomaticBehaviour.SelectedIndex = cboIsSP2AutomaticBehaviour.Items.IndexOf(cboIsSP2AutomaticBehaviour.Items.FindByValue("D"))
        txtLengthSP2Process.Text = "0"
        cboIsSP2Automatic.Enabled = True

        cboIsSPAutomaticBehaviour.Enabled = False
        txtLengthSPProcess.Enabled = False

        cboIsSP1AutomaticBehaviour.Enabled = False
        txtLengthSP1Process.Enabled = False

        cboIsSP2AutomaticBehaviour.Enabled = False
        txtLengthSP2Process.Enabled = False


        txtLMDProcessed.Text = "0"
        cboLMDProcessed.SelectedIndex = 0

        txtLMDTaken.Text = "0"
        cboLMDTaken.SelectedIndex = 0

        txtGPLC.Text = "0"
        cboGPLC.SelectedIndex = 0

        ''ke 1
        txtDaysToRemind.Text = "0"
        cboDaysToRemind.SelectedIndex = 0

        ''ke 2
        txtDaysToRemind2.Text = "0"
        cboDaysToRemind2.SelectedIndex = 0

        ''ke 3
        txtDaysToRemind3.Text = "0"
        cboDaysToRemind3.SelectedIndex = 0

        txtDaysOverduetocall.Text = "0"
        cboDaysOverduetocall.SelectedIndex = 0

        txtPreviousOverduetoremind.Text = "0"
        cboPreviousOverduetoremind.SelectedIndex = 0

        txtPDCRequesttocall.Text = "0"
        cboPDCRequesttocall.SelectedIndex = 0


        txtDaysToRemind.Text = "0"
        cboDaysToRemind.SelectedIndex = 0

        ''2
        'txtDaysToRemind2.Text = "0"
        'cboDaysToRemind2.SelectedIndex = 0


        ' ''3
        'txtDaysToRemind3.Text = "0"
        'cboDaysToRemind3.SelectedIndex = 0

        txtDaysToGenerateDCR.Text = "0"
        cboDaysToGenerateDCR.SelectedIndex = 0

        txtDaysToGenerateRAL.Text = "0"
        cboDaysToGenerateRAL.SelectedIndex = 0

        txtDaysBeforeDuetoRN.Text = "0"
        cboDaysBeforeDuetoRN.SelectedIndex = 0

        txtRALPeriod.Text = "0"
        cboRALPeriod.SelectedIndex = 0

        txtMaxDaysRNPaid.Text = "0"
        cboMaxDaysRNPaid.SelectedIndex = 0

        txtMaxPTPYDays.Text = "0"
        cboMaxPTPYDays.SelectedIndex = 0

        txtPTPYBank.Text = "0"
        cboPTPYBank.SelectedIndex = 0

        txtPTPYCompany.Text = "0"
        cboPTPYCompany.SelectedIndex = 0

        txtPTPYSupplier.Text = "0"
        cboPTPYSupplier.SelectedIndex = 0

        ''meisan
        TxtPerpanjanganSKT.Text = "1"
        cboPerpanjanganSKT.SelectedIndex = 0

        txtRALExtension.Text = "5"
        cboRALExtension.SelectedIndex = 0

        txtInventoryExpected.Text = "0"
        cboInventoryExpected.SelectedIndex = 0
    End Sub

    Public Sub bindFiedsForEdit()
        cboIsSPAutomaticBehaviour.Enabled = Product.IsSPAutomatic
        cboIsSPAutomatic.SelectedIndex = cboIsSPAutomatic.Items.IndexOf(cboIsSPAutomatic.Items.FindByValue(IIf(Product.IsSPAutomatic = True, "1", "0")))
        cboIsSPAutomaticBehaviour.SelectedIndex = cboIsSPAutomaticBehaviour.Items.IndexOf(cboIsSPAutomaticBehaviour.Items.FindByValue(Product.IsSPAutomaticBehaviour))
        txtLengthSPProcess.Text = CStr(Product.LengthSPProcess)


        cboIsSP1AutomaticBehaviour.Enabled = Product.IsSP1Automatic

        cboIsSP1Automatic.SelectedIndex = cboIsSP1Automatic.Items.IndexOf(cboIsSP1Automatic.Items.FindByValue(IIf(Product.IsSP1Automatic = True, "1", "0")))
        cboIsSP1AutomaticBehaviour.SelectedIndex = cboIsSP1AutomaticBehaviour.Items.IndexOf(cboIsSP1AutomaticBehaviour.Items.FindByValue(Product.IsSP1AutomaticBehaviour))
        txtLengthSP1Process.Text = CStr(Product.LengthSP1Process)

        cboIsSP2AutomaticBehaviour.Enabled = Product.IsSP2Automatic
        cboIsSP2Automatic.SelectedIndex = cboIsSP2Automatic.Items.IndexOf(cboIsSP2Automatic.Items.FindByValue(IIf(Product.IsSP2Automatic = True, "1", "0")))
        cboIsSP2AutomaticBehaviour.SelectedIndex = cboIsSP2AutomaticBehaviour.Items.IndexOf(cboIsSP2AutomaticBehaviour.Items.FindByValue(Product.IsSP2AutomaticBehaviour))
        txtLengthSP2Process.Text = CStr(Product.LengthSP2Process)


        cboIsSPAutomatic.Enabled = Product.IsSPAutomatic
        txtLengthSPProcess.Enabled = Product.IsSPAutomatic

        cboIsSP1Automatic.Enabled = Product.IsSP1Automatic
        txtLengthSP1Process.Enabled = Product.IsSP1Automatic

        cboIsSP2Automatic.Enabled = Product.IsSP2Automatic
        txtLengthSP2Process.Enabled = Product.IsSP2Automatic


        txtLMDProcessed.Text = CStr(Product.LengthMainDocProcess)
        cboLMDProcessed.SelectedIndex = cboLMDProcessed.Items.IndexOf(cboLMDProcessed.Items.FindByValue(Product.LengthMainDocProcessBehaviour))

        txtLMDTaken.Text = CStr(Product.LengthMainDocTaken)
        cboLMDTaken.SelectedIndex = cboLMDTaken.Items.IndexOf(cboLMDTaken.Items.FindByValue(Product.LengthMainDocTakenBehaviour))

        txtGPLC.Text = CStr(Product.GracePeriodLateCharges)
        cboGPLC.SelectedIndex = cboGPLC.Items.IndexOf(cboGPLC.Items.FindByValue(Product.GracePeriodLateChargesBehaviour))

        ''1
        txtDaysToRemind.Text = CStr(Product.DeskCollPhoneRemind)
        cboDaysToRemind.SelectedIndex = cboDaysToRemind.Items.IndexOf(cboDaysToRemind.Items.FindByValue(Product.DeskCollPhoneRemindBehaviour))

        ''2
        'txtDaysToRemind2.Text = CStr(Product.DeskCollPhoneRemind2)
        'cboDaysToRemind2.SelectedIndex = cboDaysToRemind2.Items.IndexOf(cboDaysToRemind2.Items.FindByValue(Product.DeskCollPhoneRemindBehaviour2))

        ' ''3
        'txtDaysToRemind3.Text = CStr(Product.DeskCollPhoneRemind3)
        'cboDaysToRemind3.SelectedIndex = cboDaysToRemind3.Items.IndexOf(cboDaysToRemind3.Items.FindByValue(Product.DeskCollPhoneRemindBehaviour3))

        txtDaysOverduetocall.Text = CStr(Product.DeskCollOD)
        cboDaysOverduetocall.SelectedIndex = cboDaysOverduetocall.Items.IndexOf(cboDaysOverduetocall.Items.FindByValue(Product.DeskCollODBehaviour))

        txtPreviousOverduetoremind.Text = CStr(Product.PrevODToRemind)
        cboPreviousOverduetoremind.SelectedIndex = cboPreviousOverduetoremind.Items.IndexOf(cboPreviousOverduetoremind.Items.FindByValue(Product.PrevODToRemindBehaviour))

        txtPDCRequesttocall.Text = CStr(Product.PDCDayToRemind)
        cboPDCRequesttocall.SelectedIndex = cboPDCRequesttocall.Items.IndexOf(cboPDCRequesttocall.Items.FindByValue(Product.PDCDayToRemindBehaviour))

        txtDaysToRemind.Text = CStr(Product.DeskCollSMSRemind)
        cboDaysToRemind.SelectedIndex = cboDaysToRemind.Items.IndexOf(cboDaysToRemind.Items.FindByValue(Product.DeskCollSMSRemindBehaviour))

        ''ke 2

        txtDaysToRemind2.Text = CStr(Product.DeskCollSMSRemind2)
        cboDaysToRemind2.SelectedIndex = cboDaysToRemind2.Items.IndexOf(cboDaysToRemind2.Items.FindByValue(Product.DeskCollSMSRemindBehaviour2))

        ''ke 3
        txtDaysToRemind3.Text = CStr(Product.DeskCollSMSRemind3)
        cboDaysToRemind3.SelectedIndex = cboDaysToRemind3.Items.IndexOf(cboDaysToRemind3.Items.FindByValue(Product.DeskCollSMSRemindBehaviour3))


        txtDaysToGenerateDCR.Text = CStr(Product.DCR)
        cboDaysToGenerateDCR.SelectedIndex = cboDaysToGenerateDCR.Items.IndexOf(cboDaysToGenerateDCR.Items.FindByValue(Product.DCRBehaviour))

        txtDaysToGenerateRAL.Text = CStr(Product.ODToRAL)
        cboDaysToGenerateRAL.SelectedIndex = cboDaysToGenerateRAL.Items.IndexOf(cboDaysToGenerateRAL.Items.FindByValue(Product.ODToRALBehaviour))

        txtDaysBeforeDuetoRN.Text = CStr(Product.DaysBeforeDueToRN)
        cboDaysBeforeDuetoRN.SelectedIndex = cboDaysBeforeDuetoRN.Items.IndexOf(cboDaysBeforeDuetoRN.Items.FindByValue(Product.DaysBeforeDueToRNBehaviour))

        txtRALPeriod.Text = CStr(Product.RALPeriod)
        cboRALPeriod.SelectedIndex = cboRALPeriod.Items.IndexOf(cboRALPeriod.Items.FindByValue(Product.RALPeriodBehaviour))

        txtMaxDaysRNPaid.Text = CStr(Product.MaxDaysRNPaid)
        cboMaxDaysRNPaid.SelectedIndex = cboMaxDaysRNPaid.Items.IndexOf(cboMaxDaysRNPaid.Items.FindByValue(Product.MaxDaysRNPaidBehaviour))

        txtMaxPTPYDays.Text = CStr(Product.MaxPTPYDays)
        cboMaxPTPYDays.SelectedIndex = cboMaxPTPYDays.Items.IndexOf(cboMaxPTPYDays.Items.FindByValue(Product.MaxPTPYDaysBehaviour))

        txtPTPYBank.Text = CStr(Product.PTPYBank)
        cboPTPYBank.SelectedIndex = cboPTPYBank.Items.IndexOf(cboPTPYBank.Items.FindByValue(Product.PTPYBankBehaviour))

        txtPTPYCompany.Text = CStr(Product.PTPYCompany)
        cboPTPYCompany.SelectedIndex = cboPTPYCompany.Items.IndexOf(cboPTPYCompany.Items.FindByValue(Product.PTPYCompanyBehaviour))

        txtPTPYSupplier.Text = CStr(Product.PTPYSupplier)
        cboPTPYSupplier.SelectedIndex = cboPTPYSupplier.Items.IndexOf(cboPTPYSupplier.Items.FindByValue(Product.PTPYSupplierBehaviour))

        ''meisan
        TxtPerpanjanganSKT.Text = CStr(Product.PerpanjanganSKT)
        cboPerpanjanganSKT.SelectedIndex = cboPerpanjanganSKT.Items.IndexOf(cboPerpanjanganSKT.Items.FindByValue(Product.PerpanjanganSKTBehaviour))

        txtRALExtension.Text = CStr(Product.RALExtension)
        cboRALExtension.SelectedIndex = cboRALExtension.Items.IndexOf(cboRALExtension.Items.FindByValue(Product.RALExtensionBehaviour))

        txtInventoryExpected.Text = CStr(Product.InventoryExpected)
        cboInventoryExpected.SelectedIndex = cboInventoryExpected.Items.IndexOf(cboInventoryExpected.Items.FindByValue(Product.InventoryExpectedBehaviour))


    End Sub
    Public Sub InitUCViewMode()

        InitialControls()
        Dim oProduct = Product

        lblIsSPAutomatic.Text = String.Format("{0} {1} Length SP Process {2} days", IIf(oProduct.IsSPAutomatic = True, "1", "0"), Parameter.Product.Behaviour_Value(Product.IsSPAutomaticBehaviour), CStr(oProduct.LengthSPProcess))
        lblIsSP1Automatic.Text = String.Format("{0} {1} Length SP 1 Process {2} days", IIf(oProduct.IsSP1Automatic = True, "1", "0"), Parameter.Product.Behaviour_Value(Product.IsSP1AutomaticBehaviour), CStr(oProduct.LengthSP1Process))
        lblIsSP2Automatic.Text = String.Format("{0} {1} Length SP 2 Process {2} days", IIf(oProduct.IsSP2Automatic = True, "1", "0"), Parameter.Product.Behaviour_Value(Product.IsSP2AutomaticBehaviour), CStr(oProduct.LengthSP2Process))

        lblLMDProcessed.Text = String.Format("{0} days {1}", CStr(oProduct.LengthMainDocProcess), Parameter.Product.Behaviour_Value(oProduct.LengthMainDocProcessBehaviour))
        lblLMDTaken.Text = String.Format("{0} days {1}", CStr(oProduct.LengthMainDocTaken), Parameter.Product.Behaviour_Value(oProduct.LengthMainDocTakenBehaviour))
        lblGPLC.Text = String.Format("{0} days {1}", CStr(oProduct.GracePeriodLateCharges), Parameter.Product.Behaviour_Value(oProduct.GracePeriodLateChargesBehaviour))

        lblDaystoremindInstallment.Text = String.Format("{0} days {1}", CStr(oProduct.DeskCollPhoneRemind), Parameter.Product.Behaviour_Value(oProduct.DeskCollPhoneRemindBehaviour))
        lblDaysOverduetocall.Text = String.Format("{0} days {1}", CStr(oProduct.DeskCollOD), Parameter.Product.Behaviour_Value(oProduct.DeskCollODBehaviour))

        lblPreviousOverduetoremind.Text = String.Format("{0} days {1}", CStr(oProduct.PrevODToRemind), Parameter.Product.Behaviour_Value(oProduct.PrevODToRemindBehaviour))
        lblPDCRequesttocall.Text = String.Format("{0} days {1}", CStr(oProduct.PDCDayToRemind), Parameter.Product.Behaviour_Value(oProduct.PDCDayToRemindBehaviour))

        lblDaysToRemind.Text = String.Format("{0} days {1}", CStr(oProduct.DeskCollSMSRemind), Parameter.Product.Behaviour_Value(oProduct.DeskCollSMSRemindBehaviour))

        lblDaysToGenerateDCR.Text = String.Format("{0} days {1}", CStr(oProduct.DCR), Parameter.Product.Behaviour_Value(oProduct.DCRBehaviour))

        lblDaysBeforeDuetoRN.Text = String.Format("{0} days {1}", CStr(oProduct.DaysBeforeDueToRN), Parameter.Product.Behaviour_Value(oProduct.DaysBeforeDueToRNBehaviour))

        lblDaysToGenerateRAL.Text = String.Format("{0} days {1}", CStr(oProduct.ODToRAL), Parameter.Product.Behaviour_Value(oProduct.ODToRALBehaviour))

        lblRALPeriod.Text = String.Format("{0} days {1}", CStr(oProduct.RALPeriod), Parameter.Product.Behaviour_Value(oProduct.RALPeriodBehaviour))

        lblMaxDaysRNPaid.Text = String.Format("{0} days {1}", CStr(oProduct.MaxDaysRNPaid), Parameter.Product.Behaviour_Value(oProduct.MaxDaysRNPaidBehaviour))

        lblMaxPTPYDays.Text = String.Format("{0} days {1}", CStr(oProduct.MaxPTPYDays), Parameter.Product.Behaviour_Value(oProduct.MaxPTPYDaysBehaviour))

        lblPTPYBank.Text = String.Format("{0} days {1}", CStr(oProduct.PTPYBank), Parameter.Product.Behaviour_Value(oProduct.PTPYBankBehaviour))

        lblPTPYCompany.Text = String.Format("{0} days {1}", CStr(oProduct.PTPYCompany), Parameter.Product.Behaviour_Value(oProduct.PTPYCompanyBehaviour))

        lblPerpanjanganSKT.Text = String.Format("{0} days {1}", CStr(oProduct.PerpanjanganSKT), Parameter.Product.Behaviour_Value(oProduct.PTPYSupplierBehaviour))

        lblPTPYSupplier.Text = String.Format("{0} days {1}", CStr(oProduct.PTPYSupplier), Parameter.Product.Behaviour_Value(oProduct.PTPYSupplierBehaviour))
        lblRALExtension.Text = String.Format("{0} days {1}", CStr(oProduct.RALExtension), Parameter.Product.Behaviour_Value(oProduct.RALExtensionBehaviour))
        lblInventoryExpected.Text = String.Format("{0} days {1}", CStr(oProduct.InventoryExpected), Parameter.Product.Behaviour_Value(oProduct.InventoryExpectedBehaviour))

 
    End Sub

    Public Sub CollectResult(oProduct As Parameter.Product)
        With oProduct
            .IsSPAutomatic = CBool(cboIsSPAutomatic.SelectedItem.Value.Trim)
            .IsSPAutomaticBehaviour = cboIsSPAutomaticBehaviour.SelectedItem.Value.Trim
            .LengthSPProcess = CInt(txtLengthSPProcess.Text.Trim) 
            .IsSP1Automatic = CBool(cboIsSP1Automatic.SelectedItem.Value.Trim)
            .IsSP1AutomaticBehaviour = cboIsSP1AutomaticBehaviour.SelectedItem.Value.Trim
            .LengthSP1Process = CInt(txtLengthSP1Process.Text.Trim)
            .IsSP2Automatic = CBool(cboIsSP2Automatic.SelectedItem.Value.Trim)
            .IsSP2AutomaticBehaviour = cboIsSP2AutomaticBehaviour.SelectedItem.Value.Trim
            .LengthSP2Process = CInt(txtLengthSP2Process.Text.Trim)
            .LengthMainDocProcess = CInt(txtLMDProcessed.Text.Trim)
            .LengthMainDocProcessBehaviour = cboLMDProcessed.SelectedItem.Value.Trim
            .LengthMainDocTaken = CInt(txtLMDTaken.Text.Trim)
            .LengthMainDocTakenBehaviour = cboLMDTaken.SelectedItem.Value.Trim
            .GracePeriodLateCharges = CInt(txtGPLC.Text.Trim)
            .GracePeriodLateChargesBehaviour = cboGPLC.SelectedItem.Value.Trim 
            .DeskCollPhoneRemind = CInt(txtDaystoremindInstallment.Text.Trim)
            .DeskCollPhoneRemindBehaviour = cboDaystoremindInstallment.SelectedItem.Value.Trim
            .DeskCollPhoneRemindBehaviour2 = cboDaystoremindInstallment.SelectedItem.Value.Trim
            .DeskCollPhoneRemindBehaviour3 = cboDaystoremindInstallment.SelectedItem.Value.Trim
            .DeskCollOD = CInt(txtDaysOverduetocall.Text.Trim)
            .DeskCollODBehaviour = cboDaysOverduetocall.SelectedItem.Value.Trim
            .PrevODToRemind = CInt(txtPreviousOverduetoremind.Text.Trim)
            .PrevODToRemindBehaviour = cboPreviousOverduetoremind.SelectedItem.Value.Trim
            .PDCDayToRemind = CInt(txtPDCRequesttocall.Text.Trim)
            .PDCDayToRemindBehaviour = cboPDCRequesttocall.SelectedItem.Value.Trim
            .DeskCollSMSRemind = CInt(txtDaysToRemind.Text.Trim)
            .DeskCollSMSRemindBehaviour = cboDaysToRemind.SelectedItem.Value.Trim
            .DCR = CInt(txtDaysToGenerateDCR.Text.Trim)
            .DCRBehaviour = cboDaysToGenerateDCR.SelectedItem.Value.Trim
            .DaysBeforeDueToRN = CInt(txtDaysBeforeDuetoRN.Text.Trim)
            .DaysBeforeDueToRNBehaviour = cboDaysBeforeDuetoRN.SelectedItem.Value.Trim
            .ODToRAL = CInt(txtDaysToGenerateRAL.Text.Trim)
            .ODToRALBehaviour = cboDaysToGenerateRAL.SelectedItem.Value.Trim
            .RALPeriod = CInt(txtRALPeriod.Text.Trim)
            .RALPeriodBehaviour = cboRALPeriod.SelectedItem.Value.Trim
            .MaxDaysRNPaid = CInt(txtMaxDaysRNPaid.Text.Trim)
            .MaxDaysRNPaidBehaviour = cboMaxDaysRNPaid.SelectedItem.Value.Trim
            .MaxPTPYDays = CInt(txtMaxPTPYDays.Text.Trim)
            .MaxPTPYDaysBehaviour = cboMaxPTPYDays.SelectedItem.Value.Trim
            .PTPYBank = CInt(txtPTPYBank.Text.Trim)
            .PTPYBankBehaviour = cboPTPYBank.SelectedItem.Value.Trim
            .PTPYCompany = CInt(txtPTPYCompany.Text.Trim)
            .PTPYCompanyBehaviour = cboPTPYCompany.SelectedItem.Value.Trim
            .PTPYSupplier = CInt(txtPTPYSupplier.Text.Trim)
            .PTPYSupplierBehaviour = cboPTPYSupplier.SelectedItem.Value.Trim

            ''meisan
            .PerpanjanganSKT = CInt(TxtPerpanjanganSKT.Text.Trim)
            .PerpanjanganSKTBehaviour = cboPerpanjanganSKT.SelectedItem.Value.Trim


            .RALExtension = CInt(txtRALExtension.Text.Trim)
            .RALExtensionBehaviour = cboRALExtension.SelectedItem.Value.Trim
            .InventoryExpected = CInt(txtInventoryExpected.Text.Trim)
            .InventoryExpectedBehaviour = cboInventoryExpected.SelectedItem.Value.Trim
            .DeskCollSMSRemind2 = CInt(txtDaysToRemind2.Text.Trim)
            .DeskCollSMSRemindBehaviour2 = cboDaysToRemind2.SelectedItem.Value.Trim
            .DeskCollSMSRemind3 = CInt(txtDaysToRemind3.Text.Trim)
            .DeskCollSMSRemindBehaviour3 = cboDaysToRemind3.SelectedItem.Value.Trim

        End With
    End Sub
     
    Public Sub BehaviourLockSetting()  
        BehaviorHelperModule.Behaviour_Lock(Product.IsSPAutomaticBehaviour, txtLengthSPProcess, cboIsSPAutomaticBehaviour)
        BehaviorHelperModule.Behaviour_Lock(Product.IsSP1AutomaticBehaviour, txtLengthSP1Process, cboIsSP1AutomaticBehaviour)
        BehaviorHelperModule.Behaviour_Lock(Product.IsSP2AutomaticBehaviour, txtLengthSP2Process, cboIsSP2AutomaticBehaviour)
        BehaviorHelperModule.Behaviour_Lock(Product.LengthMainDocProcessBehaviour, txtLMDProcessed, cboLMDProcessed)
        BehaviorHelperModule.Behaviour_Lock(Product.LengthMainDocTakenBehaviour, txtLMDTaken, cboLMDTaken)
        BehaviorHelperModule.Behaviour_Lock(Product.GracePeriodLateChargesBehaviour, txtGPLC, cboGPLC)
        BehaviorHelperModule.Behaviour_Lock(Product.DeskCollPhoneRemindBehaviour, txtDaysToRemind, cboDaysToRemind)
        'BehaviorHelperModule.Behaviour_Lock(Product.DeskCollPhoneRemindBehaviour2, txtDaysToRemind2, cboDaysToRemind2)
        'BehaviorHelperModule.Behaviour_Lock(Product.DeskCollPhoneRemindBehaviour3, txtDaysToRemind3, cboDaysToRemind3)
        BehaviorHelperModule.Behaviour_Lock(Product.DeskCollODBehaviour, txtDaysOverduetocall, cboDaysOverduetocall)
        BehaviorHelperModule.Behaviour_Lock(Product.PrevODToRemindBehaviour, txtPreviousOverduetoremind, cboPreviousOverduetoremind)
        BehaviorHelperModule.Behaviour_Lock(Product.PDCDayToRemindBehaviour, txtPDCRequesttocall, cboPDCRequesttocall)
        BehaviorHelperModule.Behaviour_Lock(Product.DeskCollSMSRemindBehaviour, txtDaysToRemind, cboDaysToRemind)
        BehaviorHelperModule.Behaviour_Lock(Product.DeskCollSMSRemindBehaviour2, txtDaysToRemind2, cboDaysToRemind2)
        BehaviorHelperModule.Behaviour_Lock(Product.DeskCollSMSRemindBehaviour3, txtDaysToRemind3, cboDaysToRemind3)
        BehaviorHelperModule.Behaviour_Lock(Product.DCRBehaviour, txtDaysToGenerateDCR, cboDaysToGenerateDCR)
        BehaviorHelperModule.Behaviour_Lock(Product.ODToRALBehaviour, txtDaysToGenerateRAL, cboDaysToGenerateRAL)
        BehaviorHelperModule.Behaviour_Lock(Product.DaysBeforeDueToRNBehaviour, txtDaysBeforeDuetoRN, cboDaysBeforeDuetoRN)
        BehaviorHelperModule.Behaviour_Lock(Product.RALPeriodBehaviour, txtRALPeriod, cboRALPeriod)
        BehaviorHelperModule.Behaviour_Lock(Product.MaxDaysRNPaidBehaviour, txtMaxDaysRNPaid, cboMaxDaysRNPaid)
        BehaviorHelperModule.Behaviour_Lock(Product.MaxPTPYDaysBehaviour, txtMaxPTPYDays, cboMaxPTPYDays)
        BehaviorHelperModule.Behaviour_Lock(Product.PTPYBankBehaviour, txtPTPYBank, cboPTPYBank)
        BehaviorHelperModule.Behaviour_Lock(Product.PTPYCompanyBehaviour, txtPTPYCompany, cboPTPYCompany)
        BehaviorHelperModule.Behaviour_Lock(Product.PTPYSupplierBehaviour, txtPTPYSupplier, cboPTPYSupplier)
        ''meisan
        BehaviorHelperModule.Behaviour_Lock(Product.PerpanjanganSKT, TxtPerpanjanganSKT, cboPerpanjanganSKT)

        BehaviorHelperModule.Behaviour_Lock(Product.RALExtensionBehaviour, txtRALExtension, cboRALExtension)
        BehaviorHelperModule.Behaviour_Lock(Product.InventoryExpectedBehaviour, txtInventoryExpected, cboInventoryExpected)  
    End Sub
End Class