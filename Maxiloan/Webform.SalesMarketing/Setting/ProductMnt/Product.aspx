﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Product.aspx.vb" Inherits="Maxiloan.Webform.SalesMarketing.Product" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="productUmumTab.ascx" TagName="umum" TagPrefix="uc1" %>
<%@ Register Src="productSkemaTab.ascx" TagName="skema" TagPrefix="uc2" %>
<%@ Register Src="productBiayaTab.ascx" TagName="biaya" TagPrefix="uc3" %>
<%@ Register Src="productAngsuranTabe.ascx" TagName="angs" TagPrefix="uc4" %>
<%@ Register Src="productCollectionTab.ascx" TagName="coll" TagPrefix="uc5" %>
<%@ Register Src="productFinanceTab.ascx" TagName="fin" TagPrefix="uc6" %>
<%@ Register Src="../../../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Product</title>
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
    <script src="../../../js/jquery-2.1.1.min.js" type="text/javascript"></script>
    <script src="../../../js/product.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../../Include/Buttons.css" type="text/css" />
    <script language="JavaScript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;

        function fClose() {
            window.close();
            return false;
        }
        function DeleteConfirm() {
            if (confirm("Apakah yakin mau hapus data ini ?")) {
                return true;
            }
            else {
                return false;
            }
        }

        function cboIsSpAutomatic(cbo, cbob, txt) {
            var oComboval = $('#' + cbo + ' option:selected').val();
            var oComboBehaviour = $('#' + cbob);
            var otxt = $('#' + txt);
            if (oComboval == "0") {
                $('#' + cbob).prop("selectedIndex", 0);
                $('#' + cbob).attr("disabled", "disabled");
                otxt.val("0");
                otxt.attr("disabled", "disabled");
            }
            else {
                $('#' + cbob).removeAttr('disabled'); otxt.removeAttr('disabled');
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
     <div class="form_box_title"> <div class="form_single"> 
    <h4><asp:Label ID="lblHeader" runat="server"></asp:Label> </h4> </div> </div>
    <div class="form_title">
    
        <div class="form_single">
            <h3>
                DAFTAR PRODUK
            </h3>
        </div>
    </div>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_box_title">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgPaging" runat="server" Width="100%" CssClass="tablegrid" AllowSorting="True"
                        AutoGenerateColumns="False" OnSortCommand="Sorting" BorderStyle="None" BorderWidth="0"
                        DataKeyField="ProductID">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="EDIT">
                                <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" ImageUrl="../../../Images/iconedit.gif"
                                        CommandName="EDIT"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="ID PRODUK" SortExpression="ProductID" >
                                <ItemTemplate>  
                                    <%--<asp:HyperLink ID="hynProductID" runat="server" Text='<%#Container.DataItem("ProductID")%>'   NavigateUrl='<%# LinkTo(Container.DataItem("ProductID"), "Marketing") %>'></asp:HyperLink>--%>
                                    <asp:HyperLink ID="hynProductID" runat="server" Text='<%#Container.DataItem("ProductID")%>' />
                                    <%--<asp:HyperLink ID="hynProductID" runat="server" Text='<%#Container.DataItem("ProductID")%>'></asp:HyperLink>--%>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="ProductID" SortExpression="ProductID" HeaderText="ID Produk" Visible="false">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="Description" SortExpression="Description" HeaderText="NAMA PRODUK">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="IsActive" SortExpression="IsActive" HeaderText="AKTIF">
                            </asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="PERSYARATAN PEMBIAYAAN">
                                <ItemTemplate>
                                    <a href='ProductTC.aspx?ProductID=<%# DataBinder.Eval(Container, "DataItem.ProductID") %>&Description=<%# DataBinder.Eval(Container, "DataItem.Description") %>&IsActive=<%# DataBinder.Eval(Container, "DataItem.IsActive") %>'>
                                        <asp:Label ID="lblLinkProductTC" Text='PERSYARATAN PEMBIAYAAN' runat="server"></asp:Label>
                                    </a>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="PRODUK CABANG">
                                <ItemTemplate>
                                    <a href='ProductBranchHO.aspx?ProductID=<%# DataBinder.eval(Container,"DataItem.ProductID") %>&Description=<%# DataBinder.eval(Container,"DataItem.Description") %>&IsActive=<%# DataBinder.eval(Container,"DataItem.IsActive") %>'>
                                        <asp:Label ID="lblLinkProductBranchHO" Text='PRODUK CABANG' runat="server"></asp:Label>
                                    </a>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                      <uc2:ucGridNav id="GridNavigator" runat="server"/>   
                   <%-- <div class="button_gridnavigation">
                        <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../../Images/grid_navbutton01.png"
                            CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../../Images/grid_navbutton02.png"
                            CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../../Images/grid_navbutton03.png"
                            CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../../Images/grid_navbutton04.png"
                            CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                        <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                            EnableViewState="False"></asp:Button>
                        <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" MinimumValue="1"
                            ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                            ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                    <div class="label_gridnavigation">
                        <asp:Label ID="lblPage" runat="server"></asp:Label>of
                        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                        <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                    </div>--%>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonAdd" runat="server" Text="Add" CssClass="small button blue"
                CausesValidation="False"></asp:Button>
         
        </div>
        <div class="form_box_title">
            <div class="form_single">
                <h4>
                    CARI PRODUK
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cari Berdasarkan</label>
                <asp:DropDownList ID="cboSearch" runat="server">
                    <asp:ListItem Value="ProductID">ID Produk</asp:ListItem>
                    <asp:ListItem Value="Description">Nama Produk</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonSearch" runat="server" Text="Search" CssClass="small button blue"
                CausesValidation="False"></asp:Button>&nbsp;
            <asp:Button ID="ButtonReset" runat="server" Text="Reset" CssClass="small button gray"
                CausesValidation="False"></asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlAddEdit" runat="server" Visible="False">
        <div class="form_box">
            <div class="form_single">
                <label>
                    ID Produk&nbsp;</label>
                
                <asp:TextBox ID="txtProductID" runat="server" MaxLength="10"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rgvProductID" runat="server" ControlToValidate="txtProductID"
                    CssClass="validator_general" ErrorMessage="Harap isi dengan ID Produk"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label> 
                    Nama Produk&nbsp;</label>
              
                <asp:TextBox ID="txtDescription" runat="server" Width="480px" MaxLength="100"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtDescription"
                    CssClass="validator_general" ErrorMessage="Harap isi Nama Produk"></asp:RequiredFieldValidator>
            </div>
        </div>
           <div class="form_box" id="divCopyProduct" runat="server" visible="false">
            <div class="form_single">
                <label>
                    Copy Product&nbsp;</label>
                
               <asp:DropDownList ID="cboProduct" runat="server" AutoPostBack="True">
                </asp:DropDownList>
               
            </div>
        </div>
        <br />
        <div class="form_single" id="tabproductNew" runat="server" >
            <asp:TabContainer runat="server" ID="Tabs" style="height: auto;" ActiveTabIndex="0"
                Width="100%">
                <asp:TabPanel runat="server" ID="pnlTabUmum" HeaderText="UMUM">
                    <contenttemplate> 
                         <uc1:umum id="UmumTab" runat="server"/>  
                         </contenttemplate>
                </asp:TabPanel>
                <asp:TabPanel runat="server" ID="pnlTabSkema" HeaderText="SKEMA">
                    <contenttemplate>
                          <uc2:skema id="SkemaTab" runat="server"/>  

                        </contenttemplate>
                </asp:TabPanel>
                <asp:TabPanel runat="server" ID="pnlTabBiaya" HeaderText="BIAYA">
                    <contenttemplate>
                         <uc3:biaya id="BiayaTab" runat="server"/>  
                     
                        </contenttemplate>
                </asp:TabPanel>
                <asp:TabPanel runat="server" ID="pnlTabAngsuran" HeaderText="ANGSURAN">
                    <contenttemplate>
                          <uc4:angs  id="AngsuranTab" runat="server"/> 
                    
                        </contenttemplate>
                </asp:TabPanel>
                <asp:TabPanel runat="server" ID="pnlTabCollection" HeaderText="COLLECTION">
                    <contenttemplate>
                         <uc5:coll  id="CollactionTab" runat="server"/> 
                   

                        </contenttemplate>
                </asp:TabPanel>
                <asp:TabPanel runat="server" ID="pnlTabFinance" HeaderText="FINANCE">
                    <contenttemplate>
                         <uc6:fin id="FinaceTab" runat="server"/>  
                     
                        </contenttemplate>
                </asp:TabPanel>
            </asp:TabContainer>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonSave" runat="server" Text="Save" CssClass="small button blue"
                CausesValidation="true"></asp:Button>&nbsp;
            <asp:Button ID="ButtonCancel" runat="server" Text="Cancel" CssClass="small button gray"
                CausesValidation="false"></asp:Button>&nbsp;
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlBranch" runat="server">
        <div class="form_box_title">
            <div class="form_single">
                <h4>
                    PRODUK
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    ID Produk</label>
                <asp:HyperLink ID="hplB_ProductID" runat="server" Text='<%#request("ProductID")%>'>
                </asp:HyperLink>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Nama Produk</label>
                <asp:Label ID="B_lblDescription" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box_title">
            <div class="form_single">
                <h4>
                    PRODUK DIPAKAI DICABANG
                </h4>
            </div>
        </div>
        <div class="form_box_title">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgBranchList" runat="server" Width="100%" AllowSorting="True"
                        AutoGenerateColumns="False" CssClass="grid_general" BorderStyle="None" BorderWidth="0"
                        DataKeyField="BranchID" Visible="True">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="ASSIGN">
                                <ItemTemplate>
                                    <asp:CheckBox ID="chk_Branch" runat="server"></asp:CheckBox>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="CABANG" HeaderStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:Label ID="lblBranchFullName" Text='<%# DataBinder.Eval(Container, "DataItem.Name")%>'
                                        runat="server">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="BRANCHID" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblBranchID" Text='<%# DataBinder.Eval(Container, "DataItem.BranchID")%>'
                                        runat="server">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonOK" runat="server" Text="Ok" CssClass="small button blue" CausesValidation="False">
            </asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
<script language="JavaScript" type="text/javascript">
function enentChange(val) {
    var id = val;
    if (id == "SelectOne") {
    }
    else {
    }
    $("#<%= tabproductNew.ClientID %>").prop('disabled', false);
    alert(id);
        }
        </script>
</html>
