﻿#Region "Imports"
Imports Maxiloan.cbse
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class ProductOfferingBranch
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents txtPeriodFrom As ucDateCE
    Protected WithEvents txtPeriodTo As ucDateCE
    Protected WithEvents GridNavigator As ucGridNav

#Region "Constanta"
    Private m_controller As New ProductController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private oCustomclassInsRateCard As New Parameter.GeneralPaging
    Private oControllerInsRateCard As New GeneralPagingController
#End Region

#Region "Property"
    Private Property SupplierID() As String
        Get
            Return CType(viewstate("SupplierID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("SupplierID") = Value
        End Set
    End Property

    Private Property AssetCode() As String
        Get
            Return CType(viewstate("AssetCode"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AssetCode") = Value
        End Set
    End Property

    Private Property InstallmentAmount() As Decimal
        Get
            Return CType(viewstate("InstallmentAmount"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            viewstate("InstallmentAmount") = Value
        End Set
    End Property

    'Private Property InstallmentAmountBehaviour() As String
    '    Get
    '        Return CType(viewstate("InstallmentAmountBehaviour"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        viewstate("InstallmentAmountBehaviour") = Value
    '    End Set
    'End Property

    'Private Property ResidualValue() As Decimal
    '    Get
    '        Return CType(viewstate("ResidualValue"), Decimal)
    '    End Get
    '    Set(ByVal Value As Decimal)
    '        viewstate("ResidualValue") = Value
    '    End Set
    'End Property

    'Private Property ResidualValueBehaviour() As String
    '    Get
    '        Return CType(viewstate("ResidualValueBehaviour"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        viewstate("ResidualValueBehaviour") = Value
    '    End Set
    'End Property

    Private Property StartDate() As Date
        Get
            Return CType(viewstate("StartDate"), Date)
        End Get
        Set(ByVal Value As Date)
            viewstate("StartDate") = Value
        End Set
    End Property

    Private Property EndDate() As Date
        Get
            Return CType(viewstate("EndDate"), Date)
        End Get
        Set(ByVal Value As Date)
            viewstate("EndDate") = Value
        End Set
    End Property


    Private Property IsAuthorized() As Boolean
        Get
            Return CType(viewstate("IsAuthorized"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            viewstate("IsAuthorized") = Value
        End Set
    End Property


    Private Property CmdWhere() As String
        Get
            Return CType(viewstate("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("vwsCmdWhere") = Value
        End Set
    End Property

    Private Property Flag() As String
        Get
            Return CType(viewstate("Flag"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Flag") = Value
        End Set
    End Property

    Private Property ProductOfferingID() As String
        Get
            Return CType(viewstate("ProductOfferingID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ProductOfferingID") = Value
        End Set
    End Property

    Private Property ProductID() As String
        Get
            Return CType(viewstate("ProductID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ProductID") = Value
        End Set
    End Property

    Private Property Description() As String
        Get
            Return CType(viewstate("Description"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Description") = Value
        End Set
    End Property

    Private Property AddEdit() As String
        Get
            Return CType(viewstate("vwsAddEdt"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("vwsAddEdt") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(viewstate("Sort"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Sort") = Value
        End Set
    End Property

    Private Property AssetTypeID() As String
        Get
            Return CType(viewstate("AssetTypeID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AssetTypeID") = Value
        End Set
    End Property

    Private Property ScoreSchemeID() As String
        Get
            Return CType(viewstate("ScoreSchemeID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ScoreSchemeID") = Value
        End Set
    End Property

    Private Property CreditScoreSchemeID() As String
        Get
            Return CType(viewstate("CreditScoreSchemeID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CreditScoreSchemeID") = Value
        End Set
    End Property

    Private Property JournalSchemeID() As String
        Get
            Return CType(viewstate("JournalSchemeID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("JournalSchemeID") = Value
        End Set
    End Property

    Private Property ApprovalSchemeID() As String
        Get
            Return CType(viewstate("ApprovalSchemeID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ApprovalSchemeID") = Value
        End Set
    End Property

    Private Property AssetUsedNew() As String
        Get
            Return CType(viewstate("AssetUsedNew"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AssetUsedNew") = Value
        End Set
    End Property

    Private Property FinanceType() As String
        Get
            Return CType(viewstate("FinanceType"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("FinanceType") = Value
        End Set
    End Property

    Private Property KegiatanUsaha() As String
        Get
            Return CType(ViewState("KegiatanUsaha"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("KegiatanUsaha") = Value
        End Set
    End Property

    Private Property JenisPembiayaan() As String
        Get
            Return CType(ViewState("JenisPembiayaan"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("JenisPembiayaan") = Value
        End Set
    End Property


    'Private Property EffectiveRate() As Decimal
    '    Get
    '        Return CType(viewstate("EffectiveRate"), Decimal)
    '    End Get
    '    Set(ByVal Value As Decimal)
    '        viewstate("EffectiveRate") = Value
    '    End Set
    'End Property

    'Private Property EffectiveRateBehaviour() As String
    '    Get
    '        Return CType(viewstate("EffectiveRateBehaviour"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        viewstate("EffectiveRateBehaviour") = Value
    '    End Set
    'End Property

    'Private Property GrossYieldRate() As Decimal
    '    Get
    '        Return CType(viewstate("GrossYieldRate"), Decimal)
    '    End Get
    '    Set(ByVal Value As Decimal)
    '        viewstate("GrossYieldRate") = Value
    '    End Set
    'End Property

    'Private Property GrossYieldRateBehaviour() As String
    '    Get
    '        Return CType(viewstate("GrossYieldRateBehaviour"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        viewstate("GrossYieldRateBehaviour") = Value
    '    End Set
    'End Property

    'Private Property MinimumTenor() As Integer
    '    Get
    '        Return CType(viewstate("MinimumTenor"), Integer)
    '    End Get
    '    Set(ByVal Value As Integer)
    '        viewstate("MinimumTenor") = Value
    '    End Set
    'End Property

    'Private Property MaximumTenor() As Integer
    '    Get
    '        Return CType(viewstate("MaximumTenor"), Integer)
    '    End Get
    '    Set(ByVal Value As Integer)
    '        viewstate("MaximumTenor") = Value
    '    End Set
    'End Property

    'Private Property PenaltyPercentage() As Decimal
    '    Get
    '        Return CType(viewstate("PenaltyPercentage"), Decimal)
    '    End Get
    '    Set(ByVal Value As Decimal)
    '        viewstate("PenaltyPercentage") = Value
    '    End Set
    'End Property

    'Private Property PenaltyPercentageBehaviour() As String
    '    Get
    '        Return CType(viewstate("PenaltyPercentageBehaviour"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        viewstate("PenaltyPercentageBehaviour") = Value
    '    End Set
    'End Property

    'Private Property InsurancePenaltyPercentage() As Decimal
    '    Get
    '        Return CType(viewstate("InsurancePenaltyPercentage"), Decimal)
    '    End Get
    '    Set(ByVal Value As Decimal)
    '        viewstate("InsurancePenaltyPercentage") = Value
    '    End Set
    'End Property

    'Private Property InsurancePenaltyPercentageBehaviour() As String
    '    Get
    '        Return CType(viewstate("InsurancePenaltyPercentageBehaviour"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        viewstate("InsurancePenaltyPercentageBehaviour") = Value
    '    End Set
    'End Property

    'Private Property CancellationFee() As Decimal
    '    Get
    '        Return CType(viewstate("CancellationFee"), Decimal)
    '    End Get
    '    Set(ByVal Value As Decimal)
    '        viewstate("CancellationFee") = Value
    '    End Set
    'End Property

    'Private Property CancellationFeeBehaviour() As String
    '    Get
    '        Return CType(viewstate("CancellationFeeBehaviour"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        viewstate("CancellationFeeBehaviour") = Value
    '    End Set
    'End Property

    'Private Property AdminFee() As Decimal
    '    Get
    '        Return CType(viewstate("AdminFee"), Decimal)
    '    End Get
    '    Set(ByVal Value As Decimal)
    '        viewstate("AdminFee") = Value
    '    End Set
    'End Property

    'Private Property AdminFeeBehaviour() As String
    '    Get
    '        Return CType(viewstate("AdminFeeBehaviour"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        viewstate("AdminFeeBehaviour") = Value
    '    End Set
    'End Property

    'Private Property FiduciaFee() As Decimal
    '    Get
    '        Return CType(viewstate("FiduciaFee"), Decimal)
    '    End Get
    '    Set(ByVal Value As Decimal)
    '        viewstate("FiduciaFee") = Value
    '    End Set
    'End Property

    'Private Property FiduciaFeeBehaviour() As String
    '    Get
    '        Return CType(viewstate("FiduciaFeeBehaviour"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        viewstate("FiduciaFeeBehaviour") = Value
    '    End Set
    'End Property

    'Private Property ProvisionFee() As Decimal
    '    Get
    '        Return CType(viewstate("ProvisionFee"), Decimal)
    '    End Get
    '    Set(ByVal Value As Decimal)
    '        viewstate("ProvisionFee") = Value
    '    End Set
    'End Property

    'Private Property ProvisionFeeBehaviour() As String
    '    Get
    '        Return CType(viewstate("ProvisionFeeBehaviour"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        viewstate("ProvisionFeeBehaviour") = Value
    '    End Set
    'End Property

    'Private Property NotaryFee() As Decimal
    '    Get
    '        Return CType(viewstate("NotaryFee"), Decimal)
    '    End Get
    '    Set(ByVal Value As Decimal)
    '        viewstate("NotaryFee") = Value
    '    End Set
    'End Property

    'Private Property NotaryFeeBehaviour() As String
    '    Get
    '        Return CType(viewstate("NotaryFeeBehaviour"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        viewstate("NotaryFeeBehaviour") = Value
    '    End Set
    'End Property

    'Private Property SurveyFee() As Decimal
    '    Get
    '        Return CType(viewstate("SurveyFee"), Decimal)
    '    End Get
    '    Set(ByVal Value As Decimal)
    '        viewstate("SurveyFee") = Value
    '    End Set
    'End Property

    'Private Property SurveyFeeBehaviour() As String
    '    Get
    '        Return CType(viewstate("SurveyFeeBehaviour"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        viewstate("SurveyFeeBehaviour") = Value
    '    End Set
    'End Property

    'Private Property VisitFee() As Decimal
    '    Get
    '        Return CType(viewstate("VisitFee"), Decimal)
    '    End Get
    '    Set(ByVal Value As Decimal)
    '        viewstate("VisitFee") = Value
    '    End Set
    'End Property

    'Private Property VisitFeeBehaviour() As String
    '    Get
    '        Return CType(viewstate("VisitFeeBehaviour"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        viewstate("VisitFeeBehaviour") = Value
    '    End Set
    'End Property

    'Private Property ReschedulingFee() As Decimal
    '    Get
    '        Return CType(viewstate("ReschedulingFee"), Decimal)
    '    End Get
    '    Set(ByVal Value As Decimal)
    '        viewstate("ReschedulingFee") = Value
    '    End Set
    'End Property

    'Private Property ReschedulingFeeBehaviour() As String
    '    Get
    '        Return CType(viewstate("ReschedulingFeeBehaviour"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        viewstate("ReschedulingFeeBehaviour") = Value
    '    End Set
    'End Property

    'Private Property AgreementTransferFee() As Decimal
    '    Get
    '        Return CType(viewstate("AgreementTransferFee"), Decimal)
    '    End Get
    '    Set(ByVal Value As Decimal)
    '        viewstate("AgreementTransferFee") = Value
    '    End Set
    'End Property

    'Private Property AgreementTransferFeeBehaviour() As String
    '    Get
    '        Return CType(viewstate("AgreementTransferFeeBehaviour"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        viewstate("AgreementTransferFeeBehaviour") = Value
    '    End Set
    'End Property

    'Private Property ChangeDueDateFee() As Decimal
    '    Get
    '        Return CType(viewstate("ChangeDueDateFee"), Decimal)
    '    End Get
    '    Set(ByVal Value As Decimal)
    '        viewstate("ChangeDueDateFee") = Value
    '    End Set
    'End Property

    'Private Property ChangeDueDateFeeBehaviour() As String
    '    Get
    '        Return CType(viewstate("ChangeDueDateFeeBehaviour"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        viewstate("ChangeDueDateFeeBehaviour") = Value
    '    End Set
    'End Property

    'Private Property AssetReplacementFee() As Decimal
    '    Get
    '        Return CType(viewstate("AssetReplacementFee"), Decimal)
    '    End Get
    '    Set(ByVal Value As Decimal)
    '        viewstate("AssetReplacementFee") = Value
    '    End Set
    'End Property

    'Private Property AssetReplacementFeeBehaviour() As String
    '    Get
    '        Return CType(viewstate("AssetReplacementFeeBehaviour"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        viewstate("AssetReplacementFeeBehaviour") = Value
    '    End Set
    'End Property

    'Private Property RepossesFee() As Decimal
    '    Get
    '        Return CType(viewstate("RepossesFee"), Decimal)
    '    End Get
    '    Set(ByVal Value As Decimal)
    '        viewstate("RepossesFee") = Value
    '    End Set
    'End Property

    'Private Property RepossesFeeBehaviour() As String
    '    Get
    '        Return CType(viewstate("RepossesFeeBehaviour"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        viewstate("RepossesFeeBehaviour") = Value
    '    End Set
    'End Property

    'Private Property LegalisirDocFee() As Decimal
    '    Get
    '        Return CType(viewstate("LegalisirDocFee"), Decimal)
    '    End Get
    '    Set(ByVal Value As Decimal)
    '        viewstate("LegalisirDocFee") = Value
    '    End Set
    'End Property

    'Private Property LegalisirDocFeeBehaviour() As String
    '    Get
    '        Return CType(viewstate("LegalisirDocFeeBehaviour"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        viewstate("LegalisirDocFeeBehaviour") = Value
    '    End Set
    'End Property

    'Private Property PDCBounceFee() As Decimal
    '    Get
    '        Return CType(viewstate("PDCBounceFee"), Decimal)
    '    End Get
    '    Set(ByVal Value As Decimal)
    '        viewstate("PDCBounceFee") = Value
    '    End Set
    'End Property

    'Private Property PDCBounceFeeBehaviour() As String
    '    Get
    '        Return CType(viewstate("PDCBounceFeeBehaviour"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        viewstate("PDCBounceFeeBehaviour") = Value
    '    End Set
    'End Property

    'Private Property InsAdminFee() As Decimal
    '    Get
    '        Return CType(viewstate("InsAdminFee"), Decimal)
    '    End Get
    '    Set(ByVal Value As Decimal)
    '        viewstate("InsAdminFee") = Value
    '    End Set
    'End Property

    'Private Property InsAdminFeeBehaviour() As String
    '    Get
    '        Return CType(viewstate("InsAdminFeeBehaviour"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        viewstate("InsAdminFeeBehaviour") = Value
    '    End Set
    'End Property

    'Private Property InsStampDutyFee() As Decimal
    '    Get
    '        Return CType(viewstate("InsStampDutyFee"), Decimal)
    '    End Get
    '    Set(ByVal Value As Decimal)
    '        viewstate("InsStampDutyFee") = Value
    '    End Set
    'End Property

    'Private Property InsStampDutyFeeBehaviour() As String
    '    Get
    '        Return CType(viewstate("InsStampDutyFeeBehaviour"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        viewstate("InsStampDutyFeeBehaviour") = Value
    '    End Set
    'End Property

    'Private Property BiayaPolis() As Decimal
    '    Get
    '        Return CType(ViewState("BiayaPolis"), Decimal)
    '    End Get
    '    Set(ByVal Value As Decimal)
    '        ViewState("BiayaPolis") = Value
    '    End Set
    'End Property

    'Private Property BiayaPolisBehaviour() As String
    '    Get
    '        Return CType(ViewState("BiayaPolisBehaviour"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        ViewState("BiayaPolisBehaviour") = Value
    '    End Set
    'End Property

    'Private Property DaysPOExpiration() As Integer
    '    Get
    '        Return CType(viewstate("DaysPOExpiration"), Integer)
    '    End Get
    '    Set(ByVal Value As Integer)
    '        viewstate("DaysPOExpiration") = Value
    '    End Set
    'End Property

    'Private Property DaysPOExpirationBehaviour() As String
    '    Get
    '        Return CType(viewstate("DaysPOExpirationBehaviour"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        viewstate("DaysPOExpirationBehaviour") = Value
    '    End Set
    'End Property

    'Private Property InstallmentToleranceAmount() As Decimal
    '    Get
    '        Return CType(viewstate("InstallmentToleranceAmount"), Decimal)
    '    End Get
    '    Set(ByVal Value As Decimal)
    '        viewstate("InstallmentToleranceAmount") = Value
    '    End Set
    'End Property

    'Private Property InstallmentToleranceAmountBehaviour() As String
    '    Get
    '        Return CType(viewstate("InstallmentToleranceAmountBehaviour"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        viewstate("InstallmentToleranceAmountBehaviour") = Value
    '    End Set
    'End Property

    'Private Property DPPercentage() As Decimal
    '    Get
    '        Return CType(viewstate("DPPercentage"), Decimal)
    '    End Get
    '    Set(ByVal Value As Decimal)
    '        viewstate("DPPercentage") = Value
    '    End Set
    'End Property

    'Private Property DPPercentageBehaviour() As String
    '    Get
    '        Return CType(viewstate("DPPercentageBehaviour"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        viewstate("DPPercentageBehaviour") = Value
    '    End Set
    'End Property

    'Private Property RejectMinimumIncome() As Decimal
    '    Get
    '        Return CType(viewstate("RejectMinimumIncome"), Decimal)
    '    End Get
    '    Set(ByVal Value As Decimal)
    '        viewstate("RejectMinimumIncome") = Value
    '    End Set
    'End Property

    'Private Property RejectMinimumIncomeBehaviour() As String
    '    Get
    '        Return CType(viewstate("RejectMinimumIncomeBehaviour"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        viewstate("RejectMinimumIncomeBehaviour") = Value
    '    End Set
    'End Property

    'Private Property WarningMinimumIncome() As Decimal
    '    Get
    '        Return CType(viewstate("WarningMinimumIncome"), Decimal)
    '    End Get
    '    Set(ByVal Value As Decimal)
    '        viewstate("WarningMinimumIncome") = Value
    '    End Set
    'End Property

    'Private Property WarningMinimumIncomeBehaviour() As String
    '    Get
    '        Return CType(viewstate("WarningMinimumIncomeBehaviour"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        viewstate("WarningMinimumIncomeBehaviour") = Value
    '    End Set
    'End Property

    'Private Property IsSPAutomatic() As Boolean
    '    Get
    '        Return CType(viewstate("IsSPAutomatic"), Boolean)
    '    End Get
    '    Set(ByVal Value As Boolean)
    '        viewstate("IsSPAutomatic") = Value
    '    End Set
    'End Property

    'Private Property IsSPAutomaticBehaviour() As String
    '    Get
    '        Return CType(viewstate("IsSPAutomaticBehaviour"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        viewstate("IsSPAutomaticBehaviour") = Value
    '    End Set
    'End Property

    'Private Property LengthSPProcess() As Integer
    '    Get
    '        Return CType(viewstate("LengthSPProcess"), Integer)
    '    End Get
    '    Set(ByVal Value As Integer)
    '        viewstate("LengthSPProcess") = Value
    '    End Set
    'End Property

    'Private Property IsSP1Automatic() As Boolean
    '    Get
    '        Return CType(viewstate("IsSP1Automatic"), Boolean)
    '    End Get
    '    Set(ByVal Value As Boolean)
    '        viewstate("IsSP1Automatic") = Value
    '    End Set
    'End Property

    'Private Property IsSP1AutomaticBehaviour() As String
    '    Get
    '        Return CType(viewstate("IsSP1AutomaticBehaviour"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        viewstate("IsSP1AutomaticBehaviour") = Value
    '    End Set
    'End Property

    'Private Property LengthSP1Process() As Integer
    '    Get
    '        Return CType(viewstate("LengthSP1Process"), Integer)
    '    End Get
    '    Set(ByVal Value As Integer)
    '        viewstate("LengthSP1Process") = Value
    '    End Set
    'End Property

    'Private Property IsSP2Automatic() As Boolean
    '    Get
    '        Return CType(viewstate("IsSP2Automatic"), Boolean)
    '    End Get
    '    Set(ByVal Value As Boolean)
    '        viewstate("IsSP2Automatic") = Value
    '    End Set
    'End Property

    'Private Property IsSP2AutomaticBehaviour() As String
    '    Get
    '        Return CType(viewstate("IsSP2AutomaticBehaviour"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        viewstate("IsSP2AutomaticBehaviour") = Value
    '    End Set
    'End Property

    'Private Property LengthSP2Process() As Integer
    '    Get
    '        Return CType(viewstate("LengthSP2Process"), Integer)
    '    End Get
    '    Set(ByVal Value As Integer)
    '        viewstate("LengthSP2Process") = Value
    '    End Set
    'End Property

    'Private Property LengthMainDocProcess() As Integer
    '    Get
    '        Return CType(viewstate("LengthMainDocProcess"), Integer)
    '    End Get
    '    Set(ByVal Value As Integer)
    '        viewstate("LengthMainDocProcess") = Value
    '    End Set
    'End Property

    'Private Property LengthMainDocProcessBehaviour() As String
    '    Get
    '        Return CType(viewstate("LengthMainDocProcessBehaviour"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        viewstate("LengthMainDocProcessBehaviour") = Value
    '    End Set
    'End Property

    'Private Property LengthMainDocTaken() As Integer
    '    Get
    '        Return CType(viewstate("LengthMainDocTaken"), Integer)
    '    End Get
    '    Set(ByVal Value As Integer)
    '        viewstate("LengthMainDocTaken") = Value
    '    End Set
    'End Property

    'Private Property LengthMainDocTakenBehaviour() As String
    '    Get
    '        Return CType(viewstate("LengthMainDocTakenBehaviour"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        viewstate("LengthMainDocTakenBehaviour") = Value
    '    End Set
    'End Property

    'Private Property GracePeriodLateCharges() As Integer
    '    Get
    '        Return CType(viewstate("GracePeriodLateCharges"), Integer)
    '    End Get
    '    Set(ByVal Value As Integer)
    '        viewstate("GracePeriodLateCharges") = Value
    '    End Set
    'End Property

    'Private Property GracePeriodLateChargesBehaviour() As String
    '    Get
    '        Return CType(viewstate("GracePeriodLateChargesBehaviour"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        viewstate("GracePeriodLateChargesBehaviour") = Value
    '    End Set
    'End Property

    'Private Property PrepaymentPenaltyRate() As Decimal
    '    Get
    '        Return CType(viewstate("PrepaymentPenaltyRate"), Decimal)
    '    End Get
    '    Set(ByVal Value As Decimal)
    '        viewstate("PrepaymentPenaltyRate") = Value
    '    End Set
    'End Property

    'Private Property PrepaymentPenaltyRateBehaviour() As String
    '    Get
    '        Return CType(viewstate("PrepaymentPenaltyRateBehaviour"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        viewstate("PrepaymentPenaltyRateBehaviour") = Value
    '    End Set
    'End Property

    'Private Property DeskCollPhoneRemind() As Integer
    '    Get
    '        Return CType(viewstate("DeskCollPhoneRemind"), Integer)
    '    End Get
    '    Set(ByVal Value As Integer)
    '        viewstate("DeskCollPhoneRemind") = Value
    '    End Set
    'End Property

    'Private Property DeskCollPhoneRemindBehaviour() As String
    '    Get
    '        Return CType(viewstate("DeskCollPhoneRemindBehaviour"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        viewstate("DeskCollPhoneRemindBehaviour") = Value
    '    End Set
    'End Property

    'Private Property DeskCollOD() As Integer
    '    Get
    '        Return CType(viewstate("DeskCollOD"), Integer)
    '    End Get
    '    Set(ByVal Value As Integer)
    '        viewstate("DeskCollOD") = Value
    '    End Set
    'End Property

    'Private Property DeskCollODBehaviour() As String
    '    Get
    '        Return CType(viewstate("DeskCollODBehaviour"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        viewstate("DeskCollODBehaviour") = Value
    '    End Set
    'End Property

    'Private Property PrevODToRemind() As Integer
    '    Get
    '        Return CType(viewstate("PrevODToRemind"), Integer)
    '    End Get
    '    Set(ByVal Value As Integer)
    '        viewstate("PrevODToRemind") = Value
    '    End Set
    'End Property

    'Private Property PrevODToRemindBehaviour() As String
    '    Get
    '        Return CType(viewstate("PrevODToRemindBehaviour"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        viewstate("PrevODToRemindBehaviour") = Value
    '    End Set
    'End Property

    'Private Property PDCDayToRemind() As Integer
    '    Get
    '        Return CType(viewstate("PDCDayToRemind"), Integer)
    '    End Get
    '    Set(ByVal Value As Integer)
    '        viewstate("PDCDayToRemind") = Value
    '    End Set
    'End Property

    'Private Property PDCDayToRemindBehaviour() As String
    '    Get
    '        Return CType(viewstate("PDCDayToRemindBehaviour"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        viewstate("PDCDayToRemindBehaviour") = Value
    '    End Set
    'End Property

    'Private Property DeskCollSMSRemind() As Integer
    '    Get
    '        Return CType(viewstate("DeskCollSMSRemind"), Integer)
    '    End Get
    '    Set(ByVal Value As Integer)
    '        viewstate("DeskCollSMSRemind") = Value
    '    End Set
    'End Property

    'Private Property DeskCollSMSRemindBehaviour() As String
    '    Get
    '        Return CType(viewstate("DeskCollSMSRemindBehaviour"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        viewstate("DeskCollSMSRemindBehaviour") = Value
    '    End Set
    'End Property

    'Private Property DCR() As Integer
    '    Get
    '        Return CType(viewstate("DCR"), Integer)
    '    End Get
    '    Set(ByVal Value As Integer)
    '        viewstate("DCR") = Value
    '    End Set
    'End Property

    'Private Property DCRBehaviour() As String
    '    Get
    '        Return CType(viewstate("DCRBehaviour"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        viewstate("DCRBehaviour") = Value
    '    End Set
    'End Property

    'Private Property DaysBeforeDueToRN() As Integer
    '    Get
    '        Return CType(viewstate("DaysBeforeDueToRN"), Integer)
    '    End Get
    '    Set(ByVal Value As Integer)
    '        viewstate("DaysBeforeDueToRN") = Value
    '    End Set
    'End Property

    'Private Property DaysBeforeDueToRNBehaviour() As String
    '    Get
    '        Return CType(viewstate("DaysBeforeDueToRNBehaviour"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        viewstate("DaysBeforeDueToRNBehaviour") = Value
    '    End Set
    'End Property

    'Private Property ODToRAL() As Integer
    '    Get
    '        Return CType(viewstate("ODToRAL"), Integer)
    '    End Get
    '    Set(ByVal Value As Integer)
    '        viewstate("ODToRAL") = Value
    '    End Set
    'End Property

    'Private Property ODToRALBehaviour() As String
    '    Get
    '        Return CType(viewstate("ODToRALBehaviour"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        viewstate("ODToRALBehaviour") = Value
    '    End Set
    'End Property

    'Private Property RALPeriod() As Integer
    '    Get
    '        Return CType(viewstate("RALPeriod"), Integer)
    '    End Get
    '    Set(ByVal Value As Integer)
    '        viewstate("RALPeriod") = Value
    '    End Set
    'End Property

    'Private Property RALPeriodBehaviour() As String
    '    Get
    '        Return CType(viewstate("RALPeriodBehaviour"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        viewstate("RALPeriodBehaviour") = Value
    '    End Set
    'End Property

    'Private Property MaxDaysRNPaid() As Integer
    '    Get
    '        Return CType(viewstate("MaxDaysRNPaid"), Integer)
    '    End Get
    '    Set(ByVal Value As Integer)
    '        viewstate("MaxDaysRNPaid") = Value
    '    End Set
    'End Property

    'Private Property MaxDaysRNPaidBehaviour() As String
    '    Get
    '        Return CType(viewstate("MaxDaysRNPaidBehaviour"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        viewstate("MaxDaysRNPaidBehaviour") = Value
    '    End Set
    'End Property

    'Private Property MaxPTPYDays() As Integer
    '    Get
    '        Return CType(viewstate("MaxPTPYDays"), Integer)
    '    End Get
    '    Set(ByVal Value As Integer)
    '        viewstate("MaxPTPYDays") = Value
    '    End Set
    'End Property

    'Private Property MaxPTPYDaysBehaviour() As String
    '    Get
    '        Return CType(viewstate("MaxPTPYDaysBehaviour"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        viewstate("MaxPTPYDaysBehaviour") = Value
    '    End Set
    'End Property

    'Private Property PTPYBank() As Integer
    '    Get
    '        Return CType(viewstate("PTPYBank"), Integer)
    '    End Get
    '    Set(ByVal Value As Integer)
    '        viewstate("PTPYBank") = Value
    '    End Set
    'End Property

    'Private Property PTPYBankBehaviour() As String
    '    Get
    '        Return CType(viewstate("PTPYBankBehaviour"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        viewstate("PTPYBankBehaviour") = Value
    '    End Set
    'End Property

    'Private Property PTPYCompany() As Integer
    '    Get
    '        Return CType(viewstate("PTPYCompany"), Integer)
    '    End Get
    '    Set(ByVal Value As Integer)
    '        viewstate("PTPYCompany") = Value
    '    End Set
    'End Property

    'Private Property PTPYCompanyBehaviour() As String
    '    Get
    '        Return CType(viewstate("PTPYCompanyBehaviour"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        viewstate("PTPYCompanyBehaviour") = Value
    '    End Set
    'End Property

    'Private Property PTPYSupplier() As Integer
    '    Get
    '        Return CType(viewstate("PTPYSupplier"), Integer)
    '    End Get
    '    Set(ByVal Value As Integer)
    '        viewstate("PTPYSupplier") = Value
    '    End Set
    'End Property

    'Private Property PTPYSupplierBehaviour() As String
    '    Get
    '        Return CType(viewstate("PTPYSupplierBehaviour"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        viewstate("PTPYSupplierBehaviour") = Value
    '    End Set
    'End Property

    'Private Property RALExtension() As Integer
    '    Get
    '        Return CType(viewstate("RALExtension"), Integer)
    '    End Get
    '    Set(ByVal Value As Integer)
    '        viewstate("RALExtension") = Value
    '    End Set
    'End Property

    'Private Property RALExtensionBehaviour() As String
    '    Get
    '        Return CType(viewstate("RALExtensionBehaviour"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        viewstate("RALExtensionBehaviour") = Value
    '    End Set
    'End Property

    'Private Property InventoryExpected() As Integer
    '    Get
    '        Return CType(viewstate("InventoryExpected"), Integer)
    '    End Get
    '    Set(ByVal Value As Integer)
    '        viewstate("InventoryExpected") = Value
    '    End Set
    'End Property

    'Private Property InventoryExpectedBehaviour() As String
    '    Get
    '        Return CType(viewstate("InventoryExpectedBehaviour"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        viewstate("InventoryExpectedBehaviour") = Value
    '    End Set
    'End Property

    'Private Property BillingCharges() As Decimal
    '    Get
    '        Return CType(viewstate("BillingCharges"), Decimal)
    '    End Get
    '    Set(ByVal Value As Decimal)
    '        viewstate("BillingCharges") = Value
    '    End Set
    'End Property

    'Private Property BillingChargesBehaviour() As String
    '    Get
    '        Return CType(viewstate("BillingChargesBehaviour"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        viewstate("BillingChargesBehaviour") = Value
    '    End Set
    'End Property

    'Private Property LimitAPCash() As Decimal
    '    Get
    '        Return CType(viewstate("LimitAPCash"), Decimal)
    '    End Get
    '    Set(ByVal Value As Decimal)
    '        viewstate("LimitAPCash") = Value
    '    End Set
    'End Property

    'Private Property LimitAPCashBehaviour() As String
    '    Get
    '        Return CType(viewstate("LimitAPCashBehaviour"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        viewstate("LimitAPCashBehaviour") = Value
    '    End Set
    'End Property

    'Private Property IsRecourse() As Boolean
    '    Get
    '        Return CType(viewstate("IsRecourse"), Boolean)
    '    End Get
    '    Set(ByVal Value As Boolean)
    '        viewstate("IsRecourse") = Value
    '    End Set
    'End Property

    'Private Property IsActive() As Boolean
    '    Get
    '        Return CType(viewstate("IsActive"), Boolean)
    '    End Get
    '    Set(ByVal Value As Boolean)
    '        viewstate("IsActive") = Value
    '    End Set
    'End Property
    'Private Property PrepaymentPenaltyFixed() As Decimal
    '    Get
    '        Return CType(ViewState("PrepaymentPenaltyFixed"), Decimal)
    '    End Get
    '    Set(ByVal Value As Decimal)
    '        ViewState("PrepaymentPenaltyFixed") = Value
    '    End Set
    'End Property

    'Private Property PrepaymentPenaltyFixedBehaviour() As String
    '    Get
    '        Return CType(ViewState("PrepaymentPenaltyFixedBehaviour"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        ViewState("PrepaymentPenaltyFixedBehaviour") = Value
    '    End Set
    'End Property

    'Private Property PenaltyBasedOn() As String
    '    Get
    '        Return CType(ViewState("PenaltyBasedOn"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        ViewState("PenaltyBasedOn") = Value
    '    End Set
    'End Property

    'Private Property PenaltyBasedOnBehaviour() As String
    '    Get
    '        Return CType(ViewState("PenaltyBasedOnBehaviour"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        ViewState("PenaltyBasedOnBehaviour") = Value
    '    End Set
    'End Property

    'Private Property PenaltyRatePrevious() As Decimal
    '    Get
    '        Return CType(ViewState("PenaltyRatePrevious"), Decimal)
    '    End Get
    '    Set(ByVal Value As Decimal)
    '        ViewState("PenaltyRatePrevious") = Value
    '    End Set
    'End Property

    'Private Property PenaltyRateEffectiveDate() As Date
    '    Get
    '        Return CType(ViewState("PenaltyRateEffectiveDate"), Date)
    '    End Get
    '    Set(ByVal Value As Date)
    '        ViewState("PenaltyRateEffectiveDate") = Value
    '    End Set
    'End Property

    'Private Property PrepaymentPenaltyPrevious() As Decimal
    '    Get
    '        Return CType(ViewState("PrepaymentPenaltyPrevious"), Decimal)
    '    End Get
    '    Set(ByVal Value As Decimal)
    '        ViewState("PrepaymentPenaltyPrevious") = Value
    '    End Set
    'End Property

    'Private Property PrepaymentPenaltyEffectiveDate() As Date
    '    Get
    '        Return CType(ViewState("PrepaymentPenaltyEffectiveDate"), Date)
    '    End Get
    '    Set(ByVal Value As Date)
    '        ViewState("PrepaymentPenaltyEffectiveDate") = Value
    '    End Set
    'End Property

    'Private Property PrepaymentPenaltyFixedPrevious() As Decimal
    '    Get
    '        Return CType(ViewState("PrepaymentPenaltyFixedPrevious"), Decimal)
    '    End Get
    '    Set(ByVal Value As Decimal)
    '        ViewState("PrepaymentPenaltyFixedPrevious") = Value
    '    End Set
    'End Property

    'Private Property PrepaymentPenaltyFixedEffectiveDate() As Date
    '    Get
    '        Return CType(ViewState("PrepaymentPenaltyFixedEffectiveDate"), Date)
    '    End Get
    '    Set(ByVal Value As Date)
    '        ViewState("PrepaymentPenaltyFixedEffectiveDate") = Value
    '    End Set
    'End Property

    'Private Property PrioritasPembayaran() As String
    '    Get
    '        Return CType(ViewState("PrioritasPembayaran"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        ViewState("PrioritasPembayaran") = Value
    '    End Set
    'End Property

    'Private Property InsuranceDiscPercentage() As Decimal
    '    Get
    '        Return CType(ViewState("InsuranceDiscPercentage"), Decimal)
    '    End Get
    '    Set(ByVal Value As Decimal)
    '        ViewState("InsuranceDiscPercentage") = Value
    '    End Set
    'End Property

    'Private Property InsuranceDiscPercentageBehaviour() As String
    '    Get
    '        Return CType(ViewState("InsuranceDiscPercentageBehaviour"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        ViewState("InsuranceDiscPercentageBehaviour") = Value
    '    End Set
    'End Property

    'Private Property IncomeInsRatioPercentage() As Decimal
    '    Get
    '        Return CType(ViewState("IncomeInsRatioPercentage"), Decimal)
    '    End Get
    '    Set(ByVal Value As Decimal)
    '        ViewState("IncomeInsRatioPercentage") = Value
    '    End Set
    'End Property

    'Private Property IncomeInsRatioPercentageBehaviour() As String
    '    Get
    '        Return CType(ViewState("IncomeInsRatioPercentageBehaviour"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        ViewState("IncomeInsRatioPercentageBehaviour") = Value
    '    End Set
    'End Property

    'Private Property UmurKendaraanFrom() As Integer
    '    Get
    '        Return CType(ViewState("UmurKendaraanFrom"), Integer)
    '    End Get
    '    Set(ByVal Value As Integer)
    '        ViewState("UmurKendaraanFrom") = Value
    '    End Set
    'End Property

    'Private Property UmurKendaraanTo() As Integer
    '    Get
    '        Return CType(ViewState("UmurKendaraanTo"), Integer)
    '    End Get
    '    Set(ByVal Value As Integer)
    '        ViewState("UmurKendaraanTo") = Value
    '    End Set
    'End Property
#End Region

#Region "Page_Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If
        InitialDefaultPanel()
        AddHandler GridNavigator.PageChanged, AddressOf PageNavigation
        Me.FormID = "ProductOffering"
        If Not Me.IsPostBack Then
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If SessionInvalid() Then
                    Exit Sub
                End If
                Me.SearchBy = ""
                Me.SortBy = ""
            End If
            'txtPage.Text = "1"
            Me.Sort = "ProductOfferingID ASC"

            'If Request.QueryString("Flag") = "Report" Then
            '    Me.Description = Request.QueryString("Description")
            '    Me.BranchName = Request.QueryString("BranchFullName")
            '    Me.BranchID = Request.QueryString("BranchID")
            '    Me.ProductID = Request.QueryString("ProductID")
            'Else
            Me.ProductID = Request("ProductID")
            hplProductID.NavigateUrl = LinkTo(Me.ProductID, "Marketing")
            hplProductID.Text = Me.ProductID
            Me.Description = Request("Description")
            Me.BranchName = Request("BranchFullName")
            Me.BranchID = Me.sesBranchId.Replace("'", "")
            hplBranch.Text = Me.BranchName
            hplBranch.NavigateUrl = LinkToBranch(Me.ProductID, Me.BranchID)
            'End If

            Me.CmdWhere = "ProductID='" & Me.ProductID & "' and BranchID='" & Me.BranchID & "'"

            bindComboAPType()

            BindGridEntity(Me.CmdWhere)

            'cboGrossYieldRate.Visible = False
            FillCboInsuranceRateCard()

            'txtPeriodFrom.IsRequired = True
            'txtPeriodTo.IsRequired = True
        End If

    End Sub
#End Region
    Protected Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        currentPage = e.CurrentPage
        BindGridEntity(Me.CmdWhere, True)
        GridNavigator.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
    End Sub
#Region "LinkTo"
    Function LinkTo(ByVal strProductID As String, ByVal strStyle As String) As String
        Return "javascript:OpenWinProductView('" & strProductID & "','" & strStyle & "')"
    End Function
#End Region

#Region "LinkToBranch"
    Function LinkToBranch(ByVal strProductID As String, ByVal strBranchID As String) As String
        Return "javascript:OpenWinProductBranchHOView('" & strProductID & "','" & strBranchID & "')"
    End Function
#End Region

#Region "LinkToProductOffering"
    Function LinkToProductOffering(ByVal strProductOfferingID As String, ByVal strProductID As String, ByVal strBranchID As String, ByVal strStyle As String) As String
        Return "javascript:OpenWinProductOfferingBranchView('" & strProductOfferingID & "','" & strProductID & "','" & strBranchID & "','" & strStyle & "')"
    End Function
#End Region

#Region "LinkToSupplier"
    Function LinkToSupplier(ByVal strSupplierID As String, ByVal strAssetUsedNew As String, ByVal strBranchID As String) As String
        Return "javascript:OpenWinSupplier('" & strSupplierID & "','" & strAssetUsedNew & "','" & strBranchID & "')"
    End Function
#End Region

#Region "LinkToAssetMaster"
    Function LinkToAssetMaster(ByVal strAssetCode As String, ByVal strBranchID As String, ByVal strAssetTypeID As String) As String
        Return "javascript:OpenWinAsset('" & strAssetCode & "','" & strBranchID & "','" & strAssetTypeID & "')"
    End Function
#End Region

#Region "InitialDefaultPanel"
    Private Sub InitialDefaultPanel()
        pnlList.Visible = True
        pnlAddEdit.Visible = False
    End Sub
#End Region

#Region "BindGridEntity"
    Sub BindGridEntity(ByVal cmdWhere As String, Optional isFrNav As Boolean = False)
        Dim dtEntity As DataTable
        Dim oProduct As New Parameter.Product

        With oProduct
            .WhereCond = Me.CmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = Me.Sort
            .strConnection = GetConnectionString()
        End With
        oProduct = m_controller.ProductOfferingPaging(oProduct)

        If Not oProduct Is Nothing Then
            dtEntity = oProduct.ListData
            recordCount = oProduct.TotalRecords
        Else
            recordCount = 0
        End If
        If recordCount = 0 Then
            ButtonPrint.Enabled = False
        Else
            ButtonPrint.Enabled = True
        End If
        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0

        Dim oCustomClass As New Parameter.Common
        Dim oController As New AuthorizationController

        With oCustomClass
            .strConnection = GetConnectionString()
            .LoginId = Me.Loginid
            .FeatureID = "VIEW"
            .FormID = Me.FormID
            .AppID = Me.AppId
        End With
        Me.IsAuthorized = oController.CheckFeature(oCustomClass)

        dtgPaging.DataBind()

        hplProductID.Text = Me.ProductID
        lblDescription.Text = Me.Description
        If (isFrNav = False) Then
            GridNavigator.Initialize(recordCount, pageSize)
        End If
    End Sub
#End Region

#Region "dtgPaging_ItemDataBound"
    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        Dim imbDelete As ImageButton
        Dim imbEdit As ImageButton
        Dim lblIsFromProduct As New Label
        Dim hynProductOfferingID As New HyperLink

        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imbDelete"), ImageButton)
            imbDelete.Attributes.Add("Onclick", "return DeleteConfirm()")
            imbEdit = CType(e.Item.FindControl("imbEdit"), ImageButton)

            hynProductOfferingID = CType(e.Item.FindControl("hynProductOfferingID"), HyperLink)
            hynProductOfferingID.NavigateUrl = LinkToProductOffering(dtgPaging.DataKeys.Item(e.Item.ItemIndex).ToString, Me.ProductID, Me.BranchID, "Marketing")

            If Not Me.IsAuthorized Then
                Dim strHTTPServer As String
                Dim StrHTTPApp As String
                Dim strNameServer As String

                strHTTPServer = Request.ServerVariables("PATH_INFO")
                StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                strNameServer = Request.ServerVariables("SERVER_NAME")

                hynProductOfferingID = CType(e.Item.FindControl("hynProductOfferingID"), HyperLink)
                hynProductOfferingID.NavigateUrl = "http://" & strNameServer & "/" & StrHTTPApp & "/NotAuthorized.html"
            End If
        End If
    End Sub
#End Region

#Region " Navigation " 
#End Region

#Region "dtgEntity_ItemCommand"
    Private Sub dtgEntity_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        Dim oProduct As New Parameter.Product
        'Dim oProduct_Brc As New Parameter.Product

        If e.CommandName = "EDIT" Then
            If CheckFeature(Me.Loginid, Me.FormID, "EDIT", Me.AppId) Then
              

                Me.AddEdit = "EDIT"
                pnlList.Visible = False
                pnlAddEdit.Visible = True

                lblAddEdit.Text = Me.AddEdit
  

                oProduct.ProductOfferingID = dtgPaging.DataKeys.Item(e.Item.ItemIndex).ToString

                oProduct.ProductId = Me.ProductID
                oProduct.BranchId = Me.BranchID
                oProduct.strConnection = GetConnectionString()
                oProduct = m_controller.ProductOfferingEdit(oProduct)

                txtValidDateFrom.Text = oProduct.StartDate.ToString("dd/MM/yyyy")
                txtValidDateTo.Text = oProduct.EndDate.ToString("dd/MM/yyyy")

                oProduct.ProductId = Me.ProductID
                oProduct.BranchId = Me.BranchID 
                Me.ProductOfferingID = oProduct.ProductOfferingID
                Me.AssetTypeID = oProduct.AssetTypeID
                Me.ScoreSchemeID = oProduct.ScoreSchemeID
                Me.CreditScoreSchemeID = oProduct.CreditScoreSchemeID
                Me.JournalSchemeID = oProduct.JournalSchemeID
                Me.ApprovalSchemeID = oProduct.ApprovalSchemeID
                Me.AssetUsedNew = oProduct.AssetUsedNew
                Me.FinanceType = oProduct.FinanceType
                Me.KegiatanUsaha = oProduct.KegiatanUsaha
                Me.JenisPembiayaan = oProduct.JenisPembiayaan

                cboPaket.SelectedIndex = cboPaket.Items.IndexOf(cboPaket.Items.FindByValue(oProduct.paket))

                lblProductOfferingID.Visible = True
                lblProductOfferingID.Text = dtgPaging.DataKeys.Item(e.Item.ItemIndex).ToString
                txtProductOfferingID.Visible = False
                LProductOfferingID.Visible = False

                txtDescription.Text = oProduct.Description

                
                lblAssetType.Text = oProduct.AssetType_desc
                lblScoreSchemeCredit.Text = oProduct.CreditScoreSchemeMaster_desc
                lblScoreSchemeMarketing.Text = oProduct.ScoreSchemeMaster_desc
                lblJournalScheme.Text = oProduct.JournalScheme_desc
                lblApprovalScheem.Text = oProduct.ApprovalTypeScheme_desc
                 
                lblAssetUsedNew.Text = IIf(oProduct.AssetUsedNew = "U", "Used", "New")
                txtUmurKendaraanFrom.Text = oProduct.UmurKendaraanFrom.ToString
                txtUmurKendaraanTo.Text = oProduct.UmurKendaraanTo.ToString
                lblKegiatanUsaha.Text = oProduct.KegiatanUsahaDesc
                lblJenisPembiayaan.Text = oProduct.JenisPembiayaanDesc 


                

                SkemaTab.Product = oProduct
                SkemaTab.UCMode = "EDIT"
                SkemaTab.InitUCEditMode()
                SkemaTab.BehaviourLockSetting()

                BiayaTab.Product = oProduct
                BiayaTab.UCMode = "EDIT"
                BiayaTab.InitUCEditMode()
                BiayaTab.BehaviourLockSetting()

                AngsuranTab.Product = oProduct
                AngsuranTab.UCMode = "EDIT"
                AngsuranTab.InitUCEditMode()
                AngsuranTab.BehaviourLockSetting()

                CollactionTab.Product = oProduct
                CollactionTab.UCMode = "EDIT"
                CollactionTab.InitUCEditMode()
                CollactionTab.BehaviourLockSetting()

                FinaceTab.Product = oProduct
                FinaceTab.UCMode = "EDIT"
                FinaceTab.InitUCEditMode()
                FinaceTab.SetProductJualTab()
                FinaceTab.BehaviourLockSetting()
            End If

        ElseIf e.CommandName = "DELETE" Then
            If CheckFeature(Me.Loginid, Me.FormID, "DEL", Me.AppId) Then
                Dim customClass As New Parameter.Product

                With customClass
                    .ProductId = Me.ProductID
                    .ProductOfferingID = dtgPaging.DataKeys.Item(e.Item.ItemIndex).ToString
                    .BranchId = Me.BranchID
                    .strConnection = GetConnectionString()
                End With

                Dim ResultOutput As String
                ResultOutput = m_controller.ProductOfferingDelete(customClass)
                If ResultOutput = "OK" Then
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, False)
                Else
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_FAILED, True)
                End If

                Me.CmdWhere = "ProductID='" & Me.ProductID & "' and BranchID='" & Me.BranchID & "'"
                BindGridEntity(Me.CmdWhere)
            End If
        End If
    End Sub
                
#End Region

#Region "ButtonSave"
    Private Sub ButtonSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
        'If Page.IsValid Then
        Dim oProduct As New Parameter.Product
        Dim ErrMessage As String = ""

        If txtValidDateFrom.Text.Trim <> "" Then
            If (ConvertDate2(txtValidDateFrom.Text) > Me.BusinessDate) Then
                ShowMessage(lblMessage, "Periode dari harus lebih kecil dari Tanggal Business", True)
                pnlList.Visible = False
                pnlAddEdit.Visible = True
                Exit Sub
            End If
        Else
            ShowMessage(lblMessage, "Please fill in Period From!", True)
            pnlList.Visible = False
            pnlAddEdit.Visible = True
            Exit Sub
        End If

        If txtValidDateTo.Text.Trim <> "" Then
            If (ConvertDate2(txtValidDateTo.Text) < Me.BusinessDate) Then
                ShowMessage(lblMessage, "Periode harus lebih besar dari tanggal Business", True)
                pnlList.Visible = False
                pnlAddEdit.Visible = True
                Exit Sub
            End If
        Else
            ShowMessage(lblMessage, "Harap isi Periode Sampai", True)
            pnlList.Visible = False
            pnlAddEdit.Visible = True
            Exit Sub
        End If
         

        With oProduct
            .ProductId = Me.ProductID
            .Branch_ID = Me.BranchID
            .ProductOfferingID = txtProductOfferingID.Text.Trim
            .Description = txtDescription.Text

            .AssetTypeID = Me.AssetTypeID
            .ScoreSchemeID = Me.ScoreSchemeID
            .CreditScoreSchemeID = Me.CreditScoreSchemeID
            .JournalSchemeID = Me.JournalSchemeID
            .ApprovalSchemeID = Me.ApprovalSchemeID
            .AssetUsedNew = Me.AssetUsedNew
            .FinanceType = ""
            .JenisPembiayaan = Me.JenisPembiayaan
            .KegiatanUsaha = Me.KegiatanUsaha
            .UmurKendaraanFrom = CInt(txtUmurKendaraanFrom.Text.Trim)
            .UmurKendaraanTo = CInt(txtUmurKendaraanTo.Text.Trim) 

            .SupplierID = "-"

            .AssetCode = "-"

            .InsuranceRateCardID = ""
            .InstallmentAmount = 0
            .InstallmentAmountBehaviour = ""

            .ResidualValue = 0
            .ResidualValueBehaviour = ""
             
            .StartDate = ConvertDate2(txtValidDateFrom.Text)

            .EndDate = ConvertDate2(txtValidDateTo.Text) 
            .strConnection = GetConnectionString()
            .paket = cboPaket.SelectedValue.Trim
        End With
       
        SkemaTab.CollectResult(oProduct)
        BiayaTab.CollectResult(oProduct)
        AngsuranTab.CollectResult(oProduct)
        CollactionTab.CollectResult(oProduct)
        FinaceTab.CollectResult(oProduct) 

        If Me.AddEdit = "ADD" Then
            Dim dtBranch As New DataTable 
            pnlList.Visible = False
            pnlAddEdit.Visible = False
            oProduct.TotalBranch = "0" 

            Try
                ErrMessage = m_controller.ProductOfferingSaveAdd(oProduct)
                If ErrMessage = "" Then
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_SUCCESS, False)
                    pnlAddEdit.Visible = False
                    pnlList.Visible = True
                    Me.CmdWhere = "ProductID='" & Me.ProductID & "' and BranchID='" & Me.BranchID & "'"
                    BindGridEntity(Me.CmdWhere)
                ElseIf ErrMessage <> "" Then
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_FAILED, True)
                    pnlAddEdit.Visible = True
                    pnlList.Visible = False
                    Exit Sub
                End If
            Catch exp As Exception
                ShowMessage(lblMessage, exp.Message, True)
                Return
            End Try
        ElseIf Me.AddEdit = "EDIT" Then
            Try
                oProduct.ProductOfferingID = lblProductOfferingID.Text.Trim
                m_controller.ProductOfferingSaveEdit(oProduct)

                ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
                pnlAddEdit.Visible = False
                pnlList.Visible = True
                Me.CmdWhere = "ProductID='" & Me.ProductID & "' and BranchID='" & Me.BranchID & "'"
                BindGridEntity(Me.CmdWhere)
            Catch exp As Exception
                ShowMessage(lblMessage, exp.Message, True)
                Return
            End Try
        End If

        'End If
    End Sub
   
#End Region

#Region "imbAdd"
    Private Sub imbAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonAdd.Click
        Dim oProduct As New Parameter.Product
        If CheckFeature(Me.Loginid, Me.FormID, "ADD", Me.AppId) Then

            pnlAddEdit.Visible = True
            pnlList.Visible = False
            Me.AddEdit = "ADD"
            lblAddEdit.Text = Me.AddEdit
            pnlAddEdit.Visible = True
            pnlList.Visible = False

            oProduct.ProductId = Me.ProductID
            oProduct.BranchId = Me.BranchID

            oProduct.strConnection = GetConnectionString()
            oProduct = m_controller.ProductOfferingAdd(oProduct)

            lblProductOfferingID.Visible = False
            txtProductOfferingID.Visible = True
            LProductOfferingID.Visible = True

            txtProductOfferingID.Text = ""
            txtDescription.Text = ""

            Me.AssetTypeID = oProduct.AssetTypeID
            Me.ScoreSchemeID = oProduct.ScoreSchemeID
            Me.CreditScoreSchemeID = oProduct.CreditScoreSchemeID
            Me.JournalSchemeID = oProduct.JournalSchemeID
            Me.ApprovalSchemeID = oProduct.ApprovalSchemeID
            Me.AssetUsedNew = oProduct.AssetUsedNew
            Me.FinanceType = oProduct.FinanceType

            Me.KegiatanUsaha = oProduct.KegiatanUsaha
            Me.JenisPembiayaan = oProduct.JenisPembiayaan

            txtValidDateFrom.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
            txtValidDateTo.Text = Me.BusinessDate.ToString("dd/MM/yyyy")

            lblAssetType.Text = oProduct.AssetType_desc
            lblScoreSchemeCredit.Text = oProduct.CreditScoreSchemeMaster_desc
            lblScoreSchemeMarketing.Text = oProduct.ScoreSchemeMaster_desc
            lblJournalScheme.Text = oProduct.JournalScheme_desc
            lblApprovalScheem.Text = oProduct.ApprovalTypeScheme_desc

            lblKegiatanUsaha.Text = oProduct.KegiatanUsahaDesc
            lblJenisPembiayaan.Text = oProduct.JenisPembiayaanDesc




            lblAssetUsedNew.Text = IIf(oProduct.AssetUsedNew = "U", "Used", "New")
            txtUmurKendaraanFrom.Text = "0"
            txtUmurKendaraanTo.Text = "0"



            SkemaTab.Product = oProduct
            SkemaTab.UCMode = "ADD"
            SkemaTab.InitUCEditMode()
            SkemaTab.BindFiedsForEdit()

            BiayaTab.Product = oProduct
            BiayaTab.UCMode = "ADD"
            BiayaTab.InitUCEditMode()
            BiayaTab.bindFiedsForEdit()


            AngsuranTab.Product = oProduct
            AngsuranTab.UCMode = "ADD"
            AngsuranTab.InitUCEditMode()
            AngsuranTab.bindFiedsForEdit()
             

            CollactionTab.Product = oProduct
            CollactionTab.UCMode = "ADD"
            CollactionTab.InitUCEditMode()
            CollactionTab.bindFiedsForEdit()

            FinaceTab.Product = oProduct
            FinaceTab.UCMode = "ADD"
            FinaceTab.InitUCEditMode()
            FinaceTab.SetProductJualTab()
            FinaceTab.bindFiedsForEdit()
        End If
    End Sub

    'If oProduct.FinanceType = "CF" Then
    '    lblFinanceType.Text = "Consumer Finance"
    'ElseIf oProduct.FinanceType = "L" Then
    '    lblFinanceType.Text = "Leasing"
    'End If

    'txtEffectiveRate.Text = CStr(FormatNumber(oProduct.EffectiveRate, 6))
    'cboEffectiveRateBehaviour.SelectedIndex = cboEffectiveRateBehaviour.Items.IndexOf(cboEffectiveRateBehaviour.Items.FindByValue(oProduct.EffectiveRateBehaviour))
    'Hide_EffectiveRate.Value = CStr(FormatNumber(oProduct.EffectiveRate, 6))
    'Hide_EffectiveRateBeh.Value = oProduct.EffectiveRateBehaviour

    'txtGrossYeildRate.Text = CStr(FormatNumber(oProduct.GrossYieldRate, 6))
    'cboGrossYieldRate.SelectedIndex = cboGrossYieldRate.Items.IndexOf(cboGrossYieldRate.Items.FindByValue(oProduct.GrossYieldRateBehaviour))
    'Hide_GrossYieldRate.Value = CStr(FormatNumber(oProduct.GrossYieldRate, 6))
    'Hide_GrossYieldRateBeh.Value = oProduct.GrossYieldRateBehaviour

    'txtMinimumTenor.Text = CStr(oProduct.MinimumTenor)
    'txtMaximumTenor.Text = CStr(oProduct.MaximumTenor)
    'Hide_MinimumTenor.Value = CStr(oProduct.MinimumTenor)
    'Hide_MaximumTenor.Value = CStr(oProduct.MaximumTenor)

    'txtLCP_Installment.Text = CStr(FormatNumber(oProduct.PenaltyPercentage, 6))
    'cboLCP_Installment.SelectedIndex = cboLCP_Installment.Items.IndexOf(cboLCP_Installment.Items.FindByValue(oProduct.PenaltyPercentageBehaviour))
    'Hide_PenaltyPercentage.Value = CStr(FormatNumber(oProduct.PenaltyPercentage, 6))
    'Hide_PenaltyPercentageBeh.Value = oProduct.PenaltyPercentageBehaviour


    'txtPrepaymentPenaltyFixed.Text = "0"
    'cboPrepaymentPenaltyFixedBehaviour.SelectedIndex = cboPrepaymentPenaltyFixedBehaviour.Items.IndexOf(cboPrepaymentPenaltyFixedBehaviour.Items.FindByValue("D"))
    'rboPenaltyBasedOn.SelectedIndex = rboPenaltyBasedOn.Items.IndexOf(rboPenaltyBasedOn.Items.FindByValue("PB"))
    'cboPenaltyBasedOn.SelectedIndex = cboPenaltyBasedOn.Items.IndexOf(cboPenaltyBasedOn.Items.FindByValue("D"))

    'txtPenaltyRatePrevious.Text = "0"
    '    'txtPenaltyRateEffectiveDate.Text = ""
    'txtPrepaymentPenaltyPrevious.Text = "0"
    'txtPrepaymentPenaltyEffectiveDate.Text = ""
    'txtPrepaymentPenaltyFixedPrevious.Text = "0"
    'txtPrepaymentPenaltyFixedEffectiveDate.Text = ""
    'rboPrioritasPembayaran.SelectedIndex = rboPrioritasPembayaran.Items.IndexOf(rboPrioritasPembayaran.Items.FindByValue("DE"))


    'txtInsuranceDiscPercentage.Text = "0"
    'cboInsuranceDiscPercentageBehaviour.SelectedIndex = cboInsuranceDiscPercentageBehaviour.Items.IndexOf(cboInsuranceDiscPercentageBehaviour.Items.FindByValue("D"))
    'txtIncomeInsRatioPercentage.Text = "0"
    'cboIncomeInsRatioPercentageBehaviour.SelectedIndex = cboIncomeInsRatioPercentageBehaviour.Items.IndexOf(cboIncomeInsRatioPercentageBehaviour.Items.FindByValue("D"))


    '-----

    'txtLCP_Insurance.Text = CStr(FormatNumber(oProduct.InsurancePenaltyPercentage, 6))
    'cboLCP_Insurance.SelectedIndex = cboLCP_Insurance.Items.IndexOf(cboLCP_Insurance.Items.FindByValue(oProduct.InsurancePenaltyPercentageBehaviour))
    'Hide_InsurancePenaltyPercentage.Value = CStr(FormatNumber(oProduct.InsurancePenaltyPercentage, 6))
    'Hide_InsurancePenaltyPercentageBeh.Value = oProduct.InsurancePenaltyPercentageBehaviour

    'txtCancellationFee.Text = CStr(oProduct.CancellationFee)
    'cboCancellationFee.SelectedIndex = cboCancellationFee.Items.IndexOf(cboCancellationFee.Items.FindByValue(oProduct.CancellationFeeBehaviour))
    'Hide_CancellationFee.Value = CStr(oProduct.CancellationFee)
    'Hide_CancellationFeeBeh.Value = oProduct.CancellationFeeBehaviour

    'txtAdminFee.Text = CStr(oProduct.AdminFee)
    'cboAdminFee.SelectedIndex = cboAdminFee.Items.IndexOf(cboAdminFee.Items.FindByValue(oProduct.AdminFeeBehaviour))
    'Hide_AdminFee.Value = CStr(oProduct.AdminFee)
    'Hide_AdminFeeBeh.Value = oProduct.AdminFeeBehaviour

    'txtFiduciaFee.Text = CStr(oProduct.FiduciaFee)
    'cboFiduciaFee.SelectedIndex = cboFiduciaFee.Items.IndexOf(cboFiduciaFee.Items.FindByValue(oProduct.FiduciaFeeBehaviour))
    'Hide_FiduciaFee.Value = CStr(oProduct.FiduciaFee)
    'Hide_FiduciaFeeBeh.Value = oProduct.FiduciaFeeBehaviour

    'txtProvisionFee.Text = CStr(oProduct.ProvisionFee)
    'cboProvisionFee.SelectedIndex = cboProvisionFee.Items.IndexOf(cboProvisionFee.Items.FindByValue(oProduct.ProvisionFeeBehaviour))
    'Hide_ProvisionFee.Value = CStr(oProduct.ProvisionFee)
    'Hide_ProvisionFeeBeh.Value = oProduct.ProvisionFeeBehaviour

    'txtNotaryFee.Text = CStr(oProduct.NotaryFee)
    'cboNotaryFee.SelectedIndex = cboNotaryFee.Items.IndexOf(cboNotaryFee.Items.FindByValue(oProduct.NotaryFeeBehaviour))
    'Hide_NotaryFee.Value = CStr(oProduct.NotaryFee)
    'Hide_NotaryFeeBeh.Value = oProduct.NotaryFeeBehaviour

    'txtSurveyFee.Text = CStr(oProduct.SurveyFee)
    'cboSurveyFee.SelectedIndex = cboSurveyFee.Items.IndexOf(cboSurveyFee.Items.FindByValue(oProduct.SurveyFeeBehaviour))
    'Hide_SurveyFee.Value = CStr(oProduct.SurveyFee)
    'Hide_SurveyFeeBeh.Value = oProduct.SurveyFeeBehaviour

    'txtVisitFee.Text = CStr(oProduct.VisitFee)
    'cboVisitFee.SelectedIndex = cboVisitFee.Items.IndexOf(cboVisitFee.Items.FindByValue(oProduct.VisitFeeBehaviour))
    'Hide_VisitFee.Value = CStr(oProduct.VisitFee)
    'Hide_VisitFeeBeh.Value = oProduct.VisitFeeBehaviour

    'txtReschedulingFee.Text = CStr(oProduct.ReschedulingFee)
    'cboReschedulingFee.SelectedIndex = cboReschedulingFee.Items.IndexOf(cboReschedulingFee.Items.FindByValue(oProduct.ReschedulingFeeBehaviour))
    'Hide_ReschedulingFee.Value = CStr(oProduct.ReschedulingFee)
    'Hide_ReschedulingFeeBeh.Value = oProduct.ReschedulingFeeBehaviour

    'txtATFee.Text = CStr(oProduct.AgreementTransferFee)
    'cboATFee.SelectedIndex = cboATFee.Items.IndexOf(cboATFee.Items.FindByValue(oProduct.AgreementTransferFeeBehaviour))
    'Hide_AgreementTransferFee.Value = CStr(oProduct.AgreementTransferFee)
    'Hide_AgreementTransferFeeBeh.Value = oProduct.AgreementTransferFeeBehaviour

    'txtCDDRFee.Text = CStr(oProduct.ChangeDueDateFee)
    'cboCDDRFee.SelectedIndex = cboCDDRFee.Items.IndexOf(cboCDDRFee.Items.FindByValue(oProduct.ChangeDueDateFeeBehaviour))
    'Hide_ChangeDueDateFee.Value = CStr(oProduct.ChangeDueDateFee)
    'Hide_ChangeDueDateFeeBeh.Value = oProduct.ChangeDueDateFeeBehaviour

    'txtAssetReplacementFee.Text = CStr(oProduct.AssetReplacementFee)
    'cboAssetReplacementFee.SelectedIndex = cboAssetReplacementFee.Items.IndexOf(cboAssetReplacementFee.Items.FindByValue(oProduct.AssetReplacementFeeBehaviour))
    'Hide_AssetReplacementFee.Value = CStr(oProduct.AssetReplacementFee)
    'Hide_AssetReplacementFeeBeh.Value = oProduct.AssetReplacementFeeBehaviour

    'txtRepossesFee.Text = CStr(oProduct.RepossesFee)
    'cboRepossesFee.SelectedIndex = cboRepossesFee.Items.IndexOf(cboRepossesFee.Items.FindByValue(oProduct.RepossesFeeBehaviour))
    'Hide_RepossesFee.Value = CStr(oProduct.RepossesFee)
    'Hide_RepossesFeeBeh.Value = oProduct.RepossesFeeBehaviour

    'txtLDFee.Text = CStr(oProduct.LegalisirDocFee)
    'cboLDFee.SelectedIndex = cboLDFee.Items.IndexOf(cboLDFee.Items.FindByValue(oProduct.LegalisirDocFeeBehaviour))
    'Hide_LegalizedDocumentFee.Value = CStr(oProduct.LegalisirDocFee)
    'Hide_LegalizedDocumentFeeBeh.Value = oProduct.LegalisirDocFeeBehaviour

    'txtPDCBounceFee.Text = CStr(oProduct.PDCBounceFee)
    'cboPDCBounceFee.SelectedIndex = cboPDCBounceFee.Items.IndexOf(cboPDCBounceFee.Items.FindByValue(oProduct.PDCBounceFeeBehaviour))
    'Hide_PDCBounceFee.Value = CStr(oProduct.PDCBounceFee)
    'Hide_PDCBounceFeeBeh.Value = oProduct.PDCBounceFeeBehaviour

    'txtInsuranceAdminFee.Text = CStr(oProduct.InsAdminFee)
    'cboInsuranceAdminFee.SelectedIndex = cboInsuranceAdminFee.Items.IndexOf(cboInsuranceAdminFee.Items.FindByValue(oProduct.InsAdminFeeBehaviour))
    'Hide_InsuranceAdminFee.Value = CStr(oProduct.InsAdminFee)
    'Hide_InsuranceAdminFeeBeh.Value = oProduct.InsAdminFeeBehaviour

    'txtISDFee.Text = CStr(oProduct.InsStampDutyFee)
    'cboISDFee.SelectedIndex = cboISDFee.Items.IndexOf(cboISDFee.Items.FindByValue(oProduct.InsStampDutyFeeBehaviour))
    'Hide_InsuranceStampDutyFee.Value = CStr(oProduct.InsStampDutyFee)
    'Hide_InsuranceStampDutyFeeBeh.Value = oProduct.InsStampDutyFeeBehaviour

    'txtBiayaPolis.Text = CStr(oProduct.BiayaPolis)
    'cboBiayaPolis.SelectedIndex = cboBiayaPolis.Items.IndexOf(cboBiayaPolis.Items.FindByValue(oProduct.BiayaPolisBehaviour))
    'Hide_BiayaPolis.Value = CStr(oProduct.BiayaPolis)
    'Hide_BiayaPolisBeh.Value = oProduct.BiayaPolisBehaviour

    'txtPOExpirationDays.Text = CStr(oProduct.DaysPOExpiration)
    'cboPOExpirationDays.SelectedIndex = cboPOExpirationDays.Items.IndexOf(cboPOExpirationDays.Items.FindByValue(oProduct.DaysPOExpirationBehaviour))
    'Hide_POExpirationDays.Value = CStr(oProduct.DaysPOExpiration)
    'Hide_POExpirationDaysBeh.Value = oProduct.DaysPOExpirationBehaviour

    'txtInsToleranceAmount.Text = CStr(oProduct.InstallmentToleranceAmount)
    'cboInsToleranceAmount.SelectedIndex = cboInsToleranceAmount.Items.IndexOf(cboInsToleranceAmount.Items.FindByValue(oProduct.InstallmentToleranceAmountBehaviour))
    'Hide_InstallmentToleranceAmount.Value = CStr(oProduct.InstallmentToleranceAmount)
    'Hide_InstallmentToleranceAmountBeh.Value = oProduct.InstallmentToleranceAmountBehaviour

    'txtMinimumDPPerc.Text = CStr(FormatNumber(oProduct.DPPercentage, 6))
    'cboMinimumDPPerc.SelectedIndex = cboMinimumDPPerc.Items.IndexOf(cboMinimumDPPerc.Items.FindByValue(oProduct.DPPercentageBehaviour))
    'Hide_DPPercentage.Value = CStr(FormatNumber(oProduct.DPPercentage, 6))
    'Hide_DPPercentageBeh.Value = oProduct.DPPercentageBehaviour

    'txtMIITBR.Text = CStr(oProduct.RejectMinimumIncome)
    'cboMIITBR.SelectedIndex = cboMIITBR.Items.IndexOf(cboMIITBR.Items.FindByValue(oProduct.RejectMinimumIncomeBehaviour))
    'Hide_RejectMinimumIncome.Value = CStr(oProduct.RejectMinimumIncome)
    'Hide_RejectMinimumIncomeBeh.Value = oProduct.RejectMinimumIncomeBehaviour

    'txtMIITBW.Text = CStr(oProduct.WarningMinimumIncome)
    'cboMIITBW.SelectedIndex = cboMIITBW.Items.IndexOf(cboMIITBW.Items.FindByValue(oProduct.WarningMinimumIncomeBehaviour))
    'Hide_WarningMinimumIncome.Value = CStr(oProduct.WarningMinimumIncome)
    'Hide_WarningMinimumIncomeBeh.Value = oProduct.WarningMinimumIncomeBehaviour

    'Dim strIsSPAutomatic As String
    'If oProduct.IsSPAutomatic = True Then
    '    strIsSPAutomatic = "1"
    '    cboIsSPAutomaticBehaviour.Enabled = True
    '    txtLengthSPProcess.Enabled = True
    'Else
    '    strIsSPAutomatic = "0"
    '    cboIsSPAutomaticBehaviour.Enabled = False
    '    txtLengthSPProcess.Enabled = False
    'End If

    'Dim strIsSP1Automatic As String
    'If oProduct.IsSP1Automatic = True Then
    '    strIsSP1Automatic = "1"
    '    cboIsSP1AutomaticBehaviour.Enabled = True
    '    txtLengthSP1Process.Enabled = True
    'Else
    '    strIsSP1Automatic = "0"
    '    cboIsSP1AutomaticBehaviour.Enabled = False
    '    txtLengthSP1Process.Enabled = False
    'End If

    'Dim strIsSP2Automatic As String
    'If oProduct.IsSP2Automatic = True Then
    '    strIsSP2Automatic = "1"
    '    cboIsSP2AutomaticBehaviour.Enabled = True
    '    txtLengthSP2Process.Enabled = True
    'Else
    '    strIsSP2Automatic = "0"
    '    cboIsSP2AutomaticBehaviour.Enabled = False
    '    txtLengthSP2Process.Enabled = False
    'End If


    'cboIsSPAutomatic.SelectedIndex = cboIsSPAutomatic.Items.IndexOf(cboIsSPAutomatic.Items.FindByValue(strIsSPAutomatic))
    'cboIsSPAutomaticBehaviour.SelectedIndex = cboIsSPAutomaticBehaviour.Items.IndexOf(cboIsSPAutomaticBehaviour.Items.FindByValue(oProduct.IsSPAutomaticBehaviour))
    'txtLengthSPProcess.Text = CStr(oProduct.LengthSPProcess)

    'cboIsSP1Automatic.SelectedIndex = cboIsSP1Automatic.Items.IndexOf(cboIsSP1Automatic.Items.FindByValue(strIsSP1Automatic))
    'cboIsSP1AutomaticBehaviour.SelectedIndex = cboIsSP1AutomaticBehaviour.Items.IndexOf(cboIsSP1AutomaticBehaviour.Items.FindByValue(oProduct.IsSP1AutomaticBehaviour))
    'txtLengthSP1Process.Text = CStr(oProduct.LengthSP1Process)

    'cboIsSP2Automatic.SelectedIndex = cboIsSP2Automatic.Items.IndexOf(cboIsSP2Automatic.Items.FindByValue(strIsSP2Automatic))
    'cboIsSP2AutomaticBehaviour.SelectedIndex = cboIsSP2AutomaticBehaviour.Items.IndexOf(cboIsSP2AutomaticBehaviour.Items.FindByValue(oProduct.IsSP2AutomaticBehaviour))
    'txtLengthSP2Process.Text = CStr(oProduct.LengthSP2Process)

    'If oProduct.IsSPAutomatic = True Then
    '    cboIsSPAutomatic.Enabled = True
    '    txtLengthSPProcess.Enabled = True
    'End If

    'If oProduct.IsSP1Automatic = True Then
    '    cboIsSP1Automatic.Enabled = True
    '    txtLengthSP1Process.Enabled = True
    'End If

    'If oProduct.IsSP2Automatic = True Then
    '    cboIsSP2Automatic.Enabled = True
    '    txtLengthSP2Process.Enabled = True
    'End If

    'txtLMDProcessed.Text = CStr(oProduct.LengthMainDocProcess)
    'cboLMDProcessed.SelectedIndex = cboLMDProcessed.Items.IndexOf(cboLMDProcessed.Items.FindByValue(oProduct.LengthMainDocProcessBehaviour))
    'Hide_LengthMainDocProcess.Value = CStr(oProduct.LengthMainDocProcess)
    'Hide_LengthMainDocProcessBeh.Value = oProduct.LengthMainDocProcessBehaviour

    'txtLMDTaken.Text = CStr(oProduct.LengthMainDocTaken)
    'cboLMDTaken.SelectedIndex = cboLMDTaken.Items.IndexOf(cboLMDTaken.Items.FindByValue(oProduct.LengthMainDocTakenBehaviour))
    'Hide_LengthMainDocTaken.Value = CStr(oProduct.LengthMainDocTaken)
    'Hide_LengthMainDocProcessBeh.Value = oProduct.LengthMainDocTakenBehaviour

    'txtGPLC.Text = CStr(oProduct.GracePeriodLateCharges)
    'cboGPLC.SelectedIndex = cboGPLC.Items.IndexOf(cboGPLC.Items.FindByValue(oProduct.GracePeriodLateChargesBehaviour))
    'Hide_GracePeriodLateCharges.Value = CStr(oProduct.GracePeriodLateCharges)
    'Hide_GracePeriodLateChargesBeh.Value = oProduct.GracePeriodLateChargesBehaviour

    'txtPPR.Text = CStr(FormatNumber(oProduct.PrepaymentPenaltyRate, 6))
    'cboPPR.SelectedIndex = cboPPR.Items.IndexOf(cboPPR.Items.FindByValue(oProduct.PrepaymentPenaltyRateBehaviour))
    'Hide_PrepaymentPenaltyRate.Value = CStr(FormatNumber(oProduct.PrepaymentPenaltyRate, 6))
    'Hide_PrepaymentPenaltyRateBeh.Value = oProduct.PrepaymentPenaltyRateBehaviour

    'txtDaysToRemind.Text = CStr(oProduct.DeskCollPhoneRemind)
    'cboDaysToRemind.SelectedIndex = cboDaysToRemind.Items.IndexOf(cboDaysToRemind.Items.FindByValue(oProduct.DeskCollPhoneRemindBehaviour))
    'Hide_DeskCollPhoneRemind.Value = CStr(oProduct.DeskCollPhoneRemind)
    'Hide_DeskCollPhoneRemindBeh.Value = oProduct.DeskCollPhoneRemindBehaviour

    'txtDaysOverduetocall.Text = CStr(oProduct.DeskCollOD)
    'cboDaysOverduetocall.SelectedIndex = cboDaysOverduetocall.Items.IndexOf(cboDaysOverduetocall.Items.FindByValue(oProduct.DeskCollODBehaviour))
    'Hide_DeskCollOD.Value = CStr(oProduct.DeskCollOD)
    'Hide_DeskCollODBeh.Value = oProduct.DeskCollODBehaviour

    'txtPreviousOverduetoremind.Text = CStr(oProduct.PrevODToRemind)
    'cboPreviousOverduetoremind.SelectedIndex = cboPreviousOverduetoremind.Items.IndexOf(cboPreviousOverduetoremind.Items.FindByValue(oProduct.PrevODToRemindBehaviour))
    'Hide_PrevODToRemind.Value = CStr(oProduct.PrevODToRemind)
    'Hide_PrevODToRemindBeh.Value = oProduct.PrevODToRemindBehaviour

    'txtPDCRequesttocall.Text = CStr(oProduct.PDCDayToRemind)
    'cboPDCRequesttocall.SelectedIndex = cboPDCRequesttocall.Items.IndexOf(cboPDCRequesttocall.Items.FindByValue(oProduct.PDCDayToRemindBehaviour))
    'Hide_PDCDaysToRemind.Value = CStr(oProduct.PDCDayToRemind)
    'Hide_PDCDaysToRemindBeh.Value = oProduct.PDCDayToRemindBehaviour

    'txtDaysToRemind.Text = CStr(oProduct.DeskCollSMSRemind)
    'cboDaysToRemind.SelectedIndex = cboDaysToRemind.Items.IndexOf(cboDaysToRemind.Items.FindByValue(oProduct.DeskCollSMSRemindBehaviour))
    'Hide_DeskCollSMSRemind.Value = CStr(oProduct.DeskCollSMSRemind)
    'Hide_DeskCollSMSRemindBeh.Value = oProduct.DeskCollSMSRemindBehaviour

    'txtDaysToGenerateDCR.Text = CStr(oProduct.DCR)
    'cboDaysToGenerateDCR.SelectedIndex = cboDaysToGenerateDCR.Items.IndexOf(cboDaysToGenerateDCR.Items.FindByValue(oProduct.DCRBehaviour))
    'Hide_DCR.Value = CStr(oProduct.DCR)
    'Hide_DCRBeh.Value = oProduct.DCRBehaviour

    'txtDaysToGenerateRAL.Text = CStr(oProduct.ODToRAL)
    'cboDaysToGenerateRAL.SelectedIndex = cboDaysToGenerateRAL.Items.IndexOf(cboDaysToGenerateRAL.Items.FindByValue(oProduct.ODToRALBehaviour))
    'Hide_ODToRAL.Value = CStr(oProduct.ODToRAL)
    'Hide_ODToRALBeh.Value = oProduct.ODToRALBehaviour

    'txtDaysBeforeDuetoRN.Text = CStr(oProduct.DaysBeforeDueToRN)
    'cboDaysBeforeDuetoRN.SelectedIndex = cboDaysBeforeDuetoRN.Items.IndexOf(cboDaysBeforeDuetoRN.Items.FindByValue(oProduct.DaysBeforeDueToRNBehaviour))
    'Hide_DaysBeforeDueToRN.Value = CStr(oProduct.DaysBeforeDueToRN)
    'Hide_DaysBeforeDueToRNBeh.Value = oProduct.DaysBeforeDueToRNBehaviour

    'txtRALPeriod.Text = CStr(oProduct.RALPeriod)
    'cboRALPeriod.SelectedIndex = cboRALPeriod.Items.IndexOf(cboRALPeriod.Items.FindByValue(oProduct.RALPeriodBehaviour))
    'Hide_RALPeriod.Value = CStr(oProduct.RALPeriod)
    'Hide_RALPeriodBeh.Value = oProduct.RALPeriodBehaviour

    'txtMaxDaysRNPaid.Text = CStr(oProduct.MaxDaysRNPaid)
    'cboMaxDaysRNPaid.SelectedIndex = cboMaxDaysRNPaid.Items.IndexOf(cboMaxDaysRNPaid.Items.FindByValue(oProduct.MaxDaysRNPaidBehaviour))
    'Hide_MaxDaysRNPaid.Value = CStr(oProduct.MaxDaysRNPaid)
    'Hide_MaxDaysRNPaidBeh.Value = oProduct.MaxDaysRNPaidBehaviour

    'txtMaxPTPYDays.Text = CStr(oProduct.MaxPTPYDays)
    'cboMaxPTPYDays.SelectedIndex = cboMaxPTPYDays.Items.IndexOf(cboMaxPTPYDays.Items.FindByValue(oProduct.MaxPTPYDaysBehaviour))
    'Hide_MaxPTPYDays.Value = CStr(oProduct.MaxPTPYDays)
    'Hide_MaxPTPYDaysBeh.Value = oProduct.MaxPTPYDaysBehaviour

    'txtPTPYBank.Text = CStr(oProduct.PTPYBank)
    'cboPTPYBank.SelectedIndex = cboPTPYBank.Items.IndexOf(cboPTPYBank.Items.FindByValue(oProduct.PTPYBankBehaviour))
    'Hide_PTPYBank.Value = CStr(oProduct.PTPYBank)
    'Hide_PTPYBankBeh.Value = oProduct.PTPYBankBehaviour

    'txtPTPYCompany.Text = CStr(oProduct.PTPYCompany)
    'cboPTPYCompany.SelectedIndex = cboPTPYCompany.Items.IndexOf(cboPTPYCompany.Items.FindByValue(oProduct.PTPYCompanyBehaviour))
    'Hide_PTPYCompany.Value = CStr(oProduct.PTPYCompany)
    'Hide_PTPYCompanyBeh.Value = oProduct.PTPYCompanyBehaviour

    'txtPTPYSupplier.Text = CStr(oProduct.PTPYSupplier)
    'cboPTPYSupplier.SelectedIndex = cboPTPYSupplier.Items.IndexOf(cboPTPYSupplier.Items.FindByValue(oProduct.PTPYSupplierBehaviour))
    'Hide_PTPYSupplier.Value = CStr(oProduct.PTPYSupplier)
    'Hide_PTPYSupplierBeh.Value = oProduct.PTPYSupplierBehaviour

    'txtRALExtension.Text = CStr(oProduct.RALExtension)
    'cboRALExtension.SelectedIndex = cboRALExtension.Items.IndexOf(cboRALExtension.Items.FindByValue(oProduct.RALExtensionBehaviour))
    'Hide_RALExtension.Value = CStr(oProduct.RALExtension)
    'Hide_RALExtensionBeh.Value = oProduct.RALExtensionBehaviour

    'txtInventoryExpected.Text = CStr(oProduct.InventoryExpected)
    'cboInventoryExpected.SelectedIndex = cboInventoryExpected.Items.IndexOf(cboInventoryExpected.Items.FindByValue(oProduct.InventoryExpectedBehaviour))
    'Hide_InventoryExpected.Value = CStr(oProduct.InventoryExpected)
    'Hide_InventoryExpectedBeh.Value = oProduct.InventoryExpectedBehaviour

    'txtBilingCharges.Text = CStr(oProduct.BillingCharges)
    'cboBilingCharges.SelectedIndex = cboBilingCharges.Items.IndexOf(cboBilingCharges.Items.FindByValue(oProduct.BillingChargesBehaviour))
    'Hide_BillingCharges.Value = CStr(oProduct.BillingCharges)
    'Hide_BillingChargesBeh.Value = oProduct.BillingChargesBehaviour

    'txtLimitAPCash.Text = CStr(oProduct.LimitAPCash)
    'cboLimitAPCash.SelectedIndex = cboLimitAPCash.Items.IndexOf(cboLimitAPCash.Items.FindByValue(oProduct.LimitAPCashBehaviour))
    'Hide_LimitAPCash.Value = CStr(oProduct.LimitAPCash)
    'Hide_LimitAPCashBeh.Value = oProduct.LimitAPCashBehaviour

    'Dim strIsRecourse As String
    'If oProduct.IsRecourse = True Then
    '    strIsRecourse = "Yes"
    'Else
    '    strIsRecourse = "No"
    'End If
    'lblRecourse.Text = strIsRecourse

    'txtSupplier.Text = "-"
    'txtAssetCode.Text = "-"

    'txtInstallmentAmount.Text = "0"
    'cboInstallmentAmount.SelectedIndex = cboInstallmentAmount.Items.IndexOf(cboInstallmentAmount.Items.FindByValue("D"))

    'txtResidualValue.Text = "0"
    'cboResidualValue.SelectedIndex = cboResidualValue.Items.IndexOf(cboResidualValue.Items.FindByValue("D"))

    'txtValidDateFrom.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
    'txtValidDateTo.Text = Me.BusinessDate.ToString("dd/MM/yyyy")

    'hpLookupSupplier.NavigateUrl = LinkToSupplier(txtSupplier.ClientID, oProduct.AssetUsedNew, Me.BranchID)
    'hpLookupAssetCode.NavigateUrl = LinkToAssetMaster(txtAssetCode.ClientID, Me.BranchID, Me.AssetTypeID)

#End Region

#Region "Sorting"
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub
#End Region

#Region "imbReset"
    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        cboSearchBy.SelectedIndex = 0
        Me.CmdWhere = "ProductID='" & Me.ProductID & "' and BranchID='" & Me.BranchID & "'"
        BindGridEntity(Me.CmdWhere)
        txtPeriodFrom.Text = ""
        txtPeriodFrom.Text = ""
        txtSearchBy.Text = ""


    End Sub
#End Region

#Region "imbSearch"
    Private Sub imbSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonSearch.Click
        If (txtPeriodFrom.Text.Trim = "" And txtPeriodFrom.Text.Trim <> "") Then
            ShowMessage(lblMessage, "Harap isi Periode dari", True)
            Exit Sub
        End If

        If (txtPeriodFrom.Text.Trim <> "" And txtPeriodFrom.Text.Trim = "") Then
            ShowMessage(lblMessage, "Harap isi Periode Sampai", True)
            Exit Sub
        End If

        If (txtPeriodFrom.Text.Trim <> "" And txtPeriodFrom.Text.Trim <> "") Then
            If (ConvertDate2(txtPeriodFrom.Text.Trim) > ConvertDate2(txtPeriodFrom.Text.Trim)) Then
                ShowMessage(lblMessage, "Periode dari harus lebih kecil dari Period Sampai", True)
                Exit Sub
            End If
        End If

        If txtSearchBy.Text.Trim = "" And (txtPeriodFrom.Text.Trim = "" Or txtPeriodFrom.Text.Trim = "") Then
            Me.CmdWhere = "ProductID='" & Me.ProductID & "' and BranchID='" & Me.BranchID & "'"
        ElseIf txtSearchBy.Text.Trim <> "" And (txtPeriodFrom.Text.Trim = "" Or txtPeriodFrom.Text.Trim = "") Then
            Me.CmdWhere = "ProductID= '" & Me.ProductID & "' and BranchID='" & Me.BranchID & "' and " & cboSearchBy.SelectedItem.Value.Trim & " LIKE '%" & txtSearchBy.Text & "%'"
        ElseIf txtSearchBy.Text.Trim = "" And (txtPeriodFrom.Text.Trim <> "" And txtPeriodFrom.Text.Trim <> "") Then
            Me.CmdWhere = "ProductID='" & Me.ProductID & "' and BranchID='" & Me.BranchID & "' and StartDate='" & ConvertDate2(txtPeriodFrom.Text) & "' and EndDate='" & ConvertDate2(txtPeriodFrom.Text) & "'"
        ElseIf txtSearchBy.Text.Trim <> "" And (txtPeriodFrom.Text.Trim <> "" And txtPeriodFrom.Text.Trim <> "") Then
            Me.CmdWhere = "ProductID= '" & Me.ProductID & "' and BranchID='" & Me.BranchID & "' and " & cboSearchBy.SelectedItem.Value.Trim & " LIKE '%" & txtSearchBy.Text & "%' and StartDate='" & ConvertDate2(txtPeriodFrom.Text) & "' and EndDate='" & ConvertDate2(txtPeriodFrom.Text) & "'"
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub
#End Region

#Region "ButtonPrint"
    Private Sub ButtonPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonPrint.Click
        If CheckFeature(Me.Loginid, Me.FormID, "PRINT", Me.AppId) Then
            SendCookies()
            Response.Redirect("Report/ProductOfferingBranchRpt.aspx")
        End If
    End Sub
#End Region

#Region "SendCookies"
    Sub SendCookies()
        If txtSearchBy.Text.Trim = "" Then
            Me.CmdWhere = "ProductID='" & Me.ProductID & "' and BranchID='" & Me.BranchID & "'"
        Else
            Me.CmdWhere = "ProductID= '" & Me.ProductID & "' and BranchID='" & Me.BranchID & "' and " & cboSearchBy.SelectedItem.Value.Trim & "='" & txtSearchBy.Text & "'"
        End If

        Dim cookie As HttpCookie = Request.Cookies("ProductOfferingBranch")
        If Not cookie Is Nothing Then
            cookie.Values("PageFrom") = "ProductOfferingBranch"
            cookie.Values("CmdWhere") = Me.CmdWhere
            cookie.Values("ProductID") = Me.ProductID
            cookie.Values("BranchID") = Me.BranchID
            cookie.Values("Product_desc") = Me.Description
            cookie.Values("Branch_desc") = Me.BranchName
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie("ProductOfferingBranch")
            cookieNew.Values.Add("PageFrom", "ProductOfferingBranch")
            cookieNew.Values.Add("CmdWhere", Me.CmdWhere)
            cookieNew.Values.Add("ProductID", Me.ProductID)
            cookieNew.Values.Add("BranchID", Me.BranchID)
            cookieNew.Values.Add("Product_desc", Me.Description)
            cookieNew.Values.Add("Branch_desc", Me.BranchName)
            Response.AppendCookie(cookieNew)
        End If
    End Sub
#End Region

#Region "ButtonBack"
    Private Sub ButtonBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonBack.Click
        Response.Redirect("ProductBranch.aspx")
    End Sub
#End Region

#Region "imgCancel"
    Private Sub imgCancel_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        pnlList.Visible = True
        pnlAddEdit.Visible = False

        Me.CmdWhere = "ProductID='" & Me.ProductID & "' and BranchID='" & Me.BranchID & "'"
        BindGridEntity(Me.CmdWhere)
    End Sub
#End Region
#Region "Fill CBO"
    Private Sub FillCboInsuranceRateCard()
        Dim dtCard As DataTable

        With oCustomclassInsRateCard
            .strConnection = GetConnectionString()
            .WhereCond = "IsActive = 1"
            .CurrentPage = 1
            .PageSize = 1000
            .SortBy = "RateCardID"
            .SpName = "spShadowNewInsuranceRateCardHPaging"
        End With
        oCustomclassInsRateCard = oControllerInsRateCard.GetGeneralPaging(oCustomclassInsRateCard)

        If Not oCustomclassInsRateCard Is Nothing Then
            dtCard = oCustomclassInsRateCard.ListData
            recordCount = oCustomclassInsRateCard.TotalRecords
        Else
            recordCount = 0
        End If

        'cboInsuranceRateCard.DataSource = dtCard.DefaultView

        'With cboInsuranceRateCard
        '    .DataValueField = "RateCardID"
        '    .DataTextField = "RateCardDesc"
        '    .DataBind()
        '    .Items.Insert(0, "Select One")
        '    .Items(0).Value = "0"
        '    '.Items.Insert(1, "ALL")
        '    '.Items(1).Value = "ALL"
        'End With

    End Sub
#End Region
    Private Sub bindComboAPType()

        Dim customClassx As New GeneralPagingController
        Dim customClass As New Parameter.GeneralPaging

        With customClass
            .strConnection = GetConnectionString()
            .WhereCond = ""
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = ""
            .SpName = "spTblProductPaket"
        End With
        customClass = customClassx.GetGeneralPaging(customClass)
        Dim dt As DataTable
        dt = customClass.ListData
        With cboPaket
            .DataTextField = "Deskripsi"
            .DataValueField = "PaketProgram"
            .DataSource = dt
            .DataBind()
        End With

        cboPaket.SelectedValue = "RGL"

    End Sub
End Class