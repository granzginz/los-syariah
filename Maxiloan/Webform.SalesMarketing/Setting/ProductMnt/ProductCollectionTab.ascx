﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ProductCollectionTab.ascx.vb" Inherits="Maxiloan.Webform.SalesMarketing.ProductCollectionTab" %>

<div class="form_box_title">
    <div class="form_single"> <h4>COLLECTION </h4> </div>
</div>
  <div class="form_box">
	    <div class="form_single">
            <label>Buat Surat Peringatan 1</label>
            <asp:Label ID="lblIsSPAutomatic" runat="server"></asp:Label> 
            <asp:DropDownList ID="cboIsSPAutomatic" runat="server"  onchange="cboIsSpAutomatic('Tabs_pnlTabCollection_CollactionTab_cboIsSPAutomatic','Tabs_pnlTabCollection_CollactionTab_cboIsSPAutomaticBehaviour','Tabs_pnlTabCollection_CollactionTab_txtLengthSPProcess');"> <asp:ListItem Value="0" Selected="True">No</asp:ListItem> <asp:ListItem Value="1">Yes</asp:ListItem> </asp:DropDownList>
            <asp:DropDownList ID="cboIsSPAutomaticBehaviour" runat="server" > <asp:ListItem Value="D" Selected="True">Default</asp:ListItem> <asp:ListItem Value="L">Locked</asp:ListItem> </asp:DropDownList>
            <asp:Label ID="Label1" runat="server">Jangka Waktu Proses SP&nbsp;&nbsp;&nbsp;</asp:Label> 
            <asp:TextBox ID="txtLengthSPProcess" runat="server" Enabled="False"  MaxLength="4">0</asp:TextBox><asp:Label ID="Label2" runat="server">days</asp:Label>  
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator33" runat="server" ControlToValidate="txtLengthSPProcess" CssClass="validator_general" ErrorMessage="Jangka Waktu Proses SP Salah" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="Rangevalidator27" runat="server" ControlToValidate="txtLengthSPProcess" CssClass="validator_general" Type="integer" MinimumValue="0" ErrorMessage="Jangka Waktu Proses SP salah" Display="Dynamic" MaximumValue="9999"></asp:RangeValidator>
          
	    </div>
    </div>
    <div class="form_box">
	    <div class="form_single">
            <label>Buat Surat Peringatan 2</label>
            <asp:Label ID="lblIsSP1Automatic" runat="server"></asp:Label>  
            <asp:DropDownList ID="cboIsSP1Automatic" runat="server"  onchange="cboIsSpAutomatic('Tabs_pnlTabCollection_CollactionTab_cboIsSP1Automatic','Tabs_pnlTabCollection_CollactionTab_cboIsSP1AutomaticBehaviour','Tabs_pnlTabCollection_CollactionTab_txtLengthSP1Process');"> <asp:ListItem Value="0" Selected="True">No</asp:ListItem> <asp:ListItem Value="1">Yes</asp:ListItem> </asp:DropDownList> 
            <asp:DropDownList ID="cboIsSP1AutomaticBehaviour" runat="server" > <asp:ListItem Value="D" Selected="True">Default</asp:ListItem> <asp:ListItem Value="L">Locked</asp:ListItem> </asp:DropDownList>
            <asp:Label ID="Label3" runat="server">Jangka Waktu Proses SP 1</asp:Label> 
            <asp:TextBox ID="txtLengthSP1Process" runat="server" Enabled="False"  MaxLength="4">0</asp:TextBox><asp:Label ID="Label4" runat="server">days</asp:Label>
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator34" runat="server" ControlToValidate="txtLengthSP1Process" CssClass="validator_general" ErrorMessage="Jangka Waktu Proses SP 1 Salah" Display="Dynamic"></asp:RequiredFieldValidator> <asp:RangeValidator ID="Rangevalidator28" runat="server" ControlToValidate="txtLengthSP1Process" CssClass="validator_general" Type="integer" MinimumValue="0" ErrorMessage="Jangka Waktu Proses SP 1 Salah" Display="Dynamic" MaximumValue="9999"></asp:RangeValidator>
           
	    </div>
    </div>
    <div class="form_box">
	    <div class="form_single">
            <label>Buat Surat Peringatan 3</label>
            <asp:Label ID="lblIsSP2Automatic" runat="server"></asp:Label> 
             <asp:DropDownList ID="cboIsSP2Automatic" runat="server"  onchange="cboIsSpAutomatic('Tabs_pnlTabCollection_CollactionTab_cboIsSP2Automatic','Tabs_pnlTabCollection_CollactionTab_cboIsSP2AutomaticBehaviour','Tabs_pnlTabCollection_CollactionTab_txtLengthSP2Process');"> <asp:ListItem Value="0" Selected="True">No</asp:ListItem> <asp:ListItem Value="1">Yes</asp:ListItem> </asp:DropDownList> 
             <asp:DropDownList ID="cboIsSP2AutomaticBehaviour" runat="server" > <asp:ListItem Value="D" Selected="True">Default</asp:ListItem> <asp:ListItem Value="L">Locked</asp:ListItem> </asp:DropDownList><asp:Label ID="Label5" runat="server">Jangka Waktu proses SP 2</asp:Label>
             <asp:TextBox ID="txtLengthSP2Process" runat="server" Enabled="False"  MaxLength="4">0</asp:TextBox><asp:Label ID="Label6" runat="server">days</asp:Label>
             <asp:RequiredFieldValidator ID="Requiredfieldvalidator35" runat="server" ControlToValidate="txtLengthSP2Process" CssClass="validator_general" ErrorMessage="Jangka Waktu Proses SP 2 Salah" Display="Dynamic"></asp:RequiredFieldValidator>
             <asp:RangeValidator ID="Rangevalidator29" runat="server" ControlToValidate="txtLengthSP2Process" Type="integer" MinimumValue="0" ErrorMessage="Jangka Waktu Proses SP 2 salah" CssClass="validator_general" Display="Dynamic" MaximumValue="9999"></asp:RangeValidator>
             
	    </div>
    </div>
    <div class="form_box">
	    <div class="form_single">
            <label>Jangka Waktu proses Dokumen Utama</label>
            <asp:Label ID="lblLMDProcessed" runat="server"></asp:Label> 
            <asp:TextBox ID="txtLMDProcessed" runat="server"  MaxLength="4">0</asp:TextBox><asp:Label ID="Label7" runat="server">days</asp:Label>
            <asp:DropDownList ID="cboLMDProcessed" runat="server" /> 
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator36" runat="server" ControlToValidate="txtLMDProcessed" CssClass="validator_general" ErrorMessage="Jangka Waktu proses Dokumen Utama salah" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="Rangevalidator30" runat="server" ControlToValidate="txtLMDProcessed" CssClass="validator_general" Type="integer" MinimumValue="0" ErrorMessage="Jangka Waktu proses Dokumen Utama Salah" Display="Dynamic" MaximumValue="9999"></asp:RangeValidator>
	    </div>
    </div>
    <div class="form_box">
	    <div class="form_single">
            <label>Jangka Waktu Dokumen Utama diambil</label>
            <asp:Label ID="lblLMDTaken" runat="server"></asp:Label >
             <asp:TextBox ID="txtLMDTaken" runat="server"  MaxLength="4">0</asp:TextBox><asp:Label ID="Label8" runat="server">days</asp:Label>
             <asp:DropDownList ID="cboLMDTaken" runat="server" /> 
             <asp:RequiredFieldValidator ID="Requiredfieldvalidator37" runat="server" ControlToValidate="txtLMDTaken" CssClass="validator_general" ErrorMessage="Jangka Waktu Dokumen Utama diambil salah" Display="Dynamic"></asp:RequiredFieldValidator>
             <asp:RangeValidator ID="Rangevalidator31" runat="server" ControlToValidate="txtLMDTaken" Type="integer" MinimumValue="0" ErrorMessage="Jangka Waktu Dokumen Utama diambil salah" CssClass="validator_general" Display="Dynamic" MaximumValue="9999"></asp:RangeValidator>
	    </div>
    </div>
    <div class="form_box">
	    <div class="form_single">
            <label>Grace Period Keterlambatan</label>
            <asp:Label ID="lblGPLC" runat="server"></asp:Label> 
              <asp:TextBox ID="txtGPLC" runat="server"  MaxLength="4">0</asp:TextBox><asp:Label ID="Label9" runat="server">days</asp:Label>
              <asp:DropDownList ID="cboGPLC" runat="server" /> 
              <asp:RequiredFieldValidator ID="Requiredfieldvalidator38" runat="server" ControlToValidate="txtGPLC" CssClass="validator_general" ErrorMessage="Grace Period Denda Keterlambatan Salah" Display="Dynamic"></asp:RequiredFieldValidator>
              <asp:RangeValidator ID="Rangevalidator32" runat="server" ControlToValidate="txtGPLC" Type="integer" MinimumValue="0" ErrorMessage="Grace Period Denda Keterlambatan Salah" CssClass="validator_general" Display="Dynamic" MaximumValue="9999"></asp:RangeValidator>
	    </div>
    </div> 
    <div class="form_box">
	    <div class="form_single">
            <label>Jumlah hari mengingatkan angsuran</label>
            <asp:Label ID="lblDaystoremindInstallment" runat="server"></asp:Label> 
             <asp:TextBox ID="txtDaystoremindInstallment" runat="server"  MaxLength="4">0</asp:TextBox><asp:Label ID="Label10" runat="server">days</asp:Label>
             <asp:DropDownList ID="cboDaystoremindInstallment" runat="server" /> 
             <asp:RequiredFieldValidator ID="Requiredfieldvalidator40" runat="server" ControlToValidate="txtDaystoremindInstallment" CssClass="validator_general" ErrorMessage="Jumlah hari mengingatkan angsuran salah" Display="Dynamic"></asp:RequiredFieldValidator>
             <asp:RangeValidator ID="Rangevalidator34" runat="server" ControlToValidate="txtDaystoremindInstallment" CssClass="validator_general" Type="integer" MinimumValue="0" ErrorMessage="Jumlah hari mengingatkan angsuran salah" Display="Dynamic" MaximumValue="9999"></asp:RangeValidator>
	    </div>
    </div>
    <div class="form_box">
	    <div class="form_single">
            <label>Jumlah hari Overdue untuk Deskcoll</label>
            <asp:Label ID="lblDaysOverduetocall" runat="server"></asp:Label>
              <asp:TextBox ID="txtDaysOverduetocall" runat="server"  MaxLength="4">0</asp:TextBox><asp:Label ID="Label11" runat="server">days</asp:Label>
              <asp:DropDownList ID="cboDaysOverduetocall" runat="server" /> 
              <asp:RequiredFieldValidator ID="Requiredfieldvalidator41" runat="server" ControlToValidate="txtDaysOverduetocall" CssClass="validator_general" ErrorMessage="Jumlah hari Overdue untuk DeskColl salah" Display="Dynamic"></asp:RequiredFieldValidator>
              <asp:RangeValidator ID="Rangevalidator35" runat="server" ControlToValidate="txtDaysOverduetocall" CssClass="validator_general" Type="integer" MinimumValue="0" ErrorMessage="Jumlah hari Overdue untuk DeskColl salah" Display="Dynamic" MaximumValue="9999"></asp:RangeValidator>
	    </div>
    </div>
    <div class="form_box">
	    <div class="form_single">
            <label>Overdue sebelumnya untuk mengingatkan</label>
            <asp:Label ID="lblPreviousOverduetoremind" runat="server"></asp:Label> 
            <asp:TextBox ID="txtPreviousOverduetoremind" runat="server"  MaxLength="4">0</asp:TextBox><asp:Label ID="Label12" runat="server">days</asp:Label>
            <asp:DropDownList ID="cboPreviousOverduetoremind" runat="server" /> 
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator42" runat="server" ControlToValidate="txtPreviousOverduetoremind" CssClass="validator_general" ErrorMessage="Overdue sebelumnya untuk mengingatkan Salah" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="Rangevalidator36" runat="server" ControlToValidate="txtPreviousOverduetoremind" CssClass="validator_general" Type="integer" MinimumValue="0" ErrorMessage="Overdue sebelumnya untuk mengingatkan Salah" Display="Dynamic" MaximumValue="9999"></asp:RangeValidator>
	    </div>
    </div>
    <div class="form_box">
	    <div class="form_single">
            <label>Permintaan PDC untuk telepon</label>
            <asp:Label ID="lblPDCRequesttocall" runat="server"></asp:Label> 
             <asp:TextBox ID="txtPDCRequesttocall" runat="server"  MaxLength="4">0</asp:TextBox><asp:Label ID="Label13" runat="server">days</asp:Label>
             <asp:DropDownList ID="cboPDCRequesttocall" runat="server" /> 
             <asp:RequiredFieldValidator ID="Requiredfieldvalidator43" runat="server" ControlToValidate="txtPDCRequesttocall" CssClass="validator_general" ErrorMessage="Permintaan PDC untuk telepon Salah" Display="Dynamic"></asp:RequiredFieldValidator>
             <asp:RangeValidator ID="Rangevalidator37" runat="server" ControlToValidate="txtPDCRequesttocall" CssClass="validator_general" Type="integer" MinimumValue="0" ErrorMessage="Permintaan PDC untuk telepon Salah" Display="Dynamic" MaximumValue="9999"></asp:RangeValidator>
	    </div>
    </div>
    <div class="form_box">
	    <div class="form_single">
            <label>Jumlah hari reminder dgn SMS</label>
            <asp:Label ID="lblDaysToRemind" runat="server"></asp:Label> 
             <asp:TextBox ID="txtDaysToRemind" runat="server"  MaxLength="4">0</asp:TextBox><asp:Label ID="Label14" runat="server">days</asp:Label>
             <asp:DropDownList ID="cboDaysToRemind" runat="server" /> 
             <asp:RequiredFieldValidator ID="Requiredfieldvalidator44" runat="server" ControlToValidate="txtDaysToRemind" CssClass="validator_general" ErrorMessage="Jumlah hari mengingatkan dgn SMS salah" Display="Dynamic"></asp:RequiredFieldValidator>
             <asp:RangeValidator ID="Rangevalidator38" runat="server" ControlToValidate="txtDaysToRemind" Type="integer" MinimumValue="0" ErrorMessage="Jumlah hari mengingatkan dgn SMS Salah" CssClass="validator_general" Display="Dynamic" MaximumValue="9999"></asp:RangeValidator>
	    </div>
    </div>
    <div class="form_box">
	    <div class="form_single">
            <label>Jumlah hari reminder dgn SMS</label>
            <asp:Label ID="lblDaysToRemind2" runat="server"></asp:Label> 
             <asp:TextBox ID="txtDaysToRemind2" runat="server"  MaxLength="4">0</asp:TextBox><asp:Label ID="Label27" runat="server">days</asp:Label>
             <asp:DropDownList ID="cboDaysToRemind2" runat="server" /> 
             <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" ControlToValidate="txtDaysToRemind2" CssClass="validator_general" ErrorMessage="Jumlah hari mengingatkan dgn SMS salah" Display="Dynamic"></asp:RequiredFieldValidator>
             <asp:RangeValidator ID="Rangevalidator1" runat="server" ControlToValidate="txtDaysToRemind2" Type="integer" MinimumValue="0" ErrorMessage="Jumlah hari mengingatkan dgn SMS Salah" CssClass="validator_general" Display="Dynamic" MaximumValue="9999"></asp:RangeValidator>
	    </div>
    </div>
    <div class="form_box">
	    <div class="form_single">
            <label>Jumlah hari reminder dgn SMS</label>
            <asp:Label ID="lblDaysToRemind3" runat="server"></asp:Label> 
             <asp:TextBox ID="txtDaysToRemind3" runat="server"  MaxLength="4">0</asp:TextBox><asp:Label ID="Label29" runat="server">days</asp:Label>
             <asp:DropDownList ID="cboDaysToRemind3" runat="server" /> 
             <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" ControlToValidate="txtDaysToRemind3" CssClass="validator_general" ErrorMessage="Jumlah hari mengingatkan dgn SMS salah" Display="Dynamic"></asp:RequiredFieldValidator>
             <asp:RangeValidator ID="Rangevalidator2" runat="server" ControlToValidate="txtDaysToRemind3" Type="integer" MinimumValue="0" ErrorMessage="Jumlah hari mengingatkan dgn SMS Salah" CssClass="validator_general" Display="Dynamic" MaximumValue="9999"></asp:RangeValidator>
	    </div>
    </div>
    <div class="form_box">
	    <div class="form_single">
            <label>Jumlah hari buat DCR</label>
            <asp:Label ID="lblDaysToGenerateDCR" runat="server"></asp:Label> 
             <asp:TextBox ID="txtDaysToGenerateDCR" runat="server"  MaxLength="4">0</asp:TextBox><asp:Label ID="Label15" runat="server">days</asp:Label>
             <asp:DropDownList ID="cboDaysToGenerateDCR" runat="server" /> 
             <asp:RequiredFieldValidator ID="Requiredfieldvalidator45" runat="server" ControlToValidate="txtDaysToGenerateDCR" CssClass="validator_general" ErrorMessage="Jumlah hari buat DCR Salah" Display="Dynamic"></asp:RequiredFieldValidator>
             <asp:RangeValidator ID="Rangevalidator39" runat="server" ControlToValidate="txtDaysToGenerateDCR" CssClass="validator_general" Type="integer" MinimumValue="0" ErrorMessage="Jumlah hari buat DCR salah" Display="Dynamic" MaximumValue="9999"></asp:RangeValidator>
	    </div>
    </div>
    <div class="form_box">
	    <div class="form_single">
            <label>Jumlah hari sebelum JT buat Kwitansi</label>
            <asp:Label ID="lblDaysBeforeDuetoRN" runat="server"></asp:Label> 
              <asp:TextBox ID="txtDaysBeforeDuetoRN" runat="server"  MaxLength="4">0</asp:TextBox><asp:Label ID="Label16" runat="server">days</asp:Label>
              <asp:DropDownList ID="cboDaysBeforeDuetoRN" runat="server" /> 
              <asp:RequiredFieldValidator ID="Requiredfieldvalidator46" runat="server" ControlToValidate="txtDaysBeforeDuetoRN" CssClass="validator_general" ErrorMessage="Jumlah hari sebelum JT buat Kwitansi Salah" Display="Dynamic"></asp:RequiredFieldValidator>
              <asp:RangeValidator ID="Rangevalidator40" runat="server" ControlToValidate="txtDaysBeforeDuetoRN" CssClass="validator_general" Type="integer" MinimumValue="0" ErrorMessage="Jumlah hari sebelum JT buat Kwitansi Salah" Display="Dynamic" MaximumValue="9999"></asp:RangeValidator>
	    </div>
    </div>
    <div class="form_box">
	    <div class="form_single">
            <label>Jumlah hari buat SKT (Surat Kuasa Tarik)</label>
            <asp:Label ID="lblDaysToGenerateRAL" runat="server"></asp:Label>  
            <asp:TextBox ID="txtDaysToGenerateRAL" runat="server"  MaxLength="4">0</asp:TextBox><asp:Label ID="Label17" runat="server">days</asp:Label>
            <asp:DropDownList ID="cboDaysToGenerateRAL" runat="server" /> 
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator47" runat="server" ControlToValidate="txtDaysToGenerateRAL" CssClass="validator_general" ErrorMessage="Jumlah hari buat SKT salah" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="Rangevalidator41" runat="server" ControlToValidate="txtDaysToGenerateRAL" CssClass="validator_general" Type="integer" MinimumValue="0" ErrorMessage="Jumlah hari buat SKT Salah" Display="Dynamic" MaximumValue="9999"></asp:RangeValidator>
	    </div>
    </div>
    <div class="form_box">
	    <div class="form_single">
            <label>Jangka Waktu SKT</label>
            <asp:Label ID="lblRALPeriod" runat="server"></asp:Label> 
             <asp:TextBox ID="txtRALPeriod" runat="server"  MaxLength="4">0</asp:TextBox><asp:Label ID="Label18" runat="server">days</asp:Label>
             <asp:DropDownList ID="cboRALPeriod" runat="server" /> 
             <asp:RequiredFieldValidator ID="Requiredfieldvalidator48" runat="server" ControlToValidate="txtRALPeriod" CssClass="validator_general" ErrorMessage="Jangka Waktu SKT Salah" Display="Dynamic"></asp:RequiredFieldValidator>
             <asp:RangeValidator ID="Rangevalidator42" runat="server" ControlToValidate="txtRALPeriod" CssClass="validator_general" Type="integer" MinimumValue="0" ErrorMessage="Jangka Waktu SKT Salah" Display="Dynamic" MaximumValue="9999"></asp:RangeValidator>
	    </div>
    </div>
    <div class="form_box">
	    <div class="form_single">
            <label>Jumlah hari Maximum Kwitansi dibayar</label>
            <asp:Label ID="lblMaxDaysRNPaid" runat="server"></asp:Label> 
              <asp:TextBox ID="txtMaxDaysRNPaid" runat="server"  MaxLength="4">0</asp:TextBox><asp:Label ID="Label19" runat="server">days</asp:Label>
              <asp:DropDownList ID="cboMaxDaysRNPaid" runat="server" /> 
              <asp:RequiredFieldValidator ID="Requiredfieldvalidator49" runat="server" ControlToValidate="txtMaxDaysRNPaid" CssClass="validator_general" ErrorMessage="Jumlah hari Maximum Kwitansi dibayar Salah" Display="Dynamic"></asp:RequiredFieldValidator>
              <asp:RangeValidator ID="Rangevalidator43" runat="server" ControlToValidate="txtMaxDaysRNPaid" Type="integer" MinimumValue="0" ErrorMessage="Jumlah hari Maximum Kwitansi dibayar Salah" CssClass="validator_general" Display="Dynamic" MaximumValue="9999"></asp:RangeValidator>
	    </div>
    </div>
    <div class="form_box">
	    <div class="form_single">
            <label>Jumlah hari Maximum janji Bayar</label>
            <asp:Label ID="lblMaxPTPYDays" runat="server"></asp:Label> 
             <asp:TextBox ID="txtMaxPTPYDays" runat="server"  MaxLength="4">0</asp:TextBox><asp:Label ID="Label20" runat="server">days</asp:Label>
             <asp:DropDownList ID="cboMaxPTPYDays" runat="server" /> 
             <asp:RequiredFieldValidator ID="Requiredfieldvalidator50" runat="server" ControlToValidate="txtMaxPTPYDays" CssClass="validator_general" ErrorMessage="Jumlah hari Maximum janji Bayar Salah" Display="Dynamic"></asp:RequiredFieldValidator>
             <asp:RangeValidator ID="Rangevalidator44" runat="server" ControlToValidate="txtMaxPTPYDays" CssClass="validator_general" Type="integer" MinimumValue="0" ErrorMessage="Jumlah hari Maximum janji Bayar Salah" Display="Dynamic" MaximumValue="9999"></asp:RangeValidator>
	    </div>
    </div>
    <div class="form_box">
	    <div class="form_single">
            <label>Janji Bayar ke Bank</label>
            <asp:Label ID="lblPTPYBank" runat="server"></asp:Label> 
             <asp:TextBox ID="txtPTPYBank" runat="server"  MaxLength="4">0</asp:TextBox><asp:Label ID="Label21" runat="server">days</asp:Label>
             <asp:DropDownList ID="cboPTPYBank" runat="server" /> 
             <asp:RequiredFieldValidator ID="Requiredfieldvalidator51" runat="server" ControlToValidate="txtPTPYBank" CssClass="validator_general" ErrorMessage="Janji Bayar ke Bank Salah" Display="Dynamic"></asp:RequiredFieldValidator>
             <asp:RangeValidator ID="Rangevalidator45" runat="server" ControlToValidate="txtPTPYBank" CssClass="validator_general" Type="integer" MinimumValue="0" ErrorMessage="Janji Bayar ke Bank Salah" Display="Dynamic" MaximumValue="9999"></asp:RangeValidator>
	    </div>
    </div>
    <div class="form_box">
	    <div class="form_single">
            <label>Janji bayar ke Perusahaan</label>
            <asp:Label ID="lblPTPYCompany" runat="server"></asp:Label> 
                <asp:TextBox ID="txtPTPYCompany" runat="server"  MaxLength="4">0</asp:TextBox><asp:Label ID="Label22" runat="server">days</asp:Label>
                <asp:DropDownList ID="cboPTPYCompany" runat="server" /> 
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator52" runat="server" ControlToValidate="txtPTPYCompany" CssClass="validator_general" ErrorMessage="Janji bayar ke Perusahaan Salah" Display="Dynamic"></asp:RequiredFieldValidator>
                <asp:RangeValidator ID="Rangevalidator46" runat="server" ControlToValidate="txtPTPYCompany" CssClass="validator_general" Type="integer" MinimumValue="0" ErrorMessage="Janji bayar ke Perusahaan Salah" Display="Dynamic" MaximumValue="9999"></asp:RangeValidator>
	    </div>
    </div>
    <div class="form_box">
	    <div class="form_single">
            <label>Janji bayar ke Supplier</label>
            <asp:Label ID="lblPTPYSupplier" runat="server"></asp:Label> 
            <asp:TextBox ID="txtPTPYSupplier" runat="server"  MaxLength="4">0</asp:TextBox><asp:Label ID="Label23" runat="server">days</asp:Label>
            <asp:DropDownList ID="cboPTPYSupplier" runat="server" /> 
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator53" runat="server" ControlToValidate="txtPTPYSupplier" CssClass="validator_general" ErrorMessage="Janji bayar ke Supplier Salah" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="Rangevalidator47" runat="server" ControlToValidate="txtPTPYSupplier" CssClass="validator_general" Type="integer" MinimumValue="0" ErrorMessage="Janji bayar ke Supplier Salah" Display="Dynamic" MaximumValue="9999"></asp:RangeValidator>
	    </div>  
    </div>
     <div class="form_box">
	    <div class="form_single">
            <label>Freq Perpanjangan SKT</label>
            <asp:Label ID="lblPerpanjanganSKT" runat="server"></asp:Label> 
            <asp:TextBox ID="TxtPerpanjanganSKT" runat="server" text="1" MaxLength="4"/><asp:Label ID="Label28" runat="server">Kali</asp:Label>
            <asp:DropDownList ID="cboPerpanjanganSKT" runat="server" /> 
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator3" runat="server" ControlToValidate="TxtPerpanjanganSKT" CssClass="validator_general" ErrorMessage="Freq Perpanjangan SKT Salah" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="Rangevalidator3" runat="server" ControlToValidate="TxtPerpanjanganSKT" CssClass="validator_general" Type="integer" MinimumValue="0" ErrorMessage="Freq Perpanjangan SKT Salah" Display="Dynamic" MaximumValue="9999"></asp:RangeValidator>
	    </div>  
    </div>

    <div class="form_box">
	    <div class="form_single">
            <label>Perpanjangan SKT</label>
            <asp:Label ID="lblRALExtension" runat="server"></asp:Label> 
             <asp:TextBox ID="txtRALExtension" runat="server" text="5" MaxLength="4" /><asp:Label ID="Label24" runat="server">days</asp:Label>
             <asp:DropDownList ID="cboRALExtension" runat="server" /> 
             <asp:RequiredFieldValidator ID="Requiredfieldvalidator54" runat="server" ControlToValidate="txtRALExtension" CssClass="validator_general" ErrorMessage="Perpanjangan SKT Salah" Display="Dynamic"></asp:RequiredFieldValidator>
             <asp:RangeValidator ID="Rangevalidator48" runat="server" ControlToValidate="txtRALExtension" CssClass="validator_general" Type="integer" MinimumValue="0" ErrorMessage="Perpanjangan SKT Salah" Display="Dynamic" MaximumValue="9999"></asp:RangeValidator>
	    </div>
    </div>
    <div class="form_box">
	    <div class="form_single">
            <label>Jangka Waktu jadi Inventory</label>
            <asp:Label ID="lblInventoryExpected" runat="server"></asp:Label> 
             <asp:TextBox ID="txtInventoryExpected" runat="server"  MaxLength="4">0</asp:TextBox><asp:Label ID="Label25" runat="server">days</asp:Label>
             <asp:DropDownList ID="cboInventoryExpected" runat="server" /> 
             <asp:RequiredFieldValidator ID="Requiredfieldvalidator55" runat="server" ControlToValidate="txtInventoryExpected" CssClass="validator_general" ErrorMessage="Jangka Waktu jadi Inventory Salah" Display="Dynamic"></asp:RequiredFieldValidator>
             <asp:RangeValidator ID="Rangevalidator49" runat="server" ControlToValidate="txtInventoryExpected" Type="integer" MinimumValue="0" ErrorMessage="Jangka Waktu jadi Inventory Salah" CssClass="validator_general" Display="Dynamic" MaximumValue="9999"></asp:RangeValidator>
	    </div>
    </div>
   
   