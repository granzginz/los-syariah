﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ProductFinanceTab.ascx.vb" Inherits="Maxiloan.Webform.SalesMarketing.ProductFinanceTab" %>
<div class="form_box_title">
    <div class="form_single"> <h4>FINANCE </h4> </div>
</div>
 
    <div class="form_box">
	    <div class="form_single">
            <label>Limit Pembayaran di Cabang</label>
            <asp:Label ID="lblLimitAPCash" runat="server"></asp:Label> 
             <asp:TextBox ID="txtLimitAPCash" runat="server"  MaxLength="15" onblur="extractNumber(this,2,true);blockInvalid(this);this.value=numberFormat(this.value);this.value=blankToZero(this.value);"
    onkeyup="extractNumber(this,2,true);" onkeypress="return blockNonNumbers(this, event, true, true);"
    onfocus="this.value=resetNumber(this.value);" CssClass="numberAlign" OnChange="javascript:void(0)" autocomplete="off">0</asp:TextBox>
             <asp:DropDownList ID="cboLimitAPCash" runat="server" /> 
             <asp:RequiredFieldValidator ID="Requiredfieldvalidator57" runat="server" ControlToValidate="txtLimitAPCash" CssClass="validator_general" ErrorMessage="Limit Pembayaran di Cabang Salah" Display="Dynamic"></asp:RequiredFieldValidator>
             <asp:RangeValidator ID="Rangevalidator51" runat="server" ControlToValidate="txtLimitAPCash" CssClass="validator_general" Type="Currency" MinimumValue="0" ErrorMessage="Limit Pembayaran di Cabang Salah" Display="Dynamic" MaximumValue="999999999999"></asp:RangeValidator>
	    </div>
    </div>
    <div class="form_box">
	    <div class="form_single">
            <label>Recourse</label>
            <asp:Label ID="lblIsRecourse" runat="server"></asp:Label>
            <asp:DropDownList ID="cboIsRecourse" runat="server" > <asp:ListItem Value="0" Selected="True">No</asp:ListItem> <asp:ListItem Value="1">Yes</asp:ListItem> </asp:DropDownList>
	    </div>
    </div>
    <div class="form_box">
	    <div class="form_single">
            <label>Aktif</label>
            <asp:Label ID="lblIsActive" runat="server"></asp:Label>
            <asp:RadioButtonList ID="rboIsActive" runat="server" style="display:inline;" class="opt_single" RepeatDirection="Horizontal" Height="24px"> <asp:ListItem Value="1" Selected="True">Yes</asp:ListItem> <asp:ListItem Value="0">No</asp:ListItem> </asp:RadioButtonList>
	    </div>
    </div>
    <div class="form_box">
	    <div class="form_single">
            <label>Diskon Asuransi</label>
            <asp:Label ID="lblInsuranceDiscPercentage" runat="server"></asp:Label>
            <asp:TextBox ID="txtInsuranceDiscPercentage" runat="server"  MaxLength="9" onblur="extractNumber(this,2,true);blockInvalid(this);this.value=numberFormat(this.value);this.value=blankToZero(this.value);"
    onkeyup="extractNumber(this,2,true);" onkeypress="return blockNonNumbers(this, event, true, true);"
    onfocus="this.value=resetNumber(this.value);" CssClass="numberAlign" OnChange="javascript:void(0)" autocomplete="off">0</asp:TextBox><asp:Label ID="Label1" runat="server">%</asp:Label> 
            <asp:DropDownList ID="cboInsuranceDiscPercentageBehaviour" runat="server" /> 
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator62" runat="server" ControlToValidate="txtInsuranceDiscPercentage" CssClass="validator_general" ErrorMessage="Diskon Asuransi Salah" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="Rangevalidator52" runat="server" ControlToValidate="txtInsuranceDiscPercentage" Type="double" MinimumValue="0" ErrorMessage="Diskon Asuransi Salah" CssClass="validator_general" Display="Dynamic" MaximumValue="100"></asp:RangeValidator>
	    </div>
    </div> 