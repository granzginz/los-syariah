﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ProductFinanceBRTab.ascx.vb" Inherits="Maxiloan.Webform.SalesMarketing.ProductFinanceBRTab" %>

<div>
    <div class="form_box_title"> <div class="form_single"> <h4> FINANCE </h4> </div> </div>
    <div class="form_box_header">
        <div class="form_single"> <h5> Limit Pembayaran di Cabang </h5> </div>
    </div>
    
<div class="form_box">
    <div class="form_single"> <label> From HO</label>
        <asp:Label ID="lblLimitAPCash_Branch" runat="server"></asp:Label>
    </div>
</div>


<div class="form_box">
    <div class="form_single"> <label class="label_req"> Branch</label>
    <asp:Label ID="lblLimitAPCash"  runat="server"/>
    <asp:TextBox onblur="LimitAPCash();" ID="E_txtLimitAPCash" runat="server" MaxLength="15">0</asp:TextBox>
    <asp:DropDownList ID="E_cboLimitAPCashBehaviour" runat="server" /> 
    <input id="Hide_LimitAPCash" type="hidden" name="Hide_LimitAPCash" runat="server" />
    <input id="Hide_LimitAPCashBeh" type="hidden" name="Hide_LimitAPCashBeh" runat="server" />
    <asp:RequiredFieldValidator ID="Requiredfieldvalidator50" runat="server" ControlToValidate="E_txtLimitAPCash" CssClass="validator_general" ErrorMessage="Limit Pembayaran di Cabang Salah" Display="Dynamic"></asp:RequiredFieldValidator>
    <asp:RangeValidator ID="rgv_LimitAPCash" runat="server" ControlToValidate="E_txtLimitAPCash" CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Limit Pembayaran di Cabang Salah" MaximumValue="999999999999"></asp:RangeValidator>
</div>
</div>

<div class="form_box"> <div class="form_single"> <label> Recourse</label> <asp:Label ID="E_lblIsRecourse" runat="server"></asp:Label> </div> </div>

<div class="form_box">
    <div class="form_single"> <label class="label_general"> Aktif</label>
     <asp:Label ID="lblIsActive" runat="server"></asp:Label>
    <asp:RadioButtonList ID="E_rboIsActive" runat="server" class="opt_single" RepeatDirection="Horizontal" Height="4px"> <asp:ListItem Value="1" Selected="True">Yes</asp:ListItem> <asp:ListItem Value="0">No</asp:ListItem> </asp:RadioButtonList>
</div>
</div>

<div class="form_box_header">
<div class="form_single"> <h5> Diskon Asuransi</h5> </div>
</div>

<div class="form_box">
<div class="form_single"> <label>  From HO</label>
<asp:Label ID="lblInsuranceDiscPercentage_Branch" runat="server"></asp:Label>
</div>
</div>

<div class="form_box">
<div class="form_single"> <label class="label_req">Branch</label>
    <asp:Label ID="lblInsuranceDiscPercentage" runat="server"></asp:Label>
    <asp:TextBox ID="txtInsuranceDiscPercentage" runat="server" MaxLength="9">0</asp:TextBox><asp:Label ID="Label1" runat="server">%</asp:Label>
    <asp:DropDownList ID="cboInsuranceDiscPercentageBehaviour" runat="server" /> 
    <asp:RequiredFieldValidator ID="Requiredfieldvalidator62" runat="server" ControlToValidate="txtInsuranceDiscPercentage" CssClass="validator_general" ErrorMessage="Diskon Asuransi Salah" Display="Dynamic"></asp:RequiredFieldValidator>
    <asp:RangeValidator ID="Rangevalidator52" runat="server" ControlToValidate="txtInsuranceDiscPercentage" Type="double" MinimumValue="0" ErrorMessage="Diskon Asuransi Salah" CssClass="validator_general" Display="Dynamic" MaximumValue="100"></asp:RangeValidator>
</div>
</div>



</div>