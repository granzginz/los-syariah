﻿#Region "Imports"
Imports Maxiloan.cbse
Imports Maxiloan.Exceptions
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
Imports System.Web.Script.Serialization

#End Region
 
Public Class Product
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents GridNavigator As ucGridNav
#Region "Constanta"
    Private m_controller As New ProductController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private nullDate As System.DateTime = New DateTime(1900, 1, 1)
#End Region

#Region "Property"


    Private Property IsAuthorized() As Boolean
        Get
            Return CType(ViewState("IsAuthorized"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            ViewState("IsAuthorized") = Value
        End Set
    End Property

    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property AddEdit() As String
        Get
            Return CType(ViewState("vwsAddEdt"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsAddEdt") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property

    Private Property TotalBranch() As String
        Get
            Return CType(ViewState("TotalBranch"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("TotalBranch") = Value
        End Set
    End Property

    Private Property SesProductId() As String
        Get
            Return CType(ViewState("ProductId"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ProductId") = Value
        End Set
    End Property

    Public Property KegiatanUsahaS As IList(Of Parameter.KegiatanUsaha)
        Get
            Return CType(ViewState("KegiatanUsaha"), IList(Of Parameter.KegiatanUsaha))
        End Get
        Set(value As IList(Of Parameter.KegiatanUsaha))
            ViewState("KegiatanUsaha") = value
        End Set
    End Property

    Public Property Product As Parameter.Product
        Get
            Return CType(ViewState("ProductUmumTabProduct"), Parameter.Product)
        End Get
        Set(ByVal Value As Parameter.Product)
            ViewState("ProductUmumTabProduct") = Value
        End Set
    End Property



    Property kegiatanUsahaPostback As Boolean
        Get
            Return CType(ViewState("kegiatanUsahaPostback"), Boolean)
        End Get
        Set(value As Boolean)
            ViewState("kegiatanUsahaPostback") = value
        End Set
    End Property

#End Region

#Region "Page_Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If
        If CheckForm(Me.Loginid, "Product", Me.AppId) Then
            If SessionInvalid() Then
                Exit Sub
            End If
        End If
        If (kegiatanUsahaPostback = True) Then
            Return
        End If
        InitialDefaultPanel()
        Me.FormID = "Product"
        AddHandler GridNavigator.PageChanged, AddressOf PageNavigation
        If Not Me.IsPostBack Then
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If SessionInvalid() Then
                    Exit Sub
                End If
                Me.SearchBy = ""
                Me.SortBy = ""
            End If
            Session.Add("conString", GetConnectionString())
            kegiatanUsahaPostback = False

            Me.Sort = "ProductID ASC"
            LoadKegiatanUsaha()

            'FillCbo_KegiatanUsaha()
            'fillCbo_JenisPembiayaan() 

            FillCbo_ApprovalTypeScheme()
            FillCbo_AssetType()
            FillCbo_CreditScoreSchemeMaster()
            FillCbo_JournalScheme()
            FillCbo_ScoreSchemeMaster()

            Me.CmdWhere = "ALL"
            BindGridEntity(Me.CmdWhere)
            Call doBindDropDownListProduct()
            If cboProduct.SelectedValue = "SelectOne" Then
                tabproductNew.Visible = True
            Else
                tabproductNew.Visible = False
            End If
            'cboGrossYieldRate.Visible = False
        End If
    End Sub
#End Region
    Protected Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        currentPage = e.CurrentPage
        BindGridEntity(Me.CmdWhere, True)
        GridNavigator.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
    End Sub
    Sub LoadKegiatanUsaha()
        UmumTab.KegiatanUsahaS = m_controller.LoadKegiatanUsaha(GetConnectionString())
        'UmumTab.KegiatanUsaha = KegiatanUsahaS
    End Sub

#Region "BindGridEntity"
    Sub BindGridEntity(ByVal cmdWhere As String, Optional isFrNav As Boolean = False)
        Dim dtEntity As DataTable
        Dim oProduct As New Parameter.Product

        With oProduct
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = Me.Sort
            .strConnection = GetConnectionString()
        End With
        oProduct = m_controller.ProductPaging(oProduct)

        If Not oProduct Is Nothing Then
            dtEntity = oProduct.ListData
            recordCount = oProduct.TotalRecords
        Else
            recordCount = 0
        End If
        'If recordCount = 0 Then
        '    ButtonPrint.Enabled = False
        'Else
        '    ButtonPrint.Enabled = True
        'End If
        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0

        Dim oCustomClass As New Parameter.Common
        Dim oController As New AuthorizationController

        With oCustomClass
            .strConnection = GetConnectionString()
            .LoginId = Me.Loginid
            .FeatureID = "VIEW"
            .FormID = Me.FormID
            .AppID = Me.AppId
        End With
        Me.IsAuthorized = oController.CheckFeature(oCustomClass)

        dtgPaging.DataBind()

        If (isFrNav = False) Then
            GridNavigator.Initialize(recordCount, pageSize)
        End If
    End Sub
#End Region

#Region "Fill in ComboBox"
    Sub FillCbo_AssetType()
        Dim oProduct As New Parameter.Product
        Dim dtEntity As DataTable
        oProduct.strConnection = GetConnectionString()
        oProduct = m_controller.Get_Combo_AssetType(oProduct)
        dtEntity = oProduct.ListData
        UmumTab.AssetType = oProduct.ListData
        'cboAssetType.DataSource = dtEntity
        'cboAssetType.DataValueField = "AssetTypeId"
        'cboAssetType.DataTextField = "Description"
        'cboAssetType.DataBind()
        'cboAssetType.Items.Insert(0, "Select One")
        'cboAssetType.Items(0).Value = "SelectOne"
    End Sub

    Sub FillCbo_ScoreSchemeMaster()
        'Dim oProduct As New Parameter.Product
        'Dim dtEntity As DataTable
        'oProduct.strConnection = GetConnectionString()
        'oProduct = m_controller.Get_Combo_ScoreSchemeMaster(oProduct)
        'dtEntity = oProduct.ListData
        'cboScoreSchemeMarketing.DataSource = dtEntity
        'cboScoreSchemeMarketing.DataValueField = "ScoreSchemeId"
        'cboScoreSchemeMarketing.DataTextField = "Description"
        'cboScoreSchemeMarketing.DataBind()
        'cboScoreSchemeMarketing.Items.Insert(0, "Select One")
        'cboScoreSchemeMarketing.Items(0).Value = "SelectOne"
    End Sub

    Sub FillCbo_CreditScoreSchemeMaster()
        Dim oProduct As New Parameter.Product
        Dim dtEntity As DataTable
        oProduct.strConnection = GetConnectionString()
        oProduct = m_controller.Get_Combo_CreditScoreSchemeMaster(oProduct)
        dtEntity = oProduct.ListData

        UmumTab.CreditScoreSchemeMaster = oProduct.ListData
        'cboScoreSchemeCredit.DataSource = dtEntity
        'cboScoreSchemeCredit.DataValueField = "CreditScoreSchemeId"
        'cboScoreSchemeCredit.DataTextField = "Description"
        'cboScoreSchemeCredit.DataBind()
        'cboScoreSchemeCredit.Items.Insert(0, "Select One")
        'cboScoreSchemeCredit.Items(0).Value = "SelectOne"
    End Sub

    Sub FillCbo_JournalScheme()
        Dim oProduct As New Parameter.Product
        Dim dtEntity As DataTable
        oProduct.strConnection = GetConnectionString()
        oProduct = m_controller.Get_Combo_JournalScheme(oProduct)
        dtEntity = oProduct.ListData
        UmumTab.JournalScheme = oProduct.ListData

        'cboJournalScheme.DataSource = dtEntity
        'cboJournalScheme.DataValueField = "JournalSchemeId"
        'cboJournalScheme.DataTextField = "Description"
        'cboJournalScheme.DataBind()
        'cboJournalScheme.Items.Insert(0, "Select One")
        'cboJournalScheme.Items(0).Value = "SelectOne"
    End Sub

    Sub FillCbo_ApprovalTypeScheme()
        Dim oProduct As New Parameter.Product
        Dim dtEntity As DataTable
        oProduct.strConnection = GetConnectionString()
        oProduct = m_controller.Get_Combo_ApprovalTypeScheme(oProduct)
        dtEntity = oProduct.ListData
        UmumTab.ApprovalTypeScheme = oProduct.ListData

        'cboApprovalScheme.DataSource = dtEntity
        'cboApprovalScheme.DataValueField = "ApprovalSchemeId"
        'cboApprovalScheme.DataTextField = "ApprovalSchemeName"
        'cboApprovalScheme.DataBind()
        'cboApprovalScheme.Items.Insert(0, "Select One")
        'cboApprovalScheme.Items(0).Value = "SelectOne"
    End Sub

    'Sub FillCbo_KegiatanUsaha()
    '    cboKegiatanUsaha.DataValueField = "Value"
    '    cboKegiatanUsaha.DataTextField = "Text"
    '    cboKegiatanUsaha.DataSource = KegiatanUsahaS
    '    cboKegiatanUsaha.DataBind()
    '    cboKegiatanUsaha.Items.Insert(0, "Select One")
    '    cboKegiatanUsaha.Items(0).Value = "SelectOne"
    '    cboKegiatanUsaha.SelectedIndex = 0
    'End Sub
    'Sub fillCbo_JenisPembiayaan()
    '    Dim def = New List(Of Parameter.CommonValueText)
    '    def.Add(New Parameter.CommonValueText("SelectOne", "Select One"))

    '    cboJenisPembiayaan.DataValueField = "Value"
    '    cboJenisPembiayaan.DataTextField = "Text"
    '    cboJenisPembiayaan.DataSource = def
    '    cboJenisPembiayaan.Items.Insert(0, "Select One")
    '    cboJenisPembiayaan.Items(0).Value = "SelectOne"
    '    cboJenisPembiayaan.DataBind()
    'End Sub
#End Region


#Region "LinkTo"
    Public Shared Function LinkTo(ByVal strProductID As String, ByVal strStyle As String) As String
        'Return "javascript:OpenWinProductView('" & strProductID & "','" & strStyle & "');"
        Return "javascript:OpenWinProductView('" & strStyle & "','" & strProductID & "');"
    End Function
#End Region

#Region " Navigation "
    'Private Sub PagingFooter()
    '    lblPage.Text = currentPage.ToString()
    '    totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
    '    If totalPages = 0 Then
    '        ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
    '        lblTotPage.Text = "1"
    '        rgvGo.MaximumValue = "1"
    '    Else
    '        lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
    '        rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
    '    End If
    '    lblrecord.Text = recordCount.ToString

    '    If currentPage = 1 Then
    '        imbPrevPage.Enabled = False
    '        imbFirstPage.Enabled = False
    '        If totalPages > 1 Then
    '            imbNextPage.Enabled = True
    '            imbLastPage.Enabled = True
    '        Else
    '            imbPrevPage.Enabled = False
    '            imbNextPage.Enabled = False
    '            imbLastPage.Enabled = False
    '            imbFirstPage.Enabled = False
    '        End If
    '    Else
    '        imbPrevPage.Enabled = True
    '        imbFirstPage.Enabled = True
    '        If currentPage = totalPages Then
    '            imbNextPage.Enabled = False
    '            imbLastPage.Enabled = False
    '        Else
    '            imbLastPage.Enabled = True
    '            imbNextPage.Enabled = True
    '        End If
    '    End If
    'End Sub
    'Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
    '    Select Case e.CommandName
    '        Case "First" : currentPage = 1
    '        Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
    '        Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
    '        Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
    '    End Select
    '    BindGridEntity(Me.CmdWhere)
    'End Sub
    'Private Sub imgbtnPageNumb_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click
    '    If IsNumeric(txtPage.Text) Then
    '        If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
    '            currentPage = CType(txtPage.Text, Int32)
    '            BindGridEntity(Me.CmdWhere)
    '        End If
    '    End If
    'End Sub
#End Region

#Region "dtgEntity_ItemCommand"
    Private Sub dtgEntity_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        Dim oProduct As New Parameter.Product
        If e.CommandName = "EDIT" Then
            If CheckFeature(Me.Loginid, Me.FormID, "EDIT", Me.AppId) Then

                Me.AddEdit = "EDIT"
                pnlList.Visible = False
                pnlAddEdit.Visible = True
                kegiatanUsahaPostback = True
                'lblAddEdit.Text = Me.AddEdit


                Dim id = dtgPaging.DataKeys.Item(e.Item.ItemIndex).ToString

                oProduct.ProductId = id
                oProduct.strConnection = GetConnectionString()
                oProduct = m_controller.ProductEdit(oProduct)
                oProduct.ProductId = id
                txtProductID.Text = id
                txtDescription.Text = oProduct.Description
                txtProductID.Enabled = False
                'txtDescription.Enabled = False
                divCopyProduct.Visible = False
                UmumTab.ProdtID = id
                tabproductNew.Visible = True
                UmumTab.Product = oProduct
                UmumTab.UCMode = "EDIT"
                UmumTab.UCProductName = "PRODUK"
                UmumTab.InitUCEditMode()
                UmumTab.ProdtName = txtDescription.Text
                SetHeader("PRODUK", "EDIT")
                SkemaTab.Product = oProduct
                SkemaTab.UCMode = "EDIT"
                SkemaTab.InitUCEditMode()

                BiayaTab.Product = oProduct
                BiayaTab.UCMode = "EDIT"
                BiayaTab.InitUCEditMode()

                AngsuranTab.Product = oProduct
                AngsuranTab.UCMode = "EDIT"
                AngsuranTab.InitUCEditMode()

                CollactionTab.Product = oProduct
                CollactionTab.UCMode = "EDIT"
                CollactionTab.InitUCEditMode()

                FinaceTab.Product = oProduct
                FinaceTab.UCMode = "EDIT"
                FinaceTab.InitUCEditMode()
                cboProduct.SelectedValue = "SelectOne"

            End If
        End If
    End Sub
    


#End Region

#Region "imbSave"
    Private Sub ButtonSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
        If Page.IsValid Then
            Dim oProduct As New Parameter.Product

            Dim ErrMessage As String = ""


            Dim oCopyProduct As New Parameter.CopyProduct

            If cboProduct.SelectedValue <> "SelectOne" Then

                With oCopyProduct
                    .ProductId = txtProductID.Text.Trim
                    .ReferenceProductID = cboProduct.SelectedValue
                    .Description = txtDescription.Text.Trim
                    .BranID = sesBranchId
                    .strConnection = GetConnectionString()
                End With


                Try
                    m_controller.CopyProduct(oCopyProduct)

                    ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
                    kegiatanUsahaPostback = False
                    InitialDefaultPanel()
                    txtSearch.Text = ""

                    Me.CmdWhere = "ALL"
                    BindGridEntity(Me.CmdWhere)

                    Exit Sub
                Catch exp As Exception
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_FAILED, True)
                    Exit Sub
                End Try
            End If
            If UmumTab.cboKegiatanUsaha.SelectedIndex = 0 Then
                ShowMessage(lblMessage, "Harap pilih Kegiatan Usaha.....", True)
                Exit Sub
            End If
            oProduct.ProductId = txtProductID.Text.Trim
            oProduct.Description = txtDescription.Text.Trim
            UmumTab.CollectResult(oProduct)
            SkemaTab.CollectResult(oProduct)
            BiayaTab.CollectResult(oProduct)
            AngsuranTab.CollectResult(oProduct)
            CollactionTab.CollectResult(oProduct)
            FinaceTab.CollectResult(oProduct)

            oProduct.strConnection = GetConnectionString()

            If Me.AddEdit = "ADD" Then



                Dim dtBranch As New DataTable

                pnlList.Visible = False
                pnlAddEdit.Visible = False
                SesProductId = oProduct.ProductId
                oProduct.TotalBranch = "0"
                'oProduct.ProductId = UmumTab.txtProductID.Text.Trim
                'oProduct.AssetTypeID = UmumTab.cboAssetType.SelectedItem.Value.Trim
                oProduct.SaveProduct = "Product"


                Try
                    ErrMessage = m_controller.ProductSaveAdd(oProduct)
                    If ErrMessage = "" Then
                        ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_SUCCESS, False)
                        pnlAddEdit.Visible = False
                        pnlBranch.Visible = True
                        pnlList.Visible = False

                        hplB_ProductID.Text = SesProductId
                        hplB_ProductID.NavigateUrl = LinkTo(oProduct.ProductId.Trim, "Marketing")


                        Dim oProductBranch As New Parameter.Product
                        Dim dtProductBranch As DataTable
                        oProductBranch.strConnection = GetConnectionString()

                        oProductBranch = m_controller.GetBranchAll(oProduct)

                        If Not oProductBranch Is Nothing Then
                            dtProductBranch = oProductBranch.ListData
                            recordCount = oProductBranch.TotalRecords
                        Else
                            recordCount = 0
                        End If


                        dtgBranchList.DataSource = dtProductBranch
                        'dtgBranchList.CurrentPageIndex = 0
                        Try
                            dtgBranchList.DataBind()
                        Catch ex As Exception
                            dtgBranchList.CurrentPageIndex = 0
                            dtgBranchList.DataBind()
                        End Try
                    ElseIf ErrMessage <> "" Then
                        ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_FAILED, True)
                        pnlAddEdit.Visible = True
                        pnlBranch.Visible = False
                        pnlList.Visible = False
                        Exit Sub
                    End If
                Catch exp As Exception
                End Try

            ElseIf Me.AddEdit = "EDIT" Then

                'oProduct.ProductId = UmumTab.lblProductID.Text.Trim
                'oProduct.AssetTypeID = UmumTab.lblAssetTypeID.Text.Trim
                'cboProduct.Visible = True

                Try
                    m_controller.ProductSaveEdit(oProduct)

                    ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
                    pnlAddEdit.Visible = False
                    pnlList.Visible = True
                    Me.CmdWhere = "ALL"
                    BindGridEntity(Me.CmdWhere)
                    tabproductNew.Visible = True
                Catch exp As Exception
                End Try
            End If
            kegiatanUsahaPostback = False

        End If
    End Sub
#End Region
   
#Region "Add"
    Sub SetHeader(ByVal UCProdName As String, ByVal UCM As String)
        lblHeader.Text = String.Format("{0} - {1}", UCProdName.ToUpper(), UCM.ToUpper())
    End Sub
    Private Sub imbAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonAdd.Click
        If CheckFeature(Me.Loginid, Me.FormID, "ADD", Me.AppId) Then
            Session.Add("conString", GetConnectionString())

            kegiatanUsahaPostback = True
            pnlAddEdit.Visible = True
            pnlList.Visible = False


            Me.AddEdit = "ADD"

            UmumTab.UCMode = "ADD"
            UmumTab.UCProductName = "PRODUK"
            SetHeader("PRODUK", "ADD")
            UmumTab.InitUCEditMode()

            SkemaTab.UCMode = "ADD"
            SkemaTab.InitUCEditMode()
            txtProductID.Text = ""
            txtDescription.Text = ""
            txtProductID.Enabled = True
            txtDescription.Enabled = True
            cboProduct.SelectedValue = "SelectOne"
            divCopyProduct.Visible = True
           
            'UmumTab.ProdtID = txtProductID.Text
            'UmumTab.ProdtName = txtDescription.Text
            BiayaTab.UCMode = "ADD"
            BiayaTab.InitUCEditMode()

            AngsuranTab.UCMode = "ADD"
            AngsuranTab.InitUCEditMode()

            FinaceTab.UCMode = "ADD"
            FinaceTab.InitUCEditMode()

            CollactionTab.UCMode = "ADD"
            CollactionTab.InitUCEditMode()

            'lblAddEdit.Text = Me.AddEdit
            pnlAddEdit.Visible = True
            pnlList.Visible = False
        End If
    End Sub


#End Region

#Region "Print"
    'Private Sub imbPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonPrint.Click
    '    If CheckFeature(Me.Loginid, Me.FormID, "PRINT", Me.AppId) Then
    '        SendCookies()
    '        Response.Redirect("Report/ProductRpt.aspx")
    '    End If
    'End Sub
#End Region

#Region "Send Cookies"
    Sub SendCookies()
        Dim cookie As HttpCookie = Request.Cookies("Product")
        Dim strwhere As String
        If txtSearch.Text.Trim = "" Then
            strwhere = "ALL"
        Else
            strwhere = cboSearch.SelectedItem.Value + " = '" + txtSearch.Text.Trim + "'"
        End If

        If Not cookie Is Nothing Then
            cookie.Values("cmdwhere") = strwhere
            cookie.Values("PageFrom") = "Product"
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie("Product")
            cookieNew.Values.Add("cmdwhere", strwhere)
            cookieNew.Values.Add("PageFrom", "Product")
            Response.AppendCookie(cookieNew)
        End If
    End Sub
#End Region

#Region "Sorting"
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub
#End Region

#Region "Cancel"
    Private Sub imbCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        kegiatanUsahaPostback = False
        InitialDefaultPanel()
        txtSearch.Text = ""

        Me.CmdWhere = "ALL"
        BindGridEntity(Me.CmdWhere)
        tabproductNew.Visible = True
    End Sub
#End Region

#Region "Search & Reset"
    Private Sub imbSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonSearch.Click
        If txtSearch.Text.Trim <> "" Then
            Me.CmdWhere = cboSearch.SelectedItem.Value + " LIKE '%" + txtSearch.Text.Trim + "%'"
        Else
            Me.CmdWhere = "ALL"
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub

    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        cboSearch.SelectedIndex = 0
        txtSearch.Text = ""

        Me.CmdWhere = "ALL"
        BindGridEntity(Me.CmdWhere)
    End Sub

#End Region

#Region "InitialDefaultPanel"
    Private Sub InitialDefaultPanel()
        pnlAddEdit.Visible = False
        pnlBranch.Visible = False
        pnlList.Visible = True
        'lblAssetTypeID.Visible = False
    End Sub
#End Region


#Region "dtgPaging_ItemDataBound"
    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        Dim hynProductID As HyperLink
        If e.Item.ItemIndex >= 0 Then
            If Not Me.IsAuthorized Then
                Dim strHTTPServer As String
                Dim StrHTTPApp As String
                Dim strNameServer As String

                strHTTPServer = Request.ServerVariables("PATH_INFO")
                StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                strNameServer = Request.ServerVariables("SERVER_NAME")

                hynProductID = CType(e.Item.FindControl("hynProductID"), HyperLink)
                'hynProductID.NavigateUrl = "http://" & strNameServer & "/" & StrHTTPApp & "/Webform.SalesMarketing/Setting/ProductMnt/ProductView.aspx?ProductId=" & hynProductID.Text.Trim
                hynProductID.NavigateUrl = "http://" & strNameServer & "/" & StrHTTPApp & "/NotAuthorized.html"
            Else
                Dim id = dtgPaging.DataKeys.Item(e.Item.ItemIndex).ToString
                hynProductID = CType(e.Item.FindControl("hynProductID"), HyperLink)
                'hynProductID.NavigateUrl = LinkTo(id.Trim, "Marketing")
                hynProductID.NavigateUrl = LinkTo(e.Item.Cells(2).Text.Trim, "Marketing")
            End If
        End If
    End Sub
#End Region

 



#Region "imgOK"
    Private Sub B_imgOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonOK.Click
        Dim TotalRow_Branch As Integer
        Dim i As Integer
        TotalRow_Branch = dtgBranchList.Items.Count

        Dim lbl_BranchID As New Label
        Dim lbl_BranchName As New Label
        Dim chk_Branch As New CheckBox

        Dim strTotalBranchId As String = ""
        Dim strBranchId As String
        Dim counter_check As Integer = 0

        For i = 0 To TotalRow_Branch - 1
            chk_Branch = CType(dtgBranchList.Items(i).FindControl("chk_Branch"), CheckBox)
            If chk_Branch.Checked Then
                counter_check += 1
                lbl_BranchID = CType(dtgBranchList.Items(i).FindControl("lblBranchId"), Label)
                strBranchId = lbl_BranchID.Text.Trim

                strTotalBranchId = strTotalBranchId + strBranchId + ","
            End If
        Next

        If counter_check = 0 Then
            ShowMessage(lblMessage, "Harap pilih minimal 1 cabang .....", True)
            pnlBranch.Visible = True
            pnlList.Visible = False
            pnlAddEdit.Visible = False
            Exit Sub
        End If

        If strTotalBranchId <> "" Then
            strTotalBranchId = CStr(Left(strTotalBranchId, Len(strTotalBranchId) - 1))
        End If


        Dim oProduct As New Parameter.Product
        oProduct.ProductId = SesProductId
        oProduct.SaveProduct = "Branch"
        oProduct.TotalBranch = strTotalBranchId

        oProduct.strConnection = GetConnectionString()

        Dim ErrMessage As String
        Try
            ErrMessage = m_controller.ProductSaveAdd(oProduct)
            If ErrMessage = "" Then

                ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_SUCCESS, False)
                pnlAddEdit.Visible = False
                pnlBranch.Visible = False
                pnlList.Visible = True
                Me.CmdWhere = "ALL"
                BindGridEntity(Me.CmdWhere)
            Else
                ShowMessage(lblMessage, ErrMessage, True)
            End If
        Catch exp As Exception
            ShowMessage(lblMessage, exp.Message, True)
        End Try
    End Sub

#End Region
    Public Sub doBindDropDownListProduct()
        Dim pController As New ProductController
        Dim oCustom As New Parameter.Product
        Dim dtProduct As New DataTable


        oCustom.strConnection = GetConnectionString()

        oCustom.WhereCond = ""

        dtProduct = pController.DropDownListProduct(oCustom).ListData
        cboProduct.DataValueField = "ProductId"
        cboProduct.DataTextField = "Description"
        cboProduct.DataSource = dtProduct.DefaultView

        cboProduct.DataBind()
        cboProduct.Items.Insert(0, "Select One")
        cboProduct.Items(0).Value = "SelectOne"
        cboProduct.SelectedIndex = 0
    End Sub

    Protected Sub cboProduct_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboProduct.SelectedIndexChanged
        If cboProduct.SelectedValue = "SelectOne" Then
            tabproductNew.Visible = True
        Else
            tabproductNew.Visible = False
        End If
    End Sub

    Private Sub form1_Load(sender As Object, e As System.EventArgs) Handles form1.Load

    End Sub
End Class