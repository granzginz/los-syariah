﻿Public Class ProductUmumTab
    Inherits System.Web.UI.UserControl
    Protected WithEvents txtDescription As Global.System.Web.UI.WebControls.TextBox
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then

        End If

    End Sub
    Public Property UCMode As String
        Get
            Return CType(ViewState("UMUMTABUCMODE"), String)
        End Get
        Set(value As String)
            ViewState("UMUMTABUCMODE") = value
        End Set
    End Property

    Public Property UCProductName As String
        Get
            Return CType(ViewState("UCProductName"), String)
        End Get
        Set(value As String)
            ViewState("UCProductName") = value
        End Set
    End Property

    Public Property ProdtName As String
        Get
            Return CType(ViewState("ProdtName"), String)
        End Get
        Set(value As String)
            ViewState("ProdtName") = value
        End Set
    End Property
    Public Property ProdtID As String
        Get
            Return CType(ViewState("ProdtID"), String)
        End Get
        Set(value As String)
            ViewState("ProdtID") = value
        End Set
    End Property

    Public Property KegiatanUsahaS As IList(Of Parameter.KegiatanUsaha)
        Get
            Return CType(ViewState("KegiatanUsaha"), IList(Of Parameter.KegiatanUsaha))
        End Get
        Set(value As IList(Of Parameter.KegiatanUsaha))
            ViewState("KegiatanUsaha") = value
        End Set
    End Property

    Public Property Product As Parameter.Product
        Get
            Return CType(ViewState("ProductUmumTabProduct"), Parameter.Product)
        End Get
        Set(ByVal Value As Parameter.Product)
            ViewState("ProductUmumTabProduct") = Value
        End Set
    End Property


    Public Property AssetType As DataTable
        Get
            Return CType(ViewState("ASSETTYPE"), DataTable)
        End Get
        Set(value As DataTable)
            ViewState("ASSETTYPE") = value
        End Set
    End Property
    Public Property CreditScoreSchemeMaster As DataTable
        Get
            Return CType(ViewState("CREDITSCORESCHEMEMASTER"), DataTable)
        End Get
        Set(value As DataTable)
            ViewState("CREDITSCORESCHEMEMASTER") = value
        End Set
    End Property


    Public Property JournalScheme As DataTable
        Get
            Return CType(ViewState("JOURNALSCHEME"), DataTable)
        End Get
        Set(value As DataTable)
            ViewState("JOURNALSCHEME") = value
        End Set
    End Property

    Public Property ApprovalTypeScheme As DataTable
        Get
            Return CType(ViewState("APPROVALTYPESCHEME"), DataTable)
        End Get
        Set(value As DataTable)
            ViewState("APPROVALTYPESCHEME") = value
        End Set
    End Property 

    Private Sub cboKegiatanUsaha_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cboKegiatanUsaha.SelectedIndexChanged
        Dim cbo = CType(sender, DropDownList)
        Dim value = cbo.SelectedValue
        If cbo.SelectedIndex = 0 Then
            refresh_cboJenisPembiayaan("")
        Else
            refresh_cboJenisPembiayaan(value)
        End If
    End Sub
    Private Sub InitialControls()

     
        cboAssetType.Visible = (UCMode = "ADD")
        LAssetType.Visible = IIf(UCMode = "VIEW" Or UCMode = "EDIT", True, False)
         
        LScoreSchemeCredit.Visible = IIf(UCMode = "VIEW", True, False)
        LJournalScheme.Visible = IIf(UCMode = "VIEW", True, False)
        LApprovalScheme.Visible = IIf(UCMode = "VIEW", True, False)
        lblAssetUsedNew.Visible = IIf(UCMode = "VIEW", True, False)
        lblUmurKendaraan.Visible = IIf(UCMode = "VIEW", True, False)
        lblKegiatanUsaha.Visible = IIf(UCMode = "VIEW", True, False)
        lblJenisPembiayaan.Visible = IIf(UCMode = "VIEW", True, False)

        'txtProductID.Visible = (UCMode = "ADD")

        'txtDescription.Visible = IIf(UCMode = "ADD" Or UCMode = "EDIT", True, False)

          

        cboScoreSchemeCredit.Visible = IIf(UCMode = "ADD" Or UCMode = "EDIT", True, False)
        cboJournalScheme.Visible = IIf(UCMode = "ADD" Or UCMode = "EDIT", True, False)
        cboApprovalScheme.Visible = IIf(UCMode = "ADD" Or UCMode = "EDIT", True, False)
        cboJenisPembiayaan.Visible = IIf(UCMode = "ADD" Or UCMode = "EDIT", True, False)
        cboKegiatanUsaha.Visible = IIf(UCMode = "ADD" Or UCMode = "EDIT", True, False)

        ' lblAssetUsedNew.Visible = IIf(UCMode = "ADD" Or UCMode = "EDIT", True, False)
        txtUmurKendaraanFrom.Visible = IIf(UCMode = "ADD" Or UCMode = "EDIT", True, False)
        txtUmurKendaraanTo.Visible = IIf(UCMode = "ADD" Or UCMode = "EDIT", True, False)
        cboJenisPembiayaan.Visible = IIf(UCMode = "ADD" Or UCMode = "EDIT", True, False)
        rboAssetUsedNew.Visible = IIf(UCMode = "ADD" Or UCMode = "EDIT", True, False)
        cboKegiatanUsaha.Visible = IIf(UCMode = "ADD" Or UCMode = "EDIT", True, False)
        lblTo.Visible = IIf(UCMode = "ADD" Or UCMode = "EDIT", True, False)
    End Sub
  

    Sub bindFiedsForEdit()


        'txtProductID.Text = lblProductID.Text
        'txtDescription.Text = Product.Description
        cboAssetType.SelectedIndex = cboAssetType.Items.IndexOf(cboAssetType.Items.FindByValue(Product.AssetTypeID))


        LAssetType.Text = Product.AssetType_desc
        lblAssetTypeID.Text = Product.AssetTypeID.Trim
       
        cboScoreSchemeCredit.SelectedIndex = cboScoreSchemeCredit.Items.IndexOf(cboScoreSchemeCredit.Items.FindByValue(Product.CreditScoreSchemeID))
        cboJournalScheme.SelectedIndex = cboJournalScheme.Items.IndexOf(cboJournalScheme.Items.FindByValue(Product.JournalSchemeID))
        cboApprovalScheme.SelectedIndex = cboApprovalScheme.Items.IndexOf(cboApprovalScheme.Items.FindByValue(Product.ApprovalSchemeID))
        rboAssetUsedNew.SelectedIndex = rboAssetUsedNew.Items.IndexOf(rboAssetUsedNew.Items.FindByValue(Product.AssetUsedNew))


        txtUmurKendaraanFrom.Text = Product.UmurKendaraanFrom.ToString
        txtUmurKendaraanTo.Text = Product.UmurKendaraanTo.ToString

        If (Product.KegiatanUsaha = "") Then
            cboKegiatanUsaha.SelectedIndex = 0
            refresh_cboJenisPembiayaan("")
        Else
            cboKegiatanUsaha.SelectedIndex = cboKegiatanUsaha.Items.IndexOf(cboKegiatanUsaha.Items.FindByValue(Product.KegiatanUsaha))
            refresh_cboJenisPembiayaan(Product.KegiatanUsaha)
            cboJenisPembiayaan.SelectedIndex = cboJenisPembiayaan.Items.IndexOf(cboJenisPembiayaan.Items.FindByValue(Product.JenisPembiayaan))
        End If
    End Sub
    Sub initComboWhenAddMode()

        'txtProductID.Text = ""
        'txtDescription.Text = "" 
        cboAssetType.SelectedIndex = cboAssetType.Items.IndexOf(cboAssetType.Items.FindByValue("SelectOne"))
        cboScoreSchemeCredit.SelectedIndex = cboScoreSchemeCredit.Items.IndexOf(cboScoreSchemeCredit.Items.FindByValue("SelectOne"))
        cboJournalScheme.SelectedIndex = cboJournalScheme.Items.IndexOf(cboJournalScheme.Items.FindByValue("SelectOne"))
        cboApprovalScheme.SelectedIndex = cboApprovalScheme.Items.IndexOf(cboApprovalScheme.Items.FindByValue("SelectOne"))
        rboAssetUsedNew.SelectedIndex = rboAssetUsedNew.Items.IndexOf(rboAssetUsedNew.Items.FindByValue("N"))

        txtUmurKendaraanFrom.Text = "0"
        txtUmurKendaraanTo.Text = "0"

        cboKegiatanUsaha.SelectedIndex = 0
        refresh_cboJenisPembiayaan("")


    End Sub
    Sub refresh_cboJenisPembiayaan(key As String)
        Dim def = New List(Of Parameter.CommonValueText)
        def.Add(New Parameter.CommonValueText("SelectOne", "Select One"))
        cboJenisPembiayaan.DataValueField = "Value"
        cboJenisPembiayaan.DataTextField = "Text"


        If key = String.Empty Then
            cboJenisPembiayaan.DataSource = def
            cboJenisPembiayaan.DataBind()
        Else
            Dim k = KegiatanUsahaS.Where(Function(x) x.Value = key).SingleOrDefault()
            cboJenisPembiayaan.DataSource = k.JenisPembiayaan
        End If
        cboJenisPembiayaan.DataBind()
    End Sub
   

    Public Sub InitUCViewMode()

        InitialControls()

      
        LAssetType.Text = Product.AssetType_desc
        LScoreSchemeCredit.Text = Product.CreditScoreSchemeMaster_desc
        LJournalScheme.Text = Product.JournalScheme_desc
        LApprovalScheme.Text = Product.ApprovalTypeScheme_desc
        lblAssetUsedNew.Text = IIf(Product.AssetUsedNew = "N", "New", "User")
        lblUmurKendaraan.Text = String.Format("{0} To {1}", Product.UmurKendaraanFrom, Product.UmurKendaraanTo)
        lblKegiatanUsaha.Text = Product.KegiatanUsahaDesc
        lblJenisPembiayaan.Text = Product.JenisPembiayaanDesc

    End Sub
    Public Sub InitUCEditMode()

        InitialControls()

        cboAssetType.DataSource = AssetType
        cboAssetType.DataValueField = "AssetTypeId"
        cboAssetType.DataTextField = "Description"
        cboAssetType.DataBind()
        cboAssetType.Items.Insert(0, "Select One")
        cboAssetType.Items(0).Value = "SelectOne"


        cboScoreSchemeCredit.DataSource = CreditScoreSchemeMaster
        cboScoreSchemeCredit.DataValueField = "CreditScoreSchemeId"
        cboScoreSchemeCredit.DataTextField = "Description"
        cboScoreSchemeCredit.DataBind()
        cboScoreSchemeCredit.Items.Insert(0, "Select One")
        cboScoreSchemeCredit.Items(0).Value = "SelectOne"


        cboJournalScheme.DataSource = JournalScheme
        cboJournalScheme.DataValueField = "JournalSchemeId"
        cboJournalScheme.DataTextField = "Description"
        cboJournalScheme.DataBind()
        cboJournalScheme.Items.Insert(0, "Select One")
        cboJournalScheme.Items(0).Value = "SelectOne"


        cboApprovalScheme.DataSource = ApprovalTypeScheme
        cboApprovalScheme.DataValueField = "ApprovalSchemeId"
        cboApprovalScheme.DataTextField = "ApprovalSchemeName"
        cboApprovalScheme.DataBind()
        cboApprovalScheme.Items.Insert(0, "Select One")
        cboApprovalScheme.Items(0).Value = "SelectOne"



        cboKegiatanUsaha.DataSource = KegiatanUsahaS
        cboKegiatanUsaha.DataValueField = "Value"
        cboKegiatanUsaha.DataTextField = "Text"
        cboKegiatanUsaha.DataBind()
        cboKegiatanUsaha.Items.Insert(0, "Select One")
        cboKegiatanUsaha.Items(0).Value = "SelectOne"
        cboKegiatanUsaha.SelectedIndex = 0


        Dim def = New List(Of Parameter.CommonValueText)
        def.Add(New Parameter.CommonValueText("SelectOne", "Select One"))

        cboJenisPembiayaan.DataValueField = "Value"
        cboJenisPembiayaan.DataTextField = "Text"
        cboJenisPembiayaan.DataSource = def
        cboJenisPembiayaan.Items.Insert(0, "Select One")
        cboJenisPembiayaan.Items(0).Value = "SelectOne"
        cboJenisPembiayaan.DataBind()

   

        If (UCMode = "ADD") Then
            initComboWhenAddMode()
        End If
        If (UCMode = "EDIT") Then
            bindFiedsForEdit()
        End If
    End Sub

    Public Sub CollectResult(oProduct As Parameter.Product)
        With oProduct 
            '.Description = Me.ProdtName.Trim
            .AssetTypeID = cboAssetType.SelectedItem.Value.Trim
            .ScoreSchemeID = "-"
            .CreditScoreSchemeID = cboScoreSchemeCredit.SelectedItem.Value.Trim
            .JournalSchemeID = cboJournalScheme.SelectedItem.Value.Trim
            .ApprovalSchemeID = cboApprovalScheme.SelectedItem.Value.Trim
            .AssetUsedNew = rboAssetUsedNew.SelectedItem.Value.Trim
            .FinanceType = "-"
            .KegiatanUsaha = cboKegiatanUsaha.SelectedItem.Value.Trim()
            .JenisPembiayaan = cboJenisPembiayaan.SelectedItem.Value.Trim() 
            .UmurKendaraanFrom = CInt(txtUmurKendaraanFrom.Text.Trim)
            .UmurKendaraanTo = CInt(txtUmurKendaraanTo.Text.Trim)
        End With

        If (UCMode = "ADD") Then
            'oProduct.ProductId = txtProductID.Text.Trim
            oProduct.AssetTypeID = cboAssetType.SelectedItem.Value.Trim
        End If
        If (UCMode = "EDIT") Then
            oProduct.ProductId = Me.ProdtID.Trim
            oProduct.AssetTypeID = lblAssetTypeID.Text.Trim
        End If
    End Sub
End Class