﻿Public Class ProductAngsuranBRTab
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub
    Public Property UCMode As String
        Get
            Return CType(ViewState("ANGSURANTABUCMODE"), String)
        End Get
        Set(value As String)
            ViewState("ANGSURANTABUCMODE") = value
        End Set
    End Property
    Public Property Product() As Parameter.Product
        Get
            Return CType(ViewState("ProductAngsuranTabeProduct"), Parameter.Product)
        End Get
        Set(ByVal Value As Parameter.Product)
            ViewState("ProductAngsuranTabeProduct") = Value
        End Set
    End Property
    Sub InitialControls()
        lblPengaturanPembayaran.Visible = (UCMode = "VIEW")
        rboPengaturanPembayaran.Visible = (UCMode <> "VIEW")

        lblPrioritasPembayaran.Visible = (UCMode = "VIEW")
        cboPrioritasPembayaran.Visible = (UCMode <> "VIEW")

        lblInstallmentToleranceAmount.Visible = (UCMode = "VIEW")
        E_txtInstallmentToleranceAmount.Visible = (UCMode <> "VIEW")
        E_cboInstallmentToleranceAmountBehaviour.Visible = (UCMode <> "VIEW")



        lblPenaltyPercentage.Visible = (UCMode = "VIEW")
        E_txtPenaltyPercentage.Visible = (UCMode <> "VIEW")

        E_cboPenaltyPercentageBehaviour.Visible = (UCMode <> "VIEW")

        lblPenaltyBasedOn.Visible = (UCMode = "VIEW")
        rboPenaltyBasedOn.Visible = (UCMode <> "VIEW")
        cboPenaltyBasedOn.Visible = (UCMode <> "VIEW")


        lblPenaltyRateEffectiveDate.Visible = (UCMode = "VIEW")
        txtPenaltyRateEffectiveDate.Visible = (UCMode <> "VIEW")

        lblPenaltyRatePrevious.Visible = (UCMode = "VIEW")
        txtPenaltyRatePrevious.Visible = (UCMode <> "VIEW")

        lblBillingCharges.Visible = (UCMode = "VIEW")
        E_txtBillingCharges.Visible = (UCMode <> "VIEW")
        E_cboBillingChargesBehaviour.Visible = (UCMode <> "VIEW")


        lblPrepaymentPenaltyRate.Visible = (UCMode = "VIEW")
        E_txtPrepaymentPenaltyRate.Visible = (UCMode <> "VIEW")
        E_cboPrepaymentPenaltyRateBehaviour.Visible = (UCMode <> "VIEW")

        lblPrepaymentPenaltyEffectiveDate.Visible = (UCMode = "VIEW")
        txtPrepaymentPenaltyEffectiveDate.Visible = (UCMode <> "VIEW")


        lblLabPrepaymentPenaltyPrevious.Visible = (UCMode = "VIEW")
        txtPrepaymentPenaltyPrevious.Visible = (UCMode <> "VIEW")


        lblPrepaymentPenaltyFixed.Visible = (UCMode = "VIEW")
        txtPrepaymentPenaltyFixed.Visible = (UCMode <> "VIEW")
        cboPrepaymentPenaltyFixedBehaviour.Visible = (UCMode <> "VIEW")


        lblPrepaymentPenaltyFixedEffectiveDate.Visible = (UCMode = "VIEW")
        txtPrepaymentPenaltyFixedEffectiveDate.Visible = (UCMode <> "VIEW")


        lblPrepaymentPenaltyFixedPrevious.Visible = (UCMode = "VIEW")
        txtPrepaymentPenaltyFixedPrevious.Visible = (UCMode <> "VIEW")

        lbldesc1.Visible = (UCMode <> "VIEW")
        lbldesc2.Visible = (UCMode <> "VIEW")
        lbldesc3.Visible = (UCMode <> "VIEW")
        lbldesc4.Visible = (UCMode <> "VIEW")

        If (UCMode = "ADD") Then
            initFieldsForAddMode()
        End If
        If (UCMode = "EDIT") Then
            BindFiedsForEdit()
        End If
    End Sub

    Sub initFieldsForAddMode()

    End Sub
    Public Sub BindFiedsForEdit()

        E_txtPenaltyPercentage.Text = CStr(FormatNumber(Product.PenaltyPercentage, 6))
        Hide_PenaltyPercentage.Value = E_txtPenaltyPercentage.Text
        Hide_PenaltyPercentageBeh.Value = Product.PenaltyPercentageBehaviour
        E_cboPenaltyPercentageBehaviour.SelectedIndex = E_cboPenaltyPercentageBehaviour.Items.IndexOf(E_cboPenaltyPercentageBehaviour.Items.FindByValue(Product.PenaltyPercentageBehaviour))
        Behaviour_Lock(Product.PenaltyPercentageBehaviour, E_txtPenaltyPercentage, E_cboPenaltyPercentageBehaviour)

        'Installment Tolerance Amount
        E_txtInstallmentToleranceAmount.Text = CStr(Product.InstallmentToleranceAmount)
        Hide_InstallmentToleranceAmount.Value = CStr(Product.InstallmentToleranceAmount)
        Hide_InstallmentToleranceAmountBeh.Value = Product.InstallmentToleranceAmountBehaviour
        E_cboInstallmentToleranceAmountBehaviour.SelectedIndex = E_cboInstallmentToleranceAmountBehaviour.Items.IndexOf(E_cboInstallmentToleranceAmountBehaviour.Items.FindByValue(Product.InstallmentToleranceAmountBehaviour))
        Behaviour_Lock(Product.InstallmentToleranceAmountBehaviour, E_txtInstallmentToleranceAmount, E_cboInstallmentToleranceAmountBehaviour)

        txtPrepaymentPenaltyFixed.Text = CStr(FormatNumber(Product.PrepaymentPenaltyFixed, 2))
        cboPrepaymentPenaltyFixedBehaviour.SelectedIndex = cboPrepaymentPenaltyFixedBehaviour.Items.IndexOf(cboPrepaymentPenaltyFixedBehaviour.Items.FindByValue(Product.PrepaymentPenaltyFixedBehaviour))
        cboPenaltyBasedOn.SelectedIndex = cboPenaltyBasedOn.Items.IndexOf(cboPenaltyBasedOn.Items.FindByValue(Product.PenaltyBasedOnBehaviour))
        cboPrioritasPembayaran.SelectedIndex = cboPrioritasPembayaran.Items.IndexOf(cboPrioritasPembayaran.Items.FindByValue(Product.PrioritasPembayaran))
        rboPenaltyBasedOn.SelectedIndex = rboPenaltyBasedOn.Items.IndexOf(rboPenaltyBasedOn.Items.FindByValue(Product.PenaltyBasedOn))
        Behaviour_Lock(Product.PrepaymentPenaltyFixedBehaviour, txtPrepaymentPenaltyFixed, cboPrepaymentPenaltyFixedBehaviour)

        Behaviour_Lock(Product.PenaltyBasedOnBehaviour, rboPenaltyBasedOn, cboPrepaymentPenaltyFixedBehaviour)

        txtPenaltyRatePrevious.Text = CStr(FormatNumber(Product.PenaltyRatePrevious, 10))
        txtPenaltyRateEffectiveDate.Text = Product.PenaltyRateEffectiveDate.ToString("dd/MM/yyyy")
        txtPrepaymentPenaltyPrevious.Text = CStr(FormatNumber(Product.PrepaymentPenaltyPrevious, 10))
        txtPrepaymentPenaltyEffectiveDate.Text = Product.PrepaymentPenaltyEffectiveDate.ToString("dd/MM/yyyy")
        txtPrepaymentPenaltyFixedPrevious.Text = CStr(FormatNumber(Product.PrepaymentPenaltyFixedPrevious, 2))
        txtPrepaymentPenaltyFixedEffectiveDate.Text = Product.PrepaymentPenaltyFixedEffectiveDate.ToString("dd/MM/yyyy")

        E_txtPrepaymentPenaltyRate.Text = CStr(FormatNumber(Product.PrepaymentPenaltyRate, 6))
        Hide_PrepaymentPenaltyRate.Value = E_txtPrepaymentPenaltyRate.Text
        Hide_PrepaymentPenaltyRateBeh.Value = Product.PrepaymentPenaltyRateBehaviour
        E_cboPrepaymentPenaltyRateBehaviour.SelectedIndex = E_cboPrepaymentPenaltyRateBehaviour.Items.IndexOf(E_cboPrepaymentPenaltyRateBehaviour.Items.FindByValue(Product.PrepaymentPenaltyRateBehaviour))
        Behaviour_Lock(Product.PrepaymentPenaltyRateBehaviour, E_txtPrepaymentPenaltyRate, E_cboPrepaymentPenaltyRateBehaviour)

        lblPenaltyRateEffectiveDate_Branch.Text = Product.PenaltyRateEffectiveDate.ToString("dd/MM/yyyy")


        E_txtBillingCharges.Text = CStr(Product.BillingCharges)
        Hide_BillingCharges.Value = CStr(Product.BillingCharges)
        Hide_BillingChargesBeh.Value = Product.BillingChargesBehaviour
        E_cboBillingChargesBehaviour.SelectedIndex = E_cboBillingChargesBehaviour.Items.IndexOf(E_cboBillingChargesBehaviour.Items.FindByValue(Product.BillingChargesBehaviour))
        Behaviour_Lock(Product.BillingChargesBehaviour, E_txtBillingCharges, E_cboBillingChargesBehaviour)



        setBranchText()
    End Sub

    Sub setBranchText()
        lblPenaltyPercentage_Branch.Text = CStr(FormatNumber(Product.PenaltyPercentage_HO, 2)) & " ‰ / day " & Parameter.Product.Behaviour_Value(Product.PenaltyPercentageBehaviour_HO)
        lblInstallmentToleranceAmount_Branch.Text = CStr(Product.InstallmentToleranceAmount_HO) & " " & Parameter.Product.Behaviour_Value(Product.InstallmentToleranceAmountBehaviour_HO)


        lblPenaltyRateEffectiveDate_Branch.Text = Product.PenaltyRateEffectiveDate_HO.ToString("dd/MM/yyyy")
        'lblPrioritasPembayaran_Branch.Text = IIf(Product.PrioritasPembayaran_HO = "PIL", "Pokok-Bunga-Denda" Or "PLI", "Pokok-Denda-Bunga" Or "IPL", "Bunga-Pokok-Denda" Or "ILP", "Bunga-Denda-Pokok" Or "LPI", "Denda-Pokok-Bunga" Or "LIP", "Denda-Bunga-Pokok")
        'lblPrioritasPembayaran_Branch.Text = IIf(Product.PrioritasPembayaran_HO = "PIL", "Pokok-Bunga-Denda", "Pokok-Denda-Bunga")
        lblPengaturanPembayaran.Text = IIf(Product.PrioritasPembayaran_HO = "PE", "Prioritas", "Manual")
        lblPrepaymentPenaltyEffectiveDate_Branch.Text = Product.PrepaymentPenaltyEffectiveDate_HO.ToString("dd/MM/yyyy")
        lblPrepaymentPenaltyPrevious_Branch.Text = CStr(FormatNumber(Product.PrepaymentPenaltyPrevious_HO, 10))

        lblPrepaymentPenaltyFixed_Branch.Text = CStr(FormatNumber(Product.PrepaymentPenaltyFixed_HO, 2))
        lblPrepaymentPenaltyFixedEffectiveDate_Branch.Text = String.Format("{0} {1} ", Product.PrepaymentPenaltyFixedEffectiveDate_HO.ToString("dd/MM/yyyy"), Parameter.Product.Behaviour_Value(Product.PrepaymentPenaltyFixedBehaviour_HO))
        lblPrepaymentPenaltyFixedPrevious_Branch.Text = CStr(FormatNumber(Product.PrepaymentPenaltyFixedPrevious_HO, 2))

        lblPenaltyBasedOn_Branch.Text = String.Format("{0} {1}", IIf(Product.PenaltyBasedOn_HO = "PB", "Pokok + Bunga", "Pokok"), Parameter.Product.Behaviour_Value(Product.PenaltyBasedOnBehaviour_HO))
        lblPrepaymentPenaltyRate_Branch.Text = CStr(FormatNumber(Product.PrepaymentPenaltyRate_HO, 6)) & " % " & Parameter.Product.Behaviour_Value(Product.PrepaymentPenaltyRateBehaviour_HO)

        'lblPrioritasPembayaran_Branch.Text = IIf(Product.PrioritasPembayaran_HO = "DE", "Denda", "Bebas")

        lblPrepaymentPenaltyEffectiveDate_Branch.Text = Product.PrepaymentPenaltyEffectiveDate_HO.ToString("dd/MM/yyyy")
        lblPrepaymentPenaltyPrevious_Branch.Text = CStr(FormatNumber(Product.PrepaymentPenaltyPrevious_HO, 10))

        lblPrepaymentPenaltyFixed_Branch.Text = String.Format("{0} {1}", CStr(FormatNumber(Product.PrepaymentPenaltyFixed_HO, 2)), Parameter.Product.Behaviour_Value(Product.PrepaymentPenaltyFixedBehaviour_HO))

        lblPrepaymentPenaltyFixedEffectiveDate_Branch.Text = Product.PrepaymentPenaltyFixedEffectiveDate_HO.ToString("dd/MM/yyyy")
        lblPrepaymentPenaltyFixedPrevious_Branch.Text = CStr(FormatNumber(Product.PrepaymentPenaltyFixedPrevious_HO, 2))
        lblPenaltyRatePrevious_Branch.Text = Product.PenaltyRatePrevious_HO.ToString

        lblBillingCharges_Branch.Text = CStr(Product.BillingCharges_HO) & "  " & Parameter.Product.Behaviour_Value(Product.BillingChargesBehaviour_HO)
    End Sub


    Public Sub InitUCViewMode()
        InitialControls()

        lblPenaltyBasedOn.Text = String.Format("{0} {1}", IIf(Product.PenaltyBasedOn = "PB", "Pokok + Bunga", "Pokok"), Parameter.Product.Behaviour_Value(Product.PenaltyBasedOnBehaviour))
        lblPenaltyRateEffectiveDate.Text = Product.PenaltyRateEffectiveDate.ToString("dd/MM/yyyy")
        lblPenaltyRatePrevious.Text = String.Format("{0} ‰ / day ", Product.PenaltyRatePrevious.ToString)

        lblBillingCharges.Text = FormatNumber(CStr(Product.BillingCharges).Trim, 2) & "  " & Parameter.Product.Behaviour_Value(Product.BillingChargesBehaviour)
        lblPrepaymentPenaltyRate.Text = CStr(FormatNumber(Product.PrepaymentPenaltyRate, 6)) & " % " & Parameter.Product.Behaviour_Value(Product.PrepaymentPenaltyRateBehaviour)
        lblPrepaymentPenaltyEffectiveDate.Text = Product.PrepaymentPenaltyEffectiveDate.ToString("dd/MM/yyyy")
        lblLabPrepaymentPenaltyPrevious.Text = Product.PrepaymentPenaltyPrevious.ToString

        lblPrepaymentPenaltyFixed.Text = String.Format("{0} {1}", Product.PrepaymentPenaltyFixed.ToString, Parameter.Product.Behaviour_Value(Product.PrepaymentPenaltyFixedBehaviour))


        lblPrepaymentPenaltyFixedEffectiveDate.Text = Product.PrepaymentPenaltyFixedEffectiveDate.ToString("dd/MM/yyyy")

        lblPrepaymentPenaltyFixedPrevious.Text = Product.PrepaymentPenaltyFixedPrevious.ToString

        'lblPrioritasPembayaran.Text = IIf(Product.PrioritasPembayaran = "DE", "Denda", "Bebas")
        lblInstallmentToleranceAmount.Text = FormatNumber(CStr(Product.InstallmentToleranceAmount).Trim, 2) & " " & Parameter.Product.Behaviour_Value(Product.InstallmentToleranceAmountBehaviour)

        lblPenaltyPercentage.Text = String.Format("{0} ‰ / day {1}", CStr(FormatNumber(Product.PenaltyPercentage, 2)), Parameter.Product.Behaviour_Value(Product.PenaltyPercentageBehaviour))

        setBranchText()
    End Sub


    Sub bindBehaviour(ddl As DropDownList)
        ddl.DataValueField = "Value"
        ddl.DataTextField = "Text"
        ddl.DataSource = Parameter.Product.BehaviourItems
        ddl.DataBind()
    End Sub


    Public Sub InitUCEditMode()
        bindBehaviour(E_cboInstallmentToleranceAmountBehaviour)
        bindBehaviour(E_cboPenaltyPercentageBehaviour)
        bindBehaviour(cboPenaltyBasedOn)
        bindBehaviour(E_cboBillingChargesBehaviour)
        bindBehaviour(E_cboPrepaymentPenaltyRateBehaviour)
        bindBehaviour(cboPrepaymentPenaltyFixedBehaviour)

        InitialControls()
    End Sub
    Public Sub CollectResult(oProduct As Parameter.Product)
        With oProduct

            .PenaltyPercentage = CDec(E_txtPenaltyPercentage.Text.Trim)
            .PenaltyPercentageBehaviour = E_cboPenaltyPercentageBehaviour.SelectedItem.Value.Trim
            .InstallmentToleranceAmount = CDec(E_txtInstallmentToleranceAmount.Text.Trim)
            .InstallmentToleranceAmountBehaviour = E_cboInstallmentToleranceAmountBehaviour.SelectedItem.Value.Trim

            .PrepaymentPenaltyFixed = CDec(txtPrepaymentPenaltyFixed.Text.Trim)
            .PrepaymentPenaltyFixedBehaviour = cboPrepaymentPenaltyFixedBehaviour.SelectedItem.Value.Trim
            .PenaltyBasedOn = rboPenaltyBasedOn.SelectedItem.Value.Trim
            .PenaltyBasedOnBehaviour = cboPenaltyBasedOn.SelectedItem.Value.Trim

            .PenaltyRatePrevious = CDec(txtPenaltyRatePrevious.Text.Trim)
            .PenaltyRateEffectiveDate = Date.ParseExact(txtPenaltyRateEffectiveDate.Text.ToString, "dd/MM/yyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo)
            .PrepaymentPenaltyPrevious = CDec(txtPrepaymentPenaltyPrevious.Text.Trim)
            .PrepaymentPenaltyEffectiveDate = Date.ParseExact(txtPrepaymentPenaltyEffectiveDate.Text.ToString, "dd/MM/yyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo)
            .PrepaymentPenaltyFixedPrevious = CDec(txtPrepaymentPenaltyFixedPrevious.Text.Trim)
            .PrepaymentPenaltyFixedEffectiveDate = Date.ParseExact(txtPrepaymentPenaltyFixedEffectiveDate.Text.ToString, "dd/MM/yyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo)
            .PengaturanBiaya = rboPengaturanPembayaran.SelectedValue.Trim
            .PrioritasPembayaran = cboPrioritasPembayaran.SelectedValue.Trim
            .PrepaymentPenaltyRate = CDec(E_txtPrepaymentPenaltyRate.Text.Trim)
            .PrepaymentPenaltyRateBehaviour = E_cboPrepaymentPenaltyRateBehaviour.SelectedItem.Value.Trim
            .BillingCharges = CDec(E_txtBillingCharges.Text.Trim)
            .BillingChargesBehaviour = E_cboBillingChargesBehaviour.SelectedItem.Value.Trim


        End With
    End Sub

    Public Sub changed(sender As Object, e As EventArgs) Handles rboPengaturanPembayaran.SelectedIndexChanged

        cboPrioritasPembayaran.Visible = (rboPengaturanPembayaran.SelectedIndex = 0)
        TR_Prioritas.Visible = (rboPengaturanPembayaran.SelectedIndex = 0)
        TR_PrioritasHeader.Visible = (rboPengaturanPembayaran.SelectedIndex = 0)
        TR_PrioritasHO.Visible = (rboPengaturanPembayaran.SelectedIndex = 0)
    End Sub
End Class