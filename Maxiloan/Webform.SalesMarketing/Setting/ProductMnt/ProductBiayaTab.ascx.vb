﻿Public Class ProductBiayaTab
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub
    Public Property Product() As Parameter.Product
        Get
            Return CType(ViewState("ProductBiayaTabProduct"), Parameter.Product)
        End Get
        Set(ByVal Value As Parameter.Product)
            ViewState("ProductBiayaTabProduct") = Value
        End Set
    End Property
    Public Property UCMode As String
        Get
            Return CType(ViewState("BIAYATABUCMODE"), String)
        End Get
        Set(value As String)
            ViewState("BIAYATABUCMODE") = value
        End Set
    End Property

    Public Sub InitUCEditMode()
        bindBehaviour(cboCancellationFee)
        bindBehaviour(cboAdminFee)
        bindBehaviour(cboFiduciaFee)
        bindBehaviour(cboProvisionFee)
        bindBehaviour(cboProvisionFee)
        bindBehaviour(cboNotaryFee)
        bindBehaviour(cboSurveyFee)
        bindBehaviour(cboVisitFee)
        bindBehaviour(cboReschedulingFee)
        bindBehaviour(cboATFee)
        bindBehaviour(cboCDDRFee)
        bindBehaviour(cboAssetReplacementFee)
        bindBehaviour(cboRepossesFee)
        bindBehaviour(cboLDFee)
        bindBehaviour(cboPDCBounceFee)
        bindBehaviour(cboInsuranceAdminFee)
        bindBehaviour(cboISDFee)
        bindBehaviour(cboBiayaPolis)
        InitialControls()

        If (UCMode = "ADD") Then
            initFieldsForAddMode()
        End If
        If (UCMode = "EDIT") Then
            bindFiedsForEdit()
        End If
    End Sub
    Sub InitialControls()
        lblCancellationFee.Visible = (UCMode = "VIEW")
        txtCancellationFee.Visible = (UCMode <> "VIEW")
        cboCancellationFee.Visible = (UCMode <> "VIEW")

        lblAdminFee.Visible = (UCMode = "VIEW")
        txtAdminFee.Visible = (UCMode <> "VIEW")
        cboAdminFee.Visible = (UCMode <> "VIEW")

        lblFiduciaFee.Visible = (UCMode = "VIEW")
        txtFiduciaFee.Visible = (UCMode <> "VIEW")
        cboFiduciaFee.Visible = (UCMode <> "VIEW")

        lblProvisionFee.Visible = (UCMode = "VIEW")
        txtProvisionFee.Visible = (UCMode <> "VIEW")
        cboProvisionFee.Visible = (UCMode <> "VIEW")


        lblNotaryFee.Visible = (UCMode = "VIEW")
        txtNotaryFee.Visible = (UCMode <> "VIEW") 
        cboNotaryFee.Visible = (UCMode <> "VIEW")
         
        lblSurveyFee.Visible = (UCMode = "VIEW")
        txtSurveyFee.Visible = (UCMode <> "VIEW")
        cboSurveyFee.Visible = (UCMode <> "VIEW")


        lblVisitFee.Visible = (UCMode = "VIEW")
        txtVisitFee.Visible = (UCMode <> "VIEW")
        cboVisitFee.Visible = (UCMode <> "VIEW")

        lblReschedulingFee.Visible = (UCMode = "VIEW")
        txtReschedulingFee.Visible = (UCMode <> "VIEW")
        cboReschedulingFee.Visible = (UCMode <> "VIEW")
         
        lblATFee.Visible = (UCMode = "VIEW")
        txtATFee.Visible = (UCMode <> "VIEW")
        cboATFee.Visible = (UCMode <> "VIEW")


        lblCDDRFee.Visible = (UCMode = "VIEW")
        txtCDDRFee.Visible = (UCMode <> "VIEW")
        cboCDDRFee.Visible = (UCMode <> "VIEW")

        lblAssetReplacementFee.Visible = (UCMode = "VIEW")
        txtAssetReplacementFee.Visible = (UCMode <> "VIEW")
        cboAssetReplacementFee.Visible = (UCMode <> "VIEW")

        lblRepossesFee.Visible = (UCMode = "VIEW")
        txtRepossesFee.Visible = (UCMode <> "VIEW")
        cboRepossesFee.Visible = (UCMode <> "VIEW")

        lblLDFee.Visible = (UCMode = "VIEW")
        txtLDFee.Visible = (UCMode <> "VIEW")
        cboLDFee.Visible = (UCMode <> "VIEW")

        lblPDCBounceFee.Visible = (UCMode = "VIEW")
        txtPDCBounceFee.Visible = (UCMode <> "VIEW")
        cboPDCBounceFee.Visible = (UCMode <> "VIEW")

        lblInsuranceAdminFee.Visible = (UCMode = "VIEW")
        txtInsuranceAdminFee.Visible = (UCMode <> "VIEW")
        cboInsuranceAdminFee.Visible = (UCMode <> "VIEW")

        lblISDFee.Visible = (UCMode = "VIEW")
        txtISDFee.Visible = (UCMode <> "VIEW")
        cboISDFee.Visible = (UCMode <> "VIEW")


        lblBiayaPolis.Visible = (UCMode = "VIEW")
        txtBiayaPolis.Visible = (UCMode <> "VIEW")
        cboBiayaPolis.Visible = (UCMode <> "VIEW")
    End Sub
    Sub initFieldsForAddMode()
        txtCancellationFee.Text = "0"
        cboCancellationFee.SelectedIndex = cboCancellationFee.Items.IndexOf(cboCancellationFee.Items.FindByValue("D"))

        txtAdminFee.Text = "0"
        cboAdminFee.SelectedIndex = cboAdminFee.Items.IndexOf(cboAdminFee.Items.FindByValue("D"))

        txtFiduciaFee.Text = "0"
        cboFiduciaFee.SelectedIndex = cboFiduciaFee.Items.IndexOf(cboFiduciaFee.Items.FindByValue("D"))

        txtProvisionFee.Text = "0"
        cboProvisionFee.SelectedIndex = cboProvisionFee.Items.IndexOf(cboProvisionFee.Items.FindByValue("D"))

        txtNotaryFee.Text = "0"
        cboNotaryFee.SelectedIndex = cboNotaryFee.Items.IndexOf(cboNotaryFee.Items.FindByValue("D"))

        txtSurveyFee.Text = "0"
        cboSurveyFee.SelectedIndex = cboSurveyFee.Items.IndexOf(cboSurveyFee.Items.FindByValue("D"))

        txtVisitFee.Text = "0"
        cboVisitFee.SelectedIndex = cboVisitFee.Items.IndexOf(cboVisitFee.Items.FindByValue("D"))

        txtReschedulingFee.Text = "0"
        cboReschedulingFee.SelectedIndex = 0 
        txtATFee.Text = "0"
        cboATFee.SelectedIndex = 0 
        txtCDDRFee.Text = "0"
        cboCDDRFee.SelectedIndex = 0

        txtAssetReplacementFee.Text = "0"
        cboAssetReplacementFee.SelectedIndex = 0

        txtRepossesFee.Text = "0"
        cboRepossesFee.SelectedIndex = 0

        txtLDFee.Text = "0"
        cboLDFee.SelectedIndex = 0

        txtPDCBounceFee.Text = "0"
        cboPDCBounceFee.SelectedIndex = 0

        txtInsuranceAdminFee.Text = "0"
        cboInsuranceAdminFee.SelectedIndex = 0

        txtISDFee.Text = "0"
        cboISDFee.SelectedIndex = 0

        txtBiayaPolis.Text = "0"
        cboBiayaPolis.SelectedIndex = 0

    End Sub

    Public Sub bindFiedsForEdit()
        txtCancellationFee.Text = FormatNumber(Product.CancellationFee, 2)
        cboCancellationFee.SelectedIndex = cboCancellationFee.Items.IndexOf(cboCancellationFee.Items.FindByValue(Product.CancellationFeeBehaviour))

        txtAdminFee.Text = FormatNumber(Product.AdminFee, 2)
        cboAdminFee.SelectedIndex = cboAdminFee.Items.IndexOf(cboAdminFee.Items.FindByValue(Product.AdminFeeBehaviour))

        txtFiduciaFee.Text = FormatNumber(Product.FiduciaFee, 2)
        cboFiduciaFee.SelectedIndex = cboFiduciaFee.Items.IndexOf(cboFiduciaFee.Items.FindByValue(Product.FiduciaFeeBehaviour))

        txtProvisionFee.Text = FormatNumber(Product.ProvisionFee, 2)
        cboProvisionFee.SelectedIndex = cboProvisionFee.Items.IndexOf(cboProvisionFee.Items.FindByValue(Product.ProvisionFeeBehaviour))

        txtNotaryFee.Text = FormatNumber(Product.NotaryFee, 2)
        cboNotaryFee.SelectedIndex = cboNotaryFee.Items.IndexOf(cboNotaryFee.Items.FindByValue(Product.NotaryFeeBehaviour))

        txtSurveyFee.Text = FormatNumber(Product.SurveyFee, 2)
        cboSurveyFee.SelectedIndex = cboSurveyFee.Items.IndexOf(cboSurveyFee.Items.FindByValue(Product.SurveyFeeBehaviour))

        txtVisitFee.Text = FormatNumber(Product.VisitFee, 2)
        cboVisitFee.SelectedIndex = cboVisitFee.Items.IndexOf(cboVisitFee.Items.FindByValue(Product.VisitFeeBehaviour))

        txtReschedulingFee.Text = FormatNumber(Product.ReschedulingFee, 2)
        cboReschedulingFee.SelectedIndex = cboReschedulingFee.Items.IndexOf(cboReschedulingFee.Items.FindByValue(Product.ReschedulingFeeBehaviour))

        txtATFee.Text = FormatNumber(Product.AgreementTransferFee, 2)
        cboATFee.SelectedIndex = cboATFee.Items.IndexOf(cboATFee.Items.FindByValue(Product.AgreementTransferFeeBehaviour))

        txtCDDRFee.Text = FormatNumber(Product.ChangeDueDateFee, 2)
        cboCDDRFee.SelectedIndex = cboCDDRFee.Items.IndexOf(cboCDDRFee.Items.FindByValue(Product.ChangeDueDateFeeBehaviour))

        txtAssetReplacementFee.Text = FormatNumber(Product.AssetReplacementFee, 2)
        cboAssetReplacementFee.SelectedIndex = cboAssetReplacementFee.Items.IndexOf(cboAssetReplacementFee.Items.FindByValue(Product.AssetReplacementFeeBehaviour))

        txtRepossesFee.Text = FormatNumber(Product.RepossesFee, 2)
        cboRepossesFee.SelectedIndex = cboRepossesFee.Items.IndexOf(cboRepossesFee.Items.FindByValue(Product.RepossesFeeBehaviour))

        txtLDFee.Text = FormatNumber(Product.LegalisirDocFee, 2)
        cboLDFee.SelectedIndex = cboLDFee.Items.IndexOf(cboLDFee.Items.FindByValue(Product.LegalisirDocFeeBehaviour))

        txtPDCBounceFee.Text = FormatNumber(Product.PDCBounceFee, 2)
        cboPDCBounceFee.SelectedIndex = cboPDCBounceFee.Items.IndexOf(cboPDCBounceFee.Items.FindByValue(Product.PDCBounceFeeBehaviour))

        txtInsuranceAdminFee.Text = FormatNumber(Product.InsAdminFee, 2)
        cboInsuranceAdminFee.SelectedIndex = cboInsuranceAdminFee.Items.IndexOf(cboInsuranceAdminFee.Items.FindByValue(Product.InsAdminFeeBehaviour))

        txtISDFee.Text = FormatNumber(Product.InsStampDutyFee, 2)
        cboISDFee.SelectedIndex = cboISDFee.Items.IndexOf(cboISDFee.Items.FindByValue(Product.InsStampDutyFeeBehaviour))

        txtBiayaPolis.Text = FormatNumber(Product.BiayaPolis, 2)
        cboBiayaPolis.SelectedIndex = cboBiayaPolis.Items.IndexOf(cboBiayaPolis.Items.FindByValue(Product.BiayaPolisBehaviour))
    End Sub

    Public Sub InitUCViewMode()

        InitialControls()
        lblCancellationFee.Text = FormatNumber(CStr(Product.CancellationFee).Trim, 2) & " " & Parameter.Product.Behaviour_Value(Product.CancellationFeeBehaviour)
        lblAdminFee.Text = FormatNumber(CStr(Product.AdminFee).Trim, 2) & " " & Parameter.Product.Behaviour_Value(Product.AdminFeeBehaviour)
        lblFiduciaFee.Text = FormatNumber(CStr(Product.FiduciaFee).Trim, 2) & " " & Parameter.Product.Behaviour_Value(Product.FiduciaFeeBehaviour)
        lblProvisionFee.Text = FormatNumber(CStr(Product.ProvisionFee).Trim, 2) & " " & Parameter.Product.Behaviour_Value(Product.ProvisionFeeBehaviour)
        lblNotaryFee.Text = FormatNumber(CStr(Product.NotaryFee).Trim, 2) & " " & Parameter.Product.Behaviour_Value(Product.NotaryFeeBehaviour)
        lblSurveyFee.Text = FormatNumber(CStr(Product.SurveyFee).Trim, 2) & " " & Parameter.Product.Behaviour_Value(Product.SurveyFeeBehaviour)
        lblVisitFee.Text = FormatNumber(CStr(Product.VisitFee).Trim, 2) & " " & Parameter.Product.Behaviour_Value(Product.VisitFeeBehaviour)
        lblReschedulingFee.Text = FormatNumber(CStr(Product.ReschedulingFee).Trim, 2) & " " & Parameter.Product.Behaviour_Value(Product.ReschedulingFeeBehaviour)
        lblATFee.Text = FormatNumber(CStr(Product.AgreementTransferFee).Trim, 2) & " " & Parameter.Product.Behaviour_Value(Product.AgreementTransferFeeBehaviour)
        lblCDDRFee.Text = FormatNumber(CStr(Product.ChangeDueDateFee).Trim, 2) & " " & Parameter.Product.Behaviour_Value(Product.ChangeDueDateFeeBehaviour)
        lblAssetReplacementFee.Text = FormatNumber(CStr(Product.AssetReplacementFee).Trim, 2) & " " & Parameter.Product.Behaviour_Value(Product.AssetReplacementFeeBehaviour)
        lblRepossesFee.Text = FormatNumber(CStr(Product.RepossesFee).Trim, 2) & " " & Parameter.Product.Behaviour_Value(Product.RepossesFeeBehaviour)
        lblLDFee.Text = FormatNumber(CStr(Product.LegalisirDocFee).Trim, 2) & " " & Parameter.Product.Behaviour_Value(Product.LegalisirDocFeeBehaviour)
        lblPDCBounceFee.Text = FormatNumber(CStr(Product.PDCBounceFee).Trim, 2) & " " & Parameter.Product.Behaviour_Value(Product.PDCBounceFeeBehaviour)

        lblInsuranceAdminFee.Text = FormatNumber(CStr(Product.InsAdminFee).Trim, 2) & " " & Parameter.Product.Behaviour_Value(Product.InsAdminFeeBehaviour)
        lblISDFee.Text = FormatNumber(CStr(Product.InsStampDutyFee).Trim, 2) & " " & Parameter.Product.Behaviour_Value(Product.InsStampDutyFeeBehaviour)
        lblBiayaPolis.Text = FormatNumber(CStr(Product.BiayaPolis).Trim, 2) & " " & Parameter.Product.Behaviour_Value(Product.BiayaPolisBehaviour)
    End Sub

    Public Sub CollectResult(oProduct As Parameter.Product)
        With oProduct
            .CancellationFee = CDec(txtCancellationFee.Text.Trim)
            .CancellationFeeBehaviour = cboCancellationFee.SelectedItem.Value.Trim
            .AdminFee = CDec(txtAdminFee.Text.Trim)
            .AdminFeeBehaviour = cboAdminFee.SelectedItem.Value.Trim
            .FiduciaFee = CDec(txtFiduciaFee.Text.Trim)
            .FiduciaFeeBehaviour = cboFiduciaFee.SelectedItem.Value.Trim
            .ProvisionFee = CDec(txtProvisionFee.Text.Trim)
            .ProvisionFeeBehaviour = cboProvisionFee.SelectedItem.Value.Trim
            .NotaryFee = CDec(txtNotaryFee.Text.Trim)
            .NotaryFeeBehaviour = cboNotaryFee.SelectedItem.Value.Trim
            .SurveyFee = CDec(txtSurveyFee.Text.Trim)
            .SurveyFeeBehaviour = cboSurveyFee.SelectedItem.Value.Trim
            .VisitFee = CDec(txtVisitFee.Text.Trim)
            .VisitFeeBehaviour = cboVisitFee.SelectedItem.Value.Trim
            .ReschedulingFee = CDec(txtReschedulingFee.Text.Trim)
            .ReschedulingFeeBehaviour = cboReschedulingFee.SelectedItem.Value.Trim
            .AgreementTransferFee = CDec(txtATFee.Text.Trim)
            .AgreementTransferFeeBehaviour = cboATFee.SelectedItem.Value.Trim
            .ChangeDueDateFee = CDec(txtCDDRFee.Text.Trim)
            .ChangeDueDateFeeBehaviour = cboCDDRFee.SelectedItem.Value.Trim
            .AssetReplacementFee = CDec(txtAssetReplacementFee.Text.Trim)
            .AssetReplacementFeeBehaviour = cboAssetReplacementFee.SelectedItem.Value.Trim
            .RepossesFee = CDec(txtRepossesFee.Text.Trim)
            .RepossesFeeBehaviour = cboRepossesFee.SelectedItem.Value.Trim
            .LegalisirDocFee = CDec(txtLDFee.Text.Trim)
            .LegalisirDocFeeBehaviour = cboLDFee.SelectedItem.Value.Trim
            .PDCBounceFee = CDec(txtPDCBounceFee.Text.Trim)
            .PDCBounceFeeBehaviour = cboPDCBounceFee.SelectedItem.Value.Trim
            .InsAdminFee = CDec(txtInsuranceAdminFee.Text.Trim)
            .InsAdminFeeBehaviour = cboInsuranceAdminFee.SelectedItem.Value.Trim
            .InsStampDutyFee = CDec(txtISDFee.Text.Trim)
            .InsStampDutyFeeBehaviour = cboISDFee.SelectedItem.Value.Trim
            .BiayaPolis = CDec(txtBiayaPolis.Text.Trim)
            .BiayaPolisBehaviour = cboBiayaPolis.SelectedItem.Value.Trim
        End With
    End Sub
    Public Sub BehaviourLockSetting() 
        BehaviorHelperModule.Behaviour_Lock(Product.CancellationFeeBehaviour, txtCancellationFee, cboCancellationFee) 
        BehaviorHelperModule.Behaviour_Lock(Product.AdminFeeBehaviour, txtAdminFee, cboAdminFee) 
        BehaviorHelperModule.Behaviour_Lock(Product.FiduciaFeeBehaviour, txtFiduciaFee, cboFiduciaFee)
        BehaviorHelperModule.Behaviour_Lock(Product.ProvisionFeeBehaviour, txtProvisionFee, cboProvisionFee)
        BehaviorHelperModule.Behaviour_Lock(Product.NotaryFeeBehaviour, txtNotaryFee, cboNotaryFee)
        BehaviorHelperModule.Behaviour_Lock(Product.SurveyFeeBehaviour, txtSurveyFee, cboSurveyFee)
        BehaviorHelperModule.Behaviour_Lock(Product.VisitFeeBehaviour, txtVisitFee, cboVisitFee)
        BehaviorHelperModule.Behaviour_Lock(Product.ReschedulingFeeBehaviour, txtReschedulingFee, cboReschedulingFee)
        BehaviorHelperModule.Behaviour_Lock(Product.AgreementTransferFeeBehaviour, txtATFee, cboATFee)
        BehaviorHelperModule.Behaviour_Lock(Product.ChangeDueDateFeeBehaviour, txtCDDRFee, cboCDDRFee) 
        BehaviorHelperModule.Behaviour_Lock(Product.AssetReplacementFeeBehaviour, txtAssetReplacementFee, cboAssetReplacementFee)
        BehaviorHelperModule.Behaviour_Lock(Product.RepossesFeeBehaviour, txtRepossesFee, cboRepossesFee) 
        BehaviorHelperModule.Behaviour_Lock(Product.LegalisirDocFeeBehaviour, txtLDFee, cboLDFee)
        BehaviorHelperModule.Behaviour_Lock(Product.PDCBounceFeeBehaviour, txtPDCBounceFee, cboPDCBounceFee) 
        BehaviorHelperModule.Behaviour_Lock(Product.InsAdminFeeBehaviour, txtInsuranceAdminFee, cboInsuranceAdminFee) 
        BehaviorHelperModule.Behaviour_Lock(Product.InsStampDutyFeeBehaviour, txtISDFee, cboISDFee) 
        BehaviorHelperModule.Behaviour_Lock(Product.BiayaPolisBehaviour, txtBiayaPolis, cboBiayaPolis)

    End Sub


    Sub bindBehaviour(ddl As DropDownList)
        ddl.DataValueField = "Value"
        ddl.DataTextField = "Text"
        ddl.DataSource = Parameter.Product.BehaviourItems
        ddl.DataBind()
    End Sub
End Class