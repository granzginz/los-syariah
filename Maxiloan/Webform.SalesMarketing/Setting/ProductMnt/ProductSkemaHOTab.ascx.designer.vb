﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class ProductSkemaHOTab

    '''<summary>
    '''lblMaximumTenor control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblMaximumTenor As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''E_txtMaximumTenor control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents E_txtMaximumTenor As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Label1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Label1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Hide_MaximumTenor control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Hide_MaximumTenor As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''Hide_MaximumTenorBeh control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Hide_MaximumTenorBeh As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''Requiredfieldvalidator3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Requiredfieldvalidator3 As Global.System.Web.UI.WebControls.RequiredFieldValidator

    '''<summary>
    '''Rangevalidator3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Rangevalidator3 As Global.System.Web.UI.WebControls.RangeValidator

    '''<summary>
    '''lblMaximumTenor_Branch control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblMaximumTenor_Branch As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lbltxtMinimumTenor control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbltxtMinimumTenor As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''E_txtMinimumTenor control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents E_txtMinimumTenor As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Label2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Label2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Hide_MinimumTenor control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Hide_MinimumTenor As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''Hide_MinimumTenorBeh control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Hide_MinimumTenorBeh As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''Requiredfieldvalidator2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Requiredfieldvalidator2 As Global.System.Web.UI.WebControls.RequiredFieldValidator

    '''<summary>
    '''Rangevalidator2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Rangevalidator2 As Global.System.Web.UI.WebControls.RangeValidator

    '''<summary>
    '''lblMinimumTenor_Branch control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblMinimumTenor_Branch As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblAngsuranPertama control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblAngsuranPertama As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''E_cboAngsuranPertama control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents E_cboAngsuranPertama As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Hide_AngsuranPertama control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Hide_AngsuranPertama As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblAngsuranPertama_Branch control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblAngsuranPertama_Branch As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblFlatRate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblFlatRate As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''E_txtFlatRate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents E_txtFlatRate As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Label3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Label3 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''E_cboFlatRateBehaviour control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents E_cboFlatRateBehaviour As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Hide_FlatRate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Hide_FlatRate As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''Hide_FlatRateBeh control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Hide_FlatRateBeh As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''RequiredFieldValidator67 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents RequiredFieldValidator67 As Global.System.Web.UI.WebControls.RequiredFieldValidator

    '''<summary>
    '''RangeValidator55 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents RangeValidator55 As Global.System.Web.UI.WebControls.RangeValidator

    '''<summary>
    '''lblFlatRate_Branch control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblFlatRate_Branch As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblEffectiveRate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblEffectiveRate As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''E_txtEffectiveRate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents E_txtEffectiveRate As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Label4 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Label4 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''E_cboEffectiveRateBehaviour control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents E_cboEffectiveRateBehaviour As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Hide_EffectiveRate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Hide_EffectiveRate As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''Hide_EffectiveRateBeh control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Hide_EffectiveRateBeh As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''rfvEffectiveRate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rfvEffectiveRate As Global.System.Web.UI.WebControls.RequiredFieldValidator

    '''<summary>
    '''rgv_EffectiveRate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rgv_EffectiveRate As Global.System.Web.UI.WebControls.RangeValidator

    '''<summary>
    '''lblEffectiveRate_Branch control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblEffectiveRate_Branch As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblOpsiUangMuka control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblOpsiUangMuka As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''E_cboOpsiUangMuka control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents E_cboOpsiUangMuka As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Hide_OpsiUangMuka control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Hide_OpsiUangMuka As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblOpsiUangMuka_Branch control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblOpsiUangMuka_Branch As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblDPPercentage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblDPPercentage As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''E_txtDPPercentage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents E_txtDPPercentage As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Label5 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Label5 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''E_cboDPPercentageBehaviour control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents E_cboDPPercentageBehaviour As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Hide_DPPercentage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Hide_DPPercentage As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''Hide_DPPercentageBeh control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Hide_DPPercentageBeh As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''Requiredfieldvalidator24 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Requiredfieldvalidator24 As Global.System.Web.UI.WebControls.RequiredFieldValidator

    '''<summary>
    '''rgv_DPPercentage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rgv_DPPercentage As Global.System.Web.UI.WebControls.RangeValidator

    '''<summary>
    '''lblDPPercentage_Branch control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblDPPercentage_Branch As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblUangMukaAngsuran control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblUangMukaAngsuran As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''txtUangMukaAngsuran control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtUangMukaAngsuran As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Label6 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Label6 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Requiredfieldvalidator68 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Requiredfieldvalidator68 As Global.System.Web.UI.WebControls.RequiredFieldValidator

    '''<summary>
    '''Rangevalidator56 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Rangevalidator56 As Global.System.Web.UI.WebControls.RangeValidator

    '''<summary>
    '''Hide_UangMukaAngsuran control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Hide_UangMukaAngsuran As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblUangMukaAngsuran_Branch control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblUangMukaAngsuran_Branch As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblPOExpirationDays control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPOExpirationDays As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''E_txtPOExpirationDays control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents E_txtPOExpirationDays As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Label7 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Label7 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''E_cboPOExpirationDaysBehaviour control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents E_cboPOExpirationDaysBehaviour As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Hide_POExpirationDays control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Hide_POExpirationDays As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''Hide_POExpirationDaysBeh control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Hide_POExpirationDaysBeh As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''Requiredfieldvalidator22 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Requiredfieldvalidator22 As Global.System.Web.UI.WebControls.RequiredFieldValidator

    '''<summary>
    '''rgv_POExpirationDays control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rgv_POExpirationDays As Global.System.Web.UI.WebControls.RangeValidator

    '''<summary>
    '''lblPOExpirationDays_Branch control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPOExpirationDays_Branch As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblCreditProtection control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblCreditProtection As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''E_txtCreditProtection control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents E_txtCreditProtection As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Label9 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Label9 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''E_cboCreditProtectionBehaviour control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents E_cboCreditProtectionBehaviour As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Hide_CreditProtection control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Hide_CreditProtection As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''Hide_CreditProtectionBeh control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Hide_CreditProtectionBeh As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''RequiredFieldValidator5 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents RequiredFieldValidator5 As Global.System.Web.UI.WebControls.RequiredFieldValidator

    '''<summary>
    '''RangeValidator5 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents RangeValidator5 As Global.System.Web.UI.WebControls.RangeValidator

    '''<summary>
    '''lblCreditProtection_Branch control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblCreditProtection_Branch As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblAsuransiJaminan control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblAsuransiJaminan As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''E_txtAsuransiJaminan control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents E_txtAsuransiJaminan As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Label11 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Label11 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''E_cboAsuransiJaminanBehaviour control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents E_cboAsuransiJaminanBehaviour As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Hide_AsuransiJaminan control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Hide_AsuransiJaminan As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''Hide_AsuransiJaminan_Beh control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Hide_AsuransiJaminan_Beh As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''RequiredFieldValidator1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents RequiredFieldValidator1 As Global.System.Web.UI.WebControls.RequiredFieldValidator

    '''<summary>
    '''RangeValidator1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents RangeValidator1 As Global.System.Web.UI.WebControls.RangeValidator

    '''<summary>
    '''lblAsuransiJaminan_Branch control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblAsuransiJaminan_Branch As Global.System.Web.UI.WebControls.Label
End Class
