﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ProductBranchHOView.aspx.vb"
    Inherits="Maxiloan.Webform.SalesMarketing.ProductBranchHOView" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="productUmumHOTab.ascx" TagName="umum" TagPrefix="uc1" %>  
<%@ Register Src="productSkemaHOTab.ascx" TagName="skema" TagPrefix="uc2" %>  
<%@ Register Src="productBiayaHOTab.ascx" TagName="biaya" TagPrefix="uc3" %>  
<%@ Register Src="productAngsuranHOTab.ascx" TagName="angs" TagPrefix="uc4" %>  
<%@ Register Src="productCollectionHOTab.ascx" TagName="coll" TagPrefix="uc5" %> 
<%@ Register Src="productFinanceHOTab.ascx" TagName="fin" TagPrefix="uc6" %>  

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ProductBranchHOView.aspx</title>
    <script language="JavaScript" type="text/javascript">
        function fClose() {
            window.close();
            return false;
        }
    </script>
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../../Include/General.css" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
        <uc1:umum id="UmumTab" runat="server"/>
        <uc2:skema id="SkemaTab" runat="server"/>
        <uc3:biaya id="BiayaTab" runat="server"/>
        <uc4:angs id="AngsuranTab" runat="server"/>
        <uc5:coll id="CollectionTab" runat="server"/>
        <uc6:fin id="FinanceTab" runat="server"/> 


    <asp:Panel ID="pnlEdit" HorizontalAlign="Center" runat="server">
       <%-- <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    VIEW - PRODUK CABANG
                </h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <asp:Label ID="E_lblBranchID" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    ID Produk</label>
                <asp:Label ID="E_lblProductID" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Nama Produk</label>
                <asp:Label ID="E_lblDescription" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box_title">
            <div class="form_single">
                <h4>
                    UMUM
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Jenis Asset</label>
                <asp:Label ID="E_lblAssetType" runat="server"></asp:Label>
            </div>
        </div>
       <div class="form_box" style="display:none;">
            <div class="form_single">
                <label>
                    Skema Score - Marketing</label>
                <asp:Label ID="E_lblScoreSchemeID" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Kredit Scoring</label>
                <asp:Label ID="E_lblCreditScoreSchemeID" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Skema Jurnal</label>
                <asp:Label ID="E_lblJournalSchemeID" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Skema Approval</label>
                <asp:Label ID="E_lblApprovalSchemeID" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Kondisi Asset</label>
                <asp:Label ID="E_lblAssetUsedNew" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Umur Kendaraan</label>
                <asp:Label ID="lblUmurKendaraanFrom" runat="server"></asp:Label>&nbsp;&nbsp;To&nbsp;&nbsp;
                <asp:Label ID="lblUmurKendaraanTo" runat="server"></asp:Label>
            </div>
        </div>--%>
       <%-- <div class="form_box">
            <div class="form_single">
                <label>
                    Jenis Finance</label>
                <asp:Label ID="E_lblFinanceType" runat="server"></asp:Label>
            </div>
        </div>--%>
      <%--  <div class="form_box">
            <div class="form_single">
                <label>
                    Kegiatan Usaha</label>
                <asp:Label ID="E_lblKegiatanUsaha" runat="server"></asp:Label>
            </div>
        </div>

        <div class="form_box">
            <div class="form_single">
                <label>
                    Jenis Pembiayaan</label>
                <asp:Label ID="E_lblJenisPembiayaan" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box_title">
            <div class="form_single">
                <h4>
                    SKEMA
                </h4>
            </div>
        </div>
        <div style="display:none;">
        <div class="form_box_header">
            <div class="form_single">
                <h5>
                    Gross Yield Rate
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Dari KP</label>
                <asp:Label ID="lblGrossYieldRate_HO" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <asp:Label ID="lblGrossYieldRate_Branch" runat="server"></asp:Label>
            </div>
        </div>
        </div>

         
        <div class="form_box_header">
            <div class="form_single">
                <h5>
                    Jangka Waktu Maximum
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Dari KP</label>
                <asp:Label ID="lblMaximumTenor_HO" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <asp:Label ID="lblMaximumTenor_Branch" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <h5>
                    Jangka Waktu Minimum
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Dari KP</label>
                <asp:Label ID="lblMinimumTenor_HO" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <asp:Label ID="lblMinimumTenor_Branch" runat="server"></asp:Label>
            </div>
        </div>
         
        <div class="form_box_header">
            <div class="form_single">
                <h5>
                    Angsuran Pertama
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Dari KP</label>
                <asp:Label ID="lblAngsuranPertama_HO" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <asp:Label ID="lblAngsuranPertama_Branch" runat="server"></asp:Label>
            </div>
        </div>

        <div class="form_box_header">
            <div class="form_single">
                <h5>
                    Suku Bunga Flat
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Dari KP</label>
                <asp:Label ID="lblSukuBungaFlat_HO" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <asp:Label ID="lblSukuBungaFlat_Branch" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <h5>
                    Suku Bunga Effective
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Dari KP</label>
                <asp:Label ID="lblEffectiveRate_HO" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <asp:Label ID="lblEffectiveRate_Branch" runat="server"></asp:Label>
            </div>
        </div>

          <div class="form_box_header">
            <div class="form_single">
                <h5>
                    Opsi Uang Muka
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Dari KP</label>
                <asp:Label ID="lblOpsiUangMuka_HO" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <asp:Label ID="lblOpsiUangMuka_Branch" runat="server"></asp:Label>
            </div>
        </div>

         <div class="form_box_header">
            <div class="form_single">
                <h5>
                    Uang Muka (%)
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Dari KP</label>
                <asp:Label ID="lblDPPercentage_HO" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <asp:Label ID="lblDPPercentage_Branch" runat="server"></asp:Label>
            </div>
        </div>
         
          <div class="form_box_header">
            <div class="form_single">
                <h5>
                   Uang Muka (Angsuran)
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Dari KP</label>
                <asp:Label ID="lblUangMukaAngsuran_HO" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <asp:Label ID="lblUangMukaAngsuran_Branch" runat="server"></asp:Label>
            </div>
        </div>
         <div class="form_box_header">
            <div class="form_single">
                <h5>
                    Masa Berlaku PO (dalam hari)
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Dari KP</label>
                <asp:Label ID="lblPOExpirationDays_HO" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <asp:Label ID="lblPOExpirationDays_Branch" runat="server"></asp:Label>
            </div>
        </div>

        <div class="form_box_title">
            <div class="form_single">
                <h4>
                    BIAYA
                </h4>
            </div>
        </div>

        <div class="form_box_header">
            <div class="form_single">
                <h5>
                    Biaya Pembatalan
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Dari KP</label>
                <asp:Label ID="lblCancellationFee_HO" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <asp:Label ID="lblCancellationFee_Branch" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <h5>
                    Biaya Administrasi
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Dari KP</label>
                <asp:Label ID="lblAdminFee_HO" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <asp:Label ID="lblAdminFee_Branch" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <h5>
                    Biaya Fidusia
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Dari KP</label>
                <asp:Label ID="lblFiduciaFee_HO" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <asp:Label ID="lblFiduciaFee_Branch" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <h5>
                    Biaya Provisi
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Dari KP</label>
                <asp:Label ID="lblProvisionFee_HO" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <asp:Label ID="lblProvisionFee_Branch" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <h5>
                    Biaya Notaris
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Dari KP</label>
                <asp:Label ID="lblNotaryFee_HO" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <asp:Label ID="lblNotaryFee_Branch" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <h5>
                    Biaya Survey
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Dari KP</label>
                <asp:Label ID="lblSurveyFee_HO" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <asp:Label ID="lblSurveyFee_Branch" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <h5>
                    Biaya Kunjungan
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Dari KP</label>
                <asp:Label ID="lblVisitFee_HO" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <asp:Label ID="lblVisitFee_Branch" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <h5>
                    Biaya Reschedulling
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Dari KP</label>
                <asp:Label ID="lblReschedulingFee_HO" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <asp:Label ID="lblReschedulingFee_Branch" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <h5>
                    Biaya Over Kontrak
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Dari KP</label>
                <asp:Label ID="lblAgreementTransferFee_HO" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <asp:Label ID="lblAgreementTransferFee_Branch" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <h5>
                    Biaya Ganti Tgl jatuh Tempo
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Dari KP</label>
                <asp:Label ID="lblChangeDueDateFee_HO" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <asp:Label ID="lblChangeDueDateFee_Branch" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <h5>
                    Biaya Penggantian Asset
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Dari KP</label>
                <asp:Label ID="lblAssetReplacementFee_HO" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <asp:Label ID="lblAssetReplacementFee_Branch" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <h5>
                    Biaya Penarikan
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Dari KP</label>
                <asp:Label ID="lblRepossesFee_HO" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <asp:Label ID="lblRepossesFee_Branch" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <h5>
                    Biaya Legalisasi Dokumen
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Dari KP</label>
                <asp:Label ID="lblLegalizedDocumentFee_HO" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <asp:Label ID="lblLegalizedDocumentFee_Branch" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <h5>
                    Biaya Penolakan PDC
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Dari KP</label>
                <asp:Label ID="lblPDCBounceFee_HO" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <asp:Label ID="lblPDCBounceFee_Branch" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <h5>
                    Biaya Admin Asuransi
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Dari KP</label>
                <asp:Label ID="lblInsuranceAdminFee_HO" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <asp:Label ID="lblInsuranceAdminFee_Branch" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <h5>
                    Biaya Materai Asuransi
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Dari KP</label>
                <asp:Label ID="lblInsuranceStampDutyFee_HO" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <asp:Label ID="lblInsuranceStampDutyFee_Branch" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <h5>
                    Biaya Polis
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Dari KP</label>
                <asp:Label ID="lblBiayaPolis_HO" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <asp:Label ID="lblBiayaPolis_Branch" runat="server"></asp:Label>
            </div>
        </div>





        <div class="form_box_hide">
            <div class="form_single">
                <h5>
                    Prosentase Denda keterlambatan (Asuransi)
                </h5>
            </div>
        </div>
        <div class="form_box_hide">
            <div class="form_single">
                <label>
                    Dari KP</label>
                <asp:Label ID="lblInsurancePenaltyPercentage_HO" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box_hide">
            <div class="form_single">
                <label>
                    Cabang</label>
                <asp:Label ID="lblInsurancePenaltyPercentage_Branch" runat="server"></asp:Label>
            </div>
        </div>--%>
<%--
        <div class="form_box_title">
            <div class="form_single">
                <h4>
                    ANGSURAN
                </h4>
            </div>
        </div>
               <div class="form_box_header">
            <div class="form_single">
                <h5>
                    Prioritas Alokasi Pembayaran
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Dari KP</label>
                <asp:Label ID="lblPrioritasPembayaran" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <asp:Label ID="lblPrioritasPembayaran_Branch" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <h5>
                    Jumlah Toleransi terhadap Angsuran
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Dari KP</label>
                <asp:Label ID="lblInstallmentToleranceAmount_HO" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <asp:Label ID="lblInstallmentToleranceAmount_Branch" runat="server"></asp:Label>
            </div>
        </div>
          <div class="form_box_header">
            <div class="form_single">
                <h5>
                    Prosentase Denda Keterlambatan (Angsuran)
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Dari KP</label>
                <asp:Label ID="lblPenaltyPercentage_HO" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <asp:Label ID="lblPenaltyPercentage_Branch" runat="server"></asp:Label>
            </div>
        </div>


        <div class="form_box_header">
            <div class="form_single">
                <h5>
                    Dasar Perhitungan</h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Dari KP</label>
                <asp:Label ID="lblPenaltyBasedOn" runat="server"></asp:Label>&nbsp;&nbsp;
                <asp:Label ID="lblPenaltyBasedOnBehaviour" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <asp:Label runat="server" ID="lblPenaltyBasedOn_Branch"></asp:Label>&nbsp;&nbsp;
                <asp:Label runat="server" ID="lblPenaltyBasedOnBehaviour_Branch"></asp:Label>
            </div>
        </div>

        
        <div class="form_box_header">
            <div class="form_single">
                <h5>
                    Tanggal Efektif</h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Dari KP</label>
                <asp:Label ID="lblPenaltyRateEffectiveDate" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <asp:Label runat="server" ID="lblPenaltyRateEffectiveDate_Branch"></asp:Label>
            </div>
        </div>

          <div class="form_box_header">
            <div class="form_single">
                <h5>
                    Prosentase Denda Keterlambatan Sebelumnya</h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Dari KP</label>
                <asp:Label ID="lblPenaltyRatePrevious" runat="server"></asp:Label>‰ / day
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <asp:Label runat="server" ID="lblPenaltyRatePrevious_Branch"></asp:Label>‰ / day
            </div>
        </div>

        <div class="form_box_header">
            <div class="form_single">
                <h5>
                    Biaya Tagih
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Dari KP</label>
                <asp:Label ID="lblBillingCharges_HO" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <asp:Label ID="lblBillingCharges_Branch" runat="server"></asp:Label>
            </div>
        </div>

             <div class="form_box_header">
            <div class="form_single">
                <h5>
                    Denda Pelunasan dipercepat
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Dari KP</label>
                <asp:Label ID="lblPrepaymentPenaltyRate_HO" runat="server"></asp:Label>‰
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <asp:Label ID="lblPrepaymentPenaltyRate_Branch" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <h5>
                    Tanggal Efektif 
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Dari KP</label>
                <asp:Label ID="lblPrepaymentPenaltyEffectiveDate" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <asp:Label runat="server" ID="lblPrepaymentPenaltyEffectiveDate_Branch"></asp:Label>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <h5>
                    Denda Pelunasan Sebelumnya
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Dari KP</label>
                <asp:Label ID="lblPrepaymentPenaltyPrevious" runat="server"></asp:Label>‰
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <asp:Label ID="lblPrepaymentPenaltyPrevious_Branch" runat="server"></asp:Label>‰
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <h5>
                    Denda Tetap Pelunasan dipercepat
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Dari KP</label>
                <asp:Label ID="lblPrepaymentPenaltyFixed" runat="server"></asp:Label>&nbsp;&nbsp;
                <asp:Label ID="lblPrepaymentPenaltyFixedBehaviour" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <asp:Label ID="lblPrepaymentPenaltyFixed_Branch" runat="server"></asp:Label>&nbsp;&nbsp;
                <asp:Label ID="lblPrepaymentPenaltyFixedBehaviour_Branch" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <h5>
                    Tanggal Efektif
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Dari KP</label>
                <asp:Label ID="lblPrepaymentPenaltyFixedEffectiveDate" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <asp:Label ID="lblPrepaymentPenaltyFixedEffectiveDate_Branch" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <h5>
                    Denda Tetap Pelunasan Sebelumnya
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Dari KP</label>
                <asp:Label ID="lblPrepaymentPenaltyFixedPrevious" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <asp:Label ID="lblPrepaymentPenaltyFixedPrevious_Branch" runat="server"></asp:Label>
            </div>
        </div>
<div style="display:none;">

        <div class="form_box_header">
            <div class="form_single">
                <h5>
                    Pendapatan Bunga Minimum yg harus ditolak
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Dari KP</label>
                <asp:Label ID="lblRejectMinimumIncome_HO" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <asp:Label ID="lblRejectMinimumIncome_Branch" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <h5>
                    Pendapatan Bunga Minimum yg harus diingatkan
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Dari KP</label>
                <asp:Label ID="lblWarningMinimumIncome_HO" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <asp:Label ID="lblWarningMinimumIncome_Branch" runat="server"></asp:Label>
            </div>
        </div>
</div>--%>
        <div class="form_box_title">
    <%--        <div class="form_single">
                <h4>
                    COLLECTION
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <h5>
                    Buat SP 1 Otomatis
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Dari KP</label>
                <asp:Label ID="lblLengthSPProcess_HO" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <asp:Label ID="lblLengthSPProcess_Branch" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <h5>
                    Buat SP 2 Otomatis
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Dari KP</label>
                <asp:Label ID="lblLengthSP1Process_HO" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <asp:Label ID="lblLengthSP1Process_Branch" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <h5>
                    Buat SP 3 Otomatis
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Dari KP</label>
                <asp:Label ID="lblLengthSP2Process_HO" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <asp:Label ID="lblLengthSP2Process_Branch" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <h5>
                    Jangka Waktu proses Dokumen Utama
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Dari KP</label>
                <asp:Label ID="lblLengthMainDocProcess_HO" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <asp:Label ID="lblLengthMainDocProcess_Branch" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <h5>
                    Jangka Waktu Dokumen Utama diambil
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Dari KP</label>
                <asp:Label ID="lblLengthMainDocTaken_HO" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <asp:Label ID="lblLengthMainDocTaken_Branch" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <h5>
                    Grace Period Denda Keterlambatan
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Dari KP</label>
                <asp:Label ID="lblGracePeriodLateCharges_HO" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <asp:Label ID="lblGracePeriodLateCharges_Branch" runat="server"></asp:Label>
            </div>
        </div>
   
        <div class="form_box_header">
            <div class="form_single">
                <h5>
                    Jumlah hari mengingatkan angsuran
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Dari KP</label>
                <asp:Label ID="lblDeskCollPhoneRemind_HO" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <asp:Label ID="lblDeskCollPhoneRemind_Branch" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <h5>
                    Jumlah hari Overdue untuk Deskcoll
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Dari KP</label>
                <asp:Label ID="lblDeskCollOD_HO" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <asp:Label ID="lblDeskCollOD_Branch" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <h5>
                    Overdue sebelumnya untuk mengingatkan
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Dari KP</label>
                <asp:Label ID="lblPrevODToRemind_HO" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <asp:Label ID="lblPrevODToRemind_Branch" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <h5>
                    Permintaan PDC untuk telepon
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Dari KP</label>
                <asp:Label ID="lblPDCDaysToRemind_HO" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <asp:Label ID="lblPDCDaysToRemind_Branch" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <h5>
                    Jumlah hari mengingatkan dgn SMS
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Dari KP</label>
                <asp:Label ID="lblDeskCollSMSRemind_HO" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <asp:Label ID="lblDeskCollSMSRemind_Branch" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <h5>
                    Jumlah hari buat DCR
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Dari KP</label>
                <asp:Label ID="lblDCR_HO" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <asp:Label ID="lblDCR_Branch" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <h5>
                    Jumlah hari sebelum JT buat Kwitansi
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Dari KP</label>
                <asp:Label ID="lblDaysBeforeDueToRN_HO" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <asp:Label ID="lblDaysBeforeDueToRN_Branch" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <h5>
                    Jumlah hari buat SKT (Surat Kuasa Tarik)
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Dari KP</label>
                <asp:Label ID="lblODToRAL_HO" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <asp:Label ID="lblODToRAL_Branch" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <h5>
                    Jangka Waktu SKT
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Dari KP</label>
                <asp:Label ID="lblRALPeriod_HO" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <asp:Label ID="lblRALPeriod_Branch" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <h5>
                    Jumlah hari Maximum Kwitansi dibayar
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Dari KP</label>
                <asp:Label ID="lblMaxDaysRNPaid_HO" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <asp:Label ID="lblMaxDaysRNPaid_Branch" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <h5>
                    Jumlah hari Maximum janji Bayar
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Dari KP</label>
                <asp:Label ID="lblMaxPTPYDays_HO" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <asp:Label ID="lblMaxPTPYDays_Branch" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <h5>
                    Janji Bayar ke Bank
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Dari KP</label>
                <asp:Label ID="lblPTPYBank_HO" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <asp:Label ID="lblPTPYBank_Branch" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <h5>
                    Janji bayar ke Perusahaan
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Dari KP</label>
                <asp:Label ID="lblPTPYCompany_HO" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <asp:Label ID="lblPTPYCompany_Branch" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <h5>
                    Janji bayar ke Supplier
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Dari KP</label>
                <asp:Label ID="lblPTPYSupplier_HO" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <asp:Label ID="lblPTPYSupplier_Branch" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <h5>
                    Perpanjangan SKT
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Dari KP</label>
                <asp:Label ID="lblRALExtension_HO" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <asp:Label ID="lblRALExtension_Branch" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <h5>
                    Jangka Waktu jadi Inventory
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Dari KP</label>
                <asp:Label ID="lblInventoryExpected_HO" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <asp:Label ID="lblInventoryExpected_Branch" runat="server"></asp:Label>
            </div>
        </div>
--%>
       <%-- <div class="form_box_title">
            <div class="form_single">
                <h4>
                    FINANCE
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <h5>
                    Limit Pembayaran di Cabang
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Dari KP</label>
                <asp:Label ID="lblLimitAPCash_HO" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <asp:Label ID="lblLimitAPCash_Branch" runat="server"></asp:Label>
            </div>
        </div>

              <div class="form_box">
            <div class="form_single">
                <label>
                    Recourse</label>
                <asp:Label ID="E_lblIsRecourse" runat="server"></asp:Label>
            </div>
        </div>
        
        <div class="form_box_header">
            <div class="form_single">
                <h5>
                    Diskon Asuransi
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Dari KP</label>
                <asp:Label ID="lblInsuranceDiscPercentage" runat="server"></asp:Label>&nbsp;%&nbsp;
                <asp:Label ID="lblInsuranceDiscPercentageBehaviour" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <asp:Label ID="lblInsuranceDiscPercentage_Branch" runat="server"></asp:Label>%&nbsp;&nbsp;
                <asp:Label ID="lblInsuranceDiscPercentageBehaviour_Branch" runat="server"></asp:Label>
            </div>
        </div>
        
  
        <div class="form_box">
            <div class="form_single">
                <label>
                    Aktif</label>
                <asp:Label ID="E_lblIsActive" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonClose" OnClientClick="fClose();" runat="server" CausesValidation="true"
                Text="Close" CssClass="small button gray"></asp:Button>
        </div>--%>

    </asp:Panel>
    </form>
</body>
</html>
