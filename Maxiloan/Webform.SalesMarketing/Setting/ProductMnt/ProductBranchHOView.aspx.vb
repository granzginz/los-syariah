﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
#End Region

Public Class ProductBranchHOView
    Inherits Maxiloan.Webform.WebBased
    Private m_controller As New ProductController

#Region "Property"
    Private Property ProductID() As String
        Get
            Return CType(viewstate("ProductID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ProductID") = Value
        End Set
    End Property

    Private Property Branch_ID() As String
        Get
            Return CType(viewstate("Branch_ID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Branch_ID") = Value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not IsPostBack Then
            Me.ProductID = Request("ProductID")
            Me.Branch_ID = Request("BranchID")
            bind()
        End If        
    End Sub
#Region "Behaviour_Value"
    Function Behaviour_Value(ByVal Behaviour As String) As String
        If Behaviour = "D" Then
            Return "Default"
        ElseIf Behaviour = "L" Then
            Return "Locked"
        ElseIf Behaviour = "N" Then
            Return "Minimum"
        ElseIf Behaviour = "X" Then
            Return "Maximum"
        End If
    End Function
#End Region

#Region "Bind"
    Sub bind()
        Dim oProduct As New Parameter.Product

        oProduct.ProductId = Me.ProductID
        oProduct.Branch_ID = Me.Branch_ID
        oProduct.strConnection = GetConnectionString
        oProduct = m_controller.ProductBranchHOEdit(oProduct)

        oProduct.ProductId = Me.ProductID
        oProduct.Branch_ID = Me.Branch_ID

        UmumTab.Product = oProduct
        UmumTab.UCMode = "VIEW"
        UmumTab.UCProductName = "PRODUK CABANG"
        UmumTab.InitUCViewMode()

        SkemaTab.Product = oProduct
        SkemaTab.UCMode = "VIEW"
        SkemaTab.InitUCViewMode()

        BiayaTab.Product = oProduct
        BiayaTab.UCMode = "VIEW"
        BiayaTab.InitUCViewMode()

        AngsuranTab.Product = oProduct
        AngsuranTab.UCMode = "VIEW"
        AngsuranTab.InitUCViewMode()

        CollectionTab.Product = oProduct
        CollectionTab.UCMode = "VIEW"
        CollectionTab.InitUCViewMode()

        FinanceTab.Product = oProduct
        FinanceTab.UCMode = "VIEW"
        FinanceTab.InitUCViewMode()

    End Sub
#End Region

End Class


        'E_lblBranchID.Text = oProduct.BranchFullName
        'E_lblProductID.Text = Me.ProductID
        'E_lblDescription.Text = oProduct.Description
        'E_lblAssetType.Text = oProduct.AssetType_desc
        'E_lblScoreSchemeID.Text = oProduct.ScoreSchemeMaster_desc
        'E_lblCreditScoreSchemeID.Text = oProduct.CreditScoreSchemeMaster_desc
        'E_lblJournalSchemeID.Text = oProduct.JournalScheme_desc
        'E_lblApprovalSchemeID.Text = oProduct.ApprovalTypeScheme_desc

        'E_lblAssetUsedNew.Text = IIf(oProduct.AssetUsedNew = "U", "Used", "New")

        'If oProduct.FinanceType = "CF" Then
        '    E_lblFinanceType.Text = "Consumer Finance"
        'ElseIf oProduct.AssetUsedNew = "L" Then
        '    E_lblFinanceType.Text = "Leasing"
        'End If

        'E_lblKegiatanUsaha.Text = oProduct.KegiatanUsahaDesc
        'E_lblJenisPembiayaan.Text = oProduct.JenisPembiayaanDesc

        'lblAngsuranPertama_HO.Text = IIf(oProduct.AngsuranPertama_HO = "ARR", "ARREAR", "ADVANCE")
        'lblAngsuranPertama_Branch.Text = IIf(oProduct.AngsuranPertama = "ARR", "ARREAR", "ADVANCE")

        'lblSukuBungaFlat_HO.Text = String.Format("{0} % {1}", FormatNumber(oProduct.SukuBungaFlat_HO, 6), Behaviour_Value(oProduct.SukuBungaFlatBehaviour_HO))
        'lblSukuBungaFlat_Branch.Text = String.Format("{0} % {1}", FormatNumber(oProduct.SukuBungaFlat, 6), Behaviour_Value(oProduct.SukuBungaFlatBehaviour))

        'lblOpsiUangMuka_HO.Text = IIf(oProduct.OpsiUangMuka_HO = "P", "PROSENTASE", "ANGSURAN")
        'lblOpsiUangMuka_Branch.Text = IIf(oProduct.OpsiUangMuka = "P", "PROSENTASE", "ANGSURAN")

        'lblUangMukaAngsuran_HO.Text = String.Format("{0} x", FormatNumber(oProduct.OpsiUangMukaAngsur_HO, 6))
        'lblUangMukaAngsuran_Branch.Text = String.Format("{0} x", FormatNumber(oProduct.OpsiUangMukaAngsur, 6))
        ''====
        'lblUmurKendaraanFrom.Text = oProduct.UmurKendaraanFrom_HO.ToString
        'lblUmurKendaraanTo.Text = oProduct.UmurKendaraanTo_HO.ToString

        'lblPenaltyBasedOn.Text = IIf(oProduct.PenaltyBasedOn_HO = "PB", "Pokok + Bunga", "Pokok") 


        'lblPenaltyBasedOnBehaviour.Text = Behaviour_Value(oProduct.PenaltyBasedOnBehaviour_HO) 

        'lblPenaltyRatePrevious.Text = oProduct.PenaltyRatePrevious_HO.ToString
        'lblPenaltyRateEffectiveDate.Text = oProduct.PenaltyRateEffectiveDate_HO.ToString("dd/MM/yyyy")


        'lblPrepaymentPenaltyFixed.Text = oProduct.PrepaymentPenaltyFixed_HO.ToString


        'lblPrepaymentPenaltyFixedBehaviour.Text = Behaviour_Value(oProduct.PrepaymentPenaltyFixedBehaviour_HO)


        'lblPrepaymentPenaltyPrevious.Text = oProduct.PrepaymentPenaltyPrevious_HO.ToString
        'lblPrepaymentPenaltyEffectiveDate.Text = oProduct.PrepaymentPenaltyEffectiveDate_HO.ToString("dd/MM/yyyy")
        'lblPrepaymentPenaltyFixedEffectiveDate.Text = oProduct.PrepaymentPenaltyFixedEffectiveDate_HO.ToString("dd/MM/yyyy")
        'lblPrepaymentPenaltyFixedPrevious.Text = oProduct.PrepaymentPenaltyFixedPrevious_HO.ToString

        'lblPrioritasPembayaran.Text = IIf(oProduct.PrioritasPembayaran_HO = "DE", "Denda", "Bebas")



        'lblPenaltyBasedOn_Branch.Text = IIf(oProduct.PenaltyBasedOn = "PB", "Pokok + Bunga", "Pokok") 

        '    lblPenaltyBasedOnBehaviour_Branch.Text = Behaviour_Value( oProduct.PenaltyBasedOnBehaviour)

        'lblPenaltyRatePrevious_Branch.Text = oProduct.PenaltyRatePrevious.ToString
        'lblPenaltyRateEffectiveDate_Branch.Text = oProduct.PenaltyRateEffectiveDate.ToString("dd/MM/yyyy")


        'lblPrepaymentPenaltyFixed_Branch.Text = oProduct.PrepaymentPenaltyFixed.ToString

        'lblPrepaymentPenaltyFixedBehaviour_Branch.Text = Behaviour_Value( oProduct.PrepaymentPenaltyFixedBehaviour )

        'lblPrepaymentPenaltyPrevious_Branch.Text = oProduct.PrepaymentPenaltyPrevious.ToString
        'lblPrepaymentPenaltyEffectiveDate_Branch.Text = oProduct.PrepaymentPenaltyEffectiveDate.ToString("dd/MM/yyyy")
        'lblPrepaymentPenaltyFixedEffectiveDate_Branch.Text = oProduct.PrepaymentPenaltyFixedEffectiveDate.ToString("dd/MM/yyyy")
        'lblPrepaymentPenaltyFixedPrevious_Branch.Text = oProduct.PrepaymentPenaltyFixedPrevious.ToString

        'lblPrioritasPembayaran_Branch.Text = IIf(oProduct.PrioritasPembayaran = "DE", "Denda", "Bebas")
      
         
        '---------------------------

        'lblEffectiveRate_HO.Text = CStr(FormatNumber(oProduct.EffectiveRate_HO, 6)) & " % " & Behaviour_Value(oProduct.EffectiveRateBehaviour_HO)
        'lblEffectiveRate_Branch.Text = CStr(FormatNumber(oProduct.EffectiveRate, 6)) & " % " & Behaviour_Value(oProduct.EffectiveRateBehaviour)

        'lblGrossYieldRate_HO.Text = CStr(FormatNumber(oProduct.GrossYieldRate_HO, 6)) & " % " & Behaviour_Value(oProduct.GrossYieldRateBehaviour_HO)
        'lblGrossYieldRate_Branch.Text = CStr(FormatNumber(oProduct.GrossYieldRate, 6)) & " % " & Behaviour_Value(oProduct.GrossYieldRateBehaviour)

        'lblMinimumTenor_HO.Text = CStr(oProduct.MinimumTenor_HO) & " months"
        'lblMinimumTenor_Branch.Text = CStr(oProduct.MinimumTenor) & " months"

        'lblMaximumTenor_HO.Text = CStr(oProduct.MaximumTenor_HO) & " months"
        'lblMaximumTenor_Branch.Text = CStr(oProduct.MaximumTenor) & " months"

        'lblPenaltyPercentage_HO.Text = CStr(FormatNumber(oProduct.PenaltyPercentage_HO, 2)) & " ‰ / day " & Behaviour_Value(oProduct.PenaltyPercentageBehaviour_HO)
        'lblPenaltyPercentage_Branch.Text = CStr(FormatNumber(oProduct.PenaltyPercentage, 2)) & " ‰ / day " & Behaviour_Value(oProduct.PenaltyPercentageBehaviour)

        'lblInsurancePenaltyPercentage_HO.Text = CStr(oProduct.InsurancePenaltyPercentage_HO) & " ‰ / day " & Behaviour_Value(oProduct.InsurancePenaltyPercentageBehaviour_HO)
        'lblInsurancePenaltyPercentage_Branch.Text = CStr(oProduct.InsurancePenaltyPercentage) & " ‰ / day " & Behaviour_Value(oProduct.InsurancePenaltyPercentageBehaviour)

        'lblCancellationFee_HO.Text = FormatNumber(CStr(oProduct.CancellationFee_HO).Trim, 2) & " " & Behaviour_Value(oProduct.CancellationFeeBehaviour_HO)
        'lblCancellationFee_Branch.Text = FormatNumber(CStr(oProduct.CancellationFee).Trim, 2) & " " & Behaviour_Value(oProduct.CancellationFeeBehaviour)

        'lblAdminFee_HO.Text = FormatNumber(CStr(oProduct.AdminFee_HO).Trim, 2) & " " & Behaviour_Value(oProduct.AdminFeeBehaviour_HO)
        'lblAdminFee_Branch.Text = FormatNumber(CStr(oProduct.AdminFee).Trim, 2) & " " & Behaviour_Value(oProduct.AdminFeeBehaviour)

        'lblFiduciaFee_HO.Text = FormatNumber(CStr(oProduct.FiduciaFee_HO).Trim, 2) & " " & Behaviour_Value(oProduct.FiduciaFeeBehaviour_HO)
        'lblFiduciaFee_Branch.Text = FormatNumber(CStr(oProduct.FiduciaFee).Trim, 2) & " " & Behaviour_Value(oProduct.FiduciaFeeBehaviour)

        'lblProvisionFee_HO.Text = FormatNumber(CStr(oProduct.ProvisionFeeHO).Trim, 2) & " " & Behaviour_Value(oProduct.ProvisionFeeBehaviour_HO)
        'lblProvisionFee_Branch.Text = FormatNumber(CStr(oProduct.ProvisionFee).Trim, 2) & " " & Behaviour_Value(oProduct.ProvisionFeeBehaviour)

        'lblNotaryFee_HO.Text = FormatNumber(CStr(oProduct.NotaryFee_HO).Trim, 2) & " " & Behaviour_Value(oProduct.NotaryFeeBehaviour_HO)
        'lblNotaryFee_Branch.Text = FormatNumber(CStr(oProduct.NotaryFee).Trim, 2) & " " & Behaviour_Value(oProduct.NotaryFeeBehaviour)

        'lblSurveyFee_HO.Text = FormatNumber(CStr(oProduct.SurveyFee_HO).Trim, 2) & " " & Behaviour_Value(oProduct.SurveyFeeBehaviour_HO)
        'lblSurveyFee_Branch.Text = FormatNumber(CStr(oProduct.SurveyFee).Trim, 2) & " " & Behaviour_Value(oProduct.SurveyFeeBehaviour)


        'lblVisitFee_HO.Text = FormatNumber(CStr(oProduct.VisitFee_HO).Trim, 2) & " " & Behaviour_Value(oProduct.VisitFeeBehaviour_HO)
        'lblVisitFee_Branch.Text = FormatNumber(CStr(oProduct.VisitFee).Trim, 2) & " " & Behaviour_Value(oProduct.VisitFeeBehaviour)

        'lblReschedulingFee_HO.Text = FormatNumber(CStr(oProduct.ReschedulingFee_HO).Trim, 2) & " " & Behaviour_Value(oProduct.ReschedulingFeeBehaviour_HO)
        'lblReschedulingFee_Branch.Text = FormatNumber(CStr(oProduct.ReschedulingFee).Trim, 2) & " " & Behaviour_Value(oProduct.ReschedulingFeeBehaviour)

        'lblAgreementTransferFee_HO.Text = FormatNumber(CStr(oProduct.AgreementTransferFee_HO).Trim, 2) & " " & Behaviour_Value(oProduct.AgreementTransferFeeBehaviour_HO)
        'lblAgreementTransferFee_Branch.Text = FormatNumber(CStr(oProduct.AgreementTransferFee).Trim, 2) & " " & Behaviour_Value(oProduct.AgreementTransferFeeBehaviour)

        'lblChangeDueDateFee_HO.Text = FormatNumber(CStr(oProduct.ChangeDueDateFee_HO).Trim, 2) & " " & Behaviour_Value(oProduct.ChangeDueDateFeeBehaviour_HO)
        'lblChangeDueDateFee_Branch.Text = FormatNumber(CStr(oProduct.ChangeDueDateFee).Trim, 2) & " " & Behaviour_Value(oProduct.ChangeDueDateFeeBehaviour)

        'lblAssetReplacementFee_HO.Text = FormatNumber(CStr(oProduct.AssetReplacementFee_HO).Trim, 2) & " " & Behaviour_Value(oProduct.AssetReplacementFeeBehaviour_HO)
        'lblAssetReplacementFee_Branch.Text = FormatNumber(CStr(oProduct.AssetReplacementFee).Trim, 2) & " " & Behaviour_Value(oProduct.AssetReplacementFeeBehaviour)

        'lblRepossesFee_HO.Text = FormatNumber(CStr(oProduct.RepossesFee_HO).Trim, 2) & " " & Behaviour_Value(oProduct.RepossesFeeBehaviour_HO)
        'lblRepossesFee_Branch.Text = FormatNumber(CStr(oProduct.RepossesFee).Trim, 2) & " " & Behaviour_Value(oProduct.RepossesFeeBehaviour)

        'lblLegalizedDocumentFee_HO.Text = FormatNumber(CStr(oProduct.LegalisirDocFee_HO).Trim, 2) & " " & Behaviour_Value(oProduct.LegalisirDocFeeBehaviour_HO)
        'lblLegalizedDocumentFee_Branch.Text = FormatNumber(CStr(oProduct.LegalisirDocFee).Trim, 2) & " " & Behaviour_Value(oProduct.LegalisirDocFeeBehaviour)

        'lblPDCBounceFee_HO.Text = FormatNumber(CStr(oProduct.PDCBounceFee_HO).Trim, 2) & " " & Behaviour_Value(oProduct.PDCBounceFeeBehaviour_HO)
        'lblPDCBounceFee_Branch.Text = FormatNumber(CStr(oProduct.PDCBounceFee).Trim, 2) & " " & Behaviour_Value(oProduct.PDCBounceFeeBehaviour)

        'lblInsuranceAdminFee_HO.Text = FormatNumber(CStr(oProduct.InsAdminFee_HO).Trim, 2) & " " & Behaviour_Value(oProduct.InsAdminFeeBehaviour_HO)
        'lblInsuranceAdminFee_Branch.Text = FormatNumber(CStr(oProduct.InsAdminFee).Trim, 2) & " " & Behaviour_Value(oProduct.InsAdminFeeBehaviour)

        'lblInsuranceStampDutyFee_HO.Text = FormatNumber(CStr(oProduct.InsStampDutyFee_HO).Trim, 2) & " " & Behaviour_Value(oProduct.InsStampDutyFeeBehaviour_HO)
        'lblInsuranceStampDutyFee_Branch.Text = FormatNumber(CStr(oProduct.InsStampDutyFee).Trim, 2) & " " & Behaviour_Value(oProduct.InsStampDutyFeeBehaviour)

        'lblBiayaPolis_HO.Text = FormatNumber(CStr(oProduct.BiayaPolis_HO).Trim, 2) & " " & Behaviour_Value(oProduct.BiayaPolisBehaviour_HO)
        'lblBiayaPolis_Branch.Text = FormatNumber(CStr(oProduct.BiayaPolis).Trim, 2) & " " & Behaviour_Value(oProduct.BiayaPolisBehaviour)

        'lblPOExpirationDays_HO.Text = CStr(oProduct.DaysPOExpiration_HO) & " days " & Behaviour_Value(oProduct.DaysPOExpirationBehaviour_HO)
        'lblPOExpirationDays_Branch.Text = CStr(oProduct.DaysPOExpiration) & " days " & Behaviour_Value(oProduct.DaysPOExpirationBehaviour)

        'lblInstallmentToleranceAmount_HO.Text = FormatNumber(CStr(oProduct.InstallmentToleranceAmount_HO).Trim, 2) & " " & Behaviour_Value(oProduct.InstallmentToleranceAmountBehaviour_HO)
        'lblInstallmentToleranceAmount_Branch.Text = FormatNumber(CStr(oProduct.InstallmentToleranceAmount).Trim, 2) & " " & Behaviour_Value(oProduct.InstallmentToleranceAmountBehaviour)

        'lblDPPercentage_HO.Text = FormatNumber(CStr(oProduct.DPPercentage_HO).Trim, 2) & " " & Behaviour_Value(oProduct.DPPercentageBehaviour_HO)
        'lblDPPercentage_Branch.Text = FormatNumber(CStr(oProduct.DPPercentage).Trim, 2) & " " & Behaviour_Value(oProduct.DPPercentageBehaviour)

        'lblRejectMinimumIncome_HO.Text = FormatNumber(CStr(oProduct.RejectMinimumIncome_HO).Trim, 2) & " " & Behaviour_Value(oProduct.RejectMinimumIncomeBehaviour_HO)
        'lblRejectMinimumIncome_Branch.Text = FormatNumber(CStr(oProduct.RejectMinimumIncome).Trim, 2) & " " & Behaviour_Value(oProduct.RejectMinimumIncomeBehaviour)

        'lblWarningMinimumIncome_HO.Text = FormatNumber(CStr(oProduct.WarningMinimumIncome_HO).Trim, 2) & " " & Behaviour_Value(oProduct.WarningMinimumIncomeBehaviour_HO)
        'lblWarningMinimumIncome_Branch.Text = FormatNumber(CStr(oProduct.WarningMinimumIncome).Trim, 2) & " " & Behaviour_Value(oProduct.WarningMinimumIncomeBehaviour)

        'Dim strIsSPAutomatic_HO As String = IIf(oProduct.IsSPAutomatic_HO = True, "1", "0") 

        'Dim strIsSP1Automatic_HO As String=IIf (oProduct.IsSP1Automatic_HO = True , "1" ,"0")

        'Dim strIsSP2Automatic_HO As String = IIf(oProduct.IsSP2Automatic_HO = True, "1", "0") 

        'Dim strIsSPAutomatic As String = IIf(oProduct.IsSPAutomatic = True, "Yes", "No") 

        'Dim strIsSP1Automatic As String=IIf (oProduct.IsSP1Automatic = True ,"Yes","No")

        'Dim strIsSP2Automatic As String = IIf(oProduct.IsSP2Automatic = True, "Yes", "No") 

        'lblLengthSPProcess_HO.Text = strIsSPAutomatic_HO & " Length SP Process " & CStr(oProduct.LengthSPProcess_HO) & " days"
        'lblLengthSPProcess_Branch.Text = strIsSPAutomatic & " Length SP Process " & CStr(oProduct.LengthSPProcess) & " days"

        'lblLengthSP1Process_HO.Text = strIsSP1Automatic_HO & " Length SP 1 Process " & CStr(oProduct.LengthSP1Process_HO) & " days"
        'lblLengthSP1Process_Branch.Text = strIsSP1Automatic & " Length SP 1 Process " & CStr(oProduct.LengthSP1Process) & " days"

        'lblLengthSP2Process_HO.Text = strIsSP2Automatic_HO & " Length SP 2 Process " & CStr(oProduct.LengthSP2Process_HO) & " days"
        'lblLengthSP2Process_Branch.Text = strIsSP2Automatic & " Length SP 2 Process " & CStr(oProduct.LengthSP2Process) & " days"

        'lblLengthMainDocProcess_HO.Text = CStr(oProduct.LengthMainDocProcess_HO) & " days " & Behaviour_Value(oProduct.LengthMainDocProcessBehaviour_HO)
        'lblLengthMainDocProcess_Branch.Text = CStr(oProduct.LengthMainDocProcess) & " days " & Behaviour_Value(oProduct.LengthMainDocProcessBehaviour)

        'lblLengthMainDocTaken_HO.Text = CStr(oProduct.LengthMainDocTaken_HO) & " days " & Behaviour_Value(oProduct.LengthMainDocTakenBehaviour_HO)
        'lblLengthMainDocTaken_Branch.Text = CStr(oProduct.LengthMainDocTaken) & " days " & Behaviour_Value(oProduct.LengthMainDocTakenBehaviour)

        'lblGracePeriodLateCharges_HO.Text = CStr(oProduct.GracePeriodLateCharges_HO) & " days " & Behaviour_Value(oProduct.GracePeriodLateChargesBehaviour_HO)
        'lblGracePeriodLateCharges_Branch.Text = CStr(oProduct.GracePeriodLateCharges) & " days " & Behaviour_Value(oProduct.GracePeriodLateChargesBehaviour)

        ''lblPrepaymentPenaltyRate_HO.Text = CStr(FormatNumber(oProduct.PrepaymentPenaltyRate_HO, 6)) & " % " & Behaviour_Value(oProduct.PrepaymentPenaltyRateBehaviour_HO)
        ''lblPrepaymentPenaltyRate_Branch.Text = CStr(FormatNumber(oProduct.PrepaymentPenaltyRate, 6)) & " % " & Behaviour_Value(oProduct.PrepaymentPenaltyRateBehaviour)

        'lblDeskCollPhoneRemind_HO.Text = CStr(oProduct.DeskCollPhoneRemind_HO) & " days " & Behaviour_Value(oProduct.DeskCollPhoneRemindBehaviour_HO)
        'lblDeskCollPhoneRemind_Branch.Text = CStr(oProduct.DeskCollPhoneRemind) & " days " & Behaviour_Value(oProduct.DeskCollPhoneRemindBehaviour)

        'lblDeskCollOD_HO.Text = CStr(oProduct.DeskCollOD_HO) & " days " & Behaviour_Value(oProduct.DeskCollODBehaviour_HO)
        'lblDeskCollOD_Branch.Text = CStr(oProduct.DeskCollOD) & " days " & Behaviour_Value(oProduct.DeskCollODBehaviour)

        'lblPrevODToRemind_HO.Text = CStr(oProduct.PrevODToRemind_HO) & " days " & Behaviour_Value(oProduct.PrevODToRemindBehaviour_HO)
        'lblPrevODToRemind_Branch.Text = CStr(oProduct.PrevODToRemind) & " days " & Behaviour_Value(oProduct.PrevODToRemindBehaviour)

        'lblPDCDaysToRemind_HO.Text = CStr(oProduct.PDCDayToRemind_HO) & " Dayss " & Behaviour_Value(oProduct.PDCDayToRemindBehaviour_HO)
        'lblPDCDaysToRemind_Branch.Text = CStr(oProduct.PDCDayToRemind) & " Dayss " & Behaviour_Value(oProduct.PDCDayToRemindBehaviour)

        'lblDeskCollSMSRemind_HO.Text = CStr(oProduct.DeskCollSMSRemind_HO) & " days " & Behaviour_Value(oProduct.DeskCollSMSRemindBehaviour_HO)
        'lblDeskCollSMSRemind_Branch.Text = CStr(oProduct.DeskCollSMSRemind) & " days " & Behaviour_Value(oProduct.DeskCollSMSRemindBehaviour)

        'lblDCR_HO.Text = CStr(oProduct.DCR_HO) & " days " & Behaviour_Value(oProduct.DCRBehaviour_HO)
        'lblDCR_Branch.Text = CStr(oProduct.DCR) & " days " & Behaviour_Value(oProduct.DCRBehaviour)

        'lblDaysBeforeDueToRN_HO.Text = CStr(oProduct.DaysBeforeDueToRN_HO) & " days " & Behaviour_Value(oProduct.DaysBeforeDueToRNBehaviour_HO)
        'lblDaysBeforeDueToRN_Branch.Text = CStr(oProduct.DaysBeforeDueToRN) & " days " & Behaviour_Value(oProduct.DaysBeforeDueToRNBehaviour)

        'lblODToRAL_HO.Text = CStr(oProduct.ODToRAL_HO) & " days " & Behaviour_Value(oProduct.ODToRALBehaviour_HO)
        'lblODToRAL_Branch.Text = CStr(oProduct.ODToRAL) & " days " & Behaviour_Value(oProduct.ODToRALBehaviour)

        'lblRALPeriod_HO.Text = CStr(oProduct.RALPeriod_HO) & " days " & Behaviour_Value(oProduct.RALPeriodBehaviour_HO)
        'lblRALPeriod_Branch.Text = CStr(oProduct.RALPeriod) & " days " & Behaviour_Value(oProduct.RALPeriodBehaviour)

        'lblMaxDaysRNPaid_HO.Text = CStr(oProduct.MaxDaysRNPaid_HO) & " days " & Behaviour_Value(oProduct.MaxDaysRNPaidBehaviour_HO)
        'lblMaxDaysRNPaid_Branch.Text = CStr(oProduct.MaxDaysRNPaid) & " days " & Behaviour_Value(oProduct.MaxDaysRNPaidBehaviour)

        'lblMaxPTPYDays_HO.Text = CStr(oProduct.MaxPTPYDays_HO) & " days " & Behaviour_Value(oProduct.MaxPTPYDaysBehaviour_HO)
        'lblMaxPTPYDays_Branch.Text = CStr(oProduct.MaxPTPYDays) & " days " & Behaviour_Value(oProduct.MaxPTPYDaysBehaviour)

        'lblPTPYBank_HO.Text = CStr(oProduct.PTPYBank_HO) & " days " & Behaviour_Value(oProduct.PTPYBankBehaviour_HO)
        'lblPTPYBank_Branch.Text = CStr(oProduct.PTPYBank) & " days " & Behaviour_Value(oProduct.PTPYBankBehaviour)

        'lblPTPYCompany_HO.Text = CStr(oProduct.PTPYCompany_HO) & " days " & Behaviour_Value(oProduct.PTPYCompanyBehaviour_HO)
        'lblPTPYCompany_Branch.Text = CStr(oProduct.PTPYCompany) & " days " & Behaviour_Value(oProduct.PTPYCompanyBehaviour)

        'lblPTPYSupplier_HO.Text = CStr(oProduct.PTPYSupplier_HO) & " days " & Behaviour_Value(oProduct.PTPYSupplierBehaviour_HO)
        'lblPTPYSupplier_Branch.Text = CStr(oProduct.PTPYSupplier) & " days " & Behaviour_Value(oProduct.PTPYSupplierBehaviour)

        'lblRALExtension_HO.Text = CStr(oProduct.RALExtension_HO) & " days " & Behaviour_Value(oProduct.RALExtensionBehaviour_HO)
        'lblRALExtension_Branch.Text = CStr(oProduct.RALExtension) & " days " & Behaviour_Value(oProduct.RALExtensionBehaviour)

        'lblInventoryExpected_HO.Text = CStr(oProduct.InventoryExpected_HO) & " days " & Behaviour_Value(oProduct.InventoryExpectedBehaviour_HO)
        'lblInventoryExpected_Branch.Text = CStr(oProduct.InventoryExpected) & " days " & Behaviour_Value(oProduct.InventoryExpectedBehaviour)

        'lblBillingCharges_HO.Text = FormatNumber(CStr(oProduct.BillingCharges_HO).Trim, 2) & "  " & Behaviour_Value(oProduct.BillingChargesBehaviour_HO)
        'lblBillingCharges_Branch.Text = FormatNumber(CStr(oProduct.BillingCharges).Trim, 2) & "  " & Behaviour_Value(oProduct.BillingChargesBehaviour)


        'lblInsuranceDiscPercentage.Text = oProduct.InsuranceDiscPercentage_HO.ToString
        'lblInsuranceDiscPercentageBehaviour.Text = Behaviour_Value(oProduct.InsuranceDiscPercentageBehaviour_HO)
        'lblInsuranceDiscPercentage_Branch.Text = oProduct.InsuranceDiscPercentage.ToString

        'lblInsuranceDiscPercentageBehaviour_Branch.Text = Behaviour_Value(oProduct.InsuranceDiscPercentageBehaviour)
        'lblLimitAPCash_HO.Text = FormatNumber(CStr(oProduct.LimitAPCash_HO).Trim, 2) & "  " & Behaviour_Value(oProduct.LimitAPCashBehaviour_HO)
        'lblLimitAPCash_Branch.Text = FormatNumber(CStr(oProduct.LimitAPCash).Trim, 2) & "  " & Behaviour_Value(oProduct.LimitAPCashBehaviour)

        'E_lblIsRecourse.Text = IIf(oProduct.IsRecourse = True, "Yes", "No")

        'E_lblIsActive.Text = IIf(oProduct.IsActive = True, "Yes", "No")

