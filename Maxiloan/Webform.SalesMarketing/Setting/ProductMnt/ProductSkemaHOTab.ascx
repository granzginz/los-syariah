﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ProductSkemaHOTab.ascx.vb" Inherits="Maxiloan.Webform.SalesMarketing.ProductSkemaHOTab" %>

 
<%--  <div class="form_box_hide">
<div class="form_box_header form_box_hide">
<div class="form_single"> <h5> Gross Yield Rate </h5> </div> </div>
 <div class="form_box_hide">
<div class="form_single">
<label class="label_req"> From HO</label>
<asp:TextBox onblur="GrossYieldRate();" ID="E_txtGrossYieldRate" runat="server" MaxLength="9">0</asp:TextBox>%
<asp:DropDownList ID="E_cboGrossYieldRateBehaviour" runat="server" Visible="false"> <asp:ListItem Value="N">Minimum</asp:ListItem> </asp:DropDownList>
<input id="Hide_GrossYieldRate" type="hidden" name="Hide_GrossYieldRate" runat="server" />
<input id="Hide_GrossYieldRateBeh" type="hidden" name="Hide_GrossYieldRateBeh" runat="server" />
<%-- <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" ControlToValidate="E_txtGrossYieldRate"
    CssClass="validator_general" ErrorMessage="Gross Yield Rate Salah" Display="Dynamic"></asp:RequiredFieldValidator>
<asp:RangeValidator ID="rgv_GrossYield" runat="server" ControlToValidate="E_txtGrossYieldRate"
    CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Gross Yield Rate Salah"
    MaximumValue="100"></asp:RangeValidator>--%>
<%--</div>
</div>
<div class="form_box_hide"> <div class="form_single"> <label> Branch</label> <asp:Label ID="lblGrossYieldRate_Branch" runat="server"></asp:Label>
</div>
</div>  
</div> --%>
<div class="form_box_title">
<div class="form_single"> <h4> SKEMA </h4> </div>
</div>
                
<div class="form_box_header">
<div class="form_single"> <h5> Jangka Waktu Maximum </h5> </div>
</div>
<div class="form_box">
<div class="form_single">
<label class="label_req"> From HO</label>
<asp:Label ID="lblMaximumTenor" runat="server"></asp:Label>

<asp:TextBox onblur="MaximumTenor('Tabs_pnlTabSkema_SkemaTab_E_txtMinimumTenor','Tabs_pnlTabSkema_SkemaTab_E_txtMaximumTenor','Tabs_pnlTabSkema_SkemaTab_Hide_MaximumTenor');" ID="E_txtMaximumTenor" runat="server" MaxLength="3">1</asp:TextBox><asp:Label ID="Label1" runat="server">Month</asp:Label>
<input id="Hide_MaximumTenor" type="hidden" name="Hide_MaximumTenor" runat="server" />
<input id="Hide_MaximumTenorBeh" type="hidden" name="Hide_MaximumTenorBeh" runat="server" />
<asp:RequiredFieldValidator ID="Requiredfieldvalidator3" runat="server" ControlToValidate="E_txtMaximumTenor" CssClass="validator_general" ErrorMessage="Jangka Waktu Maximum salah" Display="Dynamic"></asp:RequiredFieldValidator>
<asp:RangeValidator ID="Rangevalidator3" runat="server" ControlToValidate="E_txtMaximumTenor" CssClass="validator_general" Type="Double" MinimumValue="1" ErrorMessage="Jangka Waktu Maximum salah" MaximumValue="999"></asp:RangeValidator>
</div>
</div>
<div class="form_box">
<div class="form_single">
<label> Branch</label>
<asp:Label ID="lblMaximumTenor_Branch" runat="server"></asp:Label>
</div>
</div>

<div class="form_box_header">
<div class="form_single">
<h5> Jangka Waktu Minimum </h5>
</div>
</div>
<div class="form_box">
<div class="form_single">
<label class="label_req"> From HO</label>
   <asp:Label ID="lbltxtMinimumTenor" runat="server"></asp:Label>
<asp:TextBox onblur="MinimumTenor('Tabs_pnlTabSkema_SkemaTab_E_txtMinimumTenor','Tabs_pnlTabSkema_SkemaTab_E_txtMaximumTenor','Tabs_pnlTabSkema_SkemaTab_Hide_MinimumTenor');" ID="E_txtMinimumTenor" runat="server" MaxLength="3">1</asp:TextBox><asp:Label ID="Label2" runat="server">Month</asp:Label>
<input id="Hide_MinimumTenor" type="hidden" name="Hide_MinimumTenor" runat="server" />
<input id="Hide_MinimumTenorBeh" type="hidden" name="Hide_MinimumTenorBeh" runat="server" />
<asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" ControlToValidate="E_txtMinimumTenor" CssClass="validator_general" ErrorMessage="Jangka Waktu Minimum salah" Display="Dynamic"></asp:RequiredFieldValidator>
<asp:RangeValidator ID="Rangevalidator2" runat="server" ControlToValidate="E_txtMinimumTenor" CssClass="validator_general" Type="Double" MinimumValue="1" ErrorMessage="Jangka Waktu Minimum salah" MaximumValue="999"></asp:RangeValidator>
</div>
</div>
<div class="form_box">
<div class="form_single">
<label> Branch</label>
<asp:Label ID="lblMinimumTenor_Branch" runat="server"></asp:Label>
</div>
</div>

<div class="form_box_header">
<div class="form_single">
<h5> Angsuran Pertama </h5>
</div>
</div>
<div class="form_box">
<div class="form_single">
<label class="label_req">From HO</label>
<asp:Label ID= "lblAngsuranPertama" runat="server"></asp:Label>
<asp:DropDownList ID="E_cboAngsuranPertama" runat="server" > <asp:ListItem Value="ARR" Selected="True">ARREAR</asp:ListItem> <asp:ListItem Value="ADV">ADVANCE</asp:ListItem>  </asp:DropDownList>
<input id="Hide_AngsuranPertama" type="hidden" name="Hide_AngsuranPertama" runat="server" />
</div>
</div>
<div class="form_box">
<div class="form_single">
<label>Branch</label>
<asp:Label ID="lblAngsuranPertama_Branch" runat="server"></asp:Label>
</div>
</div>


<div class="form_box_header">
<div class="form_single">
<h5> Suku Bunga Flat </h5>
</div>
</div> 
<div class="form_box">
<div class="form_single">
<label class="label_req"> From HO</label>
<asp:Label ID="lblFlatRate" runat="server"></asp:Label>
<asp:TextBox ID="E_txtFlatRate" runat="server"  MaxLength="9">0</asp:TextBox><asp:Label ID="Label3" runat="server">%</asp:Label>
<asp:DropDownList ID="E_cboFlatRateBehaviour" runat="server" /> 
<input id="Hide_FlatRate" type="hidden" name="Hide_FlatRate" runat="server" />
<input id="Hide_FlatRateBeh" type="hidden" name="Hide_FlatRateBeh" runat="server" />
<asp:RequiredFieldValidator ID="RequiredFieldValidator67" runat="server" ControlToValidate="E_txtFlatRate" CssClass="validator_general" ErrorMessage="Suku bunga Flat salah" Enabled="True" Display="Dynamic"></asp:RequiredFieldValidator>
<asp:RangeValidator ID="RangeValidator55" runat="server" ControlToValidate="E_txtFlatRate" CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Seku bunga Flat salah" Enabled="True" MaximumValue="100"></asp:RangeValidator>
</div>
</div> 

<div class="form_box">
<div class="form_single">
<label>
    Branch</label>
<asp:Label ID="lblFlatRate_Branch" runat="server"></asp:Label>
</div>
</div>
<div class="form_box_header">
<div class="form_single">
<h5> Suku Bunga Effective </h5>
</div>
</div>
<div class="form_box">
<div class="form_single">
<label class="label_req"> From HO</label>
<asp:Label ID="lblEffectiveRate" runat="server"></asp:Label>
<asp:TextBox onblur="EffectiveRate();" ID="E_txtEffectiveRate" runat="server" MaxLength="9">0</asp:TextBox><asp:Label ID="Label4" runat="server">%</asp:Label>
<asp:DropDownList ID="E_cboEffectiveRateBehaviour" runat="server"/> 
<input id="Hide_EffectiveRate" type="hidden" name="Hide_EffectiveRate" runat="server" />
<input id="Hide_EffectiveRateBeh" type="hidden" name="Hide_EffectiveRateBeh" runat="server" />
<asp:RequiredFieldValidator ID="rfvEffectiveRate" runat="server" ControlToValidate="E_txtEffectiveRate" CssClass="validator_general" ErrorMessage="Suku bunga Effective salah" Display="Dynamic"></asp:RequiredFieldValidator>
<asp:RangeValidator ID="rgv_EffectiveRate" runat="server" ControlToValidate="E_txtEffectiveRate" CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Suku bunga Effective salah" MaximumValue="100"></asp:RangeValidator>
</div>
</div>
<div class="form_box">
<div class="form_single">
<label> Branch</label>
<asp:Label ID="lblEffectiveRate_Branch" runat="server"></asp:Label>
</div>
</div> 
                
<div class="form_box_header">
<div class="form_single">
<h5> Opsi Uang Muka </h5>
</div>
</div>
<div class="form_box">
<div class="form_single">
<label class="label_req"> From HO</label>
<asp:Label ID="lblOpsiUangMuka" runat="server"></asp:Label>
<asp:DropDownList ID="E_cboOpsiUangMuka" runat="server" > <asp:ListItem Value="P" Selected="True">PROSENTASE</asp:ListItem> <asp:ListItem Value="A">ANGSURAN</asp:ListItem>  </asp:DropDownList>
<input id="Hide_OpsiUangMuka" type="hidden" name="Hide_AOpsiUangMuka" runat="server" />
</div>
</div>
<div class="form_box">
<div class="form_single">
<label> Branch</label>
<asp:Label ID="lblOpsiUangMuka_Branch" runat="server"></asp:Label>
</div>
</div>

<div class="form_box_header">
<div class="form_single">
<h5> Uang Muka (%) </h5>
</div>
</div>
<div class="form_box">
<div class="form_single">
<label class="label_req"> From HO</label>
<asp:Label ID="lblDPPercentage" runat="server"></asp:Label>
<asp:TextBox onblur="DPPercentage();" ID="E_txtDPPercentage" runat="server" MaxLength="9">0</asp:TextBox><asp:Label ID="Label5" runat="server">%</asp:Label>
<asp:DropDownList ID="E_cboDPPercentageBehaviour" runat="server"/> 
<input id="Hide_DPPercentage" type="hidden" name="Hide_DPPercentage" runat="server" />
<input id="Hide_DPPercentageBeh" type="hidden" name="Hide_DPPercentageBeh" runat="server" />
<asp:RequiredFieldValidator ID="Requiredfieldvalidator24" runat="server" ControlToValidate="E_txtDPPercentage" CssClass="validator_general" ErrorMessage="Prosentase Minimum DP Salah" Display="Dynamic"></asp:RequiredFieldValidator>
<asp:RangeValidator ID="rgv_DPPercentage" runat="server" ControlToValidate="E_txtDPPercentage" CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Prosentase Minimum DP Salah" MaximumValue="100"></asp:RangeValidator>
</div>
</div>
<div class="form_box">
<div class="form_single">
<label> Branch</label>
<asp:Label ID="lblDPPercentage_Branch" runat="server"></asp:Label>
</div>
</div>
                
<div class="form_box_header">
<div class="form_single"> <h5> Uang Muka (Angsuran) </h5> </div>
</div>
<div class="form_box">
<div class="form_single">
<label>From HO</label>
<asp:Label ID="lblUangMukaAngsuran" runat="server"></asp:Label>
<asp:TextBox ID="txtUangMukaAngsuran" runat="server"  MaxLength="9">0</asp:TextBox><asp:Label ID="Label6" runat="server">x</asp:Label> 
<asp:RequiredFieldValidator ID="Requiredfieldvalidator68" runat="server" ControlToValidate="txtUangMukaAngsuran" CssClass="validator_general" ErrorMessage="Uang Muka (Angsuran) Salah" Display="Dynamic"></asp:RequiredFieldValidator>
<asp:RangeValidator ID="Rangevalidator56" runat="server" ControlToValidate="txtUangMukaAngsuran" CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Uang Muka (Angsuran) Salah" Display="Dynamic"  MaximumValue="500"></asp:RangeValidator> <input id="Hide_UangMukaAngsuran" type="hidden" name="Hide_UangMukaAngsuran" runat="server" />
</div>
</div> 
<div class="form_box">
<div class="form_single">
<label> Branch</label>
<asp:Label ID="lblUangMukaAngsuran_Branch" runat="server"></asp:Label>
</div>
</div>


<div class="form_box_header">
<div class="form_single">
<h5> Masa Berlaku PO (dalam hari) </h5>
</div>
</div>
<div class="form_box">
<div class="form_single">
<label class="label_req"> From HO</label>
<asp:Label ID="lblPOExpirationDays" runat="server"></asp:Label>

<asp:TextBox onblur="com_NX('Tabs_pnlTabSkema_SkemaTab_E_cboPOExpirationDaysBehaviour','Tabs_pnlTabSkema_SkemaTab_E_txtPOExpirationDays','Tabs_pnlTabSkema_SkemaTab_Hide_POExpirationDays','Masa Berlaku PO');" ID="E_txtPOExpirationDays" runat="server" MaxLength="4">0</asp:TextBox><asp:Label ID="Label7" runat="server">days</asp:Label>
<asp:DropDownList ID="E_cboPOExpirationDaysBehaviour" runat="server"/> 
<input id="Hide_POExpirationDays" type="hidden" name="Hide_POExpirationDays" runat="server" />
<input id="Hide_POExpirationDaysBeh" type="hidden" name="Hide_POExpirationDaysBeh" runat="server" />
<asp:RequiredFieldValidator ID="Requiredfieldvalidator22" runat="server" ControlToValidate="E_txtPOExpirationDays" CssClass="validator_general" ErrorMessage="Masa Berlaku PO salah" Display="Dynamic"></asp:RequiredFieldValidator> <asp:RangeValidator ID="rgv_POExpirationDays" runat="server" ControlToValidate="E_txtPOExpirationDays" CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Masa Berlaku PO salah" MaximumValue="9999"></asp:RangeValidator>
</div>
</div>
<div class="form_box">
<div class="form_single">
<label> Branch</label>
<asp:Label ID="lblPOExpirationDays_Branch" runat="server"></asp:Label>
</div>
</div>


<div class="form_box_header">
<div class="form_single">
<h5> Credit Protection  </h5>
</div>
</div> 
<div class="form_box">
<div class="form_single">
<label class="label_req"> From HO</label>
<asp:Label ID="lblCreditProtection" runat="server"></asp:Label>
<asp:TextBox ID="E_txtCreditProtection" runat="server"  MaxLength="9">0</asp:TextBox><asp:Label ID="Label9" runat="server">%</asp:Label>
<asp:DropDownList ID="E_cboCreditProtectionBehaviour" runat="server" /> 
<input id="Hide_CreditProtection" type="hidden" name="Hide_CreditProtection" runat="server" />
<input id="Hide_CreditProtectionBeh" type="hidden" name="Hide_CreditProtectionBeh" runat="server" />
<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="E_txtCreditProtection" CssClass="validator_general" ErrorMessage="Credit Protection salah" Enabled="True" Display="Dynamic"></asp:RequiredFieldValidator>
<asp:RangeValidator ID="RangeValidator5" runat="server" ControlToValidate="E_txtFlatRate" CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Credit Protection salah" Enabled="True" MaximumValue="100"></asp:RangeValidator>
</div>
</div> 
<div class="form_box">
<div class="form_single">
<label> Branch</label>
<asp:Label ID="lblCreditProtection_Branch" runat="server"></asp:Label>
</div>
</div> 

<div class="form_box_header">
<div class="form_single">
<h5> Asuranasi Jaminan Kredit </h5>
</div>
</div> 
<div class="form_box">
<div class="form_single">
<label class="label_req"> From HO</label>
<asp:Label ID="lblAsuransiJaminan" runat="server"></asp:Label>
<asp:TextBox ID="E_txtAsuransiJaminan" runat="server"  MaxLength="9">0</asp:TextBox><asp:Label ID="Label11" runat="server">%</asp:Label>
<asp:DropDownList ID="E_cboAsuransiJaminanBehaviour" runat="server" /> 
<input id="Hide_AsuransiJaminan" type="hidden" name="Hide_CreditProtection" runat="server" />
<input id="Hide_AsuransiJaminan_Beh" type="hidden" name="Hide_CreditProtectionBeh" runat="server" />
<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="E_txtAsuransiJaminan" CssClass="validator_general" ErrorMessage="Asuransi Jaminan Kredit salah" Enabled="True" Display="Dynamic"></asp:RequiredFieldValidator>
<asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="E_txtAsuransiJaminan" CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Asuransi Jaminan Kredit salah" Enabled="True" MaximumValue="100"></asp:RangeValidator>
</div>
</div> 
<div class="form_box">
<div class="form_single">
<label> Branch</label>
<asp:Label ID="lblAsuransiJaminan_Branch" runat="server"></asp:Label>
</div>
</div>

