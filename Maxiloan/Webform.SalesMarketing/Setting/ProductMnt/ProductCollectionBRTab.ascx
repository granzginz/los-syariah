﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ProductCollectionBRTab.ascx.vb" Inherits="Maxiloan.Webform.SalesMarketing.ProductCollectionBRTab" %>

<div> <div class="form_box_title"> <div class="form_single"> <h4> COLLECTION </h4> </div> </div>
                                
<div class="form_box_header">
        <div class="form_single"> <h5> Buat Surat Peringatan 1 Otomatis </h5> </div>
        </div>
        
    <div class="form_box"> <div class="form_single"> 
    <label> Branch</label>  <asp:Label ID="lblLengthSPProcess_Branch" runat="server"></asp:Label> </div> </div>


    <div class="form_box"> 
    <div class="form_single">
    <label class="label_req"> From HO</label>
        <asp:label  id='lblIsSPAutomatic' runat='server'></asp:label>
       <asp:DropDownList ID="E_cboIsSPAutomatic" runat="server" onchange="cboIsSpAutomaticChange('Tabs_pnlTabCollection_CollactionTab_E_cboIsSPAutomatic','Tabs_pnlTabCollection_CollactionTab_E_txtLengthSPProcess');"> <asp:ListItem Value="1" Selected="True">Yes</asp:ListItem> <asp:ListItem Value="0">No</asp:ListItem> </asp:DropDownList> <asp:label  id='Label1' runat='server'>Length SP Process</asp:label> 
        <asp:TextBox ID="E_txtLengthSPProcess" runat="server" MaxLength="4">0</asp:TextBox><asp:label  id='Label2' runat='server'>days</asp:label> 
        <input id="Hide_LengthSPProcess" type="hidden" name="Hide_LengthSPProcess" runat="server" />
        <asp:RequiredFieldValidator ID="Requiredfieldvalidator27" runat="server" ControlToValidate="E_txtLengthSPProcess" CssClass="validator_general" ErrorMessage="Jangka Waktu Proses SP Salah" Display="Dynamic"></asp:RequiredFieldValidator>
        <asp:RangeValidator ID="rgv_LengthSPProcess" runat="server" ControlToValidate="E_txtLengthSPProcess" CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Jangka Waktu Proses SP Salah" MaximumValue="9999"></asp:RangeValidator>
    </div>
    </div>

    <div class="form_box_header">
    <div class="form_single"> <h5> Buat Surat Peringatan 2 Otomatis </h5>
    </div>
    </div>
    
    <div class="form_box">
    <div class="form_single">
    <label>
    Branch</label>
    <asp:Label ID="lblLengthSP1Process_Branch" runat="server"></asp:Label>
    </div>
    </div>
    <div class="form_box">
    <div class="form_single">
    <label class="label_req"> From HO</label>
          <asp:label  id='lblIsSP1Automatic' runat='server'></asp:label>
       <asp:DropDownList ID="E_cboIsSP1Automatic" runat="server" onchange="cboIsSpAutomaticChange('Tabs_pnlTabCollection_CollactionTab_E_cboIsSP1Automatic','Tabs_pnlTabCollection_CollactionTab_E_txtLengthSP1Process');"> <asp:ListItem Value="1" Selected="True">Yes</asp:ListItem> <asp:ListItem Value="0">No</asp:ListItem> </asp:DropDownList> <asp:label  id='Label3' runat='server'>Length SP Process</asp:label> 
        <asp:TextBox ID="E_txtLengthSP1Process" runat="server" MaxLength="4">0</asp:TextBox><asp:label  id='Label4' runat='server'>days</asp:label> 
        <input id="Hide_LengthSP1Process" type="hidden" name="Hide_LengthSP1Process" runat="server" />
        <asp:RequiredFieldValidator ID="Requiredfieldvalidator28" runat="server" ControlToValidate="E_txtLengthSP1Process" CssClass="validator_general" ErrorMessage="Jangka Waktu Proses SP1 Salah" Display="Dynamic"></asp:RequiredFieldValidator>
        <asp:RangeValidator ID="rgv_LengthSP1Process" runat="server" ControlToValidate="E_txtLengthSP1Process" CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Jangka Waktu Proses SP1 Salah" MaximumValue="9999"></asp:RangeValidator>
    </div>
    </div>

    <div class="form_box_header">
    <div class="form_single">
    <h5> Buat Surat Peringatan 3 Otomatis </h5>
    </div>
    </div>
    
    <div class="form_box">
    <div class="form_single">
        <label> Branch</label>
        <asp:Label ID="lblLengthSP2Process_Branch" runat="server"></asp:Label>
    </div>
    </div>
    <div class="form_box">
    <div class="form_single">
        <label class="label_req"> From HO</label>
        <asp:Label ID="lblIsSP2Automatic" runat="server"></asp:Label> 
        <asp:DropDownList ID="E_cboIsSP2Automatic" runat="server" onchange="cboIsSpAutomaticChange('Tabs_pnlTabCollection_CollactionTab_E_cboIsSP2Automatic','Tabs_pnlTabCollection_CollactionTab_E_txtLengthSP2Process');"> <asp:ListItem Value="1" Selected="True">Yes</asp:ListItem> <asp:ListItem Value="0">No</asp:ListItem> </asp:DropDownList> <asp:Label ID="Label5" runat="server"> Length SP Process</asp:Label>
        <asp:TextBox ID="E_txtLengthSP2Process" runat="server" MaxLength="4">0</asp:TextBox><asp:Label ID="Label6" runat="server">days</asp:Label> 
        <input id="Hide_LengthSP2Process" type="hidden" name="Hide_LengthSP2Process" runat="server" />
        <asp:RequiredFieldValidator ID="Requiredfieldvalidator29" runat="server" ControlToValidate="E_txtLengthSP2Process" CssClass="validator_general" ErrorMessage="Jangka Waktu Proses SP2 Salah" Display="Dynamic"></asp:RequiredFieldValidator>
        <asp:RangeValidator ID="rgv_LengthSP2Process" runat="server" ControlToValidate="E_txtLengthSP2Process" CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Jangka Waktu Proses SP2 Salah" MaximumValue="9999"></asp:RangeValidator>
    </div>
    </div>

    <div class="form_box_header">
    <div class="form_single"> <h5> Jangka Waktu proses Dokumen Utama </h5>
    </div>
    </div>
    
    <div class="form_box">
    <div class="form_single">
    <label> Branch</label>
    <asp:Label ID="lblLengthMainDocProcess_Branch" runat="server"></asp:Label>
    </div>
    </div>
    <div class="form_box">
    <div class="form_single">
    <label class="label_req"> From HO</label>
            <asp:Label ID="lblLMDProcessed" runat="server"></asp:Label> 
            <asp:TextBox onblur=" com_NX('Tabs_pnlTabCollection_CollactionTab_Hide_LengthMainDocProcessBeh', 'Tabs_pnlTabCollection_CollactionTab_E_txtLengthMainDocProcess', 'Tabs_pnlTabCollection_CollactionTab_Hide_LengthMainDocProcess', 'Jangka Waktu proses Dokumen Utama'); " ID="E_txtLengthMainDocProcess" runat="server" MaxLength="4">0</asp:TextBox><asp:Label ID="Label25" runat="server">days</asp:Label> 
            <asp:DropDownList ID="E_cboLengthMainDocProcessBehaviour" runat="server" /> 
            <input id="Hide_LengthMainDocProcess" type="hidden" name="Hide_LengthMainDocProcess" runat="server" />
            <input id="Hide_LengthMainDocProcessBeh" type="hidden" name="Hide_LengthMainDocProcessBeh" runat="server" />
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator30" runat="server" ControlToValidate="E_txtLengthMainDocProcess" CssClass="validator_general" ErrorMessage="Jangka Waktu proses Dokumen Utama salah" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="rgv_LengthMainDocProcess" runat="server" ControlToValidate="E_txtLengthMainDocProcess" CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Jangka Waktu proses Dokumen Utama salah" MaximumValue="9999"></asp:RangeValidator>
    </div>
    </div>

    <div class="form_box_header">
    <div class="form_single"> <h5> Jangka Waktu Dokumen Utama diambil </h5>
    </div>
    </div>
    
    <div class="form_box">
    <div class="form_single">
    <label> Branch</label> <asp:Label ID="lblLengthMainDocTaken_Branch" runat="server"></asp:Label>
    </div>
    </div>

    <div class="form_box">
    <div class="form_single">
    <label class="label_req"> From HO</label>
            <asp:Label ID="lblLMDTaken" runat="server"></asp:Label >
           <asp:TextBox onblur="com_NX('Tabs_pnlTabCollection_CollactionTab_Hide_LengthMainDocTakenBeh', 'Tabs_pnlTabCollection_CollactionTab_E_txtLengthMainDocTaken', 'Tabs_pnlTabCollection_CollactionTab_Hide_LengthMainDocTaken', 'JJangka Waktu Dokumen Utama diambil');" ID="E_txtLengthMainDocTaken" runat="server" MaxLength="4">0</asp:TextBox><asp:label  id='Label7' runat='server'>days</asp:label> 
            <asp:DropDownList ID="E_cboLengthMainDocTakenBehaviour" runat="server" /> 
            <input id="Hide_LengthMainDocTaken" type="hidden" name="Hide_LengthMainDocTaken" runat="server" />
            <input id="Hide_LengthMainDocTakenBeh" type="hidden" name="Hide_LengthMainDocTakenBeh" runat="server" />
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator31" runat="server" ControlToValidate="E_txtLengthMainDocTaken" CssClass="validator_general" ErrorMessage="Jangka Waktu Dokumen Utama diambil salah" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="rgv_LengthMainDocTaken" runat="server" ControlToValidate="E_txtLengthMainDocTaken" CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Jangka Waktu Dokumen Utama diambil salah" MaximumValue="9999"></asp:RangeValidator>
    </div>
    </div>
    <div class="form_box_header">
    <div class="form_single">
        <h5> Grace Period Denda Keterlambatan </h5>
    </div>
    </div>
      <div class="form_box">
    <div class="form_single">
    <label> Branch</label> <asp:Label ID="lblGracePeriodLateCharges_Branch" runat="server"></asp:Label>
    </div>
    </div>
    <div class="form_box">
    <div class="form_single">
    <label class="label_req"> From HO</label>
            <asp:Label ID="lblGPLC" runat="server"></asp:Label>
            <asp:TextBox  onblur="com_NX('Tabs_pnlTabCollection_CollactionTab_Hide_GracePeriodLateChargesBeh', 'Tabs_pnlTabCollection_CollactionTab_E_txtGracePeriodLateCharges', 'Tabs_pnlTabCollection_CollactionTab_Hide_GracePeriodLateCharges', 'Grace Period Denda Keterlambatan');"  ID="E_txtGracePeriodLateCharges" runat="server" MaxLength="4">0</asp:TextBox><asp:label  id='Label8' runat='server'>days</asp:label> 
            <asp:DropDownList ID="E_cboGracePeriodLateChargesBehaviour" runat="server"/> 
            <input id="Hide_GracePeriodLateCharges" type="hidden" name="Hide_GracePeriodLateCharges" runat="server" />
            <input id="Hide_GracePeriodLateChargesBeh" type="hidden" name="Hide_GracePeriodLateChargesBeh" runat="server" />
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator32" runat="server" ControlToValidate="E_txtGracePeriodLateCharges" CssClass="validator_general" ErrorMessage="Grace Period Denda Keterlambatan Salah" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="rgv_GracePeriodLateCharges" runat="server" ControlToValidate="E_txtGracePeriodLateCharges" CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Grace Period Denda Keterlambatan Salah" MaximumValue="9999"></asp:RangeValidator>
    </div>
    </div>
  

    <div class="form_box_header">
    <div class="form_single">
        <h5> Jumlah hari mengingatkan angsuran </h5>
    </div>
    </div>
    
    <div class="form_box">
    <div class="form_single">
    <label> Branch</label> <asp:Label ID="lblDeskCollPhoneRemind_Branch" runat="server"></asp:Label>
    </div>
    </div>
    <div class="form_box">
    <div class="form_single">
    <label class="label_req"> From HO</label>
            <asp:Label ID="lblDaystoremindInstallment" runat="server"></asp:Label> 
            <asp:TextBox   onblur="com_NX('Tabs_pnlTabCollection_CollactionTab_Hide_DeskCollPhoneRemindBeh', 'Tabs_pnlTabCollection_CollactionTab_E_txtDeskCollPhoneRemind', 'Tabs_pnlTabCollection_CollactionTab_Hide_DeskCollPhoneRemind', 'Jumlah hari mengingatkan angsuran');" ID="E_txtDeskCollPhoneRemind" runat="server" MaxLength="4">0</asp:TextBox><asp:label  id='Label9' runat='server'>days</asp:label> 
            <asp:DropDownList ID="E_cboDeskCollPhoneRemindBehaviour" runat="server"/> 
            <input id="Hide_DeskCollPhoneRemind" type="hidden" name="Hide_DeskCollPhoneRemind" runat="server" />
            <input id="Hide_DeskCollPhoneRemindBeh" type="hidden" name="Hide_DeskCollPhoneRemindBeh" runat="server" />
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator34" runat="server" ControlToValidate="E_txtDeskCollPhoneRemind" CssClass="validator_general" ErrorMessage="Jumlah hari mengingatkan angsuran salah" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="rgv_DeskCollPhoneRemind" runat="server" ControlToValidate="E_txtDeskCollPhoneRemind" CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Jumlah hari mengingatkan angsuran salah" MaximumValue="9999"></asp:RangeValidator>
    </div>
    </div>

    <div class="form_box_header">
    <div class="form_single">
            <h5> Jumlah hari Overdue untuk Deskcoll </h5>
    </div>
    </div>
    
    <div class="form_box">
    <div class="form_single"> <label> Branch</label>
    <asp:Label ID="lblDeskCollOD_Branch" runat="server"></asp:Label>
    </div>
    </div>

    <div class="form_box">
    <div class="form_single">
            <label class="label_req"> From HO</label>
            <asp:Label ID="lblDaysOverduetocall" runat="server"></asp:Label>
          <asp:TextBox onblur="com_NX('Tabs_pnlTabCollection_CollactionTab_Hide_DeskCollODBeh', 'Tabs_pnlTabCollection_CollactionTab_E_txtDeskCollOD', 'Tabs_pnlTabCollection_CollactionTab_Hide_DeskCollOD', 'Jumlah hari Overdue untuk Deskcoll');" ID="E_txtDeskCollOD" runat="server" MaxLength="4">0</asp:TextBox><asp:label  id='Label10' runat='server'>days</asp:label>  
            <asp:DropDownList ID="E_cboDeskCollODBehaviour" runat="server"/> 
            <input id="Hide_DeskCollOD" type="hidden" name="Hide_DeskCollOD" runat="server" />
            <input id="Hide_DeskCollODBeh" type="hidden" name="Hide_DeskCollODBeh" runat="server" />
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator101" runat="server" ControlToValidate="E_txtDeskCollOD" CssClass="validator_general" ErrorMessage="Jumlah hari Overdue untuk DeskColl salah" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="rgv_DeskCollOD" runat="server" ControlToValidate="E_txtDeskCollOD" Type="Double" MinimumValue="0" ErrorMessage="Jumlah hari Overdue untuk DeskColl salah" CssClass="validator_general" MaximumValue="9999"></asp:RangeValidator>
    </div>
    </div>
    <div class="form_box_header">
    <div class="form_single">
    <h5> Overdue sebelumnya untuk mengingatkan </h5>
    </div>
    </div>
    
    <div class="form_box">
    <div class="form_single">
        <label> Branch</label> <asp:Label ID="lblPrevODToRemind_Branch" runat="server"></asp:Label>
    </div>
    </div>
    <div class="form_box">
    <div class="form_single">
    <label class="label_req"> From HO</label>
            <asp:Label ID="lblPreviousOverduetoremind" runat="server"></asp:Label> 
               <asp:TextBox onblur="com_NX('Tabs_pnlTabCollection_CollactionTab_Hide_PrevODToRemindBeh', 'Tabs_pnlTabCollection_CollactionTab_E_txtPrevODToRemind', 'Tabs_pnlTabCollection_CollactionTab_Hide_PrevODToRemind', 'Overdue sebelumnya untuk mengingatkan');"  ID="E_txtPrevODToRemind" runat="server" MaxLength="4">0</asp:TextBox><asp:label  id='Label11' runat='server'>days</asp:label> 
            <asp:DropDownList ID="E_cboPrevODToRemindBehaviour" runat="server"/> 
            <input id="Hide_PrevODToRemind" type="hidden" name="Hide_PrevODToRemind" runat="server" />
            <input id="Hide_PrevODToRemindBeh" type="hidden" name="Hide_PrevODToRemindBeh" runat="server" />
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator100" runat="server" ControlToValidate="E_txtPrevODToRemind" CssClass="validator_general" ErrorMessage="Overdue sebelumnya untuk mengingatkan Salah" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="rgv_PrevODToRemind" runat="server" ControlToValidate="E_txtPrevODToRemind" CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Overdue sebelumnya untuk mengingatkan Salah" MaximumValue="9999"></asp:RangeValidator>
    </div>
    </div>

    <div class="form_box_header">
    <div class="form_single">
        <h5> Permintaan PDC untuk telepon </h5>
    </div>
    </div>
    
    <div class="form_box">
    <div class="form_single">   
            <label> Branch</label> <asp:Label ID="lblPDCDaysToRemind_Branch" runat="server"></asp:Label>
    </div>
    </div>
    <div class="form_box">
    <div class="form_single">
    <label class="label_req"> From HO</label>
            <asp:Label ID="lblPDCRequesttocall" runat="server"></asp:Label> 
       <asp:TextBox onblur="com_NX('Tabs_pnlTabCollection_CollactionTab_Hide_PDCDaysToRemindBeh', 'Tabs_pnlTabCollection_CollactionTab_E_txtPDCDaysToRemind', 'Tabs_pnlTabCollection_CollactionTab_Hide_PDCDaysToRemind', 'Permintaan PDC untuk telepon');" ID="E_txtPDCDaysToRemind" runat="server" MaxLength="4">0</asp:TextBox><asp:label  id='Label12' runat='server'>days</asp:label> 
            <asp:DropDownList ID="E_cboPDCDaysToRemindBehaviour" runat="server"/> 
            <input id="Hide_PDCDaysToRemind" type="hidden" name="Hide_PDCDaysToRemind" runat="server" />
            <input id="Hide_PDCDaysToRemindBeh" type="hidden" name="Hide_PDCDaysToRemindBeh" runat="server" />
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator36" runat="server" ControlToValidate="E_txtPDCDaysToRemind" CssClass="validator_general" ErrorMessage="Permintaan PDC untuk telepon Salah" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="rgv_PDCDaysToRemind" runat="server" ControlToValidate="E_txtPDCDaysToRemind" CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Permintaan PDC untuk telepon Salah" MaximumValue="9999"></asp:RangeValidator>
    </div>
    </div>

    <div class="form_box_header">
    <div class="form_single">
            <h5> Jumlah hari mengingatkan dgn SMS </h5>
    </div>
    </div>
    <div class="form_box">
    <div class="form_single">
    <label class="label_req"> From HO</label>
            <asp:Label ID="lblDaysToRemind" runat="server"></asp:Label> 
             <asp:TextBox  onblur="com_NX('Tabs_pnlTabCollection_CollactionTab_Hide_DeskCollSMSRemindBeh', 'Tabs_pnlTabCollection_CollactionTab_E_txtDeskCollSMSRemind', 'Tabs_pnlTabCollection_CollactionTab_Hide_DeskCollSMSRemind', 'Jumlah hari mengingatkan dgn SMS');" ID="E_txtDeskCollSMSRemind" runat="server" MaxLength="4">0</asp:TextBox><asp:label  id='Label13' runat='server'>days</asp:label> 
            <asp:DropDownList ID="E_cboDeskCollSMSRemindBehaviour" runat="server"/> 
            <input id="Hide_DeskCollSMSRemind" type="hidden" name="Hide_DeskCollSMSRemind" runat="server" />
            <input id="Hide_DeskCollSMSRemindBeh" type="hidden" name="Hide_DeskCollSMSRemindBeh" runat="server" />
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator37" runat="server" ControlToValidate="E_txtDeskCollSMSRemind" CssClass="validator_general" ErrorMessage="Jumlah hari mengingatkan dgn SMS salah" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="rgv_DeskCollSMSRemind" runat="server" ControlToValidate="E_txtDeskCollSMSRemind" CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Jumlah hari mengingatkan dgn SMS salah" MaximumValue="9999"></asp:RangeValidator>
    </div>
    </div>
    <div class="form_box">
    <div class="form_single">
    <label> Branch</label>
    <asp:Label ID="lblDeskCollSMSRemind_Branch" runat="server"></asp:Label>
    </div>
    </div>

    <div class="form_box_header">
    <div class="form_single">
            <h5> Jumlah hari reminder dgn SMS </h5>
    </div>
    </div>
    <div class="form_box">
    <div class="form_single">
    <label class="label_req"> From HO</label>
            <asp:Label ID="lblDaysToRemind_2" runat="server"></asp:Label> 
            <asp:TextBox  onblur="com_NX('Tabs_pnlTabCollection_CollactionTab_Hide_DeskCollSMSRemindBeh_2', 'Tabs_pnlTabCollection_CollactionTab_E_txtDeskCollSMSRemind_2', 'Tabs_pnlTabCollection_CollactionTab_Hide_DeskCollSMSRemind_2', 'Jumlah hari reminder dgn SMS');" ID="E_txtDeskCollSMSRemind_2" runat="server" MaxLength="4">0</asp:TextBox><asp:label  id='Label27' runat='server'>days</asp:label> 
            <asp:DropDownList ID="E_cboDeskCollSMSRemindBehaviour_2" runat="server"/> 
            <input id="Hide_DeskCollSMSRemind_2" type="hidden" name="Hide_DeskCollSMSRemind_2" runat="server" />
            <input id="Hide_DeskCollSMSRemindBeh_2" type="hidden" name="Hide_DeskCollSMSRemindBeh_2" runat="server" />
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" ControlToValidate="E_txtDeskCollSMSRemind_2" CssClass="validator_general" ErrorMessage="Jumlah hari reminder dgn SMS salah" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="rgv_DeskCollSMSRemind_2" runat="server" ControlToValidate="E_txtDeskCollSMSRemind_2" CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Jumlah hari reminder dgn SMS salah" MaximumValue="9999"></asp:RangeValidator>
    </div>
    </div>
    <div class="form_box">
    <div class="form_single">
    <label> Branch</label>
    <asp:Label ID="lblDeskCollSMSRemind_Branch2" runat="server"></asp:Label>
    </div>
    </div>
    <div class="form_box_header">
    <div class="form_single">
            <h5> Jumlah hari reminder dgn SMS </h5>
    </div>
    </div>
    <div class="form_box">
    <div class="form_single">
    <label class="label_req"> From HO</label>
            <asp:Label ID="lblDaysToRemind_3" runat="server"></asp:Label> 
            <asp:TextBox  onblur="com_NX('Tabs_pnlTabCollection_CollactionTab_Hide_DeskCollSMSRemindBeh_3', 'Tabs_pnlTabCollection_CollactionTab_E_txtDeskCollSMSRemind_3', 'Tabs_pnlTabCollection_CollactionTab_Hide_DeskCollSMSRemind_3', 'Jumlah hari reminder dgn SMS');" ID="E_txtDeskCollSMSRemind_3" runat="server" MaxLength="4">0</asp:TextBox><asp:label  id='Label29' runat='server'>days</asp:label> 
            <asp:DropDownList ID="E_cboDeskCollSMSRemindBehaviour_3" runat="server"/> 
            <input id="Hide_DeskCollSMSRemind_3" type="hidden" name="Hide_DeskCollSMSRemind_3" runat="server" />
            <input id="Hide_DeskCollSMSRemindBeh_3" type="hidden" name="Hide_DeskCollSMSRemindBeh_3" runat="server" />
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" ControlToValidate="E_txtDeskCollSMSRemind_3" CssClass="validator_general" ErrorMessage="Jumlah hari reminder dgn SMS salah" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="RangeValidator2" runat="server" ControlToValidate="E_txtDeskCollSMSRemind_3" CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Jumlah hari mengingatkan dgn SMS salah" MaximumValue="9999"></asp:RangeValidator>
    </div>
    </div>
    <div class="form_box">
    <div class="form_single">
    <label> Branch</label>
    <asp:Label ID="lblDeskCollSMSRemind_Branch3" runat="server"></asp:Label>
    </div>
    </div>
    <div class="form_box_header">
    <div class="form_single">
            <h5> Jumlah hari buat DCR </h5>
    </div>
    </div>
    
    <div class="form_box">
    <div class="form_single">
    <label> Branch</label> <asp:Label ID="lblDCR_Branch" runat="server"></asp:Label>
    </div>
    </div>

    <div class="form_box">
    <div class="form_single">
            <label class="label_req"> From HO</label>
            <asp:Label ID="lblDaysToGenerateDCR" runat="server"></asp:Label> 
            <asp:TextBox  onblur="com_NX('Tabs_pnlTabCollection_CollactionTab_Hide_DCRBeh', 'Tabs_pnlTabCollection_CollactionTab_E_txtDCR', 'Tabs_pnlTabCollection_CollactionTab_Hide_DCR', 'Jumlah hari buat DCR');"  ID="E_txtDCR" runat="server" MaxLength="4">0</asp:TextBox><asp:label  id='Label14' runat='server'>days</asp:label>  
            <asp:DropDownList ID="E_cboDCRBehaviour" runat="server"/> 
            <input id="Hide_DCR" type="hidden" name="Hide_DCR" runat="server" />
            <input id="Hide_DCRBeh" type="hidden" name="Hide_DCRBeh" runat="server" />
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator38" runat="server" ControlToValidate="E_txtDCR" CssClass="validator_general" ErrorMessage="Jumlah hari buat DCR Salah" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="rgv_DCR" runat="server" ControlToValidate="E_txtDCR" Type="Double" CssClass="validator_general" MinimumValue="0" ErrorMessage="Jumlah hari buat DCR Salah" MaximumValue="9999"></asp:RangeValidator>
    </div>
    </div>

    <div class="form_box_header">
    <div class="form_single">
        <h5> Jumlah hari sebelum JT buat Kwitansi </h5>
    </div>
    </div>
     <div class="form_box">
    <div class="form_single">
    <label> Branch</label>
    <asp:Label ID="lblDaysBeforeDueToRN_Branch" runat="server"></asp:Label>
    </div>
    </div>
    <div class="form_box">
    <div class="form_single">
    <label class="label_req"> From HO</label>
            <asp:Label ID="lblDaysBeforeDuetoRN" runat="server"></asp:Label> 
        <asp:TextBox onblur="com_NX('Tabs_pnlTabCollection_CollactionTab_Hide_DaysBeforeDueToRNBeh', 'Tabs_pnlTabCollection_CollactionTab_E_txtDaysBeforeDueToRN', 'Tabs_pnlTabCollection_CollactionTab_Hide_DaysBeforeDueToRN', 'Jumlah hari sebelum JT buat Kwitansi');"  ID="E_txtDaysBeforeDueToRN" runat="server" MaxLength="4">0</asp:TextBox><asp:label  id='Label15' runat='server'>days</asp:label> 
            <asp:DropDownList ID="E_cboDaysBeforeDueToRNBehaviour" runat="server"/> 
             <input id="Hide_DaysBeforeDueToRN" type="hidden" name="Hide_DaysBeforeDueToRN" runat="server" />
             <input id="Hide_DaysBeforeDueToRNBeh" type="hidden" name="Hide_DaysBeforeDueToRNBeh" runat="server" />
             <asp:RequiredFieldValidator ID="Requiredfieldvalidator39" runat="server" ControlToValidate="E_txtDaysBeforeDueToRN" CssClass="validator_general" ErrorMessage="Jumlah hari sebelum JT buat Kwitansi Salah" Display="Dynamic"></asp:RequiredFieldValidator>
             <asp:RangeValidator ID="rgv_DaysBeforeDueToRN" runat="server" ControlToValidate="E_txtDaysBeforeDueToRN" CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Jumlah hari sebelum JT buat Kwitansi Salah" MaximumValue="9999"></asp:RangeValidator>
    </div>
    </div>
    <div class="form_box_header">
    <div class="form_single">
    <h5> Jumlah hari buat SKT (Surat Kuasa Tarik) </h5>
    </div>
    </div>
     <div class="form_box">
    <div class="form_single">
    <label> Branch</label>
    <asp:Label ID="lblODToRAL_Branch" runat="server"></asp:Label>
    </div>
    </div>
    <div class="form_box">
    <div class="form_single">
    <label class="label_req"> From HO</label>
            <asp:Label ID="lblDaysToGenerateRAL" runat="server"></asp:Label>  
            <asp:TextBox  onblur="com_NX('Tabs_pnlTabCollection_CollactionTab_Hide_ODToRALBeh', 'Tabs_pnlTabCollection_CollactionTab_E_txtODToRAL', 'Tabs_pnlTabCollection_CollactionTab_Hide_ODToRAL', 'Jumlah hari buat SKT (Surat Kuasa Tarik)');" ID="E_txtODToRAL" runat="server" MaxLength="4">0</asp:TextBox><asp:label  id='Label16' runat='server'>days</asp:label> 
            <asp:DropDownList ID="E_cboODToRALBehaviour" runat="server"/> 
            <input id="Hide_ODToRAL" type="hidden" name="Hide_ODToRAL" runat="server" />
            <input id="Hide_ODToRALBeh" type="hidden" name="Hide_ODToRALBeh" runat="server" />
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator40" runat="server" ControlToValidate="E_txtODToRAL" CssClass="validator_general" ErrorMessage="Jumlah hari buat SKT salah" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="rgv_ODToRAL" runat="server" ControlToValidate="E_txtODToRAL" CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Jumlah hari buat SKT salah" MaximumValue="9999"></asp:RangeValidator>
    </div>
    </div>
    <div class="form_box_header">
    <div class="form_single">
    <h5> Jangka Waktu SKT </h5>
    </div>
    </div>
      <div class="form_box">
    <div class="form_single">
    <label> Branch</label>
    <asp:Label ID="lblRALPeriod_Branch" runat="server"></asp:Label>
    </div>
    </div>
    <div class="form_box">
    <div class="form_single">
    <label class="label_req"> From HO</label>
            <asp:Label ID="lblRALPeriod" runat="server"></asp:Label> 
           <asp:TextBox  onblur="com_NX('Tabs_pnlTabCollection_CollactionTab_Hide_RALPeriodBeh', 'Tabs_pnlTabCollection_CollactionTab_E_txtRALPeriod', 'Tabs_pnlTabCollection_CollactionTab_Hide_RALPeriod', 'Jangka Waktu SKT');" ID="E_txtRALPeriod" runat="server" MaxLength="4">0</asp:TextBox><asp:label  id='Label17' runat='server'>days</asp:label> 
            <asp:DropDownList ID="E_cboRALPeriodBehaviour" runat="server"/> 
            <input id="Hide_RALPeriod" type="hidden" name="Hide_RALPeriod" runat="server" />
            <input id="Hide_RALPeriodBeh" type="hidden" name="Hide_RALPeriodBeh" runat="server" />
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator41" runat="server" ControlToValidate="E_txtRALPeriod" CssClass="validator_general" ErrorMessage="Jangka Waktu SKT Salah" Display="Dynamic"></asp:RequiredFieldValidator> <asp:RangeValidator ID="rgv_RALPeriod" runat="server" ControlToValidate="E_txtRALPeriod" CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Jangka Waktu SKT Salah" MaximumValue="9999"></asp:RangeValidator>
    </div>
    </div>
  
    <div class="form_box_header">
    <div class="form_single">
    <h5> Jumlah hari Maximum Kwitansi dibayar </h5>
    </div>
    </div>
     <div class="form_box">
    <div class="form_single">
    <label>  Branch</label>
    <asp:Label ID="lblMaxDaysRNPaid_Branch" runat="server"></asp:Label>
    </div>
    </div>
    <div class="form_box">
    <div class="form_single">
    <label class="label_req"> From HO</label>
            <asp:Label ID="lblMaxDaysRNPaid" runat="server"></asp:Label>
             <asp:TextBox  onblur="com_NX('Tabs_pnlTabCollection_CollactionTab_Hide_MaxDaysRNPaidBeh', 'Tabs_pnlTabCollection_CollactionTab_E_txtMaxDaysRNPaid', 'Tabs_pnlTabCollection_CollactionTab_Hide_MaxDaysRNPaid', 'Jumlah hari Maximum Kwitansi dibayar');" ID="E_txtMaxDaysRNPaid" runat="server" MaxLength="4">0</asp:TextBox><asp:label  id='Label18' runat='server'>days</asp:label> 
            <asp:DropDownList ID="E_cboMaxDaysRNPaidBehaviour" runat="server"/> 
            <input id="Hide_MaxDaysRNPaid" type="hidden" name="Hide_MaxDaysRNPaid" runat="server" />
            <input id="Hide_MaxDaysRNPaidBeh" type="hidden" name="Hide_MaxDaysRNPaidBeh" runat="server" />
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator42" runat="server" ControlToValidate="E_txtMaxDaysRNPaid" CssClass="validator_general" ErrorMessage="Jumlah hari Maximum Kwitansi dibayar Salah" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="rgv_MaxDaysRNPaid" runat="server" ControlToValidate="E_txtMaxDaysRNPaid" CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Jumlah hari Maximum Kwitansi dibayar Salah" MaximumValue="9999"></asp:RangeValidator>
    </div>
    </div>
   
    <div class="form_box_header">
    <div class="form_single"> <h5> Jumlah hari Maximum janji Bayar </h5>
    </div>
    </div>
    <div class="form_box">
    <div class="form_single">
    <label> Branch</label>
    <asp:Label ID="lblMaxPTPYDays_Branch" runat="server"></asp:Label>
    </div>
    </div>
    <div class="form_box">
    <div class="form_single">
    <label class="label_req"> From HO</label>
            <asp:Label ID="lblMaxPTPYDays" runat="server"></asp:Label> 
               <asp:TextBox  onblur="com_NX('Tabs_pnlTabCollection_CollactionTab_Hide_MaxPTPYDaysBeh', 'Tabs_pnlTabCollection_CollactionTab_E_txtMaxPTPYDays', 'Tabs_pnlTabCollection_CollactionTab_Hide_MaxPTPYDays', 'Jumlah hari Maximum janji Bayar');" ID="E_txtMaxPTPYDays" runat="server" MaxLength="4">0</asp:TextBox><asp:label  id='Label19' runat='server'>days</asp:label> 
            <asp:DropDownList ID="E_cboMaxPTPYDaysBehaviour" runat="server"/> 
            <input id="Hide_MaxPTPYDays" type="hidden" name="Hide_MaxPTPYDays" runat="server" />
            <input id="Hide_MaxPTPYDaysBeh" type="hidden" name="Hide_MaxPTPYDaysBeh" runat="server" />
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator43" runat="server" ControlToValidate="E_txtMaxPTPYDays" CssClass="validator_general" ErrorMessage="Jumlah hari Maximum janji Bayar Salah" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="rgv_MaxPTPYDays" runat="server" ControlToValidate="E_txtMaxPTPYDays" CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Jumlah hari Maximum janji Bayar Salah" MaximumValue="9999"></asp:RangeValidator>
    </div>
    </div>
    
    <div class="form_box_header">
    <div class="form_single">
    <h5> Janji Bayar ke Bank </h5>
    </div>
    </div>
     <div class="form_box">
    <div class="form_single">
    <label> Branch</label> <asp:Label ID="lblPTPYBank_Branch" runat="server"></asp:Label>
    </div>
    </div>
    <div class="form_box">
    <div class="form_single">
    <label class="label_req"> From HO</label>
            <asp:Label ID="lblPTPYBank" runat="server"></asp:Label> 
          <asp:TextBox  onblur="com_NX('Tabs_pnlTabCollection_CollactionTab_Hide_PTPYBankBeh', 'Tabs_pnlTabCollection_CollactionTab_E_txtPTPYBank', 'Tabs_pnlTabCollection_CollactionTab_Hide_PTPYBank', 'Janji Bayar ke Bank');" ID="E_txtPTPYBank" runat="server" MaxLength="4">0</asp:TextBox><asp:label  id='Label20' runat='server'>days</asp:label> 
            <asp:DropDownList ID="E_cboPTPYBankBehaviour" runat="server"/> 
            <input id="Hide_PTPYBank" type="hidden" name="Hide_PTPYBank" runat="server" />
            <input id="Hide_PTPYBankBeh" type="hidden" name="Hide_PTPYBankBeh" runat="server" />
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator44" runat="server" ControlToValidate="E_txtPTPYBank" CssClass="validator_general" ErrorMessage="Janji Bayar ke Bank Salah" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="rgv_PTPYBank" runat="server" ControlToValidate="E_txtPTPYBank"  CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Janji Bayar ke Bank Salah" MaximumValue="9999"></asp:RangeValidator>
    </div>
    </div>

   <div class="form_box_header">
   <div class="form_single"> <h5>Freq Perpanjangan SKT </h5>
    </div>
    </div>
    <div class="form_box">
    <div class="form_single">
    <label> Branch</label> <asp:Label ID="lblPerpanjanganSKT_Branch" runat="server"></asp:Label>
    </div>
    </div>
    <div class="form_box">
    <div class="form_single">
    <label class="label_req"> From HO</label>
            <asp:Label ID="lblPerpanjanganSKT" runat="server"></asp:Label> 
            <asp:TextBox  onblur="com_NX('Tabs_pnlTabCollection_CollactionTab_Hide_PerpanjanganSKTBeh', 'Tabs_pnlTabCollection_CollactionTab_E_txtPerpanjanganSKT', 'Tabs_pnlTabCollection_CollactionTab_Hide_PerpanjanganSKT', 'Freq Perpanjangan SKT');" ID="E_txtPerpanjanganSKT" runat="server" MaxLength="4">0</asp:TextBox><asp:label  id='Label28' runat='server'>kali</asp:label> 
            <asp:DropDownList ID="E_cboPerpanjanganSKTBehaviour" runat="server"/> 
            <input id="Hide_PerpanjanganSKT" type="hidden" name="Hide_PerpanjanganSKT" runat="server" />
            <input id="Hide_PerpanjanganSKTBeh" type="hidden" name="Hide_PerpanjanganSKTBeh" runat="server" />
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator60" runat="server" ControlToValidate="E_txtPerpanjanganSKT" CssClass="validator_general" ErrorMessage="Perpanjangan SKT Salah" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="rgv_PerpanjanganSKT" runat="server" ControlToValidate="E_txtPerpanjanganSKT" CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Perpanjangan SKT Salah" MaximumValue="9999"></asp:RangeValidator>
    </div>
    </div>
    <div class="form_box_header">
    <div class="form_single">
    <h5> Janji bayar ke Perusahaan </h5>
    </div>
    </div>
    <div class="form_box">
    <div class="form_single">
    <label> Branch</label>
    <asp:Label ID="lblPTPYCompany_Branch" runat="server"></asp:Label>
    </div>
    </div>
    <div class="form_box">
    <div class="form_single">
    <label class="label_req"> From HO</label>
            <asp:Label ID="lblPTPYCompany" runat="server"></asp:Label> 
                 <asp:TextBox  onblur="com_NX('Tabs_pnlTabCollection_CollactionTab_Hide_PTPYCompanyBeh', 'Tabs_pnlTabCollection_CollactionTab_E_txtPTPYCompany', 'Tabs_pnlTabCollection_CollactionTab_Hide_PTPYCompany', 'Janji bayar ke Perusahaan');" ID="E_txtPTPYCompany" runat="server" MaxLength="4">0</asp:TextBox><asp:label  id='Label21' runat='server'>days</asp:label> 
            <asp:DropDownList ID="E_cboPTPYCompanyBehaviour" runat="server"/> 
            <input id="Hide_PTPYCompany" type="hidden" name="Hide_PTPYCompany" runat="server" />
            <input id="Hide_PTPYCompanyBeh" type="hidden" name="Hide_PTPYCompanyBeh" runat="server" />
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator45" runat="server" ControlToValidate="E_txtPTPYCompany" CssClass="validator_general" ErrorMessage="Janji bayar ke Perusahaan Salah" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="rgv_PTPYCompany" runat="server" ControlToValidate="E_txtPTPYCompany" CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Janji bayar ke Perusahaan Salah" MaximumValue="9999"></asp:RangeValidator>
    </div>
    </div>
   
    <div class="form_box_header">
    <div class="form_single"> <h5> Janji bayar ke Supplier </h5>
    </div>
    </div>
    
    <div class="form_box">
    <div class="form_single">
    <label> Branch</label>
    <asp:Label ID="lblPTPYSupplier_Branch" runat="server"></asp:Label>
    </div>
    </div>
    <div class="form_box">
    <div class="form_single">
    <label class="label_req"> From HO</label>
                <asp:Label ID="lblPTPYSupplier" runat="server"></asp:Label> 
                 <asp:TextBox  onblur="com_NX('Tabs_pnlTabCollection_CollactionTab_Hide_PTPYSupplierBeh', 'Tabs_pnlTabCollection_CollactionTab_E_txtPTPYSupplier', 'Tabs_pnlTabCollection_CollactionTab_Hide_PTPYSupplier', 'Janji bayar ke Supplier');" ID="E_txtPTPYSupplier" runat="server" MaxLength="4">0</asp:TextBox><asp:label  id='Label22' runat='server'>days</asp:label> 
                <asp:DropDownList ID="E_cboPTPYSupplierBehaviour" runat="server"/> 
                <input id="Hide_PTPYSupplier" type="hidden" name="Hide_PTPYSupplier" runat="server" />
                <input id="Hide_PTPYSupplierBeh" type="hidden" name="Hide_PTPYSupplierBeh" runat="server" />
                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator46" runat="server" ControlToValidate="E_txtPTPYSupplier" CssClass="validator_general" ErrorMessage="Janji bayar ke Supplier Salah" Display="Dynamic"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="rgv_PTPYSupplier" runat="server" ControlToValidate="E_txtPTPYSupplier" CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Janji bayar ke Supplier Salah" MaximumValue="9999"></asp:RangeValidator>
    </div>
    </div>

    <div class="form_box_header">
    <div class="form_single"> <h5> Perpanjangan SKT </h5>
    </div>
    </div>
     <div class="form_box">
    <div class="form_single">
    <label> Branch</label> <asp:Label ID="lblRALExtension_Branch" runat="server"></asp:Label>
    </div>
    </div>

    <div class="form_box">
    <div class="form_single">
    <label class="label_req"> From HO</label>
            <asp:Label ID="lblRALExtension" runat="server"></asp:Label> 
          <asp:TextBox  onblur="com_NX('Tabs_pnlTabCollection_CollactionTab_Hide_RALExtensionBeh', 'Tabs_pnlTabCollection_CollactionTab_E_txtRALExtension', 'Tabs_pnlTabCollection_CollactionTab_Hide_RALExtension', 'Perpanjangan SKT');" ID="E_txtRALExtension" runat="server" MaxLength="4">0</asp:TextBox><asp:label  id='Label23' runat='server'>days</asp:label> 
            <asp:DropDownList ID="E_cboRALExtensionBehaviour" runat="server"/> 
            <input id="Hide_RALExtension" type="hidden" name="Hide_RALExtension" runat="server" />
            <input id="Hide_RALExtensionBeh" type="hidden" name="Hide_RALExtensionBeh" runat="server" />
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator47" runat="server" ControlToValidate="E_txtRALExtension" CssClass="validator_general" ErrorMessage="Perpanjangan SKT Salah" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="rgv_RALExtension" runat="server" ControlToValidate="E_txtRALExtension" CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Perpanjangan SKT Salah" MaximumValue="9999"></asp:RangeValidator>
    </div>
    </div>
   

    <div class="form_box_header">
    <div class="form_single"> <h5> Jangka Waktu jadi Inventory </h5>
    </div>
    </div>

        <div class="form_box"> 
        <div class="form_single">
            <label> Branch</label>
            <asp:Label ID="lblInventoryExpected_Branch" runat="server"></asp:Label>
           </div>
    </div>

    <div class="form_box">
    <div class="form_single"> 
    <label class="label_req"> From HO</label>
            <asp:Label ID="lblInventoryExpected" runat="server"></asp:Label> 
             <asp:TextBox  onblur="com_NX('Tabs_pnlTabCollection_CollactionTab_Hide_InventoryExpectedBeh', 'Tabs_pnlTabCollection_CollactionTab_E_txtInventoryExpected', 'Tabs_pnlTabCollection_CollactionTab_Hide_InventoryExpected', 'Jangka Waktu jadi Inventory');"  ID="E_txtInventoryExpected" runat="server" MaxLength="4">0</asp:TextBox><asp:label  id='Label24' runat='server'>days</asp:label> 
            <asp:DropDownList ID="E_cboInventoryExpectedBehaviour" runat="server"/> 
            <input id="Hide_InventoryExpected" visible="false" type="hidden" name="Hide_InventoryExpected" runat="server" />
            <input id="Hide_InventoryExpectedBeh" visible="false" type="hidden" name="Hide_InventoryExpectedBeh" runat="server" />
    <asp:RequiredFieldValidator ID="Requiredfieldvalidator48" runat="server" ControlToValidate="E_txtInventoryExpected" CssClass="validator_general" ErrorMessage="Jangka Waktu jadi Inventory Salah" Display="Dynamic"></asp:RequiredFieldValidator>
    <asp:RangeValidator ID="rgv_InventoryExpected" runat="server" ControlToValidate="E_txtInventoryExpected" CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Jangka Waktu jadi Inventory Salah" MaximumValue="9999"></asp:RangeValidator>
    </div>
    </div>


                
    </div>