﻿Public Class ProductCollectionHOTab
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub
    Public Property Product() As Parameter.Product
        Get
            Return CType(ViewState("ProductCollectionTabProduct"), Parameter.Product)
        End Get
        Set(ByVal Value As Parameter.Product)
            ViewState("ProductCollectionTabProduct") = Value
        End Set
    End Property
    Public Property UCMode As String
        Get
            Return CType(ViewState("COLLECTIONTABUCMODE"), String)
        End Get
        Set(value As String)
            ViewState("COLLECTIONTABUCMODE") = value
        End Set
    End Property
    Sub InitialControls()

        lblIsSPAutomatic.Visible = (UCMode = "VIEW")
        E_cboIsSPAutomatic.Visible = (UCMode <> "VIEW")
        E_txtLengthSPProcess.Visible = (UCMode <> "VIEW")

        lblIsSP1Automatic.Visible = (UCMode = "VIEW")
        E_cboIsSP1Automatic.Visible = (UCMode <> "VIEW")
        E_txtLengthSP1Process.Visible = (UCMode <> "VIEW")



        lblIsSP2Automatic.Visible = (UCMode = "VIEW")
        E_cboIsSP2Automatic.Visible = (UCMode <> "VIEW")
        E_txtLengthSP2Process.Visible = (UCMode <> "VIEW")

        lblLMDProcessed.Visible = (UCMode = "VIEW")
        E_txtLengthMainDocProcess.Visible = (UCMode <> "VIEW")
        E_cboLengthMainDocProcessBehaviour.Visible = (UCMode <> "VIEW")



        lblLMDTaken.Visible = (UCMode = "VIEW")
        E_txtLengthMainDocTaken.Visible = (UCMode <> "VIEW")
        E_cboLengthMainDocTakenBehaviour.Visible = (UCMode <> "VIEW")

        lblGPLC.Visible = (UCMode = "VIEW")
        E_txtGracePeriodLateCharges.Visible = (UCMode <> "VIEW")
        E_cboGracePeriodLateChargesBehaviour.Visible = (UCMode <> "VIEW")

        lblDaystoremindInstallment.Visible = (UCMode = "VIEW")
        E_txtDeskCollPhoneRemind.Visible = (UCMode <> "VIEW")
        E_cboDeskCollPhoneRemindBehaviour.Visible = (UCMode <> "VIEW")

        lblDaysOverduetocall.Visible = (UCMode = "VIEW")
        E_txtDeskCollOD.Visible = (UCMode <> "VIEW")
        E_cboDeskCollODBehaviour.Visible = (UCMode <> "VIEW")

        lblPreviousOverduetoremind.Visible = (UCMode = "VIEW")
        E_txtPrevODToRemind.Visible = (UCMode <> "VIEW")
        E_cboPrevODToRemindBehaviour.Visible = (UCMode <> "VIEW")

        lblPDCRequesttocall.Visible = (UCMode = "VIEW")
        E_txtPDCDaysToRemind.Visible = (UCMode <> "VIEW")
        E_cboPDCDaysToRemindBehaviour.Visible = (UCMode <> "VIEW")

        lblDaysToRemind.Visible = (UCMode = "VIEW")
        E_txtDeskCollSMSRemind.Visible = (UCMode <> "VIEW")
        E_cboDeskCollSMSRemindBehaviour.Visible = (UCMode <> "VIEW")


        ''meisan

        lblDaysToRemind_2.Visible = (UCMode = "VIEW")
        E_txtDeskCollSMSRemind_2.Visible = (UCMode <> "VIEW")
        E_cboDeskCollSMSRemindBehaviour_2.Visible = (UCMode <> "VIEW")

        lblDaysToRemind_3.Visible = (UCMode = "VIEW")
        E_txtDeskCollSMSRemind_3.Visible = (UCMode <> "VIEW")
        E_cboDeskCollSMSRemindBehaviour_3.Visible = (UCMode <> "VIEW")

        lblDaysToGenerateDCR.Visible = (UCMode = "VIEW")
        E_txtDCR.Visible = (UCMode <> "VIEW")
        E_cboDCRBehaviour.Visible = (UCMode <> "VIEW")


        lblDaysBeforeDuetoRN.Visible = (UCMode = "VIEW")
        E_txtDaysBeforeDueToRN.Visible = (UCMode <> "VIEW")
        E_cboDaysBeforeDueToRNBehaviour.Visible = (UCMode <> "VIEW")

        lblDaysToGenerateRAL.Visible = (UCMode = "VIEW")
        E_txtODToRAL.Visible = (UCMode <> "VIEW")
        E_cboODToRALBehaviour.Visible = (UCMode <> "VIEW")

        lblRALPeriod.Visible = (UCMode = "VIEW")
        E_txtRALPeriod.Visible = (UCMode <> "VIEW")
        E_cboRALPeriodBehaviour.Visible = (UCMode <> "VIEW")

        lblMaxDaysRNPaid.Visible = (UCMode = "VIEW")
        E_txtMaxDaysRNPaid.Visible = (UCMode <> "VIEW")
        E_cboMaxDaysRNPaidBehaviour.Visible = (UCMode <> "VIEW")

        lblMaxPTPYDays.Visible = (UCMode = "VIEW")
        E_txtMaxPTPYDays.Visible = (UCMode <> "VIEW")
        E_cboMaxPTPYDaysBehaviour.Visible = (UCMode <> "VIEW")

        lblPTPYBank.Visible = (UCMode = "VIEW")
        E_txtPTPYBank.Visible = (UCMode <> "VIEW")
        E_cboPTPYBankBehaviour.Visible = (UCMode <> "VIEW")

        lblPTPYCompany.Visible = (UCMode = "VIEW")
        E_txtPTPYCompany.Visible = (UCMode <> "VIEW")
        E_cboPTPYCompanyBehaviour.Visible = (UCMode <> "VIEW")


        lblPTPYSupplier.Visible = (UCMode = "VIEW")
        E_txtPTPYSupplier.Visible = (UCMode <> "VIEW")
        E_cboPTPYSupplierBehaviour.Visible = (UCMode <> "VIEW")

        ''freq Perpanjangan SKT

        lblPerpanjanganSKT.Visible = (UCMode = "VIEW")
        E_txtPerpanjanganSKT.Visible = (UCMode <> "VIEW")
        E_cboPerpanjanganSKTBehaviour.Visible = (UCMode <> "VIEW")

        lblRALExtension.Visible = (UCMode = "VIEW")
        E_txtRALExtension.Visible = (UCMode <> "VIEW")
        E_cboRALExtensionBehaviour.Visible = (UCMode <> "VIEW")

        lblInventoryExpected.Visible = (UCMode = "VIEW")
        E_txtInventoryExpected.Visible = (UCMode <> "VIEW")
        E_cboInventoryExpectedBehaviour.Visible = (UCMode <> "VIEW")

        Label1.Visible = (UCMode <> "VIEW")
        Label2.Visible = (UCMode <> "VIEW")
        Label3.Visible = (UCMode <> "VIEW")
        Label4.Visible = (UCMode <> "VIEW")

        Label5.Visible = (UCMode <> "VIEW")
        Label6.Visible = (UCMode <> "VIEW")
        Label7.Visible = (UCMode <> "VIEW")
        Label8.Visible = (UCMode <> "VIEW")
        Label9.Visible = (UCMode <> "VIEW")
        Label10.Visible = (UCMode <> "VIEW")
        Label11.Visible = (UCMode <> "VIEW")
        Label12.Visible = (UCMode <> "VIEW")
        Label13.Visible = (UCMode <> "VIEW")
        Label14.Visible = (UCMode <> "VIEW")
        Label15.Visible = (UCMode <> "VIEW")
        Label16.Visible = (UCMode <> "VIEW")
        Label17.Visible = (UCMode <> "VIEW")
        Label18.Visible = (UCMode <> "VIEW")
        Label19.Visible = (UCMode <> "VIEW")
        Label20.Visible = (UCMode <> "VIEW")
        Label21.Visible = (UCMode <> "VIEW")
        Label22.Visible = (UCMode <> "VIEW")
        Label23.Visible = (UCMode <> "VIEW")
        Label24.Visible = (UCMode <> "VIEW")
        Label25.Visible = (UCMode <> "VIEW")

        If (UCMode = "ADD") Then
            initFieldsForAddMode()
        End If
        If (UCMode = "EDIT") Then
            BindFiedsForEdit()
        End If
    End Sub

    Sub initFieldsForAddMode()

    End Sub

    Public Sub BindFiedsForEdit()

        E_cboIsSPAutomatic.SelectedIndex = E_cboIsSPAutomatic.Items.IndexOf(E_cboIsSPAutomatic.Items.FindByValue(IIf(Product.IsSPAutomatic_HO = True, "1", "0")))
        E_txtLengthSPProcess.Text = CStr(Product.LengthSPProcess_HO)

        Hide_LengthSPProcess.Value = CStr(Product.LengthSPProcess_HO)

        If Product.IsSPAutomatic_HO = True Then
            Behaviour_Lock(Product.IsSPAutomaticBehaviour_HO, E_txtLengthSPProcess, E_cboIsSPAutomatic)
        Else
            E_cboIsSPAutomatic.Enabled = False
            E_txtLengthSPProcess.Enabled = False
        End If



        ' SP1Automatic
        E_cboIsSP1Automatic.SelectedIndex = E_cboIsSP1Automatic.Items.IndexOf(E_cboIsSP1Automatic.Items.FindByValue(IIf(Product.IsSP1Automatic_HO = True, "1", "0")))
        E_txtLengthSP1Process.Text = CStr(Product.LengthSP1Process_HO)
        Hide_LengthSP1Process.Value = CStr(Product.LengthSP1Process_HO)


        If Product.IsSP1Automatic_HO = True Then
            Behaviour_Lock(Product.IsSP1AutomaticBehaviour_HO, E_txtLengthSP1Process, E_cboIsSP1Automatic)
        Else
            E_cboIsSP1Automatic.Enabled = False
            E_txtLengthSP1Process.Enabled = False
        End If
    

        ' SP2Automatic
        E_cboIsSP2Automatic.SelectedIndex = E_cboIsSP2Automatic.Items.IndexOf(E_cboIsSP2Automatic.Items.FindByValue(IIf(Product.IsSP2Automatic_HO = True, "1", "0")))
        E_txtLengthSP2Process.Text = CStr(Product.LengthSP2Process_HO)
        Hide_LengthSP2Process.Value = CStr(Product.LengthSP2Process_HO)

        If Product.IsSP2Automatic_HO = True Then
            Behaviour_Lock(Product.IsSP2AutomaticBehaviour_HO, E_txtLengthSP2Process, E_cboIsSP2Automatic)
        Else
            E_cboIsSP2Automatic.Enabled = False
            E_txtLengthSP2Process.Enabled = False
        End If
        

        'Length Main Document Processed
        E_txtLengthMainDocProcess.Text = CStr(Product.LengthMainDocProcess_HO)
        Hide_LengthMainDocProcess.Value = CStr(Product.LengthMainDocProcess_HO)
        Hide_LengthMainDocProcessBeh.Value = Product.LengthMainDocProcessBehaviour_HO
        E_cboLengthMainDocProcessBehaviour.SelectedIndex = E_cboLengthMainDocProcessBehaviour.Items.IndexOf(E_cboLengthMainDocProcessBehaviour.Items.FindByValue(Product.LengthMainDocProcessBehaviour_HO))
        Behaviour_Lock(Product.LengthMainDocProcessBehaviour_HO, E_txtLengthMainDocProcess, E_cboLengthMainDocProcessBehaviour)

        

        'Length Main Document Taken
        E_txtLengthMainDocTaken.Text = CStr(Product.LengthMainDocTaken_HO)
        Hide_LengthMainDocTaken.Value = CStr(Product.LengthMainDocTaken_HO)
        Hide_LengthMainDocProcessBeh.Value = Product.LengthMainDocTakenBehaviour_HO
        E_cboLengthMainDocTakenBehaviour.SelectedIndex = E_cboLengthMainDocTakenBehaviour.Items.IndexOf(E_cboLengthMainDocTakenBehaviour.Items.FindByValue(Product.LengthMainDocTakenBehaviour_HO))
        Behaviour_Lock(Product.LengthMainDocTakenBehaviour_HO, E_txtLengthMainDocTaken, E_cboLengthMainDocTakenBehaviour)

        


        'Grace Period Late Charges
        E_txtGracePeriodLateCharges.Text = CStr(Product.GracePeriodLateCharges_HO)
        Hide_GracePeriodLateCharges.Value = CStr(Product.GracePeriodLateCharges_HO)
        Hide_GracePeriodLateChargesBeh.Value = Product.GracePeriodLateChargesBehaviour_HO
        E_cboGracePeriodLateChargesBehaviour.SelectedIndex = E_cboGracePeriodLateChargesBehaviour.Items.IndexOf(E_cboGracePeriodLateChargesBehaviour.Items.FindByValue(Product.GracePeriodLateChargesBehaviour_HO))
        Behaviour_Lock(Product.GracePeriodLateChargesBehaviour_HO, E_txtGracePeriodLateCharges, E_cboGracePeriodLateChargesBehaviour)

         
        E_txtDeskCollPhoneRemind.Text = CStr(Product.DeskCollPhoneRemind_HO)
        Hide_DeskCollPhoneRemind.Value = CStr(Product.DeskCollPhoneRemind_HO)
        Hide_DeskCollPhoneRemindBeh.Value = Product.DeskCollPhoneRemindBehaviour_HO
        E_cboDeskCollPhoneRemindBehaviour.SelectedIndex = E_cboDeskCollPhoneRemindBehaviour.Items.IndexOf(E_cboDeskCollPhoneRemindBehaviour.Items.FindByValue(Product.DeskCollPhoneRemindBehaviour_HO))
        Behaviour_Lock(Product.DeskCollPhoneRemindBehaviour_HO, E_txtDeskCollPhoneRemind, E_cboDeskCollPhoneRemindBehaviour)




        'Days overdue to call
        E_txtDeskCollOD.Text = CStr(Product.DeskCollOD_HO)
        Hide_DeskCollOD.Value = CStr(Product.DeskCollOD_HO)
        Hide_DeskCollODBeh.Value = Product.DeskCollODBehaviour_HO
        E_cboDeskCollODBehaviour.SelectedIndex = E_cboDeskCollODBehaviour.Items.IndexOf(E_cboDeskCollODBehaviour.Items.FindByValue(Product.DeskCollODBehaviour_HO))
        Behaviour_Lock(Product.DeskCollODBehaviour_HO, E_txtDeskCollOD, E_cboDeskCollODBehaviour)




        'Previous overdue to remind
        E_txtPrevODToRemind.Text = CStr(Product.PrevODToRemind_HO)
        Hide_PrevODToRemind.Value = CStr(Product.PrevODToRemind_HO)
        Hide_PrevODToRemindBeh.Value = Product.PrevODToRemindBehaviour_HO
        E_cboPrevODToRemindBehaviour.SelectedIndex = E_cboPrevODToRemindBehaviour.Items.IndexOf(E_cboPrevODToRemindBehaviour.Items.FindByValue(Product.PrevODToRemindBehaviour_HO))
        Behaviour_Lock(Product.PrevODToRemindBehaviour_HO, E_txtPrevODToRemind, E_cboPrevODToRemindBehaviour)


        'PDC request to call
        E_txtPDCDaysToRemind.Text = CStr(Product.PDCDayToRemind_HO)
        Hide_PDCDaysToRemind.Value = CStr(Product.PDCDayToRemind_HO)
        Hide_PDCDaysToRemindBeh.Value = Product.PDCDayToRemindBehaviour_HO
        E_cboPDCDaysToRemindBehaviour.SelectedIndex = E_cboPDCDaysToRemindBehaviour.Items.IndexOf(E_cboPDCDaysToRemindBehaviour.Items.FindByValue(Product.PDCDayToRemindBehaviour_HO))
        Behaviour_Lock(Product.PDCDayToRemindBehaviour_HO, E_txtPDCDaysToRemind, E_cboPDCDaysToRemindBehaviour)


        'Days to remind by SMS
        E_txtDeskCollSMSRemind.Text = CStr(Product.DeskCollSMSRemind_HO)
        Hide_DeskCollSMSRemind.Value = CStr(Product.DeskCollSMSRemind_HO)
        Hide_DeskCollSMSRemindBeh.Value = Product.DeskCollSMSRemindBehaviour_HO
        E_cboDeskCollSMSRemindBehaviour.SelectedIndex = E_cboDeskCollSMSRemindBehaviour.Items.IndexOf(E_cboDeskCollSMSRemindBehaviour.Items.FindByValue(Product.DeskCollSMSRemindBehaviour_HO))
        Behaviour_Lock(Product.DeskCollSMSRemindBehaviour_HO, E_txtDeskCollSMSRemind, E_cboDeskCollSMSRemindBehaviour)

        ''meisan
        E_txtDeskCollSMSRemind_2.Text = CStr(Product.DeskCollSMSRemind2_HO)
        Hide_DeskCollSMSRemind_2.Value = CStr(Product.DeskCollSMSRemind2_HO)
        Hide_DeskCollSMSRemindBeh_2.Value = Product.DeskCollSMSRemindBehaviour2_HO
        E_cboDeskCollSMSRemindBehaviour_2.SelectedIndex = E_cboDeskCollSMSRemindBehaviour_2.Items.IndexOf(E_cboDeskCollSMSRemindBehaviour_2.Items.FindByValue(Product.DeskCollSMSRemindBehaviour2_HO))
        Behaviour_Lock(Product.DeskCollSMSRemindBehaviour2_HO, E_txtDeskCollSMSRemind_2, E_cboDeskCollSMSRemindBehaviour_2)


        E_txtDeskCollSMSRemind_3.Text = CStr(Product.DeskCollSMSRemind3_HO)
        Hide_DeskCollSMSRemind_3.Value = CStr(Product.DeskCollSMSRemind3_HO)
        Hide_DeskCollSMSRemindBeh_3.Value = Product.DeskCollSMSRemindBehaviour3_HO
        E_cboDeskCollSMSRemindBehaviour_3.SelectedIndex = E_cboDeskCollSMSRemindBehaviour_3.Items.IndexOf(E_cboDeskCollSMSRemindBehaviour_3.Items.FindByValue(Product.DeskCollSMSRemindBehaviour3_HO))
        Behaviour_Lock(Product.DeskCollSMSRemindBehaviour3_HO, E_txtDeskCollSMSRemind_3, E_cboDeskCollSMSRemindBehaviour_3)


        ''Freq Perpanjangan SKT
        E_txtPerpanjanganSKT.Text = CStr(Product.PerpanjanganSKT_HO)
        Hide_PerpanjanganSKT.Value = CStr(Product.PerpanjanganSKT_HO)
        Hide_PerpanjanganSKTBeh.Value = Product.PerpanjanganSKTBehaviour_HO
        E_cboPerpanjanganSKTBehaviour.SelectedIndex = E_cboPerpanjanganSKTBehaviour.Items.IndexOf(E_cboPerpanjanganSKTBehaviour.Items.FindByValue(Product.PerpanjanganSKTBehaviour_HO))
        Behaviour_Lock(Product.PerpanjanganSKTBehaviour_HO, E_txtPerpanjanganSKT, E_cboPerpanjanganSKTBehaviour)



        'Days to generate DCR
        E_txtDCR.Text = CStr(Product.DCR_HO)
        Hide_DCR.Value = CStr(Product.DCR_HO)
        Hide_DCRBeh.Value = Product.DCRBehaviour_HO
        E_cboDCRBehaviour.SelectedIndex = E_cboDCRBehaviour.Items.IndexOf(E_cboDCRBehaviour.Items.FindByValue(Product.DCRBehaviour_HO))

        Behaviour_Lock(Product.DCRBehaviour_HO, E_txtDCR, E_cboDCRBehaviour)



        'Days before due to generate Receipt Notes
        E_txtDaysBeforeDueToRN.Text = CStr(Product.DaysBeforeDueToRN_HO)
        Hide_DaysBeforeDueToRN.Value = CStr(Product.DaysBeforeDueToRN_HO)
        Hide_DaysBeforeDueToRNBeh.Value = Product.DaysBeforeDueToRNBehaviour_HO
        E_cboDaysBeforeDueToRNBehaviour.SelectedIndex = E_cboDaysBeforeDueToRNBehaviour.Items.IndexOf(E_cboDaysBeforeDueToRNBehaviour.Items.FindByValue(Product.DaysBeforeDueToRNBehaviour_HO))
        Behaviour_Lock(Product.DaysBeforeDueToRNBehaviour_HO, E_txtDaysBeforeDueToRN, E_cboDaysBeforeDueToRNBehaviour)



        'Days to generate RAL
        E_txtODToRAL.Text = CStr(Product.ODToRAL_HO)
        Hide_ODToRAL.Value = CStr(Product.ODToRAL_HO)
        Hide_ODToRALBeh.Value = Product.ODToRALBehaviour_HO
        E_cboODToRALBehaviour.SelectedIndex = E_cboODToRALBehaviour.Items.IndexOf(E_cboODToRALBehaviour.Items.FindByValue(Product.ODToRALBehaviour_HO))
        Behaviour_Lock(Product.ODToRALBehaviour_HO, E_txtODToRAL, E_cboODToRALBehaviour)



        'RAL Period
        E_txtRALPeriod.Text = CStr(Product.RALPeriod_HO)
        Hide_RALPeriod.Value = CStr(Product.RALPeriod_HO)
        Hide_RALPeriodBeh.Value = Product.RALPeriodBehaviour_HO
        E_cboRALPeriodBehaviour.SelectedIndex = E_cboRALPeriodBehaviour.Items.IndexOf(E_cboRALPeriodBehaviour.Items.FindByValue(Product.RALPeriodBehaviour_HO))
        Behaviour_Lock(Product.RALPeriodBehaviour_HO, E_txtRALPeriod, E_cboRALPeriodBehaviour)



        'Maximum days Receipt notes paid
        E_txtMaxDaysRNPaid.Text = CStr(Product.MaxDaysRNPaid_HO)
        Hide_MaxDaysRNPaid.Value = CStr(Product.MaxDaysRNPaid_HO)
        Hide_MaxDaysRNPaidBeh.Value = Product.MaxDaysRNPaidBehaviour_HO
        E_cboMaxDaysRNPaidBehaviour.SelectedIndex = E_cboMaxDaysRNPaidBehaviour.Items.IndexOf(E_cboMaxDaysRNPaidBehaviour.Items.FindByValue(Product.MaxDaysRNPaidBehaviour_HO))
        Behaviour_Lock(Product.MaxDaysRNPaidBehaviour_HO, E_txtMaxDaysRNPaid, E_cboMaxDaysRNPaidBehaviour)

        'Maximum Promise To Pay days
        E_txtMaxPTPYDays.Text = CStr(Product.MaxPTPYDays_HO)
        Hide_MaxPTPYDays.Value = CStr(Product.MaxPTPYDays_HO)
        Hide_MaxPTPYDaysBeh.Value = Product.MaxPTPYDaysBehaviour_HO
        E_cboMaxPTPYDaysBehaviour.SelectedIndex = E_cboMaxPTPYDaysBehaviour.Items.IndexOf(E_cboMaxPTPYDaysBehaviour.Items.FindByValue(Product.MaxPTPYDaysBehaviour_HO))




        'Promise To Pay To Bank
        E_txtPTPYBank.Text = CStr(Product.PTPYBank_HO)
        Hide_PTPYBank.Value = CStr(Product.PTPYBank_HO)
        Hide_PTPYBankBeh.Value = Product.PTPYBankBehaviour_HO
        E_cboPTPYBankBehaviour.SelectedIndex = E_cboPTPYBankBehaviour.Items.IndexOf(E_cboPTPYBankBehaviour.Items.FindByValue(Product.PTPYBankBehaviour_HO))
        Behaviour_Lock(Product.PTPYBankBehaviour_HO, E_txtPTPYBank, E_cboPTPYBankBehaviour)


        'Promise To Pay To Company
        E_txtPTPYCompany.Text = CStr(Product.PTPYCompany_HO)
        Hide_PTPYCompany.Value = CStr(Product.PTPYCompany_HO)
        Hide_PTPYCompanyBeh.Value = Product.PTPYCompanyBehaviour_HO
        E_cboPTPYCompanyBehaviour.SelectedIndex = E_cboPTPYCompanyBehaviour.Items.IndexOf(E_cboPTPYCompanyBehaviour.Items.FindByValue(Product.PTPYCompanyBehaviour_HO))
        Behaviour_Lock(Product.PTPYCompanyBehaviour_HO, E_txtPTPYCompany, E_cboPTPYCompanyBehaviour)




        'Promise To Pay To Supplier
        E_txtPTPYSupplier.Text = CStr(Product.PTPYSupplier_HO)
        Hide_PTPYSupplier.Value = CStr(Product.PTPYSupplier_HO)
        Hide_PTPYSupplierBeh.Value = Product.PTPYSupplierBehaviour_HO
        E_cboPTPYSupplierBehaviour.SelectedIndex = E_cboPTPYSupplierBehaviour.Items.IndexOf(E_cboPTPYSupplierBehaviour.Items.FindByValue(Product.PTPYSupplierBehaviour_HO))
        Behaviour_Lock(Product.PTPYSupplierBehaviour_HO, E_txtPTPYSupplier, E_cboPTPYSupplierBehaviour)


        'RAL Extension
        E_txtRALExtension.Text = CStr(Product.RALExtension_HO)
        Hide_RALExtension.Value = CStr(Product.RALExtension_HO)
        Hide_RALExtensionBeh.Value = Product.RALExtensionBehaviour_HO
        E_cboRALExtensionBehaviour.SelectedIndex = E_cboRALExtensionBehaviour.Items.IndexOf(E_cboRALExtensionBehaviour.Items.FindByValue(Product.RALExtensionBehaviour_HO))
        Behaviour_Lock(Product.RALExtensionBehaviour_HO, E_txtRALExtension, E_cboRALExtensionBehaviour)




        'Inventory Expected
        E_txtInventoryExpected.Text = CStr(Product.InventoryExpected_HO)
        Hide_InventoryExpected.Value = CStr(Product.InventoryExpected_HO)
        Hide_InventoryExpectedBeh.Value = Product.InventoryExpectedBehaviour_HO
        E_cboInventoryExpectedBehaviour.SelectedIndex = E_cboInventoryExpectedBehaviour.Items.IndexOf(E_cboInventoryExpectedBehaviour.Items.FindByValue(Product.InventoryExpectedBehaviour_HO))
        Behaviour_Lock(Product.InventoryExpectedBehaviour_HO, E_txtInventoryExpected, E_cboInventoryExpectedBehaviour)



        setBranchText()
    End Sub

    Sub setBranchText()
        lblLengthSPProcess_Branch.Text = IIf(Product.IsSPAutomatic = True, "Yes", "No") & " Length SP Process " & CStr(Product.LengthSPProcess) & " days"
        lblLengthSP1Process_Branch.Text = IIf(Product.IsSP1Automatic = True, "Yes", "No") & " Length SP 1 Process " & CStr(Product.LengthSP1Process) & " days"
        lblLengthSP2Process_Branch.Text = IIf(Product.IsSP2Automatic = True, "Yes", "No") & " Length SP 2 Process " & CStr(Product.LengthSP2Process) & " days"
        lblLengthMainDocProcess_Branch.Text = CStr(Product.LengthMainDocProcess) & " days " & Parameter.Product.Behaviour_Value(Product.LengthMainDocProcessBehaviour)
        lblLengthMainDocTaken_Branch.Text = CStr(Product.LengthMainDocTaken) & " days " & Parameter.Product.Behaviour_Value(Product.LengthMainDocTakenBehaviour)
        lblGracePeriodLateCharges_Branch.Text = CStr(Product.GracePeriodLateCharges) & " days " & Parameter.Product.Behaviour_Value(Product.GracePeriodLateChargesBehaviour)

        lblDeskCollPhoneRemind_Branch.Text = CStr(Product.DeskCollPhoneRemind) & " days " & Parameter.Product.Behaviour_Value(Product.DeskCollPhoneRemindBehaviour)
        lblDeskCollOD_Branch.Text = CStr(Product.DeskCollOD) & " days " & Parameter.Product.Behaviour_Value(Product.DeskCollODBehaviour)
        lblPrevODToRemind_Branch.Text = CStr(Product.PrevODToRemind) & " days " & Parameter.Product.Behaviour_Value(Product.PrevODToRemindBehaviour)
        lblPDCDaysToRemind_Branch.Text = CStr(Product.PDCDayToRemind) & " Dayss " & Parameter.Product.Behaviour_Value(Product.PDCDayToRemindBehaviour)
        lblDeskCollSMSRemind_Branch.Text = CStr(Product.DeskCollSMSRemind) & " days " & Parameter.Product.Behaviour_Value(Product.DeskCollSMSRemindBehaviour)

        ''meisan
        lblDeskCollSMSRemind_Branch2.Text = CStr(Product.DeskCollSMSRemind2) & " days " & Parameter.Product.Behaviour_Value(Product.DeskCollSMSRemindBehaviour3)

        lblDCR_Branch.Text = CStr(Product.DCR) & " days " & Parameter.Product.Behaviour_Value(Product.DCRBehaviour)
        lblDaysBeforeDueToRN_Branch.Text = CStr(Product.DaysBeforeDueToRN) & " days " & Parameter.Product.Behaviour_Value(Product.DaysBeforeDueToRNBehaviour)
        lblODToRAL_Branch.Text = CStr(Product.ODToRAL) & " days " & Parameter.Product.Behaviour_Value(Product.ODToRALBehaviour)
        lblRALPeriod_Branch.Text = CStr(Product.RALPeriod) & " days " & Parameter.Product.Behaviour_Value(Product.RALPeriodBehaviour)
        lblMaxDaysRNPaid_Branch.Text = CStr(Product.MaxDaysRNPaid) & " days " & Parameter.Product.Behaviour_Value(Product.MaxDaysRNPaidBehaviour)

        lblMaxPTPYDays_Branch.Text = CStr(Product.MaxPTPYDays) & " days " & Parameter.Product.Behaviour_Value(Product.MaxPTPYDaysBehaviour)
        lblPTPYBank_Branch.Text = CStr(Product.PTPYBank) & " days " & Parameter.Product.Behaviour_Value(Product.PTPYBankBehaviour)
        lblPTPYCompany_Branch.Text = CStr(Product.PTPYCompany) & " days " & Parameter.Product.Behaviour_Value(Product.PTPYCompanyBehaviour)
        lblPTPYSupplier_Branch.Text = CStr(Product.PTPYSupplier) & " days " & Parameter.Product.Behaviour_Value(Product.PTPYSupplierBehaviour)

        ''freqperpanjanganskt
        lblPerpanjanganSKT.Text = CStr(Product.PerpanjanganSKT) & " kali " & Parameter.Product.Behaviour_Value(Product.PerpanjanganSKTBehaviour)
        lblRALExtension_Branch.Text = CStr(Product.RALExtension) & " days " & Parameter.Product.Behaviour_Value(Product.RALExtensionBehaviour)
        lblInventoryExpected_Branch.Text = CStr(Product.InventoryExpected) & " days " & Parameter.Product.Behaviour_Value(Product.InventoryExpectedBehaviour)

    End Sub
    Public Sub InitUCViewMode()
        InitialControls()
         
        lblIsSPAutomatic.Text = String.Format("{0} {1} Length SP Process {2} days", IIf(Product.IsSPAutomatic_HO = True, "1", "0"), Parameter.Product.Behaviour_Value(Product.IsSPAutomaticBehaviour_HO), CStr(Product.LengthSPProcess_HO))
        lblIsSP1Automatic.Text = String.Format("{0} {1} Length SP 1 Process {2} days", IIf(Product.IsSP1Automatic_HO = True, "1", "0"), Parameter.Product.Behaviour_Value(Product.IsSP1AutomaticBehaviour_HO), CStr(Product.LengthSP1Process_HO))
        lblIsSP2Automatic.Text = String.Format("{0} {1} Length SP 2 Process {2} days", IIf(Product.IsSP2Automatic_HO = True, "1", "0"), Parameter.Product.Behaviour_Value(Product.IsSP2AutomaticBehaviour_HO), CStr(Product.LengthSP2Process_HO)) 
        lblLMDProcessed.Text = String.Format("{0} days {1}", CStr(Product.LengthMainDocProcess_HO), Parameter.Product.Behaviour_Value(Product.LengthMainDocProcessBehaviour_HO))
        lblLMDTaken.Text = String.Format("{0} days {1}", CStr(Product.LengthMainDocTaken_HO), Parameter.Product.Behaviour_Value(Product.LengthMainDocTakenBehaviour_HO))
        lblGPLC.Text = String.Format("{0} days {1}", CStr(Product.GracePeriodLateCharges_HO), Parameter.Product.Behaviour_Value(Product.GracePeriodLateChargesBehaviour_HO)) 
        lblDaystoremindInstallment.Text = String.Format("{0} days {1}", CStr(Product.DeskCollPhoneRemind_HO), Parameter.Product.Behaviour_Value(Product.DeskCollPhoneRemindBehaviour_HO))
        lblDaysOverduetocall.Text = String.Format("{0} days {1}", CStr(Product.DeskCollOD_HO), Parameter.Product.Behaviour_Value(Product.DeskCollODBehaviour_HO)) 
        lblPreviousOverduetoremind.Text = String.Format("{0} days {1}", CStr(Product.PrevODToRemind_HO), Parameter.Product.Behaviour_Value(Product.PrevODToRemindBehaviour_HO))
        lblPDCRequesttocall.Text = String.Format("{0} days {1}", CStr(Product.PDCDayToRemind_HO), Parameter.Product.Behaviour_Value(Product.PDCDayToRemindBehaviour_HO)) 
        lblDaysToRemind.Text = String.Format("{0} days {1}", CStr(Product.DeskCollSMSRemind_HO), Parameter.Product.Behaviour_Value(Product.DeskCollSMSRemindBehaviour_HO))

        ''meisan

        lblDaysToRemind_2.Text = String.Format("{0} days {1}", CStr(Product.DeskCollSMSRemind2_HO), Parameter.Product.Behaviour_Value(Product.DeskCollSMSRemindBehaviour2_HO))
        lblDaysToRemind_3.Text = String.Format("{0} days {1}", CStr(Product.DeskCollSMSRemind3_HO), Parameter.Product.Behaviour_Value(Product.DeskCollSMSRemindBehaviour3_HO))

        lblPerpanjanganSKT.Text = String.Format("{0} kali {1}", CStr(Product.PerpanjanganSKT_HO), Parameter.Product.Behaviour_Value(Product.PerpanjanganSKTBehaviour_HO))

        lblDaysToGenerateDCR.Text = String.Format("{0} days {1}", CStr(Product.DCR_HO), Parameter.Product.Behaviour_Value(Product.DCRBehaviour_HO)) 
        lblDaysBeforeDuetoRN.Text = String.Format("{0} days {1}", CStr(Product.DaysBeforeDueToRN_HO), Parameter.Product.Behaviour_Value(Product.DaysBeforeDueToRNBehaviour_HO)) 
        lblDaysToGenerateRAL.Text = String.Format("{0} days {1}", CStr(Product.ODToRAL_HO), Parameter.Product.Behaviour_Value(Product.ODToRALBehaviour_HO)) 
        lblRALPeriod.Text = String.Format("{0} days {1}", CStr(Product.RALPeriod_HO), Parameter.Product.Behaviour_Value(Product.RALPeriodBehaviour_HO)) 
        lblMaxDaysRNPaid.Text = String.Format("{0} days {1}", CStr(Product.MaxDaysRNPaid_HO), Parameter.Product.Behaviour_Value(Product.MaxDaysRNPaidBehaviour_HO)) 
        lblMaxPTPYDays.Text = String.Format("{0} days {1}", CStr(Product.MaxPTPYDays_HO), Parameter.Product.Behaviour_Value(Product.MaxPTPYDaysBehaviour_HO)) 
        lblPTPYBank.Text = String.Format("{0} days {1}", CStr(Product.PTPYBank_HO), Parameter.Product.Behaviour_Value(Product.PTPYBankBehaviour_HO))
        lblPTPYCompany.Text = String.Format("{0} days {1}", CStr(Product.PTPYCompany_HO), Parameter.Product.Behaviour_Value(Product.PTPYCompanyBehaviour_HO))
        lblPTPYSupplier.Text = String.Format("{0} days {1}", CStr(Product.PTPYSupplier_HO), Parameter.Product.Behaviour_Value(Product.PTPYSupplierBehaviour_HO))
        ''freq perpanjangan skt

        lblRALExtension.Text = String.Format("{0} days {1}", CStr(Product.RALExtension_HO), Parameter.Product.Behaviour_Value(Product.RALExtensionBehaviour_HO))
        lblInventoryExpected.Text = String.Format("{0} days {1}", CStr(Product.InventoryExpected_HO), Parameter.Product.Behaviour_Value(Product.InventoryExpectedBehaviour_HO))


        setBranchText()
    End Sub

    Public Sub InitUCEditMode()

        bindBehaviour(E_cboLengthMainDocProcessBehaviour)
        bindBehaviour(E_cboLengthMainDocTakenBehaviour)
        bindBehaviour(E_cboGracePeriodLateChargesBehaviour)
        bindBehaviour(E_cboDeskCollPhoneRemindBehaviour)
        bindBehaviour(E_cboDeskCollODBehaviour)
        bindBehaviour(E_cboPrevODToRemindBehaviour)
        bindBehaviour(E_cboPDCDaysToRemindBehaviour)
        bindBehaviour(E_cboDeskCollSMSRemindBehaviour)
        bindBehaviour(E_cboDeskCollSMSRemindBehaviour_2)
        bindBehaviour(E_cboDeskCollSMSRemindBehaviour_3)
        bindBehaviour(E_cboDCRBehaviour)
        bindBehaviour(E_cboDaysBeforeDueToRNBehaviour)
        bindBehaviour(E_cboODToRALBehaviour)
        bindBehaviour(E_cboRALPeriodBehaviour)
        bindBehaviour(E_cboMaxDaysRNPaidBehaviour)
        bindBehaviour(E_cboMaxPTPYDaysBehaviour)
        bindBehaviour(E_cboPTPYBankBehaviour)
        bindBehaviour(E_cboPTPYCompanyBehaviour)
        bindBehaviour(E_cboPTPYSupplierBehaviour)
        bindBehaviour(E_cboRALExtensionBehaviour)
        bindBehaviour(E_cboInventoryExpectedBehaviour)
        bindBehaviour(E_cboPerpanjanganSKTBehaviour)
        InitialControls()
        InitialControls()
    End Sub
    Public Sub CollectResult(oProduct As Parameter.Product)
        With oProduct
            .IsSPAutomatic = CBool(E_cboIsSPAutomatic.SelectedItem.Value.Trim)
            .LengthSPProcess = CInt(E_txtLengthSPProcess.Text.Trim) 
            .IsSP1Automatic = CBool(E_cboIsSP1Automatic.SelectedItem.Value.Trim)
            .LengthSP1Process = CInt(E_txtLengthSP1Process.Text.Trim) 
            .IsSP2Automatic = CBool(E_cboIsSP2Automatic.SelectedItem.Value.Trim)
            .LengthSP2Process = CInt(E_txtLengthSP2Process.Text.Trim) 
            .LengthMainDocProcess = CInt(E_txtLengthMainDocProcess.Text.Trim)
            .LengthMainDocProcessBehaviour = E_cboLengthMainDocProcessBehaviour.SelectedItem.Value.Trim
            .LengthMainDocTaken = CInt(E_txtLengthMainDocTaken.Text.Trim)
            .LengthMainDocTakenBehaviour = E_cboLengthMainDocTakenBehaviour.SelectedItem.Value.Trim
            .GracePeriodLateCharges = CInt(E_txtGracePeriodLateCharges.Text.Trim)
            .GracePeriodLateChargesBehaviour = E_cboGracePeriodLateChargesBehaviour.SelectedItem.Value.Trim
             
            .DeskCollPhoneRemind = CInt(E_txtDeskCollPhoneRemind.Text.Trim)
            .DeskCollPhoneRemindBehaviour = E_cboDeskCollPhoneRemindBehaviour.SelectedItem.Value.Trim
            .DeskCollOD = CInt(E_txtDeskCollOD.Text.Trim)
            .DeskCollODBehaviour = E_cboDeskCollODBehaviour.SelectedItem.Value.Trim
            .PrevODToRemind = CInt(E_txtPrevODToRemind.Text.Trim)
            .PrevODToRemindBehaviour = E_cboPrevODToRemindBehaviour.SelectedItem.Value.Trim
            .PDCDayToRemind = CInt(E_txtPDCDaysToRemind.Text.Trim)
            .PDCDayToRemindBehaviour = E_cboPDCDaysToRemindBehaviour.SelectedItem.Value.Trim
            .DeskCollSMSRemind = CInt(E_txtDeskCollSMSRemind.Text.Trim)
            .DeskCollSMSRemindBehaviour = E_cboDeskCollSMSRemindBehaviour.SelectedItem.Value.Trim

            'meisan
            .DeskCollSMSRemind2 = CInt(E_txtDeskCollSMSRemind_2.Text.Trim)
            .DeskCollSMSRemindBehaviour2 = E_cboDeskCollSMSRemindBehaviour_2.SelectedItem.Value.Trim

            .DeskCollSMSRemind3 = CInt(E_txtDeskCollSMSRemind_3.Text.Trim)
            .DeskCollSMSRemindBehaviour3 = E_cboDeskCollSMSRemindBehaviour_3.SelectedItem.Value.Trim

            .DCR = CInt(E_txtDCR.Text.Trim)
            .DCRBehaviour = E_cboDCRBehaviour.SelectedItem.Value.Trim
            .DaysBeforeDueToRN = CInt(E_txtDaysBeforeDueToRN.Text.Trim)
            .DaysBeforeDueToRNBehaviour = E_cboDaysBeforeDueToRNBehaviour.SelectedItem.Value.Trim
            .ODToRAL = CInt(E_txtODToRAL.Text.Trim)
            .ODToRALBehaviour = E_cboODToRALBehaviour.SelectedItem.Value.Trim
            .RALPeriod = CInt(E_txtRALPeriod.Text.Trim)
            .RALPeriodBehaviour = E_cboRALPeriodBehaviour.SelectedItem.Value.Trim
            .MaxDaysRNPaid = CInt(E_txtMaxDaysRNPaid.Text.Trim)
            .MaxDaysRNPaidBehaviour = E_cboMaxDaysRNPaidBehaviour.SelectedItem.Value.Trim
            .MaxPTPYDays = CInt(E_txtMaxPTPYDays.Text.Trim)
            .MaxPTPYDaysBehaviour = E_cboMaxPTPYDaysBehaviour.SelectedItem.Value.Trim
            .PTPYBank = CInt(E_txtPTPYBank.Text.Trim)
            .PTPYBankBehaviour = E_cboPTPYBankBehaviour.SelectedItem.Value.Trim
            .PTPYCompany = CInt(E_txtPTPYCompany.Text.Trim)
            .PTPYCompanyBehaviour = E_cboPTPYCompanyBehaviour.SelectedItem.Value.Trim
            .PTPYSupplier = CInt(E_txtPTPYSupplier.Text.Trim)
            .PTPYSupplierBehaviour = E_cboPTPYSupplierBehaviour.SelectedItem.Value.Trim

            ''Freq Perpanjangan SKT
            .PerpanjanganSKT = CInt(E_txtPerpanjanganSKT.Text.Trim)
            .PerpanjanganSKTBehaviour = E_cboPerpanjanganSKTBehaviour.SelectedItem.Value.Trim

            .RALExtension = CInt(E_txtRALExtension.Text.Trim)
            .RALExtensionBehaviour = E_cboRALExtensionBehaviour.SelectedItem.Value.Trim
            .InventoryExpected = CInt(E_txtInventoryExpected.Text.Trim)
            .InventoryExpectedBehaviour = E_cboInventoryExpectedBehaviour.SelectedItem.Value.Trim


            .IncomeInsRatioPercentage = 0
            .IncomeInsRatioPercentageBehaviour = "D"

            .IsSPAutomatic = CBool(E_cboIsSPAutomatic.SelectedItem.Value.Trim)
            .LengthSPProcess = CInt(E_txtLengthSPProcess.Text.Trim)

            .IsSP1Automatic = CBool(E_cboIsSP1Automatic.SelectedItem.Value.Trim)
            .LengthSP1Process = CInt(E_txtLengthSP1Process.Text.Trim)

            .IsSP2Automatic = CBool(E_cboIsSP2Automatic.SelectedItem.Value.Trim)
            .LengthSP2Process = CInt(E_txtLengthSP2Process.Text.Trim)

            .LengthMainDocProcess = CInt(E_txtLengthMainDocProcess.Text.Trim)
            .LengthMainDocProcessBehaviour = E_cboLengthMainDocProcessBehaviour.SelectedItem.Value.Trim
            .LengthMainDocTaken = CInt(E_txtLengthMainDocTaken.Text.Trim)
            .LengthMainDocTakenBehaviour = E_cboLengthMainDocTakenBehaviour.SelectedItem.Value.Trim
            .GracePeriodLateCharges = CInt(E_txtGracePeriodLateCharges.Text.Trim)
            .GracePeriodLateChargesBehaviour = E_cboGracePeriodLateChargesBehaviour.SelectedItem.Value.Trim
        End With
    End Sub
    Sub bindBehaviour(ddl As DropDownList)
        ddl.DataValueField = "Value"
        ddl.DataTextField = "Text"
        ddl.DataSource = Parameter.Product.BehaviourItems
        ddl.DataBind()
    End Sub


End Class