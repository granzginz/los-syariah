﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ProductBiayaHOTab.ascx.vb" Inherits="Maxiloan.Webform.SalesMarketing.ProductBiayaHOTab" %>
   <div>

<div class="form_box_title">
<div class="form_single"> <h4> BIAYA </h4> </div> </div>
    <div class="form_box_header">
        <div class="form_single"> <h5> Biaya Pembatalan </h5> </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label class="label_req"> From HO</label>
            <asp:Label ID="lblCancellationFee" runat="server" />
            <asp:TextBox onblur="com_NX('Tabs_pnlTabBiaya_BiayaTab_Hide_CancellationFeeBeh','Tabs_pnlTabBiaya_BiayaTab_E_txtCancellationFee','Tabs_pnlTabBiaya_BiayaTab_Hide_CancellationFee','Jumlah Toleransi terhadap Angsuran');" ID="E_txtCancellationFee" runat="server" MaxLength="10">0</asp:TextBox>
            <asp:DropDownList ID="E_cboCancellationFeeBehaviour" runat="server"/> 
            <input id="Hide_CancellationFee" type="hidden" name="Hide_CancellationFee" runat="server" />
            <input id="Hide_CancellationFeeBeh" type="hidden" name="Hide_CancellationFeeBeh" runat="server" />
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator6" runat="server" ControlToValidate="E_txtCancellationFee" CssClass="validator_general" ErrorMessage="Biaya Pembatalan Salah" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="rgv_CancellationFee" runat="server" ControlToValidate="E_txtCancellationFee" CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Biaya Pembatalan Salah" MaximumValue="9999999999"></asp:RangeValidator>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single"> <label> Branch</label> <asp:Label ID="lblCancellationFee_Branch" runat="server"></asp:Label> </div>
    </div>

    <div class="form_box_header">
        <div class="form_single"> <h5> Biaya Administrasi </h5> </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label class="label_req"> From HO</label>
            <asp:Label ID="lblAdminFee" runat="server" />
            <asp:TextBox onblur="com_NX('Tabs_pnlTabBiaya_BiayaTab_Hide_AdminFeeBeh','Tabs_pnlTabBiaya_BiayaTab_E_txtAdminFee','Tabs_pnlTabBiaya_BiayaTab_Hide_AdminFee','Biaya Administrasi');" ID="E_txtAdminFee" runat="server" MaxLength="10">0</asp:TextBox>
            <asp:DropDownList ID="E_cboAdminFeeBehaviour" runat="server" /> 
            <input id="Hide_AdminFee" type="hidden" name="Hide_AdminFee" runat="server" />
            <input id="Hide_AdminFeeBeh" type="hidden" name="Hide_AdminFeeBeh" runat="server" />
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator17" runat="server" ControlToValidate="E_txtAdminFee" CssClass="validator_general" ErrorMessage="Biaya Administrasi Salah" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="rgv_AdminFee" runat="server" ControlToValidate="E_txtAdminFee" CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Biaya Administrasi Salah" MaximumValue="9999999999"></asp:RangeValidator>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single"> <label> Branch</label> <asp:Label ID="lblAdminFee_Branch" runat="server"></asp:Label> </div>
    </div>

    <div class="form_box_header">
        <div class="form_single">
            <h5> Biaya Fidusia </h5>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label class="label_req"> From HO</label>
            <asp:Label ID="lblFiduciaFee" runat="server" />
            <asp:TextBox onblur="com_NX('Tabs_pnlTabBiaya_BiayaTab_Hide_FiduciaFeeBeh','Tabs_pnlTabBiaya_BiayaTab_E_txtFiduciaFee','Tabs_pnlTabBiaya_BiayaTab_Hide_FiduciaFee','Biaya Fidusia');" ID="E_txtFiduciaFee" runat="server" MaxLength="10">0</asp:TextBox>
            <asp:DropDownList ID="E_cboFiduciaFeeBehaviour" runat="server" /> 
            <input id="Hide_FiduciaFee" type="hidden" name="Hide_FiduciaFee" runat="server" />
            <input id="Hide_FiduciaFeeBeh" type="hidden" name="Hide_FiduciaFeeBeh" runat="server" />
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator80" runat="server" ControlToValidate="E_txtFiduciaFee" CssClass="validator_general" ErrorMessage="Biaya Fidusia Salah" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="rgv_FiduciaFee" runat="server" ControlToValidate="E_txtFiduciaFee" CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Biaya Fidusia Salah" MaximumValue="9999999999"></asp:RangeValidator>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single"> <label> Branch</label> <asp:Label ID="lblFiduciaFee_Branch" runat="server"></asp:Label> </div>
    </div>

    <div class="form_box_header">
        <div class="form_single"> <h5> Biaya Provisi </h5> </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label class="label_req"> From HO</label>
            <asp:Label ID="lblProvisionFee" runat="server" />
            <asp:TextBox onblur="com_NX('Tabs_pnlTabBiaya_BiayaTab_Hide_ProvisionFeeBeh','Tabs_pnlTabBiaya_BiayaTab_E_txtProvisionFee','Tabs_pnlTabBiaya_BiayaTab_Hide_ProvisionFee','Biaya Provisi');" ID="E_txtProvisionFee" runat="server" MaxLength="10">0</asp:TextBox>
            <asp:DropDownList ID="E_cboProvisionFeeBehaviour" runat="server"/> 
            <input id="Hide_ProvisionFee" type="hidden" name="Hide_ProvisionFee" runat="server" />
            <input id="Hide_ProvisionFeeBeh" type="hidden" name="Hide_ProvisionFeeBeh" runat="server" />
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator90" runat="server" ControlToValidate="E_txtProvisionFee" CssClass="validator_general" ErrorMessage="Biaya Provisi Salah" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="rgv_ProvisionFee" runat="server" ControlToValidate="E_txtProvisionFee" CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Biaya Provisi Salah" MaximumValue="9999999999"></asp:RangeValidator>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single"> <label> Branch</label> <asp:Label ID="lblProvisionFee_Branch" runat="server"></asp:Label> </div>
    </div>

    <div class="form_box_header">
        <div class="form_single">
            <h5> Biaya Notaris </h5>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label class="label_req"> From HO</label>
            <asp:Label ID="lblNotaryFee" runat="server" />
            <asp:TextBox onblur="com_NX('Tabs_pnlTabBiaya_BiayaTab_Hide_NotaryFeeBeh','Tabs_pnlTabBiaya_BiayaTab_E_txtNotaryFee','Tabs_pnlTabBiaya_BiayaTab_Hide_NotaryFee','Biaya Notaris');" ID="E_txtNotaryFee" runat="server" MaxLength="10">0</asp:TextBox>
            <asp:DropDownList ID="E_cboNotaryFeeBehaviour" runat="server"/> 
            <input id="Hide_NotaryFee" type="hidden" name="Hide_NotaryFee" runat="server" />
            <input id="Hide_NotaryFeeBeh" type="hidden" name="Hide_NotaryFeeBeh" runat="server" />
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator10" runat="server" ControlToValidate="E_txtNotaryFee" CssClass="validator_general" ErrorMessage="Biaya Notaris Salah" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="rgv_NotaryFee" runat="server" ControlToValidate="E_txtNotaryFee" CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Biaya Notaris Salah" MaximumValue="9999999999"></asp:RangeValidator>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single"> <label> Branch</label> <asp:Label ID="lblNotaryFee_Branch" runat="server"></asp:Label> </div>
    </div>

    <div class="form_box_header">
        <div class="form_single">
            <h5> Biaya Survey  </h5>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label class="label_req"> From HO</label>
            <asp:Label ID="lblSurveyFee" runat="server" />
            <asp:TextBox onblur="com_NX('Tabs_pnlTabBiaya_BiayaTab_Hide_SurveyFeeBeh','Tabs_pnlTabBiaya_BiayaTab_E_txtSurveyFee','Tabs_pnlTabBiaya_BiayaTab_Hide_SurveyFee','Biaya Survey');" ID="E_txtSurveyFee" runat="server" MaxLength="10">0</asp:TextBox>
            <asp:DropDownList ID="E_cboSurveyFeeBehaviour" runat="server"/> 
            <input id="Hide_SurveyFee" type="hidden" name="Hide_SurveyFee" runat="server" />
            <input id="Hide_SurveyFeeBeh" type="hidden" name="Hide_SurveyFeeBeh" runat="server" />
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator11" runat="server" ControlToValidate="E_txtSurveyFee" CssClass="validator_general" ErrorMessage="Biaya Survey salah" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="rgv_SurveyFee" runat="server" ControlToValidate="E_txtSurveyFee" CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Biaya Survey salah" MaximumValue="9999999999"></asp:RangeValidator>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single"> <label> Branch</label> <asp:Label ID="lblSurveyFee_Branch" runat="server"></asp:Label> </div>
    </div>

    <div class="form_box_header">
        <div class="form_single"> <h5> Biaya Kunjungan </h5></div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label class="label_req"> From HO</label>
            <asp:Label ID="lblVisitFee" runat="server" />
            <asp:TextBox onblur="com_NX('Tabs_pnlTabBiaya_BiayaTab_Hide_VisitFeeBeh','Tabs_pnlTabBiaya_BiayaTab_E_txtVisitFee','Tabs_pnlTabBiaya_BiayaTab_Hide_VisitFee','Biaya Kunjungan');" ID="E_txtVisitFee" runat="server" MaxLength="10">0</asp:TextBox>
            <asp:DropDownList ID="E_cboVisitFeeBehaviour" runat="server"/> 
            <input id="Hide_VisitFee" type="hidden" name="Hide_VisitFee" runat="server" />
            <input id="Hide_VisitFeeBeh" type="hidden" name="Hide_VisitFeeBeh" runat="server" />
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator12" runat="server" ControlToValidate="E_txtVisitFee" CssClass="validator_general" ErrorMessage="Biaya Kunjungan Salah" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="rgv_VisitFee" runat="server" ControlToValidate="E_txtVisitFee" CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Biaya Kunjungan Salah" MaximumValue="9999999999"></asp:RangeValidator>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single"> <label> Branch</label> <asp:Label ID="lblVisitFee_Branch" runat="server"></asp:Label> </div>
    </div>

    <div class="form_box_header">
        <div class="form_single">
            <h5> Biaya Reschedulling </h5>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label class="label_req"> From HO</label>
            <asp:Label ID="lblReschedulingFee" runat="server" />
            <asp:TextBox onblur="com_NX('Tabs_pnlTabBiaya_BiayaTab_Hide_ReschedulingFeeBeh','Tabs_pnlTabBiaya_BiayaTab_E_txtReschedulingFee','Tabs_pnlTabBiaya_BiayaTab_Hide_ReschedulingFee','Biaya Reschedulling');" ID="E_txtReschedulingFee" runat="server" MaxLength="10">0</asp:TextBox>
            <asp:DropDownList ID="E_cboReschedulingFeeBehaviour" runat="server"/> 
            <input id="Hide_ReschedulingFee" type="hidden" name="Hide_ReschedulingFee" runat="server" />
            <input id="Hide_ReschedulingFeeBeh" type="hidden" name="Hide_ReschedulingFeeBeh" runat="server" />
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator13" runat="server" ControlToValidate="E_txtReschedulingFee" CssClass="validator_general" ErrorMessage="Biaya Rescheduling Salah" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="rgv_ReschedulingFee" runat="server" ControlToValidate="E_txtReschedulingFee" CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Biaya Rescheduling Salah" MaximumValue="9999999999"></asp:RangeValidator>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single"> <label> Branch</label> <asp:Label ID="lblReschedulingFee_Branch" runat="server"></asp:Label> </div>
    </div>

    <div class="form_box_header">
        <div class="form_single"> <h5> Biaya Over Kontrak </h5> </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label class="label_req"> From HO</label>
             <asp:Label ID="lblAgreementTransferFee" runat="server" />
            <asp:TextBox  onblur="com_NX('Tabs_pnlTabBiaya_BiayaTab_Hide_AgreementTransferFeeBeh','Tabs_pnlTabBiaya_BiayaTab_E_txtAgreementTransferFee','Tabs_pnlTabBiaya_BiayaTab_Hide_AgreementTransferFee','Biaya Over Kontrak ');" ID="E_txtAgreementTransferFee" runat="server" MaxLength="10">0</asp:TextBox>
            <asp:DropDownList ID="E_cboAgreementTransferFeeBehaviour" runat="server" /> 
            <input id="Hide_AgreementTransferFee" type="hidden" name="Hide_AgreementTransferFee" runat="server" />
            <input id="Hide_AgreementTransferFeeBeh" type="hidden" name="Hide_AgreementTransferFeeBeh" runat="server" />
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator14" runat="server" ControlToValidate="E_txtAgreementTransferFee" CssClass="validator_general" ErrorMessage="Biaya Over Kontrak salah" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="rgv_AgreementTransferFee" runat="server" ControlToValidate="E_txtAgreementTransferFee" CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Biaya Over Kontrak salah" MaximumValue="9999999999"></asp:RangeValidator>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single"> <label> Branch</label> <asp:Label ID="lblAgreementTransferFee_Branch" runat="server"></asp:Label> </div>
    </div>

    <div class="form_box_header">
        <div class="form_single"> <h5> Biaya Ganti Tgl jatuh Tempo </h5> </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label class="label_req"> From HO</label>
            <asp:Label ID="lblChangeDueDateFee" runat="server" />
            <asp:TextBox onblur="com_NX('Tabs_pnlTabBiaya_BiayaTab_Hide_ChangeDueDateFeeBeh','Tabs_pnlTabBiaya_BiayaTab_E_txtChangeDueDateFee','Tabs_pnlTabBiaya_BiayaTab_Hide_ChangeDueDateFee',' Biaya Ganti Tgl jatuh Tempo');"  ID="E_txtChangeDueDateFee" runat="server" MaxLength="10">0</asp:TextBox>
            <asp:DropDownList ID="E_cboChangeDueDateFee" runat="server"/> 
            <input id="Hide_ChangeDueDateFee" type="hidden" name="Hide_ChangeDueDateFee" runat="server" />
            <input id="Hide_ChangeDueDateFeeBeh" type="hidden" name="Hide_ChangeDueDateFeeBeh" runat="server" />
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator15" runat="server" ControlToValidate="E_txtChangeDueDateFee" CssClass="validator_general" ErrorMessage="Biaya Ganti Tgl jatuh Tempo Salah" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="rgv_ChangeDueDateFee" runat="server" ControlToValidate="E_txtChangeDueDateFee" CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Biaya Ganti Tgl jatuh Tempo Salah" MaximumValue="9999999999"></asp:RangeValidator>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single"> <label> Branch</label> <asp:Label ID="lblChangeDueDateFee_Branch" runat="server"></asp:Label>
        </div>
    </div>

    <div class="form_box_header">
        <div class="form_single"> <h5> Biaya Penggantian Asset </h5> </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label class="label_req"> From HO</label>
            <asp:Label ID="lblAssetReplacementFee" runat="server" />
            <asp:TextBox onblur="com_NX('Tabs_pnlTabBiaya_BiayaTab_Hide_AssetReplacementFeeBeh','Tabs_pnlTabBiaya_BiayaTab_E_txtAssetReplacementFee','Tabs_pnlTabBiaya_BiayaTab_Hide_AssetReplacementFee','Biaya Penggantian Asset');" ID="E_txtAssetReplacementFee" runat="server" MaxLength="10">0</asp:TextBox>
            <asp:DropDownList ID="E_cboAssetReplacementFeeBehaviour" runat="server"/> 
            <input id="Hide_AssetReplacementFee" type="hidden" name="Hide_AssetReplacementFee" runat="server" />
            <input id="Hide_AssetReplacementFeeBeh" type="hidden" name="Hide_AssetReplacementFeeBeh" runat="server" />
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator16" runat="server" ControlToValidate="E_txtAssetReplacementFee" CssClass="validator_general" ErrorMessage="Biaya Penggantian Asset salah" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="rgv_AssetReplacementFee" runat="server" ControlToValidate="E_txtAssetReplacementFee" CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Biaya Penggantian Asset salah" MaximumValue="9999999999"></asp:RangeValidator>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single"> <label> Branch</label> <asp:Label ID="lblAssetReplacementFee_Branch" runat="server"></asp:Label> </div>
    </div>

    <div class="form_box_header">
        <div class="form_single"> <h5> Biaya Penarikan </h5> </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label class="label_req"> From HO</label>
            <asp:Label ID="lblRepossesFee" runat="server" />
            <asp:TextBox onblur="com_NX('Tabs_pnlTabBiaya_BiayaTab_Hide_RepossesFeeBeh','Tabs_pnlTabBiaya_BiayaTab_E_txtRepossesFee','Tabs_pnlTabBiaya_BiayaTab_Hide_RepossesFee','Biaya Penarikan');" ID="E_txtRepossesFee" runat="server" MaxLength="10">0</asp:TextBox>
            <asp:DropDownList ID="E_cboRepossesFeeBehaviour" runat="server"/> 
            <input id="Hide_RepossesFee" type="hidden" name="Hide_RepossesFee" runat="server" />
            <input id="Hide_RepossesFeeBeh" type="hidden" name="Hide_RepossesFeeBeh" runat="server" />
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator70" runat="server" ControlToValidate="E_txtRepossesFee" CssClass="validator_general" ErrorMessage="Biaya Penarikan Salah" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="rgv_RepossesFee" runat="server" ControlToValidate="E_txtRepossesFee" CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Biaya Penarikan Salah" MaximumValue="9999999999"></asp:RangeValidator>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single"> <label> Branch</label> <asp:Label ID="lblRepossesFee_Branch" runat="server"></asp:Label>
        </div>
    </div>

    <div class="form_box_header">
        <div class="form_single"> <h5> Biaya Legalisasi Dokumen </h5> </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label class="label_req"> From HO</label>
            <asp:Label ID="lblLegalizedDocumentFee" runat="server" />
            <asp:TextBox onblur="com_NX('Tabs_pnlTabBiaya_BiayaTab_Hide_LegalizedDocumentFeeBeh','Tabs_pnlTabBiaya_BiayaTab_E_LegalizedDocumentFee','Tabs_pnlTabBiaya_BiayaTab_Hide_LegalizedDocumentFee','Biaya Legalisasi Dokumen');" ID="E_LegalizedDocumentFee" runat="server" MaxLength="10">0</asp:TextBox>
            <asp:DropDownList ID="E_cboLegalizedDocumentFeeBehaviour" runat="server"/> 
            <input id="Hide_LegalizedDocumentFee" type="hidden" name="Hide_LegalizedDocumentFee" runat="server" />
            <input id="Hide_LegalizedDocumentFeeBeh" type="hidden" name="Hide_LegalizedDocumentFeeBeh" runat="server" />
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator18" runat="server" ControlToValidate="E_LegalizedDocumentFee" CssClass="validator_general" ErrorMessage="Biaya Legalisasi Dokumen Salah" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="rgv_LegalizedDocumentFee" runat="server" ControlToValidate="E_LegalizedDocumentFee" CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Biaya Legalisasi Dokumen Salah" MaximumValue="9999999999"></asp:RangeValidator>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single"> <label> Branch</label> <asp:Label ID="lblLegalizedDocumentFee_Branch" runat="server"></asp:Label> </div>
    </div>

    <div class="form_box_header">
        <div class="form_single"> <h5> Biaya Penolakan PDC </h5> </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label class="label_req"> From HO</label>
            <asp:Label ID="lblPDCBounceFee" runat="server" />
            <asp:TextBox onblur="com_NX('Tabs_pnlTabBiaya_BiayaTab_Hide_PDCBounceFeeBeh','Tabs_pnlTabBiaya_BiayaTab_E_PDCBounceFee','Tabs_pnlTabBiaya_BiayaTab_Hide_PDCBounceFee','Biaya Penolakan PDC');" ID="E_PDCBounceFee" runat="server" MaxLength="10">0</asp:TextBox>
            <asp:DropDownList ID="E_cboPDCBounceFeeBehaviour" runat="server"/> 
            <input id="Hide_PDCBounceFee" type="hidden" name="Hide_PDCBounceFee" runat="server" />
            <input id="Hide_PDCBounceFeeBeh" type="hidden" name="Hide_PDCBounceFeeBeh" runat="server" />
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator19" runat="server" ControlToValidate="E_PDCBounceFee" CssClass="validator_general" ErrorMessage="Biaya Penolakan PDC salah" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="rgv_PDCBounceFee" runat="server" ControlToValidate="E_PDCBounceFee" CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Biaya Penolakan PDC salah" MaximumValue="9999999999"></asp:RangeValidator>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label> Branch</label>
            <asp:Label ID="lblPDCBounceFee_Branch" runat="server"></asp:Label>
        </div>
    </div>

    <div class="form_box_header">
        <div class="form_single">
            <h5> Biaya Admin Asuransi </h5>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label class="label_req"> From HO</label>
            <asp:Label ID="lblInsuranceAdminFee" runat="server" />
            <asp:TextBox onblur="com_NX('Tabs_pnlTabBiaya_BiayaTab_Hide_InsuranceAdminFeeBeh','Tabs_pnlTabBiaya_BiayaTab_E_txtInsuranceAdminFee','Tabs_pnlTabBiaya_BiayaTab_Hide_InsuranceAdminFee','Biaya Admin Asuransi');" ID="E_txtInsuranceAdminFee" runat="server" MaxLength="10">0</asp:TextBox>
            <asp:DropDownList ID="E_cboInsuranceAdminFeeBehaviour" runat="server"/> 
            <input id="Hide_InsuranceAdminFee" type="hidden" name="Hide_InsuranceAdminFee" runat="server" />
            <input id="Hide_InsuranceAdminFeeBeh" type="hidden" name="Hide_InsuranceAdminFeeBeh" runat="server" />
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator20" runat="server" ControlToValidate="E_txtInsuranceAdminFee" CssClass="validator_general" ErrorMessage="Biaya Admin Asuransi salah" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="rgv_InsuranceAdminFee" runat="server" ControlToValidate="E_txtInsuranceAdminFee" CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Biaya Admin Asuransi salah" MaximumValue="9999999999"></asp:RangeValidator>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single"> <label> Branch</label> <asp:Label ID="lblInsuranceAdminFee_Branch" runat="server"></asp:Label> </div>
    </div>

    <div class="form_box_header">
        <div class="form_single"> <h5> Biaya Materai Asuransi </h5> </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label class="label_req">
                From HO</label>
                <asp:Label ID="lblInsuranceStampDutyFee" runat="server" />
            <asp:TextBox onblur="com_NX('Tabs_pnlTabBiaya_BiayaTab_Hide_InsuranceStampDutyFeeeBeh','Tabs_pnlTabBiaya_BiayaTab_E_txtInsuranceStampDutyFee','Tabs_pnlTabBiaya_BiayaTab_Hide_InsuranceStampDutyFee','Biaya Materai Asuransi');" ID="E_txtInsuranceStampDutyFee" runat="server" MaxLength="10">0</asp:TextBox>
            <asp:DropDownList ID="E_cboInsuranceStampDutyFeeBehaviour" runat="server"/> 
            <input id="Hide_InsuranceStampDutyFee" type="hidden" name="Hide_InsuranceStampDutyFee" runat="server" />
            <input id="Hide_InsuranceStampDutyFeeBeh" type="hidden" name="Hide_InsuranceStampDutyFeeBeh" runat="server" />
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator21" runat="server" ControlToValidate="E_txtInsuranceStampDutyFee" CssClass="validator_general" ErrorMessage="Biaya Materai Asuransi salah" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="rgv_InsuranceStampDutyFee" runat="server" ControlToValidate="E_txtInsuranceStampDutyFee" CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Biaya Materai Asuransi salah" MaximumValue="9999999999"></asp:RangeValidator>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single"> <label> Branch</label> <asp:Label ID="lblInsuranceStampDutyFee_Branch" runat="server"></asp:Label> </div>
    </div>

    <div class="form_box_header">
        <div class="form_single"> <h5> Biaya Polis </h5> </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label class="label_req"> From HO</label>
            <asp:Label ID="lblBiayaPolis" runat="server" />
            <asp:TextBox onblur="com_NX('Tabs_pnlTabBiaya_BiayaTab_Hide_BiayaPolisBeh','Tabs_pnlTabBiaya_BiayaTab_E_txtBiayaPolis','Tabs_pnlTabBiaya_BiayaTab_Hide_BiayaPolis',' Biaya Polis');" ID="E_txtBiayaPolis" runat="server" MaxLength="10">0</asp:TextBox>
            <asp:DropDownList ID="E_cboBiayaPolisBehaviour" runat="server"/> 
            <input id="Hide_BiayaPolis" type="hidden" name="Hide_BiayaPolis" runat="server" />
            <input id="Hide_BiayaPolisBeh" type="hidden" name="Hide_BiayaPolisBeh" runat="server" />
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator7" runat="server" ControlToValidate="E_txtBiayaPolis" CssClass="validator_general" ErrorMessage="Biaya Biaya Polis salah" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="rgv_BiayaPolis" runat="server" ControlToValidate="E_txtBiayaPolis" CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Biaya Biaya Polis salah" MaximumValue="9999999999"></asp:RangeValidator>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single"> <label> Branch</label> <asp:Label ID="lblBiayaPolis_Branch" runat="server"></asp:Label> </div>
    </div>
     
</div>