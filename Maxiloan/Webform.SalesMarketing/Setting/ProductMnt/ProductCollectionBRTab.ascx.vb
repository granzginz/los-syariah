﻿Public Class ProductCollectionBRTab
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub




    Public Property Product() As Parameter.Product
        Get
            Return CType(ViewState("ProductCollectionTabProduct"), Parameter.Product)
        End Get
        Set(ByVal Value As Parameter.Product)
            ViewState("ProductCollectionTabProduct") = Value
        End Set
    End Property
    Public Property UCMode As String
        Get
            Return CType(ViewState("COLLECTIONTABUCMODE"), String)
        End Get
        Set(value As String)
            ViewState("COLLECTIONTABUCMODE") = value
        End Set
    End Property
    Sub InitialControls()

        lblIsSPAutomatic.Visible = (UCMode <> "VIEW")
        E_cboIsSPAutomatic.Visible = (UCMode <> "VIEW")
        E_txtLengthSPProcess.Visible = (UCMode <> "VIEW")

        lblIsSP1Automatic.Visible = (UCMode <> "VIEW")
        E_cboIsSP1Automatic.Visible = (UCMode <> "VIEW")
        E_txtLengthSP1Process.Visible = (UCMode <> "VIEW")



        lblIsSP2Automatic.Visible = (UCMode <> "VIEW")
        E_cboIsSP2Automatic.Visible = (UCMode <> "VIEW")
        E_txtLengthSP2Process.Visible = (UCMode <> "VIEW")

        lblLMDProcessed.Visible = (UCMode <> "VIEW")
        E_txtLengthMainDocProcess.Visible = (UCMode <> "VIEW")
        E_cboLengthMainDocProcessBehaviour.Visible = (UCMode <> "VIEW")



        lblLMDTaken.Visible = (UCMode <> "VIEW")
        E_txtLengthMainDocTaken.Visible = (UCMode <> "VIEW")
        E_cboLengthMainDocTakenBehaviour.Visible = (UCMode <> "VIEW")

        lblGPLC.Visible = (UCMode <> "VIEW")
        E_txtGracePeriodLateCharges.Visible = (UCMode <> "VIEW")
        E_cboGracePeriodLateChargesBehaviour.Visible = (UCMode <> "VIEW")

        lblDaystoremindInstallment.Visible = (UCMode <> "VIEW")
        E_txtDeskCollPhoneRemind.Visible = (UCMode <> "VIEW")
        E_cboDeskCollPhoneRemindBehaviour.Visible = (UCMode <> "VIEW")

        lblDaysOverduetocall.Visible = (UCMode <> "VIEW")
        E_txtDeskCollOD.Visible = (UCMode <> "VIEW")
        E_cboDeskCollODBehaviour.Visible = (UCMode <> "VIEW")

        lblPreviousOverduetoremind.Visible = (UCMode <> "VIEW")
        E_txtPrevODToRemind.Visible = (UCMode <> "VIEW")
        E_cboPrevODToRemindBehaviour.Visible = (UCMode <> "VIEW")

        lblPDCRequesttocall.Visible = (UCMode <> "VIEW")
        E_txtPDCDaysToRemind.Visible = (UCMode <> "VIEW")
        E_cboPDCDaysToRemindBehaviour.Visible = (UCMode <> "VIEW")

        lblDaysToRemind.Visible = (UCMode <> "VIEW")
        E_txtDeskCollSMSRemind.Visible = (UCMode <> "VIEW")
        E_cboDeskCollSMSRemindBehaviour.Visible = (UCMode <> "VIEW")

        lblDaysToGenerateDCR.Visible = (UCMode <> "VIEW")
        E_txtDCR.Visible = (UCMode <> "VIEW")
        E_cboDCRBehaviour.Visible = (UCMode <> "VIEW")


        lblDaysBeforeDuetoRN.Visible = (UCMode <> "VIEW")
        E_txtDaysBeforeDueToRN.Visible = (UCMode <> "VIEW")
        E_cboDaysBeforeDueToRNBehaviour.Visible = (UCMode <> "VIEW")

        lblDaysToGenerateRAL.Visible = (UCMode <> "VIEW")
        E_txtODToRAL.Visible = (UCMode <> "VIEW")
        E_cboODToRALBehaviour.Visible = (UCMode <> "VIEW")

        lblRALPeriod.Visible = (UCMode <> "VIEW")
        E_txtRALPeriod.Visible = (UCMode <> "VIEW")
        E_cboRALPeriodBehaviour.Visible = (UCMode <> "VIEW")

        lblMaxDaysRNPaid.Visible = (UCMode <> "VIEW")
        E_txtMaxDaysRNPaid.Visible = (UCMode <> "VIEW")
        E_cboMaxDaysRNPaidBehaviour.Visible = (UCMode <> "VIEW")

        lblMaxPTPYDays.Visible = (UCMode <> "VIEW")
        E_txtMaxPTPYDays.Visible = (UCMode <> "VIEW")
        E_cboMaxPTPYDaysBehaviour.Visible = (UCMode <> "VIEW")

        lblPTPYBank.Visible = (UCMode <> "VIEW")
        E_txtPTPYBank.Visible = (UCMode <> "VIEW")
        E_cboPTPYBankBehaviour.Visible = (UCMode <> "VIEW")

        lblPTPYCompany.Visible = (UCMode <> "VIEW")
        E_txtPTPYCompany.Visible = (UCMode <> "VIEW")
        E_cboPTPYCompanyBehaviour.Visible = (UCMode <> "VIEW")


        lblPTPYSupplier.Visible = (UCMode <> "VIEW")
        E_txtPTPYSupplier.Visible = (UCMode <> "VIEW")
        E_cboPTPYSupplierBehaviour.Visible = (UCMode <> "VIEW")

        lblRALExtension.Visible = (UCMode <> "VIEW")
        E_txtRALExtension.Visible = (UCMode <> "VIEW")
        E_cboRALExtensionBehaviour.Visible = (UCMode <> "VIEW")

        lblInventoryExpected.Visible = (UCMode <> "VIEW")
        E_txtInventoryExpected.Visible = (UCMode <> "VIEW")
        E_cboInventoryExpectedBehaviour.Visible = (UCMode <> "VIEW")

        lblPerpanjanganSKT.Visible = (UCMode = "VIEW")
        E_txtPerpanjanganSKT.Visible = (UCMode <> "VIEW")
        E_cboPerpanjanganSKTBehaviour.Visible = (UCMode <> "VIEW")

        lblDaysToRemind_2.Visible = (UCMode = "VIEW")
        E_txtDeskCollSMSRemind_2.Visible = (UCMode <> "VIEW")
        E_cboDeskCollSMSRemindBehaviour_2.Visible = (UCMode <> "VIEW")

        lblDaysToRemind_3.Visible = (UCMode = "VIEW")
        E_txtDeskCollSMSRemind_3.Visible = (UCMode <> "VIEW")
        E_cboDeskCollSMSRemindBehaviour_3.Visible = (UCMode <> "VIEW")


        Label1.Visible = (UCMode <> "VIEW")
        Label2.Visible = (UCMode <> "VIEW")
        Label3.Visible = (UCMode <> "VIEW")
        Label4.Visible = (UCMode <> "VIEW")

        Label5.Visible = (UCMode <> "VIEW")
        Label6.Visible = (UCMode <> "VIEW")
        Label7.Visible = (UCMode <> "VIEW")
        Label8.Visible = (UCMode <> "VIEW")
        Label9.Visible = (UCMode <> "VIEW") 
        Label10.Visible = (UCMode <> "VIEW")
        Label11.Visible = (UCMode <> "VIEW")
        Label12.Visible = (UCMode <> "VIEW")
        Label13.Visible = (UCMode <> "VIEW")
        Label14.Visible = (UCMode <> "VIEW")
        Label15.Visible = (UCMode <> "VIEW")
        Label16.Visible = (UCMode <> "VIEW")
        Label17.Visible = (UCMode <> "VIEW")
        Label18.Visible = (UCMode <> "VIEW")
        Label19.Visible = (UCMode <> "VIEW")
        Label20.Visible = (UCMode <> "VIEW") 
        Label21.Visible = (UCMode <> "VIEW")
        Label22.Visible = (UCMode <> "VIEW")
        Label23.Visible = (UCMode <> "VIEW")
        Label24.Visible = (UCMode <> "VIEW")
        Label25.Visible = (UCMode <> "VIEW")

        If (UCMode = "ADD") Then
            initFieldsForAddMode()
        End If
        If (UCMode = "EDIT") Then
            BindFiedsForEdit()
        End If
    End Sub

    Sub initFieldsForAddMode()

    End Sub

    Public Sub BindFiedsForEdit()

        E_cboIsSPAutomatic.SelectedIndex = E_cboIsSPAutomatic.Items.IndexOf(E_cboIsSPAutomatic.Items.FindByValue(IIf(Product.IsSPAutomatic = True, "1", "0")))
        E_txtLengthSPProcess.Text = CStr(Product.LengthSPProcess)

        Hide_LengthSPProcess.Value = CStr(Product.LengthSPProcess)
        If Product.IsSPAutomatic_HO = True Then
            Behaviour_Lock(Product.IsSPAutomaticBehaviour_HO, E_txtLengthSPProcess, E_cboIsSPAutomatic)
        Else
            E_cboIsSPAutomatic.Enabled = False
            E_txtLengthSPProcess.Enabled = False
        End If



        ' SP1Automatic
        E_cboIsSP1Automatic.SelectedIndex = E_cboIsSP1Automatic.Items.IndexOf(E_cboIsSP1Automatic.Items.FindByValue(IIf(Product.IsSP1Automatic = True, "1", "0")))
        E_txtLengthSP1Process.Text = CStr(Product.LengthSP1Process)
        Hide_LengthSP1Process.Value = CStr(Product.LengthSP1Process)

        If Product.IsSP1Automatic_HO = True Then
            Behaviour_Lock(Product.IsSP1AutomaticBehaviour_HO, E_txtLengthSP1Process, E_cboIsSP1Automatic)
        Else
            E_cboIsSP1Automatic.Enabled = False
            E_txtLengthSP1Process.Enabled = False
        End If



        ' SP2Automatic
        E_cboIsSP2Automatic.SelectedIndex = E_cboIsSP2Automatic.Items.IndexOf(E_cboIsSP2Automatic.Items.FindByValue(IIf(Product.IsSP2Automatic = True, "1", "0")))
        E_txtLengthSP2Process.Text = CStr(Product.LengthSP2Process)
        Hide_LengthSP2Process.Value = CStr(Product.LengthSP2Process)

        If Product.IsSP2Automatic = True Then
            Behaviour_Lock(Product.IsSP2AutomaticBehaviour, E_txtLengthSP2Process, E_cboIsSP2Automatic)
        Else
            E_cboIsSP2Automatic.Enabled = False
            E_txtLengthSP2Process.Enabled = False
        End If


        'Length Main Document Processed
        E_txtLengthMainDocProcess.Text = CStr(Product.LengthMainDocProcess)
        Hide_LengthMainDocProcess.Value = CStr(Product.LengthMainDocProcess)
        Hide_LengthMainDocProcessBeh.Value = Product.LengthMainDocProcessBehaviour
        E_cboLengthMainDocProcessBehaviour.SelectedIndex = E_cboLengthMainDocProcessBehaviour.Items.IndexOf(E_cboLengthMainDocProcessBehaviour.Items.FindByValue(Product.LengthMainDocProcessBehaviour))
        Behaviour_Lock(Product.LengthMainDocProcessBehaviour, E_txtLengthMainDocProcess, E_cboLengthMainDocProcessBehaviour)



        'Length Main Document Taken
        E_txtLengthMainDocTaken.Text = CStr(Product.LengthMainDocTaken)
        Hide_LengthMainDocTaken.Value = CStr(Product.LengthMainDocTaken)
        Hide_LengthMainDocProcessBeh.Value = Product.LengthMainDocTakenBehaviour
        E_cboLengthMainDocTakenBehaviour.SelectedIndex = E_cboLengthMainDocTakenBehaviour.Items.IndexOf(E_cboLengthMainDocTakenBehaviour.Items.FindByValue(Product.LengthMainDocTakenBehaviour))
        Behaviour_Lock(Product.LengthMainDocTakenBehaviour, E_txtLengthMainDocTaken, E_cboLengthMainDocTakenBehaviour)




        'Grace Period Late Charges
        E_txtGracePeriodLateCharges.Text = CStr(Product.GracePeriodLateCharges)
        Hide_GracePeriodLateCharges.Value = CStr(Product.GracePeriodLateCharges)
        Hide_GracePeriodLateChargesBeh.Value = Product.GracePeriodLateChargesBehaviour
        E_cboGracePeriodLateChargesBehaviour.SelectedIndex = E_cboGracePeriodLateChargesBehaviour.Items.IndexOf(E_cboGracePeriodLateChargesBehaviour.Items.FindByValue(Product.GracePeriodLateChargesBehaviour))
        Behaviour_Lock(Product.GracePeriodLateChargesBehaviour, E_txtGracePeriodLateCharges, E_cboGracePeriodLateChargesBehaviour)


        E_txtDeskCollPhoneRemind.Text = CStr(Product.DeskCollPhoneRemind)
        Hide_DeskCollPhoneRemind.Value = CStr(Product.DeskCollPhoneRemind)
        Hide_DeskCollPhoneRemindBeh.Value = Product.DeskCollPhoneRemindBehaviour
        E_cboDeskCollPhoneRemindBehaviour.SelectedIndex = E_cboDeskCollPhoneRemindBehaviour.Items.IndexOf(E_cboDeskCollPhoneRemindBehaviour.Items.FindByValue(Product.DeskCollPhoneRemindBehaviour))
        Behaviour_Lock(Product.DeskCollPhoneRemindBehaviour, E_txtDeskCollPhoneRemind, E_cboDeskCollPhoneRemindBehaviour)




        'Days overdue to call
        E_txtDeskCollOD.Text = CStr(Product.DeskCollOD)
        Hide_DeskCollOD.Value = CStr(Product.DeskCollOD)
        Hide_DeskCollODBeh.Value = Product.DeskCollODBehaviour
        E_cboDeskCollODBehaviour.SelectedIndex = E_cboDeskCollODBehaviour.Items.IndexOf(E_cboDeskCollODBehaviour.Items.FindByValue(Product.DeskCollODBehaviour))
        Behaviour_Lock(Product.DeskCollODBehaviour, E_txtDeskCollOD, E_cboDeskCollODBehaviour)




        'Previous overdue to remind
        E_txtPrevODToRemind.Text = CStr(Product.PrevODToRemind)
        Hide_PrevODToRemind.Value = CStr(Product.PrevODToRemind)
        Hide_PrevODToRemindBeh.Value = Product.PrevODToRemindBehaviour
        E_cboPrevODToRemindBehaviour.SelectedIndex = E_cboPrevODToRemindBehaviour.Items.IndexOf(E_cboPrevODToRemindBehaviour.Items.FindByValue(Product.PrevODToRemindBehaviour))
        Behaviour_Lock(Product.PrevODToRemindBehaviour, E_txtPrevODToRemind, E_cboPrevODToRemindBehaviour)


        'PDC request to call
        E_txtPDCDaysToRemind.Text = CStr(Product.PDCDayToRemind)
        Hide_PDCDaysToRemind.Value = CStr(Product.PDCDayToRemind)
        Hide_PDCDaysToRemindBeh.Value = Product.PDCDayToRemindBehaviour
        E_cboPDCDaysToRemindBehaviour.SelectedIndex = E_cboPDCDaysToRemindBehaviour.Items.IndexOf(E_cboPDCDaysToRemindBehaviour.Items.FindByValue(Product.PDCDayToRemindBehaviour))
        Behaviour_Lock(Product.PDCDayToRemindBehaviour, E_txtPDCDaysToRemind, E_cboPDCDaysToRemindBehaviour)


        'Days to remind by SMS
        E_txtDeskCollSMSRemind.Text = CStr(Product.DeskCollSMSRemind)
        Hide_DeskCollSMSRemind.Value = CStr(Product.DeskCollSMSRemind)
        Hide_DeskCollSMSRemindBeh.Value = Product.DeskCollSMSRemindBehaviour
        E_cboDeskCollSMSRemindBehaviour.SelectedIndex = E_cboDeskCollSMSRemindBehaviour.Items.IndexOf(E_cboDeskCollSMSRemindBehaviour.Items.FindByValue(Product.DeskCollSMSRemindBehaviour))
        Behaviour_Lock(Product.DeskCollSMSRemindBehaviour, E_txtDeskCollSMSRemind, E_cboDeskCollSMSRemindBehaviour)

        'Days to generate DCR
        E_txtDCR.Text = CStr(Product.DCR)
        Hide_DCR.Value = CStr(Product.DCR)
        Hide_DCRBeh.Value = Product.DCRBehaviour
        E_cboDCRBehaviour.SelectedIndex = E_cboDCRBehaviour.Items.IndexOf(E_cboDCRBehaviour.Items.FindByValue(Product.DCRBehaviour))

        Behaviour_Lock(Product.DCRBehaviour, E_txtDCR, E_cboDCRBehaviour)



        'Days before due to generate Receipt Notes
        E_txtDaysBeforeDueToRN.Text = CStr(Product.DaysBeforeDueToRN)
        Hide_DaysBeforeDueToRN.Value = CStr(Product.DaysBeforeDueToRN)
        Hide_DaysBeforeDueToRNBeh.Value = Product.DaysBeforeDueToRNBehaviour
        E_cboDaysBeforeDueToRNBehaviour.SelectedIndex = E_cboDaysBeforeDueToRNBehaviour.Items.IndexOf(E_cboDaysBeforeDueToRNBehaviour.Items.FindByValue(Product.DaysBeforeDueToRNBehaviour))
        Behaviour_Lock(Product.DaysBeforeDueToRNBehaviour, E_txtDaysBeforeDueToRN, E_cboDaysBeforeDueToRNBehaviour)



        'Days to generate RAL
        E_txtODToRAL.Text = CStr(Product.ODToRAL)
        Hide_ODToRAL.Value = CStr(Product.ODToRAL)
        Hide_ODToRALBeh.Value = Product.ODToRALBehaviour
        E_cboODToRALBehaviour.SelectedIndex = E_cboODToRALBehaviour.Items.IndexOf(E_cboODToRALBehaviour.Items.FindByValue(Product.ODToRALBehaviour))
        Behaviour_Lock(Product.ODToRALBehaviour, E_txtODToRAL, E_cboODToRALBehaviour)



        'RAL Period
        E_txtRALPeriod.Text = CStr(Product.RALPeriod)
        Hide_RALPeriod.Value = CStr(Product.RALPeriod)
        Hide_RALPeriodBeh.Value = Product.RALPeriodBehaviour
        E_cboRALPeriodBehaviour.SelectedIndex = E_cboRALPeriodBehaviour.Items.IndexOf(E_cboRALPeriodBehaviour.Items.FindByValue(Product.RALPeriodBehaviour))
        Behaviour_Lock(Product.RALPeriodBehaviour, E_txtRALPeriod, E_cboRALPeriodBehaviour)



        'Maximum days Receipt notes paid
        E_txtMaxDaysRNPaid.Text = CStr(Product.MaxDaysRNPaid)
        Hide_MaxDaysRNPaid.Value = CStr(Product.MaxDaysRNPaid)
        Hide_MaxDaysRNPaidBeh.Value = Product.MaxDaysRNPaidBehaviour
        E_cboMaxDaysRNPaidBehaviour.SelectedIndex = E_cboMaxDaysRNPaidBehaviour.Items.IndexOf(E_cboMaxDaysRNPaidBehaviour.Items.FindByValue(Product.MaxDaysRNPaidBehaviour))
        Behaviour_Lock(Product.MaxDaysRNPaidBehaviour, E_txtMaxDaysRNPaid, E_cboMaxDaysRNPaidBehaviour)

        'Maximum Promise To Pay days
        E_txtMaxPTPYDays.Text = CStr(Product.MaxPTPYDays)
        Hide_MaxPTPYDays.Value = CStr(Product.MaxPTPYDays)
        Hide_MaxPTPYDaysBeh.Value = Product.MaxPTPYDaysBehaviour
        E_cboMaxPTPYDaysBehaviour.SelectedIndex = E_cboMaxPTPYDaysBehaviour.Items.IndexOf(E_cboMaxPTPYDaysBehaviour.Items.FindByValue(Product.MaxPTPYDaysBehaviour))
        Behaviour_Lock(Product.MaxPTPYDaysBehaviour, E_txtMaxPTPYDays, E_cboMaxPTPYDaysBehaviour)



        'Promise To Pay To Bank
        E_txtPTPYBank.Text = CStr(Product.PTPYBank)
        Hide_PTPYBank.Value = CStr(Product.PTPYBank)
        Hide_PTPYBankBeh.Value = Product.PTPYBankBehaviour
        E_cboPTPYBankBehaviour.SelectedIndex = E_cboPTPYBankBehaviour.Items.IndexOf(E_cboPTPYBankBehaviour.Items.FindByValue(Product.PTPYBankBehaviour))
        Behaviour_Lock(Product.PTPYBankBehaviour, E_txtPTPYBank, E_cboPTPYBankBehaviour)


        'Promise To Pay To Company
        E_txtPTPYCompany.Text = CStr(Product.PTPYCompany)
        Hide_PTPYCompany.Value = CStr(Product.PTPYCompany)
        Hide_PTPYCompanyBeh.Value = Product.PTPYCompanyBehaviour
        E_cboPTPYCompanyBehaviour.SelectedIndex = E_cboPTPYCompanyBehaviour.Items.IndexOf(E_cboPTPYCompanyBehaviour.Items.FindByValue(Product.PTPYCompanyBehaviour))
        Behaviour_Lock(Product.PTPYCompanyBehaviour, E_txtPTPYCompany, E_cboPTPYCompanyBehaviour)




        'Promise To Pay To Supplier
        E_txtPTPYSupplier.Text = CStr(Product.PTPYSupplier)
        Hide_PTPYSupplier.Value = CStr(Product.PTPYSupplier)
        Hide_PTPYSupplierBeh.Value = Product.PTPYSupplierBehaviour
        E_cboPTPYSupplierBehaviour.SelectedIndex = E_cboPTPYSupplierBehaviour.Items.IndexOf(E_cboPTPYSupplierBehaviour.Items.FindByValue(Product.PTPYSupplierBehaviour))
        Behaviour_Lock(Product.PTPYSupplierBehaviour, E_txtPTPYSupplier, E_cboPTPYSupplierBehaviour)



        'RAL Extension
        E_txtRALExtension.Text = CStr(Product.RALExtension)
        Hide_RALExtension.Value = CStr(Product.RALExtension)
        Hide_RALExtensionBeh.Value = Product.RALExtensionBehaviour
        E_cboRALExtensionBehaviour.SelectedIndex = E_cboRALExtensionBehaviour.Items.IndexOf(E_cboRALExtensionBehaviour.Items.FindByValue(Product.RALExtensionBehaviour))
        Behaviour_Lock(Product.RALExtensionBehaviour, E_txtRALExtension, E_cboRALExtensionBehaviour)




        'Inventory Expected
        E_txtInventoryExpected.Text = CStr(Product.InventoryExpected)
        Hide_InventoryExpected.Value = CStr(Product.InventoryExpected)
        Hide_InventoryExpectedBeh.Value = Product.InventoryExpectedBehaviour
        E_cboInventoryExpectedBehaviour.SelectedIndex = E_cboInventoryExpectedBehaviour.Items.IndexOf(E_cboInventoryExpectedBehaviour.Items.FindByValue(Product.InventoryExpectedBehaviour))
        Behaviour_Lock(Product.InventoryExpectedBehaviour, E_txtInventoryExpected, E_cboInventoryExpectedBehaviour)

        'Freq Perpanjangan SKT
        E_txtPerpanjanganSKT.Text = CStr(Product.PerpanjanganSKT_HO)
        Hide_PerpanjanganSKT.Value = CStr(Product.PerpanjanganSKT_HO)
        Hide_PerpanjanganSKTBeh.Value = Product.PerpanjanganSKTBehaviour_HO
        E_cboPerpanjanganSKTBehaviour.SelectedIndex = E_cboPerpanjanganSKTBehaviour.Items.IndexOf(E_cboPerpanjanganSKTBehaviour.Items.FindByValue(Product.PerpanjanganSKTBehaviour_HO))
        Behaviour_Lock(Product.PerpanjanganSKTBehaviour_HO, E_txtPerpanjanganSKT, E_cboPerpanjanganSKTBehaviour)

        setBranchText()
    End Sub

    Sub setBranchText()
        lblLengthSPProcess_Branch.Text = IIf(Product.IsSPAutomatic_HO = True, "Yes", "No") & " Length SP Process " & CStr(Product.LengthSPProcess_HO) & " days"
        lblLengthSP1Process_Branch.Text = IIf(Product.IsSP1Automatic_HO = True, "Yes", "No") & " Length SP 1 Process " & CStr(Product.LengthSP1Process_HO) & " days"
        lblLengthSP2Process_Branch.Text = IIf(Product.IsSP2Automatic_HO = True, "Yes", "No") & " Length SP 2 Process " & CStr(Product.LengthSP2Process_HO) & " days"
        lblLengthMainDocProcess_Branch.Text = CStr(Product.LengthMainDocProcess_HO) & " days " & Parameter.Product.Behaviour_Value(Product.LengthMainDocProcessBehaviour_HO)
        lblLengthMainDocTaken_Branch.Text = CStr(Product.LengthMainDocTaken_HO) & " days " & Parameter.Product.Behaviour_Value(Product.LengthMainDocTakenBehaviour_HO)
        lblGracePeriodLateCharges_Branch.Text = CStr(Product.GracePeriodLateCharges_HO) & " days " & Parameter.Product.Behaviour_Value(Product.GracePeriodLateChargesBehaviour_HO)

        lblDeskCollPhoneRemind_Branch.Text = CStr(Product.DeskCollPhoneRemind_HO) & " days " & Parameter.Product.Behaviour_Value(Product.DeskCollPhoneRemindBehaviour_HO)
        lblDeskCollOD_Branch.Text = CStr(Product.DeskCollOD_HO) & " days " & Parameter.Product.Behaviour_Value(Product.DeskCollODBehaviour_HO)
        lblPrevODToRemind_Branch.Text = CStr(Product.PrevODToRemind_HO) & " days " & Parameter.Product.Behaviour_Value(Product.PrevODToRemindBehaviour_HO)
        lblPDCDaysToRemind_Branch.Text = CStr(Product.PDCDayToRemind_HO) & " Dayss " & Parameter.Product.Behaviour_Value(Product.PDCDayToRemindBehaviour_HO)
        lblDeskCollSMSRemind_Branch.Text = CStr(Product.DeskCollSMSRemind_HO) & " days " & Parameter.Product.Behaviour_Value(Product.DeskCollSMSRemindBehaviour_HO)

        lblDCR_Branch.Text = CStr(Product.DCR_HO) & " days " & Parameter.Product.Behaviour_Value(Product.DCRBehaviour_HO)
        lblDaysBeforeDueToRN_Branch.Text = CStr(Product.DaysBeforeDueToRN_HO) & " days " & Parameter.Product.Behaviour_Value(Product.DaysBeforeDueToRNBehaviour_HO)
        lblODToRAL_Branch.Text = CStr(Product.ODToRAL_HO) & " days " & Parameter.Product.Behaviour_Value(Product.ODToRALBehaviour_HO)
        lblRALPeriod_Branch.Text = CStr(Product.RALPeriod_HO) & " days " & Parameter.Product.Behaviour_Value(Product.RALPeriodBehaviour_HO)
        lblMaxDaysRNPaid_Branch.Text = CStr(Product.MaxDaysRNPaid_HO) & " days " & Parameter.Product.Behaviour_Value(Product.MaxDaysRNPaidBehaviour_HO)

        lblMaxPTPYDays_Branch.Text = CStr(Product.MaxPTPYDays_HO) & " days " & Parameter.Product.Behaviour_Value(Product.MaxPTPYDaysBehaviour_HO)
        lblPTPYBank_Branch.Text = CStr(Product.PTPYBank_HO) & " days " & Parameter.Product.Behaviour_Value(Product.PTPYBankBehaviour_HO)
        lblPTPYCompany_Branch.Text = CStr(Product.PTPYCompany_HO) & " days " & Parameter.Product.Behaviour_Value(Product.PTPYCompanyBehaviour_HO)
        lblPTPYSupplier_Branch.Text = CStr(Product.PTPYSupplier_HO) & " days " & Parameter.Product.Behaviour_Value(Product.PTPYSupplierBehaviour_HO)
        lblRALExtension_Branch.Text = CStr(Product.RALExtension_HO) & " days " & Parameter.Product.Behaviour_Value(Product.RALExtensionBehaviour_HO)
        lblInventoryExpected_Branch.Text = CStr(Product.InventoryExpected_HO) & " days " & Parameter.Product.Behaviour_Value(Product.InventoryExpectedBehaviour_HO)

    End Sub
    Public Sub InitUCViewMode()
        InitialControls()

        lblIsSPAutomatic.Text = String.Format("{0} {1} Length SP Process {2} days", IIf(Product.IsSPAutomatic = True, "1", "0"), Parameter.Product.Behaviour_Value(Product.IsSPAutomaticBehaviour), CStr(Product.LengthSPProcess))
        lblIsSP1Automatic.Text = String.Format("{0} {1} Length SP 1 Process {2} days", IIf(Product.IsSP1Automatic = True, "1", "0"), Parameter.Product.Behaviour_Value(Product.IsSP1AutomaticBehaviour), CStr(Product.LengthSP1Process))
        lblIsSP2Automatic.Text = String.Format("{0} {1} Length SP 2 Process {2} days", IIf(Product.IsSP2Automatic = True, "1", "0"), Parameter.Product.Behaviour_Value(Product.IsSP2AutomaticBehaviour), CStr(Product.LengthSP2Process))
        lblLMDProcessed.Text = String.Format("{0} days {1}", CStr(Product.LengthMainDocProcess), Parameter.Product.Behaviour_Value(Product.LengthMainDocProcessBehaviour))
        lblLMDTaken.Text = String.Format("{0} days {1}", CStr(Product.LengthMainDocTaken), Parameter.Product.Behaviour_Value(Product.LengthMainDocTakenBehaviour))
        lblGPLC.Text = String.Format("{0} days {1}", CStr(Product.GracePeriodLateCharges), Parameter.Product.Behaviour_Value(Product.GracePeriodLateChargesBehaviour))
        lblDaystoremindInstallment.Text = String.Format("{0} days {1}", CStr(Product.DeskCollPhoneRemind), Parameter.Product.Behaviour_Value(Product.DeskCollPhoneRemindBehaviour))
        lblDaysOverduetocall.Text = String.Format("{0} days {1}", CStr(Product.DeskCollOD), Parameter.Product.Behaviour_Value(Product.DeskCollODBehaviour))
        lblPreviousOverduetoremind.Text = String.Format("{0} days {1}", CStr(Product.PrevODToRemind), Parameter.Product.Behaviour_Value(Product.PrevODToRemindBehaviour))
        lblPDCRequesttocall.Text = String.Format("{0} days {1}", CStr(Product.PDCDayToRemind), Parameter.Product.Behaviour_Value(Product.PDCDayToRemindBehaviour))
        lblDaysToRemind.Text = String.Format("{0} days {1}", CStr(Product.DeskCollSMSRemind), Parameter.Product.Behaviour_Value(Product.DeskCollSMSRemindBehaviour))
        lblDaysToGenerateDCR.Text = String.Format("{0} days {1}", CStr(Product.DCR), Parameter.Product.Behaviour_Value(Product.DCRBehaviour))
        lblDaysBeforeDuetoRN.Text = String.Format("{0} days {1}", CStr(Product.DaysBeforeDueToRN), Parameter.Product.Behaviour_Value(Product.DaysBeforeDueToRNBehaviour))
        lblDaysToGenerateRAL.Text = String.Format("{0} days {1}", CStr(Product.ODToRAL), Parameter.Product.Behaviour_Value(Product.ODToRALBehaviour))
        lblRALPeriod.Text = String.Format("{0} days {1}", CStr(Product.RALPeriod), Parameter.Product.Behaviour_Value(Product.RALPeriodBehaviour))
        lblMaxDaysRNPaid.Text = String.Format("{0} days {1}", CStr(Product.MaxDaysRNPaid), Parameter.Product.Behaviour_Value(Product.MaxDaysRNPaidBehaviour))
        lblMaxPTPYDays.Text = String.Format("{0} days {1}", CStr(Product.MaxPTPYDays), Parameter.Product.Behaviour_Value(Product.MaxPTPYDaysBehaviour))
        lblPTPYBank.Text = String.Format("{0} days {1}", CStr(Product.PTPYBank), Parameter.Product.Behaviour_Value(Product.PTPYBankBehaviour))
        lblPTPYCompany.Text = String.Format("{0} days {1}", CStr(Product.PTPYCompany), Parameter.Product.Behaviour_Value(Product.PTPYCompanyBehaviour))
        lblPTPYSupplier.Text = String.Format("{0} days {1}", CStr(Product.PTPYSupplier), Parameter.Product.Behaviour_Value(Product.PTPYSupplierBehaviour))
        lblRALExtension.Text = String.Format("{0} days {1}", CStr(Product.RALExtension), Parameter.Product.Behaviour_Value(Product.RALExtensionBehaviour))
        lblInventoryExpected.Text = String.Format("{0} days {1}", CStr(Product.InventoryExpected), Parameter.Product.Behaviour_Value(Product.InventoryExpectedBehaviour))
        lblPerpanjanganSKT.Text = String.Format("{0} kali {1}", CStr(Product.PerpanjanganSKT_HO), Parameter.Product.Behaviour_Value(Product.PerpanjanganSKTBehaviour_HO))
        lblDaysToRemind_2.Text = String.Format("{0} days {1}", CStr(Product.DeskCollSMSRemind2_HO), Parameter.Product.Behaviour_Value(Product.DeskCollSMSRemindBehaviour2_HO))
        lblDaysToRemind_3.Text = String.Format("{0} days {1}", CStr(Product.DeskCollSMSRemind3_HO), Parameter.Product.Behaviour_Value(Product.DeskCollSMSRemindBehaviour3_HO))
        setBranchText()
    End Sub

    Public Sub InitUCEditMode()

        bindBehaviour(E_cboLengthMainDocProcessBehaviour)
        bindBehaviour(E_cboLengthMainDocTakenBehaviour)
        bindBehaviour(E_cboGracePeriodLateChargesBehaviour)
        bindBehaviour(E_cboDeskCollPhoneRemindBehaviour)
        bindBehaviour(E_cboDeskCollODBehaviour)
        bindBehaviour(E_cboPrevODToRemindBehaviour)
        bindBehaviour(E_cboPDCDaysToRemindBehaviour)
        bindBehaviour(E_cboDeskCollSMSRemindBehaviour)
        bindBehaviour(E_cboDCRBehaviour)
        bindBehaviour(E_cboDaysBeforeDueToRNBehaviour)
        bindBehaviour(E_cboODToRALBehaviour)
        bindBehaviour(E_cboRALPeriodBehaviour)
        bindBehaviour(E_cboMaxDaysRNPaidBehaviour)
        bindBehaviour(E_cboMaxPTPYDaysBehaviour)
        bindBehaviour(E_cboPTPYBankBehaviour)
        bindBehaviour(E_cboPTPYCompanyBehaviour)
        bindBehaviour(E_cboPTPYSupplierBehaviour)
        bindBehaviour(E_cboRALExtensionBehaviour)
        bindBehaviour(E_cboInventoryExpectedBehaviour)
        bindBehaviour(E_cboPerpanjanganSKTBehaviour)
        bindBehaviour(E_cboDeskCollSMSRemindBehaviour_2)
        bindBehaviour(E_cboDeskCollSMSRemindBehaviour_3)
        InitialControls()
        InitialControls()
    End Sub
    Public Sub CollectResult(oProduct As Parameter.Product)
        With oProduct

            .IsSPAutomatic = CBool(E_cboIsSPAutomatic.SelectedItem.Value.Trim)
            .LengthSPProcess = CInt(E_txtLengthSPProcess.Text.Trim)
            .IsSP1Automatic = CBool(E_cboIsSP1Automatic.SelectedItem.Value.Trim)
            .LengthSP1Process = CInt(E_txtLengthSP1Process.Text.Trim)
            .IsSP2Automatic = CBool(E_cboIsSP2Automatic.SelectedItem.Value.Trim)
            .LengthSP2Process = CInt(E_txtLengthSP2Process.Text.Trim)
            .LengthMainDocProcess = CInt(E_txtLengthMainDocProcess.Text.Trim)
            .LengthMainDocProcessBehaviour = E_cboLengthMainDocProcessBehaviour.SelectedItem.Value.Trim
            .LengthMainDocTaken = CInt(E_txtLengthMainDocTaken.Text.Trim)
            .LengthMainDocTakenBehaviour = E_cboLengthMainDocTakenBehaviour.SelectedItem.Value.Trim
            .GracePeriodLateCharges = CInt(E_txtGracePeriodLateCharges.Text.Trim)
            .GracePeriodLateChargesBehaviour = E_cboGracePeriodLateChargesBehaviour.SelectedItem.Value.Trim 
            .DeskCollPhoneRemind = CInt(E_txtDeskCollPhoneRemind.Text.Trim)
            .DeskCollPhoneRemindBehaviour = E_cboDeskCollPhoneRemindBehaviour.SelectedItem.Value.Trim
            .DeskCollOD = CInt(E_txtDeskCollOD.Text.Trim)
            .DeskCollODBehaviour = E_cboDeskCollODBehaviour.SelectedItem.Value.Trim
            .PrevODToRemind = CInt(E_txtPrevODToRemind.Text.Trim)
            .PrevODToRemindBehaviour = E_cboPrevODToRemindBehaviour.SelectedItem.Value.Trim
            .PDCDayToRemind = CInt(E_txtPDCDaysToRemind.Text.Trim)
            .PDCDayToRemindBehaviour = E_cboPDCDaysToRemindBehaviour.SelectedItem.Value.Trim
            .DeskCollSMSRemind = CInt(E_txtDeskCollSMSRemind.Text.Trim)
            .DeskCollSMSRemindBehaviour = E_cboDeskCollSMSRemindBehaviour.SelectedItem.Value.Trim 
            .DCR = CInt(E_txtDCR.Text.Trim)
            .DCRBehaviour = E_cboDCRBehaviour.SelectedItem.Value.Trim
            .DaysBeforeDueToRN = CInt(E_txtDaysBeforeDueToRN.Text.Trim)
            .DaysBeforeDueToRNBehaviour = E_cboDaysBeforeDueToRNBehaviour.SelectedItem.Value.Trim
            .ODToRAL = CInt(E_txtODToRAL.Text.Trim)
            .ODToRALBehaviour = E_cboODToRALBehaviour.SelectedItem.Value.Trim
            .RALPeriod = CInt(E_txtRALPeriod.Text.Trim)
            .RALPeriodBehaviour = E_cboRALPeriodBehaviour.SelectedItem.Value.Trim
            .MaxDaysRNPaid = CInt(E_txtMaxDaysRNPaid.Text.Trim)
            .MaxDaysRNPaidBehaviour = E_cboMaxDaysRNPaidBehaviour.SelectedItem.Value.Trim
            .MaxPTPYDays = CInt(E_txtMaxPTPYDays.Text.Trim)
            .MaxPTPYDaysBehaviour = E_cboMaxPTPYDaysBehaviour.SelectedItem.Value.Trim
            .PTPYBank = CInt(E_txtPTPYBank.Text.Trim)
            .PTPYBankBehaviour = E_cboPTPYBankBehaviour.SelectedItem.Value.Trim
            .PTPYCompany = CInt(E_txtPTPYCompany.Text.Trim)
            .PTPYCompanyBehaviour = E_cboPTPYCompanyBehaviour.SelectedItem.Value.Trim
            .PTPYSupplier = CInt(E_txtPTPYSupplier.Text.Trim)
            .PTPYSupplierBehaviour = E_cboPTPYSupplierBehaviour.SelectedItem.Value.Trim
            .RALExtension = CInt(E_txtRALExtension.Text.Trim)
            .RALExtensionBehaviour = E_cboRALExtensionBehaviour.SelectedItem.Value.Trim
            .InventoryExpected = CInt(E_txtInventoryExpected.Text.Trim)
            .InventoryExpectedBehaviour = E_cboInventoryExpectedBehaviour.SelectedItem.Value.Trim 
            .IncomeInsRatioPercentage = 0
            .IncomeInsRatioPercentageBehaviour = "D" 
            .IsSPAutomatic = CBool(E_cboIsSPAutomatic.SelectedItem.Value.Trim)
            .LengthSPProcess = CInt(E_txtLengthSPProcess.Text.Trim) 
            .IsSP1Automatic = CBool(E_cboIsSP1Automatic.SelectedItem.Value.Trim)
            .LengthSP1Process = CInt(E_txtLengthSP1Process.Text.Trim) 
            .IsSP2Automatic = CBool(E_cboIsSP2Automatic.SelectedItem.Value.Trim)
            .LengthSP2Process = CInt(E_txtLengthSP2Process.Text.Trim) 
            .LengthMainDocProcess = CInt(E_txtLengthMainDocProcess.Text.Trim)
            .LengthMainDocProcessBehaviour = E_cboLengthMainDocProcessBehaviour.SelectedItem.Value.Trim
            .LengthMainDocTaken = CInt(E_txtLengthMainDocTaken.Text.Trim)
            .LengthMainDocTakenBehaviour = E_cboLengthMainDocTakenBehaviour.SelectedItem.Value.Trim
            .GracePeriodLateCharges = CInt(E_txtGracePeriodLateCharges.Text.Trim)
            .GracePeriodLateChargesBehaviour = E_cboGracePeriodLateChargesBehaviour.SelectedItem.Value.Trim

            .PerpanjanganSKT = CInt(E_txtPerpanjanganSKT.Text.Trim)
            .PerpanjanganSKTBehaviour = E_cboPerpanjanganSKTBehaviour.SelectedItem.Value.Trim

            .DeskCollSMSRemind2 = CInt(E_txtDeskCollSMSRemind_2.Text.Trim)
            .DeskCollSMSRemindBehaviour2 = E_cboDeskCollSMSRemindBehaviour_2.SelectedItem.Value.Trim

            .DeskCollSMSRemind3 = CInt(E_txtDeskCollSMSRemind_3.Text.Trim)
            .DeskCollSMSRemindBehaviour3 = E_cboDeskCollSMSRemindBehaviour_3.SelectedItem.Value.Trim
        End With
    End Sub
    Sub bindBehaviour(ddl As DropDownList)
        ddl.DataValueField = "Value"
        ddl.DataTextField = "Text"
        ddl.DataSource = Parameter.Product.BehaviourItems
        ddl.DataBind()
    End Sub


End Class