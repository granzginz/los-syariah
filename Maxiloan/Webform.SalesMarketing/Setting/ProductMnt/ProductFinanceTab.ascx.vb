﻿Public Class ProductFinanceTab
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Public Property UCMode As String
        Get
            Return CType(ViewState("FINANCETABUCMODE"), String)
        End Get
        Set(value As String)
            ViewState("FINANCETABUCMODE") = value
        End Set
    End Property
    Public Property Product() As Parameter.Product
        Get
            Return CType(ViewState("ProductFinanceTabProduct"), Parameter.Product)
        End Get
        Set(ByVal Value As Parameter.Product)
            ViewState("ProductFinanceTabProduct") = Value
        End Set
    End Property
    Sub InitialControls()
        lblLimitAPCash.Visible = (UCMode = "VIEW")
        txtLimitAPCash.Visible = (UCMode <> "VIEW")
        cboLimitAPCash.Visible = (UCMode <> "VIEW")

        lblIsRecourse.Visible = (UCMode = "VIEW")
        cboIsRecourse.Visible = (UCMode <> "VIEW")

        lblIsActive.Visible = (UCMode = "VIEW")
        rboIsActive.Visible = (UCMode <> "VIEW")

        lblInsuranceDiscPercentage.Visible = (UCMode = "VIEW")
        txtInsuranceDiscPercentage.Visible = (UCMode <> "VIEW")
        cboInsuranceDiscPercentageBehaviour.Visible = (UCMode <> "VIEW")
        Label1.Visible = (UCMode <> "VIEW")
    End Sub
    Sub initFieldsForAddMode()


        txtInsuranceDiscPercentage.Text = "0"
        cboInsuranceDiscPercentageBehaviour.SelectedIndex = 0
        'txtIncomeInsRatioPercentage.Text = "0"
        'cboIncomeInsRatioPercentageBehaviour.SelectedIndex = cboIncomeInsRatioPercentageBehaviour.Items.IndexOf(cboIncomeInsRatioPercentageBehaviour.Items.FindByValue("D"))

        txtLimitAPCash.Text = FormatNumber("0", 2)
        cboLimitAPCash.SelectedIndex = 0

        cboIsRecourse.SelectedIndex = cboIsRecourse.Items.IndexOf(cboIsRecourse.Items.FindByValue("0"))
        rboIsActive.SelectedIndex = rboIsActive.Items.IndexOf(rboIsActive.Items.FindByValue("1"))


    End Sub
    Public Sub bindFiedsForEdit()
        txtLimitAPCash.Text = FormatNumber(Product.LimitAPCash, 0)
        cboLimitAPCash.SelectedIndex = cboLimitAPCash.Items.IndexOf(cboLimitAPCash.Items.FindByValue(Product.LimitAPCashBehaviour))
        cboIsRecourse.SelectedIndex = cboIsRecourse.Items.IndexOf(cboIsRecourse.Items.FindByValue(IIf(Product.IsRecourse, "1", "0")))
        rboIsActive.SelectedIndex = rboIsActive.Items.IndexOf(rboIsActive.Items.FindByValue(IIf(Product.IsActive, "1", "0")))
        txtInsuranceDiscPercentage.Text = FormatNumber(Product.InsuranceDiscPercentage, 10)
        cboInsuranceDiscPercentageBehaviour.SelectedIndex = cboInsuranceDiscPercentageBehaviour.Items.IndexOf(cboInsuranceDiscPercentageBehaviour.Items.FindByValue(Product.InsuranceDiscPercentageBehaviour))
        'txtIncomeInsRatioPercentage.Text = CStr(FormatNumber(Product.IncomeInsRatioPercentage, 10))
        'cboIncomeInsRatioPercentageBehaviour.SelectedIndex = cboIncomeInsRatioPercentageBehaviour.Items.IndexOf(cboIncomeInsRatioPercentageBehaviour.Items.FindByValue(Product.IncomeInsRatioPercentageBehaviour))

    End Sub
    Public Sub InitUCViewMode()
        InitialControls()
        lblLimitAPCash.Text = String.Format("{0} {1}", FormatNumber(CStr(Product.LimitAPCash), 2), Parameter.Product.Behaviour_Value(Product.LimitAPCashBehaviour))
        lblIsRecourse.Text = IIf(CStr(Product.IsRecourse) = "True", "Yes", "No")
        lblIsActive.Text = IIf(CStr(Product.IsActive) = "True", "Yes", "No")
        lblInsuranceDiscPercentage.Text = String.Format("{0} % {1}", Product.InsuranceDiscPercentage, Parameter.Product.Behaviour_Value(Product.InsuranceDiscPercentageBehaviour))
    End Sub
    Public Sub InitUCEditMode()
        bindBehaviour(cboLimitAPCash)
        bindBehaviour(cboInsuranceDiscPercentageBehaviour)
        bindBehaviour(cboInsuranceDiscPercentageBehaviour)
        InitialControls()

        If (UCMode = "ADD") Then
            initFieldsForAddMode()
        End If
        If (UCMode = "EDIT") Then
            bindFiedsForEdit()
        End If
    End Sub

    Public Sub SetProductJualTab()
        cboIsRecourse.Visible = False
        rboIsActive.Visible = False
        lblIsRecourse.Visible = True
        lblIsActive.Visible = False
        lblIsRecourse.Text = IIf(CStr(Product.IsRecourse) = "True", "Yes", "No")
        lblIsActive.Text = IIf(CStr(Product.IsActive) = "True", "Yes", "No") 
    End Sub
    Public Sub BehaviourLockSetting() 
        BehaviorHelperModule.Behaviour_Lock(Product.LimitAPCashBehaviour, txtLimitAPCash, cboLimitAPCash)  
        BehaviorHelperModule.Behaviour_Lock(Product.InsuranceDiscPercentageBehaviour, txtInsuranceDiscPercentage, cboInsuranceDiscPercentageBehaviour)
    End Sub
    Public Sub CollectResult(oProduct As Parameter.Product)

        With oProduct 
            .InsuranceDiscPercentage = CDec(txtInsuranceDiscPercentage.Text.Trim)
            .InsuranceDiscPercentageBehaviour = cboInsuranceDiscPercentageBehaviour.SelectedItem.Value.Trim

            .IncomeInsRatioPercentage = 0
            .IncomeInsRatioPercentageBehaviour = "D"

            .LimitAPCash = CInt(txtLimitAPCash.Text.Trim)
            .LimitAPCashBehaviour = cboLimitAPCash.SelectedItem.Value.Trim
            .IsRecourse = CBool(cboIsRecourse.SelectedItem.Value.Trim)
            .IsActive = CBool(rboIsActive.SelectedItem.Value.Trim)

        End With
    End Sub
    Sub bindBehaviour(ddl As DropDownList)
        ddl.DataValueField = "Value"
        ddl.DataTextField = "Text"
        ddl.DataSource = Parameter.Product.BehaviourItems
        ddl.DataBind()
    End Sub
End Class