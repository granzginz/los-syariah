﻿Imports Maxiloan.Controller
'Imports Maxiloan.Webform.UserController
Public Class ProductSkemaTab
    ''Inherits Maxiloan.Webform.WebBased
    Inherits System.Web.UI.UserControl
    Private m_controllerProd As New ProductController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        GetCboSkePemb_("TblSkemaPembiayaan", cboSkemaPembiayaan)
    End Sub

    Public Property UCMode As String
        Get
            Return CType(ViewState("SKEMATABUCMODE"), String)
        End Get
        Set(value As String)
            ViewState("SKEMATABUCMODE") = value
        End Set
    End Property

    Public Property Product() As Parameter.Product
        Get
            Return CType(ViewState("ProductSkemaTabProduct"), Parameter.Product)
        End Get
        Set(ByVal Value As Parameter.Product)
            ViewState("ProductSkemaTabProduct") = Value
        End Set
    End Property
    Sub InitialControls()

        lblMinimumTenor.Visible = (UCMode = "VIEW")
        txtMaximumTenor.Visible = (UCMode <> "VIEW")

        lblMaximumTenor.Visible = (UCMode = "VIEW")
        txtMinimumTenor.Visible = (UCMode <> "VIEW")

        lblAngsuranPertama.Visible = (UCMode = "VIEW")
        cboAngsuranPertama.Visible = (UCMode <> "VIEW")

        lblSukuBungaFlat.Visible = (UCMode = "VIEW")
        txtFlatRate.Visible = (UCMode <> "VIEW")
        cboFlatRateBehaviour.Visible = (UCMode <> "VIEW")

        lblEffectiveRate.Visible = (UCMode = "VIEW")
        txtEffectiveRate.Visible = (UCMode <> "VIEW")
        cboEffectiveRateBehaviour.Visible = (UCMode <> "VIEW")

        lblOpsiUangMuka.Visible = (UCMode = "VIEW")
        cboOpsiUangMuka.Visible = (UCMode <> "VIEW")

        lblMinimumDPPerc.Visible = (UCMode = "VIEW")
        txtMinimumDPPerc.Visible = (UCMode <> "VIEW")
        cboMinimumDPPerc.Visible = (UCMode <> "VIEW")

        lblUangMukaAngsuran.Visible = (UCMode = "VIEW")
        txtUangMukaAngsuran.Visible = (UCMode <> "VIEW")

        lblPOExpirationDays.Visible = (UCMode = "VIEW")
        txtPOExpirationDays.Visible = (UCMode <> "VIEW")
        cboPOExpirationDays.Visible = (UCMode <> "VIEW")

        ''MEISAN
        LblCreditProtect.Visible = (UCMode = "VIEW")
        TxtCreditProtect.Visible = (UCMode <> "VIEW")
        cboCreditProtect.Visible = (UCMode <> "VIEW")

        lblAsuransiKredit.Visible = (UCMode = "VIEW")
        txtAsuransiKredit.Visible = (UCMode <> "VIEW")
        cboAsuransiKredit.Visible = (UCMode <> "VIEW")

        Label1.Visible = (UCMode <> "VIEW")
        Label2.Visible = (UCMode <> "VIEW")
        Label3.Visible = (UCMode <> "VIEW")
        Label4.Visible = (UCMode <> "VIEW")
        Label5.Visible = (UCMode <> "VIEW")
        Label6.Visible = (UCMode <> "VIEW")
        Label7.Visible = (UCMode <> "VIEW")

        lblGLTDP.Visible = (UCMode <> "VIEW")
        GLTitipan.Visible = (UCMode <> "VIEW")

        If (UCMode = "ADD") Then
            initFieldsForAddMode()
        End If
        If (UCMode = "EDIT") Then
            BindFiedsForEdit()
        End If

    End Sub
    Sub initFieldsForAddMode()
        txtEffectiveRate.Text = "1"
        cboEffectiveRateBehaviour.SelectedIndex = cboEffectiveRateBehaviour.Items.IndexOf(cboEffectiveRateBehaviour.Items.FindByValue("D"))

        'txtGrossYeildRate.Text = "1"
        'cboGrossYieldRate.SelectedIndex = cboGrossYieldRate.Items.IndexOf(cboGrossYieldRate.Items.FindByValue("N"))

        txtMinimumTenor.Text = "1"
        txtMaximumTenor.Text = "1"

        txtPOExpirationDays.Text = "0"
        cboPOExpirationDays.SelectedIndex = cboPOExpirationDays.Items.IndexOf(cboPOExpirationDays.Items.FindByValue("D"))

        TxtCreditProtect.Text = "0"
        cboCreditProtect.SelectedIndex = cboCreditProtect.Items.IndexOf(cboCreditProtect.Items.FindByValue("D"))

        txtMinimumDPPerc.Text = "0"
        cboMinimumDPPerc.SelectedIndex = cboMinimumDPPerc.Items.IndexOf(cboMinimumDPPerc.Items.FindByValue("D"))

        cboAngsuranPertama.SelectedIndex = 0
        txtFlatRate.Text = 0
        cboFlatRateBehaviour.SelectedIndex = 0
        cboOpsiUangMuka.SelectedIndex = 0
        txtUangMukaAngsuran.Text = 0

    End Sub
    Public Sub BindFiedsForEdit()

        txtEffectiveRate.Text = CStr(FormatNumber(Product.EffectiveRate, 6))
        cboEffectiveRateBehaviour.SelectedIndex = cboEffectiveRateBehaviour.Items.IndexOf(cboEffectiveRateBehaviour.Items.FindByValue(Product.EffectiveRateBehaviour))

        'txtGrossYeildRate.Text = CStr(FormatNumber(Product.GrossYieldRate, 6))
        'cboGrossYieldRate.SelectedIndex = cboGrossYieldRate.Items.IndexOf(cboGrossYieldRate.Items.FindByValue(Product.GrossYieldRateBehaviour))

        txtMinimumTenor.Text = CStr(Product.MinimumTenor)
        txtMaximumTenor.Text = CStr(Product.MaximumTenor)


        txtPOExpirationDays.Text = CStr(Product.DaysPOExpiration)
        cboPOExpirationDays.SelectedIndex = cboPOExpirationDays.Items.IndexOf(cboPOExpirationDays.Items.FindByValue(Product.DaysPOExpirationBehaviour))

        txtMinimumDPPerc.Text = CStr(FormatNumber(Product.DPPercentage, 6))
        cboMinimumDPPerc.SelectedIndex = cboMinimumDPPerc.Items.IndexOf(cboMinimumDPPerc.Items.FindByValue(Product.DPPercentageBehaviour))

        cboAngsuranPertama.SelectedIndex = cboAngsuranPertama.Items.IndexOf(cboAngsuranPertama.Items.FindByValue(Product.AngsuranPertama))

        txtFlatRate.Text = CStr(Product.SukuBungaFlat)
        cboFlatRateBehaviour.SelectedIndex = cboFlatRateBehaviour.Items.IndexOf(cboFlatRateBehaviour.Items.FindByValue(Product.SukuBungaFlatBehaviour))

        cboOpsiUangMuka.SelectedIndex = cboOpsiUangMuka.Items.IndexOf(cboOpsiUangMuka.Items.FindByValue(Product.OpsiUangMuka))

        txtUangMukaAngsuran.Text = CStr(Product.OpsiUangMukaAngsur)

        'tambahan meisan
        TxtCreditProtect.Text = CStr(Product.CreditProtection)
        cboCreditProtect.SelectedIndex = cboCreditProtect.Items.IndexOf(cboCreditProtect.Items.FindByValue(Product.CreditProtectionBehaviour))

        txtAsuransiKredit.Text = CStr(Product.AsuransiKredit)
        cboAsuransiKredit.SelectedIndex = cboAsuransiKredit.Items.IndexOf(cboAsuransiKredit.Items.FindByValue(Product.AsuransiKreditBehaviour))


    End Sub
    Public Sub InitUCViewMode()

        InitialControls()

        lblMinimumTenor.Text = String.Format("{0} months", FormatNumber(CStr(Product.MinimumTenor), 2))
        lblMaximumTenor.Text = String.Format("{0} months", FormatNumber(CStr(Product.MaximumTenor), 2))
        lblAngsuranPertama.Text = IIf(Product.AngsuranPertama = "ARR", "ARREAR", "ADVANCE")
        lblSukuBungaFlat.Text = String.Format("{0} % {1}", FormatNumber(Product.SukuBungaFlat, 6), Parameter.Product.Behaviour_Value(Product.SukuBungaFlatBehaviour))
        lblEffectiveRate.Text = String.Format("{0} % {1}", FormatNumber(Product.EffectiveRate, 6), Parameter.Product.Behaviour_Value(Product.EffectiveRateBehaviour))
        lblOpsiUangMuka.Text = IIf(Product.OpsiUangMuka = "P", "PROSENTASE", "ANGSURAN")
        lblMinimumDPPerc.Text = String.Format("{0} % {1}", FormatNumber(Product.DPPercentage, 6), Parameter.Product.Behaviour_Value(Product.DPPercentageBehaviour))
        lblUangMukaAngsuran.Text = String.Format("{0} x", FormatNumber(Product.OpsiUangMukaAngsur, 6))
        lblPOExpirationDays.Text = String.Format("{0} days {1}", CStr(Product.DaysPOExpiration), Parameter.Product.Behaviour_Value(Product.DaysPOExpirationBehaviour))
        LblCreditProtect.Text = String.Format("{0} % {1}", FormatNumber(Product.CreditProtection, 6), Parameter.Product.Behaviour_Value(Product.CreditProtectionBehaviour))
        lblAsuransiKredit.Text = String.Format("{0} % {1}", FormatNumber(Product.AsuransiKredit, 6), Parameter.Product.Behaviour_Value(Product.AsuransiKreditBehaviour))
    End Sub

    Public Sub InitUCEditMode()


        bindBehaviour(cboFlatRateBehaviour)
        bindBehaviour(cboEffectiveRateBehaviour)
        bindBehaviour(cboMinimumDPPerc)
        bindBehaviour(cboPOExpirationDays)
        ''MEISAN
        bindBehaviour(cboCreditProtect)
        bindBehaviour(cboAsuransiKredit)

        InitialControls()
    End Sub
    Public Sub CollectResult(oProduct As Parameter.Product)
        With oProduct

            .EffectiveRate = CDec(txtEffectiveRate.Text.Trim)
            .EffectiveRateBehaviour = cboEffectiveRateBehaviour.SelectedItem.Value.Trim
            .GrossYieldRate = 0
            .GrossYieldRateBehaviour = "D"

            .MinimumTenor = CInt(txtMinimumTenor.Text.Trim)
            .MaximumTenor = CInt(txtMaximumTenor.Text.Trim)

            .DaysPOExpiration = CInt(txtPOExpirationDays.Text.Trim)
            .DaysPOExpirationBehaviour = cboPOExpirationDays.SelectedItem.Value.Trim
            .DPPercentage = CDec(txtMinimumDPPerc.Text.Trim)
            .DPPercentageBehaviour = cboMinimumDPPerc.SelectedItem.Value.Trim

            .AngsuranPertama = cboAngsuranPertama.SelectedItem.Value.Trim()
            .SukuBungaFlat = CDec(txtFlatRate.Text)
            .SukuBungaFlatBehaviour = cboFlatRateBehaviour.SelectedItem.Value.Trim()

            .OpsiUangMuka = cboOpsiUangMuka.SelectedItem.Value.Trim()
            .SkemaPembiayaan = cboSkemaPembiayaan.SelectedValue.Trim()
            .OpsiUangMukaAngsur = CInt(txtUangMukaAngsuran.Text)

            ''MEISAN
            .CreditProtection = CInt(TxtCreditProtect.Text.Trim)
            .CreditProtectionBehaviour = cboCreditProtect.SelectedItem.Value.Trim

            .AsuransiKredit = CInt(txtAsuransiKredit.Text.Trim)
            .AsuransiKreditBehaviour = cboAsuransiKredit.SelectedItem.Value.Trim

        End With
    End Sub

    Sub bindBehaviour(ddl As DropDownList)
        ddl.DataValueField = "Value"
        ddl.DataTextField = "Text"
        ddl.DataSource = Parameter.Product.BehaviourItems
        ddl.DataBind()
    End Sub


    Public Sub BehaviourLockSetting()
        BehaviorHelperModule.Behaviour_Lock(Product.SukuBungaFlatBehaviour, txtFlatRate, cboFlatRateBehaviour)
        BehaviorHelperModule.Behaviour_Lock(Product.EffectiveRateBehaviour, txtEffectiveRate, cboEffectiveRateBehaviour)
        BehaviorHelperModule.Behaviour_Lock(Product.DPPercentageBehaviour, txtMinimumDPPerc, cboMinimumDPPerc)
        BehaviorHelperModule.Behaviour_Lock(Product.DaysPOExpirationBehaviour, txtPOExpirationDays, cboPOExpirationDays)
        ''MEISAN
        BehaviorHelperModule.Behaviour_Lock(Product.CreditProtectionBehaviour, TxtCreditProtect, cboCreditProtect)
        BehaviorHelperModule.Behaviour_Lock(Product.AsuransiKredit, txtAsuransiKredit, cboAsuransiKredit)
    End Sub

    Protected Sub GetCboSkePemb_(ByVal table As String, cboReference As DropDownList)
        Dim a As String = Session("conString")
        Dim dtEntity As DataTable = Nothing
        Dim oCboSkePemb As New Parameter.Product
        oCboSkePemb.strConnection = a
        oCboSkePemb.Table = table
        oCboSkePemb = m_controllerProd.GetCboSkePemb(oCboSkePemb)
        If Not oCboSkePemb Is Nothing Then
            dtEntity = oCboSkePemb.ListData
        End If

        cboReference.DataSource = dtEntity.DefaultView
        cboReference.DataTextField = "Description"
        cboReference.DataValueField = "ID"
        cboReference.DataBind()
        'cboReference.Items.Insert(0, "Select One")
        'cboReference.Items(0).Value = "Select One"

    End Sub
End Class