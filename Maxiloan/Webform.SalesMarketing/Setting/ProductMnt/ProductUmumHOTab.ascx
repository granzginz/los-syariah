﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ProductUmumHOTab.ascx.vb" Inherits="Maxiloan.Webform.SalesMarketing.ProductUmumHOTab" %>
    <div class="form_box_title">
        <div class="form_single"> <h4><asp:Label ID="lblHeader" runat="server"></asp:Label> </h4> </div> </div>
        <div class="form_box">
        <div class="form_single"><label> Cabang</label>
        <asp:Label ID="E_lblBranchID" runat="server"></asp:Label>
</div>
</div>
<div class="form_box">
    <div class="form_single">
        <label> ID Produk</label>
        <asp:Label ID="E_lblProductID" runat="server"></asp:Label>
    </div>
</div>
<div class="form_box">
    <div class="form_single">
        <label> Nama Produk</label>
        <asp:Label ID="E_lblDescription" runat="server"></asp:Label>
    </div>
</div> 

<div class="form_box_title">  <div class="form_single"> <h4> UMUM </h4> </div> </div>

    <div class="form_box">
    <div class="form_single">
        <label> Jenis Asset</label>
        <asp:Label ID="E_lblAssetType" runat="server" Visible="True"></asp:Label>
    </div>
</div>
<div class="form_box"  style="display:none">
    <div class="form_single">
        <label>Skema Score - Marketing</label>
        <asp:Label ID="E_lblScoreSchemeID" runat="server" Visible="True"></asp:Label>
    </div>
</div> 
<div class="form_box">
    <div class="form_single">
        <label> Kredit Scoring </label>
        <asp:Label ID="E_lblCreditScoreSchemeID" runat="server" Visible="True"></asp:Label>
    </div>
</div>
<div class="form_box">
    <div class="form_single">
        <label> Skema Jurnal</label>
        <asp:Label ID="E_lblJournalSchemeID" runat="server" Visible="True"></asp:Label>
    </div>
</div>
<div class="form_box">
    <div class="form_single">
        <label> Skema Approval</label>
        <asp:Label ID="E_lblApprovalSchemeID" runat="server" Visible="True"></asp:Label>
    </div>
</div>
<div class="form_box">
    <div class="form_single">
        <label> Kondisi Asset</label>
        <asp:Label ID="E_lblAssetUsedNew" runat="server" Visible="True"></asp:Label>
    </div>
</div>
<div class="form_box">
    <div class="form_single">
        <label>Umur Kendaraan</label>
         <asp:Label ID="lblUmurKendaraan" runat="server"/>
        <asp:TextBox ID="txtUmurKendaraanFrom" runat="server" MaxLength="9">0</asp:TextBox>&nbsp;&nbsp;To&nbsp;&nbsp;
        <asp:TextBox ID="txtUmurKendaraanTo" runat="server" MaxLength="9">0</asp:TextBox>
    </div>
</div>
<div class="form_box">
    <div class="form_single">
        <label> Kegiatan Usaha</label>
        <asp:Label ID="E_lblKegiatanUsaha" runat="server" Visible="True"></asp:Label>
    </div>
</div>

    <div class="form_box">
    <div class="form_single">
        <label> Jenis Pembiayaan</label>
        <asp:Label ID="E_lblJenisPembiayaan" runat="server" Visible="True"></asp:Label>
    </div>
</div>