﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
#End Region
Public Class ProductView
    Inherits Maxiloan.Webform.WebBased

    Private m_controller As New ProductController

    Private Property ProductID() As String
        Get
            Return CType(ViewState("ProductID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ProductID") = Value
        End Set
    End Property
   


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.ProductID = Request.QueryString("ProductID")
            Me.BranchID = Request.QueryString("BranchID")
            Bind()
        End If
    End Sub

    Sub Bind()
        Dim oProduct As New Parameter.Product

        oProduct.ProductId = Me.ProductID
        oProduct.strConnection = GetConnectionString()

        oProduct = m_controller.ProductView(oProduct)
        oProduct.ProductId = Me.ProductID

        UmumTab.Product = oProduct
        UmumTab.UCMode = "VIEW"
        UmumTab.UCProductName = "PRODUK"
        UmumTab.InitUCViewMode()

        SkemaTab.Product = oProduct
        SkemaTab.UCMode = "VIEW"
        SkemaTab.InitUCViewMode()

        BiayaTab.Product = oProduct
        BiayaTab.UCMode = "VIEW"
        BiayaTab.InitUCViewMode()


        AngsuranTab.Product = oProduct
        AngsuranTab.UCMode = "VIEW"
        AngsuranTab.InitUCViewMode()

        CollectionTab.Product = oProduct
        CollectionTab.UCMode = "VIEW"
        CollectionTab.InitUCViewMode()

        FinanceTab.Product = oProduct
        FinanceTab.UCMode = "VIEW"
        FinanceTab.InitUCViewMode()
    End Sub

End Class