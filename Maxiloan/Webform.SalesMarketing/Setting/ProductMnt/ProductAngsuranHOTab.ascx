﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ProductAngsuranHOTab.ascx.vb" Inherits="Maxiloan.Webform.SalesMarketing.ProductAngsuranHOTab" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
  <div>
    <div class="form_box_title"> 
    <div class="form_single"> <h4> ANGSURAN </h4> </div> 
  </div>

   <%-- <div class="form_box_header">
    <div class="form_single"> <h5> Prioritas Alokasi Pembayaran</h5> </div>
</div>
<div class="form_box">
    <div class="form_single"> <label class="label_general"> From HO</label>
         <asp:Label ID="lblPrioritasPembayaran" runat="server" />
        <asp:RadioButtonList ID="rboPrioritasPembayaran" runat="server" CssClass="opt_single_product" RepeatDirection="Horizontal"> <asp:ListItem Value="DE" Selected="True">Denda</asp:ListItem> <asp:ListItem Value="BE">Bebas</asp:ListItem> </asp:RadioButtonList>
    </div>
</div>
<div class="form_box">
    <div class="form_single"> <label> Branch</label> <asp:Label ID="lblPrioritasPembayaran_Branch" runat="server"></asp:Label> </div>
</div>--%>

<%-- <div class="form_box_header">
    <div class="form_single"> <h5>Pengaturan Pembayaran</h5> </div>
</div>
<div class="form_box" >
    <div class="form_single"> <label class="label_general"> From HO</label>
         <asp:Label ID="lblPengaturanPembayaran" runat="server" />
        <asp:RadioButtonList ID="rboPengaturanPembayaran" runat="server" CssClass="opt_single_product" RepeatDirection="Horizontal"><asp:ListItem Value="PR" Selected="True">Prioritas</asp:ListItem> <asp:ListItem Value="ML">Manual</asp:ListItem>  </asp:RadioButtonList>
    </div>
</div>
<div class="form_box">
    <div class="form_single"> <label>Branch</label> <asp:Label ID="lblPengaturanPembayaran_Branch" runat="server"></asp:Label> </div>
</div>
 <div class="form_box_header">
    <div class="form_single"> <h5>Prioritas Alokasi Pembayaran</h5> </div>
</div>
<div class="form_box" id="TR_Prioritas" runat="server">
    <div class="form_single"> <label class="label_general"> From HO</label>
         <asp:Label ID="Label1" runat="server" />
        <asp:label  ID="lblPrioritasPembayaran" runat="server" />
         <asp:DropDownList ID="cboPrioritasPembayaran" runat="server" CssClass="opt_single_product"> 
            <asp:ListItem Value="PIL">Pokok-Bunga-Denda</asp:ListItem>
            <asp:ListItem Value="PLI">Pokok-Denda-Bunga</asp:ListItem> 
            <asp:ListItem Value="IPL">Bunga-Pokok-Denda</asp:ListItem> 
            <asp:ListItem Value="ILP">Bunga-Denda-Pokok</asp:ListItem> 
            <asp:ListItem Value="LPI">Denda-Pokok-Bunga</asp:ListItem> 
            <asp:ListItem Value="LIP">Denda-Bunga-Pokok</asp:ListItem> 
         </asp:DropDownList>
    </div>
</div>
<div class="form_box">
    <div class="form_single"> <label>Branch</label> <asp:Label ID="lblPriotasPembayaran_Branch" runat="server"></asp:Label> </div>
</div>--%>
<div class="form_box">
    <div class="form_single">
        <label class="label_general">Pengaturan Pembayaran</label>
        <asp:label  ID="lblPengaturanPembayaran" runat="server" />
        <asp:RadioButtonList ID="rboPengaturanPembayaran" runat="server"  AutoPostBack="true" CssClass="opt_single_product" RepeatDirection="Horizontal"> 
        <asp:ListItem Value="PR" Selected="True">Prioritas</asp:ListItem> <asp:ListItem Value="ML">Manual</asp:ListItem> </asp:RadioButtonList>
    </div>
</div>
<div class="form_box" id="TR_Prioritas" runat="server">
    <div class="form_single">
        <label class="label_general">Prioritas Alokasi Pembayaran</label>
        <asp:label  ID="lblPrioritasPembayaran" runat="server" />
         <asp:DropDownList ID="cboPrioritasPembayaran" runat="server"> 
            <asp:ListItem Value="PIL" Selected="True">Pokok-Bunga-Denda</asp:ListItem>
            <asp:ListItem Value="PLI">Pokok-Denda-Bunga</asp:ListItem> 
            <asp:ListItem Value="IPL">Bunga-Pokok-Denda</asp:ListItem> 
            <asp:ListItem Value="ILP">Bunga-Denda-Pokok</asp:ListItem> 
            <asp:ListItem Value="LPI">Denda-Pokok-Bunga</asp:ListItem> 
            <asp:ListItem Value="LIP">Denda-Bunga-Pokok</asp:ListItem> 
         </asp:DropDownList>
    </div>
</div>
<div class="form_box_header">
    <div class="form_single"> <h5> Jumlah Toleransi terhadap Angsuran </h5>
    </div>
</div>
<div class="form_box">
    <div class="form_single">
        <label class="label_req"> From HO</label>
        <asp:Label ID="lblInstallmentToleranceAmount" runat="server"/> 
        <asp:TextBox onblur="com_NX('Tabs_pnlTabAngsuran_AngsuranTab_Hide_InstallmentToleranceAmountBeh','Tabs_pnlTabAngsuran_AngsuranTab_E_txtInstallmentToleranceAmount','Tabs_pnlTabAngsuran_AngsuranTab_Hide_InstallmentToleranceAmount','Jumlah Toleransi terhadap Angsuran');" ID="E_txtInstallmentToleranceAmount" runat="server" MaxLength="15">0</asp:TextBox>
        <asp:DropDownList ID="E_cboInstallmentToleranceAmountBehaviour" runat="server"/> 
        <input id="Hide_InstallmentToleranceAmount" type="hidden" name="Hide_InstallmentToleranceAmount" runat="server" />
        <input id="Hide_InstallmentToleranceAmountBeh" type="hidden" name="Hide_InstallmentToleranceAmountBeh" runat="server" />
        <asp:RequiredFieldValidator ID="Requiredfieldvalidator23" runat="server" ControlToValidate="E_txtInstallmentToleranceAmount" CssClass="validator_general" ErrorMessage="Jumlah Toleransi terhadap Angsuran salah" Display="Dynamic"></asp:RequiredFieldValidator>
        <asp:RangeValidator ID="rgv_InstallmentToleranceAmount" runat="server" ControlToValidate="E_txtInstallmentToleranceAmount" Type="Double" MinimumValue="0" ErrorMessage="Jumlah Toleransi terhadap Angsuran salah" CssClass="validator_general" MaximumValue="999999999999"></asp:RangeValidator>
    </div>
</div>
<div class="form_box">
    <div class="form_single"> <label> Branch</label> <asp:Label ID="lblInstallmentToleranceAmount_Branch" runat="server"></asp:Label> </div>
</div>

<div class="form_box_header">
    <div class="form_single"> <h5> Prosentase Denda Keterlambatan (Angsuran) </h5> </div>
</div>
<div class="form_box">
    <div class="form_single">
        <label class="label_req"> From HO</label>
        <asp:Label ID="lblPenaltyPercentage" runat="server"/>
        <asp:TextBox onblur="com_NX('Tabs_pnlTabAngsuran_AngsuranTab_Hide_PenaltyPercentageBeh','Tabs_pnlTabAngsuran_AngsuranTab_E_txtPenaltyPercentage','Tabs_pnlTabAngsuran_AngsuranTab_Hide_PenaltyPercentage','Prosentase Denda Keterlambatan (Angsuran)','100');" ID="E_txtPenaltyPercentage" runat="server" MaxLength="9">0</asp:TextBox>
       <asp:Label ID="lbldesc1" runat='server'>‰ / day</asp:Label>  
        <asp:DropDownList ID="E_cboPenaltyPercentageBehaviour" runat="server"/> 
        <input id="Hide_PenaltyPercentage" type="hidden" name="Hide_PenaltyPercentage" runat="server" />
        <input id="Hide_PenaltyPercentageBeh" type="hidden" name="Hide_PenaltyPercentageBeh" runat="server" />
        <asp:RequiredFieldValidator ID="Requiredfieldvalidator4" runat="server" ControlToValidate="E_txtPenaltyPercentage" CssClass="validator_general" ErrorMessage="Prosentase Denda Keterlambatan Angsuran salah" Display="Dynamic"></asp:RequiredFieldValidator> 
        <asp:RangeValidator ID="rgvPenaltyPercentage" runat="server" ControlToValidate="E_txtPenaltyPercentage" CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Prosentase Denda Keterlambatan Angsuran salah" MaximumValue="100"></asp:RangeValidator>
    </div>
</div>
<div class="form_box">
    <div class="form_single">
        <label> Branch</label> <asp:Label ID="lblPenaltyPercentage_Branch" runat="server"></asp:Label> 
        </div>
</div>

<div class="form_box_header">
    <div class="form_single"> <h5> Dasar Perhitungan</h5> </div>
</div>
<div class="form_box">
    <div class="form_single">
        <label class="label_general"> From HO</label>
          <asp:Label ID="lblPenaltyBasedOn" runat="server"/>
        <asp:RadioButtonList ID="rboPenaltyBasedOn" runat="server" CssClass="opt_single_product" RepeatDirection="Horizontal"> <asp:ListItem Value="PB" Selected="True">Pokok + Bunga</asp:ListItem> <asp:ListItem Value="PO">Pokok</asp:ListItem> </asp:RadioButtonList>
        <asp:DropDownList ID="cboPenaltyBasedOn" runat="server" /> 
    </div>
</div>
<div class="form_box">
    <div class="form_single">
        <label> Branch</label>
        <asp:Label runat="server" ID="lblPenaltyBasedOn_Branch"></asp:Label >
    </div>
</div>
                 
<div class="form_box_header">
    <div class="form_single"> <h5> Tanggal Efektif</h5> </div>
</div>
<div class="form_box">
    <div class="form_single">
        <label class="label_req"> From HO</label>
          <asp:Label ID="lblPenaltyRateEffectiveDate" runat="server"/>
        <asp:TextBox ID="txtPenaltyRateEffectiveDate" runat="server"></asp:TextBox> 
        <asp:CalendarExtender ID="txtPenaltyRateEffectiveDate_CalendarExtender1" runat="server" Enabled="True" TargetControlID="txtPenaltyRateEffectiveDate" Format="dd/MM/yyyy"> </asp:CalendarExtender>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator61" runat="server" ErrorMessage="*" CssClass="validator_general" ControlToValidate="txtPenaltyRateEffectiveDate"></asp:RequiredFieldValidator>
    </div>
</div>
<div class="form_box">
    <div class="form_single">
        <label> Branch</label>
        <asp:Label runat="server" ID="lblPenaltyRateEffectiveDate_Branch"></asp:Label>
    </div>
</div>
                 
<div class="form_box_header">
    <div class="form_single">
        <h5> Prosentase Denda Keterlambatan Sebelumnya</h5> </div>
</div>
<div class="form_box">
    <div class="form_single">
        <label> From HO</label>
         <asp:Label ID="lblPenaltyRatePrevious" runat="server"/>
        <asp:TextBox ID="txtPenaltyRatePrevious" runat="server" MaxLength="9">0</asp:TextBox><asp:Label ID="lbldesc2" runat='server'>‰ / day</asp:Label>   
    </div>
</div>
<div class="form_box">
    <div class="form_single">
        <label> Branch</label> <asp:Label runat="server" ID="lblPenaltyRatePrevious_Branch"></asp:Label>‰ / day
    </div>
</div>

<div class="form_box_header">
    <div class="form_single">
        <h5> Biaya Tagih </h5>
    </div>
</div>
<div class="form_box">
    <div class="form_single">
        <label class="label_req"> From HO</label>
          <asp:Label ID="lblBillingCharges" runat="server"/>
        <asp:TextBox onblur="com_NX('Tabs_pnlTabAngsuran_AngsuranTab_Hide_BillingChargesBeh','Tabs_pnlTabAngsuran_AngsuranTab_E_txtBillingCharges','Tabs_pnlTabAngsuran_AngsuranTab_Hide_BillingCharges','Biaya Tagih');" ID="E_txtBillingCharges" runat="server" MaxLength="15">0</asp:TextBox>
        <asp:DropDownList ID="E_cboBillingChargesBehaviour" runat="server"/> 
        <input id="Hide_BillingCharges" type="hidden" name="Hide_BillingCharges" runat="server" />
        <input id="Hide_BillingChargesBeh" type="hidden" name="Hide_BillingChargesBeh" runat="server" />
        <asp:RequiredFieldValidator ID="Requiredfieldvalidator49" runat="server" ControlToValidate="E_txtBillingCharges" CssClass="validator_general" ErrorMessage="Biaya Tagih Salah" Display="Dynamic"></asp:RequiredFieldValidator>
        <asp:RangeValidator ID="rgv_BillingCharges" runat="server" ControlToValidate="E_txtBillingCharges" CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Biaya Tagih Salah" MaximumValue="999999999999"></asp:RangeValidator>
    </div>
</div>
<div class="form_box">
    <div class="form_single">
        <label> Branch</label>
        <asp:Label ID="lblBillingCharges_Branch" runat="server"></asp:Label>
    </div>
</div>

    <div class="form_box_header">
    <div class="form_single">
        <h5> Denda Pelunasan dipercepat </h5>
    </div>
</div>
<div class="form_box">
    <div class="form_single">
        <label class="label_req"> From HO</label>
          <asp:Label ID="lblPrepaymentPenaltyRate" runat="server"/>
        <asp:TextBox onblur="com_NX('Tabs_pnlTabAngsuran_AngsuranTab_Hide_PrepaymentPenaltyRateBeh','Tabs_pnlTabAngsuran_AngsuranTab_E_txtPrepaymentPenaltyRate','Tabs_pnlTabAngsuran_AngsuranTab_Hide_PrepaymentPenaltyRate','Denda Pelunasan dipercepat','100');" ID="E_txtPrepaymentPenaltyRate" runat="server" MaxLength="9">0</asp:TextBox> <asp:Label ID="lbldesc3" runat=server>‰ </asp:Label>   
        <asp:DropDownList ID="E_cboPrepaymentPenaltyRateBehaviour" runat="server"/> 
        <input id="Hide_PrepaymentPenaltyRate" type="hidden" name="Hide_PrepaymentPenaltyRate" runat="server" />
        <input id="Hide_PrepaymentPenaltyRateBeh" type="hidden" name="Hide_PrepaymentPenaltyRateBeh" runat="server" />
        <asp:RequiredFieldValidator ID="Requiredfieldvalidator33" runat="server" ControlToValidate="E_txtPrepaymentPenaltyRate" CssClass="validator_general" ErrorMessage="Denda Pelunasan dipercepat Salah" Display="Dynamic"></asp:RequiredFieldValidator>
        <asp:RangeValidator ID="rgv_PrepaymentPenaltyRate" runat="server" ControlToValidate="E_txtPrepaymentPenaltyRate" CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Denda Pelunasan dipercepat Salah" MaximumValue="100"></asp:RangeValidator>
    </div>
</div>
<div class="form_box">
    <div class="form_single">
        <label> Branch</label>
        <asp:Label ID="lblPrepaymentPenaltyRate_Branch" runat="server"></asp:Label>
    </div>
</div>

<div class="form_box_header">
    <div class="form_single">
        <h5>
            Tanggal Efektif</h5>
    </div>
</div>
<div class="form_box">
    <div class="form_single">
        <label class="label_req"> From HO</label>
         <asp:Label ID="lblPrepaymentPenaltyEffectiveDate" runat="server"/>
        <asp:TextBox ID="txtPrepaymentPenaltyEffectiveDate" runat="server"></asp:TextBox>
        <asp:CalendarExtender ID="txtPrepaymentPenaltyEffectiveDate_CalendarExtender" runat="server" Enabled="True" TargetControlID="txtPrepaymentPenaltyEffectiveDate" Format="dd/MM/yyyy"> </asp:CalendarExtender>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator59" runat="server" ErrorMessage="*" CssClass="validator_general" ControlToValidate="txtPrepaymentPenaltyEffectiveDate"></asp:RequiredFieldValidator>
    </div>
</div>
<div class="form_box">
    <div class="form_single">
        <label> Branch</label>
        <asp:Label runat="server" ID="lblPrepaymentPenaltyEffectiveDate_Branch"></asp:Label>
    </div>
</div>

<div class="form_box_header">
    <div class="form_single">
        <h5> Denda Pelunasan Sebelumnya</h5>
    </div>
</div>
<div class="form_box">
    <div class="form_single">
        <label> From HO</label>
        <asp:Label ID="lblLabPrepaymentPenaltyPrevious" runat="server"/>
        <asp:TextBox ID="txtPrepaymentPenaltyPrevious" runat="server" MaxLength="9">0</asp:TextBox> <asp:Label ID="lbldesc4" runat="server">‰ / day</asp:Label>
    </div>
</div>
<div class="form_box">
    <div class="form_single">
        <label> Branch</label> <asp:Label ID="lblPrepaymentPenaltyPrevious_Branch" runat="server"></asp:Label> / day
    </div>
</div>

<div class="form_box_header">
    <div class="form_single">
        <h5> Denda Tetap Pelunasan dipercepat</h5> </div>
</div>
<div class="form_box">
    <div class="form_single">
        <label class="label_req"> From HO</label>
         <asp:Label ID="lblPrepaymentPenaltyFixed" runat="server"/>
        <asp:TextBox ID="txtPrepaymentPenaltyFixed" runat="server" />
        <asp:DropDownList ID="cboPrepaymentPenaltyFixedBehaviour" runat="server"/> 
        <asp:RequiredFieldValidator ID="Requiredfieldvalidator58" runat="server" ControlToValidate="txtPrepaymentPenaltyFixed" CssClass="validator_general" ErrorMessage="Denda Tetap Pelunasan dipercepat tidak boleh kosong" Display="Dynamic"></asp:RequiredFieldValidator>
    </div>
</div>
<div class="form_box">
    <div class="form_single">
        <label> Branch</label>
        <asp:Label ID="lblPrepaymentPenaltyFixed_Branch" runat="server"></asp:Label> 
    </div>
</div>

<div class="form_box_header">
    <div class="form_single">
        <h5> Tanggal Efektif</h5>
    </div>
</div>
<div class="form_box">
    <div class="form_single">
        <label class="label_req"> From HO</label>
       <asp:Label ID="lblPrepaymentPenaltyFixedEffectiveDate" runat="server"/>
        <asp:TextBox ID="txtPrepaymentPenaltyFixedEffectiveDate" runat="server"></asp:TextBox>
        <asp:CalendarExtender ID="txtPrepaymentPenaltyFixedEffectiveDate_CalendarExtender1" runat="server" Enabled="True" TargetControlID="txtPrepaymentPenaltyFixedEffectiveDate" Format="dd/MM/yyyy"> </asp:CalendarExtender>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator60" runat="server" ErrorMessage="*" CssClass="validator_general" ControlToValidate="txtPrepaymentPenaltyFixedEffectiveDate"></asp:RequiredFieldValidator>
    </div>
</div>
<div class="form_box">
    <div class="form_single">
        <label> Branch</label>
        <asp:Label ID="lblPrepaymentPenaltyFixedEffectiveDate_Branch" runat="server"></asp:Label>
    </div>
</div>

<div class="form_box_header">
    <div class="form_single">
        <h5> Denda Tetap Pelunasan Sebelumnya</h5> </div>
</div>
<div class="form_box">
    <div class="form_single">
        <label> From HO</label>
           <asp:Label ID="lblPrepaymentPenaltyFixedPrevious" runat="server"/>
        <asp:TextBox ID="txtPrepaymentPenaltyFixedPrevious" runat="server" MaxLength="9">0</asp:TextBox>
    </div>
</div>
<div class="form_box">
    <div class="form_single">
        <label> Branch</label>
        <asp:Label ID="lblPrepaymentPenaltyFixedPrevious_Branch" runat="server"></asp:Label>
    </div>
</div> 
</div>