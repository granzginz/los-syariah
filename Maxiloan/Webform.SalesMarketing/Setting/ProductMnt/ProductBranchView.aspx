﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ProductBranchView.aspx.vb"
    Inherits="Maxiloan.Webform.SalesMarketing.ProductBranchView" %>
    <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="productUmumHOTab.ascx" TagName="umum" TagPrefix="uc1" %>   
<%@ Register Src="productSkemaHOTab.ascx" TagName="skema" TagPrefix="uc2" %>  
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ProductBranchView</title>
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../../Include/General.css" type="text/css" />
    <script language="JavaScript" type="text/javascript">
        function fClose() {
            window.close();
            return false;
        }				
    </script>
</head>
<body>
    <form id="form1" runat="server">
    
       <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:Panel ID="pnlAddEdit" runat="server" Visible="true">

        <uc1:umum id="UmumTab" runat="server"/>
        <uc2:skema id="SkemaTab" runat="server"/>  


        <%--<div class="form_title">
            <div class="form_single">           
                <h3>
                    VIEW - PRODUK CABANG
                </h3>
            </div>
        </div>  
        <div class="form_box">
	        <div class="form_single">
                <label>ID Produk</label>
                <asp:Label ID="lblProductID" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Nama Produk</label>
                <asp:Label ID="lblDescription" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">              
                <h4>
                    UMUM
                </h4>
            </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Jenis Asset</label>
                <asp:Label ID="lblAssetType" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Skema Score - Marketing</label>
                <asp:Label ID="lblScoreSchemeId" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Skema Score - Kredit</label>
                <asp:Label ID="lblCreditScoreSchemeId" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Skema Jurnal</label>
                <asp:Label ID="lblJournalSchemeId" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Skema Approval</label>
                <asp:Label ID="lblApprovalSchemeId" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Asset : Used/New</label>
                <asp:Label ID="lblAssetUsedNew" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	    <div class="form_single">
            <label>Umur Kendaraan</label>
            <asp:Label ID="lblUmurKendaraanFrom" runat="server"></asp:Label>&nbsp;&nbsp;To&nbsp;&nbsp;
            <asp:Label ID="lblUmurKendaraanTo" runat="server"></asp:Label>
	    </div>
        </div> 
        <div class="form_box">
	        <div class="form_single">
                <label>Jenis Finance</label>
                <asp:Label ID="lblFinanceType" runat="server"></asp:Label>
	        </div>
        </div>--%>

<%--        <div class="form_box_header">
            <div class="form_single">              
                <h4>
                    SKEMA KREDIT
                </h4>
            </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Suku Bunga Effective</label>
                <asp:Label ID="lblEffectiveRate" runat="server"></asp:Label>&nbsp;%
                <asp:Label ID="lblEffectiveRateBehaviour" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Gross Yield Rate</label>
                <asp:Label ID="lblGrossYieldRate" runat="server"></asp:Label>&nbsp;%
                <asp:Label ID="lblGrossYieldRateBehaviour" runat="server"></asp:Label>
	        </div>  
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Jangka Waktu Minimum</label>
                <asp:Label ID="lblMinimumTenor" runat="server"></asp:Label>&nbsp;months
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Jangka Waktu Maximum</label>
                <asp:Label ID="lblMaximumTenor" runat="server"></asp:Label>&nbsp;months
	        </div>
        </div>--%>
      <%--  <div class="form_box_header">
            <div class="form_single">              
                <h4>
                    BIAYA-BIAYA
                </h4>
            </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Prosentase Denda Keterlambatan (Angsuran)</label>
                <asp:Label ID="lblLCP_Installment" runat="server"></asp:Label>&nbsp;‰ / day
                <asp:Label ID="lblLCP_InstallmentBehaviour" runat="server"></asp:Label>
	        </div>
        </div>--%>
       <%-- <div class="form_box">
	        <div class="form_single">
                <label>Prosentase Denda keterlambatan (Asuransi)</label>
               
	        </div>
        </div>--%>
      <%--   <asp:Label Visible="false" ID="lblLCP_Insurance" runat="server"></asp:Label>
                <asp:Label Visible="false" ID="lblLCP_InsuranceBehaviour" runat="server"></asp:Label>
        <div class="form_box">
	        <div class="form_single">
                <label>Dasar Perhitungan</label>
                <asp:Label ID="lblPenaltyBasedOn" runat="server"></asp:Label>&nbsp;&nbsp;
                <asp:Label ID="lblPenaltyBasedOnBehaviour" runat="server"></asp:Label>
	        </div>
        </div>    
        <div class="form_box">
	        <div class="form_single">
                <label>Tanggal Efektif</label>
                <asp:Label ID="lblPenaltyRateEffectiveDate" runat="server"></asp:Label>            
	        </div>
        </div>  
        <div class="form_box">
	        <div class="form_single">
                <label>Prosentase Denda Keterlambatan Sebelumnya</label>
                <asp:Label ID="lblPenaltyRatePrevious" runat="server"></asp:Label>‰            
	        </div>
        </div>     
        <div class="form_box">
	        <div class="form_single">
                <label>Biaya Pembatalan</label>
                <asp:Label ID="lblCancellationFee" runat="server"></asp:Label>
                <asp:Label ID="lblCancellationFeeBehaviour" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Biaya Administrasi</label>
                <asp:Label ID="lblAdminFee" runat="server"></asp:Label>
                <asp:Label ID="lblAdminFeeBehaviour" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Biaya Fidusia</label>
                <asp:Label ID="lblFiduciaFee" runat="server"></asp:Label>
                <asp:Label ID="lblFiduciaFeeBehaviour" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Biaya Provisi</label>
                <asp:Label ID="lblProvisionFee" runat="server"></asp:Label>
                <asp:Label ID="lblProvisionFeeBehaviour" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Biaya Notaris</label>
                <asp:Label ID="lblNotaryFee" runat="server"></asp:Label>
                <asp:Label ID="lblNotaryFeeBehaviour" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Biaya Survey</label>
                <asp:Label ID="lblSurveyFee" runat="server"></asp:Label>
                <asp:Label ID="lblSurveyFeeBehaviour" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Biaya Kunjungan</label>
                <asp:Label ID="lblVisitFee" runat="server"></asp:Label>
                <asp:Label ID="lblVisitFeeBehaviour" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Biaya Reschedulling</label>
                <asp:Label ID="lblReschedulingFee" runat="server"></asp:Label>&nbsp;
                <asp:Label ID="lblReschedulingFeeBehaviour" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Biaya Over Kontrak</label>
                <asp:Label ID="lblATFee" runat="server"></asp:Label>
                <asp:Label ID="lblATFeeBehaviour" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Biaya Ganti Tgl jatuh Tempo</label>
                <asp:Label ID="lblCDDRFee" runat="server"></asp:Label>
                <asp:Label ID="lblCDDRFeeBehaviour" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Biaya Penggantian Asset</label>
                <asp:Label ID="lblAssetReplacementFee" runat="server"></asp:Label>
                <asp:Label ID="lblAssetReplacementFeeBehaviour" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Biaya Penarikan</label>
                <asp:Label ID="lblRepossesFee" runat="server"></asp:Label>&nbsp;
                <asp:Label ID="lblRepossesFeeBehaviour" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Biaya Legalisasi Dokumen</label>
                <asp:Label ID="lblLDFee" runat="server"></asp:Label>&nbsp;
                <asp:Label ID="lblLDFeeBehaviour" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Biaya Penolakan PDC</label>
                <asp:Label ID="lblPDCBounceFee" runat="server"></asp:Label>
                <asp:Label ID="lblPDCBounceFeeBehaviour" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Biaya Admin Asuransi</label>
                <asp:Label ID="lblInsuranceAdminFee" runat="server"></asp:Label>
                <asp:Label ID="lblInsuranceAdminFeeBehaviour" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Biaya Materai Asuransi</label>
                <asp:Label ID="lblISDFee" runat="server"></asp:Label>&nbsp;
                <asp:Label ID="lblISDFeeBehaviour" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Biaya Polis</label>
                <asp:Label ID="lblBiayaPolis" runat="server"></asp:Label>&nbsp;
                <asp:Label ID="lblBiayaPolisBehaviour" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">              
                <h4>    
                    PROSES KREDIT
                </h4>
            </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Masa Berlaku PO (dalam hari)</label>
                <asp:Label ID="lblPOExpirationDays" runat="server"></asp:Label>&nbsp;days&nbsp;
                <asp:Label ID="lblPOExpirationDaysBehaviour" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="fo  rm_single">
                <label>Jumlah Toleransi terhadap Angsuran</label>
                <asp:Label ID="lblInsToleranceAmount" runat="server"></asp:Label>&nbsp;
                <asp:Label ID="lblInsToleranceAmountBehaviour" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Prosentase Minimum DP</label>
                <asp:Label ID="lblMinimumDPPerc" runat="server"></asp:Label>&nbsp;%&nbsp;
                <asp:Label ID="lblMinimumDPPercBehaviour" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Pendapatan Bunga Minimum yg harus ditolak</label>
                <asp:Label ID="lblMIITBR" runat="server"></asp:Label>&nbsp;%&nbsp;
                <asp:Label ID="lblMIITBRBehaviour" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Pendapatan Bunga Minimum yg harus diingatkan</label>
                <asp:Label ID="lblMIITBW" runat="server"></asp:Label>&nbsp;%&nbsp;
                <asp:Label ID="lblMIITBWBehaviour" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Prioritas Alokasi Pembayaran</label>
                <asp:Label ID="lblPrioritasPembayaran" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">              
                <h4>
                    COLLECTION  
                </h4>
            </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Buat SP 1 Otomatis</label>
                <asp:Label ID="lblIsSPAutomatic" runat="server"></asp:Label>&nbsp;
                <asp:Label ID="lblIsSPAutomaticBehaviour" runat="server"></asp:Label>&nbsp;Length
                SP Process
                <asp:Label ID="lblLengthSPProcess" runat="server"></asp:Label>&nbsp;days
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Buat SP 2 Otomatis</label>
                <asp:Label ID="lblIsSP1Automatic" runat="server"></asp:Label>&nbsp;
                <asp:Label ID="lblIsSP1AutomaticBehaviour" runat="server"></asp:Label>&nbsp;Length
                SP 1 Process
                <asp:Label ID="lblLengthSP1Process" runat="server"></asp:Label>&nbsp;days
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Buat SP 3 Otomatis</label>
                <asp:Label ID="lblIsSP2Automatic" runat="server"></asp:Label>&nbsp;
                <asp:Label ID="lblIsSP2AutomaticBehaviour" runat="server"></asp:Label>&nbsp;Length
                SP 2 Process
                <asp:Label ID="lblLengthSP2Process" runat="server"></asp:Label>&nbsp;days
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Jangka Waktu proses Dokumen Utama</label>
                <asp:Label ID="lblLMDProcessed" runat="server"></asp:Label>&nbsp;days
                <asp:Label ID="lblLMDProcessedBehaviour" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Jangka Waktu Dokumen Utama diambil</label>
                <asp:Label ID="lblLMDTaken" runat="server"></asp:Label>&nbsp;days
                <asp:Label ID="lblLMDTakenBehaviour" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Grace Period Denda Keterlambatan</label>
                <asp:Label ID="lblGPLC" runat="server"></asp:Label>&nbsp;days
                <asp:Label ID="lblGPLCBehaviour" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Denda Pelunasan dipercepat</label>
                <asp:Label ID="lblPPR" runat="server"></asp:Label>&nbsp;%
                <asp:Label ID="lblPPRBehaviour" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	    <div class="form_single">
            <label>Tanggal Efektif</label>
            <asp:Label ID="lblPrepaymentPenaltyEffectiveDate" runat="server"></asp:Label>
	    </div>
    </div>
    <div class="form_box">
	    <div class="form_single">
            <label>Denda Pelunasan Sebelumnya</label>
            <asp:Label ID="lblPrepaymentPenaltyPrevious" runat="server"></asp:Label>‰
	    </div>
    </div>
    <div class="form_box">
	    <div class="form_single">
            <label>Denda Tetap Pelunasan dipercepat</label>
            <asp:Label ID="lblPrepaymentPenaltyFixed" runat="server"></asp:Label>&nbsp;&nbsp;
            <asp:Label ID="lblPrepaymentPenaltyFixedBehaviour" runat="server"></asp:Label>
	    </div>
    </div>
    <div class="form_box">
	    <div class="form_single">
            <label>Tanggal Efektif</label>
            <asp:Label ID="lblPrepaymentPenaltyFixedEffectiveDate" runat="server"></asp:Label>
	    </div>
    </div>
    <div class="form_box">
	    <div class="form_single">
            <label>Denda Pelunasan Tetap Sebelumnya</label>
            <asp:Label ID="lblPrepaymentPenaltyFixedPrevious" runat="server"></asp:Label>
	    </div>
    </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Jumlah hari mengingatkan angsuran</label>
                <asp:Label ID="lblDaystoremindInstallment" runat="server"></asp:Label>&nbsp;days
                <asp:Label ID="lblDaystoremindInstallmentBehaviour" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Jumlah hari Overdue untuk Deskcoll</label>
                <asp:Label ID="lblDaysOverduetocall" runat="server"></asp:Label>&nbsp;days
                <asp:Label ID="lblDaysOverduetocallBehaviour" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">   
                <label>Overdue sebelumnya untuk mengingatkan</label>
                <asp:Label ID="lblPreviousOverduetoremind" runat="server"></asp:Label>&nbsp;days
                <asp:Label ID="lblPreviousOverduetoremindBehaviour" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Permintaan PDC untuk telepon</label>
                <asp:Label ID="lblPDCRequesttocall" runat="server"></asp:Label>&nbsp;days
                <asp:Label ID="lblPDCRequesttocallBehaviour" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Jumlah hari mengingatkan dgn SMS</label>
                <asp:Label ID="lblDaysToRemind" runat="server"></asp:Label>&nbsp;days
                <asp:Label ID="lblDaysToRemindBehaviour" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Jumlah hari buat DCR</label>
                <asp:Label ID="lblDaysToGenerateDCR" runat="server"></asp:Label>&nbsp;days
                <asp:Label ID="lblDaysToGenerateDCRBehaviour" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Jumlah hari sebelum JT buat Kwitansi</label>
                <asp:Label ID="lblDaysBeforeDuetoRN" runat="server"></asp:Label>&nbsp;days
                <asp:Label ID="lblDaysBeforeDuetoRNBehaviour" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Jumlah hari buat SKT (Surat Kuasa Tarik)</label>
                <asp:Label ID="lblDaysToGenerateRAL" runat="server"></asp:Label>&nbsp;days
                <asp:Label ID="lblDaysToGenerateRALBehaviour" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Jangka Waktu SKT</label>
                <asp:Label ID="lblRALPeriod" runat="server"></asp:Label>&nbsp;days
                <asp:Label ID="lblRALPeriodBehaviour" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Jumlah hari Maximum Kwitansi dibayar</label>
                <asp:Label ID="lblMaxDaysRNPaid" runat="server"></asp:Label>&nbsp;days
                <asp:Label ID="lblMaxDaysRNPaidBehaviour" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Jumlah hari Maximum janji Bayar</label>
                <asp:Label ID="lblMaxPTPYDays" runat="server"></asp:Label>&nbsp;days
                <asp:Label ID="lblMaxPTPYDaysBehaviour" runat="server"></asp:Label>
	        </div>  
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Janji Bayar ke Bank</label>
                <asp:Label ID="lblPTPYBank" runat="server"></asp:Label>&nbsp;days
                <asp:Label ID="lblPTPYBankBehaviour" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Janji bayar ke Perusahaan</label>
                <asp:Label ID="lblPTPYCompany" runat="server"></asp:Label>&nbsp;days
                <asp:Label ID="lblPTPYCompanyBehaviour" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Janji bayar ke Supplier</label>
                <asp:Label ID="lblPTPYSupplier" runat="server"></asp:Label>&nbsp;days
                <asp:Label ID="lblPTPYSupplierBehaviour" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Perpanjangan SKT</label>
                <asp:Label ID="lblRALExtension" runat="server"></asp:Label>&nbsp;days
                <asp:Label ID="lblRALExtensionBehaviour" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Jangka Waktu jadi Inventory</label>
                <asp:Label ID="lblInventoryExpected" runat="server"></asp:Label>&nbsp;days
                <asp:Label ID="lblInventoryExpectedBehaviour" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Biaya Tagih</label>
                <asp:Label ID="lblBilingCharges" runat="server"></asp:Label>&nbsp;
                <asp:Label ID="lblBilingChargesBehaviour" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">              
                <h4>
                    FINANCE
                </h4>
            </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Limit Pembayaran di Cabang</label>
                <asp:Label ID="lblLimitAPCash" runat="server"></asp:Label>&nbsp;
                <asp:Label ID="lblLimitAPCashBehaviour" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Recourse</label>
                <asp:Label ID="lblIsRecourse" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Aktif</label>
                <asp:Label ID="lblIsActive" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Diskon Asuransi</label>
                <asp:Label ID="lblInsuranceDiscPercentage" runat="server"></asp:Label>&nbsp;%&nbsp;
                <asp:Label ID="lblInsuranceDiscPercentageBehaviour" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Rasio Penghasilan thd Angsuran</label>
                <asp:Label ID="lblIncomeInsRatioPercentage" runat="server"></asp:Label>&nbsp;%&nbsp;
                <asp:Label ID="lblIncomeInsRatioPercentageBehaviour" runat="server"></asp:Label>
	        </div>
        </div>--%>
        <div class="form_button">
            <asp:Button ID="ButtonClose" OnClientClick="fClose()" runat="server" CausesValidation="false"  Text="Close" CssClass ="small button gray">
            </asp:Button>
	    </div>        
    </asp:Panel>
    </form>
</body>
</html>
