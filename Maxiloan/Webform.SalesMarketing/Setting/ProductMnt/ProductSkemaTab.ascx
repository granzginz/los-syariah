﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ProductSkemaTab.ascx.vb" Inherits="Maxiloan.Webform.SalesMarketing.ProductSkemaTab" %>
 <div class="form_box_title">
    <div class="form_single"> <h4> SKEMA </h4> </div>
 </div>

<div class="form_box">
    <div class="form_single">
        <label> Skema Pembiayaan</label>
        <asp:DropDownList ID="cboSkemaPembiayaan" runat="server" /> 
        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="cboSkemaPembiayaan" CssClass="validator_general" ErrorMessage="SkemaPembiayaan harus di pilih" Enabled="True" Display="Dynamic"></asp:RequiredFieldValidator>--%>
        <%--<asp:RangeValidator ID="RangeValidator5" runat="server" ControlToValidate="cboSkemaPembiayaan" CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="SkemaPembiayaan harus di pilih" Enabled="True" MaximumValue="100"></asp:RangeValidator>--%>
    </div>
</div>  
                              
<div class="form_box">
    <div class="form_single">
        <label> Jangka Waktu Maximum</label>
        <asp:Label ID="lblMinimumTenor" runat="server"></asp:Label> 
        <asp:TextBox ID="txtMaximumTenor" runat="server"  MaxLength="3">1</asp:TextBox><asp:Label ID="Label1" runat="server">months</asp:Label> 
        <asp:RequiredFieldValidator ID="Requiredfieldvalidator9" runat="server" ControlToValidate="txtMaximumTenor" CssClass="validator_general" ErrorMessage="Jangka Waktu Maximum salah" Enabled="True" Display="Dynamic" Visible="True"></asp:RequiredFieldValidator>
        <asp:RangeValidator ID="Rangevalidator3" runat="server" ControlToValidate="txtMaximumTenor" CssClass="validator_general" Type="Double" MinimumValue="1" ErrorMessage="Jangka Waktu Maximum salah" Display="Dynamic" MaximumValue="999"></asp:RangeValidator>
        <asp:CompareValidator ID="Comparevalidator2" runat="server" ControlToValidate="txtMaximumTenor" CssClass="validator_general" Type="Double" ErrorMessage="Jangka Waktu Maximum harus >= Jangka waktu Minimum" Enabled="True" Display="Dynamic" Visible="True" Operator="GreaterThanEqual" ControlToCompare="txtMinimumTenor" Font-Size="8" Font-Name="Verdana"></asp:CompareValidator>
        <asp:CompareValidator ID="Comparevalidator1" runat="server" ControlToValidate="txtMinimumTenor" CssClass="validator_general" Type="Double" ErrorMessage="" Enabled="true" Display="Dynamic" Visible="True" Operator="LessThanEqual" ControlToCompare="txtMaximumTenor" Font-Size="8" Font-Name="Verdana"></asp:CompareValidator>
    </div>
</div> 
<div class="form_box">
    <div class="form_single">
        <label> Jangka Waktu Minimum</label>
         <asp:Label ID="lblMaximumTenor" runat="server"></asp:Label> 
        <asp:TextBox ID="txtMinimumTenor" runat="server"  MaxLength="3">1</asp:TextBox><asp:Label ID="Label2" runat="server">months</asp:Label> 
        <asp:RequiredFieldValidator ID="Requiredfieldvalidator8" runat="server" ControlToValidate="txtMinimumTenor" CssClass="validator_general" ErrorMessage="Jangka Waktu Minimum salah" Display="Dynamic"></asp:RequiredFieldValidator>
        <asp:RangeValidator ID="Rangevalidator2" runat="server" ControlToValidate="txtMinimumTenor" CssClass="validator_general" Type="Double" MinimumValue="1" ErrorMessage="Jangka Waktu Minimum salah" Display="Dynamic" MaximumValue="999"></asp:RangeValidator>
    </div>
</div>
              
<div class="form_box">
    <div class="form_single">
        <label>Angsuran Pertama</label>
        <asp:Label ID="lblAngsuranPertama" runat="server"></asp:Label> 
         <asp:DropDownList ID="cboAngsuranPertama" runat="server" > <asp:ListItem Value="ARR" Selected="True">ARREAR</asp:ListItem> <asp:ListItem Value="ADV">ADVANCE</asp:ListItem>  </asp:DropDownList> 
        </div>
</div>
<div class="form_box">
    <div class="form_single">
        <label> Margin Flat</label>
        <asp:Label ID="lblSukuBungaFlat" runat="server"></asp:Label> 
        <asp:TextBox ID="txtFlatRate" runat="server"  MaxLength="9">0</asp:TextBox><asp:Label ID="Label3" runat="server">%</asp:Label> 
        <asp:DropDownList ID="cboFlatRateBehaviour" runat="server" /> 
        <asp:RequiredFieldValidator ID="RequiredFieldValidator67" runat="server" ControlToValidate="txtFlatRate" CssClass="validator_general" ErrorMessage="Suku bunga Flat salah" Enabled="True" Display="Dynamic"></asp:RequiredFieldValidator>
        <asp:RangeValidator ID="RangeValidator55" runat="server" ControlToValidate="txtFlatRate" CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Seku bunga Flat salah" Enabled="True" MaximumValue="100"></asp:RangeValidator>
    </div>
</div> 
<div class="form_box">
    <div class="form_single">
        <label> Margin Effective</label>
        <asp:Label ID="lblEffectiveRate" runat="server"></asp:Label> 
        <asp:TextBox ID="txtEffectiveRate" runat="server"  MaxLength="9">1</asp:TextBox><asp:Label ID="Label4" runat="server">%</asp:Label> 
        <asp:DropDownList ID="cboEffectiveRateBehaviour" runat="server" /> 
        <asp:RequiredFieldValidator ID="rfv" runat="server" ControlToValidate="txtEffectiveRate" CssClass="validator_general" ErrorMessage="Suku bunga Effective salah" Enabled="True" Display="Dynamic"></asp:RequiredFieldValidator>
        <asp:RangeValidator ID="rgv" runat="server" ControlToValidate="txtEffectiveRate" CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Seku bunga Effective salah" Enabled="True" MaximumValue="100"></asp:RangeValidator>
    </div>
</div>  
<div class="form_box">
    <div class="form_single">
        <label>Opsi Uang Muka</label> 
        <asp:Label ID="lblOpsiUangMuka" runat="server"></asp:Label> 
         <asp:DropDownList ID="cboOpsiUangMuka" runat="server" > <asp:ListItem Value="P" Selected="True">PROSENTASE</asp:ListItem> <asp:ListItem Value="A">ANGSURAN</asp:ListItem>  </asp:DropDownList>
</div>
</div>
    <div class="form_box">
    <div class="form_single">
        <label>Uang Muka (%)</label>
        <asp:Label ID="lblMinimumDPPerc" runat="server"></asp:Label> 
        <asp:TextBox ID="txtMinimumDPPerc" runat="server"  MaxLength="9">0</asp:TextBox><asp:Label ID="Label5" runat="server">%</asp:Label>  
        <asp:DropDownList ID="cboMinimumDPPerc" runat="server" /> <asp:Label ID="lblGLTDP" runat="server">GL TDP</asp:Label>
        <asp:RequiredFieldValidator ID="Requiredfieldvalidator30" runat="server" ControlToValidate="txtMinimumDPPerc" CssClass="validator_general" ErrorMessage="Prosentase Minimum DP Salah" Display="Dynamic"></asp:RequiredFieldValidator>
        <asp:RangeValidator ID="Rangevalidator24" runat="server" ControlToValidate="txtMinimumDPPerc" CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Prosentase Minimum DP Salah" Display="Dynamic" MaximumValue="100"></asp:RangeValidator>
    </div>
</div> 
    <div class="form_box">
    <div class="form_single">
        <label>Uang Muka (Angsuran)</label>
        <asp:Label ID="lblUangMukaAngsuran" runat="server"></asp:Label> 
        <asp:TextBox ID="txtUangMukaAngsuran" runat="server"  MaxLength="9">0</asp:TextBox><asp:Label ID="Label6" runat="server">x</asp:Label><asp:Label ID="GLTitipan" runat="server">GL Titipan Ansuran</asp:Label>  
        <asp:RequiredFieldValidator ID="Requiredfieldvalidator68" runat="server" ControlToValidate="txtUangMukaAngsuran" CssClass="validator_general" ErrorMessage="Uang Muka (Angsuran) Salah" Display="Dynamic"></asp:RequiredFieldValidator>
        <asp:RangeValidator ID="Rangevalidator56" runat="server" ControlToValidate="txtUangMukaAngsuran" CssClass="validator_general" Type="Double" MinimumValue="0" ErrorMessage="Uang Muka (Angsuran) Salah" Display="Dynamic"  MaximumValue="500"></asp:RangeValidator>
    </div>
</div> 

    <div class="form_box">
    <div class="form_single">
        <label>Masa Berlaku PO (dalam hari)</label>
         <asp:Label ID="lblPOExpirationDays" runat="server"></asp:Label> 
         <asp:TextBox ID="txtPOExpirationDays" runat="server"  MaxLength="4">0</asp:TextBox><asp:Label ID="Label7" runat="server">days</asp:Label> 
         <asp:DropDownList ID="cboPOExpirationDays" runat="server" />
         <asp:RequiredFieldValidator ID="Requiredfieldvalidator28" runat="server" ControlToValidate="txtPOExpirationDays" CssClass="validator_general" ErrorMessage="Masa Berlaku PO salah" Display="Dynamic"></asp:RequiredFieldValidator>
         <asp:RangeValidator ID="Rangevalidator22" runat="server" ControlToValidate="txtPOExpirationDays" CssClass="validator_general" Type="integer" MinimumValue="0" ErrorMessage="Masa Berlaku PO salah" Display="Dynamic" MaximumValue="9999"></asp:RangeValidator>
    </div>
    </div>

    <div class="form_box">
    <div class="form_single">
        <label>Proteksi Pembiayaan</label>
         <asp:Label ID="LblCreditProtect" runat="server"></asp:Label> 
         <asp:TextBox ID="TxtCreditProtect" runat="server"  MaxLength="4">0</asp:TextBox><asp:Label ID="Label9" runat="server">%</asp:Label> 
         <asp:DropDownList ID="cboCreditProtect" runat="server" />
         <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" ControlToValidate="TxtCreditProtect" CssClass="validator_general" ErrorMessage="Credit Protection Salah" Display="Dynamic"></asp:RequiredFieldValidator>
         <asp:RangeValidator ID="Rangevalidator1" runat="server" ControlToValidate="TxtCreditProtect" CssClass="validator_general" Type="integer" MinimumValue="0" ErrorMessage="Credit Protection Salah" Display="Dynamic" MaximumValue="9999"></asp:RangeValidator>
    </div>
    </div>

    <div class="form_box">
    <div class="form_single">
        <label>Asuransi Jaminan Pembiayaan</label>
         <asp:Label ID="lblAsuransiKredit" runat="server"></asp:Label> 
         <asp:TextBox ID="txtAsuransiKredit" runat="server"  MaxLength="4">0</asp:TextBox><asp:Label ID="Label10" runat="server">%</asp:Label> 
         <asp:DropDownList ID="cboAsuransiKredit" runat="server" />
         <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" ControlToValidate="txtAsuransiKredit" CssClass="validator_general" ErrorMessage="Asuransi Jaminan Kredit salah" Display="Dynamic"></asp:RequiredFieldValidator>
         <asp:RangeValidator ID="Rangevalidator4" runat="server" ControlToValidate="txtAsuransiKredit" CssClass="validator_general" Type="integer" MinimumValue="0" ErrorMessage="Asuransi Jaminan Kredit salah" Display="Dynamic" MaximumValue="9999"></asp:RangeValidator>
    </div>
</div>