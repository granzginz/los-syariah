﻿Public Class ProductSkemaBRTab
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Public Property UCMode As String
        Get
            Return CType(ViewState("SKEMATABUCMODE"), String)
        End Get
        Set(value As String)
            ViewState("SKEMATABUCMODE") = value
        End Set
    End Property

    Public Property Product() As Parameter.Product
        Get
            Return CType(ViewState("ProductSkemaTabProduct"), Parameter.Product)
        End Get
        Set(ByVal Value As Parameter.Product)
            ViewState("ProductSkemaTabProduct") = Value
        End Set
    End Property
    Sub InitialControls()

        lblMaximumTenor.Visible = (UCMode = "VIEW")
        E_txtMaximumTenor.Visible = (UCMode <> "VIEW")

        lbltxtMinimumTenor.Visible = (UCMode = "VIEW")
        E_txtMinimumTenor.Visible = (UCMode <> "VIEW")

        lblAngsuranPertama.Visible = (UCMode = "VIEW")
        E_cboAngsuranPertama.Visible = (UCMode <> "VIEW")


        lblFlatRate.Visible = (UCMode = "VIEW")
        E_txtFlatRate.Visible = (UCMode <> "VIEW")
        E_cboFlatRateBehaviour.Visible = (UCMode <> "VIEW")


        lblEffectiveRate.Visible = (UCMode = "VIEW")
        E_txtEffectiveRate.Visible = (UCMode <> "VIEW")
        E_cboEffectiveRateBehaviour.Visible = (UCMode <> "VIEW")


        lblOpsiUangMuka.Visible = (UCMode = "VIEW")
        E_cboOpsiUangMuka.Visible = (UCMode <> "VIEW")


        lblDPPercentage.Visible = (UCMode = "VIEW")
        E_txtDPPercentage.Visible = (UCMode <> "VIEW")
        E_cboDPPercentageBehaviour.Visible = (UCMode <> "VIEW")


        lblUangMukaAngsuran.Visible = (UCMode = "VIEW")
        txtUangMukaAngsuran.Visible = (UCMode <> "VIEW")

        lblPOExpirationDays.Visible = (UCMode = "VIEW")
        E_txtPOExpirationDays.Visible = (UCMode <> "VIEW")
        E_cboPOExpirationDaysBehaviour.Visible = (UCMode <> "VIEW")

        lblCreditProtection.Visible = (UCMode = "VIEW")
        E_txtCreditProtection.Visible = (UCMode <> "VIEW")
        E_cboCreditProtectionBehaviour.Visible = (UCMode <> "VIEW")

        lblAsuransiJaminan.Visible = (UCMode = "VIEW")
        E_txtAsuransiJaminan.Visible = (UCMode <> "VIEW")
        E_cboAsuransiJaminanBehaviour.Visible = (UCMode <> "VIEW")


        Label1.Visible = (UCMode <> "VIEW")
        Label2.Visible = (UCMode <> "VIEW")
        Label3.Visible = (UCMode <> "VIEW")
        Label4.Visible = (UCMode <> "VIEW")
        Label5.Visible = (UCMode <> "VIEW")
        Label6.Visible = (UCMode <> "VIEW")
        Label7.Visible = (UCMode <> "VIEW")

        Label9.Visible = (UCMode <> "VIEW")
        Label11.Visible = (UCMode <> "VIEW")


        If (UCMode = "ADD") Then
            initFieldsForAddMode()
        End If
        If (UCMode = "EDIT") Then
            bindFiedsForEdit()
        End If
    End Sub
    Sub initFieldsForAddMode()

    End Sub
    Public Sub BindFiedsForEdit()

        E_txtEffectiveRate.Text = CStr(FormatNumber(Product.EffectiveRate, 6))
        Hide_EffectiveRate.Value = E_txtEffectiveRate.Text
        Hide_EffectiveRateBeh.Value = Product.EffectiveRateBehaviour
        E_cboEffectiveRateBehaviour.SelectedIndex = E_cboEffectiveRateBehaviour.Items.IndexOf(E_cboEffectiveRateBehaviour.Items.FindByValue(Product.EffectiveRateBehaviour))
        Behaviour_Lock(Product.EffectiveRateBehaviour, E_txtEffectiveRate, E_cboEffectiveRateBehaviour)


        rgv_EffectiveRate.MinimumValue = "0"
        rgv_EffectiveRate.MaximumValue = "100"

        If Product.EffectiveRateBehaviour = "N" Then
            rgv_EffectiveRate.MinimumValue = Hide_EffectiveRate.Value
            rgv_EffectiveRate.MaximumValue = "100"
            rgv_EffectiveRate.ErrorMessage = "Harap is Suku Bunga Effective antara " & rgv_EffectiveRate.MinimumValue & " dan 100 "
        ElseIf Product.EffectiveRateBehaviour = "X" Then
            rgv_EffectiveRate.MinimumValue = "0"
            rgv_EffectiveRate.MaximumValue = Hide_EffectiveRate.Value
            rgv_EffectiveRate.ErrorMessage = "Harap is Suku Bunga Effective antara 1 dan " & rgv_EffectiveRate.MaximumValue
        End If



        'Minimum Tenor
        E_txtMinimumTenor.Text = CStr(Product.MinimumTenor)
        Hide_MinimumTenor.Value = CStr(Product.MinimumTenor)
        'Maximum Tenor
        E_txtMaximumTenor.Text = CStr(Product.MaximumTenor)
        Hide_MaximumTenor.Value = CStr(Product.MaximumTenor)


        Hide_AngsuranPertama.Value = Product.AngsuranPertama
        E_cboAngsuranPertama.SelectedIndex = E_cboAngsuranPertama.Items.IndexOf(E_cboAngsuranPertama.Items.FindByValue(Product.AngsuranPertama))

        E_txtFlatRate.Text = CStr(FormatNumber(Product.SukuBungaFlat, 6))
        Hide_FlatRate.Value = E_txtFlatRate.Text
        Hide_FlatRateBeh.Value = Product.SukuBungaFlatBehaviour
        E_cboFlatRateBehaviour.SelectedIndex = E_cboFlatRateBehaviour.Items.IndexOf(E_cboFlatRateBehaviour.Items.FindByValue(Product.SukuBungaFlatBehaviour))
        Behaviour_Lock(Product.SukuBungaFlatBehaviour, E_txtFlatRate, E_cboFlatRateBehaviour)

        E_cboOpsiUangMuka.SelectedIndex = E_cboOpsiUangMuka.Items.IndexOf(E_cboOpsiUangMuka.Items.FindByValue(Product.OpsiUangMuka))
        Hide_OpsiUangMuka.Value = Product.OpsiUangMuka

        txtUangMukaAngsuran.Text = CStr(Product.OpsiUangMukaAngsur)
        Hide_UangMukaAngsuran.Value = CStr(Product.OpsiUangMukaAngsur)
        E_txtPOExpirationDays.Text = CStr(Product.DaysPOExpiration)
        E_cboPOExpirationDaysBehaviour.SelectedIndex = E_cboPOExpirationDaysBehaviour.Items.IndexOf(E_cboPOExpirationDaysBehaviour.Items.FindByValue(Product.DaysPOExpirationBehaviour))


        E_txtCreditProtection.Text = CStr(FormatNumber(Product.CreditProtection, 6))
        Hide_CreditProtection.Value = E_txtCreditProtection.Text
        Hide_CreditProtectionBeh.Value = Product.CreditProtection_HO
        E_cboCreditProtectionBehaviour.SelectedIndex = E_cboCreditProtectionBehaviour.Items.IndexOf(E_cboCreditProtectionBehaviour.Items.FindByValue(Product.CreditProtectionBehaviour_HO))
        Behaviour_Lock(Product.CreditProtectionBehaviour_HO, E_txtCreditProtection, E_cboCreditProtectionBehaviour)

        E_txtAsuransiJaminan.Text = CStr(FormatNumber(Product.CreditProtection, 6))
        Hide_AsuransiJaminan.Value = E_txtAsuransiJaminan.Text
        Hide_AsuransiJaminan_Beh.Value = Product.AsuransiKredit_HO
        E_cboAsuransiJaminanBehaviour.SelectedIndex = E_cboAsuransiJaminanBehaviour.Items.IndexOf(E_cboAsuransiJaminanBehaviour.Items.FindByValue(Product.AsuransiKreditBehaviour_HO))
        Behaviour_Lock(Product.AsuransiKreditBehaviour_HO, E_txtAsuransiJaminan, E_cboAsuransiJaminanBehaviour)


        setBranchText()
    End Sub

    Sub setBranchText()
        lblEffectiveRate_Branch.Text = CStr(FormatNumber(Product.EffectiveRate_HO, 6)) & " % " & Parameter.Product.Behaviour_Value(Product.EffectiveRateBehaviour_HO)
        lblMinimumTenor_Branch.Text = CStr(Product.MinimumTenor_HO) & " months"
        lblMaximumTenor_Branch.Text = CStr(Product.MaximumTenor_HO) & " months"
        lblAngsuranPertama_Branch.Text = IIf(Product.AngsuranPertama_HO = "ARR", "ARREAR", "ADVANCE")
        lblFlatRate_Branch.Text = CStr(FormatNumber(Product.SukuBungaFlat_HO, 6)) & " % " & Parameter.Product.Behaviour_Value(Product.SukuBungaFlatBehaviour_HO)
        lblOpsiUangMuka_Branch.Text = IIf(Product.OpsiUangMuka_HO = "P", "PROSENTASE", "ANGSURAN")
        lblDPPercentage_Branch.Text = CStr(Product.DPPercentage_HO) & " " & Parameter.Product.Behaviour_Value(Product.DPPercentageBehaviour_HO)
        lblUangMukaAngsuran_Branch.Text = CStr(Product.OpsiUangMukaAngsur_HO) & " x"
        lblPOExpirationDays_Branch.Text = CStr(Product.DaysPOExpiration_HO) & " days " & Parameter.Product.Behaviour_Value(Product.DaysPOExpirationBehaviour_HO)
        lblAsuransiJaminan_Branch.Text = CStr(Product.AsuransiKredit) & " % " & Parameter.Product.Behaviour_Value(Product.AsuransiKreditBehaviour)
        lblCreditProtection_Branch.Text = CStr(Product.CreditProtection) & " % " & Parameter.Product.Behaviour_Value(Product.CreditProtectionBehaviour)
    End Sub
    Public Sub InitUCViewMode()
        InitialControls()
        lbltxtMinimumTenor.Text = String.Format("{0} months", FormatNumber(CStr(Product.MinimumTenor), 2))
        lblMaximumTenor.Text = String.Format("{0} months", FormatNumber(CStr(Product.MaximumTenor), 2))
        lblAngsuranPertama.Text = IIf(Product.AngsuranPertama = "ARR", "ARREAR", "ADVANCE")
        lblFlatRate.Text = String.Format("{0} % {1}", FormatNumber(Product.SukuBungaFlat, 6), Parameter.Product.Behaviour_Value(Product.SukuBungaFlatBehaviour))
        lblEffectiveRate.Text = String.Format("{0} % {1}", FormatNumber(Product.EffectiveRate, 6), Parameter.Product.Behaviour_Value(Product.EffectiveRateBehaviour))
        lblOpsiUangMuka.Text = IIf(Product.OpsiUangMuka = "P", "PROSENTASE", "ANGSURAN")
        lblDPPercentage.Text = String.Format("{0} % {1}", FormatNumber(Product.DPPercentage, 6), Parameter.Product.Behaviour_Value(Product.DPPercentageBehaviour))
        lblUangMukaAngsuran.Text = String.Format("{0} x", FormatNumber(Product.OpsiUangMukaAngsur, 6))
        lblPOExpirationDays.Text = String.Format("{0} days {1}", CStr(Product.DaysPOExpiration), Parameter.Product.Behaviour_Value(Product.DaysPOExpirationBehaviour))

        lblAsuransiJaminan.Text = String.Format("{0} % {1}", FormatNumber(Product.AsuransiKredit, 6), Parameter.Product.Behaviour_Value(Product.AsuransiKreditBehaviour_HO))
        lblCreditProtection.Text = String.Format("{0} % {1}", FormatNumber(Product.CreditProtection_HO, 6), Parameter.Product.Behaviour_Value(Product.CreditProtectionBehaviour_HO))
        setBranchText()
    End Sub
    Sub bindBehaviour(ddl As DropDownList)
        ddl.DataValueField = "Value"
        ddl.DataTextField = "Text"
        ddl.DataSource = Parameter.Product.BehaviourItems
        ddl.DataBind()
    End Sub
    Public Sub InitUCEditMode()
        bindBehaviour(E_cboFlatRateBehaviour)
        bindBehaviour(E_cboEffectiveRateBehaviour)
        bindBehaviour(E_cboDPPercentageBehaviour)
        bindBehaviour(E_cboPOExpirationDaysBehaviour)

        bindBehaviour(E_cboAsuransiJaminanBehaviour)
        bindBehaviour(E_cboCreditProtectionBehaviour)


        InitialControls()
    End Sub
    Public Sub CollectResult(oProduct As Parameter.Product)
        With oProduct
            .EffectiveRate = CDec(E_txtEffectiveRate.Text.Trim)
            .EffectiveRateBehaviour = E_cboEffectiveRateBehaviour.SelectedItem.Value.Trim
            .GrossYieldRate = 0
            .GrossYieldRateBehaviour = ""
            .MinimumTenor = CInt(E_txtMinimumTenor.Text.Trim)
            .MaximumTenor = CInt(E_txtMaximumTenor.Text.Trim)
            .DaysPOExpiration = CInt(E_txtPOExpirationDays.Text.Trim)
            .DaysPOExpirationBehaviour = E_cboPOExpirationDaysBehaviour.SelectedItem.Value.Trim
            .DPPercentage = CDec(E_txtDPPercentage.Text.Trim)
            .DPPercentageBehaviour = E_cboDPPercentageBehaviour.SelectedItem.Value.Trim

            .AngsuranPertama = E_cboAngsuranPertama.SelectedItem.Value.Trim()
            .SukuBungaFlat = CDec(E_txtFlatRate.Text)
            .SukuBungaFlatBehaviour = E_cboFlatRateBehaviour.SelectedItem.Value.Trim()

            .OpsiUangMuka = E_cboOpsiUangMuka.SelectedItem.Value.Trim()
            .OpsiUangMukaAngsur = CInt(txtUangMukaAngsuran.Text)

            .CreditProtection = CInt(E_txtCreditProtection.Text.Trim)
            .CreditProtectionBehaviour = E_cboCreditProtectionBehaviour.SelectedItem.Value.Trim()

            .AsuransiKredit = CInt(E_txtAsuransiJaminan.Text.Trim)
            .AsuransiKreditBehaviour = E_cboAsuransiJaminanBehaviour.SelectedItem.Value.Trim()

        End With
    End Sub
End Class