﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ProductOfferingBranch.aspx.vb" Inherits="Maxiloan.Webform.SalesMarketing.ProductOfferingBranch" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../../../Webform.UserController/ucDateCE.ascx" TagName="ucDateCE" TagPrefix="uc1" %>

<%@ Register Src="productSkemaTab.ascx" TagName="skema" TagPrefix="uc2" %>  
<%@ Register Src="productBiayaTab.ascx" TagName="biaya" TagPrefix="uc3" %>  
<%@ Register Src="productAngsuranTabe.ascx" TagName="angs" TagPrefix="uc4" %>  
<%@ Register Src="productCollectionTab.ascx" TagName="coll" TagPrefix="uc5" %>  
<%@ Register Src="productFinanceTab.ascx" TagName="fin" TagPrefix="uc6" %>  
<%@ Register Src="../../../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ProductOfferingBranch</title>
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../../Include/Buttons.css" type="text/css" />
    <script src="../../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script language="JavaScript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;

        function OpenWinProductView(pProductID, pStyle) {
            var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
            window.open('http://<%#Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.SalesMarketing/Setting/ProductMnt/ProductView.aspx?ProductID=' + pProductID + '&style=' + pStyle, 'Product', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=1');
        }

        function OpenWinProductBranchHOView(pProductID, pBranchID) {
            var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
            window.open('http://<%#Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.SalesMarketing/Setting/ProductMnt/ProductBranchHOView.aspx?ProductID=' + pProductID + '&BranchID=' + pBranchID, 'Product', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=1');
        }

        function OpenWinProductOfferingBranchView(pProductOfferingID, pProductID, pBranchID, pStyle) {
            var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
            window.open('http://<%#Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.SalesMarketing/Setting/ProductMnt/ProductOfferingBranchView.aspx?style=' + pStyle + '&ProductOfferingID=' + pProductOfferingID + '&ProductID=' + pProductID + '&BranchID=' + pBranchID, 'Product', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=1');
        }

        function OpenWinSupplier(pSupplierID, pAssetUsedNew, pBranchID) {
            var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
            window.open('http://<%#Request.servervariables("SERVER_NAME")%>/' + App + '/General/LookUpMultiSupplier.aspx?SupplierID=' + pSupplierID + '&AssetUsedNew=' + pAssetUsedNew + '&BranchID=' + pBranchID, 'UserLookup', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenWinAsset(pAssetCode, pBranchID, pAssetTypeID) {
            var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
            window.open('http://<%#Request.servervariables("SERVER_NAME")%>/' + App + '/General/LookUpMultiAssetCode.aspx?AssetCode=' + pAssetCode + '&BranchID=' + pBranchID + '&AssetTypeID=' + pAssetTypeID, 'UserLookup', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }

        function fBack() {
            history.back(-1);
            return false;
        }

        function fClose() {
            window.close();
            return false;
        }
        function DeleteConfirm() {
            if (confirm("Apakah yakin mau hapus data ini ?")) {
                return true;
            }
            else {
                return false;
            }
        }

        function toggleTrPriod(e) {
            if (e == 'ML') {
                $('#Tabs_pnlTabAngsuran_AngsuranTab_TR_Prioritas').hide();
            }
            else
            { $('#Tabs_pnlTabAngsuran_AngsuranTab_TR_Prioritas').show(); }  

        }

        $(document).ready(function () {
            toggleTrPriod($("#RadioDiv input:radio:checked").val());

            $('#RadioDiv input').click(function () {
                toggleTrPriod($("#RadioDiv input:radio:checked").val()); 
            });

        });  
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
   <%-- <asp:UpdatePanel runat="server" ID="updatePanel1">--%>
     <%--   <ContentTemplate>--%>
            <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>
                        DAFTAR PRODUK JUAL
                    </h3>
                </div>
            </div>
            <asp:Panel ID="pnlList" runat="server">
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtgPaging" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                                OnSortCommand="Sorting" BorderStyle="None" BorderWidth="0" DataKeyField="ProductOfferingID"
                                CssClass="grid_general">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="EDIT">
                                        <ItemStyle CssClass="command_col"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbEdit" runat="server" CausesValidation="False" ImageUrl="../../../Images/iconedit.gif"
                                                CommandName="EDIT"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="DELETE">
                                        <ItemStyle CssClass="command_col"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbDelete" runat="server" CausesValidation="False" ImageUrl="../../../Images/icondelete.gif"
                                                CommandName="DELETE"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="ID PROD JUAL" SortExpression="ProductOfferingID">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hynProductOfferingID" runat="server" Text='<%#container.dataitem("ProductOfferingID")%>'>
                                            </asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="NAMA PRODUK JUAL" SortExpression="Description">
                                        <ItemTemplate>
                                            <asp:Label ID="lblProductOfferingDescription" runat="server" Text='<%#container.dataitem("Description")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="MULAI TANGGAL" SortExpression="StartDate">
                                        <ItemTemplate>
                                            <asp:Label ID="lblStartDate" runat="server" Text='<%#format(container.dataitem("StartDate"),"dd/MM/yyyy")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="SAMPAI TANGGAL" SortExpression="EndDate">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEndDate" runat="server" Text='<%#format(container.dataitem("EndDate"),"dd/MM/yyyy")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                              <uc2:ucGridNav id="GridNavigator" runat="server"/>   
                            <%--<div class="button_gridnavigation">
                                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../../Images/grid_navbutton01.png"
                                    CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../../Images/grid_navbutton02.png"
                                    CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../../Images/grid_navbutton03.png"
                                    CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../../Images/grid_navbutton04.png"
                                    CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                                    EnableViewState="False"></asp:Button>
                                <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" MinimumValue="1"
                                    ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"></asp:RangeValidator>
                                <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                                    ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                            </div>
                            <div class="label_gridnavigation">
                                <asp:Label ID="lblPage" runat="server"></asp:Label>of
                                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                            </div>--%>
                        </div>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonAdd" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="ButtonPrint" runat="server" Enabled="true" Text="Print" CssClass="small button blue" style="display:none;">
                    </asp:Button>
                    <asp:Button ID="ButtonBack" runat="server" CausesValidation="False" Text="Back" CssClass="small button gray">
                    </asp:Button>
                </div>
                <div class="form_box_title">
                    <div class="form_single">
                        <h4>
                            CARI PRODUK JUAL
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            ID Produk</label>
                        <asp:HyperLink ID="hplProductID" runat="server"></asp:HyperLink>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Nama Produk</label>
                        <asp:Label ID="lblDescription" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Cabang</label>
                        <asp:HyperLink ID="hplBranch" runat="server"></asp:HyperLink>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Cari Berdasarkan</label>
                        <asp:DropDownList ID="cboSearchBy" runat="server">
                            <asp:ListItem Value="ProductOfferingID">ID PRODUK JUAL</asp:ListItem>
                            <asp:ListItem Value="Description">NAMA PRODUK JUAL</asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtSearchBy" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Periode
                        </label>
                        <uc1:ucdatece id="txtPeriodFrom" runat="server" />
                       <label class="label_auto">s/d</label> 
                        <uc1:ucdatece id="txtPeriodTo" runat="server" />
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonSearch" runat="server" CausesValidation="True" Text="Search"
                        CssClass="small button blue"></asp:Button>
                    <asp:Button ID="ButtonReset" runat="server" CausesValidation="False" Text="Reset"
                        CssClass="small button gray"></asp:Button>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlAddEdit" runat="server">
             <div class="form_single">
<asp:TabContainer runat="server" ID="Tabs" style="Height:auto;"  ActiveTabIndex="0"  Width="100%">
               
    <asp:TabPanel runat="server" ID="pnlTabUmum" HeaderText="UMUM"   >
    <ContentTemplate>
    <div>
        <div class="form_box_title">
            <div class="form_single">
                <h4> PRODUK JUAL - <asp:Label ID="lblAddEdit" runat="server"></asp:Label> </h4>
            </div>
            </div>
            <div class="form_box">
            <div class="form_single">
                <label class="label_req"> ID Produk Jual </label>
                <asp:Label ID="lblProductOfferingID" runat="server"></asp:Label>
                <asp:TextBox ID="txtProductOfferingID" runat="server" MaxLength="10"></asp:TextBox>
                <asp:Label ID="LProductOfferingID" runat="server"></asp:Label>
                <asp:RequiredFieldValidator ID="rgvProductOfferingID" runat="server" ControlToValidate="txtProductOfferingID"
                    CssClass="validator_general" ErrorMessage="Harap isi ID Produk Jual"></asp:RequiredFieldValidator>
            </div>
            </div>
            <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    Nama Produk Jual
                </label>
                <asp:TextBox ID="txtDescription" runat="server" Width="512px" MaxLength="100"></asp:TextBox>
                <asp:Label ID="LDescription" runat="server"></asp:Label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtDescription"
                    CssClass="validator_general" ErrorMessage="harap isi Nama Produk Jual"></asp:RequiredFieldValidator>
            </div>
            </div>
            <div class="form_box">
            <div class="form_single">
                <label>Paket Program</label>
                <asp:DropDownList ID="cboPaket" runat="server"></asp:DropDownList>
            </div>
            </div>
            <div class="form_box_header">
            <div class="form_single"> <h5> UMUM </h5> </div>
            </div>
            <div class="form_box">
            <div class="form_single">
                <label> Jenis Asset</label>
                <asp:Label ID="lblAssetType" runat="server"></asp:Label>
            </div>
            </div>
            <div class="form_box" style="display:none;">
            <div class="form_single">
                <label>  Skema Score - Marketing</label>
                <asp:Label ID="lblScoreSchemeMarketing" runat="server"></asp:Label>
            </div>
            </div>
            <div class="form_box">
            <div class="form_single">
                <label>Scoring Pembiayaan</label>
                <asp:Label ID="lblScoreSchemeCredit" runat="server"></asp:Label>
            </div>
            </div>
            <div class="form_box">
            <div class="form_single">
                <label> Skema Jurnal</label>
                <asp:Label ID="lblJournalScheme" runat="server"></asp:Label>
            </div>
            </div>
            <div class="form_box">
            <div class="form_single">
                <label> Skema Approval</label>
                <asp:Label ID="lblApprovalScheem" runat="server"></asp:Label>
            </div>
            </div>
            <div class="form_box">
            <div class="form_single">
                <label>
                    Kondisi Asset</label>
                <asp:Label ID="lblAssetUsedNew" runat="server"></asp:Label>
            </div>
            </div>
            <div class="form_box">
            <div class="form_single">
                <label> Umur Asset Yang Dibiayai</label>
                <asp:TextBox ID="txtUmurKendaraanFrom" runat="server" MaxLength="9">0</asp:TextBox>&nbsp;&nbsp;To&nbsp;&nbsp;
                <asp:TextBox ID="txtUmurKendaraanTo" runat="server" MaxLength="9">0</asp:TextBox>
            </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label> Kegiatan Usaha</label>
                    <asp:Label ID="lblKegiatanUsaha" runat="server" Visible="True"></asp:Label>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>Jenis Pembiayaan</label>
                    <asp:Label ID="lblJenisPembiayaan" runat="server" Visible="True"></asp:Label>
                 </div>
            </div>
    </div> 
     <div class="form_box">
     <div class="form_single">
                        <label class="label_req">
                            Periode</label>
                        <asp:TextBox ID="txtValidDateFrom" runat="server"></asp:TextBox>
                        <asp:CalendarExtender ID="txtValidDateFrom_CalendarExtender" runat="server" Enabled="True"
                            TargetControlID="txtValidDateFrom" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*"
                            CssClass="validator_general" ControlToValidate="txtValidDateFrom"></asp:RequiredFieldValidator>
                        sampai
                        <asp:TextBox ID="txtValidDateTo" runat="server"></asp:TextBox>
                        <asp:CalendarExtender ID="txtValidDateTo_CalendarExtender" runat="server" Enabled="True"
                            TargetControlID="txtValidDateTo" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="*"
                            CssClass="validator_general" ControlToValidate="txtValidDateTo"></asp:RequiredFieldValidator>
                    </div>
  </div>
    </ContentTemplate>
</asp:TabPanel>


    <asp:TabPanel runat="server" ID="pnlTabSkema" HeaderText="SKEMA"  >
    <ContentTemplate>
        <uc2:skema id="SkemaTab" runat="server"/>  
    </ContentTemplate>
</asp:TabPanel>


<asp:TabPanel runat="server" ID="pnlTabBiaya" HeaderText="BIAYA"  >
    <ContentTemplate>
      <uc3:biaya id="BiayaTab" runat="server"/>  
    </ContentTemplate>
</asp:TabPanel>

                    
    <asp:TabPanel runat="server" ID="pnlTabAngsuran" HeaderText="ANGSURAN"  >
    <ContentTemplate>
       <uc4:angs  id="AngsuranTab" runat="server"/> 
    </ContentTemplate>
    </asp:TabPanel>

                        
    <asp:TabPanel runat="server" ID="pnlTabCollection" HeaderText="COLLECTION"  >
    <ContentTemplate>
       <uc5:coll  id="CollactionTab" runat="server"/> 
        </ContentTemplate>
    </asp:TabPanel>
                         
    <asp:TabPanel runat="server" ID="pnlTabFinance" HeaderText="FINANCE"  >
        <ContentTemplate>
       <uc6:fin id="FinaceTab" runat="server"/>
        </ContentTemplate>
    </asp:TabPanel>
</asp:TabContainer>

 <div class="form_button">
                    <asp:Button ID="ButtonSave" runat="server" CausesValidation="true" CssClass="small button blue"
                        Text="Save"></asp:Button>
                    <asp:Button ID="ButtonCancel" runat="server" CausesValidation="false" CssClass="small button gray"
                        Text="Cancel"></asp:Button>
                </div>
            </asp:Panel>
    
    </form>
</body>
</html>
 