﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ProductBranch.aspx.vb"
    Inherits="Maxiloan.Webform.SalesMarketing.ProductBranch" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %> 
<%@ Register Src="ProductSkemaBRTab.ascx" TagName="skema" TagPrefix="uc2" %>  

<%@ Register Src="productBiayaBRTab.ascx" TagName="biaya" TagPrefix="uc3" %>  
<%@ Register Src="productAngsuranBRTab.ascx" TagName="angs" TagPrefix="uc4" %>  
 <%@ Register Src="ProductCollectionBRTab.ascx" TagName="coll" TagPrefix="uc5" %>   
<%@ Register Src="ProductFinanceBRTab.ascx" TagName="fin" TagPrefix="uc6" %>   
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ProductBranch</title>
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%#Request.servervariables("SERVER_NAME")%>/';			
    </script>
    <link rel="Stylesheet" href="../../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../../Include/Buttons.css" type="text/css" />
  
    <script src="../../../js/jquery-2.1.1.min.js" type="text/javascript"></script>
     <script src="../../../js/product.js" type="text/javascript"></script>
       <script src="../../../Maxiloan.js" type="text/javascript"></script>
    <script type="text/javascript" language="JavaScript">
        var x = screen.width;
        var y = screen.height - 100;
        function OpenWinProductBranchHOView(pProductID, pBranchID) {
            var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
            window.open('http://<%#Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.SalesMarketing/Setting/ProductMnt/ProductBranchHOView.aspx?ProductID=' + pProductID + '&BranchID=' + pBranchID, 'Product', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=1');
        }

        //EffectiveRate
        function EffectiveRate() {
            var oCombo = document.forms[0].Hide_EffectiveRateBeh;
            var otxt = document.forms[0].E_txtEffectiveRate;
            var otxt_GYR = document.forms[0].E_txtGrossYieldRate;
            var ohide = document.forms[0].Hide_EffectiveRate;

            if (parseFloat(document.forms[0].E_txtEffectiveRate.value) > parseFloat(document.forms[0].E_txtGrossYieldRate.value)) {
                alert('Suku Margin Effective harus <= Gross Yield Rate');
                otxt.value = ohide.value;
            }

            /*if (oCombo.value == "N"){					
            if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
            alert('You have to fill Effective Rate between ' + ohide.value + ' and 100');
            otxt.value = ohide.value;						
            }
            }
            else 
            {  
            if (oCombo.value == "X") {	
            if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
            alert('You have to fill Effective Rate between 1 and ' + ohide.value);
            otxt.value = ohide.value; }
            }															
            }
            */

        }
        //GrossYieldRate
        function GrossYieldRate() {
            var oCombo = document.forms[0].Hide_GrossYieldRateBeh;
            var otxt = document.forms[0].E_txtGrossYieldRate;
            var otxt_EF = document.forms[0].E_txtEffectiveRate;
            var ohide = document.forms[0].Hide_GrossYieldRate;

            /*if (oCombo.value == "N"){					
            if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
            alert('You have to fill Gross Yield Rate between ' + ohide.value + ' and 100');
            otxt.value = ohide.value;						
            }
            }
            else 
            {  
            if (oCombo.value == "X") {	
            if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
            alert('You have to fill Gross Yield Rate between 1 and ' + ohide.value);
            otxt.value = ohide.value; }
            }										
            }*/
            if (parseFloat(otxt.value) < parseFloat(otxt_EF.value)) {
                alert('Suku Margin Effective harus <= Gross Yield Rate');
                otxt.value = ohide.value;
            }

        }

        //Minimum Tenor
        function MinimumTenor() {
            var otxt = document.forms[0].E_txtMinimumTenor;
            var otxt_max = document.forms[0].E_txtMaximumTenor;
            var ohide = document.forms[0].Hide_MinimumTenor;

            /*
            if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
            alert('You have to fill Minimum Tenor between ' + ohide.value + ' and 999');
            otxt.value = ohide.value;						
            }	
            */
            if (parseFloat(otxt.value) > parseFloat(otxt_max.value)) {
                alert('Jangka Waktu Minimum harus <= Jangka Waktu Maximum');
                otxt.value = ohide.value;
            }

        }

        //Maximum Tenor
        function MaximumTenor() {
            var otxt_min = document.forms[0].E_txtMinimumTenor;
            var otxt = document.forms[0].E_txtMaximumTenor;
            var ohide = document.forms[0].Hide_MaximumTenor;

            /*	
            if (parseFloat(otxt.value) > parseFloat(ohide.value)) 
            {
            alert('You have to fill Maximum Tenor between 0 and ' + ohide.value);
            otxt.value = ohide.value;						
            }		
            */

            if (parseFloat(otxt.value) < parseFloat(otxt_min.value)) {
                alert('Jangka Waktu Minimum harus <= Jangka waktu Maximum');
                otxt.value = ohide.value;
            }
        }


        // Late Charges Percentage (Installment)
        function PenaltyPercentage() {
            var oCombo = document.forms[0].Hide_PenaltyPercentageBeh;
            var otxt = document.forms[0].E_txtPenaltyPercentage;
            var ohide = document.forms[0].Hide_PenaltyPercentage;

            if (oCombo.value == "N") {
                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
                    alert('Harap isi Prosentase Ta widh keterlambatan angsuran antara ' + ohide.value + ' dan 100');
                    otxt.value = ohide.value;
                }
            }
            else {
                if (oCombo.value == "X") {
                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
                        alert('Harap isi Prosentase Ta widh keterlambatan angsuran antara 0 dan ' + ohide.value);
                        otxt.value = ohide.value;
                    }
                }
            }
        }


        // Late Charges Percentage (Insurance)
        function InsurancePenaltyPercentage() {
            var oCombo = document.forms[0].Hide_InsurancePenaltyPercentageBeh;
            var otxt = document.forms[0].E_txtInsurancePenaltyPercentage;
            var ohide = document.forms[0].Hide_InsurancePenaltyPercentage;

            if (oCombo.value == "N") {
                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
                    alert('Harap isi Prosentase Ta widh keterlambatan Asuransi antara ' + ohide.value + ' dan 100');
                    otxt.value = ohide.value;
                }
            }
            else {
                if (oCombo.value == "X") {
                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
                        alert('Harap isi Prosentase Ta widh keterlambatan Asuransi antara 0 dan ' + ohide.value);
                        otxt.value = ohide.value;
                    }
                }
            }
        }


        // Cancellation Fee
//        function CancellationFee() {
//            var oCombo = document.forms[0].Hide_CancellationFeeBeh;
//            var otxt = document.forms[0].E_txtCancellationFee;
//            var ohide = document.forms[0].Hide_CancellationFee;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Biaya Pembatalan antara  ' + ohide.value + ' dan 9999999999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Biaya Pembatalan antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }


//        // Admin Fee
//        function AdminFee() {
//            var oCombo = document.forms[0].Hide_AdminFeeBeh;
//            var otxt = document.forms[0].E_txtAdminFee;
//            var ohide = document.forms[0].Hide_AdminFee;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Biaya Administrasi antara ' + ohide.value + ' dan 9999999999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Biaya Administrasi antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }


//        // Fiducia Fee
//        function FiduciaFee() {
//            var oCombo = document.forms[0].Hide_FiduciaFeeBeh;
//            var otxt = document.forms[0].E_txtFiduciaFee;
//            var ohide = document.forms[0].Hide_FiduciaFee;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Biaya Fidusia antara ' + ohide.value + ' dan 9999999999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Biaya Fidusia antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }


        // Provision Fee
//        function ProvisionFee() {
//            var oCombo = document.forms[0].Hide_ProvisionFeeBeh;
//            var otxt = document.forms[0].E_txtProvisionFee;
//            var ohide = document.forms[0].Hide_ProvisionFee;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Biaya Provisi antara ' + ohide.value + ' dan 9999999999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Biaya Provisi antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }

//        // Notary Fee
//        function NotaryFee() {
//            var oCombo = document.forms[0].Hide_NotaryFeeBeh;
//            var otxt = document.forms[0].E_txtNotaryFee;
//            var ohide = document.forms[0].Hide_NotaryFee;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Biaya Notaris antara ' + ohide.value + ' dan 9999999999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Biaya Notaris antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }

//        // Survey Fee
//        function SurveyFee() {
//            var oCombo = document.forms[0].Hide_SurveyFeeBeh;
//            var otxt = document.forms[0].E_txtSurveyFee;
//            var ohide = document.forms[0].Hide_SurveyFee;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Biaya Survey antara ' + ohide.value + ' dan 9999999999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Biaya Survey antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }

//        // Visit Fee
//        function VisitFee() {
//            var oCombo = document.forms[0].Hide_VisitFeeBeh;
//            var otxt = document.forms[0].E_txtVisitFee;
//            var ohide = document.forms[0].Hide_VisitFee;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Biaya Kunjungan antara ' + ohide.value + ' dan 9999999999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Biaya Kunjungan antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }

//        // Rescheduling Fee
//        function ReschedulingFee() {
//            var oCombo = document.forms[0].Hide_ReschedulingFeeBeh;
//            var otxt = document.forms[0].E_txtReschedulingFee;
//            var ohide = document.forms[0].Hide_ReschedulingFee;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Biaya Rescheduling antara ' + ohide.value + ' dan 9999999999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Biaya Rescheduling antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }

//        // AgreementTransferFee
//        function AgreementTransferFee() {
//            var oCombo = document.forms[0].Hide_AgreementTransferFeeBeh;
//            var otxt = document.forms[0].E_txtAgreementTransferFee;
//            var ohide = document.forms[0].Hide_AgreementTransferFee;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Biaya OverKontrak antara ' + ohide.value + ' dan 9999999999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Biaya OverKontrak antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }

//        // ChangeDueDateFee
//        function ChangeDueDateFee() {
//            var oCombo = document.forms[0].Hide_ChangeDueDateFeeBeh;
//            var otxt = document.forms[0].E_txtChangeDueDateFee;
//            var ohide = document.forms[0].Hide_ChangeDueDateFee;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Biaya Ganti Tanggal jatuh Tempo antara ' + ohide.value + ' dan 9999999999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Biaya Ganti Tanggal Jatuh Tempo antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }

//        // AssetReplacementFee
//        function AssetReplacementFee() {
//            var oCombo = document.forms[0].Hide_AssetReplacementFeeBeh;
//            var otxt = document.forms[0].E_txtAssetReplacementFee;
//            var ohide = document.forms[0].Hide_AssetReplacementFee;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Biaya Ganti Asset antara ' + ohide.value + ' dan 9999999999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Biaya ganti Asset antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }

//        // RepossesFee
//        function RepossesFee() {
//            var oCombo = document.forms[0].Hide_RepossesFeeBeh;
//            var otxt = document.forms[0].E_txtRepossesFee;
//            var ohide = document.forms[0].Hide_RepossesFee;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Biaya Tarik Asset antara ' + ohide.value + ' dan 9999999999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Biaya Tarik Asset antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }

//        // LegalizedDocumentFee
//        function LegalizedDocumentFee() {
//            var oCombo = document.forms[0].Hide_LegalizedDocumentFeeBeh;
//            var otxt = document.forms[0].E_LegalizedDocumentFee;
//            var ohide = document.forms[0].Hide_LegalizedDocumentFee;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Biaya Legalisasi Dokumen antara ' + ohide.value + ' dan 9999999999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Biaya Legalisasi Dokumen antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }

//        // PDCBounceFee
//        function PDCBounceFee() {
//            var oCombo = document.forms[0].Hide_PDCBounceFeeBeh;
//            var otxt = document.forms[0].E_PDCBounceFee;
//            var ohide = document.forms[0].Hide_PDCBounceFee;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Biaya Penolakan PDC antara ' + ohide.value + ' dan 9999999999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Biaya Penolakan PDC antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }


//        // InsuranceAdminFee
//        function InsuranceAdminFee() {
//            var oCombo = document.forms[0].Hide_InsuranceAdminFeeBeh;
//            var otxt = document.forms[0].E_txtInsuranceAdminFee;
//            var ohide = document.forms[0].Hide_InsuranceAdminFee;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Biaya Admin Asuransi antara ' + ohide.value + ' dan 9999999999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Biaya Admin Asuransi antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }

//        // InsuranceStampDutyFee
//        function InsuranceStampDutyFee() {
//            var oCombo = document.forms[0].Hide_InsuranceStampDutyFeeBeh;
//            var otxt = document.forms[0].E_txtInsuranceStampDutyFee;
//            var ohide = document.forms[0].Hide_InsuranceStampDutyFee;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Biaya Materai Asuransi antara ' + ohide.value + ' dan 9999999999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Biaya Materai Asuransi antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }

        // POExpirationDays
        function POExpirationDays() {
            var oCombo = document.forms[0].Hide_POExpirationDaysBeh;
            var otxt = document.forms[0].E_txtPOExpirationDays;
            var ohide = document.forms[0].Hide_POExpirationDays;

            if (oCombo.value == "N") {
                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
                    alert('Harap isi Masa Berlaku PO antara ' + ohide.value + ' dan 9999');
                    otxt.value = ohide.value;
                }
            }
            else {
                if (oCombo.value == "X") {
                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
                        alert('Harap isi Masa Berlaku PO antara 0 dan ' + ohide.value);
                        otxt.value = ohide.value;
                    }
                }
            }
        }


        // InstallmentToleranceAmount
//        function InstallmentToleranceAmount() {
//            var oCombo = document.forms[0].Hide_InstallmentToleranceAmountBeh;
//            var otxt = document.forms[0].E_txtInstallmentToleranceAmount;
//            var ohide = document.forms[0].Hide_InstallmentToleranceAmount;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Jumlah Toleransi terhadap Angsuran antara ' + ohide.value + ' dan 999999999999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Jumlah Toleransi terhadap Angsuran antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }

        // DPPercentage
        function DPPercentage() {
            var oCombo = document.forms[0].Hide_DPPercentageBeh;
            var otxt = document.forms[0].E_txtDPPercentage;
            var ohide = document.forms[0].Hide_DPPercentage;

            if (oCombo.value == "N") {
                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
                    alert('Harap isi Prosentase Minimum DP antara ' + ohide.value + ' dan 100');
                    otxt.value = ohide.value;
                }
            }
            else {
                if (oCombo.value == "X") {
                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
                        alert('Harap isi Prosentase Minimum DP antara 0 dan ' + ohide.value);
                        otxt.value = ohide.value;
                    }
                }
            }
        }

        // RejectMinimumIncome
        function RejectMinimumIncome() {
            var oCombo = document.forms[0].Hide_RejectMinimumIncomeBeh;
            var otxt = document.forms[0].E_txtRejectMinimumIncome;
            var ohide = document.forms[0].Hide_RejectMinimumIncome;

            if (oCombo.value == "N") {
                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
                    alert('Harap isi Pendapatan Margin Minimum yg harus ditolak antara ' + ohide.value + ' dan 999999999999');
                    otxt.value = ohide.value;
                }
            }
            else {
                if (oCombo.value == "X") {
                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
                        alert('Harap isi Pendapatan Margin Minimum yg harus ditolak antara 0 dan ' + ohide.value);
                        otxt.value = ohide.value;
                    }
                }
            }
        }

        // WarningMinimumIncome
        function WarningMinimumIncome() {
            var oCombo = document.forms[0].Hide_WarningMinimumIncomeBeh;
            var otxt = document.forms[0].E_txtWarningMinimumIncome;
            var ohide = document.forms[0].Hide_WarningMinimumIncome;

            if (oCombo.value == "N") {
                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
                    alert('Harap isi Pendapatan Margin Minimum yg harus diingatkann antara ' + ohide.value + ' dan 999999999999');
                    otxt.value = ohide.value;
                }
            }
            else {
                if (oCombo.value == "X") {
                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
                        alert('Harap isi Pendapatan Margin Minimum yg harus diingatkann antara 0 dan ' + ohide.value);
                        otxt.value = ohide.value;
                    }
                }
            }
        }


//        // Length Main Document Processed
//        function LengthMainDocProcess() {
//            var oCombo = document.forms[0].Hide_LengthMainDocProcessBeh;
//            var otxt = document.forms[0].E_txtLengthMainDocProcess;
//            var ohide = document.forms[0].Hide_LengthMainDocProcess;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Jangka Waktu proses Dokumen Utama  antara ' + ohide.value + ' dan 9999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Jangka Waktu proses Dokumen Utama  antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }

        // Length Main Document Taken
//        function LengthMainDocTaken() {
//            var oCombo = document.forms[0].Hide_LengthMainDocTakenBeh;
//            var otxt = document.forms[0].E_txtLengthMainDocTaken;
//            var ohide = document.forms[0].Hide_LengthMainDocTaken;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Jangka Waktu Dokumen Utama diambil antara ' + ohide.value + ' dan 9999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Jangka Waktu Dokumen Utama diambil antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }

//        // Grace Period Late Charges
//        function GracePeriodLateCharges() {
//            var oCombo = document.forms[0].Hide_GracePeriodLateChargesBeh;
//            var otxt = document.forms[0].E_txtGracePeriodLateCharges;
//            var ohide = document.forms[0].Hide_GracePeriodLateCharges;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Grace Period Denda Keterlambatan antara ' + ohide.value + ' dan 9999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Grace Period Denda Keterlambatan antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }


        // Prepayment Penalty Rate
//        function PrepaymentPenaltyRate() {
//            var oCombo = document.forms[0].Hide_PrepaymentPenaltyRateBeh;
//            var otxt = document.forms[0].E_txtPrepaymentPenaltyRate;
//            var ohide = document.forms[0].Hide_PrepaymentPenaltyRate;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Denda Pelunasan dipercepat antara ' + ohide.value + ' dan 100');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Denda Pelunasan dipercepat antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }

//        // Days to remind Installment
//        function DeskCollPhoneRemind() {
//            var oCombo = document.forms[0].Hide_DeskCollPhoneRemindBeh;
//            var otxt = document.forms[0].E_txtDeskCollPhoneRemind;
//            var ohide = document.forms[0].Hide_DeskCollPhoneRemind;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Jumlah hari mengingatkan angsuran antara ' + ohide.value + ' dan 9999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Jumlah hari mengingatkan angsuran antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }


        // Days overdue to call
//        function DeskCollOD() {
//            var oCombo = document.forms[0].Hide_DeskCollODBeh;
//            var otxt = document.forms[0].E_txtDeskCollOD;
//            var ohide = document.forms[0].Hide_DeskCollOD;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Jumlah hari Overdue untuk Deskcoll antara ' + ohide.value + ' dan 9999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Jumlah hari Overdue untuk Deskcoll antara 0 and ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }


        // Previous overdue to remind
//        function PrevODToRemind() {
//            var oCombo = document.forms[0].Hide_PrevODToRemindBeh;
//            var otxt = document.forms[0].E_txtPrevODToRemind;
//            var ohide = document.forms[0].Hide_PrevODToRemind;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Overdue sebelumnya untuk mengingatkan antara ' + ohide.value + ' dan 9999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Overdue sebelumnya untuk mengingatkan antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }

        // PDC request to call
//        function PDCDaysToRemind() {
//            var oCombo = document.forms[0].Hide_PDCDaysToRemindBeh;
//            var otxt = document.forms[0].E_txtPDCDaysToRemind;
//            var ohide = document.forms[0].Hide_PDCDaysToRemind;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Permintaan PDC untuk telepon antara ' + ohide.value + ' dan 9999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Permintaan PDC untuk telepon antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }


        // Days to remind by SMS
//        function DeskCollSMSRemind() {
//            var oCombo = document.forms[0].Hide_DeskCollSMSRemindBeh;
//            var otxt = document.forms[0].E_txtDeskCollSMSRemind;
//            var ohide = document.forms[0].Hide_DeskCollSMSRemind;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Jumlah hari mengingatkan dgn SMS antara ' + ohide.value + ' dan 9999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Jumlah hari mengingatkan dgn SMS antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }

//        // Days to generate DCR
//        function DCR() {
//            var oCombo = document.forms[0].Hide_DCRBeh;
//            var otxt = document.forms[0].E_txtDCR;
//            var ohide = document.forms[0].Hide_DCR;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Jumlah hari buat DCR antara ' + ohide.value + ' dan 9999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Jumlah hari buat DCR antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }

//        // Days before due to generate Receipt Notes
//        function DaysBeforeDueToRN() {
//            var oCombo = document.forms[0].Hide_DaysBeforeDueToRNBeh;
//            var otxt = document.forms[0].E_txtDaysBeforeDueToRN;
//            var ohide = document.forms[0].Hide_DaysBeforeDueToRN;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Jumlah hari sebelum JT buat Kwitansi antara ' + ohide.value + ' dan 9999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Jumlah hari sebelum JT buat Kwitansi antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }

        // Days to generate RAL
//        function ODToRAL() {
//            var oCombo = document.forms[0].Hide_ODToRALBeh;
//            var otxt = document.forms[0].E_txtODToRAL;
//            var ohide = document.forms[0].Hide_ODToRAL;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Jumlah hari buat SKT antara ' + ohide.value + ' dan 9999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Jumlah hari buat SKT antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }

//        // RAL Period
//        function RALPeriod() {
//            var oCombo = document.forms[0].Hide_RALPeriodBeh;
//            var otxt = document.forms[0].E_txtRALPeriod;
//            var ohide = document.forms[0].Hide_RALPeriod;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Jangka Waktu SKT antara ' + ohide.value + ' dan 9999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Jangka Waktu SKT antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }

//        // Maximum days Receipt notes paid
//        function MaxDaysRNPaid() {
//            var oCombo = document.forms[0].Hide_MaxDaysRNPaidBeh;
//            var otxt = document.forms[0].E_txtMaxDaysRNPaid;
//            var ohide = document.forms[0].Hide_MaxDaysRNPaid;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Jumlah hari Maximum Kwitansi dibayar antara ' + ohide.value + ' dan 9999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Jumlah hari Maximum Kwitansi dibayar antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }

//        // Maximum Promise To Pay days
//        function MaxPTPYDays() {
//            var oCombo = document.forms[0].Hide_MaxPTPYDaysBeh;
//            var otxt = document.forms[0].E_txtMaxPTPYDays;
//            var ohide = document.forms[0].Hide_MaxPTPYDays;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Jumlah hari Maximum janji Bayar antara ' + ohide.value + ' dan 9999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Jumlah hari Maximum janji Bayar antara  0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }


//        // Promise To Pay To Bank
//        function PTPYBank() {
//            var oCombo = document.forms[0].Hide_PTPYBankBeh;
//            var otxt = document.forms[0].E_txtPTPYBank;
//            var ohide = document.forms[0].Hide_PTPYBank;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Janji Bayar ke Bank antara ' + ohide.value + ' dan 9999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Janji Bayar ke Bank antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }


//        // Promise To Pay To Company
//        function PTPYCompany() {
//            var oCombo = document.forms[0].Hide_PTPYCompanyBeh;
//            var otxt = document.forms[0].E_txtPTPYCompany;
//            var ohide = document.forms[0].Hide_PTPYCompany;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Janji bayar ke Perusahaan antara ' + ohide.value + ' dan 9999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Janji bayar ke Perusahaan antara  0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }

//        // Promise To Pay To Supplier
//        function PTPYSupplier() {
//            var oCombo = document.forms[0].Hide_PTPYSupplierBeh;
//            var otxt = document.forms[0].E_txtPTPYSupplier;
//            var ohide = document.forms[0].Hide_PTPYSupplier;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Janji bayar ke Supplier antara ' + ohide.value + ' dan 9999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Janji bayar ke Supplier antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }

//        // RAL Extension
//        function RALExtension() {
//            var oCombo = document.forms[0].Hide_RALExtensionBeh;
//            var otxt = document.forms[0].E_txtRALExtension;
//            var ohide = document.forms[0].Hide_RALExtension;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Perpanjangan SKT antara ' + ohide.value + ' dan 9999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Perpanjangan SKT antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }

//        // Inventory Expected
//        function InventoryExpected() {
//            var oCombo = document.forms[0].Hide_InventoryExpectedBeh;
//            var otxt = document.forms[0].E_txtInventoryExpected;
//            var ohide = document.forms[0].Hide_InventoryExpected;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Jangka Waktu jadi Inventory antara ' + ohide.value + ' dan 9999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Jangka Waktu jadi Inventory antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }


//        // Billing Charges
//        function BillingCharges() {
//            var oCombo = document.forms[0].Hide_BillingChargesBeh;
//            var otxt = document.forms[0].E_txtBillingCharges;
//            var ohide = document.forms[0].Hide_BillingCharges;

//            if (oCombo.value == "N") {
//                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
//                    alert('Harap isi Biaya Tagih antara ' + ohide.value + ' dan 999999999999');
//                    otxt.value = ohide.value;
//                }
//            }
//            else {
//                if (oCombo.value == "X") {
//                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
//                        alert('Harap isi Biaya Tagih antara 0 dan ' + ohide.value);
//                        otxt.value = ohide.value;
//                    }
//                }
//            }
//        }


        // Limit A/P Disbursement at Branch
        function LimitAPCash() {
            var oCombo = document.forms[0].Hide_LimitAPCashBeh;
            var otxt = document.forms[0].E_txtLimitAPCash;
            var ohide = document.forms[0].Hide_LimitAPCash;

            if (oCombo.value == "N") {
                if (parseFloat(otxt.value) < parseFloat(ohide.value)) {
                    alert('Harap isi Limit Pembayaran di Cabang antara ' + ohide.value + ' dan 999999999999');
                    otxt.value = ohide.value;
                }
            }
            else {
                if (oCombo.value == "X") {
                    if (parseFloat(otxt.value) > parseFloat(ohide.value)) {
                        alert('Harap isi Limit Pembayaran di Cabang antara 0 dan ' + ohide.value);
                        otxt.value = ohide.value;
                    }
                }
            }
        }

        //=====================================	

        function cboIsSpAutomaticChange() {
            var oCombo = document.forms[0].E_cboIsSPAutomatic;
            var otxt = document.forms[0].E_txtLengthSPProcess;

            if (oCombo.options[oCombo.selectedIndex].value == "0") {
                otxt.disabled = true;
                otxt.value = "0";
            }
            else {
                otxt.disabled = false;
                otxt.value = "0";
            }
        }

        function cboIsSp1AutomaticChange() {
            var oCombo = document.forms[0].E_cboIsSP1Automatic;
            var otxt = document.forms[0].E_txtLengthSP1Process;

            if (oCombo.options[oCombo.selectedIndex].value == "0") {
                otxt.disabled = true;
                otxt.value = "0";
            }
            else {
                otxt.disabled = false;
                otxt.value = "0";
            }
        }

        function cboIsSp2AutomaticChange() {
            var oCombo = document.forms[0].E_cboIsSP2Automatic;
            var otxt = document.forms[0].E_txtLengthSP2Process;

            if (oCombo.options[oCombo.selectedIndex].value == "0") {
                otxt.disabled = true;
                otxt.value = "0";
            }
            else {
                otxt.disabled = false;
                otxt.value = "0";
            }
        }
				
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <%--<asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>--%>
            <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>
                        DAFTAR PRODUK CABANG
                    </h3>
                </div>
            </div>
            <asp:Panel ID="pnlList" runat="server">
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtgPaging" runat="server" Width="100%" DataKeyField="ProductID"
                                BorderStyle="None" BorderWidth="0" OnSortCommand="Sorting" AutoGenerateColumns="False"
                                AllowSorting="True" CssClass="grid_general">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="EDIT">
                                        <ItemStyle CssClass="command_col"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbEdit" runat="server" CausesValidation="False" ImageUrl="../../../Images/iconedit.gif"
                                                CommandName="EDIT"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="ID PRODUK" SortExpression="ProductID">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hynProductID" runat="server" Text='<%#container.dataitem("ProductID")%>'
                                                NavigateUrl="">
                                            </asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="NAMA PRODUK" SortExpression="Description">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDescription" runat="server" Text='<%#container.dataitem("Description")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="AKTIF" SortExpression="IsActive">
                                        <ItemTemplate>
                                            <asp:Label ID="lblIsActive" runat="server" Text='<%#container.dataitem("IsActive")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PERSYARATAN Pembiayaan">
                                        <ItemTemplate>
                                            <a href='ProductBranchTC.aspx?ProductID=<%# DataBinder.eval(Container,"DataItem.ProductID") %>&Description=<%# DataBinder.eval(Container,"DataItem.Description") %>&IsActive=<%# DataBinder.eval(Container,"DataItem.IsActive") %>&BranchFullName=<%# DataBinder.eval(Container,"DataItem.BranchFullName") %>'>
                                                <asp:Label ID="lblLinkProductTC" Text='PERSYARATAN PEMBIAYAAN' runat="server"></asp:Label>
                                            </a>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PRODUK JUAL">
                                        <ItemTemplate>
                                            <a href='ProductOfferingBranch.aspx?ProductID=<%# DataBinder.eval(Container,"DataItem.ProductID") %>&Description=<%# DataBinder.eval(Container,"DataItem.Description") %>&IsActive=<%# DataBinder.eval(Container,"DataItem.IsActive")%>&BranchFullName=<%# DataBinder.eval(Container,"DataItem.BranchFullName") %>'>
                                                <asp:Label ID="lblLinkProductOfferingBranch" Text='PRODUK JUAL' runat="server"></asp:Label>
                                            </a>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                            <div class="button_gridnavigation">
                                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../../Images/grid_navbutton01.png"
                                    CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../../Images/grid_navbutton02.png"
                                    CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../../Images/grid_navbutton03.png"
                                    CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../../Images/grid_navbutton04.png"
                                    CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                                    EnableViewState="False"></asp:Button>
                                <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" MinimumValue="1"
                                    ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"></asp:RangeValidator>
                                <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                                    ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                            </div>
                            <div class="label_gridnavigation">
                                <asp:Label ID="lblPage" runat="server"></asp:Label>of
                                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form_button" STYLE="display:none;">
                    <asp:Button ID="ButtonPrint" runat="server" Enabled="true" Text="Print" CssClass="small button blue">
                    </asp:Button>
                </div>
                <div class="form_box_title">
                    <div class="form_single">
                        <h4>
                            CARI PRODUK CABANG
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Cari Berdasarkan</label>
                        <asp:DropDownList ID="cboSearchBy" runat="server">
                            <asp:ListItem Value="ProductID">ID Produk</asp:ListItem>
                            <asp:ListItem Value="Description">Nama Produk</asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtSearchBy" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonSearch" runat="server" CausesValidation="False" Text="Search"
                        CssClass="small button blue"></asp:Button>&nbsp;
                    <asp:Button ID="ButtonReset" runat="server" CausesValidation="False" Text="Reset"
                        CssClass="small button gray"></asp:Button>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlEdit" runat="server">
             
                <div class="form_single">
            <asp:TabContainer runat="server" ID="Tabs" style="Height:auto;"  ActiveTabIndex="0"  Width="100%">
               
                     <asp:TabPanel runat="server" ID="pnlTabUmum" HeaderText="UMUM"   >
                        <ContentTemplate>  
                <div class="form_box_title">
                    <div class="form_single">
                        <h4>
                            PRODUK CABANG - EDIT
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Cabang</label>
                        <asp:Label ID="E_lblBranchID" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            ID Produk</label>
                        <asp:Label ID="E_lblProductID" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Nama Produk</label>
                        <asp:Label ID="E_lblDescription" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box_title">
                    <div class="form_single">
                        <h4>
                            UMUM
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Jenis Asset</label>
                        <asp:Label ID="E_lblAssetType" runat="server" Visible="True"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Skema Score - Marketing</label>
                        <asp:Label ID="E_lblScoreSchemeID" runat="server" Visible="True"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Skema Score - Pembiayaan</label>
                        <asp:Label ID="E_lblCreditScoreSchemeID" runat="server" Visible="True"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Skema Jurnal</label>
                        <asp:Label ID="E_lblJournalSchemeID" runat="server" Visible="True"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Skema Approval</label>
                        <asp:Label ID="E_lblApprovalSchemeID" runat="server" Visible="True"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                        Kondisi Asset </label>
                        <asp:Label ID="E_lblAssetUsedNew" runat="server" Visible="True"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Umur Asset Yang Dibiayai</label>
                        <asp:TextBox ID="txtUmurKendaraanFrom" runat="server" MaxLength="9">0</asp:TextBox>&nbsp;&nbsp;To&nbsp;&nbsp;
                        <asp:TextBox ID="txtUmurKendaraanTo" runat="server" MaxLength="9">0</asp:TextBox>
                    </div>
                </div>
                  <div class="form_box">
                    <div class="form_single">
                        <label> Kegiatan Usaha</label>
                        <asp:Label ID="lblKegiatanUsaha" runat="server" Visible="True"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>Jenis Pembiayaan</label>
                        <asp:Label ID="lblJenisPembiayaan" runat="server" Visible="True"></asp:Label>
                     </div>
                </div>

                         </ContentTemplate> 
                         </asp:TabPanel>
                 
                       <asp:TabPanel runat="server" ID="pnlTabSkema" HeaderText="SKEMA"  >
                        <ContentTemplate>
                            
                          <uc2:skema id="SkemaTab" runat="server"/>   
                        </ContentTemplate>
                        </asp:TabPanel>

                        
                       <asp:TabPanel runat="server" ID="pnlTabBiaya" HeaderText="BIAYA"  >
                        <ContentTemplate>
                      <uc3:biaya id="BiayaTab" runat="server"/>   
                     
                        </ContentTemplate>
                        </asp:TabPanel>

                       <asp:TabPanel runat="server" ID="pnlTabAngsuran" HeaderText="ANGSURAN"  >
                        <ContentTemplate>
                         <uc4:angs  id="AngsuranTab" runat="server"/>  
                    
                        </ContentTemplate>
                        </asp:TabPanel>

                        
                       <asp:TabPanel runat="server" ID="pnlTabCollection" HeaderText="COLLECTION"  >
                        <ContentTemplate>
                        <uc5:coll  id="CollactionTab" runat="server"/> 
                        
                         </ContentTemplate>
                         </asp:TabPanel>
                       <asp:TabPanel runat="server" ID="pnlTabFinance" HeaderText="FINANCE"  >
                        <ContentTemplate>
                          <uc6:fin id="FinaceTab" runat="server"/>   

                         </ContentTemplate>
                        </asp:TabPanel>
                     </asp:TabContainer>
                 
                 </div>  
                 <div class="form_button">
                    <asp:Button ID="ButtonSave" runat="server" Text="Save" CssClass="small button blue"
                        CausesValidation="true"></asp:Button>
                    <asp:Button ID="ButtonCancel" runat="server" Text="Cancel" CssClass="small button gray"
                        CausesValidation="false"></asp:Button>
                </div>
            </asp:Panel>
        <%--</ContentTemplate>
    </asp:UpdatePanel>--%>
    </form>
</body>
</html>
