﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ProductUmumTab.ascx.vb" Inherits="Maxiloan.Webform.SalesMarketing.ProductUmumTab" %>

 
<div>
   
   
   <%-- <div class="form_box">
        <div class="form_single"> 
            <label> ID Produk&nbsp;</label>
            <asp:Label ID="lblProductID" runat="server"></asp:Label>
            <asp:TextBox ID="txtProductID" runat="server"  MaxLength="10"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rgvProductID" runat="server" ControlToValidate="txtProductID" CssClass="validator_general" ErrorMessage="Harap isi dengan ID Produk"></asp:RequiredFieldValidator> 
        </div>
    </div>
    
    <div class="form_box"> 
        <div class="form_single">
             <label> Nama Produk&nbsp;</label> 
            <asp:Label ID="LDescription" runat="server"></asp:Label> 
             <asp:TextBox ID="txtDescription" runat="server"  Width="480px" MaxLength="100"></asp:TextBox>
              <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtDescription" CssClass="validator_general" ErrorMessage="Harap isi Nama Produk"></asp:RequiredFieldValidator>
        </div>
    </div> --%>
  
    <div class="form_box_title">
    <div class="form_single"> <h4> UMUM </h4> </div> </div>
    <div class="form_box">
        <div class="form_single">
            <label> Jenis Asset&nbsp;</label>  
            <asp:Label ID="LAssetType" runat="server"></asp:Label> 
             <asp:DropDownList ID="cboAssetType" runat="server" />
             <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" ControlToValidate="cboAssetType"
                            CssClass="validator_general" ErrorMessage="Harap pilih Jenis Asset" Display="Dynamic"
                            InitialValue="SelectOne"></asp:RequiredFieldValidator>
            <asp:Label ID="lblAssetTypeID" runat="server" Visible="False"></asp:Label>
        </div>
    </div>

    <div class="form_box">
    <div class="form_single">
        <label>Scoring&nbsp;</label> 
        <asp:Label ID="LScoreSchemeCredit" runat="server"></asp:Label>  
         <asp:DropDownList ID="cboScoreSchemeCredit" runat="server" /> 
         <asp:RequiredFieldValidator ID="Requiredfieldvalidator4" runat="server" ControlToValidate="cboScoreSchemeCredit"
                            CssClass="validator_general" ErrorMessage="Harap pilih Skema Score - Credit"
                            InitialValue="SelectOne"></asp:RequiredFieldValidator>
    </div>
    </div>

    <div class="form_box">
        <div class="form_single">
            <label> Skema Jurnal&nbsp;</label> 
            <asp:Label ID="LJournalScheme" runat="server"></asp:Label>
            <asp:DropDownList ID="cboJournalScheme" runat="server" />
             <asp:RequiredFieldValidator ID="Requiredfieldvalidator5" runat="server" ControlToValidate="cboJournalScheme"
                            CssClass="validator_general" ErrorMessage="Harap pilih Skema Jurnal" InitialValue="SelectOne"></asp:RequiredFieldValidator> 
        </div>
    </div>

    <div class="form_box">
        <div class="form_single">
            <label> Skema Approval&nbsp;</label>
            <asp:Label ID="LApprovalScheme" runat="server"></asp:Label> 
            <asp:DropDownList ID="cboApprovalScheme" runat="server" />
        </div>
    </div>
    
    <div class="form_box">
    <div class="form_single">
        <label class="label_general"> Kondisi Asset</label>
        <asp:Label ID="lblAssetUsedNew" runat="server"></asp:Label>
        <asp:RadioButtonList ID="rboAssetUsedNew" runat="server" class="opt_single" RepeatDirection="Horizontal" Height="24px">
            <asp:ListItem Value="N" Selected="True">New</asp:ListItem>
            <asp:ListItem Value="U">Used</asp:ListItem>
        </asp:RadioButtonList>
     </div>
     </div>

     <div class="form_box">
     <div class="form_single">
        <label>Umur Asset</label>
        <asp:Label ID="lblUmurKendaraan" runat="server"></asp:Label>
        <asp:TextBox ID="txtUmurKendaraanFrom" runat="server"  MaxLength="9">0</asp:TextBox><asp:label  runat="server" id="lblTo">&nbsp;&nbsp;To&nbsp;&nbsp;</asp:label>
        <asp:TextBox ID="txtUmurKendaraanTo" runat="server"  MaxLength="9">0</asp:TextBox>
      </div>
      </div>
       
      <div class="form_box">
        <div class="form_single"> 
            <label>Tujuan Pembiayaan&nbsp;</label>
            <asp:Label ID="lblKegiatanUsaha" runat="server"></asp:Label>
             <asp:DropDownList ID="cboKegiatanUsaha" runat="server" AutoPostBack="true" />
             <asp:RequiredFieldValidator ID="Requiredfieldvalidator65" runat="server" ControlToValidate="cboKegiatanUsaha" CssClass="validator_general" ErrorMessage="Harap pilih Kegiatan Usaha" InitialValue="SelectOne"></asp:RequiredFieldValidator>                     
        </div>
       </div>
       
       <div class="form_box">
       <div class="form_single">
            <label>Jenis Pembiayaan&nbsp;</label> 
            <asp:Label ID="lblJenisPembiayaan" runat="server"></asp:Label>
            <asp:DropDownList ID="cboJenisPembiayaan" runat="server" /> 
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator66" runat="server"  ControlToValidate="cboJenisPembiayaan" CssClass="validator_general" ErrorMessage="Harap pilih Jenis Pembiayaan" InitialValue="SelectOne"></asp:RequiredFieldValidator> 
        </div>
        </div> 
</div>
