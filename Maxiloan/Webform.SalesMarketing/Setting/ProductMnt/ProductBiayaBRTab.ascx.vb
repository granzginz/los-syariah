﻿Public Class ProductBiayaBRTab
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Public Property UCMode As String
        Get
            Return CType(ViewState("ProductBiayaHOTABUCMODE"), String)
        End Get
        Set(value As String)
            ViewState("ProductBiayaHOTABUCMODE") = value
        End Set
    End Property

    Public Property Product() As Parameter.Product
        Get
            Return CType(ViewState("ProductBiayaHOProduct"), Parameter.Product)
        End Get
        Set(ByVal Value As Parameter.Product)
            ViewState("ProductBiayaHOProduct") = Value
        End Set
    End Property
    Sub InitialControls()
        lblCancellationFee.Visible = (UCMode = "VIEW")
        E_txtCancellationFee.Visible = (UCMode <> "VIEW")
        E_cboCancellationFeeBehaviour.Visible = (UCMode <> "VIEW")

        lblAdminFee.Visible = (UCMode = "VIEW")
        E_txtAdminFee.Visible = (UCMode <> "VIEW")
        E_cboAdminFeeBehaviour.Visible = (UCMode <> "VIEW")

        lblFiduciaFee.Visible = (UCMode = "VIEW")

        E_txtFiduciaFee.Visible = (UCMode <> "VIEW")
        E_cboFiduciaFeeBehaviour.Visible = (UCMode <> "VIEW")

        lblProvisionFee.Visible = (UCMode = "VIEW")
        E_txtProvisionFee.Visible = (UCMode <> "VIEW")
        E_cboProvisionFeeBehaviour.Visible = (UCMode <> "VIEW")



        lblNotaryFee.Visible = (UCMode = "VIEW")
        E_txtNotaryFee.Visible = (UCMode <> "VIEW")
        E_cboNotaryFeeBehaviour.Visible = (UCMode <> "VIEW")


        lblSurveyFee.Visible = (UCMode = "VIEW")
        E_txtSurveyFee.Visible = (UCMode <> "VIEW")
        E_cboSurveyFeeBehaviour.Visible = (UCMode <> "VIEW")


        lblVisitFee.Visible = (UCMode = "VIEW")
        E_txtVisitFee.Visible = (UCMode <> "VIEW")
        E_cboVisitFeeBehaviour.Visible = (UCMode <> "VIEW")



        lblReschedulingFee.Visible = (UCMode = "VIEW")
        E_txtReschedulingFee.Visible = (UCMode <> "VIEW")
        E_cboReschedulingFeeBehaviour.Visible = (UCMode <> "VIEW")

        lblAgreementTransferFee.Visible = (UCMode = "VIEW")
        E_txtAgreementTransferFee.Visible = (UCMode <> "VIEW")
        E_cboAgreementTransferFeeBehaviour.Visible = (UCMode <> "VIEW")

        lblChangeDueDateFee.Visible = (UCMode = "VIEW")
        E_txtChangeDueDateFee.Visible = (UCMode <> "VIEW")
        E_cboChangeDueDateFee.Visible = (UCMode <> "VIEW")



        lblAssetReplacementFee.Visible = (UCMode = "VIEW")
        E_txtAssetReplacementFee.Visible = (UCMode <> "VIEW")
        E_cboAssetReplacementFeeBehaviour.Visible = (UCMode <> "VIEW")


        lblRepossesFee.Visible = (UCMode = "VIEW")
        E_txtRepossesFee.Visible = (UCMode <> "VIEW")
        E_cboRepossesFeeBehaviour.Visible = (UCMode <> "VIEW")


        lblLegalizedDocumentFee.Visible = (UCMode = "VIEW")
        E_LegalizedDocumentFee.Visible = (UCMode <> "VIEW")
        E_cboLegalizedDocumentFeeBehaviour.Visible = (UCMode <> "VIEW")


        lblPDCBounceFee.Visible = (UCMode = "VIEW")
        E_PDCBounceFee.Visible = (UCMode <> "VIEW")
        E_cboPDCBounceFeeBehaviour.Visible = (UCMode <> "VIEW")


        lblInsuranceAdminFee.Visible = (UCMode = "VIEW")
        E_txtInsuranceAdminFee.Visible = (UCMode <> "VIEW")
        E_cboInsuranceAdminFeeBehaviour.Visible = (UCMode <> "VIEW")


        lblInsuranceStampDutyFee.Visible = (UCMode = "VIEW")
        E_txtInsuranceStampDutyFee.Visible = (UCMode <> "VIEW")
        E_cboInsuranceStampDutyFeeBehaviour.Visible = (UCMode <> "VIEW")


        lblBiayaPolis.Visible = (UCMode = "VIEW")
        E_txtBiayaPolis.Visible = (UCMode <> "VIEW")
        E_cboBiayaPolisBehaviour.Visible = (UCMode <> "VIEW")

        If (UCMode = "ADD") Then
            initFieldsForAddMode()
        End If
        If (UCMode = "EDIT") Then
            bindFiedsForEdit()
        End If
    End Sub


    Sub initFieldsForAddMode()

    End Sub
    Public Sub BindFiedsForEdit()

        'Cancellation Fee
        E_txtCancellationFee.Text = CStr(Product.CancellationFee)
        Hide_CancellationFee.Value = CStr(Product.CancellationFee)
        Hide_CancellationFeeBeh.Value = Product.CancellationFeeBehaviour
        E_cboCancellationFeeBehaviour.SelectedIndex = E_cboCancellationFeeBehaviour.Items.IndexOf(E_cboCancellationFeeBehaviour.Items.FindByValue(Product.CancellationFeeBehaviour))
        Behaviour_Lock(Product.CancellationFeeBehaviour, E_txtCancellationFee, E_cboCancellationFeeBehaviour)



        'Admin Fee
        E_txtAdminFee.Text = CStr(Product.AdminFee)
        Hide_AdminFee.Value = CStr(Product.AdminFee)
        Hide_AdminFeeBeh.Value = Product.AdminFeeBehaviour
        E_cboAdminFeeBehaviour.SelectedIndex = E_cboAdminFeeBehaviour.Items.IndexOf(E_cboAdminFeeBehaviour.Items.FindByValue(Product.AdminFeeBehaviour))

        Behaviour_Lock(Product.AdminFeeBehaviour, E_txtAdminFee, E_cboAdminFeeBehaviour)



        'Fiducia Fee
        E_txtFiduciaFee.Text = CStr(Product.FiduciaFee)
        Hide_FiduciaFee.Value = CStr(Product.FiduciaFee)
        Hide_FiduciaFeeBeh.Value = Product.FiduciaFeeBehaviour

        E_cboFiduciaFeeBehaviour.SelectedIndex = E_cboFiduciaFeeBehaviour.Items.IndexOf(E_cboFiduciaFeeBehaviour.Items.FindByValue(Product.FiduciaFeeBehaviour))
        Behaviour_Lock(Product.FiduciaFeeBehaviour, E_txtFiduciaFee, E_cboFiduciaFeeBehaviour)




        'Provision Fee
        E_txtProvisionFee.Text = CStr(Product.ProvisionFeeHO)
        Hide_ProvisionFee.Value = CStr(Product.ProvisionFeeHO)
        Hide_ProvisionFeeBeh.Value = Product.ProvisionFeeBehaviour
        E_cboProvisionFeeBehaviour.SelectedIndex = E_cboProvisionFeeBehaviour.Items.IndexOf(E_cboProvisionFeeBehaviour.Items.FindByValue(Product.ProvisionFeeBehaviour))
        Behaviour_Lock(Product.ProvisionFeeBehaviour, E_txtProvisionFee, E_cboProvisionFeeBehaviour)



        'Notary Fee
        E_txtNotaryFee.Text = CStr(Product.NotaryFee)
        Hide_NotaryFee.Value = CStr(Product.NotaryFee)
        Hide_NotaryFeeBeh.Value = Product.NotaryFeeBehaviour
        E_cboNotaryFeeBehaviour.SelectedIndex = E_cboNotaryFeeBehaviour.Items.IndexOf(E_cboNotaryFeeBehaviour.Items.FindByValue(Product.NotaryFeeBehaviour))
        Behaviour_Lock(Product.NotaryFeeBehaviour, E_txtNotaryFee, E_cboNotaryFeeBehaviour)




        'Survey Fee
        E_txtSurveyFee.Text = CStr(Product.SurveyFee)
        Hide_SurveyFee.Value = CStr(Product.SurveyFee)
        Hide_SurveyFeeBeh.Value = Product.SurveyFeeBehaviour

        E_cboSurveyFeeBehaviour.SelectedIndex = E_cboSurveyFeeBehaviour.Items.IndexOf(E_cboSurveyFeeBehaviour.Items.FindByValue(Product.SurveyFeeBehaviour))
        Behaviour_Lock(Product.SurveyFeeBehaviour, E_txtSurveyFee, E_cboSurveyFeeBehaviour)



        'Visit Fee
        E_txtVisitFee.Text = CStr(Product.VisitFee)
        Hide_VisitFee.Value = CStr(Product.VisitFee)
        Hide_VisitFeeBeh.Value = Product.VisitFeeBehaviour
        E_cboVisitFeeBehaviour.SelectedIndex = E_cboVisitFeeBehaviour.Items.IndexOf(E_cboVisitFeeBehaviour.Items.FindByValue(Product.VisitFeeBehaviour))
        Behaviour_Lock(Product.VisitFeeBehaviour, E_txtVisitFee, E_cboVisitFeeBehaviour)




        'Rescheduling Fee
        E_txtReschedulingFee.Text = CStr(Product.ReschedulingFee)
        Hide_ReschedulingFee.Value = CStr(Product.ReschedulingFee)
        Hide_ReschedulingFeeBeh.Value = Product.ReschedulingFeeBehaviour

        E_cboReschedulingFeeBehaviour.SelectedIndex = E_cboReschedulingFeeBehaviour.Items.IndexOf(E_cboReschedulingFeeBehaviour.Items.FindByValue(Product.ReschedulingFeeBehaviour))
        Behaviour_Lock(Product.ReschedulingFeeBehaviour, E_txtReschedulingFee, E_cboReschedulingFeeBehaviour)



        'AgreementTransfer Fee
        E_txtAgreementTransferFee.Text = CStr(Product.AgreementTransferFee)
        Hide_AgreementTransferFee.Value = CStr(Product.AgreementTransferFee)
        Hide_AgreementTransferFeeBeh.Value = Product.AgreementTransferFeeBehaviour
        E_cboAgreementTransferFeeBehaviour.SelectedIndex = E_cboAgreementTransferFeeBehaviour.Items.IndexOf(E_cboAgreementTransferFeeBehaviour.Items.FindByValue(Product.AgreementTransferFeeBehaviour))
        Behaviour_Lock(Product.AgreementTransferFeeBehaviour, E_txtAgreementTransferFee, E_cboAgreementTransferFeeBehaviour)



        'ChangeDueDate Fee
        E_txtChangeDueDateFee.Text = CStr(Product.ChangeDueDateFee)
        Hide_ChangeDueDateFee.Value = CStr(Product.ChangeDueDateFee)
        Hide_ChangeDueDateFeeBeh.Value = Product.ChangeDueDateFeeBehaviour
        E_cboChangeDueDateFee.SelectedIndex = E_cboChangeDueDateFee.Items.IndexOf(E_cboChangeDueDateFee.Items.FindByValue(Product.ChangeDueDateFeeBehaviour))
        Behaviour_Lock(Product.ChangeDueDateFeeBehaviour, E_txtChangeDueDateFee, E_cboChangeDueDateFee)


        'AssetReplacement Fee
        E_txtAssetReplacementFee.Text = CStr(Product.AssetReplacementFee)
        Hide_AssetReplacementFee.Value = CStr(Product.AssetReplacementFee)
        Hide_AssetReplacementFeeBeh.Value = Product.AssetReplacementFeeBehaviour

        E_cboAssetReplacementFeeBehaviour.SelectedIndex = E_cboAssetReplacementFeeBehaviour.Items.IndexOf(E_cboAssetReplacementFeeBehaviour.Items.FindByValue(Product.AssetReplacementFeeBehaviour))
        Behaviour_Lock(Product.AssetReplacementFeeBehaviour, E_txtAssetReplacementFee, E_cboAssetReplacementFeeBehaviour)



        'Reposses Fee
        E_txtRepossesFee.Text = CStr(Product.RepossesFee)
        Hide_RepossesFee.Value = CStr(Product.RepossesFee)
        Hide_RepossesFeeBeh.Value = Product.RepossesFeeBehaviour

        E_cboRepossesFeeBehaviour.SelectedIndex = E_cboRepossesFeeBehaviour.Items.IndexOf(E_cboRepossesFeeBehaviour.Items.FindByValue(Product.RepossesFeeBehaviour))
        Behaviour_Lock(Product.RepossesFeeBehaviour, E_txtRepossesFee, E_cboRepossesFeeBehaviour)



        'LegalisirDoc Fee
        E_LegalizedDocumentFee.Text = CStr(Product.LegalisirDocFee)
        Hide_LegalizedDocumentFee.Value = CStr(Product.LegalisirDocFee)
        Hide_LegalizedDocumentFeeBeh.Value = Product.LegalisirDocFeeBehaviour
        E_cboLegalizedDocumentFeeBehaviour.SelectedIndex = E_cboLegalizedDocumentFeeBehaviour.Items.IndexOf(E_cboLegalizedDocumentFeeBehaviour.Items.FindByValue(Product.LegalisirDocFeeBehaviour))
        Behaviour_Lock(Product.LegalisirDocFeeBehaviour, E_LegalizedDocumentFee, E_cboLegalizedDocumentFeeBehaviour)



        'PDCBounce Fee
        E_PDCBounceFee.Text = CStr(Product.PDCBounceFee)
        Hide_PDCBounceFee.Value = CStr(Product.PDCBounceFee)
        Hide_PDCBounceFeeBeh.Value = Product.PDCBounceFeeBehaviour
        E_cboPDCBounceFeeBehaviour.SelectedIndex = E_cboPDCBounceFeeBehaviour.Items.IndexOf(E_cboPDCBounceFeeBehaviour.Items.FindByValue(Product.PDCBounceFeeBehaviour))

        Behaviour_Lock(Product.PDCBounceFeeBehaviour, E_PDCBounceFee, E_cboPDCBounceFeeBehaviour)


        'InsuranceAdmin Fee
        E_txtInsuranceAdminFee.Text = CStr(Product.InsAdminFee)
        Hide_InsuranceAdminFee.Value = CStr(Product.InsAdminFee)
        Hide_InsuranceAdminFeeBeh.Value = Product.InsAdminFeeBehaviour
        E_cboInsuranceAdminFeeBehaviour.SelectedIndex = E_cboInsuranceAdminFeeBehaviour.Items.IndexOf(E_cboInsuranceAdminFeeBehaviour.Items.FindByValue(Product.InsAdminFeeBehaviour))
        Behaviour_Lock(Product.InsAdminFeeBehaviour, E_txtInsuranceAdminFee, E_cboInsuranceAdminFeeBehaviour)


        'InsuranceStampDutyFee 
        E_txtInsuranceStampDutyFee.Text = CStr(Product.InsStampDutyFee)
        Hide_InsuranceStampDutyFee.Value = CStr(Product.InsStampDutyFee)
        Hide_InsuranceStampDutyFeeBeh.Value = Product.InsStampDutyFeeBehaviour
        E_cboInsuranceStampDutyFeeBehaviour.SelectedIndex = E_cboInsuranceStampDutyFeeBehaviour.Items.IndexOf(E_cboInsuranceStampDutyFeeBehaviour.Items.FindByValue(Product.InsStampDutyFeeBehaviour))
        Behaviour_Lock(Product.InsStampDutyFeeBehaviour, E_txtInsuranceStampDutyFee, E_cboInsuranceStampDutyFeeBehaviour)



        'BiayaPolis 
        E_txtBiayaPolis.Text = CStr(Product.InsStampDutyFee)
        Hide_BiayaPolis.Value = CStr(Product.InsStampDutyFee)
        Hide_BiayaPolisBeh.Value = Product.InsStampDutyFeeBehaviour
        E_cboBiayaPolisBehaviour.SelectedIndex = E_cboBiayaPolisBehaviour.Items.IndexOf(E_cboBiayaPolisBehaviour.Items.FindByValue(Product.InsStampDutyFeeBehaviour))
        Behaviour_Lock(Product.InsStampDutyFeeBehaviour, E_txtBiayaPolis, E_cboBiayaPolisBehaviour)



        setBranchText()
    End Sub

    Sub setBranchText()
        lblCancellationFee_Branch.Text = CStr(Product.CancellationFee_HO) & " " & Parameter.Product.Behaviour_Value(Product.CancellationFeeBehaviour_HO)
        lblAdminFee_Branch.Text = CStr(Product.AdminFee_HO) & " " & Parameter.Product.Behaviour_Value(Product.AdminFeeBehaviour_HO)
        lblFiduciaFee_Branch.Text = CStr(Product.FiduciaFee_HO) & " " & Parameter.Product.Behaviour_Value(Product.FiduciaFeeBehaviour_HO)
        lblProvisionFee_Branch.Text = CStr(Product.ProvisionFeeHO) & " " & Parameter.Product.Behaviour_Value(Product.ProvisionFeeBehaviour_HO)
        lblNotaryFee_Branch.Text = CStr(Product.NotaryFee_HO) & " " & Parameter.Product.Behaviour_Value(Product.NotaryFeeBehaviour)
        lblSurveyFee_Branch.Text = CStr(Product.SurveyFee) & " " & Parameter.Product.Behaviour_Value(Product.SurveyFeeBehaviour_HO)
        lblVisitFee_Branch.Text = CStr(Product.VisitFee_HO) & " " & Parameter.Product.Behaviour_Value(Product.VisitFeeBehaviour_HO)
        lblReschedulingFee_Branch.Text = CStr(Product.ReschedulingFee_HO) & " " & Parameter.Product.Behaviour_Value(Product.ReschedulingFeeBehaviour_HO)
        lblAgreementTransferFee_Branch.Text = CStr(Product.AgreementTransferFee_HO) & " " & Parameter.Product.Behaviour_Value(Product.AgreementTransferFeeBehaviour_HO)
        lblChangeDueDateFee_Branch.Text = CStr(Product.ChangeDueDateFee_HO) & " " & Parameter.Product.Behaviour_Value(Product.ChangeDueDateFeeBehaviour_HO)
        lblAssetReplacementFee_Branch.Text = CStr(Product.AssetReplacementFee_HO) & " " & Parameter.Product.Behaviour_Value(Product.AssetReplacementFeeBehaviour_HO)
        lblRepossesFee_Branch.Text = CStr(Product.RepossesFee_HO) & " " & Parameter.Product.Behaviour_Value(Product.RepossesFeeBehaviour_HO)
        lblLegalizedDocumentFee_Branch.Text = CStr(Product.LegalisirDocFee_HO) & " " & Parameter.Product.Behaviour_Value(Product.LegalisirDocFeeBehaviour_HO)
        lblPDCBounceFee_Branch.Text = CStr(Product.PDCBounceFee_HO) & " " & Parameter.Product.Behaviour_Value(Product.PDCBounceFeeBehaviour_HO)
        lblInsuranceAdminFee_Branch.Text = CStr(Product.InsAdminFee_HO) & " " & Parameter.Product.Behaviour_Value(Product.InsAdminFeeBehaviour_HO)
        lblInsuranceStampDutyFee_Branch.Text = CStr(Product.InsStampDutyFee_HO) & " " & Parameter.Product.Behaviour_Value(Product.InsStampDutyFeeBehaviour_HO)
        lblBiayaPolis_Branch.Text = CStr(Product.BiayaPolis_HO) & " " & Parameter.Product.Behaviour_Value(Product.BiayaPolisBehaviour_HO)
    End Sub

    Public Sub InitUCViewMode()
        InitialControls()

        lblCancellationFee.Text = FormatNumber(CStr(Product.CancellationFee_HO).Trim, 2) & " " & Parameter.Product.Behaviour_Value(Product.CancellationFeeBehaviour_HO)
        lblAdminFee.Text = FormatNumber(CStr(Product.AdminFee_HO).Trim, 2) & " " & Parameter.Product.Behaviour_Value(Product.AdminFeeBehaviour_HO)
        lblFiduciaFee.Text = FormatNumber(CStr(Product.FiduciaFee_HO).Trim, 2) & " " & Parameter.Product.Behaviour_Value(Product.FiduciaFeeBehaviour_HO)
        lblProvisionFee.Text = FormatNumber(CStr(Product.ProvisionFeeHO).Trim, 2) & " " & Parameter.Product.Behaviour_Value(Product.ProvisionFeeBehaviour_HO)
        lblNotaryFee.Text = FormatNumber(CStr(Product.NotaryFee_HO).Trim, 2) & " " & Parameter.Product.Behaviour_Value(Product.NotaryFeeBehaviour_HO)
        lblSurveyFee.Text = FormatNumber(CStr(Product.SurveyFee_HO).Trim, 2) & " " & Parameter.Product.Behaviour_Value(Product.SurveyFeeBehaviour_HO)
        lblVisitFee.Text = FormatNumber(CStr(Product.VisitFee_HO).Trim, 2) & " " & Parameter.Product.Behaviour_Value(Product.VisitFeeBehaviour_HO)
        lblReschedulingFee.Text = FormatNumber(CStr(Product.ReschedulingFee_HO).Trim, 2) & " " & Parameter.Product.Behaviour_Value(Product.ReschedulingFeeBehaviour_HO)
        lblAgreementTransferFee.Text = FormatNumber(CStr(Product.AgreementTransferFee_HO).Trim, 2) & " " & Parameter.Product.Behaviour_Value(Product.AgreementTransferFeeBehaviour_HO)
        lblChangeDueDateFee.Text = FormatNumber(CStr(Product.ChangeDueDateFee_HO).Trim, 2) & " " & Parameter.Product.Behaviour_Value(Product.ChangeDueDateFeeBehaviour_HO)
        lblAssetReplacementFee.Text = FormatNumber(CStr(Product.AssetReplacementFee_HO).Trim, 2) & " " & Parameter.Product.Behaviour_Value(Product.AssetReplacementFeeBehaviour_HO)
        lblRepossesFee.Text = FormatNumber(CStr(Product.RepossesFee_HO).Trim, 2) & " " & Parameter.Product.Behaviour_Value(Product.RepossesFeeBehaviour_HO)
        lblLegalizedDocumentFee.Text = FormatNumber(CStr(Product.LegalisirDocFee_HO).Trim, 2) & " " & Parameter.Product.Behaviour_Value(Product.LegalisirDocFeeBehaviour_HO)
        lblPDCBounceFee.Text = FormatNumber(CStr(Product.PDCBounceFee_HO).Trim, 2) & " " & Parameter.Product.Behaviour_Value(Product.PDCBounceFeeBehaviour_HO)

        lblInsuranceAdminFee.Text = FormatNumber(CStr(Product.InsAdminFee_HO).Trim, 2) & " " & Parameter.Product.Behaviour_Value(Product.InsAdminFeeBehaviour_HO)
        lblInsuranceStampDutyFee.Text = FormatNumber(CStr(Product.InsStampDutyFee_HO).Trim, 2) & " " & Parameter.Product.Behaviour_Value(Product.InsStampDutyFeeBehaviour_HO)
        lblBiayaPolis.Text = FormatNumber(CStr(Product.BiayaPolis_HO).Trim, 2) & " " & Parameter.Product.Behaviour_Value(Product.BiayaPolisBehaviour_HO)

        setBranchText()
    End Sub
    Sub bindBehaviour(ddl As DropDownList)
        ddl.DataValueField = "Value"
        ddl.DataTextField = "Text"
        ddl.DataSource = Parameter.Product.BehaviourItems
        ddl.DataBind()
    End Sub
    Public Sub InitUCEditMode()
        bindBehaviour(E_cboCancellationFeeBehaviour)
        bindBehaviour(E_cboAdminFeeBehaviour)
        bindBehaviour(E_cboFiduciaFeeBehaviour)
        bindBehaviour(E_cboProvisionFeeBehaviour)
        bindBehaviour(E_cboNotaryFeeBehaviour)
        bindBehaviour(E_cboSurveyFeeBehaviour)
        bindBehaviour(E_cboVisitFeeBehaviour)
        bindBehaviour(E_cboReschedulingFeeBehaviour)
        bindBehaviour(E_cboAgreementTransferFeeBehaviour)
        bindBehaviour(E_cboChangeDueDateFee)
        bindBehaviour(E_cboAssetReplacementFeeBehaviour)
        bindBehaviour(E_cboRepossesFeeBehaviour)
        bindBehaviour(E_cboLegalizedDocumentFeeBehaviour)
        bindBehaviour(E_cboPDCBounceFeeBehaviour)
        bindBehaviour(E_cboInsuranceAdminFeeBehaviour)
        bindBehaviour(E_cboInsuranceStampDutyFeeBehaviour)
        bindBehaviour(E_cboBiayaPolisBehaviour)
        InitialControls()
    End Sub
    Public Sub CollectResult(oProduct As Parameter.Product)
        With oProduct

            .CancellationFee = CDec(E_txtCancellationFee.Text.Trim)
            .CancellationFeeBehaviour = E_cboCancellationFeeBehaviour.SelectedItem.Value.Trim
            .AdminFee = CDec(E_txtAdminFee.Text.Trim)
            .AdminFeeBehaviour = E_cboAdminFeeBehaviour.SelectedItem.Value.Trim
            .FiduciaFee = CDec(E_txtFiduciaFee.Text.Trim)
            .FiduciaFeeBehaviour = E_cboFiduciaFeeBehaviour.SelectedItem.Value.Trim
            .ProvisionFee = CDec(E_txtProvisionFee.Text.Trim)
            .ProvisionFeeBehaviour = E_cboProvisionFeeBehaviour.SelectedItem.Value.Trim
            .NotaryFee = CDec(E_txtNotaryFee.Text.Trim)
            .NotaryFeeBehaviour = E_cboNotaryFeeBehaviour.SelectedItem.Value.Trim
            .SurveyFee = CDec(E_txtSurveyFee.Text.Trim)
            .SurveyFeeBehaviour = E_cboSurveyFeeBehaviour.SelectedItem.Value.Trim
            .VisitFee = CDec(E_txtVisitFee.Text.Trim)
            .VisitFeeBehaviour = E_cboVisitFeeBehaviour.SelectedItem.Value.Trim
            .ReschedulingFee = CDec(E_txtReschedulingFee.Text.Trim)
            .ReschedulingFeeBehaviour = E_cboReschedulingFeeBehaviour.SelectedItem.Value.Trim
            .AgreementTransferFee = CDec(E_txtAgreementTransferFee.Text.Trim)
            .AgreementTransferFeeBehaviour = E_cboAgreementTransferFeeBehaviour.SelectedItem.Value.Trim
            .ChangeDueDateFee = CDec(E_txtChangeDueDateFee.Text.Trim)
            .ChangeDueDateFeeBehaviour = E_cboChangeDueDateFee.SelectedItem.Value.Trim
            .AssetReplacementFee = CDec(E_txtAssetReplacementFee.Text.Trim)
            .AssetReplacementFeeBehaviour = E_cboAssetReplacementFeeBehaviour.SelectedItem.Value.Trim
            .RepossesFee = CDec(E_txtRepossesFee.Text.Trim)
            .RepossesFeeBehaviour = E_cboRepossesFeeBehaviour.SelectedItem.Value.Trim
            .LegalisirDocFee = CDec(E_LegalizedDocumentFee.Text.Trim)
            .LegalisirDocFeeBehaviour = E_cboLegalizedDocumentFeeBehaviour.SelectedItem.Value.Trim
            .PDCBounceFee = CDec(E_PDCBounceFee.Text.Trim)
            .PDCBounceFeeBehaviour = E_cboPDCBounceFeeBehaviour.SelectedItem.Value.Trim
            .InsAdminFee = CDec(E_txtInsuranceAdminFee.Text.Trim)
            .InsAdminFeeBehaviour = E_cboInsuranceAdminFeeBehaviour.SelectedItem.Value.Trim
            .InsStampDutyFee = CDec(E_txtInsuranceStampDutyFee.Text.Trim)
            .InsStampDutyFeeBehaviour = E_cboInsuranceStampDutyFeeBehaviour.SelectedItem.Value.Trim
            .BiayaPolis = CDec(E_txtBiayaPolis.Text.Trim)
            .BiayaPolisBehaviour = E_cboBiayaPolisBehaviour.SelectedItem.Value.Trim

        End With

    End Sub
End Class