﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ProductView.aspx.vb" Inherits="Maxiloan.Webform.SalesMarketing.ProductView" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="productUmumTab.ascx" TagName="umum" TagPrefix="uc1" %>  
<%@ Register Src="productSkemaTab.ascx" TagName="skema" TagPrefix="uc2" %>  
<%@ Register Src="productBiayaTab.ascx" TagName="biaya" TagPrefix="uc3" %>  
<%@ Register Src="productAngsuranTabe.ascx" TagName="angs" TagPrefix="uc4" %>  
<%@ Register Src="productCollectionTab.ascx" TagName="coll" TagPrefix="uc5" %>  
<%@ Register Src="productFinanceTab.ascx" TagName="fin" TagPrefix="uc6" %>  

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Product View</title>
    <script language="JavaScript" type="text/javascript">
        function fClose() {
            window.close();
            return false;
        }				
    </script>
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../../Include/Buttons.css" type="text/css" />
</head>

<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
        <uc1:umum id="UmumTab" runat="server"/>
        <uc2:skema id="SkemaTab" runat="server"/>
        <uc3:biaya id="BiayaTab" runat="server"/>
        <uc4:angs id="AngsuranTab" runat="server"/>
        <uc5:coll id="CollectionTab" runat="server"/>
        <uc6:fin id="FinanceTab" runat="server"/>
   <%-- <div class="form_box">
	    <div class="form_single">
            <label>Pendapatan Bunga Minimum yg harus ditolak</label>
            <asp:Label ID="lblMIITBR" runat="server"></asp:Label>&nbsp;%&nbsp;
            <asp:Label ID="lblMIITBRBehaviour" runat="server"></asp:Label>
	    </div>
    </div>
    <div class="form_box">
	    <div class="form_single">
            <label>Pendapatan Bunga Minimum yg harus diingatkan</label>
            <asp:Label ID="lblMIITBW" runat="server"></asp:Label>&nbsp;%&nbsp;
            <asp:Label ID="lblMIITBWBehaviour" runat="server"></asp:Label>
	    </div>
    </div>  --%>

    <div class="form_button">
        <asp:Button ID="ButtonClose" OnClientClick="fClose();" runat="server"  Text="Close" CssClass ="small button gray"
        CausesValidation="false"></asp:Button>
    </div>       
    </form>
</body>
</html>
