﻿Public Class ProductUmumHOTab
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub
    Public Property UCMode As String
        Get
            Return CType(ViewState("UMUMTABUCMODE"), String)
        End Get
        Set(value As String)
            ViewState("UMUMTABUCMODE") = value
        End Set
    End Property

    Public Property UCProductName As String
        Get
            Return CType(ViewState("UCProductName"), String)
        End Get
        Set(value As String)
            ViewState("UCProductName") = value
        End Set
    End Property
    Public Property Product As Parameter.Product
        Get
            Return CType(ViewState("ProductUmumTabProduct"), Parameter.Product)
        End Get
        Set(ByVal Value As Parameter.Product)
            ViewState("ProductUmumTabProduct") = Value
        End Set
    End Property


    Private Sub InitialControls()
        lblUmurKendaraan.Visible = (UCMode = "VIEW")
        txtUmurKendaraanFrom.Visible = (UCMode <> "VIEW")
        txtUmurKendaraanTo.Visible = (UCMode <> "VIEW")
        If (UCMode = "ADD") Then
            initFieldsForAddMode()
        End If
        If (UCMode = "EDIT") Then
            BindFiedsForEdit()
        End If

    End Sub

    Sub initFieldsForAddMode()

    End Sub


    Public Sub BindFiedsForEdit()
        E_lblBranchID.Text = Product.BranchFullName
        E_lblProductID.Text = Product.ProductId
        E_lblDescription.Text = Product.Description
        E_lblAssetType.Text = Product.AssetType_desc
        E_lblScoreSchemeID.Text = Product.ScoreSchemeMaster_desc
        E_lblCreditScoreSchemeID.Text = Product.CreditScoreSchemeMaster_desc
        E_lblJournalSchemeID.Text = Product.JournalScheme_desc
        E_lblApprovalSchemeID.Text = Product.ApprovalTypeScheme_desc

        E_lblAssetUsedNew.Text = IIf(Product.AssetUsedNew = "U", "Used", "New")

        E_lblKegiatanUsaha.Text = Product.KegiatanUsahaDesc
        E_lblJenisPembiayaan.Text = Product.JenisPembiayaanDesc

        txtUmurKendaraanFrom.Text = Product.UmurKendaraanFrom_HO.ToString
        txtUmurKendaraanTo.Text = Product.UmurKendaraanTo_HO.ToString

    End Sub
    Sub SetHeader()
        lblHeader.Text = String.Format("{0} - {1}", UCProductName.ToUpper(), UCMode.ToUpper())
    End Sub

    Public Sub InitUCViewMode() 
        InitialControls() 
        SetHeader() 

        E_lblProductID.Text = Product.ProductId
        E_lblBranchID.Text = Product.BranchFullName
        E_lblDescription.Text = Product.Description
        E_lblAssetType.Text = Product.AssetType_desc
        E_lblScoreSchemeID.Text = Product.ScoreSchemeMaster_desc
        E_lblCreditScoreSchemeID.Text = Product.CreditScoreSchemeMaster_desc
        E_lblJournalSchemeID.Text = Product.JournalScheme_desc
        E_lblApprovalSchemeID.Text = Product.ApprovalTypeScheme_desc
        E_lblAssetUsedNew.Text = IIf(Product.AssetUsedNew = "N", "New", "Used")
        lblUmurKendaraan.Text = String.Format("{0} To {1}", Product.UmurKendaraanFrom, Product.UmurKendaraanTo)
        E_lblKegiatanUsaha.Text = Product.KegiatanUsahaDesc
        E_lblJenisPembiayaan.Text = Product.JenisPembiayaanDesc
    End Sub
    Public Sub InitUCEditMode()
        InitialControls()
    End Sub

    Public Sub CollectResult(oProduct As Parameter.Product)
        With oProduct
            .ProductId = E_lblProductID.Text
            .UmurKendaraanFrom = CInt(txtUmurKendaraanFrom.Text.Trim)
            .UmurKendaraanTo = CInt(txtUmurKendaraanTo.Text.Trim)
        End With
    End Sub
End Class