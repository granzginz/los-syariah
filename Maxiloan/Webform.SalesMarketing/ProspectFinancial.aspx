﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ProspectFinancial.aspx.vb"
    Inherits="Maxiloan.Webform.SalesMarketing.ProspectFinancial" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../webform.UserController/ucProspectTab.ascx" TagName="ucProspectTab" TagPrefix="uc1" %>
<%@ Register Src="../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat" TagPrefix="uc1" %>
<%@ Register Src="../webform.UserController/ucDateCE.ascx" TagName="ucDateCE" TagPrefix="uc1" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Prospect Maintenance Asset</title>
    <link rel="Stylesheet" type="text/css" href="../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../Include/Buttons.css" />
    
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <link href="../include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../js/jquery-1.9.1.min.js"></script>
    <script src="../js/jquery-ui-1.10.3.custom.min.js"></script>
    <script language="javascript" type="text/javascript">

    </script>
</head>
<body>
    <asp:UpdateProgress ID="upg1" runat="server" AssociatedUpdatePanelID="upnl1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <div runat="server" id="jlookupContent" />
    <%--<asp:UpdatePanel runat="server" ID="upnl1" UpdateMode="Conditional" ChildrenAsTriggers="false">--%>
    <asp:UpdatePanel runat="server" ID="upnl1" UpdateMode="Conditional" ChildrenAsTriggers="True">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
                onclick="hideMessage();"></asp:Label>
            <uc1:ucProspectTab id="ucProspectTab1" runat="server" />
            <div class="form_title">
                <div class="form_single">
                    <h3>
                        DATA FINANCIAL</h3>
                </div>
            </div>
            <div class="form_box">
                <div>
                <div class="form_left">
                    <label class="label_general">Rekening tabungan</label>
                        <asp:RadioButtonList ID="rdoRekeningTabungan" runat="server" RepeatDirection="Horizontal" 
                                CssClass="opt_single" AutoPostBack="true" >
                                <asp:ListItem Value="True" Selected="True">Ya</asp:ListItem>
                                <asp:ListItem Value="False">Tidak</asp:ListItem>
                            </asp:RadioButtonList>
                </div>
                <div class="form_right">
                    <label class="">No Rekening </label>
                    <asp:TextBox ID="txtNoRekening" runat="server" CssClass="medium_text"  MaxLength="50" onkeypress="return numbersonly2(event)"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfNoRekening" runat="server" ControlToValidate="txtNoRekening"
                                CssClass="validator_general" Display="Dynamic" ErrorMessage="*"
                                SetFocusOnError="true"></asp:RequiredFieldValidator>
                </div>
                </div>
            </div>
           <div class="form_box">
                <div>
                <div class="form_left">
                    <label class="label_general">Pengalaman Pembiayaan</label>
                        <asp:RadioButtonList ID="rdopengalamankredit" runat="server" RepeatDirection="Horizontal" 
                                CssClass="opt_single" AutoPostBack="true">
                                <asp:ListItem Value="True" Selected="True">Ya</asp:ListItem>
                                <asp:ListItem Value="False">Tidak</asp:ListItem>
                            </asp:RadioButtonList>
                </div>
                <div class="form_right">
                    <label class="">Pengalaman Pembiayaan Dari</label>
                    <asp:TextBox ID="txtPengalamanKredit" runat="server" CssClass="medium_text"  MaxLength="50"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfPengalamanKredit" runat="server" ControlToValidate="txtPengalamanKredit"
                                CssClass="validator_general" Display="Dynamic" ErrorMessage="*"
                                SetFocusOnError="true"></asp:RequiredFieldValidator>
                </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                <div class="form_left">
                    <%--<label class="label_general">Kepemilikan unit lain</label>--%>
                    <label class="label_general">Kepemilikan unit mobil lain</label>
                        <asp:RadioButtonList ID="rdokepemilikanunit" runat="server" RepeatDirection="Horizontal" 
                                CssClass="opt_single" AutoPostBack="true">
                                <asp:ListItem Value="True" Selected="True">Ya</asp:ListItem>
                                <asp:ListItem Value="False">Tidak</asp:ListItem>
                            </asp:RadioButtonList>
                </div>
                <div class="form_right">
                    <label class="">Tahun Asset</label>
                    <asp:TextBox ID="txtTahunKendaraan" runat="server" MaxLength="4" CssClass="small_text" onkeypress="return numbersonly2(event)">0</asp:TextBox>
                        <asp:RangeValidator ID="RangeValidator1" runat="server" Type="Integer" MinimumValue="0"
                            ErrorMessage="Tahun harus < hari Ini" ControlToValidate="txtTahunKendaraan" Display="Dynamic"
                            CssClass="validator_general"></asp:RangeValidator>
                            <asp:RequiredFieldValidator ID="rfTahunKendaraan" runat="server" ControlToValidate="txtTahunKendaraan"
                                CssClass="validator_general" Display="Dynamic" ErrorMessage="Harap tahun asset"
                                SetFocusOnError="true"></asp:RequiredFieldValidator>
                </div>
                </div>
            </div>
            <%--modify Nofi 2Mar2018--%>
            <%--<div class="form_box_hide">--%>
            <div class="form_box">
                <div>
                    <div class="form_left">
                    </div>
                    <%--<div class="form_single">--%>
                    <div class="form_right">
                        <label class="">
                           Merek Asset Yang Dibiayai
                        </label>
                        <asp:DropDownList ID="cbomerk" runat="server">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
            <%--<div class="form_box_hide">--%>
            <div class="form_box">
                <div>
                    <div class="form_left">
                    </div>
                    <%--<div class="form_single">--%>
                    <div class="form_right">
                        <label class="">
                           Kategori Asset
                        </label>
                        <asp:DropDownList ID="cbokategori" runat="server">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
            <%--<div class="form_box_hide">--%>
            <div class="form_box">
                <div>
                    <div class="form_left">
                    </div>
                    <%--<div class="form_single">--%>
                    <div class="form_right">
                        <label class="">
                           Penggunaan
                        </label>
                        <asp:DropDownList ID="cboUsage" runat="server" Width="150PX">
                            </asp:DropDownList>
                         <asp:RequiredFieldValidator ID="rfUsage" runat="server" ControlToValidate="cboUsage"
                                CssClass="validator_general" Display="Dynamic" ErrorMessage="*"
                                InitialValue="Select One" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div> 
            <div class="form_button">
                <asp:Button ID="btnProceed" runat="server" Text="Proceed" CssClass="small button green" CausesValidation="True" />
                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue" CausesValidation="True" />
                <asp:Button ID="btnCancel" runat="server" Text="Back" CssClass="small button gray"
                    CausesValidation="False"></asp:Button>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnProceed" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="rdoRekeningTabungan" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="rdopengalamankredit" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="rdokepemilikanunit" EventName="SelectedIndexChanged" />
        </Triggers>
    </asp:UpdatePanel>
    </form>
</body>
</html>
