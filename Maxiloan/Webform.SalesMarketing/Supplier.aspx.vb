﻿
#Region "Imports"
Imports System.IO
Imports Maxiloan.cbse
Imports Maxiloan.Exceptions
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
Imports System.Net


#End Region

Public Class Supplier
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents ucAddress As UcCompanyAddress
    Protected WithEvents ucAddressNPWP As UcCompanyAddress
    Protected WithEvents StartDate As ucDateCE
#Region "Constanta"
    Private m_controller As New SupplierController
    Dim oSupplier As New Parameter.Supplier
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Dim oRow As DataRow
    Dim Control As New CTKonfirmasiEbankExec
    Dim dtCSV, dtSQL As New DataTable
#End Region

#Region "Property"
    Private Property AddEdit() As String
        Get
            Return CType(ViewState("AddEdit"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("AddEdit") = Value
        End Set
    End Property
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property WhereCond() As String
        Get
            Return CType(ViewState("WhereCond"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("WhereCond") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property
    Private Property _FileType() As String
        Get
            Return CStr(ViewState("_FileType"))
        End Get
        Set(ByVal Value As String)
            ViewState("_FileType") = Value
        End Set
    End Property
    Private Property ApplicationID() As String
        Get
            Return (CType(ViewState("ApplicationID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
    Private Property SupplierID() As String
        Get
            Return (CType(ViewState("SupplierID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("SupplierID") = Value
        End Set
    End Property
#End Region

#Region "Page Load, Bindgrid"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not Me.IsPostBack Then
            If CheckForm(Me.Loginid, "Supplier", Me.AppId) Then

                If SessionInvalid() Then
                    Exit Sub
                End If
            End If

            If Request.QueryString("strFileLocation") <> "" Then
                Dim strFileLocation As String
                strFileLocation = "../XML/" & Request.QueryString("strFileLocation") & ".pdf"
                Response.Write("<script language = javascript>" & vbCrLf _
                & "var x = screen.width; var y = screen.height;  window.open('" & strFileLocation & "','accacq', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes'); " & vbCrLf _
                & "</script>")
            End If
            txtPage.Text = "1"
            If Request("currentPage") <> "" Then
                Me.currentPage = Request("currentPage")
                txtPage.Text = Me.currentPage
            End If
            Me.Sort = "Name ASC"
            If Request("cond") <> "" Then
                Me.CmdWhere = Request("cond")
            Else
                'Me.CmdWhere = "ALL"
                Me.CmdWhere = " PF = 'false' "
                If IsHoBranch = False Then
                    Me.CmdWhere = Me.CmdWhere & " and branchid = '" & Me.sesBranchId.ToString.Replace("'", "") & "'"
                End If
            End If
            InitialPanel()

            'wierd
            'BindAdd2()

            BindGridEntity(Me.CmdWhere)
            ucAddress.Style = "Marketing"
            ucAddress.ValidatorTrue()
            ucAddress.Phone1ValidatorEnabled(True)
            ucAddressNPWP.Style = "Marketing"
            ucAddressNPWP.ValidatorTrue()
            ucAddressNPWP.Phone1ValidatorEnabled(True)
            BindComboIC()
            'Btn_Download.Visible = False
            'ButtonDownload1.Visible = False
            'ButtonSearch.Visible = True
            'Btn_Upload.Visible = True
       
        End If

        If rboSell.SelectedItem.Value = "0" Then
            rboCategory.Items.FindByValue("OT").Selected = True
            rboCategory.Enabled = False

            rboBPKB.Items.FindByValue("0").Selected = True
            rboBPKB.Enabled = False

            rboAssetSold.Items.FindByValue("M").Selected = True
            rboAssetSold.Enabled = False
        Else
            rboCategory.Enabled = True
            rboBPKB.Enabled = True
            rboAssetSold.Enabled = True
        End If
        ucAddress.IsRequiredZipcode = True

    End Sub
    Sub InitialPanel()
        pnlList.Visible = True
        pnlAddEdit.Visible = False
        pnlAdd2.Visible = False
        rgvGo.Enabled = True
    End Sub

    Sub BindGridEntity(ByVal cmdWhere As String)
        Dim dtEntity As DataTable
        Dim oCustomClass As New Parameter.Supplier
        oCustomClass.PageSize = pageSize
        oCustomClass.WhereCond = cmdWhere
        oCustomClass.SortBy = Me.Sort
        oCustomClass.CurrentPage = currentPage
        oCustomClass.strConnection = GetConnectionString()
        oCustomClass = m_controller.GetSupplier(oCustomClass)
        If Not oCustomClass Is Nothing Then
            dtEntity = oCustomClass.ListData
            recordCount = oCustomClass.TotalRecords
        Else
            recordCount = 0
        End If
        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()
        PagingFooter()
    End Sub
    Private Sub fillcboProduk()
        Dim oDataTable As New DataTable
        With oSupplier
            .strConnection = GetConnectionString()
        End With
        oSupplier = m_controller.GetProduk(oSupplier)
        oDataTable = oSupplier.ListData
        With cboProduk
            .DataSource = oDataTable
            .DataTextField = "Description"
            .DataValueField = "AssetTypeID"
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = ""
        End With
    End Sub
#End Region
#Region "Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        txtPage.Text = Me.currentPage
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub imgbtnPageNumb_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int32)
                BindGridEntity(Me.CmdWhere)
            End If
        End If
    End Sub
#End Region
#Region "Sorting"
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub
#End Region
#Region "Reset-Search"
    Private Sub imbButtonReset_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbButtonReset.Click
        cboSearch.SelectedIndex = 0
        txtSearch.Text = ""
        Me.CmdWhere = " PF = 'false'"
        If IsHoBranch = False Then
            Me.CmdWhere = Me.CmdWhere & " and branchid = '" & Me.sesBranchId.ToString.Replace("'", "") & "'"
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub imbButtonSearch_Click(ByVal sender As Object, ByVal e As EventArgs) Handles imbButtonSearch.Click
        Me.CmdWhere = ""
        If txtSearch.Text.Trim <> "" Then
            Dim Search As String
            If cboSearch.SelectedIndex = 1 Then
                Search = Replace(txtSearch.Text.Trim, "'", "''")
            Else
                Search = txtSearch.Text.Trim
            End If
            Me.CmdWhere = cboSearch.SelectedItem.Value + " LIKE '%" + Search + "%' and PF = 'false'"
        Else
            Me.CmdWhere = " PF = 'false' "
        End If

        If IsHoBranch = False Then
            Me.CmdWhere = Me.CmdWhere & " and branchid = '" & Me.sesBranchId.ToString.Replace("'", "") & "'"
        End If

        BindGridEntity(Me.CmdWhere)
    End Sub
#End Region
#Region "ItemCommand"
    Private Sub dtgPaging_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        Dim Id As String
        Dim Name As String
        Dim AssetTypeID As String
        Dim Err As String = ""
        If e.CommandName = "Edit" Then
            If CheckFeature(Me.Loginid, "Supplier", "Edit", Me.AppId) Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            Id = CType(e.Item.FindControl("lnkID"), Label).Text
            Me.AddEdit = "Edit"
            bindEdit(Id)
            'Response.Redirect("CustomerPersonal.aspx?page=Edit&id=" & e.Item.Cells(4).Text.Trim & "")
        ElseIf e.CommandName = "Delete" Then
            If CheckFeature(Me.Loginid, "Supplier", "Del", Me.AppId) Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            Dim oSupplier As New Parameter.Supplier
            Id = CType(e.Item.FindControl("lnkID"), Label).Text
            oSupplier.strConnection = GetConnectionString()
            oSupplier.SupplierID = Id

            Err = m_controller.DeleteSupplier(oSupplier)
            If Err = "" Then
                txtPage.Text = "1"
                BindGridEntity(Me.CmdWhere)
            Else
                ShowMessage(lblMessage, Err, True)
            End If
        ElseIf e.CommandName = "Branch" Or e.CommandName = "Owner" Or e.CommandName = "Budget" Or e.CommandName = "Forecast" _
  Or e.CommandName = "Employee" Or e.CommandName = "Account" Or e.CommandName = "Signature" Or e.CommandName = "Relasi" Then
            Dim Target As String = "Supplier" + e.CommandName
            Id = CType(e.Item.FindControl("lnkID"), Label).Text
            Name = CType(e.Item.FindControl("lnkName"), HyperLink).Text.Trim
            'AssetTypeID = CType(e.Item.FindControl("lnkProduk"), Label).Text.Trim
            'Name = e.Item.Cells(1).Text.Trim
            Response.Redirect("" & Target & ".aspx?id=" & Id & "&name=" & Name & "&AssetTypeID=" & AssetTypeID & "&apps=Supplier&currentPage=" & txtPage.Text & "")

        ElseIf e.CommandName = "Print" Then
            'Dim cookie As HttpCookie = Request.Cookies("PerjanjianKerjasamaSuppPrint")
            'If Not cookie Is Nothing Then
            '    cookie.Values("ApplicationId") = dtgPaging.Items(e.Item.ItemIndex).Cells(6).Text
            'Else
            '    Dim cookieNew As New HttpCookie("PerjanjianKerjasamaSuppPrint")
            '    cookieNew.Values.Add("ApplicationId", dtgPaging.Items(e.Item.ItemIndex).Cells(6).Text)
            '    Response.AppendCookie(cookieNew)
            'End If
            'Response.Redirect("PerjanjianKerjasamaSuppViewer.aspx")
            Me.ID = CType(e.Item.FindControl("lnkID"), Label).Text
            'Me.BranchID = CType(e.Item.FindControl("lnkBranch"), Label).Text
            Me.WhereCond = "Supplier.supplierID = '" & Me.ID & "'"
            '& "' and BranchID='" + Me.BranchID + "'"

            Dim cookie As HttpCookie = Request.Cookies("PerjanjianKerjasamaSupplierPrint")
            If Not cookie Is Nothing Then
                cookie.Values("CmdWhere") = Me.ID.Trim ' Me.WhereCond
                Response.AppendCookie(cookie)
            Else
                Dim cookieNew As New HttpCookie("PerjanjianKerjasamaSupplierPrint")
                cookieNew.Values.Add("CmdWhere", Me.ID.Trim)
                Response.AppendCookie(cookieNew)
            End If
            Response.Redirect("PerjanjianKerjasamaSuppViewer.aspx")
        End If
    End Sub
#End Region
#Region "Add"
    Private Sub imbButtonAdd_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbButtonAdd.Click
        Dim oSupplier As New Parameter.Supplier
        Dim oAddress As New Parameter.Address
        Dim oAddressNPWP As New Parameter.Address
        Dim oReturnValue As New Parameter.Supplier

        'set parameter Untuk SupplierID
        oSupplier.strConnection = GetConnectionString()
        oSupplier.BusinessDate = Me.BusinessDate
        oSupplier.BranchId = Me.sesBranchId.Replace("'", "")

        'controller parameter Untuk SupplierID
        'oReturnValue = m_controller.GetSupplierID(oSupplier)

        'lblID.Text = oReturnValue.SupplierID

        'set parameter Untuk NoPKS
        oSupplier.strConnection = GetConnectionString()
        oSupplier.BusinessDate = Me.BusinessDate
        oSupplier.BranchId = Me.sesBranchId.Replace("'", "")

        'controller parameter NoPKS
        oReturnValue = m_controller.GetNoPKS(oSupplier)
        txtNoPKS.Text = oReturnValue.NoPKS

        Me.AddEdit = "Add"

        lblTitle.Text = "ADD"
        lblsuppID.Visible = False
        fillcboProduk()
        cboProduk.ClearSelection()
        DefaultAdd()
        bindAdd()
        BindAdd2()
        'BindGenerateSuppID()
        BindGenerateID()

    End Sub
    Sub bindAdd()
        pnlList.Visible = False
        pnlAddEdit.Visible = True
        pnlAdd2.Visible = False
        rgvGo.Enabled = False
        If Me.AddEdit = "Add" Then
            rboBadStatus.Visible = False
            txtBadStatusNormal.Visible = True
        Else
            rboBadStatus.Visible = True
            txtBadStatusNormal.Visible = False
        End If
    End Sub
    Sub BindAdd2()
        Dim oData As DataTable
        Dim oCustomClass As New Parameter.Supplier
        oCustomClass.strConnection = GetConnectionString()
        oCustomClass = m_controller.GetBranch(oCustomClass)
        If Not oCustomClass Is Nothing Then
            oData = oCustomClass.ListData
        End If
        dtgAdd.DataSource = oData.DefaultView
        dtgAdd.DataBind()
    End Sub

    Sub BindGenerateID()
        Dim oData As DataTable
        Dim oCustomClass As New Parameter.Supplier
        oCustomClass.strConnection = GetConnectionString()
        oCustomClass = m_controller.GetBranch(oCustomClass)
        If Not oCustomClass Is Nothing Then
            oData = oCustomClass.ListData
        End If
        dtgAdd.DataSource = oData.DefaultView
        dtgAdd.DataBind()
    End Sub
    Sub BindGenerateSuppID()
        Dim oData As DataTable
        Dim oCustomClass As New Parameter.Supplier
        oCustomClass.strConnection = GetConnectionString()
        oCustomClass = m_controller.GetSupplierID(oCustomClass)
        If Not oCustomClass Is Nothing Then
            oData = oCustomClass.ListData
        End If
        dtgAdd.DataSource = oData.DefaultView
        dtgAdd.DataBind()
    End Sub


    Sub DefaultAdd()
        Dim oCustomClass As New Parameter.Supplier

        'oCustomClass.SupplierID = ID

        'oCustomClass.SupplierID = ID

        'txtID.Text = ID
        lblID.Text = ""
        txtName.Text = ""
        txtNPWP.Text = ""
        txtTDP.Text = ""
        txtSIUP.Text = ""
        ucAddress.Address = ""
        ucAddress.RT = ""
        ucAddress.RW = ""
        ucAddress.Kelurahan = ""
        ucAddress.Kecamatan = ""
        ucAddress.City = ""
        ucAddress.ZipCode = ""
        ucAddress.AreaPhone1 = ""
        ucAddress.Phone1 = ""
        ucAddress.AreaPhone2 = ""
        ucAddress.Phone2 = ""
        ucAddress.AreaFax = ""
        ucAddress.Fax = ""
        ucAddress.BindAddress()
        ucAddressNPWP.Address = ""
        ucAddressNPWP.RT = ""
        ucAddressNPWP.RW = ""
        ucAddressNPWP.Kelurahan = ""
        ucAddressNPWP.Kecamatan = ""
        ucAddressNPWP.City = ""
        ucAddressNPWP.ZipCode = ""
        ucAddressNPWP.AreaPhone1 = ""
        ucAddressNPWP.Phone1 = ""
        ucAddressNPWP.AreaPhone2 = ""
        ucAddressNPWP.Phone2 = ""
        ucAddressNPWP.AreaFax = ""
        ucAddressNPWP.Fax = ""
        ucAddressNPWP.BindAddress()
        txtCPName.Text = ""
        txtCPJobTitle.Text = ""
        txtCPEmail.Text = ""
        StartDate.Text = Format(Me.BusinessDate, "dd/MM/yyyy")
        rboBadStatus.SelectedIndex = 0
        rboLevel.SelectedIndex = 2
        rboSell.SelectedIndex = 0
        rboBPKB.SelectedIndex = 0
        rboCategory.SelectedIndex = 0
        rboAssetSold.SelectedIndex = 2
        txtNoPKS.Text = ""
        cboProduk.SelectedIndex = 0
        rboPenerapanTVC.SelectedIndex = 0
        cboProduk.SelectedIndex = 0

    End Sub
#End Region
#Region "Edit"
    Sub bindEdit(ByVal ID As String)
        pnlList.Visible = False
        pnlAddEdit.Visible = True
        pnlAdd2.Visible = False

        rgvGo.Enabled = False
        lblTitle.Text = "EDIT"
        ' txtID.Visible = False

        fillcboProduk()
        lblsuppID.Visible = False
        If Me.AddEdit = "Add" Then
            rboBadStatus.Visible = False
            txtBadStatusNormal.Visible = True
        Else
            rboBadStatus.Visible = True
            txtBadStatusNormal.Visible = False
        End If
        Dim oData As New DataTable
        Dim oCustomClass As New Parameter.Supplier

        oCustomClass.SupplierID = ID
        oCustomClass.strConnection = GetConnectionString()
        oCustomClass = m_controller.SupplierEdit(oCustomClass)

        If Not oCustomClass Is Nothing Then
            oData = oCustomClass.ListData
        End If

        If oData.Rows.Count > 0 Then
            oRow = oData.Rows(0)
            ' txtID.Text = oRow.Item("SupplierID").ToString.Trim
            lblsuppID.Text = ID
            lblID.Text = ID
            'lblsuppID.Text = oRow.Item("SupplierID").ToString.Trim()
            'cboGroup.Items.FindByValue(oRow.Item(0).ToString).Selected = True
            txtName.Text = oRow.Item("SupplierName").ToString.Trim
            txtNPWP.Text = oRow.Item("NPWP").ToString.Trim
            txtTDP.Text = oRow.Item("TDP").ToString.Trim
            txtSIUP.Text = oRow.Item("SIUP").ToString.Trim
            cboICNew.SelectedIndex = cboICNew.Items.IndexOf(cboICNew.Items.FindByValue(oRow.Item("SupplierIncentiveIDNew").ToString))
            cboICUsed.SelectedIndex = cboICUsed.Items.IndexOf(cboICUsed.Items.FindByValue(oRow.Item("SupplierIncentiveIDUsed").ToString))
            cboICRO.SelectedIndex = cboICRO.Items.IndexOf(cboICRO.Items.FindByValue(oRow.Item("SupplierIncentiveIDRO").ToString))
            ucAddress.Address = oRow.Item("SupplierAddress").ToString.Trim
            ucAddress.RT = oRow.Item("SupplierRT").ToString.Trim
            ucAddress.RW = oRow.Item("SupplierRW").ToString.Trim
            ucAddress.Kelurahan = oRow.Item("SupplierKelurahan").ToString.Trim
            ucAddress.Kecamatan = oRow.Item("SupplierKecamatan").ToString.Trim
            ucAddress.City = oRow.Item("SupplierCity").ToString.Trim
            ucAddress.ZipCode = oRow.Item("SupplierZipCode").ToString.Trim
            ucAddress.AreaPhone1 = oRow.Item("SupplierAreaPhone1").ToString.Trim
            ucAddress.Phone1 = oRow.Item("SupplierPhone1").ToString.Trim
            ucAddress.AreaPhone2 = oRow.Item("SupplierAreaPhone2").ToString.Trim
            ucAddress.Phone2 = oRow.Item("SupplierPhone2").ToString.Trim
            ucAddress.AreaFax = oRow.Item("SupplierAreaFax").ToString.Trim
            ucAddress.Fax = oRow.Item("SupplierFax").ToString.Trim
            ucAddress.BindAddress()
            txtCPName.Text = oRow.Item("ContactPersonName").ToString.Trim
            txtCPJobTitle.Text = oRow.Item("ContactPersonJobTitle").ToString.Trim
            txtCPEmail.Text = oRow.Item("ContactPersonEmail").ToString.Trim
            txtCPMobile.Text = oRow.Item("ContactPersonHP").ToString.Trim
            hdnGroupSupplierCode.Value = oRow.Item("SupplierGroupID").ToString.Trim
            txtGroupSupplierName.Text = oRow.Item("SupplierGroupName").ToString.Trim

            If oRow.Item("PKSDate").ToString.Trim <> "" Then
                StartDate.Text = Format(oRow.Item("PKSDate"), "dd/MM/yyyy")
            End If
            If StartDate.Text.Trim = "01/01/1900" Then
                StartDate.Text = Format(Me.BusinessDate, "dd/MM/yyyy")
            End If

            'rboBadStatus.Items.FindByValue(oRow.Item(25).ToString).Selected = True
            rboBadStatus.SelectedIndex = rboBadStatus.Items.IndexOf(rboBadStatus.Items.FindByValue(oRow.Item("SupplierBadStatus").ToString))

            'rboLevel.Items.FindByValue(oRow.Item(26).ToString).Selected = True
            rboLevel.SelectedIndex = rboLevel.Items.IndexOf(rboLevel.Items.FindByValue(oRow.Item("SupplierLevelStatus").ToString))

            'rboSell.Items.FindByValue(IIf(oRow.Item(27).ToString = "True", "1", "0").ToString).Selected = True
            rboSell.SelectedIndex = rboSell.Items.IndexOf(rboSell.Items.FindByValue(IIf(oRow.Item("IsAutomotive").ToString = "True", "1", "0").ToString))

            'rboBPKB.Items.FindByValue(IIf(oRow.Item(28).ToString = "True", "1", "0").ToString).Selected = True
            rboBPKB.SelectedIndex = rboBPKB.Items.IndexOf(rboBPKB.Items.FindByValue(oRow.Item("IsUrusBPKB").ToString))

            'rboCategory.Items.FindByValue(oRow.Item(29).ToString).Selected = True
            rboCategory.SelectedIndex = rboCategory.Items.IndexOf(rboCategory.Items.FindByValue(oRow.Item("SupplierCategory").ToString))

            'rboAssetSold.Items.FindByValue(oRow.Item(30).ToString).Selected = True
            rboAssetSold.SelectedIndex = rboAssetSold.Items.IndexOf(rboAssetSold.Items.FindByValue(oRow.Item("SupplierAssetStatus").ToString))
            'cboProduk.SelectedIndex = cboProduk.Items.IndexOf(cboProduk.Items.FindByValue(IIf(oRow("AssetTypeID").ToString.Trim = "W", "W", "B").ToString))

            txtNoPKS.Text = oRow.Item("NoPKS").ToString.Trim

            If oRow.Item("AssetTypeID").ToString <> "" Then
                cboProduk.SelectedIndex = cboProduk.Items.IndexOf(cboProduk.Items.FindByValue(oRow.Item("AssetTypeID").ToString))
            Else
                cboProduk.SelectedIndex = 0
            End If

            If oRow.Item("PenerapanTVC").ToString <> "" Then
                rboPenerapanTVC.SelectedIndex = rboPenerapanTVC.Items.IndexOf(rboPenerapanTVC.Items.FindByValue(oRow.Item("PenerapanTVC").ToString))
            Else
                rboPenerapanTVC.SelectedIndex = 0
            End If

            If oRow.Item("PF").ToString <> "" Then
                rboPF.SelectedIndex = rboPF.Items.IndexOf(rboPF.Items.FindByValue(oRow.Item("PF").ToString))
            Else
                rboPF.SelectedIndex = 0
            End If

            If oRow.Item("isPerseorangan") = True Then
                chkPerseorangan.Checked = True
            Else
                chkPerseorangan.Checked = False
            End If
            If oRow.Item("SKBP") = True Then
                chkSKBP.Checked = True
            Else
                chkSKBP.Checked = False
            End If

            ucAddressNPWP.Address = oRow.Item("NPWPSupplierAddress").ToString.Trim
            ucAddressNPWP.RT = oRow.Item("NPWPSupplierRT").ToString.Trim
            ucAddressNPWP.RW = oRow.Item("NPWPSupplierRW").ToString.Trim
            ucAddressNPWP.Kelurahan = oRow.Item("NPWPSupplierKelurahan").ToString.Trim
            ucAddressNPWP.Kecamatan = oRow.Item("NPWPSupplierKecamatan").ToString.Trim
            ucAddressNPWP.City = oRow.Item("NPWPSupplierCity").ToString.Trim
            ucAddressNPWP.ZipCode = oRow.Item("NPWPSupplierZipCode").ToString.Trim
            ucAddressNPWP.AreaPhone1 = oRow.Item("NPWPSupplierAreaPhone1").ToString.Trim
            ucAddressNPWP.Phone1 = oRow.Item("NPWPSupplierPhone1").ToString.Trim
            ucAddressNPWP.AreaPhone2 = oRow.Item("NPWPSupplierAreaPhone2").ToString.Trim
            ucAddressNPWP.Phone2 = oRow.Item("NPWPSupplierPhone2").ToString.Trim
            ucAddressNPWP.AreaFax = oRow.Item("NPWPSupplierAreaFax").ToString.Trim
            ucAddressNPWP.Fax = oRow.Item("NPWPSupplierFax").ToString.Trim
            ucAddressNPWP.BindAddress()
            cekimage()
            cekimageApproval()
        End If
    End Sub
#End Region
#Region "Cancel"
    Private Sub imbButtonCancel_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbButtonCancel.Click
        InitialPanel()
    End Sub
#End Region
#Region "Save, OK"
    Private Sub imbButtonSave_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbButtonSave.Click
        Dim oSupplier As New Parameter.Supplier
        Dim oAddress As New Parameter.Address
        Dim oAddressNPWP As New Parameter.Address
        Dim oReturnValue As New Parameter.Supplier


        Dim fileName = ""
        Dim base64String As String = ""
        

        oSupplier.strConnection = GetConnectionString()
        oSupplier.SupplierName = txtName.Text
        oSupplier.NPWP = txtNPWP.Text
        oSupplier.SupplierGroupID = hdnGroupSupplierCode.Value.ToString.Trim

        If chkPerseorangan.Checked = True Then
            oSupplier.isPerseorangan = True
        Else
            oSupplier.isPerseorangan = False
        End If

        oSupplier.SKBP = chkSKBP.Checked

        oSupplier.TDP = txtTDP.Text
        oSupplier.SIUP = txtSIUP.Text
        If cboICNew.SelectedItem.Value <> "0" Then
            oSupplier.CardIDNew = CInt(cboICNew.SelectedItem.Value)
        Else
            oSupplier.CardIDNew = 0
        End If
        If cboICUsed.SelectedItem.Value <> "0" Then
            oSupplier.CardIDUsed = CInt(cboICUsed.SelectedItem.Value)
        Else
            oSupplier.CardIDUsed = 0
        End If
        If cboICRO.SelectedItem.Value <> "0" Then
            oSupplier.CardIDRO = CInt(cboICRO.SelectedItem.Value)
        Else
            oSupplier.CardIDRO = 0
        End If

        oAddress.Address = ucAddress.Address
        oAddress.RT = ucAddress.RT
        oAddress.RW = ucAddress.RW
        oAddress.Kelurahan = ucAddress.Kelurahan
        oAddress.Kecamatan = ucAddress.Kecamatan
        oAddress.City = ucAddress.City
        oAddress.ZipCode = ucAddress.ZipCode
        oAddress.AreaPhone1 = ucAddress.AreaPhone1
        oAddress.Phone1 = ucAddress.Phone1
        oAddress.AreaPhone2 = ucAddress.AreaPhone2
        oAddress.Phone2 = ucAddress.Phone2
        oAddress.AreaFax = ucAddress.AreaFax
        oAddress.Fax = ucAddress.Fax
        oAddressNPWP.Address = ucAddressNPWP.Address
        oAddressNPWP.RT = ucAddressNPWP.RT
        oAddressNPWP.RW = ucAddressNPWP.RW
        oAddressNPWP.Kelurahan = ucAddressNPWP.Kelurahan
        oAddressNPWP.Kecamatan = ucAddressNPWP.Kecamatan
        oAddressNPWP.City = ucAddressNPWP.City
        oAddressNPWP.ZipCode = ucAddressNPWP.ZipCode
        oAddressNPWP.AreaPhone1 = ucAddressNPWP.AreaPhone1
        oAddressNPWP.Phone1 = ucAddressNPWP.Phone1
        oAddressNPWP.AreaPhone2 = ucAddressNPWP.AreaPhone2
        oAddressNPWP.Phone2 = ucAddressNPWP.Phone2
        oAddressNPWP.AreaFax = ucAddressNPWP.AreaFax
        oAddressNPWP.Fax = ucAddressNPWP.Fax
        oSupplier.CPName = txtCPName.Text
        oSupplier.CPJobTitle = txtCPJobTitle.Text
        oSupplier.CPEmail = txtCPEmail.Text
        oSupplier.CPHP = txtCPMobile.Text
        'oSupplier.Produk = cboProduk.SelectedItem.Value

        If StartDate.Text.Trim <> "" Then
            If ConvertDate2(StartDate.Text) > Me.BusinessDate Then
                ShowMessage(lblMessage, "Tanggal harus <= Tanggal Business....", True)
                Exit Sub
            End If
            oSupplier.SupplierStartDate = ConvertDate(StartDate.Text)
        Else
            oSupplier.SupplierStartDate = StartDate.Text
        End If
        If Me.AddEdit = "Add" Then
            oSupplier.SupplierBadStatus = "N"
        Else
            oSupplier.SupplierBadStatus = rboBadStatus.SelectedValue
        End If

        oSupplier.SupplierLevelStatus = rboLevel.SelectedValue
        oSupplier.IsAutomotive = rboSell.SelectedValue
        oSupplier.IsUrusBPKB = rboBPKB.SelectedValue
        oSupplier.SupplierCategory = rboCategory.SelectedValue
        oSupplier.SupplierAssetStatus = rboAssetSold.SelectedValue
        oSupplier.BusinessDate = Me.BusinessDate
        oSupplier.BranchId = Me.sesBranchId.Replace("'", "")

        oSupplier.NoPKS = txtNoPKS.Text
        'txtNoPKS.Text = oSupplier.SupplierID
        oSupplier.PKSDate = ConvertDate2(IIf(CStr(StartDate.Text) = "", Format(Me.BusinessDate, "dd/MM/yyyy"), StartDate.Text))
        oSupplier.PenerapanTVC = rboPenerapanTVC.SelectedItem.Value
        oSupplier.PF = False ' rboPF.SelectedItem.Value
        oSupplier.Produk = cboProduk.SelectedItem.Value
        'oSupplier.ID = lblID.Text

        oSupplier.SupplierID = lblID.Text

        'set parameter Untuk NoPKS
        oSupplier.strConnection = GetConnectionString()
        oSupplier.BusinessDate = Me.BusinessDate
        oSupplier.BranchId = Me.sesBranchId.Replace("'", "")
        'controller parameter NoPKS
        'oReturnValue = m_controller.GetNoPKS(oSupplier)
        'txtNoPKS.Text = oReturnValue.NoPKS
        'oSupplier.NoPKS = txtNoPKS.Text
        'If oRow.Item("SKBP") = True Then
        '    chkSKBP.Checked = True
        'Else
        '    chkSKBP.Checked = False
        'End If
        oSupplier.SKBP = chkSKBP.Checked

        If Me.AddEdit = "Add" Then
            oReturnValue = m_controller.SupplierSaveAdd(oSupplier, oAddress, oAddressNPWP)
            If oReturnValue.Output <> "" Then
                ShowMessage(lblMessage, oReturnValue.Output, True)
                Exit Sub
            End If
          
            pnlAddEdit.Visible = False
            pnlList.Visible = True
            pnlAdd2.Visible = False
            rgvGo.Enabled = False
            lblName2.Text = txtName.Text
            lblName2.NavigateUrl = "javascript:OpenWinSupplier('" & "Marketing" & "', '" & lblsuppID.Text.Trim & "')"
        Else
            Try
                oSupplier.SupplierID = lblsuppID.Text.Trim
                oSupplier.NoPKS = txtNoPKS.Text.Trim
                m_controller.SupplierSaveEdit(oSupplier, oAddress, oAddressNPWP)
                pnlList.Visible = True
                pnlAdd2.Visible = False
                pnlAddEdit.Visible = False
            Catch ex As Exception
                ShowMessage(lblMessage, "Proses simpan data gagal.....", True)
                Exit Sub
            End Try
        End If
        BindGridEntity(Me.CmdWhere)
        cekimage()
        cekimageApproval()
    End Sub
    Private Sub imbButtonOK_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbButtonOK.Click
        Dim oSupplier As New Parameter.Supplier
        Dim oDataBranch As New DataTable
        Dim intLoop As Integer
        Dim chk As CheckBox

        oDataBranch.Columns.Add("BranchID", GetType(String))

        For intLoop = 0 To dtgAdd.Items.Count - 1
            chk = CType(dtgAdd.Items(intLoop).Cells(0).FindControl("chkAssign"), CheckBox)
            If chk.Checked Then
                oRow = oDataBranch.NewRow
                oRow("BranchID") = dtgAdd.DataKeys.Item(intLoop).ToString.Trim
                oDataBranch.Rows.Add(oRow)
            End If
        Next
        oSupplier.strConnection = GetConnectionString()
        oSupplier.SupplierID = lblsuppID.Text.Trim

        m_controller.SupplierSaveBranch(oSupplier, oDataBranch)

        pnlAddEdit.Visible = False
        pnlList.Visible = True
        pnlAdd2.Visible = False
        txtPage.Text = "1"
        Me.Sort = "Name ASC"
        Me.CmdWhere = "ALL"
        BindGridEntity(Me.CmdWhere)
    End Sub
#End Region
#Region "ItemDataBound"
    Private Sub dtgAdd_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgAdd.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim chk As CheckBox
            chk = CType(e.Item.FindControl("chkAssign"), CheckBox)
            chk.Checked = True
        End If
    End Sub
    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim lnk As Label
            Dim lnkName As HyperLink
            'Dim lnkProduk As Label

            lnk = CType(e.Item.FindControl("lnkID"), Label)
            lnkName = CType(e.Item.FindControl("lnkName"), HyperLink)
            lnkName.NavigateUrl = "javascript:OpenWinSupplier('" & "Marketing" & "', '" & lnk.Text & "')"
            'lnk.Attributes.Add("OnClick", "return OpenWin('" & lnk.Text & "','Marketing');")
            'lnkProduk = CType(e.Item.FindControl("lnkProduk"), Label)
            Dim imbDelete As ImageButton
            imbDelete = CType(e.Item.FindControl("imgDelete"), ImageButton)
            imbDelete.Attributes.Add("OnClick", "return DeleteConfirm();")
        End If
    End Sub
#End Region
    '#Region "Upload PKS"
    '    Protected Sub ButtonSearch_Click(sender As Object, e As EventArgs) Handles ButtonSearch.Click

    '        dtCSV = PopulateFromCSV()
    '        dtSQL = PopulateSQL()
    '        files.Visible = False
    '        Btn_Download.Visible = True
    '        ButtonDownload1.Visible = True
    '        ButtonSearch.Visible = False
    '        'Btn_Upload.Visible = False
    '    End Sub
    '    Protected Sub ButtonDownload1_Click(sender As Object, e As EventArgs) Handles ButtonDownload1.Click

    '        files.Visible = True
    '        Response.ContentType = "application/vnd.pdf"
    '        Response.AddHeader("content-disposition", "attachment; filename=LampiranMemo.pdf")
    '        Response.WriteFile(Server.MapPath("../XML/LampiranMemo.pdf"))
    '        Response.End()
    '        'Response.ContentType = "image/jpeg"
    '        'Response.AddHeader("content-disposition", "attachment; filename=LampiranPKS.jpg")
    '        'Response.WriteFile(Server.MapPath("../XML/LampiranPKS.jpg"))
    '        'Response.End()


    '    End Sub
    '    Private Function PopulateFromCSV()
    '        Dim appPath As String = HttpContext.Current.Request.ApplicationPath
    '        Dim physicalPath As String = HttpContext.Current.Request.MapPath(appPath)
    '        Dim directory As String = physicalPath & "\xml\"
    '        'Dim FileName As String = files.PostedFile.FileName
    '        Dim FileName As String = "LampiranMemo.pdf"
    '        Dim fullFileName As String = directory & FileName
    '        Dim dt As New DataTable
    '        files.PostedFile.SaveAs(fullFileName)
    '        lblfileuploaded.Text = FileName
    '    End Function
    '    Private Function PopulateSQL()
    '        Dim dt As New DataTable
    '        Dim param As New Parameter.PEKonfirmasiEbankExec
    '        Dim control As New Controller.CTKonfirmasiEbankExec

    '        With param
    '            .strConnection = GetConnectionString()
    '        End With

    '        param = control.GetListing(param)
    '        dt = param.ListData
    '        Return dt
    '    End Function

    '#End Region
    '#Region "Upload_Approval"
    '    Protected Sub Btn_Upload_Click(sender As Object, e As EventArgs) Handles Btn_Upload.Click

    '        dtCSV = PopulateFromCSV_A()
    '        dtSQL = PopulateSQL_A()
    '        files_A.Visible = False
    '        Btn_Upload.Visible = False
    '        lblfileuploaded.Visible = True
    '    End Sub
    '    Private Function PopulateFromCSV_A()
    '        Dim appPath As String = HttpContext.Current.Request.ApplicationPath
    '        Dim physicalPath As String = HttpContext.Current.Request.MapPath(appPath)
    '        Dim directory As String = physicalPath & "\xml\"
    '        'Dim FileName As String = files_A.PostedFile.FileName
    '        Dim FileName As String = "LampiranApproval.pdf"
    '        Dim fullFileName As String = directory & FileName
    '        Dim dt As New DataTable
    '        files_A.PostedFile.SaveAs(fullFileName)
    '        lblupload.Text = FileName
    '    End Function
    '    Private Function PopulateSQL_A()
    '        Dim dt As New DataTable
    '        Dim param As New Parameter.PEKonfirmasiEbankExec
    '        Dim control As New Controller.CTKonfirmasiEbankExec

    '        With param
    '            .strConnection = GetConnectionString()
    '        End With

    '        param = control.GetListing(param)
    '        dt = param.ListData
    '        Return dt
    '    End Function
    '    Protected Sub Btn_Download_Click(sender As Object, e As EventArgs) Handles Btn_Download.Click
    '        Dim FileName As String = "LampiranApproval"
    '        _FileType = "pdf"
    '        Response.ContentType = "application/vnd.pdf"
    '        Response.AddHeader("content-disposition", "attachment; filename=" & FileName & ".pdf")
    '        Response.WriteFile(Server.MapPath("../XML/LampiranApproval.pdf"))
    '        Response.End()
    '        files_A.Visible = True
    '    End Sub

    '#End Region
    Private Function pathFile(ByVal Group As String, ByVal ExecutorID As String) As String
        Dim strDirectory As String = ""
        strDirectory = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\" & sesBranchId.Replace("'", "") & "\" & Group & "\" & ExecutorID & "\"
        If Not System.IO.Directory.Exists(strDirectory) Then
            System.IO.Directory.CreateDirectory(strDirectory)
        End If
        Return strDirectory
    End Function

    Private Sub btnupload_Click(sender As Object, e As System.EventArgs) Handles btnupload.Click
        Dim FileName As String = ""
        Dim strExtension As String = ""
        If uplProsedurKYC.HasFile Then
            If uplProsedurKYC.PostedFile.ContentType = "application/pdf" Then
                FileName = "PKS"
                strExtension = Path.GetExtension(uplProsedurKYC.PostedFile.FileName)
                uplProsedurKYC.PostedFile.SaveAs(pathFile("Supplier", lblsuppID.Text.Replace(" ", "").Replace("'", "")) + FileName + strExtension)
            End If
        End If
        cekimage()
    End Sub

    Private Sub cekimage()
        If File.Exists(pathFile("Supplier", lblsuppID.Text.Trim) + "PKS.pdf") Then
            hypProsedurKYC.NavigateUrl = ResolveClientUrl("../xml/" & sesBranchId.Replace("'", "") & "/Supplier/" + lblsuppID.Text.Replace(" ", "").Replace("'", "") + "/PKS.pdf")
            hypProsedurKYC.Visible = True
            hypProsedurKYC.Text = "PKS.pdf"
            hypProsedurKYC.Target = "_blank"
        Else
            hypProsedurKYC.Visible = False
        End If
    End Sub

    Private Sub btnuploadApproval_Click(sender As Object, e As System.EventArgs) Handles btnuploadApproval.Click
        Dim FileName As String = ""
        Dim strExtension As String = ""
        If uplApproval.HasFile Then
            If uplApproval.PostedFile.ContentType = "application/pdf" Then
                FileName = "MemoApproval"
                strExtension = Path.GetExtension(uplApproval.PostedFile.FileName)
                uplApproval.PostedFile.SaveAs(pathFile("Supplier", lblsuppID.Text.Replace(" ", "").Replace("'", "")) + FileName + strExtension)
            End If
        End If
        cekimageApproval()
    End Sub

    Private Sub cekimageApproval()
        If File.Exists(pathFile("Supplier", lblsuppID.Text.Trim) + "MemoApproval.pdf") Then
            hyApproval.NavigateUrl = ResolveClientUrl("../xml/" & sesBranchId.Replace("'", "") & "/Supplier/" + lblsuppID.Text.Replace(" ", "").Replace("'", "") + "/MemoApproval.pdf")
            hyApproval.Visible = True
            hyApproval.Text = "MemoApproval.pdf"
            hyApproval.Target = "_blank"
        Else
            hyApproval.Visible = False
        End If
    End Sub
  
#Region "BindComboIncentiveCard"
    Private Sub BindComboIC()
        Dim oSupplierIC As New Parameter.IncentiveCard
        Dim oControllerSupplierIC As New IncentiveCardController
        Dim dtIncentiveCard As New DataTable
        With oSupplierIC
            .strConnection = GetConnectionString()
        End With
        dtIncentiveCard = oControllerSupplierIC.GetComboSupplierIncentiveCard(oSupplierIC)
        With cboICNew
            .DataSource = dtIncentiveCard
            .DataValueField = "SupplierIncentiveCardID"
            .DataTextField = "CardName"
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = "0"
        End With
        With cboICUsed
            .DataSource = dtIncentiveCard
            .DataValueField = "SupplierIncentiveCardID"
            .DataTextField = "CardName"
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = "0"
        End With
        With cboICRO
            .DataSource = dtIncentiveCard
            .DataValueField = "SupplierIncentiveCardID"
            .DataTextField = "CardName"
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = "0"
        End With
    End Sub
#End Region

    Private Sub rboSell_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rboSell.SelectedIndexChanged
        If rboSell.SelectedItem.Value = "0" Then
            rboCategory.Items.FindByValue("OT").Selected = True
            rboCategory.Enabled = False

            rboBPKB.Items.FindByValue("0").Selected = True
            rboBPKB.Enabled = False

            rboAssetSold.Items.FindByValue("M").Selected = True
            rboAssetSold.Enabled = False
        Else
            rboCategory.Enabled = True
            rboBPKB.Enabled = True
            rboAssetSold.Enabled = True
        End If
    End Sub
End Class