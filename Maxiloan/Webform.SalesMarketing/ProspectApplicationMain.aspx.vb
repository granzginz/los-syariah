﻿Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
Imports System.IO
Imports System.Collections.Generic

Public Class ProspectApplicationMain
    Inherits Maxiloan.Webform.WebBased


#Region "Controls"
    Private oController As New ProspectController
    Private m_controller As New CustomerController
    Private oCustomer As New Parameter.Customer
    'Protected WithEvents ucMailingAddress As UcCompanyAddress
    'Protected WithEvents ucJarakSurvey As ucNumberFormat
    Protected WithEvents ucProspectTab1 As ucProspectTab
    Private time As String
#End Region

#Region "Properties"
    Property ProspectAppID() As String
        Get
            Return ViewState("ProspectAppID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ProspectAppID") = Value
        End Set
    End Property
    Property PageMode() As String
        Get
            Return ViewState("PageMode").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("PageMode") = Value
        End Set
    End Property

    Property ActivityDateStart() As DateTime
        Get
            Return ViewState("ActivityDateStart").ToString
        End Get
        Set(ByVal Value As DateTime)
            ViewState("ActivityDateStart") = Value
        End Set
    End Property

    Property CustomerType() As String
        Get
            Return ViewState("CustomerType").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerType") = Value
        End Set
    End Property
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Visible = False

        If SessionInvalid() Then Exit Sub

        If Not Page.IsPostBack Then
            initObjects()
            Me.PageMode = Request("page")
            'ucMailingAddress.Phone1ValidatorEnabled(False)
            If Me.PageMode = "Edit" Then
                Dim oCustomclass As New Parameter.Prospect
                Me.ProspectAppID = Request("id")
                With oCustomclass
                    .strConnection = GetConnectionString()
                    .ProspectAppID = Me.ProspectAppID
                End With
                BindEdit()

                ucProspectTab1.ProspectAppID = Me.ProspectAppID
                ucProspectTab1.setLink()
                time = Now.Hour.ToString("00") & ":" & Now.Minute.ToString("00") & ":" & Now.Second.ToString("00")
                Me.ActivityDateStart = Me.BusinessDate + " " + time
            Else
                Me.ProspectAppID = ""
                time = Now.Hour.ToString("00") & ":" & Now.Minute.ToString("00") & ":" & Now.Second.ToString("00")
                Me.ActivityDateStart = Me.BusinessDate + " " + time
            End If
        End If
    End Sub


#Region "Init Objects"
    Protected Sub initObjects()
        ucProspectTab1.selectedTab("Application")
        btnNext.Visible = True
        btnSave.Visible = False
        PanelInfo.Visible = False
        'fillCboIDType()
        'ucMailingAddress.ValidatorTrue()
        'ucMailingAddress.showMandatoryAll()
        TxtAreaPhoneGuarantor.Text = ""
        TxtPhoneGuarantor.Text = ""
    End Sub

    Sub fillCboIDType()
        Dim oCustomer As New Parameter.Customer
        Dim dtList As DataTable
        oCustomer.strConnection = GetConnectionString()

        If Me.CustomerType = "P" Then
            oCustomer = m_controller.CustomerPersonalGetIDType(oCustomer)
        Else
            oCustomer = m_controller.CustomerCompanyGetIDType(oCustomer)
        End If

        dtList = oCustomer.listdata
        cboIDTypeP.DataSource = dtList
        cboIDTypeP.DataTextField = "Description"
        cboIDTypeP.DataValueField = "ID"
        cboIDTypeP.DataBind()
        cboIDTypeP.Items.Insert(0, "Select One")
        cboIDTypeP.Items(0).Value = "Select One"
    End Sub
#End Region

#Region "BindEdit"
    Sub BindEdit()
        Dim oApplication As New Parameter.Prospect
        Dim oData As New DataTable
        Dim oRow As DataRow

        oApplication.strConnection = GetConnectionString()
        oApplication.BranchId = Replace(Me.sesBranchId, "'", "").ToString
        oApplication.ProspectAppID = Me.ProspectAppID
        oApplication = oController.GetViewProspect(oApplication)

        If Not oApplication Is Nothing Then
            oData = oApplication.ListData
        End If

        If oData.Rows.Count > 0 Then
            oRow = oData.Rows(0)
            'txttanggalAplikasi.Text = Format(oRow("ProspectAppDate"), "dd/MM/yyyy")
            lbltanggalAplikasi.Text = Format(oRow("ProspectAppDate"), "dd/MM/yyyy")
            'txtNama.Text = oRow("Name").ToString
            lblNama.Text = oRow("Name").ToString
            'txtTempatLahir.Text = oRow("BirthPlace").ToString
            lblTempatLahir.Text = oRow("BirthPlace").ToString
            'txtTanggalLahir.Text = Format(oRow("BirthDate"), "dd/MM/yyyy")
            lblTanggalLahir.Text = Format(oRow("BirthDate"), "dd/MM/yyyy")
            'txtMobilePhone.Text = oRow("MobilePhone").ToString
            lblMobilePhone.Text = oRow("MobilePhone").ToString
            'ucJarakSurvey.Text = oRow("JarakSurvey").ToString
            lblJarakSurvey.Text = oRow("JarakSurvey").ToString
            'cboIDTypeP.SelectedIndex = cboIDTypeP.Items.IndexOf(cboIDTypeP.Items.FindByValue(oRow("IDType").ToString.Trim))
            'txtIDNumberP.Text = oRow("IDNumber").ToString.Trim
            lblIDNumberP.Text = oRow("IDNumber").ToString.Trim
            rdoReferensi.SelectedValue = CInt(oRow("IsReferenced"))
            txtPhoneReferensi.Text = oRow("PhoneReference").ToString.Trim
            txtKeteranganReferal.Text = oRow("KetReference").ToString.Trim
            rdostatusnasabah.SelectedValue = CInt(oRow("StatusNasabah"))
            'With (ucMailingAddress)
            '    .Address = oRow("Address").ToString.Trim
            '    .RT = oRow("RT").ToString.Trim
            '    .RW = oRow("RW").ToString.Trim
            '    .Kelurahan = oRow("Kelurahan").ToString.Trim
            '    .Kecamatan = oRow("Kecamatan").ToString.Trim
            '    .City = oRow("City").ToString.Trim
            '    .ZipCode = oRow("ZipCode").ToString.Trim
            '    .AreaPhone1 = oRow("AreaPhone1").ToString.Trim
            '    .Phone1 = oRow("Phone1").ToString.Trim
            '    .AreaPhone2 = oRow("AreaPhone2").ToString.Trim
            '    .Phone2 = oRow("Phone2").ToString.Trim
            '    .AreaFax = oRow("AreaFax").ToString.Trim
            '    .Fax = oRow("Fax").ToString.Trim
            '    .BindAddress()
            'End With
            lblAlamat.Text = oRow("Address").ToString.Trim
            lblRT.Text = oRow("RT").ToString.Trim
            LblRW.Text = oRow("RW").ToString.Trim
            lblKelurahan.Text = oRow("Kelurahan").ToString.Trim
            lblKecamatan.Text = oRow("Kecamatan").ToString.Trim
            lblCity.Text = oRow("City").ToString.Trim
            lblZipCode.Text = oRow("ZipCode").ToString.Trim
            lblAreaPhone1.Text = oRow("AreaPhone1").ToString.Trim
            lblPhone1.Text = oRow("Phone1").ToString.Trim
            lblAreaPhone2.Text = oRow("AreaPhone2").ToString.Trim
            lblPhone2.Text = oRow("Phone2").ToString.Trim
            lblAreaFax.Text = oRow("AreaFax").ToString.Trim
            lblFax.Text = oRow("Fax").ToString.Trim
            TxtAreaPhoneKantor.Text = oRow("OfficeAreaPhone").ToString.Trim
            TxtPhoneKantor.Text = oRow("OfficePhone").ToString.Trim
            TxtAreaPhoneGuarantor.Text = oRow("GuarantorAreaPhone").ToString.Trim
            TxtPhoneGuarantor.Text = oRow("GuarantorPhone").ToString.Trim
            lblMotherName.Text = oRow("MotherName").ToString.Trim
            lblJarakSurvey.Text = oRow("JarakSurvey").ToString.Trim
            lblGpsLatitude.Text = oRow("GPSLatitude").ToString.Trim
            lblGpsLongtitude.Text = oRow("GPSLongitude").ToString.Trim
            Me.CustomerType = oRow("CustomerType").ToString.Trim
            fillCboIDType()
            cboIDTypeP.SelectedIndex = cboIDTypeP.Items.IndexOf(cboIDTypeP.Items.FindByValue(oRow("IDType").ToString.Trim))
            rdoReferensi.Attributes.Add("onclick", "rdoReferensiChange();")
        End If
    End Sub
#End Region

#Region "Save"
    Private Sub imbSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If Me.Page.IsValid Then
                lblMessage.Visible = False

                Dim oApplication As New Parameter.Prospect
                oApplication.strConnection = GetConnectionString()
                oApplication.BusinessDate = Me.BusinessDate
                oApplication.BranchId = Replace(Me.sesBranchId, "'", "").ToString
                oApplication.ProductID = ""
                oApplication.CustomerID = ""
                'oApplication.CustomerType = "P"
                oApplication.CustomerType = Me.CustomerType
                'oApplication.Name = txtNama.Text
                oApplication.Name = lblNama.Text
                'oApplication.ProspectAppDate = IIf(txttanggalAplikasi.Text <> "", ConvertDate(txttanggalAplikasi.Text), "").ToString
                oApplication.ProspectAppDate = IIf(lbltanggalAplikasi.Text <> "", ConvertDate(lbltanggalAplikasi.Text), "").ToString
                'oApplication.BirthPlace = txtTempatLahir.Text
                oApplication.BirthPlace = lblTempatLahir.Text
                'oApplication.BirthDate = IIf(txtTanggalLahir.Text <> "", ConvertDate(txtTanggalLahir.Text), "").ToString
                oApplication.BirthDate = IIf(lblTanggalLahir.Text <> "", ConvertDate(lblTanggalLahir.Text), "").ToString
                'oApplication.Address = ucMailingAddress.Address
                'oApplication.RT = ucMailingAddress.RT
                'oApplication.RW = ucMailingAddress.RW
                'oApplication.Kecamatan = ucMailingAddress.Kecamatan
                'oApplication.Kelurahan = ucMailingAddress.Kelurahan
                'oApplication.ZipCode = ucMailingAddress.ZipCode
                'oApplication.City = ucMailingAddress.City
                'oApplication.AreaPhone1 = ucMailingAddress.AreaPhone1
                'oApplication.AreaPhone2 = ucMailingAddress.AreaPhone2
                'oApplication.AreaFax = ucMailingAddress.AreaFax
                'oApplication.Phone1 = ucMailingAddress.Phone1
                'oApplication.Phone2 = ucMailingAddress.Phone2
                'oApplication.Fax = ucMailingAddress.Fax
                'oApplication.MobilePhone = txtMobilePhone.Text

                oApplication.Address = lblAlamat.Text
                oApplication.RT = lblRT.Text
                oApplication.RW = LblRW.Text
                oApplication.Kecamatan = lblKecamatan.Text
                oApplication.Kelurahan = lblKelurahan.Text
                oApplication.ZipCode = lblZipCode.Text
                oApplication.City = lblCity.Text
                oApplication.AreaPhone1 = lblAreaPhone1.Text
                oApplication.AreaPhone2 = lblAreaPhone2.Text
                oApplication.AreaFax = lblAreaFax.Text
                oApplication.Phone1 = lblPhone1.Text
                oApplication.Phone2 = lblPhone2.Text
                oApplication.Fax = lblFax.Text
                oApplication.MobilePhone = lblMobilePhone.Text

                'oApplication.JarakSurvey = CInt(IIf(IsNumeric(ucJarakSurvey.Text), ucJarakSurvey.Text, "0"))
                oApplication.JarakSurvey = CInt(IIf(IsNumeric(lblJarakSurvey.Text), lblJarakSurvey.Text, "0"))
                oApplication.IDType = cboIDTypeP.SelectedValue
                'oApplication.IDNumber = txtIDNumberP.Text.Trim
                oApplication.IDNumber = lblIDNumberP.Text.Trim
                oApplication.IsReferenced = rdoReferensi.SelectedValue
                oApplication.PhoneReference = txtPhoneReferensi.Text
                oApplication.KetReference = txtKeteranganReferal.Text
                oApplication.StatusNasabah = rdostatusnasabah.SelectedValue
                oApplication.Mode = Me.PageMode
                oApplication.LoginId = Me.Loginid
                If Me.PageMode <> "Edit" Then
                    oApplication.ProspectAppID = ""
                Else
                    oApplication.ProspectAppID = Me.ProspectAppID
                End If

                oApplication.OfficeAreaPhone = TxtAreaPhoneKantor.Text
                oApplication.OfficePhone = TxtPhoneKantor.Text
                oApplication.GuarantorAreaPhone = TxtAreaPhoneGuarantor.Text
                oApplication.GuarantorPhone = TxtPhoneGuarantor.Text


                Dim ErrorMessage As String = ""
                Dim oReturn As New Parameter.Prospect

                oReturn = oController.ProspectSaveAdd(oApplication)

                If oReturn.Err <> "" Then
                    ShowMessage(lblMessage, ErrorMessage, True)
                Else
                    'ucProspectTab1.ProspectAppID = oReturn.ProspectAppID
                    'ucProspectTab1.setLink()
                    'Me.PageMode = "Edit"
                    ShowMessage(lblMessage, "Data saved!", False)
                    Response.Redirect("ProspectAsset.aspx?id=" & oReturn.ProspectAppID.Trim & "&page=Edit&ActivityDateStart=" & Me.ActivityDateStart & "")
                End If
            Else
                ShowMessage(lblMessage, "Data Sudah Ada", True)
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub



#End Region

    Protected Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("ProspectApplication.aspx")
    End Sub


    Private Sub btnNext_Click(sender As Object, e As System.EventArgs) Handles btnNext.Click
        btnNext.Visible = False
        btnSave.Visible = True
        txtPhoneReferensi.Enabled = False
        txtKeteranganReferal.Enabled = False
        BindGrid1()
        BindGrid2()
        PanelInfo.Visible = True
    End Sub

    Private Sub BindGrid1()
        Dim dtEntity As New DataTable
        With oCustomer
            '.Name = txtNama.Text
            .Name = lblNama.Text
            .IDType = cboIDTypeP.SelectedValue
            '.IDNumber = txtIDNumberP.Text
            .IDNumber = lblIDNumberP.Text
            '.BirthDate = IIf(txtTanggalLahir.Text <> "", ConvertDateSql(txtTanggalLahir.Text), "").ToString
            .BirthDate = IIf(lblTanggalLahir.Text <> "", ConvertDateSql(lblTanggalLahir.Text), "").ToString
            .strConnection = GetConnectionString()
            oCustomer = m_controller.BindCustomer1_002(oCustomer)
            If Not oCustomer Is Nothing Then
                dtEntity = oCustomer.listdata
            End If
        End With
        dtgList1.DataSource = dtEntity.DefaultView
        dtgList1.CurrentPageIndex = 0
        dtgList1.DataBind()
    End Sub

    Private Sub BindGrid2()
        Dim dtEntity As New DataTable
        With oCustomer
            '.Name = txtNama.Text
            .Name = lblNama.Text
            .IDType = cboIDTypeP.SelectedValue
            '.IDNumber = txtIDNumberP.Text
            .IDNumber = lblIDNumberP.Text
            '.BirthDate = IIf(txtTanggalLahir.Text <> "", ConvertDateSql(txtTanggalLahir.Text), "").ToString
            .BirthDate = IIf(lblTanggalLahir.Text <> "", ConvertDateSql(lblTanggalLahir.Text), "").ToString
            .strConnection = GetConnectionString()
            oCustomer = m_controller.BindCustomer2_002(oCustomer)
            If Not oCustomer Is Nothing Then
                dtEntity = oCustomer.listdata
            End If
        End With

        If Not oCustomer Is Nothing Then
            dtEntity = oCustomer.listdata
            For Each r As DataRow In dtEntity.Rows
                If r.Item("type").ToString.Trim = "Name + Birth Date + ID Number" Then
                    btnSave.Visible = False
                End If
            Next
        End If
        dtgList2.DataSource = dtEntity.DefaultView
        dtgList2.CurrentPageIndex = 0
        dtgList2.DataBind()
    End Sub

    Private Sub dtgList1_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgList1.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            CType(e.Item.FindControl("lblNo1"), Label).Text = CStr(e.Item.ItemIndex + 1)
            Dim lnkCustomer As LinkButton
            Dim lblCustomer As Label
            lnkCustomer = CType(e.Item.FindControl("lnkCustName"), LinkButton)
            lblCustomer = CType(e.Item.FindControl("lblCust"), Label)
            lnkCustomer.Attributes.Add("OnClick", "return OpenCust('" & lblCustomer.Text & "','Marketing');")
        End If
    End Sub

    Private Sub dtgList2_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgList2.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            CType(e.Item.FindControl("lblNo2"), Label).Text = CStr(e.Item.ItemIndex + 1)
            Dim lnkCustomer As LinkButton
            Dim lblCustomer As Label
            lnkCustomer = CType(e.Item.FindControl("lnkCustName2"), LinkButton)
            lblCustomer = CType(e.Item.FindControl("lblCust2"), Label)
            lnkCustomer.Attributes.Add("OnClick", "return OpenCust('" & lblCustomer.Text & "','Marketing');")
        End If
    End Sub
    'modify by nofi date 30Jan2018 => perbaikan hasil testing pak agus
    Protected Function rdoReferensiChange() As String
        Return "rdoReferensiChange();"
    End Function
End Class