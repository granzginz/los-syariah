﻿Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
Imports Maxiloan.General.CommonCacheHelper

Public Class Reports
    Inherits Maxiloan.Webform.WebBased
    'Protected WithEvents ucRSViewer1 As ucRSViewer

    'Private Property strReportFile As String

    Private m_controller As New DataUserControlController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack Then

            Dim strReportFile As String = "../Webform.UserController/ucRSViewerPopup.aspx?formid=" & Request("formid") & "&rsname=" & Request("rsname")

            Response.Write("<script language = javascript>" & vbCrLf _
            & "var x = screen.width; " & vbCrLf _
            & "var y = screen.height; " & vbCrLf _
            & "window.open('" & strReportFile & "','_blank', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes') " & vbCrLf _
            & "</script>")
        Else
            Me.FormID = Request("formid")

            bindBranch()

            Select Case Me.FormID
                Case "RSSALESCMO"
                    'hide or show filter controls
            End Select
        End If
    End Sub

    Private Sub bindBranch()

        Dim DtBranchName As New DataTable
        DtBranchName = CType(Me.Cache.Item(CACHE_BRANCH_ALL), DataTable)
        If DtBranchName Is Nothing Then
            Dim DtBranchNameCache As New DataTable
            DtBranchNameCache = m_controller.GetBranchAll(GetConnectionString)
            Me.Cache.Insert(CACHE_BRANCH_ALL, DtBranchNameCache, Nothing, DateTime.Now.AddHours(DEFAULT_TIME_DURATION_CACHE_HOUR), TimeSpan.Zero)
            DtBranchName = CType(Me.Cache.Item(CACHE_BRANCH_ALL), DataTable)
        End If

        With chkBranch
            .DataTextField = "Name"
            .DataValueField = "ID"
            .DataSource = DtBranchName
            .DataBind()
        End With

    End Sub

    Private Sub btnView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnView.Click  
        createCookies()
    End Sub


    Sub createCookies()
        Dim oCookies As HttpCookie = Request.Cookies("RSCOOKIES")

        If oCookies Is Nothing Then
            Dim oCookiesNew As New HttpCookie("RSCOOKIES")

            With oCookiesNew
                .Values.Add("Cabang", selectedBranch)
                .Values.Add("StartDate", "01/01/2012")
                .Values.Add("EndDate", "01/31/2012")
            End With

            Response.AppendCookie(oCookiesNew)
        Else
            With oCookies
                .Values("Cabang") = selectedBranch()
                .Values("StartDate") = "01/01/2012"
                .Values("EndDate") = "01/31/2012"
            End With

            Response.AppendCookie(oCookies)
        End If
    End Sub


    Private Function selectedBranch() As String
        Dim strID As New StringBuilder

        For Each oItem As ListItem In chkBranch.Items
            If oItem.Selected Then
                strID.Append(IIf(strID.ToString = "", oItem.Value, "," & oItem.Value))
            End If
        Next

        Return strID.ToString
    End Function


End Class