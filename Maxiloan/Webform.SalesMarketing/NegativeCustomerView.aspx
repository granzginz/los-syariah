﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="NegativeCustomerView.aspx.vb"
    Inherits="Maxiloan.Webform.SalesMarketing.NegativeCustomerView" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">    
    <title>NegativeCustomerView</title>
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />   
    <script type="text/javascript" language="JavaScript">
        function fClose() {
            window.close();
            return false;
        }			
	</script>
</head>
<body>
    <form id="form1" runat="server">
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">               
            <h3>
                BLACKLIST KONSUMEN - VIEW
            </h3>
        </div>
    </div>     
    <div class="form_box_header">
        <div class="form_single">              
            <h4>
                    DATA KONSUMEN
            </h4>
        </div>
    </div>
    <div class="form_box">
	    <div class="form_single">
            <label>Nama</label>
            <asp:Label ID="lblName" runat="server"></asp:Label>
	    </div>
    </div> 
    <div class="form_box">
	    <div class="form_single">
            <label>Jenis ID Dokumen</label>
            <asp:Label ID="lblIDType" runat="server"></asp:Label>
	    </div>
    </div> 
    <div class="form_box">
	    <div class="form_single">
            <label>No ID Dokumen</label>
            <asp:Label ID="lblIDNumber" runat="server"></asp:Label>
	    </div>
    </div> 
    <div class="form_box">
	    <div class="form_single">
            <label>Tempat / Tanggal Lahir</label>
            <asp:Label ID="lblBirth" runat="server"></asp:Label>
	    </div>
    </div> 
    <div class="form_box_header">
        <div class="form_single">              
            <h4>
                    ALAMAT
            </h4>
        </div>
    </div>
    <div class="form_box">
	    <div class="form_single">
            <label>Alamat</label>
            <asp:Label ID="lblAddress" runat="server"></asp:Label>
	    </div>
    </div> 
    <div class="form_box">
	    <div class="form_single">
            <label>RT/RW</label>
            <asp:Label ID="lblRT" runat="server"></asp:Label>&nbsp;/
            <asp:Label ID="lblRW" runat="server"></asp:Label>
	    </div>
    </div>
    <div class="form_box">
	    <div class="form_single">
            <label>Kelurahan</label>
            <asp:Label ID="lblKelurahan" runat="server"></asp:Label>
	    </div>
    </div>
    <div class="form_box">
	    <div class="form_single">
            <label>Kecamatan</label>
            <asp:Label ID="lblKecamatan" runat="server"></asp:Label>
	    </div>
    </div>
    <div class="form_box">
	    <div class="form_single">
            <label>Kota</label>
            <asp:Label ID="lblCity" runat="server"></asp:Label>
	    </div>
    </div>
    <div class="form_box">
	    <div class="form_single">
            <label>Kode Pos</label>
            <asp:Label ID="lblZipCode" runat="server"></asp:Label>
	    </div>
    </div> 
    <div class="form_box">
	    <div class="form_single">
            <label>No Telepon-1</label>
            <asp:Label ID="lblPhone1" runat="server"></asp:Label>
	    </div>
    </div>
    <div class="form_box">
	    <div class="form_single">
            <label>No Telepon-2</label>
            <asp:Label ID="lblPhone2" runat="server"></asp:Label>
	    </div>
    </div>
    <div class="form_box">
	    <div class="form_single">
            <label>No Fax</label>
            <asp:Label ID="lblFaxNo" runat="server"></asp:Label>
	    </div>
    </div>
    <div class="form_box">
	    <div class="form_single">
            <label>No HandPhone</label>
            <asp:Label ID="lblMobilePhone" runat="server"></asp:Label>
	    </div>
    </div>
    <div class="form_box">
	    <div class="form_single">
            <label>e-mail</label>
            <asp:Label ID="lblEmail" runat="server"></asp:Label>
	    </div>
    </div>
    <div class="form_box_header">
        <div class="form_single">              
            <h4>
                DATA BLACKLIST
            </h4>
        </div>
    </div>
    <div class="form_box">
	    <div class="form_single">
            <label>SKT</label>	
            <asp:Label ID="lblSKT" runat="server"></asp:Label>          
	    </div>
    </div>
    <div class="form_box">
	    <div class="form_single">
            <label>Bad Type</label>
            <asp:Label ID="lblBadType" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
	    <div class="form_single">
            <label>Sumber Data</label>
            <asp:Label ID="lblDataSource" runat="server"></asp:Label>
        </div>
    </div>   
    <div class="form_box">
	    <div class="form_single">
            <label>Reject Sebelum Survey</label>		
            <asp:Label ID="lblBefore" runat="server"></asp:Label>
	    </div>
    </div>
     <div class="form_box">
	    <div class="form_single">
            <label>Masih Aktif</label>
            <asp:Label ID="lblisActive" runat="server"></asp:Label>
        </div>
    </div>     
    <div class="form_box">
	    <div class="form_single">
            <label>Reject Setelah Survey</label>
            <asp:Label ID="lblAfter" runat="server"></asp:Label>
	    </div>
    </div>
    <div class="form_box">
	    <div class="form_single">
            <label>Catatan</label>
            <asp:Label ID="lblNotes" runat="server"></asp:Label>
	    </div>
    </div> 
    <div class="form_button">            
        <asp:Button ID="ButtonCancel" OnClientClick="fClose();" runat="server" CausesValidation="false" Text="Close" CssClass ="small button gray">
        </asp:Button>     
    </div>         
    </form>    
</body>
</html>
