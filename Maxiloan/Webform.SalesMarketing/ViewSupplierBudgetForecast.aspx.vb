﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports System.IO
Imports Maxiloan.Controller
#End Region

Public Class ViewSupplierBudgetForecast
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private m_controller As New SupplierController
    Dim oSupplier As New Parameter.Supplier
#End Region

#Region "Property"
    Private Property SupplierID() As String
        Get
            Return CType(viewstate("SupplierID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("SupplierID") = Value
        End Set
    End Property
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub

        End If

        If Not IsPostBack Then
            ButtonClose.Attributes.Add("onclick", "windowClose()")
            Me.BranchID = Request.QueryString("BranchID")
            Me.SupplierID = Request.QueryString("SupplierID")

        End If
        pnlGrid.Visible = False
        lblBranchID.Text = Me.BranchID.Trim
        lblSupplierID.Text = Me.SupplierID.Trim
        With oSupplier
            .strConnection = GetConnectionString
            .SupplierID = Me.SupplierID.Trim
        End With
        Try
            oSupplier = m_controller.SupplierView(oSupplier)
        Catch ex As Exception

        End Try
        lblSupplierGroup.Text = oSupplier.ListData.Rows(0).Item("SupplierGroupID").ToString
        lblSupplierName.Text = oSupplier.ListData.Rows(0).Item("SupplierName").ToString

    End Sub
#Region " Event Handlers "
    Private Sub imbView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonView.Click
        pnlGrid.Visible = True
        With oSupplier
            .strConnection = GetConnectionString()
            .BranchId = Me.BranchID.Trim
            .SupplierID = Me.SupplierID.Trim
            .Year = CInt(txtYear.Text.Trim)
        End With
        Try
            oSupplier = m_controller.SupplierViewBudgetForecast(oSupplier)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        Dim odata As New DataTable
        If Not oSupplier Is Nothing Then
            odata = oSupplier.ListData
        End If
        If odata.Rows.Count > 0 Then
            DtgBudgetAdd.DataSource = odata.DefaultView
            DtgBudgetAdd.DataBind()
            DtgBudgetAdd.Visible = True
        End If
    End Sub
#End Region


#Region " Sub And Functions "
    Protected Function GetSumUnit(ByVal UnitNew As Integer, ByVal unitUsed As Integer) As Integer
        Dim hasilunit As Integer
        hasilunit = UnitNew + unitUsed
        Return hasilunit
    End Function
    Protected Function GetSumAmount(ByVal UnitNew As Double, ByVal unitUsed As Double) As Double
        Dim hasilamount As Double
        hasilamount = UnitNew + unitUsed
        Return hasilamount
    End Function
#End Region

End Class