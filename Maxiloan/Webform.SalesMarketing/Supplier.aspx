﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Supplier.aspx.vb" Inherits="Maxiloan.Webform.SalesMarketing.Supplier" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ucCompanyAddress" Src="../Webform.UserController/ucCompanyAddress.ascx" %>
<%@ Register Src="../Webform.UserController/ucDateCE.ascx" TagName="ucDateCE" TagPrefix="uc2" %>
<%@ Register Src="../Webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat"
    TagPrefix="uc3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Supplier</title>
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />
    <link href="../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%#Request.servervariables("SERVER_NAME")%>/';

        //        function SellAutomotiveAsset() {
        //            var oSell = document.forms[0].rboSell;
        //            var oBPKB = document.forms[0].rboBPKB;
        //            var oCategory = document.forms[0].rboCategory;
        //            var oAssetSold = document.forms[0].rboAssetSold;

        //            alert(oSell.checked);

        //            if (oSell.options[oSell.selectedIndex].value == "0") {
        //                alert('Value rboSell = 0');
        //                oBPKB.options[oBPKB.selectedIndex].value = "0";
        //                //alert(oBPKB.options[oSell.selectedIndex].value);
        //                oBPKB.disabled = true;

        //                oCategory.options[oCategory.selectedIndex].value = "OT";
        //                //alert(oCategory.options[oSell.selectedIndex].value);
        //                oCategory.disabled = true;

        //                oAssetSold.options[oAssetSold.selectedIndex].value = "M";
        //                //alert(oAssetSold.options[oSell.selectedIndex].value);
        //                oAssetSold.disabled = true;
        //            }
     
    </script>
    <script language="javascript" type="text/javascript">
        function showimagepreview(input, imgClient) {
            if (input.files && input.files[0]) {
                var filerdr = new FileReader();
                filerdr.onload = function (e) {
                    $('#' + imgClient).attr('src', e.target.result);
                }
                filerdr.readAsDataURL(input.files[0]);
            }
        }

    </script>

</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <div runat="server" id="jlookupContent" />
    <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                DAFTAR SUPPLIER
            </h3>
        </div>
    </div>
    <asp:Panel ID="pnlList" runat="server" Width="100%">
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgPaging" runat="server" Width="100%" CssClass="grid_general"
                        DataKeyField="Name" BorderStyle="None" BorderWidth="0" OnSortCommand="Sorting"
                        AutoGenerateColumns="False" AllowSorting="True">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="EDIT">
                                <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" ImageUrl="../Images/iconedit.gif"
                                        CommandName="Edit"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DELETE">
                                <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgDelete" runat="server" CausesValidation="False" ImageUrl="../Images/icondelete.gif"
                                        CommandName="Delete"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="PKS" HeaderText="PKS">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgPrint" runat="server" CausesValidation="False" ImageUrl="../Images/IconPrinter.gif"
                                        CommandName="Print"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="supplierID" HeaderText="ID">
                                <ItemTemplate>
                                    <asp:Label ID="lnkID" runat="server" Text='<%# DataBinder.eval(Container,"DataItem.supplierID") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Name" HeaderText="NAMA SUPPLIER">
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkName" runat="server" Text='<%# DataBinder.eval(Container,"DataItem.Name") %>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn Visible="False" DataField="CPName" SortExpression="CPName" HeaderText="KONTAK">
                            </asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="Phone" SortExpression="Phone" HeaderText="TELEPON">
                            </asp:BoundColumn>
                            <asp:TemplateColumn SortExpression="AssetTypeID" HeaderText="PRODUK">
                                <ItemTemplate>
                                    <asp:Label ID="lnkProduk" runat="server" Text='<%# DataBinder.eval(Container,"DataItem.AssetTypeID") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="CABANG">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkBranch" runat="server" CommandName="Branch" CausesValidation="False">Cabang</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="PEMILIK">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkOwner" runat="server" CommandName="Owner" CausesValidation="False">Pemilik</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" HeaderText="BUDGET">
                                <ItemTemplate>
                                    <asp:Label ID="lblBudget" runat="server">Budget</asp:Label>
                                    <asp:LinkButton ID="lnkBudget" Visible="false" runat="server" CommandName="Budget"
                                        CausesValidation="False">Budget</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" HeaderText="FORECAST">
                                <ItemTemplate>
                                    <asp:Label ID="lblForecast" runat="server">ForeCast</asp:Label>
                                    <asp:LinkButton ID="lnkForecast" runat="server" Visible="False" CommandName="ForeCast"
                                        CausesValidation="False">ForeCast</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="KARYAWAN">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkEmployee" runat="server" CommandName="Employee" CausesValidation="False">Karyawan</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <%--modify by nofi tgl 30jan2018 =>hasil testing pak agus--%>
                           <%-- <asp:TemplateColumn HeaderText="RELASI" >
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkEmployeeRelasi" runat="server" CommandName="Relasi" CausesValidation="False">Relasi</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>--%>
                            <asp:TemplateColumn HeaderText="REKENING BANK">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkBankAcc" runat="server" CommandName="Account" CausesValidation="False">Rekening Bank</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" HeaderText="TANDATANGAN">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkSignature" runat="server" CommandName="Signature" CausesValidation="False">Signature</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                    <div class="button_gridnavigation">
                        <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../Images/grid_navbutton01.png"
                            CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../Images/grid_navbutton02.png"
                            CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../Images/grid_navbutton03.png"
                            CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../Images/grid_navbutton04.png"
                            CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                        <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                            EnableViewState="False"></asp:Button>
                        <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" MinimumValue="1"
                            ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                            ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                    <div class="label_gridnavigation">
                        <asp:Label ID="lblPage" runat="server"></asp:Label>of
                        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                        <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                    </div>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="imbButtonAdd" runat="server" CausesValidation="False" Text="Add"
                CssClass="small button blue"></asp:Button>
        </div>
        <div class="form_box_title">
            <div class="form_single">
                <h4>
                    CARI SUPPLIER
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cari Berdasarkan</label>
                <asp:DropDownList ID="cboSearch" runat="server">
                    <asp:ListItem Value="Name">Nama Supplier</asp:ListItem>
                    <asp:ListItem Value="SupplierID">Supplier ID</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearch" runat="server" Width="20%"></asp:TextBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="imbButtonSearch" runat="server" CausesValidation="False" CssClass="small button blue"
                Text="Search"></asp:Button>
            <asp:Button ID="imbButtonReset" runat="server" CausesValidation="False" Text="Reset"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlAddEdit" runat="server" Width="100%">
        <div class="form_box_header">
            <div class="form_single">
                <h4>
                    <asp:Label ID="lblTitle" runat="server"></asp:Label>
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Supplier ID
                </label>
                <%-- <asp:Label ID="lblsuppID" runat="server"></asp:Label>--%>
             <%--   <asp:TextBox ID="lblID" runat="server" Width="20%" MaxLength="50"></asp:TextBox>--%>
                  <asp:Label ID="lblID" runat="server" Width="20%" MaxLength="50"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    Nama Lengkap Supplier
                </label>
                <asp:Label ID="lblsuppID" runat="server" Visible="False"></asp:Label>
                <asp:TextBox ID="txtName" runat="server" Width="20%" MaxLength="50"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="harap isi Nama Supplier"
                    ControlToValidate="txtName" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator CssClass="validator_general" ID="RegularExpressionValidator3"
                    runat="server" ErrorMessage="Nama Lengkap Supplier tidak boleh isi dengan angka" ControlToValidate="txtName"
                    Display="Dynamic" ValidationExpression="^([A-Za-z ]+)$" />


            </div>
        </div>
        <div class="form_box">
            <%--edit npwp, ktp, telepon by ario--%>
            <div class="form_left">
                <label class="label_req">
                    NPWP</label>
                <asp:TextBox ID="txtNPWP" runat="server" MaxLength="30" Columns="35"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="harap isi NPWP"
                    ControlToValidate="txtNPWP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator" runat="server" ControlToValidate="txtNPWP"
                    ErrorMessage="Harap isi NPWP Angka" ValidationExpression="\d*" CssClass="validator_general"></asp:RegularExpressionValidator>
                    <%--<label class="label_req">NPWP
                    </label>
                <asp:TextBox ID="txtNPWP" runat="server" Width="20%" MaxLength="50"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" CssClass="validator_general"  runat="server" ErrorMessage="harap isi NPWP" ControlToValidate="txtNPWP"
                    Display="Dynamic"></asp:RequiredFieldValidator>
                 <asp:RegularExpressionValidator ID="Regularexpressionvalidator6" runat="server" Display="Dynamic"
                ValidationExpression="[0-9]{0,10}" ErrorMessage="Harap isi NPWP dengan Angka"
                ControlToValidate="txtNPWP" CssClass="validator_general"></asp:RegularExpressionValidator>--%>
                   
            </div>
            <div class="form_right">
                <label>
                    Peseorangan</label>
                <asp:CheckBox ID="chkPerseorangan" runat="server" /><br />
                <label>
                    SKBP</label>
                <asp:CheckBox ID="chkSKBP" runat="server" />
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    TDP</label>
                <asp:TextBox ID="txtTDP" runat="server" MaxLength="30" Columns="35"></asp:TextBox>&nbsp;
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    SIUP</label>
                <asp:TextBox ID="txtSIUP" runat="server" MaxLength="30" Columns="35"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Produk</label>
                <asp:DropDownList ID="cboProduk" runat="server">
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Group Supplier</label>
                <asp:HiddenField ID="hdnGroupSupplierCode" runat="server" />
                <asp:TextBox ID="txtGroupSupplierName" runat="server" MaxLength="30" Columns="35"></asp:TextBox>
                <button class="small buttongo blue" onclick="OpenJLookup('<%= ResolveClientUrl("~/webform.Utility/jLookup/SupplierGroup.aspx?kode=" & hdnGroupSupplierCode.ClientID & "&nama=" & txtGroupSupplierName.ClientID) %>','Daftar Group Supplier','<%= jlookupContent.ClientID %>');return false;">
                    ...</button>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Mulai Tanggal PKS
                </label>
                <uc2:ucdatece id="StartDate" runat="server" />
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    No PKS</label>
                <asp:Label ID="txtNoPKS" runat="server" MaxLength="30" Columns="35"></asp:Label>
            </div>
        </div>
         <div class="form_box">
                <div>
                    <div class="form_left">
                        <label class="label_general">Upload PKS</label>  
                        <asp:FileUpload ID="uplProsedurKYC" runat="server" />
                        <asp:HyperLink ID="hypProsedurKYC" runat="server" />
                    </div>
                    <div class="form_right">
                            <asp:Button ID="btnupload" runat="server" Text="Upload" CssClass="small button blue"  />
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label class="label_general">Upload Memo Approval</label>  
                        <asp:FileUpload ID="uplApproval" runat="server" />
                        <asp:HyperLink ID="hyApproval" runat="server" />
                    </div>
                    <div class="form_right">
                            <asp:Button ID="btnuploadApproval" runat="server" Text="Upload" CssClass="small button blue"  />
                    </div>
                </div>
            </div>
        <div class="form_box" style="display: none">
            <div class="form_single">
                <label class="label_general">
                    Private Financing</label>
                <asp:RadioButtonList ID="rboPF" runat="server" class="opt_single" RepeatDirection="Horizontal"
                    AutoPostBack="true">
                    <asp:ListItem Value="True">Yes</asp:ListItem>
                    <asp:ListItem Value="False" Selected="True">No</asp:ListItem>
                </asp:RadioButtonList>
            </div>
        </div>
        <div class="form_box_hide">
            <div class="form_single">
                <label>
                    Incentive Card Asset Baru</label>
                <asp:DropDownList ID="cboICNew" runat="server">
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_box_hide">
            <div class="form_single">
                <label>
                    Incentive Card Asset Bekas</label>
                <asp:DropDownList ID="cboICUsed" runat="server">
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_box_hide">
            <div class="form_single">
                <label>
                    Incentive Card untuk Repeat(RO)</label>
                <asp:DropDownList ID="cboICRO" runat="server">
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <h4>
                    Alamat
                </h4>
            </div>
        </div>
        <div class="form_box_uc">
            <uc1:uccompanyaddress id="ucAddress" runat="server"></uc1:uccompanyaddress>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <h4>
                    Alamat NPWP
                </h4>
            </div>
        </div>
        <div class="form_box_uc">
            <uc1:uccompanyaddress id="ucAddressNPWP" runat="server"></uc1:uccompanyaddress>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <h4>
                    <strong>KONTAK</strong>
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    Nama
                </label>
                <asp:TextBox ID="txtCPName" runat="server" Width="20%" MaxLength="50"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" CssClass="validator_general"  runat="server" ErrorMessage="harap isi nama kontak" ControlToValidate="txtCPName"
                    Display="Dynamic"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator CssClass="validator_general" ID="RegularExpressionValidator4"
                    runat="server" ErrorMessage="Nama tidak boleh isi dengan angka" ControlToValidate="txtCPName"
                    Display="Dynamic" ValidationExpression="^([A-Za-z ]+)$" />
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Jabatan</label>
                <asp:TextBox ID="txtCPJobTitle" runat="server" Width="20%" MaxLength="50"></asp:TextBox>
                <asp:RegularExpressionValidator CssClass="validator_general" ID="RegularExpressionValidator5"
                    runat="server" ErrorMessage="Jabatan tidak boleh isi dengan angka" ControlToValidate="txtCPJobTitle"
                    Display="Dynamic" ValidationExpression="^([A-Za-z ]+)$" />
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    E-mail</label>
                <asp:TextBox ID="txtCPEmail" runat="server" MaxLength="30" Width="20%"></asp:TextBox>&nbsp;
                <asp:RegularExpressionValidator CssClass="validator_general" ID="RegularExpressionValidator1"
                    runat="server" ErrorMessage="Email Address salah" ControlToValidate="txtCPEmail"
                    Display="Dynamic" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    No HandPhone</label>
                <asp:TextBox ID="txtCPMobile" runat="server" MaxLength="20" Columns="25"></asp:TextBox>&nbsp;
                <asp:RegularExpressionValidator CssClass="validator_general" ID="RegularExpressionValidator2"
                    runat="server" ErrorMessage="Harap isi dengan angka" ControlToValidate="txtCPMobile"
                    Display="Dynamic" ValidationExpression="\d*"></asp:RegularExpressionValidator>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <h4>
                    DATA SUPPLIER
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_general">
                    Bad Status</label>
                <asp:RadioButtonList Visible="false" ID="rboBadStatus" runat="server" class="opt_single"
                    RepeatDirection="Horizontal">
                    <asp:ListItem Value="N" Selected="True">Normal</asp:ListItem>
                    <asp:ListItem Value="W">Warning</asp:ListItem>
                    <asp:ListItem Value="B">Bad</asp:ListItem>
                </asp:RadioButtonList>
                <asp:TextBox ID="txtBadStatusNormal" Enabled="false" Visible="false" runat="server"
                    MaxLength="20" Columns="25" Text="Normal"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_general">
                    Level</label>
                <asp:RadioButtonList ID="rboLevel" runat="server" class="opt_single" RepeatDirection="Horizontal">
                    <asp:ListItem Value="U">Utama</asp:ListItem>
                    <asp:ListItem Value="A">Aktif</asp:ListItem>
                    <asp:ListItem Value="B" Selected="True">Biasa</asp:ListItem>
                    <asp:ListItem Value="P">Pasif</asp:ListItem>
                </asp:RadioButtonList>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_general">
                    Jual Asset Automotive</label>
                <asp:RadioButtonList ID="rboSell" runat="server" class="opt_single" RepeatDirection="Horizontal"
                    AutoPostBack="true">
                    <asp:ListItem Value="1" Selected="True">Yes</asp:ListItem>
                    <asp:ListItem Value="0">No</asp:ListItem>
                </asp:RadioButtonList>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <h4>
                    DATA AUTOMOTIVE
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_general">
                    Registrasi BPKB</label>
                <asp:RadioButtonList ID="rboBPKB" runat="server" class="opt_single" RepeatDirection="Horizontal">
                    <asp:ListItem Value="1" Selected="True">Yes</asp:ListItem>
                    <asp:ListItem Value="0">No</asp:ListItem>
                </asp:RadioButtonList>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_general">
                    Kategori</label>
                <asp:RadioButtonList class="opt_single" ID="rboCategory" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Value="MD" Selected="True">ATPM</asp:ListItem>
                    <asp:ListItem Value="AD">Authorized Dealer</asp:ListItem>
                    <asp:ListItem Value="SD">Sub Dealer</asp:ListItem>
                    <asp:ListItem Value="SR">Show Room</asp:ListItem>
                    <asp:ListItem Value="OT">Others</asp:ListItem>
                    <%--<asp:ListItem Value="PK">PK</asp:ListItem>--%>
                </asp:RadioButtonList>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_general">
                    Jual Asset</label>
                <asp:RadioButtonList ID="rboAssetSold" runat="server" class="opt_single" RepeatDirection="Horizontal">
                    <asp:ListItem Value="N">New</asp:ListItem>
                    <asp:ListItem Value="U">Used</asp:ListItem>
                    <asp:ListItem Value="M" Selected="True">Mixed</asp:ListItem>
                </asp:RadioButtonList>
            </div>
        </div>
        <div class="form_box_hide">
            <div class="form_single">
                <label class="label_general">
                    Penerapan TVC</label>
                <asp:RadioButtonList ID="rboPenerapanTVC" runat="server" class="opt_single" RepeatDirection="Horizontal">
                    <asp:ListItem Value="True">Yes</asp:ListItem>
                    <asp:ListItem Value="False">No</asp:ListItem>
                </asp:RadioButtonList>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="imbButtonSave" runat="server" CausesValidation="true" Text="Save"
                CssClass="small button blue"></asp:Button>
            <asp:Button ID="imbButtonCancel" runat="server" CausesValidation="false" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlAdd2" runat="server" Width="100%">
        <div class="form_box_header">
            <div class="form_single">
                <h4>
                    SUPPLIER
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Nama</label>
                <asp:HyperLink ID="lblName2" runat="server"></asp:HyperLink>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <h4>
                    SUPPLIER - CABANG
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgAdd" runat="server" Width="100%" DataKeyField="BranchID" AutoGenerateColumns="False"
                        AllowSorting="false" CssClass="grid_general" BorderStyle="None" BorderWidth="0">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="ASSIGN">
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkAssign" runat="server"></asp:CheckBox>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="BranchFullName" ItemStyle-HorizontalAlign="left" HeaderText="CABANG">
                            </asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="imbButtonOK" runat="server" CausesValidation="false" Text="Ok" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>

