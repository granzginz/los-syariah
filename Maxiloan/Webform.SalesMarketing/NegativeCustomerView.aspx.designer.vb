﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class NegativeCustomerView

    '''<summary>
    '''form1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents form1 As Global.System.Web.UI.HtmlControls.HtmlForm

    '''<summary>
    '''lblName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblName As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblIDType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblIDType As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblIDNumber control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblIDNumber As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblBirth control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblBirth As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblAddress control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblAddress As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblRT control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblRT As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblRW control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblRW As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblKelurahan control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblKelurahan As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblKecamatan control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblKecamatan As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblCity control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblCity As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblZipCode control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblZipCode As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblPhone1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPhone1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblPhone2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPhone2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblFaxNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblFaxNo As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblMobilePhone control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblMobilePhone As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblEmail control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblEmail As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblSKT control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblSKT As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblBadType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblBadType As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblDataSource control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblDataSource As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblBefore control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblBefore As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblisActive control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblisActive As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblAfter control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblAfter As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblNotes control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblNotes As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''ButtonCancel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ButtonCancel As Global.System.Web.UI.WebControls.Button
End Class
