﻿
#Region "Imports"
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCacheHelper
#End Region

Public Class SupplierBankAcc
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents UcBankAccount As UcBankAccount

    Private m_controller As New GroupSupplierAccountController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property AddEdit() As String
        Get
            Return CType(ViewState("vwsAddEdt"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsAddEdt") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property
    Private Property SupplierGroupID() As String
        Get
            Return CType(ViewState("SupplierGroupID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("SupplierGroupID") = Value
        End Set
    End Property
    Private Property BankID() As String
        Get
            Return CType(ViewState("BankID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("BankID") = Value
        End Set
    End Property
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not Me.IsPostBack Then
            lblMessage.Text = ""
            UcBankAccount.ShowMandatoryAll()
            Me.FormID = "GroupSupplierAccount"
            If CheckForm(Me.Loginid, Me.FormID, "MAXILOAN") Then
                txtgoPage.Text = "1"
                Me.Sort = "ID ASC"
                If (SupplierGroupID Is Nothing) Then
                    If hdnSupplierGroupID.Value <> "" Then
                        SupplierGroupID = hdnSupplierGroupID.Value
                    Else
                        SupplierGroupID = Request("id")
                    End If
                End If


                If SupplierGroupID <> "" Then
                    Me.CmdWhere = " SupplierGroupID = '" + SupplierGroupID.Trim() + "'".Trim()
                Else
                    Me.CmdWhere = "ALL"
                End If

                BindGridEntity(Me.CmdWhere)
                hdnSupplierGroupID.Value = SupplierGroupID
            End If
        End If

    End Sub
    Private Sub InitialDefaultPanel()
        pnlList.Visible = True
        pnlAddEdit.Visible = False
    End Sub
    Sub BindGridEntity(ByVal cmdWhere As String)
        Dim dtEntity As DataTable = Nothing
        Dim oGroupSupplierAccount As New Parameter.GroupSupplierAccount
        InitialDefaultPanel()
        oGroupSupplierAccount.strConnection = GetConnectionString()
        oGroupSupplierAccount.WhereCond = cmdWhere
        oGroupSupplierAccount.CurrentPage = currentPage
        oGroupSupplierAccount.PageSize = pageSize
        oGroupSupplierAccount.SortBy = Me.Sort
        oGroupSupplierAccount = m_controller.GetGroupSupplierAccount(oGroupSupplierAccount)

        If Not oGroupSupplierAccount Is Nothing Then
            dtEntity = oGroupSupplierAccount.Listdata
            recordCount = oGroupSupplierAccount.Totalrecords
        Else
            recordCount = 0
        End If
        If recordCount = 0 Then
            imbPrint.Enabled = False
        Else
            imbPrint.Enabled = True
        End If
        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()
        PagingFooter()
    End Sub
    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        Dim imbDelete As ImageButton
        Dim chekDefault As CheckBox
        Dim chekValue As HtmlInputHidden
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imbDelete"), ImageButton)
            imbDelete.Attributes.Add("Onclick", "return DeleteConfirm()")
            chekValue = CType(e.Item.FindControl("hdncheckDefault"), HtmlInputHidden)
            chekDefault = CType(e.Item.FindControl("checkDefault"), CheckBox)
            chekDefault.Checked = CBool(chekValue.Value)
        End If
    End Sub
#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            lblMessage.Text = "Data tidak ditemukan ....."
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lbltotrec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbGoPage.Click
        If IsNumeric(txtgoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtgoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtgoPage.Text, Int32)
                BindGridEntity(Me.CmdWhere)
            End If
        End If
    End Sub
#End Region
    Private Sub dtgEntity_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        Dim err As String
        Dim oGroupSupplierAccount As New Parameter.GroupSupplierAccount
        If e.CommandName = "Edit" Then
            If CheckFeature(Me.Loginid, Me.FormID, "Edit", "MAXILOAN") Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            Me.AddEdit = "EDIT"
            pnlAddEdit.Visible = True
            pnlList.Visible = False
            imbBack.Visible = False
            imbCancel.Visible = True
            imbSave.Visible = True
            imbClose.Visible = False

            lblTitleAddEdit.Text = Me.AddEdit
            oGroupSupplierAccount.ID = dtgPaging.DataKeys.Item(e.Item.ItemIndex).ToString
            oGroupSupplierAccount.strConnection = GetConnectionString()
            oGroupSupplierAccount = m_controller.GetGroupSupplierAccountList(oGroupSupplierAccount)
            imbCancel.CausesValidation = False

            hdnGroupSupplierAccount.Value = oGroupSupplierAccount.ID
            txtForPayment.Text = oGroupSupplierAccount.ForPayment.Trim
            EffectiveDate.Text = oGroupSupplierAccount.EffectiveDate.ToString("dd/MM/yyyy").Trim
            checkDefault.Checked = oGroupSupplierAccount.DefaultAccount
            BankID = oGroupSupplierAccount.GroupSupplierAccountBankID.Trim
            With UcBankAccount
                .BankName = oGroupSupplierAccount.GroupSupplierAccountBankName.Trim
                .BankBranchId = oGroupSupplierAccount.GroupSupplierAccountBankBranchID
                .AccountName = oGroupSupplierAccount.GroupSupplierAccountName.Trim
                .AccountNo = oGroupSupplierAccount.GroupSupplierAccountNo.Trim
                .BankBranch = oGroupSupplierAccount.GroupSupplierAccountBankName.Trim
                .BankID = oGroupSupplierAccount.GroupSupplierAccountBankID.Trim
                .Style = "Setting"
                .BindBankAccount()
            End With

        ElseIf e.CommandName = "Delete" Then
            If CheckFeature(Me.Loginid, Me.FormID, "Del", "MAXILOAN") Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            Dim customClass As New Parameter.GroupSupplierAccount
            With customClass
                .ID = dtgPaging.DataKeys.Item(e.Item.ItemIndex).ToString
                .strConnection = GetConnectionString()
            End With
            err = m_controller.GroupSupplierAccountDelete(customClass)
            If err <> "" Then
                lblMessage.Text = err
            Else
                lblMessage.Text = MessageHelper.MESSAGE_DELETE_SUCCESS
            End If
            BindGridEntity(Me.CmdWhere)
            txtgoPage.Text = "1"
        End If
    End Sub
    Private Sub imbSave_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbSave.Click
        Dim customClass As New Parameter.GroupSupplierAccount
        Dim ErrMessage As String = ""

        If Me.AddEdit = "ADD" Then
            BankID = UcBankAccount.BankID
        End If

        With customClass
            .SupplierGroupID = hdnSupplierGroupID.Value.Trim
            .DefaultAccount = checkDefault.Checked
            .LoginId = Me.Loginid.Trim
            .BusinessDate = Me.BusinessDate
            .EffectiveDate = Date.ParseExact(EffectiveDate.Text, "dd/MM/yyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo)
            .ForPayment = txtForPayment.Text.Trim
            .GroupSupplierAccountBankBranch = UcBankAccount.BankBranch
            .GroupSupplierAccountBankBranchID = Convert.ToInt32(UcBankAccount.BankBranchId)
            .GroupSupplierAccountBankID = BankID
            .GroupSupplierAccountName = UcBankAccount.AccountName
            .GroupSupplierAccountNo = UcBankAccount.AccountNo
            .strConnection = GetConnectionString()
        End With

        If Me.AddEdit = "ADD" Then
            ErrMessage = m_controller.GroupSupplierAccountSaveAdd(customClass)
            If ErrMessage <> "" Then
                lblMessage.Text = ErrMessage
                Exit Sub
            Else
                lblMessage.Text = MessageHelper.MESSAGE_INSERT_SUCCESS
                BindGridEntity(Me.CmdWhere)
            End If
        ElseIf Me.AddEdit = "EDIT" Then
            customClass.ID = CInt(hdnGroupSupplierAccount.Value)
            m_controller.GroupSupplierAccountSaveEdit(customClass)
            lblMessage.Text = MessageHelper.MESSAGE_UPDATE_SUCCESS
            BindGridEntity(Me.CmdWhere)
        End If
    End Sub
    Private Sub imbAdd_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbAdd.Click
        If CheckFeature(Me.Loginid, Me.FormID, "Add", "MAXILOAN") Then
            If SessionInvalid() Then
                Exit Sub
            End If
        End If
        pnlAddEdit.Visible = True
        pnlList.Visible = False
        imbBack.Visible = False
        imbCancel.Visible = True
        imbSave.Visible = True
        imbClose.Visible = False

        Me.AddEdit = "ADD"
        lblTitleAddEdit.Text = Me.AddEdit
        imbCancel.CausesValidation = False

        txtForPayment.Text = Nothing
        EffectiveDate.Text = Nothing
        checkDefault.Checked = False
        With UcBankAccount
            .BankName = Nothing
            .BankBranchId = Nothing
            .AccountName = Nothing
            .AccountNo = Nothing
            .BankBranch = Nothing
            .BankID = Nothing
            .Style = "Setting"
            .BindBankAccount()
        End With

    End Sub
    Private Sub imbPrint_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbPrint.Click
        If CheckFeature(Me.Loginid, Me.FormID, "Print", "MAXILOAN") Then
            If SessionInvalid() Then
                Exit Sub
            End If
        End If
        SendCookies()
        Response.Redirect("Report/GroupSupplierAccount.aspx")
    End Sub
    Sub SendCookies()
        Dim cookie As HttpCookie = Request.Cookies("GroupSupplierAccount")
        If Not cookie Is Nothing Then
            cookie.Values("where") = Me.CmdWhere
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie("GroupSupplierAccount")
            cookieNew.Values.Add("where", Me.CmdWhere)
            Response.AppendCookie(cookieNew)
        End If
    End Sub
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbReset.Click
        cboSearch.SelectedIndex = 0
        txtSearch.Text = ""
        Me.CmdWhere = " SupplierGroupID = '" + SupplierGroupID.Trim() + "'".Trim()
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub imbSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbSearch.Click
        If txtSearch.Text.Trim <> "" Then
            Me.CmdWhere = cboSearch.SelectedItem.Value + " = '" + txtSearch.Text.Trim + "'"
            If cboSearch.SelectedItem.Value = "DefaultAccount" Then
                Me.CmdWhere = ""
                Dim tmp1String As String = txtSearch.Text.Replace("false", "0")
                Dim tmp2String As String = tmp1String.Replace("true", "1")
                Me.CmdWhere = cboSearch.SelectedItem.Value + " = '" + tmp2String + "'"
            End If
            Me.CmdWhere += " and SupplierGroupID like '%" + SupplierGroupID.Trim() + "%'".Trim()
        Else
            Me.CmdWhere = " SupplierGroupID = '" + SupplierGroupID.Trim() + "'".Trim()
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub imbBackMenu_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbBackMenu.Click
        Response.Redirect("SupplierGroup.aspx")
    End Sub
    Private Sub imbBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbBack.Click
        Response.Redirect("SupplierBankAcc.aspx")
    End Sub
    Private Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbCancel.Click
        Response.Redirect("SupplierBankAcc.aspx?cmd=dtl&id=" + hdnSupplierGroupID.Value + "&desc=" + Request("desc") + "")
    End Sub
    Public Function ValidateChekbox() Handles CVcheckDefault.ServerValidate
        Dim valid As Boolean

        'dtgPaging.Items(i).Cells.Item(1).Text.Trim

        Dim grid As DataGrid = dtgPaging


        'For index = 1 To grid.Items

        'Next


        Return valid
    End Function
End Class