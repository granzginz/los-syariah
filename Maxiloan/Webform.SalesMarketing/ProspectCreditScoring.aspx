﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ProspectCreditScoring.aspx.vb" Inherits="Maxiloan.Webform.SalesMarketing.ProspectCreditScoring" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %>  
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Prospect Credit Scoring</title>
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <script src="../js/jquery-1.9.1.min.js"></script>
    <script src="../js/jquery-ui-1.10.3.custom.min.js"></script>

    
<script type="text/javascript">
    function doOK(e) {
        $("div.tx-overlay").css("display", "none");
        $("#divShowDlg").css("display", "none");

    }
    function showDialog() { 
            $("div.tx-overlay").css("display", "block");
            $("#divShowDlg").css("display", "block");
        }

//        $("#modal_dialog").dialog({
//            title: "jQuery Modal Dialog Popup",
//            buttons: {
//                Close: function () {
//                    $(this).dialog('close');
//                }
//            },
//            modal: true
//        });
//        return false;
//        };
</script>


 <style>
        .tr-det { 
         background:  none repeat-x scroll 0 0 gray;
        }
        .red{
            text-decoration:none;
            color:Red;
            border-color: #000;
            } 
        .tx-overlay {
                    background-color: #000;
                    height: 100%;
                    left: 0;
                    opacity: 0.5;
                    position: fixed;
                    top: 0;
                    width: 100%;
                    z-index: 10001;
                }
        .t-window {
            border-radius: 5px;
            border-width: 2px;
            box-shadow: 0 0 5px 2px #aaa;
            display: inline-block;
            position: absolute;
            z-index: 10001;
            background-color: #fff;
            border-color: #a7bac5;
        }
        div.af05{
            background-position:center;
            background-image: url('../images/journalvoucher.png');
            background-repeat:no-repeat;
              height: 305px;
            margin:0; /* If you want no margin */
            padding:0; /*if your want to padding */
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
   <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();" />

   
   <div class="tx-overlay" style="opacity: 0.5; z-index: 1000; display: none;"></div> 
    <div id="divShowDlg" class="t-widget t-window" style="top: 80px; left:80px; width: 1100px;  display:none;">
        <div style="margin:20px;height: 298px;">

            <div class="form_title">
                <div class="title_strip"> </div>
                <div class="form_single"> </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class=""> Prospek Id </label>
                        <asp:Label ID="lblProspekId" runat="server" ></asp:Label>
                    </div>
                </div>
            </div>

            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="">Score </label>
                        <asp:Label ID="lblScoreResult" runat="server" ></asp:Label>
                    </div>
                </div>
            </div>
            
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="">Decision</label>
                        <label class="">MANUAL ASSESSMENT</label>
                    </div>
                </div>
            </div>

            <div class="form_button">
                <asp:Button ID="btnGoToSurvey" runat="server" Text="Go To Survey" CssClass="small button blue" CausesValidation="false"></asp:Button>
                <asp:Button ID="btnReject" runat="server" Text="Reject" CssClass="small button blue" CausesValidation="False"></asp:Button>
            </div>

        </div>
            
    </div>



    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>DAFTAR PROSPEK</h3>
        </div>
    </div>
    <asp:Panel runat="server" ID="pnlList">


     <div class="form_title">
            <div class="form_single">
                <h4> CARI APLIKASI</h4>
            </div>
    </div>
    <div class="form_box">
    <div class="form_single">
        <label>Cari Berdasarkan </label>
        <asp:DropDownList ID="cboSearch" runat="server">
            <asp:ListItem Value="Name">Name</asp:ListItem>
            <asp:ListItem Value="ProspectAppID">No Aplikasi</asp:ListItem>
        </asp:DropDownList>
        <asp:TextBox ID="txtSearch" runat="server" />
    </div>

    </div>
    <div class="form_button">
        <asp:Button ID="btnSearch" runat="server" CssClass="small button blue" Text="Find" CausesValidation="False" />
        <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="small button gray" CausesValidation="False" />
    </div>

     
     <div class="form_box_header">
            <div class="form_single">
                <asp:DataGrid ID="dtgPaging" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                    OnSortCommand="Sorting" BorderWidth="0px" CellPadding="3" CellSpacing="1" CssClass="grid_general">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <Columns>
                        <asp:ButtonColumn Text="SCORE" HeaderText="SCORE" CommandName="Scoring" />
                      <%--  <asp:ButtonColumn Text="REJECT" HeaderText="REJECT" CommandName="Reject" /> --%>
                         <%--<asp:BoundColumn DataField= "ProspectAppID" HeaderText="DATA ID" />--%>
                        <asp:TemplateColumn HeaderText="DATA ID">
                                <ItemStyle HorizontalAlign="left" Width="10%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblProspectAppID" runat="server" Text='<%#Container.DataItem("ProspectAppID")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                         <%--<asp:BoundColumn DataField="Name" HeaderText="NAMA" />--%>
                        <asp:TemplateColumn HeaderText="NAMA">
                                <ItemStyle HorizontalAlign="left" Width="10%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblName" runat="server" Text='<%#Container.DataItem("Name")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                         <asp:BoundColumn DataField="CustomerType" HeaderText="TIPE" />
                         <asp:BoundColumn DataField="ProspectAppDate" HeaderText="TANGGAL"  DataFormatString="{0:dd/MM/yyyy}" />
                         <asp:BoundColumn DataField="Kendaraan" HeaderText="ASSET YANG DIBIAYAI" /> 
                        <asp:TemplateColumn HeaderText="ProductID"  Visible="false">
                                <ItemStyle HorizontalAlign="left" Width="10%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblProductID" runat="server" Text='<%#Container.DataItem("ProductID")%>'></asp:Label>
                                </ItemTemplate>
                         </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="ProductOfferingID" Visible="false">
                                <ItemStyle HorizontalAlign="left" Width="10%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblProductOfferingID" runat="server" Text='<%#Container.DataItem("ProductOfferingID")%>' ></asp:Label>
                                </ItemTemplate>
                         </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid> 
                 <uc2:ucGridNav id="GridNavigator" runat="server"/>  
            </div>
        </div>
        </asp:Panel>
        <asp:Panel ID="pnlScoring" runat="server" Width="100%">
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label> Nama Customer </label>
                    <asp:label ID="lblCustomerName" runat="server"></asp:label>
                </div>
                <div class="form_right">
                    <label>
                        No Prospek
                    </label>
                    <asp:label ID="lblNoProspect" runat="server"></asp:label>
                </div>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <asp:Button ID="btnCreditScoring" runat="server" CausesValidation="False" Text="Scoring Pembiayaan" CssClass="small buttongo blue" />
            </div>
        </div>
        <asp:Panel ID="pnlView" runat="server" Visible="False">
            <div class="form_box_title">
                <div class="form_single">
                    <h4>
                        HASIL SCORING PEMBIAYAAN</h4>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <asp:DataGrid ID="dtgView" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="Sorting" BorderWidth="0" CellPadding="3" CellSpacing="1" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="No">
                                <ItemStyle HorizontalAlign="left" Width="10%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblNumber" runat="server" Text="0"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            
                            <asp:TemplateColumn HeaderText="KOMPONEN SCORING">
                                <ItemStyle HorizontalAlign="left" Width="30%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblDescription" runat="server" Text='<%#container.dataitem("ScoreDescription")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="KOMPONEN CONTENT">
                                <ItemStyle HorizontalAlign="left" Width="20%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblValue" runat="server" Text='<%#container.dataitem("QueryResult")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="NILAI SCORE">
                                <ItemStyle HorizontalAlign="right" Width="20%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblScoreValue" runat="server" Text='<%#container.dataitem("ScoreValue")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="STATUS SCORE">
                                <ItemStyle HorizontalAlign="right" Width="20%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblWeight" runat="server" Text='<%#container.dataitem("ScoreStatus")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="ID" Visible="False">
                                <ItemStyle HorizontalAlign="right" Width="20%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblID" runat="server" Text='<%#container.dataitem("ScoreComponentID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                     <Label>Cut Off Score </Label> 
                     <asp:Label ID="lblGrade" runat="server"></asp:Label>
                 </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                     <Label>Score Pembiayaan</Label> 
                      <asp:Label ID="lblResult" runat="server"  ></asp:Label>   
                 </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <Label>Decision</Label> 
                    <asp:Label ID="lblDecision" runat="server"></asp:Label>
                </div>
            </div>

        </asp:Panel>
        <div class="form_button"> 
          
            <asp:Button ID="btnSave" runat="server" CausesValidation="False" Text="Approve" CssClass="small button blue" Visible ="false" />
            <asp:Button ID="btnReject2" runat="server" CausesValidation="False" Text="Reject" CssClass="small button red" Visible ="false" />
            <asp:Button ID="btnProceed" runat="server" CausesValidation="False" Text="Proceed" CssClass="small button green" Visible ="false" />
            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Cancel" CssClass="small button gray" />
        </div>
    </asp:Panel>
        <asp:Panel runat="server" ID="pnlProceed">
            <div class="form_box_header">
                <div class="form_single">
                        <h5>
                            Approval Scoring</h5>
                    </div>
            </div> 
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req"> Di Approve Oleh</label>
                        <asp:DropDownList ID="cboApprovedBy" runat="server"   />
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" Display="Dynamic" ControlToValidate="cboApprovedBy" ErrorMessage="Harap diisi di Approve Oleh" 
                            InitialValue="0" CssClass="validator_general" ></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req"> Note</label> 
                        <asp:TextBox ID="txtNoteApprove" runat="server" CssClass="multiline_textbox multiline_new" TextMode="MultiLine"></asp:TextBox>
                    </div>
                </div>
                <div class="form_button">
                <asp:Button ID="SaveProceed" runat="server" CausesValidation="false" Text="Save" CssClass="small button green"></asp:Button> 
                </div>
        </asp:Panel>
    </form>
</body>
</html>
