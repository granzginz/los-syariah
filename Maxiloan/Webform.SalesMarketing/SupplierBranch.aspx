﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SupplierBranch.aspx.vb" Inherits="Maxiloan.Webform.SalesMarketing.SupplierBranch" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat"
    TagPrefix="uc4" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>SupplierBranch</title>
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />
    <script src="../js/jquery-1.6.4.js"></script>
    <%--   <script src="../js/jquery-ui-1.10.3.custom.min.js"></script>--%>
    <script language="javascript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;
        function Incentive() {
            if (document.forms[0].rboIncentive_0.checked == true) {
                document.forms[0].txtFGM.value = 0;
                document.forms[0].txtFBM.value = 0;
                document.forms[0].txtFADH.value = 0;
                document.forms[0].txtFSalesSpv.value = 0;
                document.forms[0].txtFSalesPerson.value = 0;
                document.forms[0].txtFSuppAdm.value = 0;
                document.forms[0].txtPCompany.disabled = false;
                document.forms[0].txtPGM.disabled = false;
                document.forms[0].txtPBM.disabled = false;
                document.forms[0].txtPADH.disabled = false;
                document.forms[0].txtPSalesSpv.disabled = false;
                document.forms[0].txtPSalesPerson.disabled = false;
                document.forms[0].txtPSuppAdm.disabled = false;
                document.forms[0].txtFGM.disabled = true;
                document.forms[0].txtFBM.disabled = true;
                document.forms[0].txtFADH.disabled = true;
                document.forms[0].txtFSalesSpv.disabled = true;
                document.forms[0].txtFSalesPerson.disabled = true;
                document.forms[0].txtFSuppAdm.disabled = true;
            }
            else if (document.forms[0].rboIncentive_1.checked == true) {
                document.forms[0].txtPCompany.value = 0;
                document.forms[0].txtPGM.value = 0;
                document.forms[0].txtPBM.value = 0;
                document.forms[0].txtPADH.value = 0;
                document.forms[0].txtPSalesSpv.value = 0;
                document.forms[0].txtPSalesPerson.value = 0;
                document.forms[0].txtPSuppAdm.value = 0;
                document.forms[0].txtPCompany.disabled = true;
                document.forms[0].txtPGM.disabled = true;
                document.forms[0].txtPBM.disabled = true;
                document.forms[0].txtPADH.disabled = true;
                document.forms[0].txtPSalesSpv.disabled = true;
                document.forms[0].txtPSalesPerson.disabled = true;
                document.forms[0].txtPSuppAdm.disabled = true;
                document.forms[0].txtFGM.disabled = false;
                document.forms[0].txtFBM.disabled = false;
                document.forms[0].txtFADH.disabled = false;
                document.forms[0].txtFSalesSpv.disabled = false;
                document.forms[0].txtFSalesPerson.disabled = false;
                document.forms[0].txtFSuppAdm.disabled = false;
            }
        }

        function number_format(number, decimals, dec_point, thousands_sep) {

            number = (number + '')
            .replace(/[^0-9+\-Ee.]/g, '');
            var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function (n, prec) {
                var k = Math.pow(10, prec);
                return '' + (Math.round(n * k) / k)
                .toFixed(prec);
            };
            // Fix for IE parseFloat(0.55).toFixed(0) = 0;
            s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
            .split('.');
            if (s[0].length > 3) {
                s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
            }
            if ((s[1] || '')
            .length < prec) {
                s[1] = s[1] || '';
                s[1] += new Array(prec - s[1].length + 1)
              .join('0');
            }
            return s.join(dec);
        }

        function OpenWin(SupplierID, pStyle) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.SalesMarketing/Viewsupplier.aspx?style=' + pStyle + '&SupplierID=' + SupplierID, 'UserLookup', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenWinSupplier(BranchID, SupplierID) {
            window.open('ViewSupplierBudgetForecast.aspx?BranchID=' + BranchID + '&SupplierID=' + SupplierID + '&style=Marketing', 'UserLookup', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }

        function onRefundBungaSelect(cb) {
            if (cb.checked == true) {
                $("#<%= grdRefundBunga.ClientID %> tr input[id*='txtBungaPersen']").prop('disabled', false);
                $("#<%= txtRefundBungaPersen.ClientID %>").prop('disabled', false);
                $("#<%= chkRefundBungaAmount.ClientID %>").prop('checked', false);
                $("#<%= grdRefundBunga.ClientID %> tr input[id*='txtBungaAmount']").prop('disabled', true);
                $("#<%= grdRefundBunga.ClientID %> tr input[id*='txtBungaAmount']").val("0");
                $("#<%= txtRefundBungaAmount.ClientID %>").val("0");
                $("#<%= grdRefundBunga.ClientID %> tr input[id*='txtTotalBungaAmount']").val("0");
                $("#<%= txtRefundBungaAmount.ClientID %>").prop('disabled', true);
                $("#<%= txtRefundBungaAmount.ClientID %>").removeClass("input-validation-error");
                $("#<%= grdRefundBunga.ClientID %> tr input[id*='txtTotalBungaAmount']").removeClass("input-validation-error");

            } else {
                $("#<%= grdRefundBunga.ClientID %> tr input[id*='txtBungaPersen']").prop('disabled', true);
                $("#<%= grdRefundBunga.ClientID %> tr input[id*='txtBungaPersen']").val("0");
                $("#<%= txtRefundBungaPersen.ClientID %>").val("0");
                $("#<%= grdRefundBunga.ClientID %> tr input[id*='txtTotalBungaPersen']").val("0");
                $("#<%= txtRefundBungaPersen.ClientID %>").prop('disabled', true);
                $("#<%= txtRefundBungaPersen.ClientID %>").removeClass("input-validation-error");
                $("#<%= grdRefundBunga.ClientID %> tr input[id*='txtTotalBungaPersen']").removeClass("input-validation-error");

            }
        }

        function onRefundBungaAmountSelect(cb) {
            if (cb.checked == true) {
                $("#<%= grdRefundBunga.ClientID %> tr input[id*='txtBungaAmount']").prop('disabled', false);
                $("#<%= txtRefundBungaAmount.ClientID %>").prop('disabled', false);
                $("#<%= chkRefundBungaPersen.ClientID %>").prop('checked', false);
                $("#<%= grdRefundBunga.ClientID %> tr input[id*='txtBungaPersen']").prop('disabled', true);
                $("#<%= grdRefundBunga.ClientID %> tr input[id*='txtBungaPersen']").val("0");
                $("#<%= txtRefundBungaPersen.ClientID %>").val("0");
                $("#<%= grdRefundBunga.ClientID %> tr input[id*='txtTotalBungaPersen']").val("0");
                $("#<%= txtRefundBungaPersen.ClientID %>").prop('disabled', true);
                $("#<%= txtRefundBungaPersen.ClientID %>").removeClass("input-validation-error");
                $("#<%= grdRefundBunga.ClientID %> tr input[id*='txtTotalBungaPersen']").removeClass("input-validation-error");
            } else {
                $("#<%= grdRefundBunga.ClientID %> tr input[id*='txtBungaAmount']").prop('disabled', true);
                $("#<%= grdRefundBunga.ClientID %> tr input[id*='txtBungaAmount']").val("0");
                $("#<%= txtRefundBungaAmount.ClientID %>").val("0");
                $("#<%= grdRefundBunga.ClientID %> tr input[id*='txtTotalBungaAmount']").val("0");
                $("#<%= txtRefundBungaAmount.ClientID %>").prop('disabled', true);

                $("#<%= txtRefundBungaAmount.ClientID %>").removeClass("input-validation-error");
                $("#<%= grdRefundBunga.ClientID %> tr input[id*='txtTotalBungaAmount']").removeClass("input-validation-error");
            }
        }

        function onRefundPremiPersenSelect(cb) {
            if (cb.checked == true) {
                $("#<%= grdRefundPremi.ClientID %> tr input[id*='txtPremiPersen']").prop('disabled', false);
                $("#<%= txtRefundPremiPersen.ClientID %>").prop('disabled', false);
                $("#<%= chkRefundPremiAmount.ClientID %>").prop('checked', false);
                $("#<%= grdRefundPremi.ClientID %> tr input[id*='txtPremiAmount']").prop('disabled', true);
                $("#<%= grdRefundPremi.ClientID %> tr input[id*='txtPremiAmount']").val("0");
                $("#<%= txtRefundPremiAmount.ClientID %>").val("0");
                $("#<%= grdRefundPremi.ClientID %> tr input[id*='txtTotalPremiAmount']").val("0");
                $("#<%= txtRefundPremiAmount.ClientID %>").prop('disabled', true);

                $("#<%= txtRefundPremiAmount.ClientID %>").removeClass("input-validation-error");
                $("#<%= grdRefundPremi.ClientID %> tr input[id*='txtTotalPremiAmount']").removeClass("input-validation-error");
            } else {
                $("#<%= grdRefundPremi.ClientID %> tr input[id*='txtPremiPersen']").prop('disabled', true);
                $("#<%= grdRefundPremi.ClientID %> tr input[id*='txtPremiPersen']").val("0");
                $("#<%= txtRefundPremiPersen.ClientID %>").val("0");
                $("#<%= grdRefundPremi.ClientID %> tr input[id*='txtTotalPremiPersen']").val("0");
                $("#<%= txtRefundPremiPersen.ClientID %>").prop('disabled', true);

                $("#<%= txtRefundPremiPersen.ClientID %>").removeClass("input-validation-error");
                $("#<%= grdRefundPremi.ClientID %> tr input[id*='txtTotalPremiPersen']").removeClass("input-validation-error");
            }
        }

        function onRefundPremiAmountSelect(cb) {
            if (cb.checked == true) {
                $("#<%= grdRefundPremi.ClientID %> tr input[id*='txtPremiAmount']").prop('disabled', false);
                $("#<%= txtRefundPremiAmount.ClientID %>").prop('disabled', false);
                $("#<%= chkRefundPremiPersen.ClientID %>").prop('checked', false);
                $("#<%= grdRefundPremi.ClientID %> tr input[id*='txtPremiPersen']").prop('disabled', true);
                $("#<%= grdRefundPremi.ClientID %> tr input[id*='txtPremiPersen']").val("0");
                $("#<%= txtRefundPremiPersen.ClientID %>").val("0");
                $("#<%= grdRefundPremi.ClientID %> tr input[id*='txtTotalPremiPersen']").val("0");
                $("#<%= txtRefundPremiPersen.ClientID %>").prop('disabled', true);

                $("#<%= txtRefundPremiPersen.ClientID %>").removeClass("input-validation-error");
                $("#<%= grdRefundPremi.ClientID %> tr input[id*='txtTotalPremiPersen']").removeClass("input-validation-error");

            } else {
                $("#<%= grdRefundPremi.ClientID %> tr input[id*='txtPremiAmount']").prop('disabled', true);
                $("#<%= grdRefundPremi.ClientID %> tr input[id*='txtPremiAmount']").val("0");
                $("#<%= txtRefundPremiAmount.ClientID %>").val("0");
                $("#<%= grdRefundPremi.ClientID %> tr input[id*='txtTotalPremiAmount']").val("0");
                $("#<%= txtRefundPremiAmount.ClientID %>").prop('disabled', true);

                $("#<%= txtRefundPremiAmount.ClientID %>").removeClass("input-validation-error");
                $("#<%= grdRefundPremi.ClientID %> tr input[id*='txtTotalPremiAmount']").removeClass("input-validation-error");
            }
        }


        function CalculateRefundBungaPersen(txt) {
            var Total = 0;
            var replace = ",";
            var re = new RegExp(replace, 'g');

            for (var i = 1; 0 < $("#<%=grdRefundBunga.ClientID%> tr").length - 1; i++) {
                if ($("#<%=grdRefundBunga.ClientID%> tr:eq(" + i + ") input:[id*='txtBungaPersen']").val() != "") {
                    Total = Total + parseFloat($("#<%=grdRefundBunga.ClientID%> tr:eq(" + i.toString() + ") input:[id*='txtBungaPersen']").val().replace(re, ""));
                }
                if (($("#<%=grdRefundBunga.ClientID%> tr").length - 2) == i)
                    break;
            }
            var _ni = $("#<%= txtRefundBungaPersen.ClientID %>").val();

            var myStrni = _ni.replace(re, "");
            var _total = parseFloat(Total);
            var _nilai = parseFloat(myStrni);


            if (_nilai != _total) {
                $("#<%= txtRefundBungaPersen.ClientID %>").addClass("input-validation-error");
                $("#<%= grdRefundBunga.ClientID %> tr input[id*='txtTotalBungaPersen']").addClass("input-validation-error");
                // $(txt).addClass("input-validation-error");
            }
            else if (_total == _nilai) {
                $("#<%= txtRefundBungaPersen.ClientID %>").removeClass("input-validation-error");
                $("#<%= grdRefundBunga.ClientID %> tr input[id*='txtTotalBungaPersen']").removeClass("input-validation-error");
                //  $("#<%= grdRefundBunga.ClientID %> tr input[id*='txtBungaPersen']").removeClass("input-validation-error");
            }
            $("#<%= grdRefundBunga.ClientID %> tr input[id*='txtTotalBungaPersen']").val(_total);

        }


        function CalculateRefundBungaAmount(RefundBungaAmount) {
            var Total = 0;
            var replace = ",";
            var re = new RegExp(replace, 'g');
            for (var i = 1; 0 < $("#<%=grdRefundBunga.ClientID%> tr").length - 1; i++) {
                // $("#<%=grdRefundBunga.ClientID%> tr:eq(" + i.toString() + ") input:[id*='txtBungaPersen']").removeClass("input-validation-error");
                if ($("#<%=grdRefundBunga.ClientID%> tr:eq(" + i + ") input:[id*='txtBungaAmount']").val() != "") {
                    Total = Total + parseFloat($("#<%=grdRefundBunga.ClientID%> tr:eq(" + i.toString() + ") input:[id*='txtBungaAmount']").val().replace(re, ""));
                }
                if (($("#<%=grdRefundBunga.ClientID%> tr").length - 2) == i)
                    break;
            }
            var _ni = $("#<%= txtRefundBungaAmount.ClientID %>").val();


            var myStrni = _ni.replace(re, "");
            var _total = parseFloat(Total);
            var _nilai = parseFloat(myStrni);


            $("#<%= grdRefundBunga.ClientID %> tr input[id*='txtTotalBungaAmount']").val(number_format(_total));

            if (_nilai != _total) {
                $("#<%= txtRefundBungaAmount.ClientID %>").addClass("input-validation-error");
                $("#<%= grdRefundBunga.ClientID %> tr input[id*='txtTotalBungaAmount']").addClass("input-validation-error");
            }
            else if (_total == _nilai) {
                $("#<%= grdRefundBunga.ClientID %> tr input[id*='txtTotalBungaAmount']").removeClass("input-validation-error");
                $("#<%= txtRefundBungaAmount.ClientID %>").removeClass("input-validation-error");
            }

        }


        function CalculateRefundPremiPersen(txt) {
            var Total = 0;
            var replace = ",";
            var re = new RegExp(replace, 'g');

            for (var i = 1; 0 < $("#<%=grdRefundPremi.ClientID%> tr").length - 1; i++) {
                if ($("#<%=grdRefundPremi.ClientID%> tr:eq(" + i + ") input:[id*='txtPremiPersen']").val() != "") {
                    Total = Total + parseFloat($("#<%=grdRefundPremi.ClientID%> tr:eq(" + i.toString() + ") input:[id*='txtPremiPersen']").val().replace(re, ""));
                }
                if (($("#<%=grdRefundPremi.ClientID%> tr").length - 2) == i)
                    break;
            }
            var _ni = $("#<%= txtRefundPremiPersen.ClientID %>").val();

            var myStrni = _ni.replace(re, "");
            var _total = parseFloat(Total);
            var _nilai = parseFloat(myStrni);


            if (_nilai != _total) {
                $("#<%= txtRefundPremiPersen.ClientID %>").addClass("input-validation-error");
                $("#<%= grdRefundPremi.ClientID %> tr input[id*='txtTotalPremiPersen']").addClass("input-validation-error");
            }
            else if (_total == _nilai) {
                $("#<%= txtRefundPremiPersen.ClientID %>").removeClass("input-validation-error");
                $("#<%= grdRefundPremi.ClientID %> tr input[id*='txtTotalPremiPersen']").removeClass("input-validation-error");
            }
            $("#<%= grdRefundPremi.ClientID %> tr input[id*='txtTotalPremiPersen']").val(_total);

        }


        function CalculateRefundPremiAmount(RefundBungaAmount) {
            var Total = 0;
            var replace = ",";
            var re = new RegExp(replace, 'g');
            for (var i = 1; 0 < $("#<%=grdRefundPremi.ClientID%> tr").length - 1; i++) {

                if ($("#<%=grdRefundPremi.ClientID%> tr:eq(" + i + ") input:[id*='txtPremiAmount']").val() != "") {
                    Total = Total + parseFloat($("#<%=grdRefundPremi.ClientID%> tr:eq(" + i.toString() + ") input:[id*='txtPremiAmount']").val().replace(re, ""));
                }
                if (($("#<%=grdRefundPremi.ClientID%> tr").length - 2) == i)
                    break;
            }
            var _ni = $("#<%= txtRefundPremiAmount.ClientID %>").val();


            var myStrni = _ni.replace(re, "");
            var _total = parseFloat(Total);
            var _nilai = parseFloat(myStrni);


            $("#<%= grdRefundPremi.ClientID %> tr input[id*='txtTotalPremiAmount']").val(number_format(_total));

            if (_nilai != _total) {
                $("#<%= txtRefundPremiAmount.ClientID %>").addClass("input-validation-error");
                $("#<%= grdRefundPremi.ClientID %> tr input[id*='txtTotalPremiAmount']").addClass("input-validation-error");
            }
            else if (_total == _nilai) {
                $("#<%= grdRefundPremi.ClientID %> tr input[id*='txtTotalPremiAmount']").removeClass("input-validation-error");
                $("#<%= txtRefundPremiAmount.ClientID %>").removeClass("input-validation-error");
            }

        }



        function validate(s, args) {
            var Total = 0;
            var replace = ",";
            var re = new RegExp(replace, 'g');


            var _BungaPersen = $("#<%= txtRefundBungaPersen.ClientID %>").val();
            var _TotalBungaPersen = $("#<%= grdRefundBunga.ClientID %> tr input[id*='txtTotalBungaPersen']").val();
            var myStrBungaPersen = _BungaPersen.replace(re, "");
            var myStrTotalBungaPersen = _TotalBungaPersen.replace(re, "");

            var BungaPersen = parseFloat(myStrBungaPersen);
            var TotalBungaPersen = parseFloat(myStrTotalBungaPersen);


            if (BungaPersen != TotalBungaPersen) {

                $("#<%= txtRefundBungaPersen.ClientID %>").addClass("input-validation-error");
                $("#<%= grdRefundBunga.ClientID %> tr input[id*='txtTotalBungaPersen']").addClass("input-validation-error");
            }
            else {

                $("#<%= txtRefundBungaPersen.ClientID %>").removeClass("input-validation-error");
                $("#<%= grdRefundBunga.ClientID %> tr input[id*='txtTotalBungaPersen']").removeClass("input-validation-error");

            }

            var _BungaAmount = $("#<%= txtRefundBungaAmount.ClientID %>").val();
            var _TotalBungaAmount = $("#<%= grdRefundBunga.ClientID %> tr input[id*='txtTotalBungaAmount']").val();
            var myStrBungaAmount = _BungaAmount.replace(re, "");
            var myStrTotalBungaAmount = _TotalBungaAmount.replace(re, "");

            var BungaAmount = parseFloat(myStrBungaAmount);
            var TotalBungaAmount = parseFloat(myStrTotalBungaAmount);

            if (BungaAmount != TotalBungaAmount) {

                $("#<%= txtRefundBungaAmount.ClientID %>").addClass("input-validation-error");
                $("#<%= grdRefundBunga.ClientID %> tr input[id*='txtTotalBungaAmount']").addClass("input-validation-error");
            }
            else {

                $("#<%= grdRefundBunga.ClientID %> tr input[id*='txtTotalBungaAmount']").removeClass("input-validation-error");
                $("#<%= txtRefundBungaAmount.ClientID %>").removeClass("input-validation-error");
            }


            var _PremiPersen = $("#<%= txtRefundPremiPersen.ClientID %>").val();
            var _TotalPremiPersen = $("#<%= grdRefundPremi.ClientID %> tr input[id*='txtTotalPremiPersen']").val();
            var myStrPremiPersen = _PremiPersen.replace(re, "");
            var myStrTotalPremiPersen = _TotalPremiPersen.replace(re, "");

            var PremiPersen = parseFloat(myStrPremiPersen);
            var TotalPremiPersen = parseFloat(myStrTotalPremiPersen);

            if (PremiPersen != TotalPremiPersen) {

                $("#<%= txtRefundPremiPersen.ClientID %>").addClass("input-validation-error");
                $("#<%= grdRefundPremi.ClientID %> tr input[id*='txtTotalPremiPersen']").addClass("input-validation-error");
            }
            else {

                $("#<%= txtRefundPremiPersen.ClientID %>").removeClass("input-validation-error");
                $("#<%= grdRefundPremi.ClientID %> tr input[id*='txtTotalPremiPersen']").removeClass("input-validation-error");
            }

            var _PremiAmount = $("#<%= txtRefundPremiAmount.ClientID %>").val();
            var _TotalPremiAmount = $("#<%= grdRefundPremi.ClientID %> tr input[id*='txtTotalPremiAmount']").val();
            var myStrPremiAmount = _PremiAmount.replace(re, "");
            var myStrTotalPremiAmount = _TotalPremiAmount.replace(re, "");

            var PremiAmount = parseFloat(myStrPremiAmount);
            var TotalPremiAmount = parseFloat(myStrTotalPremiAmount);

            if (PremiAmount != TotalPremiAmount) {

                $("#<%= txtRefundPremiAmount.ClientID %>").addClass("input-validation-error");
                $("#<%= grdRefundPremi.ClientID %> tr input[id*='txtTotalPremiAmount']").addClass("input-validation-error");
            }
            else {

                $("#<%= txtRefundPremiAmount.ClientID %>").removeClass("input-validation-error");
                $("#<%= grdRefundPremi.ClientID %> tr input[id*='txtTotalPremiAmount']").removeClass("input-validation-error");
            }

            if ((BungaPersen != TotalBungaPersen) ||
                         (BungaAmount != TotalBungaAmount) ||
                         (PremiPersen != TotalPremiPersen) ||
                         (PremiAmount != TotalPremiAmount))
                           {
                args.IsValid = false;


            }
            else if ((BungaPersen == TotalBungaPersen) &&
                         (BungaAmount == TotalBungaAmount) &&
                         (PremiPersen == TotalPremiPersen) &&
                         (PremiAmount == TotalPremiAmount)
                         ) {
                args.IsValid = true;
            }

        }


        function calculateHeader() {
            var Total = 0;
            var replace = ",";
            var re = new RegExp(replace, 'g');


            var _BungaPersen = $("#<%= txtRefundBungaPersen.ClientID %>").val();
            var _TotalBungaPersen = $("#<%= grdRefundBunga.ClientID %> tr input[id*='txtTotalBungaPersen']").val();
            var myStrBungaPersen = _BungaPersen.replace(re, "");
            var myStrTotalBungaPersen = _TotalBungaPersen.replace(re, "");

            var BungaPersen = parseFloat(myStrBungaPersen);
            var TotalBungaPersen = parseFloat(myStrTotalBungaPersen);


            if (BungaPersen != TotalBungaPersen) {

                $("#<%= txtRefundBungaPersen.ClientID %>").addClass("input-validation-error");
                $("#<%= grdRefundBunga.ClientID %> tr input[id*='txtTotalBungaPersen']").addClass("input-validation-error");
            }
            else {

                $("#<%= txtRefundBungaPersen.ClientID %>").removeClass("input-validation-error");
                $("#<%= grdRefundBunga.ClientID %> tr input[id*='txtTotalBungaPersen']").removeClass("input-validation-error");

            }




            var _BungaAmount = $("#<%= txtRefundBungaAmount.ClientID %>").val();
            var _TotalBungaAmount = $("#<%= grdRefundBunga.ClientID %> tr input[id*='txtTotalBungaAmount']").val();
            var myStrBungaAmount = _BungaAmount.replace(re, "");
            var myStrTotalBungaAmount = _TotalBungaAmount.replace(re, "");

            var BungaAmount = parseFloat(myStrBungaAmount);
            var TotalBungaAmount = parseFloat(myStrTotalBungaAmount);

            if (BungaAmount != TotalBungaAmount) {

                $("#<%= txtRefundBungaAmount.ClientID %>").addClass("input-validation-error");
                $("#<%= grdRefundBunga.ClientID %> tr input[id*='txtTotalBungaAmount']").addClass("input-validation-error");
            }
            else {

                $("#<%= grdRefundBunga.ClientID %> tr input[id*='txtTotalBungaAmount']").removeClass("input-validation-error");
                $("#<%= txtRefundBungaAmount.ClientID %>").removeClass("input-validation-error");
            }


            var _PremiPersen = $("#<%= txtRefundPremiPersen.ClientID %>").val();
            var _TotalPremiPersen = $("#<%= grdRefundPremi.ClientID %> tr input[id*='txtTotalPremiPersen']").val();
            var myStrPremiPersen = _PremiPersen.replace(re, "");
            var myStrTotalPremiPersen = _TotalPremiPersen.replace(re, "");

            var PremiPersen = parseFloat(myStrPremiPersen);
            var TotalPremiPersen = parseFloat(myStrTotalPremiPersen);

            if (PremiPersen != TotalPremiPersen) {

                $("#<%= txtRefundPremiPersen.ClientID %>").addClass("input-validation-error");
                $("#<%= grdRefundPremi.ClientID %> tr input[id*='txtTotalPremiPersen']").addClass("input-validation-error");
            }
            else {

                $("#<%= txtRefundPremiPersen.ClientID %>").removeClass("input-validation-error");
                $("#<%= grdRefundPremi.ClientID %> tr input[id*='txtTotalPremiPersen']").removeClass("input-validation-error");
            }

            var _PremiAmount = $("#<%= txtRefundPremiAmount.ClientID %>").val();
            var _TotalPremiAmount = $("#<%= grdRefundPremi.ClientID %> tr input[id*='txtTotalPremiAmount']").val();
            var myStrPremiAmount = _PremiAmount.replace(re, "");
            var myStrTotalPremiAmount = _TotalPremiAmount.replace(re, "");

            var PremiAmount = parseFloat(myStrPremiAmount);
            var TotalPremiAmount = parseFloat(myStrTotalPremiAmount);

            if (PremiAmount != TotalPremiAmount) {

                $("#<%= txtRefundPremiAmount.ClientID %>").addClass("input-validation-error");
                $("#<%= grdRefundPremi.ClientID %> tr input[id*='txtTotalPremiAmount']").addClass("input-validation-error");
            }
            else {

                $("#<%= txtRefundPremiAmount.ClientID %>").removeClass("input-validation-error");
                $("#<%= grdRefundPremi.ClientID %> tr input[id*='txtTotalPremiAmount']").removeClass("input-validation-error");
            }
        }

    </script>
    <style>
        .input-validation-error
        {
            background-color: #FF0000;
            color: #FFFFFF;
        }
    </style>
</head>
<body>
    <asp:UpdateProgress ID="upg1" runat="server" AssociatedUpdatePanelID="updatePanel1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>
                        DAFTAR SUPPLIER CABANG
                    </h3>
                </div>
            </div>
            <asp:Panel ID="pnlList" runat="server">
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtgPaging" runat="server" Width="100%" DataKeyField="BranchID"
                                BorderStyle="None" BorderWidth="0" OnSortCommand="Sorting" AutoGenerateColumns="False"
                                AllowSorting="True" CssClass="grid_general">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="EDIT">
                                        <ItemStyle CssClass="command_col"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" ImageUrl="../Images/iconedit.gif"
                                                CommandName="Edit"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="DELETE">
                                        <ItemStyle CssClass="command_col"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgDelete" runat="server" CausesValidation="False" ImageUrl="../Images/icondelete.gif"
                                                CommandName="Delete"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="ID CABANG" SortExpression="BranchID">
                                        <ItemTemplate>
                                            <asp:LinkButton CausesValidation="False" ID="hplBranchID" runat="server" CommandName="BranchID"
                                                Text='<%# databinder.eval(container,"DataItem.BranchID")%>'>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="BranchFullName" SortExpression="BranchFullName" HeaderText="NAMA CABANG">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="EmployeeName" SortExpression="EmployeeName" HeaderText="NAMA CMO">
                                    </asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="BUDGET" Visible="false">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hypBudget" runat="server" CausesValidation="False" Text="Budget"></asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="FORECAST" Visible="false">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hypForeCast" runat="server" CausesValidation="False" Text="ForeCast"></asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="STATEMENT" Visible="false">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="HyStatement" runat="server" CausesValidation="False" Text="Statement of Supplier"
                                                CommandName="LnkStatement"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="INCENTIVE" >
                                        <ItemTemplate>
                                            <asp:LinkButton CausesValidation="False" ID="hplIncentive" runat="server" CommandName="Incentive"
                                                Text="Incentive">
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                            <div class="button_gridnavigation">
                                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../Images/grid_navbutton01.png"
                                    CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../Images/grid_navbutton02.png"
                                    CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../Images/grid_navbutton03.png"
                                    CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../Images/grid_navbutton04.png"
                                    CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                                    EnableViewState="False"></asp:Button>
                                <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" MinimumValue="1"
                                    ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"></asp:RangeValidator>
                                <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                                    ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                            </div>
                            <div class="label_gridnavigation">
                                <asp:Label ID="lblPage" runat="server"></asp:Label>of
                                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonAdd" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue">
                    </asp:Button>
                   
                    <asp:Button ID="ButtonBack" runat="server" CausesValidation="False" Text="Back" CssClass="small button gray">
                    </asp:Button>
                </div>
                <div class="form_box_title">
                    <div class="form_single">
                        <h4>
                            CARI SUPPLIER CABANG
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Nama Supplier</label>
                        <asp:HyperLink ID="lblName" runat="server" NavigateUrl=""></asp:HyperLink>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Cari Berdasarkan</label>
                        <asp:DropDownList ID="cboSearch" runat="server">
                            <asp:ListItem Value="BranchID">ID Cabang</asp:ListItem>
                            <asp:ListItem Value="BranchFullName">Nama Cabang</asp:ListItem>
                            <asp:ListItem Value="EmployeeName">Nama CMO</asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonSearch" runat="server" CausesValidation="False" Text="Search"
                        CssClass="small button blue"></asp:Button>
                    <asp:Button ID="ButtonReset" runat="server" CausesValidation="False" Text="Reset"
                        CssClass="small button gray"></asp:Button>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlAdd" runat="server">
                <div class="form_box_header">
                    <div class="form_single">
                        <h4>
                            SUPPLIER
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Nama Supplier</label>
                        <asp:HyperLink ID="lblNameAdd" runat="server"></asp:HyperLink>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <h5>
                            SUPPLIER CABANG
                        </h5>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtgAdd" runat="server" Width="100%" DataKeyField="BranchID" AutoGenerateColumns="False"
                                AllowSorting="false" CssClass="grid_general" BorderStyle="None" BorderWidth="0">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="ASSIGN">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkAssign" runat="server"></asp:CheckBox>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="BranchFullName" HeaderText="CABANG"></asp:BoundColumn>
                                </Columns>
                            </asp:DataGrid>
                        </div>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonSaveAdd" runat="server" CausesValidation="False" Text="Save"
                        CssClass="small button blue"></asp:Button>
                    <asp:Button ID="ButtonBackAdd" runat="server" CausesValidation="False" Text="Back"
                        CssClass="small button gray"></asp:Button>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlEdit" runat="server">
             <div id="divRefound" runat="server">
                <div class="form_box_header">
                    <div class="form_single">
                        <h4>
                            SUPPLIER CABANG -
                            <asp:Label ID="lblTitle" runat="server"></asp:Label>
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Cabang</label>
                        <asp:Label ID="lblBranchEdit" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Nama CMO<font color="red"></font></label>
                        <asp:Label ID="lblAO" runat="server"></asp:Label>
                        <asp:DropDownList ID="cboAO" runat="server">
                        </asp:DropDownList>
                        <asp:Label ID="lblReqAO" runat="server"></asp:Label>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Harap pilih Nama CMO"
                            ControlToValidate="cboAO" Display="Dynamic" InitialValue="" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                </div>
                <!-- Alokasi REFUND BUNGA -->
                <div id="divinsentiv" runat="server">
                <div class="form_box_header">
                    <div class="form_single">
                        <h4>
                            REFUND MARGIN
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Alokasi</label>
                        <asp:CheckBox runat="server" ID="chkRefundBungaPersen" class='checkBoxClassRefundBunga'
                            onclick='onRefundBungaSelect(this);' />
                        <uc4:ucnumberformat id="txtRefundBungaPersen" runat="server" textcssclass="numberAlign smaller_text" onchange="calculateHeader();" />
                        %
                        <label></label>
                    <%--   <asp:DropDownList ID="cboRefundBUnga" runat="server" class="opt_single" >
                            <asp:ListItem Value="P">Plafond</asp:ListItem>
                            <asp:ListItem Value="I">Interest</asp:ListItem>
                       </asp:DropDownList>
                       <label></label>
                         <label>Multiplier</label>
                        <uc4:ucnumberformat id="txtRefundBungaMultiplier" runat="server" textcssclass="numberAlign smaller_text"/>--%>
                    </div>
                    <div class="form_single">
                    <label></label>
                     <asp:CheckBox runat="server" ID="chkRefundBungaAmount" onclick='onRefundBungaAmountSelect(this);' />
                        <uc4:ucnumberformat id="txtRefundBungaAmount" runat="server" textcssclass="numberAlign small" onchange="calculateHeader();"/>
                        Rp.
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label style="vertical-align: top">
                            Distribusi Kepada</label>
                        <div style="display: inline-block; width: 70%">
                            <asp:DataGrid ID="grdRefundBunga" runat="server" Width="100%" DataKeyField="ID" AutoGenerateColumns="False"
                                CssClass="grid_general" BorderStyle="None" BorderWidth="0px" ShowFooter="True">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <FooterStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="NO">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblNomor" Text='<%# Container.ItemIndex + 1 %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="ID" HeaderText="ID" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="Description" HeaderText="Jabatan"></asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="Persentase">
                                        <ItemTemplate>
                                            <uc4:ucnumberformat id="txtBungaPersen" runat="server" textcssclass="numberAlign smaller_text"
                                                text='<%# databinder.eval(container,"DataItem.Persentase","{0:##0.00}")%>' onchange="CalculateRefundBungaPersen(this);"
                                                enabled='<%# databinder.eval(container,"DataItem.chekedInterestPercent")%>' />
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox runat="server" ID="txtTotalBungaPersen" CssClass="numberAlign smaller_text"
                                                ReadOnly="True">0</asp:TextBox>
                                        </FooterTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Amount">
                                        <ItemTemplate>
                                            <uc4:ucnumberformat id="txtBungaAmount" runat="server" textcssclass="numberAlign small"
                                                text='<%# databinder.eval(container,"DataItem.Amount","{0:##0.00}")%>' onchange="CalculateRefundBungaAmount(this);"
                                                enabled='<%# databinder.eval(container,"DataItem.chekedInterestAmount")%>' />
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox runat="server" ID="txtTotalBungaAmount" CssClass="numberAlign small"
                                                ReadOnly="True">0</asp:TextBox>
                                        </FooterTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                        </div>
                    </div>
                </div>
                <!-- end of REFUND BUNGA -->
                <!-- Alokasi REFUND Premi -->
                <div class="form_box_header">
                    <div class="form_single">
                        <h4>
                            REFUND PREMI
                        </h4>
                    </div>
                </div>
                <%--<div class="form_box">
                    <div class="form_single">
                        <label>
                            Alokasi</label>
                        <asp:CheckBox runat="server" ID="chkRefundPremiPersen" onclick='onRefundPremiPersenSelect(this);' />
                        <uc4:ucnumberformat id="txtRefundPremiPersen" runat="server" textcssclass="numberAlign smaller_text" onchange="calculateHeader();" />
                        %
                        <asp:CheckBox runat="server" ID="chkRefundPremiAmount" onclick='onRefundPremiAmountSelect(this);' />
                        <uc4:ucnumberformat id="txtRefundPremiAmount" runat="server" textcssclass="numberAlign small" onchange="calculateHeader();" />
                        Rp.
                    </div>
                </div>--%>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Alokasi</label>
                        <asp:CheckBox runat="server" ID="chkRefundPremiPersen" onclick='onRefundPremiPersenSelect(this);' />
                        <uc4:ucnumberformat id="txtRefundPremiPersen" runat="server" textcssclass="numberAlign smaller_text" onchange="calculateHeader();" />
                        %
                         <label></label>
                       <asp:DropDownList ID="cbo_RefundPremi" runat="server" class="opt_single" >
                            <asp:ListItem Value="S">Sum Insured</asp:ListItem>
                            <asp:ListItem Value="P">Premium</asp:ListItem>
                            <asp:ListItem Value="D">Premium Discount</asp:ListItem>
                       </asp:DropDownList>
                    </div>
                    <div class="form_single">
                      <label></label>
                        <asp:CheckBox runat="server" ID="chkRefundPremiAmount" onclick='onRefundPremiAmountSelect(this);' />
                        <uc4:ucnumberformat id="txtRefundPremiAmount" runat="server" textcssclass="numberAlign small" onchange="calculateHeader();" />
                         Rp.
                </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label style="vertical-align: top">
                            Distribusi Kepada</label>
                        <div style="display: inline-block; width: 70%">
                            <asp:DataGrid ID="grdRefundPremi" runat="server" Width="100%" DataKeyField="ID" AutoGenerateColumns="False"
                                AllowSorting="false" CssClass="grid_general" BorderStyle="None" BorderWidth="0"
                                ShowFooter="true">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <FooterStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="NO">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblNomor" Text='<%# Container.ItemIndex + 1 %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="ID" HeaderText="ID" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="Description" HeaderText="Jabatan"></asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="Persentase">
                                        <ItemTemplate>
                                            <uc4:ucnumberformat id="txtPremiPersen" runat="server" textcssclass="numberAlign smaller_text"
                                                text='<%# databinder.eval(container,"DataItem.Persentase","{0:##0.00}")%>' onchange="CalculateRefundPremiPersen(this);"
                                                enabled='<%# databinder.eval(container,"DataItem.chekedPremiPercent")%>' />
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox runat="server" ID="txtTotalPremiPersen" CssClass="numberAlign smaller_text"
                                                ReadOnly="True">0</asp:TextBox>
                                        </FooterTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Amount">
                                        <ItemTemplate>
                                            <uc4:ucnumberformat id="txtPremiAmount" runat="server" textcssclass="numberAlign small"
                                                text='<%# databinder.eval(container,"DataItem.Amount", "{0:##0.00}")%>' onchange="CalculateRefundPremiAmount(this);"
                                                enabled='<%# databinder.eval(container,"DataItem.chekedPremiAmount")%>' />
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox runat="server" ID="txtTotalPremiAmount" CssClass="numberAlign small"
                                                ReadOnly="True">0</asp:TextBox>
                                        </FooterTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                        </div>
                    </div>
                </div>

                <!-- end of REFUND Premi -->
                <!-- Alokasi REFUND Admin -->
                <%--<div class="form_box_header">
                    <div class="form_single">
                        <h4>
                            REFUND ADMIN
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Alokasi</label>
                        <asp:CheckBox runat="server" ID="chkRefundAdminPersen" onclick='onRefundAdminPersenSelect(this);' />
                        <uc4:ucnumberformat id="txtRefundAdminPersen" runat="server" textcssclass="numberAlign smaller_text" onchange="calculateHeader();"/>
                        %
                        <asp:CheckBox runat="server" ID="chkRefundAdminAmount" onclick='onRefundAdminAmountSelect(this);' />
                        <uc4:ucnumberformat id="txtRefundAdminAmount" runat="server" textcssclass="numberAlign small" onchange="calculateHeader();" />
                        Rp.
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label style="vertical-align: top">
                            Distribusi Kepada</label>
                        <div style="display: inline-block; width: 70%">
                            <asp:DataGrid ID="grdRefundAdmin" runat="server" Width="100%" DataKeyField="ID" AutoGenerateColumns="False"
                                AllowSorting="false" CssClass="grid_general" BorderStyle="None" BorderWidth="0"
                                ShowFooter="true">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <FooterStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="NO">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblNomor" Text='<%# Container.ItemIndex + 1 %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="ID" HeaderText="ID" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="Description" HeaderText="Jabatan"></asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="Persentase">
                                        <ItemTemplate>
                                            <uc4:ucnumberformat id="txtAdminPersen" runat="server" textcssclass="numberAlign smaller_text"
                                                text='<%# databinder.eval(container,"DataItem.Persentase","{0:##0.00}")%>' onchange="CalculateRefundAdminPersen(this);"
                                                enabled='<%# databinder.eval(container,"DataItem.chekedAdminPercent")%>' />
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox runat="server" ID="txtTotalAdminPersen" CssClass="numberAlign smaller_text"
                                                ReadOnly="True">0</asp:TextBox>
                                        </FooterTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Amount">
                                        <ItemTemplate>
                                            <uc4:ucnumberformat id="txtAdminAmount" runat="server" textcssclass="numberAlign small"
                                                text='<%# databinder.eval(container,"DataItem.Amount","{0:##0.00}")%>' onchange="CalculateRefundAdminAmount(this);"
                                                enabled='<%# databinder.eval(container,"DataItem.chekedAdminAmount")%>' />
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox runat="server" ID="txtTotalAdminAmount" CssClass="numberAlign small"
                                                ReadOnly="True">0</asp:TextBox>
                                        </FooterTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                        </div>
                    </div>
                </div>--%>
                <!-- end of REFUND Admin -->
                <!-- Alokasi REFUND Provisi -->
                <%--<div class="form_box_header">
                    <div class="form_single">
                        <h4>
                            REFUND PROVISI
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Alokasi</label>
                        <asp:CheckBox runat="server" ID="chkRefundProvisiPersen" onclick='onRefundProvisiPersenSelect(this);' />
                        <uc4:ucnumberformat id="txtRefundProvisiPersen" runat="server" textcssclass="numberAlign smaller_text" onchange="calculateHeader();" />
                        %
                        <asp:CheckBox runat="server" ID="chkRefundProvisiAmount" onclick='onRefundProvisiAmountSelect(this);' />
                        <uc4:ucnumberformat id="txtRefundProvisiAmount" runat="server" textcssclass="numberAlign small"  onchange="calculateHeader();" />
                        Rp.
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label style="vertical-align: top">
                            Distribusi Kepada</label>
                        <div style="display: inline-block; width: 70%">
                            <asp:DataGrid ID="grdRefundProvisi" runat="server" Width="100%" DataKeyField="ID"
                                AutoGenerateColumns="False" AllowSorting="false" CssClass="grid_general" BorderStyle="None"
                                BorderWidth="0" ShowFooter="true">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <FooterStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="NO">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblNomor" Text='<%# Container.ItemIndex + 1 %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="ID" HeaderText="ID" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="Description" HeaderText="Jabatan"></asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="Persentase">
                                        <ItemTemplate>
                                            <uc4:ucnumberformat id="txtProvisiPersen" runat="server" textcssclass="numberAlign smaller_text"
                                                text='<%# databinder.eval(container,"DataItem.Persentase","{0:##0.00}")%>' onchange="CalculateRefundProvisiPersen(this);"
                                                enabled='<%# databinder.eval(container,"DataItem.chekedRefundProvisiPercent")%>' />
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox runat="server" ID="txtTotalProvisiPersen" CssClass="numberAlign smaller_text"
                                                ReadOnly="True">0</asp:TextBox>
                                        </FooterTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Amount">
                                        <ItemTemplate>
                                            <uc4:ucnumberformat id="txtProvisiAmount" runat="server" textcssclass="numberAlign small"
                                                text='<%# databinder.eval(container,"DataItem.Amount","{0:##0.00}")%>' onchange="CalculateRefundProvisiAmount(this);"
                                                enabled='<%# databinder.eval(container,"DataItem.chekedRefundProvisiAmount")%>' />
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox runat="server" ID="txtTotalProvisiAmount" CssClass="numberAlign small"
                                                ReadOnly="True">0</asp:TextBox>
                                        </FooterTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                        </div>
                    </div>
                </div>--%>
                </div>
                <asp:CustomValidator ID="CustomValidator1" runat="server"
                     ErrorMessage="Total Alokasi tidak sesuai, Periksa kembali !" 
                     ClientValidationFunction="validate" />
                      <div class="form_box">
                     <div class="form_single">
                    <h3>
                        ADDITIONAL REWARD
                    </h3>
                </div>
                </div>
                <div class="form_box">
                <div class="form_single">
                    <label>
                        Reward per Unit</label>
                    <uc4:ucnumberformat id="txtRewardUnit" runat="server" textcssclass="numberAlign small" />&nbsp; per unit
                </div>
                </div>
                 <div class="form_box">
                <div class="form_single">
                    <h3>
                       BONUS
                    </h3>
                </div>
                </div>
                <div class="form_box">
                <div class="form_single">
                    <label>
                        Buk Bonus</label>
                    <uc4:ucnumberformat id="txtBukBonus" runat="server" textcssclass="numberAlign small" />&nbsp;
                </div>
                </div>
                <!-- end of REFUND Admin -->
                 <asp:CustomValidator ID="cv1" runat="server"
                     ErrorMessage="Total Alokasi tidak sesuai, Periksa kembali !" 
                     ClientValidationFunction="validate" />
                <div class="form_button">
                    <asp:Button ID="ButtonOK" runat="server" CausesValidation="False" Text="Ok" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="ButtonSaveEdit" runat="server" Text="Save" CssClass="small button blue"
                        CausesValidation="True"></asp:Button>
                    <asp:Button ID="ButtonCancelEdit" runat="server" CausesValidation="False" Text="Cancel"
                        CssClass="small button gray"></asp:Button>
                </div>
                <asp:Panel runat="server" ID="PnlHide" Visible="false">
                    <div class="form_box_header">
                        <div class="form_single">
                            <h5>
                                Incentive Card
                            </h5>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label>
                                Incentive Card Asset Yang Dibiayai Baru</label>
                            <asp:Label ID="lblNew" runat="server"></asp:Label>
                            <asp:DropDownList ID="cboNew" runat="server">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label>
                                Incentive Card Asset Yang Dibiayai Bekas</label>
                            <asp:Label ID="lblUsed" runat="server"></asp:Label>
                            <asp:DropDownList ID="cboUsed" runat="server">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label>
                                Incentive Card Repeat Order</label>
                            <asp:Label ID="lblRO" runat="server"></asp:Label>
                            <asp:DropDownList ID="cboRO" runat="server">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label>
                                Metode Distribusi Incentive</label>
                            <asp:Label ID="lblIncentive" class="label_general" runat="server"></asp:Label>
                            <asp:RadioButtonList ID="rboIncentive" runat="server" class="opt_single" RepeatDirection="Horizontal">
                                <asp:ListItem Value="P" Selected="True">Percentage</asp:ListItem>
                                <asp:ListItem Value="F">Fixed Amount</asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <div class="form_1">
                                <label>
                                    Penerima</label>
                            </div>
                            <div class="form_2">
                                <label>
                                    Percentage</label>
                            </div>
                            <div class="form_3">
                                <label>
                                    Fixed Amount</label>
                            </div>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <div class="form_1">
                                <label>
                                    Perusahaan</label>
                            </div>
                            <div class="form_2">
                                <asp:Label ID="lblPCompany" runat="server"></asp:Label>
                                <asp:TextBox ID="txtPCompany" runat="server" Width="80%" MaxLength="15"></asp:TextBox>
                                <asp:RangeValidator ID="RangeValidator15" runat="server" ErrorMessage="RangeValidator"
                                    MinimumValue="0" CssClass="validator_general" Type="Double" ControlToValidate="txtPCompany"
                                    MaximumValue="100"></asp:RangeValidator>
                                <asp:RangeValidator ID="Rangevalidator14" runat="server" ErrorMessage="Tahun harus <= Tanggal Business"
                                    MinimumValue="0" CssClass="validator_general" Type="Double" ControlToValidate="txtPCompany"
                                    Display="Dynamic" MaximumValue="100"></asp:RangeValidator>
                                <asp:RangeValidator CssClass="validator_general" ID="rangeValidator1" runat="server"
                                    ErrorMessage="Harap isi dengan angka antara 0 s/d 100" MinimumValue="0" Type="Double"
                                    ControlToValidate="txtPCompany" Display="Dynamic" MaximumValue="100"></asp:RangeValidator>
                            </div>
                            <div class="form_3">
                                <label>
                                    -</label>
                            </div>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <div class="form_1">
                                <label>
                                    GM-General Manager</label>
                            </div>
                            <div class="form_2">
                                <asp:Label ID="lblPGM" runat="server"></asp:Label>
                                <asp:TextBox ID="txtPGM" runat="server" Width="80%" MaxLength="15"></asp:TextBox>
                                <asp:RangeValidator ID="Rangevalidator2" runat="server" ErrorMessage="Harap isi dengan angka antara 0 s/d 100"
                                    MinimumValue="0" CssClass="validator_general" Type="Double" ControlToValidate="txtPGM"
                                    Display="Dynamic" MaximumValue="100"></asp:RangeValidator>
                            </div>
                            <div class="form_3">
                                <asp:Label ID="lblFGM" runat="server"></asp:Label>
                                <asp:TextBox ID="txtFGM" runat="server" Width="80%" MaxLength="15"></asp:TextBox>
                                <asp:RangeValidator ID="Rangevalidator13" runat="server" ErrorMessage="harap isi dengan angka"
                                    MinimumValue="0" CssClass="validator_general" Type="Double" ControlToValidate="txtFGM"
                                    Display="Dynamic" MaximumValue="999999999999999"></asp:RangeValidator>
                            </div>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <div class="form_1">
                                <label>
                                    BM-Branch Manager</label>
                            </div>
                            <div class="form_2">
                                <asp:Label ID="lblPBM" runat="server"></asp:Label>
                                <asp:TextBox ID="txtPBM" runat="server" Width="80%" MaxLength="15"></asp:TextBox>
                                <asp:RangeValidator ID="Rangevalidator3" runat="server" ErrorMessage="Harap isi dengan angka antara 0 s/d 100"
                                    MinimumValue="0" CssClass="validator_general" Type="Double" ControlToValidate="txtPBM"
                                    Display="Dynamic" MaximumValue="100"></asp:RangeValidator>
                            </div>
                            <div class="form_3">
                                <asp:Label ID="lblFBM" runat="server"></asp:Label>
                                <asp:TextBox ID="txtFBM" runat="server" Width="80%" MaxLength="15"></asp:TextBox>
                                <asp:RangeValidator ID="Rangevalidator12" runat="server" ErrorMessage="harap isi dengan angka"
                                    MinimumValue="0" CssClass="validator_general" Type="Double" ControlToValidate="txtFBM"
                                    Display="Dynamic" MaximumValue="999999999999999"></asp:RangeValidator>
                            </div>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <div class="form_1">
                                <label>
                                    ADH-Administration Head</label>
                            </div>
                            <div class="form_2">
                                <asp:Label ID="lblPADH" runat="server"></asp:Label>
                                <asp:TextBox ID="txtPADH" runat="server" Width="80%" MaxLength="15"></asp:TextBox>
                                <asp:RangeValidator ID="Rangevalidator4" runat="server" ErrorMessage="harap isi dengan angka antara 0 s/d 100"
                                    MinimumValue="0" CssClass="validator_general" Type="Double" ControlToValidate="txtPADH"
                                    Display="Dynamic" MaximumValue="100"></asp:RangeValidator>
                            </div>
                            <div class="form_3">
                                <asp:Label ID="lblFADH" runat="server"></asp:Label>
                                <asp:TextBox ID="txtFADH" runat="server" Width="80%" MaxLength="15"></asp:TextBox>
                                <asp:RangeValidator ID="Rangevalidator11" runat="server" ErrorMessage="Harap isi dengan angka"
                                    MinimumValue="0" CssClass="validator_general" Type="Double" ControlToValidate="txtFADH"
                                    Display="Dynamic" MaximumValue="999999999999999"></asp:RangeValidator>
                            </div>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <div class="form_1">
                                <label>
                                    Sales Supervisor</label>
                            </div>
                            <div class="form_2">
                                <asp:Label ID="lblPSalesSpv" runat="server"></asp:Label>
                                <asp:TextBox ID="txtPSalesSpv" runat="server" Width="80%" MaxLength="15"></asp:TextBox>
                                <asp:RangeValidator ID="Rangevalidator5" runat="server" ErrorMessage="Harap isi dengan angka antara 0 s/d 100"
                                    MinimumValue="0" CssClass="validator_general" Type="Double" ControlToValidate="txtPSalesSpv"
                                    Display="Dynamic" MaximumValue="100"></asp:RangeValidator>
                            </div>
                            <div class="form_3">
                                <asp:Label ID="lblFSalesSpv" runat="server"></asp:Label>
                                <asp:TextBox ID="txtFSalesSpv" runat="server" Width="80%" MaxLength="15"></asp:TextBox>
                                <asp:RangeValidator CssClass="validator_general" ID="Rangevalidator10" runat="server"
                                    ErrorMessage="harap isi dengan angka" MinimumValue="0" Type="Double" ControlToValidate="txtFSalesSpv"
                                    Display="Dynamic" MaximumValue="999999999999999"></asp:RangeValidator>
                            </div>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <div class="form_1">
                                <label>
                                    Sales Person</label>
                            </div>
                            <div class="form_2">
                                <asp:Label ID="lblPSalesPerson" runat="server"></asp:Label>
                                <asp:TextBox ID="txtPSalesPerson" runat="server" Width="80%" MaxLength="15"></asp:TextBox>
                                <asp:RangeValidator ID="Rangevalidator6" runat="server" ErrorMessage="Harap isi dengan angka antara 0 s/d 100"
                                    MinimumValue="0" CssClass="validator_general" Type="Double" ControlToValidate="txtPSalesPerson"
                                    Display="Dynamic" MaximumValue="100"></asp:RangeValidator>
                            </div>
                            <div class="form_3">
                                <asp:Label ID="lblFSalesPerson" runat="server"></asp:Label>
                                <asp:TextBox ID="txtFSalesPerson" runat="server" Width="80%" MaxLength="15"></asp:TextBox>
                                <asp:RangeValidator ID="Rangevalidator9" runat="server" ErrorMessage="harap isi dengan angka"
                                    MinimumValue="0" Type="Double" CssClass="validator_general" ControlToValidate="txtFSalesPerson"
                                    Display="Dynamic" MaximumValue="999999999999999"></asp:RangeValidator>
                            </div>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <div class="form_1">
                                <label>
                                    Supplier Admin</label>
                            </div>
                            <div class="form_2">
                                <asp:Label ID="lblPSuppAdm" runat="server"></asp:Label>
                                <asp:TextBox ID="txtPSuppAdm" runat="server" Width="80%" MaxLength="15"></asp:TextBox>
                                <asp:RangeValidator ID="Rangevalidator7" runat="server" ErrorMessage="Harap isi dengan angka antara 0 s/d 100"
                                    MinimumValue="0" CssClass="validator_general" Type="Double" ControlToValidate="txtPSuppAdm"
                                    Display="Dynamic" MaximumValue="100"></asp:RangeValidator>
                            </div>
                            <div class="form_3">
                                <asp:Label ID="lblFSuppAdm" runat="server"></asp:Label>
                                <asp:TextBox ID="txtFSuppAdm" runat="server" Width="80%" MaxLength="15"></asp:TextBox>
                                <asp:RangeValidator ID="Rangevalidator8" runat="server" ErrorMessage="Harap isi dengan angka"
                                    MinimumValue="0" CssClass="validator_general" Type="Double" ControlToValidate="txtFSuppAdm"
                                    Display="Dynamic" MaximumValue="999999999999999"></asp:RangeValidator>
                            </div>
                        </div>
                    </div>
                    <div class="form_box_header">
                        <div class="form_single">
                            <h4>
                                Pembayaran Incentive
                            </h4>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <div class="form_1">
                                <label>
                                    Penerima</label>
                            </div>
                            <div class="form_2">
                                <label>
                                    Calculation Base</label>
                            </div>
                            <div class="form_3">
                                <label>
                                    Cara Pembayaran Incentive</label>
                            </div>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <div class="form_1">
                                <label>
                                    Karyawan</label>
                            </div>
                            <div class="form_2">
                                <label>
                                    Unit</label>
                            </div>
                            <div class="form_3">
                                <asp:Label class="label_general" ID="lblEUnit" runat="server"></asp:Label>
                                <asp:RadioButtonList ID="rboEUnit" runat="server" class="opt_single" RepeatDirection="Horizontal">
                                    <asp:ListItem Selected="True" Value="D">Daily</asp:ListItem>
                                    <asp:ListItem Value="M">Monthly</asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <div class="form_1">
                                <label>
                                    karyawan</label>
                            </div>
                            <div class="form_2">
                                <label>
                                    Fixed Amount</label>
                            </div>
                            <div class="form_3">
                                <asp:Label ID="lblEAF" class="label_general" runat="server"></asp:Label>
                                <asp:RadioButtonList ID="rboEAF" runat="server" class="opt_single" RepeatDirection="Horizontal">
                                    <asp:ListItem Value="D" Selected="True">Daily</asp:ListItem>
                                    <asp:ListItem Value="M">Monthly</asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <div class="form_1">
                                <label>
                                    Karyawan</label>
                            </div>
                            <div class="form_2">
                                <label>
                                    Refund</label>
                            </div>
                            <div class="form_3">
                                <asp:Label ID="lblERefund" class="label_general" runat="server"></asp:Label>
                                <asp:RadioButtonList ID="rboERefund" runat="server" class="opt_single" RepeatDirection="Horizontal">
                                    <asp:ListItem Value="D" Selected="True">Daily</asp:ListItem>
                                    <asp:ListItem Value="M">Monthly</asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <div class="form_1">
                                <label>
                                    Perusahaan</label>
                            </div>
                            <div class="form_2">
                                <label>
                                    Unit</label>
                            </div>
                            <div class="form_3">
                                <asp:Label ID="lblCUnit" runat="server" class="label_general"></asp:Label>
                                <asp:RadioButtonList ID="rboCUnit" runat="server" class="opt_single" RepeatDirection="Horizontal">
                                    <asp:ListItem Value="D" Selected="True">Daily</asp:ListItem>
                                    <asp:ListItem Value="M">Monthly</asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <div class="form_1">
                                <label>
                                    Perusahaan</label>
                            </div>
                            <div class="form_2">
                                <label>
                                    Fixed Amount</label>
                            </div>
                            <div class="form_3">
                                <asp:Label ID="lblCAF" runat="server" class="label_general"></asp:Label>
                                <asp:RadioButtonList ID="rboCAF" runat="server" class="opt_single" RepeatDirection="Horizontal">
                                    <asp:ListItem Value="D" Selected="True">Daily</asp:ListItem>
                                    <asp:ListItem Value="M">Monthly</asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <div class="form_1">
                                <label>
                                    Perusahaan</label>
                            </div>
                            <div class="form_2">
                                <label>
                                    Refund</label>
                            </div>
                            <div class="form_3">
                                <asp:Label ID="lblCRefund" runat="server" class="label_general"></asp:Label>
                                <asp:RadioButtonList ID="rboCRefund" runat="server" class="opt_single" RepeatDirection="Horizontal">
                                    <asp:ListItem Value="D" Selected="True">Daily</asp:ListItem>
                                    <asp:ListItem Value="M">Monthly</asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <asp:HyperLink ID="lnkBudget" runat="server" Visible="false">|Budget & Forecast|</asp:HyperLink>
                        </div>
                    </div>
                </asp:Panel>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
