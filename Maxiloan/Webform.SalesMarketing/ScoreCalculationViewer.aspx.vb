﻿#Region "Imports"
Imports System.IO
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
#End Region

Public Class ScoreCalculationViewer
    Inherits Maxiloan.Webform.WebBased


#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(viewstate("cmdwhere"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("cmdwhere") = Value
        End Set
    End Property
    Private Property BDate() As String
        Get
            Return CType(viewstate("BDate"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("BDate") = Value
        End Set
    End Property
#End Region
#Region "Constanta"
    Private m_coll As New PPKController
    Private oCustomClass As New Parameter.PPK
#End Region
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        GetCookies()
        BindReport()
        'CreateData()
    End Sub


#Region "BindReport"
    Sub BindReport()
        Dim oData As New DataSet
        Dim m_controller As New PPKController
        Dim oConfLetter As New Parameter.PPK

        'Dim objReport As ConfirmationLetterPrint = New ConfirmationLetterPrint
        'Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions

        'oConfLetter.strConnection = GetConnectionString
        'oConfLetter.WhereCond = Me.CmdWhere
        'oConfLetter.BranchId = Me.sesBranchId.Replace("'", "")
        'oConfLetter.LoginId = Me.Loginid
        'oConfLetter = m_controller.ListReportConfLetter(oConfLetter)
        'oData = oConfLetter.ListReport

        'objReport.SetDataSource(oConfLetter.ListReport)
        ''Wira 2015-07-02
        'AddParamField(objReport, "CompanyName", GetCompanyName())

        'CrystalReportViewer1.ReportSource = objReport
        'CrystalReportViewer1.Visible = True
        'CrystalReportViewer1.DataBind()

        'objReport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
        'objReport.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat

        'Dim strFileLocation As String
        'strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
        'strFileLocation += Me.Session.SessionID + Me.Loginid + "rpt_ConfLetter.pdf"
        'DiskOpts.DiskFileName = strFileLocation
        ''oPPK.PrintOptions.PaperSize = PaperSize.PaperA4
        'objReport.ExportOptions.DestinationOptions = DiskOpts
        'objReport.Export()
        'objReport.Close()
        'objReport.Dispose()
        Response.Redirect("ScoreCalculation.aspx?strFileLocation=" & Me.Session.SessionID & Me.Loginid & "RptScore")

    End Sub

#End Region

#Region "GetCookies"
    Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies("RptScore")
        Me.CmdWhere = cookie.Values("cmdwhere")
        Me.BDate = cookie.Values("BusinessDate")
    End Sub
#End Region
End Class