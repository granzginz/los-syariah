﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SupplierBudget.aspx.vb"
    Inherits="Maxiloan.Webform.SalesMarketing.SupplierBudget" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>SupplierBudget</title>
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        function CheckValues() {

            if (document.forms[0].txtyearAdd.value <= document.forms[0].inyear.value) {
                alert('Year Must be > year Business Date');
                document.forms[0].txtyearAdd.focus();

                return false;
            }

            else {
                return true;
            }
            return true;
        }


        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';							
    </script>
</head>
<body>
    <form id="form1" runat="server">    
    <asp:Label ID="lblMessage" runat="server" Visible ="false" ></asp:Label>                         
    <input id="inyear" type="hidden" name="inyear" runat="server" />
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">                  
            <h3>
                DAFTAR BUDGET SUPPLIER
            </h3>
        </div>
    </div>        
    <asp:Panel ID="pnlDatagrid" runat="server" Visible="false">
    <div class="form_box_header">
    <div class="form_single">
        <div class="grid_wrapper_ns">
                <asp:DataGrid ID="DtgSupplier" runat="server" Visible="true"
                        AllowSorting="True" OnSortCommand="SortGrid" AutoGenerateColumns="False" DataKeyField="BudgetYear"
                        BorderStyle="None" BorderWidth="0" Width="100%" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="EDIT">
                                <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" ImageUrl="../Images/iconedit.gif"
                                        CommandName="Edit"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="BudgetYear" HeaderText="TAHUN">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="HyYear" runat="server" Text='<%#Container.DataItem("Budgetyear")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="BudgetMonth " HeaderText="Month" Visible="False">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblBMonth" runat="server" Text='<%#Container.DataItem("BudgetMonth")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="BudgetMonthDesc" HeaderText="BULAN">
                                <ItemTemplate>
                                    <asp:Label ID="lblMONTH" runat="server" Text='<%#Container.DataItem("BudgetMonthDesc")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AssetStatusdesc" HeaderText="STATUS ASSET">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblAssetStatus" runat="server" Text='<%#Container.DataItem("AssetStatusdesc")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Unit" HeaderText="UNIT">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblUnit" runat="server" Text='<%#Container.DataItem("Unit")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AMOUNT" HeaderText="JUMLAH">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblAmount" runat="server" Text='<%#formatnumber(Container.DataItem("AMOUNT"),2)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>                       
                </asp:DataGrid>               
                <div class="button_gridnavigation">
	                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../Images/grid_navbutton01.png"
		                CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../Images/grid_navbutton02.png"
		                CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../Images/grid_navbutton03.png"
		                CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../Images/grid_navbutton04.png"
		                CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
	                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
		                EnableViewState="False"></asp:Button>
	                <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage"
		                MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer"
		                CssClass="validator_general"></asp:RangeValidator>
	                <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
		                ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>
                <div class="label_gridnavigation">
	                <asp:Label ID="lblPage" runat="server"></asp:Label>of
	                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
	                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                </div>
        </div>                  
    </div>
    </div>
    <div class="form_button">
            <asp:Button ID="ButtonAdd" runat="server" CausesValidation="False" Text="Add" CssClass ="small button blue">
            </asp:Button>
            <asp:Button ID="ButtonPrint" runat="server" Enabled="true" Text="Print" CssClass ="small button blue">
            </asp:Button>
            <asp:Button ID="ButtonBack" runat="server" CausesValidation="False" Text="Back" CssClass ="small button gray">
            </asp:Button>
    </div>              
    </asp:Panel>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_box_header">
            <div class="form_single">              
                <h4>
                    CARI BUDGET SUPPLIER
                </h4>
            </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Nama Supplier</label>
                <asp:HyperLink ID="hySupplierName" runat="server"></asp:HyperLink>
                <asp:Label ID="lblSupplierId" runat="server" Visible="False"></asp:Label>
	        </div>
        </div> 
        <div class="form_box">
	        <div class="form_single">
                <div class="form_left">
                    <label>Tahun</label>
                    <asp:TextBox ID="txtyear" runat="server"  MaxLength="4"></asp:TextBox>
		        </div>
		        <div class="form_right">		
                    <label>Bulan</label>	
                    <asp:DropDownList ID="cboMonth" runat="server">
                        <asp:ListItem Value="ALL" Selected="True">ALL</asp:ListItem>
                        <asp:ListItem Value="1">Januari</asp:ListItem>
                        <asp:ListItem Value="2">Februari</asp:ListItem>
                        <asp:ListItem Value="3">Maret</asp:ListItem>
                        <asp:ListItem Value="4">April</asp:ListItem>
                        <asp:ListItem Value="5">Mei</asp:ListItem>
                        <asp:ListItem Value="6">Juni</asp:ListItem>
                        <asp:ListItem Value="7">Juli</asp:ListItem>
                        <asp:ListItem Value="8">Agustus</asp:ListItem>
                        <asp:ListItem Value="9">September</asp:ListItem>
                        <asp:ListItem Value="10">Oktober</asp:ListItem>
                        <asp:ListItem Value="11">November</asp:ListItem>
                        <asp:ListItem Value="12">Desember</asp:ListItem>
                    </asp:DropDownList>
		        </div>
	        </div>
        </div> 
        <div class="form_box">
	        <div class="form_single">
                <label>Status Asset</label>
                <asp:DropDownList ID="cboStatus" runat="server">
                    <asp:ListItem Value="ALL" Selected="True">ALL</asp:ListItem>
                    <asp:ListItem Value="N">New</asp:ListItem>
                    <asp:ListItem Value="U">Used</asp:ListItem>
                </asp:DropDownList>
	        </div>
        </div>        
        <div class="form_button">
            <asp:Button ID="ButtonSearch" runat="server" Text="Search" CssClass ="small button blue"></asp:Button>
            <asp:Button ID="ButtonReset" runat="server" CausesValidation="False" Text="Reset" CssClass ="small button gray"></asp:Button>
        </div>       
    </asp:Panel>
    <asp:Panel ID="pnlAdd" Visible="false" runat="server">        
        <div class="form_box_header">
            <div class="form_single">                              
                <h4>
                    ADD - BUDGET SUPPLIER
                </h4>
            </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>ID Supplier</label>
                <asp:HyperLink ID="HYSupplierID2" runat="server"></asp:HyperLink>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Nama Supplier</label>
                <asp:Label ID="lblNameAdd" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
             <label class ="label_req">  Tahun (yyyy)</label>
                <asp:TextBox ID="txtyearAdd" runat="server" Width="111px" ></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Visible="true" CssClass="validator_general"
                    Display="Dynamic" ErrorMessage="Harap diisi Tahun" ControlToValidate="txtYearAdd"></asp:RequiredFieldValidator>
                <asp:RangeValidator ID="rvYear" runat="server" Display="Dynamic" ErrorMessage="Harap diisi dengan angka" CssClass="validator_general"
                    ControlToValidate="txtyearAdd" MaximumValue="9900" MinimumValue="1900" Type="Integer">Tahun harus angka</asp:RangeValidator>
	        </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">              
                <h4>
                    DATA BUDGET
                </h4>
            </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <div class="form_1">
                    <label>Bulan</label>
                </div>
                <div class="form_2">
                    <label>Asset Baru</label>
                </div>
                <div class="form_3">
                    <label>Asset Bekas</label>
                </div>
	        </div>
        </div>
        <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="DtgBudgetAdd" runat="server" Visible="False"
                        AllowSorting="True" AutoGenerateColumns="False" CssClass="grid_general" BorderStyle="None" BorderWidth="0" Width="100%">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn Visible="False">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblNo" runat="server" Text='<%#container.dataitem("No")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn>                                
                                <ItemTemplate>
                                    <asp:Label ID="lblMonthAdd" runat="server">
												<%#container.dataitem("InMonth")%>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn>                                
                                <ItemTemplate>
                                    Unit
                                    <asp:TextBox ID="txtUnit" runat="server" Width="50px"  Text='<%#container.dataitem("Unit")%>'>
                                    </asp:TextBox>
                                    <asp:RangeValidator ID="Rangevalidator1" runat="server" MaximumValue="9999" Display="Dynamic" CssClass="validator_general"
                                        ErrorMessage="Harap isi dengan angka" MinimumValue="0" Type="Double" ControlToValidate="txtUnit"></asp:RangeValidator>
                                    Amount
                                    <asp:TextBox ID="txtAmount" runat="server" Width="100px"  Text='<%#container.dataitem("Amount")%>'>
                                    </asp:TextBox>
                                    <asp:RangeValidator ID="Rangevalidator3" runat="server" MaximumValue="999999999999999"
                                        Display="Dynamic" ErrorMessage="Harap isi dengan angka" MinimumValue="0" Type="Double" CssClass="validator_general"
                                        ControlToValidate="txtAmount"></asp:RangeValidator>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn>                                
                                <ItemTemplate>
                                    Unit
                                    <asp:TextBox ID="txtUnitUsed" runat="server" Width="50px"  Text='<%#container.dataitem("Unit")%>'>
                                    </asp:TextBox>
                                    <asp:RangeValidator ID="Rangevalidator42" runat="server" MaximumValue="9999" Display="Dynamic" CssClass="validator_general"
                                        ErrorMessage="Harap isi dengan angka" MinimumValue="0" Type="Double" ControlToValidate="txtUnitUsed"></asp:RangeValidator>
                                    Amount
                                    <asp:TextBox ID="txtAmountUsed" runat="server" Width="100px"  Text='<%#container.dataitem("Amount")%>'>
                                    </asp:TextBox>
                                    <asp:RangeValidator ID="Rangevalidator2" runat="server" MaximumValue="999999999999999" CssClass="validator_general"
                                        Display="Dynamic" ErrorMessage="Harap isi dengan angka" MinimumValue="0" Type="Double"
                                        ControlToValidate="txtAmountUsed"></asp:RangeValidator>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>                        
                </asp:DataGrid>
	        </div>
        </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonSaveAdd" runat="server" CausesValidation="true" Text="Save" CssClass ="small button blue">
            </asp:Button>
            <asp:Button ID="ButtonBackAdd" runat="server" CausesValidation="False" Text="Back" CssClass ="small button gray">
            </asp:Button>
        </div>                        
    </asp:Panel>
    <asp:Panel ID="pnlEdit" runat="server" Visible="False">
        <div class="form_box_header">
            <div class="form_single">              
                <h4>
                    EDIT - BUDGET SUPPLIER
                </h4>
            </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>ID Supplier</label>
                <asp:HyperLink ID="HySupplierIDEdit" runat="server"></asp:HyperLink>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Nama Supplier</label>
                <asp:Label ID="lblSupNameEdit" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Tahun</label>
                 <asp:Label ID="lblYearEdit" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Bulan</label>
                <asp:Label ID="lblMonthEdit" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Status Asset</label>
                <asp:Label ID="lblStatusEdit" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                 <label class ="label_req">
				 Unit</label>
                <asp:TextBox ID="txtUnitEdit" Width="81px"  runat="server"></asp:TextBox>
                <asp:RangeValidator ID="Rangevalidator4" Display="Dynamic" runat="server" ErrorMessage="Harap isi dengan angka" CssClass="validator_general"
                    ControlToValidate="txtUnitEdit" MaximumValue="9999" MinimumValue="0" Type="Double"></asp:RangeValidator>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" Visible="true" CssClass="validator_general"
                    Display="Dynamic" ErrorMessage="Harap diisi" ControlToValidate="txtUnitEdit"></asp:RequiredFieldValidator>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                 <label class ="label_req">
				 Jumlah</label>
        <asp:TextBox ID="txtAmountEdit"  runat="server"></asp:TextBox>
        <asp:RangeValidator ID="Rangevalidator5" Display="Dynamic" runat="server" ErrorMessage="Harap isi dengan angka" CssClass="validator_general"
                    ControlToValidate="txtAmountEdit" MaximumValue="999999999999999" MinimumValue="0"
                    Type="Double"></asp:RangeValidator>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator3" runat="server" Visible="true" CssClass="validator_general"
                    Display="Dynamic" ErrorMessage="harap diisi" ControlToValidate="txtAmountEdit"></asp:RequiredFieldValidator>
	        </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonSaveEdit" runat="server" CausesValidation="true" Text="Save" CssClass ="small button blue">
            </asp:Button>
            <asp:Button ID="ButtonCancelEdit" runat="server" CausesValidation="False" Text="Cancel" CssClass ="small button gray">
            </asp:Button>
        </div>       
    </asp:Panel>
    </form>
</body>
</html>
