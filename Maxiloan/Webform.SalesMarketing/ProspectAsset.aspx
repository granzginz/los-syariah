﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ProspectAsset.aspx.vb"
    Inherits="Maxiloan.Webform.SalesMarketing.ProspectAsset" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../webform.UserController/ucProspectTab.ascx" TagName="ucProspectTab" TagPrefix="uc1" %>
<%@ Register Src="../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat" TagPrefix="uc1" %>
<%@ Register Src="../webform.UserController/ucDateCE.ascx" TagName="ucDateCE" TagPrefix="uc1" %> 

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Prospect Maintenance Unit</title>
    <link rel="Stylesheet" type="text/css" href="../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../Include/Buttons.css" />
    
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <link href="../include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../js/jquery-1.9.1.min.js"></script>
    <script src="../js/jquery-ui-1.10.3.custom.min.js"></script>
     
    <script language="javascript" type="text/javascript">
        //modify by nofi tgl 30jan2018 =>hasil testing pak agus rdoReferensiChange()
        $(document).ready(function () {
            cboKondisiAssetChange();
            cboAssetChange();
        });
        function UpdateKaroseri(a) {
            return true
        }
        function recalculateAngs() {

//            var otr = $('#ucOTR_txtNumber').val();
//            var uangmuka = $('#ucUangMuka_txtNumber').val();
//            var uangmukatradein = $('#ucUangMukaTradeIn_txtNumber').val();
            var otr = $('#lblOTR_txtNumber').val();
            var uangmuka = $('#lblUangMuka_txtNumber').val();
            var uangmukatradein = $('#lblUangMukaTradeIn_txtNumber').val();

            var xtotal = parseInt(otr.replace(/\s*,\s*/g, '')) -
                         parseInt(uangmuka.replace(/\s*,\s*/g, '')) +
                         parseInt(uangmukatradein.replace(/\s*,\s*/g, ''));
            var totaluangmuka = parseInt(uangmuka.replace(/\s*,\s*/g, '')) +
                         parseInt(uangmukatradein.replace(/\s*,\s*/g, ''));

            $('#lblTotalUangMuka').html(number_format(totaluangmuka));
            $('#lblPokokHutang').html(number_format(xtotal));
        }
        function number_format(number, decimals, dec_point, thousands_sep) {

            number = (number + '')
            .replace(/[^0-9+\-Ee.]/g, '');
            var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function (n, prec) {
                var k = Math.pow(10, prec);
                return '' + (Math.round(n * k) / k)
                .toFixed(prec);
            };
            // Fix for IE parseFloat(0.55).toFixed(0) = 0;
            s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
            .split('.');
            if (s[0].length > 3) {
                s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
            }
            if ((s[1] || '')
            .length < prec) {
                s[1] = s[1] || '';
                s[1] += new Array(prec - s[1].length + 1)
              .join('0');
            }
            return s.join(dec);
        }

        function getAge() {
            var dateString = $('#tanggalLahir').val()
            //            var Instmonth = parseInt($('#ucTenor_txtNumber').val());
            var Instmonth = parseInt($('#lblTenor_txtNumber').val());
            var now = new Date();
            now.setMonth(now.getMonth() + Instmonth);
            var today = new Date(now.getYear(), now.getMonth(), now.getDate());
           
            var yearNow = now.getYear();
            var monthNow = now.getMonth();
            var dateNow = now.getDate();


            var dob = new Date(dateString.substring(0, 4),
                     dateString.substring(7, 8) - 1,
                     dateString.substring(5, 6)
                     );

            var yearDob = dob.getYear();
            var monthDob = dob.getMonth();
            var dateDob = dob.getDate();
            var age = {};
            var ageString = "";
            var yearString = "";
            var monthString = "";
            var dayString = "";


            yearAge = yearNow - yearDob;

            if (monthNow >= monthDob)
                var monthAge = monthNow - monthDob;
            else {
                yearAge--;
                var monthAge = 12 + monthNow - monthDob;
            }

            if (dateNow >= dateDob)
                var dateAge = dateNow - dateDob;
            else {
                monthAge--;
                //                var dateAge = 31 + dateNow - dateDob;
                var dateAge = 31 + dateNow - dateDob;


                if (monthAge < 0) {
                    monthAge = 11;
                    yearAge--;
                }
            }

            age = {
                years: yearAge,
                months: monthAge,
                days: dateAge
            };

            if (age.years > 1) yearString = " tahun";
            else yearString = " tahun";
            if (age.months > 1) monthString = " bulan";
            else monthString = " bulan";
            if (age.days > 1) dayString = " hari";
            else dayString = " hari";


            if ((age.years > 0) && (age.months > 0) && (age.days > 0))
                ageString = age.years + yearString + ", " + age.months + monthString + ", dan " + age.days + dayString + " ";
            else if ((age.years == 0) && (age.months == 0) && (age.days > 0))
                ageString = "Only " + age.days + dayString + " ";
            else if ((age.years > 0) && (age.months == 0) && (age.days == 0))
                ageString = age.years + yearString + " ";
            else if ((age.years > 0) && (age.months > 0) && (age.days == 0))
                ageString = age.years + yearString + " dan " + age.months + monthString + " ";
            else if ((age.years == 0) && (age.months > 0) && (age.days > 0))
                ageString = age.months + monthString + " dan " + age.days + dayString + " ";
            else if ((age.years > 0) && (age.months == 0) && (age.days > 0))
                ageString = age.years + yearString + " dan " + age.days + dayString + " ";
            else if ((age.years == 0) && (age.months > 0) && (age.days == 0))
                ageString = age.months + monthString + " ";
            else ageString = "Oops! Tidak dapat menghitung usia akhir kontrak!";

            $('#lblusiaakhirkontrak').html(ageString);
        }
        function cboKondisiAssetChange() {
            var cbokondisi = eval('document.forms[0].cboKondisiAsset');
            if (cbokondisi != null) {
                if (eval('document.forms[0].cboKondisiAsset').value == 'U') { 
                    var theButton = document.getElementById('<%=txtNoPolisi.ClientID%>');
                    var theButton1 = document.getElementById('<%=txtNoSertifikat.ClientID%>');
                    theButton.disabled = false;
                    theButton1.disabled = false;
                }
                else { 
                    var theButton = document.getElementById('<%=txtNoPolisi.ClientID%>');
                    var theButton1 = document.getElementById('<%=txtNoSertifikat.ClientID%>');
                    theButton.disabled = true;
                    theButton1.disabled = true;
                }
            } 
        }
        function cboAssetChange() {
            var cboasset = document.getElementById("cboAsset");
            var nopol = document.getElementById("nopol");
            var noserti = document.getElementById("noserti");
            nopol.style.display = cboasset.value == "OTOMOTIF" ? "block" : "none";
            noserti.style.display = cboasset.value == "KPR" ? "block" : "none";
         }
    </script>
    
</head>
<body>
    <asp:UpdateProgress ID="upg1" runat="server" AssociatedUpdatePanelID="upnl1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <div runat="server" id="jlookupContent" />
    <asp:UpdatePanel runat="server" ID="upnl1" UpdateMode="Conditional" ChildrenAsTriggers="false">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
                onclick="hideMessage();"></asp:Label>
            <uc1:ucProspectTab id="ucProspectTab1" runat="server" />
            <div class="form_title">
                <div class="form_single">
                    <h3>
                        DATA UNIT</h3>
                </div>
            </div>
<%--            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="">
                           Type Asset
                        </label>
                        <asp:DropDownList ID="cboTypeAsset" runat="server" Width="150PX" AutoPostBack="true" >
                                <asp:ListItem Text="AGRI" Value="AGRI" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="MOBIL" Value="MOBIL" ></asp:ListItem>
                            </asp:DropDownList>
                        <asp:DropDownList ID="cboStatusKepemilikan" runat="server">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" Display="Dynamic"
                            InitialValue="Select One" ControlToValidate="cboStatusKepemilikan" ErrorMessage="Harap pilih Status Kepemilikan"
                            CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>--%>
            <%--modify Nofi 2Mar2018--%>
            <div class="form_box_hide">
                 <div>
                <div class="form_single">
                        <div style="float:left">
                        <label class="label_req">
                            Supplier/Dealer</label>      
                            <asp:Label ID="lblSupplierName" runat="server"></asp:Label>
                        </div>
                </div>
            </div>
            </div>
            <div class="form_box_hide">
                 <div>
                <div class="form_single">
                        <div style="float:left">
                        <label class="label_req">
                            Jenis Unit</label>                     
                        <asp:HiddenField runat="server" ID="hdfAssettypeCode" />
                        <%--<asp:HiddenField runat="server" ID="hdfAssetTypeID2" />--%>
                        
                            <%--<asp:TextBox runat="server" ID="txtAssetType" Enabled="false" CssClass="medium_text"></asp:TextBox>--%>
                            <asp:Label ID="lblAssetType" runat="server"></asp:Label>
                        </div>
<%--                        <div style="margin-top:2px">
                            <asp:Panel ID="pnlLookupAssetType" runat="server">
                            <button class="small buttongo blue" 
                            onclick ="OpenJLookup('<%= ResolveClientUrl("~/webform.Utility/jLookup/AssetType.aspx?kode=" & hdfAssettypeCode.ClientID & "&nama=" & txtAssetType.ClientID &"&AssetTypeID="& hdfAssetTypeID2.Value) %>','Daftar Supplier','<%= jlookupContent.ClientID %>');return false;">...</button>                                                                            
                            </asp:Panel>
                        </div> --%>
                 </div>
            </div>
            </div>
            <div class="form_box_hide">
                 <div>
                <div class="form_single">
                        <div style="float:left">
                        <label class="label_req">
                            Merk Unit</label>                     
                        <asp:HiddenField runat="server" ID="hdfAssetBrandCode" />                                        
                        <%--<asp:HiddenField runat="server" ID="hdfAssetTypeID" />--%>
                        
                            <%--<asp:TextBox runat="server" ID="txtAssetBrand" Enabled="false" CssClass="medium_text"></asp:TextBox>--%>
                            <asp:Label ID="lblAssetBrand" runat="server"></asp:Label>
                        </div>
<%--                        <div style="margin-top:2px">
                            <asp:Panel ID="pnlLookupAssetBrand" runat="server">
                            <button class="small buttongo blue" 
                            onclick ="OpenJLookup('<%= ResolveClientUrl("~/webform.Utility/jLookup/AssetBrand.aspx?kode=" & hdfAssetBrandCode.ClientID & "&nama=" & txtAssetBrand.ClientID &"&AssetTypeID="& hdfAssetTypeID.Value) %>','Daftar Supplier','<%= jlookupContent.ClientID %>');return false;">...</button>                                                                            
                            </asp:Panel>
                        </div> --%>
                 </div>
            </div>
            </div>
<%--            <div class="form_box">
                <uc1:ucProdOffering id="ucLookupProductOffering1" runat="server"></uc1:ucProdOffering>
            </div>--%>
<%--            <div class="form_box">
                <div>
                <div class="form_single">
                        <div style="float:left">
                        <label class="label_req">
                            Jenis Asset</label>                            
                        <asp:HiddenField runat="server" ID="hdfAssetCode" />     
                        <asp:HiddenField runat="server" ID="hdfAssetTypeID" />
                        
                            <asp:TextBox runat="server" ID="txtAssetName" Enabled="false" CssClass="medium_text"></asp:TextBox>
                        </div>
                        <div style="margin-top:2px">
                            <asp:Panel ID="pnlLookupAsset" runat="server">
                            <button class="small buttongo blue" 
                            onclick ="OpenJLookup('<%= ResolveClientUrl("~/webform.Utility/jLookup/Asset.aspx?kode=" & hdfAssetCode.ClientID & "&nama=" & txtAssetName.ClientID &"&AssetTypeID="& hdfAssetTypeID.Value) %>','Daftar Supplier','<%= jlookupContent.ClientID %>');return false;">...</button>                                                                            
                            </asp:Panel>
                        </div> 
                </div>
                </div>
            </div>--%>
           <%-- <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="">
                           Kategori Asset
                        </label>
                        <asp:Label ID="lblKategoriAsset" runat="server" ></asp:Label>
                    </div>
                </div>
            </div>--%>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="">
                           Unit
                        </label>
                        <asp:DropDownList ID="cboAsset" runat="server" Width="150PX" AutoPostBack="true" onclick="<%#cboAssetChange()%>">
                                <asp:ListItem Text="OTOMOTIF" Value="OTOMOTIF" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="KPR" Value="KPR" ></asp:ListItem>
                            </asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="">
                           Kondisi Unit
                        </label>
                        <asp:DropDownList ID="cboKondisiAsset" runat="server" Width="150PX" AutoPostBack="true" onclick="<%#cboKondisiAssetChange()%>">
                                <asp:ListItem Text="BARU" Value="N" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="BEKAS" Value="U" ></asp:ListItem>
                            </asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="label_req">
                           Tahun Pembuatan
                        </label>
                        <asp:TextBox ID="txtYear" runat="server" MaxLength="4" CssClass="small_text" onkeypress="return numbersonly2(event)"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RFVYear" runat="server" ErrorMessage="Harap isi Tahun Produksi!"
                            ControlToValidate="txtYear" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="RangeValidator3" runat="server" Type="Integer" MinimumValue="0"
                            ErrorMessage="Tahun harus < hari Ini" ControlToValidate="txtYear" Display="Dynamic"
                            CssClass="validator_general"></asp:RangeValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Tahun Salah"
                            ControlToValidate="txtYear" Display="Dynamic" ValidationExpression="\d{4}" CssClass="validator_general"></asp:RegularExpressionValidator>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="label_req">
                           Penggunaan
                        </label>
                        <asp:DropDownList ID="cboUsage" runat="server" Width="150PX">
                            </asp:DropDownList>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="cboUsage"
                                CssClass="validator_general" Display="Dynamic" ErrorMessage="Harap pilih Penggunaan!"
                                InitialValue="Select One" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
            <div class="form_box" id="noserti">
                <div>
                    <div class="form_single">
                        <label class="">
                           No Sertifikat
                        </label>
                        <asp:TextBox ID="txtNoSertifikat" runat="server" CssClass="medium_text"  MaxLength="20"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RFNoSertifikat" runat="server" ControlToValidate="txtNoSertifikat"
                                CssClass="validator_general" Display="Dynamic" ErrorMessage="Harap isi nomor sertifikat!"
                                SetFocusOnError="true"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="">
                           Paket
                        </label>
                        <asp:DropDownList ID="cboPackage" runat="server">
                            <asp:ListItem Value="S" Selected="True">SUBSIDI</asp:ListItem>
                            <asp:ListItem Value="N">NON SUBSIDI</asp:ListItem>
                            <%--<asp:ListItem Value="P">PICK UP</asp:ListItem>
                            <asp:ListItem Value="A">AGRICULTURE</asp:ListItem>
                            <asp:ListItem Value="O">OTHER (PASSENGER)</asp:ListItem>--%>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic"
                            InitialValue="" ControlToValidate="cboPackage" ErrorMessage="Harap pilih Paket"
                            CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div> 
            
            <%--tidak terpakai--%>
            <div class="form_box" id="nopol" style="visibility:hidden">
                <div>
                    <div class="form_single">
                        <label class="">
                           No Polisi
                        </label>
                        <asp:TextBox ID="txtNoPolisi" runat="server" CssClass="medium_text"  MaxLength="20"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RFNoPolisi" runat="server" ControlToValidate="txtNoPolisi"
                                CssClass="validator_general" Display="Dynamic" ErrorMessage="Harap isi nomor polisi!"
                                SetFocusOnError="true"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
            <div class="form_box_title">
                <div class="form_single">
                    <h4>
                       DATA PINJAMAN</h4>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="label_req">
                           Harga OTR
                        </label>
                        <%--<uc1:ucnumberformat runat="server" id="ucOTR" TextCssClass="numberAlign reguler_text" onclientchange="recalculateAngs()" ></uc1:ucnumberformat>--%>                        
                        <asp:Label ID="lblOTR" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="">
                           URBUN Tunai
                        </label>
                        <%--<uc1:ucnumberformat runat="server" id="ucUangMuka" TextCssClass="numberAlign reguler_text" onclientchange="recalculateAngs()" ></uc1:ucnumberformat>--%>                        
                        <asp:Label ID="lblUangMuka" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box_hide">
                <div>
                    <div class="form_single">
                        <label class="">
                           URBUN Trade In
                        </label>
                        <%--<uc1:ucnumberformat runat="server" id="ucUangMukaTradeIn" TextCssClass="numberAlign reguler_text"  onclientchange="recalculateAngs()" ></uc1:ucnumberformat>--%>                        
                        <asp:Label ID="lblUangMukaTradeIn" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="">
                           Total URBUN
                        </label>
                        <asp:Label runat="server" ID="lblTotalUangMuka"  CssClass="numberAlign regular_text"></asp:Label>
                    </div>
                </div>
            </div> 
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="">
                           Pokok Hutang
                        </label>
                        <asp:Label runat="server" ID="lblPokokHutang"  CssClass="numberAlign regular_text"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="label_req">
                           Angsuran Bulanan
                        </label>
                        <%--<uc1:ucnumberformat runat="server" id="ucAngsuranBulanan" TextCssClass="numberAlign reguler_text" ></uc1:ucnumberformat>--%>                        
                        <asp:Label ID="lblAngsuranBulanan" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="label_req">
                           Jangka Waktu
                        </label>
                        <asp:HiddenField runat="server" ID="tanggalLahir" />
                        <%--<uc1:ucnumberformat runat="server" id="ucTenor" TextCssClass="numberAlign reguler_text" onclientchange="getAge()" ></uc1:ucnumberformat>--%>                        
                        <asp:Label ID="lblTenor" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="">
                           Usia Akhir Kontrak
                        </label>
                        <asp:Label runat="server" ID="lblusiaakhirkontrak"  CssClass="regular_text"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="label_req">
                           Cara Pembayaran
                        </label>
                        <asp:DropDownList ID="cboWayPymt" runat="server">
                                <asp:ListItem Value="TF" Selected="True">Transfer</asp:ListItem>
                                <asp:ListItem Value="PD">PDC</asp:ListItem>
                                <asp:ListItem Value="CA">Tunai</asp:ListItem>
                                <asp:ListItem Value="AD">Auto Debit</asp:ListItem>
                                <asp:ListItem Value="CO">Collect</asp:ListItem>
                                <asp:ListItem Value="MM">Mini Market</asp:ListItem>
                                <asp:ListItem Value="KP">Kantor Pos</asp:ListItem>
                            </asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue"
                    CausesValidation="True"></asp:Button>
                <asp:Button ID="btnCancel" runat="server" Text="Back" CssClass="small button gray"
                    CausesValidation="False"></asp:Button>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
    </form>
</body>
</html>
