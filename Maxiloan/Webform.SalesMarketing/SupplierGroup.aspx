﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SupplierGroup.aspx.vb"
    Inherits="Maxiloan.Webform.SalesMarketing.SupplierGroup" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcCompanyAddress" Src="../Webform.UserController/UcCompanyAddress.ascx" %>
<%@ Register Src="../Webform.UserController/ucDateCE.ascx" TagName="ucDateCE" TagPrefix="uc2" %>
<%@ Register src="../Webform.UserController/ucNumberFormat.ascx" tagname="ucNumberFormat" tagprefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>DAFTAR GROUP SUPPLIER</title>
     <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <link href="../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
    <script language="JavaScript" type="text/javascript">
        function fBack() {
            history.back(-1);
            return false;
        }
        function DeleteConfirm() {
            if (confirm("Apakah yakin mau hapus data ini?")) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
   <%-- <asp:UpdatePanel runat="server" ID="updatePanel1">--%>
        <%--<ContentTemplate>--%>
            <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>
                        DAFTAR GROUP SUPPLIER
                    </h3>
                </div>
            </div>
            <asp:Panel ID="pnlList" runat="server">
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtgPaging" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                                OnSortCommand="Sorting" DataKeyField="SupplierGroupID" BorderStyle="None" BorderWidth="0"
                                CssClass="grid_general">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="EDIT">
                                        <ItemStyle CssClass="command_col"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" ImageUrl="../Images/iconedit.gif"
                                                CommandName="Edit"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="DELETE">
                                        <ItemStyle CssClass="command_col"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbDelete" runat="server" CausesValidation="False" ImageUrl="../Images/iconDelete.gif"
                                                CommandName="Delete"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="PKS" HeaderText="PKS">
                                        <ItemTemplate>
                                    <asp:ImageButton ID="imgPrint" runat="server" CausesValidation="False" ImageUrl="../Images/IconPrinter.gif"
                                        CommandName="Print"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>

                                    <%--modify by amri 2018-01-16--%>
                                     <asp:TemplateColumn SortExpression="supplierGroupID" HeaderText="ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lnkID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.supplierGroupID") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                     </asp:TemplateColumn>

                                    <asp:BoundColumn DataField="SupplierGroupName" SortExpression="SupplierGroupName"
                                        HeaderText="NAMA GROUP SUPPLIER"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="Kota" SortExpression="Kota" HeaderText="KOTA"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="ContactPerson" SortExpression="ContactPerson" HeaderText="KONTAK">
                                    </asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="REKENING BANK">
                                        <ItemTemplate>
                                            <a href='SupplierGroupBankAccount.aspx?cmd=dtl&id=<%# DataBinder.Eval(Container, "DataItem.SupplierGroupID") 
                                    %>&desc=<%# DataBinder.Eval(Container, "DataItem.SupplierGroupName") %>'>REKENING BANK</a>
                                            </a>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                            <div class="button_gridnavigation">
                                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../Images/grid_navbutton01.png"
                                    CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../Images/grid_navbutton02.png"
                                    CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../Images/grid_navbutton03.png"
                                    CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../Images/grid_navbutton04.png"
                                    CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                                    EnableViewState="False"></asp:Button>
                                <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" MinimumValue="1"
                                    ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"></asp:RangeValidator>
                                <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                                    ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                            </div>
                            <div class="label_gridnavigation">
                                <asp:Label ID="lblPage" runat="server"></asp:Label>of
                                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonAdd" runat="server" Text="Add" CssClass="small button blue"
                        CausesValidation="False"></asp:Button>
                  
                </div>
                <div class="form_box_title">
                    <div class="form_single">
                        <h4>
                            CARI GROUP SUPPLIER
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Cari Berdasarkan</label>
                        <asp:DropDownList ID="cboSearch" runat="server">
                            <asp:ListItem Value="SupplierGroupName">Nama Group Supplier</asp:ListItem>
                            <asp:ListItem Value="Kota">Kota</asp:ListItem>
                            <asp:ListItem Value="ContactPerson">Kontak</asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtSearch" runat="server" Width="20%" MaxLength="100"></asp:TextBox>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonSearch" runat="server" CausesValidation="False" Text="Search"
                        CssClass="small button blue"></asp:Button>&nbsp;
                    <asp:Button ID="ButtonReset" runat="server" CausesValidation="False" Text="Reset"
                        CssClass="small button gray"></asp:Button>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlAddEdit" runat="server">
                <div class="form_box_header">
                    <div class="form_single">
                        <h4>
                            <asp:Label ID="lblTitleAddEdit" runat="server"></asp:Label>
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Nama Group Supplier
                        </label>
                        <asp:Label ID="lblsuppgroupID" runat="server" Visible="False"></asp:Label>
                        <asp:TextBox ID="txtNamaGroupSupplier" runat="server" CssClass="long_text"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                            CssClass="validator_general" ControlToValidate="txtNamaGroupSupplier"></asp:RequiredFieldValidator>
                    </div>
                </div>

               <%-- <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    Nama Lengkap Supplier
                </label>
                <asp:Label ID="lblsuppgroupID" runat="server" Visible="False"></asp:Label>
                <asp:TextBox ID="txtName" runat="server" Width="20%" MaxLength="50"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="harap isi Nama Supplier"
                    ControlToValidate="txtName" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator CssClass="validator_general" ID="RegularExpressionValidator3"
                    runat="server" ErrorMessage="Nama Lengkap Supplier tidak boleh isi dengan angka" ControlToValidate="txtName"
                    Display="Dynamic" ValidationExpression="^([A-Za-z ]+)$" />


            </div>
        </div>--%>

                <%--edit npwp, ktp, telepon by ario--%>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            NPWP</label>
                        <asp:TextBox ID="txtNPWP" runat="server"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="Regularexpressionvalidator4" runat="server" Display="Dynamic"
                        ValidationExpression="[0-9]{0,10}" ErrorMessage="Harap isi Nomor NPWP dengan Angka"
                        ControlToValidate="txtNPWP"  CssClass ="validator_general"></asp:RegularExpressionValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtNPWP"
                        ErrorMessage="Harap isi Nomor NPWP" Display="Dynamic" CssClass ="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            TDP</label>
                        <asp:TextBox ID="txtTDP" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            SIUP</label>
                        <asp:TextBox ID="txtSIUP" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label  class="label_req">
                            Jumlah Plafond Group</label>                        
                        <uc2:ucNumberFormat ID="txtJmlPlafondGroup" runat="server"></uc2:ucNumberFormat>
                    </div>
                </div>

                 <div class="form_box">
            <div class="form_single">
                <label>
                    Mulai Tanggal PKS
                </label>
                <uc2:ucdatece id="StartDate" runat="server" />
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    No PKS</label>
                <asp:Label ID="txtNoPKS" runat="server" MaxLength="30" Columns="35"></asp:Label>
            </div>
        </div>
         <div class="form_box">
                <div>
                    <div class="form_left">
                        <label class="label_general">Upload PKS</label>  
                        <asp:FileUpload ID="uplProsedurKYC" runat="server" />
                        <asp:HyperLink ID="hypProsedurKYC" runat="server" />
                    </div>
                    <div class="form_right">
                            <asp:Button ID="btnupload" runat="server" Text="Upload" CssClass="small button blue" CausesValidation="False"  />
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label class="label_general">Upload Memo Approval</label>  
                        <asp:FileUpload ID="uplApproval" runat="server" />
                        <asp:HyperLink ID="hyApproval" runat="server" />
                    </div>
                    <div class="form_right">
                            <asp:Button ID="btnuploadApproval" runat="server" Text="Upload" CssClass="small button blue"  />
                    </div>
                </div>
            </div>

                <div class="form_box_header">
                    <div class="form_single">
                        <h4>
                            ALAMAT
                        </h4>
                    </div>
                </div>
                <div class="form_box_uc">
                    <uc1:uccompanyaddress id="UcCompanyAddress" runat="server"></uc1:uccompanyaddress>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <h4>
                            KONTAK
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label  class="label_req">
                            Nama</label>
                        <asp:TextBox ID="txtContactPerson" runat="server" CssClass="medium_text"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" InitialValue="" ControlToValidate="txtContactPerson"
                        CssClass="validator_general" ErrorMessage="*" Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Jabatan</label>
                        <asp:DropDownList ID="cboJobPosition" runat="server">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            E-mail</label>
                        <asp:TextBox ID="txtEmail" runat="server" CssClass="medium_text"></asp:TextBox>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            No HandPhone</label>
                        <asp:TextBox ID="txtNoHP" runat="server"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="expNoHP" runat="server" Display="Dynamic" ValidationExpression="[0-9]{0,15}"
                            ErrorMessage="Harap isi Nomor HP dengan Angka" CssClass="validator_general" ControlToValidate="txtNoHP"></asp:RegularExpressionValidator>
                        <asp:RequiredFieldValidator ID="expNoHP1" runat="server" ControlToValidate="txtNoHP"
                            CssClass="validator_general" ErrorMessage="Harap isi Nomor HP" Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_general">
                            Catatan</label>
                        <asp:TextBox ID="txtNotes" runat="server" CssClass="multiline_textbox" TextMode="MultiLine"></asp:TextBox>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonSave" runat="server" CausesValidation="true" Text="Save" CssClass="small button blue">
                    </asp:Button>&nbsp;
                    <asp:Button ID="ButtonCancel" runat="server" CausesValidation="true" Text="Cancel"
                        CssClass="small button gray"></asp:Button>&nbsp;
                    <asp:Button ID="ButtonBack" runat="server" CausesValidation="true" Text="Back" CssClass="small button gray">
                    </asp:Button>&nbsp;
                    <asp:Button ID="ButtonClose" runat="server" CausesValidation="False" Text="Close"
                        CssClass="small button gray"></asp:Button>
                </div>
            </asp:Panel>
            <input id="hdnEditSupplierGroupID" type="hidden" name="hdnEditSupplierGroupID" runat="server" />
       <%-- </ContentTemplate>
         <Triggers>
         <asp:AsyncPostBackTrigger ControlID="btnupload" EventName="Click" />
         </Triggers>
        
    </asp:UpdatePanel>--%>
    </form>
</body>
</html>
