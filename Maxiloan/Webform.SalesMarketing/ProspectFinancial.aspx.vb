﻿Imports System.Data.SqlClient
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
Imports System.IO
Imports System.Collections.Generic

Public Class ProspectFinancial
    Inherits Maxiloan.Webform.WebBased

#Region "Controls"
    Private oController As New ProspectController
    Protected WithEvents ucProspectTab1 As ucProspectTab
    Private time As String
#End Region

#Region "Properties"
    Property ProspectAppID() As String
        Get
            Return ViewState("ProspectAppID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ProspectAppID") = Value
        End Set
    End Property
    Property PageMode() As String
        Get
            Return ViewState("PageMode").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("PageMode") = Value
        End Set
    End Property
    Property ActivityDateStart() As DateTime
        Get
            Return ViewState("ActivityDateStart").ToString
        End Get
        Set(ByVal Value As DateTime)
            ViewState("ActivityDateStart") = Value
        End Set
    End Property

    Property ActivityDateEnd() As DateTime
        Get
            Return ViewState("ActivityDateEnd").ToString
        End Get
        Set(ByVal Value As DateTime)
            ViewState("ActivityDateEnd") = Value
        End Set
    End Property
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Visible = False

        If SessionInvalid() Then Exit Sub

        If Not Page.IsPostBack Then
            initObjects()
            Me.PageMode = Request("page")
            If Request("ActivityDateStart") = Nothing Then
                time = Now.Hour.ToString("00") & ":" & Now.Minute.ToString("00") & ":" & Now.Second.ToString("00")
                Me.ActivityDateStart = Me.BusinessDate + " " + time
            Else
                Me.ActivityDateStart = Request("ActivityDateStart")
            End If

            If Me.PageMode = "Edit" Then
                Dim oCustomclass As New Parameter.Prospect
                Me.ProspectAppID = Request("id")
                With oCustomclass
                    .strConnection = GetConnectionString()
                    .ProspectAppID = Me.ProspectAppID
                End With
                BindEdit()

                ucProspectTab1.ProspectAppID = Me.ProspectAppID
                ucProspectTab1.setLink()
            Else
                Me.ProspectAppID = ""
            End If

        End If
        btnProceed.Enabled = (PageMode = "Edit")
    End Sub


#Region "Init Objects"
    Protected Sub initObjects()
        ucProspectTab1.selectedTab("Financial")
        FillCbo(cboUsage, "tblAssetUsage")
        FillCboGeneral(cbomerk, "spMerkKendaraan")
        FillCboGeneral(cbokategori, "spKategoriKendaraan")
        RangeValidator1.MaximumValue = CStr(Year(Me.BusinessDate) + 1)
    End Sub
    Sub FillCbo(ByVal cboName As DropDownList, ByVal Table As String)
        Dim oAssetData As New Parameter.Prospect
        Dim oData As New DataTable
        oAssetData.strConnection = GetConnectionString()
        oAssetData.Table = Table
        oAssetData = oController.GetCbo(oAssetData)
        oData = oAssetData.ListData
        cboName.DataSource = oData
        cboName.DataTextField = "Description"
        cboName.DataValueField = "ID"
        cboName.DataBind()
        cboName.Items.Insert(0, "Select One")
        cboName.Items(0).Value = "Select One"
    End Sub
    Sub FillCboGeneral(ByVal cboName As DropDownList, ByVal StoreProcedure As String)
        Dim oAssetData As New Parameter.Prospect
        Dim oData As New DataTable
        oAssetData.strConnection = GetConnectionString()
        oAssetData.StoreProcedure = StoreProcedure
        oAssetData = oController.GetCboGeneral(oAssetData)
        oData = oAssetData.listdata
        cboName.DataSource = oData
        cboName.DataTextField = "Description"
        cboName.DataValueField = "ID"
        cboName.DataBind()
        cboName.Items.Insert(0, "Select One")
        cboName.Items(0).Value = "Select One"
    End Sub
#End Region

#Region "BindEdit"
    Sub BindEdit()
        Dim oApplication As New Parameter.Prospect
        Dim oData As New DataTable
        Dim oRow As DataRow

        oApplication.strConnection = GetConnectionString()
        oApplication.BranchId = Replace(Me.sesBranchId, "'", "").ToString
        oApplication.ProspectAppID = Me.ProspectAppID
        oApplication = oController.GetViewProspect(oApplication)

        If Not oApplication Is Nothing Then
            oData = oApplication.listdata
        End If

        If oData.Rows.Count > 0 Then
            oRow = oData.Rows(0)
            cboUsage.SelectedIndex = cboUsage.Items.IndexOf(cboUsage.Items.FindByValue(oRow("KepemilikanUnitLainPenggunaan").ToString))
            cbomerk.SelectedIndex = cbomerk.Items.IndexOf(cbomerk.Items.FindByValue(oRow("KepemilikanUnitLainMerk").ToString))
            cbokategori.SelectedIndex = cbokategori.Items.IndexOf(cbokategori.Items.FindByValue(oRow("KepemilikanUnitLainKategori").ToString))
            Try
                'rdokepemilikanunit.SelectedValue = oRow("KepemilikanUnitLain")
                'rdopengalamankredit.SelectedValue = oRow("PengalamanKredit")
                'rdoRekeningTabungan.SelectedValue = oRow("RekeningTabungan")
                rdokepemilikanunit.SelectedIndex = rdokepemilikanunit.Items.IndexOf(rdokepemilikanunit.Items.FindByValue(oRow("KepemilikanUnitLain").ToString))
                rdopengalamankredit.SelectedIndex = rdopengalamankredit.Items.IndexOf(rdopengalamankredit.Items.FindByValue(oRow("PengalamanKredit").ToString))
                rdoRekeningTabungan.SelectedIndex = rdoRekeningTabungan.Items.IndexOf(rdoRekeningTabungan.Items.FindByValue(oRow("RekeningTabungan").ToString))
            Catch
            End Try
            
            txtNoRekening.Text = oRow("NoRekening").ToString
            txtPengalamanKredit.Text = oRow("PengalamanKreditDari").ToString
            txtTahunKendaraan.Text = oRow("KepemilikanUnitLainTahun").ToString
            'MODIFY NOFI 20MAR2018
            If CBool(rdoRekeningTabungan.SelectedValue) Then
                txtNoRekening.Enabled = True
                rfNoRekening.Enabled = True
            Else
                txtNoRekening.Enabled = False
                rfNoRekening.Enabled = False
            End If
            If CBool(rdopengalamankredit.SelectedValue) Then
                txtPengalamanKredit.Enabled = True
                rfPengalamanKredit.Enabled = True
            Else
                txtPengalamanKredit.Enabled = False
                rfPengalamanKredit.Enabled = False
            End If
            If CBool(rdokepemilikanunit.SelectedValue) Then
                txtTahunKendaraan.Enabled = True
                rfUsage.Enabled = True
                rfTahunKendaraan.Enabled = True
                cbokategori.Enabled = True
                cbomerk.Enabled = True
                cboUsage.Enabled = True
            Else
                rfUsage.Enabled = False
                rfTahunKendaraan.Enabled = False
                cbokategori.Enabled = False
                cbomerk.Enabled = False
                cboUsage.Enabled = False
                txtTahunKendaraan.Enabled = False
            End If

            'If rdoRekeningTabungan.SelectedIndex = "True" Then
            '    txtNoRekening.Enabled = True
            '    rfNoRekening.Enabled = True
            'Else
            '    txtNoRekening.Enabled = False
            '    rfNoRekening.Enabled = False
            'End If
            'If rdopengalamankredit.SelectedIndex = "True" Then
            '    txtPengalamanKredit.Enabled = True
            '    rfPengalamanKredit.Enabled = True
            'Else
            '    txtPengalamanKredit.Enabled = False
            '    rfPengalamanKredit.Enabled = False
            'End If
            'If rdokepemilikanunit.SelectedIndex = "True" Then
            '    txtTahunKendaraan.Enabled = True
            '    rfUsage.Enabled = True
            '    rfTahunKendaraan.Enabled = True
            '    cbokategori.Enabled = True
            '    cbomerk.Enabled = True
            '    cboUsage.Enabled = True
            'Else
            '    rfUsage.Enabled = False
            '    rfTahunKendaraan.Enabled = False
            '    cbokategori.Enabled = False
            '    cbomerk.Enabled = False
            '    cboUsage.Enabled = False
            '    txtTahunKendaraan.Enabled = False
            'End If

        End If
    End Sub
#End Region

#Region "Save"
    Private Sub imbSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If validasi() = True Then
                If Me.Page.IsValid Then
                    lblMessage.Visible = False

                    Dim oApplication As New Parameter.Prospect
                    oApplication.strConnection = GetConnectionString()
                    oApplication.BusinessDate = Me.BusinessDate
                    oApplication.BranchId = Replace(Me.sesBranchId, "'", "").ToString
                    oApplication.ProspectAppID = Me.ProspectAppID
                    oApplication.RekeningTabungan = rdoRekeningTabungan.SelectedValue
                    oApplication.NoRekening = txtNoRekening.Text
                    oApplication.PengalamanKredit = rdopengalamankredit.SelectedValue
                    oApplication.PengalamanKreditDari = txtPengalamanKredit.Text
                    oApplication.KepemilikanUnitLain = rdokepemilikanunit.SelectedValue
                    oApplication.KepemilikanUnitLainTahun = txtTahunKendaraan.Text
                    oApplication.KepemilikanUnitLainMerk = cbomerk.SelectedValue
                    oApplication.KepemilikanUnitLainKategori = cbokategori.SelectedValue
                    oApplication.KepemilikanUnitLainPenggunaan = cboUsage.SelectedValue
                    oApplication.Mode = Me.PageMode
                    oApplication.LoginId = Me.Loginid

                    Dim ErrorMessage As String = ""
                    Dim oReturn As New Parameter.Prospect

                    oReturn = oController.ProspectSaveFinancial(oApplication)

                    If oReturn.Err <> "" Then
                        ShowMessage(lblMessage, ErrorMessage, True)
                    Else
                        ShowMessage(lblMessage, "Data saved!", False)
                        ucProspectTab1.ProspectAppID = Me.ProspectAppID
                        ucProspectTab1.setLink()
                        Me.PageMode = "Edit"
                    End If
                Else
                    ShowMessage(lblMessage, "Data Sudah Ada", True)
                End If
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

    Private Function validasi() As Boolean
        If rdoRekeningTabungan.SelectedValue = "1" And txtNoRekening.Text.Trim = "" Then
            ShowMessage(lblMessage, "Harap isi nomor rekening", True)
            Return False
        End If
        If rdopengalamankredit.SelectedValue = "1" And txtPengalamanKredit.Text.Trim = "" Then
            ShowMessage(lblMessage, "Harap isi pengalaman kredit dari", True)
            Return False
        End If
        Return True
    End Function

#End Region

    Protected Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("ProspectApplication.aspx")
    End Sub

    Protected Sub btnProceed_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProceed.Click

        Try
            imbSave_Click(Nothing, Nothing)
            If (oController.DoProceeded(GetConnectionString, ProspectAppID)) Then
                ProspectLog()
                ShowMessage(lblMessage, String.Format("Prospect Id {0} berhasil di Proceed.", ProspectAppID), False)
            Else
                Throw New Exception(String.Format("Prospect Id {0} belum berhasil di Proceed. Silahkan coba lagi.", ProspectAppID))
            End If
            Response.Redirect("ProspectApplication.aspx")
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
        
    End Sub


    Private Sub rdoRekeningTabungan_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles rdoRekeningTabungan.SelectedIndexChanged
        'If rdoRekeningTabungan.SelectedValue = "1" Then
        If CBool(rdoRekeningTabungan.SelectedValue) Then
            txtNoRekening.Enabled = True
            rfNoRekening.Enabled = True
        Else
            txtNoRekening.Enabled = False
            rfNoRekening.Enabled = False
            txtNoRekening.Text = 0
        End If
    End Sub

    Private Sub rdopengalamankredit_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles rdopengalamankredit.SelectedIndexChanged
        'If rdopengalamankredit.SelectedValue = "1" Then
        If CBool(rdopengalamankredit.SelectedValue) Then
            txtPengalamanKredit.Enabled = True
            rfPengalamanKredit.Enabled = True
        Else
            txtPengalamanKredit.Enabled = False
            rfPengalamanKredit.Enabled = False
            txtPengalamanKredit.Text = 0
        End If
    End Sub

    Private Sub rdokepemilikanunit_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles rdokepemilikanunit.SelectedIndexChanged
        'If rdokepemilikanunit.SelectedValue = "1" Then
        If CBool(rdokepemilikanunit.SelectedValue) Then
            txtTahunKendaraan.Enabled = True
            rfUsage.Enabled = True
            rfTahunKendaraan.Enabled = True
            cbokategori.Enabled = True
            cbomerk.Enabled = True
            cboUsage.Enabled = True
        Else
            rfUsage.Enabled = False
            rfTahunKendaraan.Enabled = False
            cbokategori.Enabled = False
            cbomerk.Enabled = False
            cboUsage.Enabled = False
            txtTahunKendaraan.Enabled = False
            txtTahunKendaraan.Text = 0
        End If

    End Sub

    Sub ProspectLog()
        time = Now.Hour.ToString("00") & ":" & Now.Minute.ToString("00") & ":" & Now.Second.ToString("00")
        Me.ActivityDateEnd = Me.BusinessDate + " " + time

        Dim oApplication As New Parameter.Prospect
        oApplication.strConnection = GetConnectionString()
        oApplication.BranchId = Replace(Me.sesBranchId, "'", "").ToString
        oApplication.ProspectAppID = Me.ProspectAppID
        oApplication.ActivityType = "SVR"
        oApplication.ActivityDateStart = Me.ActivityDateStart
        oApplication.ActivityDateEnd = Me.ActivityDateEnd
        oApplication.ActivityUser = Me.Loginid
        oApplication.ActivitySeqNo = 6

        Dim ErrorMessage As String = ""
        Dim oReturn As New Parameter.Prospect

        oReturn = oController.ProspectLogSave(oApplication)

        If oReturn.Err <> "" Then
            ShowMessage(lblMessage, ErrorMessage, True)
            Exit Sub
        End If
    End Sub
End Class