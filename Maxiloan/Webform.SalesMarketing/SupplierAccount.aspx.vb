﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.cbse
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class SupplierAccount
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents UcBankAccount As UcBankAccount
    Protected WithEvents txtEffectiveDate As ucDateCE
#Region "Constanta"
    Private m_controller As New SupplierController
    Dim oSupplier As New Parameter.Supplier
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Dim oRow As DataRow
    Dim strStyle As String = "Marketing"
#End Region
#Region "Property"
    Private Property AddEdit() As String
        Get
            Return CType(viewstate("AddEdit"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AddEdit") = Value
        End Set
    End Property
    Private Property SupplierID() As String
        Get
            Return CType(viewstate("SupplierID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("SupplierID") = Value
        End Set
    End Property
    Private Property AccountID() As String
        Get
            Return CType(viewstate("AccountID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AccountID") = Value
        End Set
    End Property
    Private Property CmdWhere() As String
        Get
            Return CType(viewstate("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property WhereCond2() As String
        Get
            Return CType(viewstate("WhereCond2"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("WhereCond2") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(viewstate("Sort"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Sort") = Value
        End Set
    End Property
    Private Property Name() As String
        Get
            Return CType(viewstate("Name"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Name") = Value
        End Set
    End Property
    Private Property DataSupplierAccount() As DataTable
        Get
            Return CType(ViewState("DataSupplierAccount"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            ViewState("DataSupplierAccount") = Value
        End Set
    End Property
#End Region

#Region "Page Load & Bindgrid"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not Me.IsPostBack Then
            If CheckForm(Me.Loginid, "SupplierAccount", Me.AppId) Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            If Request("back") = "1" Then
                GetCookies()
            Else
                If Request("cond") <> "" Then
                    Me.CmdWhere = Request("cond")
                Else
                    Me.CmdWhere = "ALL"
                End If
                Me.SupplierID = Request("id")
                Me.Name = Request("name")
                Me.WhereCond2 = "SupplierID = '" & Me.SupplierID & "'"
            End If

            lnkID.Text = Me.SupplierID
            lblName.Text = Me.Name

            Me.Sort = "SupplierAccID ASC"

            InitialPanel()

            With UcBankAccount
                .ValidatorTrue()
                .Style = strStyle
                .BindBankAccount()
            End With

            BindGrid(Me.CmdWhere)
            lblName.NavigateUrl = "javascript:OpenWin('" & lnkID.Text & "','Marketing');"
            txtEffectiveDate.IsRequired = True

        End If
    End Sub
#Region "GetCookies"
    Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies("Supplier")
        Me.CmdWhere = cookie.Values("where")
        Me.WhereCond2 = cookie.Values("where2")
        Me.Name = cookie.Values("name")
        Me.SupplierID = cookie.Values("id")
    End Sub
#End Region
    Sub BindGrid(ByVal cmdWhere As String)
        Dim dtEntity As DataTable = Nothing
        Dim oCustomClass As New Parameter.Supplier
        oCustomClass.PageSize = pageSize
        oCustomClass.WhereCond = cmdWhere
        oCustomClass.SortBy = Me.Sort
        oCustomClass.WhereCond2 = Me.WhereCond2
        oCustomClass.CurrentPage = currentPage
        oCustomClass.strConnection = GetConnectionString
        oCustomClass = m_controller.GetSupplierAccount(oCustomClass)
        If Not oCustomClass Is Nothing Then
            dtEntity = oCustomClass.ListData
            recordCount = oCustomClass.TotalRecords
            DataSupplierAccount = oCustomClass.ListData
        Else
            recordCount = 0
        End If
        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()
        PagingFooter()
    End Sub
    Sub InitialPanel()
        pnlList.Visible = True
        pnlAddEdit.Visible = False
        txtPage.Text = "1"
    End Sub
#End Region
#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then            
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGrid(Me.CmdWhere)
    End Sub
    Private Sub imgbtnPageNumb_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int32)
                BindGrid(Me.CmdWhere)
            End If
        End If
    End Sub
#End Region
#Region "Sorting"
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGrid(Me.CmdWhere)
    End Sub
#End Region
#Region "Reset-Search"
    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        cboSearch.SelectedIndex = 0
        txtSearch.Text = ""
        Me.CmdWhere = "ALL"
        BindGrid(Me.CmdWhere)
    End Sub
    Private Sub imbSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonSearch.Click
        If txtSearch.Text.Trim <> "" Then
            Dim Search As String
            Search = txtSearch.Text.Trim
            Me.CmdWhere = cboSearch.SelectedItem.Value + " = '" + Search + "'"
        Else
            Me.CmdWhere = "ALL"
        End If
        BindGrid(Me.CmdWhere)
    End Sub
#End Region
#Region "Add"
    Private Sub imbAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonAdd.Click
        pnlList.Visible = False
        pnlAddEdit.Visible = True
        BindAdd()
        Me.AddEdit = "Add"        
        txtForPayment.Text = ""
        checkDefault.Checked = False
        txtEffectiveDate.Text = Format(Me.BusinessDate, "dd/MM/yyyy")
    End Sub
    Sub BindAdd()
        UcBankAccount.BindBankAccount()
        UcBankAccount.BankID = ""
        UcBankAccount.BankBranch = ""
        UcBankAccount.AccountNo = ""
        UcBankAccount.AccountName = ""
    End Sub
#End Region

#Region "BindEdit"
    Sub BindEdit(ByVal BankName As String, ByVal Branch As String, _
    ByVal AccountNo As String, ByVal AccountName As String, ByVal BankBranchId As String, ByVal SupplierBankId As String, _
    ByVal UntukBayar As String, ByVal TanggalEfektif As String, ByVal DefaultAccount As Boolean)
        Dim oData As New DataTable
        Dim oData2 As New DataTable
        Dim oEntity As New Parameter.Supplier

        pnlList.Visible = False
        pnlAddEdit.Visible = True

        txtForPayment.Text = UntukBayar.Trim
        checkDefault.Checked = DefaultAccount
        txtEffectiveDate.Text = CDate(TanggalEfektif).ToString("dd/MM/yyyy").Trim

        With UcBankAccount
            .BankBranchId = BankBranchId.Trim
            .BindBankAccount()
            .BankID = SupplierBankId.Trim

            .BankBranch = Branch.Trim
            .AccountNo = AccountNo.Trim
            .AccountName = AccountName.Trim            
        End With

    End Sub
#End Region

#Region "ItemCommand"
    Private Sub dtgPaging_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        If e.CommandName = "Edit" Then
            If CheckFeature(Me.Loginid, "SupplierAccount", "Edit", Me.AppId) Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            Me.AddEdit = "Edit"            
            Me.AccountID = e.Item.Cells(2).Text.Trim

            BindEdit(CType(e.Item.FindControl("lblBankName"), Label).Text, _
            e.Item.Cells(4).Text.Trim, _
            e.Item.Cells(5).Text.Trim, _
            e.Item.Cells(6).Text.Trim, _
            CType(e.Item.FindControl("lblBankBranchId"), Label).Text, _
            CType(e.Item.FindControl("lblSupplierBankId"), Label).Text, _
            CType(e.Item.FindControl("lblUntukBayar"), Label).Text, _
            CType(e.Item.FindControl("lblTanggalEfektif"), Label).Text, _
            CType(e.Item.FindControl("checkDefault"), CheckBox).Checked)

        ElseIf e.CommandName = "Delete" Then
            If CheckFeature(Me.Loginid, "SupplierAccount", "Del", Me.AppId) Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            Dim oSupplierAccount As New Parameter.Supplier
            oSupplierAccount.SupplierID = Me.SupplierID
            oSupplierAccount.ID = e.Item.Cells(2).Text.Trim
            oSupplierAccount.strConnection = GetConnectionString
            Dim Err As String
            Err = m_controller.SupplierAccountDelete(oSupplierAccount)
            If Err = "" Then
                InitialPanel()
                BindGrid(Me.CmdWhere)
            Else
                ShowMessage(lblMessage, Err, True)
            End If
        End If
    End Sub
#End Region
#Region "Print"
    Private Sub imbPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonPrint.Click
        If CheckFeature(Me.Loginid, "SupplierAccount", "Print", Me.AppId) Then
            If SessionInvalid() Then
                Exit Sub
            End If
        End If
        Dim cookie As HttpCookie = Request.Cookies("Supplier")
        If Not cookie Is Nothing Then
            cookie.Values("where") = Me.CmdWhere
            cookie.Values("where2") = Me.WhereCond2
            cookie.Values("page") = "SupplierAccount"
            cookie.Values("name") = Me.Name
            cookie.Values("id") = Me.SupplierID
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie("Supplier")
            cookieNew.Values.Add("where", Me.CmdWhere)
            cookieNew.Values.Add("where2", Me.WhereCond2)
            cookieNew.Values.Add("page", "SupplierAccount")
            cookieNew.Values.Add("name", Me.Name)
            cookieNew.Values.Add("id", Me.SupplierID)
            Response.AppendCookie(cookieNew)
        End If
        Response.Redirect("SupplierReport.aspx")
    End Sub
#End Region
#Region "Back, Save & Cancel"
    Private Sub imbCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        InitialPanel()
    End Sub
    Private Sub imbSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
        Dim oSupplier As New Parameter.Supplier
        Dim status As Boolean = False

        oSupplier.strConnection = GetConnectionString()
        oSupplier.AddEdit = Me.AddEdit
        oSupplier.SupplierID = Me.SupplierID

        If Me.AddEdit = "Add" Then
            oSupplier.ID = ""
        ElseIf Me.AddEdit = "Edit" Then
            oSupplier.ID = Me.AccountID
        End If

        oSupplier.BankID = UcBankAccount.BankID
        oSupplier.BankBranch = UcBankAccount.BankBranch
        oSupplier.AccountNo = UcBankAccount.AccountNo
        oSupplier.AccountName = UcBankAccount.AccountName
        oSupplier.BankBranchId = UcBankAccount.BankBranchId
        oSupplier.TanggalEfektif = Date.ParseExact(txtEffectiveDate.Text, "dd/MM/yyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo)
        oSupplier.UntukBayar = txtForPayment.Text.Trim
        oSupplier.DefaultAccount = checkDefault.Checked


        'If DataSupplierAccount.Rows.Count > 0 Then
        '    For Each q In DataSupplierAccount.Rows
        '        If CBool(q("DefaultAccount")) = True Then
        '            status = True
        '        End If
        '    Next

        '    m_controller.SupplierAccountSave(oSupplier)

        '    If oSupplier.Output = "" Then
        '        ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_SUCCESS, False)
        '        InitialPanel()
        '        BindGrid(Me.CmdWhere)
        '    Else
        '        ShowMessage(lblMessage, oSupplier.Output, True)
        '        pnlList.Visible = False
        '        pnlAddEdit.Visible = True
        '        txtPage.Text = "1"
        '    End If

        'Else

        oSupplier = m_controller.SupplierAccountSave(oSupplier)


        If oSupplier.Output = "" Then
            ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_SUCCESS, False)
            InitialPanel()
            BindGrid(Me.CmdWhere)
        Else
            ShowMessage(lblMessage, oSupplier.Output, True)
            pnlList.Visible = False
            pnlAddEdit.Visible = True
            txtPage.Text = "1"
        End If
        'End If
    End Sub
    Private Sub imbBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonBack.Click
        Response.Redirect(Request("apps") & ".aspx?currentPage=" & Request("currentPage") & "")
    End Sub
#End Region

#Region "ItemDataBound"
    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim imbDelete As ImageButton
            Dim chekDefault As CheckBox
            Dim chekValue As HtmlInputHidden

            imbDelete = CType(e.Item.FindControl("imgDelete"), ImageButton)
            imbDelete.Attributes.Add("OnClick", "return DeleteConfirm();")
            chekValue = CType(e.Item.FindControl("hdncheckDefault"), HtmlInputHidden)
            chekDefault = CType(e.Item.FindControl("checkDefault"), CheckBox)
            chekDefault.Checked = CBool(chekValue.Value)
        End If
    End Sub
#End Region

End Class