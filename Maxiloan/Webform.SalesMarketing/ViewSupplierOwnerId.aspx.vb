﻿#Region "Imports"
Imports Maxiloan.cbse
Imports Maxiloan.Exceptions
Imports System.IO
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
#End Region

Public Class ViewSupplierOwnerId
    Inherits Maxiloan.Webform.WebBased
    Private m_controller As New SupplierController
#Region "Property"
    Property OwnerID() As String
        Get
            Return viewstate("OwnerID").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("OwnerID") = Value
        End Set
    End Property
    Property SupplierID() As String
        Get
            Return viewstate("SupplierID").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("SupplierID") = Value
        End Set
    End Property
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Me.SessionInvalid Then
            Dim strHTTPServer As String
            Dim strHTTPApp As String
            Dim strNameServer As String
            strHTTPServer = Request.ServerVariables("PATH_INFO")
            strNameServer = Request.ServerVariables("SERVER_NAME")
            strHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
            Response.Redirect("http://" & strNameServer & "/" & strHTTPApp & "/NotAuthorized.html")
        End If

        If Not Page.IsPostBack Then
            Me.OwnerID = Request("OwnerID").ToString
            Me.SupplierID = Request("SupplierID").ToString
            Bindgrid()
        End If        
    End Sub
    Sub Bindgrid()
        Dim oSupplier As New Parameter.Supplier
        Dim oData As New DataTable
        Dim oDataAttribute As New DataTable
        Dim oDataAssetDoc As New DataTable
        oSupplier.strConnection = GetConnectionString
        oSupplier.SupplierID = Me.SupplierID
        oSupplier.ID = Me.OwnerID
        oSupplier = m_controller.GetSupplierOwnerView(oSupplier)
        If Not oSupplier Is Nothing Then
            oData = oSupplier.ListData
        End If
        If oData.Rows.Count > 0 Then
            SetValueToLabel(oData)
        End If
    End Sub
    Sub SetValueToLabel(ByVal oData As DataTable)
        Dim oRow As DataRow
        oRow = oData.Rows(0)
        lblID.Text = Me.OwnerID
        lblName.Text = oRow(0).ToString.Trim
        lblTitle.Text = oRow(1).ToString.Trim
        lblBirthPlace.Text = oRow(2).ToString.Trim
        If oRow(3).ToString.Trim <> "" Then
            lblBirthDate.Text = Format(oRow(3), "dd/MM/yyyy")
        End If
        lblNPWP.Text = oRow(4).ToString.Trim
        lblIDType.Text = oRow(5).ToString.Trim
        lblIDNumber.Text = oRow(6).ToString.Trim
        lblAddress.Text = oRow(7).ToString.Trim
        lblRT.Text = oRow(8).ToString.Trim
        lblRW.Text = oRow(9).ToString.Trim
        lblKelurahan.Text = oRow(10).ToString.Trim
        lblKecamatan.Text = oRow(11).ToString.Trim
        lblcity.Text = oRow(12).ToString.Trim
        lblZip.Text = oRow(13).ToString.Trim
        lblAPhone1.Text = oRow(14).ToString.Trim
        lblPhone1.Text = oRow(15).ToString.Trim
        lblAPhone2.Text = oRow(16).ToString.Trim
        lblPhone2.Text = oRow(17).ToString.Trim
        lblAFax.Text = oRow(18).ToString.Trim
        lblFax.Text = oRow(19).ToString.Trim
        lblMobile.Text = oRow(21).ToString.Trim
        lblEmail.Text = oRow(20).ToString.Trim
    End Sub

End Class