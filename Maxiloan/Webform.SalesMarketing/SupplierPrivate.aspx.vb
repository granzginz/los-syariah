﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.cbse
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region
Public Class SupplierPrivate
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents ucAddress As UcCompanyAddress
    Protected WithEvents UcBankAccount As UcBankAccount
    Protected WithEvents StartDate As ucDateCE

#Region "Constanta"
    Private m_controller As New SupplierController
    Dim oSupplier As New Parameter.Supplier
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Dim oRow As DataRow
    Dim strStyle As String = "Marketing"
#End Region

#Region "Property"
    Private Property AddEdit() As String
        Get
            Return CType(ViewState("AddEdit"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("AddEdit") = Value
        End Set
    End Property
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property
#End Region

#Region "Page Load, Bindgrid"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not Me.IsPostBack Then
            If CheckForm(Me.Loginid, "PFSUPP", Me.AppId) Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            txtPage.Text = "1"
            If Request("currentPage") <> "" Then
                Me.currentPage = Request("currentPage")
                txtPage.Text = Me.currentPage
            End If

            Me.Sort = "Name ASC"
            If Request("cond") <> "" Then
                Me.CmdWhere = Request("cond")
            Else
                Me.CmdWhere = " PF = 'true'"
                If IsHoBranch = False Then
                    Me.CmdWhere = Me.CmdWhere & " and branchid = '" & Me.sesBranchId.ToString.Replace("'", "") & "'"
                End If
            End If
            InitialPanel()
            
            'wierd
            'BindAdd2()

            BindGridEntity(Me.CmdWhere)
            ucAddress.Style = "Marketing"
            ucAddress.ValidatorTrue()
            ucAddress.Phone1ValidatorEnabled(True)
            BindComboIC()

            'lnkSupplierID2.Attributes.Add("OnClick", "return OpenWin('" & lnkSupplierID2.Text & "','Marketing');")

            With UcBankAccount
                .ValidatorTrue()
                .Style = strStyle
                .BindBankAccount()
            End With

        End If

    End Sub
    Sub InitialPanel()
        pnlList.Visible = True
        pnlAddEdit.Visible = False
        pnlAdd.Visible = False
        pnlBtnSaveEdit.Visible = False
        rgvGo.Enabled = True
    End Sub

    Sub BindGridEntity(ByVal cmdWhere As String)
        Dim dtEntity As DataTable
        Dim oCustomClass As New Parameter.Supplier
        oCustomClass.PageSize = pageSize
        oCustomClass.WhereCond = cmdWhere
        oCustomClass.SortBy = Me.Sort
        oCustomClass.CurrentPage = currentPage
        oCustomClass.strConnection = GetConnectionString()
        oCustomClass = m_controller.GetSupplier(oCustomClass)
        If Not oCustomClass Is Nothing Then
            dtEntity = oCustomClass.ListData
            recordCount = oCustomClass.TotalRecords

            dtgPaging.DataSource = dtEntity.DefaultView
            dtgPaging.CurrentPageIndex = 0
            dtgPaging.DataBind()
            PagingFooter()
        Else
            ShowMessage(lblMessage, "Pencarian data tidak di temukan", True)
            recordCount = 0
        End If
        
    End Sub
#End Region
#Region "Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : Me.currentPage = 1
            Case "Last" : Me.currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : Me.currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : Me.currentPage = Int32.Parse(lblPage.Text) - 1
        End Select

        txtPage.Text = Me.currentPage
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub imgbtnPageNumb_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int32)
                BindGridEntity(Me.CmdWhere)
            End If
        End If
    End Sub
#End Region
#Region "Sorting"
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub
#End Region
#Region "Reset-Search"
    Private Sub imbButtonReset_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbButtonReset.Click
        cboSearch.SelectedIndex = 0
        txtSearch.Text = ""
        Me.CmdWhere = " PF = 'true'"
        If IsHoBranch = False Then
            Me.CmdWhere = Me.CmdWhere & " and branchid = '" & Me.sesBranchId.ToString.Replace("'", "") & "'"
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub imbButtonSearch_Click(ByVal sender As Object, ByVal e As EventArgs) Handles imbButtonSearch.Click
        Me.CmdWhere = ""
        If txtSearch.Text.Trim <> "" Then
            Dim Search As String
            If cboSearch.SelectedIndex = 1 Then
                Search = Replace(txtSearch.Text.Trim, "'", "''")
            Else
                Search = txtSearch.Text.Trim
            End If
            Me.CmdWhere = Me.CmdWhere + " PF = 'true' AND " + cboSearch.SelectedItem.Value + " LIKE '%" + Search + "%'"
        Else
            Me.CmdWhere = " PF = 'true'"
        End If
        If IsHoBranch = False Then
            Me.CmdWhere = Me.CmdWhere & " and branchid = '" & Me.sesBranchId.ToString.Replace("'", "") & "'"
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub
#End Region
#Region "ItemCommand"
    Private Sub dtgPaging_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        Dim Id As String
        Dim Name As String
        Dim Err As String = ""
        If e.CommandName = "Edit" Then
            If CheckFeature(Me.Loginid, "PFSUPP ", "Edit", Me.AppId) Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            Id = CType(e.Item.FindControl("lnkID"), Label).Text
            Me.AddEdit = "Edit"
            bindEdit(Id)
            'Response.Redirect("CustomerPersonal.aspx?page=Edit&id=" & e.Item.Cells(4).Text.Trim & "")
        ElseIf e.CommandName = "Delete" Then
            If CheckFeature(Me.Loginid, "PFSUPP", "Del", Me.AppId) Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            Dim oSupplier As New Parameter.Supplier
            Id = CType(e.Item.FindControl("lnkID"), Label).Text
            oSupplier.strConnection = GetConnectionString()
            oSupplier.SupplierID = Id

            Err = m_controller.DeleteSupplier(oSupplier)
            If Err = "" Then
                txtPage.Text = "1"
                BindGridEntity(Me.CmdWhere)
            Else
                ShowMessage(lblMessage, Err, True)
            End If
        ElseIf e.CommandName = "Branch" Or e.CommandName = "Owner" Or e.CommandName = "Budget" Or e.CommandName = "Forecast" _
  Or e.CommandName = "Employee" Or e.CommandName = "Account" Or e.CommandName = "Signature" Then
            Dim Target As String = "Supplier" + e.CommandName
            Id = CType(e.Item.FindControl("lnkID"), Label).Text
            Name = CType(e.Item.FindControl("lnkName"), HyperLink).Text.Trim
            'Name = e.Item.Cells(1).Text.Trim
            Response.Redirect("" & Target & ".aspx?id=" & Id & "&name=" & Name & "&apps=SupplierPrivate&currentPage=" & txtPage.Text & "")
        End If
    End Sub
#End Region
#Region "Add"
    Private Sub imbButtonAdd_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbButtonAdd.Click
        Me.AddEdit = "Add"
        lblTitle.Text = "ADD"
        lblsuppID.Visible = False
        DefaultAdd()
        bindAdd()

    End Sub
    Sub bindAdd()
        pnlList.Visible = False
        pnlAddEdit.Visible = True
        pnlAdd.Visible = True
        rgvGo.Enabled = False
        
    End Sub
    Sub DefaultAdd()
        'cboGroup.SelectedIndex = 1
        txtName.Text = ""
        'txtShortName.Text = ""
        'txtInitialName.Text = ""
        txtNPWP.Text = ""
        ucAddress.Address = ""
        ucAddress.RT = ""
        ucAddress.RW = ""
        ucAddress.Kelurahan = ""
        ucAddress.Kecamatan = ""
        ucAddress.City = ""
        ucAddress.ZipCode = ""
        ucAddress.AreaPhone1 = ""
        ucAddress.Phone1 = ""
        ucAddress.AreaPhone2 = ""
        ucAddress.Phone2 = ""
        ucAddress.AreaFax = ""
        ucAddress.Fax = ""
        ucAddress.BindAddress()
        cboEmpPosition.SelectedIndex = -1
        txtCPEmail.Text = ""
        pnlBtnSaveEdit.Visible = True
    End Sub
#End Region
#Region "Edit"
    Sub bindEdit(ByVal ID As String)
        pnlList.Visible = False
        pnlAddEdit.Visible = True
        pnlAdd.Visible = False
        pnlBtnSaveEdit.Visible = True
        rgvGo.Enabled = False
        lblTitle.Text = "EDIT"
        'txtID.Visible = False
        'lblsuppID.Visible = False

        Dim oData As New DataTable
        Dim oCustomClass As New Parameter.Supplier

        oCustomClass.SupplierID = ID
        oCustomClass.strConnection = GetConnectionString()
        oCustomClass = m_controller.SupplierEdit(oCustomClass)

        If Not oCustomClass Is Nothing Then
            oData = oCustomClass.ListData
        End If

        If oData.Rows.Count > 0 Then
            oRow = oData.Rows(0)
            'txtID.Text = ID
            lblsuppID.Text = ID
            'cboGroup.Items.FindByValue(oRow.Item(0).ToString).Selected = True
            txtName.Text = oRow.Item("SupplierName").ToString.Trim
            'txtShortName.Text = oRow.Item(2).ToString.Trim
            'txtInitialName.Text = oRow.Item(3).ToString.Trim
            txtNPWP.Text = oRow.Item("NPWP").ToString.Trim
            ucAddress.Address = oRow.Item("SupplierAddress").ToString.Trim
            ucAddress.RT = oRow.Item("SupplierRT").ToString.Trim
            ucAddress.RW = oRow.Item("SupplierRW").ToString.Trim
            ucAddress.Kelurahan = oRow.Item("SupplierKelurahan").ToString.Trim
            ucAddress.Kecamatan = oRow.Item("SupplierKecamatan").ToString.Trim
            ucAddress.City = oRow.Item("SupplierCity").ToString.Trim
            ucAddress.ZipCode = oRow.Item("SupplierZipCode").ToString.Trim
            ucAddress.AreaPhone1 = oRow.Item("SupplierAreaPhone1").ToString.Trim
            ucAddress.Phone1 = oRow.Item("SupplierPhone1").ToString.Trim
            ucAddress.AreaPhone2 = oRow.Item("SupplierAreaPhone2").ToString.Trim
            ucAddress.Phone2 = oRow.Item("SupplierPhone2").ToString.Trim
            ucAddress.AreaFax = oRow.Item("SupplierAreaFax").ToString.Trim
            ucAddress.Fax = oRow.Item("SupplierFax").ToString.Trim
            ucAddress.BindAddress()
            cboEmpPosition.SelectedIndex = cboEmpPosition.Items.IndexOf(cboEmpPosition.Items.FindByValue(oRow.Item("ContactPersonJobTitle").ToString.Trim))
            txtCPEmail.Text = oRow.Item("ContactPersonEmail").ToString.Trim
            txtCPMobile.Text = oRow.Item("ContactPersonHP").ToString.Trim

            If oRow.Item("isPerseorangan") = True Then
                chkPerseorangan.Checked = True
            Else
                chkPerseorangan.Checked = False
            End If

        End If
    End Sub
#End Region
#Region "Cancel"
    Private Sub imbButtonCancel_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbButtonCancel.Click
        InitialPanel()
    End Sub
#End Region
#Region "Save, OK"
    Private Sub imbButtonSave_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbButtonSave.Click
        Dim oSupplier As New Parameter.Supplier
        Dim oAddress As New Parameter.Address
        Dim oAddressNPWP As New Parameter.Address
        Dim oBankAccount As New Parameter.BankAccount
        Dim oReturnValue As New Parameter.Supplier

        oSupplier.strConnection = GetConnectionString()
        'oSupplier.SupplierGroupID = cboGroup.SelectedValue
        oSupplier.SupplierName = txtName.Text
        'oSupplier.SupplierShortName = txtShortName.Text
        'oSupplier.SupplierInitialName = txtInitialName.Text
        oSupplier.NPWP = txtNPWP.Text
        oSupplier.TDP = ""
        oSupplier.SIUP = ""
        oSupplier.SupplierStartDate = ""
        oSupplier.SupplierBadStatus = ""


        If chkPerseorangan.Checked = True Then
            oSupplier.isPerseorangan = True
        Else
            oSupplier.isPerseorangan = False
        End If

        oSupplier.SupplierStartDate = ""
        oSupplier.SupplierBadStatus = "N"
        
        oSupplier.SupplierLevelStatus = "B"
        oSupplier.IsAutomotive = "1"
        oSupplier.IsUrusBPKB = ""
        oSupplier.SupplierCategory = "MD"
        oSupplier.SupplierAssetStatus = "M"

        oSupplier.NoPKS = ""

        oSupplier.PF = True

        oAddress.Address = ucAddress.Address
        oAddress.RT = ucAddress.RT
        oAddress.RW = ucAddress.RW
        oAddress.Kelurahan = ucAddress.Kelurahan
        oAddress.Kecamatan = ucAddress.Kecamatan
        oAddress.City = ucAddress.City
        oAddress.ZipCode = ucAddress.ZipCode
        oAddress.AreaPhone1 = ucAddress.AreaPhone1
        oAddress.Phone1 = ucAddress.Phone1
        oAddress.AreaPhone2 = ucAddress.AreaPhone2
        oAddress.Phone2 = ucAddress.Phone2
        oAddress.AreaFax = ucAddress.AreaFax
        oAddress.Fax = ucAddress.Fax
        oSupplier.CPName = txtName.Text
        oSupplier.CPJobTitle = cboEmpPosition.SelectedItem.Value
        oSupplier.CPEmail = txtCPEmail.Text
        oSupplier.CPHP = txtCPMobile.Text

        oBankAccount.BankID = UcBankAccount.BankID
        oBankAccount.BankBranch = UcBankAccount.BankBranch
        oBankAccount.AccountNo = UcBankAccount.AccountNo
        oBankAccount.AccountName = UcBankAccount.AccountName
        oBankAccount.BankBranchId = UcBankAccount.BankBranchId

        oSupplier.BusinessDate = Me.BusinessDate
        oSupplier.BranchId = Me.sesBranchId.Replace("'", "")
        oSupplier.SupplierID = ""


        If Me.AddEdit = "Add" Then

            Try
                oReturnValue = m_controller.SupplierSaveAddPrivate(oSupplier, oAddress)
                oSupplier.strConnection = GetConnectionString()

                oSupplier.AddEdit = Me.AddEdit

                oSupplier.ID = ""
                oSupplier.SupplierID = oReturnValue.SupplierID
                oSupplier.BankID = UcBankAccount.BankID
                oSupplier.BankBranch = UcBankAccount.BankBranch
                oSupplier.AccountNo = UcBankAccount.AccountNo
                oSupplier.AccountName = UcBankAccount.AccountName
                oSupplier.BankBranchId = UcBankAccount.BankBranchId
                oSupplier.TanggalEfektif = DateTime.Now
                oSupplier.UntukBayar = "PENCAIRAN"
                oSupplier.DefaultAccount = True

                oSupplier = m_controller.SupplierAccountSave(oSupplier)

                oSupplier.AddEdit = Me.AddEdit
                oSupplier.ID = ""
                oSupplier.strConnection = GetConnectionString()
                oSupplier.SupplierID = oReturnValue.SupplierID
                oSupplier.EmployeeName = txtName.Text.Trim
                oSupplier.EmployeePosition = cboEmpPosition.SelectedItem.Value
                oSupplier.NoKTP = txtKTP.Text.Trim
                oSupplier.NoNPWP = txtNPWP.Text.Trim
                


                oSupplier.BankID = UcBankAccount.BankID.Trim
                oSupplier.BankBranch = UcBankAccount.BankBranch.Trim
                oSupplier.AccountNo = UcBankAccount.AccountNo.Trim
                oSupplier.AccountName = UcBankAccount.AccountName.Trim
                oSupplier.IsActive = "true"
                oSupplier.BankBranchId = UcBankAccount.BankBranchId
                oSupplier.SupplierSaleStatus = ""

                oSupplier = m_controller.SupplierEmployeeSave(oSupplier)


                Dim oDataBranch As New DataTable
                oDataBranch.Columns.Add("BranchID", GetType(String))
                oRow = oDataBranch.NewRow
                oRow("BranchID") = Me.sesBranchId.ToString.Replace("'", "")
                oDataBranch.Rows.Add(oRow)
                oSupplier.strConnection = GetConnectionString()
                oSupplier.SupplierID = oReturnValue.SupplierID
                oSupplier.BusinessDate = Me.BusinessDate
                m_controller.SupplierSaveBranch(oSupplier, oDataBranch)

                pnlAddEdit.Visible = False
                pnlList.Visible = True
                rgvGo.Enabled = False
                ShowMessage(lblMessage, "Proses simpan data Berhasil.....", False)
            Catch ex As Exception
                ShowMessage(lblMessage, "Proses simpan data gagal.....", True)

                Exit Sub
            End Try
            

        Else
            Try
                oSupplier.PKSDate = "01/01/1900"
                oSupplier.SupplierID = lblsuppID.Text.Trim
                oSupplier.SupplierGroupID = ""
                m_controller.SupplierSaveEdit(oSupplier, oAddress, oAddressNPWP)
                pnlList.Visible = True
                pnlAddEdit.Visible = False
            Catch ex As Exception
                ShowMessage(lblMessage, "Proses simpan data gagal.....", True)
                Exit Sub
            End Try
        End If
        InitialPanel()
        BindGridEntity(Me.CmdWhere)
    End Sub
  
#End Region
#Region "ItemDataBound"
    
    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim lnk As Label
            Dim lnkName As HyperLink

            lnk = CType(e.Item.FindControl("lnkID"), Label)
            lnkName = CType(e.Item.FindControl("lnkName"), HyperLink)
            lnkName.NavigateUrl = "javascript:OpenWinSupplier('" & "Marketing" & "', '" & lnk.Text & "')"
            'lnk.Attributes.Add("OnClick", "return OpenWin('" & lnk.Text & "','Marketing');")
            Dim imbDelete As ImageButton
            imbDelete = CType(e.Item.FindControl("imgDelete"), ImageButton)
            imbDelete.Attributes.Add("OnClick", "return DeleteConfirm();")
        End If
    End Sub
#End Region

#Region "BindComboIncentiveCard"
    Private Sub BindComboIC()
        Dim oSupplierIC As New Parameter.IncentiveCard
        Dim oControllerSupplierIC As New IncentiveCardController
        Dim dtIncentiveCard As New DataTable
        With oSupplierIC
            .strConnection = GetConnectionString()
        End With
        dtIncentiveCard = oControllerSupplierIC.GetComboSupplierIncentiveCard(oSupplierIC)
      
    End Sub
#End Region

   

End Class