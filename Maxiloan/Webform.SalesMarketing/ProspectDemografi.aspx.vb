﻿Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
Imports System.IO
Imports System.Collections.Generic

Public Class ProspectDemografi
    Inherits Maxiloan.Webform.WebBased


#Region "Controls"
    Private oController As New ProspectController
    Private m_controller As New CustomerController
    Protected WithEvents ucJumlahTanggungan As ucNumberFormat
    Protected WithEvents ucPenghasilanKotor As ucNumberFormat
    Protected WithEvents ucPenghasilanBersih As ucNumberFormat
    Protected WithEvents ucTinggalSejakBulan As ucMonthCombo
    Protected WithEvents ucKaryawanSejakCustomer As ucMonthCombo
    Protected WithEvents ucProspectTab1 As ucProspectTab
    Private time As String
#End Region

#Region "Properties"
    Property ProspectAppID() As String
        Get
            Return ViewState("ProspectAppID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ProspectAppID") = Value
        End Set
    End Property
    Property PageMode() As String
        Get
            Return ViewState("PageMode").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("PageMode") = Value
        End Set
    End Property
    Property ActivityDateStart() As DateTime
        Get
            Return ViewState("ActivityDateStart").ToString
        End Get
        Set(ByVal Value As DateTime)
            ViewState("ActivityDateStart") = Value
        End Set
    End Property
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Visible = False

        If SessionInvalid() Then Exit Sub

        If Not Page.IsPostBack Then
            initObjects()
            Me.PageMode = Request("page")
            If Request("ActivityDateStart") = Nothing Then
                time = Now.Hour.ToString("00") & ":" & Now.Minute.ToString("00") & ":" & Now.Second.ToString("00")
                Me.ActivityDateStart = Me.BusinessDate + " " + time
            Else
                Me.ActivityDateStart = Request("ActivityDateStart")
            End If

            If Me.PageMode = "Edit" Then
                Dim oCustomclass As New Parameter.Prospect
                Me.ProspectAppID = Request("id")
                With oCustomclass
                    .strConnection = GetConnectionString()
                    .ProspectAppID = Me.ProspectAppID
                End With
                BindEdit()

                ucProspectTab1.ProspectAppID = Me.ProspectAppID
                ucProspectTab1.setLink()
            Else
                Me.ProspectAppID = ""
                ucJumlahTanggungan.Text = "0"
            End If
        End If
    End Sub


#Region "Init Objects"
    Protected Sub initObjects()
        ucProspectTab1.selectedTab("Demografi")
        FillCbo_("tblMaritalStatus", cboPMarital)
        FillCbo_("tblEducation", cboPEducation)
        FillCbo_("tblHomeStatus", cboPHomeStatus)
        FillCbo_("tblJobType", cboJobType)
        FillCbo_("tblJobType", cboJobTypePasangan)
        FillCbo_("tblIndustryHeader", cboIndrustriHeader)
        FillCbo_("tblIndustryHeader", cboIndrustriHeaderPasangan)
        RangeValidator1.MaximumValue = CStr(Year(Me.BusinessDate) + 1)
        RangeValidator2.MaximumValue = CStr(Year(Me.BusinessDate) + 1)
        'FICO
        FillCbo_("tblOccupation", cboOccupation)
        FillCbo_("tblNatureOfBusiness", cboNaturalOfBusiness)
        FillCbo_("tblOccupation", cboOccupationSpouse)
        FillCbo_("tblNatureOfBusiness", cboNaturalOfBusinessSpouse)
    End Sub

    Protected Sub FillCbo_(ByVal table As String, cboReference As DropDownList)
        Dim dtEntity As DataTable = Nothing
        Dim oCustomer As New Parameter.Customer
        oCustomer.strConnection = GetConnectionString()
        oCustomer.Table = table
        oCustomer = m_controller.CustomerPersonalAdd(oCustomer)
        If Not oCustomer Is Nothing Then
            dtEntity = oCustomer.listdata
        End If

        cboReference.DataSource = dtEntity.DefaultView
        cboReference.DataTextField = "Description"
        cboReference.DataValueField = "ID"
        cboReference.DataBind()
        cboReference.Items.Insert(0, "Select One")
        cboReference.Items(0).Value = "Select One"

    End Sub
#End Region

#Region "BindEdit"
    Sub BindEdit()
        Dim oApplication As New Parameter.Prospect
        Dim oData As New DataTable
        Dim oRow As DataRow

        oApplication.strConnection = GetConnectionString()
        oApplication.BranchId = Replace(Me.sesBranchId, "'", "").ToString
        oApplication.ProspectAppID = Me.ProspectAppID
        oApplication = oController.GetViewProspect(oApplication)

        If Not oApplication Is Nothing Then
            oData = oApplication.listdata
        End If

        If oData.Rows.Count > 0 Then
            oRow = oData.Rows(0)
            rdoJenisKelamin.SelectedValue = oRow("Gender").ToString
            cboPMarital.SelectedIndex = cboPMarital.Items.IndexOf(cboPMarital.Items.FindByValue(oRow("MaritalStatus").ToString.Trim))
            ucJumlahTanggungan.Text = CInt(IIf(IsNumeric(oRow("NumOfDependence")), oRow("NumOfDependence"), 0))
            cboPEducation.SelectedIndex = cboPEducation.Items.IndexOf(cboPEducation.Items.FindByValue(oRow("Education").ToString.Trim))
            cboPHomeStatus.SelectedIndex = cboPHomeStatus.Items.IndexOf(cboPHomeStatus.Items.FindByValue(oRow("HomeStatus").ToString.Trim))
            ucTinggalSejakBulan.SelectedMonth = CInt(IIf(IsNumeric(oRow("StaySinceMonth")), oRow("StaySinceMonth"), 1))
            txtPStaySince.Text = CInt(IIf(IsNumeric(oRow("StaySinceYear")), oRow("StaySinceYear"), 0))
            ucKaryawanSejakCustomer.SelectedMonth = CInt(IIf(IsNumeric(oRow("EmploymentSinceMonth")), oRow("EmploymentSinceMonth"), 1))
            txtEmploymentYear.Text = CInt(IIf(IsNumeric(oRow("EmploymentSinceYear")), oRow("EmploymentSinceYear"), 0))
            ucPenghasilanKotor.Text = FormatNumber(oRow("MonthlyVariableIncome"), 0)
            ucPenghasilanBersih.Text = FormatNumber(oRow("MonthlyFixedIncome"), 0)
            lblBiayaHidup.Text = FormatNumber(oRow("LivingCostAmount"), 0)
            If oRow("ProfessionID").ToString.Trim <> "-" Then
                cboJobType.SelectedValue = oRow("ProfessionID").ToString.Trim
            Else
                cboJobType.SelectedIndex = 0
            End If

            cboJobTypePasangan.SelectedIndex = cboJobTypePasangan.Items.IndexOf(cboJobTypePasangan.Items.FindByValue(oRow("ProfessionIDPasangan").ToString.Trim))
            cboIndrustriHeader.SelectedIndex = cboIndrustriHeader.Items.IndexOf(cboIndrustriHeader.Items.FindByValue(oRow("KodeIndustri").ToString.Trim))
            hdfKodeIndustriBU.Value = oRow("KodeIndustriDetail").ToString.Trim
            txtNamaIndustriBU.Text = oRow("NameIndustriDetail").ToString.Trim
            cboIndrustriHeaderPasangan.SelectedIndex = cboIndrustriHeaderPasangan.Items.IndexOf(cboIndrustriHeaderPasangan.Items.FindByValue(oRow("KodeIndustriPasangan").ToString.Trim))
            hdfKodeIndustriBUPasangan.Value = oRow("KodeIndustriDetailPasangan").ToString.Trim
            txtNamaIndustriBUPasangan.Text = oRow("NameIndustriDetailPasangan").ToString.Trim
            'FICO

            cboOccupation.SelectedIndex = cboOccupation.Items.IndexOf(cboOccupation.Items.FindByValue(oRow("OccupationID").ToString.Trim))
            cboNaturalOfBusiness.SelectedIndex = cboNaturalOfBusiness.Items.IndexOf(cboNaturalOfBusiness.Items.FindByValue(oRow("NatureOfBusinessID").ToString.Trim))
            cboOccupationSpouse.SelectedIndex = cboOccupationSpouse.Items.IndexOf(cboOccupationSpouse.Items.FindByValue(oRow("OccupationSpouseID").ToString.Trim))
            cboNaturalOfBusinessSpouse.SelectedIndex = cboNaturalOfBusinessSpouse.Items.IndexOf(cboNaturalOfBusinessSpouse.Items.FindByValue(oRow("NatureOfBusinessSpouseID").ToString.Trim))
        End If
    End Sub
#End Region

#Region "Save"
    Private Sub imbSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If Me.Page.IsValid Then
                lblMessage.Visible = False

                Dim oApplication As New Parameter.Prospect
                oApplication.strConnection = GetConnectionString()
                oApplication.BusinessDate = Me.BusinessDate
                oApplication.BranchId = Replace(Me.sesBranchId, "'", "").ToString
                oApplication.ProspectAppID = Me.ProspectAppID
                oApplication.Gender = rdoJenisKelamin.SelectedValue
                oApplication.MaritalStatus = cboPMarital.SelectedValue.Trim
                oApplication.NumOfDependence = CInt(ucJumlahTanggungan.Text.Trim)
                oApplication.Education = cboPEducation.SelectedValue.Trim
                oApplication.HomeStatus = cboPHomeStatus.SelectedValue.Trim
                oApplication.StaySinceYear = txtPStaySince.Text.Trim
                oApplication.StaySinceMonth = ucTinggalSejakBulan.SelectedMonth
                oApplication.EmploymentSinceYear = txtEmploymentYear.Text.Trim
                oApplication.EmploymentSinceMonth = ucKaryawanSejakCustomer.SelectedMonth
                oApplication.MonthlyVariableIncome = CDec(ucPenghasilanKotor.Text)
                oApplication.MonthlyFixedIncome = CDec(ucPenghasilanBersih.Text)
                oApplication.ProfessionID = cboJobType.SelectedValue
                oApplication.KodeIndustri = cboIndrustriHeader.SelectedValue
                If (hdfKodeIndustriBU.Value.Trim = "") Then
                    Dim err = "Silahkan pilih Bidang Usaha Detail."
                    ShowMessage(lblMessage, err, True)
                    Exit Sub
                End If
                oApplication.KodeIndustriDetail = hdfKodeIndustriBU.Value
                oApplication.ProfessionIDPasangan = cboJobTypePasangan.SelectedValue
                oApplication.KodeIndustriPasangan = cboIndrustriHeaderPasangan.SelectedValue
                oApplication.KodeIndustriDetailPasangan = hdfKodeIndustriBUPasangan.Value
                oApplication.Mode = Me.PageMode
                oApplication.LoginId = Me.Loginid

                oApplication.Occupation = cboOccupation.SelectedValue
                oApplication.NatureOfBusiness = cboNaturalOfBusiness.SelectedValue
                oApplication.OccupationSpouse = cboOccupationSpouse.SelectedValue
                oApplication.NatureOfBusinessSpouse = cboNaturalOfBusinessSpouse.SelectedValue

                Dim ErrorMessage As String = ""
                Dim oReturn As New Parameter.Prospect

                oReturn = oController.ProspectSaveDemografi(oApplication)

                If oReturn.Err <> "" Then
                    ShowMessage(lblMessage, ErrorMessage, True)
                Else
                    ShowMessage(lblMessage, "Data saved!", False)
                    'ucProspectTab1.ProspectAppID = Me.ProspectAppID
                    'ucProspectTab1.setLink()
                    'Me.PageMode = "Edit"
                    Response.Redirect("ProspectFinancial.aspx?id=" & oReturn.ProspectAppID.Trim & "&page=Edit&ActivityDateStart=" & Me.ActivityDateStart & "")
                End If
            Else
                ShowMessage(lblMessage, "Data Sudah Ada", True)
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub



#End Region

    Protected Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("ProspectApplication.aspx")
    End Sub

End Class