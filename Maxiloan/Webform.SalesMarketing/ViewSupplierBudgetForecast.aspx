﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewSupplierBudgetForecast.aspx.vb"
    Inherits="Maxiloan.Webform.SalesMarketing.ViewSupplierBudgetForecast" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ViewSupplierBudgetForecast</title>
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';
        function windowClose() {
            window.close();
        }		
    </script>
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">    
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">           
            <h3>
                VIEW - SUPPLIER BUDGET &amp; FORECAST
            </h3>
        </div>
    </div> 
    <div class="form_box">
	    <div class="form_single">
            <label>ID Cabang</label>
            <asp:Label ID="lblBranchID" runat="server"></asp:Label>
	    </div>
    </div>
    <div class="form_box">
	    <div class="form_single">
            <label>ID Supplier</label>
            <asp:Label ID="lblSupplierID" runat="server"></asp:Label>
	    </div>
    </div>
    <div class="form_box">
	    <div class="form_single">
            <label>Group Supplier</label>
            <asp:Label ID="lblSupplierGroup" runat="server"></asp:Label>
	    </div>
    </div>
    <div class="form_box">
	    <div class="form_single">
            <label>Nama Supplier</label>
            <asp:Label ID="lblSupplierName" runat="server"></asp:Label>
	    </div>
    </div>
    <div class="form_box">
	    <div class="form_single">
             <label class ="label_req">
			Tahun</label>
            <asp:TextBox ID="txtYear" runat="server"  Width="50%"></asp:TextBox><asp:RangeValidator
            ID="rgvYear" runat="server" ErrorMessage="Tahun Salah " MinimumValue="1900"
            Type="integer" ControlToValidate="txtYear" Display="Dynamic" MaximumValue="5000">Year is not valid</asp:RangeValidator><asp:RequiredFieldValidator
            ID="rfvYear" runat="server" ErrorMessage="Harap diisi Tahun" ControlToValidate="txtYear"
            Display="Dynamic">Harap diisi tahun</asp:RequiredFieldValidator>
	    </div>
    </div>       
    <asp:Panel ID="pnlGrid" runat="server">
    <div class="form_box_header">
    <div class="form_single">
    <div class="grid_wrapper_ns"> 
            <asp:DataGrid ID="DtgBudgetAdd" runat="server"  CssClass="grid_general" BorderStyle="None" BorderWidth="0"
                        AutoGenerateColumns="False" AllowSorting="True" Width="100%" Visible="True">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn>
                                <HeaderStyle CssClass="tdjudul" HorizontalAlign="Center" Height="30px"></HeaderStyle>                                
                                <HeaderTemplate>
                                    Bulan
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblMonth" runat="server">
												<%#container.dataitem("MonthName")%>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn>                                
                                <HeaderTemplate>
                                    <table class="tablegrid" width="100%" align="center" border="0" class="tdjudul">
                                        <tr>
                                            <td colspan="3" align="center" class="tdjudul">
                                                Budget
                                            </td>
                                        </tr>
                                        <tr class="tdjudul">
                                            <td align="center" width="15%">
                                                Baru
                                            </td>
                                            <td align="center" width="15%">
                                                Bekas
                                            </td>
                                            <td align="center" width="15%">
                                                Total
                                            </td>
                                        </tr>
                                    </table>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <table cellspacing="1" cellpadding="2" width="95%" align="center" border="0">
                                        <tr>
                                            <td align="right" width="15%">
                                                <asp:Label ID="lblNewUnit" runat="server" Text='<%#container.dataitem("unitBudgetNew")%>'>
                                                </asp:Label>
                                                <br/>
                                                <asp:Label ID="LblNewAmount" runat="server" Text='<%#formatnumber(container.dataitem("amountBudgetNew"),0)%>'>
                                                </asp:Label>
                                            </td>
                                            <td align="right" width="15%">
                                                <asp:Label ID="lblAssetUnit" runat="server" Text='<%#container.dataitem("unitBudgetUsed")%>'>
                                                </asp:Label>
                                                <br/>
                                                <asp:Label ID="lblAssetAmount" runat="server" Text='<%#formatnumber(container.dataitem("amountBudgetUsed"),0)%>'>
                                                </asp:Label>
                                            </td>
                                            <td align="right" width="15%">
                                                <asp:Label ID="lblSumNewUnit" runat="server" Text='<%#GetSumUnit(container.dataitem("unitBudgetNew"),container.dataitem("unitBudgetUsed"))%>'>
                                                </asp:Label>
                                                <br/>
                                                <asp:Label ID="lblSumNewAmount" runat="server" Width="40%" Text='<%#formatnumber(GetSumAmount(container.dataitem("amountBudgetNew"),container.dataitem("amountBudgetUsed")),0)%>'>
                                                </asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn>                                
                                <HeaderTemplate>
                                    <table class="tablegrid" align="center" width="100%" border="0">
                                        <tr>
                                            <td colspan="3" align="center" class="tdjudul">
                                                ForeCast
                                            </td>
                                        </tr>
                                        <tr class="tdjudul">
                                            <td align="center" width="15%">
                                                Baru
                                            </td>
                                            <td align="center" width="15%">
                                                Bekas
                                            </td>
                                            <td align="center" width="15%">
                                                Total
                                            </td>
                                        </tr>
                                    </table>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <table cellspacing="1" width="95%" align="center" border="0">
                                        <tr>
                                            <td align="right" width="15%">
                                                <asp:Label ID="Label1" runat="server" Text='<%#container.dataitem("unitForecastNew")%>'>
                                                </asp:Label>
                                                <br/>
                                                <asp:Label ID="Label2" runat="server" Text='<%#formatnumber(container.dataitem("amountForecastNew"),0)%>'>
                                                </asp:Label>
                                            </td>
                                            <td align="right" width="15%">
                                                <asp:Label ID="Label3" runat="server" Text='<%#container.dataitem("unitForecastUsed")%>'>
                                                </asp:Label>
                                                <br/>
                                                <asp:Label ID="Label4" runat="server" Text='<%#formatnumber(container.dataitem("amountForecastUsed"),0)%>'>
                                                </asp:Label>
                                            </td>
                                            <td align="right" width="15%">
                                                <asp:Label ID="Label5" runat="server" Text='<%#GetSumUnit(container.dataitem("unitForecastNew"),container.dataitem("unitForecastUsed"))%>'>
                                                </asp:Label>
                                                <br/>
                                                <asp:Label ID="Label6" runat="server" Text='<%#formatnumber(GetSumAmount(container.dataitem("amountForecastNew"),container.dataitem("amountForecastUsed")),0)%>'>
                                                </asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>                        
                    </asp:DataGrid>
	</div>
    </div> 
    </div>          
    </asp:Panel>
    <div class="form_button">
        <asp:Button ID="ButtonView" runat="server"  Text="View" CssClass ="small button blue">
        </asp:Button>&nbsp;
        <asp:Button ID="ButtonClose" runat="server"  Text="Close" CssClass ="small button gray"
            CausesValidation="false"></asp:Button>
    </div>
    </form>
</body>
</html>
