﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SupplierRelasi.aspx.vb" Inherits="Maxiloan.Webform.SalesMarketing.SupplierRelasi" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>SupplierEmployee</title>
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />   
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:ScriptManager runat="server" ID="ScriptManager1">
        </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    DAFTAR RELASI KARYAWAN SUPPLIER
                </h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>Sales Suppervisor</label>
                <asp:DropDownList ID="cboSupervisor" runat="server" Width="150px" AutoPostBack="true" OnSelectedIndexChanged="cboSupervisor_SelectedIndexChanged"></asp:DropDownList>
  
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>Sales</label>
                <asp:DataGrid ID="dtgPaging" runat="server" Width="100%" DataKeyField="SupplierEmployeeID"
                    BorderStyle="None" BorderWidth="0" AutoGenerateColumns="False"
                    CssClass="grid_general">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <Columns>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSales" runat="server" />
                                
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="SupplierEmployeeName" HeaderText="NAMA">                                    
                            <ItemTemplate>
                                <asp:Label ID="lblEmployeeID"  Visible="false" runat="server" Text='<%# DataBinder.eval(Container,"DataItem.SupplierEmployeeID") %>'></asp:Label>
                                <asp:Label ID="lblSupplierEmployeeName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SupplierEmployeeName") %>'></asp:Label>
                                <asp:Label ID="lblsales"  Visible="false" runat="server" Text='<%# DataBinder.eval(Container,"DataItem.sales") %>'></asp:Label>
                                <asp:Label ID="lblexisting"  Visible="false" runat="server" Text='<%# DataBinder.eval(Container,"DataItem.existing") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>  
                    </Columns>
                </asp:DataGrid>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonSaveAdd" runat="server" CausesValidation="False" Text="Save" CssClass ="small button blue">
            </asp:Button>
            <asp:Button ID="ButtonBackAdd" runat="server" CausesValidation="False" Text="Back" CssClass ="small button gray">
            </asp:Button>
        </div>   
    </div>
    </form>
</body>
</html>
