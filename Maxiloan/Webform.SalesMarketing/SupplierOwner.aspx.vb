﻿Imports System.IO
Imports Maxiloan.cbse
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController

Public Class SupplierOwner
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents ucAddress As UcCompanyAddress
    Protected WithEvents txtuscBirthDate As ucDateCE
#Region "Constanta"
    Private m_controller As New SupplierController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Dim oSupplier As New Parameter.Supplier
    Dim oRow As DataRow
#End Region
#Region "Property"
    Private Property AddEdit() As String
        Get
            Return CType(ViewState("AddEdit"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("AddEdit") = Value
        End Set
    End Property
    Private Property OwnerID() As String
        Get
            Return CType(ViewState("OwnerID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("OwnerID") = Value
        End Set
    End Property
    Private Property SupplierID() As String
        Get
            Return CType(ViewState("SupplierID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("SupplierID") = Value
        End Set
    End Property
    Private Property Name() As String
        Get
            Return CType(ViewState("Name"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Name") = Value
        End Set
    End Property
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property WhereCond2() As String
        Get
            Return CType(ViewState("WhereCond2"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("WhereCond2") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property
#End Region
#Region "Page Load & Bindgrid"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not Me.IsPostBack Then
            If CheckForm(Me.Loginid, "SupplierOwner", Me.AppId) Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            If Request("back") = "1" Then
                GetCookies()
            Else
                If Request("cond") <> "" Then
                    Me.CmdWhere = Request("cond")
                Else
                    Me.CmdWhere = "ALL"
                End If
                Me.SupplierID = Request("id")
                Me.Name = Request("name")
                Me.WhereCond2 = "SupplierID = '" & Me.SupplierID & "'"
            End If
            'lnkID.Text = Me.SupplierID
            lblName.Text = Me.Name
            Me.Sort = "SupplierOwnerName ASC"
            InitialPanel()
            BindGrid(Me.CmdWhere)
            'lnkID.Attributes.Add("OnClick", "return OpenWin('" & lnkID.Text & "','Marketing');")

            lblName.NavigateUrl = "javascript:OpenWin('" & Me.SupplierID & "','Marketing');"
            FillCboIDType()
            ucAddress.Style = "Marketing"
            ucAddress.ValidatorTrue()
            ucAddress.Phone1ValidatorEnabled(False)
            ucAddress.BindAddress()            
            txtuscBirthDate.IsRequired = True
        End If
    End Sub
#Region "GetCookies"
    Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies("Supplier")
        Me.CmdWhere = cookie.Values("where")
        Me.WhereCond2 = cookie.Values("where2")
        Me.Name = cookie.Values("name")
        Me.SupplierID = cookie.Values("id")
    End Sub
#End Region
    Sub BindGrid(ByVal cmdWhere As String)
        Dim dtEntity As DataTable
        Dim oCustomClass As New Parameter.Supplier
        oCustomClass.PageSize = pageSize
        oCustomClass.WhereCond = cmdWhere
        oCustomClass.SortBy = Me.Sort
        oCustomClass.WhereCond2 = Me.WhereCond2
        oCustomClass.CurrentPage = currentPage
        oCustomClass.strConnection = GetConnectionString()
        oCustomClass = m_controller.GetSupplierOwner(oCustomClass)
        If Not oCustomClass Is Nothing Then
            dtEntity = oCustomClass.ListData
            recordCount = oCustomClass.TotalRecords
        Else
            recordCount = 0
        End If
        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()
        PagingFooter()
    End Sub
    Sub InitialPanel()
        pnlList.Visible = True
        pnlAddEdit.Visible = False
        txtPage.Text = "1"
    End Sub
    Sub FillCboIDType()
        Dim oData As New DataTable
        Dim oCustomClass As New Parameter.Supplier
        oCustomClass.strConnection = GetConnectionString()
        oCustomClass = m_controller.GetSupplierOwnerIDType(oCustomClass)
        If Not oCustomClass Is Nothing Then
            oData = oCustomClass.ListData
        End If
        cboIDType.DataSource = oData
        cboIDType.DataValueField = oData.Columns(0).ToString.Trim
        cboIDType.DataTextField = oData.Columns(1).ToString.Trim
        cboIDType.DataBind()
        cboIDType.Items.Insert(0, "Select One")
        cboIDType.Items(0).Value = ""
    End Sub
#End Region
#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGrid(Me.CmdWhere)
    End Sub
    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int32)
                BindGrid(Me.CmdWhere)
            End If
        End If
    End Sub
#End Region
#Region "Sorting"
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGrid(Me.CmdWhere)
    End Sub
#End Region
#Region "Reset-Search"
    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        cboSearch.SelectedIndex = 0
        txtSearch.Text = ""
        Me.CmdWhere = "ALL"
        BindGrid(Me.CmdWhere)
    End Sub
    Private Sub imbSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonSearch.Click
        If txtSearch.Text.Trim <> "" Then
            Dim Search As String
            Search = txtSearch.Text.Trim
            Me.CmdWhere = cboSearch.SelectedItem.Value + " = '" + Search + "'"
        Else
            Me.CmdWhere = "ALL"
        End If
        BindGrid(Me.CmdWhere)
    End Sub
#End Region
#Region "Add"
    Private Sub imbAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonAdd.Click
        pnlList.Visible = False
        pnlAddEdit.Visible = True
        BindAdd()
        Me.AddEdit = "Add"
        lblTitle.Text = "ADD"
        ButtonOK.Visible = False
    End Sub
    Sub BindAdd()
        EmptyText()
    End Sub
    Sub EmptyText()
        txtName.Text = ""
        txtTitle.Text = ""
        txtBirthPlace.Text = ""
        txtuscBirthDate.Text = ""
        txtNPWP.Text = ""
        cboIDType.ClearSelection()
        txtIDNumber.Text = ""
        ucAddress.Address = ""
        ucAddress.RT = ""
        ucAddress.RW = ""
        ucAddress.Kelurahan = ""
        ucAddress.Kecamatan = ""
        ucAddress.City = ""
        ucAddress.ZipCode = ""
        ucAddress.AreaPhone1 = ""
        ucAddress.Phone1 = ""
        ucAddress.AreaPhone2 = ""
        ucAddress.Phone2 = ""
        ucAddress.AreaFax = ""
        ucAddress.Fax = ""
        ucAddress.BindAddress()
        txtMobile.Text = ""
        txtEmail.Text = ""
    End Sub
#End Region
#Region "Save & Cancel"
    Private Sub imbCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        InitialPanel()
    End Sub
    Private Sub imbSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
        Dim oSupplier As New Parameter.Supplier
        Dim oAddress As New Parameter.Address
        Dim intLoop As Integer = 0
        Dim chk As CheckBox = Nothing

        If txtuscBirthDate.Text <> "" Then
            If ConvertDate2(txtuscBirthDate.Text) > Me.BusinessDate Then                
                ShowMessage(lblMessage, "Tanggal Lahir harus < tanggal Business.....", True)
                Exit Sub
            End If
            oSupplier.BirthDate = ConvertDate(txtuscBirthDate.Text)
        Else
            oSupplier.BirthDate = ""
        End If

        oSupplier.AddEdit = Me.AddEdit
        oSupplier.strConnection = GetConnectionString()
        oSupplier.SupplierID = Me.SupplierID
        oSupplier.Name = txtName.Text
        oSupplier.Title = txtTitle.Text
        oSupplier.BirthPlace = txtBirthPlace.Text
        oSupplier.NPWP = txtNPWP.Text
        oSupplier.IDType = cboIDType.SelectedValue
        oSupplier.IDNumber = txtIDNumber.Text
        oAddress.Address = ucAddress.Address
        oAddress.RT = ucAddress.RT
        oAddress.RW = ucAddress.RW
        oAddress.Kelurahan = ucAddress.Kelurahan
        oAddress.Kecamatan = ucAddress.Kecamatan
        oAddress.City = ucAddress.City
        oAddress.ZipCode = ucAddress.ZipCode
        oAddress.AreaPhone1 = ucAddress.AreaPhone1
        oAddress.Phone1 = ucAddress.Phone1
        oAddress.AreaPhone2 = ucAddress.AreaPhone2
        oAddress.Phone2 = ucAddress.Phone2
        oAddress.AreaFax = ucAddress.AreaFax
        oAddress.Fax = ucAddress.Fax
        oSupplier.Email = txtEmail.Text
        oSupplier.HP = txtMobile.Text

        If Me.AddEdit = "Add" Then
            oSupplier.ID = ""
        ElseIf Me.AddEdit = "Edit" Then
            oSupplier.ID = Me.OwnerID
        End If

        oSupplier = m_controller.SupplierOwnerSaveAdd(oSupplier, oAddress)
        If oSupplier.Output <> "" Then            
            ShowMessage(lblMessage, oSupplier.Output, True)
            Exit Sub
        End If

        InitialPanel()
        BindGrid(Me.CmdWhere)
    End Sub
#End Region
#Region "BindEdit"
    Sub BindEdit()        
        Dim oData As New DataTable
        Dim oData2 As New DataTable
        Dim oEntity As New Parameter.Supplier
        rgvGo.Enabled = False
        ButtonOK.Visible = False
        pnlList.Visible = False
        pnlAddEdit.Visible = True
        oEntity.strConnection = GetConnectionString()
        oEntity.SupplierID = Me.SupplierID
        oEntity.ID = Me.OwnerID
        oEntity = m_controller.GetSupplierOwnerEdit(oEntity)
        If Not oEntity Is Nothing Then
            oData = oEntity.ListData
        End If
        If oData.Rows.Count > 0 Then
            oRow = oData.Rows(0)
            txtName.Text = oRow(0).ToString.Trim
            txtTitle.Text = oRow(1).ToString.Trim
            txtBirthPlace.Text = oRow(2).ToString.Trim
            If oRow(3).ToString.Trim <> "" Then
                txtuscBirthDate.Text = Format(oRow(3), "dd/MM/yyyy")
            End If
            txtNPWP.Text = oRow(4).ToString.Trim
            cboIDType.ClearSelection()
            cboIDType.Items.FindByValue(oRow(5).ToString.Trim).Selected = True
            txtIDNumber.Text = oRow(6).ToString.Trim
            ucAddress.Address = oRow(7).ToString.Trim
            ucAddress.RT = oRow(8).ToString.Trim
            ucAddress.RW = oRow(9).ToString.Trim
            ucAddress.Kelurahan = oRow(10).ToString.Trim
            ucAddress.Kecamatan = oRow(11).ToString.Trim
            ucAddress.City = oRow(12).ToString.Trim
            ucAddress.ZipCode = oRow(13).ToString.Trim
            ucAddress.AreaPhone1 = oRow(14).ToString.Trim
            ucAddress.Phone1 = oRow(15).ToString.Trim
            ucAddress.AreaPhone2 = oRow(16).ToString.Trim
            ucAddress.Phone2 = oRow(17).ToString.Trim
            ucAddress.AreaFax = oRow(18).ToString.Trim
            ucAddress.Fax = oRow(19).ToString.Trim
            ucAddress.BindAddress()
            txtMobile.Text = oRow(21).ToString.Trim
            txtEmail.Text = oRow(20).ToString.Trim
        End If
    End Sub
#End Region
#Region "ItemCommand"
    Private Sub dtgPaging_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        If e.CommandName = "Edit" Then
            If CheckFeature(Me.Loginid, "SupplierOwner", "Edit", "MAXILOAN") Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            Me.OwnerID = CType(e.Item.Cells(2).FindControl("lnkOwnerID"), LinkButton).Text.Trim
            Me.AddEdit = "Edit"
            lblTitle.Text = "EDIT"
            BindEdit()
        ElseIf e.CommandName = "Delete" Then
            If CheckFeature(Me.Loginid, "SupplierOwner", "Del", "MAXILOAN") Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            Me.OwnerID = CType(e.Item.Cells(2).FindControl("lnkOwnerID"), LinkButton).Text.Trim
            Dim oEntity As New Parameter.Supplier
            oEntity.strConnection = GetConnectionString()
            oEntity.SupplierID = Me.SupplierID
            oEntity.ID = Me.OwnerID
            Dim err As String
            err = m_controller.GetSupplierOwnerDetele(oEntity)
            If err = "" Then
                InitialPanel()                
                ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, False)
                Me.Sort = "SupplierOwnerName ASC"
                BindGrid(Me.CmdWhere)
            Else                
                ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_FAILED, True)
                Exit Sub
            End If
        ElseIf e.CommandName = "OwnerID" Then
            If CheckFeature(Me.Loginid, "SupplierOwner", "View", "MAXILOAN") Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            Me.OwnerID = CType(e.Item.Cells(2).FindControl("lnkOwnerID"), LinkButton).Text.Trim
            Response.Redirect("ViewSupplierOwnerId.aspx?OwnerID=" & Me.OwnerID & "&SupplierID=" & Me.SupplierID & "&style=Marketing")
        End If
    End Sub
#End Region

#Region "Back"
    Private Sub imbBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonBack.Click
        Response.Redirect(Request("apps") & ".aspx?currentPage=" & Request("currentPage") & "")
    End Sub
#End Region
#Region "ItemDataBound"
    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        Dim lblSupplierOwnerName As New HyperLink
        Dim lnkOwnerID As New LinkButton
        If e.Item.ItemIndex >= 0 Then
            Dim imbDelete As ImageButton
            imbDelete = CType(e.Item.FindControl("imgDelete"), ImageButton)
            imbDelete.Attributes.Add("OnClick", "return DeleteConfirm();")

            lnkOwnerID = CType(e.Item.FindControl("lnkOwnerID"), LinkButton)
            lblSupplierOwnerName = CType(e.Item.FindControl("lblSupplierOwnerName"), HyperLink)
            lblSupplierOwnerName.NavigateUrl = "javascript:OpenWinOwnerID('" & Me.SupplierID & "','" & lnkOwnerID.Text.Trim & "','Marketing');"
        End If
    End Sub
#End Region
#Region "imbOK"
    Private Sub imbOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonOK.Click
        InitialPanel()
    End Sub
#End Region

End Class