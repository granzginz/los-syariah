﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AppraisalKPR.aspx.vb"
    Inherits="Maxiloan.Webform.SalesMarketing.AppraisalKPR" MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="../webform.UserController/ucViewApplicationKPR.ascx" TagName="ucViewApplication"
    TagPrefix="uc3" %>
<%@ Register Src="../webform.UserController/ucViewCustomerDetail.ascx" TagName="ucViewCustomerDetail"
    TagPrefix="uc2" %>
<%@ Register Src="../webform.UserController/ucApplicationTabKPR.ascx" TagName="ucApplicationTab"
    TagPrefix="uc7" %>
<%@ Register Src="../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat"
    TagPrefix="uc4" %>
<%@ Register Src="../webform.UserController/ucSupplierEmployeeGrid.ascx"
    TagName="ucSupplierEmployeeGrid" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../Webform.UserController/ucFlagTransaksi.ascx" TagName="ucFlagTransaksi"
    TagPrefix="uc4" %>
<%@ Register Src="../webform.UserController/ucDateCE.ascx" TagName="ucDateCE"
    TagPrefix="uc8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Financial Data</title>
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/Lookup.css" type="text/css" />
    
    <script src="../Maxiloan.js" type="text/javascript"></script>
    
    <link href="../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
	<script src="../js/jquery-1.9.1.js" type="text/javascript"></script>
	<script src="../js/jquery-ui-1.10.3.custom.js" type="text/javascript"></script>
    
    <script language="javascript" type="text/javascript">
        function calculateNPV(id) {
            var ntf = document.getElementById('lblNilaiNTFOTR').value;
            var totalBunga = document.getElementById('lblNilaiTotalBunga').value;
            
            if (id == "txtRefundBungaN_txtNumber") {  // ini kalo yang diisi nilai nya
                var nilAmount = document.getElementById('txtRefundBungaN_txtNumber').value;
                var result = (nilAmount / totalBunga) * 100;

                var r_formated = parseFloat(Math.floor(result * 100) / 100);
                if (!r_formated.length) {
                    //document.getElementById('txtRefundBunga_txtNumber').value = number_format(r_formated, 2, 2);
                    document.getElementById('txtRefundBunga_txtNumber').value = number_format(r_formated, 2);
                } else {
                    document.getElementById('txtRefundBunga_txtNumber').value = r_formated;
                }
                document.getElementById('txtRefundBungaN_txtNumber').value = nilAmount;

            } else {  // ini kalo yang diisi persennya
                var refundRate = document.getElementById('txtRefundBunga_txtNumber').value;

                var npv = ((totalBunga / 100) * refundRate) ;

//                npv = Math.round(npv, 0) / 1000;
//                npv = Math.ceil(npv) * 1000;
                document.getElementById('txtRefundBungaN_txtNumber').value = number_format(npv, 0,2);
            }

        }

        function number_format(number, decimals, dec_point, thousands_sep) {

            number = (number + '')
                .replace(/[^0-9+\-Ee.]/g, '');
            var n = !isFinite(+number) ? 0 : +number,
                prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
                sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
                dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
                s = '',
                toFixedFix = function (n, prec) {
                    var k = Math.pow(10, prec);
                    return '' + (Math.round(n * k) / k)
                        .toFixed(prec);
                };
            // Fix for IE parseFloat(0.55).toFixed(0) = 0;
            s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
                .split('.');
            if (s[0].length > 3) {
                s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
            }
            if ((s[1] || '')
                .length < prec) {
                s[1] = s[1] || '';
                s[1] += new Array(prec - s[1].length + 1)
                    .join('0');
            }
            return s.join(dec);
        }


        function hitungtxt(txtidA, txtidB, txtidS, toPersen) {
            var status = true;
            var A = $('#' + txtidA).val();
            var S;
            var N;
            var M;
            if ($('#' + txtidS).html() == "") {
                S = $('#' + txtidS).val();
            } else {
                S = $('#' + txtidS).html();
            }

            A = A.replace(/\s*,\s*/g, '');
            S = S.replace(/\s*,\s*/g, '');

            if (status == true) {
                if (toPersen == true) {
                    N = parseInt(A) / parseInt(S) * 100;
                    $('#' + txtidB).val(number_format(parseFloat((N * 10) / 10), 2));
                } else {
                    N = parseFloat(S) * (A / 100);
                    //$('#' + txtidA).val(number_format(A, 2));
                    $('#' + txtidB).val(number_format(N, 0));
                }
            } else {
                $('#' + txtidA).val(number_format(0, 2));
                $('#' + txtidB).val(number_format(0, 2));
            }

        }


    </script>
</head>
<body>
    


<asp:UpdateProgress ID="upg1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <div runat="server" id="jlookupContent" />
    <asp:UpdatePanel runat="server" ID="UpdatePanel1">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
                onclick="hideMessage();"></asp:Label>
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>HASIL APPRAISAL</h3>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_left">
                    <h5>ENTRY HASIL APPRAISAL</h5>
                  
                </div>
                <div class="form_right">
                    <h5>APPRAISAL BERTEMU</h5>
                </div>
            </div>
            <div class="form_box">                
                <div class="form_left">
                    <label>Appraisal</label>
                    <asp:dropdownlist id="DropAppraisal" runat="server" CssClass="numberAlign2 regular_text" OnSelectedIndexChanged="switchDropAppraisal" AutoPostBack="True" >
                    <asp:listitem value="IN">Internal</asp:listitem>
                     <asp:listitem value="EX">External</asp:listitem>
                </asp:dropdownlist>
                </div>
                <div class="form_right">
                    <label>Nama Contact Person</label>
                    <asp:textbox runat="server" ID="txtNamaContactPerson" CssClass="numberAlign2 regular_text"></asp:textbox>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>Perusahaan Appraisal</label>
                        <asp:dropdownlist id="DropPerusahaanAppraisal" runat="server" CssClass="numberAlign2 regular_text">
              
                </asp:dropdownlist>

                </div>
                <div class="form_right ">
                    <label>Hubungan Contact Person</label>
                    <asp:textbox runat="server" ID="txtHubunganContact" CssClass="numberAlign2 regular_text"></asp:textbox>
                </div>
            </div>
              <div class="form_box">
                <div class="form_left">
                    <label>Appraisal</label>
                         <asp:textbox runat="server" ID="txtAppraisal" CssClass="numberAlign2 regular_text"></asp:textbox>

                </div>
                <div class="form_right ">
                    <label>No Telepon</label>
                    <asp:textbox runat="server" ID="txtNoTelepon" CssClass="numberAlign2 regular_text"></asp:textbox>
                </div>
            </div>
               <div class="form_box">
                <div class="form_left">
                    <label>Appriser</label>
                         <asp:dropdownlist id="DropAppriser" runat="server" CssClass="numberAlign2 regular_text">
              
                </asp:dropdownlist>

                </div>
                <div class="form_right ">
                    <label>No Handphone</label>
                    <asp:textbox runat="server" ID="txtNoHP" CssClass="numberAlign2 regular_text"></asp:textbox>
                </div>
            </div>
             <div class="form_box">
                <div class="form_left" >
                    <label>Tanggal Survey</label>
                   <uc8:ucDateCE id="tglSurvey" runat="server"   ></uc8:ucDateCE> 
                       Jam
                    <asp:textbox runat="server" ID="txtJam" ></asp:textbox>
                </div>
                <div class="form_right">
                   </div>
            </div>
           
             <div class="form_box">
                <div class="form_left" style="background-color:#f9f9f9">
                 <h5>TANAH</h5>
                </div>
                <div class="form_right" style="background-color:#f9f9f9">
                    <h5>BANGUNAN</h5>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                   <%-- <label>Subsidi RISK</label>--%>
                   <label> Luas Tanah (Aplikasi)</label>
                    <asp:Label   runat="server" id="lblLuasTanah" CssClass="numberAlign2 regular_text"></asp:Label>
                </div>
                <div class="form_right">
                    <label>Luas Bangunan (Aplikasi)</label>
                    <asp:Label runat="server" ID="lblLuasBangunan" CssClass="numberAlign2 regular_text"></asp:Label>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
         
                    <label>Panjang X Lebar</label>
                    <asp:Label runat="server" ID="lblPXLTanah" CssClass="numberAlign2 regular_text"></asp:Label>
                 
                </div>
                <div class="form_right ">
                    <label>Panjang X Lebar</label>
                    <asp:Label runat="server" ID="lblPXLBangunan" CssClass="numberAlign2 regular_text"></asp:Label>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                   <%-- <label>Biaya Maintenance</label>--%>
                     <label>Luas Tanah (Appraisal)</label>
                   
                    <asp:textbox runat="server" ID="TxtLuasTanahAppraisal" CssClass="numberAlign2 regular_text"></asp:textbox>
                    <label class="label_auto numberAlign2"> m2 </label>
                    
                </div>
                <div class="form_right">
                    <label>Luas</label>
              
                       <asp:textbox runat="server" ID="txtLuasBangunan" CssClass="numberAlign2 regular_text"></asp:textbox>
                     <label class="label_auto numberAlign2"> m2 </label>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left" >
                    <label>Panjang X Lebar</label>
                     <asp:textbox runat="server" ID="txtPanjangXLebar" CssClass="numberAlign2 regular_text"></asp:textbox>
              
                </div>
                <div class="form_right" >
                        <label>Panjang X Lebar</label>
                     <asp:textbox runat="server" ID="txtPanjangXLebarBangunan" CssClass="numberAlign2 regular_text"></asp:textbox>   

                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    
                       <%-- <label>Supplier/Dealer ATPM</label>--%>
                         <label>Harga Per m2</label>
                       <uc4:ucnumberformat id="UcHargaPermTanah"  runat="server" TextCssClass="numberAlign2 reguler_text" />
                    </div>
                 
               
                <div class="form_right">
                    <label>Lantai</label>
                       <asp:textbox runat="server" ID="txtLantai" CssClass="numberAlign2 regular_text"></asp:textbox>   
                    </div>
       
            </div>
            <div class="form_box">
                <div class="form_left">
                   <%-- <label>Subsidi Bunga ATPM</label>--%>
                     <label>Nilai Pasar</label>
                    <uc4:ucnumberformat runat="server" id="UcNilaiPasarTanah" TextCssClass="numberAlign2"></uc4:ucnumberformat>
                </div>
                <div class="form_right" >
                    <label>Harga Per m2</label>
                       <uc4:ucnumberformat id="UcHargaPermBangunan"  runat="server" TextCssClass="numberAlign2 reguler_text" />
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                  <label>Lukuidasi Tanah</label>
                    <asp:textbox runat="server" ID="txtLukuidasi" CssClass="numberAlign2 regular_text"></asp:textbox>
                    <label class="label_auto numberAlign2"> % </label>   
                </div>
                <div class="form_right">
                    <label>Nilai Pasar</label>
                    <uc4:ucnumberformat id="UcNilaiPasar"  runat="server" TextCssClass="numberAlign2 reguler_text" />
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                  <%--  <label>Subsidi Uang Muka ATPM</label>--%>
                     <label>Nilai Lukuidasi Tanah</label>
                     <uc4:ucnumberformat id="UcNilaiLukuidasiTanah"  runat="server" TextCssClass="numberAlign2 reguler_text" />
                </div>
                <div class="form_right border_sum">
                     <label>Lukuidasi Bangunan</label>
                    <asp:textbox runat="server" ID="txtLukuidasiBangunan" CssClass="numberAlign2 regular_text"></asp:textbox>
                    <label class="label_auto numberAlign2"> % </label>   
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
           
                </div>
                <div class="form_right">
                    <label>Nilai Lukuidasi Bangunan</label>
                     <uc4:ucnumberformat id="UcNilaiLukuidasiBangunan"  runat="server" TextCssClass="numberAlign2 reguler_text" />
                    
                </div>
            </div>
         
            </asp:Panel>
            
            <asp:Panel runat="server" ID="pnlHasilSurvey">            
            <div class="form_title">
                <div class="form_left">
                    <h3>
                        TOTAL </h3>
                </div>
                 <div class="form_right">
                    <h3>
                        NILAI BANGUNAN UNTUK ASURANSI </h3>
                </div>
                
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Nilai Pasar
                        </label>
                      <uc4:ucnumberformat id="txtNilaiPasarTtl" runat="server" />
                    </div>
                    <div class="form_right">
                        <label>
                            Nilai Per (m2)
                        </label>
                         <uc4:ucnumberformat id="txtNilaiPermTtl" runat="server" />
                    </div>
                </div>
            </div>
                 <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Nilai Likuidasi 
                        </label>
                      <uc4:ucnumberformat id="txtNilaiLikuidasiTtl" runat="server" />
                    </div>
                    <div class="form_right">
                        <label>
                            Luas (m2)
                        </label>
                         <uc4:ucnumberformat id="txtLuasMTtl" runat="server" />
                    </div>
                </div>
            </div>
            
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Sumber Pembanding
                        </label>
                      <asp:textBox id="txtSumberPerbandingan" runat="server"></asp:textBox>
                    </div>
                    <div class="form_right">
                        <label>
                            Total
                        </label>
                         <uc4:ucnumberformat id="txtTotal" runat="server" />
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Harga Internet
                        </label>
                    <uc4:ucnumberformat id="txtHargaInternetTtl" runat="server" />
                    </div>
                    <div class="form_right">
             
                    </div>
                </div>
            </div>

            <div class="form_box">
                <div>
                    <div class="form_left">
                          <label>
                            Sumber Data Internet
                        </label>
                      <asp:textBox id="txtSumbardataInternet" runat="server"></asp:textBox>
                    </div>
                    <div class="form_right">
             
                    </div>
                </div>
            </div>
                
                <div class="form_box">
                <div>
                    <div class="form_left">
                          <label>
                            Keterangan
                        </label>
                
                            <asp:TextBox runat="server" ID="txtKeterangan" TextMode="MultiLine" Width="500px" ></asp:TextBox>
                    </div>
                    <div class="form_right">
             
                    </div>
                </div>
            </div>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlSave">
            <div class="form_button">       
                 <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button green"
                    CausesValidation="True"></asp:Button>
            
            </div>    
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
         
            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
    
    </form>
</body>
</html>
