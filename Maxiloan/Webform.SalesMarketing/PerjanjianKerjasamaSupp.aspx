﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PerjanjianKerjasamaSupp.aspx.vb" Inherits="Maxiloan.Webform.SalesMarketing.PerjanjianKerjasamaSupp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>PERJANJIAN KERJASAMA SUPPLIER</title>
    <link href="../../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        var AppInfo = window.location.pathname;
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
        var ServerName = window.location.protocol + "//" + window.location.host + '/'; 
    </script>
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlSearch" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    CETAK SURAT PERJANJIAN KERJASAMA SUPPLIER</h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cari Berdasarkan</label>
                <asp:DropDownList ID="cboSearchBy" runat="server">
                    <asp:ListItem Value="">Select One</asp:ListItem>
                    <asp:ListItem Value="ag.AgreementNo">No Kontrak</asp:ListItem>
                    <asp:ListItem Value="cst.name">Nama Customer</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearchBy" runat="server" ></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Status Cetak</label>
                <asp:DropDownList ID="cboPrinted" runat="server">
                    <asp:ListItem Value="No">No</asp:ListItem>
                    <asp:ListItem Value="Yes">Yes</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
  

      <%--   <div class="form_box">
            <div class="form_single">
                <label>
                     Tanggal Loan Activation</label>
                <uc2:ucdatece id="txtPeriod1" runat="server" />
                &nbsp; S/D
                <uc2:ucdatece id="txtPeriod2" runat="server" />
            </div>
        </div>
--%>
        <div class="form_button">
            <asp:Button ID="Btnsearch" runat="server" Text="Find" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    
    </form>
</body>
</html>
