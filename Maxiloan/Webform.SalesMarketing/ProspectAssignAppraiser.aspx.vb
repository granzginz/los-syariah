﻿#Region "Imports"
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
#End Region

Public Class ProspectAssignAppraiser
    Inherits WebBased


#Region "Constanta"
    Private oController As New ProspectAssignAppraiserController
    Private oCustomclass As New Parameter.ProspectAssignAppraiser
    Private m_controller As New FinancialDataController
    Private oRefundInsentifController As New RefundInsentifController
    Private oAssetDataController As New AssetDataController


    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1


    Protected WithEvents tglSurvey As ucDateCE
#End Region

    Private Property Err() As String
        Get
            Return CType(ViewState("Err"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Err") = Value
        End Set
    End Property
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("prosCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("prosCmdWhere") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property
    Private Property ProspectAppID() As String
        Get
            Return CType(ViewState("ProspectAppID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ProspectAppID") = Value
        End Set
    End Property
    Private Property Msg() As String
        Get
            Return CType(ViewState("Msg"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Msg") = Value
        End Set
    End Property

    Private Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If

        lblMessage.Text = ""
        lblMessage.Visible = False
        If Not Page.IsPostBack Then
            If Request.QueryString("Err") <> "" Then
                Me.Err = Request.QueryString("Err")
                ShowMessage(lblMessage, Me.Err, True)
            End If

            If Request.QueryString("Msg") <> "" Then
                Me.Err = Request.QueryString("Msg")
                ShowMessage(lblMessage, Me.Msg, False)
            End If

            txtGoPage.Text = "1"
            Me.FormID = "ASGAPPRISER"

            If SessionInvalid() Then
                    Exit Sub
                End If

            If Request("cond") <> "" Then
                Me.CmdWhere = Request("cond")
            Else
                CmdWhere = String.Format("Prospect.IsProceeded=0 and Prospect.BranchID ='{0}' and Prospect.ProspectStep='SVR' ", sesBranchId.Replace("'", "").Trim)
            End If
            Me.Sort = "ProspectAppId ASC"
            BindGrid(Me.CmdWhere)
            InitialForm()

            Dim WhereBranch As String = "BranchID = '" & Replace(Me.sesBranchId, "'", "") & "'"
            FillCboEmp("BranchEmployee", DropAppriser, WhereBranch + " and (EmployeePosition='SVY' or EmployeePosition='AO') ")
            FillCbo_(DropPerusahaanAppraisal)
        End If
    End Sub

    Sub InitialForm()
        pnlDetail.Visible = False
        pnlList.Visible = True
        pnlSearch.Visible = True


    End Sub

    Sub BindGrid(ByVal cmdWhere As String)
        Dim dtEntity As DataTable
        Dim oCustomClass As New Parameter.ProspectAssignAppraiser
        pnlList.Visible = True
        oCustomClass.PageSize = pageSize

        oCustomClass.WhereCond = cmdWhere
        oCustomClass.SortBy = Me.Sort
        oCustomClass.CurrentPage = currentPage
        oCustomClass.strConnection = GetConnectionString()
        oCustomClass = oController.GetInitial(oCustomClass)
        If Not oCustomClass Is Nothing Then
            dtEntity = oCustomClass.listdata
            recordCount = oCustomClass.TotalRecords
        Else
            recordCount = 0
        End If
        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()
        PagingFooter()
    End Sub

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan!", True)

            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblTotRec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGrid(Me.CmdWhere)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGoPage.Click
        If IsNumeric(txtGoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtGoPage.Text, Int32)
                BindGrid(Me.CmdWhere)
            End If
        End If
    End Sub


    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGrid(Me.CmdWhere)
    End Sub


    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        cboSearch.SelectedIndex = 0
        txtSearch.Text = ""
        Me.CmdWhere = String.Format("Prospect.IsProceeded=0 and Prospect.BranchID ='{0}' and Prospect.ProspectStep='SVR' ", sesBranchId.Replace("'", "").Trim)
        BindGrid(Me.CmdWhere)
    End Sub

    Private Sub imbSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        CmdWhere = String.Format("Prospect.IsProceeded=0 and Prospect.BranchID ='{0}' and Prospect.ProspectStep='SVR' ", sesBranchId.Replace("'", "").Trim)
        If txtSearch.Text.Trim <> "" Then
            CmdWhere = String.Format("isProceeded=0 and {0} LIKE '%{1}%' and  Prospect.BranchID ='{2}' and Prospect.ProspectStep='SVR' ", cboSearch.SelectedItem.Value, txtSearch.Text.Replace("%", ""), sesBranchId.Replace("'", "").Trim)
        End If
        BindGrid(Me.CmdWhere)
    End Sub

    Private Sub dtgEntity_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        If e.Item.ItemIndex >= 0 Then
            If e.CommandName = "Modify" Then
                Me.ProspectAppID = CType(e.Item.FindControl("lblApplication"), HyperLink).Text.Trim

                If SessionInvalid() Then
                    Exit Sub
                End If

                'Response.Redirect("InputDataSurveyMain.aspx?id=" & ProspectAppID.Trim & "&page=Edit")
                'OPEN PANEL
                loadData(Me.ProspectAppID)
                pnlSearch.Visible = False
                pnlList.Visible = False
                pnlReturn.Visible = False
                pnlDetail.Visible = True
            End If
        End If
    End Sub

    Private Sub loadData(ByVal prosID As String)

        Dim oAssetData As New Parameter.ProspectAssignAppraiser
        Dim oData As New DataTable
        Dim oRow As DataRow
        oAssetData.strConnection = GetConnectionString()
        oAssetData.BranchId = Replace(Me.sesBranchId, "'", "")
        oAssetData.ProspectAppID = prosID
        oAssetData = oController.GetDataAppraisal(oAssetData)

        If oAssetData IsNot Nothing Then
            oData = oAssetData.listdata

        End If

        If oData.Rows.Count > 0 Then
            oRow = oData.Rows(0)
            DropAppraisal.SelectedIndex = DropAppraisal.Items.IndexOf(DropAppraisal.Items.FindByValue(CStr(oRow.Item("SelectedAppraisal"))))
            DropPerusahaanAppraisal.SelectedIndex = DropPerusahaanAppraisal.Items.IndexOf(DropPerusahaanAppraisal.Items.FindByValue(CStr(oRow.Item("PerusahaanAppraisal"))))
            txtAppraisal.Text = oRow.Item("Appraisal").ToString.Trim
            DropAppriser.SelectedIndex = DropAppriser.Items.IndexOf(DropAppriser.Items.FindByValue(CStr(oRow.Item("Appriser"))))
            tglSurvey.Text = CDate(oRow.Item("TangggalSurvey").ToString.Trim).ToString("dd/MM/yyyy")
            txtJam.Text = oRow.Item("jam").ToString.Trim
        End If


    End Sub

    Sub ProsesReturn()
        Dim oApplication As New Parameter.ProspectAssignAppraiser

        oApplication.strConnection = GetConnectionString()
        oApplication.BusinessDate = Me.BusinessDate
        oApplication.BranchId = Me.sesBranchId.Replace("'", "").Trim
        oApplication.ProspectAppID = Me.ProspectAppID
        oApplication.Notes = txtAlasanReturn.Text.Trim

        Try
            oController.ProspectReturnUpdate(oApplication)

            If Request("cond") <> "" Then
                Me.CmdWhere = Request("cond")
            Else
                CmdWhere = String.Format("Prospect.IsProceeded=0 and Prospect.BranchID ='{0}' and Prospect.ProspectStep='SVR' ", sesBranchId.Replace("'", "").Trim)
            End If
            Me.Sort = "ProspectAppId ASC"
            BindGrid(Me.CmdWhere)
            pnlList.Visible = True
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try

    End Sub
    Protected Sub btnReturn_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnReturn.Click
        ProsesReturn()
        If Request("cond") <> "" Then
            Me.CmdWhere = Request("cond")
        Else
            CmdWhere = String.Format("Prospect.IsProceeded=0 and Prospect.BranchID ='{0}' and Prospect.ProspectStep='SVR' ", sesBranchId.Replace("'", "").Trim)
        End If
        Me.Sort = "ProspectAppId ASC"
        BindGrid(Me.CmdWhere)
        pnlList.Visible = True
        pnlSearch.Visible = True
        pnlReturn.Visible = False
    End Sub
    Protected Sub ButtonCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonCancel.Click
        If Request("cond") <> "" Then
            Me.CmdWhere = Request("cond")
        Else
            CmdWhere = String.Format("Prospect.IsProceeded=0 and Prospect.BranchID ='{0}' and Prospect.ProspectStep='SVR' ", sesBranchId.Replace("'", "").Trim)
        End If
        Me.Sort = "ProspectAppId ASC"
        BindGrid(Me.CmdWhere)

        pnlList.Visible = True
        pnlSearch.Visible = True
        pnlReturn.Visible = False
    End Sub



#Region "BindEdit"
    Sub BindEdit()
        Dim oApplication As New Parameter.ProspectAssignAppraiser
        Dim oData As New DataTable
        Dim oRow As DataRow

        oApplication.strConnection = GetConnectionString()
        oApplication.BranchId = Replace(Me.sesBranchId, "'", "").ToString
        oApplication.ProspectAppID = Me.ProspectAppID
        oApplication = oController.GetViewProspect(oApplication)

        If Not oApplication Is Nothing Then
            oData = oApplication.listdata
        End If

        If oData.Rows.Count > 0 Then
            oRow = oData.Rows(0)
            lbltanggalAplikasi.Text = Format(oRow("ProspectAppDate"), "dd/MM/yyyy")
            lblProspectAppId.Text = oRow("ProspectAppID").ToString
            lblProspectAppId.Attributes.Add("onClick", "javascript:OpenWinViewProspectInquiry('" + lblProspectAppId.Text.Trim() + "');")
            lblSupplierName.Text = oRow("SupplierName").ToString

            lblNama.Text = oRow("Name").ToString
            lblJenisAsset.Text = oRow("AssetTypeDescription").ToString
            lblMerkAsset.Text = oRow("AssetBrandDescription").ToString
        End If
    End Sub

    Private Sub dtgPaging_ItemDataBound(sender As Object, e As DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        If SessionInvalid() Then
            Exit Sub
        End If

        Dim hyTemp As HyperLink
        Dim HyappId As HyperLink

        If e.Item.ItemIndex >= 0 Then

            HyappId = CType(e.Item.FindControl("lblApplication"), HyperLink)
            hyTemp = CType(e.Item.FindControl("lblApplication"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenWinViewProspectInquiry('" & hyTemp.Text.Trim & "')"
        End If
    End Sub
#End Region
    Public Sub switchDropAppraisal()
        If DropAppraisal.SelectedValue = "IN" Then
            txtAppraisal.Enabled = False
            DropPerusahaanAppraisal.Enabled = False
            DropAppriser.Enabled = True
        Else
            txtAppraisal.Enabled = True
            DropPerusahaanAppraisal.Enabled = True
            DropAppriser.Enabled = False
        End If
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim ofinancialData As New Parameter.ProspectAssignAppraiser
        Try

            With ofinancialData
                .strConnection = GetConnectionString()
                .BusinessDate = Me.BusinessDate
                .BranchId = Replace(Me.sesBranchId, "'", "")
                .ProspectAppID = Me.ProspectAppID
                .PerusahaanAppraisal = DropPerusahaanAppraisal.SelectedValue
                .Appraisal = txtAppraisal.Text
                .Appriser = DropAppriser.SelectedValue
                .TangggalSurvey = ConvertDate2(tglSurvey.Text)
                .jam = txtJam.Text
                .SelectedAppraisal = DropAppraisal.SelectedValue



                .LuastanahApplikasi = 0

                .PanjangXLebarTanahApplikasi = ""
                .LuasTanahAppraisal = 0
                .PanjangXLebarTanah = ""
                .HargaPerMTanah = 0.00
                .NilaiPasarTanah = 0.00
                .LikuidasiTanah = 0
                .NilaiLikuidasiTanah = 0.00
                .NamaContact = ""
                .HubunganContact = ""
                .NoTelp = ""
                .NoHP = ""

                .LuasBangunanApplikasi = ""


                .PanjangXLebarBangunanApplikasi = ""


                .LuasBangunan = 0
                .PanjangXLebarBangunan = ""
                .Lantai = 0
                .HargaPermBangunan = 0.00
                .NilaiPasarBangunan = 0.00
                .NilaiLikuidasiBangunan = 0.00
                .NilaiPasarTtl = vbNull
                .NilaiPermTtl = 0.00
                .NilaiLikuidasiTtl = vbNull
                .LuasMTtl = vbNull
                .SumberPerbandingan = ""
                .HargaInternetTtl = 0.00
                .SumbardataInternet = ""
                .TotalSurvey = 0.00
                .Keterangan = ""
                .TotalNilaiLikuidasiBangunan = 0.00
            End With

            ofinancialData = oController.SaveProspectAssignAppraisal(ofinancialData)

            If ofinancialData.ExecuteResult <> "" Then
                ShowMessage(lblMessage, "Succes - save data", False)
                clearInput()
                InitialForm()
            End If


        Catch ex As Exception
            ShowMessage(lblMessage, "error - " & ex.Message, True)
        End Try

    End Sub

    Sub clearInput()
        DropAppraisal.SelectedIndex = 0
        DropPerusahaanAppraisal.SelectedIndex = 0
        txtAppraisal.Text = ""
        DropAppriser.SelectedIndex = 0
        tglSurvey.Text = ""
        txtJam.Text = ""
    End Sub

    Protected Sub FillCbo_(cboReference As DropDownList)
        Dim dtEntity As DataTable = Nothing
        Dim oCustomer As New Parameter.FinancialData
        oCustomer.strConnection = GetConnectionString()
        oCustomer.Table = "AppraisalCompany"
        oCustomer = m_controller.GetDataDrop(oCustomer)
        If Not oCustomer Is Nothing Then
            dtEntity = oCustomer.ListData
        End If

        cboReference.DataSource = dtEntity.DefaultView
        cboReference.DataTextField = "Description"
        cboReference.DataValueField = "ID"
        cboReference.DataBind()
        cboReference.Items.Insert(0, "Select One")
        cboReference.Items(0).Value = "Select One"

    End Sub
    Protected Sub FillCboEmp(ByVal Table As String, ByVal cboName As DropDownList, ByVal Where As String)
        Dim oAssetData As New Parameter.AssetData
        Dim oData As New DataTable

        oAssetData.strConnection = GetConnectionString()
        oAssetData.Where = Where
        oAssetData.Table = Table
        oAssetData = oAssetDataController.GetCboEmp(oAssetData)

        oData = oAssetData.ListData
        cboName.DataSource = oData
        cboName.DataTextField = "Name"
        cboName.DataValueField = "ID"
        cboName.DataBind()
        cboName.Items.Insert(0, "Select One")
        cboName.Items(0).Value = "Select One"
    End Sub



End Class