﻿Imports Maxiloan.Controller

Public Class UploadList
    Inherits Maxiloan.Webform.ControlBased

    Private octr As New MitraController
    Public Property mfid As String
        Get
            Return ViewState("mfid").ToString
        End Get
        Set(value As String)
            ViewState("mfid") = value
        End Set
    End Property
    Public Property mffacilityno As String
        Get
            Return ViewState("mffacilityno").ToString
        End Get
        Set(value As String)
            ViewState("mffacilityno") = value
        End Set
    End Property
    Public Property mfbatchno As String
        Get
            Return ViewState("mfbatchno").ToString
        End Get
        Set(value As String)
            ViewState("mfbatchno") = value
        End Set
    End Property

    Public Property showcheckbox As Boolean
        Get
            Return CType(ViewState("showcheckbox"), Boolean)
        End Get
        Set(value As Boolean)
            ViewState("showcheckbox") = value
        End Set
    End Property

    Public Property showbutton As Boolean
        Get
            Return CType(ViewState("showbutton"), Boolean)
        End Get
        Set(value As Boolean)
            ViewState("showbutton") = value
        End Set
    End Property

    Public Property showradio As Boolean
        Get
            Return CType(ViewState("showradio"), Boolean)
        End Get
        Set(value As Boolean)
            ViewState("showradio") = value
        End Set
    End Property

    Public Sub dobind()
        dtg1.Columns(0).Visible = showcheckbox
        dtg1.Columns(1).Visible = showradio
        dtg1.Columns(2).Visible = showbutton

        Dim oclass As New Parameter.Lending

        With oclass
            .strConnection = GetConnectionString()
        End With

        Try
            oclass = octr.uploadlist(oclass)
            dtg1.DataSource = oclass.uploadlist
            dtg1.DataBind()
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub
End Class