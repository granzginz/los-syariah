﻿Imports System.IO
Imports Maxiloan.Controller
Imports Maxiloan.Parameter

Public Class LendingDebtorUpload
    Inherits WebBased
    Private ReadOnly _jvController As New LendingProcessController


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        FormID = "LendingDebtorUpload"
        lblMessage.Visible = False
        ' btnNext.Enabled = False
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not Page.IsPostBack Then
            If Request("s") = "1" Then
                ShowMessage(lblMessage, "Sukses", False)
            End If
        End If
    End Sub
    Private filesName As String() = {"DISBHEADER", "DISBDEBTOR", "DISBDEBTORMGT", "DISBAGREEMENT", "DISBASSET"} ', "DISBINSURANCE", "DISBINSURANCEDETAIL"}


    Protected Sub btnFind_Click(sender As Object, e As EventArgs) Handles btnFind.Click

        lblInvalidMsg.Text = ""
        divinvldmsg.Visible = False


        Dim list = New List(Of String)

        Dim resultDic As New Dictionary(Of String, StreamReader)

        If Not flUpload.HasFile Then
            ShowMessage(lblMessage, "File belum dipilih!", True)
            Exit Sub
        End If

        Dim Selectedfiles As HttpFileCollection = Request.Files

        For i As Integer = 0 To Selectedfiles.Count - 1
            Dim file As HttpPostedFile = Selectedfiles(i)
            list.Add(file.FileName.Trim())
            resultDic.Add(file.FileName.Trim(), New StreamReader(file.InputStream))
        Next

        Dim selHead = list.Where(Function(x) x.Trim().ToUpper().Contains("DISBHEADER")).ToList()

        If (selHead Is Nothing Or selHead.Count <> 1) Then
            ShowMessage(lblMessage, "Silahkan pilih satu DISBHEADER !!", True)
            CloseStream(resultDic)
            Exit Sub
        End If

        Dim sel = selHead.First()
        Dim val = sel.Split("_")
        Dim searc = $"{Val(0)}_{{0}}_{Val(2)}_{Val(3)}"

        Dim validateFileList = New List(Of String)
        For Each s As String In filesName
            If Not list.Contains(String.Format(searc, s)) Then
                validateFileList.Add($"{String.Format(searc, s)}")
            End If
        Next

        searc = ""
        For Each s As String In validateFileList
            searc = searc + s + "</br>"
        Next

        'If searc <> "" Then
        If searc = "" Then
            searc = "File tidak ditemukan : </br>" + searc
            CloseStream(resultDic)
            ShowMessage(lblMessage, searc, True)
            CloseStream(resultDic)
            Exit Sub
        End If


        Dim mgr = New PaserManager()
        Dim inValidString = New StringBuilder
        Try
            mgr.NewparseToList(resultDic)
            For Each s In mgr.DisbParamList
                For Each it In s.Value
                    inValidString.AppendLine(it.ValidateString.ToString())
                Next
            Next

            If (inValidString.ToString.Trim().Length > 0) Then
                divinvldmsg.Visible = True
                lblInvalidMsg.Text = inValidString.ToString
                'lblInvalidMsg.Text = inValidString.Replace(vbCrLf, "</br>").ToString()
                CloseStream(resultDic)
                Exit Sub
            End If
            _jvController.ProcessUploadCsvLending(GetConnectionString(), mgr.DisbParamList)
        Catch ex As Exception
            CloseStream(resultDic)
            ShowMessage(lblMessage, ex.Message, True)
            Exit Sub
        End Try


        CloseStream(resultDic)
        Response.Redirect("LendingDebtorUpload.aspx?s=1")

    End Sub
    Sub CloseStream(resultDic As Dictionary(Of String, StreamReader))
        For Each reader In resultDic
            reader.Value.Close()
        Next
    End Sub
End Class