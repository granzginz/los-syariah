﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="BpkbAngsuranUpload.aspx.vb" Inherits="Maxiloan.Webform.PembiayaanMitra.BpkbAngsuranUpload" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   <title>BPKB dan Angsuran Upload</title>
     <script src="../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" /> 
    <script  type="text/javascript" src="../js/jquery-1.9.1.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"  />
        <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
        <div class="form_title">
            <div class="title_strip"></div>
            <div class="form_single">
                <h3>Bpkb dan Angsuran Upload</h3>
            </div> 
        </div> 


        <div class="form_box">
            <div class="form_single">
                <label>Pilih Applikasi</label>
                <asp:DropDownList ID="cboimpAppBy" runat="server">
                    <asp:ListItem Value="COLLATERAL" Selected="True">BPKB</asp:ListItem>
                    <asp:ListItem Value="PAYMENT">Angsuran</asp:ListItem> 
                </asp:DropDownList>
            </div>
        </div> 
        
        <div  class="form_box" style="overflow:auto; "   > 
            <div  style="width: 45%;  margin:5px;  padding: 1em;float:left;"> 
                 
                    <div class="form_single"> 
                        <label>Pilih Header File :</label> 
                        <asp:FileUpload ID="flHeader" runat="server" />  
                    </div>  
                
                    <div class="form_single"> 
                        <label>Pilih Detail File :</label> 
                        <asp:FileUpload ID="flDetail" runat="server" />  
                    </div>  
                 
                <div id="topic-progress-wrapper"  ></div>   
                <div class="form_button"> 
                    <asp:Button ID="btnFind" runat="server" Text="Upload"  CssClass="small button green" />    
                </div>  
            </div>

            <div  style="width: 45%;  margin:5px;  padding: 1em;float:right ;">
                <div id="object3"><asp:Label ID="lblInvalidMsg" runat="server" Visible="false"></asp:Label></div>  

            </div> 

        </div>  
    </form>
</body>
</html>
