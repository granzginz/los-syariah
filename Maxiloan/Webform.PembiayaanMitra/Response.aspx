﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Response.aspx.vb" Inherits="Maxiloan.Webform.PembiayaanMitra.Response" ValidateRequest="false" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />
    <script src="../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>        
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                </h3>
            </div>
        </div>
        <div class="form_button">
            <asp:Button runat="server" ID="btnback" CssClass="small button gray" Text="Back" />
        </div>
    </form>
</body>
</html>
