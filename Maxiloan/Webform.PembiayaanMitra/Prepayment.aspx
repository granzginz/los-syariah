﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Prepayment.aspx.vb" Inherits="Maxiloan.Webform.PembiayaanMitra.Prepayment" %>

<!DOCTYPE html>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ucLendingAgreementList" Src="ucLendingAgreementList.ascx" %>
<%@ Register TagPrefix="uc2" TagName="ucKontrakDetail" Src="ucKontrakDetail.ascx" %>
<%@ Register TagPrefix="uc3" TagName="ucPerhitunganPelunasan" Src="ucPerhitunganPelunasan.ascx" %>
<%@ Register TagPrefix="uc4" TagName="ucNumberFormat" Src="../Webform.UserController/ucNumberFormat.ascx" %>
<%@ Register TagPrefix="uc5" TagName="ucDateCE" Src="../Webform.UserController/ucDateCE.ascx" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="Stylesheet" type="text/css" href="../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../Include/Buttons.css" />
    <script src="../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server" ID="ScriptManager1">
        </asp:ScriptManager>
        <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>PELUNASAN
                </h3>
            </div>
        </div>

        <asp:Panel ID="PnlForm1" runat="server">
             <uc1:ucLendingAgreementList ID="ucLendingAgreementList1" runat="server"></uc1:ucLendingAgreementList>
        </asp:Panel>

        <asp:Panel ID="pnlForm2" runat="server">
             <uc2:ucKontrakDetail ID="ucKontrakDetail1" runat="server"></uc2:ucKontrakDetail>
        </asp:Panel>

         <asp:Panel ID="pnlForm3" runat="server">
             <uc3:ucPerhitunganPelunasan ID="ucPerhitunganPelunasan1" runat="server"></uc3:ucPerhitunganPelunasan>
        </asp:Panel>

        <asp:Panel ID="pnlForm4" runat="server">
             <div class="form_box">
                <div class="form_single">
                    <label>Reference No</label>
                    <asp:TextBox ID="txtReferenceNo" runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>Jumlah</label>
                    <uc4:ucNumberFormat ID="txtjumlah" runat="server"></uc4:ucNumberFormat>
                </div>
             </div>
            <div class="form_box">
                <div class="form_single">
                    <label>Tanggal Valuta</label>
                    <uc5:ucDateCE ID="ucValueDate" runat="server"></uc5:ucDateCE>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>Payment Type</label>
                    <asp:DropDownList ID="cbowop" runat="server" />
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>Rekening Bank</label>
                    <asp:DropDownList ID="cmbBankAccount" runat="server">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator3" runat="server" InitialValue="0"
                        Enabled="True" Display="Dynamic" ControlToValidate="cmbBankAccount" ErrorMessage="Harap Pilih rekening Bank!"
                        Visible="True" CssClass="validator_general">
                    </asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Keterangan</label>
                    <asp:TextBox ID="txtKeterangan" runat="server" CssClass="long_text"></asp:TextBox>
                </div>
            </div>
        </asp:Panel>

        <div class="form_button">
            <asp:Button ID="BtnPrint" runat="server" CausesValidation="False" Text="Print"
                CssClass="small button blue"></asp:Button>
            <asp:Button ID="BtnSearch" runat="server" CausesValidation="False" Text="Find"
                CssClass="small button blue"></asp:Button>
            <asp:Button ID="BtnReset" runat="server" CausesValidation="False" Text="Reset"
                CssClass="small button gray"></asp:Button>
        </div>
    </form>
</body>
</html>
