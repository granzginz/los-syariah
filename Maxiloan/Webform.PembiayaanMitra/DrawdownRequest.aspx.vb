﻿Imports Maxiloan.Controller

Public Class DrawdownRequest
    Inherits Maxiloan.Webform.WebBased
    Private octr As New MitraController
    Protected WithEvents ouploadlist1 As UploadList
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Visible = False
        If SessionInvalid() Then Exit Sub
        If Page.IsPostBack Then
        Else
            If Me.IsHoBranch Then
                ShowMessage(lblMessage, "Harap login di cabang!", True)
                Exit Sub
            Else
                Me.FormID = "LDISBREQ"
                ouploadlist1.showradio = False
                ouploadlist1.showcheckbox = True
                ouploadlist1.showbutton = False
                ouploadlist1.dobind()
            End If
        End If
    End Sub

    Private Sub BtnSave_Click(sender As Object, e As EventArgs) Handles BtnSave.Click
        Dim oclass As New Parameter.Lending
        Dim strnotif As New StringBuilder
        For Each item As DataGridItem In CType(ouploadlist1.FindControl("dtg1"), DataGrid).Items
            If item.ItemType = ListItemType.Item Or item.ItemType = ListItemType.AlternatingItem Or item.ItemType = ListItemType.EditItem Then
                If CType(item.FindControl("chk"), CheckBox).Checked Then
                    With oclass
                        '.mfid = ouploadlist1.mfid
                        '.mffacilityno = ouploadlist1.mffacilityno
                        '.mfbatchno = ouploadlist1.mfbatchno
                        .mfid = item.Cells(3).Text.Trim
                        .mffacilityno = item.Cells(4).Text.Trim
                        .mfbatchno = item.Cells(5).Text.Trim
                        .strConnection = GetConnectionString()
                    End With
                    Try
                        octr.lendingdrawdownrequest(oclass)
                        strnotif.AppendFormat("Batch {0} Saved! ENTER", oclass.mfbatchno)
                    Catch ex As Exception
                        strnotif.AppendFormat("Batch {0} Error! {1} ENTER", oclass.mfbatchno, Right(ex.Message, 30) & "...")
                        Continue For
                    End Try
                End If
            End If
        Next
        Response.Redirect("response.aspx?callerurl=drawdownrequest.aspx&notif=" & strnotif.ToString & "&iserror=0")
    End Sub
End Class