﻿#Region "Imports"
Imports System.IO
Imports Maxiloan
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
#End Region

Public Class MitraMultifinance

    Inherits Maxiloan.Webform.WebBased

    Protected WithEvents ucMailingAddress As UcCompanyAddress2

#Region " Private Const "
    Dim m_Mitra As New MitraController

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

#End Region

#Region " Property "

    Private Property isDuplicate() As String
        Get
            Return CStr(viewstate("isDuplicate"))
        End Get
        Set(ByVal Value As String)
            viewstate("isDuplicate") = Value
        End Set
    End Property

    Private Property MitraID() As String
        Get
            Return CStr(ViewState("MitraID"))
        End Get
        Set(ByVal Value As String)
            ViewState("MitraID") = Value
        End Set
    End Property

    Private Property MitraName() As String
        Get
            Return CStr(ViewState("MitraName"))
        End Get
        Set(ByVal Value As String)
            ViewState("MitraName") = Value
        End Set
    End Property

    Public Property ActionAddEdit() As String
        Get
            Return CStr(ViewState("ActionAddEdit"))
        End Get
        Set(ByVal Value As String)
            viewstate("ActionAddEdit") = Value
        End Set
    End Property

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If
        If Not Me.IsPostBack Then

            Me.FormID = "JFMitra"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                ClearAddForm()
                InitialDefaultPanel()
                Me.SearchBy = ""
                Me.SortBy = ""

                UCMailingAddress.Phone1ValidatorEnabled(False)
                ucMailingAddress.BPKBView()

                If Request("cond") <> "" Then
                    Me.SearchBy = Request("cond")
                Else
                    Me.SearchBy = "ALL"
                End If
                Me.SortBy = ""
                BindGridEntity(Me.SearchBy, Me.SortBy)

            End If
        End If
    End Sub

#Region " PanelAllFalse "
    Private Sub PanelAllFalse()
        pnlList.Visible = False
        pnlAddEdit.Visible = False
        lblMessage.Visible = False
    End Sub
#End Region

#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblTotRec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridEntity(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGoPage.Click

        If txtGoPage.Text = "" Then
            txtGoPage.Text = "0"
        Else
            If IsNumeric(txtGoPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                    currentPage = CType(txtGoPage.Text, Int32)
                    BindGridEntity(Me.SearchBy, Me.SortBy)
                End If
            End If
        End If


    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.SearchBy, Me.SortBy)
    End Sub

#End Region

#Region " InitialDefaultPanel "
    Private Sub InitialDefaultPanel()
        PanelAllFalse()
        pnlList.Visible = True
    End Sub
#End Region

#Region " BindGridEntity "
    Sub BindGridEntity(ByVal cmdWhere As String, ByVal cmSort As String)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim oMitra As New Parameter.MitraMultifinance

        With oMitra
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With


        oMitra = m_Mitra.ListMitra(oMitra)

        With oMitra
            lblTotRec.Text = CType(.TotalRecord, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecord
        End With

        dtsEntity = oMitra.ListData
        dtvEntity = dtsEntity.DefaultView
        dtvEntity.Sort = Me.SortBy

        If dtvEntity.Count <= 0 Then
            btnPrint.Enabled = False
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
        Else
            btnPrint.Enabled = True
        End If

        dtg.DataSource = dtvEntity
        Try
            dtg.DataBind()
        Catch
            dtg.CurrentPageIndex = 0
            dtg.DataBind()
        End Try

        PagingFooter()
    End Sub
#End Region

    Private Sub ClearAddForm()

        txtID.Text = ""
        txtName.Text = ""
        txtInitialName.Text = ""
        txtJoindate.Text = ""

        lblID.Visible = False
        txtID.Visible = True

        txtMainPhone.Text = ""
        txtAlternativePhone.Text = ""
        txtWebsite.Text = ""
        txtFax.Text = ""
        txtNotes.Text = ""

        With ucMailingAddress
            .Address = ""
            .RT = ""
            .RW = ""
            .Kecamatan = ""
            .Kelurahan = ""
            .City = ""
            .ZipCode = ""
            .AreaPhone1 = ""
            .Phone1 = ""
            .AreaPhone2 = ""
            .Phone2 = ""
            .AreaFax = ""
            .Fax = ""
            .Provinsi = ""
            .Style = "Setting"
            .BindAddress()
        End With

    End Sub

    Private Sub BtnAddNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnAddNew.Click
        If CheckFeature(Me.Loginid, Me.FormID, "Add", Me.AppId) Then
            If SessionInvalid() Then
                Exit Sub
            End If

            ClearAddForm()
            PanelAllFalse()
            Me.ActionAddEdit = "ADD"
            lblMenuAddEdit.Text = "ADD"
            pnlAddEdit.Visible = True

        End If
    End Sub

    Private Sub BtnSearchNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSearchNew.Click
        Dim StrSearchBy As String = cboSearchBy.SelectedItem.Value
        Dim StrSearchByValue As String = TxtSearchByValue.Text.Trim

        If StrSearchByValue = "" Then
            Me.SearchBy = "all"
            Me.SortBy = ""
        Else
            Me.SearchBy = StrSearchBy + "='" + StrSearchByValue + "'"
        End If
        PanelAllFalse()
        BindGridEntity(Me.SearchBy, Me.SortBy)
        pnlList.Visible = True
    End Sub

    Private Sub viewbyID(ByVal Companyid As String)
        Dim oMitra As New Parameter.MitraMultifinance
        Dim dtMitra As New DataTable

        Try
            Dim strConnection As String = GetConnectionString()

            With oMitra
                .strConnection = GetConnectionString()
                .MitraID = Companyid
                .SortBy = ""
                .WhereCond = "MfCode ='" & Companyid & "'"
            End With

            oMitra = m_Mitra.ListMitraByID(oMitra)
            dtMitra = oMitra.ListData

            'Isi semua field Edit Action
            txtID.Visible = False
            lblID.Visible = True
            lblID.Text = Companyid
            txtName.Text = CStr(dtMitra.Rows(0).Item("FullName")).Trim
            txtInitialName.Text = CStr(dtMitra.Rows(0).Item("InitialName")).Trim
            txtJoindate.Text = CStr(dtMitra.Rows(0).Item("JoinDate")).Trim

            With ucMailingAddress
                .Address = CStr(dtMitra.Rows(0).Item("Address")).Trim
                .RT = CStr(dtMitra.Rows(0).Item("RT")).Trim
                .RW = CStr(dtMitra.Rows(0).Item("RW")).Trim
                .Kelurahan = CStr(dtMitra.Rows(0).Item("Kelurahan")).Trim
                .Kecamatan = CStr(dtMitra.Rows(0).Item("Kecamatan")).Trim
                .City = CStr(dtMitra.Rows(0).Item("City")).Trim
                .ZipCode = CStr(dtMitra.Rows(0).Item("ZipCode")).Trim
                .Provinsi = CStr(dtMitra.Rows(0).Item("ProvinceName")).Trim
                .BindAddress()
            End With
            txtMainPhone.Text = CStr(dtMitra.Rows(0).Item("Phone1")).Trim
            txtAlternativePhone.Text = CStr(dtMitra.Rows(0).Item("Phone2")).Trim
            txtFax.Text = CStr(dtMitra.Rows(0).Item("Fax")).Trim
            txtWebsite.Text = CStr(dtMitra.Rows(0).Item("WebSite")).Trim
            txtNotes.Text = CStr(dtMitra.Rows(0).Item("Notes")).Trim

        Catch ex As Exception

        End Try

    End Sub

#Region "ADD"
    Private Sub Add()

        Dim customClass As New Parameter.MitraMultifinance
        Dim oClassAddress As New Parameter.Address

        With customClass
            .strConnection = GetConnectionString()
            .MitraID = txtID.Text.Trim
            .MitraFullName = txtName.Text.Trim
            .MitraInitialName = txtInitialName.Text.Trim
            .JoinDate = txtJoindate.Text
            .Phone1 = txtMainPhone.Text.Trim
            .Phone2 = txtAlternativePhone.Text.Trim
            .Fax = txtFax.Text.Trim
            .WebSite = txtWebsite.Text.Trim
            .Notes = txtNotes.Text.Trim
            .Provinsi = ucMailingAddress.AreaPhone1.Trim
            .LoginId = Me.Loginid.Trim
        End With

        With oClassAddress
            .Address = ucMailingAddress.Address
            .RT = ucMailingAddress.RT
            .RW = ucMailingAddress.RW
            .Kelurahan = ucMailingAddress.Kelurahan
            .Kecamatan = ucMailingAddress.Kecamatan
            .City = ucMailingAddress.City
            .ZipCode = ucMailingAddress.ZipCode
        End With

        Try
            m_Mitra.AddMitra(customClass, oClassAddress)
            ShowMessage(lblMessage, "Data Berhasil disimpan ", False)
            'MsgBox(MessageHelper.MESSAGE_INSERT_SUCCESS)
            Me.isDuplicate = "N"
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
            Me.isDuplicate = "Y"
        End Try
    End Sub
#End Region

#Region "EDIT"
    Private Sub edit(ByVal Companyid As String)
        Dim customClass As New Parameter.MitraMultifinance
        Dim oClassAddress As New Parameter.Address

        With customClass
            .strConnection = GetConnectionString()
            .MitraID = Companyid.Trim
            .MitraFullName = txtName.Text.Trim
            .MitraInitialName = txtInitialName.Text.Trim
            .JoinDate = txtJoindate.Text
            .Phone1 = txtMainPhone.Text.Trim
            .Phone2 = txtAlternativePhone.Text.Trim
            .Fax = txtFax.Text.Trim
            .WebSite = txtWebsite.Text.Trim
            .Notes = txtNotes.Text.Trim
            .Provinsi = ucMailingAddress.Provinsi.Trim
            .LoginId = Me.Loginid.Trim
        End With

        With oClassAddress
            .Address = ucMailingAddress.Address
            .RT = ucMailingAddress.RT
            .RW = ucMailingAddress.RW
            .Kelurahan = ucMailingAddress.Kelurahan
            .Kecamatan = ucMailingAddress.Kecamatan
            .City = ucMailingAddress.City
            .ZipCode = ucMailingAddress.ZipCode
        End With

        Try
            m_Mitra.EditMitra(customClass, oClassAddress)
            ShowMessage(lblMessage, "Data Berhasil diupdate ", False)
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

#End Region

    Private Sub dtg_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtg.ItemCommand
        Select Case e.CommandName

            Case "Edit"
                If CheckFeature(Me.Loginid, Me.FormID, "Edit", Me.AppId) Then
                    If SessionInvalid() Then
                        Exit Sub
                    End If

                    Me.MitraID = e.Item.Cells(2).Text
                    pnlList.Visible = False
                    pnlAddEdit.Visible = True
                    Me.ActionAddEdit = "EDIT"
                    lblMenuAddEdit.Text = "EDIT"
                    viewbyID(Me.MitraID.Trim)
                End If

            Case "Delete"
                If CheckFeature(Me.Loginid, Me.FormID, "Del", Me.AppId) Then
                    If SessionInvalid() Then
                        Exit Sub
                    End If

                    Me.MitraID = e.Item.Cells(2).Text

                    Dim customClass As New Parameter.MitraMultifinance
                    With customClass
                        .strConnection = GetConnectionString()
                        .CompanyID = Me.MitraID
                    End With

                    Try
                        m_Mitra.DeleteMitra(customClass)
                        ShowMessage(lblMessage, "Data Berhasil dihapus ", False)

                    Catch ex As Exception
                        ShowMessage(lblMessage, ex.Message, True)

                    Finally
                        BindGridEntity("ALL", "")
                    End Try
                End If

            Case "BranchMitra"
                Me.MitraID = e.Item.Cells(2).Text
                Me.MitraName = e.Item.Cells(5).Text
                Response.Redirect("MitraMultifinanceBranch.aspx?MitraID=" & Me.MitraID & "&MitraName=" & Me.MitraName)

            Case "RekBankMitra"
                Me.MitraID = e.Item.Cells(2).Text
                Me.MitraName = e.Item.Cells(5).Text
                Response.Redirect("MitraMultifinanceAccount.aspx?MitraID=" & Me.MitraID & "&MitraName=" & Me.MitraName)

            Case "PICMitra"
                Me.MitraID = e.Item.Cells(2).Text
                Me.MitraName = e.Item.Cells(5).Text
                Response.Redirect("MitraMultifinancePIC.aspx?MitraID=" & Me.MitraID & "&MitraName=" & Me.MitraName)

        End Select
    End Sub

    Private Sub BtnResetNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnResetNew.Click
        cboSearchBy.SelectedIndex = 0
        TxtSearchByValue.Text = ""
        Me.SearchBy = "ALL"
        Me.SortBy = ""
        PanelAllFalse()
        BindGridEntity("ALL", "")
        pnlList.Visible = True
    End Sub

    Private Sub dtg_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtg.ItemDataBound
        Dim imbDelete As ImageButton
        Dim lbCompanyID As Label
        Dim hyCompanyFullName As LinkButton

        If e.Item.ItemIndex >= 0 Then
            Me.MitraID = e.Item.Cells(2).Text

            lbCompanyID = CType(e.Item.FindControl("lnkCompanyID"), Label)
            hyCompanyFullName = CType(e.Item.FindControl("HyCompanyfullName"), LinkButton)

            hyCompanyFullName.Attributes.Add("OnClick", "return OpenWindowCompany('" & Me.MitraID & "')")

            imbDelete = CType(e.Item.FindControl("ImbDelete"), ImageButton)
            imbDelete.Attributes.Add("onclick", "return fConfirm()")
        End If
    End Sub

    Private Sub BtnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSave.Click
        Select Case Me.ActionAddEdit
            Case "ADD"
                PanelAllFalse()
                Add()

                If Me.isDuplicate = "N" Then
                    BindGridEntity(Me.SearchBy, Me.SortBy)
                    pnlList.Visible = True
                    pnlAddEdit.Visible = False
                Else
                    pnlList.Visible = False
                    pnlAddEdit.Visible = True
                End If

            Case "EDIT"
                PanelAllFalse()
                edit(Me.MitraID.Trim)
                BindGridEntity(Me.SearchBy, Me.SortBy)
                pnlList.Visible = True
        End Select
    End Sub

    Private Function OpenViewCompany(ByVal pCompanyID As String) As String
        Return "javascript:OpenWindowCompany('" & pCompanyID & "')"
    End Function

    Private Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        Response.Redirect("MitraMultifinance.aspx")
    End Sub
End Class