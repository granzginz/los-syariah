﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UploadList.ascx.vb" Inherits="Maxiloan.Webform.PembiayaanMitra.UploadList" %>
<div class="form_box_header">
    <div class="form_single">
        <h5>DAFTAR BATCH
        </h5>
    </div>
</div>
<div class="form_box_header">
    <div class="form_single">
        <div class="grid_wrapper_ns">
            <asp:DataGrid ID="dtg1" runat="server" AutoGenerateColumns="False" BorderWidth="0"
                BorderStyle="None" CssClass="grid_general" DataKeyField="mfbatchno" AllowSorting="True">
                <ItemStyle CssClass="item_grid" />
                <HeaderStyle CssClass="th" />
                <Columns>
                    <asp:TemplateColumn HeaderText="PILIH">
                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        <ItemTemplate>
                            <asp:CheckBox ID="chk" runat="server" Checked="true"></asp:CheckBox>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="PILIH">
                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        <ItemTemplate>
                            <asp:RadioButton ID="rbo" runat="server" Checked="true" ValidationGroup="rbogroup1"></asp:RadioButton>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="PROSES">
                        <ItemStyle CssClass="command_col" />
                        <ItemTemplate>
                            <asp:ImageButton ID="ImbReceive" ImageUrl="../../Images/IconDrawDown.gif" runat="server" CommandName="Receive" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:BoundColumn DataField="mfid" HeaderText="KODE MF"/>
                    <asp:BoundColumn DataField="mffacilityno" HeaderText="NO FASILITAS"/>
                    <asp:BoundColumn DataField="mfbatchno" HeaderText="BATCH NO"/>
                    <asp:BoundColumn DataField="mfbatchdate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="BATCH NO"/>
                    <asp:BoundColumn DataField="total_acc" DataFormatString="{0:N0}" HeaderText="KONTRAK"/>
                    <asp:BoundColumn DataField="total_ntf" DataFormatString="{0:N0}" HeaderText="POKOK"/> 
                    <asp:BoundColumn DataField="interestamount" DataFormatString="{0:N0}" HeaderText="BUNGA"/>                                         
                </Columns>
            </asp:DataGrid>
        </div>
    </div>
</div>
