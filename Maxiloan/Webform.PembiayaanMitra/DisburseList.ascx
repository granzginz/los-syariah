﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="DisburseList.ascx.vb" Inherits="Maxiloan.Webform.PembiayaanMitra.DisburseList" %>

<div class="form_box_header">
    <div class="form_single">
        <h5>DISBURSE LIST
        </h5>
    </div>
</div>
<div class="form_box_header">
    <div class="form_single">
        <div class="grid_wrapper_ns">
            <asp:DataGrid ID="dtg1" runat="server" AutoGenerateColumns="False" BorderWidth="0"
                BorderStyle="None" CssClass="grid_general" DataKeyField="MFFacilityNo" AllowSorting="True">
                <ItemStyle CssClass="item_grid" />
                <HeaderStyle CssClass="th" />
                <Columns>
                    <asp:TemplateColumn HeaderText="PILIH">
                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        <ItemTemplate>
                            <asp:CheckBox ID="chk" runat="server" Checked="true"></asp:CheckBox>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="PILIH">
                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        <ItemTemplate>
                            <asp:RadioButton ID="rbo" runat="server" ValidationGroup="rbogroup1"></asp:RadioButton>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="PROSES">
                        <ItemStyle CssClass="command_col" />
                        <ItemTemplate>
                            <asp:ImageButton ID="ImbReceive" ImageUrl="../../Images/IconDrawDown.gif" runat="server" CommandName="Receive" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:BoundColumn DataField="MFID" HeaderText="KODE MF" />
                    <asp:BoundColumn DataField="MFFacilityNo" HeaderText="FACILITY NO" />
                    <asp:BoundColumn DataField="MFBatchNo" HeaderText="BATCH NO" />

                    <asp:BoundColumn DataField="MFBatchDate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="BATCH DATE" />
                    <asp:BoundColumn DataField="total_acc" DataFormatString="{0:N0}" HeaderText="KONTRAK" />
                    <asp:BoundColumn DataField="total_ntf" DataFormatString="{0:N0}" HeaderText="TOTAL POKOK" />
                    <asp:BoundColumn DataField="interestamount" DataFormatString="{0:N0}" HeaderText="BUNGA" />
                    <asp:BoundColumn DataField="status" HeaderText="STATUS" />
                    <asp:BoundColumn DataField="dateupdate" HeaderText="DATE UPDATE" Visible="false" />
                    <asp:BoundColumn DataField="userupdate" HeaderText="USER UPDATE" Visible="false" />
                </Columns>
            </asp:DataGrid>
        </div>
    </div>
</div>
