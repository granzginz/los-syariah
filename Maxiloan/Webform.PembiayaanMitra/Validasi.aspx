﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Validasi.aspx.vb" Inherits="Maxiloan.Webform.PembiayaanMitra.Validasi" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
</head>
<body>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <asp:UpdatePanel runat="server" ID="updatePanel1">
            <ContentTemplate>
                <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
                <div class="form_title">
                    <div class="title_strip">
                    </div>
                    <div class="form_single">
                        <h3>
                            VALIDASI                  
                        </h3>                    
                    </div>
                </div>
                <asp:Panel ID="pnlHeader" runat="server">
                        <div class="form_box_header">
                            <div class="form_single">
                                <div class="grid_wrapper_ws">
                                    <asp:DataGrid ID="DtgHeader" runat="server" AutoGenerateColumns="False" BorderWidth="0"
                                        OnSortCommand="SortGrid" BorderStyle="None" CssClass="grid_general">
                                        <ItemStyle CssClass="item_grid"></ItemStyle>
                                        <HeaderStyle CssClass="th"></HeaderStyle>
                                        <Columns>
                                            <asp:TemplateColumn HeaderText="VALIDASI">
                                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="ImbPrepayment" runat="server" ImageUrl="../Images/tick_icon.gif"
                                                        CommandName="VALIDATE" CausesValidation="False"></asp:ImageButton>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblMFID" runat="server" Visible="false" Text='<%#Container.DataItem("MFID")%>'></asp:Label>
                                                    <asp:Label ID="lblMFBatchNo" runat="server" Visible="false" Text='<%#Container.DataItem("MFBatchNo")%>'></asp:Label>
                                                    <asp:Label ID="lblMFFacilityNo" runat="server" Visible="false" Text='<%#Container.DataItem("MFFacilityNo")%>'></asp:Label>
                                                    <asp:Label ID="lblMultiFinanceName" runat="server" Visible="false" Text='<%#Container.DataItem("MultiFinanceName")%>'></asp:Label>
                                                    <asp:Label ID="lblRequestDate" runat="server" Visible="false" Text='<%#Container.DataItem("RequestDate")%>' DataFormatString="{0:dd/MM/yyyy}"></asp:Label>
                                                    <asp:Label ID="lblTotalAccount" runat="server" Visible="false" Text='<%#Container.DataItem("TotalAccount")%>'></asp:Label>
                                                    <asp:Label ID="lblTotalAmount" runat="server" Visible="false" Text='<%#Container.DataItem("TotalAmount")%>' DataFormatString="{0:N0}"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>        
                                            <asp:BoundColumn DataField="MFID" HeaderText="MF ID" SortExpression="MFID"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="MFBatchNo" HeaderText="BATCH NO" SortExpression="MFBatchNo"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="MultiFinanceName" HeaderText="MULTI FINANCE" SortExpression="MultiFinanceName"></asp:BoundColumn>                               
                                            <asp:BoundColumn DataField="RequestDate" DataFormatString="{0:dd/MM/yyyy}" HeaderText="TGL. REQUEST" SortExpression="RequestDate"></asp:BoundColumn>   
                                            <asp:BoundColumn DataField="TotalAccount" HeaderText="TOTAL ACCOUNT" SortExpression="TotalAccount"></asp:BoundColumn> 
                                            <asp:BoundColumn DataField="TotalAmount" HeaderText="TOTAL AMOUNT" SortExpression="TotalAmount"></asp:BoundColumn>                                                                                                 
                                        </Columns>
                                    </asp:DataGrid>
                                    <div class="button_gridnavigation">
                                        <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../Images/grid_navbutton01.png"
                                            CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="First">
                                        </asp:ImageButton>
                                        <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../Images/grid_navbutton02.png"
                                            CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Prev">
                                        </asp:ImageButton>
                                        <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../Images/grid_navbutton03.png"
                                            CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Next">
                                        </asp:ImageButton>
                                        <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../Images/grid_navbutton04.png"
                                            CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Last">
                                        </asp:ImageButton>
                                        Page&nbsp;
                                        <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                                        <asp:Button ID="imgbtnPageNumb" runat="server" CssClass="small buttongo blue" Text="Go"
                                            EnableViewState="False"></asp:Button>
                                        <asp:RangeValidator ID="rgvGo" runat="server" ErrorMessage="No Halaman Salah" ControlToValidate="txtPage"
                                            Type="Integer" MaximumValue="999999999" MinimumValue="1" CssClass="validator_general"
                                            Display="Dynamic"></asp:RangeValidator>
                                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" Display="Dynamic"
                                            ErrorMessage="No Halaman Salah" ControlToValidate="txtPage" CssClass="validator_general"></asp:RequiredFieldValidator>
                                    </div>
                         
                                    <asp:Label ID="lblPage" runat="server"></asp:Label>of 
                                    <asp:Label ID="lblTotPage" runat="server"></asp:Label> 
                                    <div class="label_gridnavigation">Total
                                    <asp:Label ID="lblrecord" runat="server"></asp:Label> record(s)
                                </div>                            
                            </div>
                        </div>
                    </div>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
