﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucKontrakDetail.ascx.vb" Inherits="Maxiloan.Webform.PembiayaanMitra.ucKontrakDetail" %>
<div class="form_box_header">
    <div class="form_single">
        <h5> 
            DETAIL KONTRAK
        </h5>
    </div>
</div>
<div class="form_box">
    <div class="form_left">
        <label>No. Agreement</label>
        <asp:label id="txtAgreement" runat="server"/>
    </div>
    <div class="form_right">
        <label>No. MF</label>
        <asp:label id="txtMF" runat="server"/>
    </div>
</div>
<div class="form_box">
    <div class="form_left">
        <label>No. Fasilitas</label>
        <asp:label id="txtFasilitas" runat="server"/>
    </div>
    <div class="form_right">
        <label>No. Batch</label>
        <asp:label id="txtBatch" runat="server"/>
    </div>
</div>
<div class="form_box">
    <div class="form_left">
        <label>Pokok Hutang</label>
        <asp:label id="txtHutang" runat="server"/>
    </div>
    <div class="form_right">
        <label>Tenor</label>
        <asp:label id="txtTenor" runat="server"/>
    </div>
</div>
<div class="form_box">
    <div class="form_left">
        <label>Rate</label>
        <asp:label id="txtRate" runat="server"/>
    </div>
    <div class="form_right">
        <label>Paid Seq</label>
        <asp:label id="txtPaidSeq" runat="server"/>
    </div>
</div>
<div class="form_box">
    <div class="form_left">
        <label>Outstanding Pokok</label>
        <asp:label id="txtPokok" runat="server"/>
    </div>
    <div class="form_right">
        <label>Outstanding Bunga</label>
        <asp:label id="txtBunga" runat="server"/>
    </div>
</div>