﻿#Region "Imports"
'Imports CrystalDecisions.Shared
'Imports CrystalDecisions.CrystalReports.Engine

Imports Maxiloan.Parameter
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
#End Region
Public Class MitraMultifinanceAccount
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents UcBankAccount As UcBankAccount
    Protected WithEvents txtRupiah As ucNumberFormat
    Protected WithEvents oBranch As ucBranchAll
#Region " Private Const "
    Dim m_Mitra As New MitraController

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

#End Region
#Region " Property "

    Public Property MitraID() As String
        Get
            Return CStr(ViewState("MitraID"))
        End Get
        Set(ByVal Value As String)
            ViewState("MitraID") = Value
        End Set
    End Property
    Public Property MitraName() As String
        Get
            Return CStr(ViewState("MitraName"))
        End Get
        Set(ByVal Value As String)
            ViewState("MitraName") = Value
        End Set
    End Property
    Public Property MitraAccountNo() As String
        Get
            Return CStr(ViewState("MitraAccountNo"))
        End Get
        Set(ByVal Value As String)
            ViewState("MitraAccountNo") = Value
        End Set
    End Property
    Public Property ActionAddEdit() As String
        Get
            Return CStr(ViewState("ActionAddEdit"))
        End Get
        Set(ByVal Value As String)
            ViewState("ActionAddEdit") = Value
        End Set
    End Property

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not Me.IsPostBack Then
            Me.FormID = "JFMItra"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If Request.QueryString("MitraID") <> "" Then Me.MitraID = Request.QueryString("MitraID")
                If Request.QueryString("MitraName") <> "" Then Me.MitraName = Request.QueryString("MitraName")
                Me.SearchBy = Me.MitraID.Trim
                Me.SortBy = ""

                lblMitraID.Text = Me.MitraID
                lblMitraName.Text = Me.MitraName
                BindGridEntity(Me.SearchBy, Me.SortBy)
                InitialDefaultPanel()

                Dim lbmitra As LinkButton

                lbmitra = CType(Me.FindControl("lblMitraID"), LinkButton)
                lbmitra.Attributes.Add("OnClick", "return OpenWindowBranch('" & Me.MitraID & "')")

                With UcBankAccount
                    .Style = "Marketing"
                    .BindBankAccount()
                End With

            End If
        End If
    End Sub
#Region " PanelAllFalse "
    Private Sub PanelAllFalse()
        pnlList.Visible = False
        pnlAddEdit.Visible = False
        lblMessage.Visible = False
        pnlpindahcabang.Visible = False
    End Sub
#End Region
#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblTotRec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select

        BindGridEntity(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGoPage.Click

        If txtGoPage.Text = "" Then
            txtGoPage.Text = "0"
        Else
            If IsNumeric(txtGoPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                    currentPage = CType(txtGoPage.Text, Int32)
                    BindGridEntity(Me.SearchBy, Me.SortBy)
                End If
            End If
        End If


    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.SearchBy, Me.SortBy)
    End Sub

#End Region
#Region " InitialDefaultPanel "
    Private Sub InitialDefaultPanel()
        PanelAllFalse()
        lblMessage.Visible = False
        pnlList.Visible = True
        pnlAddEdit.Visible = False
        pnlpindahcabang.Visible = False
    End Sub
#End Region
#Region " BindGridEntity "
    Sub BindGridEntity(ByVal cmdWhere As String, ByVal cmSort As String)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim oEmployee As New Parameter.MitraMultifinance

        With oEmployee
            .strConnection = GetConnectionString()
            .WhereCond = "MfCode ='" & cmdWhere & "'"
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oEmployee = m_Mitra.ListRekeningMitra(oEmployee)

        With oEmployee
            lblTotRec.Text = CType(.TotalRecord, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecord
        End With

        dtsEntity = oEmployee.ListData

        dtvEntity = dtsEntity.DefaultView
        dtvEntity.Sort = Me.SortBy

        If dtvEntity.Count <= 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblMessage.Visible = True
        End If

        dtg.DataSource = dtvEntity
        Try
            dtg.DataBind()
        Catch
            dtg.CurrentPageIndex = 0
            dtg.DataBind()
        End Try

        PagingFooter()
    End Sub
#End Region

    Private Sub ClearAddForm()
        UcBankAccount.BankID = ""
        UcBankAccount.BankBranch = ""
        UcBankAccount.AccountNo = ""
        UcBankAccount.AccountName = ""
    End Sub

    Private Sub BtnAddnew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnAddnew.Click
        If CheckFeature(Me.Loginid, Me.FormID, "Add", Me.AppId) Then
            If SessionInvalid() Then
                Exit Sub
            End If
            ClearAddForm()
            PanelAllFalse()
            BindAdd()
            Me.ActionAddEdit = "ADD"
            lblMenuAddEdit.Text = "ADD"
            pnlAddEdit.Visible = True
        End If
    End Sub
#Region "ADD"
    Private Sub Add()

        Dim customClass As New Parameter.MitraMultifinance

        With customClass
            .strConnection = GetConnectionString()
            .MitraID = Me.MitraID.Trim
            .AccountType = cboAccountType.SelectedItem.Value.Trim
            .BankID = UcBankAccount.BankID
            .BranchCode = UcBankAccount.BankCodeCabang
            .AccountNo = UcBankAccount.AccountNo
            .AccountName = UcBankAccount.AccountName
            .LoginId = Me.Loginid.Trim
        End With

        Try
            m_Mitra.AddRekeningMitra(customClass)
            ShowMessage(lblMessage, "Data Berhasil disimpan ", False)
            pnlAddEdit.Visible = False
            BindGridEntity(Me.SearchBy, Me.SortBy)
            pnlList.Visible = True
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
            pnlList.Visible = False
            pnlAddEdit.Visible = True
        End Try

    End Sub
#End Region
#Region "EDIT"
    Private Sub edit(ByVal Mitraid As String, ByVal AccountMitra As String)

        Dim customClass As New Parameter.MitraMultifinance

        With customClass
            .strConnection = GetConnectionString()
            .MitraID = Me.MitraID.Trim
            .AccountType = cboAccountType.SelectedItem.Value.Trim
            .BankID = UcBankAccount.BankID
            .BranchCode = UcBankAccount.BankCodeCabang
            .AccountNo = UcBankAccount.AccountNo
            .AccountName = UcBankAccount.AccountName
            .LoginId = Me.Loginid.Trim
        End With

        Try
            m_Mitra.EditRekeningMitra(customClass)
            ShowMessage(lblMessage, "Data Berhasil disimpan ", False)
            pnlAddEdit.Visible = False
            BindGridEntity(Me.SearchBy, Me.SortBy)
            pnlList.Visible = True
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
            pnlList.Visible = False
            pnlAddEdit.Visible = True
        End Try
    End Sub

#End Region

    Private Sub viewbyID(ByVal MitrID As String, ByVal AccountNoMf As String)
        Dim oMitra As New Parameter.MitraMultifinance
        Dim dtMitra As New DataTable
        Try
            Dim strConnection As String = GetConnectionString()

            With oMitra
                .strConnection = GetConnectionString()
                .MitraID = MitrID
                .SortBy = ""
                .WhereCond = "MfCode ='" & MitrID & "' AND AccountNo = '" & AccountNoMf & "'"
            End With

            oMitra = m_Mitra.ListRekeningMitraByID(oMitra)
            dtMitra = oMitra.ListData
            cboAccountType.SelectedIndex = cboAccountType.Items.IndexOf(cboAccountType.Items.FindByValue(CStr(dtMitra.Rows(0).Item("AccountType"))))

            UcBankAccount.BankID = dtMitra.Rows(0).Item("BankID")
            UcBankAccount.BankCodeBank = dtMitra.Rows(0).Item("BankCode")
            UcBankAccount.City = dtMitra.Rows(0).Item("BankCity")
            'UcBankAccount.cboBankDisabled()
        Catch ex As Exception
        End Try
    End Sub

    Private Sub dtg_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtg.ItemCommand
        Me.MitraID = e.Item.Cells(2).Text

        Select Case e.CommandName
            Case "Edit"
                If CheckFeature(Me.Loginid, Me.FormID, "Edit", Me.AppId) Then
                    If SessionInvalid() Then
                        Exit Sub
                    End If
                    Me.MitraID = CType(e.Item.FindControl("lblMitraID"), Label).Text.Trim
                    Me.MitraAccountNo = CType(e.Item.FindControl("lblAccountNo"), Label).Text.Trim()
                    pnlList.Visible = False
                    pnlAddEdit.Visible = True
                    lblMessage.Visible = False
                    Me.ActionAddEdit = "EDIT"
                    lblMenuAddEdit.Text = "EDIT"
                    viewbyID(Me.MitraID, Me.MitraAccountNo)

                    With UcBankAccount
                        .BindBankAccount()

                        .BankBranchId = CType(e.Item.FindControl("lblBankBranchID"), Label).Text.Trim
                        .BankName = CType(e.Item.FindControl("lblBankName"), Label).Text.Trim
                        .BankBranch = CType(e.Item.FindControl("lblBankBranchName"), Label).Text.Trim
                        .AccountNo = CType(e.Item.FindControl("lblAccountNo"), Label).Text.Trim
                        .AccountName = CType(e.Item.FindControl("lblAccountName"), Label).Text.Trim
                        .BankID = CType(e.Item.FindControl("lblBankID"), Label).Text.Trim
                    End With

                End If
            Case "Delete"
                If CheckFeature(Me.Loginid, Me.FormID, "Del", Me.AppId) Then
                    If SessionInvalid() Then
                        Exit Sub
                    End If
                    lblMessage.Visible = False

                    Me.MitraID = CType(e.Item.FindControl("lblMitraID"), Label).Text.Trim
                    Me.MitraAccountNo = CType(e.Item.FindControl("lblAccountNo"), Label).Text.Trim()
                    Dim customClass As New Parameter.MitraMultifinance
                    With customClass
                        .strConnection = GetConnectionString()
                        .MitraID = Me.MitraID
                        .AccountNo = Me.MitraAccountNo
                    End With

                    Try
                        m_Mitra.DeleteRekeningMitra(customClass)
                        ShowMessage(lblMessage, "Data Berhasil dihapus ", False)
                    Catch ex As Exception
                        ShowMessage(lblMessage, ex.Message, True)
                    Finally
                        BindGridEntity("ALL", "")
                    End Try
                End If

        End Select
    End Sub
    Private Sub BtnResetnew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnResetnew.Click
        cboSearchBy.SelectedIndex = 0
        TxtSearchByValue.Text = ""
        Me.SearchBy = "ALL"
        Me.SortBy = ""
        PanelAllFalse()
        BindGridEntity("ALL", "")
        pnlList.Visible = True
    End Sub
    Private Sub dtg_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtg.ItemDataBound
        Dim imbDelete As ImageButton
        Dim lbMitraID As LinkButton
        If e.Item.ItemIndex >= 0 Then
            Me.MitraID = CType(e.Item.FindControl("lblMitraID"), Label).Text.Trim
            lbMitraID = CType(e.Item.FindControl("lnkMitraID"), LinkButton)
            lbMitraID.Attributes.Add("OnClick", "return OpenWindowEmployee('" & Me.BranchID & "','" & Me.MitraID & "')")
            imbDelete = CType(e.Item.FindControl("ImbDelete"), ImageButton)
            imbDelete.Attributes.Add("onclick", "return fConfirm()")
        End If
    End Sub
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Select Case Me.ActionAddEdit
            Case "ADD"
                Add()
            Case "EDIT"
                edit(Me.MitraID, Me.MitraAccountNo)
        End Select
    End Sub

    Private Sub BtnSearchnew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSearchnew.Click
        Dim StrSearchBy As String = cboSearchBy.SelectedItem.Value
        Dim StrSearchByValue As String = TxtSearchByValue.Text.Trim

        If StrSearchByValue = "" Then
                Me.SearchBy = "ALL"
                Me.SortBy = ""
            Else
                Me.SearchBy = StrSearchBy + " LIKE '%" + StrSearchByValue + "%'"
            End If

        PanelAllFalse()
        BindGridEntity(Me.SearchBy, Me.SortBy)
        pnlList.Visible = True
        pnlAddEdit.Visible = False
    End Sub
    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        InitialDefaultPanel()
        BindGridEntity(Me.SearchBy, Me.SortBy)
        pnlList.Visible = True
        pnlAddEdit.Visible = False
    End Sub
    Private Sub btnBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("MitraMultifinance.aspx")
    End Sub

    Sub BindAdd()
        UcBankAccount.BindBankAccount()
        UcBankAccount.BankID = ""
        UcBankAccount.BankBranch = ""
        UcBankAccount.AccountNo = ""
        UcBankAccount.AccountName = ""
        UcBankAccount.BankCodeBank = ""
        UcBankAccount.City = ""
    End Sub

End Class