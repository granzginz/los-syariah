﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="JFCriteria.aspx.vb" Inherits="Maxiloan.Webform.PembiayaanMitra.JFCriteria" %>
<%@ Register Src="../../../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %> 

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>CRITERIA</title>
    <link href="../../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
    <script language="JavaScript" type="text/javascript">
        function fBack() {
            history.back(-1);
            return false;
        }
        function DeleteConfirm() {
            if (confirm("Apakah yakin mau hapus data ini ?")) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Label ID="HiddenProductCode" runat ="server" Visible ="false" ></asp:Label> 
    <asp:Panel ID="pnlList" runat="server" Width="100%" HorizontalAlign="center">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    CRITERIA - <asp:Label ID="lblProductCode" runat="server"></asp:Label>
                </h3>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgPaging" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="Sorting" DataKeyField="ComponentId" BorderStyle="None" BorderWidth="0"
                        CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="EDIT">
                                <ItemStyle Width="5%" HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" ImageUrl="../../../Images/iconedit.gif"
                                        CommandName="EDIT"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DELETE">
                                <ItemStyle Width="5%" HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbDelete" runat="server" CausesValidation="False" ImageUrl="../../../Images/iconDelete.gif"
                                        CommandName="DEL"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="COMPONENT" SortExpression="ComponentId">
                                <ItemStyle Width="10%" HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                           <asp:Label ID="lbComponentId" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ComponentId") %>'>
                                        </asp:Label></a>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="Opertaor" SortExpression="Operator" HeaderText="OPERATOR">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="CriteriaValue" SortExpression="CriteriaValue" HeaderText="CRITERIA VALUE">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="Active" SortExpression="Active" HeaderText="ACTIVE">
                            </asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
                <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" ImageUrl="../../../Images/grid_navbutton01.png"
                        OnCommand="NavigationLink_Click" CommandName="First"></asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" ImageUrl="../../../Images/grid_navbutton02.png"
                        OnCommand="NavigationLink_Click" CommandName="Prev"></asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" ImageUrl="../../../Images/grid_navbutton03.png"
                        OnCommand="NavigationLink_Click" CommandName="Next"></asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" ImageUrl="../../../Images/grid_navbutton04.png"
                        OnCommand="NavigationLink_Click" CommandName="Last"></asp:ImageButton>
                    Page&nbsp;
                    <asp:TextBox ID="txtgoPage" runat="server" Width="34px" >1</asp:TextBox>
                    <asp:Button ID="BtnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                        EnableViewState="False"></asp:Button>
                    <asp:RangeValidator ID="rgvGo" Style="z-index: 101" runat="server" ControlToValidate="txtGoPage"
                        Type="integer" MinimumValue="1" ErrorMessage="No Halaman Salah" CssClass="validator_general" ></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtGopage"
                        ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general" ></asp:RequiredFieldValidator>
                </div>
                <div class="label_gridnavigation">
                    <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                    <asp:Label ID="lbltotrec" runat="server"></asp:Label>&nbsp;record(s)
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BackToMenu" runat="server" Enabled="true" Text="Back" CssClass="small button blue">
            </asp:Button>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    CARI CRITERIA
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cari berdasarkan</label>
                <asp:DropDownList ID="cboSearch" runat="server">
                    <asp:ListItem Value="ComponentId">Component</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearch" runat="server" Width="30%"  MaxLength="100"></asp:TextBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="BtnSearch" runat="server" CausesValidation="False" Text="Find" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlAddEdit" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    CRITERIA -&nbsp;
                    <asp:Label ID="lblTitleAddEdit" runat="server"></asp:Label>
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class ="label_req">Component</label>
                <asp:label ID ="lblEditCboCriteria" runat ="server" ></asp:label>
                    <asp:DropDownList ID="cboComponent" runat="server" CssClass="select">
                        <asp:ListItem Value ="">-Select One-</asp:ListItem>
                        <asp:ListItem Value ="AGE">Age</asp:ListItem>
                        <asp:ListItem Value ="ASSETAGE">Asset Age</asp:ListItem>
                        <asp:ListItem Value ="DPDNTNO">Dependent Number</asp:ListItem>
                        <asp:ListItem Value ="EDUC">Education</asp:ListItem>
                        <asp:ListItem Value ="HOMESTS">Home Status</asp:ListItem>
                        <asp:ListItem Value ="MRTLSTS">Marital Status</asp:ListItem>
                        <asp:ListItem Value ="PROFESSION">Profession/Job</asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Harap pilih Component"
                            ControlToValidate="cboComponent" Display="Dynamic" InitialValue="Select One" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>Operator</label>
                <%--<asp:label ID ="Label1" runat ="server" ></asp:label>--%>
                    <asp:DropDownList ID="cboOperator" runat="server" CssClass="select">
                        <asp:ListItem Value ="">-Select One-</asp:ListItem>
                        <asp:ListItem Value ="EQ">Equal</asp:ListItem>
                        <asp:ListItem Value ="NotEq">Not Equal</asp:ListItem>
                        <asp:ListItem Value ="GT">Greather Than</asp:ListItem>
                        <asp:ListItem Value ="GTE">Greather Than or Equal To</asp:ListItem>
                        <asp:ListItem Value ="LT">Less Than</asp:ListItem>
                        <asp:ListItem Value ="LTE">Less Than or Equal To</asp:ListItem>
                        <asp:ListItem Value ="SI">Select In</asp:ListItem>
                        <asp:ListItem Value ="SNI">Select Not In</asp:ListItem>
                        <asp:ListItem Value ="LIKE">Like</asp:ListItem>
                        <asp:ListItem Value ="NL">Not Like</asp:ListItem>
                    </asp:DropDownList>
                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Harap pilih Component"
                            ControlToValidate="cboComponent" Display="Dynamic" InitialValue="Select One" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    Criteria Value
                </label>
                <asp:TextBox ID="txtCriteriaValue" runat="server" TextMode="MultiLine" CssClass="multiline_textbox"></asp:TextBox>        
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Display="Dynamic"
                    ErrorMessage="Harap isi Criteria Value" ControlToValidate="txtCriteriaValue" CssClass ="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_general">Active</label>
                  <asp:RadioButtonList ID="rboActive" runat="server" RepeatDirection="Horizontal"
                      CssClass="opt_single">
                      <asp:ListItem Value="False">No</asp:ListItem>
                      <asp:ListItem Value="True">Yes</asp:ListItem>
                  </asp:RadioButtonList>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="BtnSave" runat="server" CausesValidation="true" Text="Save" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtnBack" runat="server" CausesValidation="false" Text="Back" CssClass="small button gray">
            </asp:Button>
            <asp:Button ID="BtnCancel" runat="server" CausesValidation="false" Text="Cancel"
                CssClass="small button gray"></asp:Button>
            <asp:Button ID="BtnClose" runat="server" CausesValidation="False" Text="Close" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
