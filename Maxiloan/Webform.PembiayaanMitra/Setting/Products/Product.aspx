﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Product.aspx.vb"
    Inherits="Maxiloan.Webform.PembiayaanMitra.Product" %>
<%@ Register Src="../../../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %>  
<%@ Register Src="../../../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat" TagPrefix="uc1" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PRODUCT</title>
    <link href="../../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
    <script language="JavaScript" type="text/javascript">
        function fBack() {
            history.back(-1);
            return false;
        }
        function DeleteConfirm() {
            if (confirm("Apakah yakin mau hapus data ini ?")) {
                return true;
            }
            else {
                return false;
            }
        }
       
    
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlList" runat="server" Width="100%" HorizontalAlign="center">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    DAFTAR PRODUCT
                </h3>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtg" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="Sorting" DataKeyField="ProductCode" BorderStyle="None" BorderWidth="0"
                        CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="EDIT">
                                <ItemStyle Width="5%" HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" ImageUrl="../../../Images/iconedit.gif"
                                        CommandName="EDIT"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DELETE">
                                <ItemStyle Width="5%" HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbDelete" runat="server" CausesValidation="False" ImageUrl="../../../Images/iconDelete.gif"
                                        CommandName="DEL"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="PRODUCT CODE" SortExpression="ProductCode">
                                <ItemStyle Width="10%" HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                   <asp:Label ID="lbProductCode" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ProductCode") %>'>
                                   </asp:Label></a>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="InitialName" SortExpression="InitialName" HeaderText="INITIAL NAME">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="ProductName" SortExpression="ProductName" HeaderText="PRODUCT NAME">
                            </asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="DOCUMENT">
                                <ItemStyle Width="10%" HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                   <%--<a href="../Document/JFDocument.aspx"><asp:label ID="lbldocument" CommandName ="Document" runat="server" >DOCUMENT</asp:label></a>--%>
                                    <asp:LinkButton ID="lbldocument" runat="server" CommandName ="Document" Text ="Document"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="CRITERIA">
                                <ItemStyle Width="10%" HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                   <%--<a href="../Criteria/JFCriteria.aspx" ><asp:label ID="lblcriteria" runat="server" >CRITERIA</asp:label></a>--%>
                                    <asp:LinkButton ID="lblcriteria" runat="server" CommandName ="Criteria" Text ="Criteria"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
                <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" ImageUrl="../../../Images/grid_navbutton01.png"
                        OnCommand="NavigationLink_Click" CommandName="First"></asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" ImageUrl="../../../Images/grid_navbutton02.png"
                        OnCommand="NavigationLink_Click" CommandName="Prev"></asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" ImageUrl="../../../Images/grid_navbutton03.png"
                        OnCommand="NavigationLink_Click" CommandName="Next"></asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" ImageUrl="../../../Images/grid_navbutton04.png"
                        OnCommand="NavigationLink_Click" CommandName="Last"></asp:ImageButton>
                    Page&nbsp;
                    <asp:TextBox ID="txtgoPage" runat="server" Width="34px" >1</asp:TextBox>
                    <asp:Button ID="BtnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                        EnableViewState="False"></asp:Button>
                    <asp:RangeValidator ID="rgvGo" Style="z-index: 101" runat="server" ControlToValidate="txtGoPage"
                        Type="integer" MinimumValue="1" ErrorMessage="No Halaman Salah" CssClass="validator_general" ></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtGopage"
                        ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general" ></asp:RequiredFieldValidator>
                </div>
                <div class="label_gridnavigation">
                    <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                    <asp:Label ID="lbltotrec" runat="server"></asp:Label>&nbsp;record(s)
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnAddNew" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue">
            </asp:Button>
<%--            <asp:Button ID="btnPrint" runat="server" Enabled="true" Text="Print" CssClass="small button blue">
            </asp:Button>--%>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    CARI PRODUCT
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cari berdasarkan</label>
                <asp:DropDownList ID="cboSearch" runat="server">
                    <asp:ListItem Value="ProductCode">Product Code</asp:ListItem>
                    <asp:ListItem Value="InitialName">Initial Name</asp:ListItem>
                    <asp:ListItem Value ="ProductName">Product Name</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearch" runat="server" Width="30%"  MaxLength="100"></asp:TextBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="BtnSearch" runat="server" CausesValidation="False" Text="Find" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlAddEdit" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DAFTAR PRODUCT -&nbsp;
                    <asp:Label ID="lblMenuAddEdit" runat="server"></asp:Label>
                </h4>
            </div>
        </div>
        <div class="form_title">
            <div class="form_left">
                <h4>
                    PRODUCT INFORMATION
                    <asp:Label ID="Label1" runat="server"></asp:Label>
                </h4>
            </div>
            <div class="form_right">
                <h4>
                    PRODUCT ATTRIBUTES
                    <asp:Label ID="Label2" runat="server"></asp:Label>
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label class ="label_req">
                    ProductCode</label>
                <asp:TextBox ID="txtProductCode" runat="server" Width="15%"  MaxLength="100"
                    Columns="105"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" ErrorMessage="Harap Isi ProductCode"
                    ControlToValidate="txtProductCode" CssClass="validator_general" ></asp:RequiredFieldValidator>
             </div>
            <asp:Panel runat ="server" id ="panel1">
                <div class ="form_right">
                    <label >
                        Provision Fee
                    </label>
                    <uc1:ucnumberformat runat="server" id="ucProvisionFee" TextCssClass="numberAlign reguler_text" onclientchange="recalculateAngs()" ></uc1:ucnumberformat>
                    <label >
                        %
                    </label>
                </div>
            </asp:Panel>
         </div> 
         <div class="form_box">
            <div class="form_left">
                <label class ="label_req">
                    InitialName</label>
                <asp:TextBox ID="txtInitialName" runat="server" Width="10%"  MaxLength="100"
                        Columns="105"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator3" runat="server" ErrorMessage="Harap Isi InitialName"
                        ControlToValidate="txtInitialName" CssClass="validator_general" ></asp:RequiredFieldValidator>
            </div>
             <asp:Panel runat ="server" id ="panel2">
                <div class ="form_right">
                    <label >
                        Service Fee
                    </label>
                    <uc1:ucnumberformat runat="server" id="ucServiceFee" TextCssClass="numberAlign reguler_text" onclientchange="recalculateAngs()" ></uc1:ucnumberformat>
                    <label >
                        %
                    </label>
                </div>
             </asp:Panel>
        </div> 
        <div class="form_box">
            <div class="form_left">
                <label class ="label_req">
                    Product Name</label>
                <asp:TextBox ID="txtProductName" runat="server" Width="25%"  MaxLength="100"
                    Columns="105"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator4" runat="server" ErrorMessage="Harap Isi ProductName"
                    ControlToValidate="txtProductName" CssClass="validator_general" ></asp:RequiredFieldValidator>
            </div>
             <asp:Panel runat ="server" id ="panel3">
                <div class ="form_right">
                    <label >
                        Admin Fee
                    </label>
                    <uc1:ucnumberformat runat="server" id="ucAdminFee" TextCssClass="numberAlign reguler_text" onclientchange="recalculateAngs()" ></uc1:ucnumberformat>
                    <label >
                        /Contract
                    </label>
                </div>
             </asp:Panel>
        </div>
        <div class="form_box">
           <div class="form_left">
                <label class="label_req">
                   Asset Type
                </label>
                <asp:DropDownList ID="cboAssetType" runat="server" CssClass="select">
                    <asp:ListItem>MOBIL</asp:ListItem>
                    <asp:ListItem>AGRICULTURE</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Harap pilih Asset Type"
                ControlToValidate="cboAssetType" Display="Dynamic" InitialValue="Select One" CssClass="validator_general"></asp:RequiredFieldValidator>
           </div>
             <asp:Panel runat ="server" id ="panel4">
                <div class ="form_right">
                    <label >
                        Collateral Mgt Fee
                    </label>
                    <uc1:ucnumberformat runat="server" id="ucCollateralMgt" TextCssClass="numberAlign reguler_text" onclientchange="recalculateAngs()" ></uc1:ucnumberformat>
                    <label >
                        /Contract
                    </label>
                </div>
             </asp:Panel>
        </div>
        <div class="form_box">
           <div class="form_left">
                <label class="label_req">
                   Disbursement Approval
                </label>
                <asp:DropDownList ID="cboDisbursementApp" runat="server" CssClass="select">
                    <asp:ListItem Value ="DISB">DISBURSEMENT APPROVAL</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Harap pilih Disbursement Approval"
                ControlToValidate="cboDisbursementApp" Display="Dynamic" InitialValue="Select One" CssClass="validator_general"></asp:RequiredFieldValidator>
           </div>
             <asp:Panel runat ="server" id ="panel5">
                <div class ="form_right">
                    <label >
                        Prepayment Penalty
                    </label>
                    <uc1:ucnumberformat runat="server" id="ucPrepaymentFixAmount" TextCssClass="numberAlign reguler_text" onclientchange="recalculateAngs()" ></uc1:ucnumberformat>
                    <label >
                        Fix Amount
                    </label>
                </div>
             </asp:Panel>
        </div>
        <div class="form_box">
           <div class="form_left">
                <label class="label_req">
                   Journal Scheme
                </label>
                <asp:DropDownList ID="cboJournalScheme" runat="server" CssClass="select">
                    <asp:ListItem value ="DISBDWN">Disburse Drawdown Journal</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Harap pilih Journal Scheme"
                ControlToValidate="cboJournalScheme" Display="Dynamic" InitialValue="Select One" CssClass="validator_general"></asp:RequiredFieldValidator>
           </div>
             <asp:Panel runat ="server" id ="panel6">
                <div class ="form_right">
                    <label >
                        Prepayment Penalty
                    </label>
                    <uc1:ucnumberformat runat="server" id="ucPrepaymentOS" TextCssClass="numberAlign reguler_text" onclientchange="recalculateAngs()" ></uc1:ucnumberformat>
                    <label >
                        % O/S Principal
                    </label>
                </div>
             </asp:Panel>
        </div>
        <div class="form_box">
           <div class="form_left">
                <label class="label_req">
                   Risk Acceptance Criteria
                </label>
                <asp:DropDownList ID="cboRAC" runat="server" CssClass="select">
                    <asp:ListItem Value ="KKB01">KKB Risk Criteria</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="Harap pilih Risk Acceptance Criteria"
                ControlToValidate="cboRAC" Display="Dynamic" InitialValue="Select One" CssClass="validator_general"></asp:RequiredFieldValidator>
           </div> 
            <div class="form_right">
                 <label >
                        Late Charges
                    </label>
                    <uc1:ucnumberformat runat="server" id="ucLateCharges" TextCssClass="numberAlign reguler_text" onclientchange="recalculateAngs()" ></uc1:ucnumberformat>
                    <label >
                        %
                    </label>
            </div> 
        </div> 
         <div class="form_box">
             <div class ="form_left"></div>
                <div class ="form_right">
                    <label >
                        Late Charges Grace Period
                    </label>
                    <asp:TextBox ID="txtLateChargesGracePeriod" runat="server" Width="15%"  MaxLength="100"
                        Columns="105" onkeypress="return numbersonly2(event)"></asp:TextBox>
                    <label >
                        Days
                    </label>
                </div>
        </div>
         <div class="form_box">
             <div class ="form_left"></div>
                <div class ="form_right">
                    <label >
                        Payment Priority
                    </label>
                        <asp:DropDownList ID="cboPaymentPriority" runat="server" CssClass="select">
                            <asp:ListItem selected ="True"  value ="">-Select One-</asp:ListItem>
                            <asp:ListItem value ="IPL">Interest-Principal-Late Charges</asp:ListItem>
                            <asp:ListItem value ="ILP">Interest-Late Charges-Principal</asp:ListItem>
                            <asp:ListItem value ="PIL">Principal-Interest-Late Charges</asp:ListItem>
                            <asp:ListItem value ="PLI">Principal-Late Charges-Interest</asp:ListItem>
                            <asp:ListItem value ="LIP">Late Charges-Interest-Principal</asp:ListItem>
                            <asp:ListItem value ="LPI">Late Charges-Principal-Interest</asp:ListItem>
                        </asp:DropDownList>
                </div>
        </div>
        <div class="form_box">
             <div class ="form_left"></div>
                <div class ="form_right">
                    <label class="label_general">
                        Accrued Interest Calculation
                    </label>
                        <asp:RadioButtonList ID="rboAccrued" runat="server" RepeatDirection="Horizontal"
                            CssClass="opt_single">
                            <asp:ListItem Value="A">Actual</asp:ListItem>
                            <asp:ListItem Selected ="True" Value="F">Fixed(360)</asp:ListItem>
                        </asp:RadioButtonList>
                </div> 
        </div>

        <div class="form_button">
            <asp:Button ID="BtnSave" runat="server" CausesValidation="true" Text="Save" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtnBack" runat="server" CausesValidation="false" Text="Back" CssClass="small button gray">
            </asp:Button>
            <asp:Button ID="BtnCancel" runat="server" CausesValidation="false" Text="Cancel"
                CssClass="small button gray"></asp:Button>
            <asp:Button ID="BtnClose" runat="server" CausesValidation="False" Text="Close" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>

