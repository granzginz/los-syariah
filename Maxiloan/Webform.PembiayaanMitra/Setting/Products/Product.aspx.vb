﻿#Region "Imports"
Imports Maxiloan.cbse
Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Parameter
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller

#End Region
Public Class Product
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents ucProvisionFee As ucNumberFormat
    Protected WithEvents ucAdminFee As ucNumberFormat
    Protected WithEvents ucServiceFee As ucNumberFormat
    Protected WithEvents ucCollateralMgt As ucNumberFormat
    Protected WithEvents ucPrepaymentFixAmount As ucNumberFormat
    Protected WithEvents ucPrepaymentOS As ucNumberFormat
    Protected WithEvents ucLateCharges As ucNumberFormat
#Region " Private Const "
    Dim m_Product As New JFProductController

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

#End Region
#Region " Property "
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property AddEdit() As String
        Get
            Return CType(ViewState("vwsAddEdt"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsAddEdt") = Value
        End Set
    End Property
    Private Property SortBy() As String
        Get
            Return CType(ViewState("SortBy"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("SortBy") = Value
        End Set
    End Property
#End Region

#Region "Load_2"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not Me.IsPostBack Then
            lblMessage.Text = ""
            If CheckForm(Me.Loginid, "JFProduct", "MAXILOAN") Then
                txtgoPage.Text = "1"
                Me.SortBy = "ProductCode ASC"
                If Request("cond") <> "" Then
                    Me.CmdWhere = Request("cond")
                Else
                    Me.CmdWhere = "ALL"

                End If
                BindGridEntity(Me.CmdWhere, Me.SortBy)

                If Request("cmd") = "dtl" Then
                    If CheckFeature(Me.Loginid, "JFProduct", "View", "MAXILOAN") Then
                        If SessionInvalid() Then
                            Exit Sub
                        End If
                    End If
                    BindDetail(Request("ProductCode"), Request("ProductName"))
                End If
                'BtnClose.Attributes.Add("OnClick", "return fClose()")
            End If
        End If

    End Sub
#End Region
#Region " PanelAllFalse "
    Private Sub PanelAllFalse()
        pnlList.Visible = False
        pnlAddEdit.Visible = False
        lblMessage.Visible = False
    End Sub
#End Region
#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lbltotrec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select

        BindGridEntity(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnGoPage.Click

        If txtgoPage.Text = "" Then
            txtgoPage.Text = "0"
        Else
            If IsNumeric(txtgoPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And CType(txtgoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                    currentPage = CType(txtgoPage.Text, Int32)
                    BindGridEntity(Me.SearchBy, Me.SortBy)
                End If
            End If
        End If


    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        If Me.SearchBy = "" Then
            Me.SearchBy = "ALL"
        End If
        BindGridEntity(Me.SearchBy, Me.SortBy)
    End Sub

#End Region
#Region " InitialDefaultPanel "
    Private Sub InitialDefaultPanel()
        pnlList.Visible = True
        pnlAddEdit.Visible = False
    End Sub
#End Region
#Region "BindGrid_2"
    Sub BindGridEntity(ByVal cmdWhere As String, ByVal cmSort As String)
        Dim dtEntity As DataTable = Nothing
        Dim oProduct As New Parameter.JFProduct

        InitialDefaultPanel()
        oProduct.strConnection = GetConnectionString()
        oProduct.WhereCond = cmdWhere
        oProduct.CurrentPage = currentPage
        oProduct.PageSize = pageSize
        oProduct.SortBy = cmSort
        oProduct = m_Product.GetJFProduct(oProduct)

        If Not oProduct Is Nothing Then
            dtEntity = oProduct.ListData
            recordCount = oProduct.TotalRecord
        Else
            recordCount = 0
        End If
        If recordCount = 0 Then
            'btnPrint.Enabled = False
        Else
            'btnPrint.Enabled = True
        End If
        dtg.DataSource = dtEntity.DefaultView
        dtg.CurrentPageIndex = 0
        dtg.DataBind()
        PagingFooter()
    End Sub
#End Region
#Region "dtg_itemdatabound"
    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtg.ItemDataBound
        Dim imbDelete As ImageButton
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imbDelete"), ImageButton)
            imbDelete.Attributes.Add("Onclick", "return DeleteConfirm()")
        End If
    End Sub
#End Region
#Region "BindDetail"
    Sub BindDetail(ByVal ProductCode As String, ByVal Desc As String)
        Dim oProduct As New Parameter.JFProduct
        Me.AddEdit = "VIEW"
        pnlAddEdit.Visible = True
        pnlList.Visible = False
        If Request("cmd") = "close" Then
            BtnBack.Visible = False
            BtnClose.Visible = True
        Else
            BtnBack.Visible = True
            BtnClose.Visible = False
        End If
        BtnCancel.Visible = False
        BtnSave.Visible = False

        lblMenuAddEdit.Text = Me.AddEdit
        oProduct.ProductCode = ProductCode
        oProduct.strConnection = GetConnectionString()
        oProduct = m_Product.GetJFProduct(oProduct)

        'lblID.Visible = True
        txtProductCode.Visible = False
        'lblNPP.Visible = True
        txtProductName.Visible = False


        'lblID.Text = ID
        'lblNPP.Text = Desc
    End Sub
#End Region

#Region "ClearAddForm"
    '    Private Sub ClearAddForm()

    '        txtID.Text = ""
    '        txtName.Text = ""
    '        txtInitialName.Text = ""
    '        cboCompany.SelectedIndex = 0
    '        cboArea.SelectedIndex = 0

    '        lblID.Visible = False
    '        txtID.Visible = True

    '        With UcBranchAddress
    '            .Address = ""
    '            .RT = ""
    '            .RW = ""
    '            .Kecamatan = ""
    '            .Kelurahan = ""
    '            .City = ""
    '            .ZipCode = ""
    '            .AreaPhone1 = ""
    '            .Phone1 = ""
    '            .AreaPhone2 = ""
    '            .Phone2 = ""
    '            .AreaFax = ""
    '            .Fax = ""
    '            .Style = "Setting"
    '            .BindAddress()
    '        End With

    '        With UcContactPerson
    '            .ContactPerson = ""
    '            .ContactPersonTitle = ""
    '            .MobilePhone = ""
    '            .Email = ""
    '            .EnabledContactPerson()
    '            .BindContacPerson()
    '        End With

    '        txtBranchManager.Text = ""
    '        txtADh.Text = ""
    '        txtARControlName.Text = ""
    '        txtProductivityValue.Text = "0"
    '        txtProductivityCollection.Text = "0"
    '        txtNumOfEmployee.Text = "0"

    '        cboAOCard.SelectedIndex = 0
    '    End Sub
#End Region
#Region "BtnAdd"
    Private Sub BtnAddnew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddNew.Click
        If CheckFeature(Me.Loginid, "JFProduct", "Add", "MAXILOAN") Then
            If SessionInvalid() Then
                Exit Sub
            End If
        End If
        'ClearAddForm()
        pnlAddEdit.Visible = True
        pnlList.Visible = False
        BtnBack.Visible = False
        BtnCancel.Visible = True
        BtnSave.Visible = True
        BtnClose.Visible = False

        Me.AddEdit = "ADD"
        lblMenuAddEdit.Text = Me.AddEdit
    End Sub
#End Region
#Region "ADD"
    Private Sub Add()

        Dim customClass As New Parameter.JFProduct
        'Dim oClassAddress As New Parameter.Address
        'Dim oClassPersonal As New Parameter.Personal

        With customClass
            .strConnection = GetConnectionString()
            .ProductCode = txtProductCode.Text.Trim
            .ProductName = txtProductName.Text.Trim
            .InitialName = txtInitialName.Text.Trim
            .AssetTypeCode = cboAssetType.Text
        End With
    End Sub
#End Region

#Region "EDIT"
    '    Private Sub edit(ByVal Branchid As String)

    '        Dim customClass As New Parameter.Branch
    '        Dim oClassAddress As New Parameter.Address
    '        Dim oClassPersonal As New Parameter.Personal

    '        With customClass
    '            .strConnection = GetConnectionString()
    '            .BranchId = lblID.Text.Trim
    '            .BranchInitialName = txtInitialName.Text.Trim
    '            .BranchFullName = txtName.Text.Trim

    '            .CompanyID = cboCompany.SelectedItem.Value
    '            .AreaID = cboArea.SelectedItem.Value

    '            .BranchManager = txtBranchManager.Text.Trim
    '            .ADH = txtADh.Text.Trim
    '            .ARControlName = txtARControlName.Text
    '            .BranchStatus = rbBranchStatus.SelectedItem.Value
    '            .ProductivityValue = CDbl(txtProductivityValue.Text.Trim)
    '            .ProductivityCollection = CDbl(txtProductivityCollection.Text.Trim)
    '            .EmployeeNumber = CInt(txtNumOfEmployee.Text.Trim)
    '            .AOCard = cboAOCard.SelectedItem.Value
    '            .Custodian = txtCustodian.Text.Trim
    '            .STNKPICName = txtSTNKPICName.Text
    '        End With

    '        With oClassAddress
    '            .Address = UcBranchAddress.Address
    '            .RT = UcBranchAddress.RT
    '            .RW = UcBranchAddress.RW
    '            .Kelurahan = UcBranchAddress.Kelurahan
    '            .Kecamatan = UcBranchAddress.Kecamatan
    '            .City = UcBranchAddress.City
    '            .ZipCode = UcBranchAddress.ZipCode
    '            .AreaPhone1 = UcBranchAddress.AreaPhone1.Trim
    '            .Phone1 = UcBranchAddress.Phone1.Trim
    '            .AreaPhone2 = UcBranchAddress.AreaPhone1.Trim
    '            .Phone2 = UcBranchAddress.Phone2.Trim
    '            .AreaFax = UcBranchAddress.AreaFax.Trim
    '            .Fax = UcBranchAddress.Fax.Trim
    '        End With

    '        With oClassPersonal
    '            .PersonName = UcContactPerson.ContactPerson
    '            .PersonTitle = UcContactPerson.ContactPersonTitle
    '            .MobilePhone = UcContactPerson.MobilePhone
    '            .Email = UcContactPerson.Email
    '        End With


    '        Try
    '            m_Branch.BranchEdit(customClass, oClassAddress, oClassPersonal)
    '            ShowMessage(lblMessage, "Data Berhasil diupdate ", False)
    '            pnlList.Visible = True
    '            pnlAddEdit.Visible = False
    '            BindGridEntity(Me.SearchBy, Me.SortBy)
    '            Me.Cache.Remove(CACHE_BRANCH_COLLECTION & Me.GroubDbID)
    '            'MsgBox(MessageHelper.MESSAGE_INSERT_SUCCESS)
    '        Catch ex As Exception
    '            ShowMessage(lblMessage, ex.Message, True)
    '            pnlList.Visible = False
    '            pnlAddEdit.Visible = True
    '        End Try
    '    End Sub
#End Region
#Region "viewbyID"
    '    Private Sub viewbyID(ByVal BranchID As String)
    '        Dim oBranch As New Parameter.Branch
    '        Dim dtBranch As New DataTable

    '        Try
    '            Dim strConnection As String = GetConnectionString()

    '            With oBranch
    '                .strConnection = GetConnectionString()
    '                .BranchId = BranchID

    '            End With

    '            oBranch = m_Branch.BranchByID(oBranch)
    '            dtBranch = oBranch.ListData

    '            '------------------
    '            'isi dari Database
    '            '------------------
    '            txtID.Visible = False
    '            lblID.Visible = True
    '            lblID.Text = Me.BranchID
    '            txtName.Text = CStr(dtBranch.Rows(0).Item("BranchName")).Trim
    '            txtInitialName.Text = CStr(dtBranch.Rows(0).Item("BranchInitialName")).Trim
    '            cboCompany.SelectedIndex = cboCompany.Items.IndexOf(cboCompany.Items.FindByValue(CStr(dtBranch.Rows(0).Item("CompanyID"))))
    '            cboArea.SelectedIndex = cboArea.Items.IndexOf(cboArea.Items.FindByValue(CStr(dtBranch.Rows(0).Item("AreaID"))))

    '            With UcBranchAddress
    '                .Address = CStr(dtBranch.Rows(0).Item("Address")).Trim
    '                .RT = CStr(dtBranch.Rows(0).Item("RT")).Trim
    '                .RW = CStr(dtBranch.Rows(0).Item("RW")).Trim
    '                .Kelurahan = CStr(dtBranch.Rows(0).Item("Kelurahan")).Trim
    '                .Kecamatan = CStr(dtBranch.Rows(0).Item("Kecamatan")).Trim
    '                .City = CStr(dtBranch.Rows(0).Item("City")).Trim
    '                .ZipCode = CStr(dtBranch.Rows(0).Item("ZipCode")).Trim
    '                .AreaPhone1 = CStr(dtBranch.Rows(0).Item("AreaPhone1")).Trim
    '                .Phone1 = CStr(dtBranch.Rows(0).Item("Phone1")).Trim
    '                .AreaPhone2 = CStr(dtBranch.Rows(0).Item("AreaPhone2")).Trim
    '                .Phone2 = CStr(dtBranch.Rows(0).Item("Phone2")).Trim
    '                .AreaFax = CStr(dtBranch.Rows(0).Item("AreaFax")).Trim
    '                .Fax = CStr(dtBranch.Rows(0).Item("Fax")).Trim
    '                .Style = "Setting"
    '                .BindAddress()
    '            End With

    '            With UcContactPerson
    '                .ContactPerson = CStr(dtBranch.Rows(0).Item("ContactPersonName")).Trim
    '                .ContactPersonTitle = CStr(dtBranch.Rows(0).Item("ContactPersonTitle")).Trim
    '                .MobilePhone = CStr(dtBranch.Rows(0).Item("ContactPersonHP")).Trim
    '                .Email = CStr(dtBranch.Rows(0).Item("ContactPersonEmail")).Trim
    '                .EnabledContactPerson()
    '                .BindContacPerson()
    '            End With

    '            txtBranchManager.Text = CStr(dtBranch.Rows(0).Item("BranchManager")).Trim
    '            txtADh.Text = CStr(dtBranch.Rows(0).Item("ADh")).Trim
    '            txtARControlName.Text = CStr(dtBranch.Rows(0).Item("ARControlName")).Trim

    '            txtProductivityValue.Text = CStr(dtBranch.Rows(0).Item("ProductivityValue")).Trim
    '            txtProductivityCollection.Text = CStr(dtBranch.Rows(0).Item("ProductivityCollection")).Trim
    '            txtCustodian.Text = CStr(dtBranch.Rows(0).Item("AssetDocCustodianName")).Trim
    '            txtSTNKPICName.Text = CStr(dtBranch.Rows(0).Item("STNKPICName")).Trim

    '            cboAOCard.SelectedIndex = cboAOCard.Items.IndexOf(cboAOCard.Items.FindByValue(CStr(dtBranch.Rows(0).Item("AOCard"))))
    '            rbBranchStatus.SelectedValue = dtBranch.Rows(0).Item("BranchStatus")
    '            'MsgBox(MessageHelper.MESSAGE_INSERT_SUCCESS)
    '        Catch ex As Exception
    '            ShowMessage(lblMessage, ex.Message, True)
    '        End Try
    '    End Sub
#End Region
#Region "dtg_ItemCommand"
    Private Sub dtgEntity_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtg.ItemCommand
        Dim err As String
        Dim oProduct As New Parameter.JFProduct
        Dim dtEntity As New DataTable


        If e.CommandName = "EDIT" Then
            If CheckFeature(Me.Loginid, "JFProduct", "EDIT", "MAXILOAN") Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            Me.AddEdit = "EDIT"
            pnlAddEdit.Visible = True
            pnlList.Visible = False
            BtnBack.Visible = False
            BtnCancel.Visible = True
            BtnSave.Visible = True
            BtnClose.Visible = False

            lblMenuAddEdit.Text = Me.AddEdit
            oProduct.ProductCode = dtg.DataKeys.Item(e.Item.ItemIndex).ToString
            oProduct.strConnection = GetConnectionString()
            oProduct = m_Product.GetJFProductEdit(oProduct)
            txtProductCode.Visible = True
            txtProductName.Visible = True


            txtProductCode.Text = CType(e.Item.FindControl("lbProductCode"), Label).Text
            With oProduct
                .ProductCode = txtProductCode.Text
                .strConnection = GetConnectionString()
                .SpName = "spSelectMFProduct"
            End With
            oProduct = m_Product.GetSelectJFProduct(oProduct)
            dtEntity = oProduct.ListData
            txtInitialName.Text = dtEntity.Rows(0)("InitialName")
            txtProductName.Text = dtEntity.Rows(0)("ProductName")
            ucProvisionFee.Text = dtEntity.Rows(0)("ProvisionFee")
            ucServiceFee.Text = dtEntity.Rows(0)("ServiceFee")
            ucAdminFee.Text = dtEntity.Rows(0)("AdminFee")
            ucCollateralMgt.Text = dtEntity.Rows(0)("CollateralManagementFee")
            ucPrepaymentFixAmount.Text = dtEntity.Rows(0)("PrepaymentPenaltyFixed")
            ucPrepaymentOS.Text = dtEntity.Rows(0)("PrepaymentPenaltyRate")
            ucLateCharges.Text = dtEntity.Rows(0)("LateChargesRate")
            txtLateChargesGracePeriod.Text = dtEntity.Rows(0)("LateChargesGracePeriod")
            cboPaymentPriority.SelectedValue = dtEntity.Rows(0)("PaymentPriority")
            rboAccrued.SelectedValue = dtEntity.Rows(0)("AccruedInterestCalculationType")

        ElseIf e.CommandName = "DEL" Then
            If CheckFeature(Me.Loginid, "JFProduct", "DEL", "MAXILOAN") Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            Dim customClass As New Parameter.JFProduct
            With customClass
                .ProductCode = CType(e.Item.FindControl("lbProductCode"), Label).Text
                .strConnection = GetConnectionString()
            End With
            err = m_Product.JFProductDelete(customClass)
            If err <> "" Then
                ShowMessage(lblMessage, err, True)
            Else

                ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, False)
            End If
            BindGridEntity(Me.CmdWhere, Me.SortBy)
            txtgoPage.Text = "1"
        ElseIf e.CommandName = "Document" Then
            Dim PC As Label
            PC = CType(e.Item.FindControl("lbProductCode"), Label)
            Response.Redirect("../Document/JFDocument.aspx?ProductCode=" & PC.Text & "")
        ElseIf e.CommandName = "Criteria" Then
            Dim PCRCA As Label
            PCRCA = CType(e.Item.FindControl("lbProductCode"), Label)
            Response.Redirect("../Criteria/JFCriteria.aspx?ProductCode=" & PCRCA.Text & "")
        End If
    End Sub
#End Region
#Region "BtnReset"

    Private Sub BtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        cboSearch.SelectedIndex = 0
        txtSearch.Text = ""
        Me.CmdWhere = "ALL"
        BindGridEntity(Me.CmdWhere, Me.SortBy)
    End Sub
#End Region
#Region "dtg_ItemDataBound"
    '    Private Sub dtg_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtg.ItemDataBound
    '        Dim imbDelete As ImageButton
    '        Dim lbBranchID As LinkButton

    '        If e.Item.ItemIndex >= 0 Then
    '            Me.BranchID = e.Item.Cells(2).Text

    '            lbBranchID = CType(e.Item.FindControl("lnkBranchID"), LinkButton)
    '            lbBranchID.Attributes.Add("OnClick", "return OpenWindowBranch('" & Me.BranchID & "')")

    '            imbDelete = CType(e.Item.FindControl("ImbDelete"), ImageButton)
    '            imbDelete.Attributes.Add("onclick", "return fConfirm()")
    '        End If
    '    End Sub
#End Region
#Region "imbSave"
    Private Sub BtnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSave.Click
        Dim customClass As New Parameter.JFProduct
        Dim ErrMessage As String = ""
        With customClass
            .ProductCode = txtProductCode.Text
            .ProductName = txtProductName.Text
            .InitialName = txtInitialName.Text
            .AssetTypeCode = cboAssetType.Text
            .DisburseApprovalCode = cboDisbursementApp.Text
            .JournalSchemeCode = cboJournalScheme.Text
            .CriteriaRacCode = cboRAC.Text
            .ProvisionFee = CDec(ucProvisionFee.Text)
            .AdminFee = CDec(ucAdminFee.Text)
            .ServiceFee = CDec(ucServiceFee.Text)
            .CollateralManagementFee = CDec(ucCollateralMgt.Text)
            .PrepaymentPenaltyFixed = CDec(ucPrepaymentFixAmount.Text)
            .PrepaymentPenaltyRate = CDec(ucPrepaymentOS.Text)
            .LateChargesRate = CDec(ucLateCharges.Text)
            .LateChargesGracePeriod = txtLateChargesGracePeriod.Text
            .PaymentPriority = cboPaymentPriority.Text
            .AccruedInterestCalculationType = rboAccrued.SelectedValue
            .CreatedBy = Me.Loginid
            .ChangedBy = Me.Loginid
            .CreatedDate = Date.Now.ToString("yyyy/MM/dd")
            .ChangedDate = Date.Now.ToString("yyyy/MM/dd")
            .strConnection = GetConnectionString()
        End With

        If Me.AddEdit = "ADD" Then
            ErrMessage = m_Product.JFProductAdd(customClass)
            If ErrMessage <> "" Then
                ShowMessage(lblMessage, ErrMessage, True)
                Exit Sub
            Else
                ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_SUCCESS, False)
                BindGridEntity(Me.CmdWhere, Me.SortBy)
            End If
        ElseIf Me.AddEdit = "EDIT" Then
            m_Product.JFProductUpdate(customClass)

            ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
            BindGridEntity(Me.CmdWhere, Me.SortBy)
        End If
    End Sub
#End Region
#Region "imgPrint"
    '    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click
    '        If SessionInvalid() Then
    '            Exit Sub
    '        End If

    '        If CheckFeature(Me.Loginid, Me.FormID, "Print", Me.AppId) Then
    '            Dim cookie As HttpCookie = Request.Cookies(Me.FormID)
    '            If Not cookie Is Nothing Then
    '                cookie.Values("where") = Me.SearchBy
    '                cookie.Values("sortby") = Me.SortBy
    '                Response.AppendCookie(cookie)
    '            Else
    '                Dim cookieNew As New HttpCookie(Me.FormID)
    '                cookieNew.Values.Add("where", Me.SearchBy)
    '                cookieNew.Values.Add("SortBy", Me.SortBy)
    '                Response.AppendCookie(cookieNew)
    '            End If

    '            Response.Redirect("report/Branchreport.aspx")
    '        End If
    '    End Sub
#End Region
#Region "BtnSearch"
    Private Sub BtnSearchnew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSearch.Click
        If txtSearch.Text.Trim <> "" Then
            Me.CmdWhere = cboSearch.SelectedItem.Value + " Like '%" + txtSearch.Text.Trim + "%'"
        Else
            Me.CmdWhere = "ALL"
        End If
        BindGridEntity(Me.CmdWhere, Me.SortBy)
    End Sub
#End Region
#Region "imgCancel"
    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        Response.Redirect("Product.aspx")
    End Sub
#End Region



End Class

