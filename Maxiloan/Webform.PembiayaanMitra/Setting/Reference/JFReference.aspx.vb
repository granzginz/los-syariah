﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.cbse
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
#End Region


Public Class JFReference
    Inherits Maxiloan.Webform.WebBased

    Private m_controller As New JFReferenceController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

#Region " Property "
    Private Property GroupID() As String
        Get
            Return CStr(ViewState("GroupID"))
        End Get
        Set(ByVal Value As String)
            ViewState("GroupID") = Value
        End Set
    End Property

    Private Property ReferenceKey() As String
        Get
            Return CStr(ViewState("ReferenceKey"))
        End Get
        Set(ByVal Value As String)
            ViewState("ReferenceKey") = Value
        End Set
    End Property
    Private Property ReferenceDescription() As String
        Get
            Return CStr(ViewState("ReferenceDescription"))
        End Get
        Set(ByVal Value As String)
            ViewState("ReferenceDescription") = Value
        End Set
    End Property
    Public Property ActionAddEdit() As String
        Get
            Return CStr(ViewState("ActionAddEdit"))
        End Get
        Set(ByVal Value As String)
            ViewState("ActionAddEdit") = Value
        End Set
    End Property

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If SessionInvalid() Then
            Exit Sub
        End If
        If Not Me.IsPostBack Then
            InitialDefaultPanel()
            Me.FormID = "JFReference"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then

                Me.SearchBy = ""
                Me.SortBy = ""

                If Request("cond") <> "" Then
                    Me.SearchBy = Request("cond")
                Else
                    Me.SearchBy = "ALL"
                End If
                Me.SortBy = ""
                BindGridEntity(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub

#Region " PanelAllFalse "
    Private Sub PanelAllFalse()
        pnlList.Visible = False
        PnlAddEdit.Visible = False
        lblMessage.Visible = False
    End Sub
#End Region

#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lbltotrec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridEntity(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGoPage.Click

        If txtgoPage.Text = "" Then
            txtgoPage.Text = "0"
        Else
            If IsNumeric(txtgoPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And CType(txtgoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                    currentPage = CType(txtgoPage.Text, Int32)
                    BindGridEntity(Me.SearchBy, Me.SortBy)
                End If
            End If
        End If


    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.SearchBy, Me.SortBy)
    End Sub

#End Region

#Region " InitialDefaultPanel "
    Private Sub InitialDefaultPanel()
        PanelAllFalse()
        pnlList.Visible = True
        PnlAddEdit.Visible = False
    End Sub
#End Region

#Region " BindGridEntity "
    Sub BindGridEntity(ByVal cmdWhere As String, ByVal cmSort As String)
        Dim dtvEntity As New DataView
        Dim dtsEntity As New DataTable
        Dim oEmployeePosition As New Parameter.JFReference
        Dim oEmployeePositionList As New Parameter.JFReference


        With oEmployeePosition
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With


        oEmployeePositionList = m_controller.JFReferenceList(oEmployeePosition)

        With oEmployeePositionList
            lbltotrec.Text = CType(.TotalRecord, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecord
        End With

        dtsEntity = oEmployeePositionList.listdata
        dtvEntity = dtsEntity.DefaultView
        dtvEntity.Sort = Me.SortBy

        If dtvEntity.Count <= 0 Then

            ShowMessage(lblMessage, "Data tidak ditemukan.....", True)
        End If

        dtg.DataSource = dtvEntity
        Try
            dtg.DataBind()
        Catch
            dtg.CurrentPageIndex = 0
            dtg.DataBind()
        End Try

        PagingFooter()
    End Sub
#End Region

    Private Sub ClearAddForm()
        txtGroupID.Text = ""
        txtReferenceKey.Text = ""
        txtReferenceDescription.Text = ""
        txtGroupID.Visible = True
        txtReferenceKey.Visible = True
        lblGroupID.Visible = False
        lblReferenceKey.Visible = False
    End Sub

    Private Sub viewbyID()
        txtGroupID.Text = Me.GroupID
        txtGroupID.Visible = False
        lblGroupID.Visible = True
        lblGroupID.Text = Me.GroupID
        txtReferenceKey.Text = Me.ReferenceKey
        txtReferenceKey.Visible = False
        lblReferenceKey.Visible = True
        lblReferenceKey.Text = Me.ReferenceKey
        txtReferenceDescription.Text = Me.ReferenceDescription
    End Sub

#Region "ADD"
    Private Sub Add()

        Dim customClass As New Parameter.JFReference

        With customClass
            .strConnection = GetConnectionString()
            .GroupID = txtGroupID.Text.Trim
            .ReferenceKey = txtReferenceKey.Text.Trim
            .ReferenceDescription = txtReferenceDescription.Text.Trim
            .LoginID = Me.Loginid.Trim
        End With

        Try
            m_controller.JFReferenceAdd(customClass)

            ShowMessage(lblMessage, "Data Berhasil disimpan ", False)
            pnlList.Visible = True
            PnlAddEdit.Visible = False
            BindGridEntity(Me.SearchBy, Me.SortBy)
            'MsgBox(MessageHelper.MESSAGE_INSERT_SUCCESS)
        Catch ex As Exception
            ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_FAILED, True)
            pnlList.Visible = False
            PnlAddEdit.Visible = True
        End Try
    End Sub
#End Region

#Region "EDIT"
    Private Sub edit()
        Dim customClass As New Parameter.JFReference

        With customClass
            .strConnection = GetConnectionString()
            .GroupID = txtGroupID.Text.Trim
            .ReferenceKey = txtReferenceKey.Text.Trim
            .ReferenceDescription = txtReferenceDescription.Text.Trim
            .LoginID = Me.Loginid.Trim
        End With

        Try
            m_controller.JFReferenceEdit(customClass)
            ShowMessage(lblMessage, "Data Berhasil disimpan ", False)
            pnlList.Visible = True
            PnlAddEdit.Visible = False
            BindGridEntity(Me.SearchBy, Me.SortBy)
            'MsgBox(MessageHelper.MESSAGE_INSERT_SUCCESS)
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
            pnlList.Visible = False
            PnlAddEdit.Visible = True
        End Try
    End Sub

#End Region

    Private Sub dtg_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtg.ItemCommand
        Select Case e.CommandName

            Case "Edit"
                If CheckFeature(Me.Loginid, Me.FormID, "Edit", Me.AppId) Then
                    If SessionInvalid() Then
                        Exit Sub
                    End If

                    Me.GroupID = e.Item.Cells(2).Text.Trim
                    Me.ReferenceKey = e.Item.Cells(3).Text.Trim
                    Me.ReferenceDescription = e.Item.Cells(4).Text.Trim

                    pnlList.Visible = False
                    PnlAddEdit.Visible = True
                    Me.ActionAddEdit = "EDIT"
                    lblMenuAddEdit.Text = "EDIT"
                    viewbyID()
                End If

            Case "Delete"
                If CheckFeature(Me.Loginid, Me.FormID, "Del", Me.AppId) Then
                    If SessionInvalid() Then
                        Exit Sub
                    End If

                    Me.GroupID = e.Item.Cells(2).Text.Trim
                    Me.ReferenceKey = e.Item.Cells(3).Text.Trim
                    Me.ReferenceDescription = e.Item.Cells(4).Text.Trim

                    Dim customClass As New Parameter.JFReference
                    With customClass
                        .strConnection = GetConnectionString()
                        .GroupID = Me.GroupID
                        .ReferenceKey = Me.ReferenceKey
                    End With

                    Try
                        m_controller.JFReferenceDelete(customClass)
                        ShowMessage(lblMessage, "Data Berhasil dihapus ", False)
                        BindGridEntity(Me.SearchBy, Me.SortBy)

                    Catch ex As Exception
                        ShowMessage(lblMessage, ex.Message, True)


                        'Finally
                        '    BindGridEntity("ALL", "")
                    End Try
                End If


        End Select
    End Sub

    Private Sub dtg_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtg.ItemDataBound
        Dim imbDelete As ImageButton

        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("ImbDelete"), ImageButton)
            imbDelete.Attributes.Add("onclick", "return fConfirm()")
        End If
    End Sub

    Private Sub BtnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSave.Click
        Select Case Me.ActionAddEdit
            Case "ADD"
                Add()
            Case "EDIT"
                edit()
        End Select
    End Sub

    Private Sub BtnAddNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnAddNew.Click
        If CheckFeature(Me.Loginid, Me.FormID, "Add", Me.AppId) Then
            If SessionInvalid() Then
                Exit Sub
            End If

            ClearAddForm()
            PanelAllFalse()
            Me.ActionAddEdit = "ADD"
            lblMenuAddEdit.Text = "ADD"
            PnlAddEdit.Visible = True

        End If
    End Sub

    Private Sub BtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        cboSearchBy.SelectedIndex = 0
        txtSearchBy.Text = ""
        Me.SearchBy = "ALL"
        Me.SortBy = ""
        PanelAllFalse()
        BindGridEntity("ALL", "")
        pnlList.Visible = True
        PnlAddEdit.Visible = False
    End Sub

    Private Sub Btnsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btnsearch.Click
        Dim StrSearchBy As String = cboSearchBy.SelectedItem.Value
        Dim StrSearchByValue As String = txtSearchBy.Text.Trim

        If StrSearchByValue = "" Then
            Me.SearchBy = "all"
            Me.SortBy = ""
        Else
            Me.SearchBy = StrSearchBy + " like '%" + StrSearchByValue + "%'"
        End If
        PanelAllFalse()
        BindGridEntity(Me.SearchBy, Me.SortBy)
        pnlList.Visible = True
    End Sub

    Private Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        Response.Redirect("JFReference.aspx")
    End Sub


End Class