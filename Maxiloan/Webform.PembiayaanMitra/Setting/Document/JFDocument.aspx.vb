﻿#Region "Imports"
Imports Maxiloan.cbse
Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Parameter
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
#End Region

Public Class JFDocument
    Inherits Maxiloan.Webform.WebBased
    'Inherits System.Web.UI.Page
#Region " Private Const "
    Dim m_Document As New JFDocumentController

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

#End Region
#Region " Property "
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property AddEdit() As String
        Get
            Return CType(ViewState("vwsAddEdt"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsAddEdt") = Value
        End Set
    End Property
    Private Property SortBy() As String
        Get
            Return CType(ViewState("SortBy"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("SortBy") = Value
        End Set
    End Property
    Private Property ProductCode() As String
        Get
            Return CType(ViewState("ProductCode"), String)
        End Get
        Set(value As String)
            ViewState("ProductCode") = value
        End Set
    End Property
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not Me.IsPostBack Then
            lblMessage.Text = ""
            If CheckForm(Me.Loginid, "JFDocument", "MAXILOAN") Then
                txtgoPage.Text = "1"
                Me.ProductCode = Request.QueryString("ProductCode")
                lblProductCode.Text = Me.ProductCode
                Me.SortBy = "ProductCode, DocumentCode ASC"
                If Me.ProductCode <> "" Then
                    CmdWhere = String.Format(" ProductCode = '{0}' ", Me.ProductCode)
                End If
                BindGridEntity(Me.CmdWhere, Me.SortBy)

                If Request("cmd") = "dtl" Then
                    If CheckFeature(Me.Loginid, "JFDocument", "View", "MAXILOAN") Then
                        If SessionInvalid() Then
                            Exit Sub
                        End If
                    End If
                    BindDetail(Request("ProductCode"), Request("DocumentCode"))
                End If
                'BtnClose.Attributes.Add("OnClick", "return fClose()")
            End If
        End If
    End Sub
#Region " PanelAllFalse "
    Private Sub PanelAllFalse()
        pnlList.Visible = False
        pnlAddEdit.Visible = False
        lblMessage.Visible = False
    End Sub
#End Region

#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lbltotrec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select

        BindGridEntity(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnGoPage.Click

        If txtgoPage.Text = "" Then
            txtgoPage.Text = "0"
        Else
            If IsNumeric(txtgoPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And CType(txtgoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                    currentPage = CType(txtgoPage.Text, Int32)
                    BindGridEntity(Me.SearchBy, Me.SortBy)
                End If
            End If
        End If


    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        If Me.SearchBy = "" Then
            Me.SearchBy = "ALL"
        End If
        BindGridEntity(Me.SearchBy, Me.SortBy)
    End Sub
#End Region
#Region " InitialDefaultPanel "
    Private Sub InitialDefaultPanel()
        pnlList.Visible = True
        pnlAddEdit.Visible = False
    End Sub
#End Region
#Region "BindGrid_2"
    Sub BindGridEntity(ByVal cmdWhere As String, ByVal cmSort As String)
        Dim dtEntity As DataTable = Nothing
        Dim oDocument As New Parameter.JFDocument
        InitialDefaultPanel()
        oDocument.strConnection = GetConnectionString()
        oDocument.WhereCond = cmdWhere
        oDocument.CurrentPage = currentPage
        oDocument.PageSize = pageSize
        oDocument.SortBy = cmSort
        oDocument = m_Document.GetJFDocument(oDocument)

        If Not oDocument Is Nothing Then
            dtEntity = oDocument.ListData
            recordCount = oDocument.TotalRecord
        Else
            recordCount = 0
        End If
        If recordCount = 0 Then
            'btnPrint.Enabled = False
        Else
            'btnPrint.Enabled = True
        End If
        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()
        PagingFooter()

    End Sub
#End Region
#Region "dtg_itemdatabound"
    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        Dim imbDelete As ImageButton
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imbDelete"), ImageButton)
            imbDelete.Attributes.Add("Onclick", "return DeleteConfirm()")
        End If
    End Sub
#End Region
#Region "BindDetail"
    Sub BindDetail(ByVal ProductCode As String, ByVal DocumentCode As String)
        Dim oDocument As New Parameter.JFDocument
        Me.AddEdit = "VIEW"
        pnlAddEdit.Visible = True
        pnlList.Visible = False
        If Request("cmd") = "close" Then
            BtnBack.Visible = False
            BtnClose.Visible = True
        Else
            BtnBack.Visible = True
            BtnClose.Visible = False
        End If
        BtnCancel.Visible = False
        BtnSave.Visible = False

        lblTitleAddEdit.Text = Me.AddEdit
        oDocument.ProductCode = ProductCode
        oDocument.DocumentCode = DocumentCode
        oDocument.strConnection = GetConnectionString()
        oDocument = m_Document.GetJFDocument(oDocument)

        cboDocument.Visible = False
        cboPersonalGender.Visible = False
    End Sub
#End Region
#Region "BtnAdd"
    Private Sub BtnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        If CheckFeature(Me.Loginid, "JFDocument", "Add", "MAXILOAN") Then
            If SessionInvalid() Then
                Exit Sub
            End If
        End If
        'ClearAddForm()
        lblEditCboDocument.Visible = False
        cboDocument.Visible = True
        pnlAddEdit.Visible = True
        pnlList.Visible = False
        BtnBack.Visible = False
        BtnCancel.Visible = True
        BtnSave.Visible = True
        BtnClose.Visible = False

        Me.AddEdit = "ADD"
        lblTitleAddEdit.Text = Me.AddEdit
    End Sub
#End Region
#Region "ADD"
    Private Sub Add()

        Dim customClass As New Parameter.JFDocument
        'Dim oClassAddress As New Parameter.Address
        'Dim oClassPersonal As New Parameter.Personal

        With customClass
            .strConnection = GetConnectionString()
            .DocumentCode = cboDocument.Text.Trim
            .IsPersonal = rboIsPersonal.Text.Trim
            .IsCompany = rboIsCompany.Text.Trim
            .IsMandatory = rboIsMandatory.Text
            .PersonalGender = cboPersonalGender.Text.Trim
        End With
    End Sub
#End Region
#Region "dtg_ItemCommand"
    Private Sub dtgEntity_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        Dim err As String
        Dim oDocument As New Parameter.JFDocument
        Dim dtEntity As New DataTable


        If e.CommandName = "EDIT" Then
            If CheckFeature(Me.Loginid, "JFDocument", "EDIT", "MAXILOAN") Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            Me.AddEdit = "EDIT"
            lblEditCboDocument.Visible = True
            cboDocument.Visible = False
            pnlAddEdit.Visible = True
            pnlList.Visible = False
            BtnBack.Visible = False
            BtnCancel.Visible = True
            BtnSave.Visible = True
            BtnClose.Visible = False

            lblTitleAddEdit.Text = Me.AddEdit
            oDocument.ProductCode = HiddenProductCode.Text
            oDocument.DocumentCode = dtgPaging.DataKeys.Item(e.Item.ItemIndex).ToString
            oDocument.strConnection = GetConnectionString()
            oDocument = m_Document.GetJFDocumentEdit(oDocument)
            cboPersonalGender.Visible = True


            'cboDocument.SelectedValue = CType(e.Item.FindControl("lbDocumentCode"), Label).Text

            With oDocument
                .ProductCode = Me.ProductCode
                .DocumentCode = dtgPaging.DataKeys.Item(e.Item.ItemIndex).ToString
                '.DocumentCode = cboDocument.Text
                .strConnection = GetConnectionString()
                .spName = "spSelectMFProductDocument"
            End With
            oDocument = m_Document.GetSelectJFDocument(oDocument)
            dtEntity = oDocument.ListData
            lblEditCboDocument.Text = dtEntity.Rows(0)("DocumentCode") + "-" + dtEntity.Rows(0)("DocumentName")
            rboIsPersonal.SelectedValue = dtEntity.Rows(0)("IsPersonal")
            rboIsCompany.SelectedValue = dtEntity.Rows(0)("IsCompany")
            rboIsMandatory.SelectedValue = dtEntity.Rows(0)("IsMandatory")
            cboPersonalGender.Text = dtEntity.Rows(0)("PersonalGender")

        ElseIf e.CommandName = "DEL" Then
            If CheckFeature(Me.Loginid, "JFDocument", "DEL", "MAXILOAN") Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            Dim customClass As New Parameter.JFDocument
            With customClass
                .ProductCode = Me.ProductCode
                .DocumentCode = CType(e.Item.FindControl("lbDocumentCode"), Label).Text
                .strConnection = GetConnectionString()
            End With
            err = m_Document.JFDocumentDelete(customClass)
            If err <> "" Then
                ShowMessage(lblMessage, err, True)
            Else

                ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, False)
            End If
            BindGridEntity(Me.CmdWhere, Me.SortBy)
            txtgoPage.Text = "1"
        End If
    End Sub
#End Region
#Region "imbSave"
    Private Sub BtnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSave.Click
        Dim customClass As New Parameter.JFDocument
        Dim ErrMessage As String = ""
        With customClass
            .ProductCode = Me.ProductCode
            .DocumentCode = cboDocument.SelectedValue
            .DocumentName = cboDocument.SelectedItem.Text
            .IsPersonal = rboIsPersonal.SelectedValue
            .IsCompany = rboIsCompany.SelectedValue
            .IsMandatory = rboIsMandatory.SelectedValue
            .PersonalGender = cboPersonalGender.SelectedValue
            .CreatedBy = Me.Loginid
            .ChangedBy = Me.Loginid
            .CreatedDate = Date.Now.ToString("yyyy/MM/dd")
            .ChangedDate = Date.Now.ToString("yyyy/MM/dd")
            .strConnection = GetConnectionString()
        End With

        If Me.AddEdit = "ADD" Then
            ErrMessage = m_Document.JFDocumentAdd(customClass)
            If ErrMessage <> "" Then
                ShowMessage(lblMessage, ErrMessage, True)
                Exit Sub
            Else
                ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_SUCCESS, False)
                BindGridEntity(Me.CmdWhere, Me.SortBy)
            End If
        ElseIf Me.AddEdit = "EDIT" Then
            m_Document.JFDocumentUpdate(customClass)

            ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
            BindGridEntity(Me.CmdWhere, Me.SortBy)
        End If
    End Sub
#End Region
#Region "BtnReset"

    Private Sub BtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        cboSearch.SelectedIndex = 0
        txtSearch.Text = ""
        CmdWhere = String.Format(" ProductCode = '{0}' ", Me.ProductCode)
        BindGridEntity(Me.CmdWhere, Me.SortBy)
    End Sub
#End Region
#Region "BtnSearch"
    Private Sub BtnSearchnew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSearch.Click
        CmdWhere = String.Format(" ProductCode = '{0}' ", Me.ProductCode)
        If txtSearch.Text.Trim <> "" Then
            CmdWhere = String.Format("ProductCode = '{0}' and {1} LIKE '%{2}%' ", Me.ProductCode, cboSearch.SelectedItem.Value, txtSearch.Text.Replace("%", ""))
        End If
        BindGridEntity(Me.CmdWhere, Me.SortBy)
    End Sub
#End Region
#Region "imgCancel"
    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        Response.Redirect("JFDocument.aspx?ProductCode=" & Me.ProductCode & "")
    End Sub
#End Region
#Region "imgBack"
    Private Sub btnback_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBackToMenu.Click
        Response.Redirect("../Products/Product.aspx")
    End Sub
#End Region

End Class