﻿Imports Maxiloan.Controller.Lending
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCacheHelper

Public Class Disburse
    Inherits Maxiloan.Webform.WebBased
    Private octr As New MitraController
    Protected WithEvents disburseList1 As DisburseList
    Private dtBankAccount As New DataTable
    Public Const CACHE_BANKACCOUNT As String = "CacheBankAccount"
    Protected WithEvents tjumlah As New ucNumberFormat
    Private m_controller As New DataUserControlController
    Private Property olending1 As Parameter.Lending
        Get
            Return CType(ViewState("lending"), Parameter.Lending)
        End Get
        Set(value As Parameter.Lending)
            ViewState("lending") = value
        End Set
    End Property
    Private Property ListBankAccount() As DataTable
        Get
            Return (CType(ViewState("ListBankAccount"), DataTable))
        End Get
        Set(ByVal Value As DataTable)
            ViewState("ListBankAccount") = Value
        End Set
    End Property

    Private Sub bindBankAccount(ByVal strBankType As String)
        dtBankAccount = m_controller.GetBankAccount(GetConnectionString, Me.sesBranchId, strBankType, "FD")
        cmbBankAccount.DataSource = dtBankAccount
        cmbBankAccount.DataTextField = "Name"
        cmbBankAccount.DataValueField = "ID"
        cmbBankAccount.DataBind()
    End Sub

    Private Sub bindPaymentType()
        Dim oDataTable As DataTable = New DataTable
        Dim oRow As DataRow
        Dim splitListData() As String
        Dim splitRow() As String
        Dim i As Integer
        Dim strListData As String

        oDataTable.Columns.Add("ID", System.Type.GetType("System.String"))
        oDataTable.Columns.Add("Description", System.Type.GetType("System.String"))

        oRow = oDataTable.NewRow()
        oRow("ID") = ""
        oRow("Description") = ""

        'only bank account
        strListData = "BA,Bank"
        splitListData = Split(strListData, "-")

        For i = 0 To UBound(splitListData)
            splitRow = Split(splitListData(i), ",")
            oRow("ID") = splitRow(0)
            oRow("Description") = splitRow(1)
            oDataTable.Rows.Add(oRow)
            oRow = oDataTable.NewRow()
        Next

        cbowop.DataValueField = "ID"
        cbowop.DataTextField = "Description"
        cbowop.DataSource = oDataTable
        cbowop.DataBind()
        'paymentTypeDropDownList.Items.Insert(0, "Select One")
        'paymentTypeDropDownList.Items(0).Value = "0"
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Visible = False
        If SessionInvalid() Then Exit Sub
        If Page.IsPostBack Then
        Else
            Me.FormID = "LDISB"
            If Not Me.IsHoBranch Then
                ShowMessage(lblMessage, "Harap login dikantor pusat!", True)
                pnl1.Visible = True
                pnl2.Visible = False
                Exit Sub
            Else
                disburseList1.showcheckbox = True
                disburseList1.showradio = False
                disburseList1.showbutton = False
                disburseList1.doBind()
                pnl1.Visible = True
                pnl2.Visible = False
                bindPaymentType()
                bindBankAccount("BA")
            End If
        End If
    End Sub

    Protected Sub BtnNext_Click(sender As Object, e As EventArgs) Handles BtnNext.Click
        olending1 = disburseList1.getchecked()
        txtJumlah.Text = FormatNumber(olending1.disburseamount, 0)
        txtvaluedate.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
        pnl1.Visible = False
        pnl2.Visible = True
    End Sub

    Protected Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        pnl1.Visible = True
        pnl2.Visible = False
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        With olending1
            .strConnection = Me.GetConnectionString
            .BranchId = Me.sesBranchId.Replace("'", "")
            .ValueDate = ConvertDate2(txtvaluedate.Text)
            .BusinessDate = Me.BusinessDate
            .LoginId = Me.Loginid
            .wop = cbowop.SelectedValue
            .mfname = "-"
            .disburserefno = txtReferenceNo.Text.Trim
            .bankaccountid = cmbBankAccount.SelectedValue.Trim
            .disbursenotes = txtKeterangan.Text.Trim
        End With

        Try
            octr.disburse(olending1)
            Response.Redirect("response.aspx?notif=Done&iserror=0&callerurl=disburse.aspx")
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub
End Class