﻿Imports Maxiloan.Controller
Imports Maxiloan.Parameter

Public Class Validasi
    Inherits Maxiloan.Webform.WebBased

    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 5
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Visible = False
        If SessionInvalid() Then Exit Sub
        If Not (Page.IsPostBack) Then
            If Me.IsHoBranch Then
                ShowMessage(lblMessage, "Harap login di cabang!", True)
                Exit Sub
            Else
                DoBind(Me.SortBy)
            End If
        End If
    End Sub

    Public Sub DoBind(ByVal strSort As String)
        Dim oMitraController As New MitraController
        Dim strConnection As String = Me.GetConnectionString()
        Dim oDataTable = oMitraController.DisbursementHeaderList(strConnection)

        DtgHeader.DataSource = oDataTable
        DtgHeader.DataBind()
    End Sub


#Region "Navigation"
    Private Sub PagingFooter()
        'lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
                'Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
                'Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SortBy)
    End Sub
    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles imgbtnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SortBy)
            End If
        End If
    End Sub

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles DtgHeader.SortCommand
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
            DoBind(e.SortExpression)
        Else
            Me.SortBy = e.SortExpression + " DESC"
            DoBind(e.SortExpression + " DESC")
        End If
    End Sub

#End Region

#Region "Validate"
    Private Sub DtgHeader_ItemCommand(source As Object, e As DataGridCommandEventArgs) Handles DtgHeader.ItemCommand
        Dim lblMFID As Label
        Dim lblMFBatchNo As Label
        Dim lblMFFacilityNo As Label

        lblMFID = CType(DtgHeader.Items(e.Item.ItemIndex).FindControl("lblMFID"), Label)
        lblMFBatchNo = CType(DtgHeader.Items(e.Item.ItemIndex).FindControl("lblMFBatchNo"), Label)
        lblMFFacilityNo = CType(DtgHeader.Items(e.Item.ItemIndex).FindControl("lblMFFacilityNo"), Label)

        Dim oDisburseAgreement As New DisburseAgreement
        oDisburseAgreement.strConnection = Me.GetConnectionString
        oDisburseAgreement.MFID = lblMFID.Text
        oDisburseAgreement.MFBatchNo = lblMFBatchNo.Text
        oDisburseAgreement.MFFacilityNo = lblMFFacilityNo.Text
        oDisburseAgreement.Status = "A"

        Select Case e.CommandName
            Case "VALIDATE"
                ValidateAgreement()

                'Update DISB_HEADER
                DisburseHeaderUpdate(oDisburseAgreement)

                'Reload Paging
                DoBind(Me.SortBy)
        End Select
    End Sub

    Private Sub ValidateAgreement()
        Dim oController As New MitraController

        Dim oParameterList As New List(Of AgreementToValidate)
        oParameterList = oController.LendingValidationList(Me.GetConnectionString)

        Dim oStatusReason As String = String.Empty

        For Each agr As AgreementToValidate In oParameterList
            Dim oDisburseAgreement As New DisburseAgreement
            oDisburseAgreement.strConnection = Me.GetConnectionString
            oDisburseAgreement.MFID = agr.MFID
            oDisburseAgreement.MFBatchNo = agr.MFBatchNo
            oDisburseAgreement.MFFacilityNo = agr.MFFacilityNo
            oDisburseAgreement.MFAgreementNo = agr.MFAgreementNo
            oDisburseAgreement.Status = "R"
            oDisburseAgreement.StatusReason = "-"

            Dim oMustReject As Boolean = False
            Try
                'Check Facility
                If Not (agr.AssetConditionFacility.Trim.ToUpper().Equals("A")) Then
                    If Not (agr.AssetCondition.Equals(agr.AssetConditionFacility)) Then
                        oStatusReason = String.Format("AssetCondition Harus Sama Dengan Fasilitas ({0})", agr.AssetConditionFacility)
                        oDisburseAgreement.StatusReason = oStatusReason
                        DisburseAgreementApproveReject(oDisburseAgreement)
                        Throw New System.Exception(oStatusReason)
                    End If
                End If

                'Check Tenor.
                If (agr.TenorDebtor > agr.MaxTenorFacility) Then
                    oStatusReason = String.Format("TenorDebtor Tidak Boleh Lebih Dari Yang Telah Ditentukan ({0})", agr.MaxTenorFacility)
                    oDisburseAgreement.StatusReason = oStatusReason
                    DisburseAgreementApproveReject(oDisburseAgreement)
                    Throw New System.Exception(oStatusReason)
                End If

                'Check Vehicle Country
                If Not (agr.MfgCountry.Trim.Equals(agr.MfgCountryFacility.Trim)) Then
                    oStatusReason = "Manufactured Country Harus Sama Dengan Fasilitas"
                    oDisburseAgreement.StatusReason = oStatusReason
                    DisburseAgreementApproveReject(oDisburseAgreement)
                    Throw New System.Exception(oStatusReason)
                End If

                'Check Vehicle Merk
                If Not (agr.AssetBrandFacility.Contains(agr.Merk.Trim)) Then
                    oStatusReason = "Merk Harus Terdaftar Di Fasilitas"
                    oDisburseAgreement.StatusReason = oStatusReason
                    DisburseAgreementApproveReject(oDisburseAgreement)
                    Throw New System.Exception(oStatusReason)
                End If

                Dim tenorInYear As Integer = agr.TenorMF / 12

                'Check Usia Kendaraan
                Dim usiaKendaraan As Int32 = agr.AgreementDate.Year - agr.MfgYear
                If ((usiaKendaraan + tenorInYear) > agr.MaxAssetAgeFacility) Then
                    oStatusReason = "Usia Kendaraan Melebihi Ketentuan Fasilitas"
                    oDisburseAgreement.StatusReason = oStatusReason
                    DisburseAgreementApproveReject(oDisburseAgreement)
                    Throw New System.Exception(oStatusReason)
                End If

                'Check Uang Muka Murni
                Dim dpPercentage As Decimal
                dpPercentage = (agr.DownPayment / agr.TotalOTR) * 100
                If Not (dpPercentage >= agr.MinDownPaymentFacility) Then
                    oStatusReason = "DP Harus lebih Besar Dari Yang Telah Ditentukan"
                    oDisburseAgreement.StatusReason = oStatusReason
                    DisburseAgreementApproveReject(oDisburseAgreement)
                    Throw New System.Exception(oStatusReason)
                End If

                'Check Data End User
                Dim ageMinimum As Integer = 21

                Dim ageDebitur As Int32
                ageDebitur = Me.GetCurrentAge(agr.BirthDate, agr.AgreementDate)
                If Not (agr.MaritalStatus.ToUpper().Equals("M")) Then
                    If Not (ageDebitur >= ageMinimum) Then
                        oStatusReason = "Usia Debitur Minimal 21 Tahun Atau Telah Menikah"
                        oDisburseAgreement.StatusReason = oStatusReason
                        DisburseAgreementApproveReject(oDisburseAgreement)
                        Throw New System.Exception(oStatusReason)
                    End If
                End If

                'Check Usia End Contract
                If (agr.DebtorType.Trim.ToUpper.Equals("P")) Then
                    If (agr.PersonalCustType.Trim.ToUpper.Equals("M")) Then
                        If ((ageDebitur + tenorInYear) > 55) Then
                            oStatusReason = "Usia Debitur Karyawan Maksimal 55 Tahun Pada Saat End Contract"
                            oDisburseAgreement.StatusReason = oStatusReason
                            DisburseAgreementApproveReject(oDisburseAgreement)
                            Throw New System.Exception(oStatusReason)
                        End If
                    ElseIf (agr.PersonalCustType.Trim.ToUpper.Equals("N")) Then
                        If ((ageDebitur + tenorInYear) > 60) Then
                            oStatusReason = "Usia Debitur Wirausaha Maksimal 60 Tahun Pada Saat End Contract"
                            oDisburseAgreement.StatusReason = oStatusReason
                            DisburseAgreementApproveReject(oDisburseAgreement)
                            Throw New System.Exception(oStatusReason)
                        End If
                    ElseIf (agr.PersonalCustType.Trim.ToUpper.Equals("P")) Then
                        If ((ageDebitur + tenorInYear) > 60) Then
                            oStatusReason = "Usia Debitur Profesional Maksimal 60 Tahun Pada Saat End Contract"
                            oDisburseAgreement.StatusReason = oStatusReason
                            DisburseAgreementApproveReject(oDisburseAgreement)
                            Throw New System.Exception(oStatusReason)
                        End If
                    Else
                        oStatusReason = "Tidak Dapat Menentukan PersonalCustType"
                        oDisburseAgreement.StatusReason = oStatusReason
                        DisburseAgreementApproveReject(oDisburseAgreement)
                        Throw New System.Exception(oStatusReason)
                    End If
                End If

                'Check Badan Usaha
                If (agr.DebtorType.Trim.ToUpper.Equals("C")) Then
                    If (String.IsNullOrEmpty(agr.NPWP) Or String.IsNullOrEmpty(agr.NoTDP) Or String.IsNullOrEmpty(agr.NoSIUP)) Then
                        oStatusReason = "Badan Usaha Harus Memiliki NPWP, NoTDP dan NoSIUP"
                        oDisburseAgreement.StatusReason = oStatusReason
                        DisburseAgreementApproveReject(oDisburseAgreement)
                        Throw New System.Exception(oStatusReason)
                    End If
                End If

                'Update DISB_AGREEMENT
                oDisburseAgreement.Status = "A"
                DisburseAgreementApproveReject(oDisburseAgreement)

            Catch ex As Exception
                System.Diagnostics.Debug.WriteLine(String.Format("Error! {0}", ex.Message))
                Continue For
            End Try
        Next
    End Sub

    Function DisburseAgreementApproveReject(ByVal oDisburseAgreement As Parameter.DisburseAgreement) As Boolean
        Dim oController As New MitraController
        Return oController.DisburseAgreementApproveReject(oDisburseAgreement)
    End Function

    Function DisburseHeaderUpdate(ByVal oDisburseAgreement As Parameter.DisburseAgreement) As Boolean
        Dim oController As New MitraController
        Return oController.DisburseHeaderUpdate(oDisburseAgreement)
    End Function

    Public Function GetCurrentAge(ByVal dob As Date, ByVal today As Date) As Integer
        Dim age As Integer
        age = today.Year - dob.Year
        If (dob > today.AddYears(-age)) Then age -= 1
        Return age
    End Function
#End Region
End Class