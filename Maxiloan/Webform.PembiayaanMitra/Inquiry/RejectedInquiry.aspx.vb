﻿Imports Maxiloan.Controller

Public Class RejectedInquiry
    Inherits Maxiloan.Webform.WebBased

    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 5
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Public Property Status() As String
        Get
            Return CStr(ViewState("Status"))
        End Get
        Set(ByVal Value As String)
            ViewState("Status") = Value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.FormID = "INQREJC"
        Dim oStatus As String
        Me.pageSize = 25

        If Not (Page.IsPostBack) Then

            Try
                oStatus = Convert.ToString(Request.QueryString("Status"))

                Me.Status = oStatus
                Me.SortBy = ""
                Me.SearchBy = String.Format("DISB_AGREEMENT.Status = '{0}'", Me.Status)
                DoBind(Me.SortBy, Me.SearchBy)
            Catch ex As Exception
                ShowMessage(lblMessage, ex.Message, True)
            End Try
        End If
    End Sub

    Private Sub DoBind(ByVal strSort As String, ByVal cmdWhere As String)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim cContract As New GeneralPagingController
        Dim oContract As New Parameter.GeneralPaging

        With oContract
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = ""
            .SpName = "spInquiryLendingList"
        End With

        oContract = cContract.GetGeneralPaging(oContract)

        With oContract
            lblrecord.Text = CType(.TotalRecords, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecords
        End With

        dtsEntity = oContract.ListData
        dtvEntity = dtsEntity.DefaultView
        dtvEntity.Sort = Me.SortBy

        If dtvEntity.Count <= 0 Then
            'imgPrint.Enabled = False
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
        Else
            'imgPrint.Enabled = True
        End If

        DtgInquiry.DataSource = dtvEntity
        Try
            DtgInquiry.DataBind()
        Catch
            DtgInquiry.CurrentPageIndex = 0
            DtgInquiry.DataBind()
        End Try
        PagingFooter()
    End Sub

#Region "Navigation"
    Private Sub PagingFooter()
        'lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
                'Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
                'Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SortBy, Me.SearchBy)
    End Sub
    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles imgbtnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SortBy, Me.SearchBy)
            End If
        End If
    End Sub

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles DtgInquiry.SortCommand
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
            DoBind(e.SortExpression, Me.SearchBy)
        Else
            Me.SortBy = e.SortExpression + " DESC"
            DoBind(e.SortExpression + " DESC", Me.SearchBy)
        End If
    End Sub

    Private Sub BtnSearch_Click(sender As Object, e As EventArgs) Handles BtnSearch.Click
        If (txtSearchBy.Text.Length > 0) Then
            Me.SearchBy = String.Format("DISB_AGREEMENT.Status = '{0}' AND {1} LIKE '%{2}%'", Me.Status, cboSearchBy.Text, txtSearchBy.Text)
        Else
            Me.SearchBy = String.Format("DISB_AGREEMENT.Status = '{0}'", Me.Status)
        End If
        DoBind(Me.SortBy, Me.SearchBy)
    End Sub

    Private Sub BtnReset_Click(sender As Object, e As EventArgs) Handles BtnReset.Click
        cboSearchBy.SelectedIndex = 0
        Me.SearchBy = String.Format("DISB_AGREEMENT.Status = '{0}'", Me.Status)
        DoBind(Me.SortBy, Me.SearchBy)
    End Sub

#End Region
End Class