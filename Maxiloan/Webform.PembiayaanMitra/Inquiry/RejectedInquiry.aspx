﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="RejectedInquiry.aspx.vb" Inherits="Maxiloan.Webform.PembiayaanMitra.RejectedInquiry" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>RejectedInquiry</title>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
</head>
<body>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    INQUIRY REJECTED
                </h3>
            </div>
        </div>
        <asp:UpdatePanel runat="server" ID="updatePanel1">
            <ContentTemplate>
                <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
                <div class="form_title">
                    <div class="title_strip">
                    </div>
                    <div class="form_single">
                        <h4>
                            HASIL PENCARIAN                  
                        </h4>                    
                    </div>
                </div>
                <asp:Panel ID="pnlList" runat="server">
                    <div class="form_box_header">
                        <div class="form_single">
                            <div class="grid_wrapper_ws">
                                <asp:DataGrid ID="DtgInquiry" runat="server" AutoGenerateColumns="False" BorderWidth="0"
                                        OnSortCommand="SortGrid" BorderStyle="None" CssClass="grid_general">
                                        <ItemStyle CssClass="item_grid"></ItemStyle>
                                        <HeaderStyle CssClass="th"></HeaderStyle>
                                        <Columns>
                                            <asp:TemplateColumn Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblMFID" runat="server" Visible="false" Text='<%#Container.DataItem("MFID")%>'></asp:Label>
                                                    <asp:Label ID="lblFacilityNo" runat="server" Visible="false" Text='<%#Container.DataItem("MFFacilityNo")%>'></asp:Label>
                                                    <asp:Label ID="lblMFFacilityNo" runat="server" Visible="false" Text='<%#Container.DataItem("MFFacilityNo")%>'></asp:Label>
                                                    <asp:Label ID="lblMFAgreementNo" runat="server" Visible="false" Text='<%#Container.DataItem("MFAgreementNo")%>'></asp:Label>
                                                    <asp:Label ID="lblStatus" runat="server" Visible="false" Text='<%#Container.DataItem("Status")%>'></asp:Label>
                                                    <asp:Label ID="lblStatusReason" runat="server" Visible="false" Text='<%#Container.DataItem("StatusReason")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn DataField="MFID" SortExpression="MFID" HeaderText="MF ID"></asp:BoundColumn> 
                                            <asp:BoundColumn DataField="MFFacilityNo" SortExpression="MFFacilityNo" HeaderText="NO. FASILITAS"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="MFFacilityNo" SortExpression="MFFacilityNo" HeaderText="No. FASILITAS"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="MFAgreementNo" SortExpression="MFAgreementNo" HeaderText="NO. KONTRAK"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="Status" SortExpression="Status" HeaderText="STATUS"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="StatusReason" SortExpression="StatusReason" HeaderText="ALASAN REJECT"></asp:BoundColumn>                                          
                                        </Columns>
                                    </asp:DataGrid>
                                    <div class="button_gridnavigation">
                                        <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                                            CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="First">
                                        </asp:ImageButton>
                                        <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                                            CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Prev">
                                        </asp:ImageButton>
                                        <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                                            CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Next">
                                        </asp:ImageButton>
                                        <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                                            CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Last">
                                        </asp:ImageButton>
                                        Page&nbsp;
                                        <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                                        <asp:Button ID="imgbtnPageNumb" runat="server" CssClass="small buttongo blue" Text="Go"
                                            EnableViewState="False"></asp:Button>
                                        <asp:RangeValidator ID="rgvGo" runat="server" ErrorMessage="No Halaman Salah" ControlToValidate="txtPage"
                                            Type="Integer" MaximumValue="999999999" MinimumValue="1" CssClass="validator_general"
                                            Display="Dynamic"></asp:RangeValidator>
                                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" Display="Dynamic"
                                            ErrorMessage="No Halaman Salah" ControlToValidate="txtPage" CssClass="validator_general"></asp:RequiredFieldValidator>
                                    </div>
                         
                                    <asp:Label ID="lblPage" runat="server"></asp:Label>of 
                                    <asp:Label ID="lblTotPage" runat="server"></asp:Label> 
                                    <div class="label_gridnavigation">Total
                                    <asp:Label ID="lblrecord" runat="server"></asp:Label> record(s)
                                </div>                            
                            </div>
                        </div>
                    </div>
                    <div class="form_title">
                        <div class="title_strip">
                        </div>
                        <div class="form_single">
                            <h4>
                                CARI FASILITAS                  
                            </h4>                    
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label>
                                Cari Berdasarkan</label>
                            <asp:DropDownList ID="cboSearchBy" runat="server">
                                <asp:ListItem Value="DISB_HEADER.MFID" Selected="True" >Kode MF</asp:ListItem>
                                <asp:ListItem Value="DISB_HEADER.MFBatchNo">Nomor Batch</asp:ListItem>
                                <asp:ListItem Value="DISB_HEADER.MFFacilityNo">Nomor Fasilitas</asp:ListItem>
                                <asp:ListItem Value="DISB_HEADER.MultiFinanceName">Nama Multi Finance</asp:ListItem>
                                <asp:ListItem Value="DISB_AGREEMENT.MFAgreementNo">No. Kontrak</asp:ListItem>
                            </asp:DropDownList>
                            <asp:TextBox ID="txtSearchBy" runat="server" Width="250px"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form_button">
                        <asp:Button ID="BtnSearch" runat="server" CausesValidation="False" Text="Find" CssClass="small button blue"></asp:Button>
                        <asp:Button ID="BtnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray"></asp:Button>
                    </div>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
