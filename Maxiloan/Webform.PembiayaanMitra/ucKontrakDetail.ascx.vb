﻿Imports Maxiloan.Parameter

Public Class ucKontrakDetail
    Inherits Maxiloan.Webform.ControlBased

    Public Event _getPaymentAgreement(ByRef _paymentcalculateproperty As PaymentCalculateProperty)
    Public Event _dataPaymentCalculate(_paymentcalculateproperty As PaymentCalculateProperty)
    Public Property MFID() As String
    Public Property MFFacilityNo() As String
    Public Property MFBATCHNO() As String
    Public Property MFAGREEMENTNO() As String
    Public Property PRINCIPALAMOUNT() As Decimal
    Public Property TENOR() As Int16
    Public Property RATE() As Decimal
    Public Property INTERESTAMOUNT() As Decimal
    Public Property OSPRINCIPAL() As Decimal
    Public Property OSINTEREST() As Decimal
    Public Property PAIDSEQNO() As Decimal
    Public Property STATUS() As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Public Sub DoBind()
        txtMF.Text = MFID
        txtFasilitas.Text = Me.MFFacilityNo
        txtBatch.Text = Me.MFBATCHNO
        txtAgreement.Text = Me.MFAGREEMENTNO
        txtHutang.Text = Me.PRINCIPALAMOUNT
        txtPaidSeq.Text = Me.PAIDSEQNO
        txtTenor.Text = Me.TENOR
        txtPokok.Text = Me.OSPRINCIPAL
        txtRate.Text = Me.RATE
        txtBunga.Text = Me.OSINTEREST
    End Sub

    Public Sub DoBindPaymentCalculate()
        Dim item = New PaymentCalculateProperty()
        item.MFAGREEMENTNO = txtAgreement.Text
        item.MFFACILITYNO = txtFasilitas.Text
        item.MFBATCHNO = txtBatch.Text
        item.MFID = txtMF.Text

        RaiseEvent _dataPaymentCalculate(item)
    End Sub

    Public Sub DoBindGetAgreement(ByRef property1 As Parameter.PaymentCalculateProperty)
        property1 = New Parameter.PaymentCalculateProperty
        property1.MFAGREEMENTNO = txtAgreement.Text
        RaiseEvent _getPaymentAgreement(property1)
    End Sub

End Class