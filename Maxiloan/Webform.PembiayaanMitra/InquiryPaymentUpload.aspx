﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="InquiryPaymentUpload.aspx.vb" Inherits="Maxiloan.Webform.Lending.InquiryPaymentUpload" %>

<%--<%@ Register TagPrefix="uc1" TagName="InquiryPaymentUploadList" Src="InquiryPaymentUploadList.ascx" %>--%>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>LendingFacility</title>
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" /> 
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
</head>
<body>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <progresstemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </progresstemplate>
    </asp:UpdateProgress>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <asp:UpdatePanel runat="server" ID="updatePanel1">
            <contenttemplate>
                <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
                <div class="form_title">
                    <div class="title_strip">
                    </div>
                    <div class="form_single">
                        <h3>
                            UPLOAD PAYMENT                
                        </h3>                    
                    </div>
                </div>
                <asp:Panel ID="pnlList" runat="server">
                    <div class="form_box_header">
                        <div class="form_single">
                            <div class="grid_wrapper_ws">
                                <asp:DataGrid ID="DtgLendingFacility" runat="server" AutoGenerateColumns="False" BorderWidth="0" Width="2000"
                                        OnSortCommand="SortGrid" BorderStyle="None" CssClass="grid_general">
                                        <ItemStyle CssClass="item_grid"></ItemStyle>
                                        <HeaderStyle CssClass="th"></HeaderStyle>
                                        <Columns>
                                            <%--<asp:TemplateColumn HeaderText="EDIT">
                                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="ImbEdit" runat="server" ImageUrl="../Images/iconedit.gif" CommandName="EDIT" CausesValidation="False"></asp:ImageButton>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="DETAIL">
                                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="ImbDetail" runat="server" ImageUrl="../Images/IconDocument.gif" CommandName="DETAIL" CausesValidation="False"></asp:ImageButton>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>--%>
                                            <asp:TemplateColumn Visible="false">
                                                <ItemTemplate>
                                                   <%-- <asp:Label ID="lblMFCode" runat="server" Visible="false" Text='<%#Container.DataItem("MFCode")%>'></asp:Label>
                                                    <asp:Label ID="lblFacilityNo" runat="server" Visible="false" Text='<%#Container.DataItem("MFFacilityNo")%>'></asp:Label>
                                                    <asp:Label ID="lblMFFacilityName" runat="server" Visible="false" Text='<%#Container.DataItem("MFFacilityName")%>'></asp:Label>
                                                    <asp:Label ID="lblPlafond" runat="server" Visible="false" Text='<%#Container.DataItem("Plafond")%>'></asp:Label>
                                                    <asp:Label ID="lblDrawdownStartDate" runat="server" Visible="false" Text='<%#Container.DataItem("DrawdownStartDate")%>'></asp:Label>
                                                    <asp:Label ID="lblDrawdownEndDate" runat="server" Visible="false" Text='<%#Container.DataItem("DrawdownEndDate")%>'></asp:Label>--%>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn DataField="MFID" SortExpression="MFID" HeaderText="KODE MF"></asp:BoundColumn> 
                                            <asp:BoundColumn DataField="ReferenceNo" SortExpression="ReferenceNo" HeaderText="NO. REFERENCE"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="MultiFinanceName" SortExpression="MultiFinanceName" HeaderText="MULTIFINANCE NAME"></asp:BoundColumn> 
                                            <asp:BoundColumn DataField="PaymentDate" SortExpression="PaymentDate" HeaderText="PAYMENT DATE" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn> 
                                            <asp:BoundColumn DataField="TransactionType" SortExpression="TransactionType" HeaderText="TYPE"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="MFFacilityNo" SortExpression="MFFacilityNo" HeaderText="NO. FACILITY"></asp:BoundColumn> 
                                            <asp:BoundColumn DataField="FacilityType" SortExpression="FacilityType" HeaderText="TYPE"></asp:BoundColumn> 
                                            <asp:BoundColumn DataField="TotalAccount" SortExpression="TotalAccount" HeaderText="TOTAL ACCOUNT" DataFormatString="{0:N0}"></asp:BoundColumn> 
                                            <asp:BoundColumn DataField="TotalAmount" SortExpression="TotalAmount" HeaderText="TOTAL AMOUNT" DataFormatString="{0:N0}"></asp:BoundColumn> 
                                            <asp:BoundColumn DataField="Status" SortExpression="Status" HeaderText="STATUS"></asp:BoundColumn> 
                                            <asp:BoundColumn DataField="MFAgreementNo" SortExpression="MFAgreementNo" HeaderText="NO. AGREEMENT"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="DebtorName" SortExpression="DebtorName" HeaderText="NAMA DEBITURE"></asp:BoundColumn> 
                                            <asp:BoundColumn DataField="InsSeqNo" SortExpression="InsSeqNo" HeaderText="NO. INSEQ"></asp:BoundColumn> 
                                            <asp:BoundColumn DataField="DueDate" SortExpression="DueDate" HeaderText="DUE DATE" DataFormatString="{0:N0}"></asp:BoundColumn> 
                                            <asp:BoundColumn DataField="InstallmentAmount" SortExpression="InstallmentAmount" HeaderText="INSTALLMENT AMOUNT" DataFormatString="{0:N0}"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="PrincipalAmount" SortExpression="PrincipalAmount" HeaderText="PRINCIPAL AMOUNT" DataFormatString="{0:N0}"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="InterestAmount" SortExpression="InterestAmount" HeaderText="INTEREST AMOUNT" DataFormatString="{0:N0}"></asp:BoundColumn>                                                                                                  
                                            <asp:BoundColumn DataField="LateChargesAmount" SortExpression="LateChargesAmount" HeaderText="LATE CHARGES" DataFormatString="{0:N0}"></asp:BoundColumn>                                                                                                  
                                        </Columns>
                                    </asp:DataGrid>
                                </div>   
                            <div class="button_gridnavigation">
                                        <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../Images/grid_navbutton01.png"
                                            CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="First">
                                        </asp:ImageButton>
                                        <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../Images/grid_navbutton02.png"
                                            CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Prev">
                                        </asp:ImageButton>
                                        <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../Images/grid_navbutton03.png"
                                            CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Next">
                                        </asp:ImageButton>
                                        <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                                            CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Last">
                                        </asp:ImageButton>
                                        Page&nbsp;
                                        <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                                        <asp:Button ID="imgbtnPageNumb" runat="server" CssClass="small buttongo blue" Text="Go"
                                            EnableViewState="False"></asp:Button>
                                        <asp:RangeValidator ID="rgvGo" runat="server" ErrorMessage="No Halaman Salah" ControlToValidate="txtPage"
                                            Type="Integer" MaximumValue="999999999" MinimumValue="1" CssClass="validator_general"
                                            Display="Dynamic"></asp:RangeValidator>
                                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" Display="Dynamic"
                                            ErrorMessage="No Halaman Salah" ControlToValidate="txtPage" CssClass="validator_general"></asp:RequiredFieldValidator>
                                    </div>
                         
                                    <asp:Label ID="lblPage" runat="server"></asp:Label>of 
                                    <asp:Label ID="lblTotPage" runat="server"></asp:Label> 
                                    <div class="label_gridnavigation">Total
                                    <asp:Label ID="lblrecord" runat="server"></asp:Label> record(s)                         
                            </div>
                        </div>
                    </div>
                    <%--<div class="form_button">
                        <asp:Button ID="BtnAddNew" runat="server" CausesValidation="False" Text="Add" CssClass="small button green">
                        </asp:Button>
                    </div>--%>
                    <div class="form_title">
                        <!--<div class="title_strip">
                        </div>-->
                        <div class="form_single">
                            <h5>
                                CARI FASILITAS                  
                            </h5>                    
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label>
                                Cari Berdasarkan</label>
                            <asp:DropDownList ID="cboSearchBy" runat="server">
                                <asp:ListItem Value="a.MFID"  Selected="True">Kode MF</asp:ListItem>
                                <asp:ListItem Value="a.ReferenceNo">No. Reference</asp:ListItem>
                                <asp:ListItem Value="MFAgreementNo">No. Agreement</asp:ListItem>
                                <asp:ListItem Value="DebtorName">Nama</asp:ListItem>
                            </asp:DropDownList>
                            <asp:TextBox ID="txtSearchBy" runat="server" Width="250px"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form_button">
                        <asp:Button ID="BtnSearch" runat="server" CausesValidation="False" Text="Find"
                            CssClass="small button blue"></asp:Button>
                        <asp:Button ID="BtnReset" runat="server" CausesValidation="False" Text="Reset"
                            CssClass="small button gray"></asp:Button>
                    </div>
                </asp:Panel>
                <%--<asp:Panel ID="pnlAddEdit" runat="server" visible="false">
                    <div class="form_title">
                        <div class="form_single">
                            <h4>
                               INPUT FASILITAS -&nbsp;
                                <asp:Label ID="lblMenuAddEdit" runat="server" Enabled="True"></asp:Label>
                            </h4>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label class ="label_req">Kode MF</label>
                            <asp:TextBox ID="txtMFCode" runat="server" Width="100px"  MaxLength="10"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="reqMFCode" runat="server" Width="184px"
                                Display="Dynamic" ErrorMessage="Harap isi Kode MF" ControlToValidate="txtMFCode" CssClass="validator_general" ></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label class ="label_req">No. Fasilitas</label>
                            <asp:TextBox ID="txtFacilityNo" runat="server" Width="100px"  MaxLength="10"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="reqFacilityNo" runat="server" Width="184px"
                                Display="Dynamic" ErrorMessage="Harap isi FacilityNo" ControlToValidate="txtFacilityNo" CssClass="validator_general" ></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label class ="label_req">Nama Fasilitas</label>
                            <asp:TextBox ID="txtFacilityName" runat="server" Width="250px"  MaxLength="100"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="reqFacilityName" runat="server" Width="250px"
                                Display="Dynamic" ErrorMessage="Harap isi Nama Fasilitas" ControlToValidate="txtFacilityName" CssClass="validator_general" ></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label>Jenis Fasilitas</label>
                            <asp:DropDownList ID="ddlJenisFacility" runat="server">
                                <asp:ListItem Text="PILIH" Value="0" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="JF" Value="JF"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="reqJenisFacility" runat="server" Width="250px"
                                Display="Dynamic" ErrorMessage="Harap Pilih Jenis Fasilitas" ControlToValidate="ddlJenisFacility" CssClass="validator_general" InitialValue="0"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label>Plafond</label>
                            <uc1:ucNumberFormat ID="txtPlafond" runat="server"></uc1:ucNumberFormat>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label>Sifat Fasilitas</label>
                            <asp:DropDownList ID="ddlSifatFacility" runat="server">
                                <asp:ListItem Text="PILIH" Value="0" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="REVOLVING" Value="R"></asp:ListItem>
                                <asp:ListItem Text="NON REVOLVING" Value="NR"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="reqSifatFasilitas" runat="server" Width="250px"
                                Display="Dynamic" ErrorMessage="Harap Pilih Sifat Fasilitas" ControlToValidate="ddlSifatFacility" CssClass="validator_general" InitialValue="0"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_left">
                            <label>Porsi</label>
                            <uc1:ucNumberFormat ID="ucPorsi" runat="server"></uc1:ucNumberFormat>
                            <label> %</label>
                        </div>
                        <div class="form_right">
                            <label>Kondisi Asset</label>
                            <asp:DropDownList ID="ddlAssetCondition" runat="server">
                                <asp:ListItem Text="PILIH" Value="0" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="No. Reference" Value="ReferenceNo"></asp:ListItem>
                                <asp:ListItem Text="No. Agreement" Value="MFAgreementNo"></asp:ListItem>
                                <asp:ListItem Text="Debiture Name" Value="DebtorName"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="reqAssetCondition" runat="server" Width="250px" Display="Dynamic" ErrorMessage="Harap Pilih Kondisi" ControlToValidate="ddlAssetCondition" CssClass="validator_general" InitialValue="0"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label>Tanggal Mulai</label>
                            <uc1:ucDateCE ID="ucStartDate" runat="server"></uc1:ucDateCE>
                            <label class="label_auto">s/d</label>
                            <uc1:ucDateCE ID="ucEndDate" runat="server"></uc1:ucDateCE>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_left">
                            <label class ="label_req">Late Charges</label>
                            <uc1:ucNumberFormat ID="ucLateCharges" runat="server"></uc1:ucNumberFormat>
                            <label> %</label>
                        </div>
                        <div class="form_right">
                            <label>Penalty</label>
                            <uc1:ucNumberFormat ID="ucPenalty" runat="server"></uc1:ucNumberFormat>
                            <label> %</label>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_left">
                            <label>Tanggal Fasilitas</label>
                            <uc1:ucDateCE ID="ucTglFasilitas" runat="server"></uc1:ucDateCE>
                        </div>
                        <div class="form_right">
                            <label>Minimal Drawdown</label>
                            <uc1:ucNumberFormat ID="ucMinDrawdownAmount" runat="server"></uc1:ucNumberFormat>
                        </div>
                    </div>
                    <div class="form_button">
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button green"></asp:Button>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="small button gray"></asp:Button>
                    </div>
                </asp:Panel>--%>
            </contenttemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
