﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucPerhitunganPelunasan.ascx.vb" Inherits="Maxiloan.Webform.PembiayaanMitra.ucPerhitunganPelunasan" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc2" TagName="ucDateCE" Src="../Webform.UserController/ucDateCE.ascx" %>

<div class="form_box_header">
    <div class="form_single">
        <h5> 
            PERHITUNGAN PELUNASAN
        </h5>
    </div>
</div>

<div class="form_box">
    <div class="form_single">
        <label>Tanggal Efektif</label>
        <uc2:ucDateCE ID="ucTglEfektif" runat="server"></uc2:ucDateCE>
        <asp:Button ID="BtnHitung" runat="server" Text="Hitung" ></asp:Button>
    </div>
</div>
<div class="form_box">
    <div class="form_single">
        <label>Sisa Pokok</label>
        <asp:label id="txtPokok" runat="server"/>
    </div>
</div>
<div class="form_box">
    <div class="form_single">
        <label>Bunga Berjalan</label>
        <asp:label id="txtBerjalan" runat="server"/>
    </div>
</div>
<div class="form_box">
    <div class="form_single">
        <label>Denda Pelunasan</label>
        <asp:label id="txtPerusahaan" runat="server"/>
    </div>
</div>
<div class="form_box">
    <div class="form_single">
        <label>Total Pelunasan</label>
        <asp:label id="lbltotal" runat="server"/>
    </div>
</div>