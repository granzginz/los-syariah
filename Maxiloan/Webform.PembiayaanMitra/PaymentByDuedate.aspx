﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PaymentByDuedate.aspx.vb" Inherits="Maxiloan.Webform.PembiayaanMitra.PaymentByDuedate" %>

<!DOCTYPE html>
<%@ Register TagPrefix="uc1" TagName="ucDateCE" Src="../Webform.UserController/ucDateCE.ascx" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="Stylesheet" type="text/css" href="../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../Include/Buttons.css" />
    <script src="../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server" ID="ScriptManager1">
        </asp:ScriptManager>
        <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
        <asp:Panel runat="server" ID="pnl1">
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>LENDING PAYMENT
                    </h3>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>MF Name</label>
                    <asp:DropDownList runat="server" ID="cbomf">
                        <asp:ListItem Text="FIAL" Value="260340"></asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>Fasilitas No.</label>
                    <asp:DropDownList runat="server" ID="cbofacility">
                        <asp:ListItem Text="001/PKSJF-AFI/III/20" Value="001/PKSJF-AFI/III/20"></asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>Payment Date</label>
                    <uc1:ucdatece runat="server" id="ucstartdate"></uc1:ucdatece>
                    <label class="label_auto">s/d</label>
                    <uc1:ucdatece runat="server" id="ucenddate"></uc1:ucdatece>
                </div>
            </div>
            <div class="form_button">
                <asp:Button runat="server" ID="btnFind" Text="Find" CssClass="small button blue" />
            </div>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnl2">
            <div class="form_box_title">
                <div class="form_single">
                    <h5>INSTALLMENT SUMMARY </h5>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>Jumlah Kontrak</label>
                    <asp:Label runat="server" ID="lblacc"></asp:Label>
                </div>
                <div class="form_right">
                    <label>Pokok</label>
                    <asp:Label runat="server" ID="lblprincipalamount"></asp:Label>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>Bunga</label>
                    <asp:Label runat="server" ID="lblinterestamount"></asp:Label>
                </div>
                <div class="form_right">
                    <label>Angsuran</label>
                    <asp:Label runat="server" ID="lblinstallmentamount"></asp:Label>
                </div>
            </div>

            <div class="form_button">
                <asp:Button runat="server" Text="Next" ID="btnNext" CssClass="small button blue" />
            </div>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnl3">
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>DISBURSE DETAIL
                    </h3>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>Reference No</label>
                    <asp:TextBox ID="txtReferenceNo" runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>Jumlah</label>
                    <asp:TextBox ID="txtJumlah" runat="server" ReadOnly="true"></asp:TextBox>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>Tanggal Valuta</label>
                    <uc1:ucdatece runat="server" id="uctanggalvaluta"></uc1:ucdatece>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>Payment Type</label>
                    <asp:DropDownList ID="cbowop" runat="server" />
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>Rekening Bank</label>
                    <asp:DropDownList ID="cmbBankAccount" runat="server">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator3" runat="server" InitialValue="0"
                        Enabled="True" Display="Dynamic" ControlToValidate="cmbBankAccount" ErrorMessage="Harap Pilih rekening Bank!"
                        Visible="True" CssClass="validator_general">
                    </asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Keterangan</label>
                    <asp:TextBox ID="txtKeterangan" runat="server" CssClass="long_text"></asp:TextBox>
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue"></asp:Button>
                <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Cancel" CssClass="small button gray"></asp:Button>
            </div>
        </asp:Panel>
    </form>
</body>
</html>
