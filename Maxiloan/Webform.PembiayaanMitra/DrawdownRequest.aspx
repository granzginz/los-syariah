﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="DrawdownRequest.aspx.vb" Inherits="Maxiloan.Webform.PembiayaanMitra.DrawdownRequest" ValidateRequest="false"   %>

<%@ Register TagPrefix="uc1" TagName="uploadlist" Src="UploadList.ascx" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="Stylesheet" type="text/css" href="../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../Include/Buttons.css" />
    <script src="../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>PENGAJUAN PENCAIRAN
                </h3>
            </div>
        </div>
        <uc1:uploadlist ID="ouploadlist1" runat="server"></uc1:uploadlist>
        <div class="form_button">
            <asp:Button ID="BtnSave" runat="server" Text="Save" CssClass="small button blue"></asp:Button>            
        </div>
    </form>
</body>
</html>
