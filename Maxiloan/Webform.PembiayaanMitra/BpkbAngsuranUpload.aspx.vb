﻿Imports System.IO
Imports NPOI.HSSF.Record.Common
Imports Maxiloan.Controller
Imports Maxiloan.Parameter

Public Class BpkbAngsuranUpload
    Inherits WebBased
    Private ReadOnly _jvController As New LendingProcessController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        FormID = "BpkbAngsuran"
        lblMessage.Visible = False
        ' btnNext.Enabled = False
        If SessionInvalid() Then
            Exit Sub
        End If


        If Not Page.IsPostBack Then
            If Request("s") = "1" Then
                ShowMessage(lblMessage, "Sukses", False)
            End If
        End If
    End Sub


    Protected Sub btnFind_Click(sender As Object, e As EventArgs) Handles btnFind.Click

        lblInvalidMsg.Text = ""
        lblInvalidMsg.Visible = False
        ' Dim list = New List(Of String)

        Dim resultDic As New Dictionary(Of String, StreamReader)

        If Not flHeader.HasFile Then
            ShowMessage(lblMessage, "File Header belum dipilih!", True)
            Exit Sub
        End If
        If Not flDetail.HasFile Then
            ShowMessage(lblMessage, "File Detail belum dipilih!", True)
            Exit Sub
        End If

        resultDic.Add($"{cboimpAppBy.SelectedValue}_H", New StreamReader(flHeader.FileContent))
        resultDic.Add($"{cboimpAppBy.SelectedValue}_D", New StreamReader(flDetail.FileContent))


        Dim mgr = New PaserManager()
        Dim inValidString = New StringBuilder
        Try
            mgr.NewparseBPKBAngsToList(resultDic)
            For Each s In mgr.DisbParamList
                For Each it In s.Value
                    inValidString.AppendLine(it.ValidateString.ToString())
                Next
            Next

            If (inValidString.ToString.Trim().Length > 0) Then
                lblInvalidMsg.Visible = True
                lblInvalidMsg.Text = inValidString.Replace(vbCrLf, "</br>").ToString()
                CloseStream(resultDic)
                Exit Sub
            End If
            _jvController.ProcessUploadCsvLending(GetConnectionString(), mgr.DisbParamList)
        Catch ex As Exception
            CloseStream(resultDic)
            ShowMessage(lblMessage, ex.Message, True)
            Exit Sub
        End Try
        CloseStream(resultDic)
        Response.Redirect("BpkbAngsuranUpload.aspx?s=1")

    End Sub
    Sub CloseStream(resultDic As Dictionary(Of String, StreamReader))
        For Each reader In resultDic
            reader.Value.Close()
        Next
    End Sub

End Class