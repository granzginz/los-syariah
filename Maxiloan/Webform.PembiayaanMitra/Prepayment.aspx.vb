﻿Imports Maxiloan.Controller

Imports Maxiloan.Parameter
Imports Maxiloan.Webform.UserController

Public Class Prepayment
    Inherits Maxiloan.Webform.WebBased
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 5
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Protected WithEvents ucLendingAgreementList1 As ucLendingAgreementList
    Protected WithEvents ucKontrakDetail1 As ucKontrakDetail
    Protected WithEvents ucPerhitunganPelunasan1 As ucPerhitunganPelunasan
    Private octr As New MitraController
    Protected WithEvents txtJumlah As ucNumberFormat
    Private dtBankAccount As New DataTable
    Private m_controller As New DataUserControlController
    Protected WithEvents ucValueDate As ucDateCE

    Public Property CmdWhere() As String
        Get
            Return CStr(ViewState("CmdWhere"))
        End Get
        Set(ByVal Value As String)
            ViewState("CmdWhere") = Value
        End Set
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.FormID = "FPLENDING"
        lblMessage.Visible = False
        If Not Me.IsHoBranch Then
            ShowMessage(lblMessage, "Harap login dikantor pusat!", True)
            PanelEditor("1", False)
            Exit Sub
        Else
            If Not (Page.IsPostBack) Then
                Me.CmdWhere = "STATUS is Null"
                ucLendingAgreementList1.DoBind(Me.CmdWhere)
                PanelEditor("1", True)
                bindPaymentType()
                bindBankAccount("BA")
            End If
        End If

    End Sub

#Region "PanelEditor"
    Public Sub PanelEditor(Param As String, isVisible As Boolean)
        FirstPanelDiplay()

        Select Case Param
            Case "1"
                PnlForm1.Visible = isVisible
                BtnSearch.Text = "Search"
                BtnReset.Text = "Reset"
            Case "2"
                pnlForm2.Visible = isVisible
                pnlForm3.Visible = isVisible
                BtnSearch.Text = "Next"
                BtnReset.Text = "Cancel"
            Case "3"
                pnlForm4.Visible = isVisible
                BtnSearch.Text = "Save"
                BtnReset.Text = "Cancel"
                BtnPrint.Visible = True
        End Select
    End Sub

    Public Sub FirstPanelDiplay()
        Me.pnlForm2.Visible = False
        Me.pnlForm3.Visible = False
        Me.PnlForm1.Visible = False
        Me.pnlForm4.Visible = False
        BtnPrint.Visible = False

        ucLendingAgreementList1.FirstLoad()
        ucPerhitunganPelunasan1.FirstLoad()
    End Sub

#End Region

    Private Sub _gridClick(_paymentcalculateproperty As PaymentCalculateProperty) Handles ucLendingAgreementList1._gridItemCommand

        PanelEditor("2", True)

        ucKontrakDetail1.MFID = _paymentcalculateproperty.MFID
        ucKontrakDetail1.MFFacilityNo = _paymentcalculateproperty.MFFACILITYNO
        ucKontrakDetail1.MFBATCHNO = _paymentcalculateproperty.MFBATCHNO
        ucKontrakDetail1.MFAGREEMENTNO = _paymentcalculateproperty.MFAGREEMENTNO
        ucKontrakDetail1.PRINCIPALAMOUNT = _paymentcalculateproperty.PRINCIPALAMOUNT
        ucKontrakDetail1.TENOR = _paymentcalculateproperty.TENOR
        ucKontrakDetail1.INTERESTAMOUNT = _paymentcalculateproperty.INTERESTAMOUNT
        ucKontrakDetail1.OSPRINCIPAL = _paymentcalculateproperty.OSPRINCIPAL
        ucKontrakDetail1.RATE = _paymentcalculateproperty.RATE
        ucKontrakDetail1.OSINTEREST = _paymentcalculateproperty.OSINTEREST
        ucKontrakDetail1.STATUS = _paymentcalculateproperty.STATUS
        ucKontrakDetail1.PAIDSEQNO = _paymentcalculateproperty.PAIDSEQNO
        ucKontrakDetail1.DoBind()
    End Sub

    Private Sub _btnCalculate_Click() Handles ucPerhitunganPelunasan1._perhitunganPelunasan
        ucKontrakDetail1.DoBindPaymentCalculate()
    End Sub

    Private Sub _getdataPaymentCalculate_Click(_paymentcalculateproperty As PaymentCalculateProperty) Handles ucKontrakDetail1._dataPaymentCalculate
        ucPerhitunganPelunasan1.MFAGREEMENTNO = _paymentcalculateproperty.MFAGREEMENTNO
        ucPerhitunganPelunasan1.MFFacilityNo = _paymentcalculateproperty.MFFACILITYNO
        ucPerhitunganPelunasan1.MFBATCHNO = _paymentcalculateproperty.MFBATCHNO
        ucPerhitunganPelunasan1.MFID = _paymentcalculateproperty.MFID
        ucPerhitunganPelunasan1.DoHitungPelunasan()
    End Sub

    Public _agreement As String

    Private Sub _getAgreementDate(ByRef _paymentcalculateproperty As PaymentCalculateProperty) Handles ucKontrakDetail1._getPaymentAgreement
        _agreement = _paymentcalculateproperty.MFAGREEMENTNO
    End Sub

    Private Sub BtnSearch_Click(sender As Object, ByVal e As EventArgs) Handles BtnSearch.Click
        Select Case BtnSearch.Text
            Case "Search"
                ucLendingAgreementList1.DoSearch("AND STATUS IS NULL")
            Case "Reset"
                ucLendingAgreementList1.DoBind("STATUS IS NULL")
            Case "Next"
                PanelEditor("3", True)
                txtJumlah.Text = ucPerhitunganPelunasan1.CalculateProperty.totalpelunasan
            Case "Save"
                Dim property1 As New Parameter.PaymentCalculateProperty

                Dim odt As New DataTable
                odt.Columns.Add("mfid")
                odt.Columns.Add("mffacilityno")
                odt.Columns.Add("mfbatchno")
                odt.Columns.Add("account")
                odt.Columns.Add("amount")
                odt.Columns.Add("interestamount")
                Dim r = odt.NewRow
                r("mfid") = ucPerhitunganPelunasan1.CalculateProperty.MFID
                r("mffacilityno") = ucPerhitunganPelunasan1.CalculateProperty.MFFACILITYNO
                r("mfbatchno") = ucPerhitunganPelunasan1.CalculateProperty.MFBATCHNO
                r("account") = CInt(ucPerhitunganPelunasan1.CalculateProperty.PAIDSEQNO)
                r("amount") = CDbl(ucPerhitunganPelunasan1.CalculateProperty.totalpelunasan)
                r("interestamount") = CDbl(ucPerhitunganPelunasan1.CalculateProperty.INTERESTAMOUNT)
                odt.Rows.Add(r)

                With property1
                    .strConnection = Me.GetConnectionString
                    .DisbList = odt
                    .MFAGREEMENTNO = ucPerhitunganPelunasan1.CalculateProperty.MFAGREEMENTNO
                    .TANGGALEFEKTIF = ucPerhitunganPelunasan1.CalculateProperty.TANGGALEFEKTIF
                    .SISAPOKOK = ucPerhitunganPelunasan1.CalculateProperty.SISAPOKOK
                    .PAIDSEQNO = ucPerhitunganPelunasan1.CalculateProperty.PAIDSEQNO
                    .OSINTEREST = ucPerhitunganPelunasan1.CalculateProperty.INTERESTAMOUNT
                    .BUNGABERJALAN = ucPerhitunganPelunasan1.CalculateProperty.BUNGABERJALAN
                    .DENDAPERUSAHAAN = ucPerhitunganPelunasan1.CalculateProperty.DENDAPERUSAHAAN
                    .totalpelunasan = ucPerhitunganPelunasan1.CalculateProperty.totalpelunasan
                    .BranchId = Me.sesBranchId.Replace("'", "")
                    .LoginId = Me.Loginid
                    .BusinessDate = Me.BusinessDate
                    .DateValue = ucValueDate.GetDate
                    .RecipientName = cbowop.SelectedValue
                    .ReferenceNo = txtReferenceNo.Text
                    .BankAccountID = cmbBankAccount.SelectedValue
                    .Notes = txtKeterangan.Text
                End With

                Try
                    octr.paymentCalculateDML(property1)
                    PanelEditor("1", True)
                    ucLendingAgreementList1.DoBind("STATUS IS NULL")
                    PanelEditor("1", True)
                    ShowMessage(lblMessage, "Data saved!", False)
                Catch ex As Exception
                    ShowMessage(lblMessage, ex.Message, True)
                End Try
        End Select
    End Sub

    Private Sub BtnPrint_Click(sender As Object, ByVal e As EventArgs) Handles BtnPrint.Click

    End Sub

    Private Sub BtnReset_Click(sender As Object, ByVal e As EventArgs) Handles BtnReset.Click
        Select Case BtnReset.Text
            Case "Reset"
                ucLendingAgreementList1.DiReset()
            Case "Cancel"
                PanelEditor("1", True)
                ucLendingAgreementList1.DoBind("STATUS IS NULL")
                PanelEditor("1", True)
        End Select
    End Sub

    Private Sub bindPaymentType()
        Dim oDataTable As DataTable = New DataTable
        Dim oRow As DataRow
        Dim splitListData() As String
        Dim splitRow() As String
        Dim i As Integer
        Dim strListData As String

        oDataTable.Columns.Add("ID", System.Type.GetType("System.String"))
        oDataTable.Columns.Add("Description", System.Type.GetType("System.String"))

        oRow = oDataTable.NewRow()
        oRow("ID") = ""
        oRow("Description") = ""

        'only bank account
        strListData = "BA,Bank"
        splitListData = Split(strListData, "-")

        For i = 0 To UBound(splitListData)
            splitRow = Split(splitListData(i), ",")
            oRow("ID") = splitRow(0)
            oRow("Description") = splitRow(1)
            oDataTable.Rows.Add(oRow)
            oRow = oDataTable.NewRow()
        Next

        cbowop.DataValueField = "ID"
        cbowop.DataTextField = "Description"
        cbowop.DataSource = oDataTable
        cbowop.DataBind()
    End Sub

    Private Sub bindBankAccount(ByVal strBankType As String)
        dtBankAccount = m_controller.GetBankAccount(GetConnectionString, Me.sesBranchId, strBankType, "FD")
        cmbBankAccount.DataSource = dtBankAccount
        cmbBankAccount.DataTextField = "Name"
        cmbBankAccount.DataValueField = "ID"
        cmbBankAccount.DataBind()
    End Sub
End Class