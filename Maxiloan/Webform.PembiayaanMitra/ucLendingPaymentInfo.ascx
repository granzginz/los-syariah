﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucLendingPaymentInfo.ascx.vb" Inherits="Maxiloan.Webform.PembiayaanMitra.ucLendingPaymentInfo" %>
 
<div class="form_box_header">
    <div class="form_single">
        <h5>    
             
        </h5>
    </div>
</div>
<div class="form_box_header">
    <div class="form_single">
        <div class="grid_wrapper_ws">
            <asp:DataGrid ID="DtgLendingFacility" runat="server" AutoGenerateColumns="False" BorderWidth="0"
                   BorderStyle="None" CssClass="grid_general" DataKeyField="MFAgreementNo" AllowSorting="True">
                    <ItemStyle CssClass="item_grid"></ItemStyle>
                    <HeaderStyle CssClass="th"></HeaderStyle>
                    <Columns>
                        <asp:BoundColumn DataField="MFAgreementNo" SortExpression="MFAgreementNo" HeaderText="NO. AGREEMENT"></asp:BoundColumn> 
                        <asp:BoundColumn DataField="InsSeqNo" SortExpression="InsSeqNo" HeaderText="NO. INSSEQ"></asp:BoundColumn> 
                        <asp:BoundColumn DataField="DueDate" SortExpression="DueDate" HeaderText="DUE DATE" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
                        <asp:BoundColumn DataField="InstallmentAmount" SortExpression="InstallmentAmount" HeaderText="INSTALLMENT" DataFormatString="{0:N0}"></asp:BoundColumn>
                        <asp:BoundColumn DataField="PrincipalAmount" SortExpression="PrincipalAmount" HeaderText="PRINCIPLE" DataFormatString="{0:N0}"></asp:BoundColumn> 
                        <asp:BoundColumn DataField="InterestAmount" SortExpression="InterestAmount" HeaderText="INTEREST" DataFormatString="{0:N0}"></asp:BoundColumn>
                        <asp:BoundColumn DataField="LateChargesAmount" SortExpression="LateChargesAmount" HeaderText="LATE CHARGE" DataFormatString="{0:N0}"></asp:BoundColumn>
                    </Columns>
                </asp:DataGrid>                          
        </div>
        <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../Images/grid_navbutton01.png"
                        CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="First">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../Images/grid_navbutton02.png"
                        CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Prev">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../Images/grid_navbutton03.png"
                        CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Next">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                        CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Last">
                    </asp:ImageButton>
                    Page&nbsp;
                    <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                    <asp:Button ID="imgbtnPageNumb" runat="server" CssClass="small buttongo blue" Text="Go"
                        EnableViewState="False"></asp:Button>
                    <asp:RangeValidator ID="rgvGo" runat="server" ErrorMessage="No Halaman Salah" ControlToValidate="txtPage"
                        Type="Integer" MaximumValue="999999999" MinimumValue="1" CssClass="validator_general"
                        Display="Dynamic"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" Display="Dynamic"
                        ErrorMessage="No Halaman Salah" ControlToValidate="txtPage" CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>
                         
                <asp:Label ID="lblPage" runat="server"></asp:Label>of 
                <asp:Label ID="lblTotPage" runat="server"></asp:Label> 
                <div class="label_gridnavigation">Total
                <asp:Label ID="lblrecord" runat="server"></asp:Label> record(s)
            </div>  
    </div>
</div>

