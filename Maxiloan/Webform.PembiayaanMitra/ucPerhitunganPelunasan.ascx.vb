﻿Imports Maxiloan.Parameter
Imports Maxiloan.Webform.UserController

Public Class ucPerhitunganPelunasan
    Inherits Maxiloan.Webform.ControlBased
    Protected WithEvents ucTglEfektif As ucDateCE
    Public Event _getParameter(ByRef _paymentcalculateproperty As PaymentCalculateProperty)
    Public Event _getParameterUdt(ByRef _paymentcalculateproperty As PaymentCalculateProperty)
    Public Event _perhitunganPelunasan()
    Public Property MFID() As String
    Private oLendingRateController As New Controller.MitraController
    Public Property MFFacilityNo() As String
    Public Property MFBATCHNO() As String
    Public Property MFAGREEMENTNO() As String
    Public Property ACCOUNT() As String

    Public Property CalculateProperty As Parameter.PaymentCalculateProperty
        Get
            Return CType(ViewState("PaymentCalculateProperty"), Parameter.PaymentCalculateProperty)
        End Get
        Set(value As Parameter.PaymentCalculateProperty)
            ViewState("PaymentCalculateProperty") = value
        End Set
    End Property
    Public Sub DoHitungPelunasan()
        DoBindCalculate(CalculateProperty)

        Try
            CalculateProperty = oLendingRateController.paymentCalculate(CalculateProperty)

            With CalculateProperty
                txtPokok.Text = FormatNumber(.SISAPOKOK, 0)
                txtBerjalan.Text = FormatNumber(.BUNGABERJALAN, 0)
                txtPerusahaan.Text = FormatNumber(.DENDAPERUSAHAAN, 0)
                lbltotal.Text = FormatNumber(.SISAPOKOK + .BUNGABERJALAN + .DENDAPERUSAHAAN, 0)
                RaiseEvent _getParameter(CalculateProperty)
            End With
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub

    Public Function DoBindCalculate(ByRef item As Parameter.PaymentCalculateProperty)
        Dim result As String = ""
        item = New Parameter.PaymentCalculateProperty

        With item
            .strConnection = Me.GetConnectionString
            .MFID = Me.MFID
            .MFAGREEMENTNO = Me.MFAGREEMENTNO
            .MFFACILITYNO = Me.MFFacilityNo
            .MFBATCHNO = Me.MFBATCHNO
            .TANGGALEFEKTIF = ucTglEfektif.GetDate 'ConvertDate2(txtEfektif.Text)            
        End With
        Return result
    End Function

    Public Sub FirstLoad()
        txtBerjalan.Text = ""
        ucTglEfektif.Text = ""
        txtPerusahaan.Text = ""
        txtPokok.Text = ""
        lbltotal.Text = ""
        ucTglEfektif.IsRequired = True
        ucTglEfektif.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
    End Sub

    Private Sub BtnHitung_Click(sender As Object, ByVal e As EventArgs) Handles BtnHitung.Click
        RaiseEvent _perhitunganPelunasan()
    End Sub

    Public Sub DoBindParameter(ByRef property1 As Parameter.PaymentCalculateProperty)
        property1 = New Parameter.PaymentCalculateProperty
        Dim odt As New DataTable
        odt.Columns.Add("mfid")
        odt.Columns.Add("mffacilityno")
        odt.Columns.Add("mfbatchno")
        odt.Columns.Add("account")
        odt.Columns.Add("amount")
        odt.Columns.Add("interestamount")

        Dim r = odt.NewRow
        r("mfid") = Me.MFAGREEMENTNO
        r("mffacilityno") = Me.MFFacilityNo
        r("mfbatchno") = Me.MFBATCHNO
        r("account") = CInt(Me.ACCOUNT)
        r("amount") = CDbl(txtPokok.Text.Trim)
        r("interestamount") = CDbl(txtBerjalan.Text.Trim)
        odt.Rows.Add(r)

        property1.disburselist = odt
        RaiseEvent _getParameterUdt(property1)
    End Sub
End Class