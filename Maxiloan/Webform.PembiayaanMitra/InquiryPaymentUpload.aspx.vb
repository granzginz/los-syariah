﻿Imports Maxiloan.Controller
Imports Maxiloan.Controller.Lending

Public Class InquiryPaymentUpload
    Inherits Maxiloan.Webform.WebBased
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 5
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    'Protected WithEvents ucStartDate As ucDateCE
    'Protected WithEvents ucEndDate As ucDateCE
    'Protected WithEvents ucLateCharges As ucNumberFormat
    'Protected WithEvents ucPenalty As ucNumberFormat
    'Protected WithEvents ucTglFasilitas As ucDateCE
    'Protected WithEvents ucMinDrawdownAmount As ucNumberFormat


    Public Property CmdWhere() As String
        Get
            Return CStr(ViewState("CmdWhere"))
        End Get
        Set(ByVal Value As String)
            ViewState("CmdWhere") = Value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.FormID = "FLUPAY"

        If Not (Page.IsPostBack) Then
            Me.CmdWhere = ""
            DoBind(Me.SortBy, Me.CmdWhere)
            'btnCancel.CausesValidation = False
        End If
    End Sub

    Private Sub DoBind(ByVal strSort As String, ByVal cmdWhere As String)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim cContract As New GeneralPagingController
        Dim oContract As New Parameter.GeneralPaging

        Me.CmdWhere = ""

        With oContract
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = ""
            .SpName = "splendingpaymentuploadlist"
        End With

        oContract = cContract.GetGeneralPaging(oContract)

        With oContract
            lblrecord.Text = CType(.TotalRecords, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecords
        End With

        dtsEntity = oContract.ListData
        dtvEntity = dtsEntity.DefaultView
        dtvEntity.Sort = Me.SortBy

        If dtvEntity.Count <= 0 Then
            'imgPrint.Enabled = False
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
        Else
            'imgPrint.Enabled = True
        End If

        DtgLendingFacility.DataSource = dtvEntity
        Try
            DtgLendingFacility.DataBind()
        Catch
            DtgLendingFacility.CurrentPageIndex = 0
            DtgLendingFacility.DataBind()
        End Try
        PagingFooter()
    End Sub

#Region "Navigation"
    Private Sub PagingFooter()
        'lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
                'Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
                'Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SortBy, Me.CmdWhere)
    End Sub
    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles imgbtnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SortBy, Me.CmdWhere)
            End If
        End If
    End Sub

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles DtgLendingFacility.SortCommand
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
            DoBind(e.SortExpression, Me.CmdWhere)
        Else
            Me.SortBy = e.SortExpression + " DESC"
            DoBind(e.SortExpression + " DESC", Me.CmdWhere)
        End If
    End Sub

    'Private Sub BtnAddNew_Click(sender As Object, e As EventArgs) Handles BtnAddNew.Click
    '    pnlList.Visible = False
    '    'pnlAddEdit.Visible = True
    '    'lblMenuAddEdit.Text = "ADD"
    '    'lblMenuAddEdit.Visible = True
    'End Sub

    'Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
    '    Response.Redirect("LendingFacility.aspx")
    'End Sub

    Private Sub BtnSearch_Click(sender As Object, e As EventArgs) Handles BtnSearch.Click

        If (txtSearchBy.Text.Length > 0) Then
            Me.CmdWhere = String.Format("{0} LIKE '%{1}%'", cboSearchBy.Text, txtSearchBy.Text)
        End If

        DoBind(Me.SortBy, Me.CmdWhere)
    End Sub

    Private Sub DtgLendingFacility_ItemCommand(source As Object, e As DataGridCommandEventArgs) Handles DtgLendingFacility.ItemCommand
        lblMessage.Visible = False

        Dim tempLblMFCode As String = CType(DtgLendingFacility.Items(e.Item.ItemIndex).FindControl("lblMFCode"), Label).Text
        Dim tempLblFacilityNo As String = CType(DtgLendingFacility.Items(e.Item.ItemIndex).FindControl("lblFacilityNo"), Label).Text

        'Response.Redirect("LendingRate.aspx?MFCode=" & tempLblMFCode & "&FacilityNo=" & tempLblFacilityNo)
    End Sub

#End Region
End Class