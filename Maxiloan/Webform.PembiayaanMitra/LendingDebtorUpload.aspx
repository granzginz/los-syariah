﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="LendingDebtorUpload.aspx.vb" Inherits="Maxiloan.Webform.PembiayaanMitra.LendingDebtorUpload" %>

<%@ Import Namespace="System.IO" %>
<!DOCTYPE html>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Lending Debtor Upload</title>
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
    <script type="text/javascript" src="../js/jquery-1.9.1.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" />
        <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
        <div class="form_title">
            <div class="title_strip"></div>
            <div class="form_single">
                <h3>Upload Aplikasi</h3>
            </div>
        </div>




        <div class="form_box">
            <div class="form_single">
                <label>Pilih Files :</label>
                <asp:FileUpload ID="flUpload" runat="server" Multiple="Multiple" onchange="getfilename(this);" />

            </div>
        </div>
        
        <div class="form_box" runat="server" id="divinvldmsg" Visible="false">
            <div class="form_single validator_general">
                
                    <asp:Label ID="lblInvalidMsg" runat="server" ></asp:Label>
                
            </div>
        </div>
        <div id="topic-progress-wrapper"></div>

        <div class="form_button">
            <asp:Button ID="btnFind" runat="server" Text="Upload" CssClass="small button green" />
        </div>
        

        <script>
            function getfilename(t) {
                var l = '';
                var res = '';
                for (var i = 0; i < t.files.length - 1; i++) {
                    l += '<li class="avatars" >' + t.files[i].name + ' </li>';
                }

                if (l != '') res = '<ul id="discussion">' + l + '</ul>';
                $('#topic-progress-wrapper').empty().append(res);
            }
        </script>
    </form>
</body>
</html>
