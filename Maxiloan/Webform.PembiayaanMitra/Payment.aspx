﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Payment.aspx.vb" Inherits="Maxiloan.Webform.PembiayaanMitra.Payment" %>

<!DOCTYPE html>

<%--<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>--%>
<%@ Register TagPrefix="uc1" TagName="ucLendingPaymentInfo" Src="ucLendingPaymentInfo.ascx" %>
<%@ Register TagPrefix="uc2" TagName="ucLendingPaymentHeader" Src="ucLendingPaymentHeader.ascx" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="Stylesheet" type="text/css" href="../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../Include/Buttons.css" />
    <script src="../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server" ID="ScriptManager1">
        </asp:ScriptManager>
        <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>LENDING PAYMENT
                </h3>
            </div>
        </div>
        <asp:Panel ID="PnlForm1" runat="server">
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ws">
                        <asp:DataGrid ID="DtgLendingFacility" runat="server" AutoGenerateColumns="False" BorderWidth="0"
                            OnSortCommand="SortGrid" BorderStyle="None" CssClass="grid_general">
                            <ItemStyle CssClass="item_grid"></ItemStyle>
                            <HeaderStyle CssClass="th"></HeaderStyle>
                            <Columns>
                                <asp:TemplateColumn HeaderText="">
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    <ItemTemplate>
                                        <%--<asp:ImageButton ID="ImbEdit" runat="server" text="Payment" CommandName="EDIT" CausesValidation="False"></asp:ImageButton>--%>
                                        <asp:Button ID="ImbEdit" runat="server" Text="Payment" CausesValidation="false" BorderStyle="None" BorderColor="Transparent"/>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblMFID" runat="server" Text='<%#Container.DataItem("MFID")%>'></asp:Label>
                                        <asp:Label ID="lblReferenceNo" runat="server"  Text='<%#Container.DataItem("ReferenceNo")%>'></asp:Label>
                                        <asp:Label ID="lblMultiFinanceName" runat="server"  Text='<%#Container.DataItem("MultiFinanceName")%>'></asp:Label>
                                        <asp:Label ID="lblPaymentDate" runat="server" Text='<%#Container.DataItem("PaymentDate")%>'></asp:Label>
                                        <asp:Label ID="lblTransactionType" runat="server" Text='<%#Container.DataItem("TransactionType")%>'></asp:Label>
                                        <asp:Label ID="lblFacilityNo" runat="server" Text='<%#Container.DataItem("MFFacilityNo")%>'></asp:Label>
                                        <asp:Label ID="lblFacilityType" runat="server" Text='<%#Container.DataItem("FacilityType")%>'></asp:Label>
                                        <asp:Label ID="lblTotalAccount" runat="server" Text='<%#Container.DataItem("TotalAccount")%>'></asp:Label>
                                        <asp:Label ID="lblTotalAmount" runat="server" Text='<%#Container.DataItem("TotalAmount")%>'></asp:Label>
                                        <asp:Label ID="lblStatus" runat="server" Text='<%#Container.DataItem("Status")%>'></asp:Label>
                                        <asp:Label ID="lblMFAgreementNo" runat="server" Text='<%#Container.DataItem("MFAgreementNo")%>'></asp:Label>
                                        <asp:Label ID="lblDebtorName" runat="server" Text='<%#Container.DataItem("DebtorName")%>'></asp:Label>
                                        <asp:Label ID="lblInsSeqNo" runat="server" Text='<%#Container.DataItem("InsSeqNo")%>'></asp:Label>
                                        <asp:Label ID="lblDueDate" runat="server" Text='<%#Container.DataItem("DueDate")%>'></asp:Label>
                                        <asp:Label ID="lblInstallmentAmount" runat="server" Text='<%#Container.DataItem("InstallmentAmount")%>'></asp:Label>
                                        <asp:Label ID="lblPrincipalAmount" runat="server" Text='<%#Container.DataItem("PrincipalAmount")%>'></asp:Label>
                                        <asp:Label ID="lblInterestAmount" runat="server" Text='<%#Container.DataItem("InterestAmount")%>'></asp:Label>
                                        <asp:Label ID="lblLateChargesAmount" runat="server" Text='<%#Container.DataItem("LateChargesAmount")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn DataField="MFID" SortExpression="MFID" HeaderText="KODE MF"></asp:BoundColumn>
                                <asp:BoundColumn DataField="MFFacilityNo" SortExpression="MFFacilityNo" HeaderText="NO. FASILITY"></asp:BoundColumn>
                                <asp:BoundColumn DataField="ReferenceNo" SortExpression="ReferenceNo" HeaderText="NO. REFERENCE"></asp:BoundColumn>
                                <asp:BoundColumn DataField="DueDate" SortExpression="DueDate" HeaderText="DUE DATE" DataFormatString="{0:MM/dd/yyyy}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="InstallmentAmount" SortExpression="InstallmentAmount" HeaderText="ANGSURAN" DataFormatString="{0:N0}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="Status" SortExpression="Status" HeaderText="STATUS"></asp:BoundColumn>
                            </Columns>
                        </asp:DataGrid>
                    </div>
                    <div class="button_gridnavigation">
                                        <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../Images/grid_navbutton01.png"
                                            CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="First">
                                        </asp:ImageButton>
                                        <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../Images/grid_navbutton02.png"
                                            CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Prev">
                                        </asp:ImageButton>
                                        <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../Images/grid_navbutton03.png"
                                            CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Next">
                                        </asp:ImageButton>
                                        <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                                            CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Last">
                                        </asp:ImageButton>
                                        Page&nbsp;
                                        <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                                        <asp:Button ID="imgbtnPageNumb" runat="server" CssClass="small buttongo blue" Text="Go"
                                            EnableViewState="False"></asp:Button>
                                        <asp:RangeValidator ID="rgvGo" runat="server" ErrorMessage="No Halaman Salah" ControlToValidate="txtPage"
                                            Type="Integer" MaximumValue="999999999" MinimumValue="1" CssClass="validator_general"
                                            Display="Dynamic"></asp:RangeValidator>
                                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" Display="Dynamic"
                                            ErrorMessage="No Halaman Salah" ControlToValidate="txtPage" CssClass="validator_general"></asp:RequiredFieldValidator>
                                    </div>
                         
                                    <asp:Label ID="lblPage" runat="server"></asp:Label>of 
                                    <asp:Label ID="lblTotPage" runat="server"></asp:Label> 
                                    <div class="label_gridnavigation">Total
                                    <asp:Label ID="lblrecord" runat="server"></asp:Label> record(s)                         
                            </div>
                </div>
            </div>
            <div class="form_title">
                <div class="form_single">
                    <h5>CARI FASILITAS                  
                    </h5>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Cari Berdasarkan</label>
                    <asp:DropDownList ID="cboSearchBy" runat="server">
                        <asp:ListItem Value="a.MFID" Selected="True">Kode MF</asp:ListItem>
                        <asp:ListItem Value="a.ReferenceNo">No. Reference</asp:ListItem>
                        <asp:ListItem Value="MFAgreementNo">No. Agreement</asp:ListItem>
                        <asp:ListItem Value="DebtorName">Nama</asp:ListItem>
                    </asp:DropDownList>
                    <asp:TextBox ID="txtSearchBy" runat="server" Width="250px"></asp:TextBox>
                </div>
            </div>
        </asp:Panel>

        <asp:Panel ID="PnlForm2" runat="server">
            <div class="form_box">
                <div class="form_left">
                    <label>NO. MF</label>
                    <asp:label id="txtMFID" runat="server" Font-Bold="true"/>
                </div>
                 <div class="form_right">
                    <label>NO. Facility</label>
                     <asp:label id="txtMFFacilityNo" runat="server"/>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>Total Account</label>
                    <asp:label id="txtTotalAccount" runat="server"/>
                </div>
                <div class="form_right">
                    <label>NO. Reference</label>
                    <asp:label id="txtReferenceNo" runat="server"/>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>Payment Date</label>
                    <asp:label id="txtPaymentDate" runat="server"/>
                </div>
                <div class="form_right">
                    <label>Total Ammount</label>
                    <asp:label id="txtTotalAmount" runat="server"/>
                </div>
            </div>
        </asp:Panel>

        <asp:Panel ID="PnlForm3" runat="server">
             <uc1:ucLendingPaymentInfo ID="ucLendingPaymentInfo1" runat="server"></uc1:ucLendingPaymentInfo>
        </asp:Panel>

        <asp:Panel ID="pnlForm4" runat="server">
             <uc2:ucLendingPaymentHeader ID="ucLendingPaymentHeader1" runat="server"></uc2:ucLendingPaymentHeader>
        </asp:Panel>

        <div class="form_button">
            <asp:Button ID="BtnSearch" runat="server" CausesValidation="False" Text="Find"
                CssClass="small button blue"></asp:Button>
            <asp:Button ID="BtnReset" runat="server" CausesValidation="False" Text="Reset"
                CssClass="small button gray"></asp:Button>
        </div>
    </form>
</body>
</html>
