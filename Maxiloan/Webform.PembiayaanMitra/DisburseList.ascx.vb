﻿Imports Maxiloan.Controller

Public Class DisburseList
    Inherits Maxiloan.Webform.ControlBased

    Private octr As New MitraController
    Public Property mfid As String
        Get
            Return ViewState("mfid").ToString
        End Get
        Set(value As String)
            ViewState("mfid") = value
        End Set
    End Property
    Public Property mffacilityno As String
        Get
            Return ViewState("mffacilityno").ToString
        End Get
        Set(value As String)
            ViewState("mffacilityno") = value
        End Set
    End Property
    Public Property mfbatchno As String
        Get
            Return ViewState("mfbatchno").ToString
        End Get
        Set(value As String)
            ViewState("mfbatchno") = value
        End Set
    End Property
    Public Property showcheckbox As Boolean
        Get
            Return CType(ViewState("showcheckbox"), Boolean)
        End Get
        Set(value As Boolean)
            ViewState("showcheckbox") = value
        End Set
    End Property

    Public Property showbutton As Boolean
        Get
            Return CType(ViewState("showbutton"), Boolean)
        End Get
        Set(value As Boolean)
            ViewState("showbutton") = value
        End Set
    End Property

    Public Property showradio As Boolean
        Get
            Return CType(ViewState("showradio"), Boolean)
        End Get
        Set(value As Boolean)
            ViewState("showradio") = value
        End Set
    End Property
    Public Sub doBind()
        dtg1.Columns(0).Visible = showcheckbox
        dtg1.Columns(1).Visible = showradio
        dtg1.Columns(2).Visible = showbutton

        Dim oclass As New Parameter.Lending

        With oclass
            .strConnection = GetConnectionString()
        End With

        Try
            oclass = octr.disburselist(oclass)
            dtg1.DataSource = oclass.disburselist
            dtg1.DataBind()
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub

    Public Function getchecked() As Parameter.Lending
        Dim ocls As New Parameter.Lending
        Dim odt As New DataTable
        odt.Columns.Add("mfid")
        odt.Columns.Add("mffacilityno")
        odt.Columns.Add("mfbatchno")
        odt.Columns.Add("account")
        odt.Columns.Add("amount")
        odt.Columns.Add("interestamount")
        For Each item As DataGridItem In dtg1.Items
            If item.ItemType = ListItemType.Item Or item.ItemType = ListItemType.AlternatingItem Or item.ItemType = ListItemType.EditItem Then
                If CType(item.FindControl("chk"), CheckBox).Checked Then
                    Dim r = odt.NewRow
                    ocls.totalaccounts += CDbl(item.Cells(7).Text)
                    ocls.disburseamount += CDbl(item.Cells(8).Text)
                    r("mfid") = item.Cells(3).Text.Trim
                    r("mffacilityno") = item.Cells(4).Text.Trim
                    r("mfbatchno") = item.Cells(5).Text.Trim
                    r("account") = CInt(item.Cells(7).Text.Trim)
                    r("amount") = CDbl(item.Cells(8).Text.Trim)
                    r("interestamount") = CDbl(item.Cells(9).Text.Trim)
                    odt.Rows.Add(r)
                End If
            End If
        Next
        ocls.disburselist = odt
        Return ocls
    End Function
End Class