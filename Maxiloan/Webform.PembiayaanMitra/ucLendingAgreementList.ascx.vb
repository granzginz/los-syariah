﻿Imports Maxiloan.Controller
Imports Maxiloan.Parameter

Public Class ucLendingAgreementList
    Inherits Maxiloan.Webform.ControlBased

    Public Event _gridItemCommand(_entityLendingAgreement1 As PaymentCalculateProperty)
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 5
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Public Property CmdWhere() As String

        Get
            Return CStr(ViewState("CmdWhere"))
        End Get
        Set(ByVal Value As String)
            ViewState("CmdWhere") = Value
        End Set

    End Property

    Public Sub DiReset()
        txtSearchBy.Text = String.Empty
    End Sub

    Public Sub DoSearch(ByVal strSearch As String)
        If (txtSearchBy.Text.Length > 0) Then
            Me.CmdWhere = String.Format("{0} LIKE '%{1}%'", cboSearchBy.Text, txtSearchBy.Text) & strSearch
        Else
            Me.CmdWhere = "1=1 " & strSearch
        End If
        DoBind(Me.CmdWhere)
    End Sub

    Public Sub DoBind(ByVal cmdWhere As String)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim cContract As New GeneralPagingController
        Dim oContract As New Parameter.GeneralPaging

        'Me.CmdWhere = ""

        With oContract
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = "" 'strSort
            .SpName = "splendingagreementlist"
        End With

        oContract = cContract.GetGeneralPaging(oContract)

        With oContract
            lblrecord.Text = CType(.TotalRecords, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecords
        End With

        dtsEntity = oContract.ListData
        dtvEntity = dtsEntity.DefaultView
        dtvEntity.Sort = Me.SortBy

        If dtvEntity.Count <= 0 Then
            'imgPrint.Enabled = False
            'ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
        Else
            'imgPrint.Enabled = True
        End If

        DtgLendingFacility.DataSource = dtvEntity
        Try
            DtgLendingFacility.DataBind()
        Catch
            DtgLendingFacility.CurrentPageIndex = 0
            DtgLendingFacility.DataBind()
        End Try
        PagingFooter()
    End Sub

#Region "Navigation"
    Private Sub PagingFooter()
        'lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            'ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
                'Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
                'Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind("") 'DoBind(Me.SortBy, Me.CmdWhere)
    End Sub
    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles imgbtnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind("") 'DoBind(Me.SortBy, Me.CmdWhere)
            End If
        End If
    End Sub

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles DtgLendingFacility.SortCommand
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
            DoBind("") 'DoBind(e.SortExpression, Me.CmdWhere)
        Else
            Me.SortBy = e.SortExpression + " DESC"
            DoBind("") 'DoBind(e.SortExpression + " DESC", Me.CmdWhere)
        End If
    End Sub

    Private Sub DtgLendingFacility_ItemCommand(source As Object, e As DataGridCommandEventArgs) Handles DtgLendingFacility.ItemCommand

        Select Case e.CommandName
            Case "CALCULATE"
                Dim item = New PaymentCalculateProperty()

                item.MFID = CType(DtgLendingFacility.Items(e.Item.ItemIndex).FindControl("lblMFID"), Label).Text
                item.MFFACILITYNO = CType(DtgLendingFacility.Items(e.Item.ItemIndex).FindControl("lblMFFacilityNo"), Label).Text
                item.MFBATCHNO = CType(DtgLendingFacility.Items(e.Item.ItemIndex).FindControl("lblmfbatchno"), Label).Text
                item.MFAGREEMENTNO = CType(DtgLendingFacility.Items(e.Item.ItemIndex).FindControl("lblmfagreementno"), Label).Text
                item.PRINCIPALAMOUNT = CType(DtgLendingFacility.Items(e.Item.ItemIndex).FindControl("lblprincipalamount"), Label).Text
                item.TENOR = CType(DtgLendingFacility.Items(e.Item.ItemIndex).FindControl("lbltenor"), Label).Text
                item.INTERESTAMOUNT = CType(DtgLendingFacility.Items(e.Item.ItemIndex).FindControl("lblinterestamount"), Label).Text
                item.OSPRINCIPAL = CType(DtgLendingFacility.Items(e.Item.ItemIndex).FindControl("lblosprincipal"), Label).Text
                item.RATE = CType(DtgLendingFacility.Items(e.Item.ItemIndex).FindControl("lblrate"), Label).Text
                item.OSINTEREST = CType(DtgLendingFacility.Items(e.Item.ItemIndex).FindControl("lblosinterest"), Label).Text
                item.STATUS = CType(DtgLendingFacility.Items(e.Item.ItemIndex).FindControl("lblstatus"), Label).Text
                item.PAIDSEQNO = CType(DtgLendingFacility.Items(e.Item.ItemIndex).FindControl("lblpaidseqno"), Label).Text
                RaiseEvent _gridItemCommand(item)
        End Select
    End Sub

#End Region

    Public Sub FirstLoad()
        txtPage.Text = "1"
        txtSearchBy.Text = ""
    End Sub

    Private Sub cboSearchBy_DataBinding(sender As Object, e As EventArgs) Handles cboSearchBy.DataBinding

    End Sub
End Class