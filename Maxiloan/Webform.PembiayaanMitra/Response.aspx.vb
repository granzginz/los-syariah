﻿Public Class Response
    Inherits Maxiloan.Webform.WebBased

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ShowMessage(lblMessage, Replace(Request("notif"), "ENTER", "<br>"), CType(Request("iserror"), Boolean))
    End Sub

    Private Sub btnback_Click(sender As Object, e As EventArgs) Handles btnback.Click
        Response.Redirect(Request("callerurl"))
    End Sub

End Class