﻿Imports Maxiloan.Controller.Lending
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCacheHelper
Public Class PaymentByDuedate
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents ucstartdate As ucDateCE
    Protected WithEvents ucenddate As ucDateCE
    Protected WithEvents uctanggalvaluta As ucDateCE
    Private octr As New MitraController
    Private Property paymentinfo As DataSet
        Get
            Return CType(ViewState("lending"), DataSet)
        End Get
        Set(value As DataSet)
            ViewState("lending") = value
        End Set
    End Property
    Private Property ListBankAccount() As DataTable
        Get
            Return (CType(ViewState("ListBankAccount"), DataTable))
        End Get
        Set(ByVal Value As DataTable)
            ViewState("ListBankAccount") = Value
        End Set
    End Property
    Private m_controller As New DataUserControlController
    Private dtBankAccount As New DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Visible = False
        If SessionInvalid() Then Exit Sub
        If Page.IsPostBack Then
        Else
            If Not Me.IsHoBranch Then
                ShowMessage(lblMessage, "Harap login dikantor pusat!", True)
                pnl1.Visible = False
                pnl2.Visible = False
                pnl3.Visible = False
                Exit Sub
            Else
                pnl1.Visible = True
                pnl2.Visible = False
                pnl3.Visible = False
                bindPaymentType()
                bindBankAccount("BA")
            End If

        End If
    End Sub

    Private Sub btnNext_Click(sender As Object, e As EventArgs) Handles btnNext.Click
        pnl1.Visible = False
        pnl2.Visible = False
        pnl3.Visible = True
        txtJumlah.Text = FormatNumber(paymentinfo.Tables(0).Rows(0)("installmentamount"), 0)
        uctanggalvaluta.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
    End Sub
    Sub displayinstallmentinfo()
        If paymentinfo.Tables(0).Rows.Count = 0 Then
            btnNext.Visible = False
            Exit Sub
        Else
            With paymentinfo.Tables(0)
                lblacc.Text = FormatNumber(.Rows(0)("acc"), 0)
                lblinstallmentamount.Text = FormatNumber(.Rows(0)("installmentamount"), 0)
                lblprincipalamount.Text = FormatNumber(.Rows(0)("principalamount"), 0)
                lblinterestamount.Text = FormatNumber(.Rows(0)("interestamount"), 0)
            End With
            btnNext.Visible = True
        End If
    End Sub
    Private Sub btnFind_Click(sender As Object, e As EventArgs) Handles btnFind.Click
        Dim oclass As New Parameter.Lending
        With oclass
            .strConnection = Me.GetConnectionString
            .duedatestart = ConvertDate2(ucstartdate.Text)
            .duedateend = ConvertDate2(ucenddate.Text)
            .isinstallmentpaid = 0
        End With
        Try
            octr.lendinginstallmentinfo(oclass)
            Me.paymentinfo = oclass.installmentinfo
            pnl1.Visible = True
            pnl2.Visible = True
            pnl3.Visible = False
            displayinstallmentinfo()
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim oclass As New Parameter.Lending
        With oclass
            .strConnection = Me.GetConnectionString
            .mfid = cbomf.SelectedValue.Trim
            .mffacilityno = cbofacility.SelectedValue.Trim
            .paymentreferenceno = ""
            .BusinessDate = Me.BusinessDate
            .ValueDate = ConvertDate2(uctanggalvaluta.Text)
            .wop = cbowop.SelectedValue
            .bankaccountid = cmbBankAccount.SelectedValue.Trim
            .referenceno = txtReferenceNo.Text.Trim
            .LoginId = Me.Loginid
            .paymentnotes = txtKeterangan.Text.Trim
            .paymentlist = Me.paymentinfo.Tables(1)
        End With

        Try
            octr.lendingpayment(oclass)
            Response.Redirect("response.aspx?notif=Done&iserror=0&callerurl=paymentbyduedate.aspx")
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

    Private Sub bindPaymentType()
        Dim oDataTable As DataTable = New DataTable
        Dim oRow As DataRow
        Dim splitListData() As String
        Dim splitRow() As String
        Dim i As Integer
        Dim strListData As String

        oDataTable.Columns.Add("ID", System.Type.GetType("System.String"))
        oDataTable.Columns.Add("Description", System.Type.GetType("System.String"))

        oRow = oDataTable.NewRow()
        oRow("ID") = ""
        oRow("Description") = ""

        'only bank account
        strListData = "BA,Bank"
        splitListData = Split(strListData, "-")

        For i = 0 To UBound(splitListData)
            splitRow = Split(splitListData(i), ",")
            oRow("ID") = splitRow(0)
            oRow("Description") = splitRow(1)
            oDataTable.Rows.Add(oRow)
            oRow = oDataTable.NewRow()
        Next

        cbowop.DataValueField = "ID"
        cbowop.DataTextField = "Description"
        cbowop.DataSource = oDataTable
        cbowop.DataBind()
        'paymentTypeDropDownList.Items.Insert(0, "Select One")
        'paymentTypeDropDownList.Items(0).Value = "0"
    End Sub
    Private Sub bindBankAccount(ByVal strBankType As String)
        dtBankAccount = m_controller.GetBankAccount(GetConnectionString, Me.sesBranchId, strBankType, "FD")
        cmbBankAccount.DataSource = dtBankAccount
        cmbBankAccount.DataTextField = "Name"
        cmbBankAccount.DataValueField = "ID"
        cmbBankAccount.DataBind()
    End Sub
End Class