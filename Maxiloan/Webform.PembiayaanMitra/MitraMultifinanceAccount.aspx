﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="MitraMultifinanceAccount.aspx.vb"
    Inherits="Maxiloan.Webform.Lending.MitraMultifinanceAccount" EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ValidDate" Src="../Webform.UserController/ValidDate.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcBankAccount" Src="../Webform.UserController/UcBankAccount.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcBranchAll" Src="../webform.UserController/UcBranchAll.ascx" %>
<%@ Register Src="../Webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat"
    TagPrefix="uc2" %>
<%@ Register src="../Webform.UserController/ucDateCE.ascx" tagname="ucDateCE" tagprefix="uc3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>BranchEmployee</title>
    <link href="../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <script src="../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
    <link href="../include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;
        var AppInfo = window.location.pathname;
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
        var ServerName = window.location.protocol + "//" + window.location.host + '/'; 
    </script>
    <script language="javascript" type="text/javascript">

        // function fconfirm() {
        //            if (window.confirm("apakah yakin mau hapus data ini ?"))
        //                return true;
        //            else
        //                return false;

        //        }

        //        function fback() {
        //            history.go(-1);
        //            return false;
        //        }

        //        function openwindowbranch(pbranchid) {
        //            var x = screen.width; var y = screen.height - 100;
        //            window.open(servername + app + '/webform.setup/organization/branchview.aspx?branchid=' + pbranchid + '&style=setting', 'branchview', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        //        }

        //        function openwindowemployee(pbranchid, pemployeeid) {
        //            var x = screen.width; var y = screen.height - 100;
        //            window.open('employeeview.aspx?branchid=' + pbranchid + '&employeeid=' + pemployeeid + '&style=setting', 'employeeview', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        //        }


        
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    DAFTAR REKENING
                </h3>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtg" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="Sorting" BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="EDIT">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImbEdit" runat="server" ImageUrl="../Images/iconedit.gif"
                                        CommandName="Edit"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DELETE">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImbDelete" runat="server" ImageUrl="../Images/icondelete.gif"
                                        CommandName="Delete" CausesValidation="False"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn Visible="False" DataField="MfCode"></asp:BoundColumn>
                            <asp:TemplateColumn SortExpression="MfCode" HeaderText="MF CODE">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkMitraID" runat="server" Enabled="True" Text='<%# DataBinder.Eval(Container, "DataItem.MfCode")%>'>
                                    </asp:LinkButton>
                                    <asp:Label ID="lblMitraID" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.MfCode")%>'>
                                    </asp:Label>
                                    <asp:Label ID="lblBankID" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.BankID")%>'>
                                    </asp:Label>
                                    <asp:Label ID="lblBankBranchID" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.BranchCode")%>'>
                                    </asp:Label>
                                    <asp:Label ID="lblBankName" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.AccountName")%>'>
                                    </asp:Label>
                                    <asp:Label ID="lblBankBranchName" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.BankBranchName")%>'>
                                    </asp:Label>
                                    <asp:Label ID="lblAccountNo" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.AccountNo")%>'>
                                    </asp:Label>
                                    <asp:Label ID="lblAccountName" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.AccountName")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="BankName" SortExpression="BankName" HeaderText="BANK">
                                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="AccountNo" SortExpression="AccountNo" HeaderText="ACCOUNT NO">
                                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="AccountName" SortExpression="AccountName" HeaderText="ACCOUNT NAME">
                                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="AccountTypeDesc" SortExpression="AccountTypeDesc" HeaderText="ACCOUNT TYPE">
                                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            </asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
                <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" ImageUrl="../Images/grid_navbutton01.png"
                        CommandName="First" OnCommand="NavigationLink_Click"></asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" ImageUrl="../Images/grid_navbutton02.png"
                        CommandName="Prev" OnCommand="NavigationLink_Click"></asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" ImageUrl="../Images/grid_navbutton03.png"
                        CommandName="Next" OnCommand="NavigationLink_Click"></asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" ImageUrl="../Images/grid_navbutton04.png"
                        CommandName="Last" OnCommand="NavigationLink_Click"></asp:ImageButton>
                    Page&nbsp;
                    <asp:TextBox ID="txtGoPage" runat="server" Width="34px">1</asp:TextBox>
                    <asp:Button ID="btnGoPage" runat="server" Text="Go" CssClass="small buttongo blue" />
                    <asp:RangeValidator ID="rgvGo" runat="server" MinimumValue="1" MaximumValue="999999999"
                        Type="Integer" ControlToValidate="txtGopage" ErrorMessage="No Halaman Salah"
                        CssClass="validator_general"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtGopage"
                        ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>
                <div class="label_gridnavigation">
                    <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                    <asp:Label ID="lblTotRec" runat="server"></asp:Label>&nbsp;record(s)
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="BtnAddnew" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnBack" runat="server" CausesValidation="False" Text="Back" CssClass="small button gray">
            </asp:Button>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    CARI REKENING
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Mitra ID</label>
                <asp:LinkButton ID="lblMitraID" runat="server" Width="416px"></asp:LinkButton>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Mitra Multifinance</label>
                <asp:Label ID="lblMitraName" runat="server" Width="400px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cari berdasarkan</label>
                <asp:DropDownList ID="cboSearchBy" runat="server" Width="144px" Font-Names="Verdana">
                    <asp:ListItem Value="AccountNo" Selected="True">ACCOUNT NO</asp:ListItem>
                    <asp:ListItem Value="AccountName">ACCOUNT NAME</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="TxtSearchByValue" runat="server" Width="168px"></asp:TextBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="BtnSearchnew" runat="server" Text="Find" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtnResetnew" runat="server" Text="Reset" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlAddEdit" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    REKENING MITRA -&nbsp;
                    <asp:Label ID="lblMenuAddEdit" runat="server" Enabled="True"></asp:Label>
                </h4>
            </div>
        </div>
        <div class="form_box_uc">
            <uc1:ucbankaccount id="UcBankAccount" runat="server"></uc1:ucbankaccount>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Account Type</label>
                <asp:DropDownList ID="cboAccountType" runat="server">
                    <asp:ListItem Value="E" Selected="True">Escrow</asp:ListItem>
                    <asp:ListItem Value="D">Disburse</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlpindahcabang" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    KARYAWAN - PINDAH CABANG&nbsp;
                    <asp:Label ID="Label1" runat="server" Enabled="True"></asp:Label>
                </h4>
            </div>
        </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Pindah Ke Cabang</label>
            <asp:Label ID="LblBranchName2" runat="server" Visible="False"></asp:Label>
            <uc1:ucbranchall id="oBranch" runat="server"></uc1:ucbranchall>
        </div>
    </div>
        <div class="form_button">
            <asp:Button ID="btnPindahCabang" runat="server" Text="Pindah" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnCancelPindah" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>

    </form>
</body>
</html>
