﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucLendingPaymentHeader.ascx.vb" Inherits="Maxiloan.Webform.PembiayaanMitra.ucLendingPaymentHeader" %>
 
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<div class="form_box">
    <div class="form_single">
        <label>Reference</label>
        <asp:TextBox ID="txtreferenceno" runat="server"></asp:TextBox>
    </div>
</div>
<div class="form_box">
    <div class="form_single">
        <label>Payment Date</label>
         <asp:TextBox ID="txtDateCE" runat="server"></asp:TextBox>
        <asp:CalendarExtender runat="server" ID="ceDate" TargetControlID="txtDateCE" Format="dd/MM/yyyy">
        </asp:CalendarExtender>
        <asp:RequiredFieldValidator runat="server" ID="rfvDateCE" ControlToValidate="txtDateCE"
            Display="Dynamic" ErrorMessage="Tanggal harus diisi!" CssClass="validator_general"
            SetFocusOnError="true" Enabled="false">
        </asp:RequiredFieldValidator>
        <%--Custom format "dd/MM/yyyy" wajib pakai ConvertDate2 dan ConvertDate pada code behind dan web.config en-US--%>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Penulisan tanggal salah format, input dengan format dd/MM/yyyy!"
            ValidationExpression="(0[1-9]|[12][0-9]|3[01])/(0[1-9]|1[012])/\d{4}" ControlToValidate="txtDateCE"
            SetFocusOnError="true" Display="Dynamic" CssClass="validator_general"> 
        </asp:RegularExpressionValidator>
    </div>
</div>
<div class="form_box">
    <div class="form_single">
        <label>Payment Type</label>
        <asp:DropDownList ID="cbowop" runat="server" />
    </div>
    </div>
    <div class="form_box">
    <div class="form_single">
        <label>Rekening Bank</label>
        <asp:DropDownList ID="cmbBankAccount" runat="server">
        </asp:DropDownList>
        <asp:RequiredFieldValidator ID="Requiredfieldvalidator3" runat="server" InitialValue="0"
            Enabled="True" Display="Dynamic" ControlToValidate="cmbBankAccount" ErrorMessage="Harap Pilih rekening Bank!"
            Visible="True" CssClass="validator_general">
        </asp:RequiredFieldValidator>
    </div>
</div>
<div class="form_box">
    <div class="form_single">
        <label>Jumlah</label>
        <asp:TextBox ID="txtJumblah" runat="server"></asp:TextBox>
    </div>
</div>
<div class="form_box">
    <div class="form_single">
        <label>Keterangan</label>
        <asp:TextBox ID="txtKeterangan" runat="server"></asp:TextBox>
    </div>
</div>