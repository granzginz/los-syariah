﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucLendingAgreementList.ascx.vb" Inherits="Maxiloan.Webform.PembiayaanMitra.ucLendingAgreementList" %>

<div class="form_box_header">
    <div class="form_single">
        <h5>    
                
        </h5>
    </div>
</div>
<div class="form_box_header">
    <div class="form_single">
        <div class="grid_wrapper_ws">
            <asp:DataGrid ID="DtgLendingFacility" runat="server" AutoGenerateColumns="False" BorderWidth="0"
                            OnSortCommand="SortGrid" BorderStyle="None" CssClass="grid_general">
                    <ItemStyle CssClass="item_grid"></ItemStyle>
                    <HeaderStyle CssClass="th"></HeaderStyle>
                    <Columns>
                         <asp:TemplateColumn HeaderText="CALCULATE">
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            <ItemTemplate>
                                <asp:ImageButton ID="ImbEdit" runat="server" ImageUrl="../Images/IconReceived.gif" CommandName="CALCULATE" CausesValidation="False"></asp:ImageButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lblMFID" runat="server" Text='<%#Container.DataItem("MFID")%>'></asp:Label>
                                <asp:Label ID="lblMFFacilityNo" runat="server" Text='<%#Container.DataItem("MFFacilityNo")%>'></asp:Label>
                                <asp:Label ID="lblmfbatchno" runat="server" Text='<%#Container.DataItem("mfbatchno")%>'></asp:Label>
                                <asp:Label ID="lblmfagreementno" runat="server" Text='<%#Container.DataItem("mfagreementno")%>'></asp:Label>
                                <asp:Label ID="lblprincipalamount" runat="server" Text='<%#Container.DataItem("principalamount")%>'></asp:Label>
                                <asp:Label ID="lbltenor" runat="server" Text='<%#Container.DataItem("tenor")%>'></asp:Label>
                                <asp:Label ID="lblrate" runat="server" Text='<%#Container.DataItem("rate")%>'></asp:Label>
                                <asp:Label ID="lblinterestamount" runat="server" Text='<%#Container.DataItem("interestamount")%>'></asp:Label>
                                <asp:Label ID="lblosprincipal" runat="server" Text='<%#Container.DataItem("osprincipal")%>'></asp:Label>
                                <asp:Label ID="lblosinterest" runat="server" Text='<%#Container.DataItem("osinterest")%>'></asp:Label>
                                <asp:Label ID="lblpaidseqno" runat="server" Text='<%#Container.DataItem("paidseqno")%>'></asp:Label>
                                <asp:Label ID="lblstatus" runat="server" Text='<%#Container.DataItem("status")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="MFID" SortExpression="MFID" HeaderText="NO. MF"></asp:BoundColumn> 
                        <asp:BoundColumn DataField="MFFacilityNo" SortExpression="MFFacilityNo" HeaderText="NO. FACILITY"></asp:BoundColumn> 
                        <asp:BoundColumn DataField="mfbatchno" SortExpression="mfbatchno" HeaderText="NO. BATCH"></asp:BoundColumn>
                        <asp:BoundColumn DataField="mfagreementno" SortExpression="mfagreementno" HeaderText="NO. AGREEMENT"></asp:BoundColumn>
                        <asp:BoundColumn DataField="principalamount" SortExpression="principalamount" HeaderText="PRINCIPLE" DataFormatString="{0:N0}"></asp:BoundColumn> 
                        <asp:BoundColumn DataField="tenor" SortExpression="tenor" HeaderText="TENOR" DataFormatString="{0:N0}"></asp:BoundColumn>
                        <asp:BoundColumn DataField="rate" SortExpression="rate" HeaderText="RATE" DataFormatString="{0:N0}"></asp:BoundColumn>
                        <asp:BoundColumn DataField="interestamount" SortExpression="interestamount" HeaderText="INTEREST" DataFormatString="{0:N0}"></asp:BoundColumn> 
                        <asp:BoundColumn DataField="osprincipal" SortExpression="osprincipal" HeaderText="OS PRINCIPAL" DataFormatString="{0:N0}"></asp:BoundColumn> 
                        <asp:BoundColumn DataField="osinterest" SortExpression="osinterest" HeaderText="OS INTEREST" DataFormatString="{0:N0}"></asp:BoundColumn> 
                        <asp:BoundColumn DataField="paidseqno" SortExpression="paidseqno" HeaderText="SEQ NO" DataFormatString="{0:N0}"></asp:BoundColumn> 
                        <asp:BoundColumn DataField="status" SortExpression="status" HeaderText="STATUS"></asp:BoundColumn> 
                    </Columns>
                </asp:DataGrid>                          
        </div>
        <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../Images/grid_navbutton01.png"
                        CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="First">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../Images/grid_navbutton02.png"
                        CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Prev">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../Images/grid_navbutton03.png"
                        CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Next">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../Images/grid_navbutton04.png"
                        CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Last">
                    </asp:ImageButton>
                    Page
                    <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                    <asp:Button ID="imgbtnPageNumb" runat="server" CssClass="small buttongo blue" Text="Go"
                        EnableViewState="False"></asp:Button>
                    <asp:RangeValidator ID="rgvGo" runat="server" ErrorMessage="No Halaman Salah" ControlToValidate="txtPage"
                        Type="Integer" MaximumValue="999999999" MinimumValue="1" CssClass="validator_general"
                        Display="Dynamic"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" Display="Dynamic"
                        ErrorMessage="No Halaman Salah" ControlToValidate="txtPage" CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>
                         
                <asp:Label ID="lblPage" runat="server"></asp:Label>of 
                <asp:Label ID="lblTotPage" runat="server"></asp:Label> 
                <div class="label_gridnavigation">Total
                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
            </div>  
    </div>
</div>
<div class="form_title">
    <div class="form_single">
        <h5>CARI DATA                  
        </h5>
    </div>
</div>
<div class="form_box">
    <div class="form_single">
        <label>
            Cari Berdasarkan</label>
        <asp:DropDownList ID="cboSearchBy" runat="server">
            <asp:ListItem Value="MFAgreementNo" Selected="True">No. Agreement</asp:ListItem>
            <asp:ListItem Value="DebtorName">Nama</asp:ListItem>
            <asp:ListItem Value="a.MFID" >Kode MF</asp:ListItem>
            <asp:ListItem Value="a.ReferenceNo">No. Reference</asp:ListItem>                       
        </asp:DropDownList>
        <asp:TextBox ID="txtSearchBy" runat="server"></asp:TextBox>
    </div>
</div>