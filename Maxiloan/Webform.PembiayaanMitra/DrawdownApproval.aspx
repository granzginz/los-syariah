﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="DrawdownApproval.aspx.vb" Inherits="Maxiloan.Webform.PembiayaanMitra.DrawdownApproval" %>
<%@ Register TagPrefix="uc1" TagName="batchlist" Src="BatchList.ascx" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
     <link rel="Stylesheet" type="text/css" href="../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../Include/Buttons.css" />
    <script src="../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>APPROVAL PENCAIRAN
                </h3>
            </div>
        </div>
        <uc1:batchlist ID="obatchlist1" runat="server"></uc1:batchlist>
        <div class="form_button">
            <asp:Button ID="btnapprove" runat="server" Text="Approve" CssClass="small button blue"></asp:Button>            
        </div>
    </form>
</body>
</html>
