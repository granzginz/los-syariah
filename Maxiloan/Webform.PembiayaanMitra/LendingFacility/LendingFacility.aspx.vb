﻿Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller

Public Class LendingFacility : Inherits Maxiloan.Webform.WebBased

    Private oLendingFacilityController As New Controller.MitraController

    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 5
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Protected WithEvents ucPorsi As ucNumberFormat
    Protected WithEvents ucStartDate As ucDateCE
    Protected WithEvents ucEndDate As ucDateCE
    Protected WithEvents ucLateCharges As ucNumberFormat
    Protected WithEvents ucPenalty As ucNumberFormat
    Protected WithEvents ucTglFasilitas As ucDateCE
    Protected WithEvents ucMinDrawdownAmount As ucNumberFormat

    Public Property CmdWhere() As String
        Get
            Return CStr(ViewState("CmdWhere"))
        End Get
        Set(ByVal Value As String)
            ViewState("CmdWhere") = Value
        End Set
    End Property

#Region "Methode and Function"
    Private Sub DoBind(ByVal strSort As String, ByVal cmdWhere As String)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim cContract As New GeneralPagingController
        Dim oContract As New Parameter.GeneralPaging

        Me.CmdWhere = ""

        With oContract
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = ""
            .SpName = "spLendingFacilityListPaging"
        End With

        oContract = cContract.GetGeneralPaging(oContract)

        With oContract
            lblrecord.Text = CType(.TotalRecords, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecords
        End With

        dtsEntity = oContract.ListData
        dtvEntity = dtsEntity.DefaultView
        dtvEntity.Sort = Me.SortBy

        If dtvEntity.Count <= 0 Then
            'imgPrint.Enabled = False
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
        Else
            'imgPrint.Enabled = True
        End If

        DtgLendingFacility.DataSource = dtvEntity
        Try
            DtgLendingFacility.DataBind()
        Catch
            DtgLendingFacility.CurrentPageIndex = 0
            DtgLendingFacility.DataBind()
        End Try
        PagingFooter()
    End Sub

#Region "Navigation"
    Private Sub PagingFooter()
        'lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
                'Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
                'Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SortBy, Me.CmdWhere)
    End Sub
    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles imgbtnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SortBy, Me.CmdWhere)
            End If
        End If
    End Sub
#End Region

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles DtgLendingFacility.SortCommand
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
            DoBind(e.SortExpression, Me.CmdWhere)
        Else
            Me.SortBy = e.SortExpression + " DESC"
            DoBind(e.SortExpression + " DESC", Me.CmdWhere)
        End If
    End Sub

    Private Sub BtnAddNew_Click(sender As Object, e As EventArgs) Handles BtnAddNew.Click
        panelAllFalse()
        pnlAddEdit.Visible = True
        lblMenuAddEdit.Text = "ADD"
        lblMenuAddEdit.Visible = True
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Response.Redirect("LendingFacility.aspx")
    End Sub

    Private Sub BtnSearch_Click(sender As Object, e As EventArgs) Handles BtnSearch.Click

        If (txtSearchBy.Text.Length > 0) Then
            Me.CmdWhere = String.Format("{0} LIKE '%{1}%'", cboSearchBy.Text, txtSearchBy.Text)
        End If

        DoBind(Me.SortBy, Me.CmdWhere)
    End Sub

    Private Sub DtgLendingFacility_ItemCommand(source As Object, e As DataGridCommandEventArgs) Handles DtgLendingFacility.ItemCommand
        lblMessage.Visible = False

        Select Case e.CommandName
            Case "BATCH"
                Dim tempLblMFCode As String = CType(DtgLendingFacility.Items(e.Item.ItemIndex).FindControl("lblMFCode"), Label).Text
                Dim tempLblFacilityNo As String = CType(DtgLendingFacility.Items(e.Item.ItemIndex).FindControl("lblFacilityNo"), Label).Text
                Response.Redirect("LendingBatch.aspx?MFCode=" & tempLblMFCode.Trim & "&FacilityNo=" & tempLblFacilityNo.Trim)
            Case "RATE"
                Dim tempLblMFCode As String = CType(DtgLendingFacility.Items(e.Item.ItemIndex).FindControl("lblMFCode"), Label).Text
                Dim tempLblFacilityNo As String = CType(DtgLendingFacility.Items(e.Item.ItemIndex).FindControl("lblFacilityNo"), Label).Text
                Response.Redirect("LendingRate.aspx?MFCode=" & tempLblMFCode.Trim & "&FacilityNo=" & tempLblFacilityNo.Trim)
            Case "EDIT"
                Dim tempUCNumberFormat As ucNumberFormat = New ucNumberFormat
                panelAllFalse()
                pnlAddEdit.Visible = True
                lblMenuAddEdit.Text = "ADD"
                lblMenuAddEdit.Visible = True

                txtMFCode.Text = CType(DtgLendingFacility.Items(e.Item.ItemIndex).FindControl("lblMFID"), Label).Text
                txtFacilityNo.Text = CType(DtgLendingFacility.Items(e.Item.ItemIndex).FindControl("lblFacilityNo"), Label).Text
                txtFacilityName.Text = CType(DtgLendingFacility.Items(e.Item.ItemIndex).FindControl("lblMFFacilityName"), Label).Text
                ddlJenisFacility.SelectedValue = CType(DtgLendingFacility.Items(e.Item.ItemIndex).FindControl("lblJenisFacility"), Label).Text
                DirectCast(txtPlafond, Maxiloan.Webform.UserController.ucNumberFormat).[Text] = FormatNumber((CType(DtgLendingFacility.Items(e.Item.ItemIndex).FindControl("lblPlafond"), Label).Text), 2)
                'DirectCast(txtOSPlafond, Maxiloan.Webform.UserController.ucNumberFormat).[Text] = FormatNumber((CType(DtgLendingFacility.Items(e.Item.ItemIndex).FindControl("lblOSPlafond"), Label).Text), 2)
        End Select

    End Sub

    Private Function isDMLValid(ByRef property1 As Parameter.LendingFacility) As String
        Dim result As String = ""
        property1 = New Parameter.LendingFacility

        With property1
            .strConnection = Me.GetConnectionString
            .MFCode = txtMFCode.Text.Trim
            .MFFacilityNo = txtFacilityNo.Text.Trim
            .MFFacilityName = txtFacilityName.Text.Trim
            .JenisFacility = ddlJenisFacility.SelectedValue.ToString
            Decimal.TryParse((DirectCast(txtPlafond, Maxiloan.Webform.UserController.ucNumberFormat).[Text]), .Plafond)
            .SifatFacility = ddlSifatFacility.SelectedValue.ToString
            Decimal.TryParse((DirectCast(ucPorsi, Maxiloan.Webform.UserController.ucNumberFormat).[Text]), .LenderPortion)
            .AssetCondition = ddlAssetCondition.SelectedValue.ToString
            .DrawdownStartDate = ucStartDate.GetDate
            .DrawdownEndDate = ucEndDate.GetDate
            Decimal.TryParse((DirectCast(ucLateCharges, Maxiloan.Webform.UserController.ucNumberFormat).[Text]), .LateCharges)
            Decimal.TryParse((DirectCast(ucPenalty, Maxiloan.Webform.UserController.ucNumberFormat).[Text]), .Penalty)
            .FacilityDate = ucTglFasilitas.GetDate
            Decimal.TryParse((DirectCast(ucMinDrawdownAmount, Maxiloan.Webform.UserController.ucNumberFormat).[Text]), .MinDrawDownAmount)
        End With

        If (String.Equals(property1.MFCode, "")) Then result += "\nInvalid MFCode"
        If (String.Equals(property1.MFFacilityNo, "")) Then result += "\nInvalid Facility No"
        If (String.Equals(property1.MFFacilityName, "")) Then result += "\nInvalid Facility Name"
        If (property1.LateCharges <= 0) Then result += "\nInvalid Late Charge"

        Return result.Trim
    End Function

    Private Sub panelAllFalse()
        pnlList.Visible = False
        pnlAddEdit.Visible = False
        lblMessage.Visible = False
    End Sub

#End Region


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.FormID = "LNDFAC"

        panelAllFalse()
        pnlList.Visible = True

        If Not (Page.IsPostBack) Then
            Me.CmdWhere = ""
            DoBind(Me.SortBy, Me.CmdWhere)
            btnCancel.CausesValidation = False
        End If
    End Sub
    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim property1 As New Parameter.LendingFacility
        Dim dml As String
        Dim isDataValid As String = isDMLValid(property1)

        If (String.IsNullOrEmpty(isDataValid)) Then
            If (lblMenuAddEdit.Text = "ADD") Then
                dml = "INSERT"
            ElseIf (lblMenuAddEdit.Text = "UPDATE") Then
                dml = "UPDATE"
            End If

            Try
                oLendingFacilityController.lendingFacilityDML(dml, property1)
                ShowMessage(lblMessage, (String.Format("Data sucess {0}ed", dml.ToLower)), False)
                DoBind(String.Empty, String.Empty)
            Catch ex As Exception
                ShowMessage(lblMessage, ex.Message, True)
            End Try
        Else
            ShowMessage(lblMessage, isDataValid, True)
        End If
    End Sub
End Class