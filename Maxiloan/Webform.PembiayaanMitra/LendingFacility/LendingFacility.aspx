﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="LendingFacility.aspx.vb" Inherits="Maxiloan.Webform.PembiayaanMitra.LendingFacility" %>


<!DOCTYPE html>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ucNumberFormat" Src="../../Webform.UserController/ucNumberFormat.ascx" %>
<%@ Register TagPrefix="uc2" TagName="ucDateCE" Src="../../Webform.UserController/ucDateCE.ascx" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>LendingFacility</title>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
</head>
<body>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <progresstemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </progresstemplate>
    </asp:UpdateProgress>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>LENDING FASILITAS
                </h3>
            </div>
        </div>
        <asp:UpdatePanel runat="server" ID="updatePanel1">
            <contenttemplate>
                <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
                <asp:Panel ID="pnlList" runat="server">
                    
                    <div class="form_title">
                        <div class="form_single">
                            <h4>CARI FASILITAS
                            </h4>
                        </div>
                    </div>
                    
                    <div class="form_box_header">
                        <div class="form_single">
                            <div class="grid_wrapper_ws">
                                <asp:DataGrid ID="DtgLendingFacility" runat="server" AutoGenerateColumns="False" BorderWidth="0"
                                        OnSortCommand="SortGrid" BorderStyle="None" CssClass="grid_general">
                                        <ItemStyle CssClass="item_grid"></ItemStyle>
                                        <HeaderStyle CssClass="th"></HeaderStyle>
                                        <Columns>
                                            <asp:TemplateColumn HeaderText="EDIT">
                                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="ImbEdit" runat="server" ImageUrl="../../Images/iconedit.gif" CommandName="EDIT" CausesValidation="False"></asp:ImageButton>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="DETAIL">
                                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="ImbDetail" runat="server" ImageUrl="../../Images/IconDocument.gif" CommandName="DETAIL" CausesValidation="False"></asp:ImageButton>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="RATE">
                                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="ImbRate" runat="server" ImageUrl="../../Images/IconRate.gif" CommandName="RATE" CausesValidation="False"></asp:ImageButton>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblMFCode" runat="server" Visible="false" Text='<%#Container.DataItem("MFCode")%>'></asp:Label>
                                                    <asp:Label ID="lblFacilityNo" runat="server" Visible="false" Text='<%#Container.DataItem("MFFacilityNo")%>'></asp:Label>
                                                    <asp:Label ID="lblMFFacilityName" runat="server" Visible="false" Text='<%#Container.DataItem("MFFacilityName")%>'></asp:Label>
                                                    <asp:Label ID="lblJenisFacility" runat="server" Visible="false" Text='<%#Container.DataItem("JenisFacility")%>'></asp:Label>
                                                    <asp:Label ID="lblPlafond" runat="server" Visible="false" Text='<%#Container.DataItem("Plafond")%>'></asp:Label>
                                                    <asp:Label ID="lblOSPlafond" runat="server" Visible="false" Text='<%#Container.DataItem("OSPlafond")%>'></asp:Label>
                                                    <asp:Label ID="lblSifatFacility" runat="server" Visible="false" Text='<%#Container.DataItem("SifatFacility")%>'></asp:Label>
                                                    <asp:Label ID="lblLenderPortion" runat="server" Visible="false" Text='<%#Container.DataItem("LenderPortion")%>'></asp:Label>
                                                    <asp:Label ID="lblAssetCondition" runat="server" Visible="false" Text='<%#Container.DataItem("AssetCondition")%>'></asp:Label>
                                                    <asp:Label ID="lblDrawdownStartDate" runat="server" Visible="false" Text='<%#Container.DataItem("DrawdownStartDate")%>'></asp:Label>
                                                    <asp:Label ID="lblDrawdownEndDate" runat="server" Visible="false" Text='<%#Container.DataItem("DrawdownEndDate")%>'></asp:Label>
                                                    <asp:Label ID="lblLateCharges" runat="server" Visible="false" Text='<%#Container.DataItem("LateCharges")%>'></asp:Label>
                                                    <asp:Label ID="lblPenalty" runat="server" Visible="false" Text='<%#Container.DataItem("Penalty")%>'></asp:Label>
                                                    <asp:Label ID="lblFacilityDate" runat="server" Visible="false" Text='<%#Container.DataItem("FacilityDate")%>'></asp:Label>
                                                    <asp:Label ID="lblMinDrawDownAmount" runat="server" Visible="false" Text='<%#Container.DataItem("MinDrawDownAmount")%>'></asp:Label>
                                                    <asp:Label ID="lblUserUpdate" runat="server" Visible="false" Text='<%#Container.DataItem("UserUpdate")%>'></asp:Label>
                                                    <asp:Label ID="lblDateUpdate" runat="server" Visible="false" Text='<%#Container.DataItem("DateUpdate")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn DataField="MFCode" SortExpression="KODE MF" HeaderText="MF Code"></asp:BoundColumn> 
                                            <asp:BoundColumn DataField="MFFacilityNo" SortExpression="MFFacilityNo" HeaderText="NO. FASILITAS"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="MFFacilityName" SortExpression="MFFacilityName" HeaderText="NAMA FASILITAS"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="Plafond" SortExpression="MFFacilityName" HeaderText="PLAFOND" DataFormatString="{0:#,0}"></asp:BoundColumn> 
                                            <asp:BoundColumn DataField="DrawdownStartDate" SortExpression="DrawdownStartDate" HeaderText="TGL. MULAI" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="DrawdownEndDate" SortExpression="DrawdownEndDate" HeaderText="TGL. AKHIR" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>                                                                                                  
                                            <asp:TemplateColumn HeaderText="Batch">
                                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="imgBatch" runat="server" ImageUrl="../../Images/ftv2doc.gif" CommandName="BATCH" CausesValidation="False"></asp:ImageButton>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                        </Columns>
                                    </asp:DataGrid>
                                    <div class="button_gridnavigation">
                                        <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                                            CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="First">
                                        </asp:ImageButton>
                                        <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                                            CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Prev">
                                        </asp:ImageButton>
                                        <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                                            CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Next">
                                        </asp:ImageButton>
                                        <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                                            CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Last">
                                        </asp:ImageButton>
                                        Page&nbsp;
                                        <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                                        <asp:Button ID="imgbtnPageNumb" runat="server" CssClass="small buttongo blue" Text="Go"
                                            EnableViewState="False"></asp:Button>
                                        <asp:RangeValidator ID="rgvGo" runat="server" ErrorMessage="No Halaman Salah" ControlToValidate="txtPage"
                                            Type="Integer" MaximumValue="999999999" MinimumValue="1" CssClass="validator_general"
                                            Display="Dynamic"></asp:RangeValidator>
                                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" Display="Dynamic"
                                            ErrorMessage="No Halaman Salah" ControlToValidate="txtPage" CssClass="validator_general"></asp:RequiredFieldValidator>
                                    </div>
                         
                                    <asp:Label ID="lblPage" runat="server"></asp:Label>of 
                                    <asp:Label ID="lblTotPage" runat="server"></asp:Label> 
                                    <div class="label_gridnavigation">Total
                                    <asp:Label ID="lblrecord" runat="server"></asp:Label> record(s)
                                </div>                            
                            </div>
                        </div>
                    </div>
                    <div class="form_button">
                        <asp:Button ID="BtnAddNew" runat="server" CausesValidation="False" Text="Add" CssClass="small button green">
                        </asp:Button>
                    </div>
                    <div class="form_title">
                        <div class="form_single">
                            <%--<h4>CARI FASILITAS 
                            </h4>--%>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label>
                                Cari Berdasarkan</label>
                            <asp:DropDownList ID="cboSearchBy" runat="server">
                                <asp:ListItem Value="MFCode"  Selected="True">Kode MF</asp:ListItem>
                                <asp:ListItem Value="MFFacilityNo">Nomor Fasilitas</asp:ListItem>
                                <asp:ListItem Value="MFFacilityName">No Kontrak</asp:ListItem>
                                <asp:ListItem Value="Customer.Name">Nama</asp:ListItem>
                            </asp:DropDownList>
                            <asp:TextBox ID="txtSearchBy" runat="server" Width="250px"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form_button">
                        <asp:Button ID="BtnSearch" runat="server" CausesValidation="False" Text="Find"
                            CssClass="small button blue"></asp:Button>
                        <asp:Button ID="BtnReset" runat="server" CausesValidation="False" Text="Reset"
                            CssClass="small button gray"></asp:Button>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlAddEdit" runat="server" visible="false">
                    <div class="form_title">
                        <div class="form_single">
                            <h4>
                               INPUT FASILITAS -&nbsp;
                                <asp:Label ID="lblMenuAddEdit" runat="server" Enabled="True"></asp:Label>
                            </h4>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label class ="label_req">Kode MF</label>
                            <asp:TextBox ID="txtMFCode" runat="server" Width="100px"  MaxLength="10"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="reqMFCode" runat="server" Width="184px"
                                Display="Dynamic" ErrorMessage="Harap isi Kode MF" ControlToValidate="txtMFCode" CssClass="validator_general" ></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label class ="label_req">No. Fasilitas</label>
                            <asp:TextBox ID="txtFacilityNo" runat="server" Width="100px"  MaxLength="10"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="reqFacilityNo" runat="server" Width="184px"
                                Display="Dynamic" ErrorMessage="Harap isi FacilityNo" ControlToValidate="txtFacilityNo" CssClass="validator_general" ></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label class ="label_req">Nama Fasilitas</label>
                            <asp:TextBox ID="txtFacilityName" runat="server" Width="250px"  MaxLength="100"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="reqFacilityName" runat="server" Width="250px"
                                Display="Dynamic" ErrorMessage="Harap isi Nama Fasilitas" ControlToValidate="txtFacilityName" CssClass="validator_general" ></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label>Jenis Fasilitas</label>
                            <asp:DropDownList ID="ddlJenisFacility" runat="server">
                                <asp:ListItem Text="PILIH" Value="0" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="JF" Value="JF"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="reqJenisFacility" runat="server" Width="250px"
                                Display="Dynamic" ErrorMessage="Harap Pilih Jenis Fasilitas" ControlToValidate="ddlJenisFacility" CssClass="validator_general" InitialValue="0"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label>Plafond</label>
                            <uc1:ucNumberFormat ID="txtPlafond" runat="server"></uc1:ucNumberFormat>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label>Sifat Fasilitas</label>
                            <asp:DropDownList ID="ddlSifatFacility" runat="server">
                                <asp:ListItem Text="PILIH" Value="0" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="REVOLVING" Value="R"></asp:ListItem>
                                <asp:ListItem Text="NON REVOLVING" Value="NR"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="reqSifatFasilitas" runat="server" Width="250px"
                                Display="Dynamic" ErrorMessage="Harap Pilih Sifat Fasilitas" ControlToValidate="ddlSifatFacility" CssClass="validator_general" InitialValue="0"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_left">
                            <label>Porsi</label>
                            <uc1:ucNumberFormat ID="ucPorsi" runat="server"></uc1:ucNumberFormat>
                            <label> %</label>
                        </div>
                        <div class="form_right">
                            <label>Kondisi Asset</label>
                            <asp:DropDownList ID="ddlAssetCondition" runat="server">
                                <asp:ListItem Text="PILIH" Value="0" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="ALL" Value="A"></asp:ListItem>
                                <asp:ListItem Text="BARU" Value="N"></asp:ListItem>
                                <asp:ListItem Text="BEKAS" Value="U"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="reqAssetCondition" runat="server" Width="250px" Display="Dynamic" ErrorMessage="Harap Pilih Kondisi Asset" ControlToValidate="ddlAssetCondition" CssClass="validator_general" InitialValue="0"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label>Tanggal Mulai</label>
                            <uc2:ucDateCE ID="ucStartDate" runat="server"></uc2:ucDateCE>
                            <label class="label_auto">s/d</label>
                            <uc2:ucDateCE ID="ucEndDate" runat="server"></uc2:ucDateCE>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_left">
                            <label class ="label_req">Late Charges</label>
                            <uc1:ucNumberFormat ID="ucLateCharges" runat="server"></uc1:ucNumberFormat>
                            <label> %</label>
                        </div>
                        <div class="form_right">
                            <label>Penalty</label>
                            <uc1:ucNumberFormat ID="ucPenalty" runat="server"></uc1:ucNumberFormat>
                            <label> %</label>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_left">
                            <label>Tanggal Fasilitas</label>
                            <uc2:ucDateCE ID="ucTglFasilitas" runat="server"></uc2:ucDateCE>
                        </div>
                        <div class="form_right">
                            <label>Minimal Drawdown</label>
                            <uc1:ucNumberFormat ID="ucMinDrawdownAmount" runat="server"></uc1:ucNumberFormat>
                        </div>
                    </div>
                    <div class="form_button">
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button green"></asp:Button>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="small button gray"></asp:Button>
                    </div>
                </asp:Panel>
            </contenttemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
