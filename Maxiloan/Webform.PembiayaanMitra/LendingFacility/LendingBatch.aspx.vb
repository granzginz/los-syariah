﻿Imports System.IO

Public Class LendingBatch : Inherits Maxiloan.Webform.WebBased

#Region " Private Const "
    Private oLendingRateController As New Controller.MitraController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private lnkContractNo As HyperLink

    Dim oMFCode As String
    Dim oFacilityNo As String
#End Region

#Region "Methode and Function"
#Region " Navigation "
    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If

        bindGridEntity(Me.SearchBy, Me.SortBy)
        pnlList.Visible = True
    End Sub

#End Region

#Region " Property "
    Private Property MFCode() As String
        Get
            Return CStr(ViewState("MFCode"))
        End Get
        Set(ByVal Value As String)
            ViewState("MFCode") = Value
        End Set
    End Property

    Private Property MFFacilityNo() As String
        Get
            Return CStr(ViewState("MFFacilityNo"))
        End Get
        Set(ByVal Value As String)
            ViewState("MFFacilityNo") = Value
        End Set
    End Property

    Private Property TenorFrom() As Int16
        Get
            Return CStr(ViewState("TenorFrom"))
        End Get
        Set(ByVal Value As Int16)
            ViewState("TenorFrom") = Value
        End Set
    End Property

    Private Property TenorTo() As Int16
        Get
            Return CStr(ViewState("TenorTo"))
        End Get
        Set(ByVal Value As Int16)
            ViewState("TenorTo") = Value
        End Set
    End Property

    Private Property UsedRate() As Decimal
        Get
            Return CStr(ViewState("UsedRate"))
        End Get
        Set(ByVal Value As Decimal)
            ViewState("UsedRate") = Value
        End Set
    End Property

    Private Property NewRate() As Decimal
        Get
            Return CStr(ViewState("NewRate"))
        End Get
        Set(ByVal Value As Decimal)
            ViewState("NewRate") = Value
        End Set
    End Property

    Private Property UserUpdate() As String
        Get
            Return CStr(ViewState("UserUpdate"))
        End Get
        Set(ByVal Value As String)
            ViewState("UserUpdate") = Value
        End Set
    End Property

    Private Property DateUpdate() As DateTime
        Get
            Return CStr(ViewState("DateUpdate"))
        End Get
        Set(ByVal Value As DateTime)
            ViewState("DateUpdate") = Value
        End Set
    End Property
#End Region

    Private Sub panelAllFalse()
        pnlList.Visible = False
        lblMessage.Visible = False
    End Sub

    Private Sub initialDefaultPanel()
        panelAllFalse()
        pnlList.Visible = True
    End Sub

#Region "BindGridEntity"
    Sub bindGridEntity(ByVal cmdWhere As String, ByVal cmSort As String)
        Dim property1 As New Parameter.Lending

        With property1
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        property1 = oLendingRateController.lendingBatchListPaging(property1)
        lendingRateDataGrid.DataSource = property1.ListData

        Try
            lendingRateDataGrid.DataBind()
        Catch
            lendingRateDataGrid.CurrentPageIndex = 0
            lendingRateDataGrid.DataBind()
        End Try
    End Sub
#End Region

#End Region


#Region "FormLoad"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If

        oMFCode = Request.QueryString("MFCode")
        oFacilityNo = Request.QueryString("FacilityNo")

        lblmfid.Text = oMFCode
        lblfasillitas.Text = oFacilityNo

        If Not Me.IsPostBack Then
            Me.FormID = "LendingRate"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If SessionInvalid() Then
                    Exit Sub
                End If

                initialDefaultPanel()
                Me.SearchBy = ""
                Me.SortBy = ""

                If Request("cond") <> "" Then
                    Me.SearchBy = Request("cond")
                Else
                    Me.SearchBy = "ALL"
                End If
                Me.SortBy = ""

                Me.SearchBy = String.Format("MFID = '{0}' AND MFFacilityNo='{1}'", oMFCode, oFacilityNo)

                bindGridEntity(Me.SearchBy, Me.SortBy)

                If ((Not String.IsNullOrEmpty(Request.QueryString("success"))) And ((Request.QueryString("success")) <> Nothing)) Then
                    ShowMessage(lblMessage, (String.Format("Data sucess {0}ed", (Request.QueryString("success")).ToLower)), False)
                End If
            End If
        End If
    End Sub
#End Region

    Private Sub BtnAddNew_Click(sender As Object, e As EventArgs) Handles BtnAddnew.Click
        lblMessage.Visible = False
        pnlList.Visible = False
    End Sub
    Public Sub ConvertToExcel(ByVal dt As DataTable, ByVal strFileName As String)
        Dim FileName As String
        FileName = Request.ServerVariables("APPL_PHYSICAL_PATH") & "xml\"
        FileName += strFileName + ".xls"

        Dim sw As New StreamWriter(FileName + "", False)
        Dim stringWrite As New System.IO.StringWriter
        Dim htmlWrite As New System.Web.UI.HtmlTextWriter(stringWrite)

        Dim dg As New DataGrid
        dg.DataSource = dt
        dg.DataBind()
        dg.RenderControl(htmlWrite)
        sw.Write(stringWrite.ToString)
        sw.Close()

    End Sub

    Private Sub lendingRateDataGrid_ItemCommand(source As Object, e As DataGridCommandEventArgs) Handles lendingRateDataGrid.ItemCommand
        Dim oDS As New DataSet

        Dim spName = "spMFID_260340"


        Dim property1 As New Parameter.Lending

        With property1
            .strConnection = GetConnectionString()
            .mfid = lblmfid.Text
            .mfbatchno = CType(lendingRateDataGrid.Items(e.Item.ItemIndex).FindControl("lblMFBatchNo"), Label).Text  'CType(lendingRateDataGrid.FindControl("lblMFBatchNo"), Label).Text
        End With

        oDS = oLendingRateController.DraftSoftcopy(property1)
        'lendingRateDataGrid.DataSource = property1.ListData

        Try
            ConvertToExcel(oDS.Tables(0), spName)

            'lendingRateDataGrid.DataBind()
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
            'lendingRateDataGrid.CurrentPageIndex = 0
            'lendingRateDataGrid.DataBind()
        End Try

        Select Case e.CommandName
            Case "SOFTCOPY"
                Response.ContentType = "application/XLS"
                Response.AddHeader("content-disposition", "attachment; filename=" & spName & ".xls")
                Response.WriteFile(Server.MapPath("../../XML/" & spName & ".xls"))
                Response.End()
        End Select
    End Sub

    Private Sub BtnBack_Click(sender As Object, e As EventArgs) Handles BtnBack.Click
        Response.Redirect("LendingFacility.aspx")
    End Sub

End Class