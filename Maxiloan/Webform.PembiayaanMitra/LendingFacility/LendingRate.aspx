﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="LendingRate.aspx.vb" Inherits="Maxiloan.Webform.PembiayaanMitra.LendingRate" %>



<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Lending Rate</title>
    <script type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;
        var AppInfo = window.location.pathname;
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
        var ServerName = window.location.protocol + "//" + window.location.host + '/';
    </script>
    <link rel="Stylesheet" type="text/css" href="../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../Include/Buttons.css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function fConfirm() {
            if (window.confirm("Apakah yakin mau hapus data ini ?"))
                return true;
            else
                return false;
        }
    </script>
</head>
<body>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <progresstemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </progresstemplate>
    </asp:UpdateProgress>
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server" ID="ScriptManager1">
        </asp:ScriptManager>
        <asp:UpdatePanel runat="server" ID="UpdatePanel1">
            <contenttemplate>
                <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
                    onclick="hideMessage();"></asp:Label>

                <asp:Panel ID="pnlList" runat="server">
                    <div class="form_title">
                        <div class="title_strip">
                        </div>
                        <div class="form_single">
                            <h3>LENDING RATE
                            </h3>
                        </div>
                    </div>
                    <div class="form_title">
                        <div class="form_single">
                            <h4>DAFTAR LENDING RATE
                            </h4>
                        </div>
                    </div>
                    <div class="form_box_header">
                        <div class="form_single">
                            <div class="grid_wrapper_ns">
                                <asp:DataGrid ID="lendingRateDataGrid" runat="server" AutoGenerateColumns="False" BorderWidth="0"
                                    BorderStyle="None" CssClass="grid_general">
                                    <ItemStyle CssClass="item_grid"></ItemStyle>
                                    <HeaderStyle CssClass="th"></HeaderStyle>
                                    <Columns>
                                        <asp:TemplateColumn Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblMFCode" runat="server" Visible="false" Text='<%#Container.DataItem("MFCode")%>'></asp:Label>
                                                <asp:Label ID="lblFacilityNo" runat="server" Visible="false" Text='<%#Container.DataItem("MFFacilityNo")%>'></asp:Label>
                                                <asp:Label ID="lblTenorFrom" runat="server" Visible="false" Text='<%#Container.DataItem("TenorFrom")%>'></asp:Label>
                                                <asp:Label ID="lblTenorTo" runat="server" Visible="false" Text='<%#Container.DataItem("TenorTo")%>'></asp:Label>
                                                <asp:Label ID="lblUsedRate" runat="server" Visible="false" Text='<%#Container.DataItem("UsedRate")%>'></asp:Label>
                                                <asp:Label ID="lblNewRate" runat="server" Visible="false" Text='<%#Container.DataItem("NewRate")%>'></asp:Label>
                                                <asp:Label ID="lblUserUpdate" runat="server" Visible="false" Text='<%#Container.DataItem("UserUpdate")%>'></asp:Label>
                                                <asp:Label ID="lblDateUpdate" runat="server" Visible="false" Text='<%#Container.DataItem("DateUpdate")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>

                                        <asp:BoundColumn DataField="MFCode" SortExpression="KODE MF" HeaderText="MF Code"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="MFFacilityNo" SortExpression="MFFacilityNo" HeaderText="NO. FASILITAS"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="TenorFrom" SortExpression="TenorFrom" HeaderText="TENOR FROM"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="TenorTo" SortExpression="MFFacilityName" HeaderText="TENOR TO"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="UsedRate" SortExpression="UsedRate" HeaderText="USED RATE"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="NewRate" SortExpression="NewRate" HeaderText="NEW RATE"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="UserUpdate" SortExpression="UserUpdate" HeaderText="USER UPDATE" Visible="false"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="DateUpdate" SortExpression="DateUpdate" HeaderText="DATE UPDATE" Visible="false" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>

                                        <asp:TemplateColumn HeaderText="EDIT">
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../../Images/iconedit.gif" CommandName="EDIT" CausesValidation="False"></asp:ImageButton>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </div>

                        </div>
                    </div>
                    <div class="form_button">
                        <asp:Button ID="BtnAddnew" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue"></asp:Button>
                        <asp:Button ID="BtnBack" runat="server" CausesValidation="False" Text="Back" CssClass="small button gray"></asp:Button>
                    </div>
                </asp:Panel>

                <asp:Panel ID="pnlAddEdit" runat="server">
                    <asp:Label ID="Label1" runat="server"></asp:Label>
                    <div class="form_title">
                        <div class="form_single">
                            <h4>FASILITAS -&nbsp;
                            <asp:Label ID="lblMenuAddEdit" runat="server" Enabled="True"></asp:Label>
                            </h4>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label>
                                Add Lending Rate
                            </label>
                        </div>
                    </div>

                    <div class="form_box">
                        <div class="form_single">
                            <label>
                                MF Code</label>
                            <asp:TextBox ID="mFCodeTextBox" runat="server" MaxLength="20" CssClass="medium_text"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label>
                                MF Facility No</label>
                            <asp:TextBox ID="mFFacilityNoTextBox" runat="server" MaxLength="20" CssClass="medium_text"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label>
                                Tenor From</label>
                            <asp:TextBox ID="tenorFromTextBox" runat="server" MaxLength="20" CssClass="medium_text"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label>
                                Tenor To</label>
                            <asp:TextBox ID="tenorToTextBox" runat="server" MaxLength="20" CssClass="medium_text"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label>
                                Used Rate</label>
                            <asp:TextBox ID="usedRateTextBox" runat="server" MaxLength="20" CssClass="medium_text"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label>
                                New Rate</label>
                            <asp:TextBox ID="newRateTextBox" runat="server" MaxLength="20" CssClass="medium_text"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form_button">
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue"></asp:Button>
                        <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Cancel" CssClass="small button gray"></asp:Button>
                    </div>
                </asp:Panel>
            </contenttemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
