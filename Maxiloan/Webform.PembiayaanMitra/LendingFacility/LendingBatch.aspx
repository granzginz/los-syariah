﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="LendingBatch.aspx.vb" Inherits="Maxiloan.Webform.PembiayaanMitra.LendingBatch" %>



<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Lending Rate</title>
    <script type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;
        var AppInfo = window.location.pathname;
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
        var ServerName = window.location.protocol + "//" + window.location.host + '/';
    </script>
    <link rel="Stylesheet" type="text/css" href="../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../Include/Buttons.css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function fConfirm() {
            if (window.confirm("Apakah yakin mau hapus data ini ?"))
                return true;
            else
                return false;
        }
    </script>
</head>
<body>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <progresstemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </progresstemplate>
    </asp:UpdateProgress>
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server" ID="ScriptManager1">
        </asp:ScriptManager>
        <asp:UpdatePanel runat="server" ID="UpdatePanel1">
            <contenttemplate>
                <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
                    onclick="hideMessage();"></asp:Label>

                <asp:Panel ID="pnlList" runat="server">
                    <div class="form_title">
                        <div class="title_strip">
                        </div>
                        <div class="form_single">
                            <h3>LENDING BATCH
                            </h3>
                        </div>
                    </div>
                    <div class="form_title">
                        <div class="form_single">
                            <h4>DAFTAR LENDING RATE
                            </h4>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label>Mf ID</label>
                                <asp:label id="lblmfid" runat="server"/>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label>No. Fasilitas</label>
                                <asp:label id="lblfasillitas" runat="server"/>
                        </div>
                    </div>
                    <div class="form_box_header">
                        <div class="form_single">
                            <div class="grid_wrapper_ns">
                                <asp:DataGrid ID="lendingRateDataGrid" runat="server" AutoGenerateColumns="False" BorderWidth="0"
                                    BorderStyle="None" CssClass="grid_general">
                                    <ItemStyle CssClass="item_grid"></ItemStyle>
                                    <HeaderStyle CssClass="th"></HeaderStyle>
                                    <Columns>
                                        <asp:TemplateColumn Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblMFID" runat="server" Visible="false" Text='<%#Container.DataItem("MFID")%>'></asp:Label>
                                                <asp:Label ID="lblMFBatchNo" runat="server" Visible="false" Text='<%#Container.DataItem("MFBatchNo")%>'></asp:Label>
                                                <asp:Label ID="lblMFBatchDate" runat="server" Visible="false" Text='<%#Container.DataItem("MFBatchDate")%>'></asp:Label>
                                                <asp:Label ID="lblMFFacilityNo" runat="server" Visible="false" Text='<%#Container.DataItem("MFFacilityNo")%>'></asp:Label>
                                                <asp:Label ID="lbltotal_acc" runat="server" Visible="false" Text='<%#Container.DataItem("total_acc")%>'></asp:Label>
                                                <asp:Label ID="lbltotal_ntf" runat="server" Visible="false" Text='<%#Container.DataItem("total_ntf")%>'></asp:Label>
                                                <asp:Label ID="lblstatus" runat="server" Visible="false" Text='<%#Container.DataItem("status")%>'></asp:Label>
                                                <asp:Label ID="lblApprovalDate" runat="server" Visible="false" Text='<%#Container.DataItem("ApprovalDate")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>

                                        <asp:BoundColumn DataField="MFID" SortExpression="MFID" HeaderText="MF Code"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="MFBatchNo" SortExpression="MFBatchNo" HeaderText="NO. BATCH"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="MFBatchDate" SortExpression="MFBatchDate" HeaderText="DATE BATCH" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="MFFacilityNo" SortExpression="MFFacilityNo" HeaderText="N0. FASILITAS"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="total_acc" SortExpression="total_acc" HeaderText="TOTAL ACC" DataFormatString="{0:#,0}"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="total_ntf" SortExpression="total_ntf" HeaderText="NTF" DataFormatString="{0:N}"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="status" SortExpression="status" HeaderText="status"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="ApprovalDate" SortExpression="ApprovalDate" HeaderText="ApprovalDate" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>

                                         <asp:TemplateColumn HeaderText="SOFTCOPY">
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imbDraft" runat="server" ImageUrl="../../Images/IconDocument.gif"
                                                    CommandName="SOFTCOPY" CausesValidation="False"></asp:ImageButton>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </div>

                        </div>
                    </div>
                    <div class="form_button">
                        <asp:Button ID="BtnAddnew" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue"></asp:Button>
                        <asp:Button ID="BtnBack" runat="server" CausesValidation="False" Text="Back" CssClass="small button gray"></asp:Button>
                    </div>
                </asp:Panel>
            </contenttemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
