﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Disburse.aspx.vb" Inherits="Maxiloan.Webform.PembiayaanMitra.Disburse" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="disburseList" Src="DisburseList.ascx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="Stylesheet" type="text/css" href="../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../Include/Buttons.css" />
    <script src="../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server" ID="ScriptManager1">
        </asp:ScriptManager>
        <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>

        <asp:Panel ID="pnl1" runat="server">
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>DISBURSE
                    </h3>
                </div>
            </div>
            <uc1:disburseList ID="disburseList1" runat="server"></uc1:disburseList>
            <div class="form_button">
                <asp:Button ID="BtnNext" runat="server" Text="Next" CssClass="small button blue"></asp:Button>
            </div>
        </asp:Panel>

        <asp:Panel ID="pnl2" runat="server" Visible="False">
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>DISBURSE DETAIL
                    </h3>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>Reference No</label>
                    <asp:TextBox ID="txtReferenceNo" runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>Jumlah</label>
                    <asp:TextBox ID="txtJumlah" runat="server" ReadOnly="true"></asp:TextBox>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>Tanggal Valuta</label>
                    <asp:TextBox ID="txtvaluedate" runat="server"></asp:TextBox>
                    <asp:CalendarExtender runat="server" ID="ceDate" TargetControlID="txtvaluedate" Format="dd/MM/yyyy">
                    </asp:CalendarExtender>
                    <asp:RequiredFieldValidator runat="server" ID="rfvDateCE" ControlToValidate="txtvaluedate"
                        Display="Dynamic" ErrorMessage="Tanggal harus diisi!" CssClass="validator_general"
                        SetFocusOnError="true" Enabled="false">
                    </asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Penulisan tanggal salah format, input dengan format dd/MM/yyyy!"
                        ValidationExpression="(0[1-9]|[12][0-9]|3[01])/(0[1-9]|1[012])/\d{4}" ControlToValidate="txtvaluedate"
                        SetFocusOnError="true" Display="Dynamic" CssClass="validator_general">
                    </asp:RegularExpressionValidator>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>Payment Type</label>
                    <asp:DropDownList ID="cbowop" runat="server" />
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>Rekening Bank</label>
                    <asp:DropDownList ID="cmbBankAccount" runat="server">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator3" runat="server" InitialValue="0"
                        Enabled="True" Display="Dynamic" ControlToValidate="cmbBankAccount" ErrorMessage="Harap Pilih rekening Bank!"
                        Visible="True" CssClass="validator_general">
                    </asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Keterangan</label>
                    <asp:TextBox ID="txtKeterangan" runat="server" CssClass="long_text"></asp:TextBox>
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue"></asp:Button>
                <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Cancel" CssClass="small button gray"></asp:Button>
            </div>
        </asp:Panel>

    </form>
</body>
</html>
