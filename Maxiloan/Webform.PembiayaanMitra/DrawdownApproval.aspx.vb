﻿Imports Maxiloan.Controller
Public Class DrawdownApproval
    Inherits Maxiloan.Webform.WebBased
    Private octr As New MitraController
    Protected WithEvents obatchlist1 As BatchList
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Visible = False
        If SessionInvalid() Then Exit Sub

        If Page.IsPostBack Then
        Else
            If Me.IsHoBranch Then
                ShowMessage(lblMessage, "Harap login di cabang!", True)
                Exit Sub
            Else
                Me.FormID = "LAPRDISB"
                obatchlist1.showradio = False
                obatchlist1.showcheckbox = True
                obatchlist1.showbutton = False
                obatchlist1.dobind()
            End If
        End If
    End Sub

    Private Sub approve(sender As Object, e As EventArgs) Handles btnapprove.Click
        Dim oclass As New Parameter.Lending
        Dim strnotif As New StringBuilder
        For Each item As DataGridItem In CType(obatchlist1.FindControl("dtg1"), DataGrid).Items
            If item.ItemType = ListItemType.Item Or item.ItemType = ListItemType.AlternatingItem Or item.ItemType = ListItemType.EditItem Then
                If CType(item.FindControl("chk"), CheckBox).Checked Then
                    With oclass
                        '.mfid = ouploadlist1.mfid
                        '.mffacilityno = ouploadlist1.mffacilityno
                        '.mfbatchno = ouploadlist1.mfbatchno
                        .mfid = item.Cells(3).Text.Trim
                        .mffacilityno = item.Cells(4).Text.Trim
                        .mfbatchno = item.Cells(5).Text.Trim
                        .strConnection = GetConnectionString()
                        .BranchId = Me.sesBranchId.Replace("'", "")
                        .BusinessDate = Me.BusinessDate
                    End With
                    Try
                        octr.lendingdrawdownapproval(oclass)
                        strnotif.AppendFormat("Batch {0} Saved! ENTER", oclass.mfbatchno)
                    Catch ex As Exception
                        strnotif.AppendFormat("Batch {0} Error! ENTER {1} ENTER", oclass.mfbatchno, Replace(ex.Message, vbCrLf, " "))
                        Continue For
                    End Try
                End If
            End If
        Next
        Response.Redirect("response.aspx?callerurl=drawdownapproval.aspx&notif=" & strnotif.ToString & "&iserror=0")
    End Sub
End Class