﻿Imports Maxiloan.Controller


Public Class Payment
    Inherits Maxiloan.Webform.WebBased
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 5
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Protected WithEvents ucLendingPaymentHeader1 As ucLendingPaymentHeader
    Protected WithEvents ucLendingPaymentInfo1 As ucLendingPaymentInfo
    Private octr As New MitraController

    Public Property CmdWhere() As String
        Get
            Return CStr(ViewState("CmdWhere"))
        End Get
        Set(ByVal Value As String)
            ViewState("CmdWhere") = Value
        End Set
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.FormID = "LPAYMENT"

        If Not (Page.IsPostBack) Then
            Me.CmdWhere = ""
            DoBind(Me.SortBy, Me.CmdWhere)
            PanelEditor("1", True)
        End If
    End Sub

    Private Sub DoBind(ByVal strSort As String, ByVal cmdWhere As String)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim cContract As New GeneralPagingController
        Dim oContract As New Parameter.GeneralPaging

        Me.CmdWhere = ""

        With oContract
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = ""
            .SpName = "splendingpaymentheaderlist"
        End With

        oContract = cContract.GetGeneralPaging(oContract)

        With oContract
            lblrecord.Text = CType(.TotalRecords, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecords
        End With

        dtsEntity = oContract.ListData
        dtvEntity = dtsEntity.DefaultView
        dtvEntity.Sort = Me.SortBy

        If dtvEntity.Count <= 0 Then
            'imgPrint.Enabled = False
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
        Else
            'imgPrint.Enabled = True
        End If

        DtgLendingFacility.DataSource = dtvEntity
        Try
            DtgLendingFacility.DataBind()
        Catch
            DtgLendingFacility.CurrentPageIndex = 0
            DtgLendingFacility.DataBind()
        End Try
        PagingFooter()
    End Sub

#Region "PanelEditor"
    Private Sub PanelEditor(Param As String, isVisible As Boolean)
        'FirstPanelDiplay()

        Select Case Param
            Case "1"
                'PnlForm1.Visible = isVisible
                BtnSearch.Text = "Cari"
                BtnReset.Text = "Reset"
            Case "2"
                'PnlForm2.Visible = isVisible
                'PnlForm3.Visible = isVisible
                BtnSearch.Text = "Next"
                BtnReset.Text = "Back"
            Case "3"
                'pnlForm4.Visible = isVisible
                BtnSearch.Text = "Save"
                BtnReset.Text = "Cancel"

        End Select
    End Sub

    'Private Sub FirstPanelDiplay()
    '    PnlForm2.Visible = False
    '    PnlForm3.Visible = False
    '    pnlForm4.Visible = False
    '    PnlForm1.Visible = False
    'End Sub

#End Region

#Region "Navigation"
    Private Sub PagingFooter()
        'lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SortBy, Me.CmdWhere)
    End Sub
    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As CommandEventArgs) Handles imgbtnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SortBy, Me.CmdWhere)
            End If
        End If
    End Sub

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles DtgLendingFacility.SortCommand
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
            DoBind(e.SortExpression, Me.CmdWhere)
        Else
            Me.SortBy = e.SortExpression + " DESC"
            DoBind(e.SortExpression + " DESC", Me.CmdWhere)
        End If
    End Sub

    Private Sub BtnSearch_Click(sender As Object, ByVal e As EventArgs) Handles BtnSearch.Click

        Select Case BtnSearch.Text
            Case "Cari"
                If (txtSearchBy.Text.Length > 0) Then
                    Me.CmdWhere = String.Format("{0} LIKE '%{1}%'", cboSearchBy.Text, txtSearchBy.Text)
                End If

                DoBind(Me.SortBy, Me.CmdWhere)
            Case "Next"
                ucLendingPaymentHeader1.PayDate = txtPaymentDate.Text
                ucLendingPaymentHeader1.Mfid = txtMFID.Text
                ucLendingPaymentHeader1.MfFacilityNo = txtMFFacilityNo.Text
                ucLendingPaymentHeader1.MfReferenceNo = txtReferenceNo.Text
                ucLendingPaymentHeader1.DoBind()
                PanelEditor("3", True)
            Case "Save"
                Save()
                PanelEditor("1", True)
        End Select
    End Sub

    Public _valueDate As Date
    Public _wop As String
    Public _bankAccountId As String
    Public _referenceNo As String
    Public _notes As String

    Private Sub _getParamHeade(ByRef _lendingParam As Parameter.Lending) Handles ucLendingPaymentHeader1._getParam
        _valueDate = _lendingParam.ValueDate
        _wop = _lendingParam.wop
        _bankAccountId = _lendingParam.bankaccountid
        _referenceNo = _lendingParam.referenceno
        _notes = _lendingParam.notes
    End Sub

    Private _dt As DataTable
    Private Sub _getParamList(ByRef _lendingParam As Parameter.Lending) Handles ucLendingPaymentInfo1._getParam
        _dt = _lendingParam.paymentlist
    End Sub

    Public Sub Save()
        Dim property1 As New Parameter.Lending
        ucLendingPaymentHeader1.DoBindSave(property1)
        ucLendingPaymentInfo1.DoBindParam(property1)

        With property1
            .strConnection = Me.GetConnectionString
            .mfid = txtMFID.Text
            .mffacilityno = txtMFFacilityNo.Text
            .mfpaymentref = txtReferenceNo.Text
            .BusinessDate = Me.BusinessDate
            .ValueDate = _valueDate
            .wop = _wop
            .bankaccountid = _bankAccountId
            .referenceno = _referenceNo
            .notes = _notes
            '.paymentlist = _dt
        End With

        Try
            octr.LendingPaymentSave(property1)
            Response.Redirect("response.aspx?notif=Done&iserror=0&callerurl=Payment.aspx")
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

    Private Sub BtnReset_Click(sender As Object, ByVal e As EventArgs) Handles BtnReset.Click
        Select Case BtnReset.Text
            Case "Reset"
                txtSearchBy.Text = ""
            Case "Back"
                PanelEditor(1, True)
            Case "Cancel"
                PanelEditor(1, True)
        End Select
    End Sub

    Private Sub DtgLendingFacility_ItemCommand(source As Object, e As DataGridCommandEventArgs) Handles DtgLendingFacility.ItemCommand
        lblMessage.Visible = False

        Dim tempLblMFCode As String = CType(DtgLendingFacility.Items(e.Item.ItemIndex).FindControl("lblMFID"), Label).Text
        Dim tempLblFacilityNo As String = CType(DtgLendingFacility.Items(e.Item.ItemIndex).FindControl("lblFacilityNo"), Label).Text
        Dim templblReferenceNo As String = CType(DtgLendingFacility.Items(e.Item.ItemIndex).FindControl("lblReferenceNo"), Label).Text

        PanelEditor("2", True)

        txtMFID.Text = CType(DtgLendingFacility.Items(e.Item.ItemIndex).FindControl("lblMFID"), Label).Text
        txtMFFacilityNo.Text = CType(DtgLendingFacility.Items(e.Item.ItemIndex).FindControl("lblFacilityNo"), Label).Text
        txtTotalAccount.Text = Decimal.Parse(CType(DtgLendingFacility.Items(e.Item.ItemIndex).FindControl("lblTotalAccount"), Label).Text).ToString("N0")
        txtReferenceNo.Text = CType(DtgLendingFacility.Items(e.Item.ItemIndex).FindControl("lblReferenceNo"), Label).Text

        Dim _paymentDate = CType(DtgLendingFacility.Items(e.Item.ItemIndex).FindControl("lblPaymentDate"), Label).Text
        txtPaymentDate.Text = DateTime.Parse(_paymentDate).ToString("dd/MM/yyyy")
        txtTotalAmount.Text = Decimal.Parse(CType(DtgLendingFacility.Items(e.Item.ItemIndex).FindControl("lblTotalAmount"), Label).Text).ToString("N0")

        ucLendingPaymentInfo1.DoBind()
    End Sub

#End Region

End Class