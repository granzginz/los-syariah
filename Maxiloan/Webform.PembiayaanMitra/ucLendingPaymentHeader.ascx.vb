﻿Imports Maxiloan.Controller

Public Class ucLendingPaymentHeader
    Inherits Maxiloan.Webform.ControlBased
    Private dtBankAccount As New DataTable
    Private m_controller As New DataUserControlController
    Public Event _getParam(ByRef _lendingParam As Parameter.Lending)
    Public PayDate As Date
    Public Mfid As String
    Public MfFacilityNo As String
    Public MfReferenceNo As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack Then
        Else
            bindPaymentType()
            bindBankAccount("BA")
        End If
    End Sub

    Private Sub bindBankAccount(ByVal strBankType As String)
        dtBankAccount = m_controller.GetBankAccount(GetConnectionString, Me.sesBranchId, strBankType, "FD")
        cmbBankAccount.DataSource = dtBankAccount
        cmbBankAccount.DataTextField = "Name"
        cmbBankAccount.DataValueField = "ID"
        cmbBankAccount.DataBind()
    End Sub

    Private Sub bindPaymentType()
        Dim oDataTable As DataTable = New DataTable
        Dim oRow As DataRow
        Dim splitListData() As String
        Dim splitRow() As String
        Dim i As Integer
        Dim strListData As String

        oDataTable.Columns.Add("ID", System.Type.GetType("System.String"))
        oDataTable.Columns.Add("Description", System.Type.GetType("System.String"))

        oRow = oDataTable.NewRow()
        oRow("ID") = ""
        oRow("Description") = ""

        'only bank account
        strListData = "BA,Bank"
        splitListData = Split(strListData, "-")

        For i = 0 To UBound(splitListData)
            splitRow = Split(splitListData(i), ",")
            oRow("ID") = splitRow(0)
            oRow("Description") = splitRow(1)
            oDataTable.Rows.Add(oRow)
            oRow = oDataTable.NewRow()
        Next

        cbowop.DataValueField = "ID"
        cbowop.DataTextField = "Description"
        cbowop.DataSource = oDataTable
        cbowop.DataBind()
    End Sub

    Public Sub DoBind()
        txtDateCE.Text = PayDate
    End Sub

    Public Sub DoBindSave(ByRef property1 As Parameter.Lending)
        property1 = New Parameter.Lending

        property1.ValueDate = txtDateCE.Text
        property1.wop = cbowop.SelectedItem.Text
        property1.bankaccountid = cmbBankAccount.SelectedItem.Text
        property1.referenceno = txtreferenceno.Text
        property1.notes = txtKeterangan.Text
        RaiseEvent _getParam(property1)

    End Sub

End Class