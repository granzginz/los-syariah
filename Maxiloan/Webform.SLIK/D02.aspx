﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="D02.aspx.vb" Inherits="Maxiloan.Webform.SLIK.D02" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %>
<%@ Register TagPrefix="uc5" TagName="uckodekota" Src="../Webform.UserController/uckodekota.ascx" %>
<%@ Register TagPrefix="uc5" TagName="ucDateCE" Src="../Webform.UserController/ucDateCE.ascx" %>
<%@ Register TagPrefix="uc3" TagName="UcGolonganDebitur" Src="../Webform.UserController/UcGolonganDebitur.ascx" %>
<%@ Register TagPrefix="uc4" TagName="ucSektorEkonomi" Src="../Webform.UserController/ucSektorEkonomi.ascx" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Debitur Badan Usaha</title>
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />
    <link href="../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <script src="../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script> 
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%# Request.ServerVariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%#Request.ServerVariables("SERVER_NAME")%>/';		

        function fBack() {
            history.back(-1);
            return false;
        }
        function DeleteConfirm() {
            if (confirm("Apakah yakin mau hapus data ini ?")) {
                return true;
            }
            else {
                return false;
            }
        }

        $(document).ready(function () {
            change();
        });
    </script>
</head>

<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
        <div class="form_title">
            <div class="title_strip"></div>
            <div class="form_single">
                <h3>
                    SLIK D02 - Debitur Badan Usaha
                </h3>
            </div>
        </div>
            <asp:Panel ID="pnlList" runat="server">

                <div class="form_box_header">
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="medium_text">
                                Pilih Bulan Data
                        </label>                           
                            <asp:DropDownList ID="cbobulandata" runat="server" Width ="7%" CssClass="medium_text"/>       
                    </div>              
                </div>
                <div class="form_button">
                    <asp:Button ID="btnfind" runat="server" CausesValidation="False" Text="Find" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="btnGenerateUlang" runat="server" CausesValidation="False" Text="Generate Ulang" CssClass="small button green">
                    </asp:Button>
                </div>

                <asp:Panel ID="pnlcopybulandata" runat="server">
                    <div class="form_box_header">
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label class="medium_text">
                                    Generate Bulan Data
                            </label>
                            <asp:TextBox ID="txtcopybulandata" runat="server" CssClass="medium_text" width="6%"></asp:TextBox>
                            <asp:CalendarExtender ID="CalendarExtender4" runat="server" Enabled="True"
                                TargetControlID="txtcopybulandata" Format="yyyy-MM-dd" PopupPosition="BottomRight"></asp:CalendarExtender>  
                        </div>              
                    </div>
                <div class="form_button">
                    <asp:Button ID="btncopybulandata" runat="server" CausesValidation="False" Text="Generate" CssClass="small button green">
                    </asp:Button>
                </div>
                </asp:Panel>

        <div class="form_box_title" style="margin-top: 3px;" >
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtgList" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                                DataKeyField="ID" BorderStyle="None" BorderWidth="0" OnSortCommand="SortGrid"
                                CssClass="grid_general">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="EDIT">
                                        <ItemStyle Width="2%" HorizontalAlign="Center" CssClass="command_col"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../Images/iconedit.gif"
                                                CommandName="Edit"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn  HeaderText="DELETE">
                                        <ItemStyle  Width="7%" HorizontalAlign="Center" CssClass="command_col"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbDelete" runat="server" ImageUrl="../Images/icondelete.gif"
                                                CommandName="DEL"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="ID" SortExpression="ID" HeaderText="ID" visible="false">
                                    </asp:BoundColumn> 
                                    <asp:BoundColumn DataField="BULANDATA" SortExpression="BULANDATA" HeaderText="BULAN DATA" ItemStyle-Width="10%">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="CIF" SortExpression="CIF" HeaderText="CIF" ItemStyle-Width="15%">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="NoIdentitasBadanUsaha" SortExpression="NoIdentitasBadanUsaha" HeaderText="No Identitas Badan Usaha" ItemStyle-Width="20%">
                                    </asp:BoundColumn>
                                     <asp:BoundColumn DataField="KodeJenisBadanUsaha" SortExpression="KodeJenisBadanUsaha" HeaderText="Jenis" ItemStyle-Width="5%">
                                    </asp:BoundColumn> 
                                    <asp:BoundColumn DataField="NamaBadanUsaha" SortExpression="NamaBadanUsaha" HeaderText="Nama Badan Usaha" ItemStyle-Width="20%">
                                    </asp:BoundColumn> 
                                    <asp:BoundColumn DataField="OperasiData" SortExpression="OperasiData" HeaderText="Operasi Data">
                                    </asp:BoundColumn>
                                </Columns>
                            </asp:DataGrid>
                               <uc2:ucGridNav id="GridNaviD02SLIK" runat="server"/>
                        </div>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonAdd" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="BtnExportToTxt" runat="server"  Text="Export To TXT" CssClass="small button green"></asp:Button>               
                </div>

        <div class="form_box_title" style="margin-top: 3px;" >
            <div class="form_single">
                <h5>
                    CARI DEBITUR
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>Cari Berdasarkan</label>
                <asp:DropDownList ID="cboSearch" runat="server">
                    <asp:ListItem Value="NamaBadanUsaha" Selected="True">Nama Badan Usaha</asp:ListItem>
                    <asp:ListItem Value="CIF">CIF Badan Usaha</asp:ListItem>
                    <asp:ListItem Value="NoIdentitasBadanUsaha">No Id Badan Usaha</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearch" runat="server" Width="20%"></asp:TextBox>
            </div>
        </div>
        <div class="form_button" style="padding-bottom: 0; padding-top: 3px;"> 
             <asp:Button ID="btnSearch" runat="server" Text="Find" CssClass="small button blue"
                        CausesValidation="False"></asp:Button>
             <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="small button gray"
                        CausesValidation="False"></asp:Button>
        </div>

    </asp:Panel>          
            <asp:Panel ID="pnlAdd" runat="server">

                <asp:TextBox ID="txtFlag" runat="server" CssClass="medium_text"  Width ="2%" MaxLength ="100" Columns ="105"  Readonly="true" visible="false"></asp:TextBox>
                
                <asp:TextBox ID="txtid" runat="server" CssClass="medium_text" Visible="false"></asp:TextBox>

        <div class="form_box">
            <div class="form_single">
                <label class="medium_text">
                        BulanData
                </label>
                <asp:TextBox ID="txtbulandata" runat="server" CssClass="medium_text" Width="6%" ></asp:TextBox>
                <asp:CalendarExtender ID="CalendarExtender3" runat="server" Enabled="True" TargetControlID="txtbulandata" Format="yyyyMM" PopupPosition="BottomRight" DefaultView="Months"></asp:CalendarExtender>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage="*"
                    ControlToValidate="txtbulandata" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
		<div class="form_box">
			<div class="form_single">
				<label class="label_req">
                           CIF
                </label>
                <asp:TextBox ID="txtCIF" runat="server" CssClass="medium_text" Width ="10%" MaxLength ="100" Columns ="105" ReadOnly="true"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" Display="Dynamic"
                    ControlToValidate="txtCIF" CssClass="validator_general" ErrorMessage="*"></asp:RequiredFieldValidator>
			</div>
		</div>

		<div class="form_box">
			<div class="form_single">
				<label class="label_req">
                           No Identitas Badan Usaha
                </label>
                <asp:TextBox ID="txtNoIdentitasBU" runat="server" CssClass="medium_text"  Width ="10%" MaxLength ="100" Columns ="105"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator3" runat="server" display="Dynamic"
                    ControlToValidate="txtNoIdentitasBU" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
			</div>
		</div>

		<div class="form_box">
			<div class="form_single">
				<label class="label_req">
                           Nama Badan Usaha
                </label>
                <asp:TextBox ID="txtNamaBU" runat="server" CssClass="medium_text"  Width ="20%" MaxLength ="100" Columns ="105"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator4" runat="server" display="Dynamic"
                    ControlToValidate="txtNamaBU" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
			</div>
		</div>

		<div class="form_box">
			<div class="form_single">
				<label class="label_req">
                           Kode Bentuk Badan Usaha
                </label>
                
                    <asp:DropDownList ID="cboKodeBU" runat="server" Width ="20%" MaxLength ="100" Columns ="105" CssClass="medium_text"/>

                    
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Harap pilih Bidang Usaha"
                                ControlToValidate="cboKodeBU" InitialValue="Select One" Display="Dynamic"
                                CssClass="validator_general" />
                </div>
		</div>

		<div class="form_box">
			<div class="form_single">
				<label class="label_req">
                           Tempat Pendirian
                </label>
                <asp:TextBox ID="txtTmpPendirian" runat="server" CssClass="medium_text"  Width ="10%" MaxLength ="100" Columns ="105" ReadOnly="false"></asp:TextBox>
               <asp:RequiredFieldValidator ID="Requiredfieldvalidator6" runat="server" display="Dynamic"
                    ControlToValidate="txtTmpPendirian" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
			</div>
		</div>

		<div class="form_box">
			<div class="form_single">
				<label class="label_req">
                           No Akta Pendirian
                </label>
                <asp:TextBox ID="txtNoAktaPendirian" runat="server" CssClass="medium_text"  Width ="15%" MaxLength ="100" Columns ="105" ReadOnly="false"></asp:TextBox>
               <asp:RequiredFieldValidator ID="Requiredfieldvalidator7" runat="server" display="Dynamic"
                    ControlToValidate="txtNoAktaPendirian" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
			</div>
		</div>

		<div class="form_box">
			<div class="form_single">
				<label class="label_req">
                           Tanggal Akta Pendirian
                </label>

                <asp:TextBox ID="txtTglAktaPendirian" runat="server" CssClass="medium_text"  Width ="7%" MaxLength ="100" Columns ="105"></asp:TextBox>

                 <asp:CalendarExtender ID="CalenderExtender" runat="server" Enabled="True" TargetControlID="txtTglAktaPendirian" Format="yyyy/MM/dd" PopupPosition="BottomRight"></asp:CalendarExtender>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator8" runat="server" display="Dynamic" ControlToValidate="txtTglAktaPendirian" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
			</div>
		</div>

		<div class="form_box">
			<div class="form_single">
				<label class="label_req">
                           No Akta Perubahan Terakhir
                </label>
                <asp:TextBox ID="txtNoAktaPerubahan" runat="server" CssClass="medium_text"  Width ="15%" MaxLength ="100" Columns ="105" ></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator9" runat="server" display="Dynamic"
                    ControlToValidate="txtNoAktaPerubahan" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
			</div>
		</div>

		<div class="form_box">
			<div class="form_single">
				<label class="label_req">
                           Tanggal Akta Perubahan Terakhir
                </label>
                <asp:TextBox ID="txtTglAktaPerubahan" runat="server" CssClass="medium_text"  Width ="7%" MaxLength ="100" Columns ="105"></asp:TextBox>
                <asp:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True"
                            TargetControlID="txtTglAktaPerubahan" Format="yyyy/MM/dd" PopupPosition="BottomRight"></asp:CalendarExtender>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator10" runat="server" display="Dynamic"
                    ControlToValidate="txtTglAktaPerubahan" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
			</div>
		</div>

		<div class="form_box">
			<div class="form_single">
				<label >
                           No Telepon
                </label>
                <asp:TextBox ID="txtNoTelp" runat="server" CssClass="medium_text"   Width ="10%" MaxLength ="100" Columns ="105" ReadOnly="false"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator11" runat="server" display="Dynamic"
                    ControlToValidate="txtNoTelp" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
			</div>
		</div>

		<div class="form_box">
			<div class="form_single">
				<label>
                           No Telepon Selular
                </label>
                <asp:TextBox ID="txtNoTelpSelular" runat="server" CssClass="medium_text"  Width ="10%" MaxLength ="100" Columns ="105" ReadOnly="false"></asp:TextBox>
                
			</div>
		</div>

		<div class="form_box">
			<div class="form_single">
				<label>
                           Alamat Email
                </label>
                <asp:TextBox ID="txtEmail" runat="server" CssClass="medium_text" Width ="15%" MaxLength ="100" Columns ="105" ReadOnly="false"></asp:TextBox>
              
			</div>
		</div>

		<div class="form_box">
			<div class="form_single">
				<label class="label_req">
                          Alamat
                </label>
                <asp:TextBox ID="txtAlamat" runat="server" CssClass="medium_text"  Width ="30%" MaxLength ="100" Columns ="105"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator14" runat="server" display="Dynamic"
                    ControlToValidate="txtAlamat" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
                </div>
		</div>

		<div class="form_box">
			<div class="form_single">
				<label class="label_req">
                           Kelurahan
                </label>
                <asp:TextBox ID="txtKelurahan" runat="server" CssClass="medium_text"   Width ="10%" MaxLength ="100" Columns ="105" ReadOnly="false"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator18" runat="server" display="Dynamic"
                    ControlToValidate="txtKelurahan" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>			
            </div>
		</div>

		<div class="form_box">
			<div class="form_single">
				<label class="label_req">
                           Kecamatan
                </label>
                <asp:TextBox ID="txtKecamatan" runat="server" CssClass="medium_text" Width ="10%" MaxLength ="100" Columns ="105" ReadOnly="false"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator17" runat="server" display="Dynamic"
                    ControlToValidate="txtKecamatan" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>			
            </div>
		</div>

    <asp:updatepanel runat="server">
        <ContentTemplate>
		<div class="form_box">
			<div class="form_single">
				<label class="label_req">
                           Kode Kabupaten/Kota
                </label>
                <asp:TextBox ID="txtKota" runat="server" CssClass="medium_text"  Width ="8%" MaxLength ="100" Columns ="105" ReadOnly="true"></asp:TextBox>
                <asp:HiddenField id="hdfkota" runat="server" />
			    <asp:Button ID="btnLookup3" runat="server" CausesValidation="False" Text="..." CssClass="small buttongo blue" />
                <uc5:UcKodeKota id="oKodeKota" runat="server" OnCatSelected="oKodeKota_CatSelected"></uc5:UcKodeKota>
                 <asp:RequiredFieldValidator ID="Requiredfieldvalidator16" runat="server" display="Dynamic"
                    ControlToValidate="txtKota" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>           
            </div>
		</div>

		<div class="form_box">
			<div class="form_single">
				<label class="label_req">
                           Kode Pos
                </label>
                <asp:TextBox ID="txtKodePos" runat="server" CssClass="medium_text"  Width ="5%" MaxLength ="100" Columns ="105" ReadOnly="false"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator15" runat="server" display="Dynamic"
                    ControlToValidate="txtKodePos" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>			
            </div>

		</div>

		<div class="form_box">
			<div class="form_single">
				<label class="label_req">
                           Kode Negara Domisili
                </label>
                <asp:TextBox ID="txtKodeNegara" runat="server" CssClass="medium_text"  Width ="2%" MaxLength ="100" Columns ="105" ReadOnly="false"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator19" runat="server" display="Dynamic"
                    ControlToValidate="txtKodeNegara" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
			</div>
		</div>


		<div class="form_box">
			<div class="form_single">
				<label class="label_req">
                           Kode Bidang Usaha
                </label>
                <asp:TextBox ID="txtKodeBU" runat="server" CssClass="medium_text"  Width ="5%" MaxLength ="100" Columns ="105" ReadOnly="false"></asp:TextBox>
                
                 <asp:Button ID="btnLookup2" runat="server" CausesValidation="False" Text="..." CssClass="small buttongo blue" />
                 <uc4:ucSektorEkonomi id="oSektorEkonomi" runat="server" oncatselected="CatSelectedSektorEkonomi"></uc4:ucSektorEkonomi> 
                
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator20" runat="server" display="Dynamic"
                    ControlToValidate="txtKodeBU" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
			</div>
		</div>

		<div class="form_box">
			<div class="form_single">
				<label class="label_req">
                           Kode Hubungan Dengan Pelapor
                </label>
                 <asp:DropDownList ID="cboKodeHubungan" runat="server" Width ="30%" MaxLength ="250" Columns ="105" CssClass="medium_text"/>

                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Harap pilih kode hubungan"
                                ControlToValidate="cboKodeHubungan" InitialValue="Select One" Display="Dynamic"
                                CssClass="validator_general" />
			</div>
		</div>

		<div class="form_box">
			<div class="form_single">
				<label class="label_req">
                           Melanggar BMPK/BMPD/BMPP
                </label>
                 <asp:RadioButtonList ID="rbMelanggar" runat="server" class="opt_single"  RepeatDirection="Horizontal" AutoPostBack="false">
                <asp:ListItem Value="Y">Ya</asp:ListItem>
                <asp:ListItem Value="T" Selected="True">Tidak</asp:ListItem>
                </asp:RadioButtonList>
			</div>
		</div>

		<div class="form_box">
			<div class="form_single">
				<label class="label_req">
                           Melampaui BMPK/BMPD/BMPP
                </label>
                <asp:RadioButtonList ID="rbMelampaui" runat="server" class="opt_single"  RepeatDirection="Horizontal" AutoPostBack="false">
                <asp:ListItem Value="Y">Ya</asp:ListItem>
                <asp:ListItem Value="T" Selected="True">Tidak</asp:ListItem>
                </asp:RadioButtonList>

			</div>
		</div>

		<div class="form_box">
			<div class="form_single">
				<label class="label_req">
                           Go Public
                </label>
               <asp:RadioButtonList ID="rbGoPublic" runat="server" class="opt_single"  RepeatDirection="Horizontal" AutoPostBack="false">
                <asp:ListItem Value="Y">Ya</asp:ListItem>
                <asp:ListItem Value="T" Selected="True">Tidak</asp:ListItem>
                </asp:RadioButtonList>
			</div>
		</div>

		<div class="form_box">
			<div class="form_single">
				<label class="label_req">
                           Kode Golongan Debitur
                </label>
                <asp:TextBox ID="txtKodeGolongan" runat="server" CssClass="medium_text"  Width ="5%" MaxLength ="100" Columns ="105" ReadOnly="false"></asp:TextBox>
                
                 <asp:Button ID="btnLookup" runat="server" CausesValidation="False" Text="..." CssClass="small buttongo blue" />
                 <uc3:UcGolonganDebitur id="oGolonganDebitur" runat="server" oncatselected="CatSelectedGolonganDebitur"></uc3:UcGolonganDebitur> 
            
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator25" runat="server" display="Dynamic"
                    ControlToValidate="txtKodeGolongan" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
			</div>
		</div>
             </ContentTemplate>
        </asp:updatepanel>

		<div class="form_box">
			<div class="form_single">
				<label>
                           Lembaga Pemeringkat atau Rating
                </label>
                 <asp:DropDownList ID="cboLembagaPemeringkat" runat="server" Width ="20%" MaxLength ="100" Columns ="105" CssClass="medium_text" onchange="change()"/>

                   <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ErrorMessage="Harap pilih Lembaga Pemeringkat"
                                ControlToValidate="cboLembagaPemeringkat" InitialValue="Select One" Display="Dynamic"
                                CssClass="validator_general" /> 
                    
			</div>
		</div>

        <div class="form_box" id="hideprkt">
			<div class="form_single">
				<label>
                           Peringkat atau Rating Debitur
                </label>
                <asp:TextBox ID="txtPeringkat" runat="server" CssClass="medium_text"  Width ="2%" MaxLength ="100" Columns ="105" ReadOnly="false"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator26" runat="server" display="Dynamic"
                    ControlToValidate="txtPeringkat" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
			</div>
		</div>


		<div class="form_box" id="hidetglprkt">
			<div class="form_single">
				<label>
                           Tanggal Pemeringkat
                </label>
                <asp:TextBox ID="txtTglPemeringkat" runat="server" CssClass="medium_text" Width ="7%" MaxLength ="100" Columns ="105"></asp:TextBox>
                <asp:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True"
                            TargetControlID="txtTglPemeringkat" Format="yyyy/MM/dd" PopupPosition="BottomRight"></asp:CalendarExtender>

                <asp:RequiredFieldValidator ID="Requiredfieldvalidator28" runat="server" display="Dynamic"
                    ControlToValidate="txtTglPemeringkat" CssClass="validator_general" ErrorMessage="Harap Pilih Tanggal Pemeringkat" ></asp:RequiredFieldValidator>
			</div>
		</div>

		<div class="form_box">
			<div class="form_single">
				<label>
                           Nama Grup Usaha Debitur
                </label>
                <asp:TextBox ID="txtNmGroup" runat="server" CssClass="medium_text"  Width ="20%" MaxLength ="100" Columns ="105" ReadOnly="false"></asp:TextBox>
			</div>
		</div>

		<div class="form_box">
			<div class="form_single">
				<label class="label_req">
                           Kode Kantor Cabang
                </label>
                <asp:TextBox ID="txtKodeCabang" runat="server" CssClass="medium_text"  Width ="2%" MaxLength ="100" Columns ="105" ReadOnly="true"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator30" runat="server" display="Dynamic"
                    ControlToValidate="txtKodeCabang" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
			</div>
		</div>

    <asp:TextBox ID="txtOperasiData" runat="server" CssClass="medium_text" Width ="2%" MaxLength ="100" Columns ="105" visible="false"></asp:TextBox>

    <div class="form_button">
        <asp:Button ID="ButtonSave" runat="server" Text="Save" CssClass="small button blue"></asp:Button>
        <asp:Button ID="ButtonCancel" runat="server" CausesValidation="false" Text="Cancel" CssClass="small button gray"></asp:Button>
    </div>
          </asp:Panel>                 

  </form>   
  <script type="text/javascript">
        $(document).ready(function () {
            change();
        });

        function change() {
            var DropdownList = document.getElementById('<%=cboLembagaPemeringkat.ClientID %>');
            var SelectedIndex = DropdownList.selectedIndex;
            var x = document.getElementById('hideprkt');
            var y = document.getElementById('hidetglprkt');
            var valx = document.getElementById('<%=Requiredfieldvalidator26.ClientID%>');
            var valy = document.getElementById('<%=Requiredfieldvalidator28.ClientID%>');


            if (SelectedIndex == 0) {
                x.style.display = "none";
                y.style.display = "none";
                valx.enabled = false;
                valy.enabled = false;
            }
            else {
                x.style.display = "block";
                y.style.display = "block";
                valx.enabled = true;
                valy.enabled = true;
            }
        }
    </script>  
</body>
</html>

