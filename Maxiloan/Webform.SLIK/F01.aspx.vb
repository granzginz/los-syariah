﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.cbse
Imports Maxiloan.Exceptions
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.General
Imports Maxiloan.General.CommonCookiesHelper
Imports ClosedXML.Excel
Imports Maxiloan.Framework.SQLEngine
#End Region

Public Class F01
    Inherits Webform.WebBased

#Region "Constanta"
    Private oController As New F01Controller
    Private oCustomclass As New Parameter.F01
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private oParameter As New Parameter.F01
    Private cController As New F01Controller
#End Region

#Region "Property"
    Private Property Process() As String
        Get
            Return CType(ViewState("Process"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Process") = Value
        End Set
    End Property
#End Region

#Region "Sort"
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

#Region "Cancel"
    Private Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        pnlList.Visible = True
        pnlAdd.Visible = False
        Me.SearchBy = ""
        Me.SortBy = ""

        clean()
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

#Region " Private Const "
	Protected WithEvents ucNilaiProyek As ucNumberFormat
	Protected WithEvents ucPlafonAwal As ucNumberFormat
	Protected WithEvents ucPlafonEfektif As ucNumberFormat
	Protected WithEvents ucRealisasiPencairanBulanBerjalan As ucNumberFormat
	Protected WithEvents ucDenda As ucNumberFormat
	Protected WithEvents ucBakiDebet As ucNumberFormat
	Protected WithEvents ucNilaiDalamMataUangAsal As ucNumberFormat
	Protected WithEvents oSektorEkonomi As ucSektorEkonomi
	Protected WithEvents ucTunggakanPokok As ucNumberFormat
	Protected WithEvents ucTunggakanBungaImbalan As ucNumberFormat
#End Region
	Protected WithEvents GridNaviF01SLIK As ucGridNav

#Region "Page Load"
	Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
		lblMessage.Visible = False
		AddHandler GridNaviF01SLIK.PageChanged, AddressOf PageNavigation
		If SessionInvalid() Then
			Exit Sub
		End If
		If Not IsPostBack Then
			Me.FormID = "F01"
			If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
				pnlList.Visible = True
				pnlAdd.Visible = False
				pnlcopybulandata.Visible = False
				Me.SearchBy = ""
				Me.SortBy = ""
				DoBind(Me.SearchBy, Me.SortBy)

				FillCombo()
				FillCboBulanData(cbobulandata, "TblBulanDataSIPP")
			End If
		End If

		pnlcopybulandata.Visible = False
		btnGenerateUlang.Visible = False
	End Sub
#End Region

	Private Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs)
		currentPage = e.CurrentPage
		DoBind(Me.SearchBy, Me.SortBy, True)
		GridNaviF01SLIK.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
	End Sub

	Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String, Optional isFrNav As Boolean = False)
		Dim dtEntity As DataTable = Nothing
		Dim oParameter As New Maxiloan.Parameter.F01

		oParameter.strConnection = GetConnectionString()
		oParameter.WhereCond = cmdWhere
		oParameter.CurrentPage = currentPage
		oParameter.PageSize = pageSize
		oParameter.SortBy = SortBy
		oParameter = oController.GetF01(oParameter)

		If Not oParameter Is Nothing Then
			dtEntity = oParameter.ListData
			recordCount = oParameter.TotalRecord
		Else
			recordCount = 0
		End If

		dtgList.DataSource = dtEntity.DefaultView
		dtgList.CurrentPageIndex = 0
		dtgList.DataBind()

		FillCombo()
		FillCbo(cboKodeValuta, "dbo.MitraRefValuta")

		If (isFrNav = False) Then
			GridNaviF01SLIK.Initialize(recordCount, pageSize)
		End If

		If recordCount = 0 Then
			ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
			If txtSearch.Text <> "" Or cbobulandata.SelectedIndex = 0 Then
				pnlcopybulandata.Visible = False
			Else
				pnlcopybulandata.Visible = True
				FillCombo()
			End If
		ElseIf cbobulandata.SelectedIndex <> 0 Then
			pnlcopybulandata.Visible = False
			btnGenerateUlang.Visible = True
		Else
			pnlcopybulandata.Visible = False
			btnGenerateUlang.Visible = False
		End If
	End Sub

	Private Sub FillCombo()
		Dim strConn As String
		Dim dtCombo As New DataTable
		strConn = GetConnectionString()
	End Sub

	Sub FillCbo(ByVal cboName As DropDownList, ByVal Table As String)
		Dim oAssetData As New Parameter.F01
		Dim oData As New DataTable
		oAssetData.strConnection = GetConnectionString()
		oAssetData.Table = Table
		oAssetData = cController.GetCbo(oAssetData)
		oData = oAssetData.ListData
		cboName.DataSource = oData
		cboName.DataTextField = "KeteranganSandi"
		cboName.DataValueField = "Sandi"
		cboName.DataBind()
		cboName.Items.Insert(0, "Select One")
		cboName.Items(0).Value = ""
	End Sub

	Sub FillCboBulanData(ByVal cboName As DropDownList, ByVal Table As String)
		Dim oAssetData As New Parameter.F01

		Dim oData As New DataTable
		oAssetData.strConnection = GetConnectionString()
		oAssetData.Table = Table
		oAssetData = oController.GetCboBulandataSIPP(oAssetData)
		oData = oAssetData.ListData
		cboName.DataSource = oData
		cboName.DataTextField = "ID"
		cboName.DataValueField = "ID"
		cboName.DataBind()
		cboName.Items.Insert(0, "All")
		cboName.Items(0).Value = ""
	End Sub


	Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgList.ItemDataBound
		Dim imbDelete As ImageButton
		If e.Item.ItemIndex >= 0 Then
			imbDelete = CType(e.Item.FindControl("imbDelete"), ImageButton)
			imbDelete.Attributes.Add("Onclick", "return DeleteConfirm()")
		End If
	End Sub
	Sub clean()
		txt_TanggalAkadAwal.Text = ""
		txt_TanggalAkadAkhir.Text = ""
		txt_TanggalAwalFasilitas.Text = ""
		txt_TanggalMulaiFasilitas.Text = ""
		txt_TanggalJatuhTempoFasilitas.Text = ""
		txt_TanggalMacet.Text = ""
		txt_TglRestrukturisasiAwal.Text = ""
		txt_TglRestrukturisasiAkhir.Text = ""
		txt_TanggalKondisi.Text = ""
		txtbulandata.Text = ""
		txtid.Text = ""
		txtFlag.Text = ""
		txtNoRekFasilitas.Text = ""
		txtCIF.Text = ""
		txtNoAkadAwal.Text = ""
		oParameter.Tanggal_Akad_Awal = ""
		txtNoAkadAkhir.Text = ""
		txtKodeSektorEkonomi.Text = ""
		txtKodeKabKot.Text = ""
		ucNilaiProyek.Text = ""
		txtSukuBungaImbalan.Text = ""
		txtAsalKreditPembiayaantakeover.Text = ""
		txtSumberDana.Text = ""

		ucPlafonAwal.Text = ""
		ucPlafonEfektif.Text = ""
		ucRealisasiPencairanBulanBerjalan.Text = ""
		ucDenda.Text = ""
		ucBakiDebet.Text = ""
		ucNilaiDalamMataUangAsal.Text = ""
		ucTunggakanPokok.Text = ""
		ucTunggakanBungaImbalan.Text = ""
		txtJumlahHariTunggakan.Text = ""
		txtFrekwensiTunggakan.Text = ""
		txtFrekwensiRestrukturisasi.Text = ""
		txtKeterangan.Text = ""
		txtKodeKantorCabang.Text = ""
		txtOperasiData.Text = ""

		txtSearch.Text = ""
	End Sub

	Private Sub dtgList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgList.ItemCommand
		Dim oParameter As New Parameter.F01
		Dim dtEntity As New DataTable
		Dim err As String

		Try
			If e.CommandName = "Edit" Then
				If CheckFeature(Me.Loginid, "F01", "EDIT", "MAXILOAN") Then
					If SessionInvalid() Then
						Exit Sub
					End If
				End If
				Me.Process = "EDIT"
				pnlAdd.Visible = True
				pnlList.Visible = False
				txtbulandata.Enabled = False

				oParameter.ID = dtgList.DataKeys.Item(e.Item.ItemIndex).ToString
				oParameter.strConnection = GetConnectionString()
				oParameter = oController.GetF01Edit(oParameter)

				If Not oParameter Is Nothing Then
					dtEntity = oParameter.ListData
				End If


				If dtEntity.Rows.Count > 0 Then
					oRow = dtEntity.Rows(0)
					txt_TanggalAkadAwal.Text = Format(oRow("Tanggal_Akad_Awal"), "yyyy/MM/dd").ToString
					txt_TanggalAkadAkhir.Text = Format(oRow("Tanggal_Akad_Akhir"), "yyyy/MM/dd").ToString
					txt_TanggalAwalFasilitas.Text = Format(oRow("Tanggal_Awal_Kredit"), "yyyy/MM/dd").ToString
					txt_TanggalMulaiFasilitas.Text = Format(oRow("Tanggal_Mulai"), "yyyy/MM/dd").ToString
					txt_TanggalJatuhTempoFasilitas.Text = Format(oRow("Tanggal_Jatuh_Tempo"), "yyyy/MM/dd").ToString
					txt_TanggalMacet.Text = Format(oRow("Tanggal_Macet"), "yyyy/MM/dd").ToString
					txt_TglRestrukturisasiAwal.Text = Format(oRow("Tanggal_Restrukturisasi_Awal"), "yyyy/MM/dd").ToString
					txt_TglRestrukturisasiAkhir.Text = Format(oRow("Tanggal_Restrukturisasi_Akhir"), "yyyy/MM/dd").ToString
					txt_TanggalKondisi.Text = Format(oRow("Tanggal_Kondisi"), "yyyy/MM/dd").ToString
					cboKodeValuta.SelectedIndex = cboKodeValuta.Items.IndexOf(cboKodeValuta.Items.FindByValue(oRow("Kode_Valuta").ToString.Trim))
                    cboCaraRestrukturisasi.SelectedIndex = cboCaraRestrukturisasi.Items.IndexOf(cboCaraRestrukturisasi.Items.FindByValue(oRow("Kode_Cara_Restrukturisasi").ToString.Trim))
                    cboKodeKondisi.SelectedIndex = cboKodeKondisi.Items.IndexOf(cboKodeKondisi.Items.FindByValue(oRow("Kode_Kondisi").ToString.Trim))
                    cboSifatKreditdanPembiayaan.SelectedIndex = cboSifatKreditdanPembiayaan.Items.IndexOf(cboSifatKreditdanPembiayaan.Items.FindByValue(oRow("Kode_Sifat_Kredit_Pembiayaan").ToString.Trim))
                    cboJenisKreditdanPembiayaan.SelectedIndex = cboJenisKreditdanPembiayaan.Items.IndexOf(cboJenisKreditdanPembiayaan.Items.FindByValue(oRow("Kode_Jenis_Kredit_Pembiayaan").ToString.Trim))
                    'cboJenisKreditdanPembiayaan.SelectedItem.Value = cboJenisKreditdanPembiayaan.Items.IndexOf(cboJenisKreditdanPembiayaan.Items.FindByValue(oRow("Kode_Jenis_Kredit_Pembiayaan").ToString.Trim))
                    cboKodeAkadKredit.SelectedIndex = cboKodeAkadKredit.Items.IndexOf(cboKodeAkadKredit.Items.FindByValue(oRow("Kode_Skim_Akad_Pembiayaan").ToString.Trim))
                    cboFrekuensiPerpanjangan.SelectedIndex = cboFrekuensiPerpanjangan.Items.IndexOf(cboFrekuensiPerpanjangan.Items.FindByValue(oRow("Baru_Perpanjangan").ToString.Trim))
                    cboKodeKategoriDebitur.SelectedIndex = cboKodeKategoriDebitur.Items.IndexOf(cboKodeKategoriDebitur.Items.FindByValue(oRow("Kode_Kategori_Debitur").ToString.Trim))
                    cboKodeJenisPenggunaanFasilitas.SelectedIndex = cboKodeJenisPenggunaanFasilitas.Items.IndexOf(cboKodeJenisPenggunaanFasilitas.Items.FindByValue(oRow("Kode_Jenis_Penggunaan").ToString.Trim))
                    cboKodeOrientasiPenggunaanFasilitas.SelectedIndex = cboKodeOrientasiPenggunaanFasilitas.Items.IndexOf(cboKodeOrientasiPenggunaanFasilitas.Items.FindByValue(oRow("Kode_Orientasi_Penggunaan").ToString.Trim))
                    cboJenisSukuBungaImbalan.SelectedIndex = cboJenisSukuBungaImbalan.Items.IndexOf(cboJenisSukuBungaImbalan.Items.FindByValue(oRow("Jenis_Suku_Bunga_Imbalan").ToString.Trim))
                    cboKreditPembiayaanPemerintah.SelectedIndex = cboKreditPembiayaanPemerintah.Items.IndexOf(cboKreditPembiayaanPemerintah.Items.FindByValue(oRow("Kredit_Program_Pemerintah").ToString.Trim))
                    cboKodeKualitasKredit.SelectedIndex = cboKodeKualitasKredit.Items.IndexOf(cboKodeKualitasKredit.Items.FindByValue(oRow("Kode_Kolektibilitas").ToString.Trim))
                    cboSebabMacet.SelectedIndex = cboSebabMacet.Items.IndexOf(cboSebabMacet.Items.FindByValue(oRow("Kode_Sebab_Macet").ToString.Trim))

                    If txt_TanggalAkadAwal.Text = "1900/01/01" Then
						txt_TanggalAkadAwal.Text = ""
					End If
					If txt_TanggalAkadAkhir.Text = "1900/01/01" Then
						txt_TanggalAkadAkhir.Text = ""
					End If
					If txt_TanggalAwalFasilitas.Text = "1900/01/01" Then
						txt_TanggalAwalFasilitas.Text = ""
					End If
					If txt_TanggalMulaiFasilitas.Text = "1900/01/01" Then
						txt_TanggalMulaiFasilitas.Text = ""
					End If
					If txt_TanggalJatuhTempoFasilitas.Text = "1900/01/01" Then
						txt_TanggalJatuhTempoFasilitas.Text = ""
					End If
					If txt_TanggalMacet.Text = "1900/01/01" Then
						txt_TanggalMacet.Text = ""
					End If
					If txt_TglRestrukturisasiAwal.Text = "1900/01/01" Then
						txt_TglRestrukturisasiAwal.Text = ""
					End If
					If txt_TglRestrukturisasiAkhir.Text = "1900/01/01" Then
						txt_TglRestrukturisasiAkhir.Text = ""
					End If
					If txt_TanggalKondisi.Text = "1900/01/01" Then
						txt_TanggalKondisi.Text = ""
					End If
				End If


                'cboKodeValuta.SelectedItem.Value = oParameter.Listdata.Rows(0)("Kode_Valuta")

                txtbulandata.Text = oParameter.ListData.Rows(0)("BulanData")
				txtid.Text = oParameter.ListData.Rows(0)("ID")
				txtFlag.Text = oParameter.ListData.Rows(0)("Flag_Detail")
				txtNoRekFasilitas.Text = oParameter.ListData.Rows(0)("Nomor_Rekening_Fasilitas")
				txtCIF.Text = oParameter.ListData.Rows(0)("Nomor_CIF_Debitur")
				txtNoAkadAwal.Text = oParameter.ListData.Rows(0)("Nomor_Akad_Awal")
				oParameter.Tanggal_Akad_Awal = ConvertDate(txt_TanggalAkadAwal.Text.Trim)
				txtNoAkadAkhir.Text = oParameter.ListData.Rows(0)("Nomor_Akad_Akhir")
				txtKodeSektorEkonomi.Text = oParameter.ListData.Rows(0)("Kode_Sektor_Ekonomi")
				txtKodeKabKot.Text = oParameter.ListData.Rows(0)("Kode_Dati_Kota")
				ucNilaiProyek.Text = oParameter.ListData.Rows(0)("Nilai_Proyek").ToString
				txtSukuBungaImbalan.Text = oParameter.ListData.Rows(0)("Persentase_Suku_Bunga_Imbalan")
				txtAsalKreditPembiayaantakeover.Text = oParameter.ListData.Rows(0)("Takeover_Dari")
				txtSumberDana.Text = oParameter.ListData.Rows(0)("Sumber_Dana")

				ucPlafonAwal.Text = oParameter.ListData.Rows(0)("Plafon_Awal").ToString
				ucPlafonEfektif.Text = oParameter.ListData.Rows(0)("Plafon").ToString
				ucRealisasiPencairanBulanBerjalan.Text = oParameter.ListData.Rows(0)("Realisasi_Pencairan_Bulan_Berjalan").ToString
				ucDenda.Text = oParameter.ListData.Rows(0)("Denda").ToString
				ucBakiDebet.Text = oParameter.ListData.Rows(0)("Baki_Debet").ToString
				ucNilaiDalamMataUangAsal.Text = oParameter.ListData.Rows(0)("Nilai_Dalam_Mata_Uang_Asal").ToString
				ucTunggakanPokok.Text = oParameter.ListData.Rows(0)("Tunggakan_Pokok").ToString
				ucTunggakanBungaImbalan.Text = oParameter.ListData.Rows(0)("Tunggakan_Bunga").ToString
				txtJumlahHariTunggakan.Text = oParameter.ListData.Rows(0)("Jumlah_Hari_Tunggakan")
				txtFrekwensiTunggakan.Text = oParameter.ListData.Rows(0)("Frekuensi_Tunggakan")
				txtFrekwensiRestrukturisasi.Text = oParameter.ListData.Rows(0)("Frekuensi_Restrukturisasi")
				txtKeterangan.Text = oParameter.ListData.Rows(0)("Keterangan")
				txtKodeKantorCabang.Text = oParameter.ListData.Rows(0)("Kode_Kantor_Cabang")
				txtOperasiData.Text = oParameter.ListData.Rows(0)("Operasi_Data")

			ElseIf e.CommandName = "DEL" Then
				If CheckFeature(Me.Loginid, "F01", "DEL", "MAXILOAN") Then
					If SessionInvalid() Then
						Exit Sub
					End If
				End If
				Dim customClass As New Parameter.F01
				With customClass
					.ID = dtgList.DataKeys.Item(e.Item.ItemIndex).ToString
					.strConnection = GetConnectionString()
				End With

				err = oController.GetF01Delete(customClass)
				If err <> "" Then
					ShowMessage(lblMessage, err, True)
				Else
					ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, False)
				End If
				DoBind(Me.SearchBy, Me.SortBy)
			End If
		Catch ex As Exception
			ShowMessage(lblMessage, ex.Message, True)
		End Try
	End Sub


#Region "Save"
	Private Sub ButtonSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
		Dim oParameter As New Parameter.F01
		Dim errMessage As String = ""

		If Me.Process = "ADD" Then
			With oParameter

				.BulanData = txtbulandata.Text
				.Tanggal_Akad_Awal = txt_TanggalAkadAwal.Text
				.Tanggal_Akad_Akhir = txt_TanggalAkadAkhir.Text
				.Tanggal_Awal_Kredit = txt_TanggalAwalFasilitas.Text
				.Tanggal_Mulai = txt_TanggalMulaiFasilitas.Text
				.Tanggal_Jatuh_Tempo = txt_TanggalJatuhTempoFasilitas.Text
				.Tanggal_Macet = txt_TanggalMacet.Text
				.Tanggal_Restrukturisasi_Awal = txt_TglRestrukturisasiAwal.Text
				.Tanggal_Restrukturisasi_Akhir = txt_TglRestrukturisasiAkhir.Text
				.Tanggal_Kondisi = txt_TanggalKondisi.Text
				.Kode_Sifat_Kredit_Pembiayaan = cboSifatKreditdanPembiayaan.SelectedValue
				.Kode_Jenis_Kredit_Pembiayaan = cboJenisKreditdanPembiayaan.SelectedValue
				.Kode_Skim_Akad_Pembiayaan = cboKodeAkadKredit.SelectedValue
				.Baru_Perpanjangan = cboFrekuensiPerpanjangan.SelectedValue
				.Kode_Kategori_Debitur = cboKodeKategoriDebitur.SelectedValue
				.Kode_Jenis_Penggunaan = cboKodeJenisPenggunaanFasilitas.SelectedValue
				.Kode_Orientasi_Penggunaan = cboKodeOrientasiPenggunaanFasilitas.SelectedValue
				.Jenis_Suku_Bunga_Imbalan = cboJenisSukuBungaImbalan.SelectedValue
				.Kredit_Program_Pemerintah = cboKreditPembiayaanPemerintah.SelectedValue
				.Kode_Kolektibilitas = cboKodeKualitasKredit.SelectedValue
				.Kode_Sebab_Macet = cboSebabMacet.SelectedValue
				.Kode_Cara_Restrukturisasi = cboCaraRestrukturisasi.SelectedValue
				.Kode_Kondisi = cboKodeKondisi.SelectedValue
				.Kode_Valuta = cboKodeValuta.SelectedValue
				.Nomor_Rekening_Fasilitas = txtNoRekFasilitas.Text
				.Nomor_CIF_Debitur = txtCIF.Text
				.Nomor_Akad_Awal = txtNoAkadAwal.Text
				.Nomor_Akad_Akhir = txtNoAkadAkhir.Text
				.Kode_Sektor_Ekonomi = txtKodeSektorEkonomi.Text
				.Kode_Dati_Kota = txtKodeKabKot.Text
				.Nilai_Proyek = ucNilaiProyek.Text
				.Persentase_Suku_Bunga_Imbalan = txtSukuBungaImbalan.Text
				.Takeover_Dari = txtAsalKreditPembiayaantakeover.Text
				.Sumber_Dana = txtSumberDana.Text
				.Plafon_Awal = ucPlafonAwal.Text
				.Plafon = ucPlafonEfektif.Text
				.Realisasi_Pencairan_Bulan_Berjalan = ucRealisasiPencairanBulanBerjalan.Text
				.Denda = ucDenda.Text
				.Baki_Debet = ucBakiDebet.Text
				.Nilai_Dalam_Mata_Uang_Asal = ucNilaiDalamMataUangAsal.Text
				.Tunggakan_Pokok = ucTunggakanPokok.Text
				.Tunggakan_Bunga = ucTunggakanBungaImbalan.Text
				.Jumlah_Hari_Tunggakan = txtJumlahHariTunggakan.Text
				.Frekuensi_Tunggakan = txtFrekwensiTunggakan.Text
				.Frekuensi_Restrukturisasi = txtFrekwensiRestrukturisasi.Text
				.Keterangan = txtKeterangan.Text
				.Kode_Kantor_Cabang = txtKodeKantorCabang.Text
				.Operasi_Data = "C"

				.strConnection = GetConnectionString()
			End With
			oParameter = oController.GetF01Add(oParameter)
		ElseIf Me.Process = "EDIT" Then
			With oParameter

				.ID = txtid.Text
				.Tanggal_Akad_Awal = txt_TanggalAkadAwal.Text
				.Tanggal_Akad_Akhir = txt_TanggalAkadAkhir.Text
				.Tanggal_Awal_Kredit = txt_TanggalAwalFasilitas.Text
				.Tanggal_Mulai = txt_TanggalMulaiFasilitas.Text
				.Tanggal_Jatuh_Tempo = txt_TanggalJatuhTempoFasilitas.Text
				.Tanggal_Macet = txt_TanggalMacet.Text
				.Tanggal_Restrukturisasi_Awal = ConvertDate2(txt_TglRestrukturisasiAwal.Text).ToString("yyyy/MM/dd")
				.Tanggal_Restrukturisasi_Akhir = txt_TglRestrukturisasiAkhir.Text
				.Tanggal_Kondisi = txt_TanggalKondisi.Text
				.Kode_Sifat_Kredit_Pembiayaan = cboSifatKreditdanPembiayaan.SelectedValue
				.Kode_Jenis_Kredit_Pembiayaan = cboJenisKreditdanPembiayaan.SelectedValue
				.Kode_Skim_Akad_Pembiayaan = cboKodeAkadKredit.SelectedValue
				.Baru_Perpanjangan = cboFrekuensiPerpanjangan.SelectedValue
				.Kode_Kategori_Debitur = cboKodeKategoriDebitur.SelectedValue
				.Kode_Jenis_Penggunaan = cboKodeJenisPenggunaanFasilitas.SelectedValue
				.Kode_Orientasi_Penggunaan = cboKodeOrientasiPenggunaanFasilitas.SelectedValue
				.Jenis_Suku_Bunga_Imbalan = cboJenisSukuBungaImbalan.SelectedValue
				.Kredit_Program_Pemerintah = cboKreditPembiayaanPemerintah.SelectedValue
				.Kode_Kolektibilitas = cboKodeKualitasKredit.SelectedValue
				.Kode_Sebab_Macet = cboSebabMacet.SelectedValue
				.Kode_Cara_Restrukturisasi = cboCaraRestrukturisasi.SelectedValue
				.Kode_Kondisi = cboKodeKondisi.SelectedValue
				.Kode_Valuta = cboKodeValuta.SelectedValue
				.Nomor_Rekening_Fasilitas = txtNoRekFasilitas.Text
				.Nomor_CIF_Debitur = txtCIF.Text
				.Nomor_Akad_Awal = txtNoAkadAwal.Text
				.Nomor_Akad_Akhir = txtNoAkadAkhir.Text
				.Kode_Sektor_Ekonomi = txtKodeSektorEkonomi.Text
				.Kode_Dati_Kota = txtKodeKabKot.Text
				.Nilai_Proyek = ucNilaiProyek.Text
				.Persentase_Suku_Bunga_Imbalan = txtSukuBungaImbalan.Text
				.Takeover_Dari = txtAsalKreditPembiayaantakeover.Text
				.Sumber_Dana = txtSumberDana.Text
				.Plafon_Awal = ucPlafonAwal.Text
				.Plafon = ucPlafonEfektif.Text
				.Realisasi_Pencairan_Bulan_Berjalan = ucRealisasiPencairanBulanBerjalan.Text
				.Denda = ucDenda.Text
				.Baki_Debet = ucBakiDebet.Text
				.Nilai_Dalam_Mata_Uang_Asal = ucNilaiDalamMataUangAsal.Text
				.Tunggakan_Pokok = ucTunggakanPokok.Text
				.Tunggakan_Bunga = ucTunggakanBungaImbalan.Text
				.Jumlah_Hari_Tunggakan = txtJumlahHariTunggakan.Text
				.Frekuensi_Tunggakan = txtFrekwensiTunggakan.Text
				.Frekuensi_Restrukturisasi = txtFrekwensiRestrukturisasi.Text
				.Keterangan = txtKeterangan.Text
				.Kode_Kantor_Cabang = txtKodeKantorCabang.Text
				.Operasi_Data = "U"

				.strConnection = GetConnectionString()
			End With
            oParameter = oController.GetF01Save(oParameter)
        End If

        Me.SearchBy = ""
        Me.SortBy = ""
        DoBind(Me.SearchBy, Me.SortBy)
        pnlAdd.Visible = False
        pnlList.Visible = True

        If Me.Process = "ADD" Then
            ShowMessage(lblMessage, "Simpan data berhasil.....", False)
        Else
            ShowMessage(lblMessage, "Updated data berhasil .....", False)
        End If
    End Sub

#End Region
#Region "Generate Data"
    Public Function GetCopy(ByVal customclass As Parameter.F01) As Parameter.F01
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter("@BULANDATA", SqlDbType.VarChar, 20)
            params(0).Value = customclass.BulanData

            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spSLIKF01Generate", params).Tables(0)
            Return customclass
        Catch exp As Exception
        End Try
        ShowMessage(lblMessage, "Generate Data Berhasil......", False)
    End Function

    Private Sub btncopybulandata_Click(sender As Object, e As EventArgs) Handles btncopybulandata.Click
        Dim oEntities As New Parameter.F01
        Dim dtViewData As New DataTable
        With oEntities
            .BulanData = txtcopybulandata.Text
            .strConnection = GetConnectionString()
        End With
        GetCopy(oEntities)
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region
#Region "Add"
    Private Sub imbAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonAdd.Click
        If CheckFeature(Me.Loginid, "F01", "ADD", "MAXILOAN") Then
            pnlAdd.Visible = True
            pnlList.Visible = False
			txtbulandata.Enabled = True
			txtCIF.ReadOnly = False
			txtKodeKantorCabang.ReadOnly = False

			Me.Process = "ADD"
            clean()
            'DoBind(Me.SearchBy, Me.SortBy)
        End If
    End Sub
#End Region
#Region "EXPORT"
    Public Sub ExportTableData(ByVal dtdata As DataTable, ByVal Rc As Long, ByVal nm As String)
        Dim oParameter As New Maxiloan.Parameter.F01

        Dim ResClient As New StringBuilder
        Using sw As New StringWriter(ResClient)

            Dim Header = ("H|0201|251230|" & cbobulandata.Text.Substring(0, cbobulandata.Text.Length - 2) & "|" & cbobulandata.Text.Substring(4) & "|" & "F01" & "|" & Rc & "|" & Rc)
            sw.WriteLine(Header)
            For Each drow As DataRow In dtdata.Rows
                Dim lineoftext = String.Join("|", drow.ItemArray.Select(Function(s) s.ToString).ToArray)
                sw.WriteLine(lineoftext)
            Next

            Response.Clear()
            Response.Buffer = True
            Response.Charset = ""
            Response.ContentType = "text/plain"
            Response.AddHeader("content-disposition", "attachment;filename=" & nm & ".txt")
            Response.Write(ResClient.ToString())

            Using MyMemoryStream As New MemoryStream()
                MyMemoryStream.WriteTo(Response.OutputStream)
                Response.Flush()
                Response.End()
            End Using

        End Using
    End Sub

    Public Function GetToTXT(ByVal customclass As Parameter.F01) As Parameter.F01
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter("@BULANDATA", SqlDbType.VarChar, 20)
            params(0).Value = customclass.BulanData
            params(1) = New SqlParameter("@Totalrecords", SqlDbType.BigInt)
            params(1).Direction = ParameterDirection.Output

            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spSLIKF01Txt", params).Tables(0)
            customclass.TotalRecord = CType(params(1).Value, Int64)
            Return customclass
        Catch exp As Exception
        End Try
    End Function

    Private Sub BtnExportToTxt_Click(sender As Object, e As System.EventArgs) Handles BtnExportToTxt.Click
        Dim oEntities As New Parameter.F01
        Dim dtViewData As New DataTable

        If cbobulandata.Text <> "" Then
            With oEntities
                .BulanData = cbobulandata.Text
                .strConnection = GetConnectionString()
            End With
            oEntities = GetToTXT(oEntities)
            dtViewData = oEntities.ListData
            Rc = oEntities.TotalRecord
            ExportTableData(dtViewData, Rc, "0201.251230." & oEntities.BulanData.Substring(0, oEntities.BulanData.Length - 2) & "." & oEntities.BulanData.Substring(4) & "." & "F01.01")
        Else
            ShowMessage(lblMessage, "Harap Pilih BulanData .....", True)
        End If
    End Sub
    Private Sub btnfind_Click(sender As Object, e As EventArgs) Handles btnfind.Click
		If cbobulandata.Text <> "" Then
			Me.SearchBy = "BULANDATA" & " like '%" & cbobulandata.Text.Trim & "%'"

			Me.SortBy = ""
			DoBind(Me.SearchBy, Me.SortBy)
		Else
			Me.SearchBy = ""

			Me.SortBy = ""
			DoBind(Me.SearchBy, Me.SortBy)
		End If

		If cbobulandata.Text <> "" Then

			Dim dateString, format As String
			Dim result As Date
			Dim provider As Globalization.CultureInfo = Globalization.CultureInfo.InvariantCulture

			' Untuk Parsing EOMONTH ke textbox generate
			dateString = (cbobulandata.SelectedValue)
			format = "yyyyMM"
			result = Date.ParseExact(dateString, format, provider)

			txtcopybulandata.Text = result.AddMonths(1).AddDays(-1).ToString("yyy-MM-dd")
		End If
		clean()
	End Sub
#End Region
    Protected Sub btnLookup_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnLookup.Click
        oSektorEkonomi.CmdWhere = "All"
        oSektorEkonomi.Sort = "Kode ASC"
        oSektorEkonomi.Popup()
    End Sub
    Public Sub CatSelectedSektorEkonomi(ByVal Kode As String, SektorEkonomi As String)
        txtKodeSektorEkonomi.Text = Kode

    End Sub
#Region "Search"
	Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click

		If cboSearch.Text <> "" Then
			Me.SearchBy = cboSearch.SelectedItem.Value & " like '%" & Replace(txtSearch.Text.Trim, "'", "''") & "%'"

			Me.SortBy = ""
			DoBind(Me.SearchBy, Me.SortBy)
		Else
			Me.SearchBy = ""

			Me.SortBy = ""
			DoBind(Me.SearchBy, Me.SortBy)
		End If
	End Sub
#End Region

#Region "Reset"
	Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
		cboSearch.SelectedIndex = 0
		cbobulandata.SelectedIndex = 0
		txtSearch.Text = ""
		Me.SearchBy = ""
		DoBind(Me.SearchBy, Me.SortBy)
		clean()
	End Sub

	Private Sub btnGenerateUlang_Click(sender As Object, e As EventArgs) Handles btnGenerateUlang.Click
		btnGenerateUlang.Visible = False
		pnlcopybulandata.Visible = True
	End Sub
#End Region

End Class