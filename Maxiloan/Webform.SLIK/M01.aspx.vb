﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.cbse
Imports Maxiloan.Exceptions
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.General
Imports Maxiloan.General.CommonCookiesHelper
Imports ClosedXML.Excel
Imports Maxiloan.Framework.SQLEngine
#End Region

Public Class M01
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents KodeKota As ucKodeKota
#Region "Constanta"
    Private oController As New M01Controller
    Private oCustomclass As New Parameter.M01
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private oParameter As New Parameter.M01
    Private cController As New M01Controller
#End Region

#Region "Property"
    Private Property Process() As String
        Get
            Return CType(ViewState("Process"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Process") = Value
        End Set
    End Property
#End Region

#Region "Sort"
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

#Region "Cancel"
    Private Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        pnlList.Visible = True
        pnlAdd.Visible = False
        Me.SearchBy = ""
        Me.SortBy = ""

        'clean()
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

    Protected WithEvents GridNaviM01SLIK As ucGridNav

    Sub clean()
        txtbulandata.Text = ""
        txtid.Text = ""
        txtFlag.Text = ""
        txtnoidentitas.Text = ""
        txtCIF.Text = ""
        txtnmpengurus.Text = ""
        txtalamat.Text = ""
        txtkelurahan.Text = ""
        txtkecamatan.Text = ""
        txtkdkabkot.Text = ""
        txtpangsa.Text = ""
        txtKodeCabang.Text = ""
		txtOperasiData.Text = ""
		txtSearch.Text = ""
	End Sub
#Region "Page Load"
    Public Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Visible = False
        AddHandler GridNaviM01SLIK.PageChanged, AddressOf PageNavigation
        If SessionInvalid() Then
            Exit Sub
        End If
		If Not IsPostBack Then
			Me.FormID = "M01"
			If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
				pnlList.Visible = True
				pnlAdd.Visible = False
				pnlcopybulandata.Visible = False
				Me.SearchBy = ""
				Me.SortBy = ""
				DoBind(Me.SearchBy, Me.SortBy)

				FillCombo()
				FillCboBulanData(cbobulandata, "TblBulanDataSIPP")
			End If
		End If

		pnlcopybulandata.Visible = False
		btnGenerateUlang.Visible = False
	End Sub
#End Region

    Private Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        currentPage = e.CurrentPage
        DoBind(Me.SearchBy, Me.SortBy, True)
        GridNaviM01SLIK.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
    End Sub

#Region "BindGrid"
    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String, Optional isFrNav As Boolean = False)
        Dim dtEntity As DataTable = Nothing
        Dim oParameter As New Maxiloan.Parameter.M01

        oParameter.strConnection = GetConnectionString()
        oParameter.WhereCond = cmdWhere
        oParameter.CurrentPage = currentPage
        oParameter.PageSize = pageSize
        oParameter.SortBy = SortBy
        oParameter = oController.GetM01(oParameter)

        If Not oParameter Is Nothing Then
            dtEntity = oParameter.ListData
            recordCount = oParameter.TotalRecord
        Else
            recordCount = 0
        End If

        dtgList.DataSource = dtEntity.DefaultView
        dtgList.CurrentPageIndex = 0
        dtgList.DataBind()

        FillCombo()

        If (isFrNav = False) Then
            GridNaviM01SLIK.Initialize(recordCount, pageSize)
        End If

		If recordCount = 0 Then
			ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
			If txtSearch.Text <> "" Or cbobulandata.SelectedIndex = 0 Then
				pnlcopybulandata.Visible = False
			Else
				pnlcopybulandata.Visible = True
				FillCombo()
			End If
		ElseIf cbobulandata.SelectedIndex <> 0 Then
			pnlcopybulandata.Visible = False
			btnGenerateUlang.Visible = True
		Else
			pnlcopybulandata.Visible = False
			btnGenerateUlang.Visible = False
		End If
	End Sub
#End Region
#Region "DropDown"
    Private Sub FillCombo()
        Dim strConn As String
        Dim dtCombo As New DataTable
        strConn = GetConnectionString()
    End Sub

    Sub FillCboBulanData(ByVal cboName As DropDownList, ByVal Table As String)
        Dim oAssetData As New Parameter.M01

        Dim oData As New DataTable
        oAssetData.strConnection = GetConnectionString()
        oAssetData.Table = Table
        oAssetData = oController.GetCboBulandataSIPP(oAssetData)
        oData = oAssetData.ListData
        cboName.DataSource = oData
        cboName.DataTextField = "ID"
        cboName.DataValueField = "ID"
        cboName.DataBind()
		cboName.Items.Insert(0, "All")
		cboName.Items(0).Value = ""
    End Sub
#End Region
#Region "GRID"
    Private Sub dtgList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgList.ItemCommand
        Dim oParameter As New Parameter.M01
        Dim dtEntity As New DataTable
        Dim err As String

        Try
            If e.CommandName = "Edit" Then
                If CheckFeature(Me.Loginid, "M01", "EDIT", "MAXILOAN") Then
                    If SessionInvalid() Then
                        Exit Sub
                    End If
                End If
                Me.Process = "EDIT"
                pnlAdd.Visible = True
                pnlList.Visible = False
                txtbulandata.Enabled = False

                oParameter.ID = dtgList.DataKeys.Item(e.Item.ItemIndex).ToString
                oParameter.strConnection = GetConnectionString()
                oParameter = oController.GetM01Edit(oParameter)

                If Not oParameter Is Nothing Then
                    dtEntity = oParameter.ListData
                End If



                If Not oParameter Is Nothing Then
                    dtEntity = oParameter.ListData
                End If

                If dtEntity.Rows.Count > 0 Then
                    oRow = dtEntity.Rows(0)
                    cboKdJnsIdentitas.SelectedIndex = cboKdJnsIdentitas.Items.IndexOf(cboKdJnsIdentitas.Items.FindByValue(oRow("KodeJenisIdentitas").ToString.Trim))
                    cbojeniskelamin.SelectedIndex = cbojeniskelamin.Items.IndexOf(cbojeniskelamin.Items.FindByValue(oRow("JenisKelamin").ToString.Trim))
                    cbostatuspemilik.SelectedIndex = cbostatuspemilik.Items.IndexOf(cbostatuspemilik.Items.FindByValue(oRow("StatusPengurus").ToString.Trim))
                    cbokdjabatan.SelectedIndex = cbokdjabatan.Items.IndexOf(cbokdjabatan.Items.FindByValue(oRow("KodeJabatan").ToString.Trim))
                End If
                txtbulandata.Text = oParameter.ListData.Rows(0)("BulanData")
                txtid.Text = oParameter.ListData.Rows(0)("ID")
                txtFlag.Text = oParameter.ListData.Rows(0)("FlagDetail")
                txtnoidentitas.Text = oParameter.ListData.Rows(0)("NoIDPEngurus")
                txtCIF.Text = oParameter.ListData.Rows(0)("CIF")
                txtnmpengurus.Text = oParameter.ListData.Rows(0)("NamaPengurus")
                txtalamat.Text = oParameter.ListData.Rows(0)("Alamat")
                txtkelurahan.Text = oParameter.ListData.Rows(0)("Kelurahan")
                txtkecamatan.Text = oParameter.ListData.Rows(0)("Kecamatan")
                txtkdkabkot.Text = oParameter.ListData.Rows(0)("KodeKota")
                'txtkdjabatan.Text = oParameter.ListData.Rows(0)("KodeJabatan")
                txtpangsa.Text = oParameter.ListData.Rows(0)("PangsaKepemilikan")
                txtKodeCabang.Text = oParameter.ListData.Rows(0)("KodeCabang")
                txtOperasiData.Text = oParameter.ListData.Rows(0)("OperasiData")

            ElseIf e.CommandName = "DEL" Then
                If CheckFeature(Me.Loginid, "M01", "DEL", "MAXILOAN") Then
                    If SessionInvalid() Then
                        Exit Sub
                    End If
                End If
                Dim customClass As New Parameter.M01
                With customClass
                    .ID = dtgList.DataKeys.Item(e.Item.ItemIndex).ToString
                    .strConnection = GetConnectionString()
                End With

                err = oController.GetM01Delete(customClass)
                If err <> "" Then
                    ShowMessage(lblMessage, err, True)
                Else
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, False)
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub
#End Region
    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgList.ItemDataBound
        Dim imbDelete As ImageButton
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imbDelete"), ImageButton)
            imbDelete.Attributes.Add("Onclick", "return DeleteConfirm()")
        End If
    End Sub
    Protected Sub btnLookup_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnLookup.Click
        KodeKota.CmdWhere = "All"
        KodeKota.Sort = "DatiID ASC"
        KodeKota.Popup()
    End Sub

    Protected Sub oKodeKota_CatSelected(ByVal DatiID As String, ByVal namakabkot As String)
        txtkdkabkot.Text = DatiID
    End Sub


#Region "Save"
    Private Sub ButtonSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
        Dim oParameter As New Parameter.M01
        Dim errMessage As String = ""

        If Me.Process = "ADD" Then
            With oParameter

                .FlagDetail = "D"
                .BulanData = txtbulandata.Text
                .NoIDPengurus = txtnoidentitas.Text
                .CIF = txtCIF.Text
                .KodeJenisIdentitas = cboKdJnsIdentitas.SelectedValue.Trim()
                .NamaPengurus = txtnmpengurus.Text
                .JenisKelamin = cbojeniskelamin.SelectedValue.Trim()
                .Alamat = txtalamat.Text
                .Kelurahan = txtkelurahan.Text
                .Kecamatan = txtkecamatan.Text
                .KodeKota = txtkdkabkot.Text
                .KodeJabatan = cbokdjabatan.SelectedValue.Trim()
                .PangsaKepemilikan = txtpangsa.Text
                .StatusPengurus = cbostatuspemilik.SelectedValue.Trim()
                .Alamat = txtalamat.Text
                .KodeCabang = txtKodeCabang.Text
                .OperasiData = txtOperasiData.Text

                .strConnection = GetConnectionString()
            End With
            oParameter = oController.GetM01Add(oParameter)
        ElseIf Me.Process = "EDIT" Then
            With oParameter

                .ID = txtid.Text
                .BulanData = txtbulandata.Text
                .NoIDPengurus = txtnoidentitas.Text
                .CIF = txtCIF.Text
                .KodeJenisIdentitas = cboKdJnsIdentitas.SelectedValue.Trim()
                .NamaPengurus = txtnmpengurus.Text
                .JenisKelamin = cbojeniskelamin.SelectedValue.Trim()
                .Alamat = txtalamat.Text
                .Kelurahan = txtkelurahan.Text
                .Kecamatan = txtkecamatan.Text
                .KodeKota = txtkdkabkot.Text
                .KodeJabatan = cbokdjabatan.SelectedValue.Trim()
                .PangsaKepemilikan = txtpangsa.Text
                .StatusPengurus = cbostatuspemilik.SelectedValue.Trim()
                .Alamat = txtalamat.Text
                .KodeCabang = txtKodeCabang.Text
                .OperasiData = txtOperasiData.Text

                .strConnection = GetConnectionString()
            End With
            oParameter = oController.GetM01Save(oParameter)
        End If

        Me.SearchBy = ""
        Me.SortBy = ""
        DoBind(Me.SearchBy, Me.SortBy)
        pnlAdd.Visible = False
        pnlList.Visible = True

        If Me.Process = "ADD" Then
            ShowMessage(lblMessage, "Simpan data berhasil.....", False)
        Else
            ShowMessage(lblMessage, "Updated data berhasil .....", False)
        End If
    End Sub
#End Region
#Region "Generate Data"
    Public Function GetCopy(ByVal customclass As Parameter.M01) As Parameter.M01
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter("@BULANDATA", SqlDbType.VarChar, 20)
            params(0).Value = customclass.BulanData

            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spSLIKM01Generate", params).Tables(0)
            Return customclass
        Catch exp As Exception
        End Try
        ShowMessage(lblMessage, "Generate Data Berhasil......", False)
    End Function

    Private Sub btncopybulandata_Click(sender As Object, e As EventArgs) Handles btncopybulandata.Click
        Dim oEntities As New Parameter.M01
        Dim dtViewData As New DataTable
        With oEntities
            .BulanData = txtcopybulandata.Text
            .strConnection = GetConnectionString()
        End With
        GetCopy(oEntities)
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region
#Region "Add"
    Private Sub imbAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonAdd.Click
        If CheckFeature(Me.Loginid, "M01", "ADD", "MAXILOAN") Then
            pnlAdd.Visible = True
            pnlList.Visible = False
			txtbulandata.Enabled = True
			txtCIF.ReadOnly = False
			txtKodeCabang.ReadOnly = False

			Me.Process = "ADD"
            'clean()
            'DoBind(Me.SearchBy, Me.SortBy)
        End If
    End Sub
#End Region
#Region "EXPORT"
    Public Sub ExportTableData(ByVal dtdata As DataTable, ByVal Rc As Long, ByVal nm As String)
        Dim oParameter As New Maxiloan.Parameter.M01

        Dim ResClient As New StringBuilder
        Using sw As New StringWriter(ResClient)

            Dim Header = ("H|0201|251230|" & cbobulandata.Text.Substring(0, cbobulandata.Text.Length - 2) & "|" & cbobulandata.Text.Substring(4) & "|" & "M01" & "|" & Rc & "|" & Rc)
            sw.WriteLine(Header)
            For Each drow As DataRow In dtdata.Rows
                Dim lineoftext = String.Join("|", drow.ItemArray.Select(Function(s) s.ToString).ToArray)
                sw.WriteLine(lineoftext)
            Next

            Response.Clear()
            Response.Buffer = True
            Response.Charset = ""
            Response.ContentType = "text/plain"
            Response.AddHeader("content-disposition", "attachment;filename=" & nm & ".txt")
            Response.Write(ResClient.ToString())

            Using MyMemoryStream As New MemoryStream()
                MyMemoryStream.WriteTo(Response.OutputStream)
                Response.Flush()
                Response.End()
            End Using

        End Using
    End Sub

    Public Function GetToTXT(ByVal customclass As Parameter.M01) As Parameter.M01
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter("@BULANDATA", SqlDbType.VarChar, 20)
            params(0).Value = customclass.BulanData
            params(1) = New SqlParameter("@Totalrecords", SqlDbType.BigInt)
            params(1).Direction = ParameterDirection.Output

            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spSLIKM01Txt", params).Tables(0)
            customclass.TotalRecord = CType(params(1).Value, Int64)
            Return customclass
        Catch exp As Exception
        End Try
    End Function

    Private Sub BtnExportToTxt_Click(sender As Object, e As System.EventArgs) Handles BtnExportToTxt.Click
        Dim oEntities As New Parameter.M01
        Dim dtViewData As New DataTable

        If cbobulandata.Text <> "" Then
            With oEntities
                .BulanData = cbobulandata.Text
                .strConnection = GetConnectionString()
            End With
            oEntities = GetToTXT(oEntities)
            dtViewData = oEntities.ListData
            Rc = oEntities.TotalRecord
            ExportTableData(dtViewData, Rc, "0201.251230." & oEntities.BulanData.Substring(0, oEntities.BulanData.Length - 2) & "." & oEntities.BulanData.Substring(4) & "." & "M01.01")
        Else
            ShowMessage(lblMessage, "Harap Pilih BulanData .....", True)
        End If
    End Sub
    Private Sub btnfind_Click(sender As Object, e As EventArgs) Handles btnfind.Click
		If cbobulandata.Text <> "" Then
			Me.SearchBy = "BULANDATA" & " like '%" & cbobulandata.Text.Trim & "%'"

			Me.SortBy = ""
			DoBind(Me.SearchBy, Me.SortBy)
		Else
			Me.SearchBy = ""

			Me.SortBy = ""
			DoBind(Me.SearchBy, Me.SortBy)
		End If

		If cbobulandata.Text <> "" Then

			Dim dateString, format As String
			Dim result As Date
			Dim provider As Globalization.CultureInfo = Globalization.CultureInfo.InvariantCulture

			' Untuk Parsing EOMONTH ke textbox generate
			dateString = (cbobulandata.SelectedValue)
			format = "yyyyMM"
			result = Date.ParseExact(dateString, format, provider)

			txtcopybulandata.Text = result.AddMonths(1).AddDays(-1).ToString("yyy-MM-dd")
		End If
		clean()
	End Sub
#End Region

#Region "Search"
	Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click

		If cboSearch.Text <> "" Then
			Me.SearchBy = cboSearch.SelectedItem.Value & " like '%" & Replace(txtSearch.Text.Trim, "'", "''") & "%'"

			Me.SortBy = ""
			DoBind(Me.SearchBy, Me.SortBy)
		Else
			Me.SearchBy = ""

			Me.SortBy = ""
			DoBind(Me.SearchBy, Me.SortBy)
		End If
	End Sub
#End Region

#Region "Reset"
	Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
		cboSearch.SelectedIndex = 0
		cbobulandata.SelectedIndex = 0
		txtSearch.Text = ""
		Me.SearchBy = ""
		DoBind(Me.SearchBy, Me.SortBy)
		clean()
	End Sub

	Private Sub btnGenerateUlang_Click(sender As Object, e As EventArgs) Handles btnGenerateUlang.Click
		btnGenerateUlang.Visible = False
		pnlcopybulandata.Visible = True
	End Sub
#End Region
End Class