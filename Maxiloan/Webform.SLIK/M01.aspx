﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="M01.aspx.vb" Inherits="Maxiloan.Webform.SLIK.M01" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %>
<%@ Register TagPrefix="uc5" TagName="uckodekota" Src="../Webform.UserController/uckodekota.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
        <title>Pengurus/Pemilik Debitur Badan Usaha</title>
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />
    <link href="../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <script src="../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script> 
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%# Request.ServerVariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%#Request.ServerVariables("SERVER_NAME")%>/';			
    
        function fBack() {
            history.back(-1);
            return false;
        }
        function DeleteConfirm() {
            if (confirm("Apakah yakin mau hapus data ini ?")) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
        <div class="form_title">
            <div class="title_strip"></div>
            <div class="form_single">
                <h3>
                    SLIK M01 - PENGURUS/PEMILIK DEBITUR BADAN USAHA
                </h3>
            </div>
        </div>
            <asp:Panel ID="pnlList" runat="server">

                <div class="form_box_header">
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="medium_text">
                                Pilih Bulan Data
                        </label>                           
                            <asp:DropDownList ID="cbobulandata" runat="server" Width ="7%" CssClass="medium_text"/>       
                    </div>              
                </div>
                <div class="form_button">
                    <asp:Button ID="btnfind" runat="server" CausesValidation="False" Text="Find" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="btnGenerateUlang" runat="server" CausesValidation="False" Text="Generate Ulang" CssClass="small button green">
                    </asp:Button>
                </div>

                <asp:Panel ID="pnlcopybulandata" runat="server">
                    <div class="form_box_header">
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label class="medium_text">
                                    Generate Bulan Data
                            </label>
                                <%--<asp:DropDownList ID="cbocopybulandata" runat="server" Width ="7%" CssClass="medium_text"/>--%>   
                            <asp:TextBox ID="txtcopybulandata" runat="server" CssClass="medium_text" width="6%"></asp:TextBox>
                            <asp:CalendarExtender ID="CalendarExtender4" runat="server" Enabled="True"
                                TargetControlID="txtcopybulandata" Format="yyyy-MM-dd" PopupPosition="BottomRight"></asp:CalendarExtender>  
                        </div>              
                    </div>
                <div class="form_button">
                    <asp:Button ID="btncopybulandata" runat="server" CausesValidation="False" Text="Generate" CssClass="small button green">
                    </asp:Button>
                </div>
                </asp:Panel>

        
        <div class="form_box_title" style="margin-top: 3px;" >
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgList" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                        DataKeyField="ID" BorderStyle="None" BorderWidth="0" OnSortCommand="SortGrid"
                        CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="EDIT">
                                <ItemStyle Width="2%" HorizontalAlign="Center" CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../Images/iconedit.gif"
                                        CommandName="Edit"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn  HeaderText="DELETE">
                                <ItemStyle  Width="5%" HorizontalAlign="Center" CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbDelete" runat="server" ImageUrl="../Images/icondelete.gif"
                                        CommandName="DEL"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="ID" SortExpression="ID" HeaderText="ID" visible="false">
                            </asp:BoundColumn> 
                            <asp:BoundColumn DataField="BULANDATA" SortExpression="BULANDATA" HeaderText="BULAN DATA" ItemStyle-Width="7%">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="NoIdPengurus" SortExpression="NoIdPengurus" HeaderText="Nomor ID Pengurus">
                            </asp:BoundColumn>
                             <asp:BoundColumn DataField="NamaPengurus" SortExpression="NamaPengurus" HeaderText="Nama Pengurus">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="PangsaKepemilikan" SortExpression="PangsaKepemilikan" DataFormatString="{0:00.00} %" HeaderText="Saham (%)" ItemStyle-HorizontalAlign="right">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="Alamat" SortExpression="Alamat" HeaderText="Alamat">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="NAME" SortExpression="NAME" HeaderText="Nama Debitur">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="OperasiData" SortExpression="OperasiData" HeaderText="Operasi Data">
                            </asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <uc2:ucGridNav id="GridNaviM01SLIK" runat="server"/>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonAdd" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtnExportToTxt" runat="server"  Text="Export To TXT" CssClass="small button green"></asp:Button>               
        </div>

        <div class="form_box_title" style="margin-top: 3px;" >
            <div class="form_single">
                <h5>
                    CARI DEBITUR
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>Cari Berdasarkan</label>
                <asp:DropDownList ID="cboSearch" runat="server">
                    <asp:ListItem Value="NamaPengurus" Selected="True">Nama Pengurus</asp:ListItem>
                    <asp:ListItem Value="NoIdPengurus">ID Pengurus</asp:ListItem>
                    <%--<asp:ListItem Value="NIK">NIK Debitur</asp:ListItem>--%>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearch" runat="server" Width="20%"></asp:TextBox>
            </div>
        </div>
        <div class="form_button" style="padding-bottom: 0; padding-top: 3px;"> 
             <asp:Button ID="btnSearch" runat="server" Text="Find" CssClass="small button blue"
                        CausesValidation="False"></asp:Button>
             <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="small button gray"
                        CausesValidation="False"></asp:Button>
        </div>
     </asp:Panel>
    
<asp:Panel ID="pnlAdd" runat="server">
 <asp:TextBox ID="txtid" runat="server" CssClass="medium_text" Visible="false"></asp:TextBox>

        <div class="form_box">
            <div class="form_single">
                <label class="medium_text">
                        BulanData
                </label>
                <asp:TextBox ID="txtbulandata" runat="server" CssClass="medium_text" Width="6%" ></asp:TextBox>
                <asp:CalendarExtender ID="CalendarExtender5" runat="server" Enabled="True" TargetControlID="txtbulandata" Format="yyyyMM" PopupPosition="BottomRight" DefaultView="Months"></asp:CalendarExtender>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                    ControlToValidate="txtbulandata" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>

                <asp:TextBox ID="txtFlag" runat="server" CssClass="medium_text"  Width ="2%" MaxLength ="100" Columns ="105"  Readonly="true" visible="false"></asp:TextBox>

     <div class="form_box">
			<div class="form_single">
				<label>
                           No Identitas Pengurus
                </label>
                <asp:TextBox ID="txtnoidentitas" runat="server" CssClass="medium_text"  Width ="10%" MaxLength ="100" Columns ="105"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator3" runat="server" display="Dynamic"
                    ControlToValidate="txtnoidentitas" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
			</div>
		</div>

		<div class="form_box">
			<div class="form_single">
				<label>
                           CIF
                </label>
                <asp:TextBox ID="txtCIF" runat="server" CssClass="medium_text" Width ="10%" MaxLength ="100" Columns ="105"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" Display="Dynamic"
                    ControlToValidate="txtCIF" CssClass="validator_general" ErrorMessage="*"></asp:RequiredFieldValidator>
			</div>
		</div>

		<div class="form_box">
			<div class="form_single">
				<label>
                           Kode Jenis Identitas
                </label>
                
                <asp:DropDownList ID="cboKdJnsIdentitas" runat="server" Width ="10%">
                    <asp:ListItem Enabled="true" Text="Select One" Value=" "></asp:ListItem>
                    <asp:ListItem Text="1 - KTP" Value="1"></asp:ListItem>
                    <asp:ListItem Text="2 - PASPOR" Value="2"></asp:ListItem>
                    <asp:ListItem Text="3 - NPWP" Value="3"></asp:ListItem>
                    <asp:ListItem Text="9 - LAINNYA" Value="9"></asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator4" runat="server" display="Dynamic"
                    ControlToValidate="cboKdJnsIdentitas" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
			</div>
		</div>

		<div class="form_box">
			<div class="form_single">
				<label>
                           Nama Pengurus/Pemilik Badan usaha
                </label>
                <asp:TextBox ID="txtnmpengurus" runat="server" CssClass="medium_text"  Width ="15%" MaxLength ="100" Columns ="105" ReadOnly="false"></asp:TextBox>
               <asp:RequiredFieldValidator ID="Requiredfieldvalidator6" runat="server" display="Dynamic"
                    ControlToValidate="txtnmpengurus" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
			</div>
		</div>

        <div class="form_box">
			<div class="form_single">
				<label>
                          Jenis Kelamin
                </label>
                
                <asp:DropDownList ID="cbojeniskelamin" runat="server" Width ="13%">
                    <asp:ListItem Enabled="true" Text="Select One" Value=" "></asp:ListItem>
                    <asp:ListItem Text="L - Laki-laki" Value="L"></asp:ListItem>
                    <asp:ListItem Text="P - Perempuan" Value="P"></asp:ListItem>
                    <asp:ListItem Text="B - Pemilik Adalah Badan Usaha" Value="B"></asp:ListItem>
                    <asp:ListItem Text="M - Pemilik Adalah Masyarakat" Value="M"></asp:ListItem>
                </asp:DropDownList>
               <asp:RequiredFieldValidator ID="Requiredfieldvalidator8" runat="server" display="Dynamic"
                    ControlToValidate="cbojeniskelamin" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
			</div>
		</div>   

        <div class="form_box">
			<div class="form_single">
				<label>
                          Alamat
                </label>
                <asp:TextBox ID="txtalamat" runat="server" CssClass="medium_text"  Width ="40%" MaxLength ="100" Columns ="105" ReadOnly="false"></asp:TextBox>
               <asp:RequiredFieldValidator ID="Requiredfieldvalidator9" runat="server" display="Dynamic"
                    ControlToValidate="txtalamat" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
			</div>
		</div>   

		<div class="form_box">
			<div class="form_single">
				<label>
                           Kelurahan
                </label>
                <asp:TextBox ID="txtkelurahan" runat="server" CssClass="medium_text"   Width ="10%" MaxLength ="100" Columns ="105" ReadOnly="false"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator11" runat="server" display="Dynamic"
                    ControlToValidate="txtkelurahan" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
			</div>
		</div>

		<div class="form_box">
			<div class="form_single">
				<label>
                           Kecamatan
                </label>
                <asp:TextBox ID="txtkecamatan" runat="server" CssClass="medium_text"  Width ="10%" MaxLength ="100" Columns ="105" ReadOnly="false"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator12" runat="server" display="Dynamic"
                    ControlToValidate="txtkecamatan" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
			</div>
		</div>

<%--		<div class="form_box">
			<div class="form_single">
				<label>
                           Kode Kabupaten/Kota
                </label>
                <asp:TextBox ID="txtkdkabkot" runat="server" CssClass="medium_text" Width ="10%" MaxLength ="100" Columns ="105" ReadOnly="false"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator13" runat="server" display="Dynamic"
                    ControlToValidate="txtkdkabkot" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
			</div>
		</div>--%>

        <asp:updatepanel runat="server">
        <ContentTemplate>
		<div class="form_box">
			<div class="form_single">
				<label>
                           Kode Kabupaten/Kota
                </label>
                <asp:TextBox ID="txtkdkabkot" runat="server" CssClass="medium_text"  Width ="5%" MaxLength ="100" Columns ="105" ReadOnly="false"></asp:TextBox>
			    <asp:Button ID="btnLookup" runat="server" CausesValidation="False" Text="..." CssClass="small buttongo blue" />
                <uc5:UcKodeKota id="KodeKota" runat="server" OnCatSelected="oKodeKota_CatSelected"></uc5:UcKodeKota>
            </div>
		</div>
        </ContentTemplate>
        </asp:updatepanel>

		<div class="form_box">
			<div class="form_single">
				<label>
                          Kode Jabatan
                </label>
                <asp:DropDownList ID="cbokdjabatan" runat="server" Width ="30%">
                    <asp:ListItem Enabled="true" Text="Select One" Value=" "></asp:ListItem>
                    <asp:ListItem Text="01 - PEMILIK - Direktur Utama atau Presiden Direktur " Value="01"></asp:ListItem>
                    <asp:ListItem Text="02 - PEMILIK - Direktur" Value="02"></asp:ListItem>
	                <asp:ListItem Text="03 - PEMILIK - Komisaris Utama atau Presiden Komisaris" Value="03"></asp:ListItem>
	                <asp:ListItem Text="04 - PEMILIK - Komisaris" Value="04"></asp:ListItem>
	                <asp:ListItem Text="05 - PEMILIK - Kuasa Direksi" Value="05"></asp:ListItem>
	                <asp:ListItem Text="06 - PEMILIK - Pemilik Bukan Pengurus" Value="06"></asp:ListItem>
	                <asp:ListItem Text="07 - PEMILIK - Masyarakat" Value="07"></asp:ListItem>
	                <asp:ListItem Text="08 - PEMILIK - Ketua Umum" Value="08"></asp:ListItem>
	                <asp:ListItem Text="09 - PEMILIK - Ketua" Value="09"></asp:ListItem>
	                <asp:ListItem Text="10 - PEMILIK - Sekretaris" Value="10"></asp:ListItem>
	                <asp:ListItem Text="11 - PEMILIK - Bendahara" Value="11"></asp:ListItem>
	                <asp:ListItem Text="12 - PEMILIK - Lainnya" Value="12"></asp:ListItem>
	                <asp:ListItem Text="13 - BUKAN PEMILIK - Direktur Utama atau Presiden Direktur " Value="13"></asp:ListItem>
	                <asp:ListItem Text="19 - BUKAN PEMILIK - Direktur" Value="19"></asp:ListItem>
	                <asp:ListItem Text="51 - BUKAN PEMILIK - Komisaris Utama atau Presiden Komisaris" Value="51"></asp:ListItem>
	                <asp:ListItem Text="52 - BUKAN PEMILIK - Komisaris" Value="52"></asp:ListItem>
	                <asp:ListItem Text="53 - BUKAN PEMILIK - Kuasa Direksi" Value="53"></asp:ListItem>
	                <asp:ListItem Text="54 - BUKAN PEMILIK - Pemilik Bukan Pengurus" Value="54"></asp:ListItem>
	                <asp:ListItem Text="55 - BUKAN PEMILIK - Masyarakat" Value="55"></asp:ListItem>
	                <asp:ListItem Text="57 - BUKAN PEMILIK - Ketua Umum" Value="57"></asp:ListItem>
	                <asp:ListItem Text="58 - BUKAN PEMILIK - Ketua" Value="58"></asp:ListItem>
	                <asp:ListItem Text="59 - BUKAN PEMILIK - Sekretaris" Value="59"></asp:ListItem>
	                <asp:ListItem Text="60 - BUKAN PEMILIK - Bendahara" Value="60"></asp:ListItem>
	                <asp:ListItem Text="69 - BUKAN PEMILIK - Lainnya" Value="69"></asp:ListItem>
                </asp:DropDownList>                
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator14" runat="server" display="Dynamic"
                    ControlToValidate="cbokdjabatan" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
                </div>
		</div>

		<div class="form_box">
			<div class="form_single">
				<label>
                           Pangsa Kepemilikan
                </label>
                <asp:TextBox ID="txtpangsa" runat="server" CssClass="medium_text"  Width ="2%" MaxLength ="100" Columns ="105" ReadOnly="false"></asp:TextBox>%
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator19" runat="server" display="Dynamic"
                    ControlToValidate="txtpangsa" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
			</div>
		</div>

		<div class="form_box">
			<div class="form_single">
				<label>
                           Status Pengurus/Pemilik Debitur Badan Usaha
                </label>
               
                <asp:DropDownList ID="cbostatuspemilik" runat="server" Width ="10%">
                    <asp:ListItem Enabled="true" Text="Select One" Value=" "></asp:ListItem>
                    <asp:ListItem Text="1 - Aktif" Value="1"></asp:ListItem>
                    <asp:ListItem Text="2 - Telah Berakhir" Value="2"></asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator20" runat="server" display="Dynamic"
                    ControlToValidate="cbostatuspemilik" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
			</div>
		</div>

        <div class="form_box">
			<div class="form_single">
				<label>
                           Kode Kantor Cabang
                </label>
                <asp:TextBox ID="txtKodeCabang" runat="server" CssClass="medium_text"  Width ="2%" MaxLength ="100" Columns ="105" ReadOnly="true"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator15" runat="server" display="Dynamic"
                    ControlToValidate="txtKodeCabang" CssClass="validator_general" ErrorMessage="*" ></asp:RequiredFieldValidator>
			</div>
		</div>

		 <asp:TextBox ID="txtOperasiData" runat="server" CssClass="medium_text" Width ="2%" MaxLength ="100" Columns ="105" visible="false"></asp:TextBox>

    <div class="form_button">
        <asp:Button ID="ButtonSave" runat="server" Text="Save" CssClass="small button blue"></asp:Button>
        <asp:Button ID="ButtonCancel" runat="server" CausesValidation="false" Text="Cancel" CssClass="small button gray"></asp:Button>
    </div>
          </asp:Panel>
  </form>
</body>
</html>
